Für das Editieren der Hilfeseiten haben wir uns nun doch auf MarkDown geeinigt
Visual Studio Code herunterladen = https://code.visualstudio.com/download
Starten, bei der Frage nach den Themes habe ich solarized Light eingestellt. 
Ist am ehesten für mich lesbar.
Nun brauch man noch md Files die man dann editieren kann.
Die Kieselstein User-Doku beginnt mit kieselstein.md 

Wenn man nun das Preview Fenster auch sehen will, 
so aus dem Text-Editor Strg+K und dann V drücken. 
So kommt rechts ein zweites Fenster in dem man die Preview sieht.
Achtung: jetzt wieder in das linke Editor Fenster wechseln, sonst ändert man den Inhalt des Previews, was man ja nicht will.
Also immer im rechten Fenster arbeiten.

lt. Kajetan Fuchsberger die Drawio Plugins noch einrichten
Damit die Bilder mit <Filename>.drawio.svg benennen
Das sollte dann gut gehen.

Um die Hilfe als statische HTML Seiten zu haben werden wir Hugo von Google einsetzen.
Dafür ist der "Hugo-Server" auf dem jeweiligen Rechner einzurichten.
Der Vorteil ist, dass man quasi sofort sieht wie die eigenen Seiten aussehen.
Von der Struktur her, liegt die eigentliche Hilfe unter d:\Git_KES\user-doc\content\docs\
Der Hugo ist so parametriert, dass die _index.md kommt und dann alle anderen.
Diese werden automatisch dazugehängt.

D.h. in jedem Verzeichniss muss ein _index.md sein, damit diese auch interpretiert wird.
Gibt es diese nicht, wird dieses Verzeichnis und seine ganzen Unterverzeichnisse NICHT aufgelöst.
Ich habe dies für die Beispieldateien verwendet. Damit sind diese da, aber nicht sichtbar.

Hugo Commandos usw.
https://gohugo.io/content-management/formats/
da ganz unten, Learn Markdown