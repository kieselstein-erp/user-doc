---
categories: ["Bedienung"]
tags: ["freie Texte"]
title: "freie Texte"
linkTitle: "freie Texte"
weight: 100
description: >
 Die Verwendung von Texten und Bildern in deinem **Kieselstein ERP**
---
Freie Texte zu Belegen / Modulen
================================

Freie Texte (Textbausteine), Texte bei den Modulen, Texteingaben.

In **Kieselstein ERP** unterscheiden wir drei grundsätzlich verschiedene Arten, wie Texte an die Belege angebunden werden können.

1.  Texteingaben
    In jedem Bewegungsmodul steht Ihnen die Positionsart Texteingabe zur Verfügung. Mit dieser können Sie individuelle Texte in diesem einen spezifischen Beleg erstellen.

2.  Textbausteine
    In jedem Bewegungsmodul steht auch die Positionsart Textbaustein zur Verfügung. Damit können Sie aus einem Fundus von Textbausteinen auswählen, die Sie für immer wiederkehrende Formulierungen, welche aber doch einer individuellen Zusammenstellung, je nach Inhalt, bedarf benötigen. Diese Textbausteine können unter System, Medien angelegt werden. Sie haben hier auch die Möglichkeit anstelle eines formatierten Textes Bilder in **Kieselstein ERP** einzupflegen, welche dann bei den Bewegungsmodulen entsprechend zur Verfügung stehen.

3.  Texte direkt bei den Modulen
    In Verschiedenen Modulen, können auch interne und externe Kommentare hinterlegt werden.
    Wie die Bezeichnungen bereits andeuten ist der interne Kommentar nur für interne Zwecke gedacht. Der externe Kommentar wird jeweils auf den offiziellen Belegen, z.B. der Auftragsbestätigung mitgedruckt.

Zusätzlich gibt es noch die Funktionalität der sogenannten Kopf- und Fußtexte.

Diese werden wiederum unterschieden in Standard Kopf- und Fußtexte und in die individuell übersteuerten Kopf- und Fußtexte je Beleg. Die Standard Kopf- und Fußtexte können im jeweiligen Modul, unterer Modulreiter Grunddaten, z.B. Angebotstext definiert werden. Die Übersteuerung des Standardtextes wird in den Konditionen des jeweiligen Beleges vorgenommen.

Damit können Sie die Standardfloskeln z.B. für das Angebot allgemein definieren und im Bedarfsfalle eine individuelle Anpassung, nur für ein bestimmtes Angebot vornehmen.

Bitte beachten Sie, dass diese Funktionalitäten in nahezu allen Bewegungsmodulen und wenn passend auch in fast allen Stammdatenmodulen enthalten sind. In den Stammdatenmodulen sind diese als Bemerkung bzw. Kommentar bezeichnet. Siehe bitte direkt bei den Stammdatenmodulen.

Bei der Texteingabe kann grundsätzlich frei formatiert werden. Ebenso können Tabulatoren für die Formatierung von z.B. Aufzählungen verwendet werden.

[Siehe bitte auch]( {{<relref "/verkauf/angebot/#fehlerhafter-angebotsdruck" >}} ).

#### Mein Editorfeld hat links eine rote Linie
![](Editorfeld_rote_Linie.jpg)

Wird im Editor Eingabefeld links eine rote Linie angezeigt, so bedeutet dies, dass die Textmenge bereits die speicherbare Menge der Zeichen übersteigt. Kürzen Sie ihren Text und auch die Formatierungen, bis die rote Line wieder verschwindet.

**Hinweis:**

Beachten Sie bitte, dass für die Formatierung auch Speicherplatz benötigt wird. Es kann also durchaus vorkommen, dass sie deutlich weniger Zeichen eingegeben haben und der Speicherplatz trotzdem erschöpft ist. Dies kommt dann vor, wenn Sie entsprechend viel (unsichtbare) Formatierung im Text vorgenommen haben.

#### Am Ausdruck kommen - oder _, die ich im Editor nicht sehen kann
Gehen Sie an die Stelle im Editor, an der der Strich am Ausdruck steht und schauen Sie nach, ob das Unterstreichen (Underline) eingestellt ist, oder nicht. Z.B. in dem Sie sich mit den Pfeiltasten bewegen und das Unterstreichen-Symbol beobachten. Es kann dies insbesondere bei Leerzeilen manchmal unabsichtlich vorkommen.

<a name="Breite_Texteditor_Einstellen"></a>

#### Wie passen die Breite des Texteditors und die Breite des Ausdrucks zusammen?
Es hat sich in der Praxis bewährt,

- a.) die Breite der Texte einheitlich über alle Formulare zu gestalten. Insbesondere wenn Sie speziell an Ihre Bedürfnisse angepasste Formulare verwenden, so achten Sie bitte darauf dass die freien Texte (Feldname im Report F_FREIERTEXT) immer die gleiche Breite haben.

    Abhängig von der Nutzung der Text-Blöcke gibt es verschiedene Parameter, die die Breite des Textes in Pixel, welche damit zu den Formularen passen sollten, definiert wird.
    Diese werden nur beim Neustart des Clients neu geladen.
    Die Parameter sind:
    - EDITOR_BREITE_KOMMENTAR
    - EDITOR_BREITE_SONSTIGE
    - EDITOR_BREITE_TEXTEINGABE
    - EDITOR_BREITE_TEXTMODUL

#### Größenbeschränkung der Texteingaben
<a name="Größe der Texteingaben"></a>
Bitte beachten Sie, dass in **Kieselstein ERP** aus Datenbankgründen fast alle Texteingabefelder auf 3.000 Zeichen begrenzt sind. In dieser Anzahl werden auch die Formatierungssteuerzeichen mitgerechnet.

Für Texteingaben z.B. in Angeboten usw. verwenden Sie daher einfach mehrere Texteingaben. Die Bemerkungen in Kunden / Lieferanten / Partner sind nur für sehr wichtige Hinweise gedacht. Für die Abbildung von Verkaufsaktivitäten verwenden Sie bitte eines der folgenden Module:
- Kurzbrief
- Kontakt(-management)
- Projekt

#### Einfügen von Signaturen
[Siehe]( {{<relref "/docs/stammdaten/personal/#signaturen" >}} )

#### Grundeinstellung des Editors
Der Arbeitsplatzparameter EDITOR_ZOOM steuert die Grundeinstellung der Zoom-Ansicht im Editor.
Mit dem Arbeitsplatzparameter EDITOR_SCHRIFT_GROESSE und EDITOR_SCHRIFTART können die Schriftart und Schriftgröße als Grundeinstellung definiert werden. Die Schriftart kann mit Eingabe des Namens gewählt werden, auch in Kleinschreibung. Alles was nicht explizit eine (andere) Schrift zugewiesen hat, wird in dieser Schrift dargestellt. Wird eine unbekannte Schrift eingegeben, wird die Standardschrift (Arial) verwendet. Wenn Sie nun bei der Texteingabe eine andere Schriftart verwendet haben, so können Sie mit Hilfe des Radiergummis ![](radiergummi.png)  wieder zurück zur Basiseinstellung aus den Arbeitsplatzparametern wechseln.

#### Schriften / Fonts
Das Thema Schriften / Fonts beschäftigt alle die mit verteilten Systemen arbeiten.
So ist es auch mit **Kieselstein ERP**, dass:

Aufgrund der Report-Aufbereitung am Server und der Darstellung am entsprechenden Client einige Voraussetzungen erfüllt sein müssen, damit die gewünschten Schriften / Fonts durchgängig angezeigt und verwendet werden können.

Kurz Zusammengefasst muss die gewünschte Schrift, z.B. eine Unternehmensschrift, in allen beteiligten Systemen zur Verfügung stehen.

Daher wurde die Fontauswahl in Ihrem **Kieselstein ERP** Client soweit eingeschränkt, dass nur die Fonts, die sowohl für Ihren **Kieselstein ERP** Server als auch dem Rechner auf dem Ihr **Kieselstein ERP** Client läuft, zur Verfügung stehen, verwendet werden können. D.h. sollten Sie in der Font-Auswahl ![](FontAuswahl.gif) die gewünschte Schrift NICHT vorfinden, so ist diese wahrscheinlich auf Ihrem **Kieselstein ERP** Server nicht installiert. Bitten Sie Ihren Systemadministrator diese nachzuinstallieren. Gleiches gilt in der anderen Richtung. D.h. sollte eine Schrift zwar am Server verfügbar sein, diese ist aber am gewünschten Client nicht verfügbar, so kann diese ebenfalls nicht verwendet werden.

Da es trotzdem vorkommen kann, dass Formulare / Texte mit Schriften abgespeichert werden, welche nicht (mehr) vorhanden sind, gibt es gegebenenfalls auch entsprechende Fehlermeldungen.

So bedeutet nachfolgende Fehlermeldung, welche beim Ausdruck eines Formulars auftritt

![](Schrift_am_Server_nicht_verfuegbar.jpg)

dass die im Formular abgespeicherte Schrift auf Ihrem **Kieselstein ERP** Server nicht zur Verfügung steht. Korrigieren Sie bitte den Inhalt des Ausdruckes oder installieren Sie die Schrift nach.

Bitte achten Sie bei dieser Meldung darauf ob die Schrift am Server oder am Client nicht verfügbar ist, um den richtigen Platz für das nachinstallieren zu definieren.

Da umgekehrt möglicherweise Schriften am Server eingerichtet sind, bevorzugt Schriften zur Corporate Identity passend, die auf einem (neuen) Client (noch) nicht installiert sind, gibt es einen ähnlichen Hinweis wenn die Schrift am Client nicht zur Verfügung steht. Das bedeutet, bitte beachten Sie die Fehlermeldung genau, soweit möglich wird auch ein Textausschnitt mit angegeben, sodass Sie erkennen können in welchem Textbaustein eventuell diese Schrift definiert wurde.

Da in den Textbausteinen ebenfalls verschiedenste Schriften ausgewählt werden können, z.B. auch durch Copy & Paste von anderen Systemen, sollte der Hinweis genutzt werden.

**Hinweis:** Beachten Sie vor der Definition von Schriften, dass diese von Ihnen auch aus Lizenzsicht verwendet werden können. Sehr oft gehen Anwender davon aus, dass die auf Ihrem Betriebssystem installierten Schriften überall verwendet werden dürfen. 

Dem ist leider nicht so. 

Gewisse große Konzerne sind bei Lizenzverletzungen nicht zimperlich. Prüfen Sie bitte daher unbedingt vorher.

#### Löschen von Inhalten in Textmodulen
Manchmal kommt es vor, dass Inhalten von Textmodulen soweit gelöscht werden sollten, dass der verbleibende Inhalt wirklich nur mehr ein Zeichen ist.
Um dies zu erreichen, gehen Sie bitte wie folgt vor:
- a.) in den Texteditor rein gehen und mit dem Radiergummi die Styleinformationen rücksetzen
- b.) den bestehenden Text komplett entfernen
- c.) auf speichern klicken und mit ESC oder der Tür heraus gehen und erneut speichern
- d.) Es kommt die Fehlermeldung Pflichtfeld ausfüllen
- e.) wieder rein gehen und z.B. den . einfügen
- f.) Speichern, Raus, speichern
- g.) man sieht nun nur mehr den . in den (Lieferschein) Kopftexten.

#### Einfügen von Textbausteinen
Im Modul System unterer Reiter Medien können Sie Textbausteine definieren.

Um einen vorhandenen Textbaustein in den Text im Editor einzufügen klicken Sie auf das Icon ![](Textbaustein_einfuegen.JPG) Textbaustein am Ende des Dokuments einfügen.

Nach Auswahl des Textbausteins aus der Liste können Sie den Text beliebig bearbeiten und Änderungen individuell für den vorliegenden Beleg vornehmen.

![](auswahl_textbaustein.JPG)

#### Rechtschreibprüfung
<a name="Rechtschreibprüfung"></a>
[Siehe bitte]( {{<relref "/verkauf/gemeinsamkeiten/#aktivieren-der-rechtschreibpr%c3%bcfung" >}} ).
