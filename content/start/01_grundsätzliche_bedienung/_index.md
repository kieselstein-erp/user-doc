---
categories: ["Bedienung"]
tags: ["Bedienung"]
title: "Grundsätzliche Bedienung"
linkTitle: "Bedienung"
weight: 100
description: >
 Erklärung der Bedienung deines **Kieselstein ERP**
---
Allgemeine Hinweise zur Bedienung deines **Kieselstein ERP**
========================================================

Einstieg ![](Einstieg.gif)

Durch Klick auf das Einstiegssymbol gelangen Sie in den Anmeldedialog. Für die Parametrierung der Benutzer usw. siehe Benutzer. Der **Kieselstein ERP** Administrator und das Administrator Passwort wurden Ihrem Systembetreuer bei der Installation übergeben. Dieser ist angehalten auf die Sicherheit des Systems zu achten. Melden Sie sich nun mit Ihrem **Kieselstein ERP** Benutzernamen und Ihrem Passwort am System an.

![](anmeldung.png)  

Durch die Auswahl der UI (Benutzeroberfläche)-Sprache können Sie die Dialoge des Clients in der von ihnen bevorzugten Sprache auswählen. 
Die dafür erforderlichen Übersetzungen erhalten Sie von Ihrem **Kieselstein ERP** Betreuer. [Definitionen zur Mehrsprachigkeit siehe.]( {{<relref "/start/10_mehrsprachigkeit">}} )

Nach der erfolgreichen Anmeldung stehen Ihnen die Module zur Verfügung.

Zugleich werden Sie mit Ihrem bevorzugten Mandanten angemeldet.

Bei der Anmeldung wird der Zeitabgleich zwischen Client und Server geprüft. Ist die Zeitabweichung größer einer Minute, so erscheint die Meldung 

![](zeitunterschied.JPG)

Bitte korrigieren Sie in diesem Falle unbedingt die Zeit Ihres Client Computers da ansonsten insbesondere die Zeitbuchungen der Mitarbeiter falsch angezeigt werden können. Siehe dazu bitte auch [Mit welcher Zeit werden die Daten verbucht](../Zeiterfassung/index.htm#Zeitverbuchung).

![](kein_benutzermandant.png)  

Wird dieser Hinweis angezeigt, so ist der Benutzer zwar in **Kieselstein ERP** grundsätzlich angelegt, es gibt aber keine Zuordnung des Benutzers zu einem Mandanten und zu seiner Systemrolle. Bitte im [Benutzermodul, Benutzermandant](../benutzer/index.htm#Neuer_Benutzer) entsprechend zuordnen.

Maximale Benutzeranzahl überschritten

[Siehe dazu bitte.](#Benutzeranzahl_ueberschritten)

Wo finde ich was?

[Siehe dazu bitte.]( {{<relref "/start/05_wo_finde_ich_was">}} )

Beenden ![](Beenden.gif)

Abmelden ![](Abmelden.gif)

Mandantenwechsel ![](Mandantenwechsel.gif)

Zum Mandantenwechsel müssen zuvor alle geöffneten Module geschlossen werden. Danach können Sie sich bei einem der für Sie freigeschalteten Mandanten anmelden.

## Begriffsbestimmungen:
![](Panel_Erklaerung.png)  

### Start der einzelnen Module

Die einzelnen Module starten Sie entweder durch Klick auf den jeweiligen Button in der Modulauswahl oder über das Hauptmenü, Modul und dann das jeweilige Modul.

### Wechsel zwischen den Modulen
Entweder durch Klick auf die Modul Titelzeile oder durch erneuten Klick auf das jeweilige Modul-Icon oder durch Auswahl über das Hauptmenü mit Angabe der Modulgruppe und dann Auswahl des entsprechenden Moduls.

### Gruppierung der Module
Die Module sind in ihre Verwendungsgruppen untergliedert. Diese orientieren sich wiederum an den fünf Bereichen deines **Kieselstein ERP**.
1. Warenwirtschaft mit Artikel und Stücklisten
2. Einkauf mit Lieferanten, Anfrage, Bestellung und Eingangsrechnung
3. Fertigung mit Fertigungs(Los-)Verwaltung, Zeiterfassung, Personal
4. Verkauf mit Kunden, Angebotsstücklisten, Auftrag, Lieferschein, Rechnung inkl. Gutschriften
5. Management mit der Finanzbuchhaltung
6. Die zusätzlichen Extra Module mit Partner, Benutzer/Rollen/Rechteverwaltung und den Systemdaten

Sollte für Sie die Modul Auswahlleiste an einem anderen Platz besser angebracht sein, so verschieben Sie diese bitte, indem Sie am linken Rand ![](Icon_Leiste_ziehen.gif) der Iconleiste mit der Maus ziehen. Rechtsklick stellt die default Position wieder her.

### Modul Arten
Es werden grundsätzlich drei Arten von Modulen unterschieden:
- a.) Stammdaten wie Artikel, Partner usw.
- b.) Bewegungsdaten wie Aufträge, Bestellungen usw.
- c.) Systemdaten wie Währungen, Kurse, Partnerarten usw.

[Bedienung der Auswahllisten]( {{<relref "/start/02_shortcuts">}} )

#### Unterschied zwischen der oberen und der unteren Menüleiste?
Die Hauptmenüleiste ![](Hauptmenuleiste.png) dient der Bedienung des gesamten **Kieselstein ERP** Clients. Die Modulmenüleiste ![](Modulmenueleiste.png) stellt die Funktionalitäten nur dieses einen Moduls zur Verfügung.

#### Unterschied zwischen (oberen) Modulreiter und unteren Modulreitern?
Die unteren Modulreiter ![](Unterer_Modulreiter.gif)dienen der Auswahl zusätzlicher Modulfunktionen in diesem Modul / in dieser Modulgruppe. So können Sie z.B. im Artikelmodul die Bearbeitung des Artikels auswählen oder z.B. die Preislistennamen erfassen.

Die oberen Modulreiter ![](Oberer_Modulreiter.gif) dienen der Unterteilung der verschiedenen Sichten auf die Daten eines Moduls. Im Artikel z.B. auf die grundsätzlichen Kopfdaten (die Bezeichnungen) oder die Verkaufspreise usw.

**Hinweis für MAC Benutzer**<br>
Die Darstellung der Modulreiter richtet sich nach der am MAC üblichen einzeiligen Anzeige. Es werden am linken und rechten Rand, wenn vorhanden, Pfeile angezeigt, die weitere Modulreiter anzeigen. Durch Klick auf den Pfeil wird der aktive Modulreiter weitergeschaltet. Sehr oft will man aber auch ganz gezielt in einen hinteren Modulreiter wechseln. Klicken Sie dazu auf einen der Pfeile und halten Sie die Maus ein-zwei Sekunden lang gedrückt. Nun werden die weiteren Modulreiter angezeigt und Sie können direkt den gewünschten Reiter auswählen.

![](Modulreiter_MAC.jpg)

#### Reihenfolge der Modulreiter
Grundsätzlich folgt die Anordnung der Dialogmasken der üblichen Abarbeitung. D.h. von Links nach Rechts. Dies gilt für die (oberen) Modulreiter als auch für die Inhalte der Dialoge. D.h. die Auswahl von Optionen in den Dialogen ist auch so gestaltet, dass Links die höherwertigen / grundsätzlichen Auswahlmöglichkeiten angeordnet sind, welche sich nach rechts hin verfeinern oder auch je nach der weiter Links stehenden Auswahl verändern.

So finden Sie immer links die Auswahlliste, gefolgt von den Kopfdaten und den Positionen. Dieser Logik entsprechend finden Sie danach dann normalerweise die Konditionen. Aber: in der Anfrageverwaltung sind die Konditionen nicht eine Folge der Positionen, sondern der Vorgänger zur Erfassung der Lieferdaten.

### Anlegen eines neuen Datensatzes ![](Neu.gif)
Damit wird ein neuer Datensatz initialisiert. Abgespeichert wird die Eingabe jedoch erst durch das Speichern des Datensatzes. Hinweis: Beachten Sie in den Bewegungsdaten bitte, dass die eigentliche Nummer z.B. des Auftrages erst durch das Speichern erzeugt wird und erst dadurch festgeschrieben wird. 

### Einfügen eines Datensatzes ![](Einfuegen.gif)
Steht nur bei ausgewählten Bewegungsmodulen zur Verfügung.
Damit wird ein neuer Datensatz vor der aktuellen Cursorposition eingefügt.

### <a name="Ändern eines Datensatzes"></a>Ändern eines Datensatzes ![](aendern.gif)
Damit können Sie bereits gespeicherte Daten, soweit zulässig, wieder ändern.

Der zu ändernde Datensatz wird exklusiv für Sie gesperrt. Genauer gesagt für diesen einen **Kieselstein ERP** Client. Damit wird sichergestellt, dass kein anderer Benutzer den gleichen Datensatz zur gleichen Zeit ändern kann. Die Einsicht in diesen Datensatz ist jedoch gegeben.

War beim Aufruf des Datensatzes dieser noch nicht gesperrt, wurde aber in der Zwischenzeit der Datensatz von einem anderen Benutzer gesperrt, so erscheint die Meldung:

![](Daten_Sind_Gesperrt.gif)

Bestätigen Sie mit ok. Danach werden allen Änderungsknöpfe deaktiviert.

![](Gesperrt.gif) 

Lediglich der Refreshknopf ist aktiv. Wer den Datensatz sperrt wird rechts unten angezeigt ![](Sperr_Anzeige.png).

Es kann jedoch sein, dass zwar eine Sperre aktiv ist, aber trotzdem keine Sperre angezeigt wird. Dies ist dann der Fall, wenn ein abhängiger Datensatz zu ändern versucht wird.

Beispiel: Der Partner ist gesperrt und Sie versuchen den Kunden zu ändern. In diesem Falle dürfen die Partnerdaten des Kunden nicht verändert werden, der Kunde ist gesperrt, obwohl keine Sperre angezeigt wird. Wenn Sie in diesem Falle wissen müssen, wer den "Kunden" sperrt, gehen Sie bitte in das Partnermodul und wählen dort ebenfalls den "Kunden" aus. Unter Detail sehen Sie nun rechts unten das Kurzzeichen des Sperrenden. Diese Logik hat den Vorteil, dass Sie z.B. zwar die Adressdaten des Kunden nicht ändern können, aber sehr wohl die Kundenspezifischen Daten.

Bei Bewegungsdaten werden gemeinsam mit dem gerade in Bearbeitung befindlichen Datensatz auch die Kopfdaten für die Dauer der Bearbeitung dieses einen Datensatzes gesperrt. Also für den Zeitraum zwischen dem Klick auf ändern bzw. neu und abspeichern bzw. verwerfen. Dies hat den Vorteil, dass Sie annähernd gemeinsam einen Beleg bearbeiten können. Dies ist allerdings nur hintereinander möglich. Also zuerst bearbeitet der erste Benutzer den ersten Datensatz und dann der zweite Benutzer einen weiteren Datensatz. Es kann also der andere Benutzer helfend eingreifen.

#### Ein Datensatz lässt sich nicht ändern
Der Datensatz wurde durch einen anderen Benutzer, oder durch eine andere Session gesperrt. Der Sperrende wird rechts unten in der Statuszeile mit Kurzzeichen und Namen angezeigt.
![](Sperr_Anzeige.png)

### Speichern eines Datensatzes ![](speichern.gif)
Damit werden die von Ihnen durchgeführten Eingaben / Änderungen in die Datenbank gespeichert und die Sperre dieses Datensatzes aufgehoben.

### Verwerfen der an einem Datensatz vorgenommenen Änderungen ![](verwerfen.gif)
Mit Klick auf das Icon werden die aktuell eingegebenen Daten verworfen - es wird keine Änderung übernommen. Das Änderungsdatum bleibt bestehen.

### Löschen eines Datensatzes ![](loeschen.gif)
Der Datensatz wird unwiederbringlich aus der Datenbank gelöscht.<br>
Voraussetzung dafür ist, dass keine davon abhängige Daten vorhanden sind. Sollte dies der Fall sein, so erscheint eine entsprechende Fehlermeldung mit einem Hinweis welche Daten dies sind.

### Umreihen eines Datensatzes ![](Sortieren.gif)
Steht nur bei ausgewählten Bewegungsmodulen zur Verfügung. Es wird damit der ausgewählte (angeklickte) Datensatz um eine Zeile nach vor oder nach hinten gereiht.

**Hinweis:**<br>
In einigen Modulen, wie z.B. in den Positionen der Rechnung, sind üblicherweise die Positionen nach der Anlage sortiert. Wenn nun, z.B. durch Klick auf Bezeichnung, diese Sortierung entsprechend geändert wird, kann / darf die Sortierung durch die Pfeile nicht mehr verändert werden. Daher werden die Sortierungs- / Umreihpfeile ausgeblendet. Durch einen Rechtsklick auf eine der sortierbaren Spaltenüberschriften wird die Sortierung wieder auf die Sortierung nach Anlage umgeschaltet und die Sortierpfeile wieder angezeigt.

### <a name="Zwischenablage"></a>Kopieren in ![](Internes_Kopieren.gif) und Einfügen aus ![](Internes_Einfuegen.gif) Zwischenablage
Mit diesen Funktionen können Positionen kopiert und Modulübergreifend wieder eingefügt werden.<br>
Wählen Sie dazu eine oder mehrere Positionen per Mausklick aus und klicken Sie auf Kopieren. Die im Puffer gespeicherten Einträge bleibt so lange aktuell, bis der Puffer wieder überschrieben wird. Auch nach einem Computer-Neustart kann auf die kopierten Positionen zurückgegriffen werden.<br>
Auch wenn Sie **Kieselstein ERP** mehrfach starten wird auf den gleichen Zwischenpuffer zurückgegriffen. Sie können die Positionen auch zwischen mehreren **Kieselstein ERP** -Clients austauschen, Voraussetzung ist, dass dies gleiche Clients sind, aus dem gleichen Programmverzeichnis gestartet wurden.

#### Möglichkeiten zum Kopieren: 
1. Eine einzelne Position mit der Maus oder der Tastatur auswählen und auf kopieren klicken
2. Eine zusammenhängende Gruppe mit der Maus wählen: Erste Position mit der Maus wählen, die Umschalttaste gedrückt halten und die letzte Position wählen.
3. Eine zusammenhängende Gruppe mit der Tastatur wählen: Erste Position selektieren, die Umschalttaste gedrückt halten und mit den Pfeilen der Tastatur nach oben oder unten gehen.
4. Eine nicht zusammenhängende Gruppe mit der Maus wählen: Erste Position wählen, beim Anklicken jeder weiteren Position muss die Strg-Taste gleichzeitig gedrückt werden.

Beim Klick auf Zwischenablage erscheint eine Fehlermeldung

![](Zwischenablage_Datei_anlegen.gif)

Erscheint diese Meldung, so hat der jeweilige Benutzer auf seinem Rechner kein (Schreib-)Recht um die Zwischenablagedatei anzulegen. Bitte geben Sie dem am lokalen Arbeitsplatzrechner angemeldeten Benutzer entsprechende Rechte im Betriebssystem, damit diese Datei abgelegt werden kann.

##### Wie werden die Preise bei der Übernahme aus der Zwischenablage verwendet?
Beim Kopieren und wieder Einfügen über die Zwischenablage werden auch die Preise übernommen. Ob diese nun neu berechnet werden sollten oder die bestehenden Preise weiterverwendet werden sollten, wird durch die Modulüberschreitung definiert.

D.h. bleiben Sie beim Einfügen im gleichen Modulbereich, z.B. kopieren Sie aus dem Angebot in die Rechnung, so bleiben Sie ja im Bereich Verkauf, so werden die Preise nicht verändert.

Überschreiten Sie aber die Modulgrenzen, so werden die Preise neu berechnet. Also werden wenn Sie z.B. vom Angebot in die Bestellung kopieren die Einkaufspreise anhand der beim Lieferanten für diesen Artikel hinterlegten Preise in die Bestellung übernommen. Gleiches gilt auch wenn Sie Positionen aus einer Stückliste kopieren.

##### Warum kann ich in einem Modul keine Positionen einfügen?
Das Einfügen neuer Positionen ist nur unter den gleichen Bedingungen möglich, unter denen auch neue Positionen angelegt werden dürfen. Siehe dazu bitte auch immer das jeweilige Statusdiagramm des Modules.

### Drucken ![](Drucken.gif)
Damit drucken Sie den ausgewählten Bewegungsbeleg aus. Zugleich wird damit der Status üblicherweise von angelegt auf offen geändert und steht damit für die weiteren, darauf aufbauenden Auswahllisten zur Verfügung. Üblicherweise erreichen Sie diese Druckfunktion auch über den linken Eintrag des Modul Menüs und Auswahl des Punktes drucken. Zum Drucken [siehe bitte auch]( {{<relref "/docs/stammdaten/system/drucken" >}} ).

In vielen Auswertungsansichten oder den Eingaben dafür, finden Sie zwei Symbole / Icons.

![](OK_Hacken.gif) Der Haken bedeutet die gewählte Funktion mit den getroffenen Einstellungen ausführen.

![](ESC_Tuer.gif) Die Tür bedeutet die gewählte Funktion ohne weitere Aktion wieder verlassen. Die Tür kann auch mittels der ESC Taste aktiviert werden.

Bitte beachten Sie gerade beim Drucken, dass Sie die Druckvorschau üblicherweise mit der Tür verlassen um wieder in die vorige Ansicht zurückzukommen. Wenn das Modul mit dem ![](Modulverlassen.gif) verlassen wird, so wird das Modul beendet und Sie müssen wieder mühsam Ihre Einstellungen und Auswahldefinitionen erneut eingeben.

### Anzeige des Status eines Datensatzes
![](Modulstatusleiste.png)  

Bei allen notwendigen Datensätzen wird unten eine Statusleiste angezeigt. Wobei die Statusanzeige wie folgt aufgebaut ist:

Links das erste Feld: Das Kurzzeichen der Person welche den Datensatz angelegt hat. Daneben das Datum der Anlage.

Daneben im dritten Feld das Kurzzeichen der Person, welche den Datensatz zuletzt geändert hat und daneben das Änderungsdatum.

Daran anschließend finden Sie die Statusanzeige des Datensatzes, sowie weitere Statusinformationen.

### Datumsauswahl
Für die Datumsauswahl steht mit Klick auf das Kalendersymbol ein komfortabler Kalender zur Verfügung.<br>
![](Kalender_Datumsauswahl.png)  

Blättern durch die Monate, entweder durch Auswahl des Monates über die Combobox und Angabe des gewünschten Monates. Hier bleibt das aktuelle Jahr erhalten, oder durch Klick auf die kleinen Pfeile neben der Monats-Combobox (Spinner) hier wird gegebenenfalls auch das Jahr entsprechend mit verändert.

Auswahl des Jahres durch Klick auf die kleinen Pfeile rechts neben der Jahreszahl.

Blättern durch die Kalenderwochen, durch Klick auf die (Knopf-) Pfeile am linken Rand. Hier werden auch die Monate entsprechend mit verändert.

Übernahme des heutige Datums durch Klick auf Heute, gestern durch Klick auf Gestern.

Übernahme des gewünschten Datums durch Doppelklick auf den jeweiligen Tag

Verlassen ohne Übernahme, durch Klick ins eigentliche Datumsfeld, oder ESC.

Die rot hinterlegten Tage sind entweder Sonntage oder Feiertage anhand des Betriebskalenders (Personal).

Die am linken Rand angezeigten Zahlen sind die Kalenderwochen im jeweiligen Jahr.

Beachten Sie bitte, dass wir uns für die Berechnung der Kalenderwoche an die europäische Gepflogenheiten halten. Es kann, insbesondere wenn der 1.Jänner/Januar auf einen Donnerstag fällt, zu Unterschieden zur amerikanischen Berechnung der Kalenderwoche kommen.

#### Löschen von Datumseingaben ![](Datum_loeschen.png)  
Durch Klick auf den Radiergummi

#### Direkte Eingabe eines Datums
Bitte beachten Sie hier, dass diese Eingabe von Ihrer gewählten Sprache abhängig ist. Im deutschsprachigen Bereich ist dies immer TT.MM.JJJJ bzw. TT.MM.JJ. Dies bedeutet, dass Sie das Datum einfach eintippen können und zwar in der Reihenfolge Tag, Monat, Jahr. Wird das Jahr nur zweistellig eingegeben so wird von 00-39 dies auf 20xx übersetzt und ab 40 wird es auf 19xx übersetzt. Beispiel: 05 wird auf 2005 übersetzt. Die Jahreseingaben im vierstelligen Format sind auf den Bereich 1900 - 2999 begrenzt.

Zur einfacher Eingabe haben wir diese noch mit folgender Funktionalität ausgestattet:

Eingabe von 120923 ergibt exakt obiges Datum (im deutschsprachigen Client).

Die Eingabe von 1,1,23 ergibt 01.01.2023\. D.h. auch auf den deutschen Tastaturen kann am numerischen Ziffernblock das Datum ohne Wechsel auf den Alphateil der Tastatur eingegeben werden. Das Jahrtausend muss nur eingegeben werden, wenn es explizit abweicht siehe oben.

Ist das Datum rot, so kann es nicht gespeichert werden, es erscheint der Dialog: "Alle Pflichtfelder ausfüllen..." - dies kann vorkommen wenn die Eingabe nicht den Vorgaben (siehe oben entspricht).

#### Datumsbereichsauswahl in den Ausdrucken
In vielen Journalen ist ein Datumsbereich einzugeben, für den dieser Report, die Auswertung angewandt werden sollte. Da dies sehr oft Monats- oder Jahresbereich sind haben wir dafür eine komfortable Funktion der Auswahl geschaffen.<br>
![](Datum_Monatsbereich.gif)<br>
Die Bedienung ist wie folgt:
- direkte Eingabe des Datums, diese ist wie oben beschreiben
- Klick mit der Maus auf den blauen Pfeil nach oben, verändert den Von Bis Bereich, ausgehend vom Von Datum um plus ein Monat in die Zukunft, in unserem Falle also 01.07.2013 bis 31.07.2013
- Klick mit der Maus auf den blauen Pfeil nach unten, verändert den Von Bis Bereich ausgehend vom Von Datum um minus ein Monat in die Vergangenheit.
- Klick mit der Maus auf den blauen Pfeil nach oben bei gleichzeitig gehaltener Shift (Großschreib) Taste, verändert den Von Bis Bereich ausgehend vom Von Datum um ein Jahr in die Zukunft.
- Klick mit der Maus auf den blauen Pfeil nach unten bei gleichzeitig gehaltener Shift (Großschreib) Taste, verändert den Von Bis Bereich ausgehend vom Von Datum um ein Jahr in die Vergangenheit

In einigen Auswertungen der Zeiterfassung ist oft auch die Auswertung für den gestrigen Tag erwünscht. Um auch hier die Erfassung deutlich zu vereinfachen gibt es den Gestern Knopf ![](Datum_Gesternknopf.gif). D.h. ein Klick auf diesen Knopf bewirkt, dass von bis mit dem Bereich des gestrigen Datums vorbesetzt werden.

### Comboboxauswahl
![](Combobox_Bedienung.gif)<br>
Die Auswahl mithilfe von Comboboxen steht immer dann zur Verfügung, wenn nur eine geringe Anzahl von Elementen in der Combobox erwartet werden / von **Kieselstein ERP** vorgegeben sind. Comboboxen können entweder mit der Maus bedient werden, oder wenn die Combobox selektiert ist, durch drücken des ersten Zeichens des angezeigten Textes.<br>
So können Sie z.B. in der Positionseingabe in der Rechnung (allen Bewegungsmodulen) die Positionsart einfach durch eintippen des jeweiligen Anfangsbuchstabens bestimmen, wodurch die Tastaturbedienung deutlich vereinfacht wird.

**Info:**
Der Inhalt der jeweiligen Combobox wird immer nur beim Modulstart geladen. Das bedeutet z.B. wenn Sie neue Mengeneinheiten oder neue Artikelgruppen definieren, so sind diese nicht sofort in den Auswahl-Comboboxen verfügbar, sondern es muss nach der Definition z.B. das Artikelmodul beendet und neu gestartet werden, damit die neuen Mengeneinheiten bzw. Artikelgruppen in der jeweiligen Combobox ausgewählt werden können.

### Positionsarten
<a name="Positionsarten"></a>
In den Belegen können in **Kieselstein ERP** unterschiedliche Positionsarten ausgewählt werden. Unter anderem sind hier folgende Positionsarten verfügbar:

| Positionsart | Beschreibung |
| --- |  --- |
| Ident | Auswahl eines Artikels |
| Handeingabe | Eingabe in ähnlicher Form wie ein Artikel, aber nicht im Artikelstamm gepflegt |
| Texteingabe |  Eingabe eines Textes |
| Textbaustein | Möglichkeit hinterlegte Texte/Bilder/etc. einzugeben |
| Betrifft | Eingabe eines Betreffs für den Beleg |
| Leerzeile | Einfügen einer Leerzeile |
| Seitenumbruch | Einfügen eines Seitenumbruchs |

### Belegaktivierung
Bei der Belegaktivierung (d.h. Statusänderung von angelegt auf offen bzw. deren Entsprechung in den jeweiligen Belegen) werden Belege (Angebot bis Rechnung) erst dann aktiviert, wenn diese
- tatsächlich (am Drucker) gedruckt 
- gespeichert (z.B. als pdf / xls /...)
- per E-Mail versendet
- in die große Druckvorschau gedruckt (siehe bitte auch Parameter GROSSE_DRUCKVORSCHAU_AKTIVIERT)
wurden.

Aktivierung der Belege durch tatsächliches Drucken/Mailen etc.:
- Anfrage
- Angebot
- Auftrag
- Bestellung
- Lieferschein
- Rechnung/Gutschrift

Eine weitere Möglichkeit ist: Wenn man mit der <u>rechten</u> Maustaste in den Modulen im Reiter Positionen auf das Drucksymbol klickt, so wird der Beleg direkt auf den Status 'Offen' gesetzt.
ACHTUNG: Der Beleg wird in diesem Fall nicht in der Dokumentenablage gespeichert!

[Siehe dazu bitte auch](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Statussymbole).

## Neuer Mandant
[Siehe bitte.](../System/index.htm#Neuer Mandant)

## weitere allgemeine Fragen

#### Ein Beleg(Auftrag) lässt sich nicht mehr ändern?
**Kieselstein ERP** ist grundsätzlich so programmiert, dass Bewegungsdaten änderbar sind, solange dafür kein Nachfolger, in unserem Falle ein Lieferschein, angelegt wurde. Ist ein Nachfolger vorhanden, so können nur mehr bestimmte, jeweils vom Modul abhängige, Daten verändert werden.

#### Wieso wird beim Ausdruck ???/??? angezeigt?
Siehe [Benutzer]( {{<relref "/docs/stammdaten/benutzer/#wieso-wird-beim-ausdruck-angezeigt" >}} )

### Druckdialog mit Vorschau
Der Druckdialog wird immer beim Start eines Druckvorganges, eines Druckjournals als erstes angezeigt.

Hier können Sie die Filterbedingungen für den Druck entsprechend einstellen oder auch direkt den Ausdruck, den EMail Versand veranlassen. Da die Darstellung der Daten im Druckdialog manchmal sehr klein ist, können Sie von hier aus auch in die Druckvorschau wechseln.

#### Druckvorschau
Auch in der Druckvorschau können Sie entsprechend blättern usw.

## Bedeutung der Feldfarben

In **Kieselstein ERP** finden Sie verschiedene Farben der Felder vor.

- Pflichtfelder<br>
Rot umrandete Felder sind Pflichtfelder. D.h. hier müssen von Ihnen Werte / Begriffe eingegeben werden. Bitte beachten Sie bei numerischen Eingaben den Unterschied zwischen der Eingabe der Zahl 0 und einer leeren Zahl.

Beachte bitte auch die Einstellungen unter Ansicht, Schrifteinstellung, Rot-Grün-Sehschwäche

- Automatische Rechenfelder
Automatische Rechenfelder werden mit einem blauen Hintergrund dargestellt. Dies Versinnbildlicht die Zusammengehörigkeit der Felder. Zugleich wird damit angezeigt, dass beim Verlassen eines dieser Felder eine entsprechende Rechenoperation durchgeführt wird. Je nach Hintergrund der Berechnung kann es vorkommen, dass diese Berechnung auch längere Rechenzeit beansprucht.

![](Rechenfelder.gif)

## Layout
### <a name="Layout speichern"></a>
### Können die Spaltenbreiten der Ansichten eingestellt werden?
### Können die verwendeten Module vordefiniert werden?
### Kann das Layout definiert werden?
Die Grundidee ist, dass man bei jedem Neustart seines **Kieselstein ERP** Clients gern die Module und deren Anordnung und auch die angezeigten Werte, so wie zuletzt als optimal definiert, wieder vorfinden möchte.

Dafür haben wir zwei Funktionalitäten geschaffen.
- a.) Speicherung des aktuellen Layouts<br>
Damit werden die Positionen der Module gespeichert, so wie sie zum Zeitpunkt des Speicherns waren.<br>
Diese Funktion finden Sie im Hauptmenü unter Ansicht, Layout speichern.<br><br>
Hier können Sie auch ein bereits gespeichertes Layout laden bzw. wieder löschen.<br>
    ![](Ansicht_speichern.gif)

- b.) Verschieben aller Spalten in den Modulen und Speicherung der Einstellung derselben.<br>
Wenn Sie den Mauszeiger über einen Spaltentrenner bewegen, so ändert dieser seine Form. Durch Klick mit der linken Maustaste kann nun die Spaltenbreite wunschgemäß geändert werden. Haben nun alle Spalten des gerade verwendeten Moduls die gewünschte / optimale Breite, so kann diese durch Klick auf ![](Spalte_speichern_neu.gif) gespeichert werden. Sie finden diesen Speichernknopf in der jeweiligen Auswahlliste ![](druck_speichern.JPG) rechts oben (neben dem Knopf für den Druck der Auswahlliste).<br>
Mit der Speicherung ändert sich das aussehen des Knopfes auf ![](Spalte_speichern.gif). Damit ist ersichtlich, ob bei Tabellen spezielle Spalteneinstellungen hinterlegt sind. Damit verbunden ist auch die Speicherung der Sortierung der jeweiligen Spalten.<br>
Beim Speichern der einzelnen Spaltenbreiten wird auch die Modulposition/-größe (Fenstergröße) mitgespeichert.<br>
Die Sortierung der Spalte wird durch Pfeile ![](Sortierung.gif) mit entsprechender Richtung angezeigt.<br>
Spalten können durch Klick bzw. Mehrfachklick auf die Spaltenüberschrift entsprechend sortiert werden.<br>
Sollten Spalten in Kombination sortiert werden müssen, so kann durch halten der Strg-Taste und Klick auf die weitere Spalte diese kombinierte Sortierung verwendet werden.<br>
Ein Klick mit der **rechten Maustaste löscht die Sortierungen** wieder.

    Die Daten für diese Informationen werden in den benutzerspezifischen Applikationsdaten des jeweiligen Betriebssystems auf dem lokalen Rechner abgelegt. Das bedeutet, dass diese Einstellungen auf dem jeweiligen Rechner spezifisch vorgenommen werden müssen. Dafür benötigt der am Rechner angemeldete Benutzer entsprechende Schreibrechte in seinem Benutzerverzeichnis. [Siehe](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Tabelleneinstellungen, Positionen verschieben).

    Zusätzlich können die Spalten auch Verschoben werden, also die Reihenfolge deinen Wünschen entsprechend angepasst werden. Um eine Spalte an eine andere Position zu schieben, klicken und halten Sie bitte den Spaltenkopf und ziehen Sie diesen an die für Sie richtige Stelle. Auch diese Informationen werden durch Klick auf ![](Spalte_speichern_neu.gif) bzw. ![](Spalte_speichern.gif) gespeichert.<br>
Durch Rechtsklick auf ![](Spalte_speichern.gif) kann die gespeicherte Information der Reihenfolge, Sortierung und Spaltenbreite wieder gelöscht werden. Sollte sich die Spaltenanzahl ändern, werden diese Daten automatisch gelöscht. Die Spaltenanzahl kann sich durch Ändern der Parametrierung oder durch neu hinzukommende Spalten nach einem Programmupdate ändern.

### Kann die verwendete Schriftgröße definiert werden?
Unter Ansicht, Schriftart kann die in Ihrem **Kieselstein ERP** Client verwendete Schriftart definiert werden.

Bewährt haben sich die Schriften Tahoma (Standardschrift) in 11 Punkten und normal und Verdana ebenfalls 11Punkte und gegebenenfalls Fett.

Beachten Sie bitte, dass die Layouts größtenteils auf Tahoma 11 Punkte ausgelegt sind. D.h. bei entsprechend anderen Einstellungen sind die Spalten usw. neu zu definieren.

### Wie kann ich den **Kieselstein ERP** Client einstellen, wenn ich an einer Rot-Grün-Sehschwäche leide?
Unter Ansicht, Schriftart kann die in Ihrem **Kieselstein ERP** Client verwendete Farbe für das Hervorheben der Pflichtfelder von Rot auf Blau geändert werden. Setzen Sie den Haken bei ![](rotgruen.jpg) um diese Darstellung zu aktivieren.

### Was bedeutet das ![](No_Sort.gif) in der Spaltenüberschrift?
Das ![](No_Sort.gif) in der Spaltenüberschrift ![](Spaltenueberschrift.gif) zeigt an dass nach dieser Spalte NICHT sortiert werden kann. Hintergrund: Diese Daten sind keine Datenbankfelder, sondern werden life errechnet.

### Wieso und wo kann / muss ich den Kurs nachtragen?
![](Kurs_Nachtragen.gif)

Wenn beim Anlegen einer neuen Eingangsrechnung diese Meldung erscheint, so tragen Sie bitte den Wechselkurs der Währung ein.

Grund für die Meldung ist, dass die Zuordnung zwischen einem der Mandantenkurse zu den anderen Währungen fehlt.

In diesem Fall wechseln Sie bitte in das Modul Buchhaltung ![](Finanzbuchhaltung.jpg). Wählen Sie den unteren Modulreiter Währung und tragen Sie bitte den Kurs [entsprechend nach](../Finanzbuchhaltung/index.htm#Kurse nachpflegen).

### Es ist ein schwerwiegender Fehler aufgetreten
Sollte wider erwarten eine Fehlermeldung dieser Art auftreten,

![](Schwerer_Fehler.jpg)

so klären Sie bitte zuerst mit Ihrem Administrator die Fehlermeldung ab.

Gegebenenfalls senden Sie uns die **<u>unter Details</u>** angeführten per E-Mail an <help@kieselstein-erp.org> zu. Kopieren Sie dazu den im Clientfenster enthaltenen Text in Ihr EMail Programm. Klicken Sie dazu auf den Knopf Detail und anschließend auf Zwischenablage. Damit wird die Fehlermeldung in die Zwischenablage kopiert. Fügen Sie den Inhalt danach aus der Zwischenablage in das an uns gerichtete E-Mail ein. Bitte geben Sie unbedingt eine ausführliche Beschreibung dazu, wie Sie diesen Fehler erzeugen konnten. Nur mit einer ausführlichen Beschreibung können wir versuchen den Fehler zu rekonstruieren und abstellen.

Hinweis1: Support steht nur für Mitglieder der [Kieselstein ERP eG](https://kieselstein-erp.org/) zur Verfügung. Gerne können Sie auch ohne Mitgliedschaft zur Verbesserung von **Kieselstein ERP** beitragen und uns trotzdem diese Meldungen senden.

Hinweis2: [Welche Infos benötigt eine ordentliche Fehlermeldung]( {{<relref "/docs/stammdaten/system/supportanfrage/">}} )

Hinweis3: Beachte den Parameter SCHWERWIEGENDE_FEHLER_VERSENDEN.

### <a name="Benutzeranzahl_ueberschritten"></a>Maximale Benutzeranzahl überschritten
Wird bei der Anmeldung dieser Hinweis angezeigt, so bedeutet dies, dass mit dieser Anmeldung die erlaubte Anzahl der gleichzeitig angemeldeten **Kieselstein ERP** Benutzer überschritten würde. Bitte deinen **Kieselstein ERP** Betreuer die Benutzeranzahl zu erhöhen.

### FLR Fehler. Was bedeuten diese, was ist zu tun ?
Wenn ein sogenannter FLR Fehler (FastLaneReader, damit werden die **Kieselstein ERP** Auswahllisten erzeugt) auftritt, dann ist es sehr oft so, dass in diesem Moment der **Kieselstein ERP** Server sehr beschäftigt ist und aus diesem Grunde zu langsam reagiert.

Nun stellt sich die Frage, sind meine geänderten Daten gespeichert oder nicht?

Ja. Das Speichern der Daten erfolgt immer vor dem Aufruf des Fastlanereaders, des Updates der Auswahlliste. Daher sind ihre Änderungen bereits gespeichert.

D.h. tritt ein FLR Fehler auf, so aktualisieren Sie bitte die angezeigte Auswahlliste erneut durch Klick auf den ![](Aktualisieren.gif) aktualisieren Button.

### Ich nutze Windows10/11, kann ich das **Kieselstein ERP** Icon in die Taskleiste schieben?
[siehe]( {{<relref "/docs/installation/02_client/#client-in-start--taskleiste-bringen">}} )

### Welche Java-Versionen sollte ich verwenden?
Bitte ausschließlich die in den Installationen verwendeten Open JDK (Java) Versionen verwenden.

### Kann ich meinen Datenbankserver automatisch updaten?
**Unter keinen Umständen.** [siehe]( {{<relref "/docs/installation/01_server/update">}} )

### Wie schnell muss mein **Kieselstein ERP** Server sein?
Das hängt ganz davon ab, welche Aufgaben für wieviele gleichzeitige Clients laufend anfallen und wie "schwer" diese Auswertungen / Datenbankoperationen sind.<br>
Beachten Sie, dass alle angegebenen Server-/ Clientausstattungen nur Empfehlungen sein können. Der tatsächliche Bedarf kann nur im laufenden Betrieb festgestellt werden. Hier zählen vor allem die Reaktionszeiten der unterschiedlichsten Funktionen. So wird gerne der Bestellvorschlag, die Auslieferliste, ein Journal aller offenen Aufträge als Maßzahl verwendet.<br>
Beachten Sie bei den Geschwindigkeitsvergleichen bitte auch, dass ab Java7 (**Kieselstein ERP** nutzt Java 8) das MultiThreading unterstützt wird. D.h. die Geschwindigkeitsaussage für einen Client bezieht sich auf die Geschwindigkeit einer CPU und das Datenbanksystems und damit den Festplatten. Die Anzahl der verwendeten CPU Kerne hängt von der Gleichzeitigkeit der Clientzugriffe ab. Darum ist eine Aussage wie: Am Samstag ist es gleich schnell wie am Montag an dem alle arbeiten" nur eine Aussage über die (meist nicht vorhandene) Qualität der Festplattenzugriffe. Die Wirkung mehrere CPU Kerne merken Sie erst wenn entsprechend mehr an Clients gleichzeitig laufen. Die Lastverteilung des Applikationsserver ist bereits gut optimiert.
