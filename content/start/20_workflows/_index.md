---
categories: ["Workflow"]
tags: ["Workflow"]
title: "Workflow"
linkTitle: "Workflow"
weight: 2000
description: >
 Was sind Workflows, wie kann man sie parametrieren
---
Zusammenfassung der Workflows
=============================

Hier finden Sie eine Verlinkung aller in der Dokumentation hinterlegten, beschriebenen Abläufe oft auch Ablaufdiagramm bzw. Workflow genannt.

Die in **Kieselstein ERP** abgebildeten Workflows sind fest verdrahtet und zusätzlich in vielen Bereichen entsprechend den Anwenderbedürfnissen parametrierbar.

Hier finden Sie eine Auflistung der beschriebenen Abläufe, ausgehend von einem sehr groben Durchlauf durch den in **Kieselstein ERP** abgebildeten Unternehmenskreislauf.

## Wie sieht nun der grundsätzlich Ablauf aus?
Es beginnt immer bei einer Idee oder einem (Kunden-)Projekt. D.h.

1.  Projekt
2.  Angebot
3.  Auftrag
4.  Fertigungsplanung, wenn erforderlich
5.  Beschaffung
    1.  Wareneingang
    2.  Eingangsrechnungserfassung
6.  Fertigung, wenn erforderlich
7.  Lieferung
8.  Nachkalkulation
9.  Verrechnung
10. Projektbeurteilung auf Erfolg -> Lernchance nützen

Die Prozessdiagramme dazu:

Vom Auftrag ins Los / Fertigung und dann zum Lieferschein und weiter zur Verrechnung
![](AB_LOS_LS.JPG)

Vom Auftrag über den Wareneingang zum Lieferschein und zur Verrechnung, ohne Fertigung
![](ab_ls.jpg)

Arbeitsgangbuchung
![](ag_buchung_ma.JPG)

AG Buchung Mehrmaschine
![](AG_Buchung_Mehrmaschinen.JPG)

Angebot erstellen
![](angebot_erstellen.PNG)

Artikel anlegen
![](artikel_anlegen.JPG)

Auftrag anlegen
![](auftrag_erstellen.PNG)

Entwicklung des Losstatuses, einfach
![](einfach_losstatus.PNG)

Entwicklung Losstatus komplett
![](losstatus.PNG)

Losstatus zurücknehmen
![](losstatus_rueckgaengig.PNG)

Fertigung
![](fertigung_los.PNG)

Interne Bestellung
![](interne_Bestellung_a.JPG)

Nachkalkulation
![](Nachkalkulation_AB.JPG)

Warenfluss
![](warenfluss.JPG)

Steuerkategorien
![](steuerkategorie_kd_re_icons.JPG)