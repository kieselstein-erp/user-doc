---
categories: ["Bedienung"]
tags: ["Mehrsprachigkeit"]
title: "Mehrsprachigkeit"
linkTitle: "Mehrsprachigkeit"
weight: 1000
description: >
 Mehrsprachigkeit in deinem **Kieselstein ERP** System
---
Mehrsprachigkeit
================

**Kieselstein ERP** ist grundsätzlich mehrsprachig in Daten und Anwendung ausgelegt.

In der Datenhaltung bedeutet die Mehrsprachigkeit, dass die unterschiedlichen Daten in den jeweils definierten Sprachen vorhanden sind. Es bedeutet aber auch, dass die Sprache in der mit dem Partner kommuniziert wird, beim Partner (Kunde / Lieferant) definiert werden kann.

In der Anwendung bedeutet dies, dass alle Bezeichnungen in der Sprache übersetzt sein können und dass die in der gewählten UI-Sprache vorhandenen Daten angezeigt werden.

**Kieselstein ERP** ist UniCode fähig. Das bedeutet es können auch Kyrillische, Russische usw. Schriftzeichen dargestellt und in der jeweiligen Datenbank abgespeichert werden.

In **Kieselstein ERP** sind derzeit folgende Sprachen definiert:

| Report / Ressource | Sprach- kurz- kennung | Beschreibung |
| --- |  --- |  --- |
| de_AT | deAT | Deutsch mit österreichischem Dialekt. Die Muttersprache von **Kieselstein ERP** |
| de_DE | deDE | Deutsch mit deutschem Dialekt |
| de_CH | deCH | Deutsch mit Schweizer Dialekt Hier wird die Darstellung der Zahlenformate auf die Schweizer Schreibweise angepasst. |
| en_UK | enUK | Englisch mit Dialekt des Vereinigten Königreiches Hier wird die Darstellung der Zahlenformate an die Englische Schreibweise angepasst. |
| en_US | enUS | Englisch mit amerikanischem Dialekt |
| it_IT | itIT | Italienisch |

Bitte beachte, dass unterschiedliche Dialekte, je nach Einsatzgebiet unterschiedliche Sprachen sind. Das bedeutet, dass Übersetzungen die z.B. in en_US eingegeben wurden für einen Partner welcher in en_UK definiert ist NICHT greifen, sondern dass auf die Mandantensprache zurückgegriffen wird.

Da es aus diesem Grund manchmal zu Verwechslungen kommen kann, haben wir die Möglichkeit geschaffen, bestimmte Sprachen in der Datenbank abzuschalten.

#### Können weitere Sprachen definiert werden ?
Ja dies ist jederzeit möglich. Wende dich dazu bitte an deinen  **Kieselstein ERP** Betreuer.

Es ist auch möglich sogenannte Kundenspezifische Sprachen zu definieren. D.h. es können jederzeit weitere "Dialekte" aber auch eigenständige Sprachen hinzugefügt werden. Diese stehen beim Anmeldedialog zur Verfügung und können bei der Definition der Partnersprache ebenfalls ausgewählt werden. 

Bedenke bitte, dass für Formulare immer die nächst passende Definition verwendet wird. Wird diese nicht gefunden, wird in aller Regel auf die Programmiersprache von **Kieselstein ERP** zurückgegriffen.

## Mandantensprache:

Dies ist jene Sprache in der Ihre **Kieselstein ERP** Mandant definiert ist. Siehe dazu auch System, Mandant, Kopfdaten, Kommunikationssprache.

## Einpflegen von Übersetzungen:

Um die Übersetzung der Zahlungsziele, Mengeneinheiten usw. in **Kieselstein ERP** einzutragen melden Sie sich mit Ihrem **Kieselstein ERP** Client in der gewünschten, der zu übersetzenden Sprache an. Wählen Sie dazu aus dem Feld Benutzersprache die gewünschte Sprache und klicken auf Anmeldung.

Nun wechseln Sie zu den gewünschten Texten (Kopf- und Fußzeilen, die Mengeneinheiten, Eigentumsvorbehalt usw.) und geben diese in der für Sie richtigen Übersetzung ein.

Der Aufbau ist grundsätzlich so, dass es immer einen kurzen Begriff gibt, der den zu übersetzenden Text definiert. Die Übersetzung, also die aktuelle Sprache wird nun in der sogenannten UI-Bezeichnung eingepflegt (UI steht für User Interface).

Um die Texte beim Artikel möglichst einfach zu übersetzen, wird bei der Eingabe der Artikeldetails im oberen Fenster der Artikel in der Mandantensprache und im unteren Fenster in der gewählten Clientsprache angezeigt.
![](uebersetzung.PNG)

Sie erhalten eine ähnliche Ansicht wie oben angeführt. Hier wird der Inhalt aus der Default Sprache angeführt. Durch die Einstellung am Client kann es passieren, dass Sie auf den ersten Blick deutsche Texte sehen, diese aber in einer der Landessprachen de_DE / de_CH / de_AT erfasst wurden. Bitte überprüfen Sie in diesem Fall mit welcher Sprache Sie angemeldet sind (siehe nächste Frage) und stellen die Sprache bei der Client-Anmeldung entsprechend ein. Gerne können Sie sich auch an Ihren **Kieselstein ERP**-Betreuer wenden um die Standard-Sprache beim Start des Clients zu aktualisieren bzw. können selbst die entsprechende Sprache im Client-Startskript hinterlegen.

## Vereinfachte Pflege für den Artikel
Um die Pflege der Artikeltexte ohne dem laufenden Wechsel zwischen den Clients und den Sprachen zu ermöglichen, steht die Sprachauswahl für den angezeigten und zu bearbeitenden Artikel im Artikel Detail und im Artikel Kommentar zur Verfügung.

D.h. Sie finden im Artikel, Detail ![](Artikel_Sprachumschaltung.gif) die Möglichkeit die angezeigte Sprache des Artikeldetails in die gewünschte Sprache umzuschalten.

Die Sprachumschaltung steht nur vor dem Ändern zur Verfügung. Sie bleibt für das Artikeldetail solange eingestellt, bis Sie das Artikelmodul schließen und neu starten bzw. eben bis Sie die Sprache wie gewünscht umstellen.

Eine Analoge Möglichkeit steht auch im Reiter Kommentar für die Umschaltung der Kommentare zur Verfügung. Bitte beachten Sie, dass gegebenenfalls unterschiedliche Sprachen in den beiden Detailreitern eingerichtet werden können.

## Woran kann ich erkennen in welcher Sprache ich angemeldet bin ?
Dies sehen Sie in der Titelleiste des **Kieselstein ERP** Clients. Hier finden Sie:

Softwareversion, Mandant, Angemeldeter Benutzer und Rechnername, UI-Sprache, Serververbindung. Sie sehen hier also z.B.:

![](UI_deAT.gif) für deutsch mit österreichischem Dialekt

![](UI_enUS.gif) für Englisch mit amerikanischem Dialekt.

Beispiele für falsche Definitionen:
| Fehler | Ursache |
| --- | --- |
| Mengeneinheit nicht übersetzt | Die Mengeneinheit wurde für en_US übersetzt. Der Lieferant wurde aber in en_UK definiert. Daher gab es keine Übersetzung für diese Sprache und es wurde der Text der Mandantensprache (Stk) angedruckt |
| Lieferbedingung nicht übersetzt | Es wurde seit der Freigabe der Übersetzung eine weitere Lieferbedingung hinzugefügt. Es wurde vergessen diese Übersetzung einzupflegen, deshalb wird die Lieferbedingung nicht in der gewünschten Sprache sondern in deutsch (genauer in der Mandantensprache) ausgedruckt |
| Zahlungsziel nicht übersetzt | Gleich wie bei Lieferbedingung. |
| Anreden in den Ausdrucken aber auch in den EMail-Texten werden in deutsch ausgedruckt | Pflegen Sie die Anreden in der gewünschten Sprache nach. Melden Sie sich dazu an Ihrem **Kieselstein ERP** Client in der gewünschten Sprache an, starten Sie das Modul Partner und öffnen Sie den unteren Modulreiter Grunddaten. Nun wählen Sie in den Reiter 3 Anreden und definieren Sie hier die jeweils sprachabhängige Bezeichnung |

### Welche Daten müssen alle übersetzt werden?
Nachfolgend eine Aufstellung, welche Daten alle übersetzt oder zumindest geprüft werden sollten, um eine vollständige Übersetzung in den Daten zu haben.

Hinweis: Zusätzlich zu dieser Aufstellung gehört hier auch dazu, dass die Formulare in den gewünschten Sprachen vorhanden und gültig übersetzt sind. Bitte beachten Sie, dass die EMail-Texte in diesem Sinne auch als Formulare betrachtet werden, d.h. auch übersetzt bzw. eingerichtet werden müssen.

Wir gehen hier von der englischen Übersetzung aus. Für andere Sprachen gilt dies sinngemäß.

-   Bewegungs-Module
    -   Anfrage, Grunddaten, Anfragetext -> Inquiry, Basic data, Text for requests<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen
    -   Bestellung, Grunddaten, Bestellungstext -> Order, Basic data, Text for orders<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen
    -   Bestellung, Grunddaten, Mahntext -> Order, Basic data, Text for dunning letter<br>
        Bitte hier die Mahntexte entsprechend definieren
    -   Reklamation, Grunddaten -> Complaint, Basic data<br>
        Pflegen Sie hier die Übersetzungen in den Language-dependent Labeling
    -   Projekt, Grunddaten -> Project, Basic data<br>
        Pflegen Sie hier die Übersetzungen in den Language-dependent Labeling bzw. nur Labeling
    -   Angebot, Grunddaten, Angebotstext -> Offer, Basic data, Offer text<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen
    -   Auftrag, Grunddaten, Auftragstext -> Order confirmation, Basic data, Text for order confirmations<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen
    -   Lieferschein, Grunddaten, Lieferscheintext -> Delivery note, Basic data, Text for delivery notes<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen
    -   Rechnung, Grunddaten, Rechnungstext -> Invoice, Basic data, Text for invoice<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen
    -   Rechnung, Grunddaten, Mahntext -> Invoice, Basic data, Text for dunning letter<br>
        Bitte hier die Mahntexte entsprechend definieren
    -   Rechnung Grunddaten, Gutschrifttext -> Invoice, Basic data, Text of credit note<br>
        Hier sind Kopf und Fußtext als Basis für diese Texte einzupflegen

-   Stammdaten
    -   Partner, Grunddaten -> Partner Basic data
        -   Anrede -> Title
    -   System<br>
        Im System sind vor allem die Mandantenabhängigen Daten zu definieren, wie z.B. Zahlungsziele, aber auch die Texte für den Eigentumsvorbehalt, Mengeneinheiten, usw.
        -   System, Sprache, Einheit -> System, Language, Unit<br>
            Definieren Sie hier die Übersetzungen der Einheiten, z.B. Stk -> pcs. Achten Sie dabei darauf, dass in den Formularen nur Platz für kurze Texte vorgesehen sind.
        -   System, System, Land -> System, System<br>
            Definieren Sie hier gegebenenfalls die Übersetzung der Ländernamen, falls dies auf den Adressen in der anderen Sprache angedruckt werden sollte.<br>
            Bitte beachten Sie die Postvorschriften des Versender- und des Absender-Landes. Z.B. Vereinigte Staaten von Amerika <-> United States of America
        -   System, Medien, Textbausteine -> System, Media, Text modul<br>
            Definieren Sie hier Ihre Texte in den anderen Sprachen für Ihre Textbausteine.
        -   System, Mandant -> System, Tenant<br>
            -   Zahlungsziel -> Payment Terms<br>
                Pflegen Sie hier die Übersetzung des Zahlungsziels
            -   Lieferart -> Type of delivery<br>
                Pflegen Sie hier die Übersetzung der Lieferart / Incoterms
            -   AGB -> Conditions<br>
                Pflegen Sie hier die jeweiligen Dokumente in der gewünschten Sprache ein

    -   Artikel
        Im Artikel können sowohl die Bezeichnungen als auch die Kommentare direkt aus der Clientsprache heraus bearbeitet werden.

## Wie ist das mit den Zahlen?
In der Europäischen Union wird im wesentlichen das Komma als Dezimaltrenner verwendet. Ausgenommen sind davon englischsprachige Länder. [Für Details siehe bitte] (https://de.wikipedia.org/wiki/Dezimaltrennzeichen#Komma-L%C3%A4nder)

Wichtig ist dies insofern, dass wenn du dich z.B. in Italien in Englisch anmeldest, so wird die Übergabe der Zahlen in deine Tabellenkalkulation nicht funktionieren, denn:
- Im Englischen wird das Komma als Tausendertrenner verwendet und der Punkt als Dezimaltrenner
- hingegen im Italienischen ist das Komma der Dezimaltrenner.
Es ist daher wichtig, dich möglichst in der richtigen Sprache anzumelden, auch wenn noch nicht alle Resourcen, also Texte übersetzt sind. Als Notlösung stellen wir gerne z.B. die englischen Texte in deiner gewünschten Sprache zur Verfügung. Damit kannst du zumindest starten.

Natürlich freuen wir uns, wenn du auch die Übersetzung in deine Sprache der Allgemeinheit der **Kieselstein ERP** Verwender zur Verfügung stellst.

Hinweis: Die in machen Ländern erlaubten Phantom Spaces (White Space) anstatt des Tausendertrennzeichens werden, obwohl in der Java Definition so enthalten, trotzdem als Punkt dargestellt. Dies vor allem um auch den Import in die üblichen Tabellenkalkulationen zu ermöglichen.