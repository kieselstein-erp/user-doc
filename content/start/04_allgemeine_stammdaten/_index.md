---
categories: ["Bedienung"]
tags: ["allgemeine Stammdaten"]
title: "allgemeine Stammdaten"
linkTitle: "allgemeine Stammdaten"
weight: 400
description: >
 allgemeine Stammdaten
---
Allgemeine Stammdaten
=====================

Für den Betrieb von **Kieselstein ERP** stehen verschiedenste allgemein verwendete Stammdaten zur Verfügung.

Diese werden in den entsprechenden Modulen oder je nach Anwendungsfall / Anwendungsbedarf zentral gepflegt.

Um eine klare sprachliche Trennung der "normalen" Stammdaten wie Personal, Artikel usw. zu den allgemeinen Stammdaten herzustellen, sprechen wir hier von Systemdaten.

### Wo kann ich die Einheit (Stück, Becher, Stunde ...) definieren?
Im Modul System ![](Modul_System.gif) finden Sie im unteren Modulreiter Sprache und hier im oberen Modulreiter Einheit. [Siehe dazu.]( {{<relref "/docs/stammdaten/artikel/mengeneinheit">}} )

## Wo kann ich Eigentumsvorbehalt und Lieferbedingungen definieren?
Im Modul System ![](Modul_System.gif) finden Sie im unteren Modulreiter Medien. Hier finden Sie Medien mit der Bezeichnung Eigentumsvorbehalt und Lieferbedingungen. Die Inhalte dieser Medien werden in allen Bewegungsformularen, welche darauf verweisen, das sind üblicherweise AG, AB, LS, RE, GS, RS sowie AF und BS, angedruckt.

## Wo kann ich Standardtexte bzw. Kopf- u. Fußzeilen für Lieferschein, Aufträge usw. definieren?
Im jedem Modul finden Sie im unteren Modulreiter ![](Grunddaten.gif). Wählen Sie im oberen Modulreiter nun den Modultext (z.B. ![](Bestellungstext.gif)). Hier finden Sie nun die Texttypen ![](Kopf-Fusstext.gif). Die Inhalte dieser Textmodule werden als Defaultvorschlag in die Konditionen des jeweiligen Bewegungs-Moduls übernommen. Die eventuell erforderlichen spezifischen Kopf- und Fußtexte können in den Konditionen des einzelnen Bewegungsmoduls übersteuert werden.

Ein Beispiel: Für ein Angebot benötigen Sie eine einführende entsprechende Bezugnahme auf Ihren Besuch beim Kunden, so können Sie dies individuell bei diesem einen Angebot in den Konditionen unter Kopftext eintragen.

## Wo kann ich die Sachbearbeiterfunktion definieren?
Im Modul System ![](Modul_System.gif) finden Sie im unteren Modulreiter ![](Reiter_Sprache.gif). Hier sind die wesentlichsten sprachabhängigen allgemeinen Datenen zusammengefasst. Im oberen Modulreiter steht nun unter dem Reiter Sachbearbeiterfunktion die Definition derselben zur Verfügung.

## Wo kann ich Zahlungsziel, Mehrwertsteuer, Kostenstellen, Spediteur, Lieferart definieren?
 <a name="Zahlungsziel, Mwst, etc."></a>
Diese Definitionen finden Sie im Modul System ![](Modul_System.gif) im unteren Modulreiter ![](Modulreiter_Mandant.gif). Hier können alle Mandantenabhängigen allgemeinen Stammdaten definiert werden.

Zu den [Incoterms siehe](#Incoterms)

Zu den [Mehrwertsteuersätzen siehe](#Definition neuer Mehrwertsteuersätze).

Zur Definition von [Zahlungszielen siehe](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Zahlungsziel).

## Wo kann ich die default Einstellungen für neue Kunden, Lieferanten usw. definieren
Auch diese Definition finden Sie im Modul System, unterer Modulreiter Mandant, oberer Reiter Lieferkonditionen. 

## Wie kann ich neue Stammdaten hinzufügen?
Wie oben bereits angeführt ist hier klar zwischen Systemdaten und Stammdaten zu unterscheiden. Wenn Sie echte Stammdaten ändern wollen, z.B. Artikel, Kunden, Personal, so rufen Sie dazu die jeweiligen Module auf. Wenn Systemdaten geändert werden sollen, so verwenden Sie dazu das Modul System. ![](Modul_System.gif)

Für manche sehr spezifische Systemdaten z.B. Ansprechpartnerfunktionen im Modul Partner, gibt es einen speziellen unteren Modulreiter ![](Modulreiter_Grunddaten.gif).

## Wo definiert man die Mehrwertsteuer?
Im Modul System, unterer Modulreiter Mandant. Hier finden Sie nun im oberen Modulreiter ![](Mehrwertsteuersatz.gif)

Definieren Sie hier die gültigen Mehrwertsteuersätze des Mandanten. Pro Mehrwertsteuersatz muss ein Eintrag angelegt werden. Im Feld UI Bezeichnung geben Sie die für Sie übliche Definition des Steuersatzes an.

Da auf diese Definitionen auch Berechnungen und Buchungsregeln definiert werden, kann pro Steuersatz nur ein Eintrag angelegt werden.

## Kann ich neue Mandaten anlegen?
Derzeit noch nicht. Siehe dazu bitte auch System, neue Mandanten anlegen. Bitte wende dich  hierzu an Ihren **Kieselstein ERP** Betreuer. Wird jederzeit gerne eingerichtet. Hilfreich ist, wenn du dafür bereits den gewünschten / benötigten Modulumfang kennst.

## Wie werden unterschiedliche / nicht vorhandene Sprachen in den Daten angezeigt?
Ist ein Beleg Kunden- oder Lieferanten bezogen, so werden die Dateninhalte in der Partnersprache angezeigt. Ist ein Beleg nicht Partner bezogen, so werden sowohl Dateninhalte als auch Programmdialoge in der Benutzersprache, welche beim Einstieg ausgewählt wurde, angezeigt.

Ist die Partnersprache nicht verfügbar, so werden die Dateninhalte in der Benutzersprache angezeigt. Ist auch diese Sprache nicht verfügbar so wird nur die Kennung angezeigt. Ist in der Auswahlliste die Kennung bereits enthalten, so bleibt die Anzeige leer.

## Die Funktionen der Sachbearbeiter sind nicht mandantenabhängig?
Da die Funktionen der Sachbearbeiter üblicherweise im ganzen Konzern gleich sind, wurden diese NICHT mandantenabhängig ausgeführt.

## Konzernmandant <-> Hauptmandant?
In den Mandantendefinitionen ist auch der Begriff Hauptmandant enthalten.

Der Hauptmandant wird für das default Verhalten von Parametern und vielen weiteren Punkten, z.B. Sprache verwendet. So ist die sogenannte Konzernsprache die Sprache des Hauptmandanten. Immer wenn bei einem Mandanten keine Zuordnung für einen Spracheintrag oder für einen Parameter nicht gefunden werden kann, wird die Information vom Hauptmandanten geholt.

Mit diesem Hintergrund erklärt sich, dass die Eigenschaft des Hauptmandanten nur vom Administrator mit einem entsprechenden Zusatzrecht verändert werden darf.

## Ur-Mandant?
Dies ist der erste angelegte Mandant üblicherweise mit der Mandantennummer 001. Dieser wird von uns für Sie entsprechend eingestellt. Beim Anlegen neuer Mandanten werden die Daten vom Ur-Mandanten in den neuen Mandanten kopiert. Prüfen Sie daher vor dem Anlegen weiterer Mandanten, dass der Ur-Mandant vollständig definiert ist.

## Länderkennzeichen, was wird verwendet?
Es war bisher üblich, die Länderkennzeichen nach den Autokennzeichen zu benennen.

Insgesamt gibt es drei Normen, welche, alle unterschiedlich, die Länderkennzeichen regeln.

DIN EN ISO 3166-1, Codes für Ländernamen. Hier wird zwischen Zwei- und Drei- Buchstaben-Codes unterschieden

Das internationale Unterscheidungskennzeichen für Kraftfahrzeuge, welche unterschiedliche Buchstabenlängen haben.

Sowohl laut Empfehlungen der Post (DE, AT) als auch laut Empfehlungen der Europäischen Kommission (http://publications.eu.int/code/de/de-370101.htm#fn1) sollten ab 2002 nur mehr die Zwei-Buchstaben-Codes der ISO 3166 verwendet werden.

## Wie kann der Adress-Aufbau für andere Länder eingestellt werden?
Wenn im System, unterer Reiter Land ein Haken bei ![](PLZ_nachOrt.JPG) PLZ nach Ort gesetzt ist, wird die PLZ in allen Adressblöcken der Belege nach dem Ort angedruckt.

Wenn Sie den Haken bei ![](postfach_strasse.JPG) Postfach mit Strasse setzen, so gilt folgender Adressaufbau:
FirmaZeile1
FirmaZeile2
AbteilungZeile3
Straße mit Hausnummer
Wenn ein Postfach angegeben, dann hier Postfach: Postfach Nr.
PLZ Ort ... Des Postfaches, wenn eines eingegeben ist, ansonsten das PLZ Ort der Hausadresse

## Land - PLZ - Ort liefert die Fehlermeldung, dass die Daten schon vorhanden sind
Wenn die Daten über System bzw. Kunden, Lieferanten oder Partner eingetragen werden, kann man den Ortsnamen manuell eintippen. Wenn der Ortsname bereits vorhanden ist, kommt die Fehlermeldung, dass die Daten schon vorhanden sind. Bitte wählen Sie den Ortsnamen aus der Liste aus, damit nicht mehrere Einträge für den gleichen Ort angelegt werden.

## Ich kann ein Land nicht mehr löschen?
Gerade in übernommenen Daten kommt es vor, dass für einige Länder doppelte und teilweise falsche Länderkennzeichen vergeben wurden. Um diese zu bereinigen korrigieren Sie zuerst die Land-Plz-Ort Zuordnungen, sodass das Land/Länderkennzeichen nicht mehr verwendet wird (z.B. in Adressen).

Nun kann üblicherweise das falsche Land ebenfalls gelöscht werden. Ist dies trotzdem nicht möglich, so wurde mit hoher Wahrscheinlichkeit das Land entweder als Staatszugehörigkeit im Personal oder als Reiseland definiert. Wenn Sie die fehlerhafte Zuordnung nicht finden, wenden Sie sich bitte an Ihrem **Kieselstein ERP** Betreuer. Er kann Ihnen sicher helfen.

## Währungscodes
Für die Währungscodes richten wir uns nach der ISO 4217, Codes für Währungen und Zahlungsmittel.
Siehe auch [www.iso.org](http://www.iso.org)
Wenn du auch SEPA / ISO20022 verwendest, so stimme bitte die Währungscodes auch mit deiner Bank ab.

## Stellenanzahl der Artikelnummer, der Kurzbezeichnung und Anzahl der Nachkommastellen.
In **Kieselstein ERP** wurden einige, allgemein übliche Einstellungen der Größen der Dateien angenommen, welche anders definiert, also erweitert werden können. Bitte beachten Sie dabei, dass alle Formulare auf die Standardstellen Anzahl ausgelegt sind. D.h. wenn zusätzliche Stellen angedruckt / angezeigt werden sollen, so ist eventuell eine Anpassung der Vorlagen erforderlich.

## Ein Teil der **Kieselstein ERP** Benutzer darf verschiedene Preise nicht sehen. Wie kann das eingestellt werden?
In den Rollenrechten können für Einkauf und Verkauf getrennt die Sichtbarkeiten der Preise gesteuert werden.

Diese wirken wie folgt:
- Recht: Darf Verkaufspreise sehen<br>
Hier werden in allen Preiseingabe Feldern keine Verkaufspreise angezeigt. Dieses Recht wirkt in:
    - Lieferschein
    - Artikel, Reiter VK-Preise und Reiter VK-Staffelpreise werden nicht angezeigt.
- Recht: Darf Einkaufspreise sehen<br>
Hier werden in allen Preiseingabe Felder keine Einkaufspreise und keine Gestehungspreise angezeigt. Dieses Recht wirkt in:
    - Bestellung: Wareneingang, Wareneingangspositionen, Bestellposition
    - Artikel, Reiter EK-Staffelpreise werden nicht angezeigt. Zusätzlich wird im Reiter Artikellieferant kein Preis und keine Rabattgruppe angezeigt und in der Auswahlliste und im Reiter Lager wird der Gestehungspreis unterdrückt.

## Definition neuer Mehrwertsteuersätze
<a name="Definition neuer Mehrwertsteuersätze"></a>
Zur Definition von Mehrwertsteuersätzen und Mehrwertsteuerarten wechseln Sie in das Modul System, unterer Modulreiter Mandant und nun für die Definition der Mehrwertsteuerarten auf den Reiter Mehrwertsteuerbezeichnung.<br>
Hier werden die grundsätzlich verschiedenen Mehrwertsteuerarten definiert. Diese sind teilweise Länderabhängig.

| Mehrwertsteuerart | Österreich | Deutschland | Schweiz |
| --- |  --- |  --- |  --- |
| steuerfrei | ja | ja | ja |
| reduzierter Steuersatz | 10% | 7% | 2,4%ab 1.1.2011 2,5% |
| normal Steuersatz | 20% | 19% | 7,6%, ab 1.1.2011 8% |

Im Reiter MWST definieren Sie, welche Steuersätze ab welchem Zeitraum gültig sind.<br>
Achten Sie bitte unbedingt darauf, bereits verwendete Steuersätze nicht zu ändern, sondern bei neuen Steuersätzen diese tatsächlich als neu ab dem Gültigkeitszeitpunkt zu definieren.

**Hinweis:** Es werden in den Positionen immer nur die zum Belegdatum passenden Mehrwertsteuersätze angezeigt. Wenn also der Mehrwertsteuersatz bisher 7,6% war (Schweiz bis Ende 2010) und ab 1.1.2011 auf 8% erhöht wird, so wird für alle Belege bis inkl. 31.12.2010 der Mehrwertsteuersatz von 7,6% verwendet.

Durch die Organisation, dass im Kunden/Lieferanten nur die Mehrwertsteuerart definiert wird, sind im Kunden/Lieferanten keine Umstellungen vorzunehmen, es genügt die Angabe des entsprechenden Belegdatums.

## Wegen der Änderung der Mehrwertsteuersätze muss der Lieferschein im gleichen Jahr verrechnet werden
Wird die Meldung
![](unterschiedliche_Mehrwertsteuersaetze.jpg) angezeigt, 
so bedeutet dies, dass aufgrund des jeweiligen Belegdatums unterschiedliche Mehrwertsteuersätze zur Anwendung kommen. D.h. entweder Sie verrechnen den Lieferschein mit einem Rechnungsdatum das zum Lieferdatum passt, oder Sie ändern den Lieferschein inkl. seinem Belegdatum so ab, dass er, aus der Sicht der Mehrwertsteuer zum Rechnungsdatum passt. Also dass für beide Belege die gleichen Mehrwertsteuersätze zur Anwendung kommen.

**Hinweis:** Für die Wirksamkeit der Mehrwertsteuer gilt immer das Datum der Leistungserbringung. Also wann konnte Ihr Kunde über die erbrachte Lieferung oder Leistung verfügen.

Das bedeutet bei allfälliger Umstellung eines Mehrwertsteuersatzes, dass die Rechnungslegung im gleichen Mehrwertsteuerzeitraum wie die Lieferung, also der Lieferschein erfolgen muss.

Ein aktuelles Beispiel: Die Mehrwertsteuerumstellung mit 1.7.2020 für Deutschland von 19% auf 16%.

Es müssen alle Lieferscheine deren Leistung bis inkl. 30.6.2020 erbracht wurden auch mit 30.6.2020 abgerechnet werden. Siehe dazu bitte auch Rechnungsneudatum.

Kann eine Lieferung erst nach diesem Stichtag abgerechnet werden, so muss auch der Lieferschein mit einem entsprechenden Belegdatum z.B. 1.7\. erstellt werden.

Diese Logik gilt auch für Anzahlungs- und Schlussrechnung. D.h. es können durchaus Anzahlungsrechnungen mit 19% (um bei dem Beispiel zu bleiben) erstellt werden. Wird nun die Schlussrechnung mit z.B. 10.7.2020 erstellt, so wird diese mit 16% ausgestellt, da ja erst mit der Schlussrechnung der Kunde über Ihre Leistung verfügen kann. Nun wird hier die Umsatzsteuer der Schlussrechnung gegen die bereits abgerechneten Umsatzsteuerzahlungen gegengerechnet, womit nur mehr die entsprechende Differenz, welche sich aus dem Unterschied der Mehrwertsteuersätze ergibt, offen bleibt.

### Incoterms
<a name="Incoterms"></a>
Entnommen aus http://de.wikipedia.org/wiki/Incoterms

| Code | Bedeutung | anzugebender Ort |
| :-- |  :-- |  :-- |
| EXW | ab Werk (engl.: **EX** **W**orks) | Standort des Werks |
| FCA | Frei Frachtführer (engl.: **F**ree **CA**rrier) | Frei vereinbarter Frachtführer |
| FAS | frei längsseits Schiff (engl.: **F**ree **A**longside **S**hip), nur für Schiffstransporte | vereinbarter Verladehafen |
| FOB | frei an Bord (engl.: **F**ree **O**n **B**oard), nur für Schiffstransporte | vereinbarter Verladehafen |
| CFR | Kosten und Fracht (engl.: **C**ost And **Fr**eight), nur für Schiffstransporte | vereinbarter Bestimmungshafen |
| CIF | Kosten, Versicherung und Fracht bis zum Bestimmungshafen/Bestimmungsort (engl.: **C**ost **I**nsurance **F**reight) | vereinbarter Bestimmungshafen/Bestimmungsort |
| CPT | Fracht, Porto bezahlt bis (engl.: **C**arriage **P**aid **T**o) | vereinbarter Bestimmungsort |
| CIP | Fracht, Porto und Versicherung bezahlt bis (engl.: **C**arriage **I**nsurance **P**aid) | vereinbarter Bestimmungsort |
| DAF | frei Grenze (engl.: **D**elivered **A**t **F**rontier) | vereinbarter Lieferort an der Grenze |
| DES | frei ab Schiff (engl.: **D**elivered **E**x **S**hip), nur für Schiffstransporte | vereinbarter Bestimmungshafen |
| DEQ | frei ab Kai (engl.: **D**elivered **E**x **Q**uay), nur für Schiffstransporte | vereinbarter Bestimmungshafen inkl. Entladung |
| DDU | frei unverzollt (engl.: **D**elivered **D**uty **U**npaid) | vereinbarter Bestimmungsort im Einfuhrland |
| DDP | frei verzollt (engl.: **D**elivered **D**uty **P**aid) | vereinbarter Lieferort im Einfuhrland |