---
categories: ["Bedienung"]
tags: ["wo finde ich was"]
title: "wo finde ich was"
linkTitle: "wo finde ich was"
weight: 500
description: >
 Wo finde ich was, welche Daten, welches Modul?
---
Wo finde ich was?
=================

Nachdem bei den Installationen immer wieder Fragen auftreten wo man denn nun die Definition welcher allgemeiner Daten findet, hier eine kompakte Zusammenfassung der üblichen Fragen.

| Begriff / Thema | Modul | unterer Modulreiter | wo definieren | Bemerkung |
| --- |  --- |  --- |  --- |  --- |
| Zahlungsziel | System | Mandant | Zahlungsziel |   |
| Ort |   |   |   | Zum Neuanlegen:m Orte Auswahldialog den neu Knopf benutzen und den neuen Ort, die neue Postleitzahl anlegen |
| Ort | System | System | Ort | zum korrigieren von ev. falsch geschriebenen Orten |
| Bank | Partner | Bank |   |   |
| Ansprechpartner Funktion | Partner | Grunddaten | Ansprechpartner Funktion |   |
| Mahnstufen | Rechnung | Grunddaten | Mahnstufe |   |
| Mahntexte | Rechnung | Grunddaten | Mahntexte |   |
| Eigentumsvorbehalt | System | Medien | Textbausteine | Bitte nur Texte, keine PDFs, nicht löschen. Wenn diese nicht gedruckt werden sollten einen Punkt (.) eingeben |
| Lieferbedingungen | -- " -- | -- " -- | -- " -- | -- " -- |
| Mengeneinheiten | System | Sprache | Einheit |   |
| Textbausteine | System | Medien | Textbausteine |   |