---
categories: ["Stammdaten","Bewegungsdaten"]
tags: ["Stammdaten", "Bewegungsdaten"]
title: "Stammdaten <-> Bewegungsdaten"
linkTitle: "Stammdaten <-> Bewegungsdaten"
weight: 300
description: >
 Unterschied zwischen Stammdaten und Bewegungsdaten
---
Zusammenhang zwischen Stammdaten und Bewegungsdaten
===================================================

Da wir, insbesondere bei Menschen die noch nie mit einem strukturierten System gearbeitet habe, immer wieder auf die Frage stoßen, wo muss ich denn nun meine Daten pflegen um das was ich möchte erreichen zu können, hier eine kompakte Zusammenfassung der wesentlichen Strukturen.

Diese Beschreibung ist, im Sinne der Übersicht, bei weitem nicht vollständig, sollte aber die Idee darstellen.

Für eine Sicht auf die Abläufe, die üblichen Workflows, sei auf die [Zusammenfassung]( {{<relref "/start/20_workflows">}} ) verwiesen.

## Wozu überhaupt diese Strukturen?
Im wesentlichen will man durch gute Stammdaten erreichen, dass jeder berechtigte Mitarbeiter im Unternehmen jederzeit die Information bekommt, sich holen kann, die er benötigt. Z.B. ruft ein Kunde an und ich benötige dringend ein Angebot für ... . Sind nun die Stammdaten ordentlich gepflegt, so kann jedeR MitarbeiterIn aus dem Bereich des Verkaufes, also auch ein Azubi, ein Lehrling, ein richtiges Angebot mit den richtigen Preisen erstellen. Der wesentliche Vorteil, die Geschäftsleitung, die Verkaufsleitung wird nur mehr mit den Sonderfällen belastet und kann sich so um die Kernaufgabe kümmern.

Zusätzlich erreicht man damit, dass man eine Aussage über die Vergangenheit, in der Regel in den Statistiken und eine Aussage über die Zukunft (Bewegungsvorschau, offene Aufträge, ..) bekommt. 

D.h. einfach nur durch das System konform verwenden, so werden neben her eine Menge an weiteren Unternehmensinformationen zur Verfügung gestellt, welche für die Steuerung des Unternehmens wichtig sind.

Anmerkung: Solche Daten kann per Definition ein reines Buchhaltungsprogramm nicht liefern, Sie benötigen dafür ein vollständiges und umfassendes ERP System, wie **Kieselstein ERP**.

Dass diese strukturieren Daten mit den beliebten Office-Paketen faktisch unmöglich verlässlich zu erzeugen sind, liegt in der Natur der Sache.

## Datenstruktur
Wie sieht nun die Datenstruktur im wesentlichen aus, oder auch, wo müssen welche Daten gepflegt werden um rasch und effizient zu den gewünschten Ausdrucken zu kommen.

Wir nutzen für die Beschreibung das Modul Auftrag.

Die wesentlichsten Stammdaten sind:
-   Kundenstamm (Auftraggeberadresse, Lieferadresse, Rechnungsadresse)
-   Artikelstamm (Verkaufspreise, Mengenstaffeln, Sonderkonditionen)
-   weitere Basisdaten (Zahlungsziele, Eigentumsvorbehalt, ...)

Im Auftrag finden Sie nun die Bewegungsdaten, wie
-   den Kunden der den Auftrag erteilt hat
-   die Positionen die er bestellt hat
-   und zu welchen Konditionen er bestellt hat

Und damit wissen Sie:
-   wann Sie welche Positionen / Artikel an wen liefern sollten,
-   wieviel davon noch nicht geliefert ist
-   welche Verkaufserlöse für die Zukunft geplant sind
-   welche Artikel Sie dafür einkaufen bzw. fertigen müssen
-   wann Sie dem Kunden schon eventuell Teillieferungen, einzelne Positionen gesandt haben.

Das Bild dazu, ![](Stammdaten_Bewegungsdaten.png)  

Diese strukturierte Vorgehensweise zieht sich komplett durch dein **Kieselstein ERP**.<br>
So bewirkt selbstverständlich eine auftragsbezogene Lieferung, also ein auftragsbezogener Lieferschein, immer eine aktuelle offene Posten Liste der noch zu liefernden Auftragspositionen. Womit dein offener Auftragsstand wiederum aktuell ist und du jederzeit diese Information aktuell und richtig zur Verfügung hast.

Nutze daher die Mächtigkeit deines **Kieselstein ERP** und pflegen deine / eure Stammdaten. Sorgen dafür, dass diese **jederzeit** nach Bestem Wissen **richtig** sind.

Damit kannst du den Kreislauf deines Unternehmens abbilden und die Erkenntnisse zum Wohle des Unternehmens nutzen.

So wie wir dies in unserem Unternehmenskreislauf dargestellt haben.
![](Unternehmenskreislauf_2023.png)