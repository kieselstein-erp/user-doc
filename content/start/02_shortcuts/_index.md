---
categories: ["Bedienung"]
tags: ["Shortcuts"]
title: "Shortcuts"
linkTitle: "Shortcuts"
weight: 200
description: >
 Erklärung der Bedienung deines **Kieselstein ERP**, Shortcuts, Schnellzugriffstasten
---
Tastaturbedienung von **Kieselstein ERP**
==============================

Um **Kieselstein ERP** schnell bedienen zu können wurden Tastaturbedienung und Direkt Filter implementiert.

Die Tastaturbedienung erfolgt im wesentlichen über die allgemein üblichen Shortcuts (Schnellzugriffstasten). Die direkt Filter finden Sie praktisch in jeder Auswahlliste über den Spaltenüberschriften.

### Direkt Filter
<a name="Direktfilter"></a>
Mit dem Direkt Filter kann sehr schnell ein bestimmter Datensatz, z.B. ein Lieferschein, ausgewählt werden. Um die verschiedenen Funktionalitäten zu beschreiben, wird hinter dem Feldnamen die Funktionalität symbolhaft dargestellt
![](Shortc1.gif)

Dabei bedeutet:

| Anzeige | Beschreibung |
| --- |  --- |
| Aa ... | Die Eingabe ist nicht von Groß-/Kleinschreibung abhängig. |
| A* ... | Es werden jene Datensätze angezeigt, bei denen im Feldnamen (Kunde) von links herein die entsprechenden Zeichen vorhanden sind. |
| *A ...  | Kommt meistens bei der Eingabe einer Belegnummer zur Anwendung. ![](Shortc2.gif)Bedeutet, dass die eingegebenen Zeichen (normalerweise Zahlen), nach links anhand der Regel der Nummernerzeugung des Beleges aufgefüllt werden. Mit diesem Direktfilter kann sehr komfortabel auch auf Belege aus früheren Jahren gesprungen werden. Geben Sie hier lediglich nach der Belegnummer durch Komma oder Punkt getrennt das Jahr an. z.B. 567,22 bringt Sie zum Beleg 22/0000567, wenn dieser vorhanden ist. |
| *A*.. | Bedeutet, dass der gesuchte Begriff in den durchsuchten Textstrings gesucht wird |
| A+B-C | Kombinierte Suche, manchmal auch Google Suche genannt. D.h. es werden die gesuchten Begriffe durch Leerzeichen getrennt eingegeben. Es müssen die Begriffe in den durchsuchten Feldern enthalten sein, jedoch in beliebiger Reihenfolge. Darf ein Begriff nicht vorkommen, so geben Sie davor ein - (Minuszeichen) an. |

Sind mehrere Direkt Filter angegeben und wurde in diese auch Daten eingegeben, so bewirken diese ein logisches Und. D.h. es müssen für beide Felder die Filterregeln erfüllt sein.<br>
Die Anwendung des Filters wird durch Betätigen der Enter-Taste ausgelöst. Mit der Tab-Taste können Sie vom ersten zum zweiten direkt Filter wechseln.

Zusätzlich können Sie im Direkt Filter auch nach Teilen von Namen suchen. Mit der Eingabe von % wird ein beliebiges Zeichen für die Suche definiert. Ein Beispiel:

![](Shortc3.gif)
Das bedeutet: Wird als erstes Zeichen % eingegeben, so wird nach dem Auftreten der Folgezeichen in dem zu durchsuchenden Datenfeld gesucht. In unserem Beispiel sollten alle Partner angezeigt werden, die schw(arz) im Namen haben. Also wird %schw eingegeben und sie erhalten die "Apotheke Zum Schwarzen Adler" und die "Kreis-Apotheke zum Schwarzen Falken"

**WICHTIG:** Wenn eine Filterbedingung wieder gelöscht werden soll, müssen Sie die Eingaben daraus entfernen und mit Enter bestätigen. Zusätzlich sind Direktfilter (also nur Text-/Nummernfelder) mit Rechtsklick löschbar.

### Suche nach Belegnummern
<a name="Suche nach Belegnummern"></a>
A: Um die Suche nach Belegnummern, wie z.B. die Nummer einer Ausgangsrechnung so einfach wie möglich zu gestalten, haben wir folgende Vorgehensweise gewählt.

Hintergrund: In **Kieselstein ERP** werden die Belegnummer in der Regel mit Jahr/laufende Nummer dargestellt. Diese können 2 oder 4stellig für das Jahr und bis zu 7Stellen für die laufende Nummer sein.

Nun wäre eine Eingabe von 2022/0000123 möglich, ist in der Regel aber sehr mühsam. Daher gilt folgende Definition.

Wird von einer Belegnummer nur der hintere Teile, in unserem Beispiel als die 123 eingegeben, so wird versucht diese Nummer im laufenden Geschäftsjahr zu finden. Wird sie im laufenden Geschäftsjahr nicht gefunden, so wird versucht diese Nummer im vergangenen Geschäftsjahr zu finden, wird diese auch nicht gefunden, wird nichts angezeigt. Der Vorteil dieser Logik ist, dass in aller Regel beim Geschäftsjahreswechsel die hohen Nummern das vergangene Jahr und die niedrigen Nummer das laufende Jahr betreffen. D.h. Sie finden rasch den betroffenen (meist offenen) Beleg. Wenn nun ein Beleg aus einem älteren Geschäftsjahr gesucht werden sollte, so kann nach der Nummer mit Komma oder Punkt getrennt das gewünschte Geschäftsjahr angegeben werden. So würde 123,20 den Beleg aus dem Jahr 2020 suchen.

Um nun alle Belege mit der Nummer 123 zu finden, kann vor der Nummer mit einem ! (Ausrufezeichen) angegeben werden, dass alle Jahre gesucht werden sollten. D.h. Sie finden damit 2022/0000123, 2021/0000123, 2020/0000123 usw. 

#### Welche Suchfunktionen können bei den Filtern eingegeben werden?
Bei der Suche können folgende Platzhalterzeichen (Wildcards) eingegeben werden.
| Wildcard | Wirkung |
| --- | --- |
| % | bedeutet der dem % folgende Text kann irgendwo danach kommen |
| _ | bedeutet beliebiges Zeichen an dieser Stelle |

Bitte beachten Sie, dass bei % die Suche trotzdem immer von links nach rechts durchgeführt wird.

Ein Beispiel:

Textstring z.B. in der Artikelbezeichnung:

    Faston isoliert rot 6,8mm

Wird nun F%rot%6 im Direktfilter eingegeben, so wird der Artikel gefunden. Ist aber von der Eingabe her nicht klar, ob als Dezimaltrenner der Punkt (.) oder das Komma (,) eingegeben wurden, und Sie müssen exakt auf 6,8 suchen, so sollte F%rot%6_8 eingegeben werden.

Wichtig: Die %-Suche ist immer von Links nach rechts. Das bedeutet wenn Sie in unserem Beispiel nach F%rot%iso% suchen, werden Sie keine Suchergebnisse erhalten. Daher wurde im Artikelmodul die kombinierte Suche geschaffen.

Spezielle Suche in der Elektronikbranche.

In der Elektronikbranche werden bei vielen Artikeln die Genauigkeiten in % angegeben. Um exakt z.B. nach 1% suchen zu können muss das gesuchte Zeichen mit einem Fluchtsymbol, also dem \ (Backslash) gekennzeichnet werden.

**Beispiel:** Wir suchen nach einem Widerstand mit 120R und 1%.
Sie geben in den Direktfilter bitte 120%1\% ein. Hier ist das Backslash-Zeichen die Kennung um nach einem Steuerzeichen zu suchen.

### Suche nach Telefonnummern
Nutze dafür bitte das Feld zur Suche nach Telefonnummern ![](telefonnummer.JPG).

## Wie kann die Auswahlliste weiter eingeschränkt werden?
In vielen Auswahllisten steht die Funktionalität der sogenannten Zusatzfilter zur Verfügung. Die Bedienung dieser zusätzlichen Einschränkung der angezeigten Daten ist grundsätzlich wie folgt, wobei als Beispiel die Auswahl der Partnerart im Modul Partner verwendet wird.

Klicken Sie auf  ![](Zusatzfilter.gif).

Nun wählen Sie das zusätzliche Filterkriterium aus. In unserem Fall Partnerart.

![](Direktfilter1.gif)

Klicken Sie nun auf das Plus neben dem gewählten Kriterium.

Sie können nun den Text für das gewählte Kriterium eingeben.

![](Direktfilter2.gif)

Klicken Sie nach der Eingabe auf aktualisieren ![](Direktfilter_aktualiseren.gif), damit die Filterbedingung ausgeführt wird.

Durch einen erneuten Klick auf das grüne Plus können weitere Filterbedingungen, bis zu drei, hinzugefügt werden. Durch Klick auf Minus kann die gewählte Filterbedingung wieder entfernt werden.

#### Können Auswahllisten sortiert werden?
<a name="Sortierung Spaltenüberschriften"></a>
Die Auswahllisten können normalerweise sortiert werden. Die Grundsortierung ist meist so, dass der aktuellste Eintrag oben ist. Sollte eine andere Sortierung erwünscht sein, so klicken Sie mit der Maus bitte auf die gewünschte Spalte. Durch diesen Klick wird die Sortierung nach der Spalte in aufsteigender Form vorgenommen. ![](Spalten_Sortierung.gif) Für eine absteigende Sortierung klicken Sie bitte erneut

auf die Spalte. Eine mit ![](Nicht_sortierbar.gif) gekennzeichnete Spalte kann nicht sortiert werden.

Die Sortierungen können auch bei angewendeten (Direkt-) Filtern verwendet werden.

## Auswahllisten
Die Auswahllisten werden über den sogenannten FLR (FastLaneReader) zusammengestellt.

Üblicherweise sind den Auswahllisten auch die Direkt Filter zugeordnet. Damit können Sie sehr komfortabel und schnell auf ausgewählte Belege / Datensätze springen. Üblicherweise kann die Anzeige einer Liste nach jeder der Spalten auf oder absteigend sortiert werden. Klicken Sie dazu einfach auf die gewünschte Spaltenüberschrift. Wenn nach einer Spalte nicht sortiert werden kann, so ist diese mit ![](Nicht_sortierbar.gif)

gekennzeichnet.

### Textsuche bei den Auswahllisten
In einigen Auswahllisten steht auch eine Textsuchen in den zugehörigen Positionsdaten zur Verfügung. Die Textsuche erstreckt sich immer auf die bisher eingegebene Filterbedingung. Das bedeutet: Ihre Textsuche führt um so schneller zum Ergebnis, desto weniger Einträge zu durchsuchen sind. Suchen Sie zum Beispiel Texte nur für einen bestimmten Kunden, so empfiehlt sich, den Kunden und dann den gesuchten Text einzugeben. Durch den Kunden reduzieren Sie die zu durchsuchenden Datenmengen und erhalten so sehr rasch das gewünschte Ergebnis.

### Erweiterte Suche in den Partnerdaten
Siehe bitte direkt bei Partner

### Anlegen neuer Stammdaten direkt aus den Auswahllisten heraus.
Bei einigen ausgewählten Auswahllisten finden Sie neben dem Refresh Button auch einen ![](Neu_aus_Auswahl.gif) Neu Knopf. 

Damit wird signalisiert, dass diese Auswahlliste direkt erweitert werden kann. Um z.B. einen Ansprechpartner oder einen Ort neu anzulegen, klicken Sie einfach auf den Neu Knopf. Die entsprechende Modulberechtigung vorausgesetzt, werden Sie vom Programm direkt in das entsprechende Modul geführt und können hier die Daten für die neue Stammdate eingeben. Z.B. den neuen Ansprechpartner definieren.   

## Tastaturbedienung
In einer Auswahlliste werden Tasteneingaben immer in das erste Direkt-Filter-Feld geleitet. Dies bedingt zugleich, dass der Fokus am ersten direkt Filter sitzt. Wird dieser Filter nun mit Enter bestätigt, so werden die Filterbedingungen auf die Daten angewandt und die daraus resultierende Liste der Daten dargestellt. Nun sitzt der Fokus der Eingabe wieder in der Auswahlliste und Sie können mit den Cursortasten in der Auswahlliste positionieren und mit der Enter-Taste den gewünschten Datensatz auswählen. Wurden aufgrund der Filterbedingung keine Ergebnisse geliefert, so bleibt der Fokus auf dem Direkt Filter erhalten, damit Sie die Filterbedingung entsprechend ändern können.

Die Cursortasten wirken als Positionierung des Cursorbalkens in der Auswahlliste.

Durch Betätigen von Enter gelangen Sie in den logisch nächstfolgenden Modulreiter. Das ist bei den Bewegungsdaten üblicherweise die Position, bei den Stammdaten die Detailsicht.

Derzeit sind folgende Shortcuts implementiert / geplant:

| Funktion | Shortcut | Bemerkung |
| --- |  --- |  --- |
| Neu | Strg+N |   |
| Speichern | Strg+S |   |
| Ändern | Strg+U | U ... für Update |
| Löschen | Strg+D | D ... für Delete |
| Drucken | Strg+P | P ... für Print |
| Verwerfen | Strg+Z |   |
| Texteditor | Strg+T |   |
| Etiketten drucken | Strg+Shift+P |   |
|   |   |   |
| Zurück in die Auswahlliste | Alt+1 |   |
| Direkter Sprung in z.B. Positionen etc. | Alt+3 | Die ersten neun Reiter jedes Moduls können direkt mit der ALT+Ziffer angesprungen werden. Die Ziffer wird direkt bei den Reitern angezeigt. |
| Aktualisieren | F5 | Refresh oder auch Read |
| Schließen des Moduls | Strg+F4 |   |
| Schließt **Kieselstein ERP** | Alt+F4 |   |

**Mnemonics:**

Als Mnemonics bezeichnet man die Tasten für die Menüsteuerung. Da in **Kieselstein ERP** zwei Menüs zur Verfügung stehen, haben wir folgende Lösung gewählt:

Wenn Sie die Alt-Taste alleine drücken und wieder loslassen, gelangen Sie in das Hauptmenü. Um durch das Menü zu navigieren drücken Sie nun die unterstrichenen Buchstaben / Zeichen.

Wenn Sie die Alt-Taste drücken und danach einen Buchstaben drücken, so wirkt dies im Modul Menü wiederum auf den unterstrichenen Buchstaben.

Grundsätzlich kann man sich folgende Regel merken:
- **Strg+Taste** wirkt im aktuellen Panel und ist auch für Sonderfunktionen reserviert
- **Alt+Taste** wirkt im Modulmenü
- **Alt** drücken und wieder loslassen und danach eine Taste wirkt im Hauptmenü.

Die Funktionstasten haben wir für folgende Spezialfunktionen reserviert, wobei wir auch hier versucht haben eine einheitliche Verwendung in allen Modulen zu erreichen.

Beschreibung der Tastaturbedienung des Druckdialoges

Auch im Druckdialog steht die Tastatur für die schnelle Navigation zur Verfügung.

Bitte beachten Sie den Unterschied zwischen Druckdialog
![](DruckVorschau_Dialog.jpg)

und der reinen Druckvorschau.

![](ReineDruckvorschau.gif)

| Shortcut | Funktion | Bemerkung |
| --- |  --- |  --- |
| Vergrößern der Ansicht | + am Numerischen Ziffernblock |   |
| Verkleinern der Ansicht | - am Numerischen Ziffernblock |   |
| Um eine Seite vorblättern | Bild ab |   |
| Um eine Seite zurückblättern | Bild auf |   |
| Zur ersten Seite | Pos1 | (Home) |
| Zur letzten Seite | Ende |   |
| Aktualisieren des Inhaltes des Druckdialoges | F5 |   |
| Exportieren / Speichern des Druckdialoges | Strg+S |   |
| Anzeige der Daten in der reinen Vorschau | Strg+R |   |
| Tatsächliches Drucken an den Drucker | Strg+P |   |
| Als Fax versenden | Strg+F | Hier muss die Faxnummer angegeben werden. |
| Als EMail versenden | Strg+E | Hier muss die EMail-Adresse angegeben werden |
| Verlassen des Druckdialoges | ESC |   |
| Verlassen des Vorschaufenster | Alt+F4 |   |

Verwendete Tastenkombinationen in Eingabefeldern. **ACHTUNG:** Dies ist auch Betriebssystemabhängig!

| Taste | Zweck |
| --- |  --- |
| F1 | Anwendungshilfe anzeigen |
| UMSCHALT+F1 | Tipps (kontextbezogene Hilfe) neben dem ausgewählten Steuerelement anzeigen |
| Strg+C | Ausgewählte Elemente kopieren |
| Strg+X | Ausgewählte Elemente ausschneiden |
| Strg+V | Ausgeschnittene oder kopierte Elemente einfügen |
| Strg+Z | Letzten Vorgang rückgängig machen |
| Strg+Y | Letzten Vorgang wiederholen |
| Esc | Aktuellen Task abbrechen |
| Entf | Ausgewählte Elemente löschen |

Für Windows® Betriebssysteme gibt es noch eine nützliche Tastenkombination:

| Taste | Zweck |
| --- |  --- |
| Windows-Taste+D | Anzeige des Desktops bzw. wenn nur der Desktop angezeigt wird, dann wiederherstellen der letzten Ansicht |
| Windows-Taste+L | Start des Sperrbildschirms (Logg out) |

## Eingabe von Sonderzeichen:
Grundsätzlich wird von **Kieselstein ERP** der Unicode Zeichensatz unterstützt. Sie können damit praktisch alle Zeichen welche auf nationalen Computern verfügbar sind darstellen. Sei dies Kyrillisch, Griechisch, .... usw.<br>
Trotzdem möchten wir hier die Erzeugung einiger Sonderzeichen anführen welche im deutschen Sprachraum oft zusätzlich gebraucht werden, aber auf den Standard Tastaturen nicht verfügbar sind.

Vom **Kieselstein ERP** Client wird der Tahoma Zeichensatz verwendet. Alle darin vorkommenden Zeichen werden auch von **Kieselstein ERP** dargestellt.

In den Texteingaben wird der von Ihnen eingestellte Zeichensatz verwendet. In Multiclient Umgebungen beachten Sie bitte dass alle Schriften auf allen unterschiedlichen Plattformen vorhanden sein müssen.

| Zeichen | Tastatureingabe |
| --- |  --- |
| ø | ALT+155 am numerischen Ziffernblock |
| Ø | ALT+157 am numerischen Ziffernblock |

[Siehe dazu auch ASCII-Tabelle.](ascii.pdf)

## Belegung von Funktionstasten
Insbesondere für die rasche Erfassung mit Barcodes sind in **Kieselstein ERP** in einigen Programmmodulen hinter den jeweiligen Icons auch Shortcuts der Funktionstasten hinterlegt. Diese sind vor dafür gedacht, dass die Barcodescanner als Pre-ScannCode diese Taste senden, um damit sofort in die Erfassungsmaske springen zu können. Die Funktionalität der Erfassung steht immer in der Auswahlliste zur Verfügung.

Derzeit werden folgende Funktionstasten verwendet:

| Modul | Funktionstaste | Funktionalität |
| --- |  --- |  --- |
| Los (Fertigung) | F12 | Setzen Los Zusatzstatus |
| Los (Fertigung) | F11 | Erledigen/Abliefern Lose anhand Auftrags-Packliste |
| Los (Fertigung) | F10 | Tops Artikel abliefern |
| Artikel | F12 | Lagerplatz definieren |
| Auftrag | F11 | Auftrags-Positions-Seriennummern Etiketten drucken |
