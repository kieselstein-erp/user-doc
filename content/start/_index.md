---
categories: ["Start"]
tags: ["wie beginnen"]
title: "Start, wie mit dem Kieselstein ERP beginnen"
linkTitle: "Start, mit ERP beginnen"
weight: 2
description: >
 Wie die Arbeit mit dem Kieselstein ERP beginnen
---
Erste Schritte für die Arbeit mit **Kieselstein ERP**
=====================================================

Für einen möglichst raschen Einstieg in **Kieselstein ERP** empfehlen wir folgende Vorgehensweise:

## Voraussetzungen:
- **Kieselstein ERP** ist in deinem / euren Unternehmen ordnungsgemäß installiert
- Der **Kieselstein ERP** Client ist auf deinem Rechner installiert
- Benutzername, Passwort und default Mandant sind definiert und bekannt.

Bitte beachten Sie auch das [Schulungskonzept]( {{<relref "/docs/schulung/schulungsprogramm/schulungsprogramm_dateil">}} ).

1.  Machen Sie sich mit der [grundsätzlichen Bedienung]( {{<relref "/start/01_grundsätzliche_bedienung" >}} ) von **Kieselstein ERP** vertraut.

2.  Danach machen Sie sich mit den für Sie relevanten Stammdaten vertraut, z.B. [Kunde]( {{<relref "/docs/stammdaten/kunden" >}} ), [Personal]( {{<relref "/docs/stammdaten/personal" >}} ) oder [Artikel]( {{<relref "/docs/stammdaten/artikel" >}} ).
    Beachten Sie bitte den zentralen [Partnerstamm]( {{<relref "/docs/stammdaten/partner" >}} ).
    Beachten Sie die Arbeitsweise mit den [Direktfiltern](../Allgemein/Shortcuts.htm#Direktfilter) und den [Spaltenüberschriften](../Allgemein/Shortcuts.htm#Sortierung Spaltenüberschriften).

3.  Nun verwenden Sie die für Sie relevanten Bewegungsmodule
    -   Verkauf -> [Angebote]( {{<relref "/verkauf/angebot" >}} )
    -   Einkauf -> [Bestellungen]( {{<relref "/einkauf/bestellung" >}} )
    -   Fertigung -> [Stücklisten]( {{<relref "/warenwirtschaft/stueckliste" >}} ) / [Fertigungsaufträge]( {{<relref "/fertigung" >}} )
    -   Beachten Sie dazu auch die [Gemeinsamkeiten der Bewegungsmodule]( {{<relref "/verkauf/gemeinsamkeiten">}} )
    -   Für die Zeiterfassung beachten Sie bitte die Vorgehensweise zum [ersten Einsatz.]( {{<relref "/fertigung/zeiterfassung/erster_einsatz">}} ) []( {{<relref "/verkauf/gemeinsamkeiten">}} )

4.  Drucken Sie einen der Bewegungsdatensätze und beachten Sie die Möglichkeiten beim [Drucken]( {{<relref "/docs/stammdaten/system/drucken" >}} )

5.  Zur leichteren Orientierung haben wir die Module in der Toolbar nach Themengruppen sortiert.
    Nachfolgend die Auflistung aller in **Kieselstein ERP** verfügbaren Module in der auch am Client verwendeten Gruppierung.
    -   Warenwirtschaft
        -   [Artikel]( {{<relref "/docs/stammdaten/artikel" >}} )
        -   [Stücklisten]( {{<relref "/warenwirtschaft/stueckliste" >}} )
    -   Einkauf
        -   [Lieferanten]( {{<relref "/docs/stammdaten/lieferanten" >}} )
        -   [Anfrage]( {{<relref "/einkauf/anfrage" >}} )
        -   [Bestellungen]( {{<relref "/einkauf/bestellung" >}} )
        -   [Eingangsrechnungen]( {{<relref "/einkauf/eingangsrechnung" >}} )
    -   Fertigung
        -   [Fertigungslose]( {{<relref "/fertigung" >}} )
            - [Küche]( {{<relref "/fertigung/reklamation" >}} )
            - [Instandhaltung]( {{<relref "/fertigung/reklamation" >}} )
        -   [Reklamation]( {{<relref "/fertigung/reklamation" >}} )
        -   [Zeitbewirtschaftung]( {{<relref "/fertigung/zeiterfassung" >}} )
        -   [Personal]( {{<relref "/docs/stammdaten/personal" >}} )
    -   Verkauf
        -   [Kunden]( {{<relref "/docs/stammdaten/kunden" >}} )
        -   [Projektverwaltung / ToDo Liste]( {{<relref "/verkauf/projekt" >}} )
        -   [Angebot]( {{<relref "/verkauf/angebot" >}} )
        -   [Angebotsstücklisten]( {{<relref "/verkauf/angebotsstueckliste" >}} ), Verkauf und Einkauf
            -   [Inseratenverwaltung]( {{<relref "/verkauf/auftrag" >}} )
        -   [Forecast]( {{<relref "/verkauf/auftrag" >}} )
        -   [Aufträge, mit Rahmen und Abrufen]( {{<relref "/verkauf/auftrag" >}} )
        -   [Lieferscheine]( {{<relref "/verkauf/lieferschein" >}} )
        -   [Rechnungen]( {{<relref "/verkauf/rechnung" >}} ) mit [Gutschriften]( {{<relref "/verkauf/rechnung/gutschrift" >}} )
    -   Management
        -   [Zutritt]( {{<relref "/extras/Zutritt" >}} )
        -   [Finanzbuchhaltung / Kontendefinition / Export]( {{<relref "/management/finanzbuchhaltung" >}} )
        -   [Dashboard]( {{<relref "/extras/Zutritt" >}} )
        -   [360° Kundensicht]( {{<relref "/extras/Zutritt" >}} )
    -   Extras
        -   [Partner, zentraler Adressbestand]( {{<relref "/docs/stammdaten/partner" >}} )
        -   [Benutzer, Rollen und Rechte]( {{<relref "/docs/stammdaten/benutzer" >}} )
        -   [System]( {{<relref "/docs/stammdaten/system" >}} ), von der Orts-Definition bis zu den Textmodulen
        -   [Nachrichtensystem]( {{<relref "/extras/Zutritt" >}} )
        -   [Geodatenanzeige]( {{<relref "/extras/Zutritt" >}} )

    Hinweis: Da die zur Verfügung stehenden Module von den bei dir/euch freigeschalteten  Modulen und den Rollenrechten abhängig ist, müssen nicht alle angeführten Module auch in deinem Client zur Verfügung stehen.

6.  Warum sind manche Bewegungsdatensätze änderbar und manche nicht mehr.<br>
    Der generelle Gedankengang ist, dass ein Bewegungsdatensatz solange änderbar ist, bis es einen Nachfolger gibt. Weitere Details sieht man in den Statusdiagrammen der jeweiligen Modulen.

7.  Es muss bitte eineN Administrator für die Pflege deines **Kieselstein ERP** Systems geben. DieseR ist für die Abstimmung der verschiedenen Bedürfnisse in eurem Unternehmen verantwortlich und hält die Kommunikation mit der Community / den Consultants.

## Konfiguration eurer **Kieselstein ERP** - Installation
Neben der Konfiguration von **Kieselstein ERP** entsprechend der Basisinstallation durch deinen **Kieselstein ERP** Betreuuer stehen weitere Einstellungen zur Verfügung.

Mit diesen Einstellungen kann/muss **Kieselstein ERP** an die Anforderungen in eurem  Unternehmen angepasst werden.

Grundsätzlich unterscheidet **Kieselstein ERP** in Arbeitsplatz- und Mandantenparameter. Eine Liste der Mandantenparameter und deren Wirkung finden Sie [hier]( {{<relref "/docs/stammdaten/system/parameter">}} ).

Gerade zu Beginn ist es essentiell u.a. Belegnummern und das Geschäftsjahr zu definieren.

Verwenden Sie für die Definition des Geschäftsjahres die Parameter:
- GESCHAEFTSJAHRBEGINNMONAT
- GESCHAEFTSJAHRPLUSEINS

Hiermit definieren Sie den Aufbau der Belegnummern, wieviele Stellen die Jahreszahlen und die fortlaufende Nummer erhalten soll:
- BELEGNUMMERNFORMAT_STELLEN_BELEGNUMMER
- BELEGNUMMERNFORMAT_STELLEN_GESCHAEFTSJAHR
- BELEGNUMMERNFORMAT_STELLEN_ZUFALL
- BELEGNUMMER_MANDANTKENNUNG
- BELEGNUMMERNFORMAT_TRENNZEICHEN

Mit den folgenden Parametern können Sie die Startwerte der Belegnummern für den Jahresbeginn / den Start mit **Kieselstein ERP** bestimmen. Bitte achten Sie darauf eventuell im folgenden Jahr den Parameter wieder auf den Wert 1 zurückzustellen.
- ANFRAGE_BELEGNUMMERSTARTWERT
- ANGEBOT_BELEGNUMMERSTARTWERT
- AUFTRAG_BELEGNUMMERSTARTWERT
- BESTELLUNG_BELEGNUMMERSTARTWERT
- EINGANGSRECHNUNG_BELEGNUMMERSTARTWERT
- GUTSCHRIFT_BELEGNUMMERSTARTWERT
- LIEFERSCHEIN_BELEGNUMMERSTARTWERT
- LOS_BELEGNUMMERSTARTWERT_FREIE_LOSE
- PROJEKT_BELEGNUMMERSTARTWERT
- RECHNUNG_BELEGNUMMERSTARTWERT
- REKLAMATION_BELEGNUMMERSTARTWERT

**ACHTUNG:** Änderungen an der Struktur der Belegnummern dürfen nur bei einer Neuinstallation vorgenommen werden. Änderungen in laufenden System müssen auch auf die bestehenden Daten Rücksicht nehmen und sollten daher nicht durchgeführt werden. Für Änderungen in laufenden Systemen wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

Für die Anpassung von Reports, also Ausdrucken aus **Kieselstein ERP** finden Sie [hier]( {{<relref "/docs/stammdaten/system/druckformulare">}} ) eine umfangreiche Beschreibung.

Um die Konditionen der Lieferanten und Kunden vor dem Versenden von Belegen zu überprüfen, ist in der Basisinstallation der Parameter KONDITIONEN_DES_BELEGS_BESTAETIGEN für die Belege aktiviert. Dadurch erscheint eine Meldung, sobald die Konditionen eines Beleges nicht bestätigt wurden. Gerne können Sie den Parameter auf 0 stellen und so diese Meldung deaktivieren.

Definieren Sie unternehmensintern und unternehmensweit eine Struktur für Artikelnummern. Je nach Bedarf kann diese sprechend mit Abkürzungen oder fortlaufend ohne Bezug zum Artikel selbst gestaltet werden. Um die Zeichen der Artikelnummer festzulegen verwenden Sie den Parameter ARTIKELNUMMER_ZEICHENSATZ, besonders bei der Verwendung von Barcodes achten Sie bitte darauf keine Sonderzeichen anzuführen.

#### Können Belegnummern mit einer Zufallszahl fortlaufend eingestellt werden?
Wenn Sie den Parameter BELEGNUMMERNFORMAT_STELLEN_ZUFALL auf > 0 stellen, so wird das Ende der Belegnummer um einen Zufallswert ergänzt.

Bsp. Wenn Belegnummer 70 und die Anzahl der Zufalls-Stellen = 2: 70 wird mit 10^2 multipliziert und ein Zufallswert von 0-99 hinzugefügt = 7033

Diese Berechnung wird in AG/AB/LS/RE/GS/PROFORMA unterstützt.

## Was ist bei der Einführung von **Kieselstein ERP** zu beachten?
Was ist aus Fertigungs-, Stücklisten, Kunden - Sicht zu beachten, wenn **Kieselstein ERP** eingeführt wird:

Trotz aller Vorbereitung wird man als klein und mittelständischer Unternehmer nicht alle Strukturen, die man in Zukunft haben wird abbilden / vorhersehen können.

Das bedeutet: Ja, Gedanken machen, aber nicht den Kopf zerbrechen. Einfach beginnen. Man sieht es im Laufe des Prozesses was passend ist.

## Kontaktdaten / Adressen
Anlegen der Kunden-, Lieferanten Stammdaten. So dass Adressen, Telefonnummern, Ansprechpartner stimmen.

Wenn an Fibu oder Fibuüberleitung gedacht ist und es schon bestehende Debitoren- Kreditorenkonten gibt, so bitte auch diese einpflegen.

## Artikel
Beim Artikelstamm raten wir zu grob strukturieren Artikelnummer, es kann dies auch über Artikelgruppen dargestellt werden. Es hängt dies ganz von persönlichen /firmen-Vorlieben ab.

Wird die Fibu / Fibuexport eingesetzt, so sollten die Artikelgruppen soweit definiert werden, dass dies auch zur Erlöskontenstruktur passt.

Und es sollten die Artikelbezeichnungen, nur die ersten beiden Zeilen, so definiert werden, dass es für Inhaus Verwendung eindeutig ist. Beachte dazu auch den Parameter ARTIKEL_LAENGE_BEZEICHNUNG

Definieren Sie gegebenenfalls auch die Art und Weise, wie die Artikelbezeichnung geschrieben wird.

Was kommt zuerst, welche Infos sind die wichtigsten, werden von links nach rechts geschrieben.

### Warum hat **Kieselstein ERP** keine Eigenschaften Felder?
Solche Felder sind auf den ersten Blick faszinierend. Auf den zweiten zeigt sich, dass diese sehr dynamisch an die unterschiedlichen Artikelarten angepasst werden müssten, was dann im Endeffekt immer wieder zu Unklarheiten bei der Artikelerfassung führen würde. Wir sehen daher die kurze und prägnante Erfassung direkt in den zwei Zeilen der Artikelbezeichnung in Kombination mit der komfortablen Textsuche als zielführender an, was uns unsere Anwender auch immer wieder bestätigen. Solltest du im Laufe der Arbeit mit **Kieselstein ERP** feststellen, dass diese Felder nicht ausreichend sind, so kann ein eigener Reiter mit Artikeleigenschaften mit frei definierbaren, individuellen Feldern, aktiviert werden.

## Stücklisten
Denke daran, dass die Stücklisten Strukturen, Kopfstücklisten, Unterstücklisten, Baugruppen usw. auch die Art und Weise der Fertigung abbilden.

Verwende dazu aber auch die Montageart um Stücklisteninhalte(Positionen) so zu gruppieren, dass z.B. SMD Bestückung oben, SMD Bestückung unten sauber getrennt werden kann und so der Fertigungsprozess entsprechend gesteuert werden kann.

## Weitere Definitionen
### In den Grunddaten
Die Kopf und Fußzeilen aller Module von der Anfrage bis zu Gutschrift eintragen

### Eigentumsvorbehalt und Lieferbedingungen
im System Medien definieren

### Im Angebot
die Erledigungsgründe hinterlegen

### In der Projektverwaltung
ev. die Stati, welchen dann (System, Sprache, Status) auch Icons hinterlegt werden sollen.