---
title: "Änderungen"
linkTitle: "Protokoll"
tags: ["Änderungsprotokoll"]
weight: 90
date: 2023-01-31
description: >
 Protokollierung der Änderungen am **Kieselstein ERP**
---
Da dein **Kieselstein ERP** ein Open Source Projekt ist, welches unter GitLab gehostet ist, können dort alle Änderungen eingesehen werden. Natürlich sieht man hier auch die weiteren Wünsche (Issues) welche von den verschiedensten Menschen an das Entwickler-Team herangetragen wurden. Siehe daher:
- [Kieselstein ERP](https://gitlab.com/groups/kieselstein-erp/sources/-/issues)
- [Terminal zu Kieselstein ERP](https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/issues)


Laufende stichwortartige Dokumentation der durchgeführten Änderungen / Korrekturen

| Datum | Version | Kategorie | Beschreibung |
| --- | --- | --- | --- |
| 2023-06-28 | 0.0.10 | Stückliste | Arbeitsplan kann nicht gedruckt werden, Datenanlieferung für Maschinenkosten richtiggestellt |
| 2023-06-23 | 0.0.10 | Anfrage / Bestellung | neue Bestellung aus Anfrage Mapping richtiggestellt |
| 2023-09-06 | 0.0.10 | Fibu, UVA, Deutschland | Es wurde vor einiger Zeit das Elster Formular durch das DE-Finanzamt umgestellt. Mit diesem [Script](./Update_UVA_Elster.sql) wird diese Änderung in Kieselstein nachgezogen. Es muss dafür auch das Formular finanz/finanz_uva1.* auf aktuellem Stand sein. |
