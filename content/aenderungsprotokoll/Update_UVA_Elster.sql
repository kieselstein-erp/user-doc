-- Aktualisierung des DE-Finanzamtes, da sich inzwischen das Elster-Formular geändert hat
-- 2023-09-06wh
-- es muss dafür das aktuelle (2023-09-06) finanz_uva1.* vorhanden sein

select partner_i_id from lp_finanzamt; -- Partner_i_id des Finanzamtes, ACHTUNG: wenn mehrere Mandanten / Finanzämter

select * from fb_uvaformular where finanzamt_i_id=15
order by i_gruppe, i_sort;

-- Gruppe 3 wird nun 3b
-- Gruppe 4 wird 3a
-- Gruppe 5 wird 4
-- Gruppe 6 bleibt
-- Gruppe 7 wird 5
-- Gruppe 8 wird 7 und muss um Kennzahl 67 ergänzt werden
update fb_uvaformular set i_sort = i_sort+20 where i_gruppe=3;
update fb_uvaformular set i_gruppe = 3 where i_gruppe=4;
update fb_uvaformular set i_gruppe = 4 where i_gruppe=5;
update fb_uvaformular set i_gruppe = 5 where i_gruppe=7;
update fb_uvaformular set i_gruppe = 7 where i_gruppe=8;
update fb_uvaformular set c_kennzeichen='46(47)->67' where i_gruppe=5 and i_sort=2;

select count(i_id) from fb_uvaformular where i_gruppe=8 and mandant_c_nr='001'; -- muss 0 sein
