---
title: "FAQ"
linkTitle: "FAQ"
tags: ["FAQ"]
weight: 80
date: 2023-01-31
description: >
 FAQ zum Kieselstein ERP
---
Laufende Sammlung von Fragen und dazugehörenden Antworten rund um dein Kieselstein ERP

wird anfänglich nur gesammelt und später z.B. nach Technik, Controlling, .... gruppiert.

## Bedeutung der Forecast Eigenschaften Call off Daily
Call off Daily und Call off Weekly bestimmen das Zeitfenster (Datum) in dem die Termine in den Forecastauftragspositionen nur eine lose Information oder eine echte Reservierung bewirken.\
Das bedeutet auch, dass rein durch das verstreichen der Tage eine Forecastauftragsposition vom Call off Montly in den Call off Weekly wandert und damit ein (verbindlicher) Bedarf entsteht. Somit wirken die eingetragenen Mengen und Termine dann z.B. in der internen Bestellung und auch im Bestellvorschlag.\
Bitte beachten, die offenen Positionen werden durch die Lieferung in Bezug auf die Forecastauftragsposition erfüllt, oder eben nicht erfüllt.

Für zusätzliche Auswertungen in Verbindung mit Forecast, Forecast Auflösung, Wiederbeschaffung usw., wende dich gerne an deinen **Kieselstein ERP** Betreuer:inn.

### Forecast nur zur ungefähren Planung verwenden
Wird der Forecast nur zur ungefähren Planung verwendet, müssen Call-Off Tag und Woche auf -999 (Minus) eingestellt werden und der Forecastauftrag auf (plus) 999.

![](forcast_nur_zur_schaetzung.png)  
Damit wirken die eingetragenen Artikel **nicht** in den Reservierungen / Bedarfen. Es stehen trotzdem die Auswertungen dafür zur Verfügung.

## Was ist zu tun, damit der Lagerstand stimmt?
Diese Frage wird uns oft gestellt. Wenn man dann ein klein wenig nachhakt, kommt sehr schnell heraus, dass es 1-2 Personen gibt, denen der Lagerstand wichtig ist und allen anderen, vor allem die die nicht direkt davon betroffen sind, ist es völlig egal. Leider finden wir das oft auch, dass die Geschäftsleitung, welche das schon viele Jahre macht, die Notwendigkeit dafür nicht einsieht. Wir haben das doch die letzten 30Jahre auch ohne hinbekommen.
Parallel dazu muss man aber mit der Zeit gehen und braucht dafür natürlich einen entsprechenden Webshop. Nun müssen aber die Lagerstände stimmen.

Oft kommt dann, wir brauchen eine App (was eigentlich nur ein Programm meint) mit dem wird dann alles besser.
Unsere Erfahrung: Ein schlechter Prozess wird durch eine App eher noch schlechter. Es muss zuerst, in der Regel mit Boardmitteln (bis hin zu einem Blatt Papier), der Prozess richtig gestellt werden. Erst wenn dieser stimmt und wenn **alle** Beteiligten mitmachen, alle wollen, dass der Lagerstand immer stimmt, kann und sollte man über Prozessvereinfachungen nachdenken.
Siehe dazu auch, Schaltschrank suchen.

## Warum kann auf eine nicht aktivierte Bestellung ein Wareneingang gebucht werden?
Warum kann man einen Wareneingang bei einer Bestellung buchen, ohne die Bestellung zu aktivieren bzw. zu versenden?

Der Hintergrund ist, dass man, gerade bei Bestellungen in Shops, quasi während des Wareneingangs auch weitere Positionen in die Bestellung (z.B. über den Neu-Button im Wareneingang) reinschreibt. Daher ergibt sich daraus dann die Situation dass die Bestellung einerseits im Status angelegt ist, obwohl bereits darauf ein Wareneingang gebucht ist.
Man kann dies dadurch entschärfen, dass man, nach der Erfassung der zusätzlichen Wareneingangspositionen die Bestellung wieder aktiviert. Hier wird auch gerne die Abkürzung, Rechtsklick auf das Druckersymbol im Reiter Positionen, genutzt.

## beim Erzeugen des Bestellvorschlages kommt ein komischer Fehler
Dies tritt auch beim Erzeugen der Internen Bestellung auf und auch wenn man aus dem Artikel, Journal, Bewegungsvorschau (aller Artikel) aufruft.<br>
In der Fehlermeldung steckt immer eine Zeile mit 
> Unable to find com.lp.server.fertigung.fastlanereader.generated.FLRLossollmaterial with id 15

drinnen, wobei die ID von den umgebenden Daten abhängig ist, hier also nur als Beispiel steht.

Die Ursache ist, dass, warum auch immer, die zusätzliche Reservierungs-, Fehlmengen-Verwaltung nicht mehr mit den eigentlichen Daten übereinstimmt.

Beheben kannst du dies durch die Pflege. Also System, unterer Reiter Pflege<br>
![](System_Pflege_01.png)<br>
und danach in der Reihenfolge
- Prüfe Reservierungen
- Prüfe Fehlmengen
- Prüfe Bestelltliste
damit werden diese Verbindungen wieder richtiggestellt und die Berechnungen funktionieren wieder.

Hinweis: Es kann ein sehr ähnliches Verhalten auch beim Aufruf aus dem einzelnen Artikel z.B. der Reservierungen usw. vorkommen, dann hast du exakt den defekten Artikel damit verwendet. Die Lösung ist die gleiche.