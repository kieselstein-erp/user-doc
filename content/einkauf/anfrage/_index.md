---
categories: ["Anfrage"]
tags: ["Anfrage", "Gruppen Anfrage"]
title: "Anfrage"
linkTitle: "Anfrage"
date: 2023-01-31
weight: 100
description: >
  Verwaltung der Anfragen an Lieferanten inkl. Gruppenanfragen und Anfragevorschlag
---

## Statusübergänge
![](statusuebergang_anfrage.png)

Anlegen einer Anfrage

Um eine neue Anfrage anzulegen gehen Sie bitte wie folgt vor:
Klicken Sie auf das Modul Anfrage ![](anfrage.JPG) in der nun offenen Auswahl klicken Sie auf Neu ![](neu_button.gif) oder auf ![](anfrage_kopieren.JPG) Anfrage aus Anfrage erstellen, wenn Sie die Inhalte einer bestehenden Anfrage übernehmen wollen.
Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den Lieferanten, das Projekt und den Anliefertermin.
Mit einem Klick auf Speichern ![](speichern.JPG)  bestätigen Sie die Eingaben.
Wechseln Sie in den Reiter Positionen und geben hier die Positionen, die Sie anfragen möchten ein.
Um eine neue Position hinzuzufügen klicken Sie auf Neu ![](neu_button.gif) wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten](../Allgemein/index.htm#Positionsarten)), geben die Informationen (u.a. Menge) ein und klicken auf Speichern![](speichern.JPG).
Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.
Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Lieferkonditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für diese eine Anfrage ab. 

Mit Klick auf Drucken ![](drucken.JPG) (oder oberes Menü Anfrage - Drucken...) erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG), dem Versenden per E-Mail ![](e-mail.jpg)oder speichern ![](speichern.JPG), aktivieren Sie die Anfrage, der Status (siehe oben) ändert sich von angelegt in offen. 

Sobald Sie die Antwort des Lieferanten erhalten, wählen Sie die Anfrage aus, wechseln in den Reiter Lieferkonditionen und erfassen hier das Angebot des Lieferanten. 
Danach können Sie im Reiter Lieferdaten die Inhalten zu den einzelnen Positionen der Anfrage eintragen.<br>
Wenn Sie die Inhalte der Anfrage in eine Bestellung übernehmen, so werden diese Informationen übertragen.<br>
Weiters finden Sie das Icon  ![](datenrueckpflegen.JPG) Daten in den Artikellieferant zurückpflegen. Um die Informationen der Anfrage zurückzupflegen, wählen Sie die gewünschten Zeilen aus und klicken auf das Icon.<br>
Das zweite Icon ![](datenrueckpflegen_bevorzugt.JPG) Daten in den Artikellieferant zurückpflegen als bevorzugter Lieferant bewirkt, dass die Inhalte rückgepflegt werden und der Lieferant als Bevorzugter eingetragen wird.

Wenn nun aus der Bestellung eine Anfrage erzeugt wird, wird der Erledigungsgrund 'Bestellt' in der Anfrage hinterlegt. Wenn Sie die Anfrage manuell erledigen, so geben Sie bitte einen Erledigungsgrund an.

#### Wozu benötige ich das Projekt?
Das Projekt dient der Kennzeichnung der unterschiedlichen Projekte Ihrer Anfragen. Geben Sie hier entweder explizite Kundenprojekte ein oder z.B. Jahresbedarf 2009 oder ähnliches.
Werden hier die Kundenprojektnamen eingegeben, so achten Sie bitte darauf, dass diese nicht angedruckt werden sollen, bzw. nur wenn dies so vereinbart ist.<br>
Die Angabe eines Projektnamens erleichtert die Auswertung der Lieferantenangebote ganz deutlich.

#### Die ausgewählte Kostenstellen wird wieder gelöscht?
Wenn Sie, z.B. in einer Anfrage, eine Kostenstelle ausgewählt haben und danach wird der Lieferant geändert, so wird die Kostenstelle wieder auf die Kostenstelle des Lieferanten gesetzt. Ist beim Lieferanten keine Kostenstelle definiert, so wir die Kostenstelle auf nicht gesetzt gestellt (gelöscht).

#### Erfassung der Lieferkonditionen und Erfassung der Lieferdaten?
Bitte beachten Sie, dass die Erfassung der Lieferkonditionen vor der Erfassung der eigentlichen Lieferdaten erfolgen muss.<br>
Wenn Sie Lieferdaten erfassen wollen, so geben Sie zuerst in den Lieferkonditionen die Angebotsnummer ein.<br>
Wenn Sie nun wieder Kopfdaten/Positionen/Konditionen ändern wollen, so entfernen Sie die Angebotsnummer aus den Lieferkonditionen. Bitte beachten Sie, dass hierzu keine Daten im Reiter Lieferdaten enthalten sein dürfen - löschen Sie etwaige Inhalte vor dem Löschen der Angebotsnummer.<br>
Der Status der Anfrage ändert sich auf erledigt, sobald Sie eine Bestellung erstellen, bzw. die Anfrage manuell erledigen (geben Sie hierzu bitte einen Erledigungsgrund an).

#### Warum werden die Anliefertermine nur in Wochen angegeben?
Weil eine Anfrage eine grobe Information darstellt und natürlich auch vom Bestelldatum abhängt.

#### Wie erhalte ich schnell einen Überblick über die Warenbewegungen und den Bedarf der Artikel zu denen ich eine Anfrage erstelle?
Markieren Sie eine Zeile in den Anfragepositionen, mit Klick auf das Icon Monatsstatistik ![](Monatsstatistik_anzeigen.JPG) wird die Statistik über Warenzu- und abgänge dieses Artikels aufgerufen und angezeigt.

#### Wo sehe ich eine Zusammenfassung der angebotenen Preise?
Diese sehen Sie unter Journal, Lieferdaten. Es stehen hier folgende Auswertemöglichkeiten zur Verfügung:

![](Anfragejournal.gif)

| Von - Bis | Betrachtungszeitraum der versandten Anfragen |
| --- |  --- |
| Artikelnummer von - bis | eine eventuelle Einschränkung auf gewünschte Artikel |
| Sortierung nach Projekt | Sortiert die Lieferdaten nach Projekt und dann nach Artikelnummer, sodass je Projekt übersichtlich alle angebotenen Artikel / Preise gemeinsam dargestellt werden und daraus der entsprechende Anbieter ermittelt werden kann |
| nur Projekt xy | Es werden nur die Anfragen für ein bestimmtes Projekt ausgewertet. Auch hier sind die Artikel nach Artikelnummer sortiert. Für diese Auswertung ist wichtig, dass die Projektbezeichnung der Anfragen gleichlautend sind. |
| Anfragen ohne gelieferte Mengen anführen | Es werden hier auch angefragte Positionen angeführt, die noch nicht angeboten wurden. So erhalten Sie einen Überblick, welche Artikel insgesamt noch fehlen. |

![](Anfragejournal2.gif)

Man sieht in dieser Übersicht auch deutlich die günstigste Einkaufsform. D.h. gerade in der Elektronik kann es immer wieder vorkommen, dass eine größere Stückzahl, einen geringeren Beschaffungswert ausmacht, als die gewünschte / benötigte Stückzahl.

#### Die in den Lieferdaten erfassten Lieferanten-Angebote werden nicht in den Artikelstamm übernommen?
Die erfassten Lieferdaten der Anfragen finden Sie NICHT unter Artikellieferant, sondern im Modul Artikel unter Info, Anfragestatistik.

Warum ist das so?

In Artikellieferant werden die Lieferanten hinterlegt bei denen Sie tatsächlich einkaufen.
In den Anfragen werden ja üblicherweise an deutlich mehr Lieferanten gerichtet, als bei denen dann wirklich eingekauft wird. Daher eben von den Lieferdaten keine automatische Übernahme in den Artikellieferanten aber:<br>
Wird eine Anfrage in eine Bestellung eingelesen und dort dann die Mengen bestätigt, so kann der Artikellieferant direkt übernommen werden. Bitte beachten Sie dazu unbedingt die Einstellung der Preisübernahme in der [Bestellposition]( {{<relref "/einkauf/bestellung/#kann-der-bestellpreis-automatisch-in-den-artikel-zur%c3%bcckgepflegt-werden" >}} )

#### Zurückpflegen der in den Lieferdaten erfassten Lieferanten-Angebote
<a name="Rückpflegen"></a>
Durch Klick auf den Button ![](Anfrage_Artikeluebernahme.gif) Daten in Artikellieferant zurückpflegen, werden die Daten dieses Artikels in den Artikelstamm, Artikellieferant zurückgepflegt.<br>
Bitte unterscheiden Sie zwischen zurückpflegen ![](Anfrage_Artikeluebernahme.gif) und als erster = bevorzugter Lieferant ![](Anfrage_Artikeluebernahme_bevorzugter_Lieferant.gif) zurückpflegen.

Sind für diesen Lieferanten bereits Staffelmengen eingetragen, so erscheint folgende Abfrage:
![](Anfrage_Artikel_Staffelmengen_uebernahme.jpg)<br>
Mit Ja werden die bestehenden Staffelpreise des Lieferanten gelöscht.<br>
Mit Nein werden diese belassen und eventuelle zusätzliche Staffelmengen aufgrund dieser Anfrage hinzugefügt.<br>
Mit Abbrechen werden keine Änderungen vorgenommen.

Durch die Übernahme wird auch eine Verlinkung vom Artikellieferanten zur Anfrageposition hergestellt.<br>
D.h. im Artikellieferanten finden durch diese Zuordnung Sie ![](Goto_Anfrage.gif) die Anfrage Nummer mit einem GoTo zur Anfrage. Womit wiederum vom Artikellieferanten aus direkt auf das damals erfasst Angebot Ihres Lieferanten, das Sie in der Anfrage erfasst haben gesprungen werden kann.

## Übernahme von Staffelmengen:
Sind in der Anfrage verschiedene Mengen des gleichen Artikels eingetragen, so wird die niedrigste Menge in den Artikellieferanten übernommen. Alle weiteren Mengen werden in die EK-Staffelpreise des Artikellieferanten übernommen.

Es werden dabei folgende Daten von der Anfrage in den Artikellieferanten übernommen:
![](Anfrage_Erfassung.jpg)
| Feld | Bedeutung |
| --- | --- |
| Standardmenge | |
| Mind. Bestellmenge | |
| Verpackungseinheit | |
| Wiederbeschaffungszeit | Tage oder Wochen, siehe dazu Mandantenparameter ARTIKELWIEDERBESCHAFFUNGSZEIT |
| Anliefermenge | wird nur in die EK-Staffelmengen übernommen |
| Anlieferpreis | |
| Zertifikatsart | Muss der Lieferant für diesen Artikel zertifiziert sein, z.B. Luffahrt, Medizintechnik |
| <u>Aus den Lieferkonditionen:</u><br>Angebotsnummer | |
| Angebotsdatum | Gültig ab. Ist das Angebotsdatum nicht gesetzt, so wird das Anfragedatum als Gültig ab in den Artikellieferanten übernommen. |
| Angebot gültig bis | Gültig bis |

**Hinweis:**
Sollte in Ihrer **Kieselstein ERP** Installation der Parameter Parameter DIMENSIONEN_BESTELLEN aktiviert sein, so wirkt trotzdem die Preis-Rückpflege nur auf den gewählten (Zuschnitts-)Artikel.

## Zertifikatsart:
In manchen Branchen ist es bereits in der Angebotsphase erforderlich, dem Kunden mitzuteilen, in welcher Form er das Zertifikat seiner Ware erhält. Definieren Sie hier, welches Zertifikat Sie von Ihrem Lieferanten erhalten. Diese Info wird an den Artikellieferanten weitergegeben.<br>
Beim Ausdruck des Angebotes wird die Zertifikatsart des bevorzugten Lieferanten mit angedruckt.<br>
Die Zertifikatsarten können im Modul Anfrage in den Grunddaten definiert werden.

#### Wie können Lieferdaten rasch erfasst werden?
Nutzen Sie dafür bitte im Reiter Lieferdaten ![](Anfrage_Lieferdaten_Schnellerfassung.gif) die Schnellerfassung.<br>
Damit gelangen Sie in eine eigene Erfassungsmaske mit der die Wiederbeschaffungszeit (WBZ), die Anliefermenge und der Anlieferpreis direkt eingetippt werden können<br>
![](Anfrage_Lieferdaten_Schnellerfassung2.jpg)<br>
und so sehr rasch eine große Anzahl entsprechender Daten erfasst werden kann. Wenn noch keine Anliefermenge erfasst wurde, kommt der Vorschlag aus der Anfrageposition.

Ergänzend zu den üblichen Feldwechsel mit der Tab-Taste kann hier auch die Enter-Taste zum Wechsel in das nächste Feld genutzt werden. Geänderte Werte werden in Blau dargestellt. Beim Schließen des Dialogs können die Änderungen gespeichert oder verworfen werden.<br>
Mit der Taste + wird die aktuelle Zeile gespeichert und in die gleiche Spalte der nächsten Zeile gewechselt.

Über den Button "WBZ" kann die WBZ (Wiederbeschaffungszeit) für alle Zeilen vorbelegt werden.

#### Können für die angebotenen Positionen die Preis gültig ab übernommen werden?
In den Lieferkonditionen der Anfrage gibt es das Feld "Preis gültig ab". Wenn dieses besetzt ist, wird dieses Datum als Vorschlagswert in die einzelne Position der Lieferdaten übernommen. Ist dieses nicht gesetzt, wird heute als "Preis gültig ab" in die Artikellieferantendaten übernommen. Dies wird auch in der Lieferdaten-Schnellerfassung berücksichtigt.

#### Wozu dient der Anfrage Vorschlag?
Die grundlegende Idee des Anfragevorschlages ist, dass Sie, bevor Produkte / Artikel / Teile bestellt werden, diese auch bei verschiedenen Lieferanten anfragen wollen, also sich von diesen Lieferanten ein Angebot einholen.<br>
Die grundsätzliche Funktionsweise und Bedienung des Anfragevorschlages ist identisch zum [Bestellvorschlag]( {{<relref "/einkauf/bestellung/vorschlag">}} ).<br>
Zusätzlich gibt es folgende zwei Funktionen:
- Überleitung in Gruppenanfragen ![](Anfragevorschlag_in_Gruppenanfragen_ueberleiten.gif) mit Angabe eines gemeinsamen Projektes. Bitte beachten Sie, dass für die Überleitung auch der Artikel die jeweilige Liefergruppe hinterlegt haben muss.
- Import des Anfragevorschlages anhand der [SolidWorks Schnittstelle]( {{<relref "/warenwirtschaft/stueckliste/importe/solid_works">}} ).<br>
Achten Sie bitte beim Import des Anfragevorschlages aus der SolidWorks Schnittstelle darauf, dass hier NUR die Positionen einer Stücklistenstruktur importiert werden. D.h. wird hier nur eine flache Liste ohne Kopfdaten der Stückliste angegeben, so erfolgt kein Import, da ja "nur" die Stücklistenpositionen mit ihren Unterpositionen übernommen werden
- Ebenso können Sie einen Anfragevorschlag für ein ausgewähltes Angebot erstellen. Hierzu verwenden Sie das Icon ![](anfragevorschlag_angebot.JPG) Anfragevorschlag aus Angebot erstellen und wählen anschließend durch Klick auf Angebot das gewünschte Angebot aus der Liste der Angebote aus. Nun wird ein Anfragevorschlag erstellt, der sich auf dieses Angebot bezieht.
- Beachten Sie bitte auch, dass im Gegensatz zum Bestellvorschlag im Anfragevorschlag KEINE Preispflege im Artikellieferanten erfolgt.

Zusätzlich werden im Anfragevorschlag auch eine Kompaktinformation über die bisherigen Anfragen angezeigt.<br>
D.h. es werden, in der Regel am Anfang, zwei Spalten angezeigt:
- angefragt: Die hier angezeigte Zahl signalisiert wie oft dieser Artikel in letzter Zeit angefragt wurde
- angeboten: Hier wir angezeigt, wieviele Angebotspositionen in letzter Zeit erfasst wurden<br>
In letzter Zeit: Das ist der Zeitraum von heute abzüglich dem Wert im Parameter BEREITS_ANGEFRAGT, welcher üblicherweise auf 90Tage gestellt ist.<br>
D.h. Anfragen die älter als diese 90Tage sind, werden nicht berücksichtigt oder umgekehrt, Sie erkennen damit Artikel die Sie kürzlich angefragt haben und können so die Erstellung neuer Anfragen entsprechend steuern.

#### Kann der Anfragevorschlag gemeinsam genutzt werden?
Ja. Dies ist mit dem [persönlichen Bestellvorschlag]( {{<relref "/einkauf/bestellung/vorschlag/#bestellvorschlag-gemeinsam-nutzen" >}} ) gekoppelt. D.h. aktivieren Sie bitte den persönlichen Bestellvorschlag, so steht auch der Anfragevorschlag in der gleichen Logik zur Verfügung.

#### Kann man eine Handeingabe in einen Artikel umwandeln?
Ja, mit Hilfe des Menüpunkts Bearbeiten - Handartikel in Artikel umwandeln![](handartikel_in_artikel_umwandeln.png). Um zum Beispiel nach Erteilung des Auftrags an Ihren Lieferanten Handeingaben in Artikel umzuwandeln, verwenden Sie diesen Menüpunkt. Nach Klick auf den Menüpunkt, erfolgt eine Abfrage, welche Artikelnummer Sie für den Artikel verwenden wollen, hier können Sie auch einfach eine neue Artikelnummer generieren.

![](gewuenschte_artikelnummer.JPG)

Mit Klick auf OK bestätigen Sie die Artikelnummer, ein neuer Artikel wird angelegt und in der Anfrage hinterlegt. Bitte achten Sie darauf, dass die Umwandlung nur im Belegstatus "Offen" möglich ist.

#### Wie erfasst man Preise, obwohl der Lieferant eine andere Einheit verwendet?
Verwenden Sie dazu die Eingabe der Bestellmengeneinheit im Artikel. In den Anfragepositionen und in den Lieferdaten kann der Preis in Bezug auf die Bestellmengeneinheit eingegeben werden, wenn der Artikel eine Bestellmengeneinheit und einen Umrechnungsfaktor hinterlegt hat.

#### Warum wird als Positionsmenge eine bestimmte Menge vorgeschlagen?
Bei der Artikelauswahl wird als Positionsmenge aus den Lieferdaten des Lieferanten die Standardmenge, ist diese nicht vorhanden, die Mindestbestellmenge vorgeschlagen.

#### Wie kann man eine Anfrage ent-erledigen?
Wurde eine Anfrage bereits erledigt, so kann man diese, z.B. um doch noch Daten einzupflegen,  über den Menüpunkt Bearbeiten, Manuell erledigen wieder auf offen bzw. Erfasst zurückstellen.

#### Kann der Anfragende definiert werden?
Ja. Es kann in den Kopfdaten der Anfrage mit dem Button "Anfrager" dieser definiert werden. Es wird dieser bei einer neuen Anfrage mit dem angemeldetem Benutzer vorbesetzt.<br>
Wenn Anfragen aus einer Liefergruppenanfrage erzeugt wird, wird der Anfrager aus der Liefergruppenanfrage übernommen.<br>
Wenn Anfragern aus einem Anfragevorschlag erzeugt werden, wir der Anfrage mit dem angemeldeten Benutzer vorbesetzt.<br>
Wenn eine Anfrage kopiert oder eine Bestellung aus einer Anfrage erstellt wird, wird der Anfrager durch den angemeldeten Benutzer ersetzt.

## Liefergruppen

### Wozu dient eine Liefergruppenanfrage?
Dient dazu eine Anfrage an alle Lieferanten dieser Liefergruppe zu erstellen. Es wird damit automatisch eine entsprechende Anzahl an Anfragen generiert, welche dann wiederum eigenständig antworten und Angebote liefern, die gut miteinander verglichen werden können.

Man wählt zuerst in den Kopfdaten Liefergruppenanfrage anlegen und trägt die richtige Liefergruppe ein ([Liefergruppe anlegen]( {{<relref "/docs/stammdaten/lieferanten/#wie-legt-man-eine-liefergruppe-an" >}} )

**<u>Hinweis:</u>**<br>
In der Liefergruppenauswahl im Anfragekopf werden nur diejenigen Liefergruppen angezeigt, die auch Zuordnungen zu Lieferanten haben. Sollten Sie daher keine Liefergruppe bzw. die gewünschte Liefergruppe nicht in der Anfrage vorfinden, so ist für die jeweilige Liefergruppe noch kein einziger Lieferant zugeordnet.

Bitte beachten Sie, dass für die Überleitung ([Anfragevorschlag](#Anfragevorschlag)) auch der Artikel die jeweilige Liefergruppe hinterlegt haben muss. Sie können diese im Artikel Reiter Detail zuordnen. 

#### Wo werden die Liefergruppen definiert?
Die Liefergruppen werden im [Lieferanten]( {{<relref "/docs/stammdaten/lieferanten/#wie-legt-man-eine-liefergruppe-an" >}} ) definiert.

#### Wie wird bei Liefergruppenanfragen vorgegangen?
Definieren Sie zuerst eine Liefergruppenanfrage durch Klick auf ![](Liefergruppenanfrage1.gif). In dieser müssen alle Positionen enthalten sind, welche Sie bei der gewünschten Liefergruppe anfragen wollen.

Zur Kontrolle drucken Sie diese Liefergruppenanfrage aus.

Danach wechseln Sie wieder nach Auswahl und wählen ![](Liefergruppenanfrage2.gif) Anfragen aus Liefergruppenanfrage erstellen.

Nun werden entsprechend den bei den Lieferanten hinterlegten Liefergruppen Anfragen für diese Lieferanten erstellt und die ursprüngliche Liefergruppenanfrage auf erledigt gesetzt. Die erzeugten Anfragen an die einzelnen Lieferanten können wiederum wie gewohnt weiterbearbeitet werden.

#### Alle angelegten Drucken
Im Menüpunkt Anfrage gibt es auch die Möglichkeit alle Anfragen oder nur die Anfragen einer Liefergruppe die im Status angelegt sind auf einmal zu drucken bzw. per EMail oder Fax zu versenden.
![](Liefergruppenanfrage_versenden.jpg)

In der Regel wird Angelegte Liefergruppen wandeln und dann alle drucken ausgewählt. Das bedeutet, dass zuerst alle angelegten Liefergruppenanfragen in einzelne Anfragen für die jeweiligen Liefergruppe umgewandelt werden. Damit werden die umgewandelten Liefergruppenanfragen erledigt.<br>
Zugleich wurden dadurch Anfragen (mit Liefergruppen Bezug) erzeugt. Diese sind ebenfalls im Status angelegt.<br>
Nun werden alle Anfragen, die im Status angelegt sind, ausgedruckt.

Wird "Liefergruppe wählen und wandeln und nur die daraus angelegten Anfragen drucken" gewählt, so werden nur die angelegten Liefergruppenanfragen der gewählten Liefergruppe aufgelöst und dann wiederum die daraus erzeugten angelegten Anfragen gedruckt.<br>
Das bedeutet auch, wenn es keine umzuwandelnden Liefergruppenanfragen der gewählten Liefergruppe gibt, z.B. weil bereits manuell die Liefergruppenanfrage aufgelöst wurde, werden auch keine Anfragen versandt. In diesem Falle sollte dann Angelegte Liefergruppen wandeln und dann alle drucken verwendet werden.

#### Vergleich der Angebote einer Liefergruppenanfrage
Wenn Sie eine Liefergruppenanfrage an Ihre Lieferanten versandt haben, so erhalten Sie hoffentlich von Ihren Lieferanten auch die gewünschten Positionen angeboten. Um diese nun zu vergleichen, steht unter Info Bieterübersicht ein entsprechender Vergleich der Anfragen einer Gruppenanfrage zur Verfügung.<br>
Wählen Sie dafür eine Anfrage die entweder eine Gruppenanfrage ist, oder aus einer Gruppenanfrage erzeugt wurde.<br>
Nun wählen Sie Info, Bieterübersicht.<br>
Sie bekommen damit eine Zusammenfassung aller angebotenen Positionen und wer wo der günstigste Lieferant ist.

#### Kann man auf Liefergruppen filtern?
In der Auswahlliste steht eine Combobox mit der Liste der definierten Liefergruppen als zusätzliche Filtermöglichkeit zur Verfügung.
![](Liefergruppen_Filter.gif)
Entfernen Sie gegebenenfalls den Haken bei nur Offene um eine Übersicht über alle bisherigen Anfragen zu bekommen.