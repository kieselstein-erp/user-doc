---
categories: ["Eingangsrechnung"]
tags: ["Zusatzkosten"]
title: "Zusatzkosten"
linkTitle: "Zusatzkosten"
date: 2023-01-31
weight: 200
description: >
  wiederholende Eingangsrechnungen
---
Zusatzkosten
============

Die Zusatzfunktion Eingangsrechnungs-Zusatzkosten, als unterer Modulreiter im Modul Eingangsrechnung, wurde speziell für die Verwaltung von Sonderkosten geschaffen, welche von der Idee her ähnlich den Eingangsrechnungen zu verwalten sind, aber in den normalen Eingangsrechnungen nicht erfasst werden dürfen. Beispiele dafür sind: Gehälter, Mieten und ähnliches.

Diese Zusatzkosten haben üblicherweise zwei zusätzliche Forderungen:
-   Sie sind wiederholend, in der Regel monatlich.
-   Es dürfen darauf nur ausgewählte **Kieselstein ERP** Benutzer einen Zugriff haben.

Beide Punkte werden mit den Zusatzkosten erfüllt.

D.h. um auf das Modul Zusatzkosten zugreifen zu können, müssen Sie das Recht FB_CHEFBUCHHALTER besitzen.

Sie finden die Zusatzkosten im Modul Eingangsrechnung, unterer Modulreiter Zusatzkosten ![](Zusatzkosten.gif).

Hier kann zusätzlich ein Wiederholungsintervall definiert werden.

![](Zusatzkosten_Wiederholintervall.gif)

Damit wird gesteuert, dass halb-automatisch Kopien von ER-Zusatzkosten ausgehend vom Belegdatum angelegt werden.

Im Menüpunkt Bearbeiten, Wiederholende Zusatzkosten anlegen

![](Zusatzkosten_Wiederholende_Anlegen.gif)

werden Kopien von wiederholenden Zusatzkosten angelegt.

Dabei lautet die Regel, dass ausgehend von einem Zusatzkostenbeleg, der als Wiederholende definiert wird, immer nur ein weiterer Beleg angelegt wird. Ausgangsbasis ist hier das Belegdatum des Ausgangsbeleges. Zu diesem wird das Wiederholungsintervall hinzugerechnet und damit das Belegdatum des neuen Zusatzkostenbeleges bestimmt.

Durch die Logik, dass bei einem Lauf pro Beleg immer nur ein Nachfolgebeleg angelegt wird, wird die Problematik der bei Terminänderungen bzw. Intervalländerungen oft auftretenden Mehrfach-Belege ausgehend von einem Ursprungsbeleg vermieden.

Von einem Wiederholenden-Zusatzkosten-Beleg wird nur dann ein Nachfolger angelegt, solange dieser Beleg nicht Erledigt ist.

D.h. wird ein wiederholender Zusatzkostenbeleg auf Erledigt gesetzt, so werden keine weiteren Nachfolgebelege mehr erstellt.

Wurde von einem ER-Zusatzkostenbeleg bereits ein Nachfolger angelegt, so wird dies direkt unter dem Wiederholungsintervall angezeigt.

![](Nachfolge-ER-Zusatzkosten.gif)

Für den Druck eventueller Überweisungsscheine stehen die gleichen Funktionen wie für die Eingangsrechnung zur Verfügung.

Die Zusatzkosten wirken:
- in der integrierten Finanzbuchhaltung
- im Zahlungsvorschlag
- im Liquiditätsplan

Sie finden unter Journal eine Umsatzübersicht in der nur die Zusatzkosten ausgewiesen werden. In der Umsatzübersicht der Eingangsrechnung werden die Zusatzkosten nicht angeführt.

#### Erledigen von Zusatzkosten
Haben Sie z.B. eine Leasingrate für eine Maschine in Ihren Zusatzkosten mit angelegt, so wird diese Leasingrate auch ein Ende haben. D.h. wenn Sie die letzte Leasingrate erfasst haben, so klicken Sie bitte in den Kopfdaten der Zusatzkosten auf den grünen Haken und signalisieren damit, dass die Wiederholung der Zusatzkosten damit gestoppt wird.
Falls Sie die Erledigung wieder aufheben möchten, klicken Sie bitte einfach erneut auf den Haken.
Bitte beachten Sie, dass die Erledigung der Wiederholung nichts mit der Bezahlung zu tun hat.

#### Bezahlen von Zusatzkosten
Ist ein Zusatzkosteneintrag angelegt, so bedeutet dies, ähnlich der Eingangsrechnung, dass diese Verbindlichkeit, z.B. Leasingrate zum Erfassungstermin mit Zahlungsziel besteht. Nun muss natürlich diese Verbindlichkeit auch durch eine Zahlung ausgeglichen werden. Üblicherweise wird die Leasinggesellschaft natürlich diese Beträge automatisch abbuchen. D.h. Sie erledigen im Sinne von begleichen / bezahlen diese Verbindlichkeit (= Zusatzkosten) dadurch, dass Sie die Bankzahlung auf die jeweilige Zusatzkosten eintragen. Damit wird diese in den Status erledigt = bezahlt gesetzt. Trotzdem greift für die Anlage der Wiederholung / des Nachfolgers die Definition des Belegdatums und des Wiederholungsintervalles.

#### Zu welchen Datum wird eine neue Zusatzkosten(beleg) angelegt?
Ausgehend vom Belegdatum des letzten Beleges in der Kette wird das Wiederholungsintervall zum Belegdatum dazugezählt und mit diesem Datum der neue Zusatzkostenbeleg angelegt.
Die Kette? Das ist ausgehend vom ersten Beleg für z.B. Ihre Leasingrate, der manuell mit einem Wiederholungsintervall von z.B. monatlich angelegt wird, dann der jeweils je Monat nachfolgende Beleg. So bekommen Sie bei einer Laufzeit von 24Monaten auch 24Zusatzkostenbelege. Und NUR der letzte, der mit dem Restkaufwert, wird bei der Wiederholung auf erledigt gesetzt.