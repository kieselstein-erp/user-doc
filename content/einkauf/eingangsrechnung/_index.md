---
categories: ["Eingangsrechnung"]
tags: ["Eingangsrechnung"]
title: "Eingangsrechnung"
linkTitle: "Eingangsrechnung"
date: 2023-01-31
weight: 300
description: >
  Verwaltung der Eingangsrechnungen, mit Splittbuchung, Ausgangsgutschrift, Zahlungen
---
Das Modul Eingangsrechnung dient der Erfassung aller Lieferantenrechnungen.

Viele Anwender erklären mir, das macht doch sowieso mein Steuerberater. Das mag stimmen, nur dein Steuerberater ist in der Regel ein Monat hinten (er hat ja auch andere Klienten) wenn du aber eine aktuelle, gültige Situation deines Unternehmens jederzeit (in der Regel wöchentlich) sehen willst, z.B. auch in der Liquiditätsvorschau, ist auch die Erfassung der Eingangsrechnungen unbedingt erforderlich. In Verbindung mit den Fibu-Schnittstellen sparst du damit entsprechende Kosten und kannst dein Unternehmen, gerade wenn es mal etwas schwieriger wird, wesentlich präziser steuern.

## Statusübergänge
![](statusuebergaenge.png)

#### Können neben den üblichen Eingangsrechnungen auch Mieten und Gehälter verwaltet werden?
Ja, wir haben dafür das Modul [Zusatzkosten]( {{<relref "/einkauf/eingangsrechnung/zusatzkosten" >}} ) geschaffen.

#### Wie erfasse ich Ausgangsgutschriften?
Siehe bitte [Ausgangsgutschriften]( {{<relref "/einkauf/eingangsrechnung/ausgangsgutschriften" >}} ).

#### Kann ich Daten importieren?
[Für 4Vending siehe bitte]( {{<relref "/einkauf/eingangsrechnung/4vending" >}} )<br>
[Für XLS Import siehe](#können-eingangsrechnungen-importiert-werden)

#### Kontierung der Eingangsrechnung?
Um die für die Verbuchung der in der Finanzbuchhaltung steuerlich relevanten Kontierung zu erhalten definieren Sie die Kontierung. Die Konten müssen im Modul Finanzbuchhaltung angelegt werden. Für die steuerrechtlich richtige Kontierung stimmen Sie sich bitte mit Ihrem Steuerberater ab.

#### Warum können manche Konten nicht für die Kontierung verwendet werden?
Für die Kontierung dürfen in der Regel nur Aufwandskonten verwendet werden.
Das bedeutet, dass Erlöskonten und Bankkonten in der Auswahl der Konten für die Kontierung der Eingangsrechnung nicht zur Verfügung stehen.<br>
Die oft geübte Praxis Eingangsrechnungen auf Kreditkarten Konten als Aufwand zuzuordnen ist schlichtweg falsch. Die Zahlung einer Eingangsrechnung mit einer Kreditkarte ist einfach eine andere Bank als üblich. [Siehe]( {{<relref "/management/finanzbuchhaltung/integriert/#kreditkarten" >}} ).<br>
Auch wenn Sie z.B. eine Gutschrift für den Verkauf von Schrott, z.B. Metallspänen, bekommen, so ist das nicht eine negative Eingangsrechnung, sondern ein Erlös. [Siehe]( {{<relref "/verkauf/gemeinsamkeiten/#wie_erfasse_ich_die_schrottrechnung_richtig?" >}} )

Die Definition der in der Eingangsrechnung bebuchbaren Konten erfolgt durch die UVA-Arten. Siehe Module Finanzbuchhaltung, Grunddaten, UVA-Arten. Für die **Kieselstein ERP** Anwender ohne integrierter Finanzbuchhaltung, gilt diese Sicherheitseinstellung nicht.
Die Definition welches Konto eine Bank ist, erfolgt durch die Zuordnung des Kontos zu einer Bank über die [Bankverbindungen]( {{<relref "/management/finanzbuchhaltung/#definition_der_eigenen_Bankverbindungen" >}} ).

#### Splittbuchung, Mehrfachkontierung
Da es immer wieder erforderlich ist, einen Eingangsrechnungsbeleg auf mehrere Konten zu verteilen, steht dafür die Funktion ![](Splittbuchung1.gif) zur Verfügung. D.h. wenn eine Eingangsrechnung auf mehrere Konten bzw. Kostenstellen verteilt werden sollte, muss dies angehakt werden.
Nach dem Abspeichern der Eingangsrechnung mit dem Eintrag Mehrfach- / Splittbuchung wird der Reiter ![](Kontierung.gif) Kontierung freigeschaltet. In der Anzeige der Kopfdaten erscheint solange die Info ![](Splittbuchung2.gif) bis im Reiter Kontierung die Eingangsrechnung vollständig auf die verschiedenen Konten bzw. Kostenstellen aufgeteilt ist.

Unter Kontierung geben Sie nun
![](Kontierung2.gif)
die Kostenstelle, das Sachkonto, den Bruttobetrag und den Mehrwertsteuersatz dieses Teilbetrages der Eingangsrechnung an. Durch Klick auf ![](Kontierung3.gif) Rest wird der noch nicht kontierte Betrag der Eingangsrechnung in diese Kontierung übernommen.
Achten Sie darauf, dass die Eingangsrechnung vollständig kontiert wird.

Bei der Erfassung der Splittbuchung kann auch der Nettobetrag verwendet werden, somit wird der Steueranteil automatisch errechnet.
![](brutto_netto_er.JPG)
Geben Sie dazu im Feld Nettobetrag den gewünschten Betrag ein und wählen den Steuersatz.

#### Wie wird eine Eingangs-Gutschrift richtig eingetragen?
Im Modul Eingangsrechnung kann beim Anlegen einer Eingangsrechnung die Art dieser Eingangsrechnung definiert werden.
![](ER_Art.gif)
Um eine Eingangs-Gutschrift einzutragen, wählen Sie Gutschrift aus.
Bitte beachten Sie, dass die Beträge in der Erfassung positiv eingegeben werden. In der Auswahlliste werden diese mit negativem Vorzeichen dargestellt, da es ja Gutschriften sind.
Der Ausgleich der Eingangs-Gutschriften kann nur durch eine andere Eingangsrechnung erfolgen.
Gehen Sie dazu derzeit bitte so vor, dass bei Erledigung der Eingangsrechnung, welche auch mit dieser Gutschrift zusammenhängt auch die Gutschrift über das Menü, Bearbeiten, Manuell erledigt, genauso wie die eigentliche Eingangsrechnung erledigt wird.
**Wichtig:** Bei der Erfassung einer Eingangsgutschrift, ist egal ob der Rechnungsbetrag positiv oder negativ eingegeben wird, er wird immer als negative Eingangsrechnung interpretiert. Bei einer reinen Eingangsrechnung wird das Vorzeichen so belassen wie es von Ihnen eingegeben wird.
Für die richtige Berücksichtigung der Eingangsgutschrift im Zahlungsvorschlag, sollte diese umgehend gegen die auslösende Eingangsrechnung ausgeglichen / bezahlt werden. [Siehe dazu](#Eingangsgutschrift verbuchen).

#### Kann auch eine negative Eingangsrechnung angelegt werden?
Ja. Dies empfiehlt sich dann, wenn Sie auf die erhaltene Eingangsgutschrift auch eine Zahlung erwarten.<br>
Bitte nutzen Sie die negative Eingangsrechnung immer auch dann, wenn im Zahlungsausgleich der Eingangsgutschrift auch ein Skonto beteiligt ist.<br>
Für eine Beschreibung [siehe]( {{<relref "/verkauf/rechnung/zahlung/#wie_ist_bei_der_skontozahlung_von_eingangsgutschriften_vorzugehen?" >}} ).

#### Eingabe der Lieferantenrechnungsnummer
Die Lieferantenrechnungsnummer wird im Feld Text eingegeben.

Die Eingabe der Lieferantenrechnungsnummer ist abhängig von der angebundenen Finanzbuchhaltung zweigeteilt.

Ist die in **Kieselstein ERP** integrierte Finanzbuchhaltung <u>**nicht**</u> aktiv, so lässt der erste Teil (der Linke) nur die Eingabe von bis zu sechs Ziffern zu. Im zweiten Teil der Lieferantenrechnungsnummer kann ein beschreibender Text eingegeben werden.

Die Aufteilung und Begrenzung in zwei Teile ist deshalb erforderlich, da viele fremde Buchhaltungsprogramme in den Rechnungsnummern nur Ziffern erlauben. Wird die Eingangsrechnung exportiert, so wird nur der erste Teil mit den Ziffern an die fremde Finanzbuchhaltung übergeben. Der zweite Teil dient zu Ihrer Information in **Kieselstein ERP**.

Wird die integrierte Finanzbuchhaltung von **Kieselstein ERP** verwendet, so kann der Text in einem durch eingegeben werden. Dieser wird vollständig in den Finanzbuchhaltungsteil übernommen.

Wenn der Parameter EINGANGSRECHNUNG_LIEFERANTENRECHNUNGSNUMMER_LAENGE auf -1 gestellt wird, so können im Feld der Lieferantenrechnungsnummer auch Buchstaben und nicht nur Zahlen eingegeben werden.

#### Eingabe des Textes
Hier sollten Sie vermerken, was mit der Eingangsrechnung bezahlt wurde. Die Idee dahinter teilt sich in zwei Bereiche:
1. Viele Lieferanten haben nicht rein nummerische Rechnungsnummern. Wie oben beschrieben, ist in gewissen Fällen in **Kieselstein ERP** nur eine nummerische Erfassung der Lieferantenrechnungsnummern wegen der weiterführenden Buchhaltungssysteme möglich. Tragen Sie gegebenenfalls die vollständige Lieferantenrechnungsnummer mit allen Zeichen in dieser Zeile ein.
2. Sie sollten nach 7 bzw. 10Jahren immer noch auf Anhieb wissen, warum Sie die Eingangsrechnung erhalten und auch bezahlt haben. Je eindeutiger und klarer Sie hier die Texte angeben, desto klarer können Sie oder auch Ihre NachfolgerInnen die Fragen einer eventuellen Finanzprüfung beantworten.

#### WE-Artikel wozu?
Unter dem Text finden Sie auch eine Zeile Wareneingangsartikel. Technisch gesehen ist dies eine weitere Zeile zur Beschreibung des Wareneinganges.<br>
In manchen Unternehmen ist alt geübte (aber nicht zu empfehlende) Praxis, dass hier eine Art Wareneingangsbuch OHNE echter Lagerbewirtschaftung mit gepflegt wird.<br>
D.h. Sie finden im Eingangsrechnungsjournal die Möglichkeit diesen Text zusätzlich auszugeben und so eine Art Wareneingang darzustellen.<br>
Unsere Empfehlung: Nutzen Sie eine klare Lagerwirtschaft, also die Wareneingänge anhand der Bestellungen.

#### Ich kann keine Mehrwertsteuersätze auswählen.
Bitte pflegen Sie die Mehrwertsteuersätze im Mandanten ein, um diese dann auch auswählen zu können.

#### Woher kommt der Defaultvorschlag der Mehrwertsteuer?
Der standardmäßig vorgeschlagene Mehrwertsteuersatz ist der beim Lieferanten hinterlegte Mehrwertsteuersatz.<br>
Genauer ist es die Mehrwertsteuerart (Bezeichnung) die beim Lieferanten (und analog beim Kunden) definiert ist.<br>
Der anzuwendende Steuersatz ergibt sich aus dem Datum der Eingangsrechnung.

#### Der von **Kieselstein ERP** errechnete Mehrwertsteuerbetrag stimmt nicht?
<a name="abweichende MwSt"></a>
Von **Kieselstein ERP** wird der Steuerbetrag unter Berücksichtigung des Bankers Rounding, was die aktuellste und anerkannte Rundungsmethode ist, gerundet.<br>
Unter der Voraussetzung, dass der Steuerprozentsatz richtig definiert ist, wird von **Kieselstein ERP** auch der Steuerbetrag richtig berechnet. Bitte beachten Sie dazu auch die Eigenheiten der CentRundungen in den Ländereinstellungen. Alle diesbezüglichen Rückfragen von Anwendern konnten mathematisch begründet werden.<br>
Trotzdem kommt es eben immer wieder vor, dass es zu Differenzen zwischen der auf der Rechnung Ihres Lieferanten ausgewiesenen Mehrwertsteuer und der von **Kieselstein ERP** errechneten Vorsteuer kommt. Wie ist hier vorzugehen:<br>
Es gilt in den deutschsprachigen Ländern der Grundsatz, dass die fakturierte Steuer geschuldet wird. D.h. Sie dürfen sich nicht mehr Vorsteuer abziehen, als der Lieferant an die Finanz abführt.<br>
Sie dürfen sich aber weniger Steuer abziehen, da dies zum Vorteil des Finanzamtes, also des Steuerertrages des jeweiligen Landes ist.<br>
Das bedeutet, dass, bei geringen Differenzen, vor allem zu Gunsten des Finanzamtes, können Sie überlegen ob der Aufwand der nachfolgende Schritte dafür steht, oder ob Sie der Allgemeinheit (dem Finanzamt) die 1-2 Cent/Rappen schenken.<br>
Ist der Differenzbetrag größer, ist unser Rat, den Lieferanten zur Richtigstellung seiner Rechnung aufzufordern, da ja die rechtlich formalen Voraussetzungen für die Rechnungslegung nicht erfüllt sind.<br>
Ist dies nicht möglich, so erfassen Sie bitte die Rechnung über die Splittbuchung und erfassen Sie den Vorsteuerbetrag gegen das Vorsteuerkonto als MwSt Handeingabe und den Rechnungs-Nettobetrag mit 0% MwSt.

#### Woher kommt der Defaultvorschlag der Kostenstelle?
Die standardmäßig vorgeschlagene Kostenstelle wird vom gewählten Lieferanten übernommen. Siehe dazu auch [Lieferant](../Lieferant/), Konditionen.

#### Wozu dient das Freigabedatum?
In **Kieselstein ERP** können zwei Datum zu einer Eingangsrechnung definiert werden.
Das eigentliche Eingangsrechnungsdatum und das Freigabedatum.
Das Eingangsrechnungsdatum dient für die Erfassung des Datums der Lieferantenrechnung z.B. für die Buchhaltung. Das ist also jenes Datum welches der Lieferant auf seiner Rechnung angibt.
Das Freigabedatum ist das Datum ab dem Sie die Eingangsrechnung akzeptieren. Denken Sie z.B. daran, dass Sie Ware vor dem gewünschten Liefertermin erhalten oder Sie erhalten eine Eingangsrechnung mit einem gänzlich falschen / viel zu frühen Datum. Hier geben Sie mit dem Freigabedatum an, ab wann Sie die ER akzeptieren. Ab dem Freigabedatum wird das Zahlungsziel gerechnet.
[Siehe dazu auch Geprüft / Freigabe einer Eingangsrechnung.](#Prüfung und Freigabe)

#### Wie wird das Zieldatum einer Eingangsrechnung ermittelt?
Das jeweils neu errechnete Zieldatum einer Eingangsrechnung ist das Freigabedatum zzgl. der Zieltage anhand des Zahlungszieles. Mit dem Zieldatum ist die Eingangsrechnung fällig.

#### Kann ich das Original an die Eingangsrechnung anhängen?
Mit der [Dokumentenablage]( {{<relref "/docs/stammdaten/system/dokumentenablage" >}} ) kann an die Eingangsrechnung auch die Kopie des jeweiligen Dokumentes angehängt werden. Damit haben Sie ganz praktisch die Daten / das Aussehen des Beleges vor sich, ohne lange die Papierablage durchsuchen zu müssen.

Für Belege, die Sie digital erhalten haben und es eine Aufbewahrungsfrist gibt, ist jedoch die Ablage in ein Dokumentenverwaltungssystem oder Ähnliches erforderlich. 

#### Wie kann ich eine Eingangsrechnung rückdatieren, wenn sich der Steuersatz geändert hat?
Ändern Sie die Daten in den Kopfdaten und speichern Sie. Dann ändern Sie die Kopfdaten und wählen den richtigen Steuersatz aus.

#### Wie werden Zahlungen verbucht?
Siehe [Zahlungen:]( {{<relref "/verkauf/rechnung/zahlung" >}} )
Hinweis: Für die Verbuchung von Eingangsrechnungsgutschriften [siehe bitte]
( {{<relref "/verkauf/rechnung/zahlung/#wie-kann-eine-gutschrift-bezahlt-werden" >}} ) und [auch]( {{<relref "/verkauf/rechnung/zahlung/#wie-ist-bei-der-skontozahlung-von-eingangsgutschriften-vorzugehen" >}} )

#### Kann eine abweichende Bankverbindung angegeben werden?
<a name="Abweichende Bankverbindung"></a>
Es steht auch die Möglichkeit für den [Zahlungsvorschlag]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag">}} ) eine Bankverbindung für die Zahlung auf ein Mitarbeiterkonto zu hinterlegen, wobei davon ausgegangen wird, dass der gesamte Brutto-Betrag der Eingangsrechnung an diesen Mitarbeiter zu überweisen ist. Wählen Sie dazu in den Kopfdaten der Eingangsrechnung ![](abweichende_Bankverbindung.gif) mit dem Button Abweichende Bankverbindung die Person aus, auf deren Bankverbindung (die erste) die Zahlung für diese Rechnung erfolgen sollte. Selbstverständlich muss für die Person eine entsprechende Bankverbindung hinterlegt sein.<br>
Bitte beachten, dass bei der Bankverbindung des Lieferanten auch die Währung angegeben werden kann, für die diese Bankverbindung verwendet werden sollte.<br>
![](Bankverbindung_mit_Waehrung.gif)<br>
Für Details im [Zahlungsvorschlagslauf]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag">}} ) siehe bitte dort.

#### Wechselnde Kurseingabe bei den Zahlungen
In manchen Ländern ist es üblich, dass von den Zollbehörden Wechselkurs vorgeschrieben werden, zu denen insbesondere offene Eingangsrechnungen zu betrachten sind, andererseits buchen natürlich die Banken zum Börse-Wechselkurs und nicht zu dem von den Behörden gewünschten Kurs. Daher ist es manchmal erforderlich, dass je Zahlungsbuchung in der Eingangsrechnung der tatsächlich von der Bank verbuchte Wechselkurs je Zahlung erfasst und verbucht wird.

Um diese Funktionalität zu erreichen stellen Sie im System den Parameter PARAMETER_EINGANGSRECHNUNG_ZAHLUNG_MIT_KURSEINGABE auf 1.

Nach dem Neustart des Eingangsrechnungsmodules erscheint bei der Zahlung neben dem Kurs die Checkbox Übersteuert. Wird diese angehakt, so kann der gewünschte Umrechnungskurs eingegeben werden, wobei der zum Zeitpunkt gültige Wechselkurs vorgeschlagen wird.

#### Werden die Umrechnungskurs angezeigt ?
Die Umrechnungskurse werden einerseits in den Kopfdaten der Eingangsrechnung angezeigt und andererseits bei jeder Zahlung.

Zusätzlich werden im Journal der offenen Eingangsrechnungen die original Beträge in Fremdwährung zusätzlich zu den Beträgen in Mandantenwährung angezeigt, damit z.B. bei Teilzahlungen offensichtlicher ist, welche Fremdwährungsbeträge tatsächlich offen sind. Zusätzlich werden die Umrechnungskurse angezeigt.

![](ER_offene_in_Fremdwaehrung.gif)

#### Kundendaten für E-Banking?
Wenn Sie den [Zahlungsvorschlag]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag">}} ) aus **Kieselstein ERP** nutzen, so sollten / können für jede Eingangsrechnung / für jeden Zahlungsvorgang Ihre Kundendaten an den Zahlungsempfänger übermittelt werden. Tragen Sie dafür die auf den Lieferantenbelegen angegebenen Daten in das Feld ![](Kundendaten_ELBA.gif) ein. Dieses Feld wird beim Anlegen einer Eingangsrechnung nach erfolgter Auswahl des Lieferanten automatisch auf die im Lieferanten angegebene ![](Lieferanten_Kundennummer.gif) gesetzt.

#### Zahlungsziel
[Siehe bitte]( {{<relref "/verkauf/gemeinsamkeiten/#zahlungsziel" >}} ).

#### Was ist die Aussage der Umsatzübersicht
Mit der Eingangsrechnungsumsatzübersicht sollten Sie einen raschen Überblick über Ihre offenen Verbindlichkeiten bekommen.<br>
![](ER_Uebersicht.jpg)

Die Ansicht ist in drei Bereiche geteilt. Links der Zeitbereich, in der Mitte die Offenen Eingangsrechnungen und rechts der Umsatz.<br>
So enthält z.B. die Zeile Mai alle Eingangsrechnungen, welche mit Eingangsrechnungsdatum Mai 2005 erfasst wurden.<br>
Unter der rechten Spalte Umsatz gibt es wiederum zwei Unterspalten: Brutto und Netto
Die Spalte Umsatz Brutto enthält die Summe aller für diese Zeile, in unserem Fall Mai, erfassten Eingangsrechnungen inkl. Umsatzsteuer (USt). Die Spalte Umsatz Netto enthält die Summe aller für diese Zeile erfassten Eingangsrechnungen exkl. USt.<br>
Die mittlere Spalte Offene ist ebenfalls in Brutto und Netto unterteilt. Sie enthält die zum Berechnungszeitpunkt noch offenen, also die nicht bezahlten oder teilweise nicht bezahlten, Eingangsrechnungen für diese Zeile. Auch hier unterteilt noch Brutto also inkl. Ust und Netto (exkl. Ust).<br>
Die Zeile Vorjahr ist die Summe aller Eingangsrechnungen des Vorjahres wiederum mit den vier Werten. Summe Jahr (2005) die Summe des laufenden Jahres.<br>
Die Zeile Summe Gesamt enthält nur die offenen Eingangsrechnungen.<br>
Hintergrund: Man benötigt in vielen Auswertungen und Vergleichen immer wieder die Umsätze in der jeweiligen Periode. Auch ist das eine wichtige Aussage über die Unternehmensentwicklung. Was bezahlt wurde interessiert die Geschäftsleitung, den Controller  nur insofern, dass er wissen will mit welchen Kunden es Probleme gibt, oder warum Lieferanten nicht bezahlt wurden. Diese Zusatzinfo ist wichtig, und wird in Zukunft bei den ARs auch angezeigt werden.

### Behandlung von Fremdwährungen
<a name="Fremdwährungen"></a>
Siehe [Rechnungen]( {{<relref "/verkauf/rechnung/#behandlung-von-fremdw%c3%a4hrungen" >}} ).

#### Behandlung von reinen Mehrwertsteuer-Rechnungen, reinen Vorsteuerrechnungen
<a name="reine Vorsteuerrechnungen"></a>
Gerade Einnahmen Ausgabenrechner wollen / müssen auch Ihre reinen Steuervorschreibungen z.B. für Einfuhr Umsatzsteuer erfassen können. Dafür ist es erforderlich, dass der exakte Steuerbetrag eingebucht werden kann.

Gehen Sie in dazu bitte wie folgt vor:
1. Definition der Handeingabe-Mehrwertsteuer<br>
Definieren Sie unter System, Mandant, Mwst für jeden Mandanten eine Mehrwertsteuerart Handeingabe.
![](Hand_Mwst1.gif)<br>
Verwende bitte keinen Steuersatz über 999%
2. Starten Sie nun das Eingangsrechnungsmodul (neu) ![](Hand_Mwst2.jpg)<br>
Legen Sie eine neue Eingangsrechnung an und definieren Sie Mehrfach / Splittbuchung. ![](Hand_Mwst3.gif)<br>Erfassen Sie Rechnungsnummer und vollen Betrag und speichern Sie die Eingangsrechnung ab.
3. Wählen Sie Kontierung ![](Hand_Mwst4.gif) und legen Sie mit neu einen neuen Datensatz für die (Splitt-)Kontierung an. Beim Mehrwertsteuersatz kann nun die definierte Handeingabe ausgewählt werden.
4. Geben Sie nun unter MwSt den gesamten bzw. den gewünschten Mehrwertsteueranteil an.

Bitte achte unbedingt auf die gesetzlichen Vorschriften.

#### Beim Abspeichern wird abweichendes Umsatzsteuerland gemeldet.
![](UST_unstimmig.gif)

Wird obige Meldung beim Speichern der Eingangsrechnung ausgegeben, so besagt diese, dass das Abweichende Umsatzsteuerland nicht mit dem Land des beim Konto definierten Finanzamtes zusammen stimmt. Das Abweichende Umsatzsteuerland im Lieferanten wurde vor allem für die Anwender geschaffen, die Umsatzsteuernummern in verschiedenen Ländern haben.

Um die richtige Definition sicherzustellen, prüfen Sie bitte

1. Das Abweichende Umsatzsteuerland des Lieferanten (Lieferant, Konditionen)
2. Finanzamt des Kontos (Finanzbuchhaltung, Konten, Kopfdaten, Finanzamt)
3. Das Länderkennzeichen des Finanzamtes, Partner, Finanzamt (= Name des Partners), Detail, Ort

Das durch den Ort des Finanzamtes definierte Land und das Abweichende Umsatzsteuerland des Lieferanten müssen zusammenstimmen. Siehe dazu auch [Buchen von Eingangsrechnungen mit abweichenden Umsatzsteuerländern.](#abweichende Umsatzsteuerländer)

#### Beim Abspeichern kommt eine Fehlermeldung
Wenn im Detailtext steht:

Fehlerbeschreibung: Der Beleg bezieht sich auf mehrere Finanzämter

So stimmt das durch den Lieferanten definierte (abweichende) Finanzamt mit dem Finanzamt welches bei den Konten definiert ist nicht überein. Entweder ist die Finanzamtszuordnung für den Lieferanten richtigzustellen, oder es muss eine Kontierung mit entsprechend zugeordnetem Finanzamt verwendet werden. Siehe dazu auch [Sachkonten definieren]( {{<relref "/management/finanzbuchhaltung/#welche-daten-m%c3%bcssen-f%c3%bcr-die-sachkonten-definiert-werden" >}} )

#### Können auch Überweisungsbelege gedruckt werden?
Ja. In der Eingangsrechnung stehen in den Kopfdaten, aber auch unter Eingangsrechnung Drucken zwei Formulare dafür zur Verfügung. Damit können Benutzer die noch immer Überweisungsformulare ausfüllen komfortabel diese Formulare bedruckt werden.

Hier kann ein Zusatztext und ein Betrag angegeben werden. Diese beiden Werte werden **nicht** abgespeichert. Sie dienen lediglich dem Druck des Zahlscheines.
Bitte beachten Sie, dass für den Druck der Überweisungsbelege in der Regel die maschinenlesbare Schrift OCR-A am Server und Client installiert sein müssen.

#### Können auch Schecks gedruckt werden?
Ja, fragen Sie Ihren **Kieselstein ERP** Betreuer um die Einrichtung des Formulars.

Um auch laufende Schecknummern mitdrucken zu können, muss in den Mandantenparametern der Parameter SCHECKNUMMER auf größer 0 gesetzt werden. Erst wenn die Schecknummer > 0 definiert wurde, steht dieses Feld, nach einem Neustart des Eingangsrechnungsmoduls, zur Eingabe zur Verfügung.

![](ER-Druck_Scheck.gif)

Immer beim Ausdrucken eines Schecks, wird die nächst höhere Schecknummer vorgeschlagen. Wird nun der Scheck / das Formular tatsächlich gedruckt, so wird die nun erfasste Schecknummer in den Mandantenparametern aktualisiert. Somit steht für den nächsten Ausdruck die nächste Schecknummer zur Verfügung.

#### Wo sehe ich, für welche ERs schon Überweisungen bzw. Schecks gedruckt wurden?
Dies wird in der Auswahlliste durch die Anzeige ![](ER_Zahlbeleg_gedruckt.gif) des Druckersymbols in der Spalte Status angezeigt.

Für die Anzeige, wann der Zahlungsbeleg zu diese Eingangsrechnung gedruckt wurde klicken Sie bitte auf die Eingangsrechnung. Es wird nun unten in der Statuszeile das Druckdatum angezeigt. ![](ER_Zahlbeleg_Druckdatum.gif)
Hinweis: Das Druckersymbol wird auch angezeigt, wenn eine Ausgangsgutschrift erstellt wurde.

#### Im Reiter Wareneingänge können keine Einträge hinzugefügt werden?
Da der Ablauf in der Praxis so ist, dass aus den Wareneingängen Eingangsrechnungen erzeugt werden, finden Sie die gesuchte Funktionalität im Modul Bestellungen, Reiter Wareneingang. [Für eine genauere Beschreibung siehe bitte dort]( {{<relref "/einkauf/bestellung/eingangsrechnungszuordnung">}} ).

#### Im Reiter Wareneingänge wird der Nettowert der Eingangsrechnung angezeigt ?
Diese Anzeige dient dazu, um die Übereinstimmung der Eingangsrechnung mit den darauf gebuchten Wareneingängen anzuzeigen. Um eine eventuelle Abweichung deutlich zu signalisieren wird dieser ![](Wareneingang_rot.gif) in rot angezeigt, wenn die Beträge nicht übereinstimmen. So haben Sie bei der Zuordnung mehrer Wareneingänge zu einer Eingangsrechnung rasch die Sicherheit, dass die Gesamtsumme der Wareneingänge auch mit der Eingangsrechnung übereinstimmen.

#### Kann man mehrere Wareneingänge bzw. Bestellungen einer Eingangsrechnung zuordnen?
<a name="Wareneingänge über die Eingangsrechnung zuordnen"></a>
Ja. Gerade wenn Sie von Ihrem Lieferanten monatliche Sammelrechnungen bekommen, ist oft die umgekehrte Vorgehensweise leichter.
D.h. es wird zuerst die Eingangsrechnung angelegt und dann mit dem Reiter 7 Wareneingänge die Wareneingänge der unterschiedlichsten Bestellungen, die alle als Rechnungsadresse von diesem Lieferanten sein müssen, und deren Wareneingänge noch nicht einer Eingangsrechnung zugeordnet sind, damit zugeordnet.
Klicken Sie dazu im Reiter Wareneingänge auf Neu ![](Wareneingang_zuordnen1.gif). Nun erhalten Sie die Liste der Wareneingänge die noch keine Eingangsrechnung zugeordnet haben.
![](Wareneingang_zuordnen2.jpg)
Genau genommen sind es die noch nicht einer Eingangsrechnung zugeordneten Wareneingänge der Bestellungen, deren Rechnungsadresse dem Lieferanten der Eingangsrechnung entspricht.

Wählen Sie nun durch markieren einer oder mehrerer Zeilen die entsprechenden Wareneingänge aus.
Eventuelle Transportkosten bearbeiten Sie, in dem auf Ändern ![](Wareneingang_zuordnen3.gif) geklickt wird und anschließend unten die entsprechenden Kosten eingegeben werden.
Die gewählten Wareneingänge übernehmen Sie durch Klick auf den Haken ![](Wareneingang_zuordnen4.gif).
Bitte beachten Sie, dass damit die Zuordnung bereits hergestellt wurde, auch wenn, aufgrund des Ablaufes, das Preis erfasst auf der einzelnen Wareneingangsposition noch nicht angehakt ist. Sollte dies nicht gemacht werden / vergessen werden, so wird dies durch die rote Anzeige in der Liste der Wareneingänge der Eingangsrechnung signalisiert.

Nun erhalten Sie die Liste der Positionen die mit den gewählten Wareneingängen zugebucht wurden.
Hier bestätigen Sie die Positionspreise durch Klick auf ![](Wareneingang_zuordnen5.gif) Preis erfasst. Um mehrere Zeilen auf einmal auf Preis erfasst zu setzen, markieren Sie diese Zeilen und klicken auf ![](Wareneingang_zuordnen6.gif). Dadurch werden alle markierten Positionen auf den gegenteiligen Wert der ersten Zeile gesetzt. Wenn also z.B. die erste markierte Zeile noch nicht gesetzt ist, so werden alle markierten Positionen gesetzt und umgekehrt.
Sollten Sie Positionspreise ändern müssen, so klicken Sie auf ![](Wareneingang_zuordnen7.gif) ändern.
Ist die Erfassung abgeschlossen, so verlassen Sie diese durch die Tür ![](Wareneingang_zuordnen8.gif).

Aus der Liste der Wareneingänge der Eingangsrechnungen können Sie mit dem GoTo ![](Wareneingang_zuordnen9.gif)direkt auf den betreffenden Wareneingang springen.
Mit Löschen ![](Wareneingang_zuordnen10.gif) entfernen Sie die Zuordnung wieder.

#### Auftragszuordnung wozu?
<a name="Auftragszuordnung"></a>
Im Reiter Auftragszuordnung kann der Nettowert einer Eingangsrechnung auf mehrere Aufträge zugeordnet werden.

Wir haben diese Funktion vor allem für jene Anwendungsfälle geschaffen, bei denen keine Materialbewegungen / Warenbuchungen stattfinden. Z.B. bei reinen Dienstleistungen. Bitte beachten Sie dazu unbedingt das Thema der [Transportkosten der Wareneingänge]( {{<relref "/einkauf/bestellung/#muss-ich-die-transportkosten-zus%c3%a4tzlich-als-aufwand-einem-auftrag-zuordnen" >}} ) und vermeiden Sie, dass die Kosten einmal auf dem Weg der Materialzubuchung und zusätzlich auf dem Weg der "zusätzlichen" Kostenbuchung auf die Aufträge über die Eingangsrechnung erfolgt.

#### Kann eine Eingangsrechnung auf einen bereits erledigten Auftrag zugeordnet werden?
Nein.<br>
Der Hintergrund ist, dass wenn ein Auftrag vollständig erledigt ist, so ist davon auszugehen, dass auch keine Kosten mehr auf diesen Auftrag gebucht werden dürfen. Es würde sich dadurch ja auch die Deckungsbeitragsrechnung verändern. Dies wird damit verhindert.<br>
Ausnahme: Wenn das Recht AUFT_DARF_AUFTRAG_ERLEDIGEN vorhanden ist, dann können Eingangsrechnungen auch erledigten Aufträgen auf Nachfrage zugeordnet werden.

#### Kann jeder Benutzer einen Auftrag erledigen, werden Aufträge automatisch erledigt?
Dies hängt von der Einstellung des Parameters AUFTRAG_AUTOMATISCH_VOLLSTAENDIG_ERLEDIGEN ab. Steht dieser auf 1 so werden Aufträge automatisch, wenn sie vollständig geliefert wurden erledigt und es kann auch jeder, der eine schreibende Berechtigung für die Aufträge hat, einen Auftrag erledigen oder auch enterledigen (Erledigung wieder aufheben).
Ist dieser Parameter AUFTRAG_AUTOMATISCH_VOLLSTAENDIG_ERLEDIGEN abgeschaltet (steht auf 0), so werden grundsätzlich Aufträge nicht vollständig erledigt. Dies hat den Hintergrund, dass gerade umfangreichere Aufträge vor der vollständigen Erledigung einer Prüfung unterzogen werden. Die Erledigung eines Auftrages ist in diesem Falle mit dem Rollenrecht AUFT_DARF_AUFTRAG_ERLEDIGEN verbunden. D.h. nur Benutzer die Mitglieder einer Systemrolle sind, die dieses Recht besitzt können Aufträge erledigen bzw. enterledigen.

<a name="abweichende Umsatzsteuerländer"></a>

#### Buchen von Eingangsrechnungen mit abweichenden Umsatzsteuerländern usw.
Einige unserer Schweizer Anwender unterhalten aus verschiedensten Vereinfachungsgründen auch ein Zoll-Außenlager z.B. in Deutschland. Um nun die Vorteile / Vereinfachungen nutzen zu können, haben diese Anwender auch eine deutsche Steuernummer und können damit Warenverkehr innerhalb der EU entsprechend abwickeln. Vielfach ist es nun auch so, dass deutsche Speditionen beauftragt werden, Lieferungen für Sie durchzuführen. Diese Speditionen stellen dann entsprechende Rechnungen für die erbrachten Leistungen, welche auch sehr oft gemischte Vorgänge beinhalten.

Nun hier ein etwas extremes Beispiel, welches in der Praxis durchaus vorkommt.

Spediteur R. mit Sitz in Deutschland

**Kieselstein ERP** Anwender mit Sitz in der Schweiz

Die Rechnung beinhaltet:

| Nr | Bewegung | Betrag | Kostenart / Finanzamt |
| --- |  --- |  --: |  :--: |
| 1 | Seefracht für Lieferung von Asien nach Deutschland | 1.180,62 | Deutschland |
| 2 | Zollgebühren | 215,76 | Schweiz |
| 3 | Einfuhrumsatzsteuer für Import von (anderen) Waren in die Schweiz | 1207,48 | Schweiz |

Wichtig:

Um die getrennte steuerliche Behandlung zu ermöglichen, müssen zwei Finanzämter definiert werden.
| Finanzamt | Bemerkung |
| --- | --- |
| Finanzamt Bern | Dein standard Finanzamt in der Schweiz |
| Finanzamt Konstanz | Dein Finanzamt für die Abwicklung der deutschen / innergemeinschaftlichen Warenbewegungen |

Um in den Auswertungen klar die Trennung zwischen den beiden Bereichen zu haben, sollte / wird die Kostenstellenverwaltung verwendet, mit der die Geschäfte, welche über das Schweizer Finanzamt abzuwickeln sind und die Geschäfte, die über das deutsche Finanzamt abzuwickeln sind, klar unterschieden werden. D.h. hier sind z.B. zwei Kostenstellen mit Inland und Ausland angelegt.

Zusätzlich:

Die für die Erfassung der Eingangsrechnungen erforderlichen Konten, müssen ebenfalls für die <u>jeweiligen</u> Finanzämter angelegt sein.

Da die Lieferanten (und gegebenenfalls auch Kunden) Rechnungen für die unterschiedlichen Finanzämter legen, müssen die Lieferanten einmal mit der Zuordnung (abweichendes Umsatzsteuerland) zum Finanzamt Bern und einmal mit der Zuordnung zum Finanzamt Konstanz angelegt werden. Beachten Sie, dass für die Prüfung die beim jeweiligen Finanzamt hinterlegten Länder verwendet werden.

Wie wird nun gebucht:

1. Seefracht Lieferant mit Ust-Land DE auf Kostenstelle Ausland und Kontierung Eingangsfracht mit Finanzamt Konstanz
2. Zollgebühren Lieferant mit Ust-Land CH auf Kostenstelle Inland und Kontierung Umsatzsteuer CH mit Finanzamt Bern
3. EUST Lieferant mit Ust-Land CH auf Kostenstelle Inland und Kontierung Vorsteuer CH mit Finanzamt Bern

Wichtig:

Zu deiner Sicherheit ist bei den Buchungen eine Verriegelung eingebaut, welche prüft ob das bei der Kontierung hinterlegte Finanzamt mit dem Finanzamt des Lieferanten übereinstimmt. Ist dies nicht der Fall, so erscheint die Meldung der Beleg bezieht sich auf mehrere Finanzämter.

![](Mehrere_Finanzaemter.jpg)

Dies bedeutet, dass das beim Lieferanten hinterlegte (abweichende) Umsatzsteuerland nicht mit dem Finanzamt des Kontos übereinstimmt. Bitte gleichen die Einstellungen entsprechend ab.

Warum ist das so strickt getrennt?

Wenn mehrere Finanzämter verwendet werden, so muss für jedes Finanzamt eine eigene Umsatzsteuererklärung abgegeben werden. Das bedeutet, dass alle Buchungen für das jeweilige Umsatzsteuerland (des Lieferanten) auch auf ein Konto welches dem Finanzamt zugeordnet ist gebucht werden müssen.

#### Wird nun der Brutto- oder der Nettowert in der Auswahlliste angezeigt?
<a name="Brutto- oder-Netto in Auswahlliste"></a>
Das hängt davon ab. [Siehe dazu bitte]( {{<relref "/verkauf/rechnung/#wird-nun-der-brutto--oder-der-nettowert-in-der-auswahlliste-angezeigt">}} ).

#### Erledigen einer Eingangsrechnung
Wurde z.B. eine Eingangsrechnung nur teilweise bezahlt, so kann diese auch nachträglich mit dem Menüpunkt ![](ER_Manuell_Erledigen.gif) Bearbeiten, Manuell erledigen erledigt werden. Dies ist auch bei einer Eingangsrechnungsgutschrift möglich, welche z.B. gar keinen Zahlungseintrag hat. Wird eine Eingangsrechnung erledigt, unterscheidet **Kieselstein ERP** zwischen Währungsgewinn/Verlust und Skonto. Somit wird bei Eingangsrechnungen, die erledigt gekennzeichnet werden und der Betrag nicht dem vollen Umfang entspricht, die Differenz auf Skontoerlöse gebucht. 

#### Erledigung einer Eingangsrechnung wieder aufheben.
Auch für die Enterledigung einer Eingangsrechnung, z.B. weil diese aus Versehen erledigt wurde, wählen Sie den Punkt: Bearbeiten, Manuell erledigen und bestätigen die Frage, Erledigung aufheben mit Ja.

**Hinweis:**<br>
Wird bei einer erledigten Eingangsrechnung in den Kopfdaten auf Ändern geklickt, so erscheint ebenfalls die Frage nach der Enterledigung. Es wird dabei jedoch der Status der Eingangsrechnung immer auf Teilbezahlt gesetzt. Bei Bearbeiten, Manuell erledigen wird auch der Zahlungsstatus in die Berechnung des neuen Status mit einbezogen.

#### Kann die Kontierung / Kostenstelle einer Eingangsrechnung auch geändert werden wenn diese schon bezahlt ist?
<a name="Ändern der Kontierung"></a>
Wenn eine Eingangsrechnung bereits vollständig bezahlt ist, aber noch nicht in die Finanzbuchhaltung (FiBu) übertragen wurde, ist es manchmal erforderlich, dass die Zuordnung dieser ER noch verändert werden sollte. Dies kann im oberen Modulreiter ![](Zuordnung_aendern.gif) Zuordnung durchgeführt werden. Wurde eine Eingangsrechnung auf Splittbuchung gesetzt, so kann im Reiter Kontierung diese ebenfalls solange geändert werden, bis diese nicht in die FiBu exportiert wurde. Beantworten Sie dazu die nachfolgende Frage

![](Splittbuchung_aendern.jpg) mit Ja.

#### Wieso wird der Kurs in den Kopfdaten in Rot angezeigt?
<a name="Wechselkurs_unstimmig"></a>
Diese Anzeige signalisiert, dass der in der Eingangsrechnung hinterlegte Wechselkurs zum aktuell in der Währungstabelle eingetragenen Umrechnungskurs abweicht.

Bitte prüfen Sie welcher der Kurse der richtige Wechselkurs ist.

Mit ändern, speichern wird der nun gültige Wechselkurs neu aus der Währungstabelle übernommen und der Wert in Ihrer Mandantenwährung neu errechnet.

#### Wie wird eine Eingangsgutschrift erledigt, als Bezahlt verbucht?
<a name="Eingangsgutschrift verbuchen"></a>
Um eine Eingangsgutschrift als bezahlt bzw. gegenverrechnet bzw. ausgeglichen zu verbuchen, muss diese "Zahlung" auf der Eingangsrechnung eingetragen werden, gegen die die Eingangsgutschrift gebucht werden sollte. Üblicherweise sollten Sie dies gleich wie Ihr Lieferant verbuchen.

D.h. Sie gehen auf die Eingangsrechnung.

Wählen den Reiter Zahlung und legen mit Neu einen neuen Zahlungseintrag an.

Wählen Sie nun als Zahlungsart Gutschrift ![](ER_Gutschrift_Zahlung1.gif) aus und

wählen durch Klick auf den Knopf Gutschrift ![](ER_Gutschrift_Zahlung2.gif) die gewünschte Eingangsrechnung aus.

Damit reduziert sich der offene Wert der Eingangsrechnung und wird auch im [Zahlungsvorschlag]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag">}} ) nur mit dem reduzierten Wert vorgeschlagen.

<a name="Reverse Charge, IG-Erwerb"></a>

#### Reverse Charge, IG-Erwerb
Gerade für Buchhaltungsbelange ist es bei innergemeinschaftlichen Lieferungen wesentlich, welche Art von Lieferung dies ist. Derzeit wird hier zwischen:

   IG-Lieferung ... IG-Erwerb

   IG-Leistung ... Reverse Charge

unterschieden.

![](IG_Erwerb1.gif)

Diese Angaben bewirken, in Zusammenhang mit einem gültigen Mehrwertsteuersatz, dass hier zwar Nettobeträge eingegeben werden, dass aber für die Buchung der theoretische Steuerbetrag mit berücksichtigt wird und vor allem in den Fibu-Schnittstellen diese Daten entsprechend übergeben werden.

Beachten Sie auch, dass die Anzeige der MwSt. entsprechend auf die ![](IG_Erwerb2.gif) theoretische Umsatzsteuer umgeschaltet wird und somit auch die Berechnung der angezeigten Umsatzsteuer auf dem Nettobetrag der Eingangsrechnung basiert und nicht wie bei Inlandsrechnungen auf dem Bruttobetrag. Dies wird auch durch die Umschaltung der Anzeige von Brutto-Betrag auf Betrag signalisiert.

Der Standardwert für Reverse Charge kann im Lieferanten, Reiter Konditionen eingestellt werden.

Für die Behandlung in der integrierten Finanzbuchhaltung siehe bitte.

<a name="Reverse Charge, IG-Erwerb0"></a>

#### Ein EU-Lieferant erbringt eine Inlands-Lieferung, wie kann dies richtig erfasst werden?
Bei der integrierten Buchhaltung ist hier darauf zu achten, dass dieser Lieferant als Inlandslieferant behandelt wird. D.h. für den Kreditor ist die Steuerkategorie auf Inland zu stellen.
Sie bemerken dies bei der Erfassung der Eingangsrechnung auch dadurch dass entweder IG-Erwerb automatisch angehakt (und nicht entfernbar) ist, oder dass eben das IG-Erwerb NICHT angehakt ist.

#### Ich bekommen vom gleichen EU-Lieferanten manchmal Inlands und manchmal IG-Rechnungen. Wie ist damit umzugehen?
Hier muss der Lieferant zwei verschiedene Kreditorennummern bekommen, da dies einerseits ein Inlandslieferant ist und andererseits ein ausländischer Lieferant mit oder ohne UID Nummer.
Es muss Ihr Lieferant bei der Inlandslieferung ja auch die Inlands UST an das (= IHR) Inland abführen, wozu er eine Inlands UID Nummer benötigt.

#### Zollimportpapiere
Für unsere Schweizer Anwender ist es oft wichtig, dass auch protokolliert wird, dass die zu einer Einfuhr dazugehörenden Zollpapiere tatsächlich vorhanden sind. Dazu haben wir die Protokollierung ![](ER_Zollpapiere_erhalten.gif) des Erhalts der Zollpapiere, oft auch Gelber Schein genannt, geschaffen.

Ein Klick darauf setzt das Datum wieder zurück.

Da nicht für alle Lieferanten diese Papiere erforderlich sind, muss im Lieferanten unter Konditionen ![](ER_Zollimportpapiere_erforderlich.gif) Zollimportpapiere erforderlich angehakt werden.

Um nun festzustellen für welche Eingangsrechnungen noch die Zollpapiere fehlen, bei deren Lieferanten auch angehakt ist, dass Zollimportpapiere erforderlich sind, wählen Sie Journal, fehlende Zollimportpapiere.

#### Mahnungsverwaltung
Im Menü der Eingangsrechnungen kann unter Bearbeiten, Mahnstufe das Mahndatum und die Mahnstufe zur jeweiligen Eingangsrechnung als Information hinterlegt werden.

<a name="Vorauszahlungen"></a>

#### Vorauszahlungen
Es kommt immer wieder vor, dass für verschiedene Lieferungen von Lieferanten Vorauskasse geleistet werden muss. Dies kann in zwei verschiedenen Arten abgebildet werden.

1. Sie erhalten von Ihrem Lieferanten eine Anzahlungsrechnung. Sie erfassen diese Anzahlungs-Eingangs-Rechnung mit der Eingangsrechnungsart Anzahlungsrechnung. Dies erzwingt automatisch eine Bestellung auf die sich die Anzahlungs-Eingangs-Rechnung bezieht. Wird dann vom Lieferanten die Ware geliefert erhalten Sie anschließend die Schlussrechnung über den vollen Betrag. Da sich auch die Schlussrechnung auf die gleiche Bestellung beziehen muss, ergibt sich aus der Differenz von Anzahlungsrechnung(en) und Schlussrechnung der noch offene Zahlbetrag. Gerade wenn keine integrierte Finanzbuchhaltung zum Einsatz kommt, ist diese Vorgehensweise zu empfehlen. Verwenden Sie gegebenenfalls die Auftragsbestätigung Ihres Lieferanten mit der die Vorauszahlung gefordert wird als Anzahlungsrechnung. Denken Sie auch daran dass:
  1. Sie dem Lieferanten rechtlich gesehen nur Ihr Geld leihen.
  2. Es immer wieder vorkommt, dass die Vorauszahlung (die ja als Anzahlungsrechnung erfasst wird) nicht mit der Schlussrechnung übereinstimmt.
  3. Läuft die Beschaffung dann noch über den Geschäftsjahreswechsel, so ist dies der einzig transparente Weg um die Abgrenzungen über den Jahreswechsel hin zu erreichen.

2. Wird auch die integrierte Finanzbuchhaltung eingesetzt und sind die Vorauszahlungen ein seltener Sonderfall, so kann so vorgegangen werden, dass es ein Zwischenkonto gibt, auf das die Vorauszahlung gegen Bank gebucht wird. Von diesem Konto ([als Bankkonto definiert]
( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen">}} )
 wird dann die Zahlung der schlussendlichen Rechnung gebucht und somit das Zwischenkonto wieder ausgeglichen. Diese Vorgehensweise ist nur für sehr wenige Vorauszahlungsbuchungen zu empfehlen. Steuerlich richtig ist obige Version. Um bei mehreren Lieferanten mehr Übersicht zu gewinnen kann pro Lieferant ein eigenes geleistete Anzahlungen - Konto angelegt werden, auf das die Buchungen erfolgen. 

3. Eine weitere Möglichkeit ist, dass die Vorauszahlung als Umbuchung Bank an Debitor bzw. Kreditor an Bank manuell gebucht wird. Bei der Zahlung der Rechnung/Eingangsrechnung kann bei Zahlungsart Vorauszahlung aus den "Offenen Posten" mit Buchungsart Umbuchung und Buchungsart Bankbuchung des jeweiligen Debitor/Kreditor gewählt werden. Achtung: Die Vorgehensweise in diesem Fall ist, dass die ursprüngliche Buchung um den Betrag der Zahlung reduziert und somit geändert wird. Ist die Vorauszahlung höher als der Rechnungsbetrag wird der Rest als neue Vorauszahlungs-Umbuchung gebucht. [Weitere Informationen]( {{<relref "/verkauf/rechnung/zahlung/#zahlungsart-vorauszahlung">}} )

Bitte beachten Sie, dass eine Anzahlungsrechnung nicht durch eine Vorauszahlung bezahlt werden darf. [Siehe](#kann-eine-anzahlungsrechnung-durch-eine-vorauszahlung-bezahlt-werden).

#### Was sind Anzahlungs- und Schlussrechnungen, wie ist damit umzugehen?
Siehe dazu bitte die Beschreibung für die [Ausgangs-Anzahlungs- bzw. Schlussrechnungen]( {{<relref "/verkauf/rechnung/anzahlung_und_schlussrechnung">}} ). Die Verwendung auf der Eingangsrechnungsseite ist analog. Es wird anstelle des Auftrages die Bestellung als zusammenfassende Klammer zwischen Anzahlungs- und Schlussrechnung verwendet.

{{% alert title="Vorsteuer Behandlung der Anzahlungsrechnung" color="primary" %}}
Bitte beachte, dass die in der (Inländischen) Anzahlungs-Eingangsrechnung enthaltene Vorsteuer erst mit der Zahlung in der UVA berücksichtigt werden darf.<br>
Aufgrund der aktuellen Buchungslogik kann bei der Berechnung der Vorsteuersumme nicht zwischen Anzahlungs-Eingangsrechnungen und normalen Eingangsrechnungen unterschieden werden.<br>
**Daher muss das Eingangsrechnungsdatum im gleichen Monat sein wie die vollständige Bezahlung der Anzahlungseingangsrechnung**.<br>
Anderenfalls würdest du dir unerlaubter Weise zu früh die Vorsteuer vom Finanzamt zurückholen, was richtig teuer werden kann!
{{% /alert %}}

#### Wie wird mit Schlussrechnungen umgegangen, deren Wert niedriger als die Summe der Anzahlungsrechnungen ist?
Ehrlich gesagt: Eigentlich sollte es so etwas nicht geben. Soweit die Theorie. Die Praxis lehrt uns, dass es auch das gibt. Also:

Ein Lieferant stellt ihnen eine oder mehrere Anzahlungsrechnungen. Mit der übermittelten Schlussrechnung stellt sich heraus, dass Sie zuviel angezahlt haben. Wie ist nun vorzugehen?

**Wichtig:**<br>
Durch die zu niedrige Schlussrechnung leihen Sie weiterhin ihrem Lieferanten Geld (siehe oben). D.h. der aus der Schlussrechnung noch offene Betrag muss über das Konto der geleisteten Anzahlungen (1800) gebucht werden. Definieren Sie dafür das Anzahlungskonto als [sichtbare Bankverbindung]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen)">}} ).

**WICHTIG:**<br>
Da durch das Verhalten des Lieferanten die Anzahlungs-/Schlussrechnungs-Thematik noch nicht abgeschlossen ist, sollten Sie die Schlussrechnung erst dann als bezahlt verbuchen, wenn klar ist, was mit dem überschüssigen Betrag zu geschehen hat. Bis dahin muss die Schlussrechnung offen bleiben. Buchen Sie die Differenz, die ja zu Ihren Gunsten besteht, erst aus, wenn eine weitere Verwendung dafür gegeben ist.

Damit sehen Sie jederzeit am Lieferanten/Kreditorenkonto, dass hier noch offene Beträge zu Ihren Gunsten bestehen. Wir raten dazu, die Lieferanten sehr eindringlich dazu aufzufordern, hier ordentlich zu arbeiten, sodass es zu diesen Konstellationen erst gar nicht kommt.

Wurde die Verwendung der Gelder die Sie noch vom Lieferanten bekommen definiert, so ist der Buchungsverlauf wie folgt:

1. Erfassen Sie die nachfolgende Eingangsrechnung
2. Gehen Sie auf der Schlussrechnung auf den Reiter Zahlungen und buchen Sie den negativen Zahlbetrag auf das (Bank-)Konto geleistete Anzahlungen.
3. Gehen Sie in die nachfolgende Eingangsrechnung und buchen Sie hier den gleichen Betrag, nun aber positiv, als Zahlung vom Konto geleistete Anzahlungen.

Damit ist, für diese Rechnungen das geleistete Anzahlungen-Konto ausgeglichen.

Zugleich erhalten Sie durch diese späte Buchung auch die Übersicht über den tatsächlichen Offenen Postenstand am Kreditor. Das Konto der geleisteten Anzahlungen sollte sowieso immer im Auge behalten werden, dass dieses in aller Regel ausgeglichen ist.

#### Proformarechnungen
Manchmal bekommt man von inländischen Lieferanten Proformarechnungen bei dem du dir vom Inhalt her noch nicht die Vorsteuer abziehen darfst.<br>
{{% alert title="Frag deinen Steuerberater" color="primary" %}}
**Bitte kläre diesen Umstand mit deinem Steuerberater!**
{{% /alert %}}
Wenn es der Fall ist, dass dein Lieferant zwar Geld von dir möchte, du aber dir die Vorsteuer (noch nicht, sondern) erst bei der tatsächlichen Lieferung abziehen darfst, so raten wir zu folgender Vorgehensweise.<br>
Das wichtigste ist, dass dies kein Aufwand ist, sondern nur ein Geld von a nach b buchen. D.h. diese Zahlung an deinen Lieferanten ist wie ein weiteres Bankkonto zu behandeln.<br>
Genau genommen, ist diese Zahlung auch in der Bilanz anzuführen, da ja "irgendwo" Geld herum liegt, das zwar dir gehört, du hast aber keine Verfügung darüber.<br>
- D.h. du legst ein eigenes Bilanzkonto z.B. *2290 Geleistete Anzahlungen an Lieferanten OHNE Vorsteuer* an.
- Nun erfasst du die Eingangsrechnung mit dem brutto Betrag auf dieses Konto, aber **ohne Vorsteuer**, obwohl diese ER im Inland ist und auf der ER auch die Umsatzsteuer ausgewiesen ist.<br>
Vielleicht hinterlegst du dir beim Lieferanten auch einen Hinweis (siehe Reiter Kommentar), dass im Falle des Falles darauf zu achten ist.<br>
**Wichtig:** Dies ist eine gewöhnliche Eingangsrechnung und keine Anzahlungsrechnung.
- Nun bezahlst du diese ER wie üblich.
- nun sendet der Lieferant, nach einigen Tagen / Wochen, dir die Ware und sendet dir damit auch die Schlussrechnung.
- Diese Schlussrechnung wird ebenfalls als gewöhnliche Eingangsrechnung erfasst. Die Kontierung dieser ER geht auf die üblichen Aufwandskonten.
- nun wird die Schlussrechnung gegen das Konto 2290 Geleistete Anzahlungen bezahlt. 
  - Dafür muss das Konto auch als Bankverbindung definiert sein. [Siehe]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen">}} )
- hier ist darauf zu achten, dass der Zahlbetrag zu den offenen Anzahlungsbeträgen passt

Im Endeffekt muss das Konto 2290 Geleistete Anzahlungen an Lieferanten OHNE Vorsteuer wieder null sein. Das bedeutet zugleich, dass dieses Konto immer wieder überprüft werden muss und schlussendlich zum Geschäftsjahreswechsel ausgeglichen sein sollte. Gegebenenfalls nutzt du die Spezialfunktion deiner G+V und schreibst in die Kontenbezeichnung noch ein # mit rein, womit dieses Konto am Ende der G+V / Erfolgsrechnung mit aufgeführt wird.




<a name="Vorauszahlung nicht bei Anzahlungsrechnung"></a>

#### Kann eine Anzahlungsrechnung durch eine Vorauszahlung bezahlt werden?
Nein das ist nicht gestattet. Bei der Verbuchung der Bezahlung von Anzahlungsrechnungen sind eine Vielzahl von Dingen zu berücksichtigen.<br>
Zusätzlich erstellt man eine Anzahlungsrechnung deshalb, weil der Lieferant Geld möchte bevor er zu arbeiten beginnt. D.h. die Kombination Zahlung einer Anzahlungsrechnung durch eine Zahlung die bereits früher erfolgt ist, ist in sich unlogisch. Daher ist dies nicht möglich/gestattet. Sollten Sie tatsächlich den Fall haben, dass Sie nach einer Zahlung von Ihrem Lieferanten dann doch eine Rechnung im Status einer Anzahlungsrechnung bekommen, so stornieren Sie bitte die Zahlung in der Bank und buchen exakt den gleichen Betrag zum exakt gleichen Datum mit exakt der gleichen Kontoauszugsnummer als Zahlung in die Eingangsrechnung. Alternativ können Sie dies auch über ein sogenanntes Durchläuferkonto abbilden.

<a name="ER erfassen"></a>

#### Wie wird eine Eingangsrechnung richtig erfasst?
Um eine Eingangsrechnung richtig zu erfassen sind folgende Dinge notwendig:

- Erfassung der Eingangsrechnung im **Kieselstein ERP**. Dass alle Daten richtig und wahrheitsgemäß angegeben werden erklärt sich von selbst. Achten Sie auch auf das Eingangsrechnungsdatum.
- Anbringen der **Kieselstein ERP** Eingangsrechnungsnummer und der Kontierung handschriftlich oder mit Stempel auf der "Papier"-Rechnung (des Lieferanten)
  - Beispiel1: handschriftliche Erfassung
  ![](ER_Erfassung_handschriftlich.jpg)

  - Beispiel2: Erfassung mittels Stempelunterstützung
  ![](ER_Stempel.gif)
  Denken Sie daran, bei Splittbuchungen die unterschiedlichen Kontierungen mit den gebuchten Beträgen zu versehen
- Erst nun sollten Sie die Eingangsrechnung einscannen
- Bei Rechnungen die Sie per EMail bekommen haben, sollten Sie auch an den [dokumentierten Prüfablauf]( {{<relref "/verkauf/rechnung/elektronische_rechnung/#was-ist-beim-empfang-elektronischer-rechnungen-zu-beachten" >}} )
denken.

Gerne kann auch der Menüpunkt, Eingangsrechnung, Dokumente drucken verwendet werden. Dieser ist besonders praktisch, wenn die Eingangsrechnungen z.B. über die Rest-API in dein **Kieselstein ERP** eingespielt wurde.

#### Wie gehts dann weiter?
Wenn die Eingangsrechnung bezahlt wurde, empfiehlt sich dies ebenfalls auf den Papieren mit Buchungsdatum und gegebenenfalls auch von welchem Bankkonto aus dies bezahlt wurde direkt unter der Kontierung festzuhalten. Wenn Sie einen Stempel verwenden wollen, denken Sie bei der Bestellung dieser Stempel auch an ein Feld für die Zahlung. 

#### Es wurde versehentlich eine Eingangsrechnungsgutschrift bezahlt, wie kann ich das auflösen?
Wenn eine Zahlung auf eine Rechnung (es gilt dies analog auch für Ausgangsrechnungen) getätigt wurde, die, aus welchen Gründen auch immer, (auf der Bank) gebucht wurde, so raten wir aus Gründen der Transparenz, diese falsche Buchung soweit irgend möglich direkt beim Beleg einzubuchen, damit Sie zu jederzeit einen klaren Blick auf die Situation dieses Beleges / Kunden / Lieferanten haben. Anbei ein Beispiel in dem eine Eingangsrechnungsgutschrift versehentlich bezahlt wurde.

![](ER_Ueberzahlung.jpg)
Der Ablauf war folgender:
1. versehentliche Zahlung an den Lieferanten (anstatt diesen Betrag einzubehalten)
2. Lieferant wurde informiert und hat den gesamten Betrag, also die versehentliche Zahlung und die Gutschrift zurücküberwiesen.

Schritte im **Kieselstein ERP**:
1. Änderung der Eingangsrechnungsart von Gutschrift auf Eingangsrechnung. Der negative Betrag muss erhalten bleiben.
2. Verbuchen der falschen Zahlung an den Lieferanten als positiver Betrag. Da bezahlt wurde und Sie im Eingangsrechnungsmodul sind, passend zu 1.) bzw. zum Eintrag in obigem Bild mit 3.8.
3. Verbuchung der Rückzahlung vom Lieferanten als negativer Betrag. Da Sie Geld auf eine Eingangsrechnung zurückerhalten haben, passend zu 2.) bzw. zu obigem Bild mit 5.8.

#### Können Kommentare zur Buchung erfasst werden?
Für Dokumentationszwecke, um die Buchungen auch zu einem späteren Zeitpunkt noch nachvollziehen zu können, gibt es die Möglichkeit einen Kommentar zur Zahlung und der Splittbuchung zu erfassen.
Dieser Kommentar wird in den Modulen der Eingangsrechnung und Ausgangsrechnung und auch in der Integrierten Finanzbuchhaltung von **Kieselstein ERP** angezeigt.
Sie können den Kommentar durch einen Klick auf den Textblock erfassen, wenn Sie im Bearbeiten der Buchungszeile sind. Sobald hier ein Eintrag besteht wird das Icon grün gefärbt ![](../Rechnung/Kommentar_gruen.JPG).
In der Liste der Zahlungseinträge und der Finanzbuchhaltung wird in der Spalte Kommentar ein Haken gesetzt, wenn ein Kommentar zu der Zeile besteht ![](../Rechnung/kommentar_hakerl.JPG).
Wenn Sie nun die Maus über die Buchung bewegen, wird der Eintrag angezeigt.

#### Muss ich jede Eingangsrechnung erfassen?
Ja selbstverständlich. Oft kommt die Frage in dem Zusammenhang, dass der Ablauf des Wareneingangs nicht wirklich sauber funktioniert.<br>
Dazu unser dringender Rat, sorgen Sie dafür, dass der Wareneingang so verlässlich wie möglich gebucht wird, dass er so zeitnah wie möglich gebucht wird und trennen Sie bei den Zuständigkeiten klar zwischen Einkauf und Buchhaltung. Der Einkauf ist für die Vereinbarung der Konditionen (Preise, Lieferzeiten, Zahlungsziele) mit den Lieferanten zuständig (und nicht die Buchhaltung). Der Einkauf ist auch für die sehr zeitnahe und richtige Verbuchung des Wareneingangs verantwortlich.<br>
Da es gerade bei strukturellen Problemen aber immer wieder dazu kommt, dass eine Eingangsrechnung nicht wirklich verbucht werden kann, man aber in jedem Falle einen Überblick über ALLE Eingangsrechnungen braucht, auch über die unklaren, erfassen Sie die Eingangsrechnung und kontieren Sie diese nicht. Damit wird diese NICHT in die Finanzbuchhaltung übernommen und wird in der Übersicht der Eingangsrechnungen in rot angezeigt. Auch der Ausdruck aller Eingangsrechnungen (Journal) zeigt die (noch) nicht kontierten Eingangsrechnungen in rot an.
Vermeiden Sie den Aufbau von parallelen Liste, das hat bisher nur immer zu noch mehr Chaos geführt. Sorgen Sie dafür, dass die vorgelagerten Prozesse ordentlich funktionieren.

Nutzen Sie aber auch die Chance, Ihren Einkauf in Kombination mit Ihren Lieferanten zu hinterfragen. D.h. der Einkauf vereinbart Einkaufspreise. Diese sind die Basis für Ihre Kalkulationen und somit auch für Ihre Angebote. Liefert nun der Lieferant nicht zu den vereinbarten Konditionen ist zu klären was die Ursache dafür ist. Hält sich der Lieferant nicht an die vereinbarten Bedingungen oder hat der Einkauf diese nicht sauber und klar definiert?<br>
Dazu noch ein wichtiger Tipp: Fordern Sie von Ihren Lieferanten die Erfüllung Ihrer Einkaufsbedingungen liebevoll, menschlich aber konsequent ein. Nur weil ein Lieferant sein Rechnungsschreibeprogramm nicht im Griff hat, ist das noch lange kein Grund sich nicht an gesetzliche oder vertragliche Vorgaben zu halten. Sollte ein Lieferant nicht in der Lage oder nicht Willens sein, Ihre angemessenen Bedingungen zu erfüllen und da gehört das Rechnungslegungsgesetz dazu, suchen Sie raschest möglich einen anderen Lieferanten, dem diese Vorgehensweise transparent ist. Vermeiden Sie unbedingt Single Source Lieferanten.

##### Wie funktioniert nun das Hinterfragen des Einkaufs?
- Der Einkauf sendet die Bestellungen mit Preise an die Lieferanten.
- Die Buchhaltung bekommt am Schluss die Eingangsrechnung vom Lieferanten. Wenn alles sauber läuft, d.h. für ca. 95% der Fälle, braucht nur die Bestellung übernommen werden. D.h. wird ganz vorne im Prozess die selbstverständliche Sorgfalt angewandt, haben alle an diesem Warenbeschaffungsprozess Beteiligten nur Vorteile.<br>
Bedenken Sie: Wenn Ihre Einkaufspreise nicht verbindlich sind, stimmen Ihre Angebote nicht, damit verlieren Sie Kunden, entweder weil Sie zu teuer sind oder weil Sie die Leistungen nicht zu den Preisen erbringen können. Siehe dazu auch, [Auftragsnachkalkulation]( {{<relref "/verkauf/auftrag/nachkalkulation" >}} ).

#### Was bedeutet der Reiter Reisezeiten?
In diesem Reiter werden die für diese Eingangsrechnung zugeordneten Reisezeiten angezeigt. [Siehe dazu bitte]( {{<relref "/fertigung/zeiterfassung/reisezeiten/#wie-k%c3%b6nnen-weitere-spesen-zugeordnet-werden" >}} )

#### Können Eingangsrechnungen auch mittels OCR Scan erfasst werden?
Diese Vorgehensweise ist in sehr vielen DMS Systemen üblich, hat aber den massiven Nachteil, dass die mit der Warenwirtschaft verbundenen Detailinformationen oft nur sehr mangelhaft und in den meisten Fällen überhaupt nicht erfasst werden. Daher wird dieses Verfahren von **Kieselstein ERP** nicht unterstützt. Immerhin besteht an die Materialwirtschaft oder gerne auch Warenwirtschaft die Forderung, dass der im **Kieselstein ERP** erfasste Lagerstand zu jedem Zeitpunkt mit dem physikalischen Lagerstand übereinstimmt.
Siehe dazu bitte das oben gesagte, muss ich jede Eingangsrechnung erfassen.

#### Können Eingangsrechnungen auch mittels Barcodescanner erfassen werden?
Ja. Dieses in der Schweiz und in Liechtenstein gerne verwendete Verfahren nutzt die auf den inländischen (aus Sicht der Schweiz / Liechtenstein) Rechnungen im QR-Code aufgedruckten Zahlungsdaten. Schließen Sie dazu einen geeigneten QR-Code Scanner mit Wedge-Interface an Ihren Rechner an. Bitte beachten Sie, dass diese Scanner üblicherweise eine spezielle Einstellung benötigen um auch die Zeilenumbrüche entsprechend übersetzen zu können.
Damit diese Funktion zur Verfügung steht, stellen Sie bitte den Mandantenparameter EINGANGSRECHNUNG_AUS_QR_CODE auf 1.
Der Typ des QR-Codes muss SPC sein, d.h. es werden derzeit nur Schweizer QR-Codes unterstützt.
Anhand des enthaltenen IBANS wird der entsprechende Lieferant gesucht und einen neue ER mit den vorhandenen Vorschlagswerten erstellt.
Details zum Aufbau des QR-Codes [siehe bitte](https://www.paymentstandards.ch/dam/downloads/style-guide-qr-rechnung.zip).
Um nun neue Eingangsrechnung(en) zu erfassen klicken Sie auf ![](ER_mit_QR_Code_erfassen.gif) den Barcodescanner.
Damit wird anhand der IBAN der entsprechende Lieferant gesucht. Wenn er eindeutig gefunden wird, also die IBAN nur bei exakt einem Lieferanten hinterlegt ist, so wird dieser Lieferant verwendet und es muss nur mehr das Eingangsrechnungsdatum erfasst werden. Wird kein Lieferant gefunden, so muss er aus den bestehenden Lieferanten ausgewählt, bzw. neu angelegt werden. Werden mehrere Lieferanten gefunden, so muss aus diesen ausgewählt werden.

Als Barcodescanner können übliche QR-Code Scanner mit Tastatur Schnittstelle verwendet werden. 
[Siehe dazu auch]( {{<relref "/docs/installation/11_barcode/barcodescanner_konfiguration/#usb-wedge" >}} )

So kann auch das für die Schweiz / Liechtenstein übliche PayEye von Crealogix verwendet werden. [Download siehe bitte](https://files.crealogix.com/dpay/downloads/PayEye_1.2.2.exe).
Dieses muss auf
![](Anbindung_PayEye.jpg)
Komplette Kodierzeile/Enter eingestellt werden.

#### können auch ESR Belege erfasst werden?
Ja, auch wenn dieses Verfahren bereits abgekündigt ist, haben wir dieses Einlesen des OCR-B Datenstroms unterstützt.
Der Aufbau der ESR OCR-B Daten ist wie folgt:
![](ESR_Codierzeile.jpg)
3 ... ESR Teilnehmernummer des Lieferanten bei einer Schweizer / Liechtensteinischen Bank -> Siehe Lieferant, Bankverbindung, Konto Nummer
1 ... ESR-ID Ihres Lieferanten bei der Bank (3) 6stellige Nummer-> Siehe Lieferant, Bankverbindung, ESR-ID
2 ... Referenznummer wird in der Lieferantenrechnungsnummer bzw. im ER-Text eingetragen
4 ... Ab der 3.Stelle der Betrag in Rappen bzw. Eurocent
4 ... die ersten beiden Stellen definieren die Währung. Es werden aktuell nur 01 ... CHF, 21 ... EUR akzeptiert (siehe ESR+)
Um den OCR Datenstrom einzulesen hat sich oben beschriebener Scanner (PayEye) von Crealogix bewährt.

Nach dem Scann wird versucht einen passenden Lieferanten zu ermitteln. D.h. es wird zuerst die Bank ermittelt (3) und danach nach der eindeutigen Kontonummer(1) bei dieser Bank gesucht. Wird exakt ein Lieferant gefunden, wird dieser gemeinsam mit dem Betrag(4) und der Referenznummer(2) in die Erfassung der neuen Eingangsrechnung übernommen. Wird keine Zuordnung gefunden, so erscheinen entsprechende Fehlermeldungen wie nachfolgend dargestellt.
Hinweis: Derzeit wird die Prüfziffer nicht ausgewertet. Wir gehen von einer optischen Überprüfung der eingescannten Beträge auf Richtigkeit und Glaubwürdigkeit aus.
![](ESR_Teilnehmernummer_fehlt.jpg)
D.h. es muss eine Bank mit der ESR-Teilnehmernummer, bitte inkl. der führenden 0, angelegt werden. Bitte definieren Sie unbedingt auch eine gültige BIC/SWIFT.

![](ESR_Kontozuordnung_fehlt.jpg)
Hinterlegen Sie bei Ihrem Lieferanten in der Bankverbindung die entsprechende Bank und Konto Kombination
![](Kontozuordnung.gif)

Um auch den Zahlungsvorschlag, also den SEPA Export des Zahlungsvorschlages nutzen zu können, muss bei der Bank der BIC/SWIFT und beim Konto die IBAN angegeben werden.

#### wird auch der ESR+ ohne Betrag unterstützt?
Es wird auch der mit 042> beginnende Code unterstützt. Hier fehlt allerdings der Betrag im ESR Code.
Die Codezeile sieht z.B. wie folgt aus:
![](ESR_042_Codierzeile.jpg)
Hier wird nur die Referenznummer (also der Bereich nach dem ersten > bis eine Stelle vor dem +) und das Konto (der Bereich nach dem +<Leerstelle> bis zum >) verwendet.
Das Konto muss beim Lieferanten, Bankverbindung unter Kontonr eingetragen werden. Verwenden Sie dazu z.B. die Bank Postfinance.
Da in dem OCR Datenstrom kein Betrag enthalten ist, muss nach der Erfassung der Bruttobetrag der Eingangsrechnung eingegeben werden.

#### Wo finde ich Monatsumsätze?
Nutzen Sie dafür bitte das Journal aller Eingangsrechnungen. Hier wird am Ende der Eingangsrechnungsumsatz je Monat angedruckt.

#### Wie mit Vorauskasse Eingangsrechnungen umgehen?
Wenn Sie eine Eingangsrechnung per Vorauskasse bezahlen müssen, so bieten sich hier zwei Vorgehensweisen an:
1. Die aus buchhalterischer Sicht richtige.
Erfassen Sie den Beleg der Vorauskassenrechnung als Anzahlungsrechnung. Denn genau genommen leihen Sie Ihrem Lieferanten nur Geld, damit er für Sie die Leistung erbringt. Erst wenn die Ware / Dienstleistung bei Ihnen zur vollständigen Zufriedenheit erfüllt ist, hat er seine Leistung erbracht. D.h. zu diesem Zeitpunkt ist dann eine entsprechende Schlussrechnung zu stellen.
Sollten Sie von Ihrem Lieferanten keine Schlussrechnung bekommen, müssten Sie die Vorauskassenrechnung kopieren und auch als Schlussrechnung erfassen.
Warum so kompliziert?
Weil, gerade über Bilanzstichtage hinweg, dies keinen Aufwand darstellt, sondern ein Kredit an Ihren Lieferanten (darum heißt er Kreditor).

2. die oft pragmatischere Lösung
Erfassen Sie die Vorauskassenrechnung als Eingangsrechnung und entfernen Sie beim Verbuchen der Zahlung den Haken bei (vollständig) Erledigt. Damit ist diese Eingangsrechnung noch mit 0,- offen und Sie haben eine einfache Möglichkeit der Erinnerung, dass der Wareneingang noch kommen muss.
Wichtig: Sollte sich die Vorauskasse und die tatsächliche Lieferung über Ihren Bilanzstichtag hinweg ziehen, sollten Sie unbedingt obige Variante verwenden. Ihre Bilanz würde sonst falsch sein.

[Siehe dazu auch.](#Vorauszahlungen)

#### Können die Belege der Eingangsrechnungen exportiert werden?
Neben der Funktionalität, dass beim Export der Eingangsrechnungen auch die Belege, also die gescannten PDFs, mit exportiert werden, steht auch der direkte Export zur Verfügung. D.h. Sie finden im Modul Eingangsrechnung den Punkt, Export Dokumente.
![](ER-Export1.gif)
Wählen Sie hier den gewünschten Zeitraum und den Pfad (Ordner) in den die Dateien exportiert werden sollten. Für diese Funktion ist das Recht LP_FINANCIAL_INFO_TYP_1 erforderlich.
Für das angegebene Zielverzeichnis muss der Benutzer ein entsprechendes Schreibrecht besitzen. Der gewählte Pfad wird beim nächsten Export wieder vorgeschlagen.

Der Aufbau der Dateinamen erfolgt nach folgender Logik:
1. Dateinamen bilden sich aus <Lieferantenname>_<Lieferantenrechnungsnr>_<Belegdatum>.pdf
1a. Belegdatum in Form JJJJ-MM-TT
2. Existiert die Datei bereits wird über einen Dialog nachgefragt, ob diese Datei überschrieben werden soll. Man kann mit "Ja", "Ja, alle überschreiben" und "Nein" antworten.
3. Sind mehrere PDF bei einer ER abgelegt wird an Dateinamen vor der Extension ein "_<COUNT>" angehängt, also *_1.pdf, *_2.pdf etc.
4. Wurde der Export erfolgreich durchgeführt, erscheint eine entsprechende Meldung.
5. Im Fehlerfalle erscheint eine Fehlermeldung und es wird der begonnene Export abgebrochen. Alle bis dahin exportierten Dokumente bleiben natürlich bestehen.

<a name="XLS Import"></a>

#### Können Eingangsrechnungen importiert werden?
Unter dem Menüpunkt, Eingangsrechnung, Import, XLS-Import steht ein entsprechender Import zur Verfügung.
![](ER_Import.jpg)
Dieser Import wird gerne für Massendatenübernahme z.B. aus einer automatischen Belegerkennung verwendet.
Es werden hier folgende Felder übernommen:
- Kreditorennummer, Lieferantenname, Datum, Rechnungsnummer des Lieferanten, Betrag in Lieferantenwährung
- Leerzeilen werden ignoriert
- Der Lieferant muss vorhanden sein. Er wird über die Kreditorennummer gesucht. Diese muss vorhanden und eindeutig sein, ansonsten wird die Zeile nicht importiert.
- Falls die Lieferantenrechnungsnummer bereits in einer oder mehrerer ER existiert, wird die Zeile nicht importiert.
- Währung, Mwstsatz, Kostenstelle, IG-Erwerb und Reversechargeart wird aus dem Lieferanten übernommen.
- Sind beim Lieferanten, im Reiter Konditionen Warenkonto und Kostenstelle definiert, so wird die Eingangsrechnung auf dieses Aufwandskonto kontiert. Ist kein Warenkonto bzw. keine Kostenstelle gegeben, so wird die Eingangsrechnung auf mehrfachkontiert gesetzt und muss somit manuell kontiert werden.

Dies wird gerne in Verbindung mit sogenannten Monatsrechnungen verwendet, also der [nachträglichen Zuordnung der Wareneingänge zur Eingangsrechnung](#Wareneingänge über die Eingangsrechnung zuordnen).
Für eine Musterdatei siehe deine **Kieselstein ERP** Installation Eingangsrechnungen_Import.xls

<a name="Prüfung und Freigabe"></a>

#### Können Eingangsrechnungen freigegeben werden?
Weiters können Eingangsrechnungen, z.B. nach Prüfung, freigegeben werden.
Aktivieren Sie dafür den Parameter EINGANGSRECHNUNG_PRUEFEN.
Damit dürfen die Benutzer mit dem Recht ER_DARF_EINGANGSRECHNUNGEN_PRUEFEN Eingangsrechnungen auch freigeben.
Der Grundgedanke hinter der Freigabe ist, dass einerseits sofort alle Eingangsrechnungen zu erfassen sind und erst, nach einer unter Umständen umfangreichen Prüfung (Technik, Vertragliches, ..) diese auch tatsächlich so akzeptiert wird, oder eben nicht. D.h. mit der Erfassung Ohne Freigabe ist die Eingangsrechnung trotzdem als ER erfasst und bei integierter Finanzbuchhaltung somit auch entsprechend kontiert, verbucht und somit in allen Auswertungen enthalten.
Zusätzlich ist eine nicht geprüfte und somit nicht freigegebene Eingangsrechnung zwar im Zahlungsvorschlag enthalten, ist aber per default als nicht zur Zahlung freigegeben gekennzeichnet (nicht angehakt). D.h. um eine noch nicht geprüfte Eingangsrechnung trotzdem über den Zahlungsvorschlag zu bezahlen, muss diese bewusst zur Zahlung angehakt werden.

Umgekehrt kann eine geprüft Eingangsrechnung nur geändert werden, wenn man das Recht ER_DARF_EINGANGSRECHNUNGEN_PRUEFEN besitzt und per Klick auf Ändern und der dazugeghörigen Frage die Prüfung zurücknimmt. Über das Bearbeiten-Menü kann bei einer geprüften / freigegebenen Eingangsrechnung nur das Freigabedatum und die Mahnstufe geändert werden. Die sonstigen Daten der Kopfdaten der ER können nicht verändert werden. Die Funktionen der weiteren Reiter stehen nach den üblichen Rechten zur Verfügung.

Um eine Eingangsrechnung freizugeben, wechseln Sie in die Kopfdaten und geben mit dem Hakerl ![](ER_geprueft.gif) die Eingangsrechnung als geprüft frei.

#### Welche Rechte werden für die Eingangsrechnung benötigt?
In Zusammenhang mit der Eingangsrechnung wirken folgende Rechte

| Recht | Beschreibung / Wirkung |
| --- |  --- |
| ER_EINGANGSRECHNUNG_R | Lesen der Eingangsrechnungen |
| ER_EINGANGSRECHNUNG_CUD | Schreiben in der Eingangsrechnung |
| ER_DARF_KONTIEREN | Darf eine Eingangsrechnung kontieren. Damit erreichen Sie, dass z.B. der Einkauf zwar die Eingangsrechnung erfassen darf. Die Buchhaltungstechnisch erforderliche Kontierung muss aber von jemand anderem, z.B. der Buchhaltung durchgeführt werden. |
| ER_DARF_EINGANGSRECHNUNGEN_PRUEFEN | Darf Eingangsrechnungen prüfen. Wenn der Parameter EINGANGSRECHNUNG_PRUEFEN aktiviert ist, so haben die Eingangsrechnungen einen eigenen Prüfstatus. D.h. es wird damit von Anwendern die dieses Recht besitzen hinterlegt, ob die Eingangsrechnung bereits freigegeben ist. Damit können Sie z.B. die Eingangsrechnung zwar erfassen und verbuchen, aber z.B. die technische Freigabe erfolgt eben erst nach einer ausführlichen Prüfung. |
| ER_DARF_ZAHLUNGEN_ERFASSEN | Diese Benutzer dürfen für vollständig kontierte Eingangsrechnungen, welche gegebenenfalls auch freigegeben sind, die Zahlungen erfassen. |
| LP_FINANCIAL_INFO_TYP_1 | Export der Eingangsrechnungen |
| BES_BESTELLUNG_RBES_WARENEINGANG_CUD | Für die Überleitung / Erzeugung der Eingangsrechnungen aus dem Wareneingang der Bestellungen |
| DOKUMENTE_SICHERHEITSSTUFE_0_CU ...DOKUMENTE_SICHERHEITSSTUFE_3_CU | Je nach Ihrer Unternehmensdokumentation. Für die Ablage der eingescannten / übermittelten Eingangsrechnungen in der Dokumentenablage. |

#### Wo sieht man ob eine Eingangsrechnung Geprüft / Freigegeben ist?
In der Auswahlliste der Eingangsrechnungen gibt es wenn die Eingangsrechnungen zu prüfen sind, eine Spalte G(eprüft). Hier sind die geprüften und somit freigegebenen Eingangsrechnungen entsprechend angehakt.

#### Kann für die Zahlung einer Eingangsrechnung auch eine andere Bankverbindung angegeben werden?
Sie finden in den Kopfdaten

Die abweichende Bankverbindung (AbwB) in der ER wird nun berücksichtigt in:

1. UI: Fibu, Zahlungsvorschlag, Reiter "2 Offene Posten"
Ist eine AbwB definiert werden die Daten dazu (IBAN, Bank, BIC) im FLR entsprechend angezeigt und nicht vom Lieferanten. Ein Tooltip weist darauf hin.
Im Detail-Panel werden ebenso die Daten der AbwB angezeigt, dazu erscheint darüber ein Label mit dem Namen des jeweiligen Personals, z.B. "Abweichende Bankverbindung (Agnes Bärtsch)

2. Zahlungsvorschlag-Export
Jeweils im CSV-Export sowie im SEPA XML-Export wird die AbwB anstatt der des Lieferanten verwendet. Zusätzlich kommen hier auch die Empfängerdaten (Name, Anschrift) aus dem Personal. Diese müssen mit der der IBAN des Zahlungsempfänger zusammenstimmen.

So wie beim Lieferanten wird im Falle einer AbwB geprüft, ob dem Personal auch eine Bankverbindung hinterlegt ist und ggf. beim Export ein entsprechender Fehlertext ausgegeben.

+ Dafür stehen zusätzlich folgende Felder in er_eingangsrechnung_offene2.jrxml zur Verfügung.

F_BANKVERBINDUNG_BANKNAME
F_BANKVERBINDUNG_BIC
F_BANKVERBINDUNG_IBAN
F_BANKVERBINDUNG_PERSON_ABWEICHEND

### Eingangsrechnung mit fremder Umsatzsteuer
Man hat immer wieder die Aufgabe, dass man von Eingangsrechnungen aus dem EU-Ausland, die enthaltene Vorsteuer zurückfordern will.<br>
Hier gilt aktuell die Regel, wenn der rückzufordernde Betrag 50,- € nicht übersteigt, wird dies vom ausländischen Finanzamt nicht behandelt.<br>
Meine persönliche Erfahrung: Jeder Staat wehrt sich diese Steuerbeträge rückzuerstatten. Daher muss dies sehr genau gemacht werden. Das gilt auch für die eingereichten Beträge. Sollte der Firmenwortlaut nicht auf jede Kleinigkeit stimmen, so brauchst du dies gar nicht einzureichen (man kanns natürlich trotzdem probieren).

Wie findet man nun diese Eingangsrechnungen heraus?

Das Thema ist sicherlich, um wieviele Rechnungen handelt es sich. Sind es wenige so kann man diese anhand der Eingangsrechnungsverwaltung, Sortierung nach Ort, Direktfilter auf das gewünschte Jahr (23/%) in der ER Nummer und durchsehen der abgelegten Dokumente ausreichend rasch herausfinden. ![](ER_mit_fremder_VST.png)

Ist jedoch damit zu rechnen, dass dies eine doch interessante Anzahl ist, so will man meist auch wissen, wie hoch den der erwartete Rückerstattungsbetrag ist. Daher könnte man dies wie folgt erfassen:
- Den Netto-Betrag auf das direkte Aufwandskonto z.B. Materialeinkauf mit fremder UST
- den Vorsteuerbetrag auf ein weiteres Aufwandskonto, z.B. fremde Ust zu Einkauf
Damit hat man die Vorsteuerbeträge auf diesem Konto.

Meiner Meinung nach, welche auch meiner Erfahrung geschuldet ist, ist diese fremde Vorsteuer jedenfalls auch Aufwand. Erst wenn ich tatsächlich, was nicht sicher ist, die fremde Vorsteuer zurückbekommen habe, sind das zusätzliche Erlöse und erst wenn der Zahlungseingang gegeben ist, wird das auf ein passendes Konto gebucht.

Eventuell sollte man das auch nach seinen Hauptländern und all den anderen Ländern gliedern.

### Eingangsrechnung mit Skonto kann nicht vollständig bezahlt werden
Bezahlt man eine Eingangsrechnung mit Skonto und kommt dann folgende Fehlermeldung<br>
![](ER_kann_nicht_bezahlt_werden_01.png)<br>
so gilt es zuerst herauszufinden, welche Art von Eingangsrechnung das ist. Wie z.B.:
- Inland, IG-Erwerb mit oder ohne Reversecharge
- Wie ist der Kreditor definiert, z.B. Inland, oder AuslandEUmUID
- Wie ist die Steuerkategoriezuordnung für diese Definition?
  - Finanzamt
    - Reverse Charge (z.B. ohne)
      - Steuerkategorie (z.B. AuslandEUmUID)
        - Ist der Skontoerlös für die entsprechende Mehrwertsteuerkategorie definiert (hier Steuerfrei)
