---
categories: ["Eingangsrechnung"]
tags: ["eRechnung"]
title: "eRechnung"
linkTitle: "eRechnung"
date: 2023-01-31
weight: 150
description: >
  Die eRechnung in den Eingangsrechnungen
---
eRechnung in den Eingangsrechnungen
============

Ab der Version 1.0.7 steht im **Kieselstein ERP** auch der Import über die sogenannte eRechnung zur Verfügung. Für das Thema eRechnung bei Ausgangsrechnungen [siehe]( {{<relref "/verkauf/rechnung/elektronische_rechnung/erechnung/" >}} )

Die Lösung wurde so implementiert, dass man sich damit, wenn denn deine Lieferanten mitspielen, einiges an Tipparbeit ersparen kann.

{{% alert title="ACHTUNG: Aufbewahrungspflicht auf der Datei" color="primary" %}}
Die Aufbewahrungspflicht hängt am Rechnungsoriginal. Somit ist die Vorsteuerabzugsberechtigung mit der im Original in elektronischer Form (eRechnung) erhaltenen Datei unmittelbar verknüpft. [Siehe dazu](#aufbewahrungspflicht)
{{% /alert %}}

## Zuordnen der Datei
Wenn du, so wie bisher, mit Neu eine neue Eingangsrechnung anlegst, erscheint am unteren Rand der Kopfdaten ein Drag&Drop Feld.<br>
![](eRechnung_Drag_Drop.png)<br>
Das bedeutet, ziehe hier einfach deine erhaltene Eingangsrechnung auf dieses Feld. Ist diese in einem der unterstützten Formate, so werden damit die Eingangsrechnungsdaten entsprechend vorbesetzt.

Diese Daten sind:
- IBAN des Lieferanten -> daraus wird der Lieferant erkannt und mit seinen Daten vorbesetzt<br>
  Diese sind im wesentlichen:
  - Vollständige Adresse des Lieferanten
  - Kostenstelle
  - Kontierung
  - Mehrwertsteuersatz<br>
**Hinweis:** Stimmt der im XML angegebene MwSt Prozentsatz nicht mit dem bei deinem Lieferanten definierten Prozentsatz überein, oder gibt es mehrere Mehrwertsteuersätze, so wird die Eingangsrechnung auf Mehrfach- / Splittbuchung gesetzt.<br>
**Hinweis:** Reversecharge wird nicht unterstützt. D.h. bitte selber nach den Regeln einstellen.
- Eingangsrechnungsdatum
- Eingangsrechnungs Nummer -> Beachte den Parameter EINGANGSRECHNUNG_LIEFERANTENRECHNUNGSNUMMER_LAENGE, dieser steht idealerweise auf -1
- Eingangsrechnungs Nettobetrag<br>
Der Bruttobetrag wird errechnet.
 
### Unterstützte Formate:
Es werden beim Einlesen folgende Formate unterstützt
- xRechnung Version
- x-factur
- eRechnung
- ZUGfERD

### Wird der Inhalt der XML Datei angezeigt
Die Anzeige des Inhaltes der XML Datei wird über einen von der deutschen Regierung zur Verfügung gestellten Konverter als PDF im Drag & Drop Feld unten angezeigt. Dieses PDF wird beim Speichern der Eingangsrechnung gemeinsam mit dem Original-File auch in der Dokumentenablage gespeichert.<br>
Weiters kannst du diese PDF Vorschau über den Druckbutton des PDF Viewers ![](PDF-Viewer.png)  ausdrucken. Bitte beachte, dass dieses PDF immer eine generische Darstellung der erhaltenen Daten ist und darum normalerweise nicht der Corporate Identity deines Lieferanten entspricht.

Solltest du die Eingangsrechnung ausdrucken wollen, so verwende dafür<br>![](Eingangsrechnung_drucken.png)<br>
Eingangsrechnung Dokumente drucken. Damit werden alle PDF Dateien die bei dieser Eingangsrechnung hinterlegt sind ausgedruckt, angereichert um die Daten die du für die Eingangsrechnung erfasst hast.

{{% alert title="Bitte beachte" color="primary" %}}
Du bist selbst für die rechtlich richtige Behandlung der Eingangsrechnung verantwortlich. Wir lehnen jegliche Verantwortung dazu ab.
{{% /alert %}}

### Kann jedes PDF importiert werden?
<u>**NEIN**</u><br>
Das Wesen einer eRechnung ist, dass **<u>strukturierte maschinenlesbare Daten</u>** übermittelt werden. Das bedeutet es sind dies sogenannte XML Dateien. Diese haben die Eigenschaft, dass Sie für Menschen zwar lesbar sind, aber man doch ein entsprechendes Wissen benötigt um diese Inhalte auslesen zu können. Daher wird der Inhalt der XML Datei beim Einlesen interpretiert und in einer lesbaren Form dargestellt.<br>
Ein Sonderfall sind Zugferd Dateien. Diese Dateien sind eine Kombination aus Menschen lesbaren PDF Daten, welche zusätzlich um die maschinenlesbaren XML Daten angereichert wurden. D.h. beim Einlesen (Drag & Drop) wird in der PDF Datei nach diesen XML Daten gesucht. Werden diese gefunden, wird dies interpretiert und in den Kopf der Eingangsrechnung übernommen. Werden diese Daten nicht gefunden bzw. können diese nicht interpretiert werden, wird nur<br> ![](eRechnung_kann_nicht_gelesen_werden.png)<br> angezeigt.

{{% alert title="WICHTIG" color="primary" %}}
Ein PDF, welches z.B. durch einscannen erzeugt wurde, kann NIE als eRechnung eingelesen werden, da dies nur Bildinformationen beinhaltet, also die Maschinen lesbaren Informationen fehlen.<br>
Das bedeutet auch, dass die eRechnung OHNE den handschriftlichen Buchungsvermerken im **Original eingelesen werden muss** und wenn man die handschriftlichen Notizen auch in der Dokumentenablage angeben will, dies als zusätzliches Dokument abgespeichert wird.
{{% /alert %}}

### Aufbewahrungspflicht
Mit der Einführung der elektronischen / signierten Rechnung, ist bereits die Tatsache entstanden, dass die Datei das **ORIGINAL** ist. Laut BAO132 (AT) bzw. GOBD (DE) liegt damit die Aufbewahrungspflicht in der Original-Datei. D.h. die viel gepflegte Praxis<br>
Erhalt der Rechnung -> Ausdrucken -> handschriftliche Kontierung -> Verbuchen bzw. dem Steuerberater bringen<br>
führt zu einem **Verlust der Vorsteuerabzugsberechtigung**.

![](Vorsteuerabzugsberechtigung.png)

Sind aufzeichnungs- und aufbewahrungspflichtige elektronische Unterlagen im Unternehmen eingegangen, sind diese auch **in dieser Form aufzubewahren** [...]. Sie dürfen **nicht mehr ausschließlich** in ausgedruckter Form aufbewahrt werden. ([GoDB Rz. 119](https://ao.bundesfinanzministerium.de/ao/2021/Anhaenge/BMF-Schreiben-und-gleichlautende-Laendererlasse/Anhang-64/anhang-64.html) )

**Wichtig:** Solltest du ein nicht einlesbares Format der eRechnung von deinem Lieferanten bekommen, so kannst du diese nicht Gesetzes konform verarbeiten. D.h. dein Lieferant muss die Rechnung in einem anderen Format senden. Gegebenenfalls kann auch die Importfunktion um dieses Format erweitert werden.

Das bedeutet auch, gegebenenfalls muss du:
- das Original in der Dokumentenablage deines Kieselstein ERP abspeichern
- dieses ausdrucken und um deine unbedingt notwendigen handschriftlichen Notizen ergänzen
- auch dieses Papier einscannen und als zusätzliches Dokument bei der Eingangsrechnung hinterlegen.

<u>Mein Rat:</u> Vermeide die handschriftlichen Notizen, nutze für die Auftrags- / Los-Zuordnung das Bestellmodul, bzw. die Texteingabemöglichkeiten in der Eingangsrechnungserfassung. Damit ist dein gesamter Buchungs- und Rechnungsprüfungsprozess wesentlich schlanker.

**Wichtig:** Die Aufbewahrungspflicht liegt damit auf deinen Dateien. D.h. du musst auch 10 Jahre (oder länger) nachdem du die letzte Eingangsrechnung (nur) in deinem ERP gespeichert hast, auf diese Originaldateien ordnungsgemäß zugreifen können. [Siehe Datensicherung]( {{<relref "/docs/installation/01_server/datensicherung/" >}} )

<u>Anmerkung:</u><br>
Diese Regelung gilt im deutschsprachigen Raum vorerst (ab 2025) nur für die Bundesrepublik Deutschland. Ähnliche Regelungen gibt es bereits in Italien, Polen. Es ist damit zu rechnen, dass dies sehr bald für alle EU-Staaten zur Eindämmung der Betrügereien eingeführt wird.

### Übernahme der eRechnung bei ER aus Bestellung
Die Basis der Lieferantenerkennung ist immer sein IBAN, egal ob der eRechnungsimport aus dem Neu oder dem Wareneingang angestoßen wird.

Auch wenn die neue Eingangsrechnung aus dem Wareneingang der Bestellung angestoßen wurde, so kann die eRechnung importiert werden. Hier wird geprüft:
- Stimmen die Lieferanten aus der Bestellung (Rechnungsadresse) und der IBAN aus der eRechnung überein. Ist dies nicht der Fall, erscheint eine Fehlermeldung und die Eingangsrechnung sollte verworfen werden.

![](Lieferanten_stimmen_nicht_ueberein.png)<br>
Diese Meldung kommt, wenn sich aus der eRechnung ein anderer Lieferant ergibt als in der Bestellung definiert ist.<br>
Nach dem Klick auf ok, kommt der Verwerfen Dialog. Diesen bitte mit **verwerfen** bestätigen.

Sollte die IBAN wirklich bei diesem Lieferanten zugeordnet sein, so korrigiere bitte die Daten der Bankverbindungen deiner Lieferanten.

**Hinweis:**
Bei der Prüfung der IBAN wird nicht berücksichtigt, ob diese bei einem Kunden oder einem Lieferanten hinterlegt ist. D.h. es kann obige Fehlermeldung erscheinen, wenn die IBAN bei einem Kunden hinterlegt ist und du den Lieferanten neu angelegt hast. In diesem Falle die beiden gleichen Partner zusammenführen.

- danach wird geprüft, ob die errechneten Bruttowerte der Eingangsrechnung übereinstimmen. Ist dies nicht der Fall, erscheint eine entsprechende Fehlermeldung. Es wird in jedem Falle der Wert aus der eRechnung übernommen.<br>
![](Werte_stimmen_nicht_zusammen.png)

{{% alert title="ACHTUNG" color="warning" %}}
Korrigiere in diesem Falle die Wareneingangswerte, damit auch dein Lager stimmt.
{{% /alert %}}

- Beim Speichern der Eingangsrechnung erscheint, wenn die IBAN aus der eRechnung (noch) keinem Lieferanten zugeordnet ist, die Meldung: Soll für den Lieferanten *LIEFERANT* eine neue Bankverbindung: *IBAN* angelegt werden?<br>
![](IBAN_hinterlegen.png)<br>
- Mit Ja, wird die neue bzw. eventuell zusätzliche Bankverbindung dem Lieferanten zugeordnet. Eine Prüfung ob der Lieferant bereits eine Bankverbindung hat erfolgt nicht.
- Mit Nein wird die Eingangsrechnung ebenfalls erfasst, aber keine Zuordnung der IBAN zum Lieferanten durchgeführt.

### Rechnungsnummer kann nicht übertragen werden
Kommt beim Einlesen der ER die Warnung<br>
![](Lieferantennummer_nicht_einlesbar.png)<br>
die Lieferantenrehcnungsnummer konnte nicht vollständig übertragen werden, so prüfe bitte die Einstellung des Parameters EINGANGSRECHNUNG_LIEFERANTENRECHNUNGSNUMMER_LAENGE.<br>
**Hinweis:**<br>
Es macht einen riesen Unterschied ob dieser auf 20 oder auf -1 steht.
- 20 bedeutet eine zwanzigstellige Zahl
- Minus 1 (-1) bedeutet es sind auch Texte zulässig.

### Erkennen von Eingangs-Gutschriften
Wenn eine eRechnung einen negativen Betrag hat, so wird dieser Betrag ebenfalls als negativer Wert in die Eingangsrechnung mit übernommen. Ob du dies nun als Gutschrift (in der Eingangsrechnung) kennzeichnen möchtest, oder dies einfach als negativer Betrag stehen bleibt ist dir überlassen.

Hinweis:<br>
Eine als Gutschrift gekennzeichnete Eingangsrechnung muss gegen eine andere Eingangsrechnung ausgeglichen, als Zahlung, verbucht werden. Hat eine Eingangsrechnung (nur) einen negativen Wert so kann auch eine negative Zahlung (also ein Zahlungseingang) darauf gebucht werden.

### IBAN des Lieferanten
Beim Einlesen der eRechnung wird die erkannte IBAN aus den XML Daten angezeigt. ![](Lieferanten_IBAN.png) Diese wird nur dann angezeigt, wenn eine eRechnung in die Eingangsrechnung eingelesen wird.

### Druck der eingelesenen eRechnung
Wenn eine eingelesene XML Rechnung auf Papier ausdruckt werden soll, so kann dies über den Menüpunkt Eingangsrechnung, Dokumente drucken<br>
![](Eingangsrechnungsdokument_drucken.png)<br>
durchgeführt werden.

### Prüfen der Formate
- https://www.portinvoice.com/ ist eine Weiterleitung der ZUGFeRD Community.

### weitere Infos
- [ZUGfERD](https://www.ferd-net.de/standards/zugferd-faq/index.html) 
