---
categories: ["Eingangsrechnung"]
tags: ["4Vending"]
title: "4Vending"
linkTitle: "4Vending"
date: 2023-01-31
weight: 300
description: >
  Anbindung an vendidata
---
4Vending Schnittstellen
=======================

Mit dieser Zusatzfunktion steht der Import der Ausgangsgutschriften (im Modul Eingangsrechnungen) im 4Vending XML Format zur Verfügung.
Damit haben sie die praktische Funktion, dass damit die Provisionsabrechnungen vom 4Vending direkt übernommen werden und nur mehr ausgedruckt werden müssen.

Zum Import öffnen Sie das Modul Eingangsrechnung und wählen im Menü, Eingangsrechnung, Import, 4Vending ![](4Vending1.gif)

Wichtig: Damit dieser Import die Ausgangs-Provisions-Gutschriften für Ihre Kunden erstellen kann, muss der Parameter AUSGANGSGUTSCHRIFT_AN_KUNDE auf 1 gestellt sein.

Grundsätzlich werden dabei die Daten aus JJJJMMDD_hhmmss_nnn_discounts.xml importiert.
Nach der Auswahl der Datei werden die Daten eingelesen und geprüft. Es wird dies im Kopf angezeigt und kann bei entsprechenden Datenmengen einige Minuten dauern.

Die Basis für die Kundenzuordnung sind die jeweils im 4Vending und in **Kieselstein ERP** hinterlegten Debitorennummern.
Wichtig ist auch, es muss für den indirekten Kunden (eigentlich ein versteckter Lieferant) das Wareneingangskonto definiert sein.
Beim Kunden müssen weiters die Kostenstelle und der Mehrwersteuersatz definiert sein.

Sind diese Daten nicht definiert, so erscheint eine entsprechende Fehlermeldung und der Import kann nur abgebrochen werden.
![](4Vending2.jpg)

In diesem Fall korrigieren Sie bitte die jeweils angeführten Fehler und starten danach den Import erneut.
Um die Daten effizient korrigieren zu können empfiehlt sich einen zweiten **Kieselstein ERP** Client zu starten. So können Sie bequem zwischen den Modulen hin und her wechseln.
Im zweiten Modul starten Sie die Kundenverwaltung und wählen entweder die Debitorennummer oder den Namen des Kunden.
Nun wechseln Sie in den Reiter Konditionen
![](4Vending3.jpg)
und klicken bitte auf den GoTo Button neben Lieferanten-Konditionen.
Hinweis: Ist dieser Button (Lieferanten-Konditionen) nicht vorhanden, so muss die Verbindung zwischen Kunden und (versteckten) Lieferanten erst hergestellt werden. Dafür gehen Sie bitte wie folgt vor:
a.) Es sollte nur ein versteckter Lieferant angelegt werden.
Starten Sie die Eingangsrechnungsverwaltung, Klicken Sie auf Neu. In den nun erscheinenden Kopfdaten haken Sie mit Positionen an. Nun wechselt der Lieferantenauswahlknopf auf Kunden. Klicken Sie auf diesen und wählen Sie den gewünschten Kunden aus. Dadurch ist bereits der versteckte Lieferant angelegt. Wechseln Sie nun mit dem GoTo neben dem Kundenauswahlknopf in den Kunden, auf den Reiter Konditionen und klicken weiter auf die Lieferanten-Konditionen. Geben Sie nun bitte die entsprechenden Daten ein.
![](4Vending4.jpg)

b.) Der Lieferant kann durchaus auch im Lieferantenstamm angezeigt werden.
Wechseln Sie in das Modul Lieferanten, Klicken Sie auf Partner und wählen Sie den gewünschten Kunden aus. Nach dem Speichern wechseln Sie in den Reiter Konditonen und definieren Kostenstelle, Warenkonto und gegebenenfalls den MwSt Satz.

Läuft die Import-Prüfung fehlerfrei durch so erhalten Sie die Meldung keine Fehler gefunden und der "Importieren" Knopf ist freigeschaltet.

![](4Vending4.gif)
Mit dem Klick auf importieren werden die Ausgangsgutschriften entsprechend angelegt. Nach dem Import erscheint die Meldung:

![](4Vending5.gif)
Nun noch ok anklicken und in der Auswahlliste der Eingangsrechnungen auf aktualisieren ![](aktualisieren.gif) klicken.

Wichtig: Definition des Datums der Eingangsrechnungen:
In 4Vending muss beim Export die Periode angegeben werden. Diese wird über die Schnittstelle an **Kieselstein ERP** weitergereicht. Die Eingangsrechnungen = Ausgangsgutschriften werden vom Datum her mit dem Monatsletzten dieser Periode angelegt. Das bedeutet, dass je Monat eine Datenübertragung erfolgen sollte. Bei zeitnaher Datenübernahme ins 4VD und gut gepflegten Daten, kann somit die Provisionsabrechnung im **Kieselstein ERP** sehr zeitnah durchgeführt werden.

Aus dem 4Vending werden folgende Daten übernommen:
In den Kopfdaten wird die Periodeninfo als Text geschrieben, z.B. 2015 Quartal 3
In der Kontierung wird der Automatentyp (AdditionalField->Code3) als Bezeichnung für den Artikel verwendet. Die Formel mit Werten wird ins Feld Kommentar geschrieben.
Die Werte (TotalValue) werden als Netto-Beträge übernommen.
Alle Positionen mit Wert "0" werden nicht übernommen.

Details zu 4Vending siehe bitte dort bzw. <http://www.vendidata.net/>

Article Consumption

Im Modul Fertigung (Los) steht unter Los, Import, 4Vending - Produktverbrauch der Import der aus den Fahrerfahrzeugen entnommenen Artikel zur Verfügung.
![](4Vending-Produkverbrauch1.jpg)
Damit erreichen Sie, dass die Lager der Fahrer (Touren) entlastet werden und somit die Inventur der einzelnen Fahrzeuge theroretisch keine Korrekturen erfordert.
Bitte beachten Sie, dass dies eigentlich die Buchung des Einbringens der Ware in den Automaten ist, welche in der Regel über das EasyData Mobil-Terminal erfasst wird. Diese Daten werden in der Regel täglich in das 4Vending übertragen.
Für die Erfassung müssen die Lagernummern (TourNummern) synchron sein. D.h. die Tournummer muss auch am Ende des Lagernamens in **Kieselstein ERP** zu finden sein.
Aus diesem Grunde ist es auch wichtig, dass eventuelle Außenlager eigene Lagernummern erhalten. Damit erreichen Sie eine Trennung der Verbrauche von den Fahrerfahrzeugen und den Verbrauchen von Ware aus den Außenlagern.
Es ist ja, gerade bei größeren Kundeninstallationen üblich, dass Ihre Lieferanten die Ware, z.B. Kaltgetränke, direkt anliefern und so nur die Außenlager befüllen.
Der Vorteil für Sie ist, dass Sie, durch die Buchung auch dieser Lager Zu- und Abbuchungen eine zusätzliche Prüfung sowohl der Lieferantenlieferungen als auch der Lagerstände erreichen.

Stammdaten nach 4Vending exportieren

Auch Stammdaten können nach 4Vending exportiert werden. Alle Exporte haben gemeinsam, dass sie manuell und automatisch durchgeführt werden können. Werden die Exporte automatisch durchgeführt, so werden im Erfolgs und im Fehlerfalle entsprechende EMails versandt. Es können durch ; getrennt auch mehrere EMail Adressen angeführt werden.
Für die Inbetriebnahme der jeweiligen Exportfunktion empfehlen wir zuerst den manuellen Export durchzuführen. Hier werden entsprechend ausführliche Fehlermeldungen ausgegeben. Funktioniert der gewünschte Export, so kann die Automatik aktiviert werden. Die Einrichtung des automatischen / täglichen Export finden Sie im Modul System, unterer Modulreiter Automatik. Üblicherweise wird diese Funktino dafür verwendet, dass die Stammdaten nur mehr im führenden System, in diesem Falle **Kieselstein ERP** gepflegt werden und dadurch tagesaktuell auch im 4Vending zur Verfügung stehen.

Artikel nach 4Vending exportieren

Im Modul Artikel kann im Menüpunkt Pflege, 4Vending der Artikelstamm nach 4Vending für die definierten 4Vending Artikel exportiert werden.
![](4Vending-Artikelexport1.jpg)

Für die Synchronisierung der Artikel zwischen **Kieselstein ERP** und 4Vending muss in beiden Systemen eine gemeinsame ID verwendet werden. Bei nachträglicher Einführung von **Kieselstein ERP** muss diese 4Vending-ID manuell zum bestehenden 4VD System harmonisiert werden.
Sie finden diese ID im Artikel im Reiter Technik unter  ![](4Vending6.gif) 4Vending. WICHTIG: Die 4Vending-ID ist NUR numerisch.
Für eine Initialisierung vieler Artikel wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

Beim Artikeldaten export werden alle Artikel übertragen, die eine 4Vending-ID haben.
Eine neue 4Vending-ID vergeben Sie in **Kieselstein ERP** durch ... Klick auf 4VD generieren in den Kopfdaten.
Eine bestehende 4VD ID kann durch Klick auf ![](4Vending7.gif) löschen wieder entfernt werden.

Da vom 4Vending verlangt wird, dass für jeden Artikel Einkaufspreise definiert sind, wird beim Export auf diese Daten geprüft. Sollte bei einem Artikel kein Lief1Preis definiert sein, so erscheint ein Hinweis und der Export wird nicht durchgeführt. In diesem Falle ergänzen Sie bitte den Artikellieferanten im Artikel. Das Ziel ist, dass im 4Vending für die Deckungsbeitragsrechnung der Automaten immer gültige Einkaufspreise, genauso wie gültige Verkaufspreise definiert sind.
Info: die 4Vending ID entspricht in der **Kieselstein ERP** Datenbank dem Textfeld C_UL. Damit kann diese ID auch im Artikel XLS Import übernommen werden.

Kunden nach 4Vending exportieren

Bei den Kunden wird davon ausgegangen, dass alle Kunden eindeutig über die Debitorennummer identifiziert werden können. Als zusätzliche Definitionsmöglichkeit wird die Kostenstelle des jeweiligen Kunden verwendet. D.h. jeder Kunde muss einer Kostenstelle zugewiesen sein. Ist bei der Kostenstelle (System, Mandant, Kostenstelle) ![](4Vending8.gif) Profitcenter angehakt, so werden diese Kunden exportiert. Das gemeinsame ist die Debitorennummer. D.h. ist in 4Vending ein Kunde mit einer Debitorennummer bereits angelegt so wird er mit den Daten aus dem **Kieselstein ERP** Kundenstamm ergänzt bzw. überschrieben. Bitte beachten Sie, dass für 4Vending die Debitorennummern nummerisch sein müssen.
Für den manuellen Export siehe bitte Kunde, 4Vending-Kunden Export
![](4Vending9.gif)

Der Status des Kunden im 4Vending wird anhand der Stati im **Kieselstein ERP** Kunden wie folgt abgebildet:
- Versteckt -> wird als blocked übergeben
- Gesperrt -> wird derzeit vom 4Vending nicht ausgewertet

Lieferanten nach 4Vending exportieren

Für die Synchronisierung der Lieferanten wird die Fremdsystemnummer des Lieferanten verwendet. Diese finden Sie im Reiter Konditionen des Lieferanten. Die Fremdsystemnummer entspricht der im 4Vending definierten ID.
Für die Überleitung nach 4Vending gilt, dass die Lieferanten-Kostenstelle als Profitcenter angehakt sein muss. Eine bestehender Lieferant mit der ID des **Kieselstein ERP** Fremdsystemnummer wird mit den aktuellen Daten aus **Kieselstein ERP** überschrieben. Ist ein Lieferant in **Kieselstein ERP** eine Profitcenter-Kostenstelle zugewiesen, hat aber noch keine Fremdsystemnummer eingetragen, so wird er mit einer neuen ID (= Fremdsystemnummer) versehen und im 4Vending neu angelegt. Zusätzlich muss jeder zu exportierende Lieferant eine Kreditorennummer besitzen. Auch diese Nummer muss für 4Vending rein nummerisch sein.
Für den manuelle Export siehe bitte Lieferant, 4Vending-Lieferanten Export
![](4Vending10.gif)

Einrichten der Übertragungsautomatik

Um die Übertragung der Stammdaten täglich automatisiert durchführen zu können definieren Sie im Modul System unterer Modulreiter Automatik ![](4Vending12.gif) den Automatikjob.
![](4Vending11.gif)

Im Reiter Detail definieren Sie welcher der drei Stammdaten und wohin exportiert werden sollte.
![](4Vending13.jpg)
In Linux Systemen achten Sie bitte darauf, dass der Mount entsprechend für den **Kieselstein ERP** Server mit seinen Benutzer-Rechten verfügbar sein muss.
Für Details zur Einstellung der [Automatikjobs siehe bitte dort]( {{<relref "/docs/stammdaten/system/automatik" >}} ).
Die Emails werden über den **Kieselstein ERP** Versanddienst versandt. D.h. dieser muss entsprechend eingerichtet sein. Zusätzlich können mehrere EMailadressen durch ; (Semikolon, Strichpunkt) getrennt angegeben werden.