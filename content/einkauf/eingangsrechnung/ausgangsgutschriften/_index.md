---
categories: ["Eingangsrechnung"]
tags: ["Ausgangsgutschriften"]
title: "Ausgangsgutschriften"
linkTitle: "Ausgangsgutschriften"
date: 2023-01-31
weight: 100
description: >
  Ausgangsgutschriften in der Eingangsrechnung
---
Ausgangsgutschriften im Modul Eingangsrechnung
==============================================

Es kommt vor, dass an Partner, Tippgeber, Empfehler aber auch an Zulieferer Ausgangsgutschriften erstellt werden müssen. 

Da diese Art von Belegen für Sie als UnternehmerIn einen Aufwand darstellen, müssen diese als Eingangsrechnung erfasst werden. Üblicherweise sind dafür keine Artikelbewirtschaftungen oder ähnliches erforderlich, obwohl man gerne vorgefertigte Texte verwendet.

Wir haben daher das Modul Eingangsrechnung erweitert, sodass auch Artikel bzw. Handartikel erfasst werden können und dies in Form einer Ausgangsgutschrift ausgedruckt werden kann.

Um die Eingangsrechnung in diesen Modus umzuschalten, haken Sie bitte in den Kopfdaten der Eingangsrechnung ![](mit_Positionen.gif) mit Positionen an. 

Dies bewirkt, dass die Eingangsrechnung als Splittrechnung erfasst werden muss und dass im Reiter Kontierung auch Artikel bzw. Handeingaben erfasst werden können. 

Zugleich wird die Druckfunktion auf Belegdruck umgeschalten.
Weiters wird die Lieferantenrechnungsnummer mit 0 vorbesetzt und ein vorläufiger Rechnungsbetrag von 0 angenommen.
Nach dem Speichern der Kopfdaten wechseln Sie in den Reiter Kontierung und erfassen in der üblichen Bedienungsweise von **Kieselstein ERP** Artikel bzw. Handeingaben.
Für längere beschreibende Texte zu den Positionen verwenden Sie bitte die Texteingabe zu Artikel ![](Texteingabe_in_Kontierung.gif).

**Info:** Die Rechnungsbeträge der Eingangsrechnung werden, da dies ja eine positionsweise Erfassung ist, erst beim Aktivieren, also dem tatsächlichen Ausdrucken der Ausgangsgutschrift errechnet und eingetragen. Mit diesem Aktivieren ist auch die eventuelle Verbuchung in der integrierten Finanzbuchhaltung verbunden, bzw. die Freigabe für einen eventuellen Fibu-Export.

**Tipp:** Für die effizientere Erfassung empfiehlt es sich, das beim Lieferanten, Reiter Konditionen, Kostenstelle und Warenkonto vorbelegt sind. Diese werden bei jeder Kontierungsposition automatisch aus dem Lieferantenstamm übernommen.

#### Können Kopf- bzw. Fußzeilen definiert und bearbeitet werden?
Ja. Die allgemeine Definition finden Sie in den Grunddaten der Eingangsrechnung. Die je Ausgangsgutschrift eventuell individuellen Kopf- bzw. Fußzeilen finden Sie im Reiter Kopf/Fuß.

#### Werden die Zeilen Text und WE-Artikel mit angedruckt?
Ja.

#### Kann eine bereits ausgedruckte Ausgangsgutschrift wieder geändert werden?
Ja. Wechseln Sie dazu in die Kopfdaten, Klicken Sie auf Ändern und beantworten Sie die Frage entsprechend. Beachten Sie bitte, dass hier noch weitere Regeln aus der Buchhaltung greifen, welche eventuell verhindern, dass eine aktivierte Ausgangsgutschrift (= Eingangsrechnung) noch verändert werden darf (zb. UVA Verprobung).

#### Bei der Kontierung muss ich immer Kostenstelle und Kontierung auswählen, kann man das vorbesetzen?
Ja. Definieren Sie in den Konditionen des jeweiligen Lieferanten Kostenstelle und Warenkonto. Wenn Sie nun das Eingangsrechnungsmodul neu öffnen wird für die jeweiligen Lieferanten das entsprechende Konto und die Kostenstelle vorgeschlagen.

#### Wo kann ich Belastungsanzeigen an meine Lieferanten senden?
Genauso wie die Ausgangsgutschriften Aufwände sind, sind Belastungsanzeigen an Lieferanten, z.B. für eine Lieferung in einer anderen als der gewünschten Qualität, negative Aufwände und kein Ertrag. D.h. um eine Belastungsanzeige an Ihren Lieferanten zu senden, nutzen Sie die Eingangsrechnung und setzen in den Kopfdaten ebenfalls den Haken bei mit Positionen. Nun geben Sie bitte ebenfalls die gewünschten Artikel oder Handeingaben ein, definieren aber einen negativen Preis. Damit wird beim Druck der Eingangsrechnung erkannt, dass der Gesamtwert der Eingangsrechnung negativ ist und somit der Text Belastung beim Druck der Eingangsrechnung angezeigt.

**WICHTIG:** Bitte beachten Sie, dass die angegebenen Artikel nur textliche Platzhalter sind. Es ist mit diesem Modul derzeit KEINE wie immer geartete Lagerbewirtschaftung verbunden.

Sollten Sie Waren mit Lieferantenlieferscheinen an Ihren Lieferanten zurückgesandt haben, so denken Sie bitte daran, dass dieser Lieferschein mit der Belastungsanzeige manuell erledigt werden sollte.

#### Beim Klick auf Kopf-Fußzeilen kommt eine Fehlermeldung
![](Kopf_Fuss_Zeilen.png)  

Sollte dies der Fall sein, sind wahrscheinlich die Kopf- und Fußzeilen nicht definiert.<br>
D.h. bitte im Modul der Eingangsrechnungen, auf Grunddaten, Kopf/Fußtext
![](Kopf_Fuss_Zeilen_erfassen.png)  

Dadurch werden die default Texte angelegt, womit der weitere Ablauf funktioniert.
