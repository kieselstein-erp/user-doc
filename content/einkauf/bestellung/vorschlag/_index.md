---
categories: ["Bestellung"]
tags: ["Bestellvorschlag"]
title: "Bestellvorschlag"
linkTitle: "Bestellvorschlag"
date: 2023-01-31
weight: 100
description: >
  Bestellvorschlag errechnen, oft auch Dispolauf genannt
---
Bestellvorschlag
================

Im Modul Bestellung finden Sie in den unteren Modulreitern den ![](Reiter_Bestellvoschlag.gif)
Mit dem Bestellvorschlag haben Sie ein effizientes Werkzeug um Ihre Einkaufsbedarfe festzustellen. Voraussetzung dafür ist selbstverständlich, dass alle Bedarfe in **Kieselstein ERP** eingepflegt wurden. Bitte beachten Sie, dass der Bestellvorschlag nur auf die Artikel Ihres Mandanten wirkt. Handartikel können vom Bestellvorschlag nicht berücksichtigt werden.
In vielen Branchen wird diese Funktion auch Dispolauf genannt.

#### Wie rechnet der Bestellvorschlag?
Unterschreitet die frei verfügbare Lagermenge (Lagerstand - Reservierung - Fehlmenge + Bestellungen unterwegs) den Lagermindeststand, so wird auf den Lagersollbestand aufgefüllt. Bei dieser Formel wird zusätzlich der (Liefer-)Termin der Reservierungen, Fehlmengen und Bestellungen unterwegs berücksichtigt. Dies bewirkt, dass Terminverschiebungen eines Projektes sich auch im Bestellvorschlag auswirken. Bitte beachten Sie dazu unbedingt auch die Liste der nicht mehr benötigten Bestellungen.
Alternativ dazu gibt es auch die Berechnung anhand des Lagermindeststandes. [Siehe bitte dort](#Bestellvorschlag anhand Lagermindeststand).

#### Wozu dient der Bestellvorschlag?
Der Bestellvorschlag dient der Ermittlung der Warenbedarfe aufgrund der derzeitigen Lagerstände, der Reservierungen (für Aufträge, Fertigungslose, ...) und der Fehlmengen.
D.h. bei einem neu erzeugten Bestellvorschlag sehen Sie die Waren, welche zur Fertigung / Lieferung der derzeit bekannten Aufträge und Fertigungsaufträge unter Berücksichtigung der lagernden Artikel und der bereits bestellten Artikel, noch eingekauft werden müssen. Zusätzliche Informationen zur [Standortunterstützung]( {{<relref "/einkauf/bestellung/standorte">}} )

#### Wieso muss ich den Bestellvorschlag per Hand starten?
In vielen Unternehmen ist es so, dass die Abarbeitung des Bestellvorschlages einige Zeit in Anspruch nimmt. Würde der Bestellvorschlag durch einen Automatismus permanent neu errechnet werden, so würde sich auf die Ausgangsbasis für den Einkauf permanent verändern. Es hat sich in der Praxis bewährt den Bestellvorschlag, je nach Branche einmal täglich oder einmal wöchentlich neu zu erzeugen. Auf Wunsch kann diese Automatikfunktion entsprechend eingerichtet werden.

#### Können mehrere Benutzer zugleich einen Bestellvorschlag bearbeiten?
Die Idee des Bestellvorschlages ist, so wie oben beschrieben, dass immer das fehlende Material für das gesamte Unternehmen errechnet und damit bestellt wird. Daraus ergibt sich, dass der Bestellvorschlag nur von einer Person bearbeitet werden sollte. Zusätzlich kann der Bestellvorschlag auch von anderen Personen eingesehen werden, aber nicht verändert werden.
Wenn nun, vor allem aufgrund der Trennung der Bereiche, es erforderlich sein sollte, dass der Bestellvorschlag von mehreren Personen gleichzeitig genutzt werden sollte, so steht auch diese Funktion zur Verfügung. Siehe bitte [Bestellvorschlag gemeinsam nutzen](#persoenlicher Bestellvorschlag).

#### Wie startet man einen Bestellvorschlag?
Mit ![](Bestellvorschlag_Neu_Erzeugen.gif) kann ein neuer Bestellvorschlag erstellt werden. Bitte beachten Sie, dass dazu der bestehende Bestellvorschlag gelöscht wird.

![](Bestellvorschlag_Starten.png)  

Vor dem eigentlichen Start des Erzeugens des Bestellvorschlages müssen noch folgende Eingaben für die Berechnung des Wunsch-Liefertermines, welcher in den Bestellvorschlag übernommen wird, gemacht werden.

### Vorlaufzeit für die Aufträge in Tagen
<a name="Vorlaufzeit"></a>
Aufgrund der Reservierungen im Auftrag bzw. im Fertigungslos, hier werden selbstverständlich auch die Fehlmengen berücksichtigt, wurden die Artikel zum Liefertermin (AB), bzw. zum Produktionsbeginn (LO) reserviert. Nun ist es aus Gründen Ihrer internen Organisation erforderlich, dass die benötigten Artikel einige Tage vorher bei Ihnen eintreffen. Oft wird auch von den Lieferanten der Liefertermin nicht als bei Ihnen eintreffend, sondern beim Ihm abgehend behandelt. Um dies zu berücksichtigen, wird bei den Terminen zu denen Sie die Ware tatsächlich benötigen, noch die oben angeführte Vorlaufzeit abgezogen und dies als Liefertermin für die Bestellungen vorgeschlagen. Für die Vorbelegung siehe bitte Parameter: DEFAULT_VORLAUFZEIT_AUFTRAG

Wenn bei einer Position in der Stückliste für den Fertigungsauftrag der Haken für "sofortige Bestellung" gesetzt wird, so wird diese Position sofort in den Bestellvorschlag aufgenommen (siehe [sofortige Bestellung]( {{<relref "/warenwirtschaft/stueckliste/#bestimmte-artikel-sollten-immer-fr%c3%bcher-oder-sp%c3%a4ter-bestellt-werden" >}} ) ).

### Toleranz
<a name="Toleranz"></a>
Dies gibt die Toleranzfrist für geplante Wareneingänge aus Bestellungen an, welche eventuell um die Toleranzfrist zu spät kommen, z.B. aufgrund geänderter Termine aus den Auftragsbestätigungen oder Lieferterminverschiebungen.

Für die Vorbelegung siehe bitte Parameter: TOLERANZFRIST_BESTELLVORSCHLAG

D.h. Artikel die um die angegeben Anzahl an Tagen zu spät bei Ihnen eintreffen, werden trotzdem als noch rechtzeitig berücksichtigt.

Kommt der Artikel nicht rechtzeitig, wird er erneut in den Bestellvorschlag mit aufgenommen.

Sollte die Terminsteuerung faktisch deaktiviert werden, so hat sich in der Praxis eine Eingabe von 999 Tagen bewährt.

### Liefertermin für Artikel ohne Reservierungen
Ergibt sich aufgrund von Materialverbrauch ohne Auftrag eine Lagermindeststandsunterschreitung, so muss auch für diese Artikel, das ist z.B. bei Verbrauchsmaterialien wie Lötzinn, Schmiermittel der Fall, die Ware zu einem bestimmten Termin bestellt werden. Stellen Sie also hier den gewünschten Liefertermin für diese Artikel entsprechend ein. Siehe dazu bitte auch [Lagermindeststand]( {{<relref "/docs/stammdaten/artikel/#lagermindestbestand---lagersollbestand-wozu-brauch-ich-das" >}} ).

### Lose
Sollte ein Bestellvorschlag nur für bestimmte Lose durchgeführt werden, so geben Sie hier die Liste der Lose an, die berücksichtigt werden sollten. Die Berechnung der Daten erfolgt in der Form, dass nur die in den angeführten Losen enthaltenen Artikel berücksichtigt werden. Für die berücksichtigten Artikel wird jedoch der gesamte Bedarf anhand der vorhandenen / eingepflegten Daten errechnet.

### Warum wird für jeden der berücksichtigten Artikel trotzdem eine Gesamtbetrachtung gemacht?

Um dies zu erklären folgendes Beispiel:
| Beleg | Termin | Bedarf |
| --- | --- | --: |
| Lagerstand | | 100 Stk. |
| Los 1 | 1.9.2019 | 25 Stk. |
| Los 2 | 2.9.2019 | 70 Stk. |
| Los 3 | 3.9.2019 | 40 Stk. |

Wird nun der Bestellvorschlag nur unter Berücksichtigung von Los 1 und Los 3 erzeugt, so werden, auf den ersten Blick nur 65 Stk. benötigt. Da 100 Stk. auf Lager sind, könnte man annehmen, dass dies ausreichend ist. In Wirklichkeit nehmen Sie aber dem dazwischen zu produzierenden Los eine entsprechende Stückzahl weg, sodass, wenn die terminliche Reihung eingehalten wird, zum Fertigungszeitpunkt des Loses 3 nur eine zu geringe Stückzahl verfügbar ist. Daher muss auch für die Betrachtung einzelner Lose für jeden einzelnen Artikel trotzdem die gesamte zukünftige Lagerstandsentwicklung betrachtet werden.<br>
[Siehe dazu auch Druck Losausgabe mehrerer Lose]( {{<relref "/fertigung/losverwaltung/#druck-mehrerer-losausgaben-in-einer-liste" >}} )

[Siehe auch die Bedeutung der kleinen f am rechten Rand der Los-Auswahlliste]( {{<relref "/fertigung/losverwaltung/#was-bedeutet-das-kleine-f" >}} )

### Aufträge
Werden hier Aufträge angegeben, so werden der Berechnung alle Lose der angegebenen Aufträge berücksichtigt.<br>
Es gilt das oben für Lose Beschriebene.

### Nur die betroffenen Lospositionen berücksichtigen
Wird diese Einstellung angehakt, so werden bei der Berücksichtigung der Lospositionen nur diejenigen Lospositionen berücksichtigt, die sich tatsächlich anhand der angegebenen Losliste bzw. der Aufträge ergeben. Diese Funktion wird üblicherweise nur bei unsauberen Strukturen im Lager benötigt. Wenn Sie Ihr Lager und auch Ihre Fertigungsaufträge im Griff haben, so ist diese Funktion kontraproduktiv, da es vorkommen kann, dass Ware NICHT bestellt wird, obwohl es aus Sicht der Verbrauche notwendig wäre. Es wird damit die oben beschriebene Gesamtbetrachtung abgeschaltet.

In anderen Worten bewirkt diese Funktion, dass der Bestellvorschlag so ausgeführt wird, als wie wenn bei den Verbrauchen auf die Lose nur die angeführten / errechneten Lose vorhanden wären. Alle anderen geplanten Lagerbewegungen bleiben erhalten, auch Losablieferungen auf andere Lose.

### Mit "Nicht-Lager-Bewirtschafteten" Artikeln
Ist diese Möglichkeit angehakt, so werden bei der Erzeugung des Bestellvorschlages auch noch die Nicht Lagerbewirtschafteten Artikel, die sich aufgrund des / der Lose ergeben in den Bestellvorschlag mit aufgenommen. Als Menge wird die beim Artikellieferanten eingegebene Standardmenge eingetragen, bzw. wenn diese nicht definiert ist wird null eingetragen.

Als Termin wird der oben angeführte Liefertermin für Artikel ohne Reservierungen verwendet.

### Bestellvorschlag anhand Lagermindeststand nur für eigengefertigte Artikel
<a name="Bestellvorschlag anhand Lagermindeststand"></a>
Wird für die Berechnung des Bestellvorschlag die Funktion ![](Bestellvorschlag_anhand_Lagermindesstand.gif) "Einen Bestellvorschlag anhand des Stücklistenmindestlagerstandes neu erstellen" verwendet, so baut die Berechnung der benötigten Mengen grundsätzlich auf den Lagermindestständen der Kopfstücklisten auf. D.h. es wird der Bedarf anhand der Endprodukte gesteuert. 

Grundsätzlich werden die Lagermindeststände der einzelnen Artikel auf Grund der Stückliste berechnet, Lagermindeststände berücksichtigt und hiervon die Menge der Bestellungen abgezogen.
Reservierungen werden in dieser Variante des Bestellvorschlags nicht berücksichtigt. Wenn Sie den Bedarf aus der Fertigung mitberücksichtigen wollen, so verwenden Sie bitte die "normale" Form des Bestellvorschlags (hier wird der Artikel auch angeführt). Der Bestellvorschlag anhand Lagermindestständen der Stückliste ist dafür gedacht, dass man den Lagerstand einiger Kopfstücklistenprodukte mittels Lagermindeststand und ohne fertigungsgesteuertem Bedarf überwacht.

In anderen Worten. Mit dieser Funktion stellen Sie sicher, dass die für Ihre fertigen Geräte benötigten Artikel bestellt werden. Dazu hinterlegen Sie bei den gewünschten Artikeln (den fertigen Geräten die Sie immer auf Lager haben möchten) einen Lagermindeststand. Damit wird die "immer" auf Lager frei verfügbare Menge definiert. Wenn der Lagerbestand + in Fertigung + offene Bestellungen kleiner als der Lagermindestbestand ist, ist ein Bedarf gegeben. Für diesen Bedarf wird die komplette Stücklistenauflösung gerechnet, mit allen Unterstücklisten. Davon werden die offenen Bestellungen, Mengen in Fertigung und lagernden Mengen abgezogen. Das Ergebnis sind die Mengen die tatsächlich zu bestellen sind.<br>
Bitte beachten Sie, dass die Basis der Berechnung immer der Lagermindeststand ist. Dieser muss größer 0 sein. Und es muss sich um eine [Kopfstückliste]( {{<relref "/warenwirtschaft/stueckliste/#kopfst%c3%bcckliste--endprodukt" >}} ) handeln.

Im Bestellvorschlag anhand Stkl-Mindestlagerstand werden Stücklisten vom Typ SetArtikel nicht berücksichtigt.

### Verdichtungszeitraum
Als Verdichtungszeitraum für die Verdichtung des Bestellvorschlags wird der Wert des Mandantenparameters BESTELLVORSCHLAG_VERDICHTUNGSZEITRAUM_TAGE verwendet.

### Welche Lieferanten werden verwendet?
Es wird immer der bevorzugte Lieferant des jeweiligen Artikels automatisch in den Bestellvorschlag übernommen

Der bevorzugte Lieferant ist jener, der in der Anzeige des Artikellieferanten an oberster Stelle gereiht ist.

Bitte beachten Sie die Besonderheiten des [Zentralen Artikelstamms]( {{<relref "/docs/stammdaten/artikel/mehrmandanten" >}} ).

### Wieso gibt es im Bestellvorschlag Positionen ohne Belegnummer
Der Bestellvorschlag baut auf dem Lagermindeststand auf. Ergibt sich nun aus den zukünftigen (fiktiven) Lagerbewegungen, dass der Lagermindeststand unterschritten wird, so ist unter Umständen kein Beleg der Auslöser und kann daher auch nicht angedruckt werden. Dies tritt z.B. bei Verbrauchsmaterial auf. Wenn z.B. Lötzinn oder Schmiermittel nur verbraucht werden, so haben diese, um immer ausreichenden Lagerstand zu haben, einen Lagermindeststand. Bei Artikeln dieser Art wird immer nur der Verbrauch z.B. als Handentnahme oder auf ein allgemeines Verbrauchslos gebucht. Wird nun der Lagermindeststand unterschritten, so wird, ohne Bezug zu irgend einem Beleg auf den Sollbestand aufgefüllt.

#### Wie werden die Termine errechnet, wie hängt das zusammen?
[Siehe dazu]( {{<relref "/docs/schulung/begriffe/#welcher-termin-bedeutet-was" >}} )

#### Wozu dient die Bewegungsvorschau?
In der Bewegungsvorschau sehen Sie die zukünftige Lagerstandsentwicklung zur Basis der Lagerstände, Reservierungen und Fehlmengen sowie der eintreffenden Bestellungen. Sie erhalten damit also eine Sicht auf die Berechnungsgrundlage des Bestellvorschlages eines einzelnen Artikels. Sie können damit sehr einfach die tatsächlich benötigten Mengen nachvollziehen.

#### Wozu dient die Liste der nicht mehr benötigten Bestellungen?
In der Liste der nicht mehr benötigten Bestellungen sehen Sie diejenigen Bestellungen, welche aufgrund des neu erzeugten Bestellvorschlages und der aktuellen Reservierungs- und Lagerstände nicht mehr benötigt werden.

Wenn Sie die Liste der nicht mehr benötigten Bestellungen mit dem Bestellvorschlag vergleichen, kann es sein, dass Sie darin auch Bestellungen wiederfinden, die auch im Bestellvorschlag aufgeführt sind. Sie sollten diese Bestellungen entsprechend verschieben, oder die betroffenen Reservierungen (und Fehlmengen) entsprechend umplanen.

#### Wie bearbeitet man den Bestellvorschlag, kann man neue Positionen hinzufügen?
Durch Klick auf ![](Bestellvorschlag_Neu.gif) kann eine neue Position im Bestellvorschlag eingefügt werden.<br>
Durch Klick auf ![](Bestellvorschlag_aendern.gif) kann eine Bestellvorschlagsposition verändert werden.

#### Kann man in einen Bestellvorschlag auch Daten importieren?
Ja. Wählen Sie dazu im Menü Bestellvorschlag, Import.<br>
![](Bestellvorschlag_importieren.jpg)<br>
Damit kann eine xls Datei mit Artikelnummer und (Bestell-) Menge importiert werden. Bei der Übernahme kann der gewünschte Liefertermin angegeben werden. Zugleich kann definiert werden, ob dies eine Ergänzung oder ein neuer Bestellvorschlag ist.<br>
**Hinweis:** Der Positionen des Imports werden OHNE der Kennzeichnung Vormerkung übernommen. D.h. bei der nächsten Erzeugung des Bestellvorschlages werden diese in der Regel wieder gelöscht.

#### Wie geht der Bestellvorschlag mit Rahmenreservierungen um?
Sie werden genauso verarbeitet wie andere Reservierungen mit dem Unterschied, dass daraus Rahmenbestellungen erstellt werden.<br>
Bitte beachten Sie dazu, dass bei der [Überleitung](#Bestellungen_erzeugen) der Rahmenbestellungen der Punkt Abrufbestellungen zu vorhandenen Rahmen erzeugen angehakt sein muss.

#### Können Verpackungseinheiten und Mindestbestellmengen berücksichtigt werden?
Diese Funktion ist mit dem Verdichten des Bestellvorschlages kombiniert.<br>
D.h. wenn Sie auf ![](Verdichten.gif) Verdichten klicken, so werden die Mindestbestellmengen und die Verpackungseinheiten im Bestellvorschlag berücksichtigt / eingerechnet. Mit dem Einrechnen der Mindestbestellmengen und der Verpackungseinheiten kombiniert ist auch die Funktion des Verdichtens. D.h. es werden alle gleichen Artikel, welche innerhalb von 14Tagen zu bestellen sind, auf eine Position zusammengezogen.<br>
Für die Berücksichtigung der Mindestbestellmengen und der Verpackungseinheiten wird wie folgt vorgegangen:<br>
Es wird je Artikel und Lieferant die Mindestbestellmenge und die Verpackungseinheit berücksichtigt. D.h. dies ist unter Umständen auch für Stangenmaterial, bzw. Verpackungseinheit und ähnliches relevant.<br>
<u>**Beispiel:**</u>:<br>
Bestellvorschlag Ausgangssituation, also nachdem er wie bisher üblich erzeugt wurde:<br>
Es wird hier nur ein Artikel und ein Lieferant dargestellt. Diese Funktion wird für jeden Artikel + Lieferant ausgeführt.

| Menge | Termin | Resultierende Gesamtmenge |
| --: |  --- |  --: |
| 100 Stk. | 23.4. | 100 |
| 50 Stk. | 25.4. | 150 |
| 75 Stk. | 26.4. | 225 |
| 199 | 30.4. | 424 |

Mindestmenge: 75Stk
Verpackungseinheit: 25 Stk

| Menge | Termin | Resultierende Gesamtmenge |
| --: |  --- |  --: |
| 100 Stk | 23.4. | 100 |
| 75 Stk | 25.4. | 175 |
| 75 Stk | 26.4. | 250 |
| 175 | 30.4. | 425 |

Aufgrund der Mindestmenge muss zum 25.4\. mehr bestellt werden. Dies schleppt sich bis zum 26.4\. durch und wird erst am 30.4\. wieder ausgeglichen. Zusätzlich muss um ein Stück mehr bestellt werden, da die Verpackungseinheit die gewünschte Stückzahl nicht Stückgenau zulässt.

#### Welche Preise werden nach dem Verdichten verwendet?
Da sich aufgrund des Verdichtens üblicherweise auch die zu bestellenden Mengen ändern, werden nach diesem Durchlauf auch die aktuellen Einkaufspreise anhand der Mengenstaffeln aktualisiert. Dafür wird für jede Position der bevorzugte Lieferant eingetragen und für diesen dann die jeweiligen Preise aus den Einkaufsmengenstaffeln geholt / angewandt.<br>
Damit ergibt sich, dass eventuelle vorherige Preis- bzw. Lieferantenänderungen überschrieben werden.<br>
Das bedeutet wiederum: Führen Sie bitte das Verdichten **vor** der Preispflege durch.

#### Später bestellbare Positionen löschen
Durch Klick auf ![](SpaeterPositionen_Loeschen.gif) werden die Positionen aus dem Bestellvorschlag entfernt, welche auch zu einem späteren Zeitpunkt noch rechtzeitig bestellt werden können. Voraussetzung dafür ist, dass in den Artikellieferanten die übliche Lieferzeit der Artikel angegeben ist. So kann anhand des Termins der Berechnung des nächsten Bestellvorschlages ermittelt werden, ob die im derzeitigen Bestellvorschlag befindlichen Artikel auch beim nächsten Lauf des Bestellvorschlages noch rechtzeitig bestellt würden.

#### Wie macht man aus dem Bestellvorschlag einzelne Bestellungen?
Durch Klick auf ![](Bestellvorschlag_ueberleiten.gif) werden einzelne Bestellungen erzeugt. Hier kann ausgewählt werden, ob der gesamte Bestellvorschlag oder nur einzelne Lieferanten in Einzelbestellungen übergeleitet werden.

#### Wie werden aus dem Bestellvorschlag einzelne Bestellungen erzeugt ?<a name="Bestellungen_erzeugen"></a>
Durch Klick auf den Button ![](Bestellungen_aus_Bestellvorschlag_erzeugen.gif) wird die Überleitung des Bestellvorschlages in Einzelbestellungen gestartet.

Es stehen hier folgende Möglichkeiten zur Verfügung:
| Variante | Beschreibung |
| --- |  --- |
| für jeden Lieferanten und gleichen Termin eine Bestellung anlegen | Es werden alle Positionen des Bestellvorschlags in Bestellungen umgewandelt.Je Lieferant und nur Positionen mit gleichen Terminen werden in die jeweils eine Bestellung übernommen. D.h. für jeden unterschiedlichen Bestelltermin wird eine eigene Bestellung angelegt. |
| je Lieferant eine Bestellung anlegen | Wie oben, jedoch werden die Positionen mit den unterschiedlichsten Terminen alle in eine Bestellung je Lieferant übernommen. In den Positionsterminen werden die Wunschtermine hinterlegt. |
| ein Lieferant und ein Termin | Hier muss der Lieferant und der gewünschte Termin ausgewählt werden. Es werden nur die zutreffenden Positionen aus dem Bestellvorschlag in eine Bestellung übertragen. |
| ein Lieferant und alle Positionen des Lieferanten | Nach Auswahl des Lieferanten werden alle Positionen mit unterschiedlichsten Terminen in eine Bestellung übertragen. Die Wunschtermine aus dem Bestellvorschlag werden in den Positionsterminen hinterlegt. |
| Abrufbestellungen zu vorhandenen Rahmenbestellungen erzeugen. | Bei dieser Überleitung wird versucht eine Zuordnung der Bestellvorschlagspositionen zu den offenen Rahmenbestellungen herzustellen. Also die Bestellvorschlagspositionen als Abrufe anzulegen.Wird der selbe Artikel zum selben Termin mehrfach benötigt, so werden diese Positionen zusammengezogen.Von der Reihenfolge her, werden immer die ältesten offenen Rahmenpositionen zuerst erfüllt. Gegebenenfalls wird, bei Bedarf, der letzte Rahmen überbestellt. Also mehr bestellt als eigentlich laut Rahmen noch abzurufen wäre. Diese Überbestellungen werden am Ende der Überleitung angezeigt. |

Wenn die Zusatzfunktion Projektklammer bei Ihnen aktiviert ist, so besteht die Möglichkeit bei der Überleitung auch diese zu berücksichtigen. Setzen Sie den Haken bei ![](projektklammer_beruecksichtigen.JPG). Nun wird die Projektnummer bei der aus dem Bestellvorschlag erstellten Bestellung mit übergeben. Im Projektverlauf wird die Bestellung somit angeführt und bei der Nachkalkulation miteinbezogen.

Wenn ein mandantenübergreifender Artikelstamm genutzt wird, werden nur die Bestellvorschlags-Einträge des aktuellen Mandanten berücksichtigt.

#### Wieso sind manche Zeilen des Bestellvorschlages blau?
Damit wird signalisiert, dass diese Artikel nicht lagerbewirtschaftet sind.

<u>Hinweis:</u><br>
Beim Ausdruck des Bestellvorschlages werden die Nicht Lagerbewirtschafteten Artikel mit NLG angezeigt.

#### Wie macht man normalerweise einen Bestellvorschlag?
Der Ablauf um einen Bestellvorschlag optimiert durchzuführen ist folgender:
1.  Neuen Bestellvorschlag erzeugen
2.  Prüfen Sie die nun detailliert angeführten Inhalte, ob dies den von Ihnen erwarteten Daten entspricht
3.  Verdichten Sie die Daten des Bestellvorschlages
4.  Gegebenenfalls löschen Sie die später bestellbaren Positionen
5.  Nach der eventuell erforderlichen Überarbeitung übertragen Sie die Positionen je Lieferant oder alle in Einzelbestellungen

#### Lieferanten / Einkauf optimieren
<a name="Optimieren"></a>
Im Einkauf ergibt sich immer wieder die Herausforderung, dass der bevorzugte Lieferant für die geplante / erforderliche Beschaffung zwar der optimale Lieferant wäre, nur erzielt man die Mindestbestellwerte nicht. Daher geht es immer wieder darum, dass anhand der hinterlegten Artikellieferanten für die errechneten Bedarfe die möglichen Lieferanten aufgezeigt werden.

Die Vorgehensweise dazu ist wie folgt:
1. Erstellen Sie den Bestellvorschlag wie oben beschrieben
2. wechseln Sie in den oberen Modulreiter Lieferanten optimieren
3. wählen Sie den gewünschten Lieferanten aus der Combobox aus

![](Bestellvorschlag_Lieferanten_optimieren.JPG)

Nun erhalten Sie eine Anzeige ähnlich der obigen.

In dieser Anzeige sind neben den Mengen und Werten vor allem folgende Informationen relevant:
- In der rechten Spalte wird die Sortierung des Artikellieferanten angezeigt. Alles andere als die 1 (eins) bedeutet, dass dieser Lieferant NICHT der bevorzugte Lieferant ist.
- Ist die Zeile grau hinterlegt, so bedeutet dies, das für diese Position des Bestellvorschlages derzeit ein anderer Lieferant als der in der Combobox gewählte hinterlegt ist. Es ist jedoch im Artikellieferanten dieses Artikels der Lieferant als möglicher Lieferant hinterlegt.
- wenn Sie den Lieferanten aus der Liste auswählen, so werden rechts davon Informationen zu den Lieferkonditionen (Lieferart, Zahlungsziel, Mindestbestellwert) angeführt

Um nun auch den gewählten Lieferanten auf der Zeile in den Bestellvorschlag zu übernehmen, klicken Sie auf den grünen Haken ![](bestaetigen.JPG) bzw. drücken Sie Strg+L. Somit wird im Bestellvorschlag der Lieferant dieser Position auf den (in der Combobox) ausgewählten geändert und auch der angezeigte Preis wird übernommen.

War bei diesem Lieferanten kein Einkaufspreis definiert, so wird der Preis im Bestellvorschlag nicht geändert.

Um für den Artikel den bevorzugten Lieferanten wiederherzustellen klicken Sie auf ![](lieferant_zuruecksetzen.JPG) diesen Button (Lieferantensymbol).

Weiters können Sie für den Artikel die Monatsstatistik ![](Monatsstatistik_anzeigen.JPG) aufrufen um weitere Informationen zum Bedarf zb. im letzten Jahr zu erhalten.

Das GoTo-Icon  ![](gehe_zu_artikel.JPG) ermöglicht das wechseln in den Artikel der markierten Zeile.

In der Auswahlliste werden zu Ihrer Information noch:
| Feld | Bedeutung |
| --- | --- |
| WB | Wiederbeschaffungszeit |
| Fixkosten | die Fixkosten |

auf dieser Position mit angezeigt .

Weiters finden Sie in der Liste des Bestellvorschlag (Reiter 1) im unteren Bereich des Fensters die Funktion für eine Position den günstigsten Lieferanten auszuwählen.

![](bestellvorschlag_lieferanten_liste.JPG)

Wenn Sie einen Zeile im Bestellvorschlag markieren, so wird dieser Teil des Fensters aktualisiert. 

Sie erhalten eine Liste der Lieferanten dieses Artikels mit den dazugehörigen Preisen, Verpackungseinheit (VPE), Mindestbestellmengen (Min) und der Wiederbeschaffungszeit (WBZ).
Wenn Staffelpreise für den Einkauf definiert sind, werden diese ebenso angeführt.
Die niedrigste WBZ und der günstigste Preis sind Grün markiert, der günstigste Wert in Gelb. Der aktuell gewählte Lieferant ist grau hinterlegt.

Markieren Sie nun den gewünschten Lieferanten und klicken auf den Button Übernehmen. Damit ändern Sie den Lieferanten bei der ausgewählten Position des Bestellvorschlags.

#### Wie ändere ich den Lieferanten einer Position?
Einerseits können Sie über das Optimieren von Lieferanten ([siehe oben](#lieferanten--einkauf-optimieren)) den Lieferanten eines Artikels aus der Liste der Artikellieferanten abändern.

Soll der Artikel nun bei einem anderen Lieferanten bestellt werden, als in den Artikellieferanten hinterlegt, so verwenden Sie dazu das Icon ![](Lieferant_aendern.JPG) Lieferant ändern und wählen den gewünschten Lieferanten aus der Liste aus. Nach der Auswahl erfolgt die Frage, ob Sie den Lieferanten in die Grunddaten des Artikels als Artikellieferant, oder sogar bevorzugten Lieferanten (Lief1) zurückpflegen wollen.

![](Lieferant_nachtragen.JPG) 

#### Wird der Preis im Artikelstamm mitgepflegt?
Werden im Bestellvorschlag die Beschaffungspreise eines Artikels geändert, so wird automatisch der Einkaufspreis ebenfalls mitgeändert. Dies dient vor allem der einfacheren Preispflege.
Bitte beachten Sie, dass die Preisaktualisierung bei Verwendung aus dem Anfragevorschlag NICHT durchgeführt wird.

#### Kann ich Artikel zur Bestellung vormerken? Wie erstelle ich eine Vormerkliste?
Der Bedarf für Artikel wie zum Beispiel Büromaterial oder nicht lagerbewirtschaftetes Kleinmaterial kann im Bestellvorschlag erfasst werden. Wenn ein Artikel manuell hinzugefügt wird, dann wird diese 'Vormerkung' vorbesetzt. Weiters erhalten Sie die Information, wer die Vormerkung zu welchem Zeitpunkt angelegt hat ![](vormerker.JPG).

Mit Klick auf Neu können Sie einen Artikel in den Bestellvorschlag eintragen. Für die Nachvollziehbarkeit, warum ein Artikel benötigt wurde, ergänzen Sie den Textinhalt um die Information. Wenn nun ein neuer Bestellvorschlag erstellt wird, können Sie angeben, ob die Vormerkliste gelöscht werden soll. In dem Sie den Haken bei ![](vormerkliste_loeschen.JPG) setzen.

#### Woher kann man erkennen, dass Artikel auf Grund von Terminverschiebungen zusätzlich bestellt werden?
Auch wenn Positionen schon bestellt wurden, kann es sein, dass auf Grund der Terminsituation und der Einstellung [Toleranz](#toleranz) die Position nochmals im Bestellvorschlag aufscheint. Diese Zeilen werden farblich gekennzeichnet und somit hervorgehoben. Wenn es in der Bestelltliste Einträge nach dem Liefertermin gibt, dann wird die Zeile dunkelgrün angezeigt, falls der Artikel nicht lagerbewirtschaftet ist, dunkelgelb.
| Farbe | Bedeutung |
| --- | --- |
| dunkelgrün | Bestellungen nach dem Liefertermin vorhanden |
| dunkelgelb | Artikel ist nicht Lagerbewirtschaftet |
| grau | Keine Lagerzubuchung für diesen Artikel |

#### Im Bestellvorschlag wird eine Zeile in grau angezeigt. Was bedeutet das?
Für den Artikel ist: Keine Lagerzubuchung definiert. D.h. er darf nicht bestellt werden. [Siehe dazu auch]( {{<relref "/docs/stammdaten/artikel/#keine-lagerzubuchung-wozu-ist-dies" >}} )

Fachlich bedeutet dies. Sie haben einen Artikel definiert, dass er selbst nie dem Lager zugebucht werden darf, aber: In einem Los, in einem Kundenauftrag usw. steckt trotzdem so ein Artikel drinnen. Suchen Sie über Bewegungsvorschau, Verwendungsnachweis des Artikels was die Ursache ist und stellen Sie diese nachhaltig ab.

#### Wann wurde der aktuell angezeigte Bestellvorschlag erzeugt?
Wählen Sie im Menü, Info, Zuletzt erzeugt. Hier wird wann und mit welchen Daten der Bestellvorschlag zuletzt angezeigt wurde dargestellt. Ist diese Liste leer, so wurde Ihr Bestellvorschlag vor der Protokollierung der Durchführung zuletzt erzeugt.

#### Bestellvorschlag gemeinsam nutzen
<a name="persoenlicher Bestellvorschlag"></a>
Wenn der Bestellvorschlag von mehreren Personen gemeinsam genutzt werden sollte, so kann dies durch aktivieren des Parameters PERSOENLICHER_BESTELLVORSCHLAG erreicht werden.
Wir bitten Sie dazu folgendes zu beachten:

- Bevor der persönliche Bestellvorschlag aktiviert wird, muss der globale Bestellvorschlag geleert werden
- der persönliche Bestellvorschlag ist vor allem dafür gedacht, dass mehrere Einkäufer, die unterschiedliche Kundenprojekte bearbeiten den jeweils persönlichen Bestellvorschlag bekommen. Dies wird dadurch erreicht, dass beim Erzeugen des Bestellvorschlages entsprechende Lose ausgewählt werden, wodurch nur die Artikel/Bauteile des jeweiligen Loses betrachtet werden.
- Es wird damit auch erreicht, dass der Bestellvorschlag quasi parallel durch die gleiche Person auf unterschiedlichen Clients bearbeitet werden kann
- Durch das parallel Arbeiten kann sich auch ergeben, dass die selben Artikel von beiden Personen gemeinsam bearbeitet würden. Bei der Darstellung des Bestellvorschlages wird die Bearbeitung von ein und dem selben Artikel in Rot dargestellt um den Benutzern zu signalisieren, dass es hier auch zu Doppel-Bestellungen kommen könnte. Um dies hinten anzustellen, raten wir, dass sich die Benutzer abstimmen und die Einkäufer die sich nicht um die gemeinsamen Artikel kümmern, diese aus ihrem persönlichen Bestellvorschlag entfernen.
- Bei der Überleitung des Bestellvorschlages in einzelne Bestellungen kann ausgewählt werden, ob auch die gemeinsamen Artikel bestellt werden sollten. Bedenken Sie dazu das oben gesagte.

Hinweis:<br>
Mit der Aktivierung des persönlichen Bestellvorschlages wird zugleich auch der Anfragevorschlag auf persönlichen Anfragevorschlag umgeschaltet.

### Meldung: Muss beim angemeldeten Mandanten als Lieferant angelegt werden?
Diese Meldung erscheint, wenn Sie die Funktion des gemeinsamen Artikelstamm über mehrere Mandanten hinweg verwenden.<br>
![](Bestellvorschlag_anderer_Mandant_als_Lieferant_anlegen.jpg)<br>
Das bedeutet, dass Sie natürlich von einem Mandanten beim anderen Mandanten mit all den Möglichkeiten der mandantenübergreifenden Bestellung bestellen werden.<br>
Bitte beachten Sie bei der Anlage des anderen Mandanten als Lieferant, dass Sie den wirklichen Mandanten, siehe System, Mandanten verwenden.<br>
Wir raten auch dringend dazu, den anderen Mandanten nur in sehr sehr begründeten Ausnahmefällen mehrfach als Lieferant anzulegen.

