---
categories: ["Bestellung"]
tags: ["Import"]
title: "Bestell-Import"
linkTitle: "Import"
date: 2023-01-31
weight: 200
description: >
  Bestellungen Importieren
---
Bestellungsimport
=================

Mit der Funktion Bestellungsimport ![](Bestellungs_Import.gif) steht Ihnen eine komfortable Funktion zur Verfügung, um Daten z.B. aus dem Online WebShop Ihres Lieferanten importieren zu können.
Die Idee dahinter ist, dass bei bekanntem Lieferanten und bekannten Artikellieferantennummern automatisch entsprechende Bestellpositionen erzeugt werden können.
D.h. wenn Sie z.B. bei einem Ihrer Lieferanten in dessen OnlineShop eine Bestellung getätigt haben, so legen Sie in Ihrem **Kieselstein ERP** eine neue Bestellung an.
Anmerkung: Egal ob Sie diese Funktion nutzen oder nicht, es empfiehlt sich grundsätzlich auch für Bestellungen die Sie Online getätigt haben, diese in Ihrem **Kieselstein ERP** zu erfassen. Nur so haben Sie und die Geschäftsleitung einen Überblick über die zukünftigen Eingangsrechnungen und natürlich auch darüber, ob die Waren für einen bestimmten Auftrag überhaupt schon bestellt sind.

Um die Daten übernehmen zu können, wählen Sie bitte im OnlineShop den Export z.B. als CSV Datei und speichern Sie diese an einem günstigen Platz auf Ihrem Rechner ab (z.B. im Downloads Verzeichnis).
Nachdem die Bestellung angelegt ist klicken Sie auf den Bestellungsimport ![](Bestellungs_Import.gif) und wählen die oben abgespeicherte Datei aus.
Gegebenenfalls verwenden Sie eine bereits vorhandene Definition.
![](Bestellungs_Import2.gif)
Ziehen Sie nun die Spaltenüberschriften auf die entsprechenden Spalten.

Für die Bedienungsdetails siehe bitte [intelligenter Stkl Import]( {{<relref "/warenwirtschaft/stueckliste/importe/intelligent">}} ).

Nach dem Klick auf weiter wird versucht eine entsprechende Zuordnung zwischen den erhaltenen Daten und den Daten in Ihrem **Kieselstein ERP** Artikelstamm herzustellen. Sie erhalten z.B. folgende Darstellung:
![](Bestellungs_Import3.gif)
Mit Liefpreis übernehmen definieren Sie, dass anstelle des Importpreis der Lieferantenpreis in die Bestellung übernommen wird.
Mit Artikellieferant Update wird die Rückwärtspflege der Lieferantenartikelnummer auf den gewählten Artikel aktiviert.
Die Rückpflege des Bestellpreises bzw. des Einkaufspreises kann im Client im jeweiligen Reiter vorgenommen werden. Durch die entsprechende Pflege und bei entsprechend intensiver Verwendung erhalten Sie einen gut gepflegten Artikelstamm und haben damit beim Import der Bestellungen nur wenige neue Artikel zu pflegen und zugleich eine gute Vorkalkulation in Ihren Stücklisten, Angeboten usw.
Durch Klick in die Haken "Liefpreise übernehmen" bzw. "Artikellieferant updaten" werden die Haken bei allen Positionen gesetzt bzw. entfernt.
Positionen wie Frachtkosten und ähnliches definieren Sie z.B. als Handartikel. Ev. durch Klick auf Undefinierte mit Handartikel füllen.

Klicken Sie nun erneut auf weiter und die Positionen werden in Ihre Bestellung übernommen.

Im abschließenden Dialog können Sie die gewählte Importdefinition unter einem für Sie sprechenden Namen abspeichern, um diese beim nächsten Import zur Verfügung zu haben.

Info: Sollte der Bestellungsimport nicht zur Verfügung stehen, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer, damit er diese praktische Erweiterung für Sie zur Verfügung stellt.

### Welche Rechte sind für den Import erforderlich?
Für den Import muss der Benutzer folgende Rechte haben
- BES_BESTELLUNG_CUD
- LP_DARF_PREISE_SEHEN_EINKAUF
- LP_DARF_PREISE_AENDERN_EINKAUF

D.h. wenn die Anzeige in den Positionen der Bestellung wie folgt aussieht:<br>
![](Fehlende_Rechte_in_der_Bestellung.png)  <br>
so fehlt dir eines der obigen Rechte.