---
categories: ["Bestellung"]
tags: ["Standorte"]
title: "Standorte"
linkTitle: "Standorte"
date: 2023-01-31
weight: 300
description: >
  Standorte, Zweigstellen, Fertigungsstätten
---
Standorte
=========

Viele Unternehmen haben Ihre Fertigungsstätten auf mehrere Standorte aufgeteilt. In einigen Unternehmen wird das was wir hier mit Standort bezeichnen auch als Zweigstelle bezeichnet. D.h. ein anderer Standort ist, eine weitere Produktionsstätte ein und des selben Unternehmens an einem anderen Ort. Das bedingt zumindest, dass der andere Standort im gleichen Land ist. Dass alle Belege die gleichen Nummernkreise benutzen, es also KEIN eigenständiges (anderes) Unternehmen ist.<br>
Die Herausforderung hier ist, dass die Lagerbewirtschaftung oft durch eine Zentrale, den Zentraleinkauf gemacht wird, es jedoch für jeden Standort auch eine wirtschaftliche Entscheidung ist, ob nun Produkte neu bestellt werden, oder ob diese von einer Fertigungsstätte, wir sprechen hier von Standorten, zur anderen transportiert werden. Auf diese Gegebenheiten kann mit der Standortverwaltung, welche mit der Funktion des Lagermindest/-sollstandes je Lager gekoppelt ist, Rücksicht genommen werden.

Um diese Funktion nutzen zu können benötigen Sie das Modul Mehrlagerverwaltung. Zusätzlich muss der Parameter LAGERMIN_JE_LAGER auf 1 gestellt werden. Wird dieser Parameter umgeschaltet, so empfehlen wir Ihren **Kieselstein ERP** Client danach neu zu starten, sich komplett neu bei diesem Mandanten anzumelden.

Definition der Standorte

Definieren Sie bitte als erstes alle Ihre Läger und die dazugehörigen Standorte (Modul Artikel, unterer Modulreiter Grunddaten, oberer Modureiter Lager).
Bei der Auswahl der Standorte stehen Ihnen alle angelegten Partner zur Verfügung.
In allen Standort Auswahlen wird dann nur mehr auf die hier unterschiedliche definierten Partner abgefragt. Bitte beachten Sie, dass Lager mit gleichen Standorten als zusammengehörig betrachtet werden. Das bedeutet, dass z.B. dass bei Lagermindestständen beide Lager mit gleichem Standort für die Standortbetrachtungen addiert werden.
![](lager_standort.PNG)
Info: Für die Anzeige möglichst kurzer Standortnamen wird jeweils die Kurzbezeichnung des ausgewählten Partners für den Standort herangezogen.

Der Bestellvorschlag nach Standort

Der wesentlichste Unterschied liegt in der Beschaffung der Artikel für den jeweiligen Standort. 

Wie werden die Standort-Informationen für den Standort ermittelt?
- Aufträge haben Abbuchungslager (Kopfdaten, Ab Lager). D.h. das Abbuchungslager ergibt wiederum den Standort, welcher für die Verbrauchsberechnung dieses Artikels herangezogen wird.
- Lose haben Abbuchungsläger. Hier wird das erste (oberste) Lager als Standort des Loses angenommen. Das Ziellager des Loses kann durchaus ein anderes Lager sein und somit auch ein anderer Standort. Dies bedeutet implizit, dass der Transport an den anderen Standort dann Teil der Arbeiten für dieses Los ist.
- Bestellungen haben Lieferadressen. Unter der Annahme, dass diese Lieferadressen den Standorten entsprechen, wird diese Anlieferung für den jeweiligen Standort angenommen. Entspricht die Lieferadresse keinem der Standorte, so wird das beim Lieferanten der Bestellung hinterlegte Zu-Lager als Standortdefinition verwendet.

![](bestellvorschlag_standort.PNG)

Mit der Auswahl eines Standorts wird der Bestellvorschlag entsprechend gefiltert, sie erhalten somit einen Vergleich über den Gesamtbedarf aber auch über den Bedarf eines einzelnen Standorts.

Zusätzlich zum Bestellvorschlag kommt auch das Erfordernis dazu, dass es zwischen den Standorten einen internen Transport gibt, welcher die Gesamtlagerkosten aller Standorte reduzieren helfen sollte.<br>
Unser Vorschlag ist daher, dass vor der Bedarfsberechnung die Ausgleichsberechnung zwischen den Standorten erfolgen sollte. Sind diese Buchungen (Ziellager Lieferscheine) erstellt, kann daran anschließend der dann noch bestehende Bedarf einer Neubeschaffung für die jeweiligen Standorte ermittelt werden.

![](standortliste_bestellvorschlag.PNG)
Mit dem Journal - Standortliste erhalten Sie eine Liste aus dem Bestellvorschlag in dem die Bedarfe / Lagerstände aller Standorte angedruckt werden.
Daraus können Sie zum Beispiel Lieferantenlieferscheine mit Ziellager buchen um den internen Transport abzubilden.

Überleitung in Bestellungen

Bei der Überleitung des Bestellvorschlages wird für jeden Standort für durchaus gleiche Lieferanten eine eigene Bestellung erzeugt. Dies hat den Vorteil, dass die Abwicklung bei Ihnen im Hause deutlich vereinfacht wird. Bitte bedenken Sie dabei, dass Sie mit Ihren Lieferanten entsprechende Vereinbarungen getroffen haben, so dass durch die dadurch eventuell entstehenden geringeren Bestellmengen trotzdem Ihre strategischen Einkaufspreise erhalten bleiben.

Die Bewegungsvorschau

Die Bewegungsvorschau steht für jeden Standort als einzelne Betrachtung aber auch als Gesamtbetrachtung zur Verfügung. Bitte beachten Sie, dass bei einer Gesamtbetrachtung die Lagermindest und Sollstände alle Standorte und somit aller Läger addiert werden. Auf Basis dieser Daten werden somit die Warenbewegungen der Zukunft dargestellt. Daraus kann sich durchaus ergeben, dass:
1. die Bewegungsvorschau der einzelnen Standorte auf andere Ergebnisse kommt als die Bewegungsvorschau als Gesamtes
2. auch wenn im Bestellvorschlag die Berechnung für alle Standorte ausgewählt wird, so wird diese trotzdem für jeden Standort extra durchgeführt, da man in diesem Falle ja die Bedarf der einzelnen Standorte wissen möchte.

Interne Bestellung

Für die Berechnung der Standorte im internen Bestellvorschlag werden die in den Stücklisten definierten Abbuchungsläger herangezogen. Sollte in einer Stückliste kein Abbuchungslager definiert sein, so wird das Hauptlager verwendet. Die Definition Sortierung Losausgabe wird bei der Standortverwaltung NICHT verwendet. Diese Vorgehensweise zur Bestimmung der Lager der aus der internen Bestellung resultierenden Lose wird auch für die Definition der Lager beim Anlegen der Lose verwendet.
Bitte beachten Sie, dass in der internen Bestellung der angezeigte Standort der auslösende Standort ist. D.h. für diesen Standort ist ein Bedarf gegeben, z.B. weil das Lager des Auftrages auf diesen Standort zeigt. Beim Anlegen der Lose, werden die Standorte aus den Definitionen der Stückliste (Ziellager bzw. Abbuchungsläger) verwendet.
Daraus ergibt sich aus, dass für die Berechnung der internen Bestellung derzeit KEINE Berücksichtigung der Standorte vorgenommen wird. In diese Überlegung ist auch der Grundgedanke so wenig wie möglich zu produzieren / auf Lager zu haben, mit eingeflossen.

#### Bei der Überleitung werde ich nach Standorten gefragt, habe aber keine Standorte zur Auswahl
Wenn die Liste der Standorte leer ist, so wurden für die Lager keine Standorte definiert. Bitte beachten Sie, dass mit der Aktivierung des Parameters LAGERMIN_JE_LAGER für jedes Lager auch der passende Partner und damit der entsprechende Standort angegeben werden muss.<br>
![](Standortauswahl.gif)<br>
Werden trotzdem, einfach nur für alle Standorte z.B. die Lose bzw. die Bestellungen übertragen, so ist das Verhalten so, dass, da kein passender Standort definiert, KEINE Lose bzw. Bestellungen angelegt werden. Bitte definieren Sie die Standorte für die Läger in den Grunddaten der Artikel.