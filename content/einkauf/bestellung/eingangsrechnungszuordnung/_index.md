---
categories: ["Bestellung"]
tags: ["Eingangsrechnungszordnung"]
title: "Eingangsrechnungszordnung"
linkTitle: "ER-Zuordnung"
date: 2023-01-31
weight: 400
description: >
  Eingangsrechnungen aus dem Wareneingang fast automatisch erzeugen
---
Eingangsrechnungszuordnung
==========================

Diese Funktion ist für die Zuordnung von Eingangs-Sammel-Rechnungen, die du z.B. monatlich vom Lieferanten erhältst gedacht. Für die automatische Erzeugung von einzelnen Eingangsrechnungen direkt aus dem Wareneingang, siehe dort.

Aus den vollständig verbuchten Wareneingängen kann, wenn alles stimmt, der Wert der Eingangsrechnung errechnet werden. D.h. wenn gelieferte Mengen, Anlieferpreise, Fixkosten, Transportkosten richtig erfasst wurden, so ergibt sich daraus der Nettowert der Eingangsrechnung.

Ist nun beim Lieferanten zusätzlich der default Mehrwertsteuersatz definiert, so kann daraus auch der Bruttowert errechnet werden.

Wurde zusätzlich beim Lieferanten auch noch das Warenkonto, also die Standardkontierung für die Eingangsrechnungen sowie die Kostenstelle definiert, so kann fast vollautomatisch eine Eingangsrechnung aus diesem Wareneingang erzeugt werden.

#### Wie wird für die Überleitung des Wareneingangs in eine Eingangsrechnung vorgegangen?
1. Tragen Sie alle oben beschriebenen Preise richtig in den Wareneingang bzw. in die Wareneingangspositionen ein.
2. Im Modulreiter Wareneingang ist wenn 1\. erfüllt ist, der Button Eingangsrechnung ![](ER_Zuordnung1.gif) freigeschaltet. Zur Zuordnung klicken Sie auf diesen Button.
3. Nun erscheint die Auswahlliste der Eingangsrechnungen dieses Lieferanten, genauer gesagt, der bei dieser Bestellung hinterlegten Eingangsrechnungen der Rechnungsadresse dieser Bestellung.<br>![](ER_Zuordnung2.gif)
4. Wählen Sie hier nun die eventuell bereits angelegte Eingangsrechnung mittels Doppelklick (oder Enter) aus oder legen Sie mit Neu ![](ER_Zuordnung3.gif) eine neue Eingangsrechnung mit den aus der Bestellung übernehmbaren Daten an.
5. Erscheint die Meldung:<br>
![](ER_Zuordnung6.gif)<br>
So definieren Sie bitte für diesen Lieferanten den Mehrwertsteuersatz ![](ER_Zuordnung7.gif) in den Kopfdaten des Lieferanten.
Als Lieferant wird die bei der Bestellung hinterlegte Rechnungsadresse verwendet.
6. Ist der Mehrwertsteuersatz beim Lieferanten definiert, so wird das verfügbare **Kieselstein ERP** Fenster zweigeteilt. In der linken Hälfte wird der Wareneingang der Bestellung dargestellt, in der rechten Hälfte die neu angelegte Eingangsrechnung, mit den vorbesetzten Werten.
7. Ergänzen und vervollständigen Sie nun die [Eingangsrechnung](../Eingangsrechnung/) und klicken Sie danach auf speichern ![](ER_Zuordnung8.gif).
8. Klicken Sie nun erneut im linken Fenster, also in der Detaildarstellung des Wareneingangs, auf Eingangsrechnung und wählen Sie die soeben erstellte Eingangsrechnung aus, und speichern Sie diese Änderung des Wareneingangs durch Klick auf die Diskette, oder Strg+S ab.

Zusätzlich:

Bei der Auswahl der zuzuordnenden Eingangsrechnung stehen noch folgende Möglichkeiten zur Verfügung:

![](ER_Zuordnung4.gif) ... Damit kann die ausgewählte Eingangsrechnung direkt geändert werden.

![](ER_Zuordnung5.gif) ... Löschen der Zuordnung der Eingangsrechnung zum Wareneingang.

#### Die Eingangsrechnung kann nicht angeklickt werden. Der zugehörige Knopf ist deaktiviert.
![](WE_Eingangsrechnung3.gif)

Damit die Zuordnung zur Eingangsrechnung hergestellt werden kann, ist es erforderlich, dass alle Positionen des Wareneingangs auf Ihre Einkaufspreise überprüft wurden. Dies wird dadurch dargestellt, dass für jede Wareneingangspostion der Preis als erfasst bestätigt werden muss. D.h. für jede Wareneingangsposition muss Preise erfasst angehakt sein ![](WE_Eingangsrechnung1.gif).

Um dies für alle Positionen eines Wareneingangs mit einem Klick zu bestätigen, klicken Sie bitte auf ![](WE_Eingangsrechnung2.gif).

#### Bei der nachträglichen Erfassung der Transportkosten auf einem Wareneingang erscheint ein Hinweis. Was bedeutet dieser?
Der angeführte Hinweis bedeutet:

![](BS_Erledigung_Aufheben.jpg)

1. Dass Ihre Bestellung bereits vollständig erledigt ist, da z.B. bereits alle Positionen als Preise erfasst gebucht sind und alle Mengen erledigt sind. Mengen erledigt bedeutet ausreichend oder mehr geliefert oder die Menge manuell auf erledigt gesetzt, da Unterlieferung akzeptiert wurde.
2. Bestätigen Sie die Meldung mit Ja um die Transportkosten eingeben zu können.

**Wichtig:** Durch die Erfassung der Transportkosten ändern sich die Einstandspreise der einzelnen Positionen der Bestellung und damit die Gestehungspreise der Artikel.

Eine eventuell damit verknüpfte Bestellung wird dadurch Eingangsrechnung **nicht** verändert.

Vielmehr müssen die zusätzlichen Transportkosten, wenn diese in der eigentlichen Eingangsrechnung nicht enthalten sind, noch zusätzlich nacherfasst werden. Dies ist z.B. dann der Fall, wenn die Transportkosten von einer Spedition im nachhinein verrechnet werden.