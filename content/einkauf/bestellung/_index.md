---
categories: ["Bestellung"]
tags: ["Bestellung, Wareneingang, Bestellvorschlag"]
title: "Bestellung"
linkTitle: "Bestellung"
date: 2023-01-31
weight: 200
description: >
  Verwaltung der Bestellungen, Rahmen und Abrufbestellungen, Wareneingänge, Bestellvorschlag
---
## Statusübergänge in der Bestellung

![](Bestellung.png)

| Legende | Beschreibung |
| --- | --- |
| WEP | Wareneingangsposition |
| AB | Lieferanten Auftragsbestätigung |
| BE-POS | Bestellpositionen |

[Trennung und Steuerung für mehrere Standorte]( {{<relref "/einkauf/bestellung/standorte">}} )

Anlegen einer Bestellung

Um eine neue Bestellung anzulegen gehen Sie bitte wie folgt vor:
Klicken Sie auf das Modul Bestellung ![](bestellung.JPG)  in der nun offenen Auswahl klicken Sie auf ![](datenausanfrage.JPG) Daten aus einer bestehenden Anfrage übernehmen.

Wenn Sie eine Bestellung ohne Anfrage-Bezug erstellen wollen, so klicken Sie auf Neu ![](neu.jpg).

Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den Lieferanten, Projekt, Bestellnummer, Termine - je nach Bedarf.

Mit einem Klick auf Speichern ![](speichern.JPG) bestätigen Sie die Eingaben.

Wechseln Sie in den Reiter Positionen und geben hier die Positionen der Bestellung ein. 
Um eine neue Position hinzuzufügen klicken Sie auf ![](neu.jpg) Neu wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten]( {{<relref "/start/04_allgemeine_stammdaten/#positionsarten">}} ) ), geben die Informationen (u.a. Menge) ein und klicken auf ![](speichern.JPG) Speichern. Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.

Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Konditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für diese eine Bestellung ab. 

Mit Klick auf ![](drucken.JPG) Drucken (oder oberes Menü Bestellung - Drucken...) erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG) , dem Versenden per E-Mail ![](e-mail.jpg) oder speichern ![](speichern.JPG), aktivieren Sie die Bestellung, der Status, siehe oben, ändert sich von angelegt in offen. 

#### Wie wird aus einer Bestellung mit Status "angelegt" eine mit dem Status "offen"?
Der Statusübergang von angelegt nach offen geschieht immer durch den Aufruf der Funktion Drucken dieses Beleges. Siehe dazu auch Statusübergänge.

#### Wie kann ich eine erledigte Bestellung wieder nach angelegt bringen?
Manchmal ist es erforderlich eine bereits (automatisch) vollständig erledigte Bestellung wieder zu ändern.<br>
Hinweis: Das bedeutet zugleich, dass ja bereits alle Positionen der Bestellung als vollständig geliefert verbucht sind.<br>
Der einfachste Weg ist in einen der Wareneingänge zu gehen und bei einer der Positionen das Preise erfasst zu entfernen. ![](Preise_erfasst.png)<br>
Gegebenenfalls muss du dafür vorher im Reiter Wareneingang auch noch die Zuordnung zur Eingangsrechnung entfernen ![](WE_ER_Zuordnung.png)
Damit erreichst du, dass in den Kopfdaten der Button ![](neuen_Wareneingang_buchen.png) für einen weiteren Wareneingang freigeschaltet wird. ![](neuen_wareneingang_anlegen.png)<br>
Mit ok bist du direkt im Reiter Wareneingangspositionen und kannst weitere Warenzubuchungen vornehmen und gegebenenfalls auch überliefern.<br>
Eine Änderung der Bestellpositionen, wenn die Bestellung vollständig geliefert wurde ist nur über einen kleinen Trick, der manchmal sehr mühsam sein kann, möglich.<br>
Reduziere einfach die zugebuchte Menge um eine Kleinigkeit. Damit wandert die Bestellung in den Status offen.<br>
**Anmerkung:** Wenn es nur darum geht, eine weitere Position zu einer bereits vollständig gelieferten Bestellung hinzuzufügen, ist es einfacher / übersichtlicher eine weitere Bestellung anzulegen. Man kann dafür auch die Bestellung im Reiter Auswahl kopieren.

#### Welche Lieferadresse wird in der Bestellung angegeben?
Wird in einer Bestellung keine Lieferadresse angegeben, so wird (vom Lieferanten) die Ware an die Adresse des Kunden gesendet, da auf Ihren Bestellungsausdrucken Ihre Adresse angegeben ist. Nur wenn eine zusätzliche Lieferadresse angegeben wurde, wird der Lieferant dies an diese Adresse senden. Bitte bedenken Sie, dass die Lieferadresse immer die Adresse des Warenempfängers ist, die üblicherweise die Adresse eines Ihrer Kunden ist.

#### Was ist die Lieferadresse in der Bestellung?
Die Lieferadresse ist jene Adresse an die die Ware vom Lieferanten gesandt wird. Üblicherweise ist dies die Adresse des Mandanten.<br>
Wenn es erforderlich ist, dass die Ware direkt zum Kunden z.B. an eine Baustelle (Handadresse) gesandt werden muss, dann muss die Lieferadresse angegeben werden.

#### Welche Rolle spielt der Liefertermin in der (dazugehörenden) Bestellung?
Der angegebene Liefertermin (eventuell pro Position) ist mein Wunschliefertermin für diese Position / für diese Bestellung. Zu diesem Zeitpunkt sollte die Ware beim Anwender eintreffen, bzw. bei der angegebenen Lieferadresse.<br>
Zu diesem Termin erwarte ich die Ware im Haus. Der Liefertermin wird durch den [bestätigten Liefertermin](#bestätigter Liefertermin) übersteuert.<br>
Von der Gewichtung her ist der, anhand einer Auftragsbestätigung des Lieferanten, eingepflegte Liefertermin wertvoller / verbindlicher, als der vom Anwender angegebene Wunschliefertermin. 

#### Wie lange kann der Liefertermin geändert werden?
Wenn in einer Bestellung bereits Positionen erfasst sind, so kann der Liefertermin der Bestellung nicht mehr verändert werden. Beachten Sie bitte insbesondere bei Abrufbestellungen, dass daher der Liefertermin vor der ersten Erfassung der Positionen, ev. in Sicht Rahmen, richtig eingestellt ist. In aller Regel werden Sie dies beim ersten Speichern bereits einstellen.

#### Wie wird der Vorschlagswert für den Liefertermin errechnet?
Dieser ist das aktuelle Datum plus der im Parameter DEFAULT_LIEFERZEITVERSATZ angegebenen Anzahl von Tagen. Wird dieser Parameter auf -1 gesetzt, so wird der Liefertermin in den Kopfdaten nicht vorbesetzt. D.h. es muss der gewünschte Liefertermin jedes Mal manuell eingegeben werden. Gerade bei ständig wechselnden Terminen ist dies unter Umständen ein Vorteil.

#### Bezug zu einem Kundenauftrag
In den Bestellungskopfdaten kann auch der Bezug zu einem Kundenauftrag eingegeben werden. Mit dem Parameter BESTELLUNG_AUFTRAGSBEZUG_PFLICHTFELD kann erzwungen werden, dass in den Kopfdaten immer ein Auftragsbezug hergestellt werden muss.

**Hinweis:**
Bitte beachte, dass dies <u>nur</u> eine Zusatzinformation ist. Die Verwendung der bestellten Artikel wird in **Kieselstein ERP** nach dem terminlichen Bedarf gesteuert.

**Hinweis2:**
Bei der Überleitung des Bestellvorschlages in einzelne Bestellungen wird derzeit keine Auftragszuordnung übernommen.

#### Kann ich auch in anderen Mengeneinheiten bestellen?
Ja, wenn im Artikel die sogenannte Bestellmengeneinheit definiert ist. Siehe [dazu]( {{<relref "/docs/stammdaten/artikel/mengeneinheit/#bestellmengeneinheiten" >}} )

#### Wir bekommen die Artikel Stückweise angeliefert, verwalten diese aber in Meter und haben die Preise in m³, kg. Wie geht das?
<a name="Meter, kg, Stück gleichzeitig erfassen"></a>
In manchen Branchen werden die Artikel in m (Meter) verwaltet, in der Stückliste in mm geplant. Die Einkaufspreise in m³ oder kg angegeben und trotzdem sollte man, speziell beim Wareneingang nur Stück, von Stangen oder Tafeln, angegeben. Wie ist dies zu definieren?
Stellen Sie den Parameter VERPACKUNGSMENGEN_EINGABE auf 1.<br>
Definieren Sie den Artikel z.B. als lagergeführte Mengeneinheit in m. Damit können in den Stücklisten auch mm angegeben werden, mit oder ohne Längen / Dimensionen.
Definieren Sie den Umrechnungsfaktor der Bestellmengeneinheit von m auf kg oder m³.
Nun können im Artikellieferanten m oder kg Preise angegeben werden.<br>
Definieren Sie, wenn die Umrechnung im Einkauf erfolgen sollte, die Verpackungseinheit im Artikellieferanten.<br>
Wenn die Umrechnung auch im Verkauf erfolgen sollte, definieren Sie die Verpackungsmenge im Reiter sonstiges.

Bei der Erfassung der Positionen steht nun für die Artikel / Artikellieferanten auch die VPE (Verpackungseinheit) als Erfassungswert zur Verfügung.<br>
Gegebenenfalls können für Einkauf und Verkauf verschiedene Verpackungseinheiten definiert werden.

Somit erhalten Sie z.B. folgende Erfassungsmaske:
![](Verpackungsmengen_Erfassung.jpg)
Beachten Sie bitte, dass die Berechnungsbasis immer noch die definierte lagergeführte Mengeneinheit ist, in diesem Beispiel m (Meter).<br>
Die Erfassung der VPE (Stückzahl) wird sofort in die Menge übertragen. Zusätzlich kann noch eine sogenannte Einzelmenge definiert werden, um trotz allem nicht zu den Verpackungseinheiten passende Mengen bewirtschaften zu können.

Im Bestellmodul stehen im Wareneingang nun alle drei Mengeneinheiten zur Verfügung:
![](Verpackungsmengen_Erfassung_in_WEP.jpg)
- Die Menge auf Basis der lagergeführten Mengeneinheit (Meter) (Neu gelieferte Menge).
- Die Menge der VPE (Stückzahl) z.B. Anzahl der Stangen.
- Die alternative Bestellmengeneinheit (alt.BS.Mng.Eh.) in diesem Falle kg und
es kann der Preis zur Basis der lagergeführten Mengeneinheit oder zur Basis der alternativen Bestellmengeneinheit eingegeben werden.

Sollte in Ihrer **Kieselstein ERP** Installation anstatt VPE Karton angezeigt werden, bitten Sie Ihrem **Kieselstein ERP** Betreuer um Anpassung in der LP_TEXT (Übersteuerung des Lables).

#### Kann auch in Abmessungen bestellt werden?
<a name="Dimensionen / Zuschnitt bestellen0"></a>
Ja. Um diese Funktion der Zuschnitt-Artikel zu verwenden, stellen Sie bitte den Parameter DIMENSIONEN_BESTELLEN auf 1.
Dies bewirkt, dass bei der Bestellung eines Artikel mit einer Mengeneinheit die eine Dimension hat, nach den Abmessungen des zu bestellenden Artikels gefragt wird. Ist nun ein Artikel mit diesen Abmessungen bereits vorhanden, so wird dieser Artikel verwendet, anderenfalls wird er neu angelegt.
Die Neuanlage des Artikels ist so aufgebaut, dass
bei Artikelmengeneinheiten mit Dimension 1, die Länge in mm erfasst wird und als Ergänzung der Artikelnummer verwendet wird
bei Artikelmengeneinheiten mit einer Dimension größer 1, wird Breite, Höhe und gegebenenfalls Tiefe erfasst und nach einem Artikel mit der Basisartikelnummer und den eingegebenen Abmessungen gesucht. Wird dieser nicht gefunden, so wird ein neuer Artikel mit einer vierstelligen laufenden Nummer erzeugt.
Wird diese Funktion genutzt, so empfiehlt sich auch den Parameter ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE einzuschalten.

Sollte trotz allem der Original Artikel bestellt werden, so klicken Sie bitte auf abbrechen (Originalartikel).

Hinweis:
Sollten beim Originalartikel Einkaufsmengenstaffeln hinterlegt sein, so werden diese NICHT in den abmessungsspezifischen Artikel übernommen.

#### Können auch Initiale Kosten bei den Dimensionsartikeln berücksichtigt werden?
Ja. im Artikellieferanten können auf der Höhe der Fixkosten bei aktiviertem Parameter DIMENSIONEN_BESTELLEN auch die Initialen Kosten definiert werden.
![](Inital_Kosten.gif)
Das bedeutet, dass der Einkaufspreis des Zukaufteiles über die Länge/Fläche errechnet wird und dann zusätzlich pro Stück diese Initialen Kosten dazugerechnet werden. Damit können auch Längenabhängige Bearbeitungskosten abgebildet werden.

#### Wie können die Einkaufspreise der Dimensionen-Artikel gepflegt werden?
Hintergrund:
Trotz der Vielzahl an Artikel, und ausgehend davon dass die Preise, unter Berücksichtigung der Initialen Kosten, für alle aus dem Basisartikel abgeleiteten Zuschnittsartikel gleich sind, sollten, gerade bei der aktuellen Volatilität der Aluminium- und Stahl-Einkaufspreise komfortabel gepflegt werden können.
Daher steht auch eine erweitere Einkaufspreisepflege für die Dimensionen-Artikel zur Verfügung.
Beispiel:
Basisartikel: A-P-001
Zuschnittsartikel: A-P-001_7600
Das bedeutet, der Basisartikel ist in m (Meter) definiert, der Zuschnittsartikel in Stk (Stück), wobei seine Abmessungen, in diesem Falle ist es Stangenmaterial, also die Breite in mm angegeben ist.
Wird nun der Einkaufspreis am Basisartikel geändert, so erscheint die Frage
![](Zuschnittsartikel_aktualisieren.jpg)
Mit Abbrechen bleiben Sie im Erfassungsdialog,
mit Nein wird nur der Einkaufspreis des Basisartikels geändert,
mit Ja werden alle Artikel deren Artikelnummer der Artikelnummer des Basisartikels ergänzt um _ (Unterstrich = die Trennung von der Basis Artikelnummer zur Dimension) um den neuen Einkaufspreis ergänzt. Es wird das gleiche Preisgültigkeitsdatum wie beim Basisartikel gesetzt und der gleiche Lieferant.

Da oft die Preispflege vom Zuschnittsartikel zum Basisartikel eher der Praxis entspricht, wird auch diese Funktion unterstützt. D.h. wenn Sie nun, um beim obigen Beispiel zu bleiben, den Einkaufspreis des Zuschnittsartikels A-P-001_7600 verändert, erscheint die Frage
![](Basis_und_Zuschnittsartikel_aktualisieren.jpg)
ob auch die Einkaufspreise des Basisartikels und der anderen Zuschnittsartikel aktualisiert werden sollten.
Mit Ja wird dies durchgeführt.

Diese Funktionalität, Rückpflege der Einkaufspreise über den Zuschnittsartikel, steht auch aus der Wareneingangsposition ![](Artikellieferantenpreis_aus_geliefert_Preis_aktualisieren.gif) gelieferter Preis als Einkaufspreis zurückpflegen zur Verfügung.
Bitte beachten Sie, dass die Rückpflege des Staffelpreises ![](Artikellieferantenstaffelpreis_aus_geliefert_Preis_aktualisieren.gif) nur den Staffelpreis des Zuschnittsartikels aktualisiert und keine Wirkung auf den Basisartikel hat.

**Wichtig1:** Da die Beziehung zwischen Basis-Artikel und Zuschnittsartikel rein über die Artikelnummer gegeben ist, müssen strukturierte Artikelnummern gegeben sein. A_.... versus A-
Die Reihenfolge der Artikellieferanten wird:

-   vom Basisartikel ausgehend, wenn der bevorzugte Lieferant (Lief1Preis) geändert wird, so wird dies auch in den Zuschnittsartikeln so eingetragen.
    Ist der geänderte Einkaufspreis des Artikellieferanten nicht der bevorzugte Lieferant, so wird dieser neue Eintrag bei den Zuschnittsartikeln hinten angefügt.

-   Vom Einkaufsartikel aus, wird die Reihung der Artikellieferanten **nicht** verändert.

Die Preispflege wird nur bei der Änderung der Preise durchgeführt. Das bedeutet, wenn Sie einen neuen Lieferanten für Ihren Basisartikel definieren, so wird dessen Einkaufspreis nicht in die Zuschnittsartikel übernommen. Wenn diese neuen Preise z.B. als bevorzugter Lieferant auch bei den Zuschnittsartikeln eingetragen werden sollte, so reihen Sie zuerst den neuen Artikellieferanten an die oberste Stelle und wählen dann Ändern und speichern die gleich gebliebenen weiteren Daten ab. Die beiden Frage, Einkaufspreis belassen und Preise der Zuschnittsartikel aktualisieren, beantworten Sie wunschgemäß. Bei Ja, Ja werden nun die neuen Einkaufspreis auch bei den Zuschnittsartikeln eingetragen.

#### Der angegebene Rabatt wird automatisch verändert.
Siehe dazu bitte [Verwendung der Preise und Rabatte]( {{<relref "/verkauf/rechnung/preise_in_der_rechnung">}} ).

#### Kann der Bestellpreis automatisch in den Artikel zurückgepflegt werden?
<a name="Bestellpreis zurückpflegen"></a>
Ja. Beachten Sie dazu die Einstellung der Auswahl: Preise für den Lieferanten nachpflegen.

![](Bestellpreis_zurückpflegen.gif)

Hier gibt es drei Möglichkeiten:
1. Nicht nachpflegen
2. Einzelpreis
3. als Staffelpreis unter Angabe der gewählten Staffelmenge. Bei Angabe der Positionsmenge wird diese Menge für die Staffelmenge verwendet und gegebenfalls als neuer Staffelmengeneintrag in die Einkaufsstaffel des Artikellieferanten eingetragen.

Die Basiseinstellung ob die Preise zurückgepflegt werden oder nicht, sind von der Einstellung des Parameters DEFAULT_EINKAUFSPREIS_ZURUECKPFLEGEN abhängig. 
- 0 ... nicht zurückpflegen
- 1 ... immer zurückpflegen mit automatischer Erkennung ob Staffelpreise rückpflegen oder nicht
- 2 ... immer nur als Einzelpreis zurückpflegen

Info: werden die Einzelpreise zurückgepflegt, so werden auch die Fixkosten, die Mindestmenge und die Verpackungseinheit gegebenenfalls in den Artikellieferanten nachgepflegt.

#### Was bedeuten die Felder im unteren Bereich der Bestellpositionen?
Mit diesen Feldern erhalten Sie zusätzliche Infos über den Artikel, weshalb diese auch mit Akt. LF-Daten (aktuelle Lieferanten Daten) gekennzeichnet sind.

![](Bes_Position.gif)

| Feldname | Bedeutung |
| --- |  --- |
| Fixkosten | Fixkosten auf dieser Position. Diese werden aufgrund des Artikellieferanten vorgeschlagen. |
| Verpackungseinheit | Hier wird die beim Artikellieferanten hinterlegte Verpackungseinheit angezeigt (diese kann auch bei der Bestellung angedruckt werden) |
| Mindestmenge | Hier sehen Sie die im Artikellieferanten definierte Mindestmenge. |

#### Können auch Transportkosten erfasst werden?
Im Reiter Konditionen können die geplanten Transportkosten zu einer Bestellung erfasst werden. Diese werden am Ende des Ausdruckes der Bestellung mit angeführt. Wird für diese Bestellung ein Wareneingang angelegt, so wird der jeweils offene Betrag der geplanten Transportkosten als noch einzubuchender Transportkostenbeitrag vorgeschlagen.

Über das Menü Bearbeiten, Transportkosten ändern, können die Transportkosten nachträglich geändert werden. Dazu ist das Recht BES_BESTELLUNG_CUD notwendig.

#### Es sollten die Transportkosten einer erledigten Bestellung geändert werden. Geht das?
Um Transportkosten einer erledigten Bestellung ändern zu können, ist das Recht BES_WE_KOSTEN_IM_ERLEDIGT_STATUS_AENDERN erforderlich.
**Hinweis:** Diese Recht ist per Default keiner Rolle zugeordnet.
Hat der Anwender (die Systemrolle) dieses Recht, so können, auch bei Bestellungen im Status erledigten / geliefert, durch Klick auf den
![](Transportkosten_Waehrungsumrechnung.jpg)
Taschenrechner für die Transportkosten-Währungserfassung, die Transportkosten verändert werden.

#### Wie erhalte ich schnell einen Überblick über die Warenbewegungen und den Bedarf der zu bestellenden Artikel?
Markieren Sie eine Zeile in den Bestellpositionen, mit Klick auf das Icon Monatsstatistik ![](Monatsstatistik_anzeigen.JPG) wird die Statistik über Warenzu- und abgänge dieses Artikels aufgerufen und angezeigt.

#### Wie kann ein anderer Staffelpreis verwendet werden?
wählen Sie neben dem Punkt als Staffelpreis die gewünschte Staffelmenge aus der Combobox aus ![](Staffelpreis_auswaehlen.gif). Mit Klick auf den Button ![](Staffelpreis_uebernehmen.gif) wird der zur Staffelmenge gehörende Preis in den Bestellpreis übernommen.

#### Welche Rolle spielt das LS-Datum im Reiter Wareneingang in der (dazugehörenden) Bestellung?
Dies ist das Lieferscheindatum auf dem Lieferpapier des Lieferanten, welches Basis für den Wareneingang ist.

#### Welche Rolle spielt das WE-Datum im Reiter Wareneingang in der (dazugehörenden) Bestellung?
Das ist das Datum der Wareneingangsbuchung. Mit diesem Datum werden die in den Wareneingangspositionen angegebenen Waren auf Lager gebucht.

#### Wo sehe ich welche Wareneingangsposition zu welchem Wareneingang gehört ?
Wählen Sie im Reiter den gewünschten Wareneingang aus.
Wechseln Sie nun auf Wareneingangsposition.
Hier sehen Sie nun alle Positionen der Bestellung und deren Wareneingangsinformationen. Zusätzlich wird im Detail die Lieferscheinnummer ![](WE_Position1.gif) dieses Wareneingangs angezeigt.

Die Spaltenüberschriften der Wareneingangsposition bedeuten:
![](WE_Position2.gif)

| Spaltenüberschrift | Bedeutung |
| --- |  --- |
| Offene Menge | Gesamt offene Menge dieser Wareneingangsposition |
| Gel. WE-Menge | Mit diesem Wareneingang gelieferte Menge |
| Bisherige WEPOS-Liefermenge | Insgesamt bis zu diesem Wareneingang inklusive gelieferte Menge der Bestellposition |
| BSPOS-Menge | Gesamt Bestellte Menge der Bestellposition |
| Bezeichnung | Bezeichnung des Artikel, der Handeingabe |
| Gel.WE-Preis | Geliefert-Preis der WE-Position |

Bedeutung der Preis-Bezeichnungen im Wareneingang:

![](WE_Position3.gif)

| Feldname | Bedeutung |
| --- |  --- |
| Gelieferter Preis | Preis zu dem die Ware angeliefert wurde, z.B. laut Eingangsrechnung |
| Fixkosten | Fixkosten für diese Wareneingangsposition |
| Einstandspreis | (gelieferter Preis * gelieferter Menge + anteilige Transportkosten + Fixkosten)<br>-------------------------------------------------------------------------<br> gelieferte Menge
| Bestellnettopreis | Zu diesem Preis wurde die Bestellposition, dieser Wareneingangsposition bestellt|

#### Wie kann ich zwischen einzelnen Wareneingängen wechseln?
Wechsle durch Klick auf die Pfeile ![](wechsel_wareneingaenge.JPG) im Reiter Wareneingangspositionen zwischen den einzelnen Wareneingängen wechseln.

Zur Information, auf welchen Wareneingang sich die Wareneingangspositionen beziehen, welcher Wareneingang gerade angezeigt wird, finden Sie in der Mitte der Darstellung eine Zeile mit Informationen zum Wareneingang wie zum Beispiel  ![](info_wareneingang.JPG). 

#### Kann der gelieferte Preis als Einzel-/Staffelpreis sofort beim jeweiligen Artikellieferant zurückgepflegt werden?
Ja, verwenden Sie dazu die Icons ![](rueckpflegen_wareneingang.JPG) um den gelieferten Preis als Einzelpreis (links) bzw. als Staffelpreis (rechts) beim Artikellieferanten zu hinterlegen. Das Belegdatum wird als Gültig ab Datum eingetragen. Wenn der Artikellieferant im Artikel noch nicht hinterlegt ist, so wird auch dieser bei der Rückpflege des Preises als Artikellieferant angelegt. 
Wenn ein Preis aus der WEP zurückgepflegt wird und der Einzelpreis in der Bestellposition 0 ist, dann wird anstelle des Einzeplreises der Nettopreis verwendet. Weiters kommt in der EK-Staffel eine Warnung, wenn der Einzelpreis sowohl NULL als auch 0 ist.

#### Kann der Wert der Position des Wareneingangs erfasst werden?
In dem Feld Gelieferter Preis können Sie den Preis für 1 Stück eingeben. Im Feld Gelieferter Wert können Sie für die Menge der gelieferten Menge (im Bild 10 Stk) den Gesamtpreis der Lieferungsposition eingeben. Besonders hilfreich ist diese Funktion bei Artikel, die kleine Preise, Kommazahlen und hohe Bestellmengen besitzen.<br>
Da Lieferanten oft falsche Einzelpreise angeben, ist es möglich sein, bei der Wareneingangsposition für die eine Zulieferung den (Zeilen-) Gesamtpreis anzugeben.<br>
Dieser wird dann durch die Liefermenge dividiert und somit ergibt sich der Geliefert Preis, welcher abgespeichert wird.

![](wep_preis.JPG)

#### Wie hängen Wareneingang, Wareneingangsposition und Bestellposition zusammen?
Beim Wareneingang geht es grundsätzlich darum, dass es immer wieder zu Teillieferungen auf einige / alle Bestellpositionen kommt. Daher ist es erforderlich, dass jeder Wareneingang auf jede Bestellposition buchen kann.<br>
Wir haben dies daher so gelöst, dass in den Wareneingangspositionen (WEPOS) immer alle Mengenbehafteten-Bestellpositionen (siehe dazu auch den Parameter IN_WEP_TEXTEINGABEN_ANZEIGEN) angezeigt werden.<br>
Das bedeutet, wenn Sie z.B. wissen möchten mit welchem Wareneingang eine Bestellposition angeliefert wurde, so wählen Sie einen Wareneingang aus und wechseln dann auf Wareneingangsposition. Alle Zeilen, bei denen bei Gel. WE-Menge Einträge sind, wurden mit diesem Wareneingang geliefert.

#### Beim Wareneingang wird ein Lager vorgeschlagen, woher kommt dieses?
Das beim Wareneingang vorgeschlagene Lager ergibt sich in der Regel aus der Lieferadresse aus den Kopfdaten der Bestellung. Hier greift nun das "zu Lager".<br>
Wenn in Ihrer Installation die Funktion Standortverwaltung aktiviert ist, welche mit dem Parameter LAGERMIN_JE_LAGER = 1 eingeschaltet wird, so ist der Lagervorschlag im Wareneingang mit dem Standort gekoppelt.<br>
Das bedeutet, dass:
- Gibt bereits Wareneingänge, so wird das Lager des ersten Wareneingangs vorgeschlagen
- Gibt es noch keine Wareneingänge, so:
  - Wenn der Parameter LAGERMIN_JE_LAGER = 0 steht, so kommt das Lager aus dem Zubuchungslager aus den Konditionen des Lieferanten
  - Wenn der Parameter LAGERMIN_JE_LAGER = 1 steht, so kommt das Lager aus dem Standort der Lieferadresse der Bestellung.<br>
  Gibt es nun mehrere gleiche Läger, welche diesem Standort zugeordnet sind, so wird derzeit das erste gefundene verwendet.
  Konnte kein Lager ermittelt werden, so wird wiederum das Zubuchungslager des Lieferanten verwendet.

#### Wie finde ich den Wareneingang eines Wareneingangs-Lieferscheines?
Manchmal sucht man, mit welcher Bestellung, mit welchem Wareneingang welcher Lieferantenlieferschein zugebucht wurde.<br>
Seit Version 0.2.0 steht diese Möglichkeit in der Auswahlliste der Bestellungen im Zusatzfilter ![](Zusatzfilter.png) zur Verfügung.<br>
Wähle hier das Kriterium<br>
![](Kriterium_auswaehlen.png)<br>
Lieferscheinnummer und klicke anschließend auf das grüne Plus<br>
Nun die gewünschte Lieferscheinnummer eingeben. Mit dem Klick auf aktualisieren ![](aktualisieren.png) wird dieser "Text" innerhalb des Feldes der erfassten Lieferscheinnummer gesucht.

#### Wie kann ich einer Bestellung eine Eingangsrechnung zuordnen?
Siehe bitte [Eingangsrechnungszuordnung](#Eingangsrechnungszuordnung).

#### Wie wirkt der allgemeine Rabatt in Bezug auf den zurückgepflegten Lieferantenpreis.
Dieser wird beim Rückeintragen nicht berücksichtigt.

#### Können Bestellpositionen mit negativen Mengen eingegeben werden?
Nein. Sie erhalten ja von Ihrem Lieferanten keine Gutschrift, wenn Sie ihm Ware abkaufen.

Manche Anwender wollten mit diesen Eingaben die Rabatte auf Bestellpositionen und vor allem auf eine gesamte Bestellung darstellen. Verwenden Sie dafür bitte den allgemeinen Rabatt in den Konditionen.

Begründung:

Der allgemeine Rabatt wird auch in den Wareneingang übertragen und dort vorgeschlagen. Somit wirkt er bei der Warenzubuchung und es ergeben sich somit geänderte Einstandspreise, was ja für die Deckungsbeitragsrechnung Ihres Unternehmens wesentlich ist.

#### Können Bestellpositionen mit negativen Preisen eingegeben werden?
Ja. Dies wird gerne für Preisreduktionen welche transparent dargestellt werden müssen verwendet.
Bedenken Sie bitte dass dies unter Umständen aus Sicht der Erfolgsrechnung, der Deckungsbeitragsbetrachtung nicht durchgängig ist.
Es bewirkt auch, dass negative Werte in Ihrem Lager liegen könnten, wenn der Reduktionsartikel nicht umgehend wieder ausgeliefert / vom Lager abgebucht wird.

#### Was hat die Rechnungsadresse für eine Bedeutung?
Dies ist die Adresse des rechnungslegenden Lieferanten.
Beispiel:
Die Bestellung geht an einen externen Mitarbeiter des Lieferanten, welcher in Wien sitzt.
Die Lieferung erfolgt von Eindhoven (Holland) aus.
Um nun die Überleitung der Bestellung, über den Wareneingang, in die Eingangsrechnungsverwaltung richtig zu machen (EU-Ausland), muss die Rechnungsadresse auf den tatsächlich Beliefernden definiert sein.

#### Kann die Rechnungsadresse geändert werden?
Sobald ein Wareneingang erfasst ist, können die Kopfdaten der Bestellung nicht mehr geändert werden. Wenn Sie jedoch erst bei der Rechnung bemerken, dass der Lieferant eine andere Rechnungsadresse besitzt, so finden Sie im Menü Bearbeiten den Punkt Rechnungsadresse ändern. Hiermit können Sie die Rechnungsadresse ändern solange bis Preise im Wareneingang erfasst sind.

#### Wareneingangskommentar?
Der Wareneingangskommentar dient der Kommunikation zwischen den buchenden Personen und z.B. den Mitarbeitern der Buchhaltung. Sie können mit dieser Funktion für jede Wareneingangsposition einen Kommentar, also eine Bemerkung zu dieser Position hinterlegen. Um damit verschiedene Informationen für später oder eben andere Benutzer festzuhalten. Wenn auf einer Wareneingangsposition ein Kommentar hinterlegt ist, wird dies in der linken Spalte gekennzeichnet.

![](WEP_Kommentar.jpg)

#### Wie werden Transportkosten richtig erfasst?
Transportkosten zu Wareneingängen sollten, auch wenn sie oft als Position mitbestellt werden (müssen), nicht als Handeingabe oder Artikel im Wareneingang zugebucht werden, sondern dem Wareneingang als Kostenfaktor zugebucht werden.

Dazu gibt es in den Details des jeweiligen Wareneingangs die Felder 

![](Transportkosten.gif)

TP-Kosten = Transportkosten, Zoll-Kosten, Bankspesen und sonstige Spesen. Als Gesamtheit Anlieferkosten.

Die Erfassung der Anlieferkosten in diesen Feldern bewirkt, dass diese Kosten anteilig auf die Positionen des Wareneingangs aufgeschlagen werden. Die Verteilung der gesamten Anlieferkosten erfolgt nach Positionswert. D.h. die Position mit dem höchsten Wert hat auch die höchsten Anlieferkosten zu tragen. D.h. der Einstandspreis der Positionen erhöht sich um den Faktor der anteiligen Transportkosten (genauer der gesamten Anlieferkosten).

#### Wie werden Transportkosten, Anlieferrabatt und Anlieferpreis berücksichtigt?
#### Wie wird der Einstandspreis ermittelt?
Transportkosten werden anteilig nach Menge und Preis jeder bisher gelieferten Position aufgeteilt. Der Anlieferrabatt wird als Gesamtrabatt auf diese Lieferung (eine Lieferung entspricht einem Wareneingangseintrag) berechnet. D.h. es werden auch die Transportkosten rabattiert.

Ein Beispiel:

10 Stk. Socken à 7 € sind bestellt
20 Stk. Hosen à 20 € sind bestellt
10€ Transportkosten auf diese Lieferung
12% Anlieferrabatt

4 Stk. Socken kommen:

Einstandspreis = (Gelieferter Preis + (Transportkosten/Summe gelieferter Stück aller Positionen)) - Rabatt

Einstandspreis = (7 € + (10 € / 4)) - 12 % = 8,36   

nächste WE Position
5 Hosen
nun die Aufteilung der Transportkosten 
5*10€ (Socken) + 5*20 € (Hosen) = 50 + 100 = 150

Einstandspreis Socken = 10€ + (50/150)*10/5 = 10,6667€ -50% = 5,3333€
Einstandspreis Hosen = 20€ + (100/150)*10/5 = 21,3333€ -50% = 10,6666€

Der Rabatt wirkt also auf Anlieferpreis + anteilige Transportkosten.

Jede Änderung von Transportkosten bzw. Rabatt in den Wareneingangs-Kopfdaten stößt diese Berechnung erneut an.

**Hinweis:** Die Verteilung der Transportkosten erfolgt anhand der Werte jeder Wareneingangsposition. Sind nun Positionen mit Null(0,-) Preisen enthalten, so haben diese keinen Wert, was wiederum bewirkt, dass auch keine Transportkosten darauf gebucht werden. Wir gehen grundsätzlich davon aus, dass der Warenzugang einer Bestellung mit Transportkosten auch entsprechende, den tatsächlichen Kosten entsprechende Preise und somit Werte hinterlegt hat.

#### Können die Transportkosten in einer anderen Währung als die Bestellung erfasst werden?
Sie finden rechts neben dem Feld TP-Kosten (Transportkosten) einen Knopf, der mit der Währung der Bestellung beschriftet ist. ![](Transportkosten_Aendern.gif) Damit gelangen Sie in einen Dialog in dem Sie als Rechenhilfe die Transportkosten in einer anderen Währung und zu einem anderen Stichtag für den Umrechnungskurs als der Wareneingang erfasst werden können.

![](Transportkosten_in_anderer_Waehrung.jpg)

Bitte beachten Sie, dass dies nur eine Rechenhilfe ist. Die Verbuchung der Transportkosten erfolgt immer in der Bestellwährung.

<a name="Transportkosten zuordnen"></a>

#### Muss ich die Transportkosten zusätzlich als Aufwand einem Auftrag zuordnen?
Nein auf keinen Fall.<br>
Wenn die Transportkosten in einem Wareneingang erfasst sind, so werden, siehe oben, die Transportkosten auf die angelieferten Artikel des Wareneingangs verteilt. Das bedeutet, dass die Transportkosten bereits im Gestehungspreis der Artikel enthalten sind.<br>
Würden Sie die Eingangsrechnung der Transportkosten zusätzlich dem Auftrag zuordnen, so wären die Transportkosten doppelt enthalten.<br>
Der Vorteil ist, dass die zusätzlichen Kosten direkt dem einzelnen Artikel des einen Wareneingangs zugeordnet sind. Wenn nun nur Teile der Ware ausgeliefert werden, so werden auch spätere Lieferungen mit den jeweils richtigen Transportkosten belastet.

#### Wie storniert man Bestellungen?
Eine Bestellung wird üblicherweise dann storniert, wenn diese bereits beim Lieferanten ist. D.h. die Bestellung hat Positionen und diese bleiben in der Bestellung erhalten, es wird lediglich der Status auf storniert gesetzt. Zusätzlich muss eine stornierte Bestellung trotzdem ausgedruckt werden können, aber der Vermerk **storniert** oder **Storno** wird gedruckt. Durch das Storno der Bestellung werden alle Positionen dieser Bestellung aus der Bestelltliste entfernt. 

Um nun eine offene Bestellung zu stornieren, gehen Sie bitte in die Kopfdaten der Bestellung, klicken auf ändern und speichern und danach auf stornieren ![](Bestellung_stornieren.gif).

Eine bereits teilgelieferte Bestellung kann nicht mehr storniert werden. Verwenden Sie gegebenenfalls das manuelle Erledigen der gesamten Bestellung bzw. einzelner Bestellpositionen im Wareneingang.

Wenn eine stornierte Bestellung wieder aktiviert werden soll, kann durch ein Update(Ändern/Speichern) in den Kopfdaten der Status auf angelegt zurückgesetzt werden. Durch das Drucken und den Statuswechsel auf offen werden die Artikel wieder in die Bestelltliste aufgenommen.

#### Bestellung kann nicht storniert werden ?
Erklärung / Lösung:
Eine Bestellung die einen Wareneingang hat, auch wenn dies z.B. nur Testweise oder die falsche Bestellung war, kann nicht storniert werden. D.h. wenn die Bestellung trotzdem storniert werden muss, so müssen zuerst alle Wareneingangspositionen gelöscht werden. Dann können die Wareneingänge gelöscht werden und nun kann die Bestellung storniert werden.
Kann eine Wareneingangsposition nicht gelöscht werden, so sollte über das Warenbewegungsjournal des Artikels nachgesehen werden, mit welcher Lieferung der Artikel wieder verbraucht wurde. Dann ist dieser Verbrauch zurückzunehmen und dann die Wareneingangsposition zu löschen.

#### Kann eine stornierte Bestellung wieder auf angelegt gesetzt werden ?
Ja. Wählen Sie im Menüpunkt Bearbeiten, Storno aufheben.

<a name="Wareneingang"></a>

#### Wie wird der Wareneingang / die Anlieferung richtig verbucht?
Im Panel Wareneingang wird die Lieferscheinnummer des Lieferanten eingetragen, der Rabatt kann noch verändert werden. Das Zubuchungs-Lager wird für diesen Wareneingang festgelegt. 
In den Wareneingangspositionen scheinen alle Positionen auf. Diese müssen einzeln verbucht werden, da sich in der Lieferung Abweichungen ergeben können.
Zum richtigen Wareneingang werden die jeweiligen Positionen in der gelieferten Menge verbucht. Das Programm schlägt immer die Restmenge vor, wenn mehr. Wird weniger angeliefert, so verändern Sie bitte die Menge entsprechend.
Der Einstandspreis ergibt sich aus dem Nettopreis - Rabattsätze der Lieferung plus anteilige Transportkosten. Der Nettopreis der Bestellposition wird als Information ausgegeben.

#### Kann man einen weiteren Wareneingang direkt aus der Wareneingangsposition erfassen?
Ja, klicken Sie in den Bestellungs-Kopfdaten auf ![](neuerWareneingang.JPG), danach erscheint ein Dialog in dem Sie die Informationen zum Wareneingang selbst erfassen. Geben Sie hier die Lieferscheinnummer und Datum, sowie das Lager an. Mit Klick auf OK wird der neue Wareneingang angelegt und Sie stehen im Reiter Wareneingangspositionen dieses Wareneingangs und können somit weitere Positionen erfassen.
![](neuerWareneingangErfassung.JPG)

**Hinweis:** Diese Funktion steht mit aktivierter ReelID, also AUTOMATISCHE_CHARGENNUMMER_BEI_WEP=3 **nicht** zur Verfügung.

#### Können alle Bestellpositionen auf einmal verbucht werden ?
Ja, klicken Sie einfach auf ![](WE_Position4.gif), damit werden alle noch offenen Wareneingangspositionen, der nicht Chargen- bzw. Seriennummern geführten Artikel zugebucht.
Zusätzlich kannst du auswählen, ob damit alle oder nur die markierten Bestellpositionen zugebucht werden sollten.
D.h. ist nur eine Wareneingangspositionszeile markiert, so werden alle noch nicht zugebuchten Bestellpositionen zugebucht. Sind mehrere Zeilen markiert, so werden nur diese zugebucht.
Eine Rückfrage erfolgt im Sinne des flüssigen Arbeitens nicht.

#### Können alle Bestellpositionen auf einmal als Preis erfasst verbucht werden ?
Ja, klicken Sie einfach auf ![](WE_Position5.gif), damit werden die als geliefert Preis eingetragenen Preise als Anlieferpreis bestätigt.

WICHTIG: Ist für alle Wareneingänge die Preis erfasst Information eingetragen, so wird der Status der Bestellung auf erledigt gesetzt. Daher ist es erforderlich, dass **vorher** eventuelle Transportkosten eingetragen werden.

#### Kann eine Bestellposition nachträglich hinzugefügt werden?
Wenn eine Position geliefert wurde, die nicht Teil der **Kieselstein ERP** Bestellung war, so klicken Sie im Reiter Wareneingangsposition auf das Icon ![](neu.jpg). Wenn man in den Wareneingangspositionen eine neue Position hinzufügt, kann man rechts vom der Artikelicon einen Handartikel anlegen und zuordnen. Das Hinzufügen einer Position bewirkt, dass eine neue Bestellposition und Wareneingangspostion angelegt wird. Dieser Button kann nur ausgewählt werden, wenn noch keine Eingangsrechnung für den Wareneingang definiert ist.

#### Wie werden mehrere Chargen auf einen Wareneingang zugebucht?
Derzeit kann pro Wareneingang nur eine Charge zugebucht werden.

Sollten Sie mit einer Lieferung mehrere unterschiedliche Chargen der selben Bestellpositionen erhalten, so ist es erforderlich, trotzdem mehrere Wareneingänge (der gleichen Lieferung) anzulegen und so die unterschiedlichen Chargen zuzubuchen.

Ab der Version 8523 sind mehrere Chargeninformationen pro Warenzugang möglich. Wenn im Chargendialog auf das '+'-Zeichen geklickt wird, bzw. auf Übernehmen geklickt wird, dann werden die Chargeninformationen abgefragt. Bei vorhandenen Chargen können per Doppelklick auf die Zeile die Chargeninformationen geändert werden.

#### Bei einem bestimmten Artikel sollte der Anforderer informiert werden, dass die Ware gekommen ist
<a name="WEP-Info"></a>
Um zu erreichen, dass der Anforderer ein EMail bekommt, wenn von einem bestimmten Artikel ein Wareneingang gebucht wird, setzen Sie bei dem Artikel im Reiter Bestelldaten bei ![](WEP-Info.gif) WEP-Info an Anforderer einen Haken, so wird an den Anforderer des Artikels eine entsprechende EMail Nachricht über den **Kieselstein ERP** Versanddienst gesandt, wenn für den Artikel ein Warenzugang gebucht wurde.

Info: Um für jeden gebuchten Wareneingang eine Info zu senden nutzen Sie gegebenenfalls die Möglichkeit z.B. aus der Wareneingangsetikette eine EMail Nachricht an eine fixe Adresse zu senden, was bedeutet, dass bei jedem Druck der Wareneingangsetikette, also bei jeder Kennzeichnung einer Wareneingangsposition die Information an die im Etikett eingetragene Adresse gesandt wird. Falls dies gewünscht ist, bitten Sie Ihren **Kieselstein ERP** Betreuer um Anpassung der Etikette.

Zusätzlich kann auch in jeder Bestellposition definiert werden, dass bei einer Wareneingangsbuchung eine EMail an den Anforderer gesandt wird. Die Definitionen im Artikel bzw. der Bestellposition ergänzen sich. D.h. wenn die WEP-Info im Artikel definiert ist, wird für den Artikel immer die EMail versandt. Wenn es nur in der Bestellposition gesetzt ist, wird dies dann versandt wenn für diese Bestellposition ein Wareneingang gebucht wird, egal wie die Einstellung im Artikel ist.

Bitte beachten Sie dazu auch den Parameter DEFAULT_WEP_INFO. Definiert die Defaulteinstellung der WEP-Info für neue Bestellpositionen.
Beachten Sie dazu auch die Definition in Personal/Parameter "generelle WEP-Info".
Zusätzlich kann in den Bestellungskopfdaten ein "interner Anforderer" (rechts vom Anforderer) definiert werden. Dieser erhält parallel zum Anforderer die WEP-Info-Mail zugesandt.

#### Können Chargen vereinzelt werden?
Gerade für das Thema Traceablity ist es wichtig, jedes Gebinde (z.B. eine Rolle SMD Widerstände) eigens zu erfassen.
Stellen Sie dafür den Parameter AUTOMATISCHE_CHARGENNUMMER_BEI_WEP auf 1.
Dies bewirkt, dass bei Chargengeführten Artikeln durch Klick auf das plus automatisch eine Chargennummer im Format "Bestellnummer-WENr-Posnr" vorgeschlagen wird.
Nach dem Speichern der Wareneingangsposition kann nun durch Rechtsklick auf den Etikettendrucker sofort der Ausdruck der Wareneingangsetiketten angestoßen werden.

#### Wie sollte mit der ReelID vorgegangen werden?
Gerade in der Elektronik hat sich im Zusammenhang mit den Gebinden die Vereinzelung jeder einzelnen Materialrolle, daher Reel-ID, herauskristallisiert. Eigentlich ist dies eine Erfassung jeder einzelnen Verpackung des jeweiligen Artikels / Bauteiles. Die effizientese Vorgehensweise ist, dass von Seiten Ihrer Lieferanten die Rollen nach z.B. VDA 4992 Mat-Lable (und deren Unterarten) bereits versehen sind. Damit scannen Sie jede Rolle mit dem [mobilen Barcodescanner]( {{<relref "/extras/Mobile_App">}} ) und erhalten neben der Warenzubuchung auch eine Wareneingangsetikette.
Bei der Buchung wird die ReelID aus Bestellnummer, Wareneingangsnummer und Bestellpositionsnummer erzeugt. Zusätzlich werden Datecode und Batch1 aus dem VDA Lable gelesen und in einem kombinierten String in der Chargennummer des jeweiligen Wareneingangs abgelegt. Ein Scann der nächsten "Rolle" erzeugt eine weitere ReelID mit den nächsten Daten.
Um die ReelID zu nutzen stellen Sie bitte den Parameter AUTOMATISCHE_CHARGENNUMMER_BEI_WEP auf 3\. Bitte beachten Sie, dass die Wareneingangs, Losabliefer und Artikeletiketten entsprechend angepasst werden müssen.
Der Aufbau der ReelID ist derzeit wie folgt:
17/0123456_01_999_01|170825|xyz123
BNR.........._WE_WEP_RN Datecode BatchNo
BNR = Bestellnummer
WE = Pos Nr des WEs
WEP = Pos Nr des WEPs
RN = ReelNr bzw. fortlaufende Nummer der Rolle = je eine eigene Charge je Rolle
Bitte beachten Sie, dass die Länge der ReelID in machen Mat-Lables auf 17Stellen begrenzt ist. Gegebenenfalls sollte die Belegnummernlänge reduziert werden.

Anmerkung: Aktuell gibt es für diese Anwendung die sogenannte VDA-Scann-App. Mit der kann auf einem Windwos-PC mit entsprechendem Barcodescanner, der Wareneingang, die Auslagerung ins Los und die Rücknahme der Übermengen abgewickelt werden.

#### Manuelle Zubuchung mit ReelID
Um eine manuelle Wareneingangsbuchung mit der ReelID zu nutzen, klicken Sie bitte in den Wareneingangspositionen auf ![](ReelID1.gif) Wareneingang mit ReelID buchen und geben für jede Rolle / Verpackungseinheit die entsprechenden Mengen, Datecode und Batch an. Damit erreichen Sie auch eine entsprechende Vereinzelung der Verpackungen, bzw. eine Zubuchung unterschiedlicher Chargennummern je Verpackung. In der Chargennummer werden zuerst die ReelID, dann der Datecode und dann die Batchnr. jeweils getrennt durch | (Vertical Bar) abgelegt. Beim Druck von VDA Mat Lables ist dies entsprechend zu berücksichtigen.
![](ReelID2.jpg)

Hinweis:
Sollte der ReelID Button nicht angezeigt werden, so vergrößern Sie bitte das Fenster des Bestellungsmoduls, bis dieser angezeigt wird.

#### Kann ein Wareneingang auf unterschiedliche Läger gebucht werden?
Nein. Es kann je Wareneingang das Zubuchungslager definiert werden.

Es werden damit alle Wareneingangspositionen dieses Wareneingangs auf das angegebene Lager gebucht.

#### Wie kann eine erledigte Bestellung wieder ent-erledigt werden?
Wählen Sie den gewünschten Wareneingang aus und wechseln Sie in die Wareneingangspositionen. Klicken Sie auf ändern und entfernen Sie bei einer Position den Haken bei Preis erfasst. Nun noch abspeichern und die Bestellung ist wieder im Status geliefert. Nun können z.B. die Transportkosten eingetragen werden.

#### Wie kann das Projekt in den Bestellungskopfdaten bei teilweisem Wareneingang geändert werden?
In einem ersten Blick ist dies nicht möglich. Da es gerade im Einkauf immer wieder zu Änderungen aus den verschiedenste Gründen kommen kann, hier die Vorgehensweise, wie man dies trotzdem erreicht. Bitte beachten Sie dabei, danach die Bestellung wieder zu aktivieren.
Die Vorgehensweise ist wie folgt:

-   Gehen Sie in den Reiter Bestellpositionen
    Sollten die oberen Buttons ![](BS_Aendern1.gif) deaktiviert sein, so wählen Sie einfach unten im Detail das Ändern![](BS_Aendern2.jpg).

-   Die nun erscheinende Frage
    ![](BS_Aendern3.jpg)
    Beantworten Sie mit Ja, bzw. wenn die Info auch an Ihren Lieferanten gehen sollte mit Änderungsbestellung (wodurch der Versionszähler erhöht wird)

-   Nun erscheint eine erneute Sicherheitsabfrage
    ![](BS_Aendern4.jpg)
    Bestätigen Sie diese ebenfalls mit Ja

-   Nun ist die Bestellung im Status angelegt.
    D.h. es können die Kopfdaten entsprechend verändert werden.
    Es können auch die Positionen verändert werden. Es können die Menge jedoch nur soweit reduziert werden, als Wareneingangsmengen gebucht sind.
    Die Position die bereits einen Wareneingang gebucht hat, kann in ihrer Menge erhöht werden, oder auch reduziert aber nur auf die Menge des bereits gebuchten Wareneingangs.

-   Gegebenenfalls verwerfen Sie die Änderung der gewählten Position mit ![](BS_Aendern5.gif) zurücknehmen.

-   Sind alle Änderungen durchgeführt, muss die Bestellung wieder aktiviert werden.
    Wechseln Sie dazu wieder in den Reiter Bestellpositionen.
    Um nun die Bestellung, ohne weiteren Druck bzw. EMail-Versand zu aktivieren, klicken Sie bitte mit der **rechten** Maustaste auf das Druckersymbol ![](BS_Aendern6.gif). Damit wird die Bestellung wieder in den richtigen Status gesetzt.

#### Wie kann bei einer erledigten Bestellung ein Wareneingang gelöscht werden?
Wählen Sie den gewünschten Wareneingang aus und wechseln Sie in die Wareneingangspositionen. Klicken Sie nun auf ändern und entfernen Sie den Haken bei Preis erfasst und speichern Sie die Position ab. Nun ist die Bestellung nur mehr teilgeliefert und es kann der / alle Wareneingänge gelöscht werden.
D.h. für das Löschen eines Wareneingangs darf das "Preise erfasst" nicht angehakt sein. Dadurch ist der Wareneingang nur mehr im Status geliefert und somit kann er auch gelöscht werden.
Beachten Sie bitte jedoch, wenn die Menge des Wareneingangs bereits verbraucht wurde, muss zuerst der Verbrauch zurückgenommen werden (siehe oben).

<a name="Eingangsrechnungszuordnung"></a>

#### Wie kann eine Eingangsrechnung einem Wareneingang zugeordnet werden?
Sind für alle Wareneingangspositionen eines Wareneinganges alle Preise erfasst, so ist die Zuordnung der Eingangsrechnung ![](WE_Position6.jpg) freigeschaltet. Klicken Sie auf diesen Knopf um die Zuordnung des Wareneinganges zu einer Eingangsrechnung vorzunehmen.

Durch diesen Klick gelangen Sie in die Auswahlliste der Eingangsrechnungen. Hier kann nun durch Klick auf

Neu ... ![](WE_ER1.gif) ... eine neue Eingangsrechnung angelegt werden. Es wird dabei der theoretische Wert der Eingangsrechnung anhand der Angeliefert Preise zzgl. den Transportkosten unter Berücksichtigung des Rabattes errechnet. Aufgrund des beim Lieferanten hinterlegten Mehrwertsteuersatzes wird der Bruttopreis errechnet. Wurde im Lieferanten unter Konditionen, das Warenkonto hinterlegt, so kann, mit Ausnahme der Lieferantenrechnungsnummer und dem Eingangsrechnungsdatum die Eingangsrechnung aus der Bestellung, genauer dem Wareneingang, ermittelt werden.

Durch den Klick auf Neu werden alle Daten der Eingangsrechnung wie oben beschrieben vorbesetzt. Tragen Sie die noch fehlenden Daten ein, überprüfen Sie den Rechnungswert und die Kontierung und speichern Sie die Eingangsrechnung ab.

**Wichtig:** Differiert der vorgeschlagene Eingangsrechnungswert zum tatsächlichen Betrag der Eingangsrechnung, so prüfen Sie bitte unbedingt Ihre Eingaben in den Wareneingangspositionen. Wenn alles richtig verbucht wurde, darf es zu keinen Differenzen kommen.

So hatten wir in der Praxis schon den Fall, dass in den Wareneingangspositionen immer 1000er Preise erfasst wurden und dann immer der Betrag der Eingangsrechnung korrigiert wurde. Im Endeffekt hat dies eine Differenz von 300.000,- € zwischen Warenzugangswert und Veränderung im Lager bewirkt, was wiederum bedeutete, dass die betroffenen Artikel in den betroffenen Losen deutlich zu hoch bewertet waren und damit Ihre Deckungsbeiträge viel zu nieder, oder die Verkaufspreise viel zu hoch waren. Mit allen möglichen Folgen daraus.

**Hinweis:** In der aufgeführten Liste der Eingangsrechnungen sehen Sie alle angelegten Eingangsrechnungen des jeweiligen Lieferanten. Damit können Sie auch mehrere Wareneingänge auf eine gemeinsame Eingangsrechnung zusammenhängen.

**Wichtig:** Durch jeden Klick auf Neu wird für den gewählten Wareneingang eine neue Eingangsrechnung angelegt. Die eventuell bestehende Eingangsrechnung bleibt trotzdem erhalten, verliert jedoch den Bezug zum Wareneingang. Bitte beachten Sie die entsprechende Meldung.

**Hinweis:** Haben Sie für einen Wareneingang mehrere Eingangsrechnungen erhalten, was in der Regel nicht vorkommen sollte, so muss für die richtige Zuordnung je Eingangsrechnung ein eigener Wareneingang angelegt werden.

Durch Klick auf

löschen ... ![](WE_ER2.gif) ... wird die Verbindung zwischen Eingangsrechnung und Wareneingang gelöscht.

Durch Klick auf 

ändern ... ![](WE_ER3.gif) ... kann die durch den Cursorbalken ausgewählte Eingangsrechnung geändert werden. Dies bewirkt **keine** Änderung der Zuordnung der Eingangsrechnung zum Wareneingang.

<a name="Eingangsrechnungszuordnung0"></a>

#### Die Lieferung des inländischen Lieferanten unterliegt manchmal dem Reverse Charge Prinzip
Aufgrund der Reverse Charge Bestimmungen sind bestimmte Warengruppen (z.B. Telekom, Bau-Bereich, Schrott, ..) ohne Vorsteuer auch bei Inlandslieferungen auszuweisen. D.h. abhängig was Ihr Lieferant liefert, ist es einmal mit der inländischen Mehrwertsteuer zu betrachten und eben einmal ohne.
Nun wird bei der oben beschriebenen Überleitung des Wareneingangs in die Eingangsrechnung immer der Bruttobetrag errechnet. D.h. z.B. für einen Netto-Wareneingangswert von 10.000,- € wird der hinterlegte allgemeine Mehrwertsteuersatz von 20% (für Österreich) hinzugerechnet.
Somit wird in der Eingangsrechnung 12.000,- € vorgeschlagen.
Somit stellt sich die Erfassungsmaske wie folgt dar:
![](ER_aus_BS_ReverseCharge1.gif)

Da diese Lieferung jedoch den Reverse Charge Regeln unterworfen ist, ändern Sie den Eintrag bei Reverse Charge z.B. auf Kennziffer 48, ...., also Reverse Charge.
![](ER_aus_BS_ReverseCharge2.gif)
Üblicherweise, wird von **Kieselstein ERP** auch bei dieser Änderung der Zahlenwert des Betrages gleichbelassen. Einzig, wenn Sie **direkt** aus der **Überleitung des Wareneinganges** in die Eingangsrechnung kommen, wird auch der Zahlenbetrag auf den Nettowert (des Wareneingangs) verändert.

**Hinweis:** Wenn Sie diese Änderung direkt aus dem Eingangsrechnungsmodul veranlassen, wird der Zahlenwert des Betrages **nicht** verändert.

#### Die Ware meines Lieferanten kommt einmal aus dem Inland, dann wieder aus dem Ausland?
Es kommt immer wieder vor, dass zum Zeitpunkt der Bestellung nicht bekannt ist, von welchem (EU-Aus-)Land die Lieferung tatsächlich erfolgt. D.h. hier kann beim Erzeugen der Bestellung keine wirklich richtige Rechnungsadresse angegeben werden. Da aber steuerlich eine deutliche Unterscheidung zwischen Inlands- und EU-Auslands-Lieferungen (und auch Drittland) zu machen ist, muss in diesem Falle, nachdem die Eingangsrechnung wie oben beschrieben angelegt wurde, der Lieferant auf den tatsächlich liefernden geändert werden. Beachten Sie bitte, dass der Lieferant dann auch entsprechend steuerlich richtig definiert ist. Da dies manchmal erst beim Erhalt der Eingangsrechnung wirklich klar ist, kann über den Menüpunkt Bearbeiten, Rechnungsadresse ändern, dies auch nach der Erfassung des Wareneingangs noch abgeändert werden.

#### Wie kann ich unterliefern?
Im Panel Wareneingangsposition kann mit ![](Bestellung_Erledigen.gif) eine einzelne Bestellposition erledigt werden. Das bedeutet Sie erwarten von dieser Bestellungsposition keine weitere Anlieferung von Ihrem Lieferanten. Zusätzlich wird in der rechten Spalte der Status (offen, erledigt) der jeweiligen Bestellposition angezeigt. Haben Sie versehentlich die falsche Position erledigt, so klicken Sie erneut auf den ![](Bestellung_enterledigen.gif) und ent-erledigen damit die Bestellposition wieder.

#### Wie kann ich überliefern?
Geben Sie im Panel Wareneingangsposition beim jeweiligen Artikel die entsprechend höhere Menge an. 

#### Fehlmengenauflösung?
Siehe [nachträglich Fehlmengen bei der Zubuchung auflösen]( {{<relref "/fertigung/losverwaltung/#fehlmengen-bei-der-warenzubuchung-aufl%c3%b6sen" >}} )

Neben der Auflösung der Fehlmengen der Lose, gibt es die Möglichkeit auch Reservierungen aus Aufträgen bei Warenzubuchung automatisch zu buchen. Diese Zusatzfunktion kann von Ihrem **Kieselstein ERP** Betreuer aktiviert werden. Wird nun ein Artikel mit Reservierung zugebucht, so erscheint die Fehlmengenauflösung![](fehlmengen_reservierungen.JPG)<br>
Hier können Sie erkennen, dass neben der Auftragsreservierung auch Lose diesen Artikel benötigen würden. Das R in der Spalte Typ signalisiert, dass es sich um Reservierungen handelt und F steht für Fehlmengen. Reservierungen von Losen können über die Fehlmengenbuchung nicht aufgelöst werden.<br>
Markieren Sie nun die gewünschte Zeile und klicken Sie auf das Icon ![](fehlmenge_markierte_zeile_aufloesen.JPG) um die markierte Zeile aufzulösen bzw. auf bearbeiten und ändern die Menge auf die gewünschte Menge ab.<br>
Damit wird ein Lieferschein, wenn er schon besteht und im Status angelegt ist ergänzt oder ein neuer LS angelegt und die Position eingebucht.

#### Warum kann man das Lager im Wareneingang nicht mehr verändern?
Wenn bereits Wareneingangspositionen verbucht wurden, so sind diese bereits einem Lager zugeordnet. Um die Lagerdefinition für die Zubuchungen dieser Bestellung zu ändern, müssen alle Wareneingangspositionen vorher zurückgenommen werden.

#### Was bedeutet Teillieferung in den Kopfdaten?
Unterschied Gesamtlieferung und Teillieferung:

Bei der **Gesamtlieferung** rechnet der Empfänger damit alle Artikel gemeinsam geliefert zu bekommen. Im Druck der Bestellung wird ein Vermerk mitgedruckt, dass die Positionen dieser Bestellung nur als gesamtes geliefert werden dürfen.

Wareneingänge können unabhängig von dieser Vorgabe an Ihren Lieferanten angelegt und verbucht werden.

#### Was ist eine Rahmenbestellung?
Eine Rahmenbestellung ist eine Vereinbarung mit Ihrem Lieferanten eine bestimmte Menge eines oder mehrerer Artikel in einem gewissen Zeitraum zu beziehen und diese auch zu einem besonderen Preis geliefert zu bekommen. Üblicherweise werden Rahmenvereinbarungen auch zur Verbesserung von Lieferzeiten getroffen.

#### Was ist eine Abrufbestellung?
Mit der Abrufbestellung rufen Sie Teilmengen / Teilpositionen der Rahmenbestellung ab. Die Abrufbestellung bezieht sich immer auf eine Rahmenbestellung. Dies wird auch beim Druck der Abrufbestellung entsprechend mit angegeben. Durch den Abruf einer Rahmenbestellung reduziert sich die offene Rahmenmenge. Verwenden Sie diese Information auch um die offenen Rahmenmengen mit Ihrem Lieferanten abzustimmen.

<a name="Divisor"></a>

#### Was bedeutet Divisor, manuell und Rest bei der Abrufbestellung?
![](Abruf.gif)

**Divisor:** Da die Abrufmengen einer Rahmenbestellung sehr oft Untermengen der Rahmenbestellung sind, können Sie bei der Übernahme der Rahmenbestellung in die Abrufbestellung einen Divisionsfaktor angeben, mit dem die Positionsmenge der Rahmenbestellung in den Abruf übernommen werden sollten. Dieser Divisor wird nur beim Anlegen einer neuen Abrufbestellung abgefragt.

Ein Beispiel:

Rahmenmenge 168 Stk zu 12 Abrufen

Anlage einer Abrufmenge, Divisor 12, da Sie ja 12 Abrufe geplant haben ergibt

Abrufmenge 14 Stk.

**Manuell:** Geben Sie manuell die Mengen der Positionen aus der Rahmenbestellung ein.

**Rest:** Alle verbleibenden Positionen werden errechnet und bestellt.

#### In meiner Rahmenbestellung wird eine Zeile in rot angezeigt?
Das bedeutet, dass der errechnete Preis aus dem Artikellieferanten nicht mit dem Positionspreis der Bestellung übereinstimmt.
Klicken Sie gegebenenfalls auf den Button Preis um den aktuellen Einkaufspreis aus dem Artikellieferanten zu übernehmen.

#### Die Rahmenübersicht liefert eine etwas abweichende offene Liefermenge gegenüber der Anzeige?
Dies tritt dann auf, wenn Bestellpositionen der Abrufbestellungen über- oder unterliefert werden. D.h. unter Info, Rahmenübersicht sehen Sie die exakte offene Liefermenge gegenüber der Rahmenbestellmenge, egal ob Sie bei einzelnen Abrufbestellpositionen Unter- bzw. Überlieferungen akzeptiert haben.

#### Kann man eine bestehende Bestellung kopieren und bearbeiten
Ja, die Daten für eine neue Bestellung können aus einer bestehender Bestellung verwendet werden. Verwenden Sie dazu den Button ![](bestellung_kopieren.PNG) Bestellung kopieren.

Auch beim Erstellen von Abrufbestellungen für bestehende Rahmenbestellungen hilft das Übernehmen der bestehenden Daten.<br>
Wählen Sie dazu eine schon bestehende Abrufbestellung und kopieren diesen -- nun werden die Daten (Kopfdaten und Positionen mit Rahmenauftragsbezug) übernommen.

#### Wie nutzt man eingegebene Daten wie Verpackungseinheit, Mindestbestellmenge, Staffeln?
Bei der Eingabe der Bestellpositionen kommen viele Daten aus dem Artikelstamm zum Tragen. Um die Staffelmengen per Knopfdruck auswählen zu können, finden Sie neben Fixkosten, Verpackungseinheit und Mindestmenge die ersten beiden Staffelmengen im unteren Eingabebereich. Durch Klick auf den jeweiligen ![](Staffel.gif) Button werden Menge und Staffelpreis nach oben übernommen.

#### Die Lieferanten habe unterschiedliche Verpackungseinheiten, wie gehe ich da vor?
Definieren Sie im Artikelstamm unter [Artikellieferant]( {{<relref "/docs/stammdaten/artikel/#standardmenge-mindest-bestellmenge-verpackungseinheit-wiederbeschaffungszeit" >}} ) die entsprechende Verpackungseinheit des Lieferanten und gegebenenfalls auch die dazu gehörende Mindestbestellmenge. Dadurch werden bei der Eingabe der Bestellmenge die Daten auf ein ganzzahliges Vielfaches der Verpackungseinheiten überprüft. Ist die Bestellmenge kein ganzzahliges Vielfaches, so erscheint ein entsprechender Hinweis.

#### Wie mit Verpackungseinheiten des Artikellieferanten und der Lagerbewirtschaftung in abweichender Einheit umgehen?
Anwender schildern, das Problem, dass Lieferanten, an Stelle von bestellten Stück die Anzahl in Verpackungseinheiten liefern (z.B. werden 10 Stk. bestellt, geliefert aber 10 VPE). Hierzu gibt es die Möglichkeit im Bestellungsdruck ein eigenes Feld für die VPE des Artikellieferanten einzufügen. Somit ist die Information für den Lieferanten ersichtlich, dass 10 Stück bestellt werden, diese bei ihm jedoch nur 1 Verpackungseinheit umfassen. Bitte kontaktieren Sie Ihren **Kieselstein ERP** Berater um dies bei Ihnen einzufügen.

#### Wie verbucht man Wareneingänge mit geänderten Preisen?
Man verbucht normal als Wareneingang, gibt aber den geänderten Wert in das Feld Gelieferter Preis ein. Ansonsten lässt man dieses Feld frei, dann wird der Bestellpreis verwendet. Der gelieferte Preis wird NICHT als Preis in den Artikellieferanten zurückgepflegt.
Wenn der gelieferte Preis um mehr als 10% vom Bestellpreis abweicht wird eine Warnung ausgegeben.

#### Ich habe eine Wareneingangsposition zugebucht und kann diese nicht mehr ändern.
Möglichkeit a.) Ist die soeben zugebuchte Wareneingangsposition die letzte offene Position dieser Bestellung gewesen, so wird durch diese Zubuchung automatisch die gesamte Bestellung auf erledigt gesetzt. Wenn nun dieser Wareneingang noch einmal geändert werden muss, so muss zuerst der Status der Bestellung von Erledigt auf Geliefert zurückgeändert werden. Danach kann die Wareneingangsposition wieder verändert werden.

Möglichkeit b.) Die zugebuchte Wareneingangsposition wurde bereits durch ein anderes Modul verbraucht.

D.h. es muss dieser Verbrauch zurückgenommen werden damit der Wareneingang auf die gewünschte Menge reduziert werden kann. Hinweis: Der Verbrauch in ein Los kann am Einfachsten durch die Rücknahme der Losausgabe durchgeführt werden. [Siehe dort]( {{<relref "/fertigung/losverwaltung/#r%c3%bccknahme-der-gesamten-losausgabebuchung" >}} ).

#### Ich kann eine Wareneingangsposition nicht mehr löschen.
Um eine Wareneingangsposition grundsätzlich löschen zu können, prüfen Sie bitte:

-   Ist die Bestellung im Status offen, bestätigt oder teilerledigt. Wenn die Bestellung bereits vollständig erledigt ist, so wählen Sie bitte im Menü des Bestellungsmoduls Manuel, Erledigung aufheben.

-   In der Wareneingangsposition, welche vermutlich ebenfalls im Status erledigt ist, wählen Sie ändern und entfernen nun den Haken bei Preise erfasst. Nun ist die Position im Status geliefert und es ist der Löschenknopf ![](Bestellung_stornieren.gif) freigeschaltet.
    Durch Klick darauf kann die Anlieferung dieser Position gelöscht werden.

#### Wie finde ich heraus, wodurch der Wareneingang verbraucht wurde?
Gehen Sie im Artikelmodul direkt auf den Artikel, z.B. aus den Wareneingangspositionen heraus mit dem GoTo Knopf ![](WE_POS_GoTo.gif).

Sie sind nun im Artikelmodul. Hier wählen Sie Info, Warenbewegungsjournal. In diesem Journal sehen Sie alle Warenzu- und Abgänge des Artikels. Suchen Sie hier die gewünschte Zugangsbuchung und finden Sie damit die entsprechende Lagerabbuchung.

Nun ändern Sie die Lagerabbuchung in der Form, dass der Verbrauch zur tatsächlichen Zubuchung passt. Nun kann auch der Wareneingang auf die gewünschte Menge korrigiert werden.

![](Warenbewegungsjournal.png)

Siehe dazu bitte auch [Statistik der Artikel]( {{<relref "/docs/stammdaten/artikel/#statistik-der-artikel-wo-sehe-ich-die-bewegungen-am-artikel" >}} )

#### Wareneingangsjournal
Mit dem Wareneingangsjournal haben Sie eine praktische Möglichkeit Ihre Wareneingänge im Zeitraum aufzulisten. Es stehen viel Filter- und Sortiermöglichkeiten zur Verfügung.

Da manchmal auch gewünscht ist, dass angezeigt werden sollte, wo das Material verwendet wurde, wodurch es verbraucht wurde, steht mit Warenverbrauch zur Verfügung. Wird dieser Parameter angehakt, so werden neben den Wareneingängen auch die direkt zum Wareneingang zuordenbaren Warenausgänge dargestellt.

#### Ändern der Bestellung von Erledigt auf geliefert
Wählen Sie die Bestellung aus und gehen Sie in das Wareneingangspanel dieser Bestellung. Nun klicken Sie auf Ändern. Den nachfolgenden Dialog

![](Statusaenderung_Geliefert.gif)

bestätigen Sie mit Ja.

**Wichtig:**

Da der Status immer beim Abspeichern der Wareneingänge bzw. der Wareneingangspositionen neu ermittelt wird, wird durch ein nachfolgendes Speichern der Status wieder auf erledigt gesetzt. Beenden Sie daher das Ändern des Wareneinganges durch Abbrechen.

#### geliefert / erledigt was ist der Unterschied ?
bei einer gelieferten Bestellung sind alle Positionen geliefert bzw. wegen Unterlieferung manuell auf erledigt gesetzt.

Bei einer erledigten Bestellung sind zusätzlich bei allen Wareneingangspositionen die Preise erfasst und damit freigegeben. Beachten Sie dazu bitte auch [Lagerwert zum Stichtag und offene Wareneingänge]( {{<relref "/docs/stammdaten/artikel/#lagerwert-zum-stichtag-und-offene-wareneing%c3%a4nge" >}} )

#### Wo sehe ich, welche Abrufe an meiner Rahmenbestellung hängen?
Gehen Sie in der Rahmenbestellung in die Kopfdaten. Hier werden links unten ![](Rahmen_Abrufe.gif) die Abrufe zu dieser Rahmenbestellung angezeigt. Um den Status der Mengen zu sehen, wählen Sie bitte Sicht Rahmen in den oberen Modulreitern der Bestellung.

#### Ich habe alle Positionen aus meiner Abrufbestellung gelöscht, wie kriege ich die Positionen mit Rahmenbezug wieder in meine Bestellung?
Gehen Sie in der Abrufbestellung auf Sicht Rahmen. Ändern Sie nun die ![](Rahmenabrufmenge.gif) Menge der Bestellung entsprechend auf die gewünschte Abrufmenge. Dadurch wird automatisch diese Position mit Rahmenbezug in die Bestellposition übernommen.

**Hinweis:**

Die unter Positionen neu eingepflegten Artikel, Handartikel haben keinen Bezug zur Rahmenbestellung und reduzieren daher die offene Rahmenmenge NICHT. Sie haben damit die Freiheit auch Positionen zu bestellen die in der eigentlichen Rahmenbestellung nicht enthalten waren.

#### Wieso ist die AB-Nummer so kurz?
Die bei den Lieferdaten eingebbare AB-Nummer (Auftragsbestätigungsnummer des Lieferanten) ist auf 20Stellen begrenzt. Dies hat den Hintergrund, dass diese AB-Nummer auch in den Bestellübersichten usw. mit angedruckt wird und daher entsprechend Platz benötigen würde.

#### Wie kann eine erledigte Wareneingangsposition wieder bearbeitet / gelöscht werden ?
Ist in einer Wareneingangsposition Preise erfasst ![](PreiseErfasst.gif) bereits angehakt, so kann diese nicht gelöscht werden. Um sie zu löschen muss vorher das Preise erfasst entfernt werden. Gehen Sie daher auf ändern ![](aendern.gif) und entfernen Sie den Haken bei Preise erfasst. Nun kann mit löschen ![](loeschen.gif) der Wareneingang dieser Position gelöscht werden.

Beachten Sie bitte, dass das Löschen nur möglich ist, wenn die Menge dieses Warenzugangs auch noch tatsächlich auf Lager liegt.

#### Für die Erledigung eines Wareneingangs werden Dokumente angefordert.
Ist ein Artikel als [Dokumentenpflichtig]( {{<relref "/docs/stammdaten/system/dokumentenablage/#a-namedokumentenpflichtadokumentenpflicht" >}} ) gesetzt, so muss beim Wareneingang für diese Artikel zumindest ein Dokument hinterlegt sein, anderenfalls erscheint eine entsprechende Fehlermeldung.

![](Warnung_Dokumentenpflichtig.jpg)

#### Ich kann auf einer Wareneingangsposition kein Dokument hinterlegen, der Knopf wird nicht angezeigt.
Ist in den Wareneingangspositionen noch keine einzige Wareneingangsmenge gebucht, so fehlt auch der Knopf für die Dokumentenablage. Buchen Sie zuerst eine Menge zu, danach wird sofort der Dokumentenablageknopf für diese Position angezeigt.

#### Wozu dient das Lieferantenmahnwesen?
Leider zeigt die Praxis, dass manche Lieferanten die Wunschtermine nicht einhalten (können). Mit dem Lieferantenmahnwesen können die offenen Lieferungen und die offenen Lieferbestätigungen angemahnt werden.<br>
Das Lieferantenmahnwesen ist in drei Arten geteilt.
1. Mahnung der Lieferanten-Auftragsbestätigung
Hier werden jene Bestellpositionen angemahnt, bei denen noch kein Wareneingang und kein vom Lieferanten bestätigter Liefertermine angegeben ist und deren Bestelldatum drei Tage älter als der heutige Tag ist.
2. Mahnung der Lieferungen
Hier werden jene Bestellpositionen angemahnt, bei denen bereits der Liefertermin überschritten ist.
3. Mahnen als Liefererinnerung, wenige Tage vor der eigentlichen Lieferung
Hier werden der Lieferant daran erinnert, die Bestellpositionen rechtzeitig zu liefern, die in den nächsten Tagen von ihm an Sie angeliefert werden sollte.

Es wird pro Bestellung nur eine Mahnstufe verwaltet.
Als Liefertermin wird der eingepflegte bestätigte Liefertermin herangezogen. Ist kein bestätigter Liefertermin eingetragen, so wird der ursprüngliche (Wunsch-)Liefertermin verwendet.

<u>**Hinweis:**</u>

Die Mahnstufen werden im Bestellungsmodul, Grunddaten, Mahntext bzw. Mahnstufe definiert. Hier gilt die Regel, dass immer die nächst höhere Mahnstufe (aktuelle Mahnstufe + 1) für die nächste Mahnung verwendet wird. Ist diese nicht definiert, so wird die Mahnstufe 99 verwendet.
Achten Sie daher darauf, dass die verschiedenen Mahnstufen lückenlos definiert werden. (Also von 1 - 5).

### Bestellungen mahnen
Mit dem in **Kieselstein ERP** implementierten Mahnwesen wird sowohl der Bereich der Einzelmahnung als auch der Sammelmahnung abgedeckt.

Für das Verhalten des Mahnwesens sind die Einstellungen der Mahnstufen und der Mahntexte, welche Sie im Modul Bestellung, Grunddaten vornehmen, wesentlich.

Durchführen eines Mahnlaufes, Sammelmahnung

Unter ![](Mahnlauf.gif) klicken Sie auf neu ![](Neuer_Mahnlauf.gif) und wählen aus, ob Sie echte Liefermahnungen oder AB-Mahnungen oder beides erstellen möchten.
Nun werden alle offenen Bestellungen des Mandanten nach den obigen Regeln durchsucht und die zu mahnenden Bestellungen in die Mahnliste aufgenommen. In der Auswahlliste der Mahnläufe erhalten Sie einen neuen Eintrag. In diesem ist 
![](Mahnliste.gif)  
das Datum des Mahnlaufes, die Anzahl der zu mahnenden Bestellungen und die Anzahl der davon noch nicht gemahnten Bestellungen angeführt.

Im Reiter ![](Mahnungen.gif) sehen Sie nun eine Liste aller zu mahnenden Bestellungen mit deren Positionen.
Sie können hier die Mahnung einer einzelnen Bestellung ausdrucken ![](Mahnung_Einzeln_Drucken.gif), 
eine einzelne Bestellung aus der Mahnliste herauslöschen ![](Mahnlauf_loeschen.gif),
und mit ![](Mahnung_Einzeln_aendern.gif) die Mahnstufe der Bestellung verändern.
Eine bereits gemahnte Bestellung, also eine gedruckte Mahnung kann mit ![](Einzelmahnung_Zuruecknehmen.gif) wieder zurückgenommen werden.

Der Status des Mahnungsdruckes ist anhand des Häkchens am rechten Rand ersichtlich.
![](Hackerl.gif) = Mahnung bereits gedruckt. 
Gedruckte Mahnungen reduzieren auch die Anzahl der Offenen in der Sicht Mahnungslauf.

Wurden alle Mahnungen eines Mahnlaufes versandt, so kann ein neuer Mahnlauf gestartet werden.
 **Wichtig:** Wird der Mahnungsdruck aus der Sicht ![](Mahnlauf.gif) gestartet, so werden sogenannte Sammelmahnungen erstellt. D.h. pro Lieferant wird nur eine Mahnung versandt.
Wird der Mahnungsdruck aus der Sicht ![](Mahnungen.gif) gestartet, so wird nur diese eine Bestellung gemahnt. Ein nachfolgender Mahnungsdruck aller Mahnungen könnte eine weitere Mahnung an den gleichen Lieferanten senden.

**Wichtig:** Wenn der Mahnlauf ausgedruckt wurde, werden alle Mahnungen, als versendet gekennzeichnet, selbst wenn sie nur kontrolliert wurden. Nehmen Sie den Mahnlauf zurück (siehe unten), wenn Sie sie per Fax oder Mail versenden möchten.

Mahnlauf zurücknehmen ![](Mahnlauf_zuruecknehmen.gif)

Im Modulreiter Mahnläufe. Damit werden alle bereits als ausgedruckt gekennzeichneten Mahnungen wieder als nicht ausgedruckt gekennzeichnet. Damit können Sie bereits versandte Mahnungen erneut versenden.

Mahnlauf löschen ![](Mahnlauf_loeschen.gif)

Im Modulreiter Mahnläufe. Hiermit löschen Sie den gesamten Mahnlauf. Z.B. wenn Sie vor dem Erstellen des Mahnlaufes vergessen haben Wareneingänge oder Auftragsbestätigugen einzubuchen.

**Versand der Mahnungen per Fax oder EMail:**

Mit den Knöpfen ![](Mahnen_Versand_Fax.gif) Faxversand und ![](Mahnen_Versand_EMail.gif) EMailversand können die **offenen** Bestellmahnungen als Gesamtes auf einmal versandt werden. Bitte achten Sie darauf dass hier nur mehr die offenen Bestellungen versendet werden. Wurden z.B. zur Kontrolle die Mahnung mit dem ![](Mahnen_Versand_Drucker.gif) Druckersymbol in die Vorschau gedruckt, so sind bereits alle Mahnungen ausgedruckt und ein nachträglicher Versand der Mahnungen würde keinen weiteren Versand mehr auslösen. Wurden aus Kontrollzwecken die Mahnungen in die Vorschau gedruckt, so kann dieser Status durch Klick auf den ![](Mahnen_Versand_Ruecknahme.gif) wieder zurückgenommen werden.

**Hinweis:** Nur bei den Sammelmahnungen werden die EMail-Adressen bzw. Fax-Nummern der Ansprechpartner an den Druck und damit an den Versanddienst übergeben. Werden Mahnungen zuerst in die Vorschau gedruckt und von da aus versucht jede einzelne Sammelmahnung per Fax/EMail zu versenden, so müssen derzeit die Empfängerdaten per Hand eingegeben werden. Verwenden Sie daher bitte den oben beschriebenen Versand. Möchten Sie nur eine/wenige einzelne Mahnung(en) erneut per Fax/EMail versenden, so entfernen Sie die Markierung durch ![](Mahnen_Versand_Ruecknahme.gif) und klicken Sie dann auf den jeweiligen Versandknopf.

Da die Sammelmahnungen ja Bestellungen, welche an verschiedene Ansprechpartner gingen enthalten können, werden diese nur an die zentralen Email-/Faxadressen der jeweiligen Firmen gesandt.

<a name="Mahnsperre"></a>

#### Eine Bestellung sollte nicht gemahnt werden
Damit eine Bestellung nicht gemahnt wird, kann diese über den Menüpunkt bearbeiten, Mahnsperre bis definiert und gegebenenfalls auch wieder gelöscht = entfernt werden.
Dies bedeutet, dass bis zu diesem Zeitpunkt eben keine Mahnung an den Lieferanten versandt wird.
Beachten Sie bitte, dass damit die geplanten Eintrefftermine der Ware nicht verändert werden.
D.h. üblicherweise korrigieren Sie die AB-Termine ev. mit Erhalt der Ursprungstermine, damit auch die weiteren Planungen die geänderte Terminsituation mitbekommen.

#### Es sollten nur bestimmte Artikelgruppen gemahnt werden.
Definieren Sie in den Bestellungen, unterer Modulreiter Grunddaten, oberer Modulreiter die zu mahnenden Artikelgruppen ![](Artikelgruppen_mahnen.gif). Damit wird erreicht dass nur die Artikel gemahnt werden, die dieser Artikelgruppe entsprechen.

Bitte beachten Sie, dass wenn hier Artikelgruppen definiert sind, nur diese Artikelgruppen gemahnt werden und dass damit impliziert ist, dass auch keine Handeingaben gemahnt werden können.

#### Was ist beim automatischen Mahnlauf zu beachten?
Wieso werden immer am Mittwoch keine automatischen Mahnungen erzeugt ?

#### In einigen Unternehmen ist es üblich, im automatischen Nachtjob nur die fehlenden Auftragsbestätigungen einzumahnen.
Nun kommt es immer wieder vor, dass für den Mittwoch keine Bestell-Auftragsbestätigungs-Mahnungen erzeugt werden.

Warum ?

<u>Hintergrund:</u>

Die Abmahnungen werden immer dann erzeugt, wenn drei Tage nach dem Bestelldatum noch keine Auftragsbestätigungen eingegeben sind. Haben Sie nun Lieferanten die entsprechend reagieren und werden die Auftragsbestätigungen auch umgehend eingegeben, was beim automatischen Mahnungsversand zwingend erforderlich ist, so sind in aller Regel für Mittwoch keine Auftragsbestätigungsmahnungen zu versenden, da sie ja am Sonntag davor normalerweise keine Bestellungen an Ihre Lieferanten gesandt haben.

#### Wieso werden auch Rahmenbestellungen AB gemahnt?
Gerade bei Rahmenbestellungen haben Sie ja mit Ihrem Lieferanten einen Vertrag ausgehandelt. Daher ist es auch hier erforderlich, dass Sie von Ihrem Lieferanten eine Auftragsbestätigung erhalten und diese unter Sicht Lieferantentermine einpflegen. Wurde diese Daten nicht eingepflegt, werden beim Mahnlauf auch die fehlenden Auftragsbestätigungen der Rahmenbestellungen angemahnt.

**Hinweis:** Bei einer Rahmenbestellung werden immer die gesamten Mengen AB angemahnt, da dies ja als Gesamtes bestätigt werden muss. D.h. die angemahnte Menge ist unabhängig von eventuell bereits erfolgten Rahmenabrufen und daraus resultierenden Teillieferungen.

**Hinweis2:** Da auf Rahmenbestellungen ja keine Lieferungen erfolgen, wird für Rahmenbestellungen keine Liefermahnung erzeugt.

#### Können Bestellmahnungen nur für bestimmte Artikelgruppen gemacht werden ?
Ja. Definieren Sie in den Grunddaten der Bestellung die zu mahnenden Artikelgruppen. Wir haben dies so gesteuert, dass wenn hier Artikelgruppen eingetragen sind, so werden nur diese Artikelgruppen gemahnt. Ist hier keine einzige Artikelgruppe definiert, so werden alle Mengenbehafteten Positionen angemahnt.

Dies bedeutet auch, dass bei einer Definition von zu mahnenden Artikelgruppen, Handeingaben nicht gemahnt werden.

#### An welchen Empfänger werden die durch den Nachtjob erzeugten Bestellmahnungen gesandt ?
Je nach Organisation Ihrer Unternehmens-Email Kommunikation kann es praktisch sein wenn:
- Die Bestellmahnung automatisch in CC an den Versender gesandt wird -> siehe Parameter MAHNUNG_AUTO_CC
- Der Absender der Bestellmahnung nicht der Auslösende ist, was beim Nachtjob immer der **Kieselstein ERP** Administrator wäre, sondern, wenn dies der Anforderer der Bestellung ist.

Eine Rahmenbestellung ist im Status abgerufen. Warum kann ich das nicht erledigen?

Die Rahmenbestellung berücksichtigt zu Ihrer Information auch den Status der Abrufe. D.h. ist ein Rahmenbestellung nur teilweise abgerufen, so bleibt sie im Status offen. Ist sie vollständig abgerufen, so ist sie im Status abgerufen. Erst wenn alle Abrufe im Status erledigt sind, wechselt auch die Rahmenbestellung in den Status erledigt.

<a name="Beistellware"></a>

#### Beistellware, wie wird das richtig gemacht?
Viele Lohnfertiger erhalten von Ihren Kunden Ware zur Fertigung beigestellt. In vielen Fällen ist es auch so, dass nur spezielle Teile, wie z.B. Artikel mit langen Lieferzeiten, Artikel mit Copyright usw. von den Kunden beigestellt werden. Wie wird das nun richtig gehandhabt, um die Teile auch richtig in die Warenbewirtschaftung einfließen zu lassen.

1.  Wir gehen davon aus, dass Sie die dem Kunden versprochenen Liefertermine halten wollen und daraus die Verantwortung für die Materialbeschaffung übernommen haben.

2.  Das bedeutet, dass Beistellteile von der Warenlogistik her, genauso zu behandeln sind, wie Artikel die Sie bei Lieferanten zukaufen.

3.  Das bedeutet, die Beistellteile sind als Artikel in die Stücklisten aufzunehmen.

4.  Um die Teile beim Kunden möglichst automatisch zu beschaffen, ist der Kunde auch als Lieferant anzulegen. Nutzen Sie hier die zentrale Partnerverwaltung.

5.  Über Interne Bestellung (Fertigung) und [Bestellvorschlag]( {{<relref "/einkauf/bestellung/vorschlag">}} ) werden - ausgelöst durch Ihre Auftragsbestätigung an den Kunden - die Artikel zeitgerecht bei Ihrem Kunden, der als Lieferant behandelt wird, durch eine Bestellung angefordert.
  - Das Anlegen einer Bestellung hat den deutlichen Vorteil, dass Sie den Kunden zeitgerecht darüber informieren, dass Sie für die Fertigung seiner Baugruppen / Produkte, Ware benötigen.
  - Zugleich ist damit das automatische Bestellungsmahnwesen gekoppelt, was wiederum bewirkt dass Sie Ihren Kunden auch daran erinnern, dass Sie nicht fertigen können, weil ER seine Teile nicht liefert.
    - Falls gewünscht kann die Bestellung auch direkt dem Kundenauftrag zugeordnet werden.
6.  Über den Warenzugang werden die Teile dem Lager zugebucht und damit im bei Ihnen üblichen Warenfluss behandelt. So können die Teile z.B. entsprechend etikettiert und geprüft werden.

7.  Eventuell auflaufende Transportkosten können den, meistens kostenlosen, Beistellteilen zugeordnet werden und werden so im Endeffekt auch in der Produktnachkalkulation berücksichtigt.

8.  Ist die Bestellung erledigt, kann sie durch Manuell erledigen auf Vollständig erledigt gesetzt werden.

**Hinweis:**

Es ist theoretisch auch möglich die Teile durch einen Lieferschein an den Kunden unter Angabe von negativen Mengen zuzubuchen. Das ist von der Menge her gesehen nicht falsch, es fehlen jedoch alle anderen bei der Warenbewirtschaftung zusätzlich zu beachtenden Punkte. Daher ist von dieser Vorgehensweise dringend abzuraten.

#### Eine Bestellposition lässt sich nicht mehr löschen?
Sind auf einer Bestellposition bereits eine weiterführende Buchung, z.B. Mahnung bzw. Wareneingang eingetragen, so kann diese Position nicht mehr gelöscht werden.
Gehen Sie hier so vor, dass Sie die Menge der Bestellposition auf die nun noch offene Menge, z.B. 0 (Null) ändern und senden Sie Ihrem Lieferanten diese geänderte Bestellung erneut zu, da durch diese Änderung der Vertrag mit Ihrem Lieferanten geändert wurde.

#### Können die Bestellpositionen sortiert werden?
Ja. Durch Klick auf ![](Sortieren_nach_Ident.gif) kann in den Bestellpositionen die Bestellung, welche im Status angelegt sein muss, nach der Artikelnummer / Identnummer sortiert werden. Dies ist praktisch, wenn die Bestellpositionen zusammengetragen werden um dann einen Überblick über die unterschiedlichen Artikel zu erhalten. Die Sortierung erfolgt in der Form, dass zuerst die Positionsart Ident sortiert nach Identnummern, dann die Handeingaben und danach die weiteren Positionsarten, so wie erfasst einsortiert werden.

#### Wird die Mahnstufe auch in der Übersicht angezeigt?
Ja. in der rechten Spalte der Auswahlliste, wird die jeweilige Mahnstufe der Bestellung angezeigt. Hier bedeutet die "0", dass die Bestellung nur AB-gemahnt wurde. Mahnstufen ab 1 stellen die jeweilige Liefermahnung dar. Ist die Spalte leer, so wurde keine Mahnung dafür erzeugt.

![](Anzeige_Mahnstufe.gif)

<a name="bestätigter Liefertermin"></a>

#### Kann ich auch die vom Lieferanten bestätigten Liefertermine, die Auftragsbestätigung des Lieferanten eintragen?
Ja, das ist sogar sehr wichtig.
Es steht dafür dafür der Reiter ![](Lieferantentermine1.gif) Sicht Lieferantentermine zur Verfügung.
Von der Gewichtung her ist der, anhand einer Auftragsbestätigung des Lieferanten, eingepflegte Liefertermin wertvoller / verbindlicher, als der vom Anwender angegebene Wunschliefertermin, daher diese Eingabemöglichkeit.
D.h. der eingepflegte bestätigte Liefertermin wird in der Bestelltliste des jeweiligen Artikels gemeinsam mit der Auftragsbestätigungsnummer Ihres Lieferanten und eventuell des zusätzlich erfassten Kommentars angezeigt.
Um die Auftragsbestätigungsdaten einzupflegen stehen folgende Möglichkeiten zur Verfügung.

![](Liefertermin2.gif) **Ändern:** Wählen Sie die zu ändernde Position aus und klicken Sie auf den Ändern Knopf. Geben Sie nun den bestätigten Termin (bei Ihnen eintreffend) und die AB-Nummer des Lieferanten ein. Im untern Feld besteht die Möglichkeit einen Kommentar, eine zusätzliche Information zu hinterlegen.
![](Liefertermin3.jpg)
Ist der Parameter <a name="Ursprungstermin"></a>URSPRUNGSTERMIN_FUER_LIEFERANTENBEURTEILUNG eingeschaltet, so steht, besonders für die [Lieferantenbeurteilung]( {{<relref "/fertigung/reklamation/#termintreue-auswirkung" >}} ) zusätzlich die Möglichkeit eines zweiten, des eigentlichen ursprünglich bestätigten Termins zur Verfügung. D.h. ändert Ihr Lieferant nachträglich seinen Liefertermin, was leider oft auch mehrfach vorkommt, so sollten Sie diese Änderung einpflegen. Für die Beurteilung seiner Termintreue, wird jedoch der Ursprungstermin belassen.<br>
Für den Fall, dass Sie sich bei der Erfassung des ersten AB-Termins geirrt haben, haken Sie Ursprungstermin an AB-Termin anpassen an, pflegen Sie den AB-Termin und speichern Sie diese Änderung an. Damit wird der Ursprungstermin auf den eingegebenen AB-Termin gesetzt.

![](Liefertermin4.gif) Alle Positionen auf einmal eintragen
Gerade bei umfangreichen Bestellungen, wäre es sehr mühsam, jede Position einzeln einzupflegen.
Verwenden Sie dafür diese Funktion durch Klick auf den Wecker.
![](Liefertermin5.gif)
Da es in aller Regel so ist, dass ein Großteil der Positionen zu einem Termin kommen, aber einige wenige Positionen zu einem anderen Termin, so bieten stehen zwei mögliche Vorgehensweisen zur Verfügung:
a.) Pflegen Sie zuerst die abweichenden Termine ein und wählen Sie dann "Für leere Positionen setzen". D.h. es werden die Daten nur für diejenigen Bestellpositionen eingetragen, bei denen noch keine AB-Termine eingetragen sind.
b.) Setzen Sie zuerst die AB-Termine und Nummern für alle Positionen und ändern Sie dann die anders lautenden Positionen ab. Achten Sie bitte auf Ursprungstermin an AB-Termin anpassen.

#### Wo sehe ich die Wirkungen der Terminänderungen?
[Siehe bitte]( {{<relref "/fertigung/losverwaltung/planung_auswertung/#auslieferliste-umfangreich" >}} ).

#### Kann ich auch die vom Lieferanten erneut bestätigten Liefertermine eintragen ?
Wird auch die Liefererinnerung aus dem Mahnwesen verwendet, so muss selbstverständlich sehr zeitnah auch die erneute Bestätigung der zeitgerechten Lieferung Ihres Lieferanten eingetragen werden.
Bei dieser Mahnung (Mahnstufe -1) wird der Lieferant wenige Tage vor der eigentlichen Lieferung daran erinnert, die Bestellpositionen rechtzeitig zu liefern. Er sollte darauf hin umgehend eine erneute Lieferbestätigung an Sie senden.
Diese Bestätigung tragen Sie bitte unter Lieferantentermine ein, in dem Sie für die bestätigten Positionen durch Klick auf ![](rechtzeitige_Lieferung_bestaetigen.gif) den Liefertermin als bestätigt kennzeichnen. Hinweis: Ein erneuter Klick auf den Button Liefertermin bestätigt entfernt die Bestätigung wieder.
Um die Liefererinnerung zu definieren, definieren Sie in den Grunddaten die Mahnstufe -1\. Hier hinterlegen Sie z.B. -3Tage, was bedeutet, dass drei Tage vor dem Liefertermin die Liefererinnerung ausgedruckt werden sollte.

#### Kann nachträglich der Ursprungstermin verändert werden?
Ja. Geben Sie unter AB-Termin das gewünschte Datum für den neuen Ursprungstermin ein und haken Sie Ursprungstermin an AB-Termin anpassen an. Klicken Sie nun auf speichern. Somit sind beide Termine, der Ursprungstermin und der AB-Termin gleich. Gehen Sie nun erneut auf ändern und geben Sie den eventuell anderen AB-Termin ein. Durch speichern haben Sie nun wieder die getrennten AB-Termine erfasst.

#### Wie lange kann der AB-Termin eingetragen bzw. geändert werden.
So lange bis die Bestellposition vollständig geliefert ist.

#### Kann z.B. die Qualitätssicherung auch nachträglich die bestätigen Termine ändern?
Da es immer wieder vorkommt, dass erst bei der Lieferbeurteilung festgestellt wird, dass z.B. bei den AB-Terminen Tippfehler gemacht wurden, zu diesem Zeitpunkt aber die Bestellungen schon längst erledigt sind, haben wir einen Funktion geschaffen, mit ein berechtigter Mitarbeiter, z.B. die QS, diese Termine trotzdem verändern darf, damit die automatische Beurteilung des Lieferanten wieder richtig gestellt wird.
Wenn Sie das Recht REKLA_QUALITAETSSICHERUNG_CUD besitzen, so können Sie in der Sicht Lieferantentermine durch Klick auf ![](Lieferantentermine_AB_Termine_trotzdem_aendern.gif) AB-Termin ändern, jederzeit den AB-Termin einer Bestellposition verändern.

#### Wie geht man damit um, dass ein Lieferant keine Auftragsbestätigung sendet?
In diesem Falle wird einfach kein Auftragsbestätigungstermin erfasst. Ein einpflegen von fiktiven Daten, manche Menschen geben da z.B. den 1.1.1900 ein, führt zu völlig falschen Daten. Wenn ein Lieferant keine AB sendet, so ist der Bezug für seine Lieferantenbeurteilung Ihr Wunschtermin. Genau dieser wird auch für die Beurteilung herangezogen. Geben Sie aber einen AB-Termin ein, so wird dieser für die Beurteilung herangezogen. Es führt dies dann dazu, dass z.B. ein Lieferant um 7667 Tage zu spät liefert, womit er sofort D-Lieferant ist und dadurch sofort gesperrt ist.<br>
Bitte pflegen Sie in **Kieselstein ERP** NUR die Daten ein, die Sie kennen und möglichst keine fiktiven Daten. Das macht immer Probleme.<br>
Sollte Ihnen ein Lieferant eine AB senden und keinen Liefertermin angeben, so gehen Sie einfach davon aus, dass damit auch der von Ihnen gewünschte Termin bestätigt ist, er hat ja nichts anderes auf seine AB rauf geschrieben.

#### Wozu dient die Eingabe der Lieferanten Auftragsbestätigungsdaten?
Es ist für Ihre Planung von großer Bedeutung, ob die Termine der Warenanlieferung nur Ihre Wunschtermine sind, oder ob diese Termine auch von Ihrem Lieferanten bestätigt wurden. Wir haben daher in 
**Kieselstein ERP** die Möglichkeit der Eingabe der Lieferantentermine ![](Lieferantentermine.gif) geschaffen.

Geben Sie hier die Daten der Auftragsbestätigung (AB) Ihres Lieferanten für die / alle Positionen ein. Durch Klick auf ![](Alle_AB_setzen.gif) kann das AB-Datum für alle/nur die noch nicht bestätigten Positionen gesetzt werden.

#### Wie kann ich Mengen Teilungen / Mengen Splitt des Lieferanten in meinen Bestellungen abbilden?
<a name="Bestellposition_aufteilen"></a>
Es kommt immer wieder vor, dass Sie eine Menge bestellen, Ihr Lieferant davon einen Teil sehr rasch / früher als den Rest liefern kann.
Selbstverständlich sollten Sie diese Information entsprechend erfassen.
Es ist dafür erforderlich, dass Sie die Bestellposition aufteilen, mit dem ersten und den zweiten Liefertermin.
Wechseln Sie dazu in den Reiter Bestellpositionen und klicken Sie bitte auf ![](Bestellposition_aufteilen1.gif) Aufteilen der Bestellposition.
Im nachfolgenden Dialog geben Sie bitte
![](Bestellposition_aufteilen2.jpg)
die Menge der verbleibenden / neuen Position mit dem neuen Liefertermin an.
Nach dem Klick auf speichern wird die Menge der alten Position um die neue Menge reduziert.
Somit haben Sie beide Bestelltermine abgebildet bzw. können Sie die unterschiedlichen AB-Termine auf die jeweilige Position erfassen.

#### Warum kann der Stichtag der offenen Bestellungen nur in der Zukunft sein?
Das Journal der offenen Bestellungen dient vor allem dazu, festzustellen welche Ware bis zu einem bestimmten Zeitpunkt, z.B. Ende der Woche, eintreffen sollte. Eine Rückwärts-Betrachtung ist vor allem auch wegen der erforderlichen Statusinformationen auf der einzelnen Bestellposition (denken Sie an die Erledigung von unterlieferten Positionen) nicht vorgesehen.

#### Wie wird der Stichtag bei den offenen Bestellungen angewandt?
Stichtag bedeutet, welche Bestellungen sind zum Stichtag noch offen, oder etwas förmlicher: Es werden alle Bestellungen aufgelistet, deren Bestelldatum vor oder zum dem Stichtag sind und deren Liefertermin(e) vor oder zum Stichtag sind und die noch nicht vollständig erledigt sind.

Wenn nur angelegte angehakt ist, so werden nur diejenigen Bestellungen ausgedruckt, die noch im Status angelegt sind.

Wenn nur offene Mengen angehakt, so werden nur diejenigen Bestellungen aufgelistet bei denen tatsächlich offene Mengen gegeben sind. Ist dieser Haken nicht gesetzt, so werden auch die Bestellung(sköpfe) aufgeführt, die noch nicht vollständig erledigt sind. Damit haben Sie einerseits den Überblick über die noch nicht verrechneten Bestellungen und andererseits eine Übersicht über die Waren die tatsächlich noch nicht angeliefert wurden.

#### Kann beim Journal Offene Bestellungen nach einem Projekt gefiltert werden?
Wenn die Zusatzfunktion Projektklammer bei Ihnen aktiviert ist, so kann beim Druck des Journals kann durch klick auf Projekte... ein oder auch mehrere Projekte ausgewählt werden. Somit bezieht sich das Journal nur mehr auf diese Projekte. Wird beim Druck die Sortierung Projekt verwendet, so wird nach Projektnummer sortiert.

<a name="Bestellungen eines Auftrags"></a>

#### Wie findet man Bestellungen eines Auftrags?
Aktivieren Sie im Bestellungsmodul Zusatzfilter ![](Zusatzfilter.png), 

wählen Sie Kriterium Auftrag und klicken Sie auf das + Symbol.

![](Zusatzfilter_Auftrag.gif)

und geben Sie bei Auftrag = die Auftragsnummer an (es kann hier auch nur 2655 eingegeben werden).

Entfernen Sie gegebenenfalls den Haken bei Schnellansicht und klicken Sie auf aktualisieren ![](Aktualisieren.gif) (bzw. F5).

Nun werden alle Bestellung die diesem Auftrag zugeordnet sind angezeigt.

#### Wie findet man Bestellungen eines Loses?
<a name="Lose einer Bestellung"></a>
Aktivieren Sie im Bestellungsmodul Zusatzfilter ![](Zusatzfilter.png), wählen Sie Kriterium Losnummer und klicken Sie auf das + Symbol.

Geben Sie nun bei Losnummer = die gewünschte Losnummer an.

Entfernen Sie gegebenenfalls den Haken bei Schnellansicht und klicken Sie auf aktualisieren ![](Aktualisieren.gif) (bzw. F5).

Nun werden alle Bestellung die Positionen mit Verweisen auf das Loses beinhalten angezeigt.

#### Eine bereits gemahnte Bestellung lässt sich nicht mehr ändern?
Wurde eine Bestellung bereits gemahnt, so können die einzelnen Positionen der Bestellung nicht mehr gelöscht werden, da diese ja bereits gemahnt wurde. Wird nun, warum auch immer, eine einzelne Position nicht mehr benötigt, so ändern Sie bitte nur die Menge dieser Position entsprechend auf die bereits gelieferte Menge ab, bzw. auf 0\. Damit erreichen Sie, dass diese Bestellposition als bereits vollständig geliefert betrachtet wird und es erfolgt damit keine weitere Mahnung.

#### Ändern / Anzeigen von Einkaufspreisen
Die Anzeige und auch das Ändern von Einkaufspreisen ist genauso, wie das Anzeigen und Ändern von Verkaufspreisen mit einem Benutzerrecht gekoppelt. Hier ist zu beachten, dass um Einkaufspreise ändern zu können, das Recht die Einkaufspreise ansehen zu können Voraussetzung ist. D.h. ein Benutzer der nur die Preise einsehen, aber nicht ändern darf muss LP_DARF_PREISE_SEHEN_EINKAUF besitzen. Sollte er auch die Einkaufspreise ändern können, muss er zusätzlich das Recht LP_DARF_PREISE_AENDERN_EINKAUF besitzen.

<a name="Los-Materialposition mit Bestellposition verbinden"></a>

#### Verknüpfen einer Bestellposition mit einer Los-Materialposition
Gerade in der Metallverarbeitung ist es sehr oft erforderlich, Bestellpositionen direkt mit Positionen des Loses (Fertigungsauftrags) zu verbinden. Dies kann mit dem Knopf Los ![](Loszuordnung.gif) in der Bestellposition erreicht werden.
Bei dieser Auswahl geben Sie zuerst das Los an und danach die Materialposition auf die sich diese Bestellposition bezieht.
Dadurch wird erreicht, dass Informationen der Losposition direkt auf der Bestellung mit angedruckt werden. Z.B. Längen von bestimmten Materialien.
Bei der Erzeugung des [Bestellvorschlages]( {{<relref "/einkauf/bestellung/vorschlag">}} ) wird diese Zuordnung ebenfalls unterstützt und somit werden die Daten automatisch vom Los an die Bestellung und damit zu Ihrem Lieferanten durchgereicht.
Für den Druck mit der Bestellung stehen folgende Felder zur Verfügung:
- Losnummer
- Stücklistennummer
- Stücklistenbezeichnung
- Stücklistenkurzbezeichnung
- Los-Kommentar
- Los-Projekt
- Kommentarzeile des Losmaterials

#### Andrucken von Bemerkungen am Arbeitsgang der Fremdmaterialposition
In einigen Anwendungen ist es erwünscht, dass auch die Kommentare bzw. Beschreibungen welche am Fremdarbeitsgang erfasst wurden mit angedruckt werden. Diese Einstellung kann Ihr **Kieselstein ERP** Betreuer für Sie in den Bestellformularen hinterlegen.

Der Zusammenhang ist hier folgender:
1. In der Bestellung wird wie oben beschrieben die Los-Materialposition angegeben und somit die Verknüpfung hergestellt.
2. Im Arbeitsplan des Loses wird beim (Fremd-)Arbeitsgang durch Klick auf den Knopf Material auf die Materialposition (1.)verwiesen.

Dadurch wird in der Bestellung der Inhalt des indirekten Arbeitsganges angedruckt.

#### Wie werden Fremdwährungs Wareneingänge zugebucht ?
Ist für eine Bestellung eine Fremdwährung definiert, so wird diese zum Kurs des Wareneingangsdatums zugebucht.

Definition des Umrechnungskurse [siehe bitte]( {{<relref "/management/finanzbuchhaltung/#wo-werden-die-kurse-nachgepflegt" >}} )

#### Wie wird eine Zubuchung mit einem fehlerhaften Kurs korrigiert ?
Für die Korrektur von Zubuchungen mit falschen Kursen gehen Sie bitte wie folgt vor.
- korrigieren Sie bitte zuerst den Kurs, zum jeweiligen (Wareneingangs-) Datum
- Gehen Sie nun in die Bestellung, Wareneingang.
- wählen Sie den betreffenden Wareneingang anhand des Datums aus und lösen Sie mit ändern ![](Kurs_aktualisieren1.gif), speichern ![](Kurs_aktualisieren2.gif) eine Neuberechnung der Einstandspreise des Wareneingangs aus. Dadurch wird der Kurs neu aus der Kurstabelle in den Wareneingang übernommen und damit die Gestehungspreise im Lager aktualisiert.
Je nach Gesamtstatus Ihrer Bestellung erhalten Sie eventuell folgende Rückfrage:
![](Kurs_aktualisieren3.jpg)
Beantworten Sie diese bitte mit Ja.

Die nachfolgende Meldung der Statusänderung bestätigen Sie bitte mit Ok.

#### Kann bei einer Bestellung mit Wareneingänge nachträglich die Währung geändert werden?
Nein. Da bei den Buchungen der Wareneingänge die Preise von der Lieferantenwährung in die Mandantenwährung umgerechnet werden müssen, ist eine Währungsänderung der Bestellung nicht vorgesehen. Bitte definieren Sie den Lieferanten in der vereinbarten Währung und legen Sie die Bestellung entsprechend an.

#### Es sollten nur die Artikel des Lieferanten der Bestellung angezeigt werden
Mit dem Parameter DEFAULT_ARTIKELAUSWAHL in der Kategorie Bestellung kann definiert werden, ob alle Artikel des Artikelstamms oder nur die Artikel des Lieferanten in der Auswahlliste der Artikel angezeigt werden.

Ist dieser Parameter auf 0 = Nur die Artikel des Lieferanten der Bestellung, so werden bei der Artikelauswahl normalerweise nur die Artikel des Lieferanten angezeigt. Haken Sie ![](Artikelauswahl_alle_Lieferanten_anzeigen.gif) alle Lieferanten an, so werden alle Artikel angezeigt.

Beachten Sie bitte, dass die Einstellung alle Lieferanten solange erhalten bleibt, bis das Bestellmodul wieder geschlossen wird.

#### Der Lieferant ist nicht zertifiziert, daher darf der Artikel nicht zugebucht werden.
Diese Meldung erscheint, wenn ein Artikel der aufgrund seiner Artikelgruppe zertifizierungspflichtig ist, zugebucht werden sollte und wenn beim Lieferanten keine Freigabe (Konditionen) hinterlegt ist. Siehe dazu [Zertifizierungspflichtige Artikel]( {{<relref "/docs/stammdaten/artikel/#artikel-von-zertifizierten-lieferanten-zertifizierungspflichtige-artikel" >}} )

#### Können Bestellungen auch importiert werden?
Siehe bitte [Bestellungsimport]( {{<relref "/einkauf/bestellung/import">}} ).

#### Monatsbestellung, C-Teile Bewirtschaftung
Mit der Monatsbestellung haben wir eine komfortable Möglichkeit geschaffen, die sogenannten C-Teile zu bewirtschaften.

Sie finden diesen Import im Modul Bestellung unter Bestellung, Monatsbestellung.

Die Aufgabenstellung bei den C-Teilen ist ja, dass sie eigentlich vom Wert her zu vernachlässigen sind, aber wenn z.B. eine Beilagescheibe im Wert von 0,01 € fehlt, kann ein komplettes Produkt im Wert von 10.000,- € nicht ausgeliefert werden.

So gehen Anwender dazu über die Versorgung dieser Artikel an Sublieferanten auszulagern.

Hier ist es üblich, dass regelmäßig, z.B. einmal die Woche, der Lieferant die Lagerstände aller vereinbarten Artikel prüft und wenn der Vorrat halbleer ist, wird wieder nachgefüllt.

Selbstverständlich erhalten Sie dafür eine Eingangsrechnung.

Um nun:
- die Eingangsrechnung zu überprüfen
- den theoretischen Verbrauch (was machen Sie, wenn der Mitarbeiter des Lieferanten immer viel zu viel nachfüllt, weil ...) zu überprüfen.

Wird vom Lieferanten pro Besuch eine Nachfüll-Datei geliefert. Diese ist im CSV Format und hat folgenden Aufbau:

KUNDENNR;KUNDENNAME;BEST.NR.;LIEFERSCHEIN;ARTIKELNR;ARTIKELNR_KUNDE;TEXT;GEL.MENGE;OFF.MENGE;VERLADEDATUM

Dabei haben die Felder folgende Bedeutung:
| Feld | Bedeutung |
| --- | --- |
| KUNDENNR | Ihre Kundennummer bei Ihrem Lieferanten. Damit wird der Lieferant für die Monatsbestellung bestimmt.Tragen Sie bitte Ihre Lieferantenkundennummer im Lieferantenstamm unter Kopfdaten, Kundennummer ein. |
| KUNDENNAME | Ihr Firmenname, hat für den Import keine Bedeutung |
| BEST.NR. | keine Bedeutung |
| LIEFERSCHEIN | Lieferscheinnummer des Lieferanten. Es wird dies im Wareneingang unter Lieferschein protokolliert |
| ARTIKELNR | Artikelnummer des Lieferanten. Hat für den Import keine Bedeutung |
| ARTIKELNR_KUNDE | Ihre Artikelnummer. Auf diesen Artikel wird die Bestellung ergänzt und der Wareneingang zugebucht |
| GEL.MENGE | Gelieferte Menge. Diese Menge haben Sie mit der (Wochen-)Lieferung erhalten |
| VERLADEDATUM | Datum des Wareneingangs und des Lieferanten-Lieferscheines |

Als Einkaufspreis wird der Preis des jeweiligen Artikellieferanten verwendet.

D.h. sorgen Sie dafür, dass der Einkaufspreis bei jedem Artikel des Lieferanten der C-Teile entsprechend Ihren Vereinbarungen ist.

Mit diesem Import haben Sie komfortabel die Wochenlieferung Ihres C-Teile Lieferanten importiert und können die monatliche Sammelrechnung direkt auf diese eine Bestellung zuordnen.

Die Entscheidung ob eine neue Monatsbestellung angelegt werden soll, wird anhand des Wareneingangsdatums (Verladedatum) getroffen. Gibt es eine offene Bestellung des Lieferanten im Monat des Verladedatum = Wareneingangsdatums, so wird diese Bestellung ergänzt. Wenn nicht wird eine neue Bestellung für dieses Monat angelegt.

Welche Fehlermeldungen können während des Imports auftreten und welche Bedeutung habe sie:
-   Kein Lieferant zu Kundennummer xyz gefunden.
    Stellen Sie die Kundennummer beim gewünschten Lieferanten unter Kopfdaten, Kundennummer richtig
-   Lieferant XYZ hat keine Kostenstelle hinterlegt.
    Hinterlegen Sie die Kostenstelle mit der die Bestellung für den Lieferanten angelegt werden soll.
-   Lieferant XYZ hat kein Lager hinterlegt.
    Definieren Sie das Ziellager auf das die Zubuchung der Ware erfolgen sollte.
-   Artikelnummer 1234 nicht gefunden. Die in der Spalte Artikel_Nummer_Kunde angegebene Artikelnummer ist in Ihrem Artikelstamm nicht enthalten.

Zusammenfassung:

Von der Idee her ist es so, dass die Eingangsrechnung erst erfassen werden sollten, nachdem alle Warenbewegungen erfasst sind. Oder umgekehrt: Ihr Lieferant muss sicherstellen, dass Sie keine Warenbewegungen, keine CSV Dateien für bereits erledigte Wareneingänge erhalten. Das ist ja der zusätzliche Sinn dahinter, damit haben Sie eine gewisse Kontrolle, was denn so alles verrechnen wird.

**Hinweis:**<br>
Ob die C-Teile Bewirtschaftung für dich sinnvoll ist, bitte sehr kritisch überlegen.
Dein **Kieselstein ERP** verwaltet auch die Teile mit den geringen Kosten. Die Lieferanten müssen natürlich, auch anhand des Vertrages darauf achten, dass immer die Artikel ausreichend vorrätig sind. Auch wenn du die Artikel gar nicht mehr brauchst. Achte hier auf eine Vereinbarung, dass der Lieferant die Artikel auch wieder zurücknehmen muss (mit den gleichen Werten)


#### Informationen für den Lieferanten bei Handeingabe anführen
Auch bei Handeingaben haben Sie die Möglichkeit die Artikelnummer und Bezeichnung des Lieferanten anzuführen. Verwenden Sie dazu die entsprechenden Felder.

![](bestellung_handeingabe_artikellieferant.JPG)

#### Kann der Bestellwert als gesamtes übersteuert werden?
Es kommt immer wieder vor, dass für eine Bestellung ein pauschaler Betrag vereinbart wurde. Um nun nicht mühsam jede einzelne Position auf den Wert hinzurechnen, wurde die Erfassung des manuellen Bestellwertes geschaffen. Sie finden diese im Menü, Bearbeiten, Pauschalbetrag setzen. Im Gegensatz zum Setzen des manuellen Auftragswertes, werden hier die Nettopreise in den Positionen angepasst. Eventuelle zusätzliche Korrekturen werden so vorgenommen, dass diese an der Position mit den höchsten Preisen vorgenommen werden. Reicht auch dies nicht aus, wird wiederum eine Pauschalkorrektur gebildet und bei der Bestellung hinterlegt und auch als eigene Zeile in der Bestellung mit ausgedruckt.
Bitte beachten Sie:
- die Pauschalkorrektur wird, wenn die Bestellung geändert wird wieder auf ungültig gesetzt
- werden zusätzliche Positionen hinzugefügt, so löschen diese zwar die Pauschalkorrektur, verändern aber die geänderten Nettopreise nicht. D.h. gegebenenfalls ist der nun neue Bestellwert erneut einzugeben.

#### Wie wird ein Setartikel im Wareneingang zugebucht?
Die Behandlung von Setartikeln im Wareneingang hängt auch davon ab, ob ein in einem Setartikel auch Serien- bzw. Chargengeführte Artikel enthalten sind.
- Besteht der Artikelset nur aus Artikeln die KEINE Serien-/Chargennummern haben, so muss der Kopfartikel des Artikelsets zugebucht werden. Damit werden die Set-Positionen entsprechend der zugebuchten Kopfmenge dem Lager zugebucht. Die Wareneinstandspreise werden anteilig anhand der Bestellpositionspreise auf die einzelnen Artikel aufgeteilt.
- Ist jedoch zumindest ein Serien-/ Chargennummern pflichtiger Artikel enthalten, so müssen zuerste die einzelnen Setartikelpositionen zugebucht werden um vor allem die Seriennummern der einzelnen Artikel erfassen zu können.<br>
Der Ablauf ist wie folgt:
  - Zubuchen aller einzelnen Setpositionen mit den entsprechenden Mengen
  - Angabe der daraus resultierenden Setmenge auf der Kopfposition des Artikelsets

Das bedeutet, dass beim Versuch den Setartikel-Kopf zuzubuchen, ein Hinweis erscheint, dass die Zubuchung in den Positionen erfolgen sollte.
![](Wareneingang_Artikelset.jpg)
Sind hingegen bereits einzelne Artikel in den Setpositionen zugebucht, so erscheint, bei einem unvollständigen Set der Hinweis, dass dieses Set unvollständig ist. Prüfen Sie in diesem Falle ob Ihre Zubuchungen mit der Lieferung übereinstimmen und handeln Sie entsprechend.

#### Mandanten übergreifende Bestellung?
[Siehe bitte]( {{<relref "/docs/stammdaten/artikel/mehrmandanten/#mandanten-%c3%bcbergreifende-bestellung--auftr%c3%a4ge" >}} ).

#### Wie mit Falschlieferungen durch den Lieferanten umgehen?
Um Ware an Lieferanten z.B. wegen der Lieferung falscher Teile oder defekter / schlechter Teile zu bewirtschaften gibt es zwei Vorgehensweisen:
- Die pragmatische:<br>
Gar nicht einbuchen, also keinen Wareneingang buchen, einfach mit einem Lieferschein mit einer Handeingabe zurücksenden.<br>
Vorteil: Sehr rasch, Wareneingang bleibt offen, der Termin des Wareneingangs wird nicht erfüllt. Damit etwas schlechtere Terminbeurteilung des Lieferanten.<br>
Nachteil: Es fehlt die Dokumentation, dass der Lieferant falsche Teile geliefert hat.
- Die nachvollziehbare:<br>
Die falsche Ware auf ein besonderes Falschlieferungslager zubuchen. Auf Basis des Wareneingangs eine Reklamation erstellen UND dem Lieferanten mittels [Lieferantenlieferschein]( {{<relref "/verkauf/lieferschein/#lieferantenlieferschein" >}} ) die Ware wieder zurücksenden.<br>
Nachteil: Etwas aufwendiger.<br>
deutlicher Vorteil: Es ist der Vorgang dokumentiert und Sie sehen, wieviele Falschlieferungen Ihr Lieferant denn so im Laufe des Jahres produziert und auch Ihr Wirtschaftsprüfer ist glücklich.<br>
Zu beachten. Da die Falschlieferung die Bestellung erfüllt hat, muss die Ware noch einmal bestellt werden. Wenn regelmäßig, z.B. täglich der [Bestellvorschlag]( {{<relref "/einkauf/bestellung/vorschlag">}} ) gemacht wird, wird Sie der Bestellvorschlag an den fehlenden Bedarf erinnern.

#### Wie mit der Vorsteuer im Wareneingang umgehen, wenn man nicht Vorsteuerabzugsberechtigt ist?
Die Herausforderung ist hier, dass von den Lieferanten die Preise immer netto angegeben werden und zusätzlich, dass man mit wachsenden Umsätzen hoffentlich sehr bald in den Genuss des Vorsteuerabzuges kommt. Daher sollte die gesamte Preispflege wie in **Kieselstein ERP** üblich auf Basis der Nettopreis, also der Preise ohne der Vorsteuer (oft auch nicht ganz exakt als Mehrwertsteuer bezeichnet) erfolgen.
Hier ist zusätzlich zu beachten, dass üblicherweise die Umsatzsteuer für den Verkauf auch für die allgemeinen Waren mit 0% definiert ist, damit man zum Stichtag auf die gesetzlichen 19 bzw. 20% umstellen kann. Andererseits sollten aber die von den Lieferanten bereits mitfakturierte Vorsteuer automatisch auf die Nettopreise aufgeschlagen werden. D.h. hier empfiehlt sich, für die Vorsteuer eine eigene Mehrwertsteuer (System, Mandant, Mwst Bezeichnung) anzulegen und für diese dann unter Mwst den von den Inlands-Lieferanten verrechneten Satz anzugeben.
Für diese Funktion musst der Parameter VORSTEUER_BEI_EINSTANDPREIS aktiviert werden. Da die Umstellung in der Regel zu einem Stichtag erfolgt, kann dieser Parameter dann ab einem Stichtag, z.B. 1.1.2020 deaktiviert werden. Gleichzeitig werden Sie dann den Satz für die Umsatzsteuer zum gleichen Stichtag auf den gesetzlichen Wert stellen.

Daher muss bei den Lieferanten dieser neu angelegte Vorsteuersatz definiert werden. Es wird damit erreicht, dass Ihre Artikel mit einem Einstandspreis inkl. Vorsteuer ins Lager gebucht werden. Da Sie in der Regel dann ohne MwSt fakturieren, wirkt sich dies wie beabsichtigt entsprechend in Ihrem DBII der jeweiligen Aufträge aus.

<a name="gemeinsame Betrachtung je Bestellung"></a>

#### Es werden gleiche Artikel zu unterschiedlichen Lieferterminen bestellt, man will aber den besseren Preis bekommen?
Im Artikel, Reiter Bestelldaten gibt es die CheckBox ![](gemeinsame_Betrachtung_je_Bestellung.gif) gemeinsame Betrachtung je Bestellung. Ist dieser Haken gesetzt, wird für die EK-Preisberechnung die Summe der Positionsmengen der gleichen Artikel der Bestellung herangezogen. D.h. immer wenn der Einkaufspreis in der Bestellposition gespeichert wird, werden die Preise der anderen Positionen des gleichen Artikels in dieser Bestellung ebenfalls auf diesen Preis angepasst. Somit ist für diesen Artikel in einer Bestellung immer der selbe Preis gegeben.

#### Wie können Wareneingangsetiketten gedruckt werden?
Grundsätzlich sollten Wareneingangsetiketten direkt aus dem Wareneingang ausgedruckt werden, da nur bei der direkten Erfassung des Wareneinganges auch der jeweilige Artikel eindeutig identifiziert ist. D.h. in der Erfassung der Wareneingänge kann für jede erfasste Wareneingangsposition mittels dem Symbol des Etikettendruckers ![](WE_Etikette.gif) für jede Position eine entsprechende Etikette gedruckt werden.
Zugleich können in diesem Dialog auch weitere Daten erfasst werden.<br>
![](WE_Artikeldaten.jpg)<br>
Klicken Sie dazu wie üblich auf ändern und geben Sie die passenden Daten an. Denken Sie daran danach auch auf speichern zu klicken um die Daten in den jeweiligen Artikel abzuspeichern.

Bitte achten Sie unbedingt auf den Unterschied des Druckes ALLER Wareneingangsetiketten eines Wareneingangs.

Mein dringender Rat aus der Erfahrung vieler Projekte ist, steuere den Einbuchungsprozess so, dass jede Verpackung die eingebucht wird, sofort gekennzeichnet wird. Hier ist die Gefahr des falsch Etikettierens am geringsten. Leider sind aus dem Titel der falsch-Kennzeichnung auch schon Formel1 Autos stehen geblieben.

#### Können aus der nachträglichen Materialentnahme beim Wareneingang gleich die Losetiketten mitgedruckt werden ?
Beim Drucken der Wareneingangsetiketten steht auch die Funktion ![](WEP_inkl_Losetiketten.gif) inkl. Losbuchungen zur Verfügung.
Das bedeutet: Wurde beim Wareneingang die Fehlmengen über die nachträgliche Fehlmengenauflösung gemacht, wird anhand der daraus entstandenen direkten Zubuchungen zu den Losen zusätzliche Wareneingangsetiketten generiert. D.h. wurde z.B. eine Menge von 100Stk zugebucht und davon 5 auf das Los A und 2 auf das Los B gebucht, so werden drei Etiketten ausgedruckt. Am ersten Etikett wird die Menge die direkt ins Lager gebucht wurde ausgedruckt. Auf den beiden Folgeetiketten wird zusätzlich die Information ausgedruckt, auf welches Los dieses Material wieder ausgegeben wurde.

#### Können auch alle Wareneingangsetiketten eines Wareneingangs auf einmal ausgedruckt werden?
Ja. Im Unterschied zu obigem Druck, der einzelnen Wareneingangspositionen, können auch die Etiketten für alle Positionen eines Wareneingangs gedruckt werden. Hier wird zusätzlich davon ausgegangen, dass Sie nicht nur für jede Wareneingangsposition eine Etikette benötigen, sondern diese auch für die entsprechenden Kartons (Verpackungen) der jeweiligen Artikel eine Etikette benötigen. Um die Anzahl der Etiketten zu ermitteln, wird die Wareneingangsmenge durch die Verpackungseinheit des Artikellieferanten dividiert. So erhalten Sie bei einer Verpackungseinheit von z.B. 15Stk für einen Wareneingang von 600 Stk, dementsprechend 40Etiketten.
Mein persönlicher Rat, nutze für den Wareneingang mobile Barcodescanner oder die VDA Scann App um eine sofortige und direkte Zuordnung vom Wareneingang zum Lable zu erhalten und damit die Gefahr der Verwechslung massiv zu reduzieren.