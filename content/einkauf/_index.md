---
title: "Einkauf"
linkTitle: "Einkauf"
menu:
  main:
    weight: 20
weight: 20
---

Die Module des Einkaufs von der Anfrage über den Wareneingang bis zur Eingangsrechnung und zum Zahlungsausgang.
