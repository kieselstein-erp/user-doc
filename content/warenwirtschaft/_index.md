---
categories: ["Warenwirtschaft"]
tags: ["Artikel", "Stücklisten", "Lager"]
title: "Warenwirtschaft"
linkTitle: "Warenwirtschaft"
menu:
  main:
    weight: 10
weight: 10
description: >
 Die zentralen Funktionen der Warenwirtschaft
---
Das Herzstück der Warenwirtschaft ist der Artikel, bzw. der Artikelstamm.<br>
D.h. natürlich ist bei jeder Lagerbewegung eines Artikels auch die Warenwirtschaft mit eingebunden. Im **Kieselstein ERP** wird auch, im Gegensatz zu großen Onlinekonzernen, großer Wert darauf gelegt, dass dies Transaktionsorientiert durchgeführt wird. Das bedeutet: Selbst bei einem Systemabsturz während einer Buchung eines Artikels z.B. vom Lager in einen Lieferschein, ist sichergestellt, dass dieser Artikel entweder schon im Lieferschein ist oder noch im Lager. Ein ist weder dort noch da, oder genauso schlimm, ist noch im Lager aber auch schon im Lieferschein, ist technisch (durch die Transaktionslogik) nicht möglich.<br>
Natürlich benötigt diese Technik auch ihre Zeit.

[Artikel]({{< relref "/docs/stammdaten/artikel" >}})