---
categories: ["Stücklisten"]
tags: ["Stückliste", "Import", "XLS Import"]
title: "Stücklisten Import XLS"
linkTitle: "XLS"
weight: 100
date: 2023-01-31
description: >
  Strukturierter Stücklistenimport anhand XLS Datei
---
Stücklistenimport aus XLS Datei
===============================

Mit diesem  Import steht eine Lösung für z.B. automatisch erzeugte Stücklisten zur Verfügung.

Die generelle Idee dahinter ist: Anhand einer Formelsammlung können mit wenigen Parametern sowohl Material als auch Arbeitsgänge errechnet werden. Da dahinter immer eine Menge an spezifischem Produktwissen steckt, haben Sie hier die Möglichkeit unabhängig von **Kieselstein ERP** die ganze Berechnungspower eines externen Tools, ob das nun ein Spreadsheet oder ein anderes Tool ist, zu nutzen um die für die Materialbewirtschaftung bzw. für die Kapazitätsplanung erforderlichen Positionen rasch und einfach zu erzeugen.

XLS-Import im Menü Stückliste/Import/Positionen

Mögliche Spalten: (Alle Werte/Zahlen müssen auch als solche formatiert sein)
Kopfstueckliste    Kopfbezeichnung    Artikelnummer    Artikelbezeichnung    Position    LfdNr    Kommentar    Menge    Mengeneinheit    Stuecklistenart (Stueckliste/Hilfsstueckliste/Setartikel Fertigungsgruppe APKommentarkennung APKommentarbezeichnung Montageart

Hinweis: In Artikellieferant muss die Kurzbezeichnung des jeweiligen Lieferanten übergeben werden.

Bitte beachten Sie: Die Felder Kopfstueckliste, Artikelnummer und Menge müssen vorhanden und befüllt sein!
Die Default-Stücklistenart ist "Stückliste".

Wenn der Inhalt einer Spalte leer ist, erfolgt kein Import, die eventuell vorhandenen Daten werden nicht überschrieben.
Wenn in einer Stückliste bereits eine Zeile mit der selben Artikelnummer und Position vorhanden ist, dann wird dieser übersprungen.
Die Anordnung der Spalten hat keinen Einfluss auf den Import. Eine Spalte die nicht definiert ist, bewirkt keine Änderung im Stücklistenmodul.
Ist ein Artikel in der Importdatei angegeben aber noch nicht in Ihren **Kieselstein ERP** Daten enthalten, so wird dieser Artikel neu angelegt.

XLS-Import im Menü Stückliste/Import/Arbeitsplan

Mögliche Spalten:
Kopfstueckliste    Kopfbezeichnung    AGNummer    Artikelnummer    Artikelbezeichnung    Maschinennummer    Ruestzeit    Stueckzeit    KommentarKurz    KommentarLang    NurMaschinenzeit    AGArt    AGBeginn    Aufspannung     PPMMenge

Bitte beachten Sie: Die Felder Kopfstückliste und Artikelnummer müssen vorhanden und befüllt sein.

Das Feld Kopfstückliste ist die Identnummer der Stückliste, gibt es den Artikel schon so wird dieser verwendet. Wenn es den Artikel noch nicht gibt, wird er neu angelegt und die Kopfbezeichnung als Bezeichnung übernommen. In diesem Fall werden die Default-Definitionen für die Stückliste verwendet und als Mengeneinheit Stk. definiert.

Wenn das Feld AGNummer nicht definiert ist, dann die wird die Automatik verwendet (das Hochzählen der einzelnen AG-Nummern).
Wenn der Artikel existiert, wird der bestehende verwendet, ansonsten wird ein neuer Artikel mit den Default-Werten der Arbeitszeitartikel angelegt. 
Ist das Feld Nur Maschinenzeit nicht definiert, so wird auch der Haken nicht gesetzt. 
Wenn in einer Stückliste bereits ein Arbeitsgang mit derselben Nummer vorhanden ist, dann wird dieser übersprungen.
Wenn der Inhalt einer Spalte leer ist, erfolgt kein Import, die eventuell vorhandenen Daten werden nicht überschrieben.
Die Anordnung der Spalten hat keinen Einfluss auf den Import.

Ist die AGNummer angegeben, so muss dies eine Zahl sein.
Erscheint die Fehlermeldung AGNummer darf nicht leer sein, so ist die Spalte falsch, nicht als Zahl, definiert.

Der Import baut auf folgenden Definitionen auf:

a.) Definition der Importdatei:

Geben Sie unter System, Arbeitsplatzparameter für den Rechner mit den Daten im Arbeitsplatzparameter ![](Importpfad.gif)

DATEI_AUTOMATISCHE_STUECKLISTENERZEUGUNG den vollständigen Dateinamen für die Importdatei an. Die Sichtbarkeit muss für den verwendeten Client gegeben sein.

b.) In der Datei werden die Blätter
-   HV_Materialpositionen
-   HV_Arbeitsplan
erwartet.

Die Struktur für HV_Materialpositionen muss wie folgt aussehen:

![](XLS_Import_Materialpositionen.gif)

| Feld | Bedeutung |
| --- |  --- |
| Ident  | die Artikelnummer. Wird die Artikelnummer nicht gefunden und ist eine Menge definiert, so wird die Position als Handeingabe angelegt |
| Menge | Die Menge, in aller Regel die Stückzahl 1 |
| Länge | wenn eine mehrdimensionale Mengeneinheit verwendet wird. |
| Breite | wenn eine mehrdimensionale Mengeneinheit verwendet wird. |
| Höhe | wenn eine mehrdimensionale Mengeneinheit verwendet wird. |
| Bezeichnung | Die Bezeichnung der Position wenn keine Artikelnummer angegeben wurde. |
| Mengeneinheit | Die Mengeneinheit der Position. |
| Position | Der Text für die Position (siehe Stücklisten) |
| Kommentar | Die Kommentarzeile mit dazu |
| B-Mitdrucken | 1 = angehakt, leer oder 0 = nicht angehakt |

Die Struktur für HV_Arbeitsplan muss wie folgt aussehen:

![](XLS_Import_Arbeitsplanpositionen.gif)

| Feld | Bedeutung |
| --- |  --- |
| Ident | Die Artikelnummer der Tätigkeit |
| AG | Die Nummer des Arbeitsganges |
| UAG | Die Nummer des Unterarbeitsganges |
| Stückzeit | Die Stückzeit in ms (Millisekunden) |
| Rüstzeit | Die Rüstzeit in ms (Millisekunden) |
| Maschine | Die Identifikationsnummer der Maschine |
| Kommentar_kurz | Der Kurzkommentar des Arbeitsganges |
| Kommentar_Lang | Der Langkommentar des Arbeitsganges |

Ist obiger Arbeitsplatzparameter definiert, so steht (ev. nach dem Neustart des Stücklistenmoduls) im oberen Modulreiter Positionen die Funktion des XLS Imports ![](XLS_Import.gif) zur Verfügung.

Die Datei sollte als Excel(R) 97/2000 oder 2003 zur Verfügung stehen.

Der Import ist so gestaltet, dass die Daten immer hinzugefügt werden.

So können Sie bequem z.B. bestimmte Materialien bereits vorher einpflegen und dann aus der Kalkulation hinzufügen.

Die Import Funktion ist auch so gestaltet, dass auf die Datei nur lesend zugegriffen wird. D.h. Sie können bequem eine Stückliste nach der anderen importieren.

**Wichtig:**

Der Import erfolgt direkt in die Stückliste. D.h. der Artikel der Stückliste und die Stückliste selbst müssen bereits definiert sein.

## Tipp:
Einige Anwender gehen so vor, dass die Importdatei für den Artikel-XLS Import und den Stücklisten-XLS Import in einer Datei aus der CAD zur Verfügung gestellt werden.<br>
Somit wird im Ablauf immer zuerst der Artikelimport durchgeführt und damit die Artikel entsprechend angelegt und danach wird der Import der Stücklisten-Positionen vorgenommen. So hat man relativ rasch und konsistent seine Daten im System.