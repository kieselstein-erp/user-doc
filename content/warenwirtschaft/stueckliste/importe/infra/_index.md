---
categories: ["Stücklisten"]
tags: ["Stückliste", "Import", "INFRA"]
title: "INFRA Import"
linkTitle: "INFRA"
weight: 500
date: 2023-01-31
description: >
  Stücklisten im INFRA Format importieren
---
Stücklistenimport
INFRA
=======================

Wenn der Parameter STRUKTURIERTER_STKLIMPORT= 3 gesetzt ist, so wird für den Import die INFRA-Definition verwendet.

Beim Import können in einem Lauf mehrere Dateien eingelesen werden. Das bedeutet für den eigentlichen Import wird im Stücklistenmodul unter Stückliste/Import nur ein Verzeichnis ausgewählt. In diesem müssen sich die XLS/PDF-Dateien nach nachfolgender Beschreibung befinden.

Datensatzaufbau

Der aus dem INFRA exportierte Datenbestand sollte wie folgt aufgebaut sein:

![](INFRA_Import_Spaltendefinition.gif)

Der Dateiname entspricht der Artikelnummer der bestehenden Stückliste. Gibt es für die Stückliste PDF Dateien, z.B. für Zeichnungen, Arbeitspläne oder ähnlichem so müssen die PDF-Dateien den gleichen Dateinamen wie die Stückliste tragen, jedoch mit der Endung PDF.

Bitte beachten Sie dass nur XLS Dateien importiert werden.

Umsetzung der Ebene

In der Spalte Ebene werden diese mit ..(Punkten) und Zahl hochgezählt. Hier ist die Definition so, dass beim Ebenenwechsel der Kopf-Stücklisten Artikel erkannt wird. Zusätzlich wird gegebenenfalls eine Bemerkung, welche für den Kopfartikel mit dazugegeben wurde (und keine Artikelnummer hat) als diese erkannt und der Kopfartikel aus der Zeile davor übernommen.
D.h. alle Artikel der Ebene 2 werden in die Kopfstückliste importiert.
Die Anzahl der Ebenen ist nicht begrenzt. 
![](INFRA_Import_Spaltendefinition2.gif)

XLS oder XLSX?

Bitte beachten Sie, dass nur XLS Dateien importiert werden. Beachten Sie bitte auch, dass die Spalte E als Zahl formatiert sein muss. Alle anderen Spalten sollten als String/Text formatiert sein.

Die PDF-Files müssen gleich benannt sein und werden in der Dokumentenablage bei der Stückliste hinterlegt.

weitere Infos:

Handeingaben werden ebenso importiert.

Nicht vorhandene Artikel werden angelegt, ebenso Stücklisten

Die Spalte B, PosNr wird unter Position importiert.

Die Spalten G,H,I werden ignoriert.
Die Spalten J + K werden mit | als Trennzeichen in die Spalte Kommentar übernommen.

Bitte beachten Sie, dass die Mengeneinheiten gegeben sein müssen.