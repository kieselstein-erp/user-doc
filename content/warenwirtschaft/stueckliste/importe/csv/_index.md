---
categories: ["Stücklisten","Import","CSV"]
tags: ["Import","CSV"]
title: "Stücklisten CSV Import"
linkTitle: "CSV"
weight: 300
date: 2023-02-01
description: >
 Stücklisten CSV Import
---

## Import im CSV Format

Beim CSV Import werden nur die Positionen in eine einzelne Stückliste importiert. Verwenden Sie dazu das Icon im Reiter Positionen.

Der Datenaufbau dafür ist folgender:

Ident;Menge;Bezeichnung;Mengeneinheit;Position;Kommentar;B-Mitdrucken
Bitte beachten Sie, dass die erste Zeile keine Überschriften enthalten darf, hier werden von Beginn an die Daten für den Import definiert.
| Spalte | Bedeutung |
| --- | --- |
|Ident | Artikelnummer des Artikels |
|Menge | Menge dieser Position |
|Bezeichnung | Bezeichnung des Artikels, wirkt nur, wenn die Artikelnummer im Artikelstamm nicht gefunden wurde. Z.B. für Handeingaben |
|Mengeneinheit | Mengeneinheit der Stücklistenposition.<br>Dies muss eine gültige und passende Mengeneinheit sein. |
|Position | Text für das Feld Position |
|Kommentar | Text für das Feld Kommentar |
|B_MITDRUCKEN | 0 oder 1 ... tragen Sie hier 1 ein wenn "Auf Beleg mitdrucken" gesetzt sein sollte. Sonst bitte 0 eintragen. |

Hinweis: Die zu übernehmenden Artikel müssen im Artikelstamm angelegt sein. Ist dem nicht so, so wird die Position trotzdem übernommen, jedoch ohne Artikelnummer und als Handeingabe.

Bei der Menge ist der Dezimaltrenner der Punkt (.).

Wichtig: Die Datei darf keine reinen Leerzeilen enthalten. Leerzeilen die alle sechs Felder, jedoch leer beinhalten sind zulässig, werden jedoch beim Import übersprungen.

Hinweis: Um z.B. in Open Office leere Datenfelder zu definieren, geben Sie bei den jeweiligen Feldern einfach ein Leerzeichen ein.
