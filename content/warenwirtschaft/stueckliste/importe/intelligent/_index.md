---
categories: ["Stücklisten"]
tags: ["Stückliste", "Import", "frei definierbare Struktur", "XLS","Text", "CSV", "intelligent", "automatische Erkennung"]
title: "Intelligenter Stücklisten Import"
linkTitle: "Intelligent"
weight: 200
date: 2023-01-31
description: >
  Intelligenter Stücklistenimport aus Ihrer CAD
---
Intelligenter Stücklistenimport
===============================

Mit diesem zusätzlichen Modul steht ein strukturierter Import anhand verschiedener Textformate in die Stücklisten zur Verfügung. Dies ist gerade in der Elektronik Industrie mit einerseits vielen Positionen je Stückliste / Baugruppe und andererseits doch der relativ klaren Zuordnung zu Herstellernummern eine wesentliche Funktion.

Die Grundidee ist, dass Sie von Ihrem Kunden / Ihrer Konstruktion / Ihrer Entwicklung eine Stückliste erhalten, in der:

-   Kundenartikelnummer
-   Herstellernummer
-   Ihre Artikelnummer
-   eindeutige Bezeichnung
enthalten sind.

Ein wesentlicher Punkt für den Import ist, dass eine Stückliste einem Kunden zugeordnet werden kann / sollte. D.h. wenn Sie Stücklisten von Ihren Kunden erhalten, so sollten Sie möglichst die Artikelnummern Ihres jeweiligen Kunden in den Sonderkonditionen des jeweiligen Kunden hinterlegen. Durch die Zuordnung der Stückliste zu diesem Kunden weiß **Kieselstein ERP** welche Übersetzungen von der Kundenartikelnummer zu Ihrer eigenen Kundennummer vorgenommen wird. Bei wiederholenden Aufträgen von Ihren Kunden für die Lohnfertigung sparen Sie sich somit ab der zweiten Stückliste eine Menge Zeit um die Daten verlässlich von Ihrem Kunden zu übernehmen.

Manche Menschen sprechen hier auf von einem BOM Konverter. Also genau dem was wir mit dem intelligenten Stücklisten Import machen. Wir konvertieren die Partslist, Bill of material, welche aus Ihrer CAD kommt, in eine Stückliste, mit der der gesamte Fertigungsprozess gesteuert werden kann.

#### Welche Datenformate können importiert werden?
- a.) Formate mit Trennzeichen und flexiblen Spaltenbreiten wie z.B.
	- a1.) CSV Format (Comma Separated Value)
	- a2.) Text Tabs getrennt
	- a3.) XLS Dieses Format wird vor dem eigentlichen Einlesen in Text Tabs getrennt konvertiert und darum der Spaltentrenner fest vorgeschrieben.
- b.) Formate mit festen Spaltenbreiten

WICHTIG: Um die Mengenangaben richtig interpretieren zu können, müssen die Daten im Locale Ihrer Anmeldung angeliefert werden. D.h. wenn Sie z.B. in der Schweiz sind, so müssen die Mengenangaben der Schweizer Schreibweise (' als Tausendertrenner und . als Dezimaltrenner) entsprechen. Um Daten in Ihrer nicht Standard Sprache importieren zu können, müssen Sie sich gegebenenfalls in einer anderen Sprache in **Kieselstein ERP** anmelden.

Um mit einer hohen Wahrscheinlichkeit den richtigen Artikel finden zu können, wird beim Import in folgender Reihenfolge vorgegangen:

1.  gibt es eigene Artikelnummer
2.  gibt es die Herstellerartikelnummer<br>
    Erzielt die die Herstellerartikelnummernsuche mehrfache Ergebnisse, so wird zusätzlich der Hersteller herangezogen und nur in den gefunden Artikeln geprüft.
3.  gibt es die Kundenartikelnummer?<br>Wenn ja wird diese verwendet. Es werden hier auch mehrfache Zuordnungen unterstützt.
4.  suche nach dem Text aus der Importspalte Bezeichnung nach der gleichen Logik wie in Textsuche in der Artikelauswahl
5.  Herstellerbezeichnung
6.  SI-Wert
7.  Bezeichnung 1, 2, 3

Beim Import wird der Inhalt der Spalte Bezeichnung in die Bezeichnung der Handeingabe / des anzulegenden Artikels eingetragen.

Der Inhalt der Spalte Bezeichnung1 wird in die Zusatzbezeichnung eingetragen
der Inhalt der Spalten Bezeichnung2 & 3 wird in die Zusatzbezeichnung2 eingetragen

Ist ein Hersteller angegeben, so wird dieser, wenn er noch nicht in Ihrem Datenbestand enthalten ist, automatisch angelegt. Bitte achten Sie auf die Groß-/Klein-Schreibung.

Beim Import werden gleiche Zeilen optisch zu einer zusammengefasst um die Bedienung zu vereinfachen. D.h. in der Regel wird ein Artikel nur einmal in der Importliste angezeigt. Mehrfache gleiche Artikel werden jedoch als diese mehrfachen Positionen importiert.

Die Anzeige des Importes ist farblich gestaltet und stellt die gefundenen Entsprechungen dar.
Die Bedeutung ist wie folgt:

| Farbe | Bedeutung |
| --- |  --- |
| grün  | es wurde exakt eine Entsprechung gefunden |
| gelb  | es wurden mehrere Entsprechungen gefunden |
| rot  | es wurde keine Entsprechung gefunden |

In der Spalte der Artikel können auch andere Artikel bzw. Handeingaben ausgewählt werden.

Der Ablauf des Imports ist wie folgt:

**<u>1.) Auswahl der Stückliste</u>**

Legen Sie eine entsprechende Stückliste an bzw. wählen Sie eine bereits bestehende Stückliste aus. 
Bei Kundenspezifischen Stücklisten achten Sie darauf, dass in den Kopfdaten der richtige Kunden mit den Sonderkonditionen, also den Artikelnummernübersetzungen, angegeben ist.
Bei einer bestehenden Stückliste werden die importierten Positionen nach den vorhandenen Positionen eingefügt. Gegebenenfalls muss die Stückliste vorher komplett gelöscht bzw. bereinigt werden.

**<u>2.) Import starten</u>**

Um die Importfunktion zu starten wechseln Sie in den Reiter Positionen der gewählten Stückliste und klicken Sie auf den Knopf ![](IntStklImp1.gif) Import.
(Bitte beachten Sie, dass hierzu auch die SI-Wert Zusatzfunktion verfügbar sein muss, sollte das Icon bei Ihnen nicht sichtbar sein, so kontaktieren Sie bitte Ihren **Kieselstein ERP** Betreuer.)

**<u>3.) Nun erscheint die erste Seite des Import-Assistenten</u>**

![](IntStklImp2.jpg)

Wählen Sie hier durch Klick auf Datei die zu importierende Datei aus.

Beachten Sie bitte die Auswahl der Spezifikation (des Stücklistenimports).

Mit klick auf Neue Spezifikation, erstellen Sie eine neue Spezifikation.

Um eine bestehende Spezifikation zu laden, wählen Sie Gespeicherte Spezifikation aus der Liste aus. So kann die gleiche Spezifikation, z.B. für immer den gleichen Kunden, als Voreinstellung verwendet werden.

Nach Auswahl der Datei und der Spezifikation klicken Sie auf weiter. Dadurch wird die ausgewählte Datei geladen und die ersten 100 Zeilen der zu importierenden Datei angezeigt.

**<u>4.) Definition des Importformates und Zuordnung der Spalten</u>**

Bei einer neuen Spezifikation muss das Importformat (Trennzeichen oder Fixe (Spalten-) Breite)) und danach die Zuordnung der Spalten definiert werden. Geben Sie hier auch an, ab welcher Zeile tatsächlich Daten vorliegen.

![](IntStklImp3.jpg)

**Trennzeichen:** geben Sie hier das in der Datei verwendete Trennzeichen an.
| Format |  Bedeutung |
| --- |  --- |
| CSV | ; (die im deutschsprachigen Raum übliche Form für CSV) |
| Text Tabs getrennt | \t |

Wenn kein Trennzeichen verwendet wurde, so nutzen Sie bitte folgende Funktion:

**Fixe Breite:** definieren Sie mittels Maus die entsprechenden Spalten. Bewegen Sie dazu die Maus im unteren Zeilen Fenster und klicken Sie auf die jeweils linken Spaltenränder.

Wurde ein Spaltenrand falsch definiert, so klicken Sie erneut auf die Spalte und die Markierung wird wieder entfernt. Bitte beachten Sie, dass zwischen allen Bereichen eine Trennung eingefügt ist.

**Montageart:** Definieren Sie die für die zu importierenden Positionen zu verwendende Montageart

**Bedeutung der Felder, der Spaltenüberschriften:**

| Name | Import Art | Bedeutung |
| --- |  --- | --- |
| <a name="Kundenartikelnummer"></a>Kundenartikelnummer |S, E | Artikelnummer des Bauteiles ihres Kunden. Es wird die Entsprechung in den Sonderkonditionen des im Kopf der Stückliste definierten Kunden gesucht.**Hinweis:** Wird die Spaltenüberschrift Kundenartikelnummer nicht angezeigt, so wurde die Stückliste keinem Kunden zugeordnet. Definieren Sie in diesem Falle den Kunden in den Kopfdaten der Stückliste. |
| Herstellerartikelnummer|S, E, B | Artikelnummer des Herstellers des Bauteils. Dies wird im Artikelstamm im Reiter Bestelldaten, Herstellernummer erfasst. |
| HerstellerbezeichnungS|, E, B | Bezeichnung des Herstellers für den Artikel. Dies wird im Artikelstamm im Reiter Bestelldaten, Herstellerbezeichnung erfasst. |
| SI-Wert|S, E, B | Spalte die den Wert des Bauteiles hält. Z.B. 100k (100 Kilo Ohm). Siehe dazu auch SI-Wert. |
| Bezeichnung|S, E, B | Bezeichnung des Bauteils, des Artikels |
| Bezeichnung1|S, E, B |   |
| Bezeichnung2|S, E, B |   |
| Bezeichnung3|S, E, B |   |
| Bauform|S, E, B | Die Bauform des Artikels -> siehe Artikel, Technik, Feld Bauform |
| Artikelnummer|S, E, B | Ihre eigene Artikelnummer |
| Menge|S, E, B | Die Menge für den Artikel dieser ZeileWird keine Menge übergeben, so wird diese mit 1 angenommen |
| Position|S, E | Positionsbezeichnung(en) dieser Zeile. Wird nach Position in der Stückliste übernommen. Sollte diese zu lang sein, wird in Kommentar fortgesetzt. |
| Kommentar|S | Eintrag in die Kommentarzeile |
| Referenzbemerkung|E | Eintrag in das Feld Referenzbemerkung |
| Lieferantenartikelnummer|B | Artikelnummer des Lieferanten um die Artikelnummer ermitteln zu können. Siehe Artikel, Artikellieferant, (Lieferanten) Artikelnummer |
| Importpreis|B | Wird als Bestellpreis der Position übernommen |
| <undefined> | | Zuordnung um eine Spalte wieder auf undefiniert zu setzen. |

| Import Art | Bedeutung |
| --- | --- |
| S | ist im Stücklistenimport verfügbar |
| E | ist im Import in die Einkaufsangebotsstückliste verfügbar |
| B | ist im Import der Bestelldaten verfügbar |

**<u>4a.) Zuordnung der Spalten:</u>**

Ziehen Sie mit der Maus die freien gelben Spaltenüberschriften aus dem Kopfbereich auf die entsprechende Spalte. Sollten Sie versehentlich die falsche Spalte ausgewählt haben, verwenden Sie bitte einfach die Spaltenüberschrift ![](IntStklImp4.gif) und ziehen Sie diese auf die falsch definierte Spalte bzw. ziehen Sie einfach die richtige Spaltenüberschrift auf die falsche Spalte.

![](IntStklImp5.jpg)

Sind nun alle Spalten definiert, so klicken Sie bitte auf weiter. Die Definitionen können im weiteren Verlauf unter einem Namen abgespeichert werden, um diese für den nächsten Import wiederverwenden zu können. Bei bestehenden Spezifikationen werden die gespeicherten Informationen des jeweiligen Imports geladen.

**<u>5.) Errechnung der Zuordnung der Artikeln zu den Zeilen:</u>**

Ja nach Umfang der zu analysierenden Zeilen wird ein Fortschrittsbalken angezeigt.

Ist die Analyse beendet, wird das Ergebnis des Vergleichs der Inhalte der Spalten mit dem Artikelstamm in **Kieselstein ERP** mit den entsprechend zugeordneten Artikel, z.B. wie folgt angezeigt:

![](IntStklImp6.jpg)

Hier sehen Sie wiederum die in der Importdatei definierten Zeilen und als rechteste Spalte den daraus errechneten Artikel, mit Artikelnummer und Artikelbezeichnung.

Die farbliche Gestaltung ist so, dass

| Farbe | Bedeutung |
| --- |  --- |
| grün  | es wurde exakt eine Entsprechung gefunden |
| gelb  | es wurden mehrere Entsprechungen gefunden |
| rot  | es wurde keine Entsprechung gefunden |
| weiss  | diese Zeile wird als Handartikel übernommen |

Bei gelb bzw. rot sollten Sie den jeweils richtigen Artikel auswählen bzw. zuordnen.

Klicken Sie dazu in der zu ändernden Zeile in die Spalte des Artikels. In der nun erscheinenden Auswahlbox

![](IntStklImp6.gif)

können neben Handartikel, der Auswahl aus dem Artikelstamm, auch die anderen gefundenen Artikel ausgewählt werden.

Handartikel bedeutet, dass kein Artikel aus dem Artikelstamm zugeordnet wird und dieser nur mit Menge und Bezeichnung in die Stückliste übernommen wird.

Mit dem Klick auf Artikel auswählen wird der Artikelauswahldialog mit seinen Möglichkeiten aufgerufen.

Um einen neuen Artikel anzulegen, wechseln Sie wie gewohnt in das Modul Artikel, legen den Artikel an, nun erscheint dieser auch im Artikelauswahldialog. 

Mit dem Haken bei "Zusammengefasst" (per default angehakt) werden alle Positionen der Stückliste auf eine Position zusammengefasst. Entfernen Sie den Haken um jede einzelne Zeile des Imports zu sehen.

Sind nun alle zuordenbaren Artikel zugeordnet und es sind eventuell noch Positionen übrig geblieben, die nicht zugeordnet werden konnten, so klicken Sie oben auf ![](IntStklImp7.gif). Dadurch werden alle noch nicht definierten Zeilen auf übernehmen als Handartikel gestellt.

**<u>6.) Übernehmen der Daten in die Stückliste</u>**

Durch einen erneuten Klick auf weiter werden die Artikel in die Stückliste übernommen.

Auch hier wird ein Fortschrittsbalken angezeigt.

![](IntStklImp8.jpg)

Zum Abschluss des Importes wird eine kleine Zusammenfassung angezeigt. Hier haben Sie auch die Möglichkeit die Importdefinition unter einem für Sie sprechenden Namen, z.B. Name des Kunden und der CAD Lösung, abzuspeichern um sie beim nächsten Import wieder komfortabel als Vorlage zu verwenden.

Falls Sie die Vorlage nicht speichern möchten, so bitte nicht speichern wählen, oder gegebenenfalls überschreiben Sie eine bereits vorhandene Definition.

Mit dem Klick auf Beenden ist der Stücklistenimport beendet. Sie werden wiederum in den Positionenreiter der Stückliste geführt.

Beachten Sie bitte, dass der Import die Daten immer an das Ende einer bestehenden Stückliste hinzufügt. Gegebenenfalls muss die Stückliste vorher komplett gelöscht bzw. bereinigt werden.

**<u>Hinweis:</u>**

Durch die Zuordnung (z.B. bei gelben Zeilen) von Artikel für den Import werden etwaige Einträge in der Herstellerbezeichnung/Herstellernummer im Artikel ergänzt bzw. überschrieben. Wenn ein Kunde hinterlegt ist und noch keine Sonderkondition besteht, so wird die Kundenartikelnummer im Modul Kunde zum aktuellen Zeitpunkt nachgetragen (hier werden vorhandene SOKOs nicht aktualisiert). Dies dient dem Ziel, dass beim darauffolgenden Import schneller Artikel-Treffer erzielt werden.

#### Bei meinem Import wird die Spaltenüberschrift Kundenartikelnummer nicht angezeigt?
Definieren Sie einen Kunden für diese Stückliste in den Kopfdaten derselben. [Siehe](#Kundenartikelnummer).

#### Wie können Sonderkonditionen (Rabatte) für die importierte Stückliste eingestellt werden?
Nutzen Sie hierzu den Parameter INTEL_STKL_IMPORT_RABATT um fixe Rabatte/Aufschläge zu definieren. Geben Sie den Rabatt in Prozent ein, welcher für, durch den Intelligenten Stücklistenimport neu angelegte, Kundensonderkonditionen verwendet wird.

#### Was  passiert, wenn unterschiedliche Kundenartikelnummern auf den gleichen Artikel in **Kieselstein ERP** verweisen?
Wenn ein Artikel in **Kieselstein ERP** für mehr als eine Kundenartikelnummer verwendet wird, so werden keine Änderungen vorgenommen. Die Kundenartikelnummer wird nur ergänzt, wenn zu dem Artikel in **Kieselstein ERP** noch keine Kundenartikelnummer eingetragen ist.

#### Wie kann man definieren, ob die Kundensonderkonditionen aktualisiert werden sollen?
Bei der Zuordnung der Artikel, definieren Sie, ob die Kunden-Sonderkonditionen für den Artikel aktualisiert werden sollen.

![](soko_update.JPG)

Wenn Sie den Haken rechts oben setzen, so gilt die Vorbelegung für alle Artikel (außer Handeingaben). Die schnellste Zuordnung ist, wenn Sie üben den Haken am Beginn der Spalte definieren, ob alle oder keine Sonderkonditionen aktualisiert werden sollen und danach pro Zeile individuell entscheiden, ob in diesem Fall die Sonderkonditionen des Kunden aktualisiert werden sollen.

Sobald bei manchen Zeilen ein Haken gesetzt ist und bei anderen nicht, so ändert sich der Haken ![](soko_update_icon.JPG) in ein Viereck um die Abweichung anzuzeigen.

#### Definition der Wirkung der Bezeichnungen für die Suche
Die Suche nach den passenden Artikel erfolgt in folgender Reihenfolge, wobei die Art der Suche für alle Module in gleicher Logik abläuft
Der Importer sucht mit den zugeordneten Labels in folgender Reihenfolge in den Daten des Artikels, wobei dies so programmiert ist, dass wenn ein Kriterium zutrifft, wird die Suche damit für den einen Artikel beendet.
1. Lieferanten- bzw. Kundenartikelnummer (im Artikellieferant bzw. Kundensonderkonditionen)
2. Herstellerartikelnummer
3. Artikelnummer
4. Herstellerbezeichnung
5. Si-Wert
6. Suche nach Bezeichnungen, wobei diese logisch ge-odert sind. D.h. es wird für den in der Spalte Bezeichnung (bzw. Bezeichnung1, 2, 3) enthaltenen Text ein Eintrag in den Bezeichnungen in der Reihenfolge von Bezeichnung, Zusatzbezeichnung, Zusatzbezeichnung2\. Es wird jeweils in der angemeldeten Sprache des Artikels gesucht. Gibt es für eine Zuordnung ein Suchergebnis, so wird damit die Suche für den Artikel beendet.
   
Beispiel:<br>
In der Spalte Bezeichnung (des Importers) steht 100nF. In der Spalte Zusatzbezeichnung des Artikels steht 100nF. So ergibt die Suche von Importer-Bezeichnung gegen Artikel-Bezeichnung kein Ergebnis, also wird die Suche von Importer-Bezeichnung gegen Artikel-Zusatzbezeichnung fortgesetzt und da es ein Ergebnis gibt wird die Suche für diesen Artikel damit beendet.

Sobald ein Ergebnis, also ein oder mehrere Artikel, gefunden wurde, wird die Suche für den Artikel beendet. Wird genau ein Artikel über Lieferanten-, Kunden-, Herstellerartikelnr oder Artikelnummer gefunden, wird die Zeile grün.
Beinhaltet das Ergebnis mehr als eine maximale Anzahl (zurzeit 100) an Artikeln werden diese in der Ergebnistabelle nicht angezeigt.

#### Wie werden die Spalten in die jeweilige Stücklistenart übernommen?
Beim Import wird bei jeder Position folgendes gesetzt:
- Menge
- Wenn als Positionsart (Zeile) ein Artikel gewählt wurde
  -   Positionsart "Ident"
  -   Artikel mit seiner Bezeichnung
  -   Einheit des Artikels
-   bei Handartikeln:
  -   Positionsart "Handeingabe"
  -   Mengeneinheit "Stk"
  -   Die Bezeichnung wird gefüllt durch den ersten gefundenen String, der nicht null oder leer ist, aus folgenden Labels genau in dieser Reihenfolge: Bezeichnung, Bezeichnung1, Bezeichnung2, Bezeichnung3, Artikelnummer, Herstellerartikelnummer, Kundenartikelnummer, Si-Wert, Bauform, Artikelnummer

- Je nach Stücklistenart werden die benötigten spezifischen Felder eingetragen:
  - (klassische) Stückliste:
    - Position, vom zugewiesenen Label "Position"
    - Montageart auf default = 1.Wert der Montagearten
    - Kommentar ( Label "Kommentar")
    - Auf Beleg mitdrucken mit falsch (nicht aktiviert, kein Haken gesetzt)
  - Einkaufsangebotsstückliste
    - Position
    - Bemerkung (Label "Referenzbemerkung")
    - Artikelnummerhersteller, wenn die Zusatzfunktion WebPreisAbfrage verfügbar ist.
    - Auf Beleg mitdrucken mit falsch (nicht aktiviert, kein Haken gesetzt)
  - Angebotsstückliste:
    - Auf Beleg mitdrucken mit falsch (nicht aktiviert, kein Haken gesetzt)
    - Befüllung der Preise
  - Bestellung:
    - Bestellstatus auf "Offen"
    - Liefertermin auf aktuellen Kopfliefertermin
    - Rabatt auf 0.0
    - Nettoeinzelpreis
    - Nettogesamtpreis entweder Lieferantenpreis oder Bestellpreis, je nach Auswahl
    - Materialzuschlag auf 0.0