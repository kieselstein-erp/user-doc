---
categories: ["Stücklisten"]
tags: ["Stückliste", "Import", "Siemens NX CSV"]
title: "Stücklisten Import Siemens NX"
linkTitle: "Siemens NX"
weight: 600
date: 2023-01-31
description: >
  Typ1 Stücklistenimport, Siemens NX CSV
---
Stücklistenimport Typ2 
Siemens NX CSV
======================================

Mit diesem  Import steht ein strukturierter Import anhand der von Siemens NX erzeugten Stücklistenstruktur, welche normalerweise im CSV Format ausgegeben wird, zur Verfügung. Sie können damit komplette Maschinen in einem Zug nach **Kieselstein ERP** übernehmen.
Von der Art des Imports her, wird hier vorausgesetzt, dass alle Artikel und alle Stücklisten bereits vollständig in **Kieselstein ERP** angelegt sind. Es werden lediglich Identnummern und Mengen übernommen, bzw. bei mehrdimensionalen Artikeln auch die Länge und Breite.

Für den Import nach Typ2 **Siemens NX** muss der Mandantenparameter STRUKTURIERTER_STKLIMPORT auf 2 gestellt werden.

Nach dem Neustart des Moduls Stückliste finden Sie unter 
![](Stkl_Import.gif) den Punkt Import.

Der Aufbau der Datei ist wie folgt:
Aufbau ist fix. Ebenen = eingerückte Space.
Es werden KEINE Artikel angelegt, es müssen alle Artikel auch Stücklisten, Unterstücklisten bzw. Hilfsstücklisten bereits definiert sein.

Es werden nur Position übernommen die auch einen gültigen Artikel haben.
Es wird eine Liste mit den Positionen die nicht importiert werden konnten nach dem Import ausgegeben.

Beim Stücklistenimport Typ2 kann in den Kopfdaten einer Stückliste eine sogenannte Einkaufsstückliste angegeben werden.
Die Idee dahinter ist, dass am Anfang eines Projektes die wichtigen Teile in der Einkaufsstückliste angelegt werden. 
Damit wird erreicht, dass über die Los und den Bestellvorschlag diese Bedarfsinformation an den Einkauf gelangt.
Wurden nun die Teile in der Konstruktion tatsächlich eingeplant, so sind sie in den NX-Stücklisten enthalten. Daher wird beim Import ein Artikel der in der Importliste als auch in der Einkaufsstückliste definiert ist in der Einkaufsstückliste um die übergebene Menge reduziert.
Wichtig: Die Mengenreduktion erfolgt bei jedem Import, auch wenn Sie dieser mehrfach durchgeführt wird, wird bei jedem Importlauf die Menge reduziert. Wurde die in der Einkaufsstückliste eingetragene Menge einer Position verbraucht, wird diese gelöscht. 

Nutzen Sie mit dieser Funktion auch die Aktualisierung aller Lose, über den Menüpunkt Bearbeiten Lose aktualisieren aus dem Modul Stücklisten.

#### Wie werden Abmessungen übernommen.
Aus der mechanischen Konstruktion kommen immer auch Abmessungen für z.B. Blechteile. Um diese übernehmen zu können wurden folgende Definitionen getroffen:
-   Es definiert die Mengeneinheit des Artikels ob Abmessungen importiert werden sollen.
    -   D.h. ist der Artikel in m² definiert, so wird die Länge und Breite übernommen und die Stücklisten-Positions-Mengeneinheit auf mm² gesetzt. Zusätzlich wird die Menge dieser Stücklisten Position übernommen.
    -   Ist der Artikel in m definiert, so wird die Länge übernommen und die Stücklisten-Positions-Mengeneinheit auf mm gesetzt. Zusätzlich wird die Menge dieser Stücklisten Position übernommen.
    -   Ist der Artikel in Stk. definiert so wird die Menge übernommen.
    -   Alle anderen Mengeneinheiten werden nicht unterstützt und der Import abgebrochen.
-   Artikel mit der Bezeichnung .... werden als Rohmaterial übernommen

Es werden nur diejenigen Positionen importiert, die in der Spalte xxx ein M eingetragen haben.

Achten Sie darauf, dass bei den Mengeneinheiten auch die Dimensionen richtig definiert sind. D.h. für Meter und Millimeter Dimension 1 = Eindimensional und für m² und mm² Zweidimensional, also die Dimension 2.