---
categories: ["Stücklisten"]
tags: ["Stückliste", "Import", "Solid Works"]
title: "Stücklisten Import Solid Works"
linkTitle: "Solid Works"
weight: 400
date: 2023-01-31
description: >
  Typ1 Stücklistenimport, Solid Works
---
Stücklistenimport Typ1 
Solid Works XLS
=======================================

Mit diesem  Import steht ein strukturierter Import anhand der von Solid Works erzeugten Stücklistenstruktur, welche normalerweise im XLS Format ausgegeben wird, zur Verfügung. Sie können damit komplette Anlagen in einem Zug nach **Kieselstein ERP** übernehmen.

Der besondere Vorteil dieses Imports ist, dass Artikel die im Import definiert sind, aber noch nicht im Artikelstamm von **Kieselstein ERP** angelegt sind, neu angelegt werden. Zugleich werden gegebenenfalls dafür vorhandene Zeichnungen (PDF-Dateien) automatisch dem Artikel zugeordnet.
Ebenso wird die komplette Stücklistenstruktur entsprechend aufgebaut.

Zusätzlich steht hier auch noch der Import in den Anfragevorschlag zur Verfügung.
D.h. Sie können, neben der Fertigungssteuerung für den tatsächlichen Bedarf auch sehr komfortabel die anzufragenden Daten in den Anfrage Vorschlag übernehmen. Da in der Importdatei auch die Liefergruppe jedes Artikels mit übergeben wird, kann so auf einfache Weise eine komplette Liefergruppenanfrage an Ihre Lieferanten mit entsprechenden Zusatzinformationen der Zeichnungen gestellt werden.

Aus den Stücklisten wird der übliche **Kieselstein ERP** Weg beschritten. D.h. es werden aufgrund der angelegten Kundenaufträge entsprechende Fertigungsaufträge über die importierten Stücklisten erzeugt. Auf Basis der Fertigungsaufträge wird nun mit dem Bestellvorschlag das tatsächlich benötigte Material errechnet.
Der Trick liegt dann darin, dass aus den Anfragen die tatsächlichen Lieferanten in den Artikelstamm eingepflegt werden und somit dann die Bestellungen automatisch an die richtigen Lieferanten gemacht werden können.

Für den Import nach Typ1 **Solid Works** muss der Mandantenparameter STRUKTURIERTER_STKLIMPORT auf 1 gestellt werden.
Nach dem Neustart des Moduls Stückliste finden Sie unter 
![](Stkl_Import.gif) den Punkt Import.

Die Stücklisten werden strukturiert importiert.
Abhängig von der Pos.Nr werden die jeweiligen Positionen in eine Stückliste übernommen.
Alle nicht vorhandenen Artikel bzw. Stücklisten werden angelegt.
Am Ende des Imports erscheint gegebenenfalls ein Fenster, welche Artikel neu angelegt wurden.

Unterscheidung Normteile oder Eigenkonstruktion.

Wird für einen Artikel eine Zeichnung übergeben, so ist dies grundsätzlich kein Normteil, sondern eine Eigenkonstruktion. Anhand des Datums der Zeichnungs-Datei wird entschieden ob dies eine neuere Version des Eigenkonstruktionsteiles ist. D.h. ist das Datum neuer, so ist dies ein neueres Eigenkonstruktionsteil. Das bedeutet, dass ein weiterer Artikel angelegt wird, welcher grundsätzlich die gleiche Artikelnummer hat, jedoch am Ende der Artikelnummer wird mit _nnn die Version hochgezählt. Die alten Artikel werden auf versteckt gesetzt. Somit ist für Sie sofort ersichtlich welche die aktuelle Version eines Artikels ist.

Für den Import der Datei wird der Pfad der Importdatei verwendet. Es wird davon ausgegangen, dass die Artikelnummer ohne der Versionserweiterung und der Dateiname ohne Extension übereinstimmen. Weiters wird davon ausgegangen, dass pro Eigenkonstruktion nur jeweils eine Zeichnungsdatei übergeben wird. Diese kann eine beliebige Dateiendung haben. Bei mehreren wird die erste zufällig gefundene Datei importiert.
Die Konstruktionszeichnungen für die Eigenkonstruktion wird im Artikel unter Kommentar abgelegt. Welche Kommentarart verwendet wird, muss in den Stücklisten Grunddaten unter Kommentarimport definiert werden. Es darf nur eine Kommentarart für den Import definiert werden.

Um für die Stücklisten auch entsprechende Arbeitspläne effizient erzeugen zu können, sollten Sie in den Mandantenparametern auch den Parameter ARBEITSPLAN_DEFAULT_STUECKLISTE definiert haben.

Beim Import werden:
- die Artikelnummern, die groß / klein sind alle auf Großbuchstaben konvertiert
- Bei Normteilen werden nur Mengen und Artikelnummern importiert, keine Bezeichnungen usw.     Normteile sind die Teile die keine Zeichnungen hinterlegt haben
- Die Spalte Material wird nach der Spalte Material (Artikel, unterer Modulreiter Material) zugeordnet. Ist das Material nicht definiert, wird der Import abgebrochen
- Die Bezeichnung wird nur bei Zeichnungsteilen in die ersten beiden Zeilen des Artikels übernommen.
- Der Typ wird in die Liefergruppe des jeweiligen Artikels übernommen
- Die Abmessungen werden in der Zusatzbezeichnung2 des Artikels hinterlegt

Der Trenner für die Ebenen-Definition ist der Punkt "."

Wenn eine Stkl-Position bereits vorhanden ist (= Artikelnummer/Einheit/Dimensionen stimmen zusammen), wird diese mit den erhaltenen Daten aktualisiert, ansonsten wird die Position neu angelegt. Eventuell nicht mehr vorhandene Positionen werden gelöscht. Wenn das Löschen aufgrund von Abhängigkeiten nicht möglich ist, wird die Pos-Menge auf 0 gesetzt.

**Info:** Bitte beachten Sie, dass die Spalte 3 = Dateiname = Artikelnummer ohne führende bzw. abschließende Leerzeichen definiert sein muss.

Neben dem Import in die Stückliste gibt es auch den Import in den Anfragevorschlag. Dieser funktioniert vollkommen gleich, inkl. der Anlage der Stücklisten usw., es wird jedoch zusätzlich je Artikel ein Eintrag in den Anfragevorschlag vorgenommen.

Der Dateiaufbau ist auf folgendes Format als XLS Datei festgeschrieben.![](Solid_Works.JPG)

**Wichtig:**

Es müssen die Felder Menge und Gewicht in der Datei als Zahl definiert sein.

In die Stücklistenposition werden nur diejenigen Zeilen übernommen, bei denen auch eine Menge <> 0 eingetragen ist.

Wenn keine Maßeinheit definiert ist, wird als Mengeneinheit Stück verwendet.

Die Übernahme des Inhaltes einer bestehenden Stückliste wird so durchgeführt, dass der gesamte Stücklisteninhalt gelöscht wird und dann die im Importfile angegebenen Positionen neu eingetragen werden.

Sind Menge und Gewicht nicht als Zahl definiert, so erscheint eine entsprechende Fehlermeldung.