---
categories: ["Stücklisten", "Importe"]
tags: ["Stückliste", "Importe"]
title: "Stücklisten Importe"
linkTitle: "Importe"
weight: 400
date: 2023-01-31
description: >
  Daten in Stücklisten importieren
---
Es stehen verschiedene, teilweise auch strukturierte Importformate zur Verfügung.
