---
categories: ["Stücklisten"]
tags: ["Stückliste", "Prüfungs Definitionen"]
title: "Prüfungsdefinitionen"
linkTitle: "Prüfungsdefinitionen"
weight: 600
date: 2023-01-31
description: >
  Definitionen von Prüfungen je nach Stückliste
---
Prüfungen
=========

Mit dem Modul Prüfungen können derzeit Prüfanweisungen für die Fertigung von Kabelbäumen in **Kieselstein ERP** definiert werden.
Es stehen damit folgende zusätzliche Definitionsmöglichkeiten zur Verfügung:
- a.) Definition von Werkzeugen und deren Verschleißteilen im Modul Artikel in einem eigenen unteren Modulreiter
- b.) Definition der Prüfkombinationen in den Grunddaten des Moduls Stücklisten
- c.) Definition des Prüfplans jeder Stückliste

**<u>Hinweis:</u>**<br>
Die effiziente Verwendung der Prüfungen ist an ein über die RestAPI angebundenes Erfassungsgerät gebunden, welches aktuell nicht im Lizenzumfang des **Kieselstein ERP** enthalten ist

## Definition von Werkzeugen
Im Artikel steht der unterer Modulreiter Werkzeuge zur Verfügung.
Hier sehen Sie alle Werkzeuge die in Ihrer **Kieselstein ERP** Anwendung über alle Mandanten zur Verfügung stehen. Werkzeuge können nur bearbeitet werden, wenn Sie im jeweiligen Mandanten angemeldet sind.
Es werden hier alle Werkzeuge angezeigt, die in Ihrer **Kieselstein ERP** Installation, also über alle Mandanten, zur Verfügung stehen.
Die Werkzeuganlage kann für alle Mandanten durchgeführt werden. Die Änderung von Werkzeugdefinitionen ist nur vom jeweils ausgewählten Mandanten (=Standort ![](Pruefungen1.gif)) aus möglich.

## Definition von Verschleissteilen
Für jedes Werkzeug können zusätzlich sogenannte Verschleissteile, also jene zusätzliche Einsatzwerkzeuge, welche für das eigentliche (Haupt-)Werkzeug eingesetzt werden können und die einem entsprechenden Verschleiss unterliegen.

## Definition der möglichen Prüfkombinationen
In der Kabelfertigung geht es im wesentlichen darum, für welche Materialkombinationen Kontakt / Litze welche Werkzeuge zum Einsatz kommen und welche Messwerte / Toleranzen einzuhalten sind. Dafür können verschiedene Prüfarten definiert werden. Diese sind:

| Prüfart | Beschreibung / zu erfassende Werte | Ergebnisse |
| --- |  --- |  --- |
| Crimpen mit Isolation | Crimphöhen und -breiten für Draht und Isolationinkl. Angabe der erlaubten Toleranzen | Höhe / Breite |
| Crimpen ohne Isolation | Crimphöhe und -breite für Draht und Angabe der erlaubten Toleranzen | Höhe / Breite |
| Kraftmessung | Angabe der Kraft in N welche erforderlich ist, um den Draht / die Litze aus dem Kontakt zu reissen. Die genaue Beschreibung zur durchzuführenden Prüfung geben Sie bitte unter Kommentar ein. | gemessene Kraft |
| Massprüfung | Angabe des Maßes inkl. Toleranz auf das geprüft werden sollte | gemessenes Maß z.B. Länge |
| optische Prüfung | Beschreibung auf welche optische Eigenschaft geprüft werden sollte. | in Ordnung / nicht in Ordnung |
| elektrische Prüfung | Angabe welches elektrische Verhalten geprüft werden sollte. | in Ordnung / nicht in Ordnung |

Bitte beachten Sie, dass die Prüfkombinationen nur von einem Benutzer gleichzeitig bearbeitet werden dürfen. Für die Bearbeitung ist zusätzlich zum Artikel Schreibrecht auch das Recht REKLA_QUALITAETSSICHERUNG_CUD erforderlich.

Die grundsätzliche Definition der möglichen Prüfkombinationen erfolgt im Modul Stückliste, unterer Modulreiter Grunddaten, oberer Reiter ![](Pruefungen2.gif) Prüfkombination.

Info: Die Übersetzungen für die Bezeichnungen der Prüfarten können im Reiter Prüfarten gepflegt werden.

## Definition der durchzuführenden Prüfungen je Stückliste
Der für die jeweilige Stückliste durchzuführende Prüfplan muss je Stückliste im Reiter Prüfplan definiert werden. Auch für diesen Reiter ist das Recht REKLA_QUALITAETSSICHERUNG_CUD erforderlich. Die gewünschten / notwendigen Prüfungen müssen aus den definierten Prüfkombinationen für die jeweilige Stückliste die zutreffenden Prüfungen ausgewählt / definiert werden.<br>
Diese ergeben sich wiederum aus den in der Stückliste enthaltenen Positionen.<br>
Um auf einfache Art die Kontakt Litzen Kombination für den Prüfplan der Stückliste auswählen zu können, wurde die Funktion Prüfplan erzeugen ![](Pruefungen3.gif) im Reiter Positionen der Stückliste geschaffen. Das bedeutet: Klicken Sie zuerst auf den Kontakt (also die Zeile die den Kontaktartikel enthält) und dann auf die Litze und anschließend auf Prüfplan erzeugen. Es wird nun die Standard Prüfkombination für das Artikelpaar (Kontakt + Litze) in der Definition der Prüfkombinationen gesucht.<br>
Wird keine Kombination gefunden, so wird ![](Pruefungen4.jpg)  angezeigt. Bitte beachten Sie hier in jedem Falle die Reihenfolge der Markierung der Zeilen.
Wird die Kombination gefunden, so wird ein neuer Prüfeintrag im Prüfplan der Stückliste begonnen und Sie müssen nun noch Prüfart und Werkzeuge o.ä. definieren.

## Wirkung des Prüfplanes in der Fertigung
Der Prüfplan für die jeweilige Stückliste kann auch vom Los aus, ergänzt um die Positionsinformationen für die Fertigungsunterlagen ausgedruckt werden.<br>
Bei der Teil-Ablieferung eines Loses sollte / müssen auch die Prüfergebnisse im Reiter Prüfergebnis eingegeben werden.<br>
Die Prüfergebnisse selbst können unter Journal, Prüfergebnisse ausgedruckt werden. Beim Klick auf Los wird das Los vorgeschlagen, von dem aus Sie diese Auswertung aus aufgerufen haben.

## Können die Kommentare / Texte der Prüfkombinationen auch übersetzt werden ?

A: Ja. Melden Sie sich wie in **Kieselstein ERP** üblich bitte in der jeweiligen Clientsprache an und geben Sie bitte bei den Kommentaren die Übersetzungen an. Um die Übersetzungsarbeit zu erleichtern sehen Sie in den Vorschlägen die ursprünglichen (deutschen) Texte, solange bis Sie dies in die gewünschte Sprache übersetzt haben.

## Zusammenhang Prüfkombinationen, Prüfplan und Erfassungsterminal
In den erlaubten Prüfkombinationen werden die von Ihnen definierten Prüfkombinationen vorgegeben.
Diese werden in den jeweiligen Stücklisten Prüfplan übernommen.<br>
Beim Anlegen des Loses wird die Prüfkombination aus der Stückliste übernommen und kann gegebenenfalls im Los noch abgeändert werden.<br>
Bei der Ausgabe des Loses wird geprüft, ob die im Los definierten Prüfkombinationen noch gegeben sind.<br>
Bei der Erfassung der Ablieferbuchungen wird davon ausgegangen dass für die Prüfung mit den Vorgabewerten die Prüfkombinationen nach wie vor gegeben sind. Sollten diese nicht mehr gegeben sein, so kann die Einhaltung der Vorgabewerte bei der Erfassung der Messwerte nicht überprüft werden. Somit werden die Ablieferungen automatisch in das erste definierte Schrottlos des Mandanten gebucht. Diese Buchung hat den Zweck, dass in aller Regel die QS, die entsprechenden Prüfungen durchführen muss, damit sichergestellt ist, dass nur Ware die Ihrem Qualitätsstandard entspricht auch an den Kunden geliefert bzw. weiterverarbeitet wird.

**Warum wird keine Verriegelung vorgenommen?**<br>
Einerseits könnten wir uns in den Prüfplanpositionen der Stückliste und des Loses merken, welche Prüfkombination ursächlich verwendet wurde. Andererseits ist es aufgrund der vielen Kombinationsmöglichkeiten der Prüfkombinationen fast unmöglich sicherzustellen, dass eine einmal definierte Prüfkombination, bei der gleichzeitigen Forderung diese jederzeit ändern zu können (Sie wollen sich ja weiterentwickeln), genau in der gewählten Kombination angepasst werden kann / können werden muss. Denken Sie in diesem Zusammenhang auch an den automatischen Austausch von Artikeln, Änderung der Vorgabewerte von Artikel usw..<br>
Wir haben uns deshalb dafür entschieden, dass die Prüfungen auf das bestehen einer Prüfkombination beim Anlegen und beim Ausgeben des Loses erfolgt. Da wir in der Regel auch noch von freigegebenen Stücklisten ausgehen, wird es in sehr sehr seltenen Fällen vorkommen, dass sich Prüfkombinationen ändern. Für diese sehr seltenen Fälle ist es trotzdem möglich zu produzieren und auch Lose über das Erfassungsterminal abzuliefern, es werden jedoch sowohl die Ablieferungsbuchungen der Freigabeprüfungen bzw. der laufenden Produktion in das Schrottlager gebucht.

#### Können die Prüfpläne auch ausgedruckt werden?
Ja wählen Sie die gewünschte Stückliste und dann Info, Prüfplan.

Um eine Gesamtübersicht aller Prüfpläne zu bekommen haken Sie bitte alle Stücklisten an.