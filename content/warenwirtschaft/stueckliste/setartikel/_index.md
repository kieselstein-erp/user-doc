ss---
categories: ["Stücklisten"]
tags: ["Stückliste", "Setartikel", "Einkaufs-Set", "Verkaufs-Set"]
title: "Setartikel"
linkTitle: "Setartikel"
weight: 200
date: 2023-01-31
description: >
  Definition von Artikel-Sets für Ein- und Verkauf.
---
Setartikel
==========

Alternativ zu Stückliste und Hilfsstückliste steht in **Kieselstein ERP** auch der Setartikel als Stücklistenart zur Verfügung.

**Wichtig:** In aller Regel ist ein Setartikel nicht lagerbewirtschaftet. Beachte dies bitte bei der Anlage der Artikel.

Errechnung des Preises der Unterposition des Setartikels:<br>
VK-Preisbasis * Menge (laut Setartikeldefinition)<br>
--------------------------------------------------  x Verkaufter Preis (Nettopreis)<br>
VK-Wert des gesamten Setartikels<br>

Hinweis: Diese Berechnungsmethode wird auch für den Einkaufsset verwendet. 
In Einkaufsset kann es vorkommen, dass damit Mengenverteilungen vorgenommen werden.
Z.B. Du kaufst 10kg Schweinsschöberl, welche aus 6kg Fleisch A und 4kg aus Fleisch B besteht. Das kg kostet in beiden Fällen 4,- €
Um dies abzubilden, definiere bitte für den Artikellieferanten für Fleisch A und für Fleisch B den gleichen Preis, in der Regel 4,- €
Durch die Verteilungsberechnung ist gegeben, dass bei einer Änderung des Einstandspreises der gleiche Preis auf die einzelne Position des Setartikels durchgereicht wird.

Ein Setartikel (Artikelset), dient der Abbildung zusammengestellter Komponenten die als Verkaufsset ausgeliefert werden (z.B. Bohrmaschine im Koffer mit Netzteil und Bohrerset).
Es wird in der Stückliste ein Artikelset definiert.

Nun wird dieses Set in das Angebot -> Rechnung, Gutschrift übernommen.

Die Auswahl des Sets bewirkt dass:
- der Setartikel (nur) als Überschrift eingetragen wird, dieser definiert den Preis des Produktes
- die durch die Artikelset-Stückliste definierten Komponenten werden als eigene Positionen eingetragen und vom Lager abgebucht
- der Verkaufspreis, für die Statistik, definiert sich aus dem Setartikel. ACHTUNG: dieser kann auch manuell verändert werden.
Sind in den Set-Positionen Lagerbewirtschaftet, Chargen-/Seriennummern geführte Artikel enthalten, so müssen in LS, RE, GS, BS diese erfasst werden.
Wenn ein Setartikel in LS oder RE ausgewählt wurde, dann kann anstatt des Lagers 'KEIN_LAGER' ein Lager ausgewählt werden. Somit werden alle Set-Positionen von dem gewählten Lager abgebucht.

Hilfreich ist der Set-Artikel vor allem für die Abbildung von Handelsgeschäften oder wenn einzelne kombinierte Komponenten ohne den Zwischenschritt einer Fertigung an den Kunden geliefert werden.
Sobald der Preis des Set-Artikels festgelegt wurde, werden die Preise der einzelnen Positionen anteilig aufgeteilt.
Im jeweiligen Beleg können einzelne Komponenten je nach Anforderung angepasst /abgeändert werden (zb. der schwarze Koffer für die Bohrmaschine ist nicht auf Lager, der blaue schon).

### Beispiel der Verkaufs-Preisverteilungsberechnung:
Die Preisverteilung erfolgt nach Verkaufs-Werten. Dafür ist es erforderlich, dass jeder Artikel im Artikelset auch eine Verkaufspreisbasis hat (sonst geht die Verteilungsberechnung nicht richtig)<br>
Beispiel: immer Verkaufspreise
Menge | Bezeichnung | Einzelpreis | Gesamtpreis
---|---|---|---|
1Stk| Setartikelkopf | 1.000,- |
2Stk| Set Position A | Listenpreis 300,- | 600,-
1Stk| Set Position B | Listenpreis 500,- | 500,-
---| --- |--- |---
-| Verkaufswert der Positionen | | | 1.100,-
-| Korrekturfaktor: 0,90909, also:
2Stk | Pos A | 272,7272 |
1Stk | Pos B | 454,5454 |
-| Ergibt in diesem Falle zufällig 999,99994545 was nach BankersRounding exakt 1.000,- ergibt. Sollte sich das nicht ausgehen, wird der teuerste Artikel korrigiert. | 

**<u>Hinweis:</u>**<br>
Manchmal möchte man zwar einen Setartikel verkaufen, aber eigentlich sollte sich der Setpreis von selbst errechnen. Dies ist mit der Funktion intelligente Zwischensumme (ohne Positionspreis) möglich. Siehe dort, bzw. Gemeinsamkeiten der [Bewegungsmodule]( {{<relref "/verkauf/gemeinsamkeiten" >}} ).

### Berechnung des Wertes einer Position für das Bewegungsmodul:
Dies geschieht ausschließlich auf Basis des Preises der Setposition.
Die Preise der Unterpositionen des Setartikels werden vor allem zu Statistikgründen in die Bewegungspositionen eingetragen.

Statistik für Setartikel und Set-Unterartikel.
Es werden beide Positionen in die Statistik aufgenommen. Damit sehen Sie einerseits wieviele Sets Sie verkauft haben und andererseits wieviel von den einzelnen Artikeln benötigt wurde.

**Wichtig:** Aus diesem Grunde sind die Setartikel in der Hitliste faktisch zweimal enthalten.
Einmal als der eigentliche Setartikel (Kopf) und dann für jede Setartikelunterposition. Daher sind in den Statistiken diese Positionen üblicherweise in orange mit K(opf) und P(osition) gekennzeichnet.

Der Andruck der Setartikel in den Formularen kann entsprechend gesteuert werden. Sollten hier Anpassungen erforderlich sein, so wende dich bitte an deinen **Kieselstein ERP** Betreuer.

### Können Setartikel untergeordnete Stücklisten / Setartikel in den Positionen enthalten?
Nein. Ein Setartikel wird derzeit nur in einer Ebene abgebildet. D.h. ein Setartikel kann keine Hilfsstückliste und kein weiteres Artikelset enthalten. Da aber eine (produzierte) Stückliste in dieser Betrachtung ein gewöhnlicher Artikel ist, kann ein Setartikel im Verkauf jederzeit eine Stückliste enthalten. Im Einkauf / Wareneingang wenn es eine Fremd-gefertigte Stückliste wäre.

### Können Setpositionen Serien-/Chargennummern beinhalten?
Ja. Dies ist gerade für Geräte Sets eine sehr praktische Funktion.<br>
Bei der Abbuchung der Sets müssen in der Reihenfolge der Positionen der Stücklisten die jeweiligen Serien-/Chargennummern der Artikel angegeben werden.

**WICHTIG:** Die Lieferung von Chargen- bzw. Seriennummernbehafteten Setpositionen steht ausschließlich in Verbindung des Lieferscheines mit dem Auftrag zur Verfügung. D.h. um z.B. chargengeführte Positionen von Setartikeln ausliefern zu können, muss immer ein Auftrag angelegt werden und somit bei der Lieferung auch der Auftragsbezug hergestellt werden.

### Können Sets in den Positionen modifiziert werden?
Ja. Ein Herauslöschen einer Setartikel-Unterposition ist zulässig. Dadurch ändert sich die Bezugsbasis für die Berechnung der Verkaufspreise der einzelnen Positionen.
Werden alle Setartikel-Unterpositionen herausgelöscht, so wird der eigentliche Setartikel (die Kopf-Position) wie ein gewöhnlicher Artikel behandelt.

### Wie werden die Setartikel aus Sicht der Mehrwertsteuer betrachtet?
Für die Umsatzsteuerliche Betrachtung des Setartikels ist die so genannte Hauptleistung ausschlaggebend. Daher wird für die Berechnung des Umsatzsteuerbetrages der dem (Kopf-) Setartikel zugeordnete Steuersatz herangezogen. Die Mehrwertsteuersätze der Unterartikel werden nicht berücksichtigt.
Diese Vorgehensweise wird auch für die Erzeugung der Fibu-Exportdaten verwendet.

### Kann auch definiert werden, dass die einzelnen Positionen den Setpreis ergeben?
Bei Händlern kommt es oft dazu, dass ganze Produktgruppen zusammengestellt werden und dass es dafür einen Pauschalpreis gibt.<br>
Eigentlich ist es so, dass der Pauschalpreis eine reine Preisauszeichnung ist und sich automatisch von unten nach oben rechnet (Summe aus den eingegebenen Positionen). Für diesen Anwendungsfall verwenden Sie bitte die [Intelligente Zwischensumme]( {{<relref "/verkauf/gemeinsamkeiten/#wie-ist-die-positionsart-intelligente-zwischensumme-zu-verstehen" >}} )<br>
Um bei den Positionen keine Preise anzudrucken, setzen Sie den Haken bei [] Positionspreise anzeigen.

### Können die Positionen von Setartikeln in mehreren unterschiedlichen Lieferungen geliefert werden?
Da Sets immer zusammenhängend von den Kunden benötigt werden, müssen diese auch immer in ganzen Sätzen geliefert werden. D.h. wenn sie eine Set-Anzahl > 1 liefern, können natürlich Teilmengen geliefert werden, es muss sich aber immer eine ganzzahlige Satzgröße ergeben.

### Wie kann der Verkaufspreis eines Sets geprüft werden?
Um nun den Verkaufspreis eines Sets aus den einzelnen Artikeln zu überprüfen, kann am einfachsten die Gesamtkalkulation der Stücklistenverwaltung verwendet werden. D.h.:
- Gehe in die Stücklistenverwaltung auf den Setartikel
- Hinterlege in den Kopfdaten den passenden Kunden
- Nun wähle den Menüpunkt Info, Gesamtkalkulation. Durch das Hinterlegen des Kunden kommt rechts der Kundenverkaufspreis für jede Position und damit auch unten die Gesamtsumme.![](VK_Preis_fuer_Setpositionen.png)

Womit man in der Gesamtsumme den Verkaufswert der Einzelpositionen sieht.
Zusätzlich wird wie üblich weiter unten die Verkaufspreisbasis des Setartikels selbst angezeigt. So sieht man auch die Differenz zwischen Einzelverkaufspreis und dem Verkaufspreis des Sets.
Das ist ja meist auch der Sinn eines Sets. Wenn der Kunde das gesamte Set kauft, bekommt er für den Set einen besseren Preis als wenn er sich die Teile einzeln zusammenstellt.
