---
categories: ["Stückliste"]
tags: ["Arbeitsplan", "Tätigkeiten", "Maschinen"]
title: "Arbeitsplan"
weight: 100
date: 2023-01-31
description: >
 Definition eines Arbeitsplanes zur Beschreibung des Herstellungsprozesses einer Baugruppe.
---
Arbeitsplan
===========

Neben dem Materialbedarf steht Ihnen in **Kieselstein ERP** auch die Definition des Arbeitsplans ![](Arbeitsplan1.gif) für jede Stückliste zur Verfügung. Im Arbeitsplan werden die Arbeitsgänge definiert, in deren Reihenfolge die einzelnen Fertigungsschritte abgearbeitet werden sollten.

Es können folgende Definitionen vorgenommen werden.

![](Arbeitsplan2.gif)

| Begriff|Beschreibung|
| --- |  --- |
| Artikel | Definieren Sie die auszuführende Tätigkeit. In der Auswahlliste werden nur diejenigen Artikel angezeigt, die als Artikelart Arbeitszeit definiert sind. Beachten Sie bitte, dass für die Kalkulationen, insbesondere Vorkalkulationen die Kosten des Arbeitsganges nicht personenbezogen sind sondern aus der Tätigkeit kommen und hier wiederum aus dem Nettopreis des ersten Lieferanten dieses Artikels. |
| Arbeitsgang | Diese laufende Nummer wird pro Arbeitsgang vergeben. Die automatische Erhöhung der Arbeitsgangnummer kann im System unter Parameter (STUECKLISTE_ERHOEHUNG_ARBEITSGANG) eingestellt werden. Bitte beachten Sie, dass in **Kieselstein ERP** die Zeiterfassung auf die Tätigkeiten erfolgt. Sollten gleiche Tätigkeiten (z.B. Drehen) für verschiedene Arbeitsgänge erforderlich sein und in der Nachkalkulation sollte der Zeitaufwand zwischen den Arbeitsgängen unterschieden werden, so müssen unterschiedlichen Artikel (Tätigkeiten) dafür angelegt werden.Hier sprechen wir auch immer wieder vom Hauptarbeitsgang. |
| Unter-arbeitsgang | Neben dem Arbeitsgang kann noch die Unterarbeitsgangnummer eingegeben werden. Damit wird, insbesondere für die Maschinenzeiterfassung nach Umspann-Sollzeiten eine Gruppierung der entsprechenden Arbeitsgänge erreicht. |
| Rüstzeit | Angabe des Stückzahl unabhängigen Zeitaufwandes z.B. für Fertigungsinitialisierungen (Erstellung der Papiere, Anfertigung von Zeichnungen oder Vorrichtungen, Materialvorbereitung usw.) |
| Stückzeit | Angabe des Zeitaufwandes zur Erstellung von 1Stück Produkt. Die Angabe erfolgt in der Form von Stunden, Minuten, Sekunden und gegebenenfalls tausendstel Sekunden (Millisekunden). Neben den Zeiten wird die auf die Losgröße umgerechnete Dauer dargestellt. Diese Losgröße wird durch den Druck der Gesamtkalkulation festgelegt und von dort übernommen. |
| AG-Art | Insbesondere für die Stückzeiten ist sehr oft auch die Unterscheidung zwischen(Maschinen-)Laufzeit und (Mitarbeiter-)Umspannzeit erforderlich.In Kombination mit der Losnachkalkulation nach Soll-Umspannzeit ist diese Einstellung für die richtige Berechnung der anerkannten Zeiten wichtig. Siehe. |
| AG-Beginn | Versatz des Arbeitsgang Beginns in Tagen zum geplanten Beginn der Fertigung des Loses. Damit kann der Beginn jedes einzelnen Arbeitsschrittes entsprechend versetzt eingeplant werden.In der Stückliste wird immer nur der eigentliche Versatz angegeben. D.h. Sie geben nur bei den Arbeitsgängen, welche gegenüber dem Vorgänger Arbeitsgang später begonnen werden (können) den versetzten Arbeitsgangbeginn an.Bei der Überleitung in den Fertigungsauftrag wird der jeweilige Versatz kummuliert übertragen. |
| Aufspannung | Bei größeren Aufträgen werden immer wieder mehrere Teile auf einmal gefertigt. Hier wird bei der Stückzeit die echte (Umspann-) Zeit für den Wechsel aller Teile angegeben. Für den Vergleich der Soll-/Istzeiten wird pro Stück nur die Stückzeit / Aufspannung herangezogen.In der Kunststofftechnik ist dieser Begriff gleichbedeutend mit der Schusszahl, also derjenigen Anzahl an Werkstücken, welche bei einem Spritzdurchgang = Schuss hergestellt werden. |
| Maschine | Definieren Sie gegebenenfalls zusätzlich die Maschine, welche benötigt wird um den Arbeitsgang durchzuführen. |
| Nur Maschinenzeit | Die angegebene Tätigkeit ist eine reine Maschinen(Lauf-)zeit |
| Material | Für diese Tätigkeit wird auch Material, der Stücklistenpositionen verwendet, z.B. für den Fremdarbeitsgang. |
| Kommentar | Eine kurze Beschreibung des Arbeitsganges |
| Kommentar | Eine lange Beschreibung des Arbeitsganges. |

**Wichtig:**

Um die Maschinenzuordnung in den einzelnen (Haupt-)Arbeitsgängen richtig zu steuern und Fehlbedienungen zu vermeiden, wird beim Neuanlegen bzw. Änderungen in den Unterarbeitsgängen die Maschine innerhalb des jeweiligen Hauptarbeitsganges gleich gesetzt. Da in aller Regel im Hauptarbeitsgang 0 die planerischen Tätigkeiten eingeplant sind (z.B. Konstruktion, Werkzeugausgabe, Materialvorbereitung) wird dies beim Hauptarbeitsgang 0 nicht erzwungen.<br>
**Hinweis:**<br>
Es wird dies so gehandhabt, dass die Maschinen der Unterarbeitsgänge 0, 1, 2 immer synchron gehalten werden. Die Maschinen der weiteren Arbeitsgänge, welche z.B. für die Messautomaten verwendet werden, sind davon nicht betroffen.

Ansonsten wird dieses Verhalten für die Unterarbeitsgänge des Hauptarbeitsganges erzwungen, die als Arbeitsgangart Laufzeit oder Umspannzeit definiert haben. Es wird hier vom eigentlichen Rüstarbeitsgang = Arbeitsgangart leer ausgegangen und für alle Unterarbeitsgänge des Hauptarbeitsgangs gesetzt, bei denen eine Arbeitsgangart eingetragen ist.

#### Hinweis zu reine Mannzeit
Dies ist zusätzlich noch von der Einstellung des Feldes reine Mannzeit im Artikelstamm des Arbeitszeitartikels abhängig. D.h. ist "reine Mannzeit" angehakt, so wird beim Arbeitsgang die hinterlegte Maschine nicht verändert.

![](Reine_Mannzeit_NZI.gif)

Ist im Artikel "nur zur Info" angehakt, so kann bei Arbeitszeitartikeln diese Einstellung im Los-Fertigungsbegleitschein ausgewertet werden.

#### Kann ein Arbeitsplan von einer anderen Stückliste kopiert werden ?
Ja, verwenden Sie dazu im zu ergänzenden Arbeitsplan das Icon ![](Arbeitsplan_einlesen.gif), Arbeitsplan von anderer Stückliste übernehmen.

#### Kann ein standard Arbeitsplan hinterlegt werden?
Da es oft sehr praktisch ist, bei einer neuen Stückliste automatisch einen allgemeinen Arbeitsplan zu hinterlegen, haben wir, bei der Neuanlage einer Stückliste das automatische Kopieren eines Arbeitsplanes von einer bestehenden Stückliste geschaffen.

D.h. wenn im Parameter ARBEITSPLAN_DEFAULT_STUECKLISTE eine gültige Stückliste hinterlegt ist, so wird bei der Neuanlage einer Stückliste automatisch der Arbeitsplan dieser Stückliste importiert.

Damit können Sie die allgemeinen Tätigkeiten vordefinieren und nur die erforderlichen Änderungen für die individuelle Stückliste vornehmen.

#### Kann der Arbeitsplan auch importiert werden?
Ja. Mit dem Klick auf ![](ArbeitsplanImport.gif) wird der Import der CSV Datei gestartet.

Der Datenaufbau dafür ist folgender:

Ident;Stückzeit;Rüstzeit
| Feld | Beschreibung |
| --- | --- |
| Ident | Artikelnummer der Arbeitszeit |
| Stückzeit | Stückzeit dieses Arbeitsganges in ms (Millisekunden) |
| Rüstzeit | Rüstzeit dieses Arbeitsganges in ms |

Beispiel:

AZ-001;10000;20000

Hinweis: Die Arbeitsgangnummer wird automatisch vergeben.

Hinweis: Siehe dazu auch Stückliste, Import, Arbeitsplan

### Arbeitsgang Arten (AG-Art)
<a name="Arbeitsgangarten"></a>

Die Arbeitsgang Art liefert eine wesentliche Unterscheidung des Verhaltens der Ist-Zeit Berechnung für den jeweiligen Arbeitsgang.

Ist die Arbeitsgangart nicht definiert, so wird die gebuchte Istzeit (Beginn bis Ende) verwendet. 

Ist die Arbeitsgangart definiert, mit Laufzeit oder Umspannzeit, so greift zusätzlich der Parameter THEORETISCHE_IST_ZEIT_RECHNUNG.

Ist dieser nicht definiert, so wird auch bei diesen Arbeitsgangarten die gebuchte Istzeit verwendet.

Ist dieser definiert (= 1), so wird anstelle der Istzeit die theoretische Istzeit verwendet.

D.h. es wird angenommen, dass die Solldaten des Arbeitsgangs stimmen und daher werden diese Zeiten mit der Summe der Gut- und Schlechtstück multipliziert und als theoretische Istzeit verwendet.

Wichtig, bei definierter THEORETISCHE_IST_ZEIT_RECHNUNG: Wird ein Arbeitsgang ohne Arbeitsgangart gestartet, wir sprechen hier auch von Rüstzeit und wird in diesem Zeitraum zugleich die Arbeitsgangarten Umspannzeit oder Laufzeit gebucht, so wird die tatsächliche Dauer der Rüstzeit um die theoretische Istzeit reduziert um so die echte Nettozeit des Rüstens bestmöglich zu ermitteln.

#### Kann der Arbeitsplan auch ausgedruckt werden?
Ja. Wählen Sie in der Menüzeile, ausdrucken, Arbeitsplan. Geben Sie die gewünschte Stückzahl ein und klicken Sie auf aktualisieren.

![](Arbeitsplan3.gif)

#### Wie definiert man einen Arbeitsgangversatz?
In der Stückliste können Sie den zeitlichen Ablauf der Fertigung über das Feld AG-Beginn steuern. 
Geben Sie hier beim Arbeitsgang in Tagen an welcher zeitliche Versatz zum vorhergehenden AG einzuplanen ist. 

Beispielsweise benötigt das Trocknen nach dem Lackieren zusätzlich zur Arbeitszeit der Mitarbeiter weitere Tage in denen die darauffolgenden Arbeitsgänge noch nicht beginnen können. 

Die Eingaben des Arbeitsgangversatzes werden in das Los übernommen und in der Auslastungsvorschau berücksichtigt.