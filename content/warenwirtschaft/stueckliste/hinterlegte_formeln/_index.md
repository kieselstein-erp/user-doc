---
categories: ["Stücklisten"]
tags: ["Stückliste", "Formeln", "Konfigurator"]
title: "Formelstücklisten"
linkTitle: "Formelstücklisten"
weight: 500
date: 2023-01-31
description: >
  Stücklisten anhand Formeln automatisch generieren
---
Stückliste mit Formeln
======================

in Ihrem **Kieselstein ERP** stehen auch Formeln für die Errechnung von Mengen je Stücklistenposition zur Verfügung.
Wenden Sie sich für die Freischaltung dieser Funktion bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

Die Idee hinter dieser Funktionalität ist, dass abhängig von verschiedenen Eingabeparametern, welche je Stückliste unterschiedlich sein können, entsprechende Berechnungen für die Ermittlung der Positionsdaten wie Menge, Abmessungen (B x H x T) und Artikel durchgeführt werden können.
In weiterer Folge kann anhand der errechneten Daten eine echte Stückliste erzeugt werden, mit der die Produktion dann die entsprechenden Vorgaben bekommt.

Das bedeutet, dass die grundsätzliche Idee ist, dass anhand der Parameter "nur" die Menge der jeweiligen Stücklistenposition bzw. Arbeitsplanposition verändert wird.<br>
Beispiel: Hat man nun verschiedene Werte oder auch Maschinen, die abhängig von Parametern zum Einsatz kommen, so definiert man alle Maschinen mit den vorerst gedachten Rüst und Stückzeiten. In den Formeln ändert man dann die jeweilige Rüst bzw. Stückzeit so ab, dass nur die gewünschte Maschine eine echte Sollzeit hat.<br>
Analog wird bei den Positionen vorgegangen.

{{% alert title="ACHTUNG" color="warning" %}}
Die Verwendung der Formelstücklisten in der Überleitung in das Angebot, stehen derzeit nur **OHNE** KUNDEN_POSITIONSKONTIERUNG zur Verfügung.
{{% /alert %}}

## Vorgehensweise
Identifizieren Sie die Stücklisten bei denen Sie die Formeln hinterlegen möchten. In aller Regel werden dies neue Stücklisten sein.
Haken Sie bei diesen Stücklisten ![](SMF1.gif) mit Formeln an.
Damit wird der Reiter Parameter freigeschaltet.
Definieren Sie im Reiter Parameter nun die Parameter die für diese Stückliste gelten sollten. Dies könnte z.B. wie folgt aussehen:
![](SMF2.jpg)

Das bedeutet, dass die Kennung, mit der Sie dann weiter rechnen, LH ist und die Bezeichnung entsprechend sprechend für Ihre Anwender.
Ist Pflichtfeld angehakt, so muss hier etwas eingegeben werden.
Sind Min bzw. Max-Wert definiert, so darf der erfasste Wert nur zwischen diesen Grenzen liegen.

Zusätzlich steht noch bei der Texteingabe auch die Möglichkeit der Combobox zur Verfügung.
![](SMF8.jpg)
D.h. wird bei String noch Combobox angehakt, so ist der Bereich zu definieren. Die auswählbaren Werte sind jeweils durch einen senkrechten Strich | (vertical Bar, Pipe) getrennt.
Ist Pflichtfeld nicht agehakt, so kann auch leer ausgewählt werden, sonst wird der erste Wert aus Bereich vorgeschlagen.

### Kopieren von Parametern
Gerade wenn neue Stücklisten Bäume aufgebaut werden, so müssen die Parameter durchgängig definiert sein.
Das kann natürlich manuell gemacht werden, es können aber zusätzlich von übergeordneten Stücklisten aus der Baumstruktur heraus die Parameter übernommen werden. Klicken Sie dazu bitte in der Parameterauswahlliste auf ![](SMF9.gif) fehlende Parameter nachtragen. Sie erhalten nun eine Auswahlliste in der die über dieser (Formel-)Stückliste liegenden Formelstücklisten enthalten Stücklisten aufgelistet sind. In anderen Worten man baut von oben nach unten den Stücklistenbaum auf und übernimmt dann ebenfalls von oben nach unten die Parameter.
Da dies nicht immer so sein wird gibt es in der Auswahlliste der Formelstücklisten auch die Möglichkeit
![](SMF10.jpg)
alle Formelstücklisten anzuhaken.

### Was bedeuten die verschiedenen Fehlermeldungen?
Da bei der Eingabe der Formeln direkt oder nur indirekt erkennbare Fehler auftreten können, haben wir entsprechende Fehlermeldungen eingebaut. Diese können sowohl bei der Erfassung der Formel, als auch bei der Interpretation der Formel auftreten.
So könnte z.B. folgender Fehler bei der Interpretation auftreten:
![](SMF3.jpg)
Dies bedeutet, dass in einer Stückliste auf einer Position eine fehlerhafte Formel gespeichert wurde. Da bei der Speicherung bereits ein Fehler gemeldet wurde, wurde das erforderliche Class-File nicht erzeugt. Um die richtige Stücklistenposition zu finden, klicken Sie einfach auf den GoTo Button bei Stücklistenposition und Sie stehen direkt auf der fehlerhaften Formel. Bessern Sie bitte die falsche Formel entsprechend aus.

![](SMF4.jpg)
Diese Meldung, welche z.B. beim Speichern der Formel auftritt besagt, dass eine entsprechende Methode nicht gefunden wurde.
Nachfolgend finden Sie eine Liste oft verwendeter Funktionen.

![](SMF6.jpg)
Tritt diese Meldung beim Ausführen der Gesamtkalkulation auf, so bedeutet dies, dass eine Formel hinterlegt wurde, deren Rückgabewert nicht den geforderten Werten entspricht. Bitte beachten Sie, dass in den Positionen für die Menge ein BigDecimal und im Arbeitsplan kein Rückgabewert (nur setzen von Stück- und Rüstzeit) vorgegeben sind.
Das würde in obigem Beispiel bedeuten, dass im Arbeitsplan ein Wert retourniert wird, weil z.B. eine Formel aus der Position kopiert wurde.

![](SMF7.jpg)
Hier liegt ein Fehler in der Formel vor, der erst bei der Ausführung festgestellt wird.

### Woran erkennt man, ob hinter einer Stücklistenposition eine Formel hinterlegt ist?
Stücklistenpositionen mit Formeln werden in **Kieselstein ERP** blau dargestellt.
![](SMF5.gif)

### Es können über die Zwischenablage keine neuen Positionen eingefügt werden?
Bitte prüfen Sie ob alle Parameter die in der/den Formeln enthalten sind, auch in der neuen Stückliste, in die eingefügt werden sollte, vorhanden sind.

### Wie werden Variablen aufgelöst?
Wichtig bei der Verwendung von Variablen ist deren Einsatz
- in ihrer Reihenfolge
- in ihrer Ebene / Unterebene

D.h. Werte aus Variablen können erst verwendet werden, wenn sie initialisiert und errechnet sind.
Zusätzlich ist zu beachten dass die Berechnung der Stücklisten immer je Stückliste erfolgt und danach erst die Unterstücklisten berechnet werden.<br>
Dies hat den Effekt, wenn in einer Stückliste(n Ebene) die Variable einer Unterstückliste benötigt werden, so stehen diese, da die übergeordnete Stückliste zuerst durchlaufen wird, nicht zur Verfügung. Erst durch den "zweiten" Lauf, in dem die Unterstücklisten aufgelöst werden, werden die Variablen initialisiert und stehen somit immer erst eine Ebene darunter zur Verfügung.

### Wie wirkt der Parameter Kunde?
Wenn in einer Formelstückliste Kunden mit dem Parameter ![](Formel_Kundenauswahl.gif) KundeId abgefragt wird, so bewirkt dies, dass der vom Anwender gewählte Kunde, der dann in aller Regel ein Pflichtfeld sein sollte, bei der Überleitung in Produktstücklisten, in alle (Unter-)Stücklisten die Aufgrund der Formeln neu erzeugt werden, eingetragen wird.

### Wie können Produktstücklisten erzeugt werden?
Neben der Variante, dass aus dem Angebot heraus direkt Stücklisten in Verkaufssicht vorkalkuliert und dann durch die Erzeugung eines Angebotes auch Produktstücklisten angelegt werden können, können auch aus dem Modul Stücklisten heraus, Menüpunkt, Bearbeiten, Konfigurieren eine entsprechende Produktstückliste mit den gegebenen Werten / Parametern angelegt werden.<br>
Es wird dazu, die Formelstückliste mit allen formelbehafteten Unterstücklisten kopiert und anstelle der Formeln die errechneten Werte eingetragen. Die Artikelnummer der Stücklisten werden aus den Formelstücklisten übernommen und um eine laufende 2+4 stellige Nummer ergänzt. Die ersten zwei Stellen sind das Geschäftsjahr der Anlage gefolgt von einer vierstelligen laufenden Nummer. Diese wird auf die angelegte maximale Länge der Artikelnummer rechtsbündig durch Ergänzung um _ (Underline / Unterstrich) aufgefüllt.<br>
Um eine entsprechende Durchgängigkeit zu erreichen, werden diese Produktstücklistennummern zentrale verwaltet und über alle Arten von Formelstücklisten hochgezählt. Damit wird erreich, dass eventuell angelegte nur Unterstücklisten, z.B. für die Fertigung von Ersatzteilen, nicht mit anderen Formelstücklisten kollidieren.

Bei der Erzeugung der Produktstücklisten wird weiters, bei eingeschaltetem Parameter DIMENSIONEN_BESTELLEN diejenigen Artikel angelegt, bei denen die Dimensionen für die stückgenaue Beschaffung der für Sie gefertigten Artikel benötigt werden. [Details siehe](../Bestellung/index.htm#Dimensionen / Zuschnitt bestellen).

## Nachfolgend eine Sammlung von Formeln

### Division
```bash
BigDecimal lh = $P{LH};
  return (lh.divide(new BigDecimal(100)));
```
Der Parameter LH wird durch 100 dividiert und als Menge zurückgegeben.

### Ein Tausenstel von LL mit 4 multipliziert und zum Ergebnis 5 dazu
```bash
BigDecimal ll = $P{LL};
   return (ll.multiply(new BigDecimal(0.001)).multiply(new BigDecimal(4.0))).add(new BigDecimal(5));
```

### Auf eine gewisse Anzahl erhöhen / runden
```bash
BigDecimal ll = $P{LL};
int anzahl;
anzahl = ll.intValue() / 1000;
anzahl += 4;
return new BigDecimal(anzahl);
```
In Worten: LL auf ganzzahlige Anzahl umrechnen (pro einem Meter ein Artikel) und dann noch vier dazuzählen und dies als Menge retour.

### Bedingung
```bash
BigDecimal ll = $P{LL};
if(ll.compareTo(new BigDecimal(1000))>0) {
   return (new BigDecimal(1));
}
return(new BigDecimal(0.5));
```
In Worten: Wenn LL größer 1000 dann gib 1 zurück, sonst 0,5
Bitte beachten Sie, dass die Menge immer als BigDecimal zurückgegeben werden muss.

### Bedingung anhand von Texten
```bash
String af = $P{AF};
BigDecimal m;
if (af.toUpperCase().contains("ALU")) {
    m = new BigDecimal(1.00);
} else {
    m = new BigDecimal(0.00); }
return m;
```

### Bei einem eindimensionalen Artikel die Breite (=Dimension1) setzen und die Menge gleich belassen
```bash
BigDecimal ll = $P{LL};
BigDecimal mng = getMenge();
Double d1 = new Double ((ll.doubleValue() - 175) / 1000);
setDimension1( d1 );
return (mng);
```

## Weiterführende Beispiele
Siehe ?:\kieselstein\dist\clients\import-files\Stuecklisten_Formeln.java 
