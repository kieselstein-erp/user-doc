---
categories: ["Stücklisten"]
tags: ["Stückliste", "Arbeitsplan", "Prüfanweisung", "Formelgenerator","Automatische Stücklisten", "Stücklisten Arten", "Hierarchien"]
title: "Stücklistenverwaltung"
linkTitle: "Stücklisten"
weight: 10
date: 2023-01-31
description: >
  Wie mit Stücklisten arbeiten
---
Eine Stückliste ist immer eine Verlängerung / Ergänzung eines Artikels.<br>
Ein Stücklisten Baum hat technisch gesprochen kein oberes Ende und kein unteres Ende. D.h. er kann jederzeit oben und unten Erweitert werden. Im **Kieselstein ERP** können Stücklisten beliebiger Tiefe / Ebenen verwaltet werden. So haben die uns bekannten Installationen, z.B. im Anlagenbau, ca. 250 beteiligte Stücklisten mit ca. 15 Unter-Ebenen.<br>
Wir wünschen dir, dass deine Strukturen nicht ganz so komplex sind (zumindest am Beginn).

## Stückliste

Statusübergang: Keiner, erst ab Freigabe, Prototypen, Versionen
 
Die hierarchischen Stücklisten in **Kieselstein ERP** dienen der Abbildung eines zusammenfügenden Fertigungsprozesses.

Grundsätzlich sind Stücklisten immer Artikel, also in diesem Sinne eine Erweiterung des Artikels.

**Rezepturen**: Da die Stücklisten sich immer auf Artikel beziehen und die Artikel Mengeneinheiten haben, können mit der **Kieselstein ERP** Stücklistenverwaltung auch Rezepturen (Rezeptverwaltung) abgebildet werden. Hier sind dann die gefertigten Produkte ein "Mengeneinheit" (z.B. kg, l) des Artikels. Siehe dazu auch {Erfassungsfaktor}.

BOM, Bill of material auch das eine Bezeichnung für die Stückliste, welche aus der Elektronik kommt und "nur" den Materialanteil einer Stückliste betrifft.

- Erfassung von Kundenspezifischen Stücklisten im Web-Portal
- [Stücklisten mit Formeln]( {{<relref "/warenwirtschaft/stueckliste/hinterlegte_formeln/"  >}} )
- [Erfassung und Definition von Prüfungen]( {{<relref "/warenwirtschaft/stueckliste/pruefungen/"  >}} )

Erklärung der [Zusammenhänge Stückliste und Fertigung]( {{<relref "/fertigung/losverwaltung/stueckliste_fertigung/">}} )
<a href="Zusammenhänge Stückliste und Fertigung"> </a>

 ![](Stueckliste.jpg)

### Begriffsdefinitionen:
Da sehr viele verschiedene Begriffe für Stücklisten in Gebrauch sind, hier unsere Begriffsdefinition bzw. die Übersetzung dazu:

#### Kopfstückliste / Endprodukt
<a name="Kopfstückliste"></a>

Oft auch  Kopfstückliste genannt. Das oberste Ende einer hierarchischen Stücklistenstruktur. Sie können sich das auch als ein Produkt welches versandfertig verpackt ist vorstellen.

#### Baugruppe
Ein aus einem oder mehreren Artikeln bestehender (neuer) Artikel, welcher durch den Fertigungsprozess erzeugt wurde. Es kann auch nur die Bearbeitung eines Artikels sein. Z.B. ein gedrehtes Teil im Roh-Zustand und im lackierten Zustand.<br>
Eine Baugruppe kann wiederum eine Baugruppe beinhalten. Aus der Sicht dieser Baugruppe ist dies jedoch ein gewöhnlicher Artikel.

#### Mengenstückliste
Mengenmäßige Sicht auf die Ausgangsstückliste (von der aus die Betrachtung nach unten erfolgt) inkl. aller Komponenten (Artikel) der Unterstücklisten. Dargestellt in einer flachen Ebene und verdichtet auf reine Mengeninformation je unterschiedlichem Artikel.

#### Strukturstückliste
Sicht auf die Stückliste mit allen Unterstücklisten (Struktur) Informationen. Dient zur Darstellung der Stücklistenzusammenhänge / Abhängigkeiten.

#### Baukastenstückliste
Entspricht dem Begriff Baugruppe. [Siehe](#baugruppe)

#### Ebene
Aus der Sicht des Endproduktes befindet sich eine Unterstückliste eine oder mehrere Ebenen tiefer unter diesem Endprodukt. In einer Ebene können mehrere Baugruppen sein. Aber eine Baugruppe kann nur in einer Ebene sein.

**Wichtig:** 
Die eingepflegte Stücklistenstruktur stellt immer eine Abbildung des Fertigungsprozesses dar. In jeder Baugruppe ist ein in sich geschlossener Fertigungs-Prozess abgebildet.

Dieser Fertigungsprozess wird durch die Stückliste (= Mengen und Materialbedarf für den Fertigungsprozess) und dem dazugehörenden [Arbeitsplan]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan/" >}} ) (= durchzuführende Arbeitsschritte (Arbeitsgänge) um aus den Materialien das Produkt = die Baugruppe zu fertigen) gebildet. Aus diesem Grunde hat eine Stückliste immer mindestens zwei Definitionsschichten. Die Materialschicht und die Schicht des Arbeitsplanes. Hier können bei komplexen Fertigungsprozessen noch weitere Schichten hinzukommen (Z.B. Programmsteuerungen, Prüfanweisungen, ..).

**Wichtig:**
Die Standard-Bearbeitung der **Kieselstein ERP** Stückliste bezieht sich immer auf eine Ebene der Stückliste.

Die begriffliche Trennung von Endprodukt, Baugruppe, Einkaufsteil dient in **Kieselstein ERP** lediglich der leichteren Beschreibung des Verhaltens, der Zusammenhänge. Grundsätzlich sind alle drei Arten Artikel und haben, wenn sie Stücklisten für diesen Mandanten sind, noch die Stücklistenpositionen und Arbeitszeiten / Arbeitspläne als zusätzliche Eigenschaften (Definitionsschichten).

Insofern kann für jede Stückliste, egal ob Endprodukt oder Baugruppe, ein Verkaufspreis, ein Lagerstand, eine Beschreibung usw. hinterlegt werden. Wir unterscheiden gerade bei der Beschreibung auch klar zwischen der Beschreibung des Artikels und der Beschreibung der Stückliste. Die Artikelbeschreibung ist "verkaufs"-orientiert, die Stücklistenbeschreibung ist fertigungsorientiert.

## Grundsätzlicches
Grundsätzlich ist eine Stückliste die technische Beschreibung eines Produktes. Egal ob dieses Produkt im eigenen Hause gefertigt wird oder außer Haus. Um Stücklisten effizient verwalten und verwenden zu können, benötigt man verschiedenen Sichten auf diese Stückliste(n). Diese werden in **Kieselstein ERP** durch unterschiedliche Ausdrucke oder auch Darstellungsarten am Bildschirm realisiert.

Prinzipiell kann bei allen Stücklistendrucken angegeben werden, ob die Unterstücklisten eingebunden werden sollen oder nicht. Damit wird sofort erreicht, dass für Gesamtbetrachtungen eines Produktes alle Komponenten (= die enthaltenen Artikel) angeführt werden. Werden Unterstücklisten eingebunden, so kann zwischen einer Strukturdarstellung oder einer Mengendarstellung unterschieden werden. In der Strukturdarstellung werden die verschiedenen Ebenen durch Einrückungen, ähnlich einer Baum-Struktur, dargestellt. Die Artikel der Unterstücklisten (z.B. der Artikel Baugruppe1 im obigen Bild) bleiben inkl. Positionsinformationen erhalten. In den Mengendarstellungen werden die Inhalte der Unterstücklisten in die Betrachtungsebene aufgenommen. Die Artikel der Unterstücklisten werden für die richtige Mengenbetrachtung aus der Liste entfernt. D.h. bei einer Mengenbetrachtung inkl. Unterstückliste werden die Artikel Baugruppe (lt. obigem Bild) nicht angeführt. Ebenso werden gleiche Artikel auf einen Eintrag mit entsprechender Menge verdichtet.
Durch diese Logik lassen sich alle oben angeführten Sichten auf die Stücklisten darstellen.
Wichtig: Beachten Sie, dass als Fremdfertigung gekennzeichnete Stücklisten nicht weiter aufgelöst werden. D.h. ab dieser Ebene wird die Stückliste komplett zugekauft. Die darin enthaltenen Teile werden vom Fremdfertiger selbst beschafft.

### Montageart wozu?
Gerade in Maschinenbau Unternehmen wird immer wieder die Frage gestellt, wozu die Montageart denn gut sei.

Ursprünglich kommt die Montageart aus der Elektronik Fertigung. Hier hat man die Aufgabenstellung, dass verschiedenes Material zu unterschiedlichen Fertigungsschritten in der Produktion benötigt wird. Daraus ergibt sich, dass die Ausgabestücklisten nach diesen Montagearten sortiert ausgedruckt werden müssen. Denken Sie hier z.B. an Material Vorbereitung, Bestücken, Löten, komplettieren. Im Maschinenbau sind diese Montagearten auf den ersten Blick nicht erforderlich. Bei näherer Betrachtung ergibt sich auch hier, dass es eine Bearbeitung mit einem nachfolgenden Zusammenbau gibt. Nutzen Sie die Definition der Montageart um Ihre Stücklisten zu optimieren.

#### Montageart Mandantenabhängig, warum?
Mandant 001 ist ein Maschinenbau Unternehmen, Mandant 002 ein Elektronik Unternehmen. Wären die Montagearten nicht Mandantenabhängig, so würde 001 immer die Montagearten von 002 sehen, obwohl er sie wirklich nie braucht. Darum ist auch die Montageart Mandantenabhängig.

#### Wo kann die Standard Montageart eingestellt werden?
Stellen Sie die Standard Montageart, also Ihre üblicher weise / am häufigsten verwendete Montageart in den Grunddaten der Stücklisten ein. Reihen Sie die Montagearten mit den Pfeilen so, dass diese nach Wichtigkeit gereiht sind. Die erste / oberste Montageart wird als Standard Montageart verwendet. Nach der Reihung muss das Stücklistenmodul geschlossen und neu geöffnet werden, damit die neuen Montagearten und deren Reihung geladen werden.

**Stücklisten sind grundsätzlich mandantenabhängig.**

D.h. es kann auch die Situation geben, dass ein Artikel beim einen Mandanten eine Stückliste ist und der andere Mandant dies als Einkaufsteil vom ersten Mandanten bezieht.

Um dies rasch zu erkennen wird in den Artikel Kopfdaten rechts oben die Information angezeigt ob dies eine Stückliste ist oder nicht.

#### Unechte Stücklisten
Werden von **Kieselstein ERP** in der Form unterstützt, dass in den Lose sogenannte (freie) Materiallisten angelegt werden können.<br>
Wenn Stücklistenartikel auch in der Produktion bewirtschaftet werden sollen, müssen zumindest die Kopfdaten der Stückliste, also die Verbindung zum Artikel definiert werden.

### Einlesen von Stücklisten
Beim Einlesen von Stücklisten werden grundsätzlich die Positionen der einzulesenden Stückliste in die Positionen der gerade in Bearbeitung befindlichen Stücklisten eingelesen (kopiert). Wird beim Einlesen noch Unterstücklisten mit Einlesen ausgewählt, so werden auch die Positionen der in der einzulesenden Unterstückliste mit einkopiert. Es erfolgt KEINE Kopie der Stücklistenstruktur.<br>
Siehe dazu [Import von Stücklisten aus XLS Dateien]( {{<relref "/warenwirtschaft/stueckliste/importe/" >}} ), mit denen sehr wohl Stücklistenstrukturen importiert werden können.

### Angebotsstückliste in Stückliste einfügen
Nach Auftragserteilung kann in **Kieselstein ERP** die Angebotsstückliste direkt in eine Stückliste eingefügt werden. Öffnen Sie dazu eine neue oder bestehende Stückliste. Nun können Sie in den Stücklistenpositionen durch Klick auf das Icon "Agstkl-Positionen einer best. Angebotsstückliste importieren" diese importieren. Weiters finden Sie den Button *Preise neu berechnen* welcher die Preise für alle Positionen neu berechnet.<br>
Die erhebliche Zeitersparnis und Datenkonsistenz die dadurch erreicht wird, ermöglicht eine effiziente Art der Überleitung der angebotenen Produkte in die Fertigung. Die investierte Zeit und das Know-How in die Angebotsstückliste kann direkt in die Stückliste übertragen werden. 

Bitte beachten Sie dazu auch die Funktion Angebotsstückliste in Stückliste übertragen in den Kopfdaten der jeweiligen [Angebotsstückliste]( {{<relref "/verkauf/angebotsstueckliste/#wie-wird-eine-angebotsst%c3%bcckliste-in-eine-st%c3%bcckliste-%c3%bcbertragen" >}} ).

### Mengeneinheiten in der Stückliste, Dimensionen, in den Stücklistenpositionen
Siehe dazu auch [Mengeneinheit]( {{<relref "/docs/stammdaten/artikel/mengeneinheit/" >}} ).<br>
Hat ein Artikel über die Mengeneinheit eine Dimension definiert, so wird der Materialbedarf durch die Stückzahl (Menge) und der sich aus den Abmessungen ergebenden Menge, errechnet.
Beispiel: 3 Stk Blech mit 800x1217mm², wobei das Blech in m² definiert ist.<br>
Ergibt einen Materialbedarf von 2,92 m²<br>
Da insbesondere in der Maschinenbau-Branche die Artikel in den Stücklisten üblicherweise in mm definiert werden, aber die Artikel in m lagerbewirtschaftet sind, unterscheiden wir in **Kieselstein ERP** zwischen Ausgangsmengeneinheit und Zielmengeneinheit.<br>
Die Ausgangsmengeneinheit (Mengeneinheit der Position) ist die Mengeneinheit in der die Stückliste definiert ist. Sie kann je Stücklistenposition definiert werden. Voraussetzung: Es gibt für die Zielmengeneinheit entsprechende Umrechnungsfaktoren anhand der Mengeneinheiten Definition, welche im Modul System unterer Modulreiter System, unterer Reiter Sprache und Modulreiter Einheitkonvertierung definiert werden.<br>
Die Stückliste selbst wird immer für ein Mengeneinheit des Stücklisten-Kopf-Artikels geschrieben. Eventuell ist dies auch vom [Erfassungsfaktor](#wozu-dient-der-erfassungsfaktor) abhängig.

#### Können die benötigten Mengen automatisch berechnet werden?
dafür gibt es folgende Möglichkeiten.
##### Verwenden eines JRubby Scripts um mit Klick auf *Berechnen* die gewünschte Menge 
![](Berechnen.gif) anhand z.B. Verpackungseinheiten usw. zu berechnen. Für die Einrichtung des Scripts wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.
##### Formelstücklisten
D.h. anhand von Parametern werden entsprechende Produktionsstücklisten errechnet. [Siehe dazu bitte]( {{<relref "/warenwirtschaft/stueckliste/hinterlegte_formeln/" >}} ).
##### Verschnittfaktoren
Im Artikel können Verschnittfaktoren hinterlegt werden. Diese wirken auf der Basis der Mengeneinheit des Artikels bei der Berechnung des Materialbedarfes für die jeweilige Stücklistenposition.

Hat also der Artikel Blech aus dem obigen Beispiel einen Verschnittfaktor von 10% definiert, so wird als Bedarf
| Faktor | Wert |
| --: | --: |
| | 2,920m² |
| +10% |   0,292m² |
| ---- | ----- |
| | 3,212m² |
ermittelt.

##### Verschnittbasismenge
Zusätzlich kann eine Verschnittbasismenge angegeben werden. 

Die Verschnittbasismenge wird vor allem für Stangenmaterial benötigt.

Dazu ein Beispiel:<br>
Sie benötigen zur Fertigung ein Formrohr in der Länge von 1,60 m. Die Formrohre haben üblicherweise eine Länge von 6m .

Welche Menge = Länge müssen Sie bestellen, um daraus 100Stk á 1,60m fertigen zu können?

Berechnung ohne Verschnittbasismenge:

1,60 x 100 / 6 = 26,67 Stk, also würden Sie 27Stk bestellen, was zu wenig ist, denn:

Von einer Stange erhalten Sie 3,75 Stk, also 3Stk welche für diesen Fertigungsauftrag verwendbar sind.

D.h. Sie müssen 100 / 3 = 33,33 Stk bestellen, also exakt 34Stk um den Fertigungsauftrag, ohne Berücksichtigung eines zusätzlichen Verlustes erfüllen zu können. Dies bedeutet Sie müssen 34Stk*6m = 204m für diesen Fertigungsauftrag reservieren.

Die Berechnung des Mengenbedarfes für ein Los dieser Stückliste wird in folgender Reihenfolge durchgeführt.
1. Umrechnung der Mengen und Dimensionen auf eine Menge (3Stk á 200x150mm = 90.000mm²)
2. Umrechnung anhand der Ausgangsmengeneinheit der Stückliste auf die Zielmengeneinheit des Artikels

    (Ausgangsmenge = mm², Zielmengeneinheit = m², .... 90.000 / 1.000.000 = 0,090m²)

3. Berücksichtigung des Verschnittfaktors (10%, ... 0,090 + 10% = 0,099m²)

4. Berücksichtigung der Verschnittbasismenge (siehe oben)

Die sich daraus ergebende Menge wird in die Los(Fertigungs)verwaltung übernommen.

Siehe dazu auch Übertrag des Verschnittes in ein eigenes Lager, bei der Losausgabe.

#### Welche Mengen werden bei den verschiedenen Stücklistendrucken angedruckt?
<a name="Zielmengeneinheit"></a>
  - Werden beim Drucken keine Losgrößen berücksichtigt, also nur der Stücklisteninhalt ausgedruckt, so greifen die Ausgangsmengenarten aus der jeweiligen Stücklisten Position. Zusätzlich werden die Dimensionen, je nach Mengenart, angedruckt. Zusätzlich wird zu Ihrer Information auch die Menge in der Zielmengenart mit angedruckt.
  - Werden beim Drucken Losgrößen berücksichtigt, so geht es um Lagerstände / Lagerinformationen. Daher werden die Mengen in der Zielmengenart angegeben. Zusätzlich werden in den (Ziel-) Mengen die Dimensionsinformation berücksichtigt. D.h. Sie erhalten hier die Menge, welche sich auf das Lager bezieht.
  - In der Gesamtkalkulation und in der Ausgabestückliste werden zusätzlich zur Dimensionsumrechnung auch die [Verschnittfaktoren]( {{<relref "/docs/stammdaten/artikel/#verschnittbasis--verschnittfaktor-" >}} ) berücksichtigt.
  - In der Berechnung der Zielmenge wird auch der Fertigungs-Verpackungseinheit sowie die Verschnittfaktoren mit berücksichtigt. Es können sich daraus höhere Zielmengen ergeben. Die Mengenbasis der Berechnung ist eine ME (Satz) des zu fertigenden Produktes.

### Verschiedene Mengeneinheiten auf einem Artikel  
kg, m, mm auf einem Artikel und in den Stücklisten

Gerade in Unternehmen der Maschinenbaubranche ist diese Kombination der Mengeneinheiten sehr oft erforderlich.

Das bedeutet, der Artikel wird in kg bestellt, in m² am Lager geführt und in mm² durch die Konstruktion vorgegeben.

Wie kann das in **Kieselstein ERP** dargestellt werden?

Um in der Stückliste neben der Artikelmengeneinheit (auch Lagermengeneinheit genannt) auch mm bzw. mm² angeben zu können, ist es erforderlich, dass der gewünschte Artikel in einer durch die *Umrechnungsfaktoren der Mengeneinheiten* definierbaren Mengeneinheit definiert ist. Z.B. in m². Ist der Artikel in seiner Lagermengeneinheit entsprechend definiert, so kann in der Stücklistenposition die Positionsmengeneinheit entsprechend umgerechnet eingegeben werden. z.B. mm². Für den Einkauf / das Bestellwesen muss nun im Artikel noch die Umrechnung auf die Bestellmengeneinheit angegeben werden. Z.B. kg.
- Artikelmengeneinheit   m²  
- Stücklisten(Positions)mengeneinheit   mm²  
- Bestellmengeneinheit   kg  

### Leitauftrag
Für die Auswahl des Leitauftrages werden die offenen und teilerledigten Aufträge angezeigt. Diese Hinterlegung hat derzeit keine weitere Funktion.

### Jahreslos
Damit kann signalisiert werden, dass bei der Anlage neuer Lose anhand Kundenauftrag ein Los dieser Stückliste nur einmal pro Geschäftsjahr angelegt wird.<br>
Genauer wird bei neue Lose anhand Kundenauftrag diese Stückliste nicht berücksichtigt, wenn für das aktuelle Geschäftsjahr ein Los im Status angelegt, ausgegeben, in Produktion oder Teilerledigt für diese Stückliste schon vorhanden ist.<br>
Üblicherweise wird dies für laufende Produktionen verwendet und vor allem dann wenn eher eine Zuruf-Produktion gegeben ist, also die Mitarbeiter Teile z.B. in ein Kanbansystem einfach nachfüllen.

### Durchlaufzeit
Die Durchlaufzeit stellt den organisatorischen Zeitbedarf für die Fertigung eines Produktes dar. Sie bestimmt die Fertigungsdauer des Produktes in Arbeitstagen, also unter Berücksichtigung der Wochenende und der Feiertage. Sie dient als Vorschlagswert für die Berechnung des Fertigungsbeginns bei der Überleitung in die Losverwaltung. Oder anders formuliert: Jede Baugruppe hat eine übliche / Standard Dauer um Sie durch die Fertigung zu bringen. Das hat mit den Arbeitszeiten der Baugruppe nichts zu tun, sondern berücksichtigt, als grobe Näherung, die Liege- und Verteilzeiten. usw.. Diese Zeit wird, gerade bei Unterstücklisten, in der Internen Bestellung für die Berechnung des Versatzes des Produktionstermine herangezogen. Zugleich bestimmt die Durchlaufzeit den Beginntermin eines Loses. Also: Liefertermin aus dem Kundenauftrag, abzüglich der Vorlaufzeit ergibt den Endtermin des Kopfloses. Endtermin des Loses abzüglich der Durchlaufzeit ergibt den Starttermin des Loses. Dieser Starttermin ist zugleich der Endtermin des nächsten Unterloses. Der Starttermin ist für das Material zugleich der späteste Termin zu dem die benötigte Ware eintreffen sollte. Im Bestellvorschlag wird hier zusätzlich noch die Vorlaufzeit berücksichtigt.

In anderen Worten: Dies ist die Wiederbeschaffungszeit der Fertigung, wenn das gesamte Material verfügbar ist. Siehe dazu bitte auch, Auftrag Wiederbeschaffung.

#### Wozu brauch ich überhaupt die Durchlaufzeit, ich hab doch den Arbeitsplan ?
Der Arbeitsplan definiert den Kapazitätsbedarf je Arbeitsgang, je Tätigkeit. In modernen Fertigungsbetrieben ist es üblich, dass möglichst alle Arbeitsgänge parallel gefertigt werden. D.h. auf der Maschine A wird der erste Arbeitsgang begonnen. Ist dieser fertig, so wird auf der Maschine B der zweite Arbeitsgang begonnen, auf der Maschine A wird das nächste Stück vom ersten Arbeitsgang produziert. Daher ist die Durchlaufzeit nicht / nur mit enormen Aufwand berechenbar.<br>
Siehe dazu auch [AUTOMATISCHE_ERMITTLUNG_AG_BEGINN]( {{<relref "/fertigung/losverwaltung/#automatische-ermittlung-los-ende-termin" >}} )

## Kalkulation, Gesamtkalkulation?
Für die Kalkulation ihrer gesamten Stückliste steht die Gesamtkalkulation zur Verfügung. Bei dieser Berechnung werden alle enthaltenen Unterstücklisten in Strukturform dargestellt, sodass eine klare Übersicht der eingesetzten Materialien und Kosten gegeben ist. Zusätzlich werden am Ende verschiedene Gemeinkostenfaktoren mit angeführt. Die verschiedenen Gemeinkostenfaktoren können im System, unterer Modulreiter Parameter definiert werden. Für die Gesamtkalkulation werden vier Preisgruppen angedruckt.
| Preisgruppe | Beschreibung |
| --- | --- |
| a.) Gestehungspreis /  Gestehungswert | Dies zeigt diejenigen Kosten auf, die entstehen würden, wenn die Stückliste direkt vom Lager produziert würde. |
| b.) Lief1preis / Lief1wert | Diese Kosten entstehen, wenn die gesamte Ware neu vom bevorzugten Lieferanten beschafft wird.<br>In dieser Betrachtung werden auch die Mengenstaffeln des Lieferanten berücksichtigt.|
| c.) KalkPreis / KalkWert | Der kalkulatorische Wert ist der in der einzelnen Stücklisten Position eingegebene Kalk.Preis multipliziert mit der Zielmenge.<br>Bitte beachten Sie, dass der kalkulatorische Preis NUR für Stücklistengesamtkalkulation verwendet wird und KEINE Auswirkung auf die Berechnung der tatsächlichen Kosten hat. Für die Berechnung der tatsächlichen Kosten, wird immer vom Gestehungspreis des jeweiligen Artikels zum jeweiligen Zeitpunkt ausgegangen.|
| d.) Verkaufspreis / Verkaufswert | Ist bei der Ausgangs / Kopf-Stückliste ein Kunde hinterlegt, so wird für die Ermittlung der Verkaufspreise der sich über die Verkaufspreisfindung ergebende Verkaufspreis verwendet. Darin werden auch die Kunden-Sonderkonditionen und die Verkaufsmengenstaffeln berücksichtigt.<br>Für den Maschinenstundensatz wird in diesem Falle der VK-Stundensatz der Maschine verwendet. |

In einigen Branchen, z.B. in der Kunststoffverarbeitung, ist es erforderlich, dass für die Kalkulation von Einzelteilen entsprechend höhere Losgrößen hinterlegt werden. Hintergrund ist: Wenn nur von einem Fertigprodukt ausgegangen wird, würden die zu tausenden gefertigten Teile nicht in dieser Stückzahl betrachtet werden und die anteilige Belastung der Fertigung mit den Rüstkosten würde die Kalkulation komplett verfälschen.<br>
Dieses Verhalten kann mit dem Parameter BASIS_GESAMTKALKULATION gesteuert werden. Ist dieser aktiviert (= 1) so werden für die Mengenbetrachtung von Unterstücklisten immer ganzzahlige Vielfache der Fertigungssatzgröße (Artikel, Bestelldaten) herangezogen.<br>
Wird diese Betrachtung von Ihnen verwendet, so sollten Sie dies in der Praxis auch entsprechend umsetzen und bei der Internen Bestellung auch die Funktion der Verdichtung verwenden, welche auch die Fertigungssatzgrößen entsprechend mit einrechnet.

Mit der Option *Unterstücklisten verdichten* in der Gesamtkalkulation werden die Positionen der Unterstücklisten nicht im Detail angeführt, es erfolgt keine weitere Auflösung der Stücklistentiefe.<br>
Die Kosten der Unterstücklisten werden in die übergeordnete Position summiert. Somit erhält man eine übersichtliche Darstellung der Kalkulation und zugleich sind trotzdem die Änderungen in den Unterstücklisten enthalten.

Mit dem ![](gesamtmenge_material_beruecksichtigen.JPG) wird der Lief1Preis und der Verkaufspreis über die Summe der Mengen der gleichen Artikel der Gesamtkalkulation ermittelt. D.h. ist Artikel A in den diversen Unterstücklisten in der Gesamtkalkulation 5x zu je 200 Stk. enthalten, so werden die Preise für Artikel A mit der Summe von 1.000 Stk. ermittelt.

Zusätzlich kann die Gesamtkalkulation um die Felder des größten und kleinsten Lief1-Preis der letzten 2 Jahre erweitert werden. <br>
Mit dem Button Lief1Preis übernehmen  werden die kalkulierten Preise zurückgeschrieben und der Report als PDF in der Dokumentenablage abgelegt.<br>
Weiters kann der Ausdruck der Gesamtkalkulation durch die Felder Arbeitsgang, Unterarbeitsgang und Kommentar erweitert werden.

#### Wie können Entwicklungskosten berücksichtigt werden?
Oft geht es auch darum, dass die Entwicklung eines Produktes natürlich entsprechende Kosten verursacht. Nun will man diese in der Gesamtkalkulation der Stücklisten berücksichtigen, aber andererseits auch wissen, wann diese Entwicklung hereingespielt ist.<br>
Bewährt hat sich, dies über einen Lagerbewirtschafteten aber nicht lagerbewerteten Artikel abzubilden. D.h. bitte legen Sie einen Artikel für die Entwicklung des Produktes-A an. Legen Sie hier einen entsprechenden Wert (Menge x Gestehungspreis) auf Lager. Nun tragen Sie in der jeweiligen Stückliste eine passende Stückzahl ein. Wichtig: Dieser Artikel muss lagerbewirtschaftet sein. Bei der Überleitung der Stückliste in ein Los wird der Entwicklungskosten-Artikel ebenfalls mit übernommen und somit vom Lager abgebucht. Das bedeutet, wenn Sie vom Entwicklungskosten-Artikel keinen Lagerstand mehr haben, wurden die Entwicklungskosten auf Ihr Produkt-A umgelegt. Ab diesem Zeitpunkt ist der Deckungsbeitrag Ihres Produktes entsprechend höher. Ob Sie dies nun für eine Preisanpassung nutzen, oder in eine Neuentwicklung investieren, ist selbstverständlich Ihre Entscheidung.

#### Wie wird der Lief1preis Positionen berechnet?
Make or Buy Entscheidung. Siehe dazu bitte auch die Auswertung von Ist-Daten im Modul [Artikel]( {{<relref "/docs/stammdaten/artikel/#kaufen-oder-selber-machen--make-or-buy-entscheidung" >}} ).<br>
Einerseits treffen wir immer wieder auf die Frage, sollte ich das nun selber bauen, oder sollte ich das von meinem Lieferanten zukaufen. Um Ihnen diese Entscheidung leichter zu machen, haben wir die Lief1Preis Positionen Spalte in der Stücklistengesamtkalkulation eingeführt. Hier werden die Einkaufswerte der Unterpositionen sowohl Material als auch Arbeitszeit aufsummiert. Da dies neben dem Lief1Preis angedruckt wird, sehen Sie die Differenz und können so relativ rasch entscheiden, ob Sie dieses Teil zukaufen wollen.
In diesem Einkaufspreis, welcher pro Stück angegeben wird, sind auch dann Arbeitskosten enthalten, wenn für diese kein Lief1Preis angegeben ist. Hier wird dann der Gestehungspreis der Arbeitszeiten herangezogen. Die Maschinenkosten werden zum hinterlegten Kalk.Stundensatz der Maschine gerechnet.

Oft wird auch gewünscht, dass die Differenzen der Baugruppen ob eigengefertigt oder zugekauft klarer dargestellt werden sollten.
Dafür haben wir die Ansicht der Stücklistenstruktur, die Sie im Stücklistenmodul unter Info, Gesamtkalkulation, Variante Struktur ![](Strukturdarstellung.png) finden, entsprechend aufbereitet.<br>
Hier sehen Sie die Kosten bei Eigenproduktion = Lief1Preis der Positionen der Stückliste und die Kosten bei Zukauf. Zusätzlich werden noch die Werte der Eigenproduktion herausgerechnet für die es Einkaufs=Zukaufspreise gibt.

#### was bedeutet das Lief1Preis zu alt?
In der Stücklisten Gesamtkalkulation und in der Angebotsvorkalkulation wird, wenn der Parameter LIEF1PREIS_GUELTIG_IN_MONATEN_FUER_REPORT definiert ist, ein Lief1Preis (Einkaufspreis des bevorzugten Lieferanten) in lila hinterlegt, wenn das Datum für Preis gültig ab älter als die im Parameter angegebenen Monate ist.<br>
Um diese Auswertung wieder abzuschalten, stellen Sie den Wert auf -1.

#### Können auch Mindestbestellmengen bzw. Verpackungseinheiten berücksichtigt werden?
Haken Sie dazu ![](MinBesBeruecksichtigen.png) mindest Bestellmenge und Verpackungseinheiten berücksichtigen an. Dies bewirkt, dass die errechnete Menge der jeweiligen Position auf die Mindestbestellmenge bzw. Verpackungseinheit aufgerundet wird. Diese so errechnet Stückzahl wird als Berechnungsbasis für den Lieferanten-Staffel-Preis verwendet. Damit ergibt sich unter Umständen ein anderer Stückpreis.

#### Wie wird die benötigte Menge für die Gesamtkalkulation errechnet?
[Siehe oben](#welche-mengen-werden-bei-den-verschiedenen-stücklistendrucken-angedruckt)

#### Meine angeführten Stücklisten werden nicht in Details aufgelöst?
Wenn Stücklisten als Fremdfertigung (Siehe Reiter Kopfdaten) gekennzeichnet sind, so werden diese wie "Einkaufs-"Material behandelt. D.h. diese Stückliste und alle darunter liegenden Komponenten werden nicht mehr weiter ausgedruckt, da es ja als Einkaufs-Material zu betrachten ist. Beachten Sie dazu beim Druck der Stückliste in der Variante Allgemein bzw. Allgemein mit Preis den Haken bei ![](fremdgefertigte_stkl_aufloesen.gif) fremdgefertigte Stücklisten auflösen.<br>
**Hinweis:**<br>
Wird bei den Ausdrucken der Stücklisten Unterstücklisten einbinden angehakt, so werden grundsätzlich die Unterstücklisten mit eingebunden.<br>
Im Unterschied zum Ausdruck Allgemein bzw. Allgemein mit Preis wird beim Ausdruck der Ausgabestückliste darauf abgezielt eine Berechnung des benötigten Materials für eine gewünschte Stückzahl zu erhalten. Aus diesem Grunde sind die (Köpfe der) Unterstücklisten beim Ausdruck der Ausgabestückliste NICHT enthalten. Es sind ja die Berechnungen der Mengen der einzelnen Stücklistenpositionen enthalten. D.h. es besteht ein großer Unterschied zu den allgemeinen Drucken. Hier bleiben die "Überschriften" also die Unterstücklistenköpfe erhalten, aber NUR zur Übersicht.

Für eventuelle Prüfungen, ob eine Stückliste unter Berücksichtigung der teilweise lagernden Halbfertigfabrikate nutzen Sie bitte Info, [Minderverfügbarkeit](#minderverfügbarkeit).

#### Was bedeutet das '*' bzw. was bedeuten die eckigen Klammern '[]' im Druck Gesamtkalkulation?
Wenn in der Spalte Info ein '*' mit angedruckt wird, bedeutet das, das der Gestehungspreis der Unterstückliste gegenüber der Summe des Gestehungspreises der Unterstücklistenpositionen um mehr als 10% abweicht. Wenn eine Unterstückliste enthalten ist, dann wird der Gestehungspreis der Unterstückliste in eckigen Klammern angedruckt. Dieser Gestehungspreis wird nicht zur Berechnung der Stückliste verwendet und dient daher nur zur Information.

#### Es werden falsche Preise in der Gesamtkalkulation verwendet
Bei der Stücklisten-Gesamtkalkulation werden bei Liefer- und Verkaufspreisen jeweils die Mengenstaffeln berücksichtigt. Dies kann unter Umständen andere Preis ergeben als ursprünglich angenommen.

#### Es wird eine falsche Stückzeit/Mann errechnet
Bei der Stücklisten-Gesamtkalkulation werden bei Liefer- und Verkaufspreisen jeweils die Mengenstaffeln berücksichtigt. Dies kann unter Umständen andere Preis ergeben als ursprünglich 

#### Wie kann ich Stücklisten sonst noch ausdrucken ?
Neben der Gesamtkalkulation, mit ihrer entsprechend komplexen Auflösung, steht auch der Ausdruck des Arbeitsplanes und der Stückliste unter dem Menüpunkt Stückliste zur Verfügung.

Im Menüpunkt Stückliste, Drucken stehen wiederum folgende Möglichkeiten zur Verfügung:<br>
![](Stkl_Druck_Auswahl.gif)
- Allgemein:
Ein Übersichtsdruck über die Positionen der Stückliste wie z.B. für die Bauteilebestückung oder die Montage benötigt 
- Allgemein mit Preis:
Wählen Sie hier zusätzlich noch den gewünschten Preis aus
Hier stehen Gestehungspreis, VK-Preis = Verkaufspreisbasis und EK-Preis = Einkaufspreis des bevorzugen Lieferanten = 1.Lieferant zur Verfügung. Zusätzlich steht, wenn im Stücklistenkopf ein Kunde angegeben wurde der kundenspezifische Preis zur Verfügung. Für eine eventuelle Mengenstaffelberechnung wird nur die Menge der Position verwendet.
Beachten Sie bitte, dass für die Preisberechnung des kundenspezifischen Preises nicht nur die Sonderkonditionen sondern auch die für den Kunden definierte Preisliste herangezogen wird.
- Ausgabestückliste
Geben Sie hier zusätzlich noch den gewünschten Multiplikator für die Berechnungsbasis, die sogenannte Losgröße an. Damit erhalten Sie eine Liste mit benötigten Mengen und den aktuellen Lagerständen. Bitte beachten Sie, dass dies eine rein theoretische Betrachtung ist. Bei diesem Druck werden keinerlei Lagerbuchungen bzw. Verfügbarkeitsberechnungen durchgeführt. Sie dienen lediglich für eine eventuelle Abschätzung für weitere Planungen. Eine Option im Ausgangstücklistendruck ist  'nur Abbuchungsläger'. In diesem Fall wird im Feld Lagerstand die Summe der Lagerstände der Abbuchungsläger angezeigt. Im Feld Lagerort werden alle Lagerorte der Abbuchungsläger angezeigt. Der Lager-Auswahl-Button ist deaktiviert, ausserdem kann nur eine Stücklisten ausgewählt werden.<br>

Weitere Optionen beim Druck der Stückliste:<br>
- Werden Unterstücklisten mit eingebunden, so gelten bei, "nur Abbuchungsläger" diese auch für die Berechnungen der Unterstücklisten.<br>
![](Stkl_Druck_Unterstueckliste.gif)<br>
Wird Unterstücklisten einbinden NICHT angehakt, so wird nur diese eine Ebene der Stückliste ausgedruckt. Wird Unterstücklisten einbinden angehakt, so erhalten Sie, bei Unterstücklistenstruktur belassen, eine Strukturstückliste mit der Sie auf einfache Weise einen kompletten Überblick über ihre Artikel / Stücklistenstruktur erhalten. Wird Unterstücklistenstruktur belassen nicht angehakt, so werden die Stücklistenpositionen nach der gewählten Untersortierung aufgelistet.

- ![](Stkl_Druck_StklKommentar.gif) ist dieses Feld angehakt, so wird der Kommentar zur Stückliste (Bearbeiten, Kommentar) mit angedruckt.

- ![](Stkl_Druck_Positionskommentar.gif) ist dieses Feld angehakt, so wird der Kommentar jeder Zeile der Stückliste mit gedruckt.

- ![](Stkl_Druck_Zusammenfassen.gif) Hier werden alle Positionen des gleichen Artikels auf eine Zeile verdichtet. Dies ergibt in Kombination mit dem Unterstückliste einbinden z.B. einen vollständigen Materialbedarf für ein Gerät / Produkt.

- Sortierung: Wählen Sie hier die gewünschte Sortierungsstruktur. Die Reihenfolge ist von oben nach unten.<br>
Somit kann z.B. für die SMD Fertigung zuerst nach der Montageart, Oben / unten, dann nach der (Bestückungs-)Position und dann erst nach dem Artikel sortiert werden. Somit erhalten Sie den für Ihre Leiterplatte passenden Bestückungsdruck. Oder auch für die Metallverarbeitung den Druck nach den Laufenden Nummern welche den Zeichnungspositionsnummern entsprechen. Dass diese Funktionen eine entsprechend sorgfältige Erfassung in den Stücklistenpositionen voraussetzt ergibt sich von selbst.

#### Kann man mehrere Stücklisten für den Druck zusammenfassen?
Für den Druck der Ausgabestückliste können Sie mehrere Stücklisten zusammenfassen. Hierzu klicken Sie auf den Button ![](selektierte_stkl.PNG) und wählen die gewünschten Stücklisten aus.
Verwenden Sie dazu die Markierung wie [hier beschrieben]( {{<relref "/docs/client/#markieren-von-mehreren-zeilen-bei-auswahlm%c3%b6glichkeiten" >}} )

Somit erhalten Sie eine Zusammenfassung über mehrere Stücklisten hinweg, um zum Beispiel den Materialbedarf für Kalkulationen zu erkennen.

#### Es wurde versehentlich ein Artikel zur Stückliste gemacht. Kann ich dies wieder löschen?
Wenn für diese Stückliste noch keine Lose usw. angelegt wurden und wenn die Stücklisten Positionen und der Arbeitsplan leer sind, so kann durch einen Klick auf das Löschen  die Stückliste vollständig entfernt werden. Damit ist der Artikel wieder ein reiner Artikel.

#### Warum werden beim Druck Vorkalkulation alle Stücklisten und deren Positionen angedruckt?
Die Vorkalkulation aus dem Angebot enthält definitionsgemäß alle Stücklisten und Unterstücklisten und deren Positionen, da für die Fertigung der Stückliste alle Materialien benötigt werden und diese daher im Gestehungspreis zu berücksichtigen sind. Beachten Sie bitte dazu unbedingt die Definition der Angebotsstücklistenauflösungstiefe unter Angebot.

## Fremdfertigungsstückliste
Eine Fremdfertigungsstückliste dient der Definition eines Produktes, welches vollständig außer Haus produziert und beschafft wird. D.h. eine als ![](Fremdfertigung.gif) Fremdfertigung gekennzeichnete Stückliste wird wie ein gewöhnlicher Artikel behandelt. D.h. sie wird im Bestellvorschlag berücksichtigt, in der internen Bestellung jedoch nicht. Auch bei der Angebots Vorkalkulation wird eine Fremdfertigungsstückliste nicht weiter aufgelöst, da diese ja zum Gestehungspreis bei Ihrem Lieferanten eingekauft wird.<br>
Das bedeutet: Eine Fremdfertigungsstückliste dient der technischen Definition einer Baugruppe / eines Produktes, aber nicht der Abbildung der eigenen Fertigung. Das bedingt wiederum, dass in der internen Bestellung eine Fremdfertigungsstückliste nicht weiter / tiefer aufgelöst wird. Siehe dazu auch [Veredelung](#fremdfertigung).

#### Kann eine selbst gefertigte Stückliste trotzdem eingekauft werden?
Ja. Es wird zwar ein Hinweis ausgegeben, dass dies eine eigengefertigte Stückliste ist, trotzdem können Sie diese zusätzlich einkaufen.<br>
In anderen Worten, der Haken Fremdfertigung in den Kopfdaten der Stückliste dient der Steuerung für den Bestellvorschlag (wenn Fremdfertigung) bzw. für die interne Bestellung (wenn nicht Fremdfertigung)<br>

#### Anzeige des Artikels
Warum muss man beim manuellen Eintragen des Artikels danach auf etwas klicken bzw. auf die Tabulatortaste drücken, um den Artikel zu sehen?

Die Aktualisierung, gemeinsam mit der Prüfung auf eine gültige Artikelnummer und somit die Übernahme der Artikeldaten für die weitere Verwendung wird beim Verlassen des Feldes ausgeführt.

#### Auftrag erscheint nicht
Warum erscheint ein bestimmter Auftrag in der Auswahlliste nicht, obwohl er angelegt ist?

Um eine Stückliste einem (Leit-)Auftrag zuordnen zu können, muss der Auftrag im Status offen oder teilerledigt sein.

### Zielmengenart und Umrechnung
Werden keine Losgrößen berücksichtigt, also nur der Stücklisteninhalt, so greifen die Ausgangsmengenarten aus der jeweiligen Stücklisten Position und es sind die Dimensionen anzudrucken.<br>
Werden Losgrößen berücksichtigt, so geht es um Lager, also Zielmengenart und daher auch um die umgerechneten Mengen, in denen die Dimensionen berücksichtigt sind.

#### Ich würde gerne auch den Lagerstand in den Stücklisten sehen
Aktivieren Sie dafür den Mandantenparameter STUECKLISTE / LAGERINFO_IN_POSITIONEN und starten Sie das Stücklistenmodul neu. Damit erhalten sie drei zusätzliche Spalten Lagerstand, Verfügbar, Bestellt im Reiter Positionen der Stückliste. Beachten Sie dass dies die aktuelle Sicht auf das Lager ist und keinerlei Verbrauchsberechnung beinhaltet.

#### Es wurde der Einkaufspreis des ersten Lieferanten geändert. In der Gesamtstücklistenkalkulation hat dies aber keine Auswirkung?
Die Gesamtstücklistenkalkulation der Lieferantenpreise berücksichtigt auch die Mengenstaffeln. D.h. es wird der am Besten zu den benötigten Mengen passende Einkaufspreis ermittelt. Wurde nun nur der Einkaufspreis des Artikellieferanten verändert und die Mengenstaffeln nicht angepasst, so wird wie bisher der Preis anhand der Mengenstaffel, also der alte Preis verwendet. Bitte korrigieren Sie auch die Staffelpreise.

#### Kann eine Stückliste importiert werden?
Ja. Es stehen derzeit insgesamt mehrere Arten von Stücklistenimport zur Verfügung.
- [CSV Import]( {{<relref "/warenwirtschaft/stueckliste/importe/csv/" >}} )
- [Solid Works XLS Import]( {{<relref "/warenwirtschaft/stueckliste/importe/solid_works/" >}} )
- [Siemens NX CSV Import]( {{<relref "/warenwirtschaft/stueckliste/importe/siemens_nx/" >}} )
- [Import aus XLS Datei]( {{<relref "/warenwirtschaft/stueckliste/importe/xls/" >}} ) (Tabellenkalkulation, Spreadsheet)
- [Intelligenter Import]( {{<relref "/warenwirtschaft/stueckliste/importe/intelligent/" >}} )
- [INFRA]( {{<relref "/warenwirtschaft/stueckliste/importe/infra/" >}} )

#### Kann eine Standard Arbeitsplan definiert werden?
Ja. Es hat sich als praktisch herausgestellt, wenn beim Anlegen einer neuen Stückliste automatisch ein Standard Arbeitsplan einkopiert wird. Um dies so flexibel wie möglich zu erhalten haben wir den Weg der Definition einer Standard Stückliste gewählt. Damit können alle Werte die im Arbeitsplan definiert werden können auch vordefiniert werden.

Um diese Funktion zu erhalten, gehen Sie bitte wie folgt vor:

- a.) Legen Sie eine Standard Stückliste an
- b.) Befüllen Sie diese im Arbeitsplan mit den gewünschten Daten
- c.) Tragen Sie nun unter System Parameter für den Parameter ARBEITSPLAN_DEFAULT_STUECKLISTE die Artikelnummer Ihrer Standard Stückliste ein

#### Materialbuchung bei Ablieferung
Bewirkt, dass das Material bei Losausgabe im Lager bleibt (es werden Fehlmengen erzeugt, damit die Bestellvorschläge / BWG Vorschau usw. wieder stimmt).
Wird nun eine Ablieferung des Loses vorgenommen, so wird die Sollsatzgröße nachgebucht. {Siehe dazu auch}.

#### Enthaltene Stücklisten ausgeben
Ist hier der Haken gesetzt (= default), so werden auch die in der Stückliste enthaltenen Unterstücklisten bei der Losausgabebuchung vom Lager in die Produktion gebucht. Ist der Haken nicht gesetzt, so verbleiben die Mengen der Unterstücklisten als Fehlmengen auf Lager und werden erst bei der Ablieferungsmeldung des Loses vom Lager in die Fertigung gebucht. Beachten Sie dazu bitte auch den Parameter BEI_LOS_ERLEDIGEN_MATERIAL_NACHBUCHEN. Abhängig von diesem Parameter wird bei der Los-Ablieferungsbuchung automatisch die benötigte Sollsatzgröße wenn vorhanden vom Lager in das Los gebucht und dann die Ablieferungsbuchung durchgeführt. Sind die benötigten Sollsatzgrößen nicht ins Los gebucht so erscheint eine entsprechende Fehlermeldung. {Siehe dazu bitte auch}.

### Abbuchungsläger
Im Reiter Abbuchungsläger  können Sie für die Stückliste die Reihenfolge der Läger festlegen, von denen das Material im Los abgebucht wird. Diese Abbuchungsläger haben Vorrang vor den Los-Abbuchungslägern und der Definition in den Artikel-Grunddaten. {Siehe dazu auch}.

### Fremdfertigung
Wie bildet man die Veredelung bzw. Fremdvergabe einzelner Arbeitsschritte bzw. Lohnfertigung eines Artikels in **Kieselstein ERP** ab:

Eine sehr gute Abbildung der für die Fremdfertigung erforderlichen Schritte kann mit der  Mehrlagerverwaltung kombiniert mit einer mehrstufigen Stückliste erreicht werden. 

 ![](Fremdfertigung.png)

1. Der Eigenfertigungsanteil des Artikels wird mit einer Rohteilstückliste produziert.

2. Dieses Rohteil (Hebel-Roh) wird auf ein Halbfertig-Lager gebucht (Ziellager des Loses)

3. Mit einem Lieferschein an den Lieferanten mit Abbuchungslager Halbfertiglager und Ziellager Lieferantenlager wird die Halbfertigware an den Lieferanten geliefert. Zugleich wird eine Bestellung über die Lohnveredelung beim Lieferanten ausgelöst.
Hinweis: Da Sie diesen Artikel Lohnveredelung in der Stückliste enthalten haben und dieser Artikel über den Bestellvorschlag auch bestellt wird, hat Ihr Lieferant zeitgerecht die Information, dass in Zukunft etwas für Sie zu fertigen ist.

4. Vom Lieferanten wird die Veredelung vorgenommen.

5. Sie erhalten den veredelten Artikel und damit gemeinsam die "Lieferung" des Artikels Veredelung. 

6. Diese Veredelung wird dem Lager zugebucht.

7. In der übergeordneten Stückliste (Hebel galvanisiert) wird das Rohteil verbraucht und zusätzlich die Lohnveredelung.

8. Vom Lieferanten wird die Lohnveredelung verrechnet. Diese Lohnveredlungskosten wurden über die Zubuchung (6.) dem übergeordneten Los zugebucht. Damit sind die Rohmaterialherstellkosten und die Veredlungskosten in den Gestehungskosten des übergeordneten Loses (Hebel galvanisiert) enthalten.

9. Der veredelte Artikel (Hebel galvanisiert) kann nun weiterverarbeitet werden.

 

Hinweis: Ob im veredelten Artikel noch weitere Fertigungsschritte enthalten sind, oder ob dieser direkt auf ein weiteres Halbfertiglager gebucht wird hängt von Ihren Bedürfnissen in Bezug auf Lagertransparenz ab.

 

Vorteile dieser Abbildung:<br>
- Da man die Rohteile immer auf eigenen Lägern hat, ist der Lagerstand bei den Lieferanten aber auch der Halbfertigfabrikate klar definiert.
- Man sieht die Kosten der Veredelung und den Zusammenhang zwischen den einzelnen Artikeln.

Zum Thema [Fremdfertigung siehe bitte auch]( {{<relref "/fertigung/losverwaltung/fremdfertigung">}} ).

#### Kann man die Lohnveredelung, Verlängerte Werkbank, auch einfacher abbilden?
Diese Frage wird oft an uns herangetragen. Die Basis für Ihre Entscheidung ob eine einfache Struktur oder obige Struktur verwendet werden muss, hängt davon ab, ob die Lagerstände der Halbfertigfabrikate die sich aus den einzelnen Lohnveredelungsschritten ergeben, bekannt sein müssen oder nicht. Ist dies nicht verforderlich, so hat sich in der Praxis folgende Vorgehensweise bewährt.

Es wird der fertige Artikel mit seiner Artikelnummer angelegt. Die verschiedenen Fremdarbeitsgänge werden mit der gleichen Artikelnummer, welche am Ende um ein Kennzeichen (F1, F2, ... oder auch die Art des Fremdarbeitsgangs (z.B. D ... Drehen, V ... Veredeln)) ergänzt wird angelegt. Hinter diesen Fremdarbeitsgängen können die unterschiedlichen Lieferanten mit den jeweiligen Einkaufs(staffel)preisen hinterlegt werden. Somit erhalten Sie eine einfache durchgängige Sicht auf die Preisgestaltung Ihres Artikels und auch welche Arbeitsgänge Sie bei welchem Lieferanten beschaffen müssen. Ev. verwenden Sie dies in Kombination mit dem Beginnterminoffset in der Stücklistenposition. Um zu Erkennen in welchem Fertigungsstand Ihr Los (und damit der von den Sub-Lieferanten geieferte Veredelungsprozess) ist, nutzen Sie einfach den Reiter Material des Loses. Das was von Ihren Lieferanten schon angeliefert (und von Ihnen zugebucht) wurde, hat keine / nur teilweise Fehlmenge.

Ein Beispiel:<br>
Es ist ein Drehteil zu fertigen und zu passivieren. Da Sie ein Elektronik Unternehmen sind, wird alles außer Haus gemacht, nur das Rohmaterial mit seinen speziellen Eigenschaften beschaffen Sie doch selbst.<br>
1122 .... Artikelnummer der herzustellenden Stückliste<br>
5000 .... Rohmaterial Messing<br>
Dieses Rohmaterial ist nach Zeichnung zu drehen und danach zu passivieren.<br>
Also gibt es noch zwei weitere Lohnveredelungs-Artikel die Sie zukaufen müssen, die einen fixen Staffel-Preis haben und gegebenenfalls auch entsprechende Lieferzeiten<br>
1122D ... Fremdarbeitsgang (auch Lohnveredelung) drehen lt. Zeichnung zum Preis von ...<br>
1122P ... Fremdarbeitsgang passivieren mit 10µ zum Preis von ...<br>

Das hat den enormen Vorteil, dass Sie jederzeit eine richtige Vor-Kalkulation haben. Beim Bestellvorschlag scheinen die Lohnveredelungsartikel bei den gewünschte Lieferanten auf. Und den eigentlichen Produktionsfortschritt sehen Sie im Los sehen, unter der Voraussetzung, dass die Wareneingänge ordentlich gebucht werden. Hinterlegen Sie nun noch die jeweiligen Bestelldaten wie Zeichnungen, Beschreibungen bei den Fremdarbeitsgangartikel so ist für den Lieferanten alles klar und Sie müssen nur die Zeichnungen Anweisungen welche bei den Artikeln hinterlegt sind auf aktuellem Stand halten, was Sie soweiso machen müssen.

Eine Weiterentwicklung davon ergibt sich, wenn für die Fremdarbeitsgänge auch noch Tätigkeiten mit festen Rüstzeiten auf der "Maschine" Fremdarbeitsgang hinterlegt werden. Mit dem passenden Maschinenzeitmodell ergibt sich daraus eine fest einzuplanende Dauer für den Fremdarbeitsgang und damit eine entsprechende Sicht in der Kapazitäsplanung und zugleich können Sie die Anweisungen des Fremdarbeitsgangs als [Hinweis in die Bestellung]( {{<relref "/einkauf/bestellung/#verkn%c3%bcpfen-einer-bestellposition-mit-einer-los-materialposition" >}} ) mit aufnehmen.

## Hilfsstückliste
<a name="Hilfsstückliste"></a>
In **Kieselstein ERP** können auch Hilfsstücklisten, welche in anderen Systemen manchmal Phantomstücklisten genannt werden, definiert werden.<br>
Diese haben die Eigenschaft, dass bei der Überleitung der übergeordneten Stückliste die darin enthaltene Hilfsstückliste mit ihren Einzelteilen und Arbeitsgängen in die übergeordnete Stückliste eingefügt wird. Man könnte auch sagen, dass der Stücklistenbaum flachgedrückt wird.
Das bedeutet, das Hilfsstücklisten nur zur einfacheren / strukturierten Erstellung der Stücklisten benötigt werden.<br>
Denken Sie dazu z.B. an ein Gehäuse, in welches mehrere Griffe (z.B. 4Stk) eingearbeitet werden müssen.<br>
Jetzt könnten Sie natürlich diese Griffe in der Stückliste des Gehäuses 1:1 integrieren. Wenn Sie nun ein weiteres Gehäuse mit Griffen benötigen so ist das wiederum hier zu definieren, usw. Bei Änderungen müssen Sie bei dieser Methode alle Stücklisten ändern. Verwenden Sie für die Griffe aber die Hilfsstückliste, so definieren Sie ein Mal einen Griff und fügen diesen als Unterstückliste in die Gehäuse Stückliste ein. D.h. Ihre Definition für den Griff ist klar ersichtlich und wird einfach in die Gehäuse Stückliste übernommen. Sind nun Änderungen am Griff vorzunehmen, so ändern Sie die Definition der Hilfsstückliste und schon sind alle Stücklisten geändert. Andererseits haben Sie aufgrund dieser Definition nicht die Erfordernis das Zwischenprodukt als eigenes Los zu fertigen, fertig zu melden usw..<br>
Ein ähnliches Beispiel aus dem Bereich der Elektrotechnik wären Einbau-Schalter, Leuchtaster und ähnliches, welche vom Begriff her auch immer nur Schalter und Leuchten sind, im technischen Detail aber aus, Schalter, Schalter Montagering, Anschlussdrähten usw. bestehen.

<u>Hinweis:</u><br>
Hilfsstücklisten haben, wenn sie richtig definiert sind, keinen Lagerstand und auch keine Reservierungen bzw. Fehlmengen. Hilfsstücklisten werden auch nicht beim Lieferanten bestellt, im Gegensatz zu Fremdfertigungsstücklisten.<br>
Hilfsstücklisten haben, da sie nicht lagernd sind auch keine Verkaufspreise.<br>
Manche unserer Anwender erklären sich für den Beginn die Hilfsstückliste als Fertigungsset bzw. als Phantomstückliste.

<u>Hinweis:</u>
Werden Stücklisten mit Hilfsstücklisten in die Fertigung(Los) übernommen, so werden bis zu 10 Hierarchieebenen der Hilfsstücklisten aufgelöst (flachgedrückt). Dies kann z.B. für die Abbildung einfacher Variantenstücklisten verwendet werden. Bitte beachten Sie jedoch, dass eine Hilfsstückliste nicht als Soll-Position in ein bestehendes Los einfügen können, die Auflösung erfolgt nur als Teil einer Stückliste. Wenn eine Hilfsstückliste in ein bestehendes Los eingefügt wird, so erhalten Sie eine Fehlermeldung.

#### Hilfsstückliste oder Stückliste und was ist dann eine Unterstückliste.
<a name="Unterstückliste"></a>
Der Begriff der Unterstückliste ist ein Hilfs-Vokabel um einen in einer Stückliste enthaltenen Artikel so zu beschreiben, dass klar ersichtlich ist, ob dies nun ein gewöhnlicher Einkaufsartikel ist oder ob es eben ein Artikel ist, der selbst wiederum aus mehreren Artikeln / Fertigungsschritten besteht. Ob diese Unterstückliste nun eine Hilfsstückliste ist oder eine (echte) Stückliste ist hier nicht von Bedeutung.<br>
Siehe dazu auch das Bild oben bzw. nachfolgend und den Begriff Kopfstückliste.<br>
Auch dies ist rein ein Hilfs-Vokabel um bei Besprechungen über die Struktur leichter das Gewünschte zu bezeichnen.<br>
Es kann jederzeit eine Kopfstückliste in einer übergeordneten Stückliste zu einer Unterstückliste werden.

Hilfsstückliste und Stückliste müssen jedoch klar von einander getrennt werden. Siehe dazu oben.

#### Wie kann nun der Unterschied zwischen Stückliste und Hilfsstückliste erklärt werden?
Da von den Anwender immer wieder die Frage nach dem Unterschied zwischen den beiden Arten der Stückliste kommt hier eine etwas pragmatische Erklärung.
  * Erklärung für Elektriker: Denken Sie an eine rote Signalleuchte die Sie in einen Schaltschrank einbauen. Von der Denke her, ein Teil. Von der Materialwirtschaft besteht die rote Signalleuchte aus Fassung, Anschlussdrähten, Bohrarbeiten, Lämpchen, Kappe usw.. In den Stücklisten will ich nur die rote Signalleuchte stehen haben (so wird ja gedacht) im Los, also im Materialbedarf muss ich aber die Fassung, die Anschlussdrähte, die Bohrarbeiten, das Lämpchen, die Kappe usw. haben, weil ja alle Teile aus dem Lager entnommen werden. Einen Lagerstand für die rote Signalleuchte hat es nie gegeben, es wurde diese auch nie produziert. Also ist die rote Signalleuchte die Hilfsstückliste.<br>
Oder in anderen Worten, ein Makro. 
  * Erklärung aus der Welt der Küche: Für jeden Kuchen gibt es einen Basis Teig. Dieser besteht aus Mehl, Zucker, Eier, Bearbeitungsanweisung usw.. Will ich aber z.B. einen Früchtetorte mit köstlichen Erdbeeren backen, so brauche ich den Basis Teig und die Erdbeeren und das Gelee usw.. Also steht im Rezept (= Stückliste) Basis Teig plus Erdbeeren plus Gelee usf.. Beginne ich nun mit dem Kuchenbacken, dann nehme ich Mehl, Zucker, Eier, Erdbeeren, Gelee und menge dies alles zu einer köstlichen Erdbeertorte zusammen. Einen Lagerstand für den Basisteig hat es nie gegeben, er wurde ja gleichzeitig mit den anderen Zutaten verarbeitet. Also ist der Basis Teig die Hilfsstückliste. 
#### Bei der Gesamtkalkulation sehe ich die Hilfsstückliste, wird nun das Los aufgelöst fehlt die Hilfsstückliste
Genau dieses Verhalten ist mit der Hilfsstückliste beabsichtigt.<br>
Ein extremes Beispiel ist, wenn in einer Stückliste eine leere Hilfsstückliste enthalten ist. So wird bei der Gesamtkalkulation diese als Info noch angezeigt. Bei der Überleitung in das Fertigungslos wird der Inhalt der Hilfsstückliste anstatt derselben in die Stückliste eingefügt. Oder in anderen Worten die (Material-) Positionen und die Arbeitsgänge der Hilfsstückliste werden in der Ebene der Stückliste hinzugefügt und der Kopf der Hilfsstückliste wird entfernt (sonst wäre er ja doppelt und damit keine Hilfsstückliste mehr). Aus obigem Beispiel ergibt sich damit, dass der Kopf der Hilfsstückliste aus dem Los verschwindet und da keine Inhalt da sind, ist die gesamte Hilfsstückliste verschwunden. Tragen Sie in diesem Falle entweder Artikel in die Hilfsstückliste ein, oder ändern Sie die Art des Artikels / der Stückliste entsprechend ab.

#### Hilfsstückliste, Stückliste und Ebene
Oft werden die Ebenen mit der Anzahl der Stücklisten verwechselt. Daher hier noch einmal die Darstellung was mit Stücklisten-Ebenen gemeint ist.

Dazu auch eine kleine Information aus der Praxis:

In der Automobilindustrie verwendet man zwischen 9 bis 12 Ebenen. Dies ist bereits enorm komplex.

Der Großteil unserer Anwender hat zwischen 2-3 Ebenen, egal ob nun im Maschinenbau/Metallverarbeitung oder in der Elektronik/Elektrotechnik oder auch in der Lebensmittelproduktion.

Eine sehr tiefe Anwendung die mir bekannt ist, ergibt sich dadurch, dass der Anwender sehr viele verschiedene Fertigungsstandorte und sehr viele Fremdfertiger hat. Womit er für die lagertechnische Abbildung auf ca. 9Ebenen kommt.

Hier kommt dazu, dass z.B. bei 4 Ebenen durchaus 50 verschiedene Stücklisten beteiligt sein können.

 ![](Stueckliste_Ebenen_Hilfsstuecklisten.png)

#### Wie erzeuge ich eine Unterstückliste?
In **Kieselstein ERP** wird eine Stückliste dann zur Unterstückliste, wenn die Stückliste in eine andere Stückliste eingebunden wurde. 

D.h. um Unterstücklisten anzulegen, wird zuerst der Artikel um die Stücklisteninformationen ergänzt und danach dadurch, dass der Artikel respektive die Stückliste in eine übergeordnete Stückliste eingebunden wird, wird diese Stückliste zur Unterstückliste. 

Damit haben Sie jederzeit die Möglichkeit Ihren Stücklistenbaum in jeder Richtung zu ergänzen. 
- [Setartikel]( {{<relref "/warenwirtschaft/stueckliste/setartikel/" >}} )
- [Ersatztypen]( {{<relref "/warenwirtschaft/stueckliste/ersatztypen/" >}} )
- Materialentnahme bei Ausgabe oder bei Erledigung?<br>
[Siehe bitte Fertigung]( {{<relref "/fertigung/" >}} )
- keine automatische Materialbuchung?<br>
[Siehe bitte Fertigung]( {{<relref "/fertigung/" >}} )
- Überlieferbar?[Siehe bitte Fertigung]( {{<relref "/fertigung/losverwaltung/#kann-ein-los-auch-%c3%bcberliefert-werden" >}} )
- In Lagerstandsdetailsauswertung berücksichtigen?
[Siehe bitte Auftragsjournal offene Details]( {{<relref "/verkauf/auftrag/#offene-auftragsdetails" >}} )

#### Auf Beleg mitdrucken
In der Eingabe der Stücklistenpositionen steht unten die Checkbox "Auf Beleg mitdrucken" ![](AufBelegMitdrucken.gif) zur Verfügung. Wird dies angehakt, so bedeutet dies, dass auf den offiziellen Verkaufsbelegen (Angebot bis Rechnung) diese Zeilen mitgedruckt werden. Voraussetzung dafür ist, dass die, in den Standard Formularen enthaltenen Ausgabe-Parameter mit in Ihren Reportvorlagen enthalten sind.

#### Sehe ich auch wo welche Artikel verwendet werden? 
Siehe bitte Artikel, Info, [Verwendungsnachweis]( {{<relref "/docs/stammdaten/artikel/#sehe-ich-auch-wo-welche-artikel-verwendet-werden" >}} )

#### Wozu Ziellager in den Stücklisten?
In den Kopfdaten der Stückliste finden Sie auch die Möglichkeit das ![](Stueckliste_Ziellager.gif) auszuwählen. Wozu ?

In vielen Fertigungsbetrieben ist es gängige Praxis, dass Halbfertigfabrikate entweder auf ein eigenes Lager gebucht werden oder zur weiteren Veredelung sofort an den Lieferanten weitergesandt werden. Um nun den Buchungsaufwand entsprechend zu reduzieren, kann in der Stückliste das Standard Ziellager für die entsprechende Stückliste definiert werden.

#### Artikel in allen Stücklisten ersetzen
Im Menüpunkt Bearbeiten, "Artikel ersetzen durch" kann in allen Stücklisten ein Artikel durch einen anderen ersetzt / ausgetauscht werden. Es werden dabei auch die Arbeitspläne durchforstet. Die Ersatztypen der Stücklistenposition werden nicht geändert. 
![](Ersetzen_Durch.jpg)<br>
Der obere Artikel ist derjenige, der durch den unteren in allen Stücklisten ersetzt werden soll. 

#### Maschine in allen Stücklisten / Arbeitsplänen ersetzen
<a name="Maschine ersetzen"></a>
Im Menüpunkt Bearbeiten, "Maschine ersetzen durch" kann in allen Stücklisten, eigentlich den Arbeitsplänen der Stücklisten eine Maschine durch eine andere ersetzt / ausgetauscht werden. Es werden dabei auch die Arbeitspläne durchforstet.

#### Aktualisierung aller Lose
Werden Stücklisten geändert, so besteht immer wieder der Wunsch, dass alle aktuell in Arbeit befindlichen Lose auch mitgeändert werden sollen. Wir haben daher die Funktion Lose aktualisieren im Menü, Bearbeiten geschaffen. 
![](Alle_Lose_anhand_Stueckliste_aktualisieren.jpg)
Beantworten Sie die anschließende Frage entsprechend.<br>
Es werden dabei alle Lose, der einen oder aller Stücklisten, aktualisiert, die im Status angelegt oder In Produktion sind.<br>
Teilerledigte oder erledigte sowie stornierte Lose werden nicht verändert.<br>
Es wird auch der jeweilige Arbeitsplan aktualisiert.<br>

Bei Losen in Produktion werden die Lagerbuchungen soweit möglich an die Solldaten angepasst.<br>
Beachten Sie, dass teilerledigte Lose NICHT mehr mitgeändert werden (dürfen).<br>
D.h. überzähliges Material wird zurückgegeben, zusätzlich benötigtes Material wird ins Los gebucht.<br>
Diese Lagerbuchungen werden in einem Report am Ende der Korrekturbuchungen einmalig ausgedruckt. Ein neuerlicher Druck dieser Veränderungen steht nicht zur Verfügung. 

<u>Hinweis:</u><br>
Alle Materialpositionen die Nachträglich, ohne Sollbezug entnommen wurden, werden nicht verändert, da diese Funktion meistens dann benötigt wird, wenn Produkte noch im Entwicklungsstadium sind und daher für Umbauten, Reparaturen auch immer wieder zusätzliches ungeplantes Material entnommen werden muss. Daher bleibt dieses Material am Los.

Der Parameter LOSE_AKTUALISIEREN steuert, ob die Aktualisierung aller Lose aktiv ist. Stellen Sie diesen Parameter auf  0 so ist die Option 'Alle Stücklisten aktualisieren' nicht mehr verfügbar.<br>
0 ... nur die Lose der gewählten Stückliste aktualisieren und nur Lose die im Status angelegt sind<br>
1 ... Globale Aktualisierung: Stückliste aller Lose in den Status angelegt, ausgegeben, in Produktion werden aktualisiert und ein Aktualisieren aller Lose der gleichen Status anhand aller Stücklisten ist ebenfalls möglich. Hinweis: Teilerledigte und erledigte Lose werden nicht aktualisiert.

#### Techniker?
Es gibt immer wieder die Situation, dass für ein (Fertigungs-)Projekt ein verantwortlicher Techniker benannt werden muss.<br>
Dies wird durch die Definition des Technikers ![](Techniker_Auswaehlen.gif) in den Los-Kopfdaten erfüllt.<br>
Abhängig vom Parameter LOSAUSWAHL_TECHNIKERFILTER wirkt die zusätzliche Zuordnung weiterer Techniker im oberen Modulreiter ![](Zusaetzliche_Techniker_definieren.gif) Techniker. D.h. ist der Parameter auf 1, so dürfen nur der in den Kopfdaten benannte und somit verantwortliche Techniker und die unter Techniker definierten Mitarbeiter Zeiten auf dieses Los buchen.<br>
Ist dieser Parameter auf 0, so dürfen alle Personen Zeiten auf die Lose buchen.<br>
Ist nun ein Los bereits ausgegeben, so kann der verantwortliche Techniker auf einfache Weise über den Knopf ![](Nachtraeglich_verantwortlichen_Techniker_aendern.gif) Technikerzuordnung ändern verändert werden.<br>
Um die Zuordnung der Techniker ändern zu können, benötigen Sie das Rollenrecht FERT_TECHNIKER_BEARBEITEN_ERLAUBT. 

## Stücklisten im Projektumfeld
Im Umfeld des Projektfeldes stellt sich die Frage wie Stücklisten in Kombination mit den Hilfsstücklisten verwendet werden sollten.<br>
Hintergrund ist meistens, dass in erster Betrachtung jedes Projekt ein Unikat darstellt. Sieht man genauer hin, so gilt das meist nur mehr für das Endprodukt. Die verwendeten Komponenten stellen oft bereits in anderen Projekten gefertigte Teile dar.

Bitte machen Sie sich vorneweg mit den Begriffen [Hilfsstückliste](#hilfsstückliste) und [Unterstückliste](#wie-erzeuge-ich-eine-unterstückliste) vertraut.<br>
Für das Verständnis was denn nun eine Hilfsstückliste sein sollte und was ein Stückliste und damit ein herzustellender Artikel sein sollte hilft oft die Beantwortung zweier einfacher Fragen:
- wird dieser Artikel, z.B. als Ersatzteil, verkauft? 
- hat der Artikel einen Lagerstand bzw. einen Gestehungspreis? 

Wird eine der oben angeführten Fragen mit Ja beantwortet, so sollte davon ausgegangen werden, dass dieser Artikel als Stückliste zu betrachten ist. Insbesondere wenn Sie den Artikel für längere Zeit auf Lager legen oder dieser in einer (Zwischen-)Inventur erfasst werden sollte, muss er als lagerbewirtschafteter Artikel und damit als Stückliste geführt werden. Für die Entscheidung ob Stückliste oder doch nur Hilfsstückliste beachten Sie dazu bitte auch die [Halbfertigfabrikatsinventur in der Fertigungsverwaltung]( {{<relref "/fertigung/losverwaltung/#halbfertigfabrikatsinventur" >}} ). Wobei ganz klar eine Bedingung besteht. Wenn in Ihrem Inventurstand xx Stk Produkt yyy stehen muss, muss der Artikel als lagerbewirtschaftete Stückliste geführt werden.<br>
**Wichtig:**<br>
Die Fertigung einer Stückliste bedingt immer das anlegen eines eigenen Loses um dieses Teil zu produzieren.

### Wieso so kompliziert ?
Ja, in erster Betrachtung klingt das kompliziert.<br>
Um ein Gerät / Produkt herzustellen sind gewisse Schritte erforderlich. Wenn Sie diese nachhaltig, planbar und wiederholbar haben wollen, müssen diese geplant werden und gegebenenfalls auch über den Lebenszeitraum des Produktes angepasst werden. Genau diese Möglichkeiten haben Sie mit der Stücklistenverwaltung.

#### Ich kriege doch alle Daten aus meiner CAD raus.
Das stimmt meistens für die Material-Stücklisten. Im Bereich der Elektronik / Elektrotechnik stimmt selbst das nicht exakt, da oft nur Platzhalter für Artikel verwendet (Widerstand 1/4W) werden. D.h. um einen Artikel tatsächlich herstellen zu können benötigen Sie immer exakte Angaben welcher Artikel das ist. Also nicht nur Widerstand 1/4W sondern auch Wert, Toleranz, Spannungsfestigkeit usw. oder für die Metallverarbeitung nicht nur Mutter M3, sondern Qualität, Material, Toleranz etc..

In den CAD Stücklisten fehlt aber immer der Arbeitsplan, also was ist zu tun um aus den Einzelteilen, dem Rohmaterial, ein neues Ganzes herzustellen. In der Stückliste wird definiert, welche Tätigkeiten durchgeführt werden müssen um das Produkt herzustellen. Diese Arbeitsschritte werden im [Arbeitsplan]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan/" >}} ) durch die Auflistung der Arbeitsgänge definiert. Der Arbeitsplan ist in **Kieselstein ERP** ein Teil der Stückliste.

#### Ich habe doch meinen Projektplan
Die Denke in Projektschritten mit den Forderungen der Fertigung / der Produktionsabläufe in Einklang zu bringen, ist sicher eine wesentliche Aufgabenstellung an die Projekt- / Fertigungs- / Konstruktionsleitung. Der Vorteil daraus ist, dass sehr rasch wiederverwendbare Teile erkannt werden und so der oft vorhandene Produktwildwuchs reduziert wird. D.h. dass immer mehr standardisierte Artikel eingesetzt werden. Dies wirkt sich auf die Verfügbarkeit, die Lagerbevorratung und auch auf die Stückkosten der Teile aus und trägt somit zur effizienteren Produktion bei.

#### Wozu dient der Erfassungsfaktor?
#### Für welche Menge wird eine Stückliste geschrieben?
Die Stückliste dient dazu um eine Mengeneinheit eines Artikels, wer nennen dies auch den Stücklisten-Kopf-Artikel, zu produzieren. Ist z.B. der Kopfartikel mit einer Mengeneinheit kg definiert, so definiert die Stückliste wieviel von welchem Material Sie benötigen um ein kg des Kopfartikels erzeugen zu können. Dies wird gerne auch für die Rezeptverwaltung verwendet.

Mit dem Erfassungsfaktor definieren Sie eine eventuelle Umrechnung auf eine andere Erfassung. 

Beispiele dazu sind:
- In der Küche sind die Rezepte in aller Regel für vier Portionen ausgelegt. Also ein Erfassungsfaktor von 4.
- Denken Sie ihr Produkt z.B. in Gebindegrößen, so sind hier die Gebindemengen angegeben. Z.B. hat das Fass in dem das Kunststoffgranulat abgefüllt wird immer 100 kg, das Granulat selbst wird aber in kg Mengengeführt, in den Stücklistenpositionen aber immer der Inhalt von einem Fass angegeben, so muss der Erfassungsfaktor auf 100 (kg) stehen.

Ein Beispiel aus dem veredelnden Handel:<br>
Aufgabe: Es wird Kunststoffmaterial das lose geschüttet angeliefert wird gemahlen und in Fässern zu 100kg abgefüllt.<br>
Sowohl das Rohmaterial als auch das gemahlene werden in kg bewirtschaftet.<br>
Im Artikel sind angelegt:
- Rohmaterial ungemahlen, Mengeneinheit kg
- Material gemahlen, Mengeneinheit kg
- Gebinde, Fass für 100kg

Es gibt eine Stückliste um das Material gemahlen zu produzieren.<br>
In den Stücklistenpositionen sind eingetragen:<br>
100 kg Rohmaterial ungemahlen<br>
1Stk Gebinde

Würde nun bei dieser Stückliste der Erfassungsfaktor auf den default Wert 1 belassen, so würde dies bedeuten: Um ein kg Material gemahlen zu erzeugen werden 100kg Rohmaterial benötigt. D.h. für die Denkweise in produzierten Fässern (beim Schreiben der Stückliste), stellen Sie den Erfassungsfaktor, passend zu unserem Beispiel auf 100. Das bedeutet also mit den Materialpositionen der Stückliste werden 100 Mengeneinheiten des Fertigproduktes erzeugt, in unserem Falle also 100kg.

Dass der Erfassungsfaktor auch für die Arbeitspläne gilt ist für uns selbstverständlich. Siehe dazu auch die nachfolgende Beschreibung.

Hinweis: Der Erfassungsfaktor ist nur ein Denkhilfsmittel. Wenn Sie, um beim obigen Beispiel zu bleiben, dann 200kg produzieren, werden die Mengen bei der Überleitung der Stückliste in das Los selbstverständlich entsprechend umgerechnet.

#### Können Stücklisten auch prozentuell erfasst werden?
<a name="Kunststofffertigung"></a>
Gerade in der Chemie aber auch in der Kosmetik usw. ist oft die Denkweise Mischungen der Rezepturen in Prozenten anzugeben. Dies kann in **Kieselstein ERP** sehr einfach abgebildet werden.<br>
Die Basis ist, dass das Zielprodukt meist in kg oder g als lagergeführte Mengeneinheit definiert ist.<br>
Daraus ergibt sich, dass für z.B. 100kg des Fertigproduktes xxkg der Rohwaren erforderlich sind.<br>
Um dies einfachst eingeben zu können, definieren Sie beim Erfassungsfaktor 100 (kg).<br>
Nun geben Sie die Anteile der Rohwaren, der Vorprodukte in kg an.

Sollten aus Ihrem Produktionsprozess neue (Abfall-)Produkte entstehen, so geben Sie bitte diese Mengen / Prozentsätze mit negativem Vorzeichen an. Das bedeutet, dass zum Fertigstellungszeitpunkt der Produkte diese Mengen an Lager zurückgebucht werden.

Da ein chemischer Prozess in der Regel unabhängig von den Mengen (Stückzahlen) ist, definieren Sie bitte bei den Arbeitszeiten nur Rüstzeiten, was bedingt, dass Sie pro Fertigungssatzgröße (Durchlaufprozess) ein Los anlegen sollten. In der Fertigungssatzgröße geben Sie die optimale Fertigungsmenge passend zu Ihren Misch-Mitteln an.

#### Wie sind die Stücklisten in der Kunststofffertigung anzulegen?
<a name="Werkzeug"></a>
In der Kunststofffertigung ist in der Regel die Schusszahl die Basis für die Berechnungen. Also diejenige Anzahl an Teilen die bei jedem Spritzvorgang aus dem Werkzeug fallen (sollten).<br>
In **Kieselstein ERP** wird dies in den Kopfdaten der Stücklisten unter Erfassungsfaktor abgebildet. Bitten Sie Ihren **Kieselstein ERP** Betreuer dies entsprechend zu benennen.
Um Material und Arbeitszeit richtig zu erfassen ist folgendes zu beachten.
1. Material<br>
Geben Sie beim Rohmaterial das Gesamtgewicht pro Schuss ein, inkl. einem verworfenen Anguss. Üblicherweise wird dies in g (Gramm) eingegeben werden. Nutzen Sie dazu die Positionsmengeneinheit für die Umrechnung. 
2. Arbeitszeit<br>
Für die Rüstzeiten werden in der Regel sowohl Mitarbeiter als auch Maschine benötigt.
Für die Stückzeiten läuft üblicherweise die Maschine alleine. Nutzen Sie hier bitte Nur Maschinenzeit. 
3. Werkzeug<br>
Hier ist meistens die Forderung, dass nur bekannt sein sollte ob das Werkzeug im Hause ist, oder ob es beim Kunden, beim Werkzeugmacher o.ä. ist. D.h. das Werkzeug wird 
    - Lagerbewirtschaftet
    - Wenn es außer Haus geht wird dies durch einen Lieferschein dokumentiert. Kommt es zurück, wird auch dies durch einen Lieferschein mit negativer Menge festgehalten.
    - Die ursächliche Anlieferung wird wie für jedes "Material" im Wareneingang einer Bestellung gebucht
    - Damit Sie das Werkzeug auch finden, hinterlegen Sie entsprechend den Lagerort für jedes Werkzeug
    - Jedes Werkzeug ist ein eigener Artikel 
4. Werkzeug Verfügbarkeit im eigenen Hause<br>
Da ein Werkzeug nur für einen Fertigungsprozess und somit für ein Los verwendet werden kann, hinterlegen Sie bereits in der Stückliste, dass das Werkzeug
    - a.) Ein lagerbewirtschafteter Artikel ist
    - b.) diese Stücklistenposition wird mit 1Stk Rüstmenge angegeben, was bewirkt, dass das Werkzeug zum Produktionsbeginn aus dem Lager in das Los entnommen wird
    - c.) eine weitere Stücklistenposition wird mit (minus) -1Stk Rüstmenge angegeben. Dies bewirkt, dass das Werkzeug zum Zeitpunkt der vollständigen Ablieferung wieder ans Lager zurückgegeben wird.<br>
Hinweis: Es kann ein Los mit Rüstmenge nur ausgegeben werden, wenn diese Rüstmenge auch tatsächlich auf Lager ist. Siehe dazu Parameter: RUESTMENGE_DARF_FEHLMENGE_BUCHEN
5. Abbildung der sofortigen Wiederverwendung des Angusses<br>
Wird beim Rohmaterial der Anguss mit eingegeben und wird dieser aber sofort wieder vermalen und erneut dem Produktionsprozess zugeführt, so können Sie diese Wiederverwendung durch die Angabe einer negativen Menge pro Schuss definieren. Dies bewirkt, dass
    - das gesamte Material bei der Anlage des Loses aus dem Lager gebucht wird. Es wird auch davon ausgegangen, dass zu Beginnzeitpunkt des Loses das gesamte Material zur Verfügung steht.
    - das wiederverwendete Material des Angusses wird bei der vollständigen Erledigung des Loses vom Los ans Lager zurück gebucht. 

Zum weiteren Ablauf siehe bitte [Besonderheiten in der Kunststofffertigung]
( {{<relref "/fertigung/losverwaltung/#besonderheiten-in-der-kunststofffertigung" >}} ).

#### Bestimmte Artikel sollten immer früher oder später bestellt werden
In einigen Fertigungsbereichen ist es vorteilhaft, wenn verschiedene Artikel später als zum Fertigungslosbeginn und in anderen Bereichen, wenn diese früher als zum Losbeginn bestellt werden.

Um dies auf der einzelnen Stücklistenposition steuern zu können, gibt es den Eintrag ![](Beginnterminoffset.gif) Beginnterminoffset in Tagen auf der einzelnen Stücklistenposition.

Dies bewirkt, dass beim Anlegen eines Loses bei der Berechnung des Reservierungstermines dieser Versatz zum Losbeginntermin dazugezählt wird.

Dieses Feld steht daher auch im Reiter Material im Los zur Verfügung.

#### Sonderform für sofortige Bestellung
<a name="frühzeitige_Beschaffung"></a>
In manchen Unternehmen geht es vor allem darum, dass bestimmte, sehr schwer zu beschaffende Artikel idealerweise bereits zu Beginn der Produktentwicklung bestellt werden. Mit dem Parameter FRUEHZEITIGE_BESCHAFFUNG kann definiert werden, dass anstelle des Versatzes nur die Checkbox ![](Sofortige_Bestellung.gif) Sofortige Bestellung angehakt werden muss. Dies bewirkt, dass als Beginnterminoffset ein Eintrag von -999 Tagen gesetzt wird, was wiederum dazu führt, dass bei der Berechnung des nächsten Bestellvorschlages diese Stücklistenposition, wenn sie in einem aktuellen Los enthalten ist, mit einem Liefertermin von heute bestellt wird.

#### Wie lange kann eine Stückliste und damit das Fertigungslos aktualisiert werden?
**Kieselstein ERP** wurde für KMUs entwickelt. Eine der wesentlichsten Eigenschaften der KMUs ist, dass sie sehr flexibel sind. Das spiegelt sich daher auch in der Stücklistenverwaltung wieder. Darum ist es bei vielen **Kieselstein ERP** Anwendern so, dass die Stückliste erst stimmt, wenn die Anlage beim Kunden ist. D.h. solange bis das Fertigungslos tatsächlich vollständig erledigt ist, kann das Fertigungslos geändert werden und es gibt selbstverständlich die Möglichkeit das Los immer wieder anhand der Stückliste zu aktualisieren.

Einige Lösungen sprechen hierbei auch von wachsenden Stücklisten.

Wie beginnt man nun, wenn ein neuer Auftrag angelegt wurde und man erst mit der Konstruktion der Anlage beginnt, also keine wie immer gearteten Details einer Anlage, eines Gerätes, eines Produktes bekannt sind.

Gehen Sie hier ganz pragmatisch vor.
1. Erstellen Sie die Auftragsbestätigung an den Kunden. In dieser sind die vom Kunden bestellten Artikel enthalten. Es ist von Vorteil wenn diese bereits Stücklisten sind, dies kann aber auch nach der Erstellung und dem Versand der Auftragsbestätigung gemacht werden. 
2. Der Konstrukteur kann unmittelbar nach dem Versenden der Auftragsbestätigung (genauer nach dem diese im Status offen ist) mit seiner Konstruktionsarbeit beginnen und seine Zeit direkt auf den AUFTRAG buchen. 
3. Wenn die grobe Struktur der Stücklisten mit den Haupteilen, den lieferkritischen Teilen stimmt können Sie die (Basis-) Lose dafür anlegen. Wenn Sie diese Lose gleich ausgeben, können die Mitarbeiter darauf buchen, auch wenn noch keine wirklichen Inhalte, keine echten Solldaten gegeben sind. 
4. Nun wird die Stückliste vervollständigt und aktualisiert. Wenn eine Stückliste in **Kieselstein ERP** (teilweise) fertig ist, so kann diese entweder im Stücklistenmodul mit Bearbeiten, Lose aktualisieren oder im Los-Modul mit Bearbeiten anhand Stückliste bzw. anhand Arbeitsplan aktualisieren auf den aktuellen Stand gebracht werden. Üblicherweise werden hier die Materialkorrekturbuchungen mit dieser Aktualisierung durchgeführt. Es gibt jedoch auch eine Einstellung, dass nur die Solldaten aktualisiert werden.<br>
So ergänzen Sie während der Herstellung der Anlage laufend die Stücklisten und erhalten zum Schluss, faktisch gemeinsam mit der Fertigstellung der Anlage auch die fertige Stückliste. Denken Sie bei dieser Vorgehensweise auch daran, dass es auch die gegenteilige Denkweise, die gegenteilige Herangehensweise gibt und dass diese genauso von **Kieselstein ERP** unterstützt wird.

D.h. es werden nur die benötigten Lose anhand der Stücklisten angelegt. Diese beinhalten meist nur die allerwesentlichsten Komponenten. Der Mitarbeiter baut anhand von Montagezeichnungen die Anlage zusammen und bucht das vom Lager entnommene Material mittels [mobilem Barcodescanner]( {{<relref "/docs/installation/05_kes_app/" >}} ) direkt ins Los. Somit entspricht zum Fertigstellungszeitpunkt das Los dem tatsächlich entnommenen Material. Nun kann über die Zwischenablage der Soll oder Ist-Inhalt des Loses in die Stückliste übernommen werden.

### Verwendungsnachweis in der Stückliste
In den Kopfdaten der Stückliste gibt es den Button (rechts außen) ![](Stkl_goto_uebergeordnet.gif) "In übergeordnete Stückliste wechseln" der Klick auf den Button bewirkt:
- Wenn es keine übergeordnete Stückliste gibt, bei dem die Stückliste verwendet wird, wird eine Meldung ausgegeben.
- Wenn es genau eine Stückliste gibt, bei dem die Stückliste verwendet wird, dann wechselt man dorthin, ähnlich dem GoTo-Button.
- Wenn es mehrere Stücklisten gibt, erscheint vorher ein Auswahldialog mit dem in die gewünschte Stückliste gewechselt wird.

Somit können Sie schnell überprüfen in welchen Stücklisten die aktuell ausgewählte/bearbeitete Stückliste enthalten ist.

#### Protokollierung der Änderungen im Arbeitsplan und bei den Positionen
Unter Info, Änderungen sehen Sie die Änderungen, die in den Positionen und beim Arbeitsplan durchgeführt wurden. Diese Protokollierung ermöglicht eine wesentliche Zeitersparnis beim Such-/Prüfaufwand bei eventuellen Ungereimtheiten. Die Aufzeichnung der Änderungen ermöglicht das Nachvollziehen und schafft Transparenz über die Entwicklung der Stückliste.

#### Wie signalisiere ich der Produktion, dass die Stückliste für die Fertigung freigegeben ist?
<a name="Freigabe"></a>
Wenn die Zusatzfunktion Stücklistenfreigabe und das zugehörige Recht STK_FREIGABE_CUD gegeben ist, so kann die Stückliste mit der Information "Freigegeben" gekennzeichnet werden.<br>
Dazu klicken Sie auf den grünen Haken, nun erscheint die Information mit Datum und der Person, die die Freigabe durchgeführt hat.![](freigegeben.JPG)

Wenn eine Stückliste freigegeben ist, kann auf die Stückliste nur mehr lesend zugegriffen werden.<br>
Um eine Stückliste nochmals bearbeiten zu können, klicken Sie erneut in den Kopfdaten auf den grünen Haken um die Freigabe wieder zurückzunehmen.<br>
Personen, die das das Recht STK_FREIGABE_CUD nicht besitzen, können Freigaben nicht durchführen oder entfernen.<br>
Wenn eine Stückliste nicht freigegeben ist, können Lose nur nach Rückfrage angelegt werden (dies gilt auch für Lose aus Aufträgen).<br>
![](nicht_freigegeben.JPG)

Die Freigabe wird als grüner Haken in der Stücklistenauswahl bzw. in der internen Bestellung signalisiert. ![](freigabe_auswahl.JPG)

Weiters können die Ausdrucke der Stückliste (zum Beispiel Andruck des Datums und der Person, die die Stückliste freigegeben hat für Rückfragen) angepasst werden. Wenden Sie sich dazu gerne an Ihren **Kieselstein ERP** Betreuer.

Für die Verbindung mit der [Artikelfreigabe siehe dort]( {{<relref "/docs/stammdaten/artikel/freigabe/" >}} ).

#### Kann der Arbeitsplan trotz Freigabe der Stückliste verändert werden?
Es gibt auch das zusätzliche Benutzerrecht STK_SOLLZEITEN_FREIGEGEBENE_STK_CUD. Damit wird definiert, dass dieser Benutzer (dieser Systemrolle) den Arbeitsplan einer Stückliste ändern darf, obwohl diese Stückliste bereits freigegeben ist. Der Gedanke dahinter ist, dass man trotz freigegebener Materialien, die Fertigung optimiert und somit die Sollzeiten entsprechend anzupassen sind.

#### Auswahl der Artikel-Positionen der Stückliste nach den Kunden-Artikelnummern
Um einen Artikel nach der beim Kunden in den Sonderkonditionen hinterlegten kundenspezifischen Artikelnummern auszuwählen, so klicken Sie auf das Icon Kunde links neben der Artikelauswahl. 
![](kundenartikelnummer.JPG)<br>
Nun erhalten Sie eine Liste aller Artikel des im Los definierten Kunden. Wenn in den Kopfdaten des Loses kein Kunde definiert ist, so werden alle Artikel mit Kundenartikelnummern in der Auswahl angezeigt.
![](stueckliste_kundenartikelnummer.JPG)

#### Reichweite und Jahresbedarf
Im Modul Stückliste finden Sie das Journal Reichweite. 
![](reichweite_journal.PNG)

Gemeinsam mit der Artikelhitliste aus dem Modul Artikel - Journal - Hitliste, stellen Sie den Jahresbedarf der Artikel und deren Reichweite fest. 

Je nach Definition werden hier Lagermindest- und sollstand, die Stücklistendurchlaufszeit und die Wiederbeschaffungsmoral berücksichtigt. 

In der Hitliste ist die Jahresmenge die theoretische Jahresmenge aus den Artikel Bestelldaten mit Vergleich zu den verkauften Mengen. 

In der Reichweite werden Stücklisten vom Typ SetArtikel nicht berücksichtigt. 

Verwenden Sie die Journale zur Verhandlung mit Ihren Lieferanten und Kunden für die langfristige Unternehmensplanung und -steuerung.   

### Verhalten der Stücklisten bei mehreren Mandanten mit gemeinsamen Artikelstamm
Wird Ihr **Kieselstein ERP** in mehreren Mandanten eingesetzt und gleichzeitig für die / alle Mandanten ein gemeinsamer Artikelstamm verwendet, so gilt die Regel, dass eine Stückliste NUR in einem Mandanten definiert sein kann. Damit kombiniert ist, dass die Stücklistengesamtkalkulation über ALLE Mandanten erfolgen kann, was natürlich von den entsprechenden Rechten bei den anderen Mandanten abhängig ist. Zusätzlich wirkt der GoTo Button auf die untergeordnete Stückliste so, dass gegebenenfalls in den anderen Mandanten gewechselt wird. Dazu muss ein **Kieselstein ERP** Client für den anderen Mandanten geöffnet sein.
Zusätzlich steht beim Verwendungsnachweis des Artikels die Auswertung des Einsatzes des Artikels über alle Mandanten zur Verfügung.

#### Verlagern der Produktion von einem zum anderen Mandanten
Es kommt immer wieder vor, dass eine Stückliste im einen Mandanten definiert wird und dann z.B. die Serienfertigung in einem anderen Mandanten / Werk durchgeführt wird. Mit dem zentralen Artikelstamm kann daher die Stückliste von einem in den andern Mandanten verlagert werden ![](Stkl_Mandant1.gif), unter der Voraussetzung, dass Sie entsprechende Rechte (STK_STUECKLISTE_CUD) im jeweils anderen Mandanten haben. Bitte beachten Sie, dass bei der Verlagerung einige Daten nicht übernommen werden können und daher auf den Standardwert zurückgesetzt werden. Folgende Daten werden verändert:
  * Aus den Kopfdaten wird der Kunde gelöscht
  * Aus den Kopfdaten wird die Scriptart gelöscht
  * Als Ziellager wird das Hauptlager des Ziel-Mandanten verwendet
  * Die Fertigungsgruppe wird auf den Standardwert des Zielmandanten gesetzt.
  * Im Arbeitsplan werden die Maschinen gelöscht, da diese an den neuen Mandanten angepasst werden müssen.
  * In den Materialpositionen wird versucht die Montageart anhand der Bezeichnungen zu übertragen. Gelingt dies nicht, wird die standard Montageart des Ziel-Mandanten verwendet.

Bitte beachten Sie, dass aufgrund obiger, leider erforderlicher Veränderungen, ein z.B. testweises verlagern in den anderen Mandanten und mit anschließendem Zurückwechseln unter Umständen für Sie wichtige Daten verliert.<br>
Bitte beachten Sie dazu auch die Möglichkeit die Stückliste eines anderen Mandanten für die Fertigung zu verwenden.

#### Beim Verlagern der Stückliste kommt ein Fehler vom Server
Klickt man auf Details so wird in der Titelzeile Fehlercode 131 angezeigt. Das bedeutet, dass im Zielmandanten KEINE Stückliste angelegt werden kann, da Basiseinstellungen für die Stückliste fehlen. Unser Vorschlag zur Behebung: Öffnen Sie das Modul Stücklisten gleichzeitige in beiden Mandanten und vergleichen Sie die Grunddaten. Wesentliche Punkte sind die Montageart(en) und die Fertigungsgruppe(n). 

#### Fertigungsgruppe, wozu ist das und wo definiert man das?
<a name="Fertigungsgruppe"></a>
Die Fertigungsgruppen dienen der Zuordnung von Bereichen zu verschiedenen Mitarbeiter-Fertigungsgruppen.<br>
In der Regel ist es in kleinen und mittleren Fertigungsbetrieben so, dass bestimmte Mitarbeitergruppen immer gleiche / ähnliche Produkte / Stücklisten fertigen. Um nun die Steuerung für diese Gruppen zu vereinfachen wurden die Fertigungsgruppen geschaffen. D.h. es stehen in verschiedenen Journalen und Report aber auch z.B. in der Losauswahlliste die Filtermöglichkeit für eine Fertigungsgruppe zur Verfügung. Zusätzlich können [Benutzerrechte]
( {{<relref "/docs/stammdaten/benutzer/#einschr%c3%a4nkung-auf-fertigungsgruppen" >}} ) auf eine/mehrere Fertigungsgruppen eingeschränkt werden. Was wiederum bedeutet, dass in der Fertigung der jeweilige Mitarbeiter nur die Lose seiner Fertigungsgruppen sieht.

Die Fertigungsgruppen werden im Modul Stücklisten, unterer Reiter Grunddaten, oberer Modulreiter Fertigungsgruppe definiert.

Zur weiteren Verwendung der Fertigungsgruppen siehe auch Papierlose Fertigung.

#### Formularnummer in den Fertigungsgruppen, wozu ist das?
Je nach Aufgaben der unterschiedlichen Fertigungsgruppen ist es oft hilfreich, wenn die Fertigungsbegleitscheine der jeweiligen Fertigungsgruppe unterschiedliche gestaltet sind. Dafür sind die Formularnummern, die keinen Zusammenhang mit der Sortierung der Fertigungsgruppe haben, gedacht. Bitte beachten Sie bei der Anlage neuer Fertigungsgruppen, dass nur vorhandene Formulare verwendet werden können. Sollten Sie ein neues Formular benötigen, so bitten Sie Ihren **Kieselstein ERP** Betreuer dieses für Sie entsprechend Ihren Wünschen einzurichten. Wenn ein vorhandenes Formular verwendet werden sollte, so geben Sie bitte dessen Nummer an.
In der Regel werden KEINE Formularnummern hinterlegt und somit das Standard Formular verwendet.

#### Minderverfügbarkeit?
<a name="Minderverfügbarkeit"></a>
Es gibt in der Produktion von komplexen Geräten / Baugruppen / Maschinen immer wieder die Situation, dass man für die Materialplanung gerne wissen möchte ob eine bestimmte Konstellation von fertigen Geräten jetzt/heute produziert werden kann. In der Regel ist dies immer eine Art Wunschmenge an fertigen Produkten, welche man idealerweise in einer Stückliste zusammenstellt.
D.h. z.B. um die Produktionsmöglichkeit für eine bestimmte Produktgruppe zu prüfen, tragen Sie die gewünschten Mengen in die Stückliste ein und wählen dann Info, Minderverfügbarkeit.<br>
![](Material_Minderverfuegbarkeit.gif)

In dieser Auswertung werden die angegebenen Stücklisten von oben nach unten aufgelöst und bei den Stücklisten der lagernden (Unter-) Baugruppen die Bedarfe um die lagernde Stücklistenmenge reduziert. Somit ergibt sich der tatsächliche Bedarf für den jeweiligen Einzelartikel. Bitte beachten Sie, dass die Einzelartikel zu guter Letzt verdichtet dargestellt werden. D.h. Artikel die keinen Bedarf haben, also aus dem Lager bedient werden, werden in der Liste nicht angeführt.
Bitte beachten Sie auch, dass auf der Liste zwar die Lagerstände angeführt sind, es jedoch trotzdem den entsprechend angedruckten Bedarf gibt.<br>
Zusätzlich sind in diesem Ausdruck auch die offenen Bestellpositionen angeführt, um leichter entscheiden zu können, ob nun die Produktion gestartet wird oder nicht.<br>
Hakt man in Fertigung berücksichtigen an, so wird die offene Losmenge (Losgröße minus bereits abgeliefert) als quasi lagernd bei der Stückliste mit berücksichtigt, wodurch der Bedarf der Komponenten entsprechend sinkt.

#### Ursprungserklärung / Langezeit Lieferantenerklärung?
Die Frage für uns(er) Produzenten lautet, gerade wenn exportiert wird, wie kann man auf einfache Weise erkennen, dass die Ursprungsbestimmungen eingehalten werden.
Davon ausgehend, dass alle verwendeten Artikel eines Produktes / Gerätes in den Stücklisten abgebildet sind, kann dies sehr einfach aus den Stücklisten herausgelesen werden. Dafür haben wir den Druck der Stückliste mit Preis eingerichtet. In der üblichen Formulardefinition wird das Ursprungsland mit der Kostenverteilung dann angedruckt, wenn für die Stückliste kein Kunde definiert ist.
Wählen Sie daher die Kopfstückliste Ihres Gerätes, dann<br>
![](Ursprungserklaerung1.gif) Stückliste, Drucken und dann<br>
![](Ursprungserklaerung2.gif)<br>
allgemein mit Preis und um alle Artikel Ihres Produktes zu sehen, Unterstücklisten einbinden.

Welchen Preis Sie als Basis für die Berechnung verwenden bleibt Ihnen überlassen. Achten Sie auf jeden Fall auf die Ursprungsvorschriften.

WICHTIG: Für diese Betrachtung kommt das Ursprungsland aus dem jeweiligen Artikel und geht natürlich davon aus, dass die Länder der europäischen Union richtig definiert sind.
![](Ursprungserklaerung3.jpg)<br>
Die Verteilung der Artikel und deren Ursprungsländer wird in der Gesamtsumme mit ausgegeben.

#### Stücklisten Darstellung in Baumstruktur
Manchmal hilft es, wenn man die Baumstruktur verschiedener Stücklisten direkt sieht.
Dafür wurde das Fenster Baumansicht ![](Baumsicht.gif) in der Stücklistenauswahl geschaffen.<br>
Hier sehen Sie nach dem ersten Klick alle Kopfstücklisten. Also die Stücklisten die selbst in keiner anderen Stückliste enthalten sind.

Hinweis: Sollten Sie die erwartete Stückliste in dieser Darstellung nicht finden, so wechseln Sie bitte in den Artikelstamm und wählen den Stücklistenartikel aus. Nun zeigen Sie mit Info, Verwendungsnachweis die Stücklisten an, in denen der Artikel enthalten ist. Damit ist auch der Grund gegeben, warum die von Ihnen gesuchte Stückliste keine Kopfstückliste ist.

## Einmalstückliste
Gerade in den Elektronik Entwicklungsfirmen geht es um den Prototypenbau. D.h. ja, man braucht eine Stückliste, aber ob diese 10Stück Prototypen dann so gefertigt werden, weiß kein Mensch. Aber man sollte doch eine gewisse Lagerbewirtschaftung und ähnliches zur Verfügung haben.

Daher haben wir, den in der Losverwaltung schon länger verfügbaren Einmalartikel erweitert und auch die Möglichkeit einer Einmalstückliste geschaffen.

### Anlegen einer Einmalstückliste
Um eine Einmalstückliste anzulegen, geht man im Stücklistenmodul auf neu und klickt auf den Artikelauswahl-Button. Hier steht (wie aus der Materialliste des Loses heraus) auch der Nagel für die Anlage eines Einmalartikels zur Verfügung. Hier nun die Daten der Einmalstückliste einpflegen. Mit dem Speichern der Stückliste wird der (kurz dazwischen) angelegte Einmalartikel automatisch eine Einmalstückliste.

Eine ähnliche Vorgehensweise gibt es für die Umwandelung in den Stücklistenpositionen. D.h. es werden die Positionen wie üblich importiert. Unbekannt, als Handartikel gekennzeichnet usw.. Nun in der Stückliste die in Einmalartikel umzuwandelnden Handartikel markieren (multiselekt) und mit dem Nagel diese Positionen in Einmalartikel umwandeln.

Zur weiteren Erklärung des Einmalartikels [siehe]( {{<relref "/fertigung/losverwaltung/#einmalartikel" >}} )

## PPM Menge
Die PPM Menge dient der Erfassung der Fehler pro verbautem Artikel.<br>
Dafür muss die Zusatzfunktionsberechtigung PRUEFPLAN1 aktiviert sein.

Z.B. werden in einem Arbeitsgang fünf gleiche Teile (z.B. Schrauben) montiert / verbaut. Diese Definition wird im Arbeitsgang gemeinsam mit den Rüst- und Stückzeiten erfasst.<br>
![](PPM_Menge_erfassen.png)

Die PPM kann mit dem Stücklisten Arbeitsplanimport importiert werden.

Nun geht es, vor allem aus Qualitätssicherungsgründen darum, dass man über einen Zeitraum wissen muss, wie das Verhältnis der guten und schlechten Verarbeitung (der montierten Schrauben) ist. Dieses Verhältnis wird in der Regel in PPM (Parts per Million) angegeben.

Ein anderes Beispiel aus der Elektrotechnik.<br>
In einen Stecker müssen Litzen eingesteckt werden. Nun ist dieses verdrahten des Steckers ein Arbeitsgang in dem z.B. 5Litzen verarbeitet werden. Also ist die Basis 5 PPM_Mengen = 5 x den Kontakt auf die Lize aufbringen und 5 x richtig in das Steckergehäuse einstecken.