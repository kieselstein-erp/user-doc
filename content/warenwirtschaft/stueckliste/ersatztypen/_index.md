---
categories: ["Ersatztypen"]
tags: ["Ersatztypen", "Stückliste"]
title: "Ersatztypen in der Stücklistenverwaltung"
linkTitle: "Ersatztypen"
weight: 300
date: 2023-01-31
description: >
  Wozu Ersatztypen
---
Ersatztypen in Stücklisten
==========================

Gerade für die Elektronikbranche steht auch die Ersatztypenverwaltung auf der einzelnen Stücklistenposition zur Verfügung. Ziel ist, dass je Stücklistenposition diejenigen Artikel angegeben werden können, welche anstelle des original Bauteiles verwendet werden können.

Um die Ersatztypen einer Position zu definieren, wählen Sie in den Stücklisten im oberen Modulreiter Positionen diejenige Stücklistenposition aus, für die Sie die Ersatztypen definieren wollen und wechseln Sie dann in den Reiter Ersatz ![](Ersatztypen1.gif).

Geben Sie hier nun diejenigen Artikel an, die anstelle des unter Positionen angegebenen Originaltyps verwendet werden können.

Bei den Ausdrucken der Stückliste werden die Ersatztypen jeweils unter den Originalpositionen angeführt.

Wird beim Ausdruck die Stückliste nach Artikel verdichtet, so werden die Ersatztypen nicht berücksichtigt.

Sind hinter einer original Stücklistenposition Ersatztypen hinterlegt, so wird die Zeile in grün dargestellt.

Wird aus einer Stückliste mit Ersatztypen ein Los angelegt, so werden diese in der Form übernommen, dass zuerst die Originaltypen/positionen mit der entsprechenden Sollmenge eingetragen ist und danach in grün die dazu gehörigen Ersatztypen aufgelistet werden.

Wenn nun anstelle der Originaltypen die Ersatztypen verwendet werden sollen, so ändern Sie bitte die entsprechenden Sollmengen der jeweiligen (Ersatz) Positionen ab. Bitte achten Sie darauf, dass der Gesamtbedarf / die Gesamtmenge als Gesamtes den richtigen Wert beibehält. Wir haben bewusst auf Verriegelungen verzichtet, um Ihnen alle Möglichkeiten zur Materialsteuerung zur Verfügung zu stellen.

Durch die Änderungen der Sollmengen werden die Reservierungen / Fehlmengen des Loses entsprechend mitgeändert. Dadurch ändern sich auch die im Bestellvorschlag angeführten Mengen.

Da die Ersatztypen auch in der theoretischen Fehlmenge mit angeführt wird, haben Sie sehr schnell eine Übersicht, welche Bauteile Sie anstelle der Originaltypen einsetzen könnten um das Los unter Verwendung der Ersatztypen zu fertigen.

**Hinweis:**

In der Fertigung wird die Materialliste in den Losen nicht wie üblich nach Artikelnummer sortiert dargestellt, sondern nach der Sortierung Stückliste zum Zeitpunkt der Übernahme in das Los um den entsprechenden Zusammenhang der Ersatztypen darstellen zu können.

Ersatztypen im Artikel

Zusätzlich können im Artikel allgemeine Ersatztypen für den jeweiligen Haupttypen definiert werden. Damit ist es möglich sogenannte sächliche Artikel und deren freigegebenen herstellerspezifische Artikel zu definieren. Bitte beachten Sie, dass die je Artikel definierten Ersatztypen automatisch in die Los-Material-Position als mögliche Ersatztypen übernommen werden.

Komfortable Erfassungsfunktion für Stücklisten mit Ersatztypen

Wenn Sie die Zusatzfunktion Ersatztypen auf Stücklistenposition besitzen, so steht in der Stückliste, im Reiter Material neben der Artikelauswahl auch die Auswahl mit gleichzeitiger Definitiion der Ersatztypen zur Verfügung. D.h. durch Klick auf ![](Ersatztypen2.gif)Artikel mit Ersatztypendefinition erscheint eine Artikelauswahlliste der sächlichen Artikel, also derjenigen Artikel bei denen Ersatztypen definiert sind.
Hinweis: Artikel die keine Ersatztypen haben scheinen in dieser Liste auch nicht auf.
Wählen Sie hier nun den sächlichen Artikel aus.
![](Ersatztypen3.gif)

Nach der Auswahl des sächlichen Artikels erscheint ein Dialog in dem die Ersatztypen für diese Stücklistenposition definiert werden können.
![](Ersatztypen4.jpg)

Wird der sächliche Artikel oberhalb der Linie ausgewählt, bedeutet dies, dass für diese Stücklisten Position keine spezifische Definition vorgenommen wird und dass immer alle definierten Ersatztypen auf dieser Stücklistenposition für die Fertigung / für die Bestellung verwendet werden dürfen.

Wird hingegen unter der Linie ein Artikel mit dem linken Radiobutton ![](Ersatztypen5.gif) ausgewählt, so bedeutet dies, dass dies der bevorzugte Artikel für diese Stücklistenposition ist. mit den Checkboxen können die möglichen erlaubten Ersatztypen für diese Stücklistenposition zusätzlich definiert werden.

So könnte eine spezifische Definition wie folgt aussehen:
![](Ersatztypen6.jpg)
Was bedeutet, dass entgegen obiger Darstellung KEM als bevorzugter Artikel in die Stücklistenposition eingetragen wird und dass AVX und SAM als mögliche Ersatztypen für diese Stücklistenposition verwendet werden könnten.
Bitte beachten Sie, dass zum komfortablen Ändern auch auf ![](Ersatztypen2.gif)geklickt werden sollte.

Nach der Auswahl der gewünschten Artikel klicken Sie bitte auf Ok und nehmen die weiteren Definitionen der Stücklistenposition vor.
Danach speichern Sie bitte die erfassten Daten.

#### Wie werden Ersatztypen in das Los übernommen?
<a name="Ersatztypen Übernahme"></a>
Bitte beachten Sie dazu den Parameter ERSATZTYPEN_SOLLMENGE

| Wert | Bedeutung |
| --- |  --- |
| 0 | Die gesamte Menge wird als Sollmenge dem Originalartikel zugewiesen |
| 1 | billigster Lief1Preis bekommt gesamte Sollmenge |
| 2 | Sollmenge wird anhand der Lagermengen der Ersatzartikel verteilt. D.h. es werden zuerst die Sollmengen der Ersatztypen anhand deren Lagerstände bedient und diese verteilten Sollmengen dann von der Sollmenge der Originalartikel abgezogen. Der Original-Artikel wird nur verwendet, wenn von den Ersatzartikel zu wenig auf Lager ist. |

<a name="Ersatztypen Übernahme0"></a>

#### Können die Ersatztypen aus den Artikeln in den Stücklisten angedruckt werden?
Um in den verschiedenen Formularen die Ersatztypen die über den Artikelstamm definiert sind andrucken zu können steht ein entsprechender Helper zur Verfügung. Dieser ist beispielhaft im Druck der Stücklisten umgesetzt.