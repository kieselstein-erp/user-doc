---
title: "Dokumentation Kieselstein ERP"
linkTitle: "Dokumentation"
menu:
  main:
weight: 1
description: >
 Willkommen bei der Kieselstein ERP eG
---
Diese Dokumentation zum Open Source **Kieselstein ERP** sollte dir helfen, dein(e) Unternehmen, mit dem **Kieselstein ERP** optimal zum Erfolg zu steuern.

Viele Dinge sind allgemein gültig und somit nicht unbedingt an ein bestimmtes ERP gebunden. Der besondere Vorteil des **Kieselstein ERP** ist, dass es mit seinen integrierten Abläufen, gerade kleine und kleine mittelständische Unternehmen dabei unterstützt, die Unternehmensprozesse gut abzubilden. Andere Systeme sind oft (nur) Frameworks, welche durch entsprechende Consulting-Leistung an die Bedürfnisse des Unternehmens angepasst werden. Auch gehen diese Systeme davon aus, dass die Unternehmer:innen wissen, was alles für die Unternehmensführung erforderlich ist. **Kieselstein ERP** kommt mit einem großen Umfang an Funktionen, die durch entsprechende Parametrierung aktiviert werden. D.h. das immer wieder Neu-Erfinden von Funktionen / Abläufen ist damit deutlich reduziert.

Ein weiterer wesentlicher Punkt ist, dass das **Kieselstein ERP** von den Erfordernissen der Produktion, der Herstellung von Waren/Produkten, der Erbringung von Dienstleistungen aus geht und damit an die tatsächlichen Abläufe dieser Art von Unternehmen angepasst ist. Ein sehr großer Teil der üblichen Systeme kommen aus der Bankenwelt oder aus der Finanzbuchhaltung. So ist es für **Kieselstein ERP** selbstverständlich, dass Stücklisten mit einer beliebigen Anzahl von Hierarchieebenen umgehen können, dass die Bewirtschaftung von Mensch- und Maschinenzeit ein integraler Bestandteil des Systems ist. Aber natürlich gibt es auch so Dinge wie integrierte Finanzbuchhaltung, Liquiditätsplanung und viele andere mehr.

[Reinschauen](https://gitlab.com/kieselstein-erp), herunterladen, nutzen.

Mit **Kieselstein ERP** bekommst du das Open Source ERP System mit einem großen Umfang, der für die typischen KMU, kleinen und mittelständischen Unternehmen erforderlich ist und das völlig kostenlos.

Warum **Kieselstein ERP**?<br>
Nachdem doch einige Menschen danach gefragt haben:<br>
Wirft man einen Kieselstein ins Wasser, so schlägt er kreisförmige Wellen die sich immer weiter ausbreiten. So wünschen wir dem **Kieselstein ERP** dass es sich immer weiter ausbreitet und für viele Menschen und Unternehmen Nutzen stiftet.

Warum **Open Source**?<br>
Nur Quelle offene Produkte, ob das jetzt Konstruktionszeichnungen für LKW's sind oder der Quellcode der Software, ermöglichen den Menschen die Hilfe zur Selbsthilfe und machen sie damit unabhängig und hoffentlich auch ein kleines Stückchen freier.