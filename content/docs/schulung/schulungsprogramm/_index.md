---
title: "Schulungsprogramm"
linkTitle: "Programm"
weight: 40
description: >
  Das Schulungsprogramm zum Kieselstein-ERP System
---
Es ist vor allem dafür gedacht, vorab die gewünschten Schulungspunkte bzw. Module festzulegen und ein Gefühl für den Zeitaufwand zu bekommen.

Schulungskonzept **Kieselstein ERP**
=========================

Mit diesem Schulungskonzept möchten wir dir ein Werkzeug zur Vorgehensweise für die Basis-Schulung deines **Kieselstein ERP** Systems geben. Auch hier sei betont, dass wir von entsprechenden Kenntnissen in der Bedienung von derartigen Programmen ausgehen. Eine Erklärung, wie man eine Maus bedient, was Shortcuts (oder auch Schnellzugriffstasten) sind und wie / wozu sie zu verwenden sind, ist nicht Aufgabe von **Kieselstein ERP**. Gegebenenfalls ist ein "Computer" Grundkurs, oder dein IT-Betreuer dafür die richtige Anlaufstelle.

Definiere einen Power-User der zugleich der Projektverantwortliche für die Umsetzung / Einführung von **Kieselstein ERP** in deinem Unternehmen ist. Die Aufgabe dieser Person ist, das Projekt **Einführung und Umsetzung** **Kieselstein ERP** massiv vorwärtszutreiben. Zugleich ist er die Schnittstelle zu uns.

Es ist enorm wichtig, dass diese Person die Unterstützung durch das Management / die Geschäftsleitung hat. Es kann dies, nicht zuletzt aus Gründen des zeitlichen Aufwands, nicht die Geschäftsleitung sein.

Weiters muss der Projektverantwortliche über alle in deinem/euren Unternehmen eingesetzten **Kieselstein ERP** Module bestens informiert sein, die gesamten (Unternehmens-) Prozesse, die dahinterstecken, verstanden haben und darüber bestens informiert sein. D.h. er/sie ist auch in der Lage die Zusammenhänge zu verstehen. Und er/sie ist befähigt, die sich daraus ergebenden organisatorischen Änderungen umzusetzen / einzufordern.

Zugleich ist der Einführungsverantwortliche die Schnittstelle zu uns. D.h. alle Umsetzungen werden unternehmensintern vom Projektleiter in euren Unternehmen koordiniert und dann in abgestimmter Form an uns herangetragen.

Bitte beachten Sie dabei auch die Grundregeln des Projektmanagements. Sehr bewährt hat sich die Arbeitsmethode in der jeder Punkt nach

**A**ufforderung, **B**eschluss, **E**mpfehlung, **F**eststellung

qualifiziert wird. Ich kann dazu auch das Buch: "IT-Projekt-Management kompakt" (Pascal Mangold, Springer Verlag) sehr empfehlen.

Modulschulungen

| Anwender: |  | Benutzer: |  |
| --- |  --- |  --- |  --- |

| Bereich | Modul | Unterpunkte | Erforderlich für Anwender<br>für Benutzer | Vorkenntnisse1-3<br>keine / mittel / sehr gute | Durchgeführt<br>Intensität (1-3) |
| --- |  --- |  --- |  --- |  --- |  --- |
| Verkauf | Kunden | Anlage, Ändern,  Konditionen Finden Rechnungsadresse Ansprechpartner Kurzbrief Selektionen |  |  |  |
|  | Angebot |  |  |  |  |
|  | Angebotsstücklisten   Verkauf |  |  |  |  |
|  | Einkaufs-Angebotsstücklisten |  |  |  |  |
|  | Auftrag | Anlegen, Positionen einfügen, Aktivieren / Drucken |  |  |  |
|  | Lieferschein | Auftragsübernahme, Anlegen, Positionen einfügen, Aktivieren / Drucken |  |  |  |
|  | Rechnung | Lieferschein Übernahme, Sammelrechnung, Anlegen, Positionen einfügen, Aktivieren / Drucken |  |  |  |
| Warenwirtschaft | Artikel | Anlegen Chargen- / Seriennummern / Lagerbewirtschaftet<br>Definition Verkaufspreise<br>Definition Lieferanten<br>Handlagerzubuchung |  |  |  |
|  | Stücklisten | Anlegen, Kopfdaten, Positionen, Arbeitsplan, Grunddaten, Gesamtkalkulation, Einzeldrucke |  |  |  |
| Einkauf | Lieferanten | Anlage, Ändern, Konditionen, Finden, Rechnungsadresse, Ansprechpartner, Kurzbrief, Beurteilung |  |  |  |
|  | Anfrage | Anlegen, Kopfdaten, Positionen, Lieferdaten<br>Gruppenanfragen erstellen   umwandeln / versenden |  |  |  |
|  | Bestellung | Anlegen, Kopfdaten, Positionen<br> Liefertermine Rückpflegen, Terminbestätigungen, Wareneingang, Wareneingangspositionen,  Eingangsrechnung erzeugen |  |  |  |
|  | Eingangsrechnung | Anlegen, Kontierung, Splittbuchung, Kundenauftrag zuordnen |  |  |  |
| Fertigung | Los | Anlegen, manuell, anhand Auftrag, Aus Auftrag<br>Ausgeben -> In Produktion, Fehlmengen / Reservierungen, Abliefern, Sollmengen, Ändern, Ersatztypen, Nachträgliche Materialentnahme, Zeitbuchungen, Sollzeiten / Istzeiten, Gut-/Schlecht-Stück, Wertefluss, Zusammenhang Stücklisten / Lose |  |  |  |
|  | Reklamation |  |  |  |  |
|  | Zeiterfassung |  |  |  |  |
|  | Personal |  |  |  |  |
| Management | Finanzbuchhaltung |  |  |  |  |
| Allgemein | Partner | Zusammenhang   Kunden / Lieferanten   / PartnerAnsprechpartner ändernSerienbrief |  |  |  |
|  | Benutzer | Benutzer anlegen, Systemrollen definieren, Benutzer Mandant |  |  |  |
|  | System | Lieferarten, Zahlungsziel, Spedition erweitern |  |  |  |

Intensität (1-3), 1 ... angesprochen, 3 ... Intensiv im Detail durchgearbeitet

## Punkte / Themen / Überschriften:
### Allgemein:
1.  Anmeldung am Client
    - Mandantenwechsel
2.  Wo finde ich welche Module
3.  Obere / untere Modulreiter
4.  Bedienung der Direktfilter und der Auswahlspalten
    - Umreihen, Sortieren, Definition speichern
5.  Bedienung Zusatzfilter
6.  Stammdaten
7.  Bewegungsdaten
8.  Die Verknüpfung von Stamm- und Bewegungsdaten

Nach diesen einführenden und allgemein gültigen Punkten sei nun auf die Besonderheiten der unterschiedlichen Branchen hingewiesen.

### Dienstleistung
1.  Auftrag
2.  Buchen von Zeiten auf einen Auftrag
3.  Nachkalkulation / Zeitaufwand
4.  Verrechnung eines Auftrags

### Warenwirtschaft
#### Produktion
Allgemein II:
1.  Mahnwesen Ausgangsrechnungen
2.  Zahlungsbuchungen
3.  Überleitung in die Finanzbuchhaltung

### Finanzbuchhaltung:
1.  Voraussetzungen:
    - Gute Kenntnisse über die Bedienung von **Kieselstein ERP**
    - Gute Buchhaltungskenntnisse
    - Aktives Vorgehen
    - Kontenrahmen eingespielt und Steuerkategorien der Finanzämter eingerichtet
2.  Dauer der Fibu-Grundschulung ca. 4 Std.
3. Inhalte:
    - Definition Sachkonten
    - Erklärung Steuerkategorien und deren Definition
    - Umsatzsteuervoranmeldung -> wie kommen die Werte in das Formular
    - Erlöskontenfindung, Artikel->Artikelgruppe->Erlöskonto->Länder(art)-Übersetzung
    - Kontierung Eingangsrechnung, Splittbuchung

      Wirkung von Land, EU-Mitglied von bis und gemeinsames Postversendungsland

## Voraussetzungen:
-   **Kieselstein ERP** ist in Ihrem Unternehmen ordnungsgemäß installiert
-   Der **Kieselstein ERP** Client ist auf Ihrem Rechner installiert
-   Benutzername, Passwort und Default-Mandant sind definiert und bekannt.

**![](Erste_Schritte.png) Lernschritte**

1.  Machen Sie sich mit der [grundsätzlichen Bedienung]( {{<relref "/start/01_grundsätzliche_bedienung" >}} ) von **Kieselstein ERP** vertraut.

2.  Danach machen Sie sich mit den für Sie relevanten Stammdaten vertraut, z.B. [Kunde]( {{<relref "/docs/stammdaten/kunden" >}} ), [Personal]( {{<relref "/docs/stammdaten/personal" >}} ) oder [Artikel]( {{<relref "/docs/stammdaten/artikel" >}} ).
    Beachten Sie bitte den zentralen [Partnerstamm]( {{<relref "/docs/stammdaten/partner" >}} ).
    Beachten Sie die Arbeitsweise mit den [Direktfiltern]( {{<relref "/start/02_shortcuts/#direkt-filter" >}} ) und den [Spaltenüberschriften]( {{<relref "/start/02_shortcuts/#k%c3%b6nnen-auswahllisten-sortiert-werden" >}} ).

3.  Nun verwende die für dich/euch relevanten Bewegungsmodule
> - Verkauf -> [Angebote]( {{<relref "/verkauf/angebot" >}} )
> - Einkauf -> [Bestellungen]( {{<relref "/einkauf/bestellung" >}} )
> - Fertigung -> [Stücklisten]( {{<relref "/warenwirtschaft/stueckliste" >}} ) / [Fertigungsaufträge]( {{<relref "/fertigung" >}} )

4.  Drucke einen der Bewegungsdatensätze und beachte die Möglichkeiten beim [Drucken]( {{<relref "/docs/stammdaten/system/drucken" >}} )

5.  Zur leichteren Orientierung haben wir die Module in der Toolbar nach Themengruppen sortiert.
    Nachfolgend die Auflistung aller in **Kieselstein ERP** verfügbaren Module in der auch am Client verwendeten Gruppierung.
> - Warenwirtschaft
> > - [Artikel]( {{<relref "/docs/stammdaten/artikel" >}} )
> > - [Stücklisten]( {{<relref "/warenwirtschaft/stueckliste" >}} )
> -   Einkauf
> > -   [Lieferanten]( {{<relref "/docs/stammdaten/lieferanten" >}} )
> > -   [Anfrage]( {{<relref "/einkauf/anfrage" >}} )
> > -   [Bestellungen]( {{<relref "/einkauf/bestellung" >}} )
> > -   [Eingangsrechnungen]( {{<relref "/einkauf/eingangsrechnung" >}} )
> -   Fertigung
> > -   [Fertigungslose]( {{<relref "/fertigung" >}} )
> > - Küchenmodul
> > -   [Reklamation]( {{<relref "/fertigung/reklamation" >}} )
> > - Instandhaltung
> > -   [Zeiteingabe]( {{<relref "/fertigung/zeiterfassung" >}} )
> > -   [Personal]( {{<relref "/docs/stammdaten/personal" >}} )
> -   Verkauf
> > -   [Kunden]( {{<relref "/docs/stammdaten/kunden" >}} )
> > -   [Projektverwaltung / ToDo Liste, Projektklammer]( {{<relref "/verkauf/projekt" >}} )
> > -   [Angebot]( {{<relref "/verkauf/angebot" >}} )
> > -   [Angebotsstücklisten]( {{<relref "/verkauf/angebotsstueckliste" >}} ), Verkauf und Einkauf
> > - Inseratenverwaltung
> > - Forecastplanung
> > -   [Aufträge, mit Rahmen und Abrufen]( {{<relref "/verkauf/auftrag" >}} )
> > -   [Lieferscheine]( {{<relref "/verkauf/lieferschein" >}} )
> > -   [Rechnungen]( {{<relref "/verkauf/rechnung" >}} ) mit [Gutschriften]( {{<relref "/verkauf/rechnung/gutschrift" >}} )
> -   Management
> > -   [Zutritt]( {{<relref "/extras/Zutritt" >}} )
> > -   [Finanzbuchhaltung / Kontendefinition / Export]( {{<relref "/management/finanzbuchhaltung" >}} )
> > - Dashboard
> > - 360° Kundensicht
> -   Extras
> > -   [Partner, zentraler Adressbestand]( {{<relref "/docs/stammdaten/partner" >}} )
> > -   [Benutzer, Rollen und Rechte]( {{<relref "/docs/stammdaten/benutzer" >}} )
> > -   [System]( {{<relref "/docs/stammdaten/system" >}} ), von der Orts-Definition bis zu den Textmodulen
> > - EMail Client (preliminary)
> > - Nachrichtensystem
> > - Geodaten Anzeige

Hinweis: Da die zur Verfügung stehenden Module von den freigeschalteten Modulen und den Rollenrechten abhängig ist, müssen nicht alle angeführten Module auch in Eurer Installation aufscheinen. Wenn weitere Module benötigt werden, bitte an den **Kieselstein ERP** Betreuer wenden. 

6.  Warum sind manche Bewegungsdatensätze änderbar und manche nicht mehr.
    Der generelle Gedankengang ist, dass ein Bewegungsdatensatz so lange änderbar ist, bis es einen Nachfolger gibt. Weitere Details entnimm bitte den bei den jeweiligen Modulen angeführten Statusdiagrammen.

7.  Der/Euer Administrator sollte die Pflege deines/eures **Kieselstein ERP** Systems übernehmen. [Siehe dazu auch.]( {{<relref "/docs/installation/08_nach_der_installation" >}} )


![](screenshot.png)  