---
title: "Schulungsprogramm"
linkTitle: "detailliert"
weight: 41
description: >
  Die Programmpunkte für die Schulung zum Kieselstein-ERP System
---
Ist vor allem auch gedacht um vorab, die gewünschten zu schulenden Punkte / Module festzulegen und ein Zeitgefühl dafür zu bekommen.

Eine Übersicht über die Schulungen und Schulungsinhalte zu **Kieselstein ERP**

Grundsätzlich gliedert sich die Schulung in Basisschulungen und Expertenschulungen.

Die Basisschulungen umfassen einen fixen Rahmen mit fixen Inhalten.

Die Expertenschulungen gehen tief in die Möglichkeiten von **Kieselstein ERP** und sind daher immer auch Schulungen, die speziell auf die aktuellen Bedürfnisse des jeweiligen **Kieselstein ERP** Anwenders abgestimmt sind.

Beide Arten verlangen Vorbereitungsarbeit, um diese so effizient wie möglich durchzuführen.
Bitte beachten Sie bei allen Schulungen die menschlichen Voraussetzungen für eine effiziente Schulung.

Grundsätzlich sind jeweils On-the-Job-Trainings vorgesehen. D.h. das Erlernte / Vorgetragene sollte unmittelbar nach dem Vortrag auch selbst durchgearbeitet werden. Gerade in komplexen Systemen, wie dem umfassenden ERP System **Kieselstein ERP**, ist insbesondere zum Anfang, das **<u>tägliche Üben</u>** ein unbedingtes Muss.

Idealerweise werden diese Punkte bereits vor der ersten Schulung von den Teilnehmer:innen durchgearbeitet / durchgesehen, damit klar ist, welcher Teilnehmerkreis mit welchen Modulen geschult wird.

Es werden die geschulten Teilnehmer jedes Modules namentlich erfasst.

Der **Kieselstein ERP** Verantwortliche des Anwenders muss bei jedem Schulungs-Modul mit dabei sein (sonst ist er nicht der **Kieselstein ERP** Verantwortliche und kann die absolut erforderliche Koordinierungsleistung nicht erbringen)

## Basisschulung
Grundlagen<br>
Dauer: 2 Std<br>
Teilnehmerkreis: alle Poweruser<br>
Vorbereitende Unterlagen: keine<br>
Vorbereitende Arbeiten: Jeder hat seinen Login und wenn möglich auch seinen Rechner mit einem Zugang zum **Kieselstein ERP**.<br>
- Ablauf im Unternehmen, Kreis 
  - was ist vom Prozess her vor "mir" und nach mir. (Warum sollte ich mehr erfassen) 
  - Benutzeroberfläche (Direkt Filter, Neu, Ändern, ..) 
  - Adressen, Kunden, Lieferanten anlegen 
  - Artikel anlegen 
  - Drucken 
  - Dokumentenablage 
  - weitere Informationen, Journale

Und erst danach die weiteren Schulungen. D.h. nach diesem sehr allgemein gehaltenen, wie geht denn was, muss ein jeder Schulungsteilnehmer einen Artikel, einen Kunden und einen Lieferanten angelegt haben, und zwar so richtig selbst. Danach gibt es Pause und dann wird mit Modul 01 begonnen.


Voraussetzungen: Login und Rechner<br>
Ein einfacher Auftrag mit dem Artikel und ein komplexer Auftrag<br>
Wenn auch Stücklisten und Fertigung, dann bitte eine sehr einfache Stückliste mit 1-2 Unterstücklisten.

## Modul 01, Basis, Bedienung und Stammdaten
-	Direktfilter, Zusatzfilter
-	obere untere Modulreiter
-	Module in den Vordergrund holen, mehrere Clients gleichzeitig
-	Journal, Infos, Statistiken, Auswertungen
-	Drucken und Speichern
	-	Daten exportieren
		-	Zwischenablage, Speichern unter
	-	Statuswechsel
-	Layout speichern
-	Größen und Farben einstellen

### Modul 01a, Gemeinsamkeiten der Bewegungsmodule
-	Positionsarten
-	Übersteuerte Artikelbezeichnungen, Artikelkommentar übersteuern
-	Hinterlegen von Texten bei Artikel-Positionen
-	Gestaltungsmöglichkeiten (Leerzeile, Seitenumbruch, Endsumme)
-	intelligente Zwischensumme, Setartikel, Stückliste (auf Beleg mitdrucken)
-	Verkaufspreisfindung / Einkaufspreisfindung
-	Wie Bilder aus der Zwischenablage übernehmen

## Modul 02, Artikel
-	Artikelnummer, ev. Generieren
	-	Strukturen der Artikelnummern
-	Mengeneinheiten
	-	Lagergeführt, Bestellmenge, Verpackungseinheitenmenge je Lieferant / Mengenstaffel
-	Artikelgruppen
-	Serien und Chargennummern, nicht lagerbewirtschaftet
-	Mindestdeckungsbeitrag
-	Kommentare, Beschreibungen, Bilder, Dateiverweis
-	Dokumentenlink, Dokumentenverweis, Dokumentenablage
-	Verkaufspreise, Mengenstaffeln
-	Einkaufspreise, Mengenstaffeln
-	Gestehungspreis
-	Lager / Mehrlagerverwaltung / Standorte
	-	Lagermindeststand
	-	Lagerstand < 0
-	Preiseinheiten, Nachkommastellen
-	Hersteller <-> Lieferanten
-	Ersatztypen
-	Artikel Freigabe, Artikel Sperren
-	doppelte Artikel / Artikel zusammenfassen / mehrere Artikelnummern für die gleichen Artikel
-	Artikel Versionen / Revisionen (Lagerstand auf alten Versionen)
-	Seriennummern Stammblatt
-	Seriennummern finden

## Modul 03, Stücklisten
-	Positionen
	-	abweichende Mengeneinheiten, mehr Dimensionale Artikel,
		-	Fehlerhafte Zielmengen
	-	Ersatztypen
-	Arbeitsplan, Tätigkeiten, Maschinen
-	Default Durchlaufzeit ↔ automatische Berechnung der Los-Beginn/Endetermine
-	Abbuchungslager, Ziellager
-	Materialentnahme bei 
-	Gesamtkalkulation
	-	Vorkalkulation anhand Lief1Preis, von den Einzelkosten zum theoretischen Verkaufspreis
	-	Make-or-Buy Entscheidungshilfe
-	Importe
	-	XLS -> Positionen, Arbeitsplan
	-	int.Stkl.Import
-	Stücklisten Freigabe
-	Änderungen in betroffenen Losen nachziehen

### Modul 03a, Zentraler Artikelstamm
-	mit welchem Mandanten wird welche Stückliste produziert
-	Mandanten übergreifende Bestellung
-	Ware (zwischen den Mandanten) unterwegs, Transportzeit

## Modul 04, Sales / Verkauf
-	Partner, Kunden, Lieferanten
	-	Unterschiedliche Sicht auf die gleichen Daten
	-	Ansprechpartner
	-	Konditionen
	-	Inland / Ausland, UID, EORI, 
	-	doppelte Adressen / Zusammenfassen
	-	Sonderkonditionen, Kundenartikelnummern
-	Rechnungsadresse -> Parameter KUNDENAUSWAHL_STRUKTURIERT
-	Projekt
	-	Projektklammer
	-	Forecast anhand Projekte
	-	Bereiche, Kategorie, Typen
		-	Verbindung mit Artikel
	-	Verantwortliche, Durchführende, weitere daran Beteiligte (Techniker)
		-	Wirkung in der mobilen Offline-Erfassung
	-	Projektverlauf, Nachkalkulation
-	Angebot
	-	Positionsarten
	-	kopieren von Angeboten und oder Positionen, Zwischenablage
	-	Angebotsstücklisten
	-	Verkaufspreis
		-	Vorbelegung, Auswahl, Mengenstaffeln
		-	Sonderkonditionen, Kundenartikelnummern
	-	Vorkalkulation, Verfügbarkeit, Auflösungstiefe
	-	Nachfassen, Realisierungstermin, gewichteter Hoffnungsfall
	-	Journale
	-	intelligente Zwischensumme, Setartikel, Stückliste (auf Beleg mitdrucken)
	-	Angebotsstücklistenschnellerfassung

## Modul 05, Verkauf, Auftragsabwicklung
-	Auftrag
	-	Übernahme aus Angebot in Rahmen- oder freien Auftrag
	-	freier Auftrag
	-	wiederholender Auftrag
	-	Rahmenauftrag, Abrufauftrag
	-	einzelne Positionen erledigen
	-	Journale
	-	Nachkalkulation / Deckungsbeitrag
	-	Wiederbeschaffung
	-	Packliste, Kommissionierschein
		-	Wirkung Nutzen für/mit mobilen Geräten, automatische Zuordnung
	-	Importe
	-	Freigabe
	-	Erledigen, manuell oder automatisch
-	Rasche Auskunft über den Auftragsstatus an den Kunden

## Modul 06, Lieferung, Verrechnung, offene Posten
-	Sicht Auftrag, Sammel-Lieferschein, Verketteter Lieferschein, Sammelrechnung
-	Anzahlungsrechnung, Schlussrechnung
-	Gewichtsangabe, Warenverkehrsnummer, Ursprungsland
-	Mehrwertsteuersätze
-	Ausliefervorschlag, Auszuliefernde Positionen
	-	offene Anzahlungsrechnungen
-	Lieferpapiere, Etiketten, SSCC
-	echte Proformarechnung
-	Gutschrift
-	Mahnen, Sammelmahnung, Mahntexte, Mahngebühren

## Modul 07, Fertigung
-	Anlegen
	-	Manuell, anhand Auftrag, interne Bestellung
	-	Bedeutung der Termine / Farben in der internen Bestellung
-	Beginntermine, Dauer, Endetermin
	-	negative Sollmengen, Werkzeuge
-	Anpassen des benötigten / gewünschten Materials
-	Anpassen der geplanten Zeiten auf Maschinen / Personen
-	Wann wird das Material gebucht,
	-	wie genau muss das gebucht werden, muss ich jede Beilagscheibe buchen
	-	Welches Material wird gebucht, Ersatzartikel
-	Lose ausgeben
	-	Erzeugen der Fertigungsbegleitscheine
	-	Anzeigen / Ausdrucken von Zeichnungen
	-	Ausdrucken der Ausgabeliste, Materialentnahmeschein
		-	Buchen automatisch, manuell, mit mobilen Geräten
-	wachsende Stückliste
	-	Was ist bei Ersatztypen zu beachten
-	Vergleich mit Stückliste
-	wo sieht man den Fertigungsfortschritt
-	Freie Materialliste
-	Auslieferliste
-	Stückrückmeldung
-	Fehlererfassung
-	Halbfertigfabrikatsinventur, Work in Progress
-	Nachkalkulation auf Losebene
	-	Entwicklung der Produktion der Baugruppen / Losstatistik
	-	Wo kommen die Kosten her?
		-	Materialkosten
		-	Personalkosten
		-	Maschinenkosten
	-	Gesamtkalkulation
-	Auslastungsvorschau, einfach / detailliert
-	offene Arbeitsgänge, Maschinen und Material optimieren (gleiches Material gemeinsam machen)
-	Lose verdichten, Zusammenfassen
-	Losgrößen ändern
-	Lose splitten
-	Lose auf einmal ausgeben
-	wiederholende Lose
-	Lose abliefern
	-	Unterliefern
	-	Überliefern
	-	Geräte Seriennummern
-	Sollsatzmenge unterschritten
	-	das bedeutet
	-	darf man das?
	-	bei welchen Artikeln wird das am Terminal trotzdem erlaubt
-	Übertragen der aktuellen Materialentnahmen als Solldaten in die Stückliste

## Modul 08, Zeitbewirtschaftung, Personal, Maschinen
Wer darf was und wer sieht was, DSGVO
-	Personal
	-	Personalkostenquelle
	-	Kollektivvertrag / Tarifvertrag, Überstundendefinition
	-	Zeitmodelle
		-	Zwangspausen, Fixe Pausen
	-	Abteilung, Heimatkostenstelle
	-	Eigenschaften nutzen
	-	Erzeugen der Personalbarcodes
-	Zeiterfassung
	-	Buchungen korrigieren
	-	Buchen mit den Terminal(s) (coming soon)
		-	Buchen auf abweichende Maschinen
		-	auf welche Maschinen habe ich mich angemeldet
		-	Zeiterfassungsstifte
		-	Mehrmaschinen Bedienung
		-	Anzeigen von Artikeldaten (Stammblatt/Dokumentenlink)
		-	Anzeigen der Fertigungsfortschrittes Soll/Ist-Zeiten
		-	Buchen von Abwesenheiten
		-	Urlaubsantrag
		-	Statusanzeige
		-	Stechuhr Modus, Personenliste
		-	Ausweisnummer / Barcode / RFID
		-	Los Auswahlliste
	-	Manuelles Buchen
		-	Zeiterfassung
		-	im Los, Auftrag, Angebot, Projekt
		-	Favoriten
		-	Kopieren von Zeitbuchungen, wozu das
	-	Änderungsprotokoll
	-	Sonderzeiten
		-	Urlaubsantrag und Freigaben
	-	Reisezeiterfassung
		-	km, Fahrtenbuch, Unveränderbarkeit
		-	Diäten
	-	Monatsabrechnung
	-	Zeitbuchungsjournal, Gestern Liste
	-	Anwesenheitsliste
	-	Schichtzeiten
	-	Zeitmodelle
		-	automatische Schichtzeiterkennung
	-	Export der Zeitbewegungsdaten
	-	Pflege der automatischen Zeitbuchungen
	-	Ändern der Loszuordnung / auf falsches Los gebucht
		- Wie lange darf wer was korrigieren, Wirkung der Parameter
	- Mobile Offline Erfassung
		-	Erhaltene Buchungsfehler korrigieren
		-	Zeitbestätigung
	- Maschinen
		- Maschinenzeitmodelle
		- Maschinengruppen
		- Mehrmaschine <-> Mitarbeiter geführte Maschine
		- parallele Maschinenbedienung
	- Zeiten auswerten
		- wie und wofür 

## Modul 09, Einkauf
-	Anfrage, Gruppenanfrage, Bieterübersicht
	-	Erfassung der Lieferkonditionen und der Lieferdaten
	-	Rückpflege in den Artikelstamm
-	Bestellung
	- Auftragsbestätigung
  	- Wareneingang
  	- Mahnwesen
  	- Mahntexte
  	- Bestellvorschlag
		- Pflege der Artikeldaten
			- Einkauf optimieren
		- int. BS Import
	- Eingangsrechnung
		- Eingangsrechnungs Gutschriften
		- Anzahlungsrechnung / Schlussrechnung
		- Splittbuchung
		- Positionskontierung (die ausgehende Provisonsgutschrift)
		- Anlegen von Bankdaten je Lieferant
		- Verbuchen der Schrottgutschrift
	- Bestellvorschlag, Dispolauf
		- optimieren der einzukaufenden Artikel
	- Anfragevorschlag

## Modul 10, kleine Finanzbuchhaltung
-	Export der Daten
	-	Welche Formate
	-	Belege mitexportieren
-	Zahlungsverwaltung und Mahnwesen
-	Verbuchen von Zahlungen in ER, RE, GS
-	SEPA Kontoauszugsimport
-	Zahlungsvorschlag
-	Reverse Charge
-	Intrastat Meldung
-	Zusammenfassende Meldung

## Modul 11 Reklamation, Abwicklung von Reparaturen, QS, Rücknahme, RMA
-	Lieferantenbeurteilung
-	Messmittel wie abbilden
-	Dokumentenpflicht

## Modul 12 Fremdfertigung
-	Ziellieferschein
-	Fremdfertigungs-Arbeitsgang, -Zukauf der Dienstleistung des Fremdfertigers
-	Zubuchung und automatische Erledigung
-	Definition für die Kapazitätsplanung / Zeitplanung

## Modul 13 Angebotsstücklisten
-	Kalkulationsarten
-	Mengenstaffeln
-	Einkaufsangebotsstückliste
	-	EinkaufsAnfrageStückliste
-	Vorlage für die Angebotsstücklistenschnellerfassung

## Modul 14 Termine und deren Zusammenhänge
Das **Kieselstein ERP** ist ein vom Kundentermin getriebenes System. Von der Idee her, sollte man immer lieferfähig sein, bei einem Lagerstand von 0 (was natürlich nicht geht).

-	Rüstzeiten / Losgrößen
	-	Was ist die optimale Losgröße.
		-	Wie lange wird der Kunde das Produkt noch exakt in dieser Ausführung abnehmen?
		-	Habe ich Lagerplatz?
		-	Will ich mein Kapital / meine Liquidität so lange binden (Was würden Sie bei der Bank dafür bezahlen, wenn Sie keine Sicherheiten dafür hätten)

## Modul 15 Versionen, Revisionen, Artikeln und Stücklisten und deren Änderungen
-	vom lagernden Artikel der alten Version bis zur strukturierten Artikel- und Stücklistenänderung 
	-	nach oben hin.

## Modul 16 Optimaler Zeitpunkt der Formularanpassungen
- Logo, Kopf- und Fußzeilen ... bei der ersten Basisschulung
- offizielle Formulare ... nach Verständnis der Zusammenhänge, eher gegen Ende der Basisschulung und erst kurz vor Going Live

## Modul 20 Mehrsprachigkeit
-	was wo wie definieren
-	Was ist falsch, wenn es zu gemischten Texten kommt

## Modul 25 CRM
-	von welchem CRM sprechen wir (Operatives, Analytisches, oder nur Akquise)
-	Serienbrief, Kurzbrief, HTML, Selektionen

## Modul 30, integrierte Finanzbuchhaltung
-	für welche Anwender ist dieses Modul gedacht
-	wie steuert man die Buchungsautomatiken
-	welcher Kontenrahmen kommt zur Anwendung
-	SEPA Kontoauszugs Import
-	Zahlungsvorschlag
-	UVA, Ust-Verprobung
-	Saldenliste
-	BWA, Ergebnisgruppen, Bilanzgruppen definieren und anwenden
	-	Vergleichsdruck der Eröffnungsbilanz
-	Manuelle Buchung, Eröffnungsbuchung
-	Splittbuchung, Buchungsschablonen
-	Ausziffern
-	offene Posten, Abgleich mit ERP Teil
-	Ust aus Vorjahren, ist auch ein FA_Zahllastkonto ?
-	Fremde Vorsteuer / abweichendes Finanzamt

## Modul 50, mobile Geräte
-	Offline
-	Online

## Modul 80, Arbeitsvorbereitung, Fertigungssteuerung
-	Auslastungsvorschau
	-	Einfach
	-	Detailliert
-	Maschinenbelegung

## Modul 90, Controlling
-	Nachkalkulation
-	Produktivitätsstatistik
	-	Mitarbeiter
	-	Maschinen
-	Liquiditätsvorschau
-	offene Posten
-	Einfache Erfolgsrechnung
-	Provisionsabrechnung
-	Eigene Termintreue
-	KPI
-	offene Anzahlungsrechnungen

## Modul 91, Periodisch verwendete Funktionen
-	Inventur
-	Halbfertigfabrikatsinventur / Work in Progress

## Modul 92, Periodisch verwendete Funktionen der Finanzbuchhaltung
-	Erfolgsrechnung
-	Bilanz
-	UVA
- KPI
- Einfache Erfolgsrechnung
- Liquiditätsvorschau

# Expertenschulung
## Modul 100, Formulare
- Selbst anpassen, neue Formulare erstellen
- Formularvarianten definieren
- Definitionen der Formular-E-Mail-Vorlagen
[Reportgeneratoren]({{< relref "/docs/installation/10_reportgenerator" >}})

## Modul 101, System
-	Parametrierung, Einstellung wesentlicher Parameter
-	Automatikjobs
-	Pflege
-	Versandauftrag
-	Mandanten
-	Grunddaten
	-	wo finde ich welche Grunddaten
-	Änderungsprotokollierung
  -	Umfasst immer nur wenige ausgewählte Datenfelder in den jeweiligen Modulen<br>
		dafür aber immer möglichst aussagekräftig.
-	Nachrichtensystem
-	Eigenschaften für Module selbst definieren
-	Dokumentenlink einrichten

## Modul 102, Berechtigungen,
-	Benutzer
-	Rollen & Rechte
-	Benutzer Mandant

### Modul 102a Freischalten weiterer Funktionen, weiterer Benutzer

## Modul 103, Datensicherung
-	wozu
-	wie oft
-	wie prüfen
- physikalisch von der gesamten IT getrennt.

## Modul 104, Eigenschaften definieren
	Artikel Eigenschaften
	Chargen Eigenschaften

## Modul 105, Installationen
- [Client]({{< relref "/docs/installation/02_client" >}})
- Einrichten [Twain]({{< relref "/docs/installation/14_dokumentenscann/_index.md" >}})  / Dokumentenscann


## Modul 110, Formelstücklisten

## Modul 120, CleverCure
## Modul 121, Forecast, EDI Importe
-	Liefermengenfortschrittszahl

## Modul 122, Zutrittssystem
## Modul 123, Cockpit
## Modul 124, Dashboard und dessen Konfiguration
## Modul 140, Instandhaltung
## Modul 150, Küche

## Modul 200, Externe Geräte, Anbindungen
-	Terminal
	- offline	Zeiterfassungsstifte
	- ZE-Terminal (coming soon))
		-	Zeitsynchronisierung
-	Barcodescanner
	- mobile Erfassungsgeräte, z.B.	Memor1 von Datalogic
-	Barcode 1D / 2D
-	Etiketten, Etikettendrucker

# Zeitaufwand
Wie man aus dieser Aufstellung sieht, gibt es eine deutliche Anzahl an Punkten, die geschult werden sollten. Vieles kann man sich aus der umfangreichen Online Hilfe heraus lesen, aber: Für vieles ist eine gute Kommunikation mit dem/der **Kieselstein ERP** Betreuer:in notwendig. Auch gerade die Dinge **Best Practice**, was ist in welchen Branchen üblich, was könnte in deinem Unternehmen funktionieren.

Für eine ungefähre Zeitschätzung sollte man davon ausgehen, dass man im Schnitt für jeden Punkt 10 Minuten benötigt. Die Summe der für den Schulungstag geplanten Punkte ergibt dann eine sehr grobe Richtlinie, was an diesem Tag möglich ist. Somit ergibt sich auch, wie viele Schulungstage man eventuell brauchen wird.
Es hängt dies immer auch, von den Teilnehmern ab. 
