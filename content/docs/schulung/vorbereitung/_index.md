---
title: "Schulungsvorbereitung"
linkTitle: "Vorbereitung"
weight: 10
description: >
  Vorbereitung zur Kieselstein-ERP Schulung
---
## Vorbereitung
- Maximal fünf Schulungsteilnehmer:innen
  - jedeR Schulungsteilnehmer:in hat einen Namensaufsteller mit Funktion im Unternehmen
- bitte um Info, wenn wir einen Beamer mitbringen sollten
- bitte um Info, wenn wir ein Flipchart mitbringen sollten

{{% alert title="Ruhe im Besprechungsraum" color="warning" %}}
Wir wollen konzentriert und effizient schulen, also<br>
Handy, E-Mails usw. sind verboten, stören das Arbeiten massiv.<br>
Das gilt bitte auch für die eventuell teilnehmende Geschäftsleitung, COO usw.
{{% /alert %}}

### weitere Unterlagen / Infos
Damit wir den Server möglichst gut vorbereiten können, bitte vor der ersten Basisschulung noch folgende Dinge an uns übermitteln:
- Briefpapier inkl. Logo
  - das Logo bitte in einer geringen, aber ausreichenden Auflösung
  - und bitte im png Format
- falls es ein großes Logo ist, bitte auch noch eine kleine / reduzierte Variante davon
- Die Benutzernamen, unter denen sich die ersten Schulungsteilnehmer anmelden möchten
- Falls es mehrere beteiligte Mandanten / Firmen gibt, bitte überlegen, wie die leichte Unterscheidung unter den Belegnummern gemacht wird.
Bewährt haben sich sprechende Buchstaben anstatt des Belegtrenners (der Default ein / Schrägstrich ist) oder unmittelbar nach dem Belegtrenner ein Kennungsbuchstabe, wobei dies bedeutet, dass nur mehr sechs Stellen für die laufende Nummerierung innerhalb des Jahres bleiben.

## Ablauf
Es werden von uns "nur" die Poweruser geschult, von diesen wird, ev. gemeinsam mit dem Consultant/der Consulterin die Umsetzung im eigenen Unternehmen festgelegt. Mit diesem Umsetzungswissen werden dann die weiteren / einzelnen Mitarbeiter:innen Gruppen durch die Poweruser geschult.

Wichtig ist, dass allen klar ist, dass die eigene Aufgabe im gesamten Unternehmens-Verbund zu sehen ist. Das kann durchaus bedeuten, dass die eigene Arbeit etwas mehr wird, damit sich aber die nachfolgenden Arbeiten deutlich vereinfachen, effizienter werden.

So muss z.B. beim Auftrag alles eindeutig und klar definiert sein. Damit läuft der weitere Prozess einfach und rasch durch. Das kann bedeuten, dass die Auftragserfassung etwas länger dauert. Alles andere wäre ein beständiges Nachbessern und man muss auch noch an dies und jenes denken. Wir wollen saubere Vorgaben, damit die weiteren Unternehmensprozessschritte möglichst reibungslos ablaufen können.

Bei manchen Anwendern wurde sogar festgelegt, dass das schon beim Angebot sauber definiert sein muss.

## Täglich üben
Es muss allen klar sein, dass nur durch tägliches Üben, von mindestens einer Stunde pro Tag, ein Lernen und Verfestigen des Wissens gegeben ist.

Idealerweise wird diese Lern-Stunde jeden Morgen durchgeführt.

Bitte nehmt dies sehr ernst. Wenn nicht geübt wird und wir merken das innerhalb weniger Minuten, ist die ganze Schulung wertlos, also teuer und frustrierend.

Und ja, **Kieselstein ERP** funktioniert komplett anderes wie das alte System, vor allem ist es kein Word und kein Excel. Damit erreicht man, dass man sich auf die Zahlen verlassen kann, viel rascher herausfindet, warum welche Kosten entstanden sind. 

Denn: Der eigentliche Zweck eines jeden ERP Systems ist, mit diesem System das Unternehmen **nachhaltig** zum Erfolg zu führen, stetig besser zu werden.

Und auch verlässliche Zahlen für alle, auch externe Partner, zu liefern.

## Ansprechpartner / Inhaus ERP-VerantwortlicheR
Zentrale Person, die alle - zumindest aber die ERP-Prozesse - kennt und beherrscht und die Koordination übernimmt.

Diese Person ist auch unser zentraleR Ansprechpartner.

Bitte um Verständnis, dass wir nur Anfragen dieser Person beantworten. Eventuelle Ausnahmen bestätigen nur die Regel.

## Spezial-Vorbereitung für die Geschäftsleitung, den/die COO
- welche Kennzahlen sind wichtig
- tägliche
- wöchentliche
- monatliche
- gibt es tatsächlich jährliche Kennzahlen, die nicht während des laufenden Jahres abgefragt werden?

Diese Kennzahlen können sein:
- Umsatz
- Auftragseingang
- Lagerbestand, gegliedert nach Lagerarten (Sperrlager, Konsignationslager, Lieferantenlager, Halbfertig Lager, ...)
- Halbfertigbestand, Work in Progress, also der Anarbeitungsgrad in der Fertigung
- Ladenhüter
- Anzahl der abgelieferten Lose
- Reklamationskosten
- offene Rechnungen
- Anzahl / Wert der Gutschriften, ev. auch Retouren vom Kunden
- offene Aufträge
- offene Bestellungen, inkl. noch nicht fakturierter Wareneingänge
- offene Eingangsrechnungen
- Anwesende Stunden der Mitarbeiter:innen, vor allem in Relation zum Umsatz
- Laufzeiten der Maschinen, wie viele Stunden welcher Tätigkeitsart wurden im Zeitraum gebucht
- gegliedert nach den Arbeitszeit-Artikelgruppen

Es muss bitte immer genau definiert werden, was unter dem jeweiligen Punkt verstanden wird.