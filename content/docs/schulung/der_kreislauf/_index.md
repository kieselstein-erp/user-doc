---
title: "Der Unternehmens Kreislauf"
linkTitle: "Kreislauf"
weight: 50
description: >
  Der Kreislauf im Unternehmen
---

Eine Beschreibung des Unternehmenskreislaufes

## Projekt
- Die Idee, eine Kundenanfrage

## Angebot
- mit Vorkalkulation, also einer ersten Schätzung, die hoffentlich richtig ist

## Auftrag
- Die Bestellung vom Kunden bekommen und die Auftragspositionen zumindest als Artikel erfasst
- Hier beginnt der eigentliche Erfolgsprozess, wobei er natürlich schon beim Projekt, und vor allem beim richtig (vor-)kalkulierten Angebot beginnt

## im Produktionsbetrieb
- also alles, was veredelt, verändert, bearbeitet wird und sei es nur dass zwei Teile zu einem neuen Ganzen zusammengefügt werden

## im Handel 
- geht es dann bei Anfrage / Bestellung weiter

## Stücklisten
- Materialbedarf für ein Stück Produkt, können aber auch Portionen sein
- und natürlich auch Unterstücklisten
	
## Arbeitszeitbedarf dafür, gegliedert in
- Menschzeit
- Maschinenzeit
- Initialkosten
- Rüstzeit
- Stückzeit
- Prüfanweisungen

## Fertigung
- Überleitung aus den (noch) zu fertigenden offenen Auftragspositionen inkl. Unterstücklisten
- Also die Stückliste multipliziert mit der zu fertigenden Menge ergibt das Fertigungs-Los
- Dies bewirkt den Materialbedarf (Reservierung) und den Kapazitätsbedarf (Mensch und Maschinen-Zeit) daraus

## Material
- **Anfrage**
	- Errechnen des benötigten Materials (Anfragevorschlag)
	- Anfragen an die Lieferanten richten
	- Angebote der Lieferanten einpflegen, optimalen Lieferanten ermitteln
- **Bestellung**
	- Errechnen des benötigten Materials (Bestellvorschlag)
	- Bestellungen an die Lieferanten senden
	- Terminbestätigungen der Lieferanten einpflegen
	- Wareneingänge erfassen -> damit ist das Material am Lager und kann für die Fertigung verwendet werden

## Kapazität
- Planen / Einteilen der einzelnen Arbeitsgänge / Tätigkeiten

Aus Material- und Kapazitätsverfügbarkeit / -bedarf ergibt sich der geplante Liefertermin

## Fertigung
- **Produzieren**
	- Material aus dem Lager in die Fertigung
	- Kapazität verbrauchen
	- Istzeit erfassen, um den Unterschied von Soll zu Ist zu sehen
	- je nach Branche detaillierte Stückrückmeldung für jeden Arbeitsgang
- **Abliefern**
	- Rückmelden der gefertigten Teilmenge an Lager zur weiteren Verwendung, z.B. Verbrauch in übergeordnetem Fertigungsauftrag oder Auslieferung mittels LS/RE

## Lieferschein
- Liefern der lagernden Teile anhand der offenen Auftragspositionen, oder auch freie (Ersatzteil-)Lieferungen

## Rechnung
- Verrechnen der offenen Lieferscheine

## Auftrag
- Nachkalkulation des Auftrages
- Soll-/Ist-Vergleich, tatsächlicher Ertrag bis zum DBIII
- Erkenntnisgewinn, damit Verbesserung der Angebots-Vorkalkulation

## Aufgaben der Buchhaltung
- **Bestellung**
	- Prüfung der Eingangsrechnung mit der Bestellung -> Preispflege -> Gestehungspreis
- **Eingangsrechnung**
	- Erfassung anhand der Bestellung
	- Kontierung
- **Bankbuchung**
- **Mahnwesen**
- **G+V / BWA**
- **UVA**

## Stammdaten
- **Artikel**
	- Serien / Chargennummern
	- Lagerstände
	- Ein- & Verkaufspreise
	
	Es ist (fast) alles ein Artikel, Bauteile, Stücklisten, Tätigkeiten, Zukaufteile.<br>
	Ein Artikel ist auch immer die Isolation der Informationen vom Lieferanten an den Kunden und umgekehrt.
- **Kunden**
	- Definition der Konditionen inkl. Artikel Sonderkonditonen
- **Lieferanten**
	- Definition der Konditionen und Einkaufs-Artikel
- **Reklamation**