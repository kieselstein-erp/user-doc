---
title: "Schulung Tagesablauf"
linkTitle: "Tagesablauf"
weight: 20
description: >
  Wie den Tag gestalten, Zeiten, welche Teilnehmer
---

Ein kleines Rundherum zum Tagesablauf.

Wir brauchen bitte für unsere Notebooks einen Zugang zu eurem **Kieselstein ERP**-System.<br>
Idealerweise per Lan, wenn nicht anderes möglich auch per <u>stabilem</u> WLan/WiFi.

Bitte ein Plätzchen an dem man auch mal in Ruhe etwas komplexere Datenbearbeitungen, Formularanpassungen u.ä. durchführen kann.

Wir beginnen unsere Schulungen pünktlich. Wenn sechs Personen 10 Minuten warten müssen, ist das einen Mensch-Stunde, mit entsprechenden Kosten.

Wir machen dazwischen aber immer wieder auch mal eine Pause, so ca. alle zwei Stunden.<br>
Es wäre gut, wenn kleine Erfrischungsgetränke, Kaffee, für die Teilnehmer zur Verfügung stehen würde.

Mittags machen wir eine Pause von ca. 30 Minuten. Bitte maximal einen kleinen Snack vorbereiten, kein umfangreiches Mittagessen.

Am Ende des Tages sollten wir uns auf eine entsprechende ToDo Liste einigen und auch den nächsten Schulungstermin festlegen.

{{% alert title="Ein Wort zu Handys, EMails und ähnlichen Unterbrechungen" color="primary" %}}
Es gebietet die Wertschätzung den Kolleg:innen gegenüber, dass alle konzentriert mitarbeiten. Wenn Einzelne dann "dauernd" rauslaufen, chatten, so ist das absolut kontraproduktiv. Bitte schaufelt den Schulungszeitraum frei.
{{% /alert %}}

So könnte ein ausgefüllter Schulungstag wie folgt aussehen:
- Beginn pünktlich um 8:00
- 08:00-10:00
- 10:00-10:15 15' Pause
- 10:15-12:15
- 12:15-12:45 kurze Mittagspause
- 12:45-14:45
- 14:45-15:00 kurze Pause
- 15:00-17:00
- 17:00- Open End
	- Vergabe der Aufgaben bis zum nächsten Termin
	-  nächster Termin und ev. Teilnehmer
	- ev. sonst noch zu erledigende Punkte

Für die ersten Schulungs-Meetings wäre es gut, wenn die Teilnehmer entsprechende Namenskärtchen vor sich auf dem Tisch stehen haben, damit wir uns leichter tun die entsprechenden Menschen anzusprechen. Idealerweise auch mit Angabe der Aufgabe.
