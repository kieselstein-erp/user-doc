---
title: "Begriffe"
linkTitle: "Begriffe"
weight: 90
description: >
  Erklärung der verschiedensten Begriffe rund um dein **Kieselstein ERP**
---
Begriffe rund um dein **Kieselstein ERP**
==============================

#### Was ist ERP eigentlich?
**E**nterprise-**R**esource-**P**lanning (ERP) bezeichnet die unternehmerische Aufgabe, die in einem Unternehmen vorhandenen Ressourcen (wie z. B. Kapital, Betriebsmittel, Personal, ...) möglichst effizient für den betrieblichen Ablauf einzuplanen. Der ERP-Prozess wird in Unternehmen heute häufig durch mehr oder minder komplexe ERP-Systeme unterstützt.
ERP-Systeme sollten weitgehend alle Geschäftsprozesse abbilden. Eine durchgehende Integration und eine Abkehr von Insellösungen führen zu einem re-zentralisierten System, in dem Ressourcen unternehmensweit verwaltet werden können.
Typische Funktionsbereiche einer ERP-Software sind: Materialwirtschaft (Beschaffung, Lager), Produktion, Finanz- und Rechnungswesen, Controlling und Vertrieb (Marketing).
Die Einführung einer ERP-Software ist bei mittelständischen und größeren Unternehmen ein komplexes Projekt.

**Kieselstein ERP** zeichnet sich insbesondere durch die effiziente Abbildung agiler Geschäftsprozesse aus (agilis lat.: schnell, flink). D.h. **Kieselstein ERP** passt sich mit Hilfe einer integrierten Prozessengine an die Prozesse des Unternehmens an und nicht umgekehrt (wie es bei den gängigen ERP Systemen oft der Fall ist!)
Ein ERP (Enterprise Ressource Planning) System
- dient der Abbildung der spezifischen Organisation eines Unternehmens
- ist eine Übersetzung der Realwelt des Unternehmens in datentechnische Strukturen, Funktionen und Prozesse
- umfasst alle betrieblichen Funktionen, also von der klassische Warenwirtschaft, über das gesamte Fakturierungswesen, die Vertriebssteuerung, die Finanzbuchhaltung, die gesamte Agenda der Beschaffung, das Personalwesen, die Fertigungssteuerung, das Management-Informationssystem inkl. einem vielfältigen Controlling, Anbindung der Lieferanten und Kunden z.B. per Internet, den Unternehmensleitstand u.v.a.m...
- ist das Herz eines jeden Unternehmens, die Daten, d.h. das Wissen über Kundenverhalten und über (Kunden-) Prozesse werden nur einmal erfasst und stehen damit Unternehmensweit für vielfältigste Betrachtungen und Auswertungen zur Verfügung.
- ermöglicht dem Unternehmer rasch die richtigen Entscheidungen zu treffen, z.B. durch ein entsprechendes Kennzahlensystem.
- kann durch Customizing an eine Vielzahl von unterschiedlichen Unternehmen angepasst werden.
- bietet durch den gesamtheitlichen Ansatz den anwendenden Unternehmen enorme Vorteile, sowohl in Bezug auf den ROI (Return of Investment) in der Investition, als auch in ihrer Durchschlagskraft und Reaktionsschnelligkeit im eigenen Geschäftsumfeld.

Oder, wie es ein Anwender formuliert hat:
- Die Geschäftsprozesse werden durch das ERP-System nachhaltig beeinflusst. Deshalb muss das ERP-System die Unternehmensziele in hohem Maße unterstützen. In diesem Sinne ist das ERP-System als strategische Waffe einzustufen.

#### EAI was ist das?
EAI = Enterprise Application Integration;
d.h. die Integration von unterschiedlicher Unternehmenssoftware (z.B. mit CMS/Marketing, Zeiterfassung /SAP-HR, Finanz/ SAP-FI, B2x: Unternehmensportale/Konzernmutter, Fremdportale wie ebay, branchenspezifische Einkaufsportale etc.);
dies ist meist notwendig, da ein ERP-System nie im Vakuum eingeführt wird, sondern mit bestehenden oder zukünftigen Systemen integriert werden muss!

Bei EAI spielen Web-Services mit dem SOAP Protokoll für die Funktionen und der XML Beschreibungssprache für die Daten eine zentrale Rolle. 
Web-Services können u.a. hervorragend mit einer J2EE Architektur realisiert werden.

### Kanban was ist das?
**Kanban** (jap., dt. *Schild, Karte*; manchmal fälschlich *Kamban*) ist eine Methode der Produktionsablaufsteuerung nach dem Hol- beziehungsweise Zuruf- oder Pullprinzip und orientiert sich ausschließlich am Bedarf einer verbrauchenden Stelle im Fertigungsablauf. Autonome Regelkreise auf Workflow-Ebene bilden das Kernelement dieser flexiblen Produktionssteuerung. Es ermöglicht eine nachhaltige Reduzierung der Bestände bestimmter Zwischenprodukte. Zudem ermöglicht es auch die Reduktion/ Optimierung von Beständen auf der Endproduktebene. Im günstigsten Fall kann die gesamte Wertschöpfungskette vom Endprodukt bis zur Bearbeitung des Einsatzmaterials auf der ersten Fertigungs-/Produktionsstufe gesteuert werden.

ACHTUNG: Kanban ist etwas für lange laufende Produkte. Wird es falsch eingesetzt führt es, gerade bei auftragsorientierten Unternehmen, also den typischen **Kieselstein ERP** Anwendern dazu, dass das falsche Produkt zum falschen Zeitpunkt produziert wird. Nutze anstelle Kanban, die interne Bestellung der Losverwaltung, auch wenn der Aufbau der Stücklistenstrukturen in den ersten Tagen etwas mühsamer ist.

Bedenke auch, mit einer reinen Kanban Steuerung kommst du erst zum Ereignis der "roten Karte" (mindestlagerstand unterschritten) darauf, dass dir Material fehlt. Gab es in letzter Zeit Mehrverbrauch, oder haben sich die Lieferzeiten von 2 Wochen auf 6 Monate verändert (was alle paar Jahre mal vorkommt), so müssen alle nachfolgenden Prozesse warten, bis dein Produkt vorrätig ist.<br>
Natürlich kann man das mit einer Anpassung der Lagermindeststände abfangen, nur wer macht das zumindest wöchentlich?<br>
Nutze die Möglichkeiten deines ERP-Systems!

#### Wie ist der Zusammenhang zwischen den Terminen?
#### Welcher Termin bedeutet was?
<a name="Termine"></a>
![](Termine_und_Zusammenhaenge.png)

#### EDIFACT
EDIFACT ist die Abkürzung für "Electronic Data Interchange for Administration Commerce and Transport". Er ist ein globaler Standard, dessen Entwicklung auf die Initiative der UNO zurückgeht und der bereits im September 1987 von der International Standard Organization als ISO9735 approbiert und registriert wurde. Der Begriff EDI umfasst verschiedene Konzepte für die elektronische Abwicklung von geschäftlichen Vorgängen zwischen verschiedenen Unternehmen, Branchen und Ländern.

Strukturierte Daten wie Bestellungen oder Rechnungen werden in einheitlichen Formaten untereinander ausgetauscht, wodurch viele Abläufe beschleunigt und dadurch Kosten eingespart werden können. Die verschiedenen Branchen setzten hierbei unterschiedliche Standards ein, wie z.B. S.W.I.F.T. bei Banken.

<a name="Gestehungspreis"></a>
#### Gestehungspreis / gleitender Durchschnittspreis
Der Gestehungspreis ist der gleitende Lagerdurchschnittspreis, der bei jeder Warenzubuchung gebildet wird. In **Kieselstein ERP** wird der Begriff Gestehungspreis verwendet, welcher von den Gestehungskosten eines Produktes herrührt. Für den Bereich des Einkaufes kann dieser gleichbedeutend mit dem gleitenden Durchschnittspreis verwendet werden.
Für den Bereich der Fertigung sind in den Gestehungskosten immer auch die Arbeits- und Maschinenzeiten mitberücksichtigt. Weiters werden im Bereich der Warenbeschaffung neben dem Einstandspreis auch die Transportkosten usw. berücksichtigt, was dann zum Begriff des Gestehungspreises führt.
Zur Erklärung der Berechnung des Gestehungspreise kann die Beschreibung aus Wiki verwendet werden.
Der **gleitende Durchschnittspreis** (kurz **GLD-Preis**) wird bei der Bestandsbewertung von Positionen, die zu unterschiedlichen Preisen eingekauft wurden, verwendet. Er verwendet die Methode des Gleitenden Mittels, um einen Durchschnittspreis zu errechnen. Er entspricht den mengengewichteten Einstandspreisen der einzelnen Beschaffungsvorgängen, also dem durchschnittlichen historischen Einstandspreis.

[![](GleitenderDurchschnittspreis_Formel.gif)](http://upload.wikimedia.org/math/4/7/d/47d7c5d7e1414c6c779fbb6a5f0567cf.png)
mit
- ![](GLD_neu.gif): neuer Durchschnittspreis
- ![](GLD_alt.gif): alter Durchschnittspreis
- xalt: bisherige Bestandsmenge
- pneu: Einstandspreis der neuen Ware
- xneu: Einstandsmenge der neuen Ware

Da die Bestände unter Umständen aus Beschaffungsvorgängen stammen, die weit in der Vergangenheit liegen, aber auch aus rechentechnischen Gründen, wäre eine geschichtete und zum jeweiligen Einkaufspreis bewertete Bestandsführung zur Ermittlung des aktuellen Durchschnittspreises unnötig kompliziert und fehl am Platz. Weiters löst der gleitende Durchschnittspreis auch das Problem, wie zwischenzeitliche Bestandsabgänge zu bewerten sind. Hierzu wird der am Stichtag gültige GLD-Preis verwendet.

Naturgemäß stimmen GLD-Preis und aktueller Markteinstandspreis in aller Regel nicht überein. Er liegt höher (bzw. tiefer) als der Marktpreis, wenn in der Vergangenheit im Schnitt zu höheren (tieferen) Kosten eingekauft worden ist.

Aus Gründen der Rechnungslegung kann der GLD-Preis für die Bestandsbewertung in der Bilanz ggf. nicht verwendet werden, wenn der aktuelle Einkaufspreis unter dem GLD-Preis liegt (Niederstwertprinzip). Aus betriebswirtschaftlicher Sicht sollten die kalkulatorischen GLD-Preise von Zeit zu Zeit überprüft und auf- oder abgewertet werden. Da sich buchhalterische und leistungswirtschaftliche Anforderungen zum Teil widersprechen, ist es ggf. sinnvoll und notwendig für die buchhalterischen und betriebswirtschaftlich/kalkulatorischen Bedürfnisse zwei unterschiedlichen Sichten auf die Bestandsbewertung zu unterhalten.

Der gleitende Durchschnittspreis spielt für die Bestandsbewertung unter anderem in folgenden Gebieten eine Rolle:

-   Bewertung von fremdbeschafften Rohmaterialien, Halbfabrikaten und Handelsprodukten

-   Bewertung von Wertpapieren im Börsendepot

Von "<http://de.wikipedia.org/wiki/Gleitender_Durchschnittspreis>"
Manche Menschen können mit einer Formel, einem Beispiel wesentlich mehr anfangen, als mit einer Beschreibung. Für diese haben wir dieses [Beispiel](./Gestehungspreis.ods) dazugegeben.

Info: Obiges Verhalten bedeutet auch, dass beim Nulldurchgang im Lager (also wenn der Lagerstand zeitlich und datentechnisch 0,00 ist), der aktuelle Gestehungspreis aus dem Einstandspreis (Zubuchungspreis) gebildet wird.

Gestehungspreisbuchung über mehrere Lager

Grundsätzlich wird der Gestehungspreis eines Artikels bei jeder Lagerzubuchung aktualisiert.

Das bedeutet:<br>
Wird ein Artikel z.B. auf das Hauptlager zugebucht, so wird für diesen Artikel der nun gültige Gestehungspreis errechnet. Zugleich wird der Einstandspreis dieses Artikels bei dieser Lagerzubuchungsbewegung mit abgespeichert.
Wird nun der gleiche Artikel vom Hauptlager in ein anderes Lager umgebucht, so wird er zum aktuellen Gestehungspreis entnommen und mit diesem Preis als Einstandspreis dem anderen Lager (z.B. Halle 2) zugebucht, wodurch sich ein neuer Gestehungspreis auf diesem Lager für den Artikel ergibt.

Warum ist das so?<br>
Eigentlich haben wir es hier mit einer Wertewanderung zu tun. D.h. der Artikel wird mit Menge und Einstandspreis als Wert dem Lager (Wareneingang) zugebucht. Daraus ergibt sich (Alter Lagerwert + neu hinzugekommener Wert) / nun aktueller Lagerstand der neue Gestehungspreis. Hintergrund dieser Durchschnittsberechnung ist, dass die Artikel, die nun im Lager liegen, kein "Mascherl" mehr haben und man daher bei der Lagerentnahme nur mehr den Durchschnittspreis, genauer den Wert, entnehmen kann.
D.h. nun wandert der Wert, der aus der ersten Zubuchung eben nicht mehr der Einstandswert, sondern der Gestehungswert ist, in das zweite Lager (z.B. Halle 2) und wird dort wieder zugebucht.

Diese Vorgehensweise, insbesondere die Bildung des gleitenden Lagerdurchschnittspreises wurde bereits von mehreren Wirtschaftstreuhändern geprüft und für richtig befunden.

#### Merkmale am Kunden / Partner
In **Kieselstein ERP** wird dafür der Begriff Selektion verwendet.

#### Pseudobaugruppe, Phantomstückliste
Wir verwenden dafür den Begriff [Hilfsstückliste]( {{<relref "/warenwirtschaft/stueckliste/#hilfsst%C3%BCckliste" >}} ).

#### Retrograde Bestandsbewertung
Diese "theoretische" Berechnung der "Gestehungspreises" eines Produktes wird in **Kieselstein ERP** nicht angewendet, da großer Wert auf die Erfassung der tatsächlichen Gestehungskosten gelegt wird, um aufzuzeigen wo die tatsächlichen Kostentreiber / Deckungsbeitragstreiber liegen. Definition von Retrograd siehe: <http://de.wikipedia.org/wiki/Retrograde_Bewertung>

#### URE
In der Schweiz ist der Begriff URE sehr gebräuchlich. Er steht für Unterhalt, Reparaturen, Ersatz(teile)

#### AHV
Dieser Begriff steht in der Schweiz für die Sozialabgaben.