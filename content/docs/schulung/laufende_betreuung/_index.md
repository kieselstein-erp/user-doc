---
title: "laufende Betreuung"
linkTitle: "Betreuung"
weight: 50
description: >
  laufende Betreuung
---

Insbesondere nach den ersten Basis-Schulungen muss man üben, um das Arbeiten mit dem **Kieselstein ERP** entsprechend effizient zu beherrschen.
Hier kommt es immer wieder vor, dass man an irgendeiner Kleinigkeit hängen bleibt.

Hier ist der pragmatische Weg:
- den/die inHouse ERP-VerantwortlicheN ansprechen. Hoffentlich kann er/sie die Frage rasch beantworten
- geht das nicht, sende bitte ein E-Mail, direkt an deinen **Kieselstein ERP** Betreuer mit einer entsprechenden Beschreibung und idealerweise auch einen Screenshot
	- den Screenshot bitte so klein wie möglich, aber so groß, dass alle Informationen drauf sind.
- in dringenden Fällen hilft natürlich auch ein Telefonanruf bei deinem **Kieselstein ERP** Betreuer
- Es sollte auf diesen Schulungsrechnern ein Teamviewer (http:) installiert sein, um gegebenenfalls rasch und effektiv die Schritte erklären zu können.