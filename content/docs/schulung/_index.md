---
title: "Schulung"
linkTitle: "Schulung"
weight: 0400
description: >
  Informationen rund um das Thema Schulung zum OpenSource Kieselstein ERP System
---

Dinge, die für eine effiziente Schulung des **Kieselstein ERP** Systems zu beachten sind.

Von der Vorbereitung über den Tagesablauf bis zu den Fragen ans Controlling.