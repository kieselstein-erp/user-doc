---
title: "Schulung Zugang"
linkTitle: "Zugang"
weight: 30
description: >
  Zugangsdaten für die Betreuung
---
Die Basis-Schulungen finden immer vor Ort statt. Das ist definitiv das Effizienteste.
Wir hoffen alle, dass wir die Fernwartungsschulungen, wie wir sie zu Corona Zeiten machen mussten, nicht mehr notwendig haben.

Um rasch mit der Schulung beginnen zu können, muss bitte
- der **Kieselstein ERP**-Server bereits vollständig installiert sein
- es muss vor Ort die Netzwerkverbindung vom Consultant Notebook zum **Kieselstein ERP**-Server möglich sein.
- idealerweise per Lan-Kabel. Wenn nicht anders möglich auch per stabilem WLan
- üblicherweise wird von uns nichts gedruckt
- IP-Adresse und Full-DNC Name des **Kieselstein ERP**-Servers müssen bekannt sein
- Der Server muss so eingerichtet sein, dass Client, RestAPI und DB-Zugriff möglich sind (siehe Installation, Ports, offen)
- bei Windowsserver bitte auch einen RDP Zugang mit einem User mit lokalen administrativen Rechten, inkl. den erforderlichen Passwörtern
- Bei Linux bitte root bzw. root Rechte mit sudo, inkl. den erforderlichen Passwörtern

Idealerweise sind vor der ersten Schulung bereits einige **Kieselstein ERP** Clients installiert. Falls nicht: Bitte um zur Verfügungstellung der Zugangsdaten um die Clients installieren zu können. Details siehe [Clientinstallation]({{< relref "/docs/installation/02_client" >}}).

Da die Schulung idealerweise ein on the Job Training ist, wünschen wir uns, dass die Schulungsteilnehmer zumindest in zweier Gruppen einen Rechner mit installiertem **Kieselstein ERP** Client haben.

Sollte die Installation nicht möglich sein, so wird dies spätestens zur ersten größeren Pause von uns nachgeholt. Bitte die [Zugangsdaten]({{< relref "/docs/installation/02_client" >}}) bereit halten / zur Verfügung stellen.
