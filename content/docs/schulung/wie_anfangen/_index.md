---
title: "Wie beginnen"
linkTitle: "Wie beginnen"
weight: 60
description: >
  Wie fange ich an
---
## Wie sollte ich denn nun beginnen
Diese Frage stellen sich die meisten (Jung-) Unternehmer:innen, welche sich nun glücklich selbständig gemacht haben und zu der Überzeugung gekommen sind, ein umfassendes ERP System einzusetzen.

Da ist ja so viel zu tun!


- Ich lese nach welche Sachen / Dinge / Punkte gehen
- Ich suche nach Lösungen in den FAQ's
- Muster ist die Fibu
- Das Ziel ist immer zuerst eine Beschreibung zu haben, wofür was gut ist
- Zuerst sehr allgemein und dann detaillierter
- Orientieren auch am Aufbau eines Werbeemails

**Also zuerst den Teaser**

Der Schwerpunkt sollte auf jeden Fall auf dem Modul und seinen Funktionen liegen. Beschreiben Sie nur eine Funktion nach der anderen - verraten Sie nicht alle Funktionen.

Dann einen längeren beschreibenden Text und dann die einzelnen Punkte des Moduls


