---
title: "Installation"
linkTitle: "Installation"
weight: 0300
description: >
  Wie installiert man das Kieselstein ERP System?
---
# Installation
[Server]( {{<relref 01_server >}} )
[Clients]( {{<relref 02_client >}} )
## nach der Installation
[Nach der Installation]( {{<relref 08_nach_der_installation >}} )
### Sicherheit (Passwörter, VPN)
### Warum Betriebssystem Unabhängigkeit
## Umfang freischalten
### welchen Umfang?
## Einrichten der Bearbeitung der Hilfe
[Hilfe]( {{<relref 20_hilfeeditor >}} )