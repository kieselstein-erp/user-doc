---
title: "Wie ein Kieselstein ERP System wieder herstellen"
linkTitle: "Restore"
categories: ["Installation", "Server Installation"]
tags: ["Installation", "Server"]
weight: 8
description: >
  Desasterrecovery, Restore
---
Wie für die Wiederherstellung deines **Kieselstein ERP** Servers vorgehen.

{{% alert title="Durchlesen, Ruhe bewahren" color="primary" %}}
Solltest du wirklich den dramatischen Fall haben, dass dein **Kieselstein ERP** Server unkontrolliert abgekracht ist, nimm dir vor irgend welchen weiteren Maßnahmen die Zeit dieses kurze Kapitel durchzulesen. 

In aller Ruhe.

Und wirf auch den Chef raus. Wenn er / sie will, dass es rasch geht, brauchst du vor allem Zeit, um die Schritte durchführen zu können. Das hat sich in allen Situationen bewährt.
{{% /alert %}}

{{% alert title="Trainieren" color="primary" %}}
Trainiere diese Wiederherstellung z.B. an einem Testsystem. Wenn du das 2-3 Mal gemacht hast, ist das alles kein Thema.
{{% /alert %}}

D.h. diese Beschreibung kannst du auch dafür verwenden, ein Testsystem deines Kieselsteins aufzusetzen. Und ja, du / dein IT-Betreuer solltest das üben. Denn im Falle des Desasters und wir wünschen uns alle, dass das nie eintritt, solltest du das schon öfter geübt haben.

## Voraussetzungen
Um dein **Kieselstein ERP** System wieder herstellen zu können, benötigst du in jedem Falle
- Das Backup deiner ERP Daten, welche üblicherweise unter ?:\kieselstein\sicherung\ liegen. Der Name des Dump lautet üblicherweise KIESELSTEIN.backup
- Das Backup deiner Dokumentendatenbank, liegt üblicherweise im gleichen Verzeichnis und lautet KIESELSTEIN_DOCUMENTS.backup
- Das Backup der Anwenderspezifischen Reports, liegt üblicherweise im gleichen Verzeichnis und lautet reports.zip
- Wenn du eine Fileorientierte Datenbank hast, dann auch das Backup von ?:\kieselstein\data\jackrabbit mit allen Dateien
- eine aktuelle Version des Kieselstein ERP Systems.

**WICHTIG:**<br>
Kümmere dich, mindestens einmal monatlich darum, dass dieses Backup auch in dein externes Sicherungsmedium wandert **<u>UND</u>** vollständig ist. Idealerweise machst du z.B. quartalsweise, jedenfalls aber jahresweise einen Restoretest.

Siehe dazu auch [Sicherung]( {{<relref "/docs/installation/01_server/datensicherung/" >}} )

## Vorgehensweise
- Nun installierst du einen neuen Kieselstein ERP Server wie unter [Kurzfassung Neuinstallation]( {{<relref "/docs/installation/01_server/01_einleitung/#kurzfassung-der-neuinstallation" >}} ) beschrieben.<br>
**WICHTIG:**<br>
Den Server **nicht** starten.
- nun löschst du die beiden durch die Scripte angelegten Datenbanken KIESELSTEIN und KIESELSTEIN_DOCUMENTS. Du kannst diese gerne auch auf KIESELSTEIN_default bzw. KIESELSTEIN_DOCUMENTS umbenennen
- Lege nun mit "neue Datenbank erstellen" eine KIESELSTEIN und eine KIESELSTEIN_DOCUMENTS an.
- Restore nun in die Datenbanken deine Sicherungen
- kopiere deine Anwenderreports auf ?:\kieselstein\data\reports
- bei einer Fileorientierten Datenbank, kopiere deine Sicherung der Files in exakt der gleichen Struktur auf ?:\kieselstein\data\jackrabbit
- für nun das Liquibaseupdate durch<br>
```bash
liquibase.sh update / run-liquibase.bat update
```
- nun kannst du deinen Server starten.<br>
Sollte deine Installation älter gewesen sein, müssen auch die Clients aktualisiert werden, sonst können die Anwender mit den bisherigen Accounts weiterarbeiten.

**Wichtig:**<br>
Dass der Datenbankserver die gleiche Major Version wie die gesicherte Datei hat oder etwas neuer ist, sollte selbstverständlich sein.<br>
Hinweis:<br>
Du siehst in einem guten Editor in den ersten Zeilen deiner Datensicherung im Klartext die Datenbankversion von der aus diese Datenbank gesichert wurde.

## Dauer
Wie lange wird so ein Restore dauern?
- Wenn du die neue Maschine zur Verfügung hast (wie ist das am 24.Dezember um 15:00), so wirst du zuerst die Backupdateien einkopieren müssen
  - wie lange dauert, das, wo sind diese, wie alt sind diese.
- Der Restore der Datenbanken dauert in der Regel etwas länger als das Backup selbst, was bei großen Dokumentendatenbank ein Thema werden kann (>12Std usw.)
- wie lange dauert der Download der benötigten Programme (siehe Neuinstallation)
- rechne noch 2-4 Std dazu, ev. musst du erst das Betriebssystem der neuen Maschine aufsetzen.

## Datenverlust
Du hast NUR die Daten zur Verfügung, die in deinem Backup enthalten sind. Wenn also dein Backup von gestern Abend ist und heute Abend, eine Sekunde bevor das Backup gültig abgespeichert werden konnte, kaputt ist, so hast du einen ganzen Tag verloren.

Kannst du dir das leisten, oder muss öfter gesichert werden. Das kann bis zum gespiegelten Datenbankserver gehen. Siehe [Sicherung]( {{<relref "/docs/installation/01_server/datensicherung/" >}} )


## Fehler beim Datenrestore
Beim Restore der Datenbanken dürfen grundsätzlich keine Fehler auftreten.

Auch hier keine Regel ohne Ausnahme.

Folgende Fehler sind normal und kommen daher, je nach Situation vor:
- Schema existiert bereits<br>
![](Schema_existiert_bereits.png)<br>
Das bedeutet, dass das dbo Schema schon vorhanden ist und daher das auch im Backup enthaltene Schema nicht eingelesen wurde. Dieser Hinweis kann ignoriert werden.
- Benutzer xyz ist nicht vorhanden<br>
Gerade beim Import von älteren Datenbanken kann diese Meldung, wenn du direkt von der Neu-Installation aus startest, vorkommen.<br>
Von Kieselstein ERP werden nur die Benutzer postgres und hvguest verwendet. Diese werden auch bei der Neuinstallation automatisch entsprechend eingerichtet. Alle anderen sind für *das normale Funktionieren* nicht erforderlich. Es kann aber durchaus Installationen geben, bei denen es z.B. einen speziellen Benutzer gibt, mit dem von extern Veränderungen an deiner Datenbank vorgenommen werden. Du musst grundsätzlich wissen, welche Zusatzdienste hier laufen und unter welchen Benutzern.

Ein gerne gemachter Fehler ist, dass eine komplette Neuinstallation gemacht wird und dann die Backupdatei in die bestehende KIESELSTEIN restored wird. Dies ist falsch. Es muss **immer** von einer neu angelegten Datenbank direkt **in** diese **leere Datenbank restored** werden.
