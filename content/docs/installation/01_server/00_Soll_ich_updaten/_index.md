---
title: "Macht ein Update meines Kieselstein ERP Servers Sinn?"
linkTitle: "Updaten?"
categories: ["Installation", "Server Installation"]
tags: ["Installation", "Server"]
weight: 4
description: >
  Sollte ich mein Kieselstein ERP aktualisieren
---
Wo sehe ich, ob ich mein **Kieselstein ERP** aktualisieren sollte.

## Kernel / Basissystem

Aktuelle Version -> siehe Client, Fenstertitel ![](Client_Fenstertitel.png)   bzw. siehe ?:\kieselstein\dist\version.txt

Was ist in der aktuell verfügbaren Version enthalten bzw. welche Erweiterungen sind für die nächste Zeit geplant. [Siehe](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/milestones)

[Herunterladen der aktuellen Version](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/releases)


## Terminal
Aktuelle Version: Siehe Titelzeile, hier steht z.B.: 0.0.xx.0 ![](Terminal_Fenstertitel.png)  

Die aktuell verfügbare Version [siehe](https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/releases)<br>
Hier findest du unter **Versionshinweise** welche Änderungen in der jeweiligen Version enthalten sind.<br>
Von hier kannst du auch die aktuelle Terminalversion herunterladen

## mobile App
Aktuelle Version: Siehe Konfiguration ![](Konfiguration.png) und dann ganz unten Version.<br>
Weitere Vorgehensweise wie unter [Terminal](#terminal) beschrieben.
