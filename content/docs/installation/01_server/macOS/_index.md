---
title: "Installation Kieselstein ERP Server unter macOS"
linkTitle: "macOS"
categories: ["Server Installation"]
tags: ["macOS"]
weight: 40
description: >
  Installation Kieselstein ERP Server unter macOS
---
Hier findest du nur in textlicher Form zusammengestellt, wie eine erste Installation unter MacOS erfolgen könnte.

Wir freuen uns, wenn ein entsprechender Profi, diese Beschreibung ergänzt.

Diese Installation baut auf der Kieselstein ERP Version 1.0.1 auf.

Wir haben auch einige Hintes für die Anwender anderer Betriebssysteme mit dazugegeben.

Wenn man im Finder verschiedene Devices usw. nicht findet, dann am Desktop auf
Gehe Zu (Computer) und dann dieses Device links reinziehen. Ab dem Zeitpunkt ist es da.

Ab OS X Version 8? steht der Launcher zum Starten der Dienste zur Verfügung
launchctl start/stop (dienst)
   mit list sieht man alle Dienste und die die eine PID haben laufen

find / -name xxx*.* findet alle Dateien ab Root

Rechtsklick mit der Maus bringt Einsetzen (aus der Zwischenablage)


#### prüfen welche Prozesse laufen
- ps aux | grep postgres
- Programme, Dienstprogramm, Aktivitätsanzeige<br>
ev. aus dem /Library das Postgres komplett entfernen

#### wo bin ich?
mit pwd bekommt man den aktuellen Pfad im Terminal, auf dem man steht

#### Downloads
idealerweise die Downloads über den Finder aus dem Download Verzeichnis starten

#### IP Adresse: ifconfig
#### Root user
Den Root User gibt es nicht wirklich, aber<br>
sudo -s eigenes Password -> damit ich ausreichend Rechte habe

#### Postgres deinstallieren
???  open /Library/PostgreSQL/Version/uninstall-postgresql.app/Contents/MacOS/installbuilder.sh
Sollte die Deinstallation nicht gehen, das Verzeichnis entfernen. Ev. dazu über Dienstprogramme, Aktivitäten den laufenden Prozess killen.

#### Postgres 15 installieren
- Bin als Administrator / mit administrativen Rechten angemeldet
- Download PostgresVersion 15.x
- dmg Datei aus Download öffnen und installieren. Auf den Port und das PW achten
- Pfad: /Library/PostgreSQL/15<br>
ohne Stack Builder aber mit pgAdmin 4

Nach der Installation von PostgreSQL 15 findest du diesen direkt im Finder

#### Anpassen der Konfiguration
Je nach MAC Version und Postgresversion musst du die pg_hba.conf und die postgresql.conf anpassen. [Siehe]( {{<relref "/docs/installation/01_server/debian" >}} )

#### Download Kieselstein ERP
von GitLab anscheinend nur mit Safari

#### Download Java11
Java11 auf die richtige Architektur achten (X64 oder ARM) JDK FX !!

#### altes / falsches Java deinstallieren
Java deinstallieren (laut Oracle)
- Klicken Sie im Dock auf das Finder-Symbol.
- Klicken Sie auf den Ordner Utilities
- Doppelklicken Sie auf das Terminal-Symbol
- Kopieren und fügen Sie die folgenden Befehle im Terminalfenster ein:
```
sudo rm -fr /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin
sudo rm -fr /Library/PreferencePanes/JavaControlPanel.prefPane
sudo rm -fr ~/Library/Application\ Support/Oracle/Java
```
Versuchen Sie nicht, Java zu deinstallieren, indem Sie die Java-Tools aus /usr/bin entfernen. Dieses Verzeichnis ist Teil der Systemsoftware. Änderungen werden von Apple zurückgesetzt, wenn Sie das nächste Mal ein BS-Update durchführen.

#### Verzeichnisse löschen mit File-Inhalten
rm -r Path

#### Kieselstein installieren
- /opt/kieselstein anlegen
- cd /opt
-  mkdir kieselstein
-  cd kieselstein
ins download wechseln, z.B. Finder und das .tar mittels Doppelklick entpacken<br>
Das Dist kopieren und in der Hauptmenüleiste (ganz oben) mit Gehe Zu auf /opt/kieselstein wechseln und dort einsetzen (dorthin kopieren)

#### Environment-Variablen
Nun die Environmentvariablen anlegen / ergänzen

Laut einigen Beschreibungen sind diese für alle User auf 
/etc/bashrc ... das File gegebenenfalls mit Nano anlegen
export PATH=$PATH:/Library/PostgreSQL/15/bin
export KIESELSTEIN_DATA=/opt/kieselstein/data/
export KIESELSTEIN_DIST=/opt/kieselstein/dist/
Terminal neu starten
ev. mit printenv die ganzen environment variablen prüfen

**ABER**
Es müssen diese auf meinen Mac in die bash-profile. Also:
- ~/.bash-profile eingeben. Damit findet man auch wo das File ist und dann ergänzen<br>
mit Export, so wie oben beschrieben und wichtig danach<br>
mit source ~/.bash-profile aktivieren

#### liquibase 
herunterladen und installieren. Landet auf /usr/local/opt/liquibase

##### Datenbank einrichten
- Terminal neu starten
- im Terminal nun auf /opt/kieselstein/dist/bootstrap/liquibase wechseln
- createdb.sh ausführen und 4x pw eingeben
- dann aus dem Verzeichnis das ./liquibase.sh ausführen

Es dürfen keine Fehler kommen und es müssen 18 oder mehr updates ausgeführt angezeigt werden

#### Client am gleichen MAC starten
Im Finder aus /opt/kieselstein/dist/clients, das Kieselstein-client...tar mit Rechtsklick und Archivierungsprogramm öffnen. Damit bekommst du das in das clients mit den Unterverzeichnissen bin und lib

Wenn mehrfach verteilt werden sollte, die kieselstein-client.sh die IP Adresse anpassen

#### Nun den KES Server starten
auf /opt/kieselstein/dist/bin wechseln <br>
und launch-kieselstein-main-server.sh

mit gehe zu auf deployment wechseln und prüfen ob startet

#### Nun den Client starten.
Um das aus dem Finder zu starten, musste du die Sicherheitseinstellungen erweitern
D.h. *Öffnen einer App durch Aussetzen der Sicherheitseinstellungen*
- Suche im Finder  auf deinem Mac nach der App, die du öffnen möchtest.
- Klicke bei gedrückter Taste „ctrl“ auf das Symbol der App und wähle „Öffnen“ aus dem Kontextmenü aus.
- Klicke auf „Öffnen“.
- Die App wird als Ausnahme zu deinen Sicherheitseinstellungen gesichert, sodass du sie künftig wie jede autorisierte App durch Doppelklicken öffnen kannst.

## ToDos
zu klären sind noch folgende Dinge
- a.) wie den Dienst am MAC einrichten
- b.) wie das Desktop Icon einrichten

## Zusatz-Infos
- Arbeitet man nicht auf einer echten MAC Tastatur sondern auf einer PC-USB Tastatur
  - AltGR+7 = | Pipe
