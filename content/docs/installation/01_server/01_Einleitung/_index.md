---
title: "Kurzanleitung Installation und Update Kieselstein ERP Server"
linkTitle: "Kurzanleitung Update und Installation"
categories: ["Installation", "Server Installation"]
tags: ["Installation", "Server"]
weight: 5
description: >
  Kurzanleitung wie den Kieselstein ERP Applikationsserver installieren bzw. updaten
---
Wie für die Installation deines **Kieselstein ERP** Servers vorgehen.

Diese Beschreibung teilt sich, unabhängig von den Betriebssystemen in zwei Bereiche:
- Völlige [Neuinstallation](#kurzfassung-der-neuinstallation)
- [Update](#kurzfassung-des-updates) einer bestehenden **Kieselstein ERP** Installation.
- [Wiederherstellung]( {{<relref "/docs/installation/01_server/02_recovery/" >}} ) 

Sie gilt ab der Kieselstein ERP Version 1.0.6, welche auch am Server Java 11 voraussetzt.

## Kurzfassung der Neuinstallation
Beachte das unter [Vorbereitung](#vorbereitung-für-serverinstallation) geschriebene

Unabhängig vom Betriebssystem ist die Vorgehensweise im Wesentlichen immer die gleiche. Die wesentlichsten Unterschiede zwischen den Betriebssystemen ist das Thema:
- Rechte -> hier setzen wir entsprechend umfassendes Wissen voraus.
- wo ist das Root-Verzeichnis deiner **Kieselstein ERP** Installation
  - Windows: Lokales Laufwerk (dargestellt mit ?:) und dann ?:\kieselstein
  - Linux(e): /opt/kieselstein

In der nachfolgenden Beschreibung wird von Windows als Server Betriebssystem ausgegangen. Wir wissen, dass Linux Admins, immer auch ein umfassendes Wissen in der Windows Administration haben und dies entsprechend übersetzen können.

### Benötigte Programme
- AZUL OpenJDK FX für Java 11 [siehe](https://www.azul.com/downloads/?package=jdk#zulu)
- PostgresQL 15, Installation ausgenommen für MAC OS immer ohne PGAdmin und Stack Builder
- PGadmin
- Latest Release deines **Kieselstein ERP** von [Gitlab](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/releases)
- Datenbankversionsverwaltung, [liquibase](https://www.liquibase.com/download)

### Installation
Für Windows die Empfehlung alles auf ein eigenes Laufwerk zu installieren z.B. D:
- Installation Java, inkl. permanentem Setzen des Java_Home
- Installation PostgresQL, alles default, PW: postgres
- Installation PGadmin
- Installation Liquibase
- Unter Linux die Schriften installieren
- Installation **Kieselstein ERP**
  - Einrichten der Verzeichnisse data und dist unter ?:\Kieselstein
  - Einkopieren des Dist Verzeichnisses aus dem kieselstein-distpack-?.?.?.tar.gz auf ?:\kieselstein\dist
- unter Windows hinzufügen des Pfades auf die PostgresQL Runtimes z.B. ?:\Program Files\PostgreSQL\15\bin
- Einrichten der Environment Variablen wie unter ?:\kieselstein\dist\bin\readme.md beschrieben Unter Linux darauf achten, dies OHNE EXPORT zu machen und danach den Server neu starten
- erstellen der Datenbanken
  - aus ?:\kieselstein\dist\bootstrap\liquibase createdb.bat(sh) ausführen und 4x das DB-Passwort angeben
  - danach 
  ```bash
  liquibase.sh update / run-liquibase.bat update
  ```
  ausführen.

- erstellen des Clients
  - ?:\kieselstein\dist\clients das kieselstein-client-?.?.?.tar entzippen und die beiden Verzeichnisse bin und lib auf c:\kieselstein\client kopieren
  - im c:\kieselstein\client\bin das kieselstein-client.bat das localhost:8080 auf *Kieselstein Server IP-Adresse*:8080 korrigieren
  - Bewährt hat sich nun das Verzeichnis c:\kieselstein\client zu zippen und auf ?:\kieselstein\dist\clients zur Verfügung zu stellen.
- erster Server Start
  - den Server manuell aus einer CMD-Shell starten, um eventuelle Fehlermeldungen o.ä. zu sehen
  - ?:\kieselstein\dist\bin\launch-kieselstein-main-server.bat(sh) starten<br>Es sollte sofort / nach wenigen Sekunden unter ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\deployments die drei Dateien mit dodeploy erscheinen.
  - Nach weiteren Sekunden wenigen Minuten müssen diese verschwinden und dafür *.deployed erscheinen. Ist dem nicht so, in den *.failed nachsehen oder unter ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\log\server.log
  - den Client starten und anmelden, also<br>c:\kieselstein\client\bin\kieselstein-client.bat starten und mit Admin, admin anmelden. Nun muss die Standard Maske deines **Kieselstein ERP** erscheinen.
- Als Dienst / Service einrichten
  - den Cmd-Shell wieder stoppen (Strg+C)
  - Windows:
    - auf ?:\kieselstein\dist\bootstrap\service\windows wechseln
    - install-kieselstein-services.bat ausführen
    - In die Dienste wechseln und den Dienst starten und auf automatisch, verzögerter Start stellen
  - Linux
    - auf ?:\kieselstein\dist\bootstrap\service\linux wechseln
    - install-kieselstein-services.sh ausführen
    - Den Dienst mit systemctl start kieselstein-main-server starten
  - nun müssen nach wenigen Sekunden / Minuten unter ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\deployments die drei Dateien mit dodeploy erscheinen.
  - in den Client wechseln / neu starten und z.B. die Benutzerverwaltung oder das System aufrufen.
- Gratulation, dein **Kieselstein ERP** läuft

**Hinweis:**<br>
Gegebenenfalls an die Freigabe des Ports 8080 für den Zugriff innerhalb deines Netzwerkes denken.

## Kurzfassung des Updates
Grundsätzlich sind die **Kieselstein ERP** Updates so gestaltet, dass diese, egal welche Version deine Ausgangsdatenbank hat, jederzeit aktualisiert werden können. Die Unterschiede liegen in der Ausgangsbasis für den Beginn der Liquibase Installation und gegebenenfalls in der Verlagerung der anwenderspezifischen Reports.

Es wird immer auf die aktuelle Version upgedated. Also diejenige die du aus dem Gitlab heruntergeladen hast. Ein Downgrade ist nicht vorgesehen.<br>
Denke daran, dass du ein vollständiges und überprüftes Backup deiner Daten und Reports gemacht hast, bevor du mit dem Update beginnst.

Bewährt hat sich hier, das nächtliche Backup zu nutzen. D.h. es wird, kurz vor dem Beginn des automatischen Backups, der bestehende **Kieselstein ERP** Server gestoppt. Somit können keine Veränderungen an den Daten (von den "normalen" Anwendern) durchgeführt werden und du kannst das Backup, das mit Vacuum schon auch mal einige Stunden dauern kann, als Sicherheit für dein Update nutzen.

### Voraussetzungen
Diese Beschreibung geht davon aus, dass du auf 1.0.6 oder höher aktualisierst. D.h. es müssen folgende Dinge eingerichtet und funktionsfähig sein:
- Liquibase
- Java 11 passend zu deinem Betriebssystem
- aktuelle Kieselstein ERP Release

### Vorgehensweise
- Stoppen des/der Dienste deines Kieselstein ERP Systems
  - Windows: Dienste Stoppen
    - falls von vor 1.0.3 dann aus ?:\kieselstein\dist\bootstrap\service\windows\delete-kieselstein-services.bat ausführen<br>
    **WICHTIG:** Dies vor den weiteren Schritten, da sonst die Pfade nicht mehr stimmen
  - Linux:
    - systemctl stop kieselstein-main-server.service
    - falls die Ausgangsinstallation vor der 1.?.? ist
      - REST Service löschen da dieser ab der 1.?.? im Wildfly integriert ist.<br>
        systemctl stop kieselstein-rest-server.service<br>
        systemctl disable kieselstein-rest-server.service<br>
        rm /etc/systemd/system/kieselstein-rest-server.service
- umbenennen des ?:\kieselstein\dist auf die Version aus ?:\kieselstein\dist\Version.txt sodass dies nun z.B. ?:\kieselstein\dist.0.2.14 lautet
- das dist aus der aktuellen Release auf ?:\kieselstein\dist entpacken
- Datenbank updaten
  - wenn deine Datenbank vor der 0.0.13 ist oder die initiale 17366 ist, dann
    - für Windows aus ?:\kieselstein\dist\bootstrap\liquibase<br>
    run-liquibase.bat changelog-sync --label-filter="0.0.12" ausführen
    - für Linux aus /opt/kieselstein/dist/bootstrap/liquibase<br>
```bash
    ./liquibase.sh changelog-sync --label-filter="0.0.12"
```
ausführen.

  - für alle höheren Versionen und nach obigem<br>
```bash
liquibase.sh update / run-liquibase.bat update
```
ausführen

- Wenn deine Ausgangsinstallation vor der 1.0.3 ist, so müssen die Anwender Reports nach data verschoben werden. D.h. ab der 1.0.3. und höher sind die Anwenderreports unter ?:\kieselstein\data\reports\ (auf das s bei report<u>**s**</u> achten)<br>
D.h. wenn dein Briefpapier bisher unter ?:\kieselstein\dist\wildfly-12.0.0.Final\helium\server\helium\report\report\allgemein\anwender war, so muss der Inhalt des Anwenderverzeichnisses nun nach ?:\kieselstein\data\reports\allgemein
- Solltest du eine Zwischenversion nach der 0.2.14 bis zu 1.0.3 besitzen, so ist die Ausgangsbasis ?:\kieselstein\dist\wildfly-26.1.2.Final\kieselstein\reports\allgemein\
- **<u>WICHTIG:</u>** Nutze die Gelegenheit wirklich nur die Anwenderreports zu übertragen von denen du dir sicher bist, dass diese tatsächlich verwendet werden.<br>
Diese neue Struktur wurde eingeführt um das Update deines Kieselstein ERP, insbesondere unter der Berücksichtigung der Fremdsprachigen Anwenderspezifischen Reports quasi mit wenigen Klicks zu ermöglichen.
- Wenn deine Ausgangsbasis vor der 1.0.3 war, dann nun die Dienste neu installieren, also
  - Windows ?:\kieselstein\dist\bootstrap\service\windows\install-kieselstein-services.bat
  - Linux: /opt/kieselstein/dist/bootstrap/service/linux\install-kieselstein-services.sh
  - nun auch noch an die Anpassung des Workspace.xml für die der Dokumentendatenbank denken. Siehe dazu [Windows](( {{<relref "/docs/installation/01_server/windows_java11/update_von_kieselstein_0_2/#neue-xml-datei" >}} )) bzw. [Linux](( {{<relref "/docs/installation/01_server/debian_java11/update_von_kieselstein_0_2/#neue-xml-datei" >}} ))
  - Umstellung des Sicherungsscriptes:<br>Denke auch daran, dass jetzt die Anwenderreports unter ?:\kieselstein\data\reports sind und daher dieses Verzeichnis gesichert werden muss.

Anmerkung: Ab der Version 1.x.x ist die Restful API im Wildfly integriert. Damit wurden ab der 1.x.x auch der Port für den Zugriff auf die Rest-Services auf 8080 geändert.<br>
Dies muss gegebenenfalls in den peripheren Geräten wie Terminals, mobile App, eigene Apps die die Kieselstein ERP Rest nutzen geändert werden.<br>
Alternativ steht auch ein kleiner Proxy dafür zur Verfügung.

- Kieselstein Dienste starten<br>
  - Windows: Dienste Starten
  - Linux: systemctl start kieselstein-main-server
- Nun die Clients wie oben beschrieben zur Verfügung stellen, also:<br>
  - ?:\kieselstein\dist\clients das kieselstein-client-?.?.?.tar entzippen und die beiden Verzeichnisse bin und lib auf c:\kieselstein\client kopieren
  - im c:\kieselstein\client\bin das kieselstein-client.bat das localhost:8080 auf *Kieselstein Server IP-Adresse*:8080 korrigieren
  - Bewährt hat sich nun das Verzeichnis c:\kieselstein\client zu zippen und auf ?:\kieselstein\dist\clients zur Verfügung zu stellen.
- Prüfen dass dein Kieselstein Server läuft
  - es müssen nach wenigen Sekunden / Minuten unter ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\deployments die drei Dateien mit dodeploy erscheinen.
  - in das Client-Bin-Verzeichnis wechseln und den Client neu starten und z.B. die Benutzerverwaltung oder das System aufrufen. Also: ?:\kieselstein\clients\bin\kieselstein-client.bat 

## Ausrollen der neuen Clients
du findest nun in einem Web-Browser unter http://IP_deines_Kieselstein_Servers:8080 die Startseite deines Kieselstein ERP Servers.<br>
![](Download_Clients.png)  <br>
Hier auf download clients klicken.<br>
In diesem Verzeichnis findest du alles, was du auf deinem Kieselstein ERP Server unter Clients (?:\kieselstein\dist\clients) zur Verfügung gestellt hast.<br>
D.h. du kannst hier z.B. das passende APK für die mobile App zur Verfügung stellen, oder auch das Installationsprogramm für deine Terminals.
