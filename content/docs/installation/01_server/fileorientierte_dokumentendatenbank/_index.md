---
title: "File orientierte Dokumente Datenbank"
linkTitle: "File orientierte Dokumente Datenbank"
categories: ["File orientierte Dokumente Datenbank"]
tags: ["File orientierte Dokumente Datenbank"]
weight: 500
description: >
  Wo werden meine Dokumente gespeichert
---
Dein **Kieselstein ERP** hat grundsätzlich zwei Datenbanken.
- Die reinen ERP-Daten, klein, schlank, schnell. Hier sind die ganzen "Zahlen" gespeichert.
- Die Dokumente-Datenbank. Hier sind die Dokumente abgelegt.<br>Diese gibt es wiederum in zwei Ausführungen.
  - Default als Datenbank-Tabelle. D.h. die "Bilder" der Dokumente (Ausgangsrechnungen, Eingangsrechnungen, Spezifikationen) werden als sogenannter BLOB direkt in der Dokumentendatenbank gespeichert. Das bewirkt wiederum, dass im Laufe der Jahre diese Datenbank immer größer wird, einige zig GByte sind normal, und so das tägliche Backup dieser Daten immer länger dauert. Je nach Installation kann es auch dazu kommen, dass die Nacht (12Std) zu kurz wird und somit kein konsistentes Backup mehr gegeben ist.
  - Daher gibt es als Alternative die sogenannte fileorientierte Dokumentendatenbank.<br>
  Idealerweise wird diese bereits bei der Installation deines **Kieselstein ERP** eingerichtet.
  Es müssen dafür nur zwei Konfigurationsdateien im Kieselstein-ERP Wildfly configuration ausgetauscht / angepasst werden.
  Du benötigst dafür die angepasste:
  - kieselstein.xml
  - kieselstein_jackrabbit.xml<br>
  Hier wird der eigentliche Pfad auf die binären Daten unter <DataStore ..... festgelegt.<br>
  Der Vorteil ist, dass die eigentliche Dokumente nur immer neue Daten sind und somit, z.B. mit rsync, nur eine Veränderungs (Erweiterungs) Sicherung gemacht werden muss und damit die dafür benötigte Zeit wesentlich kürzer ist. Es muss allerdings diese zusätzliche Sicherung auch eingerichtet werden.

## Ein Wort zur Veränderbarkeit in den Dokumenten-Daten
Da diese Frage immer wieder kommt, also kann ein Super Spezialist die Daten der Dokumente verändern? Hierzu muss man folgendes wissen:
- die Dokumente werden als BLOB Binary Large OBject abgelegt. Dies ist nichts anderes als eine Verkettung von Dateiblöcken in denen die eigentlichen Daten hintereinander eingereiht sind. D.h. das herausfinden einer einzelnen Datei, ist schon ziemlich aufwändig und man muss schon sehr genau wissen was man sucht.
- Nun werden z.B. die Ausgangsrechnungen als Jasper-Objecte (in Dateiform) abgelegt.
- werden nun Daten eingefügt (aus 100 wird 1.000) oder Daten verändert, aus einer 0 wird eine 1, so stimmt die Filegröße nicht mehr und oder die Checksumme stimmt nicht mehr. Damit ist die gesamte Dokumentendatenbank unbrauchbar.

Ob man dies nun, mit einer entsprechend kriminellen Energie auch richtig stellen könnte, ist mir nicht bekannt. Hier kommt mit dazu, dass die Person die dieses macht, physikalisch auf deinen **Kieselstein ERP** Server Zugriff haben muss (root). Wenn das alles gelingt, hast du ein anderes Problem.

Daher betrachten wir diese Daten als unveränderbar.
