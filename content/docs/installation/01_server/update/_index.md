---
title: "Update"
linkTitle: "Update"
categories: ["Update"]
tags: ["Update"]
weight: 90
description: >
  Muss ich regelmäßig updaten und wenn ja, was?
---
Für die Beschreibung des Server-Updates deiner **Kieselstein ERP** Installation siehe die betriebssystemspezifischen Updates in den Unterkapiteln
- [Debian Update]( {{<relref "/docs/installation/01_server/update/debian/">}} )<br>
- [Windows Update]( {{<relref "/docs/installation/01_server/update/windows/">}} )

## Allgemeine Punkte zum Thema Updaten / Aktualisieren
Da wir in der Praxis durchaus auch erleben, dass einfach ohne jegliche Notwendigkeit Updates eingespielt werden, hier unsere Gedanken und Informationen dazu.

Wir gehen hier davon aus, dass dein **Kieselstein ERP**-Server in einem sicheren Netzwerk betrieben wird.<br>
Die nachfolgenden Infos sind für Server, die vorne an der Front (= WorldWideWeb) stehen, wie z.B. WebServer, **nicht** zutreffend.

1. Wozu muss aktualisiert werden? Gibt es einen wichtigen Grund?<br>
Nur dann sollte ein Update eingespielt werden.<br>
Es gilt der alte Grundsatz, **never touch a running system!**<br>
Wir können dies nur bestätigen.

2. Die Kieselstein ERP eG mit Ihren Consultants sind **KEINE** IT-Betreuer.<br>
Bitte suche dir einen sehr guten IT-Betreuer. Der auch etwas von IT-Infrastruktur versteht und auch weiß, warum man mechanisch getrennte Backups braucht.

3. Welchen Wert hat dein ERP System für dein Unternehmen?<br>
Welche Dinge werden passieren, wenn dein ERP nicht verfügbar ist?
- ich weiß nicht was ich heute produzieren sollte
- ich weiß nicht welche Waren ich einkaufen sollte
- ich kann keinen Kunden anrufen und ihm erklären, dass er seine Lieferung später bekommt
- ich habe keine Zeiterfassung meiner Mitarbeiter:innen und auch nicht meiner Maschinen
- das kann man noch lange fortsetzen

Wenn nun durch die, bei manchen Menschen ausgeprägte Update-Manie, täglich / wöchentlich neue Versionen in das **Live-System** eingespielt werden, das oft noch ohne ein qualifiziertes Backup / Fallback zu haben, dein ERP System, warum auch immer, nicht mehr funktioniert, so können wir von der Kieselstein ERP eG dazu nur sagen, Pech gehabt. Hoffentlich etwas dazu gelernt.

Unsere Techniker:innen sind Programmierer oder Consultants, aber keine IT-Leute die sich mit Servern und PC's mehr als notwendig herum ärgern.

## Wie alt darf meine Kieselstein ERP Installation sein
Dafür gibt es mehrere Dinge zu beachten.

Wenn deine Installation zu deiner / eurer Zufriedenheit läuft, keine gesetzlichen Änderungen anstehen usw. besteht kein Grund ein Update durchzuführen. Bedenke dabei, dass sich die IT-Welt immer schneller weiterentwickelt. So kann es durchaus sein, dass ein Betriebssystemupdate auch ein Update deines Kieselstein ERP erzwingt. Plane solche Dinge langfristig.

Macht aber deine Installation Problem, so stelle bitte sicher, dass du die aktuelle Kieselstein ERP Version im Einsatz hast. Technische Änderungen werden nur aufbauend auf der aktuellen Version durchgeführt. Sollten mit der aktuellen Version Probleme / Wünsche gegeben sein, so beachte dazu auch das unter [Supportanfrage]( {{<relref "http://localhost:1313/docs/stammdaten/system/supportanfrage/" >}} ) geschriebene. 

## Richtige Update-Vorgehensweise 
1. Ist das Update wirklich notwendig
2. erstelle ein vollwertiges Backup. Du musst im Falle des Falles auf diesen Stand zurückstellen können.<br>
Manche unserer Mitglieder machen das in dem Sinne, dass die VM (virtuelle Maschine) des Echtsystems gesichert und kopiert wird. Beachte dazu das Thema vollwertige Sicherung einer VM mit der PostgresQL!<br>
Nun können am Echtsystem die Updates durchgeführt werden. Sollten sich Probleme herausstellen, kann man auf die qualifizierte und vollwertige Sicherung zurückgreifen.<br>
Oder es werden auf der kopierten VM die notwendigen Tests durchgeführt und erst danach die Updates am Echtsystem ausgerollt.
3. Selbstverständlich ist dein IT-Betreuer greifbar und deine Tests der neuen Version werden zu normalen Bürozeiten durchgeführt und sind nicht zeitkritisch. D.h. ob eine Fehlerbehebung einige Tage dauert, ist nicht relevant.

## Was mache ich, wenn mein System gecrasht ist
In solchen Fällen, welche in der Regel, bei gut gepflegter Hardware, sehr sehr selten auftreten, helfen wir nach besten Kräften. Wir wollen grundsätzlich, dass die Systeme laufen. So ist es uns kürzlich gelungen, ein gecrashtes Windowssystem mit guten vorhandenen Backupdaten innerhalb von wenigen Stunden, <u>nachdem der neue Server wieder zur Verfügung gestanden ist</u>, zum Laufen zu bringen.

Dein Kieselstein ERP läuft immer auf einem Server, auch wenn der Rechner mit vielleicht etwas geringerer Leistung ausgelegt ist.<br> 
Server sind immer mit gespiegelten Platten oder Raid-Platten ausgestattet. Es läuft eine Festplattenüberwachung und die Meldungen des Überwachungssystems werden erst genommen und umgehend, raschest möglich, durch deinen IT-Betreuer beseitigt.<br>
Dass ein qualifiziertes und verifiziertes Backup zur Verfügung steht, ist selbstverständlich.

## Zusammenfassend:
Seid euch des Wertes eurer Daten und Systeme bewusst. Durch die Update Manie wird nur eine Geringschätzung den eigenen Systemen gegenüber zum Ausdruck gebracht.

# Update Prozess für dein Kieselstein ERP:
Vor jedem Update Prozess muss sowohl von der Datenbank als auch von den File-Systemen ein Backup gemacht werden.

## Update Schritte je Betriebssystem:
[Debian Update]( {{<relref "/docs/installation/01_server/update/debian/">}} )<br>
[Windows Update]( {{<relref "/docs/installation/01_server/update/windows/">}} )

## Clients
Nach dem Update des Servers müssen auch die Clients wieder aktualisiert werden.
Siehe hierfür bitte [Installation Clients]( {{<relref "/docs/installation/02_client/">}} ).
> Sollte die Version des Kieselstein-Clients nicht mit der Version des Servers oder der Datenbank übereinstimmen, kommt eine entsprechende Fehlermeldung und kann die Anmeldung nicht durchgeführt werden.

Fehlermeldungen mit Client-Version 0.0.13 oder kleiner:
![](client_error_version_0.0.12.png)
Dies wird nach dem Anmeldeversuch bei Versionen, welche vor der Versionierung erstellt wurden, angezeigt.
Hier muss das Client-Programm aktualisiert werden.

Ab Kieselstein Version 0.0.13 werden die jeweiligen Versionen angezeigt und die Anmeldung verhindert:

![](client_error_version_0.0.13.png)

## Troubleshooting
Wenn die Datenbankversion nicht mit der Wildfly Installation übereinstimmt,
kommt beim Starten des Clients folgende Fehlermeldung:
![](client_database_version_error_new.png)
bzw. bei einem Umstieg von einer Version vor 0.0.13:
![](client_database_version_error.png)

Am Server wird hier auch ein Fehler geloggt (kieselstein-dist/wildfly-12.0.0.Final/standalone/log/server.log)
```
2024-02-28 09:10:26,607 ERROR [com.lp.server.system.ejbfac.SystemFacBean] (EJB default - 8) Database version may not match server version (0.0.14)! Make sure the database was updated.: javax.persistence.PersistenceException: org.hibernate.exception.SQLGrammarException: could not extract ResultSet
        at org.hibernate.jpa.spi.AbstractEntityManagerImpl.convert(AbstractEntityManagerImpl.java:1692)
        at org.hibernate.jpa.spi.AbstractEntityManagerImpl.convert(AbstractEntityManagerImpl.java:1619)
        at org.hibernate.jpa.spi.AbstractEntityManagerImpl.find(AbstractEntityManagerImpl.java:1106)
        at org.hibernate.jpa.spi.AbstractEntityManagerImpl.find(AbstractEntityManagerImpl.java:1033)
        at org.jboss.as.jpa.container.AbstractEntityManager.find(AbstractEntityManager.java:213)
```
Hier muss der Dienst wieder gestoppt werden und das Liquibase update durchgeführt werden.

## Releasehinweise
**TODO Is a comming feature!**
In Zukunft kann es sein, dass nach einem Versionsupdate manuelle Datenbereinigungen durchgeführt werden müssen.
Sollte eine solche Datenbereinigung notwendig sein, wird nach der Anmeldung eines Benutzers eine Liste (wenn mehrere) der noch nicht durchgeführten Aktionen angezeigt.
Die Punkte in der Releasehinweise-Tabelle können nur von dem Kieselstein Admin-Benutzer als erledigt markiert werden.
Solange noch nicht erledigte Releasehinweise vorhanden sind, werden diese jedem Benutzer bei der Anmeldung angezeigt.