---
title: "Update Kieselstein ERP Server unter Debian"
linkTitle: "Debian"
categories: ["Server Update"]
tags: ["Debian Server"]
weight: 10
description: >
  Update Kieselstein ERP Server unter Debian
---
## Applicationsserver Updaten
- Dist-Pack auf den Server kopieren
- Dienste beenden
  - ``systemctl stop kieselstein-main-server.service``
  - ``systemctl stop kieselstein-rest-server.service``
- Alte Programmdateien löschen:
  - ``rm /opt/kieselstein/dist/wildfly-*.Final/standalone/deployments/*.ear``
  - ``rm /opt/kieselstein/dist/wildfly-*.Final/standalone/deployments/*.war``
  - ``rm /opt/kieselstein/dist/wildfly-*.Final/standalone/deployments/*.rar``
  - ``rm /opt/kieselstein/dist/wildfly-*.Final/standalone/deployments/*.deployed``
  - ``rm /opt/kieselstein/dist/apache-tomcat-*/webapps/*.war``
    - und auch alle Verzeichnisse mit kieselstein-rest##*.* löschen
- Dist-Paket über die bestehende Installation entpacken
  - ``tar -xvf <pfad-zum-dist-packet>.gz -C /opt/kieselstein``
- Client-Scripte anpassen
  - Es muss geprüft werden, ob es manuelle Änderungen im Client-Paket für die Startskripte gab und diese eventuell bei den neuen Startscripten nachziehen (Hostname und Splashscreen zum Beispiel).
  - Alte Client-Scripte löschen oder klar markieren als OLD (Siehe im Verzeichnis /opt/kieselstein/dist/clients/)
- Berechtigungen wieder für den Kieselstein Ordner setzen
  - ``chown -R kieselstein:kieselstein /opt/kieselstein``
- Wenn im dist-Verzeichnis mehrere Tomcat oder Wildfly Versionen enthalten sind, prüfen welche verwendet wird und die alten löschen.
  - Prüfen welche Tomcat-Version verwendet wird: 
    - ``cat /opt/kieselstein/dist/bin/launch-kieselstein-rest-server.sh``
    - Suche nach Zeile: export CATALINA_HOME="${KIESELSTEIN_DIST}/apache-tomcat-x.x.x", diese Tomcat Version wird verwendet, die andere(n) müssen gelöscht werden.
  - Prüfen welche Wildfly-Version verwendet wird: 
    - ``cat /opt/kieselstein/dist/bin/launch-kieselstein-main-server.sh``
    - Suche nach Zeile: wildfly_bin_dir=${KIESELSTEIN_DIST}/wildfly-x.x.x.Final/bin, diese Wildfly Version wird verwendet die andere(n) müssen gelöscht werden.
    - **Achtung** Aktuell sind im Wildfly noch die Report-Dateien enthalten sowie eventuelle Anwender-Reports.
      - Es müssen, wenn sich die Wildfly Version ändert, vor dem Löschen des alten Wildfly Ordners alle Anwender-Reports in den neuen Wildfly kopiert werden.
      - /opt/kieselstein/dist/wildfly-*.Final/server/helium/report/*/anwender

## Datenbankmigrationen durchführen
### Hinweis: Wenn Kieselstein Version kleiner als 0.0.13 ist:
Muss die initiale Migration gemacht werden.<br>
**Das gilt auch für den Start mit den Demodaten.**

### Installation Liquibase
Für die Datenbankmigrationen, muss das Tool liquibase installiert werden (mit root Rechten).
```
wget -O- https://repo.liquibase.com/liquibase.asc | gpg --dearmor > liquibase-keyring.gpg && \
cat liquibase-keyring.gpg | sudo tee /usr/share/keyrings/liquibase-keyring.gpg > /dev/null && \
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/liquibase-keyring.gpg] https://repo.liquibase.com stable main' | sudo tee /etc/apt/sources.list.d/liquibase.list
apt-get update
apt-get install liquibase
```
### Initial-Migrationen setzen
- In das Liquibase Verzeichnis gehen
  - ``cd /opt/kieselstein/dist/bootstrap/liquibase``
  - Bestehende Datenstrukturen als bereits migriert markieren
    - ``./liquibase.sh changelog-sync --label-filter="0.0.12"``

#### **ACHTUNG** Wenn die Datenbank von der Default-Installation abweicht, müssen in der /opt/kieselstein/dist/bootstrap/liquibase/liquibase.properties Datei die Verbindungseinstellungen angepasst werden:
```
liquibase.command.url=jdbc:postgresql://localhost:5432/KIESELSTEIN
liquibase.command.username=postgres
liquibase.command.password=postgres
```

## Datenbank Updaten
```
cd /opt/kieselstein/dist/bootstrap/liquibase
./liquibase.sh update
```
![](liquibase_update_output.png)

{{% alert title="Hinweis" color="info" %}}
Sollten hier lange Listen von kryptischen Befehlen kommen, funktioniert das Datenbankupdate nicht.
{{% /alert %}}


## Dienste wieder aktivieren
```
systemctl start kieselstein-main-server.service
systemctl start kieselstein-rest-server.service
```

## Aktualisieren der Clients
Siehe hierfür bitte [Installation Clients]( {{<relref "/docs/installation/02_client/">}} ).

## Verlagerung der Anwenderreports
Bis inkl. der Version 0.2.14 wurden die Anwender-Reports innerhalb des Reportverzeichnisses unter anwender abgelegt.(?:\kieselstein\dist\wildfly-12.0.0.Final\server\helium\report\..) <br>
Ab der Version 1.0.3 (die dazwischen solltest du nicht verwenden) gibt es die Trennung in Dist und Data. [Details siehe bitte]( {{<relref "/docs/installation/10_reportgenerator/anwenderreports/">}} )
