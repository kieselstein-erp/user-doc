---
title: "Update Kieselstein ERP Server unter Windows Server"
linkTitle: "Windows Server"
categories: ["Server Update"]
tags: ["Windows Server"]
weight: 10
description: >
  Update Kieselstein ERP Server unter Windows Server
---
## Für die [Kurzfassung siehe](#kurzfassung-des-updatevorgangs)

Schritt für Schritt Anleitung

# Applikationsserver Updaten
- Dist-Pack auf den Server kopieren
- Dienste beenden
  - **Beide Kieselstein Dienste (Kieselstein Main Server & Kieselstein REST Server) beenden**
  - ![](quit_services.png)
- Alte Programmdateien löschen:
  - Im Verzeichnis ?:\kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments
    - Alle Dateien mit den Dateiendungen **ear**, **rar** und **war** löschen
    - und auch die **deployed** löschen
  - Im Verzeichnis ?:\kieselstein\dist\apache-tomcat-8.5.93\webapps
    - Alle Dateien mit den Dateiendung **war** löschen
    - Alle Verzeichnisse mit kieselstein-rest##*.* löschen
  - im Verzeichnis ?:\kieselstein\dist\clients alle **gz** Dateien löschen
- Dist-Packet über die bestehende Installation entpacken (In der Windows Eingabeaufforderung *cmd.exe*)
  - ``tar -xvf <pfad-zum-dist-packet>.gz -C ?:\kieselstein\``
  - ![](extract_tar.png)
- Client-Scripte anpassen
  - Es muss geprüft werden, ob es manuelle Änderungen im Client-Paket für die Startskripte gab und diese eventuell bei den neuen Startskripten nachziehen (Hostname und Splashscreen zum Beispiel).
  - Alte Client-Scripte löschen oder klar markieren als OLD (Siehe im Verzeichnis ?:\kieselstein\dist\clients\)
- **TODO** Scripte Pfade müssen noch angepasst werden.
- Wenn im dist-Verzeichnis mehrere Tomcat oder Wildfly Versionen enthalten sind, prüfen welche verwendet wird und die alten löschen
  - Prüfen welche Tomcat-Version verwendet wird:
    - ``cat ?:\kieselstein\dist\bin\launch-kieselstein-rest-server.sh``
    - Suche nach Zeile: export CATALINA_HOME="${KIESELSTEIN_DIST}/apache-tomcat-x.x.x", diese Tomcat Version wird verwendet die andere(n) müssen gelöscht werden.
  - Prüfen welche Wildfly-Version verwendet wird:
    - ``cat ?:\kieselstein\dist\bin\launch-kieselstein-main-server.sh``
    - Suche nach Zeile: wildfly_bin_dir=${KIESELSTEIN_DIST}/wildfly-x.x.x.Final/bin, diese Wildfly Version wird verwendet die andere(n) müssen gelöscht werden.
    - **Achtung** Aktuell sind im Wildfly noch die Report-Dateien enthalten so wie eventuelle Anwender-Reports.
      - Es müssen, wenn sich die Wildfly Version ändert, vor dem Löschen des alten Wildfly Ordners alle Anwender-Reports in den neuen Wildfly kopiert werden.
      - ?:\kieselstein\dist\wildfly-*.Final\server\helium\report\*\anwender

# Datenbankmigrationen durchführen
## Hinweis: Wenn Kieselstein Version kleiner als 0.0.13 ist:
Muss die initiale Migration gemacht werden.<br>
**Das gilt auch für den Start mit den Demodaten.**

### Installation Liquibase
Für die Datenbankmigrationen, muss das Tool liquibase installiert werden.
Aktuelle Liquibase Version vom git Repository runterladen:
https://github.com/liquibase/liquibase/releases
Hier bis zu den Assets runterscrollen und dann den passenden (Windows-)Installer auswählen. Z.B.: liquibase-windows-x64-installer-x.x.x.exe<br>
![](Installer_auswaehlen.png)  <br>
Den Installer am Server ausführen und durchklicken.<br>
Bitte darauf achten, dass Add Liquibase to PATH angehakt bleibt.<br>
![](Liquibase_path.png)<br>
**Achtung nach der Installation von Liquibase müssen für die weiteren Arbeiten neue Eingabeaufforderungen (cmd) gestartet werden!** Ein Neustart des Servers ist nicht erforderlich.

{{% alert title="ACHTUNG" color="warning" %}}
Das Liquibase ergänzt die Pfad-Angabe nur Benutzer spezifisch.<br>
Das bedeutet, wenn du danach das Update unter einem anderen Benutzer machst, hast du keinen Zugriff. Unsere Empfehlung. Verschiebe die Pfad-Ergänzung von den Benutzervariablen in die Systemvariablen.<br>
![](Liquibase_path2.png)
{{% /alert %}}

### Initial-Migrationen setzen
- In der Windows Eingabeaufforderung *cmd.exe*
  - In das Verzeichnis wechseln
    - ``cd ?:\kieselstein\dist\bootstrap\liquibase``
    - Bestehende Datenstrukturen als bereits migriert markieren
      - ``liquibase changelog-sync --label-filter="0.0.12"``
  - ![](liquibase_setversion.png)

#### **ACHTUNG** Wenn die Datenbank von der Default-Installation abweicht müssen in der ?:\kieselstein\dist\bootstrap\liquibase\liquibase.properties Datei die Verbindungseinstellungen angepasst werden:
```
liquibase.command.url=jdbc:postgresql://localhost:5432/KIESELSTEIN
liquibase.command.username=postgres
liquibase.command.password=postgres
```

## Datenbank Updaten
In der Windows Eingabeaufforderung *cmd.exe*
```
cd ?:\kieselstein\dist\bootstrap\liquibase
liquibase update
```
![](liquibase_update_output.png)

{{% alert title="Hinweis" color="info" %}}
Sollten hier lange Listen von kryptischen Befehlen kommen, funktioniert das Datenbankupdate nicht.
{{% /alert %}}

## Prüfen, ob das Update funktioniert hat
In der Praxis hat sich bewährt, dass man vor dem Start der Dienste zumindest den Main-Prozess als "Programm" startet.<br>
Idealerweise wechselst du dazu in das Verzeichnis ?:\kieselstein\dist\bin und startest launch-kieselstein-main-server.bat.<br>
Dieser muss problemlos durchlaufen.<br>
Sollte hier eine Meldung ähnlich nachfolgender kommen:<br>
![](Server_startet_nicht.png)<br>
*"(" kann syntaktisch an dieser Stelle nicht verarbeitet werden*<br>
so fehlt in deinen Pfaden der Zugriff auf die postgresQL Runtime Programme.<br>
Trage diese in den [Pfaddefinitionen](#nachtragen-postgresql-runtime-pfad) ein.

- Problemlos durchlaufen bedeutet, dass der Startschirm angezeigt werden muss. Dieser sieht unter Windows wie folgt aus:
![](Server_Start_01.png)

Wenn der Start durchgelaufen ist, was je nach Performance der Maschine auch mal ein paar Minuten dauern kann, so kommt danach die Anzeige des sogenannten Shoptimers.

![](Server_Start_02.png)

Damit siehst du, dass dein Server läuft. Nun kannst du diesen mit Strg+C stoppen und den [Dienst wieder aktivieren](#dienste-wieder-aktivieren).

### Nachtragen PostgresQL Runtime Pfad
- Rechtsklick auf das Windows Start-Symbol
- System
- suche nun erweiterte Systemeinstellungen. Je nach Windows-Version ist dies optisch an einem anderen Platz, aber immer unter System Info zu finden
- Umgebungsvariablen anklicken
![](Umgebungsvariablen.png)
- Suche nun im unteren Bereich (Systemvariablen) die Variable Path
![](Pfad_01.png) und klicke auf "Bearbeiten"
- ![](Pfad_02.png)
Hier muss nun, abhängig von der verwendeten pgAdmin Version der Pfad auf die pgAdmin runtime eingetragen sein. Bei pgAdmin 4 V7 ist dies "c:\Program Files\pgAdmin 4\v7\runtime". Bei pgAdmin 4 V8 ist dies "c:\Program Files\pgAdmin 4\runtime".
- Wenn alles eingetragen ist, beende zumindest den Umgebungsvariablen und den Systemeigenschaften Dialog.
- starte ein **neues** CMD-Fenster und gib psql (+ Enter) ein. Es muss die Abfrage des psql nach dem Benutzerpasswort kommen. Kommt eine Meldung mit *Der Befehl "psql" ist entweder falsch ..... *so ist deine Pfad-Definition falsch, womit auch der Serverstart nicht funktionieren wird.

Funktioniert dies, würde ich mit der oben beschriebenen Prüfung fortfahren.

# Dienste wieder aktivieren
- **Beide Kieselstein Dienste (Kieselstein Main Server & Kieselstein REST Server) starten**
- ![](start_services.png)

# Aktualisieren der Clients
Siehe hierfür bitte [Installation Clients]( {{<relref "/docs/installation/02_client/">}} ).

# Verlagerung der Anwenderreports
Bis inkl. der Version 0.2.14 wurden die Anwender-Reports innerhalb des Reportverzeichnisses unter anwender abgelegt.(?:\kieselstein\dist\wildfly-12.0.0.Final\server\helium\report\..) <br>
Ab der Version 1.0.3 (die dazwischen solltest du nicht verwenden) gibt es die Trennung in Dist und Data. [Details siehe bitte]( {{<relref "/docs/installation/10_reportgenerator/anwenderreports/">}} )

# Probleme / Lösungen
Nachfolgend eine Sammlung von bisher bekannten Problemen und deren Lösungen beim Updateprozess. Vom Grundgedanken her gelten diese Dinge für beide Betriebssysteme, auch wenn die Aufrufe und die erforderlichen Betriebssystemspezifischen Rechte durchaus unterschiedlich sein können.

## Liquibase meldet Checksum Konflikt
Während des Liquibase Updates kommen Fehlermeldungen. Wie z.B.:<br>
> Starte Liquibase am 08:47:40 (Version 4.28.0 #2272, kompiliert am 2024-05-16 19:00+0000)<br>
> Liquibase Version: 4.28.0<br>
> Liquibase Open Source 4.28.0 by Liquibase<br>
> ERROR: Exception Details<br>
> ERROR: Exception Primary Class:  ValidationFailedException<br>
> ERROR: Exception Primary Reason:  Validierung war nicht erfolgreich:<br>
>     Bei 1 ChangeSets stimmt die Prüfsumme nicht mehr mit der Historientabelle überein.<br>
>          changelogs/0.1.1/punkt_ist_dezimaltrenner_parameter.xml::add-parameter::KSE was:<br> > 9:2e4dc472a79dcf110b919b796195b677 but is now: 9:ff143e7c43a38123ddfb731c38d613c9

Siehe dazu ![](Liquibase_Fehlermeldung_Teil1.png)<br>
bzw. <br>
![](Liquibase_Fehlermeldung_Teil2.png)<br>


### Clear Checksums
run-liquibase.bat clear-checksums (für Linux ./liquibase.sh clear-checksums)

Um eventuell falsche Checksums zu löschen und damit weiter zu kommen.<br>
Aufruf von ?:\kieselstein\dist\bootstrap\liquibase\  aus
Siehe zusätzlich auch --help

### Die Checksum in der Tabelle manuell richtig stellen
Ich habe dafür folgendes Script verwendet. Dieses ist sicherlich nicht vollständig und muss gegebenenfalls auf die aktuelle Situation angepasst werden. Die Vorgehensweise sollte damit aber klar sein.<br>
**ACHTUNG:**
Ob diese Vorgehensweise die Konsistenz deiner Daten bzw. die Konsistenz deiner Datenbank zerstört, kann hier nicht geklärt werden. Achte auf die Inhalte der Liquibase Änderungsscripte. Es gilt dies auch für das Clear Checksums

-- korrigieren der checksum der zwischenversion der 0.1.1
> update databasechangelog set md5sum = '9:ff143e7c43a38123ddfb731c38d613c9' where
> md5sum='9:2e4dc472a79dcf110b919b796195b677';
>
> update databasechangelog set md5sum = '9:ff143e7c43a38123ddfb731c38d613c9' where
> md5sum='9:67af6ebc94a79ccc6073d7044b4fe2dc';

## Spalte ist bereits vorhanden
Es kommt beim Update eine Meldung, dass die Spalte xy bereits in der Datenbank eingetragen ist.<br>
![](Spalte_existiert_bereits.png)
Wenn man sich sicher ist, dass in der Tabellen Spalte KEINE Einträge sind, kann man diese einfach löschen und danach das Update erneut ausführen.<br>
Ist dem nicht so, so kann man das betreffende XML entsprechend auskommentieren. Die Update-XMLs, welche Versionsweise organisiert sind, findest du unter <br>
> ?:\kieselstein\dist\bootstrap\liquibase\changelogs\Versionsnummern

Hier das betreffende XML heraussuchen und z.B. die Extension mit .nix ergänzen. Danach das Update erneut ausführen.

Passend zu obiger Fehlermeldung:<br>
?:\kieselstein\dist\bootstrap\liquibase\changelogs\0.0.14\los_material_vollstaendig_parameter.xml 

# Kurzfassung des Updatevorgangs
Wenn auf deinem Kieselstein bereits die Liquibase Version installiert ist, also ab 0.1.0 so hat sich folgende Vorgehensweise bewährt:
- herunterladen des aktuellen [Releases](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/releases)
- stoppen der Dienste kieselstein-rest-server, kieselstein-main-server
- kopieren des ..\kieselstein\dist Verzeichnises auf die bisherige Versionsnummer (siehe d:\kieselstein\dist\VERSION.txt)
- Ausführen des [loeschen.bat](loeschen.bat)
- kopieren des im kieselstein-distpack-0.2.4.tar.gz enthaltenen dist auf das (bereinigte) dist Verzeichnis. Überschreiben aller vorhandenen Dateien.<br>
**ACHTUNG:** Es werden damit (Stand Juni 2024) alle ev. speziell angepassten fremdsprachigen Reports durch den Standard überschrieben. Änderung ist geplant.
- Update der Datenbank durch [inital.bat](initial.bat)
- Anpassen der Clientdateien
  - üblicherweise gibt es im d:\kieselstein\dist\ ein client.zip mit den aktuellen Daten für die Clients.
  - das lib löschen
  - aus ?:\kieselstein\dist\clients\kieselstein-ui-swing-0.2.4.tar.gz das lib in obiges client.zip einkopieren
  - im bin das kieselstein-ui-swing.bat die Zeile mit
  set CLASSPATH=%APP_HOME%\lib\kieselstein-ui-swing-0.2.4.jar; ........ in dein speziell auf deine Installation angepasstes kieselstein-ui-swing.bat übernehmen.
- Dienste starten und gegebenenfalls unter d:\kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments prüfen, dass der Wildfly tatsächlich startet.<br>
Gegebenenfalls auch den Start des RestAPI Servers (Tomcat) durch Aufruf der RestAPI Dokumentation prüfen.
- Nun den Client am Server starten -> sollte gehen
- Infos an die Anwender, dass neuer Client zu installieren ist.