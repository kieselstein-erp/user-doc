rem Initiales setzen der Liquibase Verwaltung
set DB_Version=
%KIESELSTEIN_DIST:~0,2%
cd %KIESELSTEIN_DIST:~2%\bootstrap\liquibase\
if not %errorlevel%==0 goto ende
rem vorher prüfen ob die Versionsverwaltung schon da ist
set PGPASSWORD=postgres
for /f %%i in ('psql -qtAX -U postgres -d KIESELSTEIN -c "SELECT min(database_version) FROM lp_anwender"^&call echo %%^^errorlevel%%^>return.txt') do set DB_VERSION=%%i
set PGPASSWORD=
echo "Errorlevel" %errorlevel%
echo "DB_Version" %DB_VERSION%
pause
if not %errorlevel%==0 goto grundumstellung
if %DB_VERSION%x==x goto grundumstellung
pause
goto update_only
:grundumstellung
echo "Grundumstellung"
pause
call liquibase changelog-sync --label-filter="0.0.12"
pause
:update_only
echo "Updaten"
call liquibase update
echo "DB-Update durchgeführt"
:ende
pause
