rem Script um das Update einer bestehenden Installation möglichst zu vereinfachen
rem Es muss vorher das bestehende Dist-Verzeichnis gesichert werden
del %KIESELSTEIN_DIST%\wildfly-12.0.0.Final\standalone\deployments\*.ear
del %KIESELSTEIN_DIST%\wildfly-12.0.0.Final\standalone\deployments\*.rar
del %KIESELSTEIN_DIST%\wildfly-12.0.0.Final\standalone\deployments\*.war
del %KIESELSTEIN_DIST%\wildfly-12.0.0.Final\standalone\deployments\*.deployed
del /Q %KIESELSTEIN_DIST%\wildfly-12.0.0.Final\standalone\log\*.*
del /Q %KIESELSTEIN_DIST%\launch-kieselstein-main.error.log 

FOR /D %%I IN (%KIESELSTEIN_DIST%\bootstrap\liquibase\changelogs\*.*) DO RMDIR /S /Q "%%I"  

del %KIESELSTEIN_DIST%\apache-tomcat-8.5.93\webapps\*.war
rem nachdem rmdir /S/Q %KIESELSTEIN_DIST%\apache-tomcat-8.5.93\webapps\kieselstein-rest*.* nicht geht das in einer For Schleife
FOR /D %%I IN (%KIESELSTEIN_DIST%\apache-tomcat-8.5.93\webapps\kieselstein-rest*.*) DO RMDIR /S /Q "%%I"  

del %KIESELSTEIN_DIST%\clients\*.gz

echo "dist aus dem heruntergeladenen ZIP über das bestehende kopieren und alles überschreiben"
echo "liquibase update aus %KIESELSTEIN_DIST%\bootstrap\liquibase ausführen"
echo "clients neu einrichten"
pause