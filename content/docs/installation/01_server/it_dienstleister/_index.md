---
title: "IT-Betreuung"
linkTitle: "IT-Betreuung"
categories: ["IT-Betreuung"]
tags: ["IT-Betreuung"]
weight: 400
description: >
  Was sind die Aufgaben deines IT-Betreuers
---
Nachfolgend eine lose Aufstellung der Aufgaben deines IT-Betreuers für den Betrieb deines **Kieselstein ERP**-Systems, welche nicht vollständig ist.

Es sollte dies dazu dienen, den Umfang einer verantwortungsvollen IT-Betreuung zu skizzieren. Ergänzungen dazu sind willkommen.

Bitte beachte auch, dass dein **Kieselstein ERP** zwar die IT massiv nutzt. Die Betreuung deiner IT ist aber in keinster Weise die Aufgabe der Genossenschaft bzw. der Techniker:innen der Consultants, auch wenn das andere Firmen anders handhaben mögen. [Siehe dazu auch Update]( {{<relref "/docs/installation/01_server/update/" >}} ) 

Welche Aufgaben sollte die IT-Betreuung nun übernehmen:
- Zur Verfügungstellung eines geeigneten ERP-Servers
- Prüfung der Performance des Servers, zur Verfügungstellung einer ausreichenden Performance
- Einrichten der Updates sowohl Clients als auch Server
- Netzwerk-Zugriffssicherheit
- Datensicherung inkl. Überprüfung
- WLan Access
- Planung von Erweiterungen (lieber ein geplanter Stillstand als ungeplantes Chaos)
- ist bei Problemen innerhalb von xx Std verfügbar, zumindest an Arbeitstagen und an normalen Wochenenden und ....
- weiß auch, wie man ein **Kieselstein ERP** installiert und die Datenbanken wieder herstellt.

Anmerkung:<br>
Wenn du diese Betreuung selbst übernimmst, gehen wir davon aus, dass eine entsprechende Qualifikation gegeben ist. Du also auch die Verantwortung dafür übernimmst / übernehmen kannst.
Von der Verfügbarkeit des Backups (zu jeder Zeit) bis zu Updates usw..

Für die Einrichtung der Zugangsdaten des (externen) Betreuers [siehe]( {{<relref "/docs/installation/08_nach_der_installation/#mein-it-betreuer-m%C3%B6chte-zugriff" >}} ) 