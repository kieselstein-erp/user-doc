---
title: "Datensicherung"
linkTitle: "Backup"
categories: ["Datensicherung"]
tags: ["Datensicherung"]
weight: 200
description: >
  Einige Gedanken zur Datensicherung
---
Da wir in der Praxis immer erleben, dass die Datensicherung zwar angeblich gemacht wird, aber dann doch nicht funktioniert, hier einige Gedanken dazu.

## Wozu überhaupt Datensicherung?
Wie hoffentlich allgemein bekannt ist, ist gerade die IT nicht vor Fehlern gefeit. D.h. deine Daten und diese sind heutzutage das Herzstück deines Unternehmens, müssen in jedem Falle "jederzeit" zur Verfügung stehen.<br>
Solltest du anderer Meinung sein, schalte PC und Handy aus, was geht dann für dich noch?<br><br>
{{% alert title="Gesetzliche Aufbewahrungspflichten" color="warning" %}}
Beachte dass in den gesetzlichen Aufbewahrungspflichten BAO132, GoDB, ... eine Aufbewahrungspflicht von mindestens 7 (AT) / 10 (DE/LI/CH) Jahren oder mehr gegeben ist. Du musst zum Ende der Aufbewahrungspflicht in der Lage sein, die Dateien sowohl elektronisch als auch Menschen lesbar zur Verfügung zu stellen.
{{% /alert %}}
In diesem Zusammenhang wird es sicherlich auch interessant werden, ob wir in 10 (30) Jahren die Dateiformate überhaupt noch lesen können.

Siehe dazu auch [eRechnung]( {{<relref "/einkauf/eingangsrechnung/er_erechnung/#aufbewahrungspflicht" >}} ) bzw. [generelle Aufbewahrungspflicht]( {{<relref "/verkauf/gemeinsamkeiten/#aufbewahrungspflichten" >}} )

## Wo aufbewahren?
Die Datensicherung muss **mechanisch vom Rest der IT getrennt** sein.<br>
D.h. die Bänder, Kassetten dürfen nach der durchgeführten Datensicherung auch elektrisch nicht mehr mit dem Rest der IT verbunden sein. So werfen RDX Laufwerke bzw. Streamer am Ende des positiv abgeschlossenen Sicherungsvorganges die Kassette aus.<br>
Üblicherweise ist das so organisiert, dass eine Bürokraft sich um die tägliche Sicherung kümmert und wenn das Medium nicht ausgeworfen wurde, die Verpflichtung hat die zuständige Stelle zu informieren.<br>
Dass zumindest die Monatssicherung außer Haus gebracht wird, ist selbstverständlich. Wenn du Tagessicherungen machst, was für eine Produktionsunternehmen eher die Regel ist, so gilt dies für die Wochensicherungen. Das Außer-Haus-Bringen kann ein Bank-Safe sein, oder deine weit genug entfernte Privat-Wohnung. Der Gedanke hier ist immer, wenn jemand / etwas, beide Daten(stämme) zugleich stiehlt, angreift, also z.B. den Server klaut und deine Kassetten aus dem Privathaus, so hast du sowieso ein anderes Problem.

## Wieviele Medien brauche ich?
Um diese Frage zu beantworten, muss man sich überlegen, wie oft ich eigentlich sichern muss.<br>
Der Schlüssel dafür ist für mich:
- wenn zur letzten Sekunde des gerade durchgeführten Backups, welches noch nicht vollständig ist, das System ausfällt, so sind meine (gesamten) Daten weg.
- kann ich diese wiederherstellen?
- wie lange dauert die Nacherfassung?

D.h. wenn ich die Daten wiederherstellen kann und ich diese innerhalb einer Wochenendaktion nacherfassen kann, so ist der Zeitraum, für den ich das mache, jener für den ich meine Sicherung machen muss. Ich würde jedoch immer für jede Woche eine Sicherung machen (man vergisst zu viel). Die Frage ist eher, ob eine Tagessicherung ausreicht, oder ob es stündliche Sicherungen von Teilen sein müssen, oder ob es gespiegelte Server braucht, welche in der Regel nur eine Datenbanktransaktion hinten sind.<br>
Man sollte das auch nicht übertreiben. Wenn ich in fünf Jahren, die Zeitdaten eines Tages von meinen 150 Mitarbeiter:innen nicht exakt habe, wird mich das nicht umbringen.
## Das ist doch alles so teuer?
Ja das stimmt. So kostet aktuell eine 1 TB RDX Kassette ca. 180,- €. Davon brauche ich, bei wöchentlicher Sicherung und Monatsbackup 4 + 12 Stk -> 2.880,- €.<br>
Setzt man nun für die Nacherfassung einen Stundensatz von 80,- € (Inkl. aller Nebenkosten) an, so sind das 36 Std, also 4 Menschtage!<br>
Dazu kommt: Ein IT-Systemausfall ist komplett versicherbar, die Datenwiederherstellung aus einem guten Backup ist darin enthalten, aber das Herzaubern von Daten ist nicht versicherbar.
## Wie lange kann ich mir einen Systemausfall leisten?
Heutzutage ist alles in der IT gespeichert. Also z.B. auch die Telefonnummern in der Telefonanlage, welche ja auch ein IT System ist. Fällt nun die IT aus, woher weißt du die Telefonnummern der Kunden, die du anrufen solltest?<br>
Es gibt schon sehr lange Statistiken, die besagen eine dreitägige Nicht-Verfügbarkeit des IT-Systems haben einen Großteil der Unternehmen nicht überstanden.
### Desaster Recovery Time
Zu obigem Thema kommt auch dazu, wie lange brauche ich, um überhaupt die Daten wiederherstellen zu können. Es ist ja wunderbar, wenn alles, in der Regel in Images gesichert ist. Braucht man nun eine einzelne Datei daraus, so muss man, je nach Backupsystem, das gesamte Image zurücksichern (wo findet man schnell mal 500 GB freien Platz) um dann die eine Datei mit 2 GB daraus extrahieren zu können. Oder eben auch, das Image ist gesichert, muss aber über die Internetleitung übertragen werden. Diese ist im Moment aber gut ausgelastet bzw. die Übertragung von großen Datenmengen ist noch immer problematisch, muss über eine geeignete Software gemacht werden.

Bedenke dies alles mit, auch wie kommt man zu den verschiedensten Passwörtern usw. Wer hat die Daten, wo sind sie? Der/diejenige ist gerade in Urlaub, im Krankenhaus. Woher weiß ich überhaupt von wann (Zeitpunkt) das Backup ist, das ich einspielen möchte.<br>
Wir raten, mach dir einen definitiven Plan, wie im Falle des Falles vorzugehen ist. Das beginnt bei der Beschaffung eines neuen Servers (im nächsten Computershop) und endet bei den Zeiten, die für die verschiedensten Restores benötigt werden. Schreib diese Tabelle mal zusammen und lass diesen schlimmen Fall dann am 24.12. um 16:30 auftreten. Wie sieht der Zeitablauf aus. Wenn du dann auch noch einen Datenverlust hast, denk an die dann sehr kurzen Fristen der DSGVO. Ich wünsche uns allen, dass wir das nie brauchen.
## Reicht es, wenn ich Veränderungen sichere?
Diese Frage kommt immer bei entsprechend großen Datenmengen, also wenn die Nacht für die Sicherung zu kurz wird.<br>
Unsere Erfahrung. Nur Veränderungssicherungen sind extrem gefährlich. Denn im Falle der Rücksicherung braucht nur ein Medium in der Kette defekt zu sein und das Restore ist nicht mehr möglich. Daher **muss** in den Sicherungskonzepten **immer** ein aktuelles und vollständiges Backup auf dem Sicherungsmedium gespeichert sein.
## Wie oft muss ich die Sicherung prüfen?
### Reicht, wenn ich schaue, dass die Files erzeugt werden?
Bitte achte bei der Erzeugung der Backupdateien darauf, dass das Ende des Backups rechtzeitig vor dem Weiterkopieren auf das Sicherungsmedium abgeschlossen ist. Du solltest das monatlich überprüfen.
### muss ich das Restore prüfen
Bitte nimm dir die Zeit, auch den Restore der Daten regelmäßig zu prüfen. Regelmäßig bedeutet in diesem Falle, zumindest jedes halbe Jahr, besser öfter. D.h. wirklich in das Testsystem die Backupdaten reinspielen. Es müssen die Daten des letzten Tages, also des Tages der Datensicherung im Zugriff stehen.<br>
Es kommt leider zu oft vor, dass nur Filezuwachs und Datum geprüft werden, aber in der langen Kette irgendetwas nicht stimmt und du im Falle des Falles, dann doch keine Daten hast.
## ich mache das alles online
ACHTUNG: Im Falle eines Verschlüsselungstrojaners ist die Gefahr, dass durch einen Fehler / Irrtum in der Konfiguration, der Trojaner auch dein Backup verschlüsselt, extrem hoch. Nur sehr sehr versierte IT-Betreuer sind in der Lage dies richtig aufzusetzen.<br>
Daher unser Rat, immer **mechanisch getrennt** aufbewahren. Und auch außer Haus. Denkt an Hochwasser, Blitzschlag, generelle Überspannung, Feuer, Diebstahl.<br>
Ich habe persönlich noch keinen IT-Betreuer kennengelernt, der das wirklich beherrscht. Daher rate ich lieber ein paar Euro mehr für die Sicherheit auszugeben.

Für umfassende Desaster Recovery Szenarien, wendet euch gerne an uns.

Zum Thema Angriffe siehe auch:
Real-Time DDoS Attack Map | NETSCOUT Omnis Threat Horizon, https://horizon.netscout.com/

## Wie alt darf meine Hardware sein?
Auch diese Frage kommt immer wieder.

Der Grundgedanke: Lieber ein geplanter Stillstand als ein unkontrollierter Ausfall.<br>
D.h. es geht darum, dein ERP System (und auch deine restliche Serverlandschaft) "immer" zur Verfügung zu haben. Dies kann man großteils durch vorbeugende Wartung erreichen.

So sagt uns die Statistik, dass **Serverplatten alle vier Jahre** ausgetauscht werden müssen.<br>
**Server Hardware gehört alle sieben Jahre** ausgetauscht.<br>

Auch wenn das die Metallverarbeiter manchmal schreckt, deren teure Maschinen auch mal 30 Jahre halten, hier handelt es sich um hochkomplexe elektronische Geräte, die 24/7 im Einsatz sind und von denen wir erwarten, dass sie immer funktionieren.

Und was dazu kommt, wenn dein Server ausfällt: es steht nicht nur eine Maschine, sondern die ganze Firma. D.h. du hast nicht nur den Produktionsausfall eines Mitarbeiters, sondern deines gesamten Teams. Du kannst keinen Kunden anrufen, weißt nicht was zu liefern / fertigen ist, usw.

Daher: Ja das kostet Geld; ein ungeplanter Stillstand kostet dramatisch mehr und ist immer mit Datenverlust verbunden.