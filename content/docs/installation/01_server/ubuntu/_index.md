---
title: "Installation Kieselstein ERP Server unter Ubuntu"
linkTitle: "Ubuntu"
categories: ["Server Installation"]
tags: ["Ubuntu Server"]
weight: 20
description: >
  Installation Kieselstein ERP Server unter Ubuntu
---
Hier kommen die eventuellen Besonderheiten zu einer Ubuntu Installation rein. Aktuell bitte wie unter Debian beschrieben vorgehen.

www.azul.com/downloads/ ... damit die Werbung nicht kommt

wenn apt nicht gegangen, weil z.B. zu wenig Rechte dann apt reinstall usw.

apt install postgresql holt die aktuellste Postgresversion derzeit 16
ACHTUNG: Installiert OHNE Passwort. daher
su postgres
psql
  damit bist du im Scripteditor commander
  nun


Ubuntu Version lsb_release -a

su postgres -> der Sudo geht irgendwie nicht
Mit sudo su kannst du auch root-Rechte bekommen.

Anmerkung: 
Wenn das Kopieren per Fernwartung nicht geht, dann über den Firefox auf die docs.kieselstein-erp.org gehen und von dort die Kommandos herauskopieren. Im Ubuntu dann mit rechter Maustaste in den Eingabefeldern einfügen.

Je nach Maschine für das filldb sich etwas gedulden. Das kann dauern.

export KIESELSTEIN_JAVA_OPT_XMX=5G
export KIESELSTEIN_JAVA_OPT_XMS=512m

reboot ... = shutdown -r now
