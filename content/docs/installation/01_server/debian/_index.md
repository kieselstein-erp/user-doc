---
title: "Installation Kieselstein ERP Server unter Debian"
linkTitle: "Debian"
categories: ["Server Installation"]
tags: ["Debian Server"]
weight: 10
description: >
  Installation Kieselstein ERP Server unter Debian
---
Den **Kieselstein ERP** Server auf einem frischen Debian installieren.

{{% alert title="in DEUTSCH installieren" color="primary" %}}
Default MUSS das Betriebssystem in de (deutsch) installiert sein und in der richtigen Timezone. Alles andere artet in Arbeit aus.
{{% /alert %}}

## Debian 11 installieren mit root und user
Was mir alles so bei der Installation unter Debian unterkommt.<br>
Es ist dies ein Debian 11 mit Desktop Gnome (Std)
- installiert in einer VM
-	Ram: 6144 MB, Minimaler Ram 4096 MB, Maximaler Ram 8192 MB
- Prozessoren: 4
- Festplatte 64 GB

Boot-Auswahl:
- Grafical install von einer Netzwerk DVD Installation
- Sprache: German / Deutsch
- Land: Österreich / Deutschland / Schweiz / Liechtenstein
- Tastatur: Deutsch
- Rechnername: Großbuchstaben, Ziffern, Minus
- Domain-Name ... leer belassen
- Eine Platte / Laufwerk eingerichtet
- Systemumgebung Debian desktop environment
  - Gnome, default
  - Standard-Systemwerkzeuge

Dauer ca. 30 Minuten

### ssh server einrichten
OpenSSH Server installieren<br>
apt update<br>
apt upgrade<br>
apt install openssh-server<br>

Root Login per SSH ausschalten<br>
nano /etc/ssh/sshd_config<br>
und folgende Zeile ändern<br>
> PermitRootLogin yes

und speichern.

systemctl restart ssh

### Midnight Commander
Ein praktisches Werkzeug ist auch der Midnight Commander.

apt install mc

### gedit
Natürlich kann man die Dateien mit dem Nano bearbeiten. Praktischer ist jedoch der gedit.

### Anmelden und als root arbeiten
Da üblicherweise kein root installiert wird, man aber die Rechte braucht, einfach mit su root und dem eigenen PW anmelden.
{{% alert title="Wichtig" color="warning" %}}
Riesenunterschied zwischen su root der hat nur normalen User context und<br>
su - root<br>
Liefert auch Zugriff auf Programme die echte Root Privilegien erfordern. Z.B. das update-grub
{{% /alert %}}
  
### IP Adresse des Rechners	
ip address

### Java installieren
www.azul.com, klick auf Downloads ![](Azul_Download.png)  <br>
runterscrollen und Java8 LTS, Debian, x86 64.bit, JDK FX auswählen = 8u372b07
- .deb herunterladen
![](Azul_OpenJdk8FX.png)
{{% alert title="nur Java FX verwenden" color="warning" %}}
***WICHTIG:*** Achte darauf, das Java FX auszuwählen. Alle anderen werden immer wieder mal Abstürzen.
{{% /alert %}}

- Terminal öffen, 
- su - root
- auf das home des herunterladenden Users wechseln und ins Downloads, z.B. /home/keg/Downloads
- apt install ./zulu8.70.0.23-ca-fx-jdk8.0.372-linux_amd64.deb
- danach mit java -version überprüfen

### Installation postgresql
- www.postgresql.org/download/linux/debian
- auf Copy Script klicken
- ins Terminal wechseln, in dem man als su - root angemeldet ist
- Einfügen (rechte Maus)
- hinten um -14 für Postgres 14 ergänzen und enter.
  
#### Prüfen ob läuft:
- systemctl is-enabled postgresql
- systemctl status postgresql
  
#### Password für User postgres setzen
- sudo -u postgres psql
- ALTER USER postgres WITH PASSWORD 'postgres'; [**ACHTUNG:** Beachte die GoDB]( {{<relref "/management/finanzbuchhaltung/godb/#unver%C3%A4nderbarkeit-der-daten-manipulationssicherheit" >}} )

- \q (um den psql Editor wieder zu verlassen)
#### Einstellungen anpassen
{{% alert title="Einstellungen anpassen" color="warning" %}}
***WICHTIG:*** Die DateStyle Einstellungen prüfen. Der Default ist im Debian anders als im Windows und in jeder Linux Distribution ist es wiederum anders.
{{% /alert %}}
Als su - root
- Pfad: /etc/postgresql/14/main
- Einstellen postgresql.conf
  - listen_addresses = '*'		# what IP address(es) to listen on;	ist im Debian default auf localhost
  -	datestyle = 'iso, dmy'		# umstellen!<br>
	Auf die Timezone achten. Diese muss auf 'Europe/Vienna' (bzw. Berlin gerne auch Zuerich) stehen

- Gegebenenfalls auch den Zugriff von außen einrichten. D.h.:
	- Einrichten mit User postgres, PW: postgres
	- Ergänzen der pg_hba.conf
		host    all             all             127.0.0.1/32        scram-sha-256
		host    all             all             192.168.xx.0/24		scram-sha-256

-	Datenbankserver neu starten
	systemctl restart postgresql
  
## Installation Kieselstein ERP, Version <aktuelle Version>:
- Verzeichnis anlegen
  - cd /opt
	- mkdir kieselstein
	- cd kieselstein
	- mkdir dist
	- mkdir data

## Installation Liquibase
Für die Datenbankmigrationen, muss das Tool liquibase installiert werden.
```
wget -O- https://repo.liquibase.com/liquibase.asc | gpg --dearmor > liquibase-keyring.gpg && \
cat liquibase-keyring.gpg | sudo tee /usr/share/keyrings/liquibase-keyring.gpg > /dev/null && \
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/liquibase-keyring.gpg] https://repo.liquibase.com stable main' | sudo tee /etc/apt/sources.list.d/liquibase.list
apt-get update
apt-get install liquibase
```

###  JAVA_HOME
Im Debian 11 ist das eigentliche Java unter 	/usr/lib/jvm/zulu-fx-8-amd64/jre/bin/java

Wir setzen die Enviroment Variable unter /etc/environment
-	export JAVA_HOME=/usr/lib/jvm/zulu-fx-8-amd64/jre
-	export KIESELSTEIN_DIST=/opt/kieselstein/dist
-	export KIESELSTEIN_DATA=/opt/kieselstein/data

Anmerkung: Das wirkt nur für den Service nach einem Neustart des gesamten Systems.<br>
Um den Server manuell starten zu können, empfiehlt sich die Einrichtung des unten beschriebenen Start.sh
### Rechte setzen
Als su - root auf /opt wechseln und<br>
chmod 777 kieselstein -R<br>
**ACHTUNG**: Sicherheit!!

### Herunterladen aktuelle Kieselstein ERP Version
[Siehe]( {{<relref "/docs/installation/01_server/#wie-komme-ich-zu-einer-aktuellen-version">}} )

Die Datei von Gitlab herunterladen, mit dem Archivemanager öffnen und in das dist die Dateien aus dem Archivemanager reinkopieren.

### Erzeugen der Datenbanken
In das Verzeichnis /opt/kieselstein/dist/bootstrap/database wechseln
-	sudo -u postgres ./createDb.sh<br>
Anmerkung: damit wird das als User Postgres ausgeführt und daher das PostgresPW entsprechend 4x abgefragt
- danach die Default Daten einfügen<br>
sudo -u postgres ./fillDb.sh<br>
Anmerkung: Auch hier das PW für den user Postgres angeben (1x)

### Einrichten des Dienstes und starten des Servers
als root nach /opt/kieselstein/dist/bootstrap/service/linux<br>
./install-kieselstein-services.sh<br>
ausführen. Damit wird auch **systemctl start kieselstein-main-server.service** gestartet.<br>
- **Wichtig1:** Es muss dafür das environment gesetzt worden sein, also auch an den Reboot denken.
- **Wichtig2:** du musst das als su - root ausführen. Achte auf den Unterschied, wo das Minuszeichen steht. [Linux Knowledge]( {{<relref "/docs/installation/40_knowledge/#allgemein">}} )

#### läuft der Server ?
bewährt hat sich das Laufen des Servers in folgender Reihenfolge zu prüfen:
1. /opt/kieselstein/dist/wildfly-12.0..Final/standalone/deployments<br>
Hier müssen für alle drei Dateien auch .deployed Dateien stehen
2. Prüfen ob die RestAPI geht:<br>
einen Browser starten (Firefox), http://localhost:8080/kieselstein-rest-docs/ muss die Restapi Dokumentation bringen. Hier idealerweise interactive interface nutzen und beide Ping testen.
3. Client starten und mit Admin, admin anmelden.<br>
Also unter /opt/kieselstein/dist/clients/kieselstein-ui-swing-xxxx.tar.gz mit dem Archivemanager öffnen und idealerweise nach /opt/kieselstein/dist/client/ entpacken und danach aus .../bin/ den ./kieselstein-ui-swing starten

### Schriften installieren
Kommt beim Drucken die Meldung<br>
![](Schriftart_nicht_gefunden.png)  <br>
Schriftart 'null' am Server nicht verfügbar, so müssen die in den Reports verwendeten Schriften noch installiert werden.<br>
Üblicherweise wird von **Kieselstein ERP** die Schriftart Arial verwendet. D.h. diese nachinstallieren. Dazu:
  1. herunterladen installationspaket: [ttf-mscorefonts-installer_3.8_all.deb](http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.8_all.deb) -> Download erlauben
  2. in das Downloadverzeichnis wechseln
  3. als su - root<br>
 	apt install ./ttf-mscorefonts-installer_3.8_all.deb<br>
  lädt ein Menge Dateien von sourceforge herunter.<br>
  Am besten danach den Server neu starten (shutdown -r now)




## weiters zu tun
- Backup einrichten  
- Einrichten der Zugriffe von anderen Rechner aus.


## Installation auf Hyper V
Anmerkungen worauf bei der Installation mit Hyper V zu achten ist:
- Generation virtueller Computer -> 2. Generation
- Minimales und maximales Ram angeben
- Anzahl der erlaubten Prozessoren definieren
- SCSI-Controller
  - Hinzufügen von DVD Laufwerk
  - Imagedatei für Debian angeben
- Firmware
  - Bootreihenfolge auf DVD Laufwerk

Anmerkung:
Wenn das Ding nicht von dem ISO Image Boote will, die Netzwerkkarte auf nicht verbunden stellen

### Ansicht
Wenn man nun versehentlich die Ansicht der VM auf 25% stellt, kann man nicht mehr zurück, weil das Menü nicht breit genug ist.

Der Trick, der hilft ist, 
- die Verbindung zur Maschine herstellen,
- die Maus über den Menüpunkt Ansicht stellen
- die VM mit Strg+S starten und sofort
- auf Ansicht klicken
- Dann wieder auf Automatik stellen
Hat mit Debian nichts zu tun, auch wenn im Hyper V da einige andere Dinge fehlen.

### Auflösung im Hyper V
Bildschirmauflösung unter Hyper V ändern, ergänzen des Booteintrages
- als root anmelden (su - root)
- auf etc/default wechseln
- mit nano die datei grub editieren und
- in der Zeile die mit GRUB_CMDLINE_LINUX_DEFAULT beginnt hinten 
  video=hyperv_fb:\<Breite>x<Höhe><br>
	dazuschreiben. z.B.: Breite = 1680 x 1050<br>
	**ACHTUNG**: muss unter Hochkommas sein. D.h. die Zeile lautet dann<br> 	GRUB_CMDLINE_LINUX_DEFAULT="quiet splash video=hyperv_fb:1680×1050"
  - danach update-grub
  - danach neu starten (shutdown -r now)
