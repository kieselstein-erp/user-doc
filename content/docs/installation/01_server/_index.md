---
title: "Installation Kieselstein ERP Server"
linkTitle: "Server"
categories: ["Installation", "Server Installation"]
tags: ["Installation", "Server"]
weight: 100
description: >
  Wie den Kieselstein ERP Applikationsserver installieren
---

Wie für die Installation deines Kieselstein ERP Servers vorgehen

### Vorbereitung für Serverinstallation
Wird die Serverinstallation von uns durchgeführt, so benötigen wir, neben den ganzen Zugangsdaten, 
- bitte die Info auf welches Laufwerk die Installation erfolgen sollte.
- In virtuellen Umgebungen, bitte genau den Server angeben, auf den es installiert werden sollte.
- bitte die Größen Kalkulationen beachten siehe [Anforderungen](anforderungen)

{{% alert title="Ein dringender Rat" color="primary" %}}
Installiere dein **Kieselstein ERP** immer auf einem dedizierten Rechner, der nur für dein **Kieselstein ERP** da ist. Gerne in einer virtuellen Maschine.<br>
Leider hat die Praxis zu oft gezeigt, dass andere Programme durch komische Fehlfunktionen dein Kieselstein zerstört haben. Die Reparatur ist dann entsprechend aufwändig. In einem Falle mussten wir dann die Arbeit des Tages verwerfen und auf das Gott sei Dank vorhandene Backup zurückgreifen.
{{% /alert %}}

### Wie komme ich zu einer aktuellen Version?
Wir freuen uns über jedeN der uns beim Testen der aktuellen Software hilft. Insofern stehen unter dem Link die latest Release und ein aktueller Build zur Verfügung.<br>
D.h. über den [Link](https://gitlab.com/kieselstein-erp/sources/kieselstein) kommst du zum Repository des Kernsystems von **Kieselstein ERP**. ![](Aktuelle_und_latest_Release.png)<br>
Nun kannst du durch Klick auf Latest Release ![](Latest_Release.png) die letzte gültige Version herunterladen.<br>
Alternativ bekommst du den aktuellen Stand durch Klick auf ![](Pipeline_passed.png), welche den neuesten von der Technik / Softwareentwicklung zur Verfügung dargestellten Stand darstellt.<br>

Um die Latest Release zu verwenden, klicke auf den Button und dann auf <br>
![](Download.png)<br>

Um die neueste Version herunterzuladen, Klicke auf Pipeline passed (du solltest nur Build mit einem Status von Passed verwenden).<br>
Nun wird die Build Pipeline angezeigt. Klicke hier im rechten Bereich auf Artefakte herunterladen und wähle build-distpack:archive

{{% alert title="ACHTUNG" color="warning" %}}
Wenn du Zwischenversionen verwendest, solltest du diese ausschließlich für Tests auf einem Testsystem verwenden. Wir haben diese Zwischenversionen weder ausführlich getestet, noch ist sichergestellt, dass diese problemlos zur richtigen Release aktualisiert werden können.
{{% /alert %}}


Entzippe aus dieser Datei das dist Verzeichnis nach ?:\kieselstein\dist

Lege parallel dazu ein Verzeichnis data (?:\kieselstein\data) an

Für die Installation benötigst du auch
- Postgresserver Version 14 oder 15, https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
- PGadmin in der aktuellen Version mindesten PGadmin 4, Version 7.x, https://www.pgadmin.org/download/pgadmin-4-windows/<br>
**ACHTUNG:** Für die Prüfung der Datenbankversion müssen die Runtime-Programme des Postgres mit in den Pfad aufgenommen werden. Dieser ändert sich zwischen den Versionen. Du findest diese aber von der Struktur her unter ?:\Program Files\PostgreSQL\14\bin\, wobei 14 die Version ist.

wir verwenden gerne auch noch:
- Firefox oder Google Chrome (https://www.mozilla.org/de/firefox/new/)
- TotalCommand von Ghilser siehe https://www.ghisler.com/ddownload.htm
- Libre Office siehe https://de.libreoffice.org/download/download/
- Acrobate Reader von Adobe siehe https://get.adobe.com/de/reader/
- Notepad++ siehe https://notepad-plus-plus.org/downloads/
- 7Zip siehe https://www.7-zip.org/download.html muss installiert werden

#### Installation Datenbankserver
Es werden aktuell ausschließlich PostgresQL Version 14 und 15 unterstützt. MS-SQL wird nicht unterstützt. Für eine eventuelle Konvertierung deiner MS-SQL Datenbank wende dich bitte an die Kieselstein ERP eG. Neuere PostgresQL Versionen können funktionieren, sind aber aktuell von uns nicht freigegeben.<br>
Bei der Installation darauf achten, dass nur PostgresSQL Server und die Command Line Tools installiert werden.<br>
![](Datenbankserver.png)<br>
Bei der Installation muss auch das default Passwort für den User Postgres angegeben werden (postgres). Beachte in diesem Zusammenhang auch die Themen der Datenzugriffssicherheit. Aktuell kann das Datenbankpasswort durch die Environmentvariable MAIN_DB_PASS übersteuert werden.

Bitte prüfe, dass auch der Port auf 5432 vorgeschlagen wird.

Beachte, dass die Installation in Windowssystemen idealerweise auf einem eigenen Laufwerk erfolgt. Also z.B. nicht auf C: sondern, vom Betriebssystem unabhängig auf z.B. D:

#### Installation PGadmin
Die abschließende Meldung nach dem Rechner-Neustart kann ignoriert werden.

#### Setzen Environmentvariable
- path Erweiterung auf Postgres "?:\Program Files\PostgreSQL\14\bin\"<br>**Hinweis:** Es muss auch der Dienst auf diesen Pfad zugreifen können.
- KIESELSTEIN_DIST=?:\kieselstein\dist
- KIESELSTEIN_DATA=?:\kieselstein\data
- eventuell ein abweichendes Datenbankpasswort: MAIN_DB_PASS

Es gibt noch weitere optionale Environment-Variablen welche in der README.md 
Datei bei den Start-Scripten (im KIESELSTEIN_DIST/bin/) beschrieben sind.

#### Datenbank einrichten
- ?:\Kieselstein\dist\bootstrap\database\createDb.bat ausführen, 4x postgres PW eingeben
- ?:\Kieselstein\dist\bootstrap\database\fillDb.bat ausführen, 1x postgres PW eingeben

**ACHTUNG:**<br>
Wenn du bestehende Daten übernimmst, schau im Kieselstein ERP Wiki nach wie es nun weiter geht. Ansonsten:

##### Vorbereitete Daten
Wir haben für Testzwecke eine sehr kleine und einfache Musterdatenbank eingerichtet. Diese findest du [hier](./demo_daten/demodaten.zip). Bitte beachte:
- in Deutsch Österreichisch anmelden ![](deAT_Anmelden.png)
- es sind <u>keine</u> Dokumente enthalten
- es ist die integrierte Finanzbuchhaltung aktiviert und damit auch die österreichischen Steuersätze und Formulare usw.

D.h. anstatt des fillDb.bat führst du das fillDb_Demodaten.bat aus.

Eine weitere Variante sind Demodaten in deDE und ohne Fibu. Diese findest du [hier](./demo_daten/demodaten_DE_OF.zip). Bitte beachte:
- in Deutsch Deutsch anmelden
- es sind <u>keine Dokumente</u> enthalten
- die integrierte Finanzbuchhaltung ist deaktiviert und es sind die deutschen Mehrwertsteuersätze eingerichtet.

D.h. anstatt des fillDb.bat führst du das fillDb_Demodaten_DE_OF.bat aus.

{{% alert title="WICHTIG" color="primary" %}}
Diese Daten müssen auf die aktuelle Datenbankversion gehoben werden.<br>
D.h. bevor du mit der Installation fortfährst, muss je nach Betriebssystem das Datenbankupdate ausgeführt werden.
- für Windows [siehe]( {{<relref "update/windows" >}} )
- für Debian (und andere Linux-Versionen) [siehe]( {{<relref "update/debian" >}} )
{{% /alert %}}

##### Leere Datenbank ab der Version 1.x.x
Ab der Version 1.x.x steht anstatt obiger FillDb eine leere Datenbank zur Verfügung.
D.h. der Installationsprozess ändert sich insofern, das du nach dem CreateDb das Liquibase aus dem Liquibaseverzeichnis (?:\kieselstein\dist\bootstrap\liquibase) mit run-liquibase.bat update eine leere aber funktionsfähige Datenbank erzeugst. In dieser sind auch alle Änderungen passend zur installierten Version enthalten.<br>
Für Linux Anwender, ebenfalls in das liquibase Verzeichnis wechseln (/opt/kieselstein/dist/bootstrap/liquibase) und mit ./liquibase.sh update die leere Datenbank erzeugen.

**ACHTUNG:** Liquibase ab Version 4.29 verlangt Java 11

#### Java installieren
- Azul Java 8 mit FX installieren [siehe](#welche-java-version-für-den-server)
- darauf achten, dass Java Home gesetzt wird ![](Java_Home.png)
Prüfen, dass auch installiert durch Command Shell, Java -version
![](Java_Version.png)

#### Client vorbereiten
Aus ?:\Kieselstein\dist\clients\kieselstein-ui-swing-0.0.11.tar.gz mehrfach in die Ordner (zip) reinwechseln bis zum kieselstein-ui-swing-0.0.11. Hier die beiden Verzeichnisse bin und lib z.B. nach ?:\Kieselstein\dist\client kopieren. Nun das ?:\Kieselstein\dist\client\bin\kieselstein-ui-swing.bat editieren und den localhost auf die IP-Adresse des Servers austauschen. Wenn in anderen Sprache gestartet werden sollte, auch noch das -Dloc=de_AT auf z.B. -Dloc=de_DE oder -Dloc=de_CH oder -Dloc=en_US oder -Dloc=it_IT oder pl_PL oder sl_SL austauschen. (Weitere Sprachen, bitte melden)<br>
Das Kieselstein-Desktop-Icon findest du [hier](/docs/installation/02_client/Kieselstein.ico)
<br>
Weiteres zu Sprachen am Client [siehe]( {{<relref "/docs/installation/02_client/#sprachsteuerung-bei-anmeldung" >}} )

#### Server Dienst einrichten
- Mit Administratorrechten eine Command Shell starten.
- Es hat sich bewährt vorher einmalig den Service manuell zu starten, also:<br>
?:\Kieselstein\dist\bin\launch-kieselstein-main-server.bat<br>starten. Üblicherweise, je nach Datenbankgröße und Leistungsfähigkeit des Rechners kann es ein bisschen dauern bis der Dienst läuft. Man sieht das unter ?:\Kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments\<br>Hier muss für alle drei Files deployed stehen. [Wenn nicht siehe]( {{<relref "/docs/installation/01_server/windows#der_server_startet_nicht" >}} )
- nun testweise den Client aus ?:\Kieselstein\dist\client\bin\kieselstein-ui-swing.bat starten 
und mit Admin, admin anmelden.<br>
Geht die Anmeldung, d.h. man sieht die Button Bar ![](Button_Bar.png) so läuft der Zugriff grundsätzlich. Nun den Client beenden und dann den Server (mit Strg+C) stoppen und mit der Einrichtung des Serverdienstes fortsetzen.
- aus ?:\Kieselstein\dist\bootstrap\service\windows\  install-kieselstein-services.bat aufrufen
- in die Dienste / Services wechseln und die beiden Kieselsteindienste starten. Beim ersten Start hat sich die Überwachung des deployments bewährt. Sollte diese mit failed stehen bleiben, dann im log nachsehen. Hier kann man meist, auch wenn es mühsam ist, die Ursache finden.

**Fehlermeldung beim Einrichten des Dienstes**
Kommt beim Einrichten des Dienstes die Meldung
![](Fehler_beim_Dienst_einrichten.png)
so bedeutet dies "nur", dass bisher kein Kieselstein ERP Dienst eingerichtet ist (und nicht upgedated werden konnte).
Du findest trotzdem nun die neu eingerichteten ![](Kieselstein_Dienste_eingerichtet.png) in den Windows-Diensten.

## Tipps und Tricks
Eine lose Sammlung von Tipps, KnowHow und ähnlichem für den/die Consultant im Rahmen der Installation.

**ACHTE immer auf die Sicherheit**
Durch das Öffnen von Ports, gibt es natürlich auch mehr Möglichkeiten das System anzugreifen. Also denke auch an die Verwendung von VPN Tunnel usw..

Dass Betriebssystem, Virenscanner und Firewall immer aktuell sind, ergibt sich schon aus der DSGVO.

Die Themen rund um die Sicherheit, Passwörter abweichend vom default usw. sind im Web ausreichend abgehandelt. [**Kieselstein ERP eG** Mitglieder](https://kieselstein-erp.org/) schauen im Wiki unter dem Suchwort Sicherheit nach.

### Ports die für die Kommunikation benötigt werden
| Port | Zweck |
| --- | --- |
| 8080 | für den Zugriff auf den Kieselstein ERP Applikationsserver |
| 8280 | für den Zugriff auf die RestAPI Ab Version 1.x.x greift die RestAPI auf 8080 zu |
| 5432 | für den Zugriff auf die Postgres-Datenbank |
| 22 | für Linux, SSH Kommunikation |
  
Das sind die Standard Ports. Diese können, z.B. für weitere parallele Installationen am gleichen Rechner auch abweichen.

Siehe dazu: .../kieselstein/dist/bin/launch-kieselstein-main-server.bat bzw. .sh

### Ergänzung an der pg_hba.conf
Hier sollte zusätzlich der gewünschte Netzwerkkreis eingetragen werden, mit dem auch ein Zugriff erlaubt ist. Beispiel:
- host all all 192.168.xx.0/24 scram-sha-256

was bedeutet, dass aus dem Subnetz xx alle IP-Geräte die ein Passwort wissen auf die Datenbank zugreifen können.

Die pg_hba.conf findest du, je nach Betriebssystem unter:
| Betriebssystem | Pfad | Bemerkung |
| --- | --- | --- |
| Windows(R) | c:\Programm Files\PostgreSQL\VV\data\ | |
| Debian | /etc/postgresql/14/main | Achtung: Wenn englisch installiert, muss der datestyle auf 'ios, dmy' gestellt werden. |
| Ubuntu | /etc/postgresql/14/main | ev. muss zusätzlich in der postgresql.conf listen_addresses='*' gestellt werden |
| iOS | .. | |


### Starten der Dienste unter Linux
systemctl start/stop wildfly.service
systemctl start/stop tomcat.service

| Betriebssystem | Pfad |
| --- | --- |
| Debian | /etc/systemd/system/,  |

### Ändern der IP-Adresse des Kieselstein ERP Servers
<u>**ACHTUNG:**</u> Wenn auf dem **Kieselstein ERP** Server die IP Adresse verändert wurde, so muss danach unbedingt der **Kieselstein ERP** Server neu gestartet werden.

### Vorbereitung für die Client-Installation
Vom Client aus auf http://Kieselstein-ERP-SERVER-IP-Adresse:8080 gehen.<br>
![](Welcome.png)<br>
Auf der Willkommensseite findest du links unten den Link zu download clients.<br>
hier werden alle Dateien angezeigt die im Server unter c:\Kieselstein\dist\clients zur Verfügung stehen. D.h. üblicherweise werden hier die angepassten ?:\Kieselstein\dist\client Verzeichnisse als Client.zip einkopiert und somit dem Anwender zur Verfügung gestellt.
Bewährt hat sich auch, dass hier ein aktuelles Java für den Client hinterlegt wird.<br>
https://www.azul.com/downloads/?version=java-11-lts&os=windows&architecture=x86-64-bit&package=jdk-fx#zulu

#### Tools

Tools (VDA-Scann-App, Terminal, Android-Mobile-App, evtl. Java) können im `{KIESELSTEIN_DATA}/tools` Ordner abegelgt werden und über http://Kieselstein-ERP-SERVER-IP-Adresse:8080/tools heruntergeladen werden.

### Welche Java Version für den Server?
Der Basis-Link für das erprobte Zulu Java findest du unter https://www.azul.com/downloads/?package=jdk#zulu
Hier dann für den Server nur Java 8 mit jdk-FX verwenden.
![](Zulu_Java8_FX.png)

Für Windows-User empfiehlt sich die .msi herunterzuladen.

### virtualisierte Betriebsumgebungen
Bitte achte massivst darauf, dass auch in virtuellen Betriebsumgebungen ausreichen Ram zur Verfügung steht. Es kommt leider immer wieder vor, dass manche IT-Betreuer glauben, der VM ausreichend Platz gegeben zu haben, es kommt dieses RAM aber bei der VM nicht an. Hier empfiehlt sich, eine fixe Speicher Zuweisung zu verwenden.

### Wie gehts nun weiter?
