---
title: "Update Kieselstein ERP Server von Version 0.2.x auf 1.x.x unter Debian"
linkTitle: "Migration von Kieselstein 0.2.x"
categories: ["Server Update"]
tags: ["Debian Server"]
weight: 10
description: >
  Migration von Kieselstein Version 0.2.x auf 1.x.x mit Java-11 Installation unter Debian.
---
Wenn eine bestehende Kieselstein Installation mit der Version 0.2.x vorhanden ist, können folgende Schritte für das Update auf Version 1.x.x durchgeführt werden.

Falls möglich, snapshot vom Server machen.

## Kieselstein Dienste deaktivieren
Dienste beenden
- **Beide Kieselstein Dienste (Kieselstein Main Server & Kieselstein REST Server) beenden**
```shell
systemctl stop kieselstein-main-server.service
# REST Service kann gelöscht werden da dieser jetzt im Wildfly integriert ist.
systemctl stop kieselstein-rest-server.service
systemctl disable kieselstein-rest-server.service
rm /etc/systemd/system/kieselstein-rest-server.service
```


## Backup des Kieselstein Dist-Verzeichnis erstellen
Das aktuell installierte Kieselstein in ein eigenes Verzeichnis mit der aktuellen Versionsnummer im Namen wegsichern.
Beispiel: 
```shell
cp -r /opt/kieselstein/ /opt/kieselstein-0.2.10
```

## Neues Java Installieren
- Azul Java 11 mit FX installieren Siehe hierfür auch [Java-Version]({{<relref "docs/installation/01_server/debian_java11/#java-installieren">}}).

## Altes Java Deinstallieren (optional)
```shell
apt remove <Pfald zum alten Java-Deb-Paket.>
```

## JAVA_HOME Umgebungsvariablen anpassen
Mit **vi** oder Nano oder einem anderen Texteditor öffnen.
```shell
vi /etc/environment
```
Nur den Wert für JAVA_HOME anpassen, alles andere kann so bleiben wie es ist.
- JAVA_HOME=/usr/lib/jvm/zulu-fx-11-amd64

Neue Umgebungsvariable laden
```shell
source /etc/environment
```

## Nicht mehr benötigte Anwendungen entfernen
### Reports sichern
Damit die Anwender-Reports nach dem Update wieder zur Verfügung stehen, muss der Reports-Ordner in ein neues Verzeichnis kopiert werden.
```shell
mkdir /opt/kieselstein/dist/wildfly-26.1.2.Final/
mkdir /opt/kieselstein/dist/wildfly-26.1.2.Final/kieselstein/
cp -r /opt/kieselstein/dist/wildfly-12.0.0.Final/server/helium/report/ /opt/kieselstein/dist/wildfly-26.1.2.Final/kieselstein/reports/
```

**WICHTIG:** Dies muss vor dem Entpacken des neuen Dist-Packets erfolgen, damit neuere Versionen der Standard-Reports richtig nachgezogen werden.

{{% alert title="ACHTUNG" color="warning" %}}
Obige Beschreibung gilt nur für die 1.0.x **VOR** der 1.0.3. Ab der 1.0.3 ist auch die Verlagerung der Anwenderreprots nach ../kieselstein/data/report**s** gegeben. Für Details dazu siehe bitte( {{<relref "/docs/installation/10_reportgenerator/anwenderreports" >}} )
{{% /alert %}}

### Alte Programmdateien löschen
Folgende Ordner können nun komplett gelöscht werden:
```shell
rm -rf /opt/kieselstein/dist/apache-tomcat-*
rm -rf /opt/kieselstein/dist/bin
rm -rf /opt/kieselstein/dist/bootstrap
rm -rf /opt/kieselstein/dist/service
rm -rf /opt/kieselstein/dist/wildfly-12.0.0.Final
```

## Installation Kieselstein ERP, Version (aktuelle Version):
### Herunterladen aktuelle Kieselstein ERP Version
[Siehe auch]( {{<relref "/docs/installation/01_server/#wie-komme-ich-zu-einer-aktuellen-version">}} )

Dist-Paket über die bestehende Installation entpacken (In der Windows Eingabeaufforderung *cmd.exe*)
Beispiel:
```bash
# Beispiel für Download des Dist-Pakets.
wget https://gitlab.com/kieselstein-erp/sources/kieselstein/-/jobs/7650408929/artifacts/raw/kieselstein-distpack/build/distributions/kieselstein-distpack-1.0.0-rc.1.tar.gz
# Beispiel für das Entpacken des Dist-Packets.
tar -xvf ./kieselstein-distpack-1.0.0-rc.1.tar.gz -C /opt/kieselstein
```

## Datenbank Updaten
```shell
cd /opt/kieselstein/dist/bootstrap/liquibase/
./liquibase.sh update
```

### Hinweis: Wenn die bestehende Kieselstein Version kleiner als 0.0.13 ist:
> Dann muss dem Liquibase noch mitgeteilt werden, dass es bereit die Grund-Datenstruktur gibt,
> somit muss vor dem Befehl `./liquibase.sh update` noch folgender Befehl aufgerufen werden:
> `./liquibase.sh changelog-sync --label-filter="0.0.12"`

## Einrichten des Dienstes und starten des Servers
```bash
cd /opt/kieselstein/dist/bootstrap/service/linux
./install-kieselstein-services.sh
```

## NGINX Webserver (Optional)
Die Kieselstein-REST Schnittstelle wurde mit dem Update in den Wildfly integriert und ist somit auch über den Port: 8080 erreichbar.

Sollte es notwendig sein, dass diese wie bisher über den Port 8280 erreichbar ist, kann hier ein Nginx-Webserver als Proxy vorgeschaltet werden (Siehe [NGINX Webserver]({{<relref "docs/installation/01_server/debian_java11/#nginx-webserver-optional">}}))

## Läuft der Server ?
Siehe [Läuft der Server]({{<relref "docs/installation/01_server/debian_java11/#läuft-der-server">}})

## Dokumentendatenbank Workspace.xml anpassen
Wenn eine bestehende Dokumentendatenbank existiert, müssen hier folgende Parameter Werte (**Achtung** diese Werte sind 2x in der XML-Datei vorhanden) angepasst werden:
> /opt/kieselstein/data/jackrabbit/workspaces/default/workspace.xml

- driver: javax.naming.InitialContext 
- url: java:/JRDS

Und folgende Parameter können gelöscht werden:
- user
- password

### Beispiel:
#### Alte XML-Datei
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Workspace name="default">
    <FileSystem class="org.apache.jackrabbit.core.fs.db.DbFileSystem">
        <param name="driver" value="org.postgresql.Driver"/>
        <param name="url" value="jdbc:postgresql://${org.kieselstein.db-doc.host}:${org.kieselstein.db-doc.port}/${org.kieselstein.db-doc.name}"/>
        <param name="schema" value="postgresql"/>
        <param name="user" value="postgres"/>
        <param name="password" value="postgres"/>
        <param name="schemaObjectPrefix" value="ws_"/>
    </FileSystem>
    <PersistenceManager class="org.apache.jackrabbit.core.persistence.bundle.PostgreSQLPersistenceManager">
        <param name="driver" value="org.postgresql.Driver"/>
        <param name="url" value="jdbc:postgresql://${org.kieselstein.db-doc.host}:${org.kieselstein.db-doc.port}/${org.kieselstein.db-doc.name}"/>
        <param name="user" value="postgres"/>
        <param name="password" value="postgres"/>
        <param name="schema" value="postgresql"/>
        <param name="schemaObjectPrefix" value="jcr_${wsp.name}_"/>
        <param name="externalBLOBs" value="false"/>
    </PersistenceManager>
    <SearchIndex class="org.apache.jackrabbit.core.query.lucene.SearchIndex">
        <param name="path" value="${wsp.home}/index"/>
    </SearchIndex>
</Workspace>
```

#### Neue XML-Datei:
```xml
<?xml version="1.0" encoding="UTF-8"?><Workspace name="default">
    <FileSystem class="org.apache.jackrabbit.core.fs.db.DbFileSystem">
        <param name="driver" value="javax.naming.InitialContext"/>
        <param name="url" value="java:/JRDS"/>
        <param name="schema" value="postgresql"/>
        <param name="schemaObjectPrefix" value="ws_"/>
    </FileSystem>
    <PersistenceManager class="org.apache.jackrabbit.core.persistence.pool.PostgreSQLPersistenceManager">
        <param name="driver" value="javax.naming.InitialContext"/>
        <param name="url" value="java:/JRDS"/>
        <param name="schema" value="postgresql"/>
        <param name="schemaObjectPrefix" value="jcr_${wsp.name}_"/>
        <param name="externalBLOBs" value="false"/>
    </PersistenceManager>
    <SearchIndex class="org.apache.jackrabbit.core.query.lucene.SearchIndex">
        <param name="path" value="${wsp.home}/index"/>
    </SearchIndex>
</Workspace>
```

### Datei basierte Dokumenten Datenbank

Man kann das jackrabbit config file mit der `DOC_CONFIG` Umgebungsvariable ändern.

Umgebungsvariable auf den folgenden Wert setzen, um die Datei-basierte Dokumenten Datenbank zu verwenden.

DOC_CONFIG=/opt/kieselstein/conf/jackrabbit-datastore-fs.xml

Die dokumente werden standardmäsßig in folgenden Ordner gespeichert. Kann mit der `DOC_REPO` Umgebungsvariable geändert werden.

DOC_REPO=/opt/kieselstein/data/jackrabbit

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Repository>
    <Security appName="Jackrabbit">
        <AccessManager class="org.apache.jackrabbit.core.security.simple.SimpleAccessManager"/>
        <LoginModule class="org.apache.jackrabbit.core.security.simple.SimpleLoginModule">
            <param name="anonymousId" value="anonymous"/>
        </LoginModule>
    </Security>
    <Workspaces rootPath="${rep.home}/workspaces" defaultWorkspace="default"/>
    <DataStore class="org.apache.jackrabbit.core.data.FileDataStore">
        <param name="path" value="${rep.home}/datastore"/>
        <param name="minRecordLength" value="100"/>
    </DataStore>
</Repository>
```

## weiters zu tun
- Clients Updaten (diese benötigen jetzt auch Java 11) [siehe auch]({{<relref "docs/installation/02_client">}})
- Wenn Nginx nicht installiert wurde bei allen Zeiterfassung-Terminals oder anderen Programmen, welche die REST-Schnittstelle verwenden, den Port auf 8080 ändern.

**Empfehlung:**<br>
Insbesondere für die Tests in der ersten Zeit, sollte für ein eventuelles Fallback auf den Clients sowohl die Version für den Java 8 Server als auch für den Java 11 Server parallel vorgehalten werden. Damit man, im schlimmsten Falle, schnell auf die Vorgängerversion zurückwechseln kann.

{{% alert title="ACHTUNG" color="warning" %}}
Wenn der SEPA-Import genutzt wird, muss der Primarykey für i_id der Kontoauszüge angepasst werden. Dazu in der Tabelle fb_sepakontoauszug die größte id suchen und in der lp_primarykey den Eintrag für "sepakontoauszug" auf eine größere ID setzen.
{{% /alert %}}
```bash
update lp_primarykey set i_index = (select max(i_id)+1 from fb_sepakontoauszug) where c_name like '%sepa%';
```
Siehe dazu auch [#307}(https://gitlab.com/kieselstein-erp/sources/kieselstein/-/issues/307).
