---
title: "Installation Kieselstein ERP Server unter Debian"
linkTitle: "Debian JAVA11 (Work in Progress)"
categories: ["Server Installation"]
tags: ["Debian Server"]
weight: 15
description: >
  Installation Kieselstein ERP Server unter Debian
---
Den **Kieselstein ERP** Server auf einem frischen Debian installieren.

{{% alert title="in DEUTSCH installieren" color="primary" %}}
Default MUSS das Betriebssystem in de (deutsch) installiert sein und in der richtigen Timezone. Alles andere artet in Arbeit aus.
{{% /alert %}}

Dauer ca. 30 Minuten

## SSH-Server einrichten
OpenSSH Server installieren
```bash
apt update
apt upgrade
apt install openssh-server
```

## Datenbank
### Installation
```bash
apt-get install wget sudo curl gnupg2
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
apt-get install postgresql-15
```
### Konfiguration anpassen
Mit **vi** oder anderen Texteditor öffnen.
```bash
vi /etc/postgresql/15/main/postgresql.conf
```
Hier müssen das Datums-Format und die Zeitzone umgestellt werden.
```properties
datestyle = 'iso, dmy'
timezone = 'Europe/Vienna'
# Auf die Timezone achten. Diese muss auf ‘Europe/Vienna’ (bzw. Berlin gerne auch Zuerich) stehen
```

### Zugriffskonfiguration
Sollte ein Zugriff von einem anderen System auf die Datenbank notwendig sein, muss hier auch der `listen_addresses` Wert 
gesetzt werden (auf * für alle, bzw. die IP-Adressen, welche unbedingt Zugriff auf die Datenbank brauchen).

Gegebenenfalls auch den Zugriff von außen einrichten. D.h.:
- Einrichten mit User postgres, PW: postgres
- Ergänzen der /etc/postgresql/15/main/pg_hba.conf
host    all             all             127.0.0.1/32        scram-sha-256
host    all             all             192.168.xx.0/24

### Prüfen ob die Datenbank läuft:
```bash
systemctl is-enabled postgresql
systemctl status postgresql
```

### Datenbank-Passwort setzen
Datenbank öffnen
```bash
sudo -u postgres psql
```
Passwort für den Datenbankbenutzer setzen:
```sql
ALTER USER postgres WITH PASSWORD '<Sicheres Passwort>';
exit
```
*Hinweis:* Es sollte ein sicheres Passwort verwendet werden, welches über einen Passwort-Generator erstellt wurde (**Achtung** das Passwort wird hier in folgenden Schritten noch benötigt!).

## Java installieren
### Download der Java Version:
```bash
wget https://cdn.azul.com/zulu/bin/zulu11.74.15-ca-fx-jdk11.0.24-linux_amd64.deb
```

Alternativ kann der Download auch über die Azul-Seite erfolgen.<br>
Siehe hierfür auch [Java-Version]({{<relref "/docs/installation/01_server/anforderungen/#java-version">}}).

### Installation der Java Version:
Den folgenden Befehl im Download-Verzeichnis der Java-Version ausführen.
```bash
apt install ./zulu11.74.15-ca-fx-jdk11.0.24-linux_amd64.deb
```
Installation überprüfen:
```bash
java -version
```

## Installation Liquibase
Für die Datenbankmigrationen, muss das Tool liquibase installiert werden.<br>
**ACHTUNG:** Du musst dafür root-Rechte haben z.B. su - root (und dann dein eigenes PW eingeben)
```bash
wget -O- https://repo.liquibase.com/liquibase.asc | gpg --dearmor > liquibase-keyring.gpg && \
cat liquibase-keyring.gpg | tee /usr/share/keyrings/liquibase-keyring.gpg > /dev/null && \
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/liquibase-keyring.gpg] https://repo.liquibase.com stable main' | tee /etc/apt/sources.list.d/liquibase.list
```

```bash
apt-get update
```

```bash
apt-get install liquibase
```

Auch hier kannst du prüfen, ob die Installation gegangen ist.
```bash
liquibase --version
```

**Hinweis:**<br>
Sollte die Installation des Liquibase nicht möglich sein, muss das gpg aktualisiert werden. Vermutlich ist das ein Signal für eine veraltete Installation, daher das gesamte Betriebssystem aktualisieren.

## Schriften Installieren
Damit die Reports funktionieren, muss die Schriftart Arial installiert werden.
Dies kann unter Linux so durchgeführt werden.
```bash
wget http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.8_all.deb
apt install ./ttf-mscorefonts-installer_3.8_all.deb
```


## Umgebungsvariablen setzen
Mit **vi** oder **nano** oder anderem Texteditor öffnen.

```bash
vi /etc/environment
```

Wichtig! Die Umgebungsvariablen müssen OHNE `export` hinzugefügt werden.

- JAVA_HOME=/usr/lib/jvm/zulu-fx-11-amd64
- KIESELSTEIN_DIST=/opt/kieselstein/dist
- KIESELSTEIN_DATA=/opt/kieselstein/data
- MAIN_DB_PASS=*(Sicheres Passwort welches für den Datenbank-Benutzer verwendet wurde)*
- DOC_DB_PASS=*(Sicheres Passwort welches für den Datenbank-Benutzer verwendet wurde)*

Optional kann auch die `KIESELSTEIN_WILDFLY_CONFIG` Umgebungsvariable gesetzt werden.
Wichtig ist dabei das die Variable keine anderen Variablen beinhalten darf.

Bsp: `KIESELSTEIN_WILDFLY_CONFIG=/opt/kieselstein/data/wildfly`

Es müssen dann noch einmalig folgende files vom wildfly ordner in den KIESELSTEIN_WILDFLY_CONFIG kopiert werden.

/opt/kieselstein/dist/wildfly-26.1.2.Final/standalone/configuration/
* application-roles.properties
* application-users.properties
* mgmt-groups.properties
* mgmt-users.properties

Danach noch einen Applikations-User mit `/opt/kieselstein/dist/wildfly-26.1.2.Final/bin/add-user.sh` hinzufügen.

```shell
source /etc/environment
```

oder Server Neustarten
```bash
reboot
```

## Installation Kieselstein ERP, Version (aktuelle Version):
Verzeichnisse anlegen:
```bash
mkdir /opt/kieselstein
mkdir /opt/kieselstein/dist
mkdir /opt/kieselstein/data
```

### Herunterladen aktuelle Kieselstein ERP Version
[Siehe auch]( {{<relref "/docs/installation/01_server/#wie-komme-ich-zu-einer-aktuellen-version">}} )

Dist-Paket über die bestehende Installation entpacken (In der Windows Eingabeaufforderung *cmd.exe*)
Beispiel:
```bash
# Beispiel für Download des Dist-Pakets.
wget https://gitlab.com/kieselstein-erp/sources/kieselstein/-/jobs/7650408929/artifacts/raw/kieselstein-distpack/build/distributions/kieselstein-distpack-1.0.0-rc.1.tar.gz
# Beispiel für das Entpacken des Dist-Packets.
tar -xvf ./kieselstein-distpack-1.0.0-rc.1.tar.gz -C /opt/kieselstein
```

### Datenbank Initialisieren
```bash
cd /opt/kieselstein/dist/bootstrap/liquibase/
./createdb.sh
```
Hier muss dann 4x das Passwort für den Postgres-Benutzer eingegeben werden.

Danach auch die default Datenbank auf den aktuellen Stand deines Kieselstein Servers heben.
```bash
./liquibase.sh update
```

## Einrichten des Dienstes und starten des Servers
```bash
cd /opt/kieselstein/dist/bootstrap/service/linux
./install-kieselstein-services.sh
```
ausführen. Damit wird auch **systemctl start kieselstein-main-server.service** gestartet.<br>
**ACHTUNG**: Es muss dafür das environment gesetzt worden sein

## WildFly user hinzufügen

Wird für BasicAuth bei EDIFACT und CLEVERCURE benötigt.

KIESELSTEIN_DATA/wildfly Ordner erstellen und die folgenden Dateien aus KIESELSTEIN_DIST/wildfly-26.1.2.Final/standalone/configuration hineinkopieren.

* application-roles.properties
* application-users.properties
* mgmt-groups.properties
* mgmt-users.properties
 
## NGINX Webserver (Optional)
Sollte für die REST-Schnittstelle ein eigener Port notwendig sein. Damit der Zugriff zum Beispiel über das Internet zur Verfügung gestellt werden kann oder bestehende Zeiterfassung-Terminals oder andere Anwendungen bereits den Port: 8280 verwenden kann ein Nginx-Webserver als Proxy hierfür installiert werden.
```bash
# Nginx-Webserver installieren:
apt install nginx
# Konfiguration für Kieselstein verlinken:
ln -s /opt/kieselstein/dist/bootstrap/nginx/kieselstein.conf  /etc/nginx/sites-enabled/kieselstein-main.cfg
# Testen, ob die Konfiguration gültig ist:
nginx -t
# Nginx-Webserver Dienst neustarten
systemctl restart nginx
```
Für das Freischalten im Internet sollte auf jeden Fall ein SSL-Zertifikat noch hinterlegt werden (siehe hier auch [Configuring HTTPS servers](http://nginx.org/en/docs/http/configuring_https_servers.html) bzw. [Nginx-Webserver]({{<relref "/docs/installation/01_server/anforderungen/#nginx-webserver">}}))

Zum Überpüfen, ob der Nginx funktioniert und mit dem Kieselstein kommuniziert, kann die Url:
**http://(Name oder IP-Adresse des Servers):8280/kieselstein-rest/services/rest/api/v1/system/ping**
aufgerufen werden.

### läuft der Server ?
bewährt hat sich das Laufen des Servers in folgender Reihenfolge zu prüfen:
1. /opt/kieselstein/dist/wildfly-26.1.2.Final/standalone/deployments/<br>
   Hier müssen für alle drei Dateien auch .deployed Dateien stehen
2. Prüfen, ob die RestAPI geht:<br>
   einen Browser starten, http://localhost:8080/kieselstein-rest-docs/ muss die Restapi Dokumentation bringen. 
   Bzw. auch mit dem Nginx-Port http://localhost:8280/kieselstein-rest-docs/ aufrufen (falls Nginx-Installiert wurde).
3. Client starten und mit Admin, admin anmelden.<br>

## weiters zu tun
- Backup einrichten
- Einrichten der Zugriffe von anderen Rechner aus.
