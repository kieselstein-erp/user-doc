---
title: "Anforderungen Kieselstein ERP Server"
linkTitle: "Anforderungen"
categories: ["Server Anforderungen"]
tags: ["Server Anforderungen"]
weight: 100
description: >
  Anforderungen an den Kieselstein ERP Server
---
Die Anforderungen an den **Kieselstein ERP** Server sind von verschiedenen Eckpunkten abhängig. Im wesentlichen sind dies Benutzeranzahl und Betriebssystem.

## Server
### Windows
Die Verwendung eines 64Bit Betriebssystems ist heutzutage selbstverständlich.<br>
Bei Verwendung von Windows gehen wir davon aus, dass dieses, auch für den Testbetrieb, ordnungsgemäß lizenziert ist.
| Eigenschaft | Betriebssystem | Datenbank | Kieselstein<br> appServer| je Power User | je std. User |
| --- | --- | --- | --- | --- | --- |
| Kerne | 1 | 1 | 1 | 0,5| 0,1 |
| Ram [GB] | 4 | 2 | 2 | 0,1 | 0,01 |
| Platte [GB] | 100 | 70 | 70  | | |

Ganz wesentlich in die Geschwindigkeit des Gesamtsystems geht die Zugriffsgeschwindigkeit der / auf die Festplatten ein. Wir raten, auch wenn es teuer ist, zu **SSD Platten**. Sollte dies nicht möglich sein, zu Raid10.<br>
ACHTUNG: Bei Virtualisierung kommt es immer wieder vor, dass die Platten zwar sehr schnell sind, aber da alle virtuellen Maschinen auf die gleiche Hardware zugreifen, ist im Endeffekt der Festplattencontroller der Flaschenhals. Hier ist zusätzlich zu bedenken, dass die Zugriffe oft mit kleinen Datenmengen erfolgen. D.h. die in der IT üblichen Geschwindigkeitsmessungen haben keine Aussagekraft (die Optimierungsalgorithmen der VM's greifen leider nicht).

Die Größe der Platten hängt auch an der Verwendung der Dokumentenablage. Kann also, bei intensiver Nutzung, was im Sinne des Systems ist, auch deutlich darüber hinaus anwachsen.

In dieser Betrachtung ist der, für das unumgängliche zumindest tägliche Backup benötigte Platz, nicht berücksichtigt. Auch dies hängt von der Datenmenge der Installation ab. So beginnen Neuinstallationen bei einigen wenigen GB und wachsen danach schon mal auf 150 GB oder mehr an.

Eine wesentliche Frage in diesem Zusammenhang ist das Thema des Backups bzw., wie alt dürfen die Daten bei einem eventuellen Ausfall sein. Das geht bis hin zu gespiegelten Datenbankservern.<br>
Diese Dinge klären wir am besten in einem persönlichen Gespräch.

**ACHTUNG:** Es muss am Server IMMER der doppelte Speicherplatz wie dein Datenbankbackup belegt frei sein.

#### Welche Windows Betriebssysteme können genutzt werden?
Auch das hängt vom Einsatzbereich ab.<br>
Für eine anfänglich kleine Installation mit 2 Usern reicht auch mal ein Windows11 (Windows 10 wird nicht mehr lange zur Verfügung stehen). Für den Einsatz in einem Unternehmen mit 5 Usern oder mehr, raten wir zum Einsatz eines Server Betriebssystems. Da der ganze grafische Overhead auf dem Server nicht benötigt wird, gerne auch Linux, Debian.<br>
In jedem Falle aber bitte ausschließlich **64 Bit**-Systeme.

### Benötigte Werkzeuge für den Server
#### Postgre-SQL Datenbank 
Es werden die Version 14 oder 15 der PostgreSQL Datenbank unterstützt.
Siehe Downloads unter: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

#### Liquibase Migrationstool
Für die Datenbankmigrationen wird das [Liquibase](https://www.liquibase.com/) Tool benötigt.
Die aktuelle Liquibase Version kann vom git Repository heruntergeladen: https://github.com/liquibase/liquibase/releases werden. 
Hier bis zu den Assets runterscrollen und dann den passenden (Bsp.: Windows-)Installer auswählen. Z.B.: liquibase-windows-x64-installer-x.x.x.exe

#### Java-Version
**ACHTUNG** Java Version 11 ist erst ab der Kieselstein-Version 1.0.0 und höher unterstützt; vorher bitte Java-Version 8 verwenden!

Es wird für den Server die Azul-Java Version 11 benötigt.
Siehe dazu www.azul.com, klick auf Downloads ![](Azul_Download.png) <br>
runterscrollen und Java11 LTS, (das jeweilige Betriebssystem auswählen), x86 64.bit, JDK FX auswählen.
![](Azul_Java_Version.png)

#### Nginx Webserver
Wenn die REST-Schnittstelle oder andere Dienste über das Internet zur Verfügung stehen sollen, dann sollte dies über einen [Nginx-Webserver](https://nginx.org/en/download.html) werden.
Es wird für die Installation eine Default-Konfiguration (unter Kieselstein/dist/bootstrap/) mit ausgeliefert, wo der Port 8280 ohne SSL vorkonfiguriert ist (wie es bei früheren Versionen mit dem Tomcat durchgeführt wurde).

Unter [nginxtutorials.com](https://nginxtutorials.com/nginx-lets-encrypt/) ist auch beschrieben wie mit Nginx ein *Lets Encrypt*-SSL-Zertifikat erstellt werden kann.

Alternativ kann natürlich auch ein "Selbstsigniertes Zertifikat" für SSL mit Nginx verwendet werden.

### weitere Last am Server
Es gibt inzwischen noch eine Anzahl von Funktionen / Zugriffen auf den Server, die für jeden Zugriff mehr oder weniger Serverlast ziehen. Auch diese sind in den obigen Überlegungen zu berücksichtigen.
- RestAPI, je nach Intensität der Nutzung
- ZE-Terminals je nach Buchungshäufigkeit. Es ist ein entsprechender Unterschied, ob nur Kommt/Geht (Anwesenheitszeit) gebucht wird, oder die papierlose Fertigung mit Zeichnungen, Auswahllisten usw. verwendet wird. Insbesondere die Zeitverteilungsberechnungen können entsprechende Serverlast erzeugen.
- Anwesenheitsliste(n): Auch hier macht es einen großen Unterschied, ob 10 Terminals im 30 Sekunden Takt jeweils eine neue Liste anfordern, oder ob diese ober den Anwesenheitslisten Proxy eigenständig aufbereitet werden.
- die Listenauswertungen per Web-Pages. Auch diese Auswertungen brauchen entsprechende CPU Leistung.

## Client
Hier reichen heutzutage (2023) übliche Standard PC's, mit mindestens 64 Bit Betriebssystemen und 8 GB Ram (wenn nur der Kieselstein-ERP Client läuft). Empfohlen 16 GB Ram, gerne mehr.<br>
Auch hier raten wir zur Verwendung von SSD Platten, da auch diese massiv in das Geschwindigkeitsverhalten eingehen.
