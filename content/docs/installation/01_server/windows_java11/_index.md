---
title: "Installation Kieselstein ERP Server unter Windows"
linkTitle: "Windows JAVA11 (Work in Progress)"
categories: ["Server Installation"]
tags: ["Windows Server"]
weight: 35
description: >
  Installation Kieselstein ERP Server unter Windows
---
Den **Kieselstein ERP** Server auf einem frischen Windows installieren.

## Installation Datenbankserver
Es werden aktuell ausschließlich PostgresQL Version 14 und 15 unterstützt. MS-SQL wird nicht unterstützt. Für eine eventuelle Konvertierung deiner MS-SQL Datenbank wende dich bitte an die Kieselstein ERP eG. Neuere PostgresQL Versionen können funktionieren, sind aber aktuell von uns nicht freigegeben.<br>
Bei der Installation darauf achten, dass nur PostgresSQL Server und die Command Line Tools installiert werden.<br><br>
![](../Datenbankserver.png)<br><br>
Bei der Installation muss auch das Passwort für den User Postgres angegeben werden.<br>
*Hinweis:* Es sollte ein sicheres Passwort verwendet werden, welches über einen Passwort-Generator erstellt wurde (**Achtung** das Passwort wird hier in folgenden Schritten noch benötigt!).

## Java installieren
- Azul Java 11 mit FX installieren Siehe hierfür auch [Java-Version]({{<relref "/docs/installation/01_server/anforderungen/#java-version">}}).
- darauf achten, dass Java Home gesetzt wird<br> ![](Java_Home.png)<br>
- Prüfen, dass Java auch installiert wurde durch Command Shell,
  ```shell
  Java -version
  ```
![](java_version.png)

## Setzen Environmentvariable
- path Erweiterung auf Postgres "?:\Program Files\pgAdmin 4\v6\runtime"
- KIESELSTEIN_DIST=?:\kieselstein\dist
- KIESELSTEIN_DATA=?:\kieselstein\data
- MAIN_DB_PASS=(Das oben definierte Datenbank-Passwort)
- DOC_DB_PASS=(Das oben definierte Datenbank-Passwort)

Optional kann auch die `KIESELSTEIN_WILDFLY_CONFIG` Umgebungsvariable gesetzt werden.
Wichtig ist dabei das die Variable keine anderen Variablen beinhalten darf.

Bsp: `KIESELSTEIN_WILDFLY_CONFIG=?:\kieselstein\data\wildfly`

Es müssen dann noch einmalig folgende files vom wildfly ordner in den KIESELSTEIN_WILDFLY_CONFIG kopiert werden.

?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\configuration
* application-roles.properties
* application-users.properties
* mgmt-groups.properties
* mgmt-users.properties

Danach noch einen Applikations-User mit `\opt\kieselstein\dist\wildfly-26.1.2.Final\bin\add-user.bat` hinzufügen.

Es gibt noch weitere optionale Environment-Variablen welche in der README.md Datei bei den Start-Scripten (im KIESELSTEIN_DIST/bin/) beschrieben sind.

![umgebungs_variablen.png](umgebungs_variablen.png)

**Achtung**: Damit die Umgebungsvariablen für den Kieselstein-Dienst funktionieren, müssen diese als Systemvariablen hinterlegt werden.

## Installation Liquibase
Für die Datenbankmigrationen muss das Tool liquibase installiert werden.<br>
Aktuelle Liquibase Version vom git Repository runterladen:<br>
https://github.com/liquibase/liquibase/releases<br>
Hier bis zu den Assets runterscrollen und dann den passenden (Windows-)Installer auswählen. Z.B.: liquibase-windows-x64-installer-x.x.x.exe<br>
![](../update/windows/Installer_auswaehlen.png)  <br>
Den Installer am Server ausführen und durchklicken.<br>
Bitte darauf achten, dass Add Liquibase to PATH angehakt bleibt.<br>
![](../update/windows/Liquibase_path.png)<br>
**Achtung nach der Installation von Liquibase müssen für die weiteren Arbeiten neue Eingabeaufforderungen (cmd) gestartet werden!** Ein Neustart des Servers ist nicht erforderlich.

{{% alert title="ACHTUNG" color="warning" %}}
Das Liquibase ergänzt die Pfad-Angabe nur benutzerspezifisch.<br>
Das bedeutet, wenn du danach das Update unter einem anderen Benutzer machst, hast du keinen Zugriff mehr.<br>
Unsere Empfehlung:<br>
Verschiebe die Pfad-Ergänzung von den Benutzervariablen in die Systemvariablen.<br>
![](Liquibase_path2.png)
{{% /alert %}}

## Installation Kieselstein ERP, Version (aktuelle Version):
### Herunterladen aktuelle Kieselstein ERP Version
[Siehe auch]( {{<relref "/docs/installation/01_server/#wie-komme-ich-zu-einer-aktuellen-version">}} )

Dist-Paket über die bestehende Installation entpacken (In der Windows Eingabeaufforderung *cmd.exe*)
Beispiel:
```bash
tar -xvf <pfad-zum-dist-packet>.gz -C ?:\kieselstein\
```

## Datenbank Initialisieren
Die installierte ?:\kieselstein\dist\bootstrap\liquibase\createdb.bat ausführen.
Hier muss dann 4x das Passwort für den Postgres-Benutzer eingegeben werden.

In der Command Shell in das Verzeichnis navigieren und mit liquibase den update Befehl durchführen:
```shell
cd ?:\kieselstein\dist\bootstrap\liquibase
run-liquibase.bat update
```

## Einrichten des Dienstes und starten des Servers
Mit Administrationsrechten das Install-Script ?:\kieselstein\dist\bootstrap\service\windows\install-kieselstein-services.bat ausführen.

## NGINX Webserver (Optional)
Sollte für die REST-Schnittstelle ein eigener Port notwendig sein. Damit der Zugriff zum Beispiel über das Internet zur Verfügung gestellt werden kann oder bestehende Zeiterfassung-Terminals oder andere Anwendungen bereits den Port: 8280 verwenden, kann ein Nginx-Webserver als Proxy hierfür installiert werden.

Mit Administrationsrechten das Install-Script ?:\kieselstein\dist\bootstrap\service\windows\install-kieselstein-nginx-service.bat ausführen.

Für das Freischalten im Internet sollte auf jeden Fall ein SSL-Zertifikat noch hinterlegt werden (siehe hier auch [Configuring HTTPS servers](http://nginx.org/en/docs/http/configuring_https_servers.html) bzw. [Nginx-Webserver]({{<relref "/docs/installation/01_server/anforderungen/#nginx-webserver">}}))

Zum Überprüfen, ob der Nginx funktioniert und mit dem Kieselstein kommuniziert, kann die Url:
**http://(Name oder IP-Adresse des Servers):8280/kieselstein-rest/services/rest/api/v1/system/ping**
aufgerufen werden.
## TODO Hinterfragen, ob die Config gleich ins richtige Verzeichnis vom Dist-Pack geladen werden kann oder ob das manuell gemacht werden muss.

## läuft der Server?
bewährt hat sich, das Laufen des Servers in folgender Reihenfolge zu prüfen:
1. ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\deployments/<br>
   Hier müssen für alle drei Dateien auch .deployed Dateien stehen
2. Prüfen ob die RestAPI geht:<br>
   einen Browser starten, http://localhost:8080/kieselstein-rest-docs/ muss die Restapi Dokumentation bringen.
   Bzw. auch mit dem Nginx-Port http://localhost:8280/kieselstein-rest-docs/ aufrufen (falls Nginx-Installiert wurde).
3. Client starten und mit Admin, admin anmelden.<br>

## weiters zu tun
- Backup einrichten  
- Einrichten der Zugriffe von anderen Rechner aus.

## Server startet nicht, was tun?
Wenn du Daten von anderen Installationen übernimmst, muss beim ersten Start deines Servers der Index der Dokumentendatenbank neu aufgebaut werden. Dies wird grundsätzlich vom Server unterstützt. Es kann aber unter Umständen so lange dauern, dass der Server von sich aus abbricht.<br>
D.h. gegebenenfalls den Server mehrfach starten und oder zusätzlich das Timeout **temporär** höher drehen. D.h. im launch-kieselstein-main-server(.bat)

### Linux (ca Zeile 60)
MAIN_SERVER_OPTS="${MAIN_SERVER_OPTS} -Djboss.as.management.blocking.timeout=3600"

### Windows (ca Zeile 90)
set MAIN_SERVER_OPTS=%MAIN_SERVER_OPTS% -Djboss.as.management.blocking.timeout=3600

hinzufügen. Erhöht das Timeout auf eine Stunde.

