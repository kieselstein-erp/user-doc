---
title: "Update Kieselstein ERP Server von Version 0.2.x auf 1.x.x unter Windows"
linkTitle: "Migration von Kieselstein 0.2.x"
categories: ["Server Update"]
tags: ["Windows Server"]
weight: 10
description: >
  Migration von Kieselstein Version 0.2.x auf 1.x.x mit Java-11 Installation unter Windows.
---
Wenn eine bestehende Kieselstein Installation mit der Version 0.2.x vorhanden ist, können folgende Schritte für das Update auf Version 1.x.x durchgeführt werden.

## Kieselstein Dienste deaktivieren
Dienste beenden
- **Beide Kieselstein Dienste (Kieselstein Main Server & Kieselstein REST Server) beenden**<br>
![](quit_services.png)
- nun die Dienste deinstallieren, also:<br>
  ```?:\kieselstein\dist\bootstrap\service\windows\delete-kieselstein-services.bat```

## Backup des Kieselstein Dist-Verzeichnis erstellen
Das aktuell installierte Kieselstein in ein eigenes Verzeichnis mit der aktuellen Versionsnummer im Namen wegsichern.<br>
Beispiel: C:\kieselstein nach C:\kieselstein-0.2.10 kopieren.

## Neues Java Installieren
- Azul Java 11 mit FX installieren Siehe hierfür auch [Java-Version]({{<relref "/docs/installation/01_server/anforderungen/#java-version">}}).
- darauf achten, dass Java Home gesetzt wird<br> ![](Java_Home.png)

- Prüfen, dass Java auch installiert wurde durch Command Shell,
  ```shell
  Java -version
  ```
![](java_version.png)

## Altes Java Deinstallieren (optional)
Über "Programme hinzufügen oder entfernen" nach JDK Suchen und die Java 8 Version deinstallieren.
![](uninstall_java.png)

**Achtung:** Diesen Schritt nur durchführen, wenn auf dem Server sicher keine andere Anwendung mehr das Java 8 JDK benötigt!<br>
Auf jeden Fall ist sicherzustellen, dass die JAVA_HOME Variable auf das Neue JDK 11 gesetzt wurde:
![](../umgebungs_variablen.png)

## Nicht mehr benötigte Anwendungen entfernen
### Bestehende Kieselstein Dienste deinstallieren
Dafür das Skript **?:\kieselstein\dist\bootstrap\service\windows\delete-kieselstein-services.bat** mit Administrator Rechten ausführen.

### Reports sichern
Damit die Anwender-Reports nach dem Update wieder zur Verfügung stehen, muss der Reports-Ordner in ein neues Verzeichnis kopiert werden.<br>
Hier muss der bestehende ?:\kieselstein\dist\wildfly-12.0.0.Final\server\helium\report nach ?:\kieselstein\dist\wildfly-26.1.2.Final\kieselstein\reports kopiert werden.

**WICHTIG:** Dies muss vor dem Entpacken des neuen Dist-Packets erfolgen, damit neuere Versionen der Standard-Reports richtig nachgezogen werden.

{{% alert title="ACHTUNG" color="warning" %}}
Obige Beschreibung gilt nur für die 1.0.x **VOR** der 1.0.3. Ab der 1.0.3 ist auch die Verlagerung der Anwenderreprots nach ?:\kieselstein\data\report**s** gegeben. Für Details dazu siehe bitte( {{<relref "/docs/installation/10_reportgenerator/anwenderreports" >}} )
{{% /alert %}}

ACHTUNG: Die Definitionen für dein JasperStudio entsprechend übertragen und auch die Einstellungen im Jasper Studio entsprechend anpassen.<br>
Dies idealerweise, bevor du die nachfolgenden Verzeichnisse löschst.

- In Jasperstudio direkt.<br>Eigenschaften des Projekts auf den neuen Pfad ändern.<br>Z.B. von ...?:\kieselstein\dist\wildfly-12.0.0.Final\server\helium\report auf ?:\kieselstein\data\reports zu ändern.<br>
Denke auch daran dass die .classpath entsprechend anzupassen ist<br>
und denke an die Verlagerung der .settings, bin und an das Neu-Schreiben des .projects für JasperStudio.

Im Verzeichnis bin solltest du auch die aktuelle kieselstein-ejb-1.0.3.jar anstatt der bisherigen ejb.jar verwenden.

### Alte Programmdateien löschen
Folgende Ordner können nun komplett gelöscht werden:
- ?:\kieselstein\dist\apache-tomcat-8.5.93
- ?:\kieselstein\dist\bin
- ?:\kieselstein\dist\bootstrap
- ?:\kieselstein\dist\service
- ?:\kieselstein\dist\wildfly-12.0.0.Final

## Installation Kieselstein ERP, Version (aktuelle Version):
### Herunterladen aktuelle Kieselstein ERP Version
## TODO Referenz auf die richtige Version setzen.
[Siehe auch]( {{<relref "/docs/installation/01_server/#wie-komme-ich-zu-einer-aktuellen-version">}} )

Dist-Paket über die bestehende Installation entpacken (In der Windows Eingabeaufforderung *cmd.exe*)
Beispiel 
```bash
tar -xvf <pfad-zum-dist-packet>.gz -C ?:\kieselstein\
```

## Datenbank Updaten
In der Command Shell in das Verzeichnis navigieren und mit liquibase den update Befehl durchführen:
```shell
cd C:\kieselstein\dist\bootstrap\liquibase
run-liquibase.bat update
```

### Hinweis: Wenn die bestehende Kieselstein Version kleiner als 0.0.13 ist:
> Dann muss dem Liquibase noch mitgeteilt werden, dass es bereit die Grund-Datenstruktur gibt, 
> somit muss vor dem Befehl `run-liquibase.bat update` noch folgender Befehl aufgerufen werden:
> `run-liquibase.bat changelog-sync --label-filter="0.0.12"`

## Einrichten des Dienstes und starten des Servers
Mit Administrationsrechten das Install-Script ?:\kieselstein\dist\bootstrap\service\windows\install-kieselstein-services.bat ausführen.

## NGINX Webserver (Optional)
Die Kieselstein-REST Schnittstelle wurde mit dem Update in den Wildfly integriert und ist somit auch über den Port: 8080 erreichbar.

Sollte es notwendig sein, dass diese wie bisher über den Port 8280 erreichbar ist, kann hier ein Nginx-Webserver als Proxy vorgeschaltet werden (Siehe [NGINX Webserver]({{<relref "docs/installation/01_server/windows_java11/#nginx-webserver-optional">}}))

## läuft der Server?
Siehe [Läuft der Server]({{<relref "docs/installation/01_server/windows_java11/#läuft-der-server">}})<br>
Es hat sich bewährt nach einem Update, insbesondere nach dem Wechsel der Java Version, den Server manuell zu starten. D.h. mit administrativen Rechten das<br>
?:\kieselstein\dist\bin\launch-kieselstein-main-server.bat<br>
auszuführen.

Beobachte hier die Ausgabe der Console. Erscheint hier:<br>
```
2024-09-19 10:58:50,075 ERROR [org.jboss.msc.service.fail] (ServerService Thread Pool -- 82) MSC000001: Failed to start service jboss.ra.deployer."jackrabbit-jca-2.22.0.rar": org.jboss.msc.service.StartException in service jboss.ra.deployer."jackrabbit-jca-2.22.0.rar": WFLYJCA0046: Failed to start RA deployment [jackrabbit-jca-2.22.0.rar]
    at org.jboss.as.connector.services.resourceadapters.deployment.
    ...
Caused by: org.jboss.jca.deployers.common.DeployException: IJ020056: Deployment failed: jackrabbit-jca-2.22.0.rar
    ...
Caused by: java.lang.UnsupportedClassVersionError: Failed to link org/apache/jackrabbit/jca/JCAResourceAdapter (Module "deployment.jackrabbit-jca-2.22.0.rar" from Service Module Loader): org/apache/jackrabbit/jca/JCAResourceAdapter has been compiled by a more recent version of the Java Runtime (class file version 55.0), this version of the Java Runtime only recognizes class file versions up to 52.0
```
**this version of the Java Runtime only recognizes class file versions up to 52.0**

so bedeutet dies, dass du vermutlich noch Java 8 auf deinem Server verwendest.<br>
Gegebenenfalls willst du auf deinem Server, warum auch immer, auch noch Java 8 verwenden und hast daher beide Java Versionen installiert.<br>
In diesem Falle muss das JAVA_HOME im Start-Batch<br>
(?:\kieselstein\dist\bin\launch-kieselstein-main-server.bat) gesetzt werden.<br>
Z.B. schreibst du in Zeile 3 set JAVA_HOME=c:\Program Files\Zulu\zulu-11, also den Pfad auf dein Java 11 rein.<br>
{{% alert title="Hinweis:" color="info" %}}
Wir raten, insbesondere am Server nur **eine** Java Version zu verwenden.<br>
Wir haben schon zu oft vermeintliche Fehler gesucht, die dann im Endeffekt die Ursache in einer falschen Javaversion hatten.
{{% /alert %}}


## Dokumentendatenbank Workspace.xml anpassen
Wenn eine bestehende Dokumentendatenbank existiert, müssen hier folgende Parameter Werte (**Achtung** diese Werte sind 2x in der XML-Datei vorhanden) angepasst werden:
> ?:\kieselstein\data\jackrabbit\workspaces\default\workspace.xml

- driver: javax.naming.InitialContext
- url: java:/JRDS

Und folgende Parameter können gelöscht werden:
- user
- password

### Beispiel:
#### Alte XML-Datei
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Workspace name="default">
    <FileSystem class="org.apache.jackrabbit.core.fs.db.DbFileSystem">
        <param name="driver" value="org.postgresql.Driver"/>
        <param name="url" value="jdbc:postgresql://${org.kieselstein.db-doc.host}:${org.kieselstein.db-doc.port}/${org.kieselstein.db-doc.name}"/>
        <param name="schema" value="postgresql"/>
        <param name="user" value="postgres"/>
        <param name="password" value="postgres"/>
        <param name="schemaObjectPrefix" value="ws_"/>
    </FileSystem>
    <PersistenceManager class="org.apache.jackrabbit.core.persistence.bundle.PostgreSQLPersistenceManager">
        <param name="driver" value="org.postgresql.Driver"/>
        <param name="url" value="jdbc:postgresql://${org.kieselstein.db-doc.host}:${org.kieselstein.db-doc.port}/${org.kieselstein.db-doc.name}"/>
        <param name="user" value="postgres"/>
        <param name="password" value="postgres"/>
        <param name="schema" value="postgresql"/>
        <param name="schemaObjectPrefix" value="jcr_${wsp.name}_"/>
        <param name="externalBLOBs" value="false"/>
    </PersistenceManager>
    <SearchIndex class="org.apache.jackrabbit.core.query.lucene.SearchIndex">
        <param name="path" value="${wsp.home}/index"/>
    </SearchIndex>
</Workspace>
```

#### Neue XML-Datei:
```xml
<?xml version="1.0" encoding="UTF-8"?><Workspace name="default">
    <FileSystem class="org.apache.jackrabbit.core.fs.db.DbFileSystem">
        <param name="driver" value="javax.naming.InitialContext"/>
        <param name="url" value="java:/JRDS"/>
        <param name="schema" value="postgresql"/>
        <param name="schemaObjectPrefix" value="ws_"/>
    </FileSystem>
    <PersistenceManager class="org.apache.jackrabbit.core.persistence.pool.PostgreSQLPersistenceManager">
        <param name="driver" value="javax.naming.InitialContext"/>
        <param name="url" value="java:/JRDS"/>
        <param name="schema" value="postgresql"/>
        <param name="schemaObjectPrefix" value="jcr_${wsp.name}_"/>
        <param name="externalBLOBs" value="false"/>
    </PersistenceManager>
    <SearchIndex class="org.apache.jackrabbit.core.query.lucene.SearchIndex">
        <param name="path" value="${wsp.home}/index"/>
    </SearchIndex>
</Workspace>
```


## weiters zu tun
- Clients Updaten (diese benötigen jetzt auch Java 11) [siehe auch]({{<relref "docs/installation/02_client">}})
- Wenn Nginx nicht installiert wurde bei allen Zeiterfassung-Terminals oder anderen Programmen, welche die REST-Schnittstelle verwenden, den Port auf 8080 ändern.

**Empfehlung:**<br>
Insbesondere für die Tests in der ersten Zeit, sollte für ein eventuelles Fallback auf den Clients sowohl die Version für den Java 8 Server als auch für den Java 11 Server parallel vorgehalten werden. Damit man, im schlimmsten Falle, schnell auf die Vorgängerversion zurückwechseln kann.