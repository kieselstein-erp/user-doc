---
title: "Installation Kieselstein ERP Server unter Windows(r)"
linkTitle: "Windows"
categories: ["Server Installation"]
tags: ["Windows Server"]
weight: 30
description: >
  Installation Kieselstein ERP Server unter Windows(r)
---
Hier findest du ergänzende Punkte zur Installation unter Windows. Die Standard-Installation ist im Hauptkapitel beschrieben.

## Firewall
Nach der Installation und dem erfolgreichen Test daran denken, dass in der Regel, zumindest für  den Port 8080.
![](Firewall01.png)  

![](firewall02.png)  
8280 wenn auch die Rest-API verwendet werden sollte (Ab der Version 1.x.x nicht mehr erforderlich)

5432 wenn auch von innerhalb des Netzwerkes auf den PostgresQL zugegriffen werden sollte.

ACHTUNG: Sicherheit!!

## Der Server startet nicht
Im Deploymentsverzeichnis (?:\Kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments) steht kieselstein-0.0.11.ear.failed.<br>
Ev. auch nur jackrabbit-jca-1.5.7.rar.failed.<br>
So kann man in den beiden Dateien nachsehen.<br>
Effizienter ist vermutlich in der ?:\Kieselstein\dist\wildfly-12.0.0.Final\standalone\log\server.log nachzusehen.<br>
Dafür zuerst das log Verzeichnis löschen um nur die aktuellsten Einträge zu bekommen und dann den Server erneut starten, bis wiederum im Deploymentsverzeichnis das failed kommt.<br>
Nun sucht man in der server.log am besten von oben nach dem ersten Eintrag mit <u>Error</u>. Meist findet man einen Eintrag wie z.B.:<br>
2023-09-22 17:15:23,014 ERROR [org.apache.jackrabbit.core.fs.db.DatabaseFileSystem] (MSC service thread 1-3) failed to initialize file system: org.postgresql.util.PSQLException: FATAL: Passwort-Authentifizierung f�r Benutzer �postgres� fehlgeschlagen

Dies bedeutet, dass der Applikationsserver sich nicht an der Datenbank anmelden konnte. 
Versuche nun dich mittels PGAdmin auf der Datenbank mit postgres, postgres anzumelden.
Ist dies nicht möglich, stimmt das Passwort der Datenbank nicht. Eventuell wurde bei der Installation des Postgresservers ein falsches oder kein Passwort vergeben. D.h. es muss das Passwort geändert werden. Wenn du das andere(falsche) Passwort kennst, melde dich mit diesem an und ändere das Passwort. Wenn du dieses nicht kennst, muss zuerst der Zugang zum PostgresQL-Server, auf dem Rechner, auf dem dieser läuft, so freigeschaltet werden, dass du dich auch ohne Passwort anmelden kannst. Dafür muss die pg_hba.conf angepasst werden. Diese findest du unter "?:\Program Files\PostgreSQL\14\data\".
Ergänze diese nun abhängig von deiner IP-Konfiguration um
- host all all 127.0.0.1/32 trust (IP V4)
- host all all ::1/128 trust (IP V6)

Nun muss der postgresql Dienst neu gestartet werden.

Wechsle nun als Administrator nach ?:\Program Files\PostgreSQL\14\bin<br>
und rufe psql.exe -U postgres auf

Nun mit
> \password postgres
>
das Passwort auf postgres setzen und dann mit
> \q
>
Das Programm wieder verlassen.

Nun musst du dich auch im PGAdmin mit dem Passwort anmelden können.<br>
Bitte beachte, dass ab PostgresQL 15 der PGAdmin 7 zum Einsatz kommen sollte.

Info:<br>
Bei der Modifikation der pg_hba.conf auf IP V4 bzw. IP V6 achten! Kommt<br>
psql: Fehler: Verbindung zum Server auf »localhost« (::1)<br>
So fehlt der passende Eintrag in der pg_hba.conf

Info:<br>
Nach diesen Änderungen empfiehlt sich auch die Verzeichnisse log, tmp, data aus dem standalone (?:\kieselstein\dist\wildfly-12.0.0.Final\standalone\) zu löschen, damit die ganzen falschen Einträge weg sind.
