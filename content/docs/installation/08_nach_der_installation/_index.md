---
categories: ["Nach der Installation"]
tags: ["Installation"]
title: "Nach der Installation"
linkTitle: "Nach der Installation"
date: 2023-01-31
weight: 800
description: >
  Arbeiten nach der Installation Ihres Kieselstein-ERP-Systems
---
Nach der Installation
=====================

Dinge die nach der erfolgreichen Installation deines **Kieselstein ERP** Systems zu machen sind. Hier findest du auch Infos, zu häufig auftretenden Fehlern, die einem einfach passieren.
## a.) Kann mich anmelden, es lässt sich kein Modul öffnen
Beim Neustart eines Modules kommt ein schwerer Fehler. Dies Fehler kommt bei allen Modulen.

Voraussetzung: In deinem Kieselstein-Server-Verzeichnis sind alle drei Module deployed.<br>
![](deployed.png)<br>
Der Client startet, du kannst dich mit Admin, admin anmelden. Es sind alle Module als Buttons in der Hauptmenüleiste sichtbar.
![](Alle_Module_sichtbar.png)<br>

Beim Klick auf irgendein Modul kommt:<br>
![](Server_Fehler01.png)<br>
Im Detail steht "nur"<br>
![](Server_fehler01_Detail.png)<br>

Im Server.log (?:\kieselstein\dist\wildfly-12.0.0.Final\standalone\log\server.log) steht ziemlich weit unten (am Ende)
- Caused by: java.sql.SQLException: IJ031070: Transaction cannot proceed: STATUS_MARKED_ROLLBACK
- davor: Caused by: org.hibernate.exception.GenericJDBCException: could not prepare statement
- und einige weitere Error-Einträge davor:<br>
Caused by: org.postgresql.util.PSQLException: FEHLER: Datum/Zeit-Feldwert ist außerhalb des gültigen Bereichs: »14/7/2023 11:51:58.567«
  Hinweis: Möglicherweise benötigen Sie eine andere »datestyle«-Einstellung.

Das bedeutet, dass der 'datestyle' in der "?:\Program Files\PostgreSQL\14\data\postgresql.conf" falsch eingestellt ist. Es muss dieser auf:
> datestyle = 'iso, dmy'

eingestellt sein. D.h. die postgresql.conf richtigstellen und dann den postgresql neu starten. Natürlich vorher den wildfly stoppen und wenn der postgresQL läuft, den Wildfly wieder starten.
## b.) Kann keine neuen Artikel anlegen
Du meldest dich an **Kieselstein ERP** an und die Benutzersprache ist nicht Deutsch Österreich, sondern irgend eine andere, z.B. ![](Logon_Sprache.png) Deutsch Deutschland.<br>
Nun ist im Artikelmodul kein Neu-Button sichtbar ![](Artikel_neu_Button_fehlt.png).

Hintergrund: Der gewählte / definierte Mandant und damit die Mandantensprache, ist in einer anderen Kommunikationssprache als deine Clientkommunikationssprache. Info: **Kieselstein ERP** kommt default in de AT = Deutsch Österreich. D.h. du wechselst in das Modul System ![](Modul_System.png), unterer Reiter Mandant ![](Reiter_Mandant.png), den vorgeschlagenen Mandanten verwenden und in den Reiter 2 Kopfdaten wechseln.<br>
Hier ![](Mandantensprache_aendern.png) auf ändern klicken und dann die Kommunikationssprache zu deiner gewünschten Mandantensprache ändern.<br>
Nun musst du noch deinen **Kieselstein ERP** Client neu starten.<br>
{{% alert title="Wichtig" color="warning" %}}
Du solltest diese Änderung nur, wenn unbedingt notwendig, durchführen. Auf jedem Fall aber bevor Artikel, Textmodule, Grunddaten usw. erfasst wurden.<br>
Musst du dies bei bereits existierenden Daten machen, wende dich an einen **Kieselstein ERP** Betreuer.
{{% /alert %}}

## c.) die HTML BDE geht nicht
Wenn man die HTML BDE aufruft, kommt im Browser eine Fehlermeldung:
![](Anmeldung_HTML_BDE_Fehler.png)  
Die bedeutet, dass der verdeckte Benutzer lpwebappzemecs in deiner Installation fehlt.

## d.) In der Monatsabrechnung beginnt die Woche mit Sonntag
![](falscher_Wochenbeginn.png)<br>
Ursache: Deine Installation läuft auf Amerikanisch! Wir wollen aber Europa haben.
Man sieht das auch unter Hilfe, Info, Server - Java Info<br>
![](Server_Info_falsch.png)<br>
Man sieht das auch im Server.log, dass *user.country = US* eingetragen ist und dass *user.language = en* eingetragen ist. Es muss beides umgestellt werden. Wichtig auch: Bitte dies auch in der Datenbank Konfiguration richtigstellen. Idealerweise läuft dein Server in Deutsch. Leider geht das nicht immer, weil z.B. die Konzernsprache Englisch ist.

Prüfe auch die server.log. In den Starteinstellungen, muss:
- user.country = DE
- user.language = DE
- user.timezone = Europe/Berlin (gerne auch Wien oder Zürich)
stehen.

Prüfe zusätzlich die Einstellungen deiner Datenbank in der postgresql.conf. Hier muss unter:
- log_timezone = 'Europe/Berlin'
- timezone = 'Europe/Berlin'
stehen.

Die Wildfly-Einstellungen kannst du in der 
- ?:\kieselstein\dist\bin\launch-kieselstein-main-server.bat bzw. der .sh übersteuern.

Hier ergänzt du die MAIN_SERVER_OPTS hinten um -Duser.timezone=Europe/Berlin -Duser.country=DE -Duser.language=DE

Analog solltest du dann auch die Rest-Server Einstellungen analog anpassen. D.h. in der
- ?:\kieselstein\dist\bin\launch-kieselstein-rest-server.bat bzw. .sh

ergänzt du die set CATALINA_OPTS hinten um -Duser.timezone=Europe/Berlin -Duser.country=DE -Duser.language=DE

Danach Rest, Wildfly, Postgres stoppen und in umgekehrter Reihenfolge starten.
Es muss im Server.log die oben angeführten user.... stehen.<br>
Im Client siehst du nun<br>
![](Server_Info_richtig.png)

Damit kommt auch die Monatsabrechnung und viele andere Auswertungen in der Wochenbetrachtung richtig. Also: ![](europaeischer_Wochenbeginn.png)

# --------------- old
Nach der Installation von **Kieselstein ERP** sollten Sie folgende Schritte durchführen / prüfen

-   Ändern Sie das Passwort des Datenbank Administrators auf ein sicheres Passwort ab.
-   Prüfen Sie die Einstellungen des Mandanten und korrigieren Sie diese gegebenenfalls
-   Einrichten des Datenbank [Backupscripts](#Backup).
-   Prüfen Sie, dass der Administrator das Recht PERS_SICHTBARKEIT_ALLE hat.
-   Prüfen Sie, dass der Administrator das Recht PERS_ZEITEINGABE_NUR_BUCHEN nicht hat.
-   Prüfen Sie, dass der Administrator Zugriff auf alle Läger hat.

[**<u>Beachten Sie bitte unbedingt die Anweisungen zum Update von Betriebssystem und Datenbank.</u>**]( {{<relref "http://localhost:1313/docs/installation/01_server/update/" >}} )

Wir empfehlen das Kieselstein ERP Verzeichnis (...)/kieselstein/ und das Verzeichnis der Datenbank vom Virenscanner auszuschließen, um Geschwindigkeitseinbußen zu vermeiden. So hatten wir in machen Konstallationen Geschwindigkeitsverluste von 100% (es hat das Backup doppelt so lange gedauert)

Bitte richten Sie keine Freigaben für Verzeichnisse des **Kieselstein ERP**-Servers ein!

Eine Ausnahme ist hier das Verzeichnis der Anwenderspezifischen Reports, falls Sie selbst Reportanpassungen vornehmen.

**Kieselstein ERP** Administrator-Handbuch
===============================

Mit diesem Administrator Handbuch wollen wir Ihnen eine Hilfestellung zum "Am Laufen halten" Ihrer **Kieselstein ERP** Installation geben. Wie allgemein üblich, liegt die Verantwortung für die Sicherheit aller EDV Komponenten bei Ihnen, d.h. in der Verantwortung des Anwenders. Das betrifft sowohl die Zugriffssicherheit als auch so Dinge wie Datensicherheit und Ähnliches. Etwaige Ansprüche aus Titeln dieser Art werden von uns grundsätzlich abgelehnt.

Was sollte der **Kieselstein ERP** Administrator regelmäßig prüfen?
-   Log-Files Wildfly checken, Anzahl der Exeptions ***???*** nur kritische suchen
-   Log-Files **Kieselstein ERP** durchsehen
-   Speicherbedarf des Wildfly (im Taskmanager) prüfen, ob er markant ansteigt. Ca. wöchentlich vergleich mit Startmemory
-   Regelmäßig Datenbanken sichern, täglich, stündlich
-   Datensicherung muss geprüft werden, sowohl Bandsicherung als auch DB Backup
-   Größen der DB Transaktion-Logs, Aktualität der Sicherung.
    Hier muss zusätzlich auf die Unterschiede zwischen MS-SQL, PostgresQL geachtet werden.
-   Prüfen des freien Platzes sowohl auf dem Laufwerk/Volume der Datenbank, also auch auf der Datenbanksicherung und dem JBoss Laufwerk. Hier muss ausreichend Platz für den Betrieb sein. Also jeweils mindestens 10GB freier Plattenplatz.

- Admin muss die DBs einspielen / zurücksichern können
- **Kieselstein ERP** Dienste müssen gestoppt und gestartet werden können, welche Dienste sind das?
    ![](Dienste.gif)
-   Starten des Wildfly, vor allem in Mehr-Rechner-Systemen und vor allem auch wenn die Datenbank deutlich länger zum Starten braucht als der JBoss.
    Sowohl unter Linux, MAC als auch unter Windows.

Was sollte der Kieselstein-ERP-Admin können?
-   Selbständig Patches einspielen können, DB Updates einspielen.
-   Client Installieren
-   Wissen über die eigene EDV Infrastruktur, insbesondere Netzwerk-Auslastung und Kieselstein-ERP-Serverauslastung
-   Servernamen und Ports, netstat -a muss ein Begriff sein
-   Grundwissen über Reports, Baumstruktur, Reports neu Compilieren
-   FAQ-Anwender muss beherrscht werden
    Login, Netzwerk, Passwort, ....
-   Drucker installieren und einrichten - Druckformulare definieren
    Insbesondere das Thema Etiketten drucken.
-   Prüfung der USV für jeden Server
-   Server starten und vor allem kontrolliert herunterfahren
- Wildfly und Datenbankserver stoppen und starten
- zumindest monatliche Prüfung, dass die Backups zeitgerecht in der globalen Sicherung laden. Dass Sicherungsmedien physikalisch von der IT getrennt sind, ist selbstverständlich. Auch selbstverständlich ist, dass jegliche Sicherung außer Haus gebracht wird und dass zumindest die Monatssicherungen auf der Bank o.ä. liegen.

Sollte der Backup-Job nicht laufen / Fehler melden, so finden Sie Fehlerhinweise in der Job-History.

Achten Sie auf ein tägliches Backup. Siehe dazu auch [Datenbankserver]( {{<relref "/docs/installation/01_server/datensicherung/" >}} )

## Herunterfahren des Linuxservers, bzw. Durchstarten
Um einen Linuxserver herunterzufahren, verbinden Sie sich bitte mit Putty auf den Server. Melden Sie sich mit root und dem bekannt gegebenen Passwort am Linux Server an. Nun geben Sie:
    shutdown -h now

zum Herunterfahren und ausschalten ein und

    shutdown -r now

zum Reboot.

Startet nach dem Start des Linuxservers der Wildfly nicht, z.B. da die Datenbank erst reorganisiert werden musste und dadurch deutlich länger als vorgesehen dauerte, so muss der wildfly per Hand gestartet werden. Bitte wiederum mit Putty verbinden und

    systemctl start kieselstein-main-server.service

eingeben.

Warten Sie nun einige Minuten und verbinden Sie sich dann mit dem **Kieselstein ERP** Client auf den Aplikationsserver Können Sie trotzdem keine Verbindung aufbauen, so wenden Sie sich bitte an Ihren Betreuer.

## Erstellen eines Datenbankbackups unter Linux

Verbinden Sie sich bitte mit Putty auf den Server. Geben Sie nun

    pg_backupdb

ein. Damit wird ein aktuelles Backup der Datenbank erzeugt und üblicherweise auf /home/postgres geschrieben. Stellen Sie sicher, dass dieses Backup regelmäßig / täglich automatisch erzeugt wird und dass diese Dateien gesichert werden.

## Sicherung der Anwenderreports

Stellen Sie sicher, dass auch Ihre Anwenderreports im täglichen Sicherungsjob mitgesichert werden. In den Reports steckt oft sehr viel Detailarbeit an der optimalen Umsetzung Ihrer Unternehmensprozesse. D.h. wenn diese verloren gingen, z.B. durch versehentliches Überschreiben, würde ein entsprechender Mehraufwand entstehen.

Auch diese Reports sollten in der täglichen Datensicherung enthalten sein.

### Mein IT Betreuer möchte Zugriff
Nachdem man üblicherweise die Betreuung seiner IT-Systeme vertrauensvoll in die Hände eines **fähigen** Betreuers legt, wird diese, z.B. monatlich, sich um das Update deiner IT-Systeme kümmern. Nun will ein guter Betreuer natürlich nach allen Updates auch wissen, ob denn nun die Systeme auch wieder laufen.<br>
Für Kieselstein stehen dafür zwei Funktionalitäten zur Verfügung:
- a.) es wird über die RestAPI ein Systemping auf den Server gemacht und geprüft, ob eine entsprechende Antwort mit Versionsnummer und relativ aktueller Uhrzeit kommt
- b.) dein IT-Betreuer meldet sich über den Client an.

In zweiten Fall benötigt er zumindest soweit einen Account, dass auch die Anmeldung durchgeführt werden kann. D.h. du legst z.B. einen User IT-Service an und legst eine Rolle IT-Service an.<br>
Der Trick liegt nun darin, dass du der Rolle **keine** Rechte gibst, somit kann sich dein Betreuer anmelden, sieht aber keine ERP Daten. Er kann damit feststellen, dass das gesamte **Kieselstein ERP** läuft, in diesem Falle ohne RestAPI.

Für den IT-Betreuer: Es kommt leider immer wieder mal vor, dass nach einem Systemupdate, der Datenbankserver deutlich länger als gewöhnlich benötigt und damit der Wildfly-Service auf dem das **Kieselstein ERP** aufbaut nicht richtig startet. Sollte dies der Fall sein, haben bisher folgende Schritte geholfen.
- Stopp des Main-Server-Dienstes
- Neustart des Main-Server-Dienstes
- ca. 5Minuten warten.<br>In jedem Falle so lange bis unter ../kieselstein/dist/wildfly-26.1.2.Final/standalone/deployments für alle drei Dateien .deployed oder .failed steht.<br>Bei deployed ist alles gut gegangen und du kannst dich anmelden, bei failed ist eventuell der Datenbankserver noch immer nicht gestartet. D.h. nun könntest du ev. ein Blick in das Log-File mit mehr Infos werfen, bzw. kann man gerne auch
- die beiden Stopp durchführen
- danach den PostgresQL-Server-Dienst stoppen und neu starten (restart)
- danach die beiden Dienste (Main und RestAPI) wieder starten

Danach wie oben beschrieben auf deployed oder failed warten.
Wenn erneut failed kommt, versuche dich über den PGAdmin anzumelden. Geht auch das nicht, hast du ein Problem mit der Datenbank. Das kann auch an zu wenig freiem Speicher liegen. Du solltest mehr als das doppelte der Größe deiner Datenbanken an freiem Festplatten-Speicher haben.

Eventuell hilft auch einfach das System / deinen **Kieselstein ERP** Server noch einmal durchzustarten.

#### Einrichten des EMail-Versandes
Damit der EMail Versand durch den IT-Betreuer eingerichtet werden kann, gib einfach obiger Rolle ein schreibendes Recht auf System. Wenn der EMail Versand dann eingerichtet ist, entfernst du dieses Recht wieder. 