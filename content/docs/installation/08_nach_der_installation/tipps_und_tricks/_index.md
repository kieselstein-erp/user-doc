---
categories: ["Nach der Installation"]
tags: ["Tipps und Tricks"]
title: "Nach der Installation"
linkTitle: "Tipps und Tricks"
date: 2023-01-31
weight: 100
description: >
  Tipps und Tricks rund um dein **Kieselstein ERP**
---
Tipps und Tricks
================

Mit dieser Seite wollen wir zusätzliche Hilfestellung beim Einsatz von **Kieselstein ERP** leisten.

**Kieselstein ERP** Server ist nach IP Änderung nicht mehr erreichbar?

Wird die IP des Rechners mit dem **Kieselstein ERP** Server geändert, so muss der **Kieselstein ERP** Server neu gestartet werden.

<a name="Unterschiedliche Eigenschaften am gleichen Drucker"></a>Ich habe zwei Grundeinstellungen bei meinem Drucker, zwischen denen ich laufend hin- und herschalten muss

z.B. für den Ausdruck von firmeninternen und -externen Dokumenten (Papier mit Firmenlogo)

Bei lokalem Drucker:
-   Start > Einstellungen > Drucker und Faxgeräte öffnen
-   Drucker hinzufügen
-   denselben Drucker auswählen, ihm einen anderen Namen zuweisen
-   die Einstellungen ändern
-   Ab jetzt werden "beide" Drucker in **Kieselstein ERP** zur Auswahl stehen

Bei einem Netzwerkdrucker muss er zuerst am Server eingerichtet werden:
-   beim Server einen neuen lokalen Drucker anlegen, den gleichen Port, wie beim bestehenden Drucker auswählen 
-   Namen sprechend wählen 
-   richtig konfigurieren 
-   bei Workstations anlegen
-   Drucker bei den Workstations einzeln konfigurieren

Es wurden Änderungen an den Fußzeilen vorgenommen, seither kommt nur mehr OK und die Fußzeilen fehlen.

Hier ist der Hintergrund, dass der für die Fußzeilen(=Subreport) im Ausgangsreport vorgesehene Platz zu gering ist. Üblicherweise ist der vorgesehene Platz nicht hoch genug.

Bestimmen Sie anhand des Fußzeilen-Subreports die erforderliche Höhe und korrigieren Sie den Ausgangsreport.

Hinweis: Achten Sie bei Subreports auch darauf, dass Sie die Höhe und Breite so wie auch im Ausgangsreport definieren.
Hinweis: Es wird für die Fußzeilen nur mehr die Fuss.jrxml verwendet.

Siehe dazu unbedingt auch ..\wildfly-12.0.0.final\standalone\log\server.log

Ich will in den Auswahllisten auch die rundherum befindlichen Daten sehen.

Manchmal ist es praktisch z.B. bei Rechnungen nicht nur die eine Rechnung Nr: 06/0000001 zu sehen, sondern auch die rundherum befindlichen Rechnungen. Um dies darzustellen, gibt es einen Trick.
- Bestimmen Sie mit dem Direktfilter den gesuchten Datensatz. Z.B. Rechnung 06/0000001\. Nun löschen Sie die Eingabe des Direktfilters wieder ohne Enter zu drücken. Nun wechseln Sie z.B. in die Kopfdaten oder in die Positionen und wechseln sofort wieder zurück in den Auswahlfilter. Durch die Logik, dass **Kieselstein ERP** auf dem ausgewählten Datensatz stehen bleibt, befinden Sie sich auf dem gewünschten Datensatz. Da aber die Filterbedingung inzwischen entfernt wurde, werden alle Rechnungen angezeigt.

Nach meinem Client-Update erhalte ich beim Start des **Kieselstein ERP** Clients eine Fehlermeldung

Could not find main class.

![](Start_Fehler.gif)

Dies bedeutet, dass auf Ihrem PC eine falsche Java-Version installiert ist. [Siehe]( {{<relref "/docs/installation/02_client" >}} )

Übersetzung von Texten

In **Kieselstein ERP** können alle Labels (Beschriftungen der Knöpfe, Eingabefelder usw.) dynamisch übersetzt werden. Bei der Verwendung einer PostgresQL Datenbank ist zu berücksichtigen, dass die eingetragenen C_TOKEN <u>**OHNE**</u> endende Leerstellen eingegeben werden.

Richtige Einstellung der Mehrwertsteuersätze für Deutschland

Da in Deutschland seit 1999 einige Mehrwertsteueränderungen waren, hier die richtige Einstellung der Mehrwertsteuersätze für allgemeine Waren. Eine Luxussteuer, wie sie in Österreich bekannt ist, gibt es in Deutschland derzeit nicht.

![](Deutsche_Mehrwertsteuersaetze.gif)

<a name="Markieren"></a>Markieren von mehreren Zeilen bei Auswahlmöglichkeiten

In den Modulen von **Kieselstein ERP** (z.B. Finanzbuchhaltung, Stücklistendruck, etc.) können Sie mehrere Zeilen markieren.

Gehen Sie dazu wie folgt vor:

Mit Klick auf eine Zeile markieren Sie diese.

Halten Sie die Strg-Taste gedrückt, um weitere Zeilen zu markieren.

Bestätigen Sie die Auswahl mit Enter.

Sollten Sie mehrere Zeilen, die sich hintereinander befinden, markieren wollen, so gehen Sie wie folgt vor:

Markieren Sie die erste Zeile und halten die Shift ("Groß/Kleinschreib")-Taste gedrückt, nun klicken Sie auf die letzte zu markierende Zeile.

Mit Drücken der Strg-Taste und A markieren Sie alle Zeilen. Zum Beispiel, wenn Sie durch Filter schon alle gewünschten Zeilen zur Auswahl stehen.

Um die gesamte Markierung wieder aufzuheben, klicken Sie ohne Tastendruck auf eine andere Zeile. 

Um nur eine Zeile wieder aus der Markierung zu nehmen, halten Sie die Strg-Taste gedrückt und klicken auf die Zeile, bei der Sie die Markierung aufheben möchten.

### Tipps zur Verwendung von Etikettendruckern

Für den Ausdruck von Etiketten empfehlen wir den Einsatz von Druckern der Firma Zebra. Für langjährigen Gebrauch ist die Serie Z4 bzw. S4 zu empfehlen.

Für manche Anwendungen mit sehr geringem Etiketten-Aufkommen werden auch die Drucker der Fa. Dymo eingesetzt. Dazu ist anzumerken, dass dies reine Einzelplatzdrucker sind. Die Einrichtung im Netzwerk ist aus unseren Erfahrungen nicht stabil. Dazu kommt, dass das Preis-Leistungsverhältnis nur für ein sehr geringes Etikettenaufkommen optimal ist. Ab ca. 1000 Etiketten im Jahr sind die kleinen Zebradrucker in den Gesamtkosten deutlich günstiger.

Da es natürlich auch zu den Zebradruckern immer wieder Fragen gibt, hier eine kleine Sammlung von Fragen und Antworten.

#### Der Drucker druckt ein Etikett und schiebt dann mehrere Etiketten nach
Führen Sie die Kalibrierung durch. Auf Windowsrechnern am einfachsten über die Eigenschaften des Druckers. Hier finden Sie in den Extras unter Aktion, Kalibrierung ausführen. Alternativ kann die Kalibrierung auch direkt am Drucker über das Druckmenü aufgerufen werden.

#### Der Drucker druckt ein Etikett und meldet dann "Paper out"
Stellen Sie sicher, dass das Papier wie im Deckel des Druckers aufgezeichnet richtig eingelegt ist. Wichtig ist hier auch, dass die seitliche Papierführung sehr knapp, aber leichtgängig eingestellt ist. Beachten Sie, dass sowohl die seitliche Führung der Etikettenrolle als auch die Führung des Papierbandes unmittelbar vor dem Thermotransferkopf möglichst exakt ist.

Vor dem Thermotransferkopf ist auf der Innenseite auch der Papiersensor angebracht. Dieser muss sauber sein.

#### Das Druckbild ist sehr schlecht
Führen Sie bitte die Kalibrierung durch.

## Weitere allgemein Tipps zur Verwendung von **Kieselstein ERP**

#### Nach einem Stromausfall startet der **Kieselstein ERP** Server nicht mehr, es gibt unkontrollierte Fehler
Bei unkontrolliertem Abschalten des **Kieselstein ERP** Servers kann es vorkommen, dass temporäre Dateien, welche eventuell auch teilweise zerstört sein können, stehen bleiben.

Dies wirkt sich dann so aus, dass z.B. der **Kieselstein ERP** Server nicht mehr startet, bei manchen Auswertungen Class not found Exceptions bringt usw..

Wenn dies der Fall ist, so stoppen Sie den **Kieselstein ERP** Server.

Nun löschen Sie aus dem Verzeichnis des **Kieselstein ERP** Servers die Unterverzeichnisse tmp und work.

Nun starten Sie den **Kieselstein ERP** Server wieder.

#### Benötige ich für den **Kieselstein ERP** Server eine unterbrechungsfreie Stromversorgung?
**Selbstverständlich.**

Manche Anwender sind zwar der Meinung, dass gespiegelte Platten, eine Datensicherung oder ähnliches ausreichen.

Wir weisen hier ausdrücklich darauf hin, dass jeder **Kieselstein ERP** Server und natürlich auch eventuell eigenständige Datenbankserver mit einer USV (unterbrechungsfreie Stromversorgung) ausgestattet sein müssen.

Wir können dazu von einem Anwender berichten, der ebenfalls der Meinung war, dass eine richtig installierte USV Luxus sei und bei dem ein Ausfall des Servers die Datenbank zerstört hat. Da dies am Abend eines intensiven Tages war, war es enorm wichtig, dass die Daten inkl. des Tages restauriert werden. Wir konnten die Daten mit einem Aufwand von ca. 3x24 Stunden zum Großteil retten. Wir waren jedoch ganz knapp daran aufzugeben.

Bedenken Sie die Kosten für einen derartigen Reparaturversuch und diese Kosten inkl. dem Datenverlust sind nicht versicherbar. Alleine um die Kosten für den Reparaturversuch bekommen Sie drei sehr gute unterbrechungsfreie Stromversorgungen.

#### Der Client meldet immer Zeitdifferenzen zum Server.
Stellen Sie sicher, dass Ihr **Kieselstein ERP** Server synchron zur internationalen Atomzeit ist. Wenden Sie sich dazu bitte gegebenenfalls an Ihren **Kieselstein ERP** Betreuer.

Nun empfiehlt es sich, dass jeder Client gegen den **Kieselstein ERP** Server synchronisiert wird. [Siehe]( {{<relref "/docs/installation/13_zeitserver" >}} )

#### Der **Kieselstein ERP** Client schließt sich immer wieder automatisch
Der Anwender hatte das Problem, dass immer bei einer längeren Pause, z.B. über Mittag, der **Kieselstein ERP** Client die Verbindung zum **Kieselstein ERP** Server verloren hat.

**Mögliche Ursache I:** Es geht die Netzwerkverbindung zwischen Client und Server verloren. Damit wird die Sitzungs-ID des **Kieselstein ERP** Clients ungültig, wodurch die Verbindung abbricht.

**Was ist zu tun?**

Schalten Sie bitte den Energiesparmodus in der Netzwerkkarte ab. Bitte wenden Sie sich an Ihren zuständigen EDV Betreuer, der Ihre Geräte kennt.

**Mögliche Ursache II:** Im Bildschirmschoner war eingestellt, dass der Rechner nach einer gewissen Zeit in den Standbymodus wechselt. Auch dadurch geht die Netzwerkverbindung zwischen Client und Server verloren. Damit wird die Sitzungs-ID des **Kieselstein ERP** Clients ungültig, wodurch die Verbindung abbricht.

**Weitere Möglichkeiten:**
- Ein Bildschirmschoner, der den Rechner in eine Art Ruhezustand versetzt.
- Die Netzwerkkarte ist defekt und fordert nach einiger Zeit immer wieder eine neue DHCP-Verbindung an
- Eventuell sollte auch im Ereignislog nachgesehen werden, ob hier weitere Hinweise auf Verbindungsabbrüche, Speicherfehler und ähnliches zu finden ist.

<a name="Geschwindigkeit"></a>

#### Wie kann die Geschwindigkeit von **Kieselstein ERP** bestimmt / gesteigert werden?
Die Geschwindigkeit von **Kieselstein ERP**, aus der Sicht des Anwenders, der vor dem Client sitzt, hängt von verschiedenen Faktoren ab.

Wir haben versucht hier eine Aufstellung der Punkte zusammenzustellen mit denen eine Abschätzung der Geschwindigkeit insbesondere die Abschätzung von Geschwindigkeitssteigerungen als ungefährer Richtwert, der bitte völlig unverbindlich ist, möglich werden sollte.

1.  Geschwindigkeit der Datenbank.
    Diese hängt wiederum von:
    -   Geschwindigkeit der CPU -> steht eine eigene CPU nur für die Datenbank zur Verfügung
    -   Wieviel kann im Ram gepuffert werden -> das hängt wiederum vom
        - verfügbaren Ram,
        - der Größe der Transaktionen,
        - der Menge an Schreiboperationen ab
    -   Die Geschwindigkeit der Platten.
        Dies ist laut unserer Erfahrung für die Datenbank der kritischste Punkt, Speicher ist meistens ausreichend vorhanden.
        Wir raten aktuell zum Einsatz von Server SSD Platten im entsprechenden Raid Verbund.
        Auch wenn das unter Umständen etwas Geld kostet, in Summe ist es die effizienteste Lösung.

2.  Geschwindigkeit des Applications-Servers
    -   Geschwindigkeit der CPUs -> da der Application Server ein reines Java Programm ist, ist hier die Geschwindigkeit der CPUs ein wesentlicher Faktor. Mit dazu kommt, dass für die unterschiedlichen Aufgaben, weitere Threads gestartet werden. Diese können auf unterschiedlichen CPU-Kernen laufen. Man kann sich das so ähnlich vorstellen, dass jeder Client einen eigenen Thread bekommt.<br>
        Daher ist neben der reinen Taktzeit der CPUs auch die Frage: wieviele CPU Kerne stehen für den Application Server zur Verfügung wichtig.<br>
        Hier kommt mit dazu, steht die CPU nur für **Kieselstein ERP** zur Verfügung oder ist der Server mit anderen Aufgaben belastet. Wenn es um Geschwindigkeit geht, so stellen Sie sicher, dass der Rechner ausschließlich für **Kieselstein ERP** (und den Datenbankserver) zur Verfügung steht.<br>
        Hier ist ein weiterer wichtiger Punkt die interne Struktur der Rechner / des Motherboards. Wir haben hier dramatische Geschwindigkeitsunterschiede bei den verschiedensten Herstellern festgestellt.<br>
    -   Größe des für den Applicationserver zur Verfügung stehenden Rams.<br>
        Je mehr Ram genutzt werden kann, desto weniger muss auf die Festplatte ausgelagert werden. Zuviel Ram würde jedoch bewirken, dass der Garbage Collector (das ist die Funktion die übriggebliebenen Speicher wieder hergibt und zyklisch in Java Programmen aufgerufen wird) seltener zusammen räumt, aber dafür zu lange dafür braucht. Hier kann durch Parametrierung des verwendeten Speichers (der wiederum unterschiedliche Arten hat) eine Optimierung vorgenommen werden.
    -   Geschwindigkeit der Verbindung zwischen Applicationserver und Datenbankserver<br>
        Ist diese intern, also raschest möglich, oder geht sie über eine externe (Netzwerk-)Verbindung
    -   Anzahl und Art und Weise bzw. Größe der laufenden Transaktionen.
        D.h. einerseits,
        -   wie ist **Kieselstein ERP** programmiert<br>
            Bei der Programmierung sind wir von einer pessimistischen und transaktionsorientierten Betrachtung ausgegangen. Pessimistisch: D.h. wenn Sie einen Kunden ändern, so geht dies nur, wenn Sie den Kunden für sich selbst, für diesen einen Client sperren können.<br>
            Transaktionsorientiert: D.h. wenn ein Artikel vom Lager in den Lieferschein gebucht wird, so wird dies in einer Transaktion durchgeführt. Sollte es zu Abbrüchen, warum auch immer kommen, so ist dadurch der Zustand vor der Transaktion gegeben. Während der Dauer der Transaktion sind die Daten für die anderen Benutzer gesperrt. D.h. eine angepasste Arbeitsweise trägt zur Geschwindigkeit bei.<br>
            Im Zweifelsfalle haben wir die Verarbeitung in einer Transaktion gewählt. Unter Umständen kann bei manchen Aktionen dies entfernt werden.<br>
            Hier sind wir gerne bereit auf Ihre Vorschläge einzugehen und gegebenenfalls die Transaktions-Verarbeitungs-Art insoweit zu ändern, dass bei gleicher Sicherheit eine schnellere Verarbeitung erreicht wird.
        -   wie arbeiten Sie mit **Kieselstein ERP**<br>
            Werden z.B. am Morgen alle Auswertungen auf zig parallel laufenden Clients gestartet, oder werden umfangreiche Auswertungen / Berechnungen z.B. Bestellvorschlag, Stichtagsbetragungen des Lagers usw. hintereinander gestartet.
    -   Was wir immer wieder feststellen mussten:<br>
        Es werden heutzutage immer mehr Server in virtuellen Umgebungen installiert. Testet man die Geschwindigkeit von **Kieselstein ERP** am Wochenende / Alleine, so ist diese vollkommen ausreichend. Im laufenden Betrieb während des Tages bricht die Performance deutlich ein.<br>
        Die Ursache liegt hier meist daran, dass:
        -   Die IO Geschwindigkeit der Platten/des Plattencontrollers in der virtuellen Maschine völlig überlastet ist und "oben" ansteht. Bitte prüfen Sie dies mit einem geeigneten Programm, z.B. Performance Manager
        -   Die Belastung des Netzwerkcontrollers immer auf 100% ausgelastet ist und daher die Netzwerkpakete im Stau steckenbleiben. Bitte auch überprüfen.
    - und natürlich, gerade für Datenbank intensive Aktionen, die Geschwindigkeit der Festplatten. Wir raten hier zur Verwendung von SSD Platten. Am Server im Raid 1, Raid10 Verbund. Und natürlich geeignete Serverplatten.

3.  Geschwindigkeit des Clients
    Da auch der **Kieselstein ERP** Client ein reines Java Programm ist, gelten auch hier grundsätzlich die Forderungen an Java Programme.
    -   D.h. je schneller die CPU(s) desto schneller läuft der Client Teil.
    -   Und natürlich: Wieviel Speicher steht für den jeweiligen **Kieselstein ERP** Client zur Verfügung. Rechnen Sie hier mit einem Speicherbedarf je **Kieselstein ERP** Client von mindestens 256 MB (im Ram). Hier kommt noch der Bedarf für das Betriebssystem hinzu, welches üblicherweise nicht unter 512 MB angesetzt werden kann (das war einmal zu Zeiten von WinXP. Rechne für Win11 mit mindestens 4 GB Ram für das Betriebssystem!). D.h. wenn nur Betriebssystem und ein **Kieselstein ERP** Client laufen und sonst nichts (kein Word, kein Outlook) empfehlen wir mindestens 8 GB Ram im Client Rechner zu haben.
    -   Werden mehrere **Kieselstein ERP** Clients parallel eingesetzt, so gehen Sie davon aus, dass für jeden **Kieselstein ERP** Client zusätzlich mindestens 256 MB Ram zur Verfügung stehen sollten.
    -   Wichtig ist auch die Größe und die Auflösung des Bildschirms bzw. die verwendete Darstellungsgröße Ihres **Kieselstein ERP** Clients. Je größer die Bildschirmfläche des genutzten **Kieselstein ERP** Clients (Bitte in Pixel), desto mehr muss vom Layoutmanager verwaltet werden (trotz Vektor orientierten Grafikkarten). D.h. wenn rein die Mindestauflösung von 1024x768 verwendet wird, ist der Client schneller und stabiler, als wenn sie in maximaler Fenstergröße auf einem 1920x1080 arbeiten.
    -   Entscheidender Faktor ist die Netzwerkanbindung zum **Kieselstein ERP** Server.
        Genauer gesagt die sogenannten Latenz-Zeiten. Je schneller die einzelne Paketanfrage vom Client zum Server und wieder zurück gelangt, desto schneller kann der Client reagieren. Hier ist die richtige Netzwerkparametrierung oft der entscheidende Faktor.
        Zusätzlich kommt hinzu, dass der eingesetzte Netzwerkswitch die geforderte Performance bringen muss. Setzen Sie manageable Switch ein, um feststellen zu können, bei welchen Verbindungen Verbesserungsbedarf besteht.
    - Auch der Client greift immer wieder auf die Festplatte zu. D.h. auch hier hat sich die Verwendung von SSD Platten sehr bewährt.

**Wie hängt nun dies alles zusammen?**

Im Client werden einerseits Daten des Servers abgefragt und angezeigt. Hier greift sowohl die Geschwindigkeit des Datenbankservers, des Applikationsservers, der Netzwerkverbindung und auch des Clients. Andererseits werden am Client Daten erfasst, von diesem vorverarbeitet und an den Server gesandt.

Ein Sonderfall sind Auswertungen wie z.B. der Druck einer Rechnung bzw. eine Lagerstandsliste zum Stichtag. Diese Journalauswertung wird komplett am Applikationsserver errechnet und grafisch im Speicher aufbereitet und erst danach an den Client zur Anzeige und zum Ausdruck gesandt. Daraus sieht man, dass auch hier alle drei (mit dem Netzwerk eigentlich vier) Komponenten im Spiel sind und zur gegebenen Geschwindigkeit beitragen.

Je mehr gleichzeitige Benutzer auf **Kieselstein ERP** arbeiten, desto mehr beeinflussen diese auch das Geschwindigkeitsverhalten.

**Noch ein wichtiger Punkt:**

Das "Abwürgen" des Clients, z.B. bei einer lange dauernden Lagerstandsberechnung, bricht den dadurch gestarteten Serverjob nicht ab. Der Server merkt erst am Ende der Berechnung, dass die Daten eigentlich nicht mehr benötigt werden. Lassen Sie daher den Client weiterlaufen und starten Sie gegebenenfalls einen zweiten Client. Dieser wird bei gleichem Rechner und gleichem Benutzernamen nicht als zusätzlicher User gewertet.

#### Netzwerk-Geschwindigkeit messen
Um die Netzwerkgeschwindigkeit, genauer die Round Tripp Zeit zu messen, nutze aus dem Hauptmenü, Hilfe, Info. Hier wird in der letzten Zeile die sogenannte Roundtrip Zeit angezeigt. Also die Zeit, die netzwerktechnisch verbraucht wird, wenn der Client einen Request zum Server sendet, diese eine winzige Operation am Server macht und die Antwort wieder an den Client zurücksendet.
![](Roundtripzeit.png)  
<br>
Typische Zeiten sind:
| Round-trip | Bemerkung |
| --- | --- |
| < 10ms | Internes Netzwerk, gutes Arbeiten möglich |
| < 25ms | WLan Anbindung, Arbeiten möglich |
| < 40ms | WLan Anbindung, Arbeiten bereits mühsam, aber für wenige Aktionen noch möglich |
| > 60ms | schlechte Anbindung, Arbeiten faktisch nicht mehr möglich |

#### Was kann eine Aufrüstung des Servers bringen
Die Geschwindigkeit von **Kieselstein ERP**, aus der Sicht des Anwenders, der vor dem Client sitzt, hängt von verschiedenen Faktoren ab. Als Einleitung siehe bitte obige Aufstellung.

Da immer wieder gefragt wird, was denn ein neuer Server bringen kann, hier eine kurze Zusammenfassung mit unverbindlichen Richtwerten / Faktoren wie sich eine Änderung gegenüber dem Verhalten auswirken kann:
-   Mehreren CPU Kerne werden sich auswirken, insbesondere wenn gleichzeitig mehrere Benutzer darauf arbeiten.
    Ich würde (als Gefühl) einen Faktor von 0,5 je CPU Kern rechnen.
    Beispiel: altes System 2 Kerne, neues System 4 Kerne, Steigerung ca. 50%
-   Eine schnellere Taktfrequenz der CPU wird sich auswirken, ich würde auch hier von 0,5 ausgehen.
    CPU Takt alt: 2 GHz, CPU Takt neu 3 GHz, Steigerung auf ca. 30%
-   Was sich deutlich auswirkt ist, wenn der Server NUR für **Kieselstein ERP** zur Verfügung steht.
-   Schnelleren Platten z.B. SSD und bessere Hardwarestruktur (North & South-Bridge, Rambus usw.) tragen ebenfalls dazu bei

Eine Bremse für die gefühlte Geschwindigkeit können, besonders für die Anwender, "mangelnde" Speicher für die Anzahl Benutzer für gleichzeitige Nutzung auf Terminal Servern sein.

**<u>Wichtiger Hinweis:</u>**

Wir gehen hier von idealen Netzwerkbedingungen aus. Leider mussten wir in der Praxis immer wieder feststellen, dass viel Zeit und Geld in die Hardware investiert wird, aber grundsätzliche Dinge wie interne Domainstrukturen, ordentliche Netzwerkinfrastruktur, Netzwerkverkabelungen die tatsächlich vermessen sind, der Einsatz von Netzwerkswitch, die auch die geforderte Performance bringen hintangestellt wird. Denken Sie hier auch daran, dass manche Rechner ganz versteckt verseucht sein können. Dies finden Sie nur durch eine umfassende Netzwerkanalyse. Bemerkt wird es oft nur dadurch, dass die Mitarbeiter über ein langsames Verhalten klagen.

#### Was kann eine Aufrüstung des Clients bringen?
Für eine Abschätzung gehen Sie einerseits bei der Steigerung der Verarbeitungsgeschwindigkeit für CPU und Speicher von den für Server angeführten Punkten aus.

GigaBit Netzwerke und Performance der Switch wirken sich eklatant in der gefühlten Verarbeitungsgeschwindigkeit aus.

Zu Geschwindigkeitsvergleichen generell.

Gehen Sie hier nur von reproduzierbaren Messergebnissen aus.

Als Mensch hat man sich an die bessere Geschwindigkeit sofort gewöhnt und nach dem dritten Aufruf z.B. einer Artikelliste ist es schon völlig normal, dass diese so schnell ist.

#### Wird ein Virenscanner benötigt?
**Ja für den Client!** Wir raten grundsätzlich dazu, auf jedem Client-Rechner einen geeigneten und aktuellsten Virenscanner einzusetzen. Sollte es auf Clientseite Geschwindigkeitsprobleme geben, so könnte das **Kieselstein ERP** Clientprogramm im Virenscanner ausgenommen werden.

**Bitte nicht am Server!** Am Server raten wir möglichst die gesamte Performance der Hardware für die Geschwindigkeit des Applikationsservers zu verwenden (und nicht um immer wieder die gleichen Dateien zu scannen).

Wie geht das, was ist nun wirklich einzustellen?
-   Installieren Sie den **Kieselstein ERP** Server auf einem Linux-Rechner
-   Stellen Sie in jedem Falle sicher, dass Ihr **Kieselstein ERP** Server in einer friendly Netzwerk-Umgebung läuft. D.h. dass er vor Angriffen jeglicher Art außerhalb seiner selbst bestmöglich geschützt ist.
-   Müssen, z.B. weil der **Kieselstein ERP** Server aus Firmenpolitik-Gründen auf einem Microsoft-Rechner läuft, Virenscanner und Firewalls installiert werden, so stellen Sie unbedingt sicher, dass die Zugriffe der **Kieselstein ERP** Clients nicht durch die Firewall z.B. des Virenscanners blockiert werden.
    Stellen Sie sicher, dass sowohl Datenbank als auch Application-Server beide mit den entsprechend temporären Dateien, von der ewigen Virenscannerei ausgeschlossen sind.

#### Müssen am **Kieselstein ERP** Server Verzeichnisse freigegeben werden?
Mit einer einzigen eventuellen Ausnahme, bitte in keinem Fall!
Bitte stellen Sie sicher, dass das Basis-**Kieselstein ERP** Verzeichnis nicht erreichbar ist. Es könnte bei Freigaben durch unsachgemäße Bedienung bzw. Versehen dazu kommen, dass die ganze **Kieselstein ERP** Server-Struktur gelöscht wird. Das einzige Verzeichnis, welches für geschulte Anwender eingerichtet werden darf, ist ab dem Root-Verzeichnis der Reports.<br>
Info: Für den Zugriff der **Kieselstein ERP** Clients ist keine wie immer geartete Freigabe erforderlich. Der Zugriff erfolgt ausschließlich über die Ports.

#### Sollte ich meinen **Kieselstein ERP** Client auf einem Netzwerklaufwerk installieren?
Technisch gesehen spricht nichts dagegen. Aus Gründen der Netzwerkbelastung raten wir in der Regel jedoch dazu, die Clients lokal zu installieren.

#### Kann ich das Layout speichern?
Ja. [Siehe bitte]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#layout" >}} )

Energiesparen / Energiesparmodus
================================

Bei manchen Anwendungen taucht die Frage auf, sollte man den **Kieselstein ERP** Server, welcher ja über das Wochenende nicht verwendet wird, abschalten, z.B. aus Energiespargründen.

Grundsätzlich ist der Gedanke des Energiesparens sehr zu begrüßen und wird von uns soweit möglich unterstützt. Es sind für das Abschalten des Servers jedoch zwei gegensätzliche Standpunkte zu berücksichtigen.

Der eine ist eben der des Energiesparens. Wenn man dies derzeit (April 2009) in Geldwert umrechnet, so bedeutet das, dass man durch das gezielte Abschalten eines Servers einen geldwerten Vorteil von ca. 3,50 € pro Wochenende (2x24x0,2kWx0,37€) erzielt. Wenn wir nun davon ausgehen, dass zwischen Weihnachten und Neujahr die Server unabhängig von weiteren Überlegungen abgeschaltet werden, so muss von ca. 50 Wochenenden pro Jahr ausgegangen werden, also von einem geldwerten Vorteil von ca. 175,- €.

Dem stehen folgende nachteilige Punkte gegenüber:
- Bei Zeiterfassung oder Zutritt, insbesondere in Kombination mit Online Buchungen / Prüfungen muss der Server zum Buchungszeitpunkt zur Verfügung stehen.
- Der Start eines Server Systems dauert üblicherweise zwischen 10 bis 15 Minuten, manchmal auch deutlich länger.
- Durch das Abschalten des Servers kühlen die Komponenten aus, die Versorgungsspannung bricht zusammen und so weiter. Beim Hochfahren werden die Komponenten wieder erhitzt, also gestresst, die internen Spannungsversorgungen müssen aufgebaut werden usw. Das bedeutet für die Komponenten des Servers ist das Aus- und Wieder-Einschalten Stress und verkürzt dadurch möglicherweise die Lebensdauer des Systems.
- Erhöhter organisatorischer Aufwand, um die Sicherung des letzten Wochentages sicherzustellen.
- Jedes Server-System ist so ausgelegt, dass es sich über die Laufzeit der Anwendung, also des Servers optimiert und dadurch schneller wird. Diese Informationen gehen beim Serverneustart verloren.
- Manchmal sind mit den Servern auch Kommunikationsdienste wie EMail und Fax verbunden. Ist der Server abgeschaltet, stehen diese nicht zur Verfügung. Bitte klären Sie, ob dies für Sie akzeptabel ist.<br>
Hier kommt dazu, dass nach dem Hochfahren des Servers automatisch vom EMail-Dienst alle für Sie anstehenden EMails abgerufen werden. Das kann durchaus zu für einige Zeit verstopften Internetverbindungen führen, was wiederum bedeutet, dass Remotezugänge erst nach einiger Zeit, das können schon mal ein - zwei Stunden sein, wieder vernünftig zur Verfügung stehen.
- Die möglicherweise nachteiligen Auswirkungen auf Rooting Tabellen in den Routern und weitere Nebeneffekte sollten ebenfalls überlegt werden.
- Bei Verwendung mehrerer Server wird das Starten deutlich komplexer und unter Umständen auch langwieriger.
- Oft wird diese Zeit von Ihrem **Kieselstein ERP** Betreuungsteam genutzt, um Updates einzuspielen oder sonstige Arbeiten an Ihrem System durchzuführen. Auch diese Arbeiten sind nur bei eingeschaltetem Server möglich.

Das bedeutet für Sie, gerade das Wiedereinschalten des Servers bis er seinen normalen Betrieb erreicht hat, kann durchaus 3 Minuten oder auch mehr in Anspruch nehmen. Es ist für die ersten 15 Minuten ab dem Einschalten noch kein Arbeiten mit dem System möglich. Teilweise muss auch mit dem Start der Clients gewartet werden, bis die Server laufen. Gehen wir nun davon aus, dass wegen des Serverstarts am Montag ein Mitarbeiter regelmäßig um 20 Minuten früher kommen muss, so sprechen wir hier von Kosten von 10,- € oder mehr pro Wochenende rein für Personalkosten. Die reduzierte Lebenserwartung des Servers ist hier nicht berücksichtigt.

Bitte bedenken Sie diese Punkte bei einer allfälligen Entscheidung.

#### Sollte der Server im Energiesparmodus betrieben werden?
Eine ehrliche Antwort ist, nein. Es ist dies ein Server, welcher jederzeit zur Verfügung stehen sollte.

Selbstverständlich müssen wir unsere Mitwelt schützen, soweit uns nur irgend möglich ist. D.h. für einen stabilen guten Betrieb sollte der Server durchaus so eingestellt werden, dass er so ressourcenschonend wie nur möglich betrieben wird. D.h. selbstverständlich sollte der Monitor echt abgeschaltet werden, es können die Festplatten nach kurzer Zeit (30 Minuten) abgeschaltet werden. Allerdings hat uns die Praxis gezeigt, dass der Energiesparbetrieb von zwei Komponenten meist zu erheblichen Problemen im Betrieb führt. Deshalb raten wir diese Komponenten vom Energiesparmodus auszunehmen.
- CPU<br>
Ein Heruntertakten der CPU hat bei einigen Installationen dazu geführt, dass bei wieder Aktivierung des vollen Betriebes die CPU nicht mehr mit voller Geschwindigkeit arbeitete und daher der Rechner neu gestartet werden musste, was gerade bei Serverbetrieb, aber auch bei Clients entsprechen lästig ist und die Verfügbarkeit von **Kieselstein ERP** enorm beeinträchtigt.
- Netzwerkkarte<br>
Wenn die Netzwerkkarte im Energiesparmodus ist, reagiert sie in den vielen Betriebssystemen oft nicht richtig. Das hat zur Folge, dass es den Anschein hat, dass der **Kieselstein ERP** Server nicht verfügbar ist. Geht man nun direkt zum Server und betätigt eine Taste so wird der Energiesparmodus der Netzwerkkarte beendet und alles verhält sich wieder normal. Sollten Sie dieses Verhalten feststellen, schalten Sie bitte den Energiesparmodus der Netzwerkkarte ab. Wir sind hier generell der Meinung, dass der geringe Mehrverbrauch an elektrischer Energie für die Netzwerkkarte, für den Serverbetrieb, in absolut keinem Verhältnis zu den unter Umständen erforderlichen Gesamtaufwänden steht.

#### Wie kann am MAC / OS X der Energiesparmodus abgeschaltet werden?
In einigen Installationen wird ein MAC / MAC Mini als **Kieselstein ERP** Server verwendet. Auch hier kommt es zu den oben beschriebenen Verhalten, weshalb auch am (Server) MAC der Energiesparmodus abgeschaltet werden sollte.
Zusätzlich stellen Sie bitte bei echtem Serverbetrieb unter Optionen, nach Stromausfall automatisch neu starten ein, damit Ihr **Kieselstein ERP** Server möglichst immer zur Verfügung steht.

#### Kann **Kieselstein ERP** in einer DMZ betrieben werden
Ja selbstverständlich. Wichtigste Voraussetzung ist, dass das Tunneling in die DMZ vollständig ist.

So gibt es bei OpenVPN zwei Arten zu Tunneln. Verwenden Sie hier bitte die Einstellung TAP-Device.

#### Was ist bei einem Austausch des **Kieselstein ERP** Servers generell zu beachten?
Nach einigen Jahren des Betriebes ist es immer wieder erforderlich den **Kieselstein ERP** Server durch eine neue Hardware zu ersetzen. Da unser Ziel ist, auch hier einen möglichst zügigen und reibungslosen Übergang zu ermöglichen, eine kurze Beschreibung der Vorgehensweise. Bitte berücksichtigen Sie, dass EDV Betreuer die Übersiedelung Ihres **Kieselstein ERP** Servers immer wieder unterschätzen und leider davon ausgehen, dass es reicht einige Files zu kopieren. 

Dem ist definitiv nicht so, es müssen der Datenbankserver und der Applikationsserver umgezogen / gesiedelt werden. Bei einer guten Vorbereitung kann die Umschaltung in kurzer Zeit durchgeführt werden. Unvorbereitete Umstellungen verursachen Stress und Kosten auf allen Seiten, der nicht notwendig ist. Gehen Sie daher bitte wie folgt vor:
1.  Reservieren Sie mindestens vier Wochen vor der Umstellung die Umstellungsunterstützung bei uns bzw. Ihrem **Kieselstein ERP** Betreuer.
2.  Planen Sie einen Parallelbetrieb der alten und der neuen Hardware.
3.  Stellen Sie uns einen Fernwartungszugang auf die neue Hardware, parallel zum Zugang zur alten Hardware zur Verfügung.
4.  Planen Sie ausreichend Zeit (eine Kalenderwoche) für die Installation von **Kieselstein ERP** auf der neuen Hardware ein. Damit können wir in aller Ruhe die Installationsarbeiten durchführen. Da oft eine Menge Daten auf die neue Hardware zu transferieren ist, benötigt dies eine gewisse Dauer. Wenn das nebenher gemacht werden kann, ist es entsprechend kostengünstiger.
5.  Testen Sie die Funktion von **Kieselstein ERP** komplett durch, bis hin zur Datensicherung, zum Zugriff auf die täglich erzeugten Sicherungsdateien durch Ihr Datensicherungssystem.
6.  Planen Sie den Tag der Umstellung, stimmen Sie den Termin mit uns und mit ihrem EDV-Verantwortlichen ab. Es müssen alle drei Beteiligten gut verfügbar sein.
7.  Bei der Durchführung der Umstellung werden die Datenbanken vom Altsystem auf die neue Hardware eingespielt.<br>
Es müssen die Clientzugriffe entsprechend umgestellt werden, oder es werden die Namen / Adressen zwischen Alt- und Neusystem entsprechend ausgetauscht.
8.  Testen Sie den Zugriff aller Clients auf das neue System. Denken Sie auch an VPN Tunnel, Zeiterfassungsterminals, Browser-Terminals usw.

Bei Berücksichtigung dieser wenigen und einfachen Punkte kann die eigentliche Umstellung meist innerhalb weniger Stunden z.B. an einem Freitagnachmittag oder sogar an einem Abend durchgeführt werden.