---
categories: ["Hilfe"]
tags: ["Hilfe", "Hilfe bearbeiten"]
title: "Installation Hilfeeditor"
linkTitle: "Hilfeeditor"
weight: 2000
date: 2023-02-01
description: >
 Wie die Hilfe bearbeiten
---
## Der eigentliche Editor
Für das Editieren der Hilfeseiten aufgrund eines Vorschlages von Steffen Bätz wird das Visual Studio Code mit MarkDown verwenden.<br>
Visual Studio Code herunterladen = https://code.visualstudio.com/download
Starten, bei der Frage nach den Themes habe ich Light modern eingestellt, ![](Hintergrund_bei_start.png)<br>
bzw. links unten das Zahnrad, Designs, Farbdesigns ![](Themes_auswaehlen.png) und dann Light Modern ![](Hintergrund.png). Ist am ehesten für mich lesbar.

Wenn man nun das Preview Fenster auch sehen will, so aus dem Text-Editor Strg+K und dann V drücken. So kommt rechts ein zweites Fenster, in dem man die Preview sieht.

**Achtung:** jetzt wieder in das linke Editor Fenster wechseln, sonst ändert man den Inhalt des Previews, was man ja nicht will.<br>
Also immer im linken Fenster arbeiten.

Eine Beschreibung der vielen Text-Commandos siehe z.B. https://www.markdownguide.org/basic-syntax

Für Kopieren der Bilder aus Zwischenablage [siehe](#kopieren-von-html-und-bildern)

### Zeichnen
Zusätzlich noch das Drawio Plugin für Visual Studio Code einrichten.
Man muss dann die Files  mit filename.drawio.svg benennen, dann kann man diese direkt bearbeiten.\
Beispiel:\
![](test.drawio.svg)\
d.h. das File dann im Verzeichnisbaum doppelklicken (ein bisschen Geduld) und dann im drawio bearbeiten. Zum Speichern auf Datei, Save gehen (Strg+S geht <u>nicht</u>).

### Formeln
Darstellen von Formeln usw.. Dafür einbinden von Latex um die Formeln darzustellen. https://www.docsy.dev/docs/adding-content/diagrams-and-formulae/<br>
Denk daran die ..\user-doc\config.toml zu ergänzen.
> Latex aktivieren für Formeln\
[params.katex]\
enable = true\
>

## Commandos / Basic Syntax 
### für Visual Studio Code
https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf

### für MarkDown
https://www.markdownguide.org/basic-syntax oder auch https://markdownmonster.west-wind.com/docs/_4rd0xjy44.htm

### Commandos die ich gerne nutze
| Commando | Wirkung |
| --- | --- |
| Strg+K & V  | Preview Fenster in der rechten Bildschirmhälfte. An das Zurückwechseln denken |
| Alt+Strg+V | Einfügen aus der Zwischenablage, je nach Datentyp kommt dann<br>Wenn Bild: Ein Dialog, mit dem der Filename angegeben wird. ACHTUNG: Überschreibt ohne Rückfrage.<br>Praktischer Trick, wenn überschrieben werden sollte, den Link auf das Bild (Rufzeichen usw.) vorher markieren.<br>Einfügen aus HTML. D.h. das vollständige HTML in die Zwischenablage kopieren. (Vollständiges HTML im Total mit F3 öffnen und dann 1 drücken für reinen Text und dann erst markieren) |
| \<br> | Zeilenumbruch erzwingen |
| \<u> \</u> | Underline ein- / ausschalten |
| \** | **Fett** ein- / ausschalten |
| \* | *Kursive* ein- / ausschalten |
| - | Auflistung ohne Nummerierung. Wenn unter Einrückungen, dann mit Tab eins weiter nach rechts reinrücken. An den Space danach denken. Geht angeblich auch mit * und + |
| 1. | Auflistung mit Nummerierung (abc geht leider nicht). Auch hier mit Tab für eine Unter-Nummerierung reinrücken. Wichtig der Space nach dem . (und keine Klammer) |
| - a.) | Da wir gerne abc nutzen, vorher das - verwenden und dann manuell das ABC angeben |
| Strg+K & C | Comment: HTML Auskommentieren des markierten Textes. Geht nur innerhalb **einer** Zeile |
| Strg+K & U | Un-Comment: Die HTML Auskommentierung wieder entfernen |
| \[Link](#) | Liefert alle Links innerhalb dieser Datei, um hinter dem Link den "GoTo" zu hinterlegen |
| Strg+P | Liste der offenen Files |
| Strg+Shift+P | alle Markdown Befehle |
| \``` | Darstellung von Code-Blöcken ohne Formatierung durch Markdown<br>muss ein- und aus-geschaltet werden |
| Strg+Ö | Startet das Terminal = Windows PowerShell und dann ins Laufwerk wechseln, aufs Verzeichnis gehen (cd .\Git_KES\user-doc\) und dann Hugo starten (hugo server --disableFastRender --navigateToChanged)
| Strg+Shift+H | Wenn links oben ![](Alles_bearbeiten.png)  das Verzeichnis der gesamten Dokumentation (Content) ausgewählt ist, so kann damit ein Replace in allen Files vorgenommen werden. <br>**ACHTUNG:** es gibt kein Undo und wenn man sich vertippt hat, ist es in allen Dateien so. |

### vom htm zum RelRef
Eine Beschreibung wie man die einmalige Aktion vom Anker \<a \</a bzw. *.htm zum relref kommt.
Ich nutze dafür Tastentrick.de, kostet 30,- € und habe auf die F11 die Mustersequenz hinterlegt.

D.h. zuerst suchen in allen *.md Dateien nach .htm mittels Totalcommander. Liefert aber nur eine Liste, was noch alles zu übertragen ist. Diese in ein Spreadsheet eintragen. Man kann sich das SpreadSheet mit obigem, Strg+Shift+H ersparen.

Zusätzlich kann MarkDown beim Strg+Shift+H auch Regular Expressions. D.h. man kann damit direkt "Übersetzungen" durchführen. Auch hier ACHTUNG: Es gibt kein Undo.
- Ersetze: ![](Regular_Expression_01.png)  
- Durch: (Inhalt des Kommentars) ![](Regular_Expression.png)  

## Die HTML-Erzeugungs-Maschine
Um die gesamte Hilfe als schnelle statische HTML Seite zu bekommen, kommt der Google-Dienst Hugo zum Einsatz. In diesem ist auch eine "Google-" Suche mitintegriert, damit Sie rasch zu den gewünschten Themen finden.

### Installation von Hugo
1. https://gohugo.io/installation/<br>
Die weitere Beschreibung geht von einer Windowsumgebung aus
2. https://gohugo.io/installation/windows/
3. Runtersuchen/scrollen zu Prebuilt binaries und Visit the latest release bzw. die darunter ausgeführten Installations-Punkte ausführen. Denken Sie auch daran die Path Variable zu ergänzen.
**ACHTEN Sie darauf die extend Version zu verwenden, sonst kommt es zu Fehlern des Hugo.**
Ev. müssen Sie dann auch noch "npm install postcss-cli" ausführen.
4. Nun muss als nächstes **go** installiert werden. Wählen Sie dafür https://go.dev/doc/install. Nach erfolgreicher Installation starten Sie ein neues Command Fenster und geben *go version* ein. Nun muss die Versionsnummer angezeigt werden.
5. Und nun muss noch NodeJS installiert werden. Wechseln Sie dafür auf https://nodejs.org/en/download/
6. In den weiteren Beispielen ist der Hugo Server auf d:\Hugo\hugo.exe und der Inhalt der / dieser Hilfe auf d:\Git_KES\user-doc\content
7. Wechseln Sie nun in das Root-Verzeichnis der Dokumentation. Das ist in unserem Beispiel: d:\Git_KES\user-doc\
8. Starten Sie nun den Hugo-Server durch die Eingabe von Hugo (ohne alles)
und beobachten Sie die Ausgabe. Es wird mit dem Download der eigentlichen Module begonnen. Dies kann je nach Leistungsgeschwindigkeit etwas dauern.
9. Sie finden die soeben erzeugte Webseite unter ..\user-doc\public\index.html
10. Einfacher ist es, wenn Sie den Hugo mit dem Parameter Server starten.
Damit erhalten Sie nach dem Start die Info:
Web Server is available at http://localhost:1313/user-doc/
11. Nun starten Sie einen Browser z.B. Firefox oder Chrome und rufen
http://localhost:1313/user-doc/ auf.
Der Hugo Server reagiert innerhalb weniger Sekunden auf Ihre Textänderungen in der Hilfe.

#### Parameter für den Hugo Server
- --navigateToChanged --> Springe in der HTML Darstellung auf die soeben geänderte Seite
- --disableFastRender --> baut den Index neu auf. Praktisch, wenn eine Seite nicht funktionieren will 

#### Update von Hugo
wie oben beschrieben auf latest Release gehen und das passende Zip herunterladen. Z.B. hugo_extended_0.128.2_windows-amd64.zip

Die heruntergeladene Datei entzippen und idealerweise über die bestehende Installation drüber-kopieren.

Danach solltest du in jedem Falle den Hugo mit<br>
```bash
hugo server --disableFastRender --navigateToChanged
```
neu starten. Hier kann es aufgrund der strengeren Prüfung durchaus zu Warnungen und Fehlermeldungen kommen. Diese bitte unbedingt beheben.

### Dateistruktur
Für eine effiziente Verwendung mit Hugo müssen einige kleine Konventionen in der Dateistruktur und auch im Inhalt der md Dateien eingehalten werden.
1. in jedem Verzeichnis gibt es eine Datei _index.md. Dies ist faktisch die Startdatei dieses Unterverzeichnisses.
2. weitere Kapitel kommen in eigene Unterverzeichnisse mit jeweils einer eigenen _index.md Datei. Damit wird erreicht, dass sowohl in VSC als auch in der durch Hugo gerenderten Web-Seite die Bilder dargestellt werden.
2. Die Reihenfolge ergibt sich aus .....
3. Beachten Sie auch, dass der Kopfblock faktisch die Überschrift des Kapitels ist und somit weitere Unterpunkte nur mehr mit **##** geschrieben werden dürfen.
4. Damit Hugo diese einbindet, müssen die Kopfinformationen, categorie, tags usw. gegeben sein. Damit erreichen Sie ein automatisches Auflisten in der Menüstruktur. Zugleich bedeutet dies aber auch, dass je eigenes Kapitel ein eigenes Unterverzeichnis mit einer eigenen _index.md Datei erforderlich ist.
5. Die Verlinkung unter den Dateien kann auf die ## Überschriften hin erfolgen. Es kann dafür auch der Syntax für Hugo verwendet werden. Siehe https://gohugo.io/content-management/cross-references/. 
6. Sortierung der Dateien in der linken Auflistung. Diese erfolgt nach Default: Weight > Date > LinkTitle > FilePath. 

## Einfügen aus Zwischenablage
Ist üblicherweise in Visual Studio Code bereits enthalten.
Kopiere das gewünschte Bild in die Zwischenablage und drücken dann Strg+Alt+V

Die Frage ist schlichtweg, wie verwaltet man hier sprechende Filenamen, sonst hat man überhaupt keinen Überblick mehr.

**Hinweis:**
Die Zuordnung von Bildern zu den entsprechenden Inhalten funktioniert nur für die _index.md Dateien. Verwendet man andere Dateinamen, z.B. quasi als Unterfile, werden die Bilder zwar in der Vorschau, aber nicht im mittels Hugo kompilierten File angezeigt.

## Zeichnen von Diagrammen usw.
Hier ist das Draw.io Plugin zu empfehlen. http://draw.io/
![](drawio.png)

struktur.drawio.svg und wie geht das jetzt?

## Zeichensätze übernehmen über die Zwischenablage
Gerade bei den unterschiedlichen Betriebssystemen ist es wichtig, dass die Kommandos OHNE den HTML Anpassungen sauber übernommen werden können. Füge daher
```bash
```bash
```
An den Beginn deiner beispielhaften Kommandos ein. Damit erhält der Anwender rechts oben das Kopieren in Zwischenablage, womit hoffentlich die Probleme der Übernahme beseitigt sind.

## Rechtschreibprüfung
Installation der Markdown Erweiterung Code Spell Checker<br>
und Installation der deutschsprachigen Erweiterung https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-german

Daran denken, dass man nach der Installation noch einmal auf Extensions geht und mit Rechtsklick auf German Code Spellchecker auf Apply Extension to all Profils wählt, damit das auch in allen Dateien angewandt wird.

Ich habe zusätzlich noch ![](Deutsch_fuer_VisualStudioCode.png) German Language Pack for Visual Studio Code installiert.

## Kopieren von (HTML) und Bildern
Markdown Paste von <u>*telesoho*</u> über Extensions ![](Extensions.png) installieren<br>
Um HTML zu kopieren, den vollständigen HTML Text inkl. aller HTML Commands markieren, in die Zwischenablage kopieren und in ein Markdown File mit Strg+Alt+V einkopieren. Je nach Datenmenge kann das durchaus einige Zeit in Anspruch nehmen.<br>
**WICHTIG:** Wenn das Einfügen nicht gehen sollte, am besten an den Bildern aus der Zwischenablage testen, so liegt das an den Trusted Restrictions.

### Trusted Restrictions
Gerade bei neuen Installationen oder Verzeichnissen muss man den Dateien und Verzeichnissen vertrauen.<br>
Gegebenenfalls erscheint am oberen Bildschirmrand ein
![](Trusted_Restricted.png)<br>
Klicke hier auf Manage und gib das gesamte Verzeichnis frei.

Oder du klickst im Menü auf Help und dann Show All Commands<br>
![](ShowAlleCommands.png)<br>
und suchst dann nach Trust.

![](Trusted_Workspace.png)<br>
und wählst nun Manage Workspace Trust.<br>
Hier nun weiter und add Folder, bzw. siehst du hier auch, was schon alles freigegeben ist.

Du merkst das auch daran, dass z.B. das Strg+Alt+V nicht reagiert. Dann zuerst das Verzeichnis wie oben beschrieben freigeben.

## Die meist genutzten MarkDown Steuerzeichen
**fett**<br>
*kursive*<br>
***fett und kursive***<br>
<u>Unterstrichen</u> wird nur durch HTML Steuerzeichen unterstützt.

> Blockeinrückungen werden mit > am linken Rand erreicht und werden durch ein weiteres > in einer leeren Zeile wieder beendet.
> 
>> mehrere Ebenen eben mit mehreren >

## Kommentare einfügen
Baut auf der HTML Kommentar-Syntax auf. Direkt im VSC Editor den Kommentar markieren und dann Strg+K und Strg+C bzw. um den Kommentar wieder zu entfernen Strg+K und Strg+U (uncomment)

## einige grundsätzliche Regeln
### Bilder
Bilder werden immer im jeweiligen Modulordner abgelegt (und nicht im IMG oder Assets Unterordner)
### Dateinamen
Werden immer ohne Umlaute / deutsche Sonderzeichen geschrieben. Offensichtlich ist es auch zu empfehlen, anstelle von Leerstellen / Spaces Underlines in den Dateinamen, auch der Bilder, zu verwenden.
### Sortierung im linken Auswahlmenü
Um die Sortierung zu erreichen, wird nur das Attribut Weight verwendet. Dies definiert die Sortierung innerhalb des jeweiligen Vorgängerverzeichnisses.
### Kapitelüberschriften
Es werden die ## für die (Kapitel-) Überschriften genutzt. Damit kann auch darauf verlinkt werden.


## Verlinken
### in Unterverzeichnisse absolut
Innerhalb der Dokumentation auf ein völlig anderes Kapitel o.ä. springen. D.h. man geht vom Root der Dokumentation aus.<br>
Beispiel:<br>
[Mengeneinheiten]( {{<relref "/docs/stammdaten/artikel/mengeneinheit"  >}} ) springt vom Root weg genau auf die Seite (= Unterverzeichnis) Mengeneinheit. In diesem Verzeichnis muss es ein _index.md geben.<br>
[Positionsarten in den Verkaufsmodulen]( {{<relref "/verkauf/gemeinsamkeiten"  >}} )
### in Unterverzeichnisse relativ
Vom Verzeichnis dieser _index.md ausgehend in ein Unterverzeichnis.<br>
Beispiel:<br>
[Mengeneinheiten]( {{<relref mengeneinheit >}} ) springt nur auf die Seite (= Unterverzeichnis) Mengeneinheit. In diesem Verzeichnis muss es ein _index.md geben.<br>
Bedenke: Bei eindeutigen Unterverzeichnisnamen kann Hugo auch relative Angaben auflösen. Einzig bei mehrdeutigen Unterverzeichnisnamen muss die absolute Pfadangabe gewählt werden.

Kurzfassung zum Einkopieren:<br>
```bash( {{<relref "" >}} )```

**WICHTIG:**<br>
Nach dem Lablenamen in eckigen Klammer muss OHNE Space dazwischen in runden Klammern der Aufruf stehen. Hier ist wieder wichtig, dass nach dem Rundeklammer auf ein Space kommt und dann erst die geschwungenen Klammern.<br>
Wird in der Vorschau der Link nicht aufgelöst, so ist in der Regel hier der Fehler zu suchen.

**Hinweis:**<br>
Es ist die relative Verlinkung zu bevorzugen, da damit auch Verlagerungen / Reorganisation der Verzeichnisstruktur möglich sind, ohne lange die absoluten Pfade korrigieren zu müssen.

### Verlinkung auf eine Kapitelüberschrift
Will man eine direkt zu einer Kapitelüberschrift springen ist nach dem Pfad mit # dann die Kapitelübeschrift in Kleinbuchstaben und Spaces gegen - getauscht anzugeben. Sind darin auch noch Umlaute enthalten landet man schnell bei der HTML Notierung. Hier ist am einfachsten die Seite mit dem Ziellink im Browser zu öffnen und den Link herauszukopieren und entsprechend anzugeben.<br>
Beispiel:<br>
[Muster Link direkt zu einer Überschrift]( {{<relref "/docs/installation/11_barcode/#barcode-definitionen-f%C3%BCr-materialbuchungen" >}} )

### Verlinken direkt in den Text
Manchmal will / muss man mitten in einen Text / eine Tabelle verlinken.<br>
Nun ist ```<a name >``` tag seit html 5 outdated, und sollte durch ```<span id="Kundenartikelnummer">``` ersetzt werden.

Will man nun aus anderen Dokumenten direkt darauf verlinken, dann muss das z.B. lauten
```[Siehe Kundenartikelnummer]({{<relref "/warenwirtschaft/stueckliste/importe/intelligent/" >}}#Kundenartikelnummer)```

### old
- Verweise auf andere Dateien, Unterverzeichnisse sollten mit z.B.[Mengeneinheiten]( {{<relref mengeneinheit >}} ) gemacht werden. Das Wort in eckiger Klammer wird in der interpretierten Webseite als Link dargestellt, welcher auf das dahinter angegebene Verzeichnis, mit einer _index.md, verweist. relref bedeutet relative Referenz in der gesamten Dokumentation. **Beachten Sie:** die Verzeichnisnamen und auch die Anker (anchor) müssen immer in <u>Kleinschreibung</u> angegeben werden. Zusätzlich muss bei Verwendung der Anker der gesamte Pfad (inkl. Dateiname bzw. Verzeichnis) in Hochkomma gesetzt sein. 
Beispiel:<br>
[Mengeneinheiten]( {{<relref "mengeneinheit" >}} ) springt nur auf die Seite Mengeneinheit.

[Bestellmengeneinheiten]( {{<relref "mengeneinheit.md#bestellmengeneinheiten" >}} ) 

Beachten Sie bitte zusätzlich, dass Leerzeichen in den Ankernamen durch - (Bindestrich) ersetzt werden.
[Praktisch ist:]( {{<relref "20_hilfeeditor#was-noch-ganz-praktisch-ist" >}} ) 

Hinweis: Beim Einkopieren passiert es immer wieder, dass doppelte (( )) runde Klammern entstehen. Damit geht der Link nicht und zeigt nur den Inhalt an. Es darf nur eine runde Klammer, Space, 2     x geschwungene Klammer sein.

### Verlinkung innerhalb einer Datei selbst
Geht relativ einfach mit eckiger Klammer und der Angabe des Ankers in runder Klammer, beginnend mit #. Tippt man das ein, kommt ab dem # sofort die Auswahliste der Anker innerhalb der Datei. **ABER:** alles in Kleinbuchstaben<br>
und Leerzeichen werden durch - Bindestrich im Link ersetzt. So geht die Verlinkung auf [Link innerhalb der Datei] nicht, aber [Link-innerhalb-der-Datei](#verlinkung-innerhalb-einer-datei-selbst) funktioniert wunderbar. Sollte man sich bei einem Link einmal nicht sicher sein, einfach den Link anlegen und speichern und dann im Browser beim Hugo-Link nachsehen.

### Verlinkung zum Download einer lokalen Datei
Manchmal möchte man auch lokale Dateien zur Verfügung stellen.
Dies ist meist auch relativ zum Verzeichnis der bearbeiteten md-Datei. Hier gibt man z.B.
[den Text was das ist]( ./unterverzeichnis/datei_zum_download.txt) an (siehe auch Jasperstudio).<br>
Mit obigem Link kommt natürlich nicht gefunden.

### Tabellen
werden mit 
| Spalte 1 | Spalte 2 rechtsbündig | Mittig |
| --- | --: | :-: |
| kurz | Zahl | X |

geschrieben. Sollte die Spalte rechtsbündig ausgerichtet sein, dann hinten ein Doppelpunkt. Sollte sie mittig ausgerichtet werden, so :-:

Wenn man keine Überschrift will, dann einfach die Überschriften durch drei Spaces ersetzen. Liefert dann nur einen beginnende Zeile.

Ist eine der Spalten mit zu viel Text gefüllt, so kann man angeblich mit vielen &nbsp; welche ohne Leerräume geschrieben sein müssen, die Breite erzwingen.

### Beschreibung wie ein Artikel aufgebaut sein sollte
1. zuerst den Teaser
2. Der Schwerpunkt sollte auf jeden Fall auf dem Modul und seinen Funktionen liegen. Beschreiben Sie nur eine Funktion nach der anderen - verraten Sie nicht alle Funktionen
3. Dann einen längeren beschreibenden Text und dann die einzelnen Punkte des Moduls
4. Das Ziel ist immer zuerst eine Beschreibung zu haben, wofür was gut ist. 
Zuerst sehr allgemein und dann detaillierter.<br>
Orientieren auch am Aufbau eines Werbeemails

## was noch ganz praktisch ist:
- Warnung:
{{% alert title="Warning" color="warning" %}}
Beachten Sie bitte ....
{{% /alert %}}

- Dies dient zu Ihrer Info
{{% alert title="Info" color="info" %}}
Information speziell für Sie / zu diesem Thema
{{% /alert %}}

- ganz besonders wichtig, als erstes zu beachten
{{% alert title="Primary" color="primary" %}}
This is a Primary.
{{% /alert %}}

- Seiteninfo
{{% pageinfo %}}
Hier die Seiteninfo
{{% /pageinfo %}}

## ToDo vor dem Einchecken
Man hat, um die optische Erscheinungsform jederzeit prüfen zu können, sowieso den Hugo immer laufen. D.h. theoretisch sollten Tipp und Link-Fehler sofort auffallen. Leider ist dem nicht immer so, was vermutlich der Komplexität geschuldet ist.

Daher: Vor dem Einchecken den Hugo beenden und mit Rebuild neu starten.<br>
Dann eventuelle Fehler beseitigen und danach erst einchecken.
![](Hugo_Restart.png)  

## Konvertieren mit Hugo, aufbauend auf DocSys
Unsere Einstellungen:
- ..\user-doc\\.vscode\settings.json 

For large documentation sets we recommend adding content under the headings in this section, though if some or all of them don’t apply to your project feel free to remove them or add your own. You can see an example of a smaller Docsy documentation site in the [Docsy User Guide](https://docsy.dev/docs/), which lives in the [Docsy theme repo](https://github.com/google/docsy/tree/master/userguide) if you'd like to copy its docs section. 

Other content such as marketing material, case studies, and community updates should live in the [About](/about/) and [Community](/community/) pages.

Find out how to use the Docsy theme in the [Docsy User Guide](https://docsy.dev/docs/). You can learn more about how to organize your documentation (and how we organized this site) in [Organizing Your Content](https://docsy.dev/docs/best-practices/organizing-content/).

## Welche Icons werden verwendet?
Selbstverständlich haben wir uns bemüht alle Programmteile in der passenden Lizenz (AGPL) zu verwenden. Daher kommen auch nur freie Icons zum Einsatz. Wir haben dafür https://www.iconfinder.com/search?price=free&license=gte__1 verwendet.

## Wo wird die generelle Description eingestellt?
Google nutzt ja sein eigenes Tool, um damit auch kurze Infos in den jeweiligen Suchergebnissen anzuzeigen. Die generelle Einstellung findet man unter .../config.toml, hier der Eintrag "description". Existiert dieser nicht, kommt das beste HMTL Tool der Welt.

## Was sonst noch so alles aufgefallen ist
### Anzeige der Links auf den Überschriften
Muss anscheinend extra aktiviert werden:<br>
https://www.docsy.dev/docs/adding-content/navigation/#heading-self-links

## Was ich sonst noch so eingestellt habe
- Zeilenumbruch auf Wortbasis einschalten: View Toggle Word Wrap
![](Zeilenumbruch.png)
- Request Completitions abschalten: ![](Completitions_abschalten.png)
- Auto Save off<br>
![](AutoSaveOff.png)<br>
Diese Funktion ist einerseits praktisch, andererseits wird mit jeder Speicherung das Neu-bilden der html Seiten gestartet, was deinen Rechner enorm belastet und so ein "Ansehen" des Ergebnisses im Browser massiv erschwert.<br>
Natürlich musst du nun selbst auf die Speicherung deiner Dateien achten.
