---
title: "Installation Client zu Kieselstein ERP unter Windows(r)"
linkTitle: "Windows"
categories: ["Client Installation"]
tags: ["Windows Client"]
weight: 30
description: >
  Installation Client zu Kieselstein ERP Server unter Windows(r)
---
Siehe bitte allgemeine Client Beschreibung

## Abschalten der Einrichtemeldung
### Windows 10
Siehe: https://support.microsoft.com/de-de/windows/benachrichtigungen-zur-ger%C3%A4teeinrichtung-deaktivieren-c88f6943-d169-23ca-5f3f-c6b927509e79

Übernommen aus obigem Artikel.

Nach jedem Neustart schlägt Windows vor, das Einrichten des Geräts zu beenden. Um dies unter Windows 10 abzuschalten, gehe bitte folgendermaßen vor:
1.  Start, Einstellungen, System, Benachrichtigungen und Aktionen
2.  Deaktiviere unter **Benachrichtigungen** das Kontrollkästchen neben **Möglichkeiten vorschlagen, wie ich mein Gerät so einrichten kann, dass Windows optimal genutzt wird**.

### Windows 11
1. Start, Einstellungen, System, Benachrichtigungen
2. ganz nach unten scrollen und zusätzliche Einstellungen aufklappen
3. ich persönlich entferne alle drei Häkchen
