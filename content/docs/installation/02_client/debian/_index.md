---
title: "Installation Kieselstein ERP Client unter Debian"
linkTitle: "Debian"
categories: ["Client Installation"]
tags: ["Debian Client"]
weight: 10
description: >
  Installation eines Kieselstein ERP Clients unter Debian
---
Den **Kieselstein ERP** Client auf einem frischen Debian installieren




- pgadmin installieren -->> siehe Clientbeschreibung
	siehe https://www.itzgeek.com/how-tos/linux/debian/how-to-install-pgadmin-on-debian-11-debian-10.html
	sudo apt update
	sudo apt install -y apt-transport-https ca-certificates software-properties-common curl

	# Debian 11
	curl -fsSL https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo gpg --dearmor -o /usr/share/keyrings/pgadmin-keyring.gpg

	# Debian 11
	echo "deb [signed-by=/usr/share/keyrings/pgadmin-keyring.gpg] https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/bullseye pgadmin4 main" | sudo tee /etc/apt/sources.list.d/pgadmin4.list

	sudo apt update
	sudo apt install -y pgadmin4-desktop
	Das Programm einfach unter Aktivitäten, Anwendungen (die Punkte unten) und pgadmin4 eingeben (rechtsklick, zu Favoriten)

	Wenn man auch die WebObefläche nutzen will (was ich persönlich nicht mag)
	sudo apt install -y pgadmin4-web	(ist das wirklich erforderlich?)

	sudo /usr/pgadmin4/bin/setup-web.sh

	pgadmin EMail Adresse postgres@wa-consulting.at
	pw: postgres
	
	Frage nach Installation des Webservers mit y beantworten und dann restart Apache Webserver und dann
	http://127.0.0.1/pgadmin4

