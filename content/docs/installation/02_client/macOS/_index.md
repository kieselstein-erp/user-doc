---
title: "Installation Client zu Kieselstein ERP unter macOS"
linkTitle: "macOS"
categories: ["Client Installation"]
tags: ["macOS Client"]
weight: 40
description: >
  Installation Client zu Kieselstein ERP Server unter macOS
---
## Installation des Clients unter macOS
Du kannst den Client deines **Kieselstein ERP** auch unter macOS betreiben.
Wichtig ist von Anfang an zu unterscheiden, mit welchem Prozessor dein MAC ausgestattet ist. Entweder X64 oder Arm. Diese Information findest du unter Apfel, über diesen MAC<br>
![](client_installation_00.png)  <br>
und dann<br>![](client_installation_00b.png)  <br>
Der Apple M1 ist das erste Arm-basierte System-on-a-Chip (SoC) von Apple für seine Mac-Computer. [Wikipedia](https://de.wikipedia.org/wiki/Apple_M1)

Das benötigte Java findest du unter
https://www.azul.com/downloads/?version=java-11-lts&os=macos&package=jdk-fx#zulu

Hier:
![](java_4_macos.png)  <br>
Nun das entsprechende Paket auswählen,<br>
![](paket_4_macos.png)  

in der Regel .dmg.

In die Downloads gehen und<br>![](Zulu_installieren.png)  <br>
mit einem Doppelklick das JDK11 von Azul installieren.

![](Zulu_installieren_02.png)  

Du kannst nun prüfen, ob die soeben installierte Javaversion auch richtig installiert wurde. Dazu ein Terminal öffnen und Java -Version eingeben.<br>
![](Zulu_installieren_03.png)  

Starte nun den Safari / deinen Browser gehe dazu auf http://IPADRESSE_KIESELSTEIN_ERP:8080.
gehen. Hier wird von der Kieselsteininstallation<br>
![](client_installation_01.png)  <br>
bereitgestellt.<br>
Die Client.zip rechts anklicken und herunterladen.<br>
![](client_installation_01a.png)  <br>
Als Ort den Schreibtisch auswählen.
Mit einem Doppelklick entzippen.

Ausführenberechtigung setzen:<br>
Mit dem Terminal (Programme, Dienstprogramme, Terminal) <br>
![](client_install_01c.png)  <br>

in das Verzeichnis wechseln.
![](client_install_01d.png)  


mit ![](client_installation_1b.png)  <br>
chmod +x ./kieselstein-ui-swing<br>
die Ausführenberechtigung für das Shellscript setzen.

Nun im Finder in das Verzeichnis (Schreibtisch, Kieselstein, bin) wechseln und<br>
![](client_installation_02.png)  

rechte Maustaste, Öffnen mit, Andere <br>
![](client_installation_03.png)  

Hier bei "Aktivieren" "Alle Programme" auswählen und das "Immer öffnen mit" anhaken.<br>
Nun das Terminal auswählen.<br>
![](client_installation_04.png)  

Die nun folgende Sicherheitsmeldung mit "Öffnen" bestätigen.<br>
![](client_installation_05.png)  

{{% alert title="Wichtig" color="primary" %}}
Wenn vom Betriebssystem Fragen kommen, ob denn nun der Zugriff auf den Drucker oder die Filestruktur erlaubt werden sollte, nicht blind den Default bestätigen, sondern genau lesen und in der Regel erlauben. Du willst ja deine Files nutzen.
{{% /alert %}}
