---
title: "Installation Kieselstein ERP Client unter Ubuntu"
linkTitle: "Ubuntu"
categories: ["Client Installation"]
tags: ["Ubuntu Client"]
weight: 20
description: >
  Installation Client zu Kieselstein ERP Server unter Ubuntu
---
Siehe bitte allgemeine Client Beschreibung

## Praktisch
Den Midnight Commander verwenden:
```bash
sudo apt install mc
```

## Java installieren
Um das passende Java11 FX zu bekommen sind folgende Schritte erforderlich:
```bash
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9
apt-add-repository 'deb http://repos.azulsystems.com/ubuntu stable main'
apt update
apt install zulu-11
```

Danach das Java Home manuell hinzufügen:
```bash
nano /etc/environment
JAVA_HOME="/usr/lib/jvm/zulu-11-amd64"

source /etc/environment
echo $JAVA_HOME
```
Anzeige:<br>
/usr/lib/jvm/zulu-11-amd64

### Alternativ herunterladen des Java FX Packages
- FX Download
- https://www.azul.com/core-post-download/?endpoint=zulu&uuid=e021b654-4fb0-4ad7-ab0a-f261a933fd71

mit dpkg installieren, einmal apt install -f, dann nochmal installieren!

## Erfahrungen auf einem neuen Ubuntu 24.04 LTS

### Java installieren
- Von [Azul.com](https://www.azul.com/downloads/?version=java-11-lts&os=ubuntu&architecture=x86-64-bit&package=jdk-fx#zulu) , JDK11 FX, Ubuntu, **DEB Paket** herunterladen.<br>
Es wird dies unter /home/username/Downloads abgelegt -> Für die eventuelle De-Installation muss das Paket an dieser oder an einer anderen auffindbaren Stelle, belassen werden.
- In Downloads wechseln und mit Doppelklick starten
- Sollte das nicht gehen.
  - ein Terminal starten
  - sudo -i -> damit wirst du root
  - dpkg -i Dateiname des Javapaketes z.B.
  - sudo apt-get install openjdk-11-jdk
  - gegebenenfalls muss vorher
    ```bash
    apt --fix-broken install
    ```
    ausgeführt werden
- mit java -version prüfen ob die Installation funktioniert hat
- mit nano /etc/environment<br>
JAVA_HOME setzen z.B.: /lib/jvm/zulu-fx-11-amd64
- mit echo $JAVA_HOME<br>
  Prüfen ob das funktioniert hat.

Gegebenenfalls den Rechner mal neu starten

## KES CLient herunterladen
Für eine allgemeine Anwendung kannst du den **Kieselstein ERP** CLient unter /opt/kieselstein installieren (und hier auch zentral alle weiteren Dinge sammeln).

Bei einem lauffähigen **Kieselstein ERP** System findest du unter
http://KIESELSTEIN_ERP_SERVER_ADRESSE:8080 alle nötigen Dateien (Für KIESELSTEIN_ERP_SERVER_ADRESSE in der Regel die IP-Adresse deines KLieselstein ERP Servers angeben )
- Hier liegt unter Client-Install idealerweise ein angepasstes Client.zip
- Dieses herunterladen und den Inhalt des zip z.B. nach /opt/kieselstein kopieren. Dabei auch den Ordner client mit kopieren
- Nun ein Terminal öffnen, sudo -i (damit bist du root)
- und nach /opt/kieselstein wechseln
- und mit chmod -R 777 client für das client-Verzeichnis und seine Programme volle Berechtigungen setzen
- nun mit cd client/bin/ ins Verzeichnis wechseln und ./kieselstein-client.sh starten

### Einrichten der Desktop Verknüpfung
Das geht am besten mit "desktop" Files. Ein Muster findest du auch auf deinem Server unter /opt/kieselstein/dist/clients/ entweder im tar/gz oder direkt dann im bin Verzeichnis.
```bash
[Desktop Entry]
Name=Kieselstein Client
Comment=Kieselstein ERP Client
Exec=/opt/kieselstein/client/bin/kieselstein-client
Icon=/opt/kieselstein/client/bin/kieselstein-icon-square-96x96px.png
Terminal=false
Type=Application
Categories=Office
```

Als Icon kannst du aus dem Gitlab gerne die [Kieselstein ERP Icons](https://gitlab.com/kieselstein-erp/sources/design/-/blob/main/icons/kieselstein-icon-square-96x96px.png) verwenden, bzw. das im Client.zip mit enthaltene Icon.

Für eine allgemeine Anwendung kannst du den Kieselstein ERP CLient unter /opt/kieselstein installieren (und hier auch zentral alle weiteren Dinge sammeln).

Also: 
- ein Terminal öffnen
- mit sudo -i root werden
- cd /opt
- mkdir kieselstein
- hier die Dateien aus dem Download inkl. Client einkopieren
- nun die desktop Datei auf deinen Schreibtisch kopieren. Z.B.<br>
```bash
cp /opt/kieselstein/client/bin/kieselstein.desktop /home/Benutzername/Schreibtisch/kieselstein.desktop
```
- nun erscheint das Icon auf dem Desktop. Dieses mit der rechten Maustaste anklicken und Starten erlauben.

