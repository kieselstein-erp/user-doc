---
title: "Installation Client"
linkTitle: "Client"
categories: ["Installation", "Client"]
tags: ["Installation", "Client"]
weight: 200
description: >
 Installationsbeschreibung für die Ausrollung des Kieselstein ERP Clients
---

Voraussetzung: Du hast auf den jeweiligen Rechnern ausreichende Installationsrechte und bist mit den Arbeiten mit Programm-Files und allem, was so dazu gehört, vertraut. Für Unterstützung wende dich als Mitglied der **Kieselstein-ERP eG** gerne an eine:n der Consulter:innen.

Ab der Version 0.0.0.8 deines **Kieselstein ERP** können die erforderlichen Installationsdateien über einen Weblink geladen werden. Gehe dazu in einem Browser auf http://IPADRESSE_KIESELSTEIN_ERP:8080.
![](link.png)

für Clients ab der Versionsverwaltung (>0.0.14) siehe 

Klicke nun auf download clients ![](client_link.png)

Hier findest du nun die vom Installationsverantwortlichen zusammengestellten Dateien zum Download zur Auswahl.<br>
![](client_download_files.png)

Es sind dies alle Dateien die in deinem Kieselstein-ERP im Verzeichnis<br>
?:\kieselstein\dist\clients abgelegt sind.<br>
Es ist Aufgabe des/der installierenden hier alle richtigen Dateien zusammenzustellen. Das bedeutet aktuell auch, dass nach jedem Update die Dateien anzupassen sind.<br>
Das Kieselstein-Desktop-Icon findest du [hier]( ./Kieselstein.ico).

In der Client.zip sind die beiden Verzeichnisse für die Installation enthalten. Lade diese herunter und kopiere diese z.B. auf c:\kieselstein\client\.<br>
![](client_download_inhalt01.png)<br>

Im bin Verzeichnis findest du drei Dateien:<br>
![](client_download_inhalt02.png)<br>
In Windowssystemen richte dir eine Desktop-Verknüpfung auf die kieselstein-ui-swing.bat ein. Dieser gibst du das Icon Kieselstein.ico.<br>
In Linux bzw. MAC-OS verwendest du die kieselstein-ui-swing.

Nun benötigst du noch das passende Java (wenn nicht schon installiert), welches ebenfalls im Browser als Download zur Verfügung steht (zulu ....).<br>
{{% alert title="Java mehrfach vorhanden?" color="warning" %}}
Wichtig: Sollte Java auf dem Client bereits für andere Programme benötigt werden, ist die weitere Vorgehensweise zu klären. Ersetzen oder parallel.
{{% /alert %}}

Üblicherweise kann ein / alle eventuell vorhandenes(n) Java entfernt werden. -> **EMPFOHLEN**

Installiere nun das Azul Java mit den default Einstellungen. Einzig bei der Frage nach dem Java Home wähle ![](Java_home_setzen.png) immer Verfügbar. [Details siehe](#java)

Nun kannst du deinen **Kieselstein ERP** Client starten.<br>
Benutzername und Passwort erhältst du von deinem/r Kieselstein ERP in Haus Verantwortlichen.

**Hinweis:**<br>
Sollte nach eine Neuinstallation von Java, das Java trotzdem nicht zur Verfügung stehen (prüfen z.B. aufmachen des CMD (Command-Shell) und Java -version), so hilft unter Umständen, gerade bei komplexen Berechtigungen, ein Neustart des Rechners.

**Hinweis:**<br>
Es hat sich bewährt, auf den Rechnern der Poweruser auch einen Teamviewer (https://www.teamviewer.com/de/download/windows/) installiert zu haben. Wir gehen davon aus, dass jeder ERP Anwender den Teamviewer starten kann und darf.


Von **Kieselstein ERP** verwendete Ports siehe bitte [Server]({{< relref "/docs/installation/01_server" >}})

{{% alert title="Hinweis" color="warning" %}}
Für Verbindungen von extern raten wir dringend gesicherte Verbindungen (VPN = Virtual Private Network) zu verwenden.
{{% /alert %}}

## Java
Wir setzen zurzeit folgende Versionen voraus:

Gerade im Clientbereich kann es vorkommen, dass andere Versionen eingesetzt werden. Bitte beachte, dass wir für das richtige Verhalten der Software ausschließlich die unten von uns zur Verfügung gestellte OpenJDK8 verwendet werden sollte. Bitte beachte bei der Verwendung von original Oracle Java 8/11 Versionen, dass diese lizenzkostenpflichtig sind.

Der Basis-Link für das erprobte Zulu Java findest du unter https://www.azul.com/downloads/?package=jdk#zulu
Hier dann Java 11 mit **jdk-FX** auswählen.

Den Download einer freien OpenJDK Version mit Java 11 für deinen Client finden man unter:
https://www.azul.com/downloads/?version=java-11-lts&os=windows&package=jdk-fx
![](Zulu_Java11_FX.png)

Achte bei der Installation des Javas darauf, dass:
- keine weiteren Java Versionen installiert sind. ACHTUNG: solltest du für andere Programme bestimmte Java Versionen benötigen, lässt sich auch das einrichten. Bitte wende dich an deinen **Kieselstein ERP** Betreuer
- bei der Installation muss das Java Home gesetzt werden, also:<br>
 ![](Java_home_setzen.png)  

Alternativ kann seit der Kieselstein Version 1.0.0 der Client auch mit einer [OpenJDK](https://openjdk.org/) Version 11 starten.
*Hinweis*: bei einem Mac mit Apple Silicon Prozess wird die Java-Zulu **FX** Version vorausgesetzt.

### Client startet nicht
Wenn unter Windows der Client nur kurz durchläuft und sich dann wieder automatisch beendet, so ist der Pfad in dem deine eigentlichen Clientdateien stecken zu lange. Mache diese kürzer.<br>
Wir raten dies unter ?:\Kieselstein\Client einzurichten

### Fehler beim Drucken von Barcodes 
Es werden viele Fehler in der Client-Console angezeigt.<br>
Die Barcode sind zerstückelt.<br>
Beim Drucken von Artikeletiketten erscheint das Vorschau-Fenster nicht. Fährt man mit der Maus über den Client erscheinen teilweise die "Bilder" des Druck-Vorschau-Fensters.<br>
![](ClientBild_zerstoert.png)<br>
Bitte prüfe, ob das aktuelle Java installiert ist.<br>
Es muss ![](Aktuelles_Java_11.png)<br>
angezeigt werden. Wird ein älteres Java angezeigt, kann es zu den beschriebenen Effekten kommen. Wichtig eben auch, nur Java mit der FX Erweiterung.<br>
Sollte die Anzeige trotzdem so "spinnen", dann ist dies ein Problem der Grafikkarte bzw. des Treibers.<br>
Das kann auch auftreten, wenn der Rechner ohne Bildschirm installiert ist und man nur per Fernwartung (Teamviewer, Remote Desktop, ...) zugreift.

## Fehlermeldung beim ersten Ausführen
Startet man unter Windows den Batch zum ersten Mal, kommt
![](WindowsInstallation_1.png)  
So auf **Weitere Informationen** klicken und dann<br>
Auf den Trotzdem ausführen Button klicken.
Dies bleibt bis zum nächsten Update / Erneuerung des Batchs erhalten.

## Client in Start / Taskleiste bringen
Hat man, wie oben beschrieben den Client.bat (eigentlich die ?:\kieselstein\dist\client\bin\kieselstein-ui-swing.bat ) als Verknüpfung auf den Desktop gelegt, so wünscht man sich oft, diese auch in den Start bzw. die Taskleiste zu bringen.

Der Trick dafür geht wie folgt:
1. In den Eigenschaften der **Verknüpfung** vorne das Ziel um C:\Windows\System32\cmd.exe /c ergänzen. ![](Verknuepfung_Anpassen_01.png)
2. Nun Rechtsklick auf die Verknüpfung. 
    - Im Windows 10 sieht man nun<br>
     ![](Verknuepfung_Anpassen_02.png)<br>
    An Start anheften bzw. an Taskleiste anheften, womit man das Start-Icon in den gewünschten Bereich bringen kann. Es sollte die Verknüpfung auf das Desktop-Icon vor dem Verschieben gemacht werden.
    - Im Windows 11 wählt man vorher noch ![](Verknuepfung_Anpassen_03.png) Weitere Optionen anzeigen und kann damit dann An Start anheften bzw. an Taskleiste anheften auswählen.

## Wird auch Windows 7 unterstützt?
Da Windows 7 schon seit etlichen Jahren nicht mehr supported wird und alleine schon aus diesem Titel dies eine grobe Verletzung der DSGVO darstellt, wird Windows 7 nicht unterstützt. Bitte verwende aktuelle Betriebssysteme, gegebenenfalls auch in einer VM (Virtual Machine) z.B. mit Virtual Box.

## Geschwindigkeit, langsamer Client
Neben der reinen Netzwerk-Zugriffs-Geschwindigkeit (siehe Hauptmenüleiste, Hilfe, Info) ist unter Umständen auch der Back-Call des Servers an den Client für die Geschwindigkeit, meist auch für die Langsamkeit verantwortlich.

D.h. es müssen sowohl der Server vom Client aus als auch der Client vom Server aus, mit ihren Netzwerk-Namen als auch mit den IP Adressen problemlos und schnell erreicht werden können. Ist dem nicht so, dauert das Blättern zwischen den Modulreitern entsprechend lange.

Gerade in Linux-Systemen oder bei VPN Verbindungen werden hier gerne Konfigurationsfehler gemacht.

In anderen Worten, es muss der DNS Name immer auf einen internen DNS Server zeigen, dann ist zumindest von dieser Seite her die Geschwindigkeit entsprechend optimiert.

Für Windows-User siehe Eigenschaften der Netzwerkverbindung<br>
![](Rueckwaertsaufloesung.png)

## Client-Updates
Nachfolgend eine kleine Beschreibung der Besonderheiten beim Update der Clients, von aus älteren Versionen.

### Alter Server
Manchmal will man die Installation vorab testen.<br>
Hier ist wichtig zu wissen, dass die neueren Clients, z.B. 0.1.1, bereits beim Starten, bevor noch irgendeine Oberfläche angezeigt wird, den Server um die Versionsnummer (der Datenbank und des Servers) abfragen.<br>
Das bedeutet, wenn du mit einem neuen Client gegen einen alten Server testest, wird dies nicht funktionieren. Der optische Eindruck ist, dass der Client nicht startet. Startest du den Client aus der Commandshell, ohne javaw.exe so siehst du
>Got Exception No such EJB method org.jboss.ejb.client.EJBMethodLocator@bc7b26d3 found on lpserver/ejb/SystemFacBean
java.lang.IllegalArgumentException: No such EJB method org.jboss.ejb.client.EJBMethodLocator@bc7b26d3 found on lpserver/ejb/SystemFacBean

Das bedeutet, der Client konnte zwar eine Verbindung zum Server herstellen, aber die Abfrage nach der Versionsnummer ist bereits fehlgeschlagen.<br>
Hier hilft nur den Server auch zu aktualisieren.

## Sprachsteuerung bei Anmeldung
Üblicherweise wird beim Start des Clients auch die Sprache vorgeschlagen, die für den Anwender die richtige ist. Diese Definition findest du unter<br>
?:\Kieselstein\client\bin\kieselstein-ui-swing.bat<br>
Hier steht (ca. in Zeile 37)<br>
´´´
set DEFAULT_JVM_OPTS="-XX:PermSize=64m" "-XX:MaxPermSize=256m" "-Djava.naming.factory.initial=org.wildfly.naming.client.WildFlyInitialContextFactory" "-Djava.naming.provider.url=remote+http://192.168.100.14:8080" "-Dloc=sl_  " "-Dsun.java2d.dpiaware=false"
´´´
mit dem Parameter -Dloc= kann gesteuert werden, welche Sprache vorgeschlagen wird. Bitte beachte: sollte die hier eingetragene Sprache in deinem **Kieselstein ERP** nicht aktiviert sein, so wird die nächstliegende Sprache verwendet.

Für die aktuell unterstützten Sprachen müssen folgende Einträge gemacht werden.
Welche Einträge in aktiviert sind, findest du in der lp_locale.
| locale | Sprache |
| --- | --- |
|deAT | Deutsch mit österreichischer Sprachausprägung |
|deCH | Deutsch mit schweizer Sprachausprägung |
|deDE | Deutsch mit deutscher Sprachausprägung |
|enGB | Britisches Englisch (nicht vollständig übersetzt) |
|enUS | Amerikanisches Englisch |
|itIT | Italienisch |
|plPL | Polnisch |
|sl | Slowenisch. Bitte beachte, dass die Definition des Locales mit sl_< >< > mit zwei nachfolgenden Spaces erfolgen muss.<br>Ist eigentlich Englisch, aber um die Zahlen wie in Europa üblich darzustellen als Slowenisch definiert|

Weitere Details [siehe]( {{<relref "/docs/installation/01_server/#client-vorbereiten" >}} )
