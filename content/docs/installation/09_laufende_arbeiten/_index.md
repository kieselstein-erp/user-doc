---
categories: ["laufende Pflege"]
title: "laufende Pflege"
linkTitle: "Pflege"
date: 2023-01-31
weight: 900
description: >
  Laufende Pflege deiner Kieselstein ERP Datenbank.
---
Wenn dein **Kieselstein ERP** schon einige Zeit im Einsatz ist, sollten regelmäßig (monatlich / halbjährlich) die Daten bereinigt werden.<br>
Diese macht üblicherweise der Datenbank Administrator. 

Die SQLs dafür sind:
- delete from LP_ENTITYLOG where t_aendern < '2020-01-01 00:00:00';  -- bitte mit Verstand einsetzen

- delete from LP_VERSANDANHANG where VERSANDAUFTRAG_I_ID in (select I_ID from LP_VERSANDAUFTRAG where STATUS_C_NR = 'Storniert');
- delete from LP_VERSANDAUFTRAG where STATUS_C_NR = 'Storniert';
 update LP_VERSANDAUFTRAG set O_MESSAGE = null, O_VERSANDINFO = null where STATUS_C_NR  = 'Erledigt' and O_INHALT is null;

- delete from LP_INSTALLER; -- sollte in deiner KIESELSTEIN leer sein.

- delete from LP_USERCOUNT where T_ZEITPUNKT < '01.01.2020 00:00:00';

