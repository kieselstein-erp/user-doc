---
title: "Installation KES-App"
linkTitle: "KES-APP"
categories: ["Installation KES-App"]
tags: ["Installation KES-App"]
weight: 500
description: >
  Installation der **Kieselstein ERP** mobilen App
---
Mit der **Kieselstein ERP** App steht ein praktisches mobiles Erfassungsgerät zur Verfügung.

Wichtig: Auf deinem **Kieselstein ERP** Server muss auch die RestAPI laufen, was ab Version 0.0.0.8 in der Regel gegeben ist und natürlich muss dein Server über den Port 8080 (https ist in Vorbereitung) erreichbar sein.

**Anmerkung:**<br>
Ab der Serverversion 1.x.x ist die REST-API im Wildfly integriert. Daher wird hier der Port 8080 verwendet. Es ist dafür die Version 0.0.30.x oder höher der mobilen App erforderlich.

## Betriebssysteme
Derzeit steht die KES-App für Android zur Verfügung.

WICHTIG: Wenn du die Identifizierung mit NFC Karten verwenden möchtest, ist eine doppelte Identifizierung (keine zwei Wege App) erforderlich. <br>
Dafür müssen für die Benutzer spezielle Rechte zweistufig eingerichtet werden.<br>
Wenn du dies nutzen musst, wende dich bitte derzeit an die Technik.

## Installation
Neben der Installation über GitLab stellen wir gerne auch das APK (Android Package Kit) direkt zur Verfügung. Üblicherweise installieren wir dies über den Totalcommander für Android. Da die direkte Installation aufgrund von Google Regeln nicht mehr möglich ist, hat Herr Ghisler unter  https://www.totalcommander.ch/android/appinstaller110.apk einen eigenen Installer ![](MultiAPK_Installer.png) zur Verfügung gestellt.<br>
Alternativ dazu kannst du auch vom GooglePlayStore Files by Google herunterladen.<br>
![](Files_by_Google.png) und damit die Installation durchführen.

### Welche mobile Barcodescanner werden unterstützt?
Aktuell werden folgende Barcodescanner unterstützt.<br>
Die Definition dazu findest du auch im [Quellcode](https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/blob/main/Terminal/MauiProgram.cs?ref_type=heads) ganz am Ende, DeviceInfo.Platform

| Hersteller | Gerät | ab App-Version | Verfügbarkeit |
| --- | --- | --- | --- |
| Datalogic | Memor 1 | 0.0.10 | wird vom Hersteller nicht mehr geliefert |
| Zebra Technologies | TC 21 bzw. TC26 | 0.0.20 | |
| Zebra Technologies | TC 22 bzw. TC27 | 0.0.32 | |

### Installation am Memor
1. WLan einrichten (Einstellungen, WLan)
2. Einstellungen, Geräteinfo, ganz unten, Build Nummer<br>
7 x Antippen, um den Entwicklermodus einzuschalten<br>
Dann in die Entwickleroptionen, USB-Debugging einschalten<br>
Zusätzlich, Einstellungen, verbundene Geräte, auf USB tippen und Datei übertragen anhaken
3. Micro USB Kabel, Verbindung PC, Memor herstellen
4. App-Installer herunterladen (Link siehe oben) und installieren
5. Dieser App Vertrauen und dann wieder zurücknehmen
6. Konfiguration des Memor Scanners
    - Einstellungen, ganz unten System, Scannersettings
      - Wedge aus
      - Prefix entfernen<br>
    Hinweis: Wenn die App sich nach dem ersten Scann automatisch beendet, so ist wahrscheinlich das Wedge-Interface des Scanners nicht deaktiviert.

Zusätzlich:<br>
Es muss die Barcode Reader App deaktiviert werden. D.h.:<br>
Android Settings - System - Scanner settings - Wedge - Enable keyboard wedge aus AUS stellen 

## Konfiguration
Beim ersten Start der App erscheint bereits der Anmeldebildschirm. Tippe hier auf ![](Konfiguration.png) das Zahnrad, um in die Konfiguration zu gelangen.<br>
![](Konfigurations_Menue.png)<br>
- Stelle hier die entsprechende Serveradresse deines **Kieselstein ERP** Servers ein.<br>
- Für das Lesen eines NFC Tags aktiviere den NFC-Leser
- definiere danach die Anmelde Sprache (Logon Sprache). Diese siehst du auch in deinem **Kieselstein ERP** Client in der Titelleiste ![](Client_Logon_Sprache.png)
- Gib danach den Mandanten ein, diesen siehst du ebenfalls in der Client Titelleiste ![](Client_Mandant.png)
- wenn gewünscht, kannst du noch ein Passwort definieren und
- die Sprache in der du deine **Kieselstein ERP** mobile App bedienen möchtest definieren. Es stehen derzeit die Sprache Englisch, drei Dialekte Deutsch, Italienisch und Polnisch zur Verfügung.

Tippe nun auf Speichern.
Daran anschließend bitte die App beenden (wie auf deinem Gerät üblich) und neu starten.

Dass du eine WLan-Verbindung zu deinem **Kieselstein ERP** Server benötigst, ist selbstverständlich. Sollte die Verbindung nicht funktionieren, so prüfe von deinem Gerät aus, ob du eine Verbindung zur [RestAPI]( {{<relref "/docs/stammdaten/system/web_links/rest_api/">}} ) herstellen kannst.

## Einloggen / Anmelden
Hier stehen zwei Möglichkeiten zur Verfügung
- mit Benutzernamen<br>
![](Anmeldung_Username.png)<br>
achte auf GROSS-klein Schreibung und Passwort und tippe anschließend auf Ok.<br>
Verwende hier den dir zugewiesenen Benutzernamen und das entsprechende Passwort.<br>
**Wichtig:** Um einen Zugriff zu bekommen, muss im Benutzermandant auch die RestAPI Systemrolle vergeben sein.<br>
- Alternativ kannst du durch Scannen ![](Barcodescannen.png) des Login-Barcodes die Anmeldung deutlich vereinfachen. Beachte, dass dieser Barcode deine Identifikation darstellt und somit nicht in falsche Hände kommen sollte. Den Barcode bekommst du im **Kieselstein ERP** Client, Benutzer, dann auf den gewünschten Benutzer gehen und auf das Drucker-Symbol ![](Benutzeranmeldung_per_Barcode_01.png) klicken.<br>
Nun dein Passwort angeben, dieses wird nicht gespeichert und verschlüsselt im Barcode angegeben. Nun kannst du durch scannen dieses Barcodes dich ebenfalls am System anmelden.

Damit erscheint nun das Hauptmenü der mobilen Erfassung.<br>
![](Hauptmenue.png)

### Connection Failure
Kommt bei der Anmeldung unmittelbar Connection Failure, so stimmt die IP Adresse nicht.
Eine weitere Fehlermöglichkeit ist, dass die Sprache nicht zu deiner Kieselstein ERP Installation passt. Stelle auch hier verfügbare Sprachen ein.
![](Connection_Failure.png)  

Ein weiterer möglicher Fehler ist, wenn bei der sich anmeldenden Person keine Ausweisnummer im Personal / Detail hinterlegt ist. Dann kommt <br>
![](Keine_Anmeldung_erlaubt.png)  

**Info:** Die Ausweisnummer wird aus Gründen der Struktur der RestAPI in jedem Falle benötigt, auch wenn der Benutzer keine Zeiterfassung macht.

Es stehen folgende Funktionen zur Verfügung:
## Abmelden
Mit Abmelden kommst du wieder zurück zur Anmeldung. Dies solltest du machen, wenn das Gerät nicht dein persönliches Gerät ist und es von den verschiedensten KollegInnen für unterschiedliche Aufgaben verwendet wird.

## Inventur
Mit Tipp auf Inventur kommst du in das Auswahlmenü der Inventuren. Sollte die Fehlermeldung "Sie verfügen nicht über die erforderlichen Benutzerrechte" kommen, so diese bitte entsprechend nachtragen. [Siehe](#rechte-für-die-inventur)
### Inventur auswählen
Hier siehst du eine Liste der angelegten Inventuren, welche noch nicht abgeschlossen sind und auf deren Lager du entsprechende Zugriffsrechte hast.<br>
![](Liste_der_angelegten_Inventuren.png)<br>
Wähle nun die gewünschte Inventur aus, womit du in der Inventurerfassung bist.<br>
![](Inventur_Erfassung.png)<br>
Hier stehen folgende Möglichkeiten zur Verfügung.
### Lagerort Scannen oder eingeben
Scanne durch Tippen auf Scannen ![](Barcodescannen.png) oder Betätigen des Scanknopfes deines Memor den gewünschten Lagerplatz. Alternativ kannst du diesen auch eingeben.<br>
Hinweis: Um einen Lagerplatz verwenden zu können, muss dieser in der Liste der Lagerplätze eingetragen sein. Drucken der Lagerplätze [siehe]( {{<relref "/docs/stammdaten/artikel/#k%C3%B6nnen-lagerplatz-etiketten-gedruckt-werden">}} ) 

Beachte: Ist ein Lagerplatz definiert, wird für jede Inventurbuchung dem gewählten Artikel dieser Lagerplatz zugeordnet. Dies so lange bis ein anderer Lagerplatz definiert wird bzw. der Lagerplatz in der Erfassung wieder gelöscht (nicht definiert) wird.

### Artikel scannen
Scanne nun den gewünschten Artikel. Je nach Inhalt des Barcodes werden Menge und Chargen- bzw. Seriennummer entsprechend aus der Etikette übernommen.

Es werden derzeit folgende Barcodekombinationen unterstützt:
| Barcodeinhalt | Bedeutung / Wirkung |
| --- | --- |
| Identnummer direkt als String | Artikel wird gewählt und angezeigt. Menge und ev. Chargennummer muss angegeben werden |
| $Iidentnummer\|Chargen bzw. Snr | Identnummer mit nachfolgender Chargen bzw. Seriennummer. Die Menge muss eingegeben werden|
| VDA4992 | Identnummer, Menge, Chargen bzw. Seriennummer werden übernommen. Wenn die Menge geändert werden sollte, so tippe nach dem Scan auf das Plus rechts unten und gib die Menge ein und bestätigen mit Ok. |
|Ident\<Tab>Menge\<Tab>Charge@ | wie VDA4992 |

Wichtig: Üblicherweise muss bei einer Inventur die Menge von der zählenden Person eingegeben werden. Wenn nun in deinen Barcodes, z.B. VDA4992 o.ä. die Menge bereits enthalten ist, so muss durch den Lagerbewirtschaftungsprozess sichergestellt sein, dass die Menge in der Verpackung(seinheit) mit der am Etikett / im Barcode angegebenen Menge übereinstimmt. Üblicherweise erkennt man das auch daran, dass bei Artikeln die mehrfach eingelagert wurden, zahlreiche Verpackungsetiketten, mit unterschiedlichen Mengen, übereinander kleben.

Daher übernehmen wir bei der Inventurerfassung die im Barcode enthaltenen Menge automatisch. Korrekturen können wie oben beschrieben vorgenommen werden.

Um die Inventurbuchung durchzuführen, tippst du auf Buchen. Wenn du die erfasste Menge bzw. Serien- oder Chargennummer geändert hast, kann es praktisch sein auch die Artikeletikette mit auszudrucken. Stelle dafür den Schalter auf ein (drucken). Beachte: Dies bleibt so lange aktiv, bis du ihn deaktivierst bzw. die App neu startest.

Nach dem Buchen bist du wieder in der Erfassungsmaske und kannst mit dem nächsten Artikel deine Inventur fortsetzen.

Gibt es für den Artikel und gegebenenfalls die Serien / Chargennummer bereits einen Eintrag in der Inventurliste, so erscheint die Frage wie damit verfahren werden sollte.
- Hinzufügen: D.h. die Menge wird zur Buchung addiert
- Korrektur: D.h. die bestehende Inventurbuchung wird gelöscht und durch deine neue Buchung ersetzt
- Abbrechen: Kehrt zur Erfassungsmaske zurück

### Artikeletikett mitdrucken
Um ein Artikeletikett ausdrucken zu können, muss der Arbeitsplatzdrucker entsprechend eingerichtet sein.


### Rechte für die Inventur
- Rollenrechte<br>
![](Inventur_Rechte_01.png)
- erlaubte Lager für die Rolle<br>
![](Inventur_rechte_02.png)

## Fertigung
Los-Nummer eingeben, gerne auch nur einen Teil davon oder natürlich idealerweise scannen
Nach der Eingabe erscheint eine Liste der gefundenen Losnummern. Das gewünschte Los durch Antippen auswählen.

Losgröße ändern durch Tippen auf den Stift

Los abliefern mit Seriennummer / Chargennummer (wenn erforderlich) durch jeweiliges Antippen und Eingabe der Menge und gegebenenfalls auch der Chargennummer (je nach Artikel) oder auch der Seriennummer.

### Los auswählen
![](Los_auswaehlen.png)  <br>
Eingabe der Losnummer bzw. eines Teiles davon.<br>
Nach der Eingabe erscheint eine Liste der gefunden Lose. Wähle das für dich richtige Los durch Antippen aus. Damit kommst du in die Bearbeitung des Loses mit verschiedenen Funktionen.

Durch Scannen der LosNummer ($L) gelangst du direkt in die Bearbeitung des Loses.

![](Los_bearbeiten.png)  

### Los Abliefern
![](Los_abliefern_mit_chargen.png)  

![](Los_abliefer_rueckmeldung.png)  


## Zeiterfassung
- Kommt
- Geht
- Pause
- Saldo
- Anträge -> Urlaubsantrag, Zeitausgleichsantrag, Krankenstandsantrag in Halbtag oder Ganztag(en)
![](Zeiterfassung_Menue.png)  

### Urlaubsantrag erfassen
![](Zeiterfassung_Urlaubsantrag.png)  

## Einstellungen für den TC 21 C
- rechts oben am Rahmen ist der Eintaster
![](tc_21_C_Eintaster.png)  
- wischen nach oben bringt alle Apps -> da findest du auch die Einstellungen
- unter Display und dann erweitert
  - Display automatisch drehen, abschalten
  - Display automatisch abschalten -> auf 2 Minuten / wie gewünscht einstellen
  - Tipp auf den rechten Button, das gefüllte Rechteck, bringt alle aktiven Apps. Hier nach rechts wischen bringt die Möglichkeit alle Apps zu schließen -> für den Restart der Kieselstein ERP App

  Es gibt für den TC21C auf einen Pistolengriff, der sehr praktisch ist.

- Einstellungen
  nach oben wischen, Einstellungen
  - WLAN
    Netzwerk & Internet
    WLAN


### Verbindungs / Ladekabel
USB-C erforderlich


### Rechte
1. Für die Ablieferbuchungen<br>
![](Rechte_1.png)  

2. Welche Rechte muss der Benutzer für die Zeiterfassungsbuchungen haben?

### Anzeige der Log Datei
Um dem Anwender, ohne in das Konfigurationsmenü einsteigen zu müssen, die Möglichkeit zu geben, das Log-File anzuzeigen, wird, wenn das mobile Erfassungsgerät geschwungen wird (die Shake Bewegung), die Anzeige des localen Log-File aufgerufen.<br>
Dies wurde bewusst so programmiert.

### App stürzt immer wieder ab
Sollte es vorkommen, dass die (neu) installierte App, gleich bei der Anmeldung sich automatisch beendet, könnte es sein, dass im APK-File ein Fehler aus dem Download enthalten ist.<br>
D.h. die App neu installieren und es noch einmal versuchen.