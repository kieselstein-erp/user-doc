---
title: "Barcodesimulator"
linkTitle: "Barcodesimulator"
weight: 300
description: >
  Einrichten eines Barcodesimulators für das **Kieselstein ERP** Zeiterfassungsterminals
---
Barcodesimulator
===

Zum Testen des **Kieselstein ERP** Zeiterfassungsterminals ist es oft praktisch die zu erzeugenden Barcodes nicht immer ausdrucken zu müssen, sondern diese entsprechend mit einem Programm zu generieren und an das Terminal zu senden.

Dafür benötigst du die Entwicklungsumgebung für die MAUI App.

Hier kann nun im Projektmappenexplorer (rechter Rand)
![](Projektmappenexplorer.png)<br>
der BarcodeSimulator ausgewählt werden.

Rechtsklick auf den BarcodeSimulator und Debuggen auswählen.

Es wird damit auch die Exe erzeugt, die du auch eigenständig starten kannst. Du findest diese unter c:\Users\BENUTZERNAME\source\repos\kieselstein-terminal-maui\BarcodeSimulator\bin\Debug\net7.0-windows\BarcodeSimulator.exe

![](Barcodesimulator.png)<br>

Kurze Beschreibung:
- Im Feld rechts oben kannst du den zu sendenden Barcodeinhalt angeben
- mit Add wird er in die Liste der zur Verfügung stehenden fertigen Barcodes mit aufgenommen
- Doppelklick auf den Barcode aus der Liste übernimmt diesen in das Textfeld um somit weitere Kommandos zu erzeugen
- Unten muss der Comport über den gesendet wird ausgewählt werden
- Klicken auf Send sendet das aus der Liste gewählte Kommando

## Nullmodem Software
Um nun am gleichen Gerät eine Verbindung zwischen dem Barcodesimulator und deinem Terminal herzustellen, benötigst du eine Software, die die Funktionalität eines Null-Modemkabels bietet.<br>
Dafür kannst du diese Software aus dem angegebenen freevirtualports.com verwenden und<br>
![](Lokale_Bridge.png)<br>
eine lokale Bridge einrichten.

Damit stellst du ein virtuelles serielles Verbindungskabel zwischen dem Barcode Simulator und deinem Terminal her. D.h. der eine Com_Port ist der Simulator und der andere ist das Terminal.

Somit ist in diesem Beispiel
| Simulator | Verbindung | Terminal |
| :-: | :-: | :-: |
| Com2 | <--> | Com1 |

Alternativ kann anstelle des freevirtualports z.B. auch den Nullmodem-Emulator von
www.sourceforge.net/projects/com0com/ verwendet werden.<br>
Ist dieser installiert, so siehst du dies im Gerätemanager unter Serialport Emulator.