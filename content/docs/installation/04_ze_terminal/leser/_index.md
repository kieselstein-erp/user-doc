---
title: "Leseeinheiten zum KES-ZE-Terminal"
linkTitle: "Leseeinheiten"
weight: 400
description: >
  Leseeinheiten zum **Kieselstein ERP** Zeiterfassungsterminals
---
Für das **Kieselstein ERP** Zeiterfassungsterminal stehen verschiedene Erfassungsgeräte zur Verfügung.

Derzeit werden folgende Geräte unterstützt:
# Barcodescanner
Alle, die eine COM-Schnittstelle simulieren und den gescannten Code 1:1 an die COM weitergeben und diesen mit CR abschließen.<br>
Ob der gescannte Code nun ein eindimensionaler Barcode oder ein QR Code ist, ist für das Terminal unwesentlich.

# RFID Leser
Es werden verschiedene RFID Leser unterstützt, mit denen in weitere Folge die unterschiedlichsten RFID-Tags gelesen werden können

## TWN3
## TWN4
Der TWN4 ist zwar nicht ganz billig, aber der Leser mit dem derzeit noch alle Arten von RFID Chips gelesen werden konnten. Von Mifare über Legic bis ... . Einzig Siemens VOS ist damit nicht lesbar.

Im Terminal als TWN3 einstellen.

Die Programmierung und damit die Auswahl welche Tags gelesen werden können, muss in einer eigenen App durchgeführt werden. Die grundsätzliche Vorgehensweise ist wie folgt:

Vor dem Beginn der Arbeiten, den Reader an USB anschließen
1. Starten Appblaster
2. Button "configurable project"
3. Multi CDC auswählen => CDC steht für ComPort
4. Links Action Item auf Transponder Types und dann Categorie Legic
5. Create Image -> damit schreibt er die Config auf die Platte und dann
6. Programm Image damit wird das Device programmiert

Im Gerätemanager muss er Serial RFID Device anzeigen.

Man kann, um herauszufinden, welche Art von Tag das ist, alle Tags in die Active Transponder Types übernehmen und sukzessive herauslöschen.<br>
Am einfachsten dazu den Leser auf Wedge stellen = Application Templates Multi Keyboard

Das Konfigurationsprogramm findest du unter TWN4DevPack406.zip

## Mifare OEM
## PROMAG_PCR300
