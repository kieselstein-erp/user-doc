---
title: "Windows Einstellungen zum KES-ZE-Terminal"
linkTitle: "Einstellungen"
weight: 200
description: >
  mögliche Einstellungen unter Windows für dein **Kieselstein ERP** Zeiterfassungsterminal
---
Für manche Anwender haben sich folgende Einstellungen bewährt / als praktisch herausgestellt.

Bitte beachte, dass diese Einstellungen von der jeweiligen Windowsversion abhängig sind und sich jederzeit ändern können / eine andere Auswirkung haben (oder auch keine mehr)

## Einstellungen Windows 11
### WLAN abschalten
### System - Benachrichtigungen:
Bitte nicht stören automatisch aktivieren -> Bei Verwendung einer App im Vollbild... <br>
**Ausschalten**

### System - Sound:
- Die Datei message-notification-103496.wav nach Windows\Media kopieren, Lautstärke -> 100
- Erweitert - weitere Soundeinstellungen -> alter Dialog öffnet sich
- Reiter Sounds:
	- Benachrichtigung: Datei auswählen: message-notification-103496
	- Button Speichern unter -> als kieselstein speichern
	- Übernehmen und OK
- Fenster schließen
	- Anmerkung: Testen klappt nicht immer, wenn das Fenster geöffnet
	- Anmerkung: Manchmal braucht es einen Neustart damit auch bei der Benachrichtigung aus der App der Sound kommt
### Zeit und Sprache - Eingabe:
- Bildschirmtastatur
	- Schaltfläche Mikrofon -> ausschalten
	- Bildschirmtastatur anzeigen -> Immer
### Personalisierung
- Farben:	
	- Modus auswählen -> Dunkel
- Texteingabe:	
	- Design -> Dunkel
	- Bildschirmtastatur -> Tastaturgröße 150, Textgröße mittel
- Taskleiste:
	- Taskleistenelemente:
		- Suchen -> Ausblenden
		- AktiveAnwendungen -> Aus
		- Widgets -> Aus
	- Symbole in der Taskleiste
		- Bildschirmtastatur -> Immer
- Andere Taskleistensymbole:
	- Alle auf Aus
- Verhalten der Taskleiste:
	- Taskleistenausrichtung -> Links
### Windows Update
- Erweiterte Optionen
	- Nutzungszeit 4:00 - 22:00
### Bildschirmtastatur:
- Tastaturlayout -> klein<br>
Neben dem Passwortdialog positionieren, ca. 1 Tastenbreite Abstand

### EdgeSwipe sperren:<br>
Regedit aufrufen und Wert ändern bei:<br>
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PolicyManager\default\LockDown\AllowEdgeSwipe<br>
change "Value" from 1 to 0

### Einstellungen Screensaver:
- Wartezeit 2 Min
- Einstellungen:
	- Application Server:	IP-Adresse deines Kieselstein-ERP Servers oder alternativ des Proxy Servers
	- Port:	8080
	- Updateintervall:	30
	- Zoomfaktor:	1,3
