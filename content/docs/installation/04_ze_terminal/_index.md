---
title: "Installation KES-ZE-Terminal"
linkTitle: "ZE-Terminal"
categories: ["Installation KES-ZE-Terminal"]
tags: ["Installation KES-ZE-Terminal"]
weight: 400
description: >
  Installation des **Kieselstein ERP** Zeiterfassungsterminals
---
Mit dem **Kieselstein ERP** Zeiterfassungsterminal steht ein praktisches Erfassungsgerät zur Verfügung um mit verschiedenen Lesern die Anwesenheitszeiten und auch die Zeiten auf Losen, Aufträgen, Projekten für Menschen und Maschinen in deren vielfältiger Form zu erfassen.

Wichtig: Auf deinem **Kieselstein ERP** Server muss auch die RestAPI laufen, was ab Version 0.0.0.8 in der Regel gegeben ist und natürlich muss dein Server über den Port 8080 (https ist in Vorbereitung) erreichbar sein.

## Betriebssysteme
Derzeit steht die KES-ZE-App für Windows und Android zur Verfügung. Eine Erweiterung auf Linux Betriebssysteme ist angedacht.

Bitte beachte, dass du für die Installation auf Windowsrechnern administrative Rechte benötigst. Es werden aktuell nur Home und Pro (jeweils Win10/11) unterstützt. Ev. andere Versionen, vor allem die die den Entwicklungsmodus nicht unterstützen, können nicht verwendet werden.

{{% alert title="Wichtig" color="warning" %}}
Dass das Windows-Betriebssystem deines Terminal-PC's auf aktuellstem Stand ist, setzen wir als selbstverständlich voraus.
{{% /alert %}}

## Programmversion
Programmtechnisch sind das Zeiterfassungsterminal und die mobile (Inventur)-App identisch. Es wird automatisch anhand des Gerätes, auf welchem das Programm ausgeführt wird, entschieden welche Anwendung nun ausgeführt wird.
| Gerät | Anwendung | Bemerkung
| --- | --- | --- |
| Windows PC | Zeiterfassungsterminal | mit seriellen Lesern |
| Android Tablett | Zeiterfassungsterminal | NFC Leser erforderlich |
| Android Phone | mobile App | Barcodescan über die Kamera, NFC Leser wenn verfügbar, Bluetooth Barcodescanner |
| Memor1 | mobile App | Barcodescan über die Scan-Engine, kein NFC Leser<br>[Mobile App siehe]( {{<relref "/docs/installation/05_KES_App/" >}} ) |
| Zebra TC21 ff | mobile App | Barcodescan über die Scan-Engine, kein NFC Leser |
| Zebra TC22 ff | mobile App | Barcodescan über sehr gute Scan-Engine, kein NFC Leser |

## Installation
Herunterladen der neuesten Version von https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/releases<br>
![](herunterladen.png)  <br>
Hier je nach Betriebssystem das passende Package auswählen.<br>
<u>Anmerkung:</u>
Die weitere Beschreibung geht von Windows aus.

WICHTIG: eine eventuell bereits vorhandene Version vorher entfernen -> siehe unten.

Klick auf Download, die heruntergeladene Datei und 

![](Installation1.png)  
- die neueste Version kopieren und z.B. nach ?\install kopieren.
- den Zielordner notieren.
- Starttaste (Windowstaste) und Eingabe von Power
![](Installation2.png)  
Windows Powershell aufrufen und als Administrator ausführen.

In das notierte Zielverzeichnis wechseln.

![](Installation3.png)  

Install.ps1 starten.

Sollte nun nachstehende Fehlermeldung kommen<br>
![](Installation4.png)  

so sind die Ausführungsrechte für Powershell Scripts nicht gesetzt.

Kommt die Fehlermeldung ist nicht signiert [so siehe](#zertifikats-fehler).

Um diese zu setzen, im PowerShell<br>
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned<br>
eingeben und die Frage nach 
![](Installation5.png)  
mit A (Ja für alle) und anschließendem Enter beantworten.

(Tipp einfach den Text kopieren und mit Strg+V in Powershell einfügen.)

Wichtig: Insbesondere das ändern der Policy erfordert Administrator Rechte. Kommt nach dem Ausführen obigen Befehls wieder die gleiche Abfrage, hast du zuwenig Rechte.

**WICHTIG:**<br>
Nun Powershell beenden und in normalen Modus starten. Dies ist insbesondere für das [Update](#eine-bestehende-installation-aktualisieren) zukünftiger Versionen wichtig. [Siehe aber auch Berechtigungsfehler](#berechtigungs-fehler)

Danach die Installation erneut mit .\Install.ps1 starten.<br>
![](Installation6.png)  
und die Frage mit A (Immer ausführen) und Enter beantworten.

Bei der ersten Installation erscheint nun die Aufforderung den Entwicklermodus zu aktivieren.<br>
![](Installation7.png)  

D.h. schalte den Entwicklermodus auf Ein / On und beantworte die nachfolgende Frage

![](Installation8.png)  

mit Ja. Danach bitte das Einstellungenfenster schließen (Klick auf das X rechts oben)

![](Installation9.png)  
Auch diese Frage mit J (Ja) beantworten.

Am oberen Rand des Powershell Fensters erscheint nun die Fortschrittsanzeige.
![](Installation10.png)  

Zum Ende der Installation erscheint<br>
![](Installation11.png)  

Klicke nun auf den Start-Button und füge die neu installierte App zum Start hinzu.
![](Installation12.png)  
Damit kannst du jederzeit, z.B. zum Testen den Terminal manuell starten.

Zusätzlich kannst du damit das Terminalprogramm auf den Desktop ziehen (aus dem Start auf den Desktop).

### .Net Framework
Ab der Terminalversion 0.0.20 ist ein aktuelles DotNet Framework erforderlich. Dies kann [hier heruntergeladen](https://dotnet.microsoft.com/en-us/download/dotnet/8.0) werden.

Die Aufforderung zur Installation erscheint beim ersten Start des Terminalprogramms.<br>
![](Installation_DotNet_Runtime.png)

Je nach Einstellung des Browsers auf deinem Terminal wird <br>
![](Download_DotNet_Runtime.png)<br>
das zu installierende Framework sofort herunter geladen.<br>
Bitte installieren. Du benötigst dafür entsprechend administrative Rechte.

**Wichtig:**<br>
Die Terminalsoftware muss unter dem dann verwendeten Benutzer eingerichtet werden. Je nach Firmenpolitik hilft manchmal folgender Trick.<br>
Gib vorab dem Terminalbenutzer lokale Administrations Rechte (Domain Admin) um die Terminalsoftware installieren und einrichten zu können. Nach der Installation und fertigem Einrichten, kann dieses Recht wieder entfernt werden.

Beachte immer, dass vor einem Update, das Betriebssystem des Terminals aktuell ist.

**Trick:**<br>
In der Praxis hat sich, gerade in komplexeren Netzwerken, bewährt vor der Installation das [Zertifikat](#zertifikats-fehler) des Terminals zu installieren, dafür brauchst du Administratorrechte, und erst danach die eigentliche Terminalsoftware unter dem ausführenden Benutzer.

## Einrichten des Terminals
Starten des Terminals über Start

Beim ersten Start kommt in der Regel die Fehlermeldung, dass sich das Terminal nicht verbinden kann.
![](Installation14.png)  
Bestätige diese Meldung und wähle anschließen das Setup des Terminals durch Klick auf  
![](Installation15.png) 


### Eintragen des ComPorts
Feststellen welcher Comport für den Barcodescanner / den RFID Leser zur Verfügung steht, über den Gerätemanager (Rechtsklick auf den Startbutton) ![](Installation16.png)  

Hier findest du unter
![](Installation17.png)  den Comport für das / die angeschlossenen Eingabegeräte. In unserem Falle nun Com4 für den Barcodescanner.<br>
**Hinweis:** Die Nummer des Comports hängt auch vom gewählten USB Anschluss ab. D.h. wenn der Standort des Terminals verändert wird, sollten bei der Aufstellung am neuen Standort die Geräte exakt an den gleichen USB Port angeschlossen werden, anderenfalls muss die Konfiguration wieder neu gemacht werden.<br>
**Wichtig:** Damit der Comport überhaupt zur Auswahl steht, muss vor dem Start des Terminalprogrammes der Barcodescanner / der RFID Leser bereits angeschlossen sein und im Gerätemanager aufscheinen. Sollte der Scanner beim Scannen eines Barcodes ein grgr Geräusch von sich geben, bedeutet dies, dass der Scanner erkennt, dass er logisch betrachtet, nicht angeschlossen ist.

Trage nun die abgefragten Parameter ein:
![](Installation18.png)  

Danach Klick auf "Speichern" und anschließend Anwendung beenden, um danach das Terminal neu zu starten.

Kommt nun die Meldung
![](Installation19.png)  
so sind folgende Fehlermöglichkeiten gegeben, stimmen folgende Werte nicht zu deiner **Kieselstein ERP** Installation.
- es muss den Benutzer Terminal, terminal geben mit entsprechenden Rechten (siehe unten)
- es muss das Logon Locale mit dem Locale des Mandanten übereinstimmen
- es muss der Logon Mandant mit dem gewünschten Mandanten übereinstimmen
Bitte beachte, dass die Sprache des Terminals nur für die Anzeige der verschiedenen Texte (Kommt, Geht usw.) verwendet wird.

Wenn Korrekturen am Setup gemacht wurden, so diese speichern und das Terminal neu starten.

{{% alert title="BEACHTE" color="info" %}}
Wenn deine Sprachdefinition nur zwei Zeichen hat, müssen danach zwei Leerstellen eingetragen werden. Es werden sonst die Sprachen nicht erkannt, was sich bis dahin auswirken kann, dass keine Buchungen, wegen fehlender Sprachen, durchgeführt werden.<br>
Ein guter Test ist auch die Anzeige der Kommt / Geht -Buchungen zu nutzen. Diese müssen in der gewünschten Sprache, wenn sie am Client bereits übersetzt sind, erscheinen.
{{% /alert %}}


Nach einem erfolgreichen Neustart erscheint in der oberen Statuszeile
![](Installation20.png)

#### Abschalten der Leser
Es kommt immer wieder die Frage, wie man die Leser, die man z.B. zu Testzwecken aktiviert hat, deaktivieren kann. Dazu ganz einfach auf den jeweiligen Port klicken und einen leeren Port zuordnen. Sollte sich aus irgend einem Grund, dieser nicht zuordnen lassen, so bitte die Terminal App [deinstallieren](#eine-bestehende-installation-entfernen)

## Zeiten buchen
Scan der Ausweisnummer. Bei einer gültigen Ausweisnummer (siehe Personal, Journal, Personalliste, Barcodeliste) wird der Name angezeigt und die derzeit möglichen Funktionen dargestellt.
![](Installation21.png)

Wähle nun deine Buchung. Nach erfolgreicher Buchung erscheint kurz die Meldung
![](Installation22.png)  

In deinem Zeiterfassungsclient findest du nun diese Buchung mit der aktuellen Uhrzeit in der Quelle den Computernamen des erfassenden Rechners.



Wenn "Offline" angezeigt wird, also das Terminal keine Verbindung zu deinem **Kieselstein ERP** hat, wird der Status alle 30" geprüft. D.h. wenn man alles eingestellt
hat, sollte es nach 30" gehen.

## Wie sieht man auf welchem Terminal gebucht wurde?
Im Modul Zeiterfassung, bei der jeweiligen Person, sieht man unter Quelle den PC-Name / Hostnamen des Rechners von dem aus gebucht wurde. So kann mit einer sprechenden Namensvergabe sehr leicht die Zuordnung, wo denn der Mitarbeiter / die Mitarbeiterin gebucht hat, treffen.<br>
![](Terminal_Quelle.png)  


## Rechte
Für das Terminal sollten (nur) die unten angeführten Rechte vergeben werden. In Kombination mit der VDA-SCANN-App ergeben sich daraus folgende Einstellungen:<br>
![](Terminal_Rechte1.png)  <br>
**ACHTUNG:** Hier fehlt das Recht: PERS_ZEITERFASSUNG_MONATSABRECHNUNG_DRUCKEN ohne diesem spinnt die Saldo Darstellung im Terminal, was man aktuell nur im LogFile sieht.

Bitte beachte, dass für das Terminal auch die Zuordnung unter Benutzermandant gegeben sein muss. Auch hier bitte nur die unbedingt notwendigen Rechte vergeben. Also:<br>
![](Terminal_Rechte2.png)  

Denke daran auch die Zuordnung für die RestAPI Systemrolle einzutragen.

**Hinweis1:** Werden die Rechte für das Terminal angepasst, ist kein Neustart des Terminal-Programms notwendig.<br>
**Hinweis2:** Wurde die Darstellung der Personen aktiviert, also Anmeldeseite und beim Start des Programms werden keine Personen angezeigt, so fehlen die Rechte für die Personen.

### Diese Rechte sind nur für das Zeiterfassungsterminal erforderlich
Also dann, wenn das Terminal nur für die Anwesenheitszeiterfassung verwendet wird.<br>
![](Terminal_Rechte3.png)

### Diese Rechte sind zusätzlich für die Fertigung erforderlich
Wird das Terminal auch für die Buchung der Fertigungsbegleitscheine verwendet, so sind zusätzlich nachfolgende Rechte erforderlich.<br>
![](Terminal_Rechte4.png)  

Denke daran, dass bei Material Buchungen bzw. Ablieferbuchungen auch Rechte auf das Lager vergeben werden müssen. Also für die Rolle Terminal auch im Reiter Lager zumindest<br>
![](Terminal_Rechte5.png)  

### Recht, um auf Aufträge zu buchen
Sollte mit dem Terminal auf Aufträge gebucht werden, so muss das Terminal auch das Recht haben, um auf den Auftrag Zeiten zu buchen. Also<br>
![](Terminal_Rechte6.png)


## Einrichten Autostart des Terminals
Damit dein Zeiterfassungsterminal immer startet, den Autostart einrichten.<br>
Hier muss zwischen Windows 10 und Windows 11 unterschieden werden.
### Windows 10
- Die Verknüpfung zum Terminalprogramm muss am Desktop eingerichtet sein.
- Windows-Taste + R und shell:startup<br>
![](Autostart1.png)  
- Windows Explorer mit Autostart öffnet sich
- Die Verknüpfung vom Desktop in den Autostartordner ziehen / bzw. mit Strg kopieren.
### Windows 11
Idealerweise bringst du zuerst das Terminal Starticon auf den Desktop.
Da diese Applikation als versteckte App betrachtet wird, den Windows-Explorer starten (Windowstaste + E) dann in die Adressleiste
> %windir%\explorer.exe shell:::{4234d49b-0245-4df3-b780-3893943456e1} einkopieren [(danke an Netzwelt)](https://www.netzwelt.de/anleitung/204447-windows-11-so-erstellt-desktop-verknuepfung-microsoft-store-apps.html). 
Hier findest du eine Liste ALLER Applikationen, die auf dem Rechner installiert sind.<br>
Hier nach dem Kieselstein-ERP Terminal ![](App_einfuegen.png) suchen und dieses auf den Desktop ziehen. Weiter mit:
- Windows-Taste + R und shell:startup<br>
![](Autostart1.png)  
- Windows Explorer mit Autostart öffnet sich
- Die Verknüpfung vom Desktop in den Autostartordner ziehen / bzw. mit Strg kopieren.

## Daran denken
Bitte denke daran, dass für den Terminalrechner in der Regel jeglicher Standby Betrieb deaktiviert werden sollte. D.h. gerne können sich die Festplatten usw. abschalten, aber die normale Bedienung wie Bildschirm müssen aktiv bleiben.

Bitte denke auch daran, dass das Terminal so eingestellt sein muss, dass es nach einem eventuellen Stromausfall sofort wieder startet.

### Einstellungen des Terminals sind Benutzerrechte
Idealerweise installierst du das Terminal unter dem Benutzer, unter dem auch das Terminalprogramm laufen sollte. Wenn du aus verschiedenen Gründen das unter einem anderen Benutzer installieren musst, denke daran, dann für den Terminalbenutzer die Konfiguration erneut durchzuführen.

## Neuerungen mit der Version 0.0.8
Mit der Version 0.0.8 des Terminals stehen zusätzlich
![](V08_01.png)  
- Anmeldeseite anzeigen
- Saldo Button anzeigen

zur Verfügung.
### Anmeldeseite anzeigen
Damit werden alle Personen die im Personalstamm eingetragen sind und eine Ausweisnummer eingetragen haben in Form einer Buttonliste angezeigt.
![](V08_02.png)  
Durch Tipp auf den Button gelangt man weiter zur Buchungsseite des Terminals.
### Saldo Button anzeigen
Wird der Saldo Button angezeigt, so wird neben Kommt, Pause, Geht auch noch Saldo angezeigt. Mit Tipp auf Saldo wird der aktuelle Zeitsaldo des laufenden Monates ermittelt und angezeigt.<br>
![](V08_03.png)  <br>
Tippt man nun auf Detail wird die Monatsabrechnung des aktuellen Monates angezeigt.

**Hinweis1**: Die Stunden des Zeitsaldos werden immer vom letzten des Vormonates errechnet. D.h. damit diese stimmen, müssen auch die Monatsabrechnungen ausgedruckt worden sein.

**Hinweis2**: Wird nach dem Klick auf den Saldo Button nichts angezeigt / es erfolgt keine Reaktion, dann fehlt bei der Person das Eintrittsdatum, womit keine Berechnung von gültigen Stunden erfolgen kann.

## Neuerungen der Version 0.0.9
Mit der Version 0.0.9 des Terminals stehen zahlreiche weitere Funktionen, vor allem im Bereich Barcode-Scan zur Verfügung. Die wesentlichste Neuerung ist allerdings die offline Fähigkeit.
D.h. es können, auch wenn das Terminal vorübergehend keine Verbindung zum **Kieselstein ERP** Server hat, die wesentlichsten Buchungen zwischengespeichert werden.
D.h. diejenigen Buchungen, die ohne Antwort vom Server nur gespeichert werden müssen, werden im Terminal gespeichert. Steht die Serververbindung dann wieder zur Verfügung, werden innerhalb kurzer Zeit die Buchungen an den Server übertragen.

[Weitere Funktionsbeschreibungen siehe]( {{<relref "/fertigung/zeiterfassung/ze_terminal"  >}} )

## Neuerungen der Version 0.0.10
- Anzeige der Maschinenliste mit Datum, sowie Korrektur kleinerer Bugs
- Reconnect des Barcodescanners, falls während des Betriebes die Verbindung verloren gegangen ist (was leider manchmal bei Stromschwankungen und ähnlichem passiert).

### Netzwerkverbindung
Für das Terminal muss eine Netzwerkverbindung, also mehr wie ein lokales Netz (nur auf dem einen Rechner) verfügbar sein, damit das Terminal dieses Netzwerk als gültige Verbindung ansieht. Localhost wird speziell behandelt.<br>
<u>Hintergrund:</u> Gerade für die mobilen Geräte, muss man sehr rasch erkennen können, ob noch
eine Netzwerkverbindung da ist oder nicht. Es würde sonst das Netzwerk-Timeout jeweils 4-5 Minuten dauern, was sehr unpraktisch wäre.

## Weitere Funktionen
### Die Konfiguration ist mittels Passwort geschützt
Wenn du auf die Konfiguration klickst, so erscheint gegebenenfalls die Abfrage nach einem Passwort.<br>
![](Passwort.png)  <br>
Hier muss das hinterlegte Passwort eingegeben werden. Bitte entsprechend ändern / sorgfältig behandeln.

### Löschen falscher Comport Zuordnungen
Gerade bei den Tests am Terminal verwendet man schon auch mal andere Erfassungsgeräte. Entfernt man nun diese, z.B. den Barcodescanner, so kommt beim nächsten Start des Terminals Comport xy ist nicht vorhanden. Um diese Meldung abzuschalten, in die Einstellungen gehen und bewusst den Comport z.B. des Barcodescanners auswählen und danach auf "Speichern" tippen. Damit ist die Zuordnung gelöscht.

### Die Meldungsbuttons sind zu klein
Je nach Größe des Terminal-Gerätes sind schon mal die Knöpfe bei den Meldungen zu klein. Abhilfe schafft man hier dadurch, dass man über das Betriebssystem die default Darstellung / Vergrößerungsfaktor höher dreht.<br>
![](Skalierung.png)  

### Die Monatsabrechnung hat mehr wie eine Seite
Wenn die Monatsabrechnung mehr als eine Seite hat, so wird trotzdem nur die erste Seite dargestellt. Bitte gestalte den Report so, dass er auf einer Seite Platz hat. Denke bitte dabei auch an deine / eure Mitarbeiter:innen die am Terminal den Report lesen wollen. Hier ist meist weniger mehr. Tipp: Stelle den Report auf Hochformat oder wenn wirklich erforderlich auf ein noch höheres Format, sodass alles auf einer Seite dargestellt werden kann.

### Timeouts der verschiedenen Anzeigen
Wir haben uns vorerst dazu durchgerungen, die Timeouts auf den verschiedenen Anzeigen nicht zu implementieren. Die bisherigen Erfahrungen haben gezeigt, dass diese immer zu lange oder zu kurz, aber sehr selten richtig sind. Es gibt anstatt dessen einen Button zurück, mit dem man wieder in die normale Erfassung, des Ausweises, kommt.

### Einrichten auf einem Windows PC
Mit der Version 0.0.9.0 kann das Terminal auch im Vollbildmodus betrieben werden. Damit auch im Vollbildmodus die Meldungen des Terminals angezeigt werden, muss der Benachrichtigungsmodus entsprechend eingestellt werden.
#### Windows 10
Windows 10:
"System" - "Benachrichtungsassistent" - "Automatische Regeln": Wenn ich eine App im Vollbildmodus verwende" auf "AUS" setzen.
![](Benachrichtigungen_W10_01.png)  


Für englische Terminals
"System" - "Notifications & actions" - "Focus assist" - "Automatic rules"
"When i'm using an app in full-screen mode" is switched to "OFF"

#### Windows 11
- Benachrichtigungen generell ein
- im "Bitte nicht stören", den Haken bei "Bei Verwendung einer App im Vollbildmodus ..." -> raus

![](Benachrichtigungen_W11_01.png)  

- und noch zusätzlich die Benachrichtigung der App Kieselstein-ERP Terminal, einschalten
![](Benachrichtigungen_W11_02.png)  

Solltest du ein Terminal in Englisch nutzen, so findest du diese Einstellungen unter
![](Benachrichtigungen_W11_en_01.png)  

bzw. dann weiter unten
![](Benachrichtigungen_W11_en_02.png)  

### Hinweis zur Bildschirmtastatur:
Diese steht nur zur Verfügung, wenn auf dem Gerät **keine** physikalische Tastatur angeschlossen ist. Das gilt auch, wenn eine Fernwartungssitzung zum Terminal aktiv ist. D.h. idealerweise NACH der Fernwartungssitzung den Terminal-PC komplett neu starten (diese Tastatureinstellungen werden nur beim Betriebssystemneustart übernommen).

#### Unter Windows 10:
- Einstellungen, System, Tablet
- Wenn ich das Gerät als Tablet verwende, immer in den Tablet Modus wechseln
- dann weiter zu:
- Zusätzliche Tablett Einstellungen
- Bildschirmtastatur anzeigen, wenn keine Tastatur angeschlossen ist.
- Zusätzlich: Tablet Modus aus.<br>
![](Tablet_w10.png)  <br>
oder<br>
![](Tablet_w10_02.png)  <br>


#### Unter Windows 11
![](Win11_Bildschirmtastatur.png)
Stelle idealerweise die Bildschirmtastatur auf immer aktiv.

##### Bedienung der Tastatur
Es ist oft praktisch nur eine kleine Tastatur eingeblendet zu haben.<br>
![](Win11_kleine_Bildschirmtastatur.png)  <br>
Dazu einfach rechts oben auf die Tastatur mit Pfeil nach unten tippen, bzw. dann auf die Tastatur mit dem Pfeil nach oben.

Um die Bildschirmtastatur zu aktivieren, müssen folgende Einstellungen getroffen werden:
- Einstellungen, Zeit und Sprache
- auf Eingabe klicken
- Bildschirmtastatur erweitern und Bildschirmtastatur anzeigen auf "Immer" stellen.
![](Win11_Bildschirmtastatur_01.png)<br>

#### Swipe Modus abschalten
Wenn das Terminal im Vollbildmodus betrieben wird und man verhindern will, dass durch Wischen 
von den Ecken das sogenannte Windows Action Center aktiviert wird und damit der Benutzer auf das Betriebssystem kommt, muss diese Funktionalität über die Registry abgeschaltet werden. Dazu sowohl in Windows 10 als auch 11 im Key
>  HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PolicyManager\default\LockDown\AllowEdgeSwipe
>
den Wert auf 0 setzen und danach neu starten.

#### Weitere mögliche Einstellungen
Manche Anwender haben gerne noch weitere Anpassungen. [Siehe dazu]( {{<relref "windows_einstellungen" >}} )

##### Anmeldeseite
Wenn du die Anmeldeseite verwendest, so kann es, je nach Version deines Terminals vorkommen, dass einmal gedrückte MitarbeiterInnen Buttons ![](Anmeldeseite_1.png) in einer Kontrastfarbe angezeigt werden, die schlecht lesbar ist.<br>
Dieses Verhalten kann unter System(Einstellungen), Barrierefreiheit, Visuelle Effekte, Animationseffekte EIN abgeschaltet werden.<br>
Bitte beachten: Nach einer Änderung dieser Einstellung bitte das Terminalprogramm neu starten.
![](Anmeldeseite_2.png)

## eine bestehende Installation aktualisieren
Lade wie beschrieben die [aktuelle Version](https://gitlab.com/kieselstein-erp/sources/kieselstein-terminal-maui/-/releases) herunter.<br>
Entpacke die Datei in ein Verzeichnis deiner Wahl, z.B. d:\kieselstein\terminal\Terminal_0.0.9.0_Test. Hier findest du nun die Datei *Terminal_0.0.Version.0_x64.msix*. Diese starten und aktualisieren wählen.

Dies sollte unter dem Terminalbenutzer erfolgen.

## eine bestehende Installation entfernen
Bevor du eine neue Version des Terminals installierst, ist es derzeit erforderlich die bestehende Installation zu beenden. Dazu, Start, Einstellungen, Apps, und dann durch Eingabe der Anfangsbuchstaben die Kiesel.

Ab der Version 0.0.10 ist das Entfernen nicht mehr erforderlich.

![](Installation23.png)  
das Terminalprogramm anzeigen und anklicken und dann

![](Installation24.png)  
Deinstallieren auswählen.

## Tipps & Tricks
Wenn das Terminal nicht startet, z.B. mit Verbindung verweigert, so prüfe, ob du vom Terminal-Rechner aus die Rest-API-Doku anzeigen kannst. Das Terminal nutzt die Restapi für die Buchungen. Ist diese nicht gestartet, ist natürlich keine Verbindung möglich.
![](Verbindung_verweigert.png)  

### Log-File
Die Terminal-App schreibt auch ein Logfile, in dem wesentliche Schritte z.B. beim Programmstart protokolliert werden. Dieses findest du unter:<br>
c:\Users\\<u>*Terminalbenutzer</u>*\AppData\Local\Packages\cce53e67-e463-4746-bede-e7cae70f832a_5wc6556p9s44j\LocalCache\KieselsteinTerminalLogs
c:\Users\keg\AppData\Local\Packages\cce53e67-e463-4746-bede-e7cae70f832a_5wc6556p9s44j\LocalCache\KieselsteinTerminalLogs\.. 


Anstatt des <u>*Terminalbenutzer</u>* bitte durch den für das Terminal verwendeten Benutzer ersetzen.

### Welches Terminal ist das?
Gerade wenn mehrere Terminals im Einsatz sind und man mit den verschiedensten Parametrierungen kämpft, ist es praktisch zu wissen welches Terminal was ist. Auch wenn es z.B. um die Einrichtung des automatischen Ausdruckes von Ablieferetiketten geht.<br>
Du findest diese Info in der Titelleiste des Konfigurationsmenüs.<br>
![](Terminal_name_ipadresse.png)

### Uhrzeiten synchronisieren
[Siehe dazu bitte]( {{<relref "/fertigung/zeiterfassung/zeitserver" >}} )

### Zertifikats-Fehler
Kommt unmittelbar beim Start der Installation die Fehlermeldung:<br>
Fehler: Das Paket bzw. Bundle ist nicht digital signiert, oder die Signatur ist beschädigt<br>
![](Zertifikatsfehler_01.png)  <br>
so sollte herausgefunden werden, was die eigentliche Ursache für den Fehler ist. Üblicherweise kommt der Fehler von einem defekten Download oder einer alten Installation unter falschen Benutzerberechtigungen. In diesem Falle raten wir zu folgender Vorgehensweise:
- Terminal-App komplett [deinstallieren](#eine-bestehende-installation-entfernen)
- Terminal-App [neu herunterladen](#eine-bestehende-installation-aktualisieren)

Sollte nun der Fehler trotzdem bestehen, kannst du versuchen die Ursache, warum das Zertifikat nicht installiert werden kann, manuell herauszufinden. Dazu in das Installationsverzeichnis der Terminalsoftware gehen und das Zertifikat manuell installieren:<br>
![](Zertifikat_manuell_installieren_01.png)  <br>
Dann am besten<br>
![](Zertifikat_manuell_installieren_02.png)  <br>

Lokaler Computer auswählen.

Danach alles default installieren.<br>
Man sollte dann anhand der Fehlermeldung erkennen können, was die eigentliche Ursache ist, warum die Installation nicht geht. Dann eben diesen Fehler entsprechend abstellen.

Eine weitere Ursache kann sein, dass die zu installierende Terminalversion ein zu altes Zertifikat verwendet. D.h. von der **Kieselstein ERP eG** wird die Terminal-App mit einem sogenannten SelfSignedZertifikat versehen. Diese Zertifikate haben eine Laufzeit von 12 Monaten ab Zertifikatserstellung. Ist nun deine Terminalversion und oder das darin verwendete Zertifikat zu alt also abgelaufen, zu erscheint eine entsprechende Fehlermeldung. Man sieht das auch, wenn du die Zertifikatsdatei z.B. ![](Zertifikatsdatei.png) mit einem Doppelklick startest, so werden die Zertifikationsinformationen<br>
 ![](Zertifikats_Informationen.png)<br>
 und damit auch die Gültigkeit von bis angezeigt.

 Ist das Zertifikat abgelaufen, kann derzeit die Terminal-App nicht mehr installiert werden. Bitte verwende eine neuere Terminal-Version.<br>
 In absoluten Ausnahmefällen kann eine alte Terminalversion von einem Programmierer mit einem neuen Zertifikat versehen werden. Bitte rechne mit einem entsprechenden Zeitaufwand, je nach Umgebung, die der Entwickler für die Neu-Einbindung aufwenden muss.

### Berechtigungs-Fehler
Erscheint bei der Installation des Terminals<br>
![](Berechtigungsfehler.png)  <br>
so hast du zu wenig Rechte, um die App zu installieren.<br>
Hier ist wichtig zu wissen, auch wenn du als Administrator angemeldet bist, aber das Powershell nicht mit als Administrator ausführen gestartet hast, hat das Powershell unter Umständen zu wenig Rechte um die Installation vorzunehmen. Daher dann, entgegen obiger Beschreibung das PowerShell für die Terminal-Installation mit "als Administrator ausführen" ![](AlsAdministrator_Ausfuehren.png)  starten.<br>
Der Nachteil (der auch ein Vorteil sein kann): Neuinstallationen / Updates müssen auch entsprechend gleich ausgeführt werden.

### Fehlermeldungen
![](Installation_Fehlermeldung_1.png)  
Deutet darauf hin, dass vom Sys-Admin keine Rechte vergeben wurden, um die Software unter diesem User zu installieren.

Prüfe zusätzlich, ob das Betriebssystem deines Terminals auf aktuellem Stand ist. Oft sind Fehler dieser Art auf fehlende Updates zurückzuführen und natürlich:
Denke daran, nach diversen Einstellungsversuchen die Terminal Hardware auch mal durchzustarten.

### Stromversorgung
Üblicherweise erwarten sich die Mitarbeiter:innen, dass Zeiterfassungs-Terminals 24/7 verfügbar sind. Daher sind diese auch Offline-fähig programmiert. Das bedeutet aber auch, dass die Stromversorgung für das jeweilige Terminal entsprechend stabil gegeben sein muss.

## Terminal AddOns
### Bildschirmschoner mit Anwesenheitsliste

### Proxyserver für Anwesenheitsliste
Wenn du mehr als 2 Terminals im Einsatz hast und ev. viele MitarbeiterInnen, bzw. eine komplexe Anwesenheitsliste. Verlagert die Abfragedauer auf den Proxyserver, wodurch dein Terminal auf den Bildschirmschoner wesentlich schneller reagiert.

### Offline Lese Stifte


