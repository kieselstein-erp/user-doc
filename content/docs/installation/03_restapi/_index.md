---
title: "Installation RestAPI"
linkTitle: "RestAPI"
categories: ["Installation RestAPI"]
tags: ["RestAPI", "Webzugriff", "Installation"]
weight: 300
description: >
  Installation der RestAPI für Ihren Kieselstein ERP Server
---
Damit kann ein entsprechender Zugriff über die RestAPI auf deinen **Kieselstein ERP** Server erfolgen. Dieser basiert wiederum auf Tomcat / Apache.

In der Standard Installation des **Kieselstein ERP** Servers wird automatisch der Rest-Server mit installiert.

Für die Dokumentation zu den aktuell verfügbaren RestAPI Calls [siehe]( {{<relref "/docs/stammdaten/system/web_links/rest_api" >}} )