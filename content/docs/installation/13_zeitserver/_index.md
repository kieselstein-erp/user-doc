---
categories: ["Zeitserver"]
title: "Zeitserver einrichten"
linkTitle: "Zeitserver"
date: 2023-01-31
weight: 1300
description: >
  Einrichten von Zeitservern
---
Zeitserver
==========
Gerade in der Zeiterfassung, aber auch für das Zutrittskontrollsystem ist eine exakte amtliche Zeit wesentlich. Stellen Sie daher sicher, dass alle beteiligten Komponenten zeitsynchron sind.

Beteiligte Komponenten sind:
-   **Kieselstein ERP** Server
-   **Kieselstein ERP** Clients
-   **Kieselstein ERP** BDE Stationen
-   Zeiterfassungsgeräte wie das ZE-Terminal, aber auch die Offline-Stifte, ev. mobile Geräte
-   Zutrittskontroller

Üblicherweise wird der **Kieselstein ERP** Server auf die Zeit eines Atomzeit-Server im Internet synchronisiert.

Alle anderen Netzwerkkomponenten werden auf diesen Server synchronisiert.

Eine Auswahl der verfügbaren Zeitserver finden Sie unter <http://www.pool.ntp.org/zone/europe> oder unter <http://timeserver.verschdl.de/>

Wir verwenden üblicherweise:
| Server | IP (V4) |
| --- |  --- |
| ntp0.fau.de (ntp0-rz.rrze.uni-erlangen.de) | 131.188.3.220 |
| ntp1.fau.de (ntp1-rz.rrze.uni-erlangen.de) | 131.188.3.221 |
| ntp2.fau.de (ntp2-rz.rrze.uni-erlangen.de) | 131.188.3.222 |
| ntp3.fau.de (ntp3-rz.rrze.uni-erlangen.de) | 131.188.3.223 |
| ptbtime1.ptb.de | 192.53.103.108 |
| ptbtime2.ptb.de | 192.53.103.104 |

## Synchronisierung der Server mit den Atomzeitservern
### Windows 2003
<dl>
<dd>w32tm /config /syncfromflags:manual /manualpeerlist:ts1.univie.ac.at</dd>
<dd>w32tm /config /update</dd>
<dd>w32tm /resync</dd>
</dl>

### Linux
Eventuell muss der NTP Dienst zuerst installiert werden. In unseren Linux-Installationen ist dieser bereits eingerichtet.
Installation mit 

<dl>
<dd>yum install ntp</dd>
<dd>chkconfig --levels 235 ntpd on</dd>
</dl>

Bei bereits laufendem ntp Dienst:
<dl>
<dd>service ntpd stop</dd>
<dd>ntpdate IP-Adresse ... verwenden Sie hier die Adresse eines erreichbaren NTP Servers</dd>
<dd>service ntpd star</dd>
<dt>Ist der Zeitdienst (ntpd) wirklich synchron ?</dt>
<dd>ntpq -p</dd>
</dl>

### MAC OS X
Öffnen Sie die Systemeinstellungen und klicken Sie auf Datum und Uhrzeit. Wählen Sie den Reiter Datum & Uhrzeit und setzen Sie einen Haken bei Datum & Uhrzeit automatisch einstellen. Wählen Sie einen Server aus der Liste aus oder geben Sie den Namen eines NTP Server, z.B. aus obiger Liste, an. Siehe dazu auch: <http://docs.info.apple.com/article.html?artnum=61273>

### Ergänzend
Siehe dazu auch: http://www.msxfaq.de/verschiedenes/timesync.htm