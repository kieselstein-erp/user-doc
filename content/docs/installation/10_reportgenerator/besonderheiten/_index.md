---
categories: ["Formulare"]
tags: ["Besonderheiten"]
title: "Besonderheiten"
linkTitle: "Besonderheiten"
date: 2023-01-31
weight: 940
description: >
  Hinweise zu besonderen Formular Verhalten und ähnlichem
---
### Kostenstellenspezifische Kopf- und Fußzeilen
Hier ist darauf zu achten, dass auch die Dateien ausdruckmedium.j* mit in die jeweiligen Subreports einkopiert werden müssen.

Solltest du in der Druckvorschau, in der Dokumentenablage übereinander gedruckte Informationen, meist in Kombination mit Speichern unter, finden<br>
![](ueberlagerte_Ausdruckmedien.png)<br>
so hilft das Einkopieren der beiden Dateien.

Hinweis: Die Pfad Definition findest du unter System, Mandant, Kostenstellen, Subdirectory.
