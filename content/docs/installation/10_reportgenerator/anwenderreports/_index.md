---
categories: ["Anwender Reports"]
tags: ["Anwender Reports", "Reportgenerator"]
title: "Anwender Reports"
linkTitle: "Anwender Reports"
date: 2023-01-31
weight: 600
description: >
  Nutzen von Anwender Reports
---
Anwender Reports müssen in dem `${KIESELSTEIN_DATA}/reports` Ordner abgelegt werden.

Der reports Ordner muss immer `reports` genannt sein.

## Namenskonvention
{KIESELSTEIN_DATA} ... bezeichnet jenes Verzeichnis, welches in deiner **Kieselstein ERP** Installation mit der Umgebungsvariablen / Environment definiert ist. In der Regel ist dies für Windows ?:\kieselstein_data\ für Linux /opt/kieselstein/data

{KIESELSTEIN_DIST} ... bezeichnet das Verzeichnis, welches in der Environment Variablen hinterlegt ist. In der Regel ist dies für Windows ?:\kieselstein_dist\ für Linux /opt/kieselstein/dist

## Finden des zu verwendenden Reports
Je nachdem wo du deine Reports hinterlegst, wird der entsprechende Report verwendet. Die Logik dafür ist folgende:

### Pfade mit einer Kostenstelle
Falls eine Kostenstelle vorhanden ist, werden zuerst folgende Pfade durchsucht.

1. {KIESELSTEIN_DATA}/reports/{modul}/{kostenstelle}/{mandant}/{locale}
2. {KIESELSTEIN_DATA}/reports/{modul}/{kostenstelle}/{mandant}
3. {KIESELSTEIN_DATA}/reports/{modul}/{kostenstelle}

## Standard Pfade
Ansonsten werden folgende Pfade nach einem Report durchsucht.

1. {KIESELSTEIN_DATA}/reports/{modul}/{mandant}/{locale_country}
2. {KIESELSTEIN_DATA}/reports/{modul}/{mandant}/{locale}
3. {KIESELSTEIN_DATA}/reports/{modul}/{mandant}
4. {KIESELSTEIN_DATA}/reports/{modul}
5. {KIESELSTEIN_DIST}/wildfly-X.X.X.Final/kieselstein/reports/{modul}/{locale_country}
6. {KIESELSTEIN_DIST}/wildfly-X.X.X.Final/kieselstein/reports/{modul}/{locale}
7. {KIESELSTEIN_DIST}/wildfly-X.X.X.Final/kieselstein/reports/{modul}

<u>Hinweis:</u><br>
Das impliziert auch, dass die Subdirectorys der Kostenstellen sich nicht mit den Mandantennummern überschneiden dürfen.

D.h. die Reports, welche sich unter {KIESELSTEIN_DIST} befinden, werden bei jedem Update überschrieben. Die Pflege der Anwender-Reports unter Data liegt in deiner Verantwortung.

### Anzeige welcher Report verwendet wird
Um den richtigen Report bearbeiten zu können, muss man auch wissen, welcher Report für den Druck den tatsächlich verwendet wird. Daher wird im Titel des Reports und in den üblichen Listen-Auswertungen der verwendete Pfad angezeigt.<br>
![](Report_Titel.png)<br>
![](Report_Name.png)<br>
Eine wichtige Information ist der führende Buchstabe vor dem Reportpfad. Hier bedeutet S, dass dies ein **S**tandard-Report ist, und ein **A** steht für den Anwender-Report.<br>
D.h. die **S**tandard-Reports findest du unter .../kieselstein/dist/wildfly-26.1.2.Final/kieselstein/reports/... und die **A**nwenderreports unter .../kieselstein/data/reports/...

## Migration der Reports
Alter Pfad {KIESELSTEIN_DIST}/wildfly-X.X.X.Final/kieselstein/reports/{modul}/anwender/{mandant}/{sprache}
Neuer Pfad KIESELSTEIN_DATA/reports/{modul}/{mandant}/{sprache}

Auf Linux kann man `find . -name "anwender" -type d` verwenden, um die Pfade aller Anwenderreport Ordner zu finden.

Beispiele:

  * {KIESELSTEIN_DIST}/wildfly-26.1.2.Final/kieselstein/reports/allgemein/anwender/001/en
  * {KIESELSTEIN_DATA}/reports/allgemein/anwender/001/en


  * {KIESELSTEIN_DIST}/wildfly-26.1.2.Final/kieselstein/reports/rechnung/anwender/001
  * {KIESELSTEIN_DATA}/reports/rechnung/001


  * {KIESELSTEIN_DIST}/wildfly-26.1.2.Final/kieselstein/reports/finanz/anwender
  * {KIESELSTEIN_DATA}/reports/finanz

### WICHTIG
In einigen alten Reports sind teilweise auch direkte Zuweisungen wie z.B. für Logos oder ähnlichem enthalten. Diese Reports müssen überarbeitet werden.

### Sicherung der Reports
{{% alert title="WICHTIG" color="primary" %}}
Denke daran, dass nun nur mehr die Reports von ../kieselstein/data/reports gesichert werden müssen.
{{% /alert %}}

### Übertragung der Report-Einstellungen
Nachdem nun die Strukturen der Reports geändert wurden, können mit nachfolgendem Script diese Werte übernommen werden.
```
update lp_standarddrucker ls
set c_reportname = case when ls.c_reportname like '%/anwender/%.jasper' then '../../../../data/reports' || substr(replace(ls.c_reportname, '/anwender', '/' || substr(ls.c_reportname, 0, POSITION('/' IN ls.c_reportname))), POSITION('/' IN ls.c_reportname)) 
                        when ls.c_reportname like '%\anwender\\%.jasper' then '..\..\..\..\data\reports' || substr(replace(ls.c_reportname, '\anwender', '\' || substr(ls.c_reportname, 0, POSITION('\' IN ls.c_reportname))), POSITION('\' IN ls.c_reportname)) 
                   end
where (c_reportname like '%/anwender/%.jasper' or c_reportname  like '%\anwender\\%.jasper')
  and not exists (
        select 7 from lp_standarddrucker ls2
        where ls2.c_pc = ls.c_pc
          and ls2.c_drucker = ls.c_drucker
          and ls2.mandant_c_nr = ls.mandant_c_nr
          and ls2.c_reportname = case when ls.c_reportname like '%/anwender/%.jasper' then '../../../../data/reports' || substr(replace(ls.c_reportname, '/anwender', '/' || substr(ls.c_reportname, 0, POSITION('/' IN ls.c_reportname))), POSITION('/' IN ls.c_reportname)) 
                                      when ls.c_reportname like '%\anwender\\%.jasper' then '..\..\..\..\data\reports' || substr(replace(ls.c_reportname, '\anwender', '\' || substr(ls.c_reportname, 0, POSITION('\' IN ls.c_reportname))), POSITION('\' IN ls.c_reportname)) 
                                 end
          and (ls2.reportvariante_i_id = ls.reportvariante_i_id or (ls2.reportvariante_i_id is null and ls.reportvariante_i_id is null))
       );
```
