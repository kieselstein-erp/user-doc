---
categories: ["Formulare"]
tags: ["XSL"]
title: "XSL-Dateien"
linkTitle: "XSL"
date: 2023-01-31
weight: 930
description: >
  Verwendung / Arbeiten mit XSL-Dateien
---
Ergänzend zu den Formularen werden im **Kieselstein ERP** auch XSL Dateien verwendet.

Diese dienen der Steuerung der EMail-Vorlagen Texte und der Anpassung der Exportformate an die verschiedenen Finanzbuchhaltungsprogramme.

Nun ist das XSL faktisch eine Art Programmiersprache, womit sich eine weite Möglichkeit für die Anpassung der Ergebnisse ergibt.

Eine schnelle Übersicht findet man z.B. unter https://www.w3.org/People/maxf/XSLideMaker/tut.pdf

Vielleicht hilft auch https://www.softwaretestinghelp.com/xslt-tutorial/ dem einen oder anderen weiter. Gefunden habe ich auch: https://www.tutorialspoint.com/xslt/index.htm

## Komische Zeichen in EMails
Erscheinen in deinen versandten EMails komische Zeichen, z.B.<br>
![](komische_zeichen.png)  
So ist die EMail-Vorlage mit einem Standard Editor bearbeitet worden, wodurch die notwendige Codedefinition verschwindet. Leider ist es so, dass man diesen Fehler nur sehr mühsam findet. Mir ist derzeit nur ein verlässlicher Weg bekannt, mit dem festgestellt werden kann, ob die Datei richtig eingestellt ist.
Lasse dir die Datei mit einem File-Viewer anzeigen der auch Hexdaten darstellen kann. In Windowssystemen nutze ich dazu den im Totalcommander integrierten Fileviewer und schalte mit der Ziffer 3 auf die Hexdarstellung um. Hier sieht man den Unterschied.
| Ansicht | Inhalt |
| --- | --- |
| Normale Darstellung | ![](mail_xsl_normale_darstellung.png)   |
|Hex-Darstellung | ![](mail_xsl_hex_darstellung.png)  <br>hier sieht man deutlich, dass in einer richtig codierten Datei am Anfang noch drei Steuerzeichen sind. Diese codieren die Datei im UTF-8-BOM Format. Leider werden diese Informationen von den allermeisten Editoren entfernt. | 

Daher dürfen diese Dateien (in Windowssystemen) nur mit dem Notepad++ bearbeitet werden. Hier wird diese Information erzeugt / hinzugefügt. Siehe dazu auch:<br>
![](mail_xsl_utf-8-bom_Codierung.png)  

{{% alert title="UTF-8-BOM" color="warning" %}}
Es müssen die EMail-XSL Vorlagen in UTF-8-BOM codiert sein.
{{% /alert %}}

**Wichtig:** Wenn man Texte von anderen Vorlagen importiert, oder "falsche" Vorlagen korrigiert, so kommt es auch im Notepad++ immer wieder vor, dass die Kodierung automatisch passend zu den gerade importierten Zeichen umgeschaltet wird. D.h. es muss unbedingt vor der Verwendung geprüft werden, ob die Kodierung noch immer auf UTF-8-BOM steht.<br>
Es kommt sonst immer wieder vor, dass gerade unsere deutschsprachigen Umlaute falsch dargestellt werden, oder dass beim Versuch die Rechnung per EMail zu senden, eine Fehlermeldung kommt.<br>
Sieht man im Server Log nach, so findet man: 
>2024-04-16 16:51:16,582 ERROR [stderr] (default task-3) [Fatal Error] :17:12: Invalid byte 1 of 1-byte UTF-8 sequence.
>
>2024-04-16 16:51:16,583 ERROR [stderr] (default task-3) System-ID unbekannt; Zeilennummer17; Spaltennummer12; org.xml.sax.SAXParseException; lineNumber: 17; columnNumber: 12; Invalid byte 1 of 1-byte UTF-8 sequence.


## Sonderzeichen in den EMail-Vorlagen definieren
Da, insbesondere in den Firmen-Namen immer wieder auch Sonderzeichen enthalten sind, müssen diese entsprechend in den XSL-Vorlagen codiert übergeben werden, da das eigentliche Sonderzeichen eine besondere Bedeutung hat. D.h. diese Zeichen sind wie gewünscht zu kodieren:
| Zielzeichen | Codierung |
| --- | --- |
| & | \&amp; |

Siehe dazu gerne auch https://www.htmlhelp.com/de/reference/html40/entities/special.html, Spalte Entität, bzw. Darstellung im Browser


## Kostenstellen Subdirectory wird bei XSL nicht unterstützt
Die mögliche Steuerung der Formularvorlagen über die zusätzliche Subdirectory Funktion in den Kostenstellen wird bei den mail.xsl Vorlagen nicht unterstützt.
