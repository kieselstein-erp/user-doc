---
categories: ["Belegkopfdaten"]
tags: ["Belegkopfdaten", "Reportgenerator"]
title: "Belegkopfdaten"
linkTitle: "Belegkopfdaten"
date: 2023-01-31
weight: 600
description: >
  Nutzen von Belegkopfdaten
---
Üblicherweise hat jeder offizielle Beleg, z.B. Rechnung, einen Informationsblock auf dem die Dinge wie Rechnungsnummer, Rechnungsdatum, Ansprechpartner usw. angegeben werden.

Diese Daten sind meist dynamisch, sollten aber trotzdem an der üblicherweise erwarteten Stelle zu finden sein.

Wir haben dafür den Subreport allgemein\belegkopfdaten.jrxml geschaffen, der von den jeweiligen Belegen zentral aufgerufen wird.

In diesem wird nur die Erscheinungsform definiert. Also Hintergrundfarbe, Schriftarten, Trennlinien, Positionierung, usw..

Die Grundidee ist, dass ein Subreportdatenobjekt erzeugt wird (siehe z.B. angb_angebot.jrxml).

In diesem ist je Zeile der erste Wert die Art dieser Zeile. Danach folgen vier Werte. Es müssen immer diese fünf Felder je Zeile gegeben sein.
Mit der Art wird gesteuert, wie die Zeile gedruckt wird.
Der Subreport selbst hat zwei Parameter, der als Überschrift z.B. Rechnung, verwendet wird und einen weiteren der für die Belegkennung (z.B. AG für Angebot) verwendet wird. Die Belegkennung wird nicht nach den verschiedenen Sprachen übersetzt.

Als Art sind aktuell folgende Werte definiert:
| Art | Beschreibung |
| --- | --- |
| Links1 | Überschrift in **fett** |
| Links2 | zwei Felder, Lable, Inhalt volle Breite |
| Links4 | vier Felder, Lable, Inhalt, Lable, Inhalt |
| Linie  | Trennlinie |
| Nicht  | nicht drucken, weil |

Zusätzlich ist es so aufgebaut, dass wenn der Inhalt (beide Inhalte) als null übergeben wird, wird die Zeile nicht gedruckt.

Wichtig: Es dürfen nur Strings übergeben werden. Damit liegt leider z.B. die Formatierung von Datumsangaben wieder im rufenden Report.

ACHTUNG: Offensichtlich muss man für trim() bzw. replace() das Null des String-Objektes vorher abfangen, sonst wird an den Subreport alles als Null übergeben.
D.h. wenn z.B. nur die Überschrift gedruckt wird, dann ist so ein Element beteiligt. Das auslösende Element findet man durch Auskommentieren.

## Positionierung im Beleg
Wir richten uns hier nach der Postverordnung und danach, dass die Adresse des Empfängers im DIN-lang Kuvert positioniert sein muss.<br>
Damit ergibt sich, dass dieser Block der Belegkopfdaten von links herein 260 Pixel und 265 Pixel breit sein muss. Die einzige mir bekannte Ausnahme wären alte Schweizer Kuverts, die genau verkehrt herum anzuordnen wären. Da auch die Schweizer inzwischen sich nach den DIN-lang Kuvert richten (dürfen / können) würde ich nichts umstellen.

Diese Ausrichtung gilt meiner Meinung nach auch, für Belege, die per PDF versandt werden, denn im Zweifel, wird das Dokument ausgedruckt.
