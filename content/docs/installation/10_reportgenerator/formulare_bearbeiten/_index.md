---
categories: ["Formulare"]
tags: ["Formulare bearbeiten"]
title: "Formulare bearbeiten"
linkTitle: "Formulare bearbeiten"
date: 2023-01-31
weight: 300
description: >
  Formulare bearbeiten
---

Wie in der Bearbeitung von Formularen vorgehen
===============================================

## Bereiche eines Formulars
Es hat sich bewährt, die Bereiche, welche Bands genannt werden, die nicht benötigt werden, mit Löschen zu entfernen. Die üblichen Standardbereiche (z.B. Summary) erscheinen dann in Grau und können jederzeit wieder aktiviert werden.

### Title


## Daten, die vom Kieselstein-ERP Server angeliefert werden
### Parameter
### Fields
### Variablen

## Empfehlungen zum Bearbeiten

## wo findet man im Jasperstudio was?

### Seiteneigenschaften:
- Report anklicken, rechte Maus
  - Show Properties
  - dann im rechten Eingabedialog, rechts unten ![](EditPageFormat.png)  Edit Page Format
  - hier kann auch die Darstellung der Feldeinheiten (Pixel, mm) eingerichtet werden.

### wie findet man Parameter / Field - Namen?
Manchmal kommt es vor, dass man einfach den Namen des Parameters / des Feldes nicht findet (weil schon 100-Mal überlesen). Dafür einfach in den Reiter Source gehen und hier nach Teilen des erwarteten Namens suchen. Meist wird es dann klar, wie der Name genau lautet.

### kann man Parameter / Fields von einem zum anderen Report kopieren?
Nein!<br>
Da die Werte / Inhalte der Parameter / Fields über sogenannte Call-Back abgerufen werden, werden vom **Kieselstein ERP** Server nur die Inhalte angeliefert, die auch tatsächlich programmtechnisch vorgesehen sind.

In allen Hauptreports stehen folgende Parameter zur Verfügung, auch wenn sie nicht in den Formularen ausgeführt sind (historisch bedingt).
- REPORT_DIRECTORY
- REPORT_ROOT_DIRECTORY
- P_LOGO_IMAGE
- P_MANDANT_OBJ
- P_SUBDIRECTORY
- P_MODUL
- P_SQLEXEC
  - P_SQLEXEC.execute(QueryString mit einem Rückgabewert)
  - P_SQLEXEC.executes(QueryString mit einem Array von Rückgabewerten)<br>Als Beispiel siehe pers_benutzerstatistik.jrxml, Variable MaxAB_Pos_1 bzw. [siehe]( {{<relref "/docs/installation/10_reportgenerator/formelsammlung/#sql-abfragen-f%c3%bcr-mehrere-werte" >}} )
  - P_SQLEXEC.subreport(QueryString mit Rückgabewert(en) die wiederum in einem Subreport verwendet werden können)

## kann man Formulare kopieren?
Das kommt darauf an!

Will man eine Reportvariante erstellen, so muss immer vom Originalformular ausgegangen werden.
Die meisten Fehler passieren, wenn man unterschiedliche Formulare, die optisch gleich sind, übertragen möchte.

Ein Beispiel:<br>
Das Los Ablieferetikett wurde schön gestaltet, es funktioniert alles. In diesem konkreten Falle wurde auch die Referenznummer der Stückliste mitgedruckt.

Nun bestand auch der Wunsch, dass das Losetikett gleich aussehen sollte, einziger Unterschied, die Chargennummer kommt aus dem Kommentar.<br>
Nun wurde einfach das Ablieferetikett kopiert und als Losetikett definiert. Ging eigentlich ganz gut, nur die Referenznummer wurde nicht gedruckt.<br>
Hintergrund: Die Fields und Parameter sind leider, aus den verschiedensten Gründen nicht immer gleich benannt. Daher findet der Callback das Field, den Parameter nicht und somit kann es nicht funktionieren.

Um nun, gerade bei Etiketten diese effizient vom einem Formular zum anderen zu übertragen hat sich folgende Arbeitsweise bewährt:
- Kopieren der neuen Ziel-Etikette auf den gewünschten Namen (alles klein, keine Umlaute nur Underline)
- Öffnen der optisch zu kopierenden Etikette und der Zieletikette im Reportgenerator
- Verschieben der Originalfelder in der Zieletiketten z.B. rechts raus, damit im eigentlichen Feld Platz wird.
- Kopieren aller Felder in die Zieletikette
- üblicherweise sind die Felder um 10x10 Pixel nach rechts unten verschoben. Also mit Strg+Cursor nach oben und nach links an die richtige Stelle schieben
- Speichern
- die Größe der Zieletikette anpassen
- die Felder / Parameter auf die richtigen Namen umbenennen
- die überzähligen Felder löschen
- Kompilieren. Hat man vergessen ein Feld umzubenennen, kommt ein entsprechender Fehler.

{{% alert title="Merke" color="warning" %}}
Fields und Parameter immer nur vom Original-Ausgangsreport nachtragen. **NIE** von irgendeinem anderen Report, auch wenn er sehr ähnlich zu sein scheint.
{{% /alert %}}

## was bedeuten die gelben Rufzeichen?
![](rufzeichen.png)  
in "alten" Reports werden für sehr viele Felder diese Ausrufezeichen angezeigt. Diese bedeuten generell, dass mit diesem Feld irgendwas nicht stimmt. Da man beim normalen Arbeiten diese Information benötigt, sollten die "falschen" Fehler / Warnungen entfernt werden.

Fährt man mit der Maus auf das Ausrufezeichen, so sieht man eine genauere Fehlermeldung. ![](Ausrufezeichen_Tooltipp.png)  
Das bedeutet nun, dass man, um diesen Hinweis wegzubekommen, die PDF Font Namen aus dem Source entfernen muss. Also:
- auf den Reiter Source klicken ![](Source.png)  
- Strg+F (Finden und Ersetzen) und PDFFontName eingeben ![](Suchen.png)  
- damit findet man den ersten Eintrag von ![](PdfFontName.png)  
- diesen durch nichts (leer) im gesamten XML ersetzen
- üblicherweise sind pdfFontName="Helvetica" und pdfFontName="Helvetica-Bold" verwendet. Diese alle entfernen. Damit sind die Warnungen bzgl. PdfFontName weg und die Rufzeichen haben wieder ihre übliche Bedeutung.

## Sammlung von praktischen Sonderzeichen
| Sonderzeichen | Bedeutung |
| --- | --- |
| • | Interpunktion |
| · | Middle Dot |
| √ | ok Häckchen, ACHTUNG: Anzeige im Browser geht so nicht. |
| Ø | Durchmesserzeichen, Alt+0216 |
Siehe dazu auch https://wiki.selfhtml.org/wiki/Zeichenreferenz oder auch https://seo-summary.de/html-sonderzeichen/

Für Sonderzeichen in den message-dateien (Sprachübersetzungen) müssen die Unicodezeichen verwendet werden. Eine Definition ist in den jeweiligen Sprachen enthalten, siehe aber auch z.B. https://symbl.cc/de/unicode/table/.


## Fehlermeldung: Parameter msg must not be empty
Wenn ein Formular, mit einem Barcode nicht gedruckt werden kann -> es kommt schwerer Fehler und es steht im Detail der Fehlertext:<br>
java.lang.NullPointerException: Parameter msg must not be empty<br>
So bedeutet dies, dass versucht wurde einen Barcode für einen Leerstring auszudrucken.<br>
Das muss bitte in der Druckbedingung für den Barcode abgefangen werden, sodass gar kein Barcode erzeugt wird.

## Druck von Datamatrix Codes
Es kommt immer wieder mal vor, dass Datamatrix Barcodes mal quadratisch und das andere Mal rechteckig gedruckt werden.<br>
Wenn das für dich wichtig ist, so muss im Reportgenerator (iReport)<br>
![](Datamatrix.png)<br>
bzw. im Jasper Studio im Reiter Barcode<br>
![](Datamatrix_JS.png)<br>
unter Shape auf Force Square gestellt werden.

Beachte bitte zusätzlich, dass die verwendete Library für den Datamatrix Code, so wie alle freien Bibliotheken, fehlerhafte Inhalte liefert, wenn drei oder mehr Sternchen in den Daten enthalten sind.