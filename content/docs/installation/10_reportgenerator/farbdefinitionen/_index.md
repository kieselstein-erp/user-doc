---
categories: ["Farbdefinitionen"]
tags: ["Farbdefinitionen", "Reportgenerator"]
title: "Farbdefinitionen"
linkTitle: "Farbdefinitionen"
date: 2023-01-31
weight: 500
description: >
  Bewährte Farbdefinitionen
---
Bei der Erstellung von Formularen / Reports ist auch immer die Frage, wie kann man besondere Felder hervorheben. Hier ist mitzubedenken, dass diese "Farben" auch auf monochromen (SW/WS) Druckern funktionieren sollten.

Gerade für den Druck von Hintergrund Farben hat sich bewährt Pastellfarben zu verwenden.

Ich habe hier einige von mir gerne verwendeten Farbkombinationen zusammengetragen.

Farbdefinitionen für Reports für das **Kieselstein ERP**-System

## Kieselsteinfarben
| Benennung | [R,G,B] Farben |
| --- | --- |
| Standard | [21,114,138] |
| hell | [33,179,203] |
| sehr hell |	[143,207,218] |
## Kieselstein Farben der Belegkopfdaten
| Benennung | [R,G,B] Farben |
| --- | --- |
| Hintergrund | [199,231,237] |
| Trennlinie | [171,219,227] |

## Hintergrund Farben = Regenbogenfarben in Pastelltönen
| Benennung | [R,G,B] Farben |
| --- | --- |
| rot | [247,171,173] |
| orange | [254,214,165] |
| gelb | [250,246,183] |
| grün | [206,229,183] |
| hellblau | [169,222,236] |
| dunkelblau | [165,193,230] |
| violett | [235,200,222] |

## weitere gern verwendete Farben
| Benennung | [R,G,B] Farben |
| --- | --- |
| hellgrau | [204,204,204] (Zebra) |
| orange | [255,161,50] (Sonntag) |
| mittleres orange | [255,184,101] |
| helles orange | [255,207,152] (Samstag) |
|  |  |
| blau | [88,193,218] |
| mittleres blau | 142,208,228] (Feiertag) |
| helles blau | [177,224,234] |
| sekundärfarbe | [4,111,159] |
|  |  |
| helles grün | [51,255,0] |
| helles hintergrund rot | [255,153,153] |
