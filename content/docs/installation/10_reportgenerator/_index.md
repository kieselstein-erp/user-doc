---
categories: ["Formulare"]
tags: ["Formulare bearbeiten", "Reportgenerator"]
title: "Installation Reportgenerator(en)"
linkTitle: "Reportgenerator"
date: 2023-01-31
weight: 1000
description: >
  Installationsbeschreibung zu den Reportgeneratoren, Tipps und Tricks, Return Variablen aus Subreport
---
Als Reportgeneratoren kommen zwei Versionen zum Einsatz.
Einerseits der alte und eigentlich nur mit Java 7 Code lauffähige iReport
und der Eclipse basierte Nachfolger Jasperstudio.

**Achtung:** Jasperstudio ist sehr ressourcenhungrig und leider nicht stabil (alle paar Stunden neu starten) auch das Arbeiten mit Subreports ist im Studio nicht wirklich implementiert. Daher verwenden wir gerne auch noch den iReport 5.5.0, trotz des massiven Nachteils, dass er nur mit Java7 Code arbeiten kann.

Für JasperStudio 6.20.x oder höher, solltest du ausreichend RAM in deinem Rechner zur Verfügung haben. Unsere Erfahrung, ab 16 GB humpelt es, aber 32GB geht's halbwegs.
https://sourceforge.net/projects/jasperstudio/files/

Installation [Jasperstudio]( {{<relref jasperstudio >}} )

**Hinweis:**
Wenn du Jasperstudio und iReport auf dem gleichen Rechner installieren möchtest, so zuerst Jasper Studio installieren und dann den iReport.
Es sollten die jrxml mit dem iReport und die jasper mit dem Studio verlinkt sein.

[Die Zusatz-Libs findest du hier]( ./jasperstudio/zusatz_lib/javase-3.2.1.jar) und 
[hier]( ./jasperstudio/zusatz_lib/core-3.2.1.jar)

## Welcher Reportgenerator muss wo verwendet werden
In den neueren Reports werden auch HelperReport Methoden verwendet, welche nur mit Java 8 compiliert wurden. Wenn diese Methoden verwendet werden, kann nur mehr mit JasperStudio kompiliert werden. D.h. wenn beim Kompilieren Fehlermeldungen wie z.B. ![](Compilier_Fehler_01.png)<br>
aufscheinen, so muss mit Jasperstudio kompiliert werden.

### Debricated Java Funktionen
Ab der Version 1.0.0 ist auch der Serverteil des **Kieselstein ERP** Servers mit Java 11 kompiliert. Das kann durchaus bedeuten, dass Funktionen, die in Java 7 zur Verfügung standen, in Java 8 bereits als debricated angekündigt waren, aber noch gingen und nun in Java 11 nicht mehr funktionieren.<br>
So wurden gerade im Bereich der Kalender und Zeit Methoden massive Änderungen durchgeführt.

## Tipps und Tricks für die Report gestaltenden Consultants
- Wenn man im Summary Band, den PSQL_Executer.subreport aufruft, muss danach noch ein Feld interpretiert werden, sonst macht der Report nichts. Das ist auch beim Jasper Studio so.
Hängt ev. auch an den Eigenschaften in dem der Subreport definiert ist!
- Variablen werden erst nach dem ersten Detail berechnet. Das bedeutet, dass diese erst nach dem Durchlaufen des (ersten) Details einen gültigen Wert haben. Will man nun z.B. einen Gruppen-Header, der ja vor dem eigentlichen Detail kommt, mit einer Variablen andrucken oder eben nicht, so kann das nicht über Variablen gelöst werden, sondern muss direkt in die Print When expression des Gruppen-Headers eingetragen werden.
- Man kann dem Report auch generelle Bedingungen mitgeben, durch welche ganze Zeilen herausgefiltert werrden (Siehe Eigenschaften des Reports).

## Rückgabe von Werten aus den Subreports
Diese Funktion ist gegeben, aber in der Definition der Return-Variablen **extrem** kritisch.

Der wesentlichste Punkt ist, dass man akzeptieren muss, dass Subreports parallel zum Hauptreport ausgeführt werden.
https://community.jaspersoft.com/wiki/how-sum-return-values-multiple-subreport-various-levels

Daher ist z.B. während der Ausführung eines Details (Detail-Bands mit Subreport) nicht sichergestellt, dass die Rückgabevariablen während des Rendering des Details-Bands bereits zur Verfügung stehen. Daher werden auch weiterrechnende Variablen, welche auf Rückgabewerten des Subreports aufbauen, nicht richtig bzw. zu spät interpretiert. Dieses "zu spät" ist offensichtlich auch von so Dingen wie Rechenlast abhängig.

Daher ist folgende Vorgehensweise offensichtlich erforderlich:
- Hauptreport
  - Variable, welche vom Subreport befüllt wird
    - Calculation:
      - System ... System bedeutet, es wird der Wert dieser Variablen **nicht** durch den (Haupt-)Report verändert (aber durch die Parameter Übergabe des Subreports und seiner Calculation-Art)
    - Resettype:
      | Resettype | Bedeutung | Hinweis |
      | --- | --- | --- |
      | Report | für Gesamtsummen unten am Ende| der Wert stimmt auch erst im Summary |
      | Group | für Gesamtsummen am Ende der Gruppe | der Wert stimmt erst im entsprechenden Group Footer |
      | None | für Zeilensummen im weiteren Detail | der Wert stimmt erst im, dem dem Subreport nachfolgenden Detail |
    - Wichtig: Wenn Sie, wie z.B. oben drei verschiedene Zwischensummen benötigen, so müssen drei Variablen definiert werden, welche durch die Parameter-Rückgabefunktion des Subreports, in drei Definitionen, befüllt werden.<br>![](ReturnParameter.png)  
    - Dass die Datentypen stimmen müssen, ist selbstverständlich.
  - Rückgabeparameter
    - Wichtig: Wenn Sie im Rückgabeparameter Calculationtyp Sum o.ä. verwenden, so dürfen vom Subreport **keine** Nullwerte kommen!
    - Interessant: Man kann durchaus zwei Variablen aus dem Subreport im Hauptreport zusammenzählen. D.h. es wird beim Rückgabeparameter die Destination Variable mehrfach angegeben und als Calculationsart SUM verwendet.

Um die vom Subreport erhaltenen Werte zu sehen, muss **nach** dem Detailband, in dem der Subreport ausgeführt wird, ein weiteres Detail-Band eingerichtet werden. In diesem stehen die Werte aus dem Rückgabeparameter, wenn der Resettype der Variablen auf none steht, zur Verfügung.

Sollte man am Ende des Reports einen Subreport mit Return Variable benötigen, so ist der Trick, dass man eine Gruppe mit Bedingung null macht. Diese so reihen, dass sie die äußerste Gruppe ist und den Subreport, der die Returnvariable liefert, im Gruppenfooter aufruft. Die Variable, in der die Returnvariable eingefüllt wird, muss auf diese Gruppe gestellt sein. Dann steht im Summary der Wert zur Verfügung. Gegebenenfalls schiebt man das Feld dann über den oberen Rand des Summarys hinaus, damit die optische Position "richtig" ist.

Wenn Werte aus dem Subreport in das rufende Report Summary übergeben werden sollten, muss im Hauptreport der Resettype auf Report stehen.

Als Beispiel siehe: rech_warenausgangsjournal_materialeinsatz.jrxml

### Trick
Will man im Summary einen Subreport mit Rückgabeparameter aufrufen, so gibt es ja nach dem Summary kein Band mehr, welches noch interpretiert wird. Daher macht man sich eine Gruppe mit Bedingung null und reiht diese als äußerste Gruppe. In den Group Footer gibt man seine Summary Variablen und den Aufruf des Subreports mit der Rückgabevariablen.
Die Variable in die der Rückgabewert eingetragen = aufsummiert wird, hat als Rest-Group diese äußerste Gruppe.
Im Summary, das ja dann danach kommt, kann man nun auf den Wert zugreifen. Gegebenenfalls schiebt man ein anzudruckendes Feld über den oberen Rand raus. Das ist nur mit der Tastatur möglich. In der Feldeingabe sind negative Werte für Top nicht zulässig. Gegebenenfalls auch direkt im XML ändern.

### Welche Parameter stehen immer zur Verfügung
Generell gilt die Regel, dass Fields und Parameter **<u>nicht</u>** von anderen Reports übernommen werden dürfen. Davon ausgenommen sind folgende Parameter, welche immer an alle Reports angeliefert werden und daher jederzeit nachgetragen werden können.
| Parameter | Bedeutung | kopieren von |
| --- | --- | --- |
| P_MANDANT_OBJ | Mandantenobjekt mit einer Vielzahl von weiteren Informationen über den Mandanten. Von Mandantennummer bis zum Länderkennzeichen | rech_rechnung.jrxml |
| P_SQLEXEC | Aufruf von Datenbank Querys aus dem Report heraus<br>Muster für Variable siehe fert_fehlteile.jrxml, Variable: LosEndetermin | rech_rechnung.jrxml |
| P_MODUL | interner Name des Moduls zur automatischen Findung der Subreports.<br>Siehe dazu auch com.lp.util.HelperReport.getSubreportPath | rech_rechnung.jrxml |
| P_LOGO_IMAGE | Logo aus dem Verzeichnis allgemein, also allgemein/logo.png | rech_rechnung.jrxml |
| P_LOGO_SUBREPORT | Pfad auf das Verzeichnis mit dem aktuell gültigen Firmenlogo mit allen Sprach und Mandanten Unterverzeichnissen | rech_rechnung.jrxml |
| P_MITLOGO | Sollte die Brief-Papier Information (Logo) gedruckt werden oder nicht | rech_rechnung.jrxml |
| REPORT_DIRECTORY | Pfad auf das Verzeichnis des aktuellen Reports, um z.B. zu Subreports zu finden | rech_rechnung.jrxml |
| REPORT_ROOT_DIRECTORY | Pfad auf das Report-Root-Verzeichnis | rech_rechnung.jrxml |

### Schriften
In Verbindung mit den Forderungen aus der eRechnung und damit aus Zugferd ergibt sich, dass Zugferd PDF Dateien nach dem Standard PDF/A-3 verwendet werden müssen.<br>
In PDF/A-3 ist gefordert, dass unter anderem nur freie Schriften verwendet werden dürfen und dass keine transparente Bilder verwendet werden dürfen.<br>
Zusätzlich wird ein Farbschema gefordert welches von **Kieselstein ERP** immer auf sRGB2014.icc gesetzt wird.

In diesem Zusammenhang ergibt sich, dass als Schrift nur mehr die <u>**Libration Sans**</u> verwendet werden darf. Diese muss auf deinem Kieselstein ERP Server installiert sein und auch auf deinen Clients. Für die Bearbeitung der Reports, muss diese Schrift sowohl in iReport als auch in JasperStudio verwendet werden.

Bitte beachte, dass für einen Zugferd kompatiblen Rechnungsversand nur freie Schriften verwendet werden dürfen. Also keine gekauften Schriften, die dann ins PDF eingebettet werden müssten und der Empfänger der PDF eine Lizenzverletzung begehen würde.

## Arbeiten mit iReport
Es darf ausschließlich iReport 5.5.0 verwendet werden. Alle anderen Versionen haben andere Bugs, die sich teilweise erst nach einiger Zeit zeigen.<br>
Die Vorteile von iReport sind seine Geschwindigkeit und seine Übersichtlichkeit und dass man mit Subreports vernünftig arbeiten kann. Der wesentlichste Nachteil ist, dass iReport nur Java 7 kann und dass neuere Funktionen des Kieselstein-Clients nur mehr unter Java 8 zur Verfügung stehen.

[Einrichten von iReport][siehe]( {{<relref "/docs/installation/10_reportgenerator/ireport/" >}} )

## Arbeiten mit Jasper Studio
das liegt leider daran, dass der iReport nur Java 7 kann und der aktuelle Kieselstein Client unter Java 8 läuft.<br>
Daher musste (aktuell ist das ein leider) auf den Nachfolger Jasperstudio 6.20.5 gewechselt werden (https://sourceforge.net/projects/jasperstudio/files/)

**ACHTUNG:** Jasperstudio 6.20.6 hat andere Property Definitionen (kann mit den Properties am Root nicht umgehen und hat weitere Bugs). Aktuell verwenden wir 6.20.5

### Welche Reports müssen mit Jasper Studio bearbeitet werden
und welche können noch im iReport 5.5.0 bearbeitet werden



## Arbeiten mit Formularen im iReport / JasperStudio
Report Editor
=============

In **Kieselstein ERP** wird der iReport für das Editieren der mitgelieferten Reports verwendet.
Bitte achten Sie darauf die exakt richtige Version des iReports für die Bearbeitung der Formulare zu verwenden.

Nachfolgend Hinweise zu der Arbeitsweise und den Eigenheiten von iReport welche bei der Gestaltung der Reports beachtet werden sollten.

**Hinweis:** Für das Arbeiten mit iReport sollte mindestens eine Zwei-Tasten Maus zur Verfügung stehen. Eine Bearbeitung der Druckformulare mit nur eine Maustaste (MAC) ist nicht möglich.

**Achtung:** Wenn Sie Druckformulare selbst bearbeiten wollen, müssen Sie diese als Anwenderreport ablegen, da ansonsten die bearbeiteten Reports bei einem Update der **Kieselstein ERP** Version gelöscht werden!!<br>
Die Hierarchie der Verzeichnisstruktur können Sie unter "[System -> Druckformulare]( {{<relref "/docs/stammdaten/system/druckformulare" >}} )" entnehmen.

## Grundsätzliche Vorgangsweise beim Bearbeiten eines Reports

1. Wenn nicht vorhanden, ein Verzeichnis für Anwenderreport anlegen, und Originalreports hineinkopieren. Bitte nur die tatsächlich benötigten Formulare einkopieren (und nicht einfach alle)
2. Bei bereits vorhandenem Anwenderreport: Backup des Anwenderreport-Verzeichnis erstellen.
3. Textfelder (Felder oder Parameter) im iReport anpassen.
4. Änderung speichern und mit "Build - Compilieren". Damit werden ihre Änderungen im laufenden System übernommen.
5. Änderung mit **Kieselstein ERP** Client ansehen (den entsprechenden Report Druck aufrufen).

Feldnamen der Reports

Starten sie das Programm iReport. Gehen Sie auf "Datei -> Öffnen" und laden Sie den entsprechenden Report.

Unter "Ansicht -> Report Felder" und "Ansicht -> Report Parameter" finden Sie die Feldnamen und die Parameter Namen des Report.

"Felder" werden nur im Detailabschnitt des Reports verwendet. Für jede z.B Rechnungsposition wird eine Zeile im Detailabschnitt des Reports gedruckt. 
Zugriff auf ein Feld mit dem Namen Gesamtpreis ist über $F{Gesamtpreis} möglich.

"Parameter" sind nur einmal im ganzen Report vorhanden.
Zugriff auf einen Parameter mit dem Namen P_KOPFTEXT ist über $P{P_KOPFTEXT} möglich.

Die Namen sind sprechend gewählt z.B. im Anwenderreport der Rechnung unter:
im Dateiverzeichnis Ihres **Kieselstein ERP**-Server (...) report/rechnung/anwender/rech_rechnung.jrxml finden Sie den Parameter P_KOPFTEXT, dieser enthält den Kopftext der Rechnung.

Wollen Sie z.B. P_KOPFTEXT ändern, gehen Sie im Report auf das entsprechende Feld, klicken mit der rechten Maustaste darauf und wählen "Eigenschaften".

Unter "Textfeld -> Ausdruck für Textfeld" sehen Sie den Wert des Feldes.
Wenn Sie unten rechts auf "Expression Editor öffnen" klicken, können Sie leicht verschiedene Felder oder Parameter auswählen und mit "Anwenden" übernehmen.

Unter "Allgemein -> Drucken wenn" wird angegeben, wann der Inhalt des Feldes angedruckt werden soll. 
Z.B. new Boolean($F{Positionsart}.equals("Stuecklistenpos"))
gibt an, dass das Feld nur gedruckt werden soll, wenn der Wert des Feldes Positionsart gleich "Stuecklistenpos" ist.

In Textfeld und Allgemein können Sie einige Konstrukte der Java Programmiersprache verwenden, die Datentypen der Felder und Parameter entsprechen Java Datentypen, weitere Informationen dazu finden Sie in der Java Dokumentation.

Wenn Sie Änderungen durchgeführt haben, gehen Sie auf "Build -> Compilieren" und ihre Änderungen werden im laufenden System übernommen.

Bevor Sie Änderungen machen, empfehlen wir ein Backup des Reportverzeichnisses zu erstellen, dadurch können fehlerhafte Änderungen zurückgenommen werden.

**Weiterführende Dokumentation:**
Kostenpflichtige iReport Dokumentation zu beziehen unter: <http://www.jasperforge.org/sf/wiki/do/viewPage/projects.ireport/wiki/HomePage> 
iReport Hilfen und Dokumentation sind über Suchmaschine im Internet auffindbar.

Dies ist nur eine Kurzanleitung wie iReport mit **Kieselstein ERP** Drucken eingesetzt werden kann und stellt keine ausführlich iReport Anleitung dar.

<a name="Breite der Barcodes"></a>

## Breite der Barcodes:
Die Größe der gedruckten Barcodes richtet sich nach der Größe des definierten Feldes. Der Barcode wird so gedruckt, dass er sowohl von der Breite her als auch von der Höhe her in die gedachte Umrahmung des Feldes passt. Zugleich wird das für den Barcode erforderliche Strich/Lücken Verhältnis beibehalten.

Anmerkung: Die Angabe erfolgt in 1/72" (Punkt bzw. Dezidot) Für die Umrechnung von Punkten auf mm verwenden Sie als Näherung bitte einen Multiplikator von 0,353. So ergeben 220 Punkte eine Breite von 77,66 mm.

In der Praxis hat sich bewährt, eine Höhe von 10 mm (28 Punkte) zu verwenden und die Breite ausreichend breit zu definieren.

(100/72 = Inch * 25,4 = mm = 1/72*25,4 = 0,35277)

Bitte bedenken Sie, dass ein gut lesbarer Barcode zu anderen Strichen und Kanten einen ausreichenden Abstand haben muss. Als Mindestabstand ist die Breite zweier Zeichen zu empfehlen. 

Bitte bedenken Sie auch, dass Barcodescanner üblicherweise eine maximale Erfassungsbreite von 70 mm haben. D.h. mit einem Rand von wenigen Zeichen darf ein Barcode maximal 220 Punkte breit sein.

<a name="Barcodetype"></a>

## Definition der Barcodetype

Die Definition der Barcodetype wird über den Formulareditor vorgenommen.

Üblicherweise wird der Code 128 verwendet. Bitte beachten Sie dazu auch [Drucken von Barcodes]( {{<relref "/docs/installation/11_barcode" >}} ).

## Welche Parameter können wo verwendet werden.

Alle Parameter sind in den vordefinierten, von uns gepflegten Reports enthalten. Sollten Sie in Ihrem eigenen (Anwender) Report Parameter vermissen, so öffnen Sie bitte den Originalreport, gehen Sie auf Ansicht, Reportparameter. Hier sehen Sie nun alle definierten Felder, Variablen und Parameter. Sie können den gewünschten Parameter nun mit der rechten Maustaste kopieren, in Ihren eigenen Report wechseln und dort mit rechter Maustaste und Einfügen in Ihre Reportparameterliste aufnehmen.

ACHTUNG: Die Parameter werden nur innerhalb der gleichen Reports unterstützt. Das Einfügen ungültiger Parameter in einen Report kann zum Abbruch des Programms führen.

### Drucken des Pageheaders aus dem Summary heraus, wenn im Summary ein Seitenumbruch ist.

Dies ist im Konzept des JasperReports nicht direkt vorgesehen. Als Workaround gehen Sie bitte wie folgt vor:

Definieren Sie eine neue Gruppe mit Namen Summary und als äußerste Gruppe definieren. D.h. sie muss in der Ansicht der Reportgruppen als erstes stehen. Die Summary-Gruppe mit Header Höhe 0 und Footer Höhe 78 anlegen.
Die Felder aus dem Original-Summary in den Footer der (neuen) Summary-Gruppe (=SummaryFooter) verschieben.

Das Original-Summary auf Höhe 0 setzen.

Anmerkung: Sind in den Detailpositionen Felder mit Höhe 0 enthalten, werden sie beim Einfügen der neuen Gruppe nicht mitverschoben. Diese müssen manuell an die richtige Position gebracht werden, also an die richtige Position in der richtigen Gruppe per Hand nachziehen. Beispiel: $F{Lerrzeile} im Detail des Rechnungsreports.

Dringende Empfehlung:

Bilder, die auf einem Report ausgedruckt werden sollten, dürfen im Dateinamen keine Leerzeichen enthalten. Der Dateiname sollte nur aus Kleinbuchstaben und _ bestehen. Er darf nur mit Buchstaben beginnen, im weiteren Namen dürfen Ziffern vorkommen, aber nicht an erster Stelle.

Definition der Abmessungen des Reports:

Die Abmessungen eines Reports / eines Formulares, z.B. einer Etikette können im Reportgenerator unter Ansicht, Reporteigenschaften definiert werden. Definieren Sie hier Abmessungen, Ausrichtungen und Ränder.
**WICHTIG:** Es hat sich bewährt, den Drucker immer im Porträt bzw. Hochformat zu definieren, auch wenn ein Formular verwendet wird, welches breiter als hoch ist. Um dies für die Reportvorlage richtig definieren zu können, müssen Sie die Höhe etwas höher als die Breite angeben. Die eigentliche Höhe des Etikettes definieren Sie dann mit der Höhe des Druckbereiches im jeweiligen Druck-Band (Detail, bzw. Title). Siehe dazu auch [Etikettendruck]( {{<relref "/docs/stammdaten/system/etikettendruck" >}} ).

Wenn Sie die Abmessungen des Reports verringern, so achten Sie bitte unbedingt darauf, dass auch die Höhen der einzelnen Elemente der Dokumentenstruktur (pageHeader, Detail usw.) angepasst werden müssen. Sind diese zu groß, erhalten Sie Fehlermeldungen wie:

"1. The detail section, the page and column headers and footers and the margins do not fit the page height."

Gehen Sie in diesem Falle in die verwendeten Elemente (Bands) und klicken Sie in die Band height. Geben Sie hier einen Wert ein der passt (mm -> DeziDots = mm/0,353) oder höher ist. Beim Verlassen des Feldes wird dieser Wert dann automatisch auf die richtige Höhe gesetzt.

Der Druck des Lieferscheins ist nach unten verschoben

Wenn man ein Feld mit einer bestimmten Höhe aus einem Report löscht, dann bleibt der dafür reservierte Platz bestehen, da dieser über die Bandbreite jenes Bandes bestimmt wird, in dem sich das Feld befindet. Um also den entstehenden Leerraum zu entfernen, der durch das Löschen des Feldes entsteht, muss man die Bandbreite entsprechend anpassen.

Beispiel: Es wird ein Feld "P_BEZEICHNUNG" mit Höhe 24 Pixel aus dem Band "Title" entfernt. In der Folge muss die Bandbreite des Bandes "Title" ebenfalls um 24 Pixel verringert werden.

Seit meiner letzten Änderungen werden die Texte mit Style Informationen gedruckt.

Wird in Ihrem Report ähnliches wie ![](Report_Style.gif) angedruckt, so muss für das jeweilige Textfeld unter Font, Is styled text angehackt sein, damit die Formatierungsinformationen aus den Kommentardateien, aus den Textdateien usw. mit angedruckt werden.

![](report_Einstellung.gif)

Datenexport aus der Druckvorschau

Damit insbesondere Listen in anderen Programmen weiterverarbeitet werden können, wird dringend empfohlen die Spalten und Zeilen auszurichten. Nur so wird erreicht, dass auch in den exportierten Daten die Spalten untereinanderstehen. Es reicht **NICHT** dies optisch auszurichten. Es muss dies mit den Jasper Werkzeugen, gleiche Breite **UND** links bzw. rechtsbündig ausgerichtet werden. Auch die Höhen müssen gleich und ausgerichtet sein.

Für den eigentlichen Export verwenden Sie am besten den CSV Export.

Um die Datei in Excel zu übernehmen, benennen Sie nun die Erweiterung der Datei bitte auf .TXT um. Nun mit Excel öffnen, Getrennte Breite, Trennzeichen Komma, Texterkennung "", markieren Sie nun alle Spalten und kennzeichnen Sie diese als Text und nun Fertigstellen.

So haben Sie die Daten optimiert importiert.

Drucken von mehrspaltigen Reports z.B. für Etiketten

Mit iReport können auch mehrspaltige Reports definiert werden, so wie sie z.B. für Laseretiketten für Laserdrucker (Zweckform, Herma, ...) benötigt werden. Definieren Sie dazu die Eigenschaften des Reports unter Bearbeiten, Reporteigenschaften und klicken Sie auf den Reiter Spalten. ![](ireport-Spalten1.gif) Geben Sie hier die gewünschte Anzahl der Spalten ein und definieren Sie auch den benötigten Zwischenraum.

Im eigentlichen Report finden Sie nun die angegebenen Spalten angezeigt. Definieren Sie nun die Inhalte der linken Spalte.

![](ireport-Spalten2.jpg)

**Wichtiger Hinweis:**

Der Druck von Etiketten auf A4 Vorlagen ist nur für den Ausdruck von einmaligen "Listen" zu empfehlen. Gerade für die Schritte, bei denen einzelne Etiketten benötigt werden, z.B. Wareneingang, werden Drucker benötigt, welche diese einzelnen Etiketten auch ausdrucken können. Das mehrmalige Einlegen von A4-Etiketten hat schon so manchen Laserdrucker ruiniert. Auch diese Formulare sind nur für den einmaligen Durchlauf durch den Laserdrucker gedacht. Wir hatten immer wieder Fälle in denen einzelne Etiketten sich vom Trägermaterial gelöst haben und auf der Trommel des Druckers geklebt sind.

Praktische Kommandos, Bedingungen im iReport

Nachfolgend eine lose Sammlung praktischer Kommandos für den iReport.

1.  Nur Drucken wenn String Leer ist:<br>
    new Boolean($F{F_POSITION}!=null&&$F{F_POSITION}.trim().length()>0)<br>
    F_POSITION ist hier der String

2.  new Boolean($F{F_POSITION}!=null&&$F{F_POSITION}.trim().length()>0 && $F{F_POSITIONSART}.equals("Ident "))

Beachten Sie bitte, dass auch jeder Abschnitt seine Bedingung hat. So sind die Bedingungen für den PageHeader üblicherweise in diesem abgebildet (<$V{IS_CURRENT_PAGE_NOT_ONE}>) und nicht bei jedem einzelnen Feld.

Funktionen, die von **Kieselstein ERP** zusätzlich für die Reportgestaltung im iReport verwendet werden können

Nachfolgend eine Sammlung ergänzender Funktionen für die Verwendung im iReport. Voraussetzung dafür ist, dass das lpclientpc.jar in den Classpath des iReport (siehe Extras, Optionen, Classpath) eingebunden ist.

Alle Funktionen sind in der Klasse com.lp.util.HelperReport gesammelt. Der Aufruf aus iReport entspricht immer dem Muster:

com.lp.util.HelperReport.FUNKTION

-   boolean pruefeObCode39Konform(String sString)
    Sind in dem String nur Code39 druckbare Zeichen enthalten

-   String wandleUmNachCode39(String input)
    Ersetzt Umlaute mit deren Entsprechung ohne Umlaut

-   Double time2Double(java.sql.Time time)

-   String ersetzeUmlaute(String input)
    Wandelt Ö auf Oe usw.

-   String wandleUmNachCode128(String input)

-   String ersetzeUmlauteUndSchneideAb(String input,int maxStellen)

-   String getWochentag(java.util.Locale locale, java.sql.Timestamp tDatum)
    Liefert den Wochentagsnamen in der jeweiligen Sprache

-   String ganzzahligerBetragInWorten(Integer betrag)

-   Integer getCalendarOfTimestamp(java.sql.Timestamp tTimestamp, Locale locale)

-   String getMonatVonJahrUndWoche(Integer iJahr, Integer iWoche, Locale locale)
    Liefert den Monatsnamen eines Jahres und einer Kalenderwoche

-   Integer getCalendarWeekOfDate(Date date)

-   Integer getCalendarWeekOfDate(String sDate, Locale locale)

-   Boolean pruefeEndsumme(BigDecimal bdReportNettoValue, BigDecimal bdHvValue, Double dAbweichung, String listeMwstsaetze, Locale reportLocale)

-   String laenderartZweierLaender(String lkzKunde, String lkzBasis, String uidNummer, java.sql.Timestamp tsEUMitglied)
    Ermittelt die Länderart des Kunden in Bezug auf die Basis (= Mandant)

- String entferneStyleInformation(String)
Entfernt die in den Texteingaben enthaltenen Styleinformationen wis bold usw.

-   int berechneModulo10(String nummer)
    'nummer' darf nur Ziffern zwischen 0 und 9 enthalten!

-   String berechneModulo10Str(String nummer)

-   Time double2Time(Number zahl)

-   Konvertierung eines Strings in einen anderen Datentyp, ohne Fehlermeldung (Exception):
    BigDecimal toBigDecimal(String bigDecimal) ; // wandelt nach BigDecimal, in Locale "deAT"
    BigDecimal toBigDecimal(String bigDecimal, Locale locale) ; // wandelt nach BigDecimal in angegebener Locale

    BigInteger toBigInteger(String bigInteger) ; // wandelt nach BigInteger mit Radix 10 (Dezimalzahlen)
    BigInteger toBigInteger(String bigInteger, int radix) ; wandelt nach BigInteger mit angegebenen Radix (oktal, hex, ....)

    Integer toInteger(String integer) ; // wandelt nach Integer

    All diesen Funktionen gemeinsam: Bei einem fehlerhaften Format, oder auch null als Parameter wird null anstatt einer Exception zurückgeliefert. Dies muss im Report entsprechend ausgewertet werden.
    Anwendungsbeispiele:
    HelperReport.toBigDecimal("25'100,12", new Locale("de", "CH")) ; // deCH
    HelperReport.toBigDecimal("25.100,12") ; // deAT
    HelperReport.toBigDecimal("-25'987.65") ;

#### Bei der Kompilierung kommt eine ClassNotFoundException Fehlermeldung
Diese Meldung bedeutet, dass der iReport Compiler eine Java Klasse nicht findet. Um die Formulare von **Kieselstein ERP** mit der gewünschten Funktionalität auszustatten, werden viele Klassen direkt von **Kieselstein ERP** zur Verfügung gestellt. Dafür muss der Pfad auf den **Kieselstein ERP** Client mit in den ClassPath des iReport eingebunden werden.
Sie finden dies unter Extras, Optionen, Classpath. Geben Sie hier den Pfad auf den aktuellen **Kieselstein ERP** Client an.
![](iRepot_Einstellungen.png)

Bitte beachten Sie, dass in der Regel der **Kieselstein ERP** Client unter ... zu finden ist.

#### Fehlermeldung unsupported version
Kommt beim Kompilieren eine Fehlermeldung ähnlich
java.lang.UnsupportedClassVersionError: com/lp/util/report/PositionRpt : Unsupported major.minor version 51.0
so bedeutet dies, dass iReport nicht unter Java 7 ausgeführt wird.

Beachten Sie bitte in jedem Falle, dass iReport nur unter Java 7 genutzt werden kann und dass Sie dafür den Client für Java 7 benötigen.
Die Clients für Wildfly sind für Java 8 kompiliert. Gegebenenfalls bitten Sie Ihren **Kieselstein ERP** Betreuer Ihnen einen passenden Client zur Verfügung zu stellen.
Gegebenenfalls siehe auch im iReport, Hilfe, Info.

## Wichtiger Hinweis:
in den Reports können nur die "fields" und "parameter" verwendet werden, welche vom **Kieselstein ERP**-Server auch angeliefert werden. D.h. wenn Felder bzw. Parameter aus anderen Reports kopiert werden, so haben diese normalerweise nicht die gewünschte Wirkung. Hintergrund: Diese Felder werden über sogenannte Call-Backs vom Application-Server befüllt und wenn diese nicht vorgesehen sind, sind sie eben NULL.

Ausnahmen:
Es gibt Parameter, die in allen Reports zur Verfügung stehen, aber nicht in allen Reports ausgeführt sind. Diese sind:
- Mandanten-obj
- psql-executer
- logo....
- report_directory
- report_root_directory
Diese Parameter können jederzeit in einen (Haupt-)Report von einem anderen Report einkopiert werden. Für die Verwendung in Subreports müssen diese selbstverständlich durchgereicht werden.

----------------
#### Was sind Styles?
In iReport können Styles folgendermaßen angelegt werden: Style Bibliothek -> Neuer Style (Symbol). Daraufhin öffnet sich ein Popup, in welchem der neue Style zu definieren ist. Bei *Style Name* ist ein Name für den aktuellen Style anzugeben, damit man beim Anlegen mehrerer Styles den Überblick behalten kann. Der *Standardstyle* beinhaltet alle Formatierungen, die ein Feld standardmäßig haben soll, z.B. ist das der häufig schwarze Vordergrund auf weißem Hintergrund. Mit dem Button 'Hinzufügen' kann man eine neue Bedingung erstellen, an welche andere Formatierungen geknüpft sind. Z.B. legt man als Bedingung *new Boolean ($F{Projektbezeichnung}.contains("Fehler"))* fest, was bedeutet, dass wenn in dem Feld 'Projektbezeichnung' das Wort 'Fehler' vorkommt, der definierte Style angewendet wird.

![Beispiel für einen Style](Report_Style.jpg)

Anwendung des definierten Styles im Report:

Man wählt in ein oder mehreren Reportfeld-Eigenschaften im Register 'Allgemein' unter 'Style' den zuvor definierten Style aus.

#### Worauf ist bei Verwendung von Styles speziell zu achten?

A: Damit die definierten Styles auch angezeigt werden, gibt es folgende Punkte zu beachten:

-   Beim Style in der Style Bibliothek:

    -   Es sollten unter 'Allgemein' Vorder-, Hintergrundfarbe und Modus definiert sein (Modus nicht undurchsichtig, Standard wäre am besten)

    -   Beim Standardstyle soll ein aussagekräftiger Name definiert werden

-   Beim Report-Feld, auf das der Style angewendet wird (Feld-Eigenschaften):

    -   Im Register 'Allgemein' ist beim Style der anzuwendende Style auszuwählen

    -   Im Register 'Allgemein' ist das Feld auf NICHT transparent zu setzen

    -   Im Register 'Alle' sind die Vorder- und die Hintergrundfarbe zu löschen (Bei Vorder- und Hintergrund rechts daneben das Kreis-Symbol anklicken)

![](Report_Style_Feldeigenschaften.jpg)

#### Meine im iReport definierten Feldgrößen passen nicht zu den Ausdrucken
Vermutlich sind die für den **Kieselstein ERP** Server verfügbaren Fonts (Schriften) nicht identisch mit den von Ihnen beim Design der Reports verwendeten Schriftvorlagen. Gerade bei Linux und MAC-Servern müssen diese aus Lizenzgründen extra installiert werden. Siehe dazu bitte auch auf Ihrer Installations-CD unter Tools/Linux/Fonts.

#### Bei den Adressetiketten werden nur leere Etiketten gedruckt.
Wie bereits an anderer Stelle angeführt, ist das Ausdrucken von Etiketten eine eigene Wissenschaft, die manchmal unterschätzt wird. Die Thematik ist hier vor allem, dass Druckertreiber nicht immer 100%ig richtig implementiert sind und auch Aufgrund der vielen zu treffenden Einstellungen manchmal etwas vergessen wird.

Hier nun eine lose ergänzte Sammlung von Punkten / Empfehlungen, die für das Etikettendrucken erforderlich sind, die getestet werden sollten, wenn der Etikettendruck nicht wie gewünscht funktioniert:

-   Stellen Sie sicher, dass Sie auf das Etikett / den Etikettendrucker eine Testseite ausdrucken können.
-   Versuchen Sie das Etikett aus einem Textverarbeitungsprogramm zu drucken.
-   Spannen Sie ein großes Blatt Papier ein, wenn möglich und drucken Sie auf dieses.
-   Etiketten Orientierung: Etiketten werden in aller Regel im iReport so definiert, dass sie für den Designer lesbar sind. Oft ist hier die Etikettenbreite größer als die Etikettenhöhe. Dadurch wird im iReport bei Änderungen der Abmessungen die Orientierung automatisch auf Querformat umgeschaltet, was von der Druckrichtung her falsch ist. Um dies zu korrigieren, muss in das jrxml File des Reports eingegriffen werden. Ändern Sie hier bei Orientation den Wert von Landscape auf Portrait ab. Speichern Sie die Datei, gehen Sie wieder in den iReport und kompilieren sie die Datei erneut. Nun kann geduckt werden.
-   Ein weißes Blatt, eine leere Etikette wird gedruckt. Um festzustellen, woran das Problem liegen könnte, vergrößern Sie das Etikettenformular entsprechend und stellen Sie die Nutzdaten einfach in die Mitte. In aller Regel erhält man nun Informationen, wo denn eigentlich hingedruckt wird.<br>
Oft ist es auch so, dass die Orientation (siehe oben), der Auslöser für dieses Verhalten ist.

-   Bei Verwendung von dedizierten Etikettendruckern, stellen Sie unbedingt in den Druckereigenschaften des Etikettendruckers das eingelegte Etikett ein.

-   Bei Verwendung von CUPS Treibern achten Sie unbedingt darauf, dass die Etikettendefinition im iReport in das Etikett passt. Schon geringe Abweichungen (1 Pixel) führen dazu, dass das Etikett gedreht oder gar nicht gedruckt wird.

#### Ausdrucken aller Bearbeiter Daten
Gerade in Angeboten, aber auch für eine gute Kommunikation mit Ihren Partnern (Kunden und Lieferanten), ist es erwünscht, nicht nur den Namen des Ansprechpartners, sondern auch seine ausführlichen Kontaktdaten mit anzudrucken. Dafür steht in den öffentlichen Reportvorlagen eine sogenannte Objectvariable zur Verfügung.

Diese lautet in allen Reports: <P_BEARBEITER>

Um nun z.B. die EMail-Adresse des Bearbeiters anzudrucken, muss folgendes Konstrukt unter Textfeld eingegeben werden: <$P{P_BEARBEITER}.getSEmail()>

**Wichtig:** Damit diese Erweiterungen im iReport kompiliert werden können, muss der Klassenpfad auf das kieselstein-ui-swing.jar / lpclientpc.jar gesetzt werden, welches du üblicherweise unter c:\kieselstein\client\lib\kieselstein-ui-swing.jar findest

Es stehen folgende Werte zur Verfügung:

| Typ | Methode |
| --- |  --- |
| java.lang.String | getSEmail() |
| java.lang.String | getSMobil() |
| java.lang.String | getSNachname() |
| java.lang.String | getSTelefonDWFirma() |
| java.lang.String | getSTelefonFirma() |
| java.lang.String | getSTitel() |
| java.lang.String | getSVorname() |

#### Ausdrucken aller Positionsinhalte als einzelne Felder
Für manche offizielle Formularvorlagen ist es erwünscht, dass, insbesondere zu Positionierungszwecken, die Felder z.B. der Artikelbezeichnungen als jeweils einzelnes Datenfeld zur Verfügung stehen. Um diese Forderung zu erfüllen, wird an einige offizielle Reportvorlagen die Objectvariable <F_POSITIONSOBJEKT> übergeben. Um nun z.B. nur die Artikelnummer mithilfe dieser Objectvariabeln zu drucken, muss folgendes Konstrukt unter Textfeld eingegeben werden: $F{F_POSITIONSOBJEKT}.getSIdent()

Es stehen in den unten angeführten Vorlagen folgende Variablen zur Verfügung.

| Typ | Aufruf | AF | BS | AG | AB | LS | RE | GS |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |
| String | getSIdent() |  |  |  |  |  |  |  |
| String | getSBezeichnung() |  |  |  |  |  |  |  |
| String | getSZusatzbezeichnung() |  |  |  |  |  |  |  |
| String | getSZusatzbezeichnung2() |  |  |  |  |  |  |  |
| String | getSKurzbezeichnung() |  |  |  |  |  |  |  |
| String | getSPositionsartCNr() |  |  |  |  |  |  |  |
| String | getSText() |  |  |  |  |  |  |  |
| BigDecimal | getBdUmrechnungsfaktor() |  |  |  |  |  |  |  |
| String | getSEinheitBestellung() |  |  |  |  |  |  |  |
| String | getSArtikelgruppe() |  |  |  |  |  |  |  |
| String | getSArtikelklasse() |  |  |  |  |  |  |  |
| String | getSArtikelreferenznr() |  |  |  |  |  |  |  |
| String | getSArtikelmaterial() |  |  |  |  |  |  |  |
| Float | getFArtikelmaterialgewicht() |  |  |  |  |  |  |  |
| String | getSVerkaufsEANNr() |  |  |  |  |  |  |  |
| String | getSWarenverkehrsnr() |  |  |  |  |  |  |  |
| String | getSUrsprungsland() |  |  |  |  |  |  |  |
| Float | getFArtikelgewicht() |  |  |  |  |  |  |  |

Bitte beachten Sie auch die unter Ergebnisklasse Textfeld erforderliche Typ-Definition.

## eine andere Sortierung als ursprünglich gewünscht
Um dies zu erreichen, fügt man nach der Definition der FieldNames die sogenannten sortField hinzu. Damit werden die Reports in der angegebenen Reihenfolge sortiert.
z.B.: \<sortField name="Lagerort"/><br>
ACHTUNG: Bei Änderungen direkt im XML, welche vom Designer nicht unterstützt werden, direkt aus dem XML heraus speichern und dann erst in den Designer zurückwechseln. XML Kommentar wird NICHT unterstützt, d.h. wieder herausgelöscht.

Das kann man natürlich auch für die Sortierung aus dem Sort-Field nutzen.

