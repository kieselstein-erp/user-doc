---
categories: ["Formulare"]
tags: ["Formulare bearbeiten", "Reportgenerator"]
title: "Formelsammlung"
linkTitle: "Formelsammlung"
date: 2023-01-31
weight: 500
description: >
  Eine Sammlung von nützlichen Formeln für den Reportgenerator
---

Die Beschreibung der Helper wurde aus allgemein\muster_qr_code.jrxml entnommen.

# iReport = Java 7

## Kalenderwoche aus Timestamp errechnen
und das immer für die vorige Woche, als vom Stichtag = Timestamp den Wochentag abzuziehen, um auf den Sonntag davor zu kommen
```
 com.lp.util.HelperReport.berechneKWJahr(
    new java.sql.Date(
    $P{P_STICHTAG}.getTime() - 
 $P{P_STICHTAG}.getDay()*1000*3600*24
        )
)
```
## SQL Abfragen für einen Return-Wert
- $P{P_SQLEXEC}.execute( SQL Query String)

## SQL Abfragen für mehrere Werte
- $P{P_SQLEXEC}.executes( SQL Query String)
[siehe]( {{<relref "/warenwirtschaft/stueckliste/hinterlegte_formeln/#object-sqlexecutes-im-script" >}} )

### Werte aus der Datenbank
- Bigint aus DB = Long im Jasper Report
- sum() aus der DB liefert immer BigDecimal

### Werte aus dem Report anders zusammenstellen und eigenständig sortieren
Manchmal will man einen Teil eines Reports anders sortieren, verschiedene Zwischensummen darstellen und ähnliches. Hier sei als Beispiel der Report ../eingangsrechnung/er_eingangsrechnung_alle.jrxml genannt. Auf diesem sieht man am Ende des Reports eine Zusammenfassung der verschiedenen Steuersätze. Dafür wird der Inhalt, den man an den Subreport übergeben will, in einer Array-List gesammelt (siehe Variable LISTE_UST_SAETZE bzw. LISTE_UST_SAETZE_ADD). Mit dieser Arraylist wird über einen Helper ein Sub-Report aufgerufen (im Summary er_eingangsrechnung_kontierung_summary). Das besondere an dem Helper ist, dass nach dem ersten Element sortiert wird. Dafür muss dieses ein String sein. Nicht zuletzt deshalb gibt es für diesen Helper zwei Ausführungen. 
- HelperReport.sortList wird diesem Helper im Sort ein Null-Wert übergeben, wird mit einer Fehlermeldung abgebrochen
- HelperReport.sortListNoNull dieser Helper akzeptiert auch null in der Sortierung

### Wo finde ich welches Feld
Oft steht man vor der Herausforderung, in welcher Tabelle welches Feld zu finden ist. Hat man nun eine Ahnung wie das Feld heißen könnte, kann man die Verwaltungstabellen des PostgresQL befragen. Also:<br>
select * from information_schema.columns where column_name like '%herst%';<br>
liefert z.B. eine Liste aller Tabellen die Spaltennamen mit herst beinhalten.

## Strings in Zahlen wandeln
- com.lp.util.HelperReport.toBigDecimal (String bigDecimal, Locale stringLocale) mit und ohne Local 
- com.lp.util.HelperReport.toInteger(String integer)

## Datum aus Date/Timestamp
    $V{LosEnde}.toLocaleString().substring(6,10)+"-"+
    $V{LosEnde}.toLocaleString().substring(3,5)+"-"+
    $V{LosEnde}.toLocaleString().substring(0,2)
    /* $V{LosEnde}.toLocaleString() liefert DD.MM.JJJJ */
**ACHTUNG:** Liefert das Datum in Abhängigkeit des Report-Locales. Also bei italienisch die italienische Schreibweise (8-nov-2023), welche für SQL Abfragen dann nicht verwendbar ist. Für SQL Abfragen daher z.B. (new SimpleDateFormat("yyyy-MM-dd", Locale.GERMAN)).format($V{Datum}) verwenden. [Siehe auch](https://stackoverflow.com/questions/4772425/change-date-format-in-a-java-string).

## Datum in String konvertieren
- (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)).format($P{P_ANGEBOTSGUELTIGKEIT})
  Locale.ENGLISH
- bei den Schreibweisen auf 24 Std 12 Std achten, für Kalenderwoche auch auf die Java Definitionen (siehe Erfolgsrechnung bzw. Link oben)

Mögliche Beispiele:
- (new SimpleDateFormat("yyyy-MM-dd", Locale.GERMAN)).format($V{DiesesMonat})+" 00:00:00"
- (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMAN)).format($F{Von})
- (new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)).format($V{V_HEUTE})	-- im in Deutsch lesbaren Format

## String in Datum konvertieren
- Wichtig Datum muss im Stringformat dd.MM.yyyy übergeben werden<br>
com.lp.util.Helper.parseString2Date($F{F_REALISIERUNGSTERMIN})

### Beginnzeitpunkt rechnen
Von Obigem bekommt man ein Date, welches mit .getTime() in ein Long verwandelt wird. Hier kann man nun ms (Millisekunden) als Long dazuzählen und mit new java.sql.Timestamp(long) in einen Timestamp verwandeln, mit dem man nun rechnen kann.<br>
So will man z.B. für den jeweiligen Tag wissen, wieviele Stunden der Mitarbeiter vor 6:00 gearbeitet hat. Also:
- z.B. in die Variable Datum, welche ein Date ist:
``` com.lp.util.Helper.parseString2Date(
(new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN)).format($F{Zeit}) )
```
- und dann in die Variable Beginn
```
new java.sql.Timestamp($V{Datum}.getTime()+(6*3600*1000))
```
- somit erhält man als long
$V{Beginn}.getTime() - $F{Kommt}.getTime()

#### Stunden dazu oder abziehen
Siehe z.B. rech_rechnung_abrechnung_detail_zeitraum.jrxml
- Variable bis
- new java.sql.Timestamp (($F{t_zeit}.getTime() + (new Double ($F{n_stunden}.doubleValue()*3600*1000)).longValue()))

Wichtig: Man rechnet mit dem Long und wenn man dann wieder eine Uhrzeit braucht, die Konvertierung auf Timestamp

### Tagesdifferenzen errechnen
Wenn man Tagesdifferenzen errechnen will, so hat sich folgende Herangehensweise bewährt:

    (($V{V_HEUTE}.getTime() - $F{Liefertermin}.getTime()) / 3600 / 24 / 1000) -> liefert die entsprechenden Tage
    V_HEUTE = Calendar.getInstance(Locale.GERMAN).getTime()

### Letzter des Monates
    com.lp.util.HelperReport.getLetztenTagDesMonats($V{V_HEUTE}, 0)
        Das ,0 ist der Monatsversatz, der dazugerechnet wird. Siehe z.B. auch fc_deltaliste_entwicklung.jrxml

### Timestamp to Date
Bedeutet, dass die Zeit beim Datum, das auch ein Timeobjekt ist, auf 00:00:00 gesetzt wird.<br>
new java.sql.Date($V{LosBeginn}.getTime())

### Kalenderobjekt
Siehe: com.lp.util.HelperReport.asKalender

### Dezimalstunden als hh:mm drucken

((String)(com.lp.util.HelperReport.konvertiereZeitDezimalInHHMMSS($V{V_SUMMEDAUER},2,false))) wobei die Variable ein BigDecimal sein muss (siehe auch PJ 19324). Wenn hinten true, dann auch Sekunden





## Zahlen in String konvertieren
- String.format("%05d", $F{F_ARBEITSGANG})<br>
nur für integer

## BigDecimal to String
String.format("%05.4f" , $V{Rabatt}.doubleValue()\*100).replace(",",".")

Je nachdem, was man damit machen muss, es kommt der String im default Locale, also ev. noch die Dezimaltrenner usw. bearbeiten.

## Time in Double konvertieren
Manchmal muss man aus der Datenbank eine Time, z.B. die Sollzeit eines Zeitmodelles, in ein Double konvertieren, um damit weiterrechnen zu können.
> $V{U_Sollzeit} == null ? 0.00 :
> ((new Double($V{U_Sollzeit}.getTime())).doubleValue() / 1000 / 3600 ) +1.0
>

## Gerade oder ungerade
### für ein int
> $V{KW_int}.intValue()\%2 == 0 ? ist eine gerade Zahl

### für ein double
> xxx.doubleValue()\%2 == 0 ? ist eine gerade Zahl

## Fremdsprachige Installationen
Bei fremdsprachigen Installationen, z.B. Konzernsprache in Englisch, müssen bereits die default Reports in der Mandantensprache, z.B. Englisch sein.<br>
D.h. unter z.B. report/auftrag/anwender/ müssen bereits die englischsprachigen Reports für den Auftrag sein. Sollte dann ein Kunde eine deutsche AB benötigen,
ist diese unter 001/de zu hinterlegen.

## Zugriff auf die Bezeichnung der intelligenten Zwischensumme
Da die intelligente Zwischensumme hierarchiefähig ist, ist der eigentliche Feldinhalt ein String[Array]. Das bedeutet, will man nun auf den Inhalt zugreifen, so muss das Array-Element angegeben werden. So erhält man z.B. mit $F{F_ZWSTEXTE}[0] eben die Bezeichnung der ersten Zeile des Textes der intelligenten Zwischensumme.

## Timestamp / Datum formatieren
von ist ein Timestamp
(new SimpleDateFormat("HH:mm", Locale.GERMAN)).format($F{Von}.getTime())

## jetzt als String
new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())

## zwei Timestamps nur das Datum vergleichen

https://tableplus.com/blog/2018/07/postgresql-how-to-extract-date-from-timestamp.html

To extract a date (yyyy-mm-dd) from a timestamp value
For example, you want to extract from '2018-07-25 10:30:30' to '2018-07-25'
1. Extract from a timestamp column:
Use date() function:
SELECT DATE(column_name) FROM table_name;
2. Extract date from a specific timestamp value:
Cast the timestamp to a date by adding ::date suffix:
SELECT '2018-07-25 10:30:30'::TIMESTAMP::DATE;
Or combine date() and substring() function:
SELECT DATE(SUBSTRING('2018-07-25 10:30:30' FROM 1 FOR 10));

## im Query mit Monaten rechnen
```(t_letztewartung+(i_wartungsintervall \* '1 month'::INTERVAL))```
<!-- siehe auch Artikel/anwender.p&s/ww_naechste_wartungen.jrxml -->

### eine weitere interessante Variante ist
```(start_date + (duration || ' month')::INTERVAL) < '2010-05-12'```<br>
siehe https://stackoverflow.com/questions/5909363/calculating-a-date-in-postgres-by-adding-months

### oder auch
https://www.postgresqltutorial.com/postgresql-string-functions/postgresql-to_char/
```
TO_CHAR(n_min, '999999.99') || ' - ' || TO_CHAR(n_max,'999999.99')
aus ...\stueckliste\stk_gesamtkalkulation_konfigurationswerte.jrxml 
```

### Von Bis Zeiten aufsummieren
```bash
select sum(EXTRACT(EPOCH FROM (t_bis - t_von))) from pers_maschinenzeitdaten where ...
```

## einen Subreport mit Tagen
> com.lp.util.HelperReport.getSubreportKalendertage(<br>
> $P{P_VON},<br>
> $P{P_BIS},<br>
> $P{P_MANDANT_OBJ}.getTheClientDto())<br>
>
Liefert einen Subreport mit Datum, Feiertag und Sollzeit (in Stunden) des Firmenzeitmodells<br>
Beispiel siehe: proj_projekt_journal_offene_gantt_zeitachse.jrxml

### Query, um eine Summe der jüngsten 10 Mengen zu erhalten
Der Trick liegt hier darin, dass man sich mit einem Select (dem inneren) die Werte holt, den man dann mit einem zweiten Select (dem äußeren) aufsummiert. Man könnte auch sagen, dass man damit einen zweistufigen Select macht. Also z.B. für die Aufträge:
```bash
select sum(n_menge) from (
select n_menge from auft_auftragposition
inner join auft_auftrag on auft_auftrag.i_id=auft_auftragposition.auftrag_i_id
where artikel_i_id in (select i_id from ww_artikel where c_nr like 'ABC%')
order by auft_auftrag.t_belegdatum desc
limit 10) as foo;
```

## Für diverse SQL Querys
Siehe https://www.postgresql.org/docs/current/functions-formatting.html alleine schon das SQL kann jede Menge

## X_TEXT direkt aus der Datenbank holen
Nachdem im Jasper üblicherweise mit Strings (und nicht mit String-Objekten) gearbeitet wird, aber in der Datenbank auch lange / große X_Texte abgelegt sind, müssen diese mittels CAST aus der Datenbank geholt werden. Z.B. für die Bemerkung aus einer Kostenstelle.
> $P{P_SQLEXEC}.execute(
 "select CAST(X_BEMERKUNG as VARCHAR(3000)) from LP_KOSTENSTELLE where C_NR = '10';"
 )
>

## Abfrage mit locale
Um das locale in den Querys verwenden zu können:<br>
{REPORT_LOCALE}.toString().replace("_","")

### z.B. Übersetzung der Bezeichnung der Mengeneinheiten
> $P{P_SQLEXEC}.execute(
 "select c_bez from lp_einheitspr where einheit_c_nr='"+$P{P_EINHEIT}+"' and locale_c_nr='"+$P{REPORT_LOCALE}.toString().replace("_","")+"';"
 )
>

## Datenreihen mit Datum generieren
Für PostgresQL (MS-SQL ist anderes und wird, da nicht OpenSource, von **Kieselstein ERP** nicht verwendet).<br>
### Tage zwischen Datumsbereich:
> SELECT day::date<br>
> FROM   generate_series(timestamp '2023-03-01', timestamp '2023-03-31', interval  '1 day') day;<br>
> Ergebnis:
> > "2023-03-01"<br>
> > "2023-03-02"<br>
> > "2023-03-03"<br>
> > ...<br>
"2023-03-28"<br>
"2023-03-29"<br>
"2023-03-30"<br>
"2023-03-31"<br>

### Stunden für Zeitbereich:
> SELECT day::timestamp<br>
FROM   generate_series(timestamp '2023-03-01 12:00', timestamp '2023-03-02 18:00', interval  '1 hour') day;
<br>
> "2023-03-01 18:00:00"<br>
"2023-03-01 19:00:00"<br>
...<br>
"2023-03-02 09:00:00"<br>
"2023-03-02 10:00:00"<br>

### 1. Tag im Monat:
> SELECT date '2023-03-01' + interval '1' month \* s.a AS date<br>
  FROM generate_series(0,3,1) AS s(a);
<br>
> Ergebnis:<br>
"2023-03-01 00:00:00"<br>
"2023-04-01 00:00:00"<br>
"2023-05-01 00:00:00"<br>

### Raufzählen
SELECT \* FROM generate_series(1,3);<br>
> Ergebnis:<br>
> 1<br>
> 2<br>
> 3 als integer

### Verfügbarkeiten laut Zeitmodell berechnen
Manchmal hat man das Problem, dass man die verfügbaren Stunden aus Zeitmodellen errechnen muss. Dies geht z.B. mit:
```bash
select sum(
(select l_verfuegbarkeit from pers_maschinenzmtagesart where tagesart_i_id=coalesce((select tagesart_i_id from pers_betriebskalender where mandant_c_nr='001' and t_datum = tage.day), extract(isodow from tage.day)+10)
	and maschinenzm_i_id=(
		select maschinenzm_i_id from pers_maschinemaschinenzm 
		where maschine_i_id=(select i_id from pers_maschine where c_identifikationsnr = '67' and mandant_c_nr='001') 
		and t_gueltigab <= tage.day 
		order by t_gueltigab desc limit 1 
	)
)
) / 1000 / 3600
from (
SELECT day::date 
FROM generate_series(timestamp '2024-08-12', timestamp '2024-08-18', interval '1 day') day) as tage;
```
Siehe z.B. pers_maschinenproduktivitaet.jrxml

## Konvertieren auf BigDecimal
Das Einfachste ist BigDecimal.valueOf(value) wobei value z.B. ein Double sein kann.

### gerundet auf Nachkommastellen
BigDecimal.valueOf(value).setScale(2, BigDecimal.ROUND_HALF_UP)

## Usecase ID wird nicht angedruckt
Macht man einen Druck der Auswahlliste (FLR Liste) muss rechts oben die UseCaseID angedruckt werden. ![](UseCaseID_andrucken.png)  <br>
Diese wird nicht angedruckt, wenn im ../report/allgemein auch der flrdruck.jasper vorhanden ist.
Zusätzlich darf es den flrdruck.jrxml ausschließlich **ein Mal** im allgemein geben und in keinem anderen Verzeichnis.

## Das führende Herr / Frau ersetzen
Manchmal will / muss man die förmlichen Anreden entfernen. Also aus Herr Mustermann nur Mustermann haben. Das macht man üblicherweise mit replace. Da es aber auch vorkommen kann, dass der Name des Ansprechpartners Herr enthält muss man das über .replaceAll("^Herr","") machen. D.h. es wird NUR das erste Herr durch den nachfolgenden (Leer-)String ersetzt. Ganz genau muss man dann auch noch auf den beginnenden String prüfen. Für die Ansprechpartner gehen wir hier davon aus, dass es immer eine entsprechende Anrede gibt.<br>
Da man damit auch oft noch das Frau entsprechend herausfiltern will, heißt das dann:
> .replaceAll("^Herr ","").trim().replaceAll("^Frau ","").trim()
>

## Subreport overflowed on a band that does not support overflow
Wenn das nicht der Title, die Page Header / Footer betrifft, dann ist der Report, der den Subreport aufruft auf Horizontal gestellt. Er muss auf Vertikal gestellt sein. Passiert gerne, wenn man Reports umkopiert, z.B. mit mehreren Columns und dann gehts am Anfang und auf einmal nicht mehr (weil dann der Overflow kommt). D.h. wenn die **Columns auf 1** (zurück-) gesetzt werden, dann **auf die Richtung achten**.

## Zu Datum Tage dazuzuzählen
> com.lp.util.Helper.addiereTageZuDatum(date Datum, int Tage)
>
> Beispiel
> > com.lp.util.Helper.addiereTageZuDatum(com.lp.util.Helper.parseString2Date($P{Liefertermin}) , -2)

### im Query zum Datum / Timestamp Tage dazuzählen
```
select (cast(fert_los.t_produktionsbeginn as date) + i_maschinenversatztage), 
	fert_los.t_produktionsbeginn, i_arbeitsgangnummer, i_unterarbeitsgang, i_maschinenversatztage, * from fert_lossollarbeitsplan
inner join fert_los on fert_los.i_id=fert_lossollarbeitsplan.los_i_id
where fert_los.c_nr='24/01622-993' and mandant_c_nr='001'
order by i_arbeitsgangnummer, i_unterarbeitsgang;
```

## Wochentage / Wochentagsnamen andrucken
- com.lp.util.HelperReport.getWochentag($P{REPORT_LOCALE}, <Timestamp>$F{Datum})
- com.lp.util.HelperReport.getWochentag($P{REPORT_LOCALE}, new java.sql.Timestamp($F{BeginnDatum}.getTime()) ... wenn das ein Date ist

## Daten anders sortieren
Üblicherweise werden die Daten des Reports anhand der gewählten Sortierung übergeben. Manchmal will man aber diese, z.B. in einer Reportvariante, anders sortieren. Um dies zu bewerkstelligen, kann **direkt im jrxml** das Sortierkriterium angegeben werden.<br>
Es muss dies zwischen field name und variable eingefügt werden.<br>
D.h. hier muss:
> <sortField name="field1"/>
eingetragen werden. Damit werden die Daten vom Jasper nach diesem Feld sortiert. Beispiel siehe z.B.: ..\report\bestellung\bes_sammelmahnung.jrxml
![](Sortierung_definieren.png)

### Sortierung des Materials des Fertigungsbegleitscheines
So wie oben beschrieben, einfach nur zwischen den Zeilen field_name und variable die Sortfields einfügen.

>	\<sortField name="F_ARBEITSGANG"/><br>
>	\<sortField name="F_UNTERARBEITSGANG"/><br>
>	\<sortField name="F_MATERIAL_LAGERORT"/><br>
>
Dies ist leider **nur im XML** ersichtlich.

### EMail Nachricht aus dem Report erzeugen
Mit folgendem Helper kann aus dem Report heraus eine Nachricht versandt werden. Z.B. wenn ein Wareneingang gebucht wurde und das Wareneingangsetikett gedruckt wird.
```
com.lp.util.HelperReport.emailSenden(
    "wareneingang@kieselstein-erp.org",
    "WE zu BS: "+$F{F_BESTELLNUMMER},
    "Es wurde ein Wareneingang von "+$F{F_IDENT}+" "+$F{F_BEZ}+" gebucht",
    $P{P_MANDANT_OBJ}.getTheClientDto())
```
- Empfänger
- Betrifft
- Nachricht
- Clientobjekt, um es versenden zu können.

### Links hinterlegen
Manchmal ist es praktisch, wenn hinter einem Text, einem Bild ein Hyperlink hinterlegt wird. Damit braucht der Anwender nur auf den Link zu klicken und schon hat er die benötigte Information. Dafür muss dies ein Text Field sein. Dann mit der rechten Maustaste auf das Field und Hyperlink auswählen.<br>
Dies nun auf<br>
![](Hyperlink_hinterlegen.png)<br>
Hyperlink type Reference stellen und in den Reference Reiter den gewünschten Webaufruf als String (mit umschließenden Hochkomma ") eintragen.<br>
Beispiel: artikel/ww_inventurstand.jrxml im Lastpagefooter.

#### Links als GoTo hinterlegen
Manchmal ist es auch praktisch aus dem Formular heraus einen Goto z.B. auf den Artikel oder einen Beleg zu machen. Dafür wird eine besondere Form des Hyperlinks verwendet. Idealerweise verwendest du die Artikelstatistik als Vorlage um dies in anderen Belegen einzubauen.<br>
![](Hyperlink_GoTo.png)<br>
Der besondere Trick liegt im Reiter Link parameters mit den Parameter namen 
- WhereToGoTo
- Key
In whereToGoTo kommt die Helperfunktion für den Aufruf des jeweiligen Modules rein<br>
in Key kommt die Belegnummer oder die I_ID des anzuzeigenden Datensatzes rein.

Im Reiter Tooltip kannst du einen entsprechenden String hinterlegen. Leider werden keine Zeilenumbrüche unterstützt.

#### Aktuell gibt es folgende Helper:
| Helper | Bedeutung |
| --- | --- |
|com.lp.util.GotoHelper.goto_RECHNUNG_AUSWAHL() | Springe auf die Rechnung |
|com.lp.util.GotoHelper.goto_LIEFERSCHEIN_AUSWAHL() | Springe auf den Lieferschein |
|com.lp.util.GotoHelper.goto_GUTSCHRIFT_AUSWAHL() | Springe auf die Gutschrift |
|com.lp.util.GotoHelper.goto_BESTELLUNG_AUSWAHL() | Springe auf die Bestellung |
|com.lp.util.GotoHelper.goto_FERTIGUNG_AUSWAHL() | Springe auf das Los |
|com.lp.util.GotoHelper.goto_AUFTRAG_AUSWAHL() | Springe auf den Auftrag |
|com.lp.util.GotoHelper.goto_ANFRAGE_AUSWAHL() | Springe auf die Anfrage |
|com.lp.util.GotoHelper.goto_ARTIKEL_AUSWAHL() | Springe auf den Artikel |
|com.lp.util.GotoHelper.goto_ANGEBOT_AUSWAHL() | Springe auf das Angebot |
|com.lp.util.GotoHelper.goto_ANGEBOTSTKL_AUSWAHL() | Springe auf die Angebotsstückliste |
|com.lp.util.GotoHelper.goto_STUECKLISTE_AUSWAHL() | Springe auf die Stückliste |
|com.lp.util.GotoHelper.goto_PROJEKT_AUSWAHL() | Springe auf das Projekt |
|com.lp.util.GotoHelper.goto_ZEITERFASSUNG_ZEITDATEN() | hier muss das ZEITDATEN_I_ID übergeben werden, womit auf die Zeitbuchung gesprungen wird |
|com.lp.util.GotoHelper.goto_ZWISCHENABLAGE() | Kopiere den Inhalt in die Betriebssystem Zwischenablage. [Siehe auch](https://gitlab.com/kieselstein-erp/sources/kieselstein/-/issues/370). |

Beachte: Ein goto innerhalb des Moduls, wird unterstützt, führt aber immer dazu, dass der soeben angezeigte Report verlassen wird.

### Währung aus dem Mandantenobjekt holen
$P{P_MANDANT_OBJ}.getMandantDto().getWaehrungCNr()

### Styles hinterlegen
Ein gutes Beispiel für Farbinformationen ist der Report<br>
```...\personal\pers_mitarbeiteruebersicht.jrxml.```<br>
Etwas einfachers, üblicheres ist z.B.<br>
```...\reports\fertigung\fert_gesamtkalkulation.jrxml```

Zusätzlich: Wenn du nachträglich Fields von anderen Reports einkopierst, greift die Stylefarbe meistens nicht. Damit diese greift musst du die Farbinformation manuell entfernen. Also:<br>
![](Styleinformation_anpassen.png)<br>
das forecolor und das backcolor über den Xml Editor manuell herauslöschen. Dann greifen die Farben.

## weitere Helper
Du findest eine Aufstellung aller verfügbaren Report-Helper im Report<br>
.../allgemein/muster_qr_code.jrxml<br>
Hier gibt es den Parameter HelperReport ![](HelperReport.png). Fügst du diesen in ein Textfield ein, so siehst du alle verfügbaren Helper.

### Liste der Helper
Hinweis: Können jederzeit programmtechnisch erweitert werden. siehe dazu Source.

```
toBigInteger( String, int ) java.math.BigInteger
toBigInteger( String ) java.math.BigInteger
toBigDecimal( String, java.util.Locale ) java.math.BigDecimal
toBigDecimal( String ) java.math.BigDecimal
getDauerEinerTaetigkeitEinesTages( String, String, java.util.Date, com.lp.server.system.service.TheClientDto ) java.math.BigDecimal
getSubreportPDFAusArtikelkommentar( String, String, String, java.util.Locale, int ) com.lp.util.LPDatenSubreport
getSubreportPDFAusArtikelkommentar( String, String, String, java.util.Locale, boolean, int ) com.lp.util.LPDatenSubreport
getMwstSummeAusListeMwstsaetze( String, java.util.Locale ) java.math.BigDecimal
getVerfuegbarkeitEinerMaschineInStunden( String, int, int, com.lp.server.system.service.TheClientDto ) java.math.BigDecimal
berechneOffenenWertEinerBestellung( String, com.lp.server.system.service.TheClientDto ) java.math.BigDecimal
berechneOffenenWertEinesAuftrags( String, com.lp.server.system.service.TheClientDto ) java.math.BigDecimal
konvertiereZeitDezimalInHHMMSS( java.math.BigDecimal, int, boolean ) String
getSubreportArtikelErsatztypen( String, com.lp.server.system.service.TheClientDto ) com.lp.util.LPDatenSubreport
getSubreportAusStringMitKommaGetrennt( String ) com.lp.util.LPDatenSubreport
getSubreportEnthaltenesLosIstMaterial( String, String, com.lp.server.system.service.TheClientDto ) com.lp.util.LPDatenSubreport
getWochentag( java.util.Locale, java.sql.Timestamp ) String
time2Double( java.sql.Time ) Double
getLief1Preis( String, java.math.BigDecimal, String, com.lp.server.system.service.TheClientDto ) java.math.BigDecimal
sortList( java.util.List ) java.util.List
double2Time( Number ) java.sql.Time
emailSenden( String, String, String, com.lp.server.system.service.TheClientDto ) boolean
haengeArrayAn( Object[], Object[] ) Object[]
getSSCC( Long, String ) String
joinStrings( String[], String ) String
zinsesZins( java.math.BigDecimal, java.math.BigDecimal, int ) java.math.BigDecimal
toInteger( String ) Integer
bildDrehen( java.awt.image.BufferedImage, int ) java.awt.image.BufferedImage
getArtikeltechnikeigenschaft( String, String, com.lp.server.system.service.TheClientDto ) String
getSubreportAllergene( String, com.lp.server.system.service.TheClientDto ) com.lp.util.LPDatenSubreport
ersetzeUmlaute( String ) String
ermittleTageEinesZeitraumes( java.util.Date, java.util.Date ) int
pruefeObCode128Konform( String ) boolean
getArtikelkommentarBild( String, String, String, java.util.Locale ) java.awt.image.BufferedImage
getSubreportAusPDFFile( String, int ) com.lp.util.LPDatenSubreport
getMonatVonJahrUndWoche( Integer, Integer, java.util.Locale ) String
ganzzahligerBetragInWorten( Integer ) String
getCalendarWeekOfDate( java.util.Date ) Integer
getCalendarWeekOfDate( String, java.util.Locale ) Integer
ersetzeUmlauteUndSchneideAb( String, int ) String
fuehreSQLQueryAus( String ) Object
getSubreportKalendertage( java.util.Date, java.util.Date, com.lp.server.system.service.TheClientDto ) com.lp.util.LPDatenSubreport
getCalendarOfTimestamp( java.sql.Timestamp, java.util.Locale ) Integer
updateLieferantBeurteilung( Integer, Integer, java.util.Date, String, com.lp.server.system.service.TheClientDto ) Integer
getArtikeleigenschaft( String, String, com.lp.server.system.service.TheClientDto ) String
wandleUmNachCode39( String ) String
wandleUmNachCode128( String ) String
pruefeObCode39Konform( String ) boolean
pruefeEndsumme( java.math.BigDecimal, java.math.BigDecimal, Double, String, java.util.Locale ) Boolean
pruefeEndsumme( java.math.BigDecimal, java.math.BigDecimal, Double ) Boolean
rundeKaufmaennisch( java.math.BigDecimal, int ) java.math.BigDecimal
getVDA4092Barcode( String ) String
berechneKWJahr( java.util.Date ) String
getLiquiditaetsKontostand( Integer, com.lp.server.system.service.TheClientDto ) java.math.BigDecimal
sortListNoNull( java.util.List ) java.util.List
getVDA4092Checksum( String ) Character
getMediastandardTextHtml( String, String, java.util.Locale ) String
getSummeIstZeitEinesBeleges( String, String, com.lp.server.system.service.TheClientDto ) Double
berechneUrlaubsAnspruch( String, java.util.Date, com.lp.server.system.service.TheClientDto ) com.lp.server.personal.service.UrlaubsabrechnungDto
sortListNoNullSprung( java.util.List, int, int ) java.util.List
entferneStyleInformation( String ) String
bildUm90GradDrehenWennNoetig( java.awt.image.BufferedImage ) java.awt.image.BufferedImage
seriennummerErzeugen( String, int ) String
berechneModulo10( String ) int
berechneModulo10Str( String ) String
addiereTageZuDatum( java.util.Date, int ) java.sql.Date
getLetztenTagDesMonats( java.util.Date, int ) java.util.Date
laenderartZweierLaender( String, String, String, java.sql.Timestamp ) String
getSubreportGeraeteseriennummernEinerLagerbewegung( String, Integer, String, com.lp.server.system.service.TheClientDto ) com.lp.util.LPDatenSubreport
```