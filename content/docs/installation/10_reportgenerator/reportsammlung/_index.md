---
categories: ["Formulare"]
title: "Report Varianten"
linkTitle: "Formular Varianten"
date: 2023-01-31
weight: 920
description: >
  Eine Sammlung praktischer / nützlicher Reportvarianten
---
Da (fast) alle Formulare auch Report-Varianten können und im Laufe der Zeit doch eine Vielzahl von praktischen Reports entstanden ist, hier eine lose Sammlung, was es denn schon so alles gibt.
Das fast bedeutet, dass für die FLR-Drucke keine Reportvarianten möglich sind, da dies generierte Reports sind.<br>
Diese Reports werden von den Mitgliedern der Kieselstein-ERP eG zur Verfügung gestellt und mit der jeweils aktuellen Version ausgerollt.<br>
So wie für alle Reports und Auswertungen. Die richtige Verwendung und die Prüfung der dargestellten Werte liegen beim Anwender / Mitglied.

| Report Zweck | Vorlage | Beschreibung | zu finden unter |
| --- | --- | --- | --- |
| nicht erledigte Aufträge mit Lagerreichweite | auft_auftrag_offene_positionen_reichweite | welche Aufträge kann ich aus dem Lager innerhalb der nächsten 8 Monate bedienen. mit möglichen und Soll-Erlösen | Auftrag, Journal, offene Positionen, Variante Reichweite
| Übermengen | fert_ausgabeliste_mit_uebermenge<br>ww_artikelreservierung | Gerade in der SMT Fertigung werden meist ganze Rollen in die Produktion gegeben. Nun will man natürlich wissen, welche Übermengen auf welchem Los gebucht sind. Dafür gibt es eine Erweiterung in der Losausgabeliste und in der Reservierungsliste | Los, Drucken, Ausgabeliste<br>Artikel, Info, Reservierungen
| Spezial KEG Personalliste | pers_personalliste_keg | Darstellung der eingetretenen Personen und deren Zeitmodelle für die Mitarbeiter VZÄ Ermittlung | Personal, Journal, Variante Mitgliederauswertung |
| LS-Gestehungspreise | ls_lieferschein_etikett_inhalt | Gestehungspreise jeder Lieferscheinposition, auch wenn es nicht Lagerbewirtschaftet Artikel sind | Lieferschein, Lieferschen, Adressetikette |
| Logindaten | pers_personalliste_login | Gerade während der Systemeinführung, wann hat nun wirklich wer am Kieselstein ERP gearbeitet | Personal, Journal, Variante Login-Daten |
| Lieferungen ohne Rechnung | rech_warenausgangsjournal_ohne_LS | Liste aller Mengenbehafteten Positionen die keinen Lieferschein (und keine Gutschrift) hinterlegt haben | Rechnung, Journal, RE-Pos ohne LS |
| Los Kommissionierliste | Los, Materialliste, fert_materialliste | Welche Artikel sollte ich an den Fremdfertiger liefern | Los, Reiter Material, Druck im Detail |
| Zahllastbetrachtung | finanz_kontoblatt_zahllast | Spezielle Sortierung des Kontodruckes, um eine gute Übersicht für das Finanzamtskonto zu bekommen. Voraussetzung dafür ist, dass in den Fibu Grunddaten, Kontoart, bei Abgabe der Abgaben Stichtag (15. / 10.) und bei Ust- Erwerbssteuerkonto die verschobene Fälligkeit in der Sortierung eingetragen ist. | Fibu, Zahllastkonto, Info, Kontoblatt, Nur Zahllast<br>![](Sortierung.png)   |
| Maschinen-Struktur-Liste | pers_maschinenliste_struktur | Strukturierte Darstellung von der Fertigungsgruppe zur Maschine | Zeiterfassung, Maschinen, Journal, Maschinenliste |
| ER mit WE | er_eingangsrechnung_alle_mit_WE | Eingangsrechnungsjournal mit den zugehörigen Bestellungen aus den Wareneingängen. Vor allem um ER ohne BS zu finden | Eingangsrechnung, Journal, Alle, Variante mit Wareneingängen |
| EK Anf Stkl Vergleich mit BS | as_einkaufsangebot_nk | Vergleich der Einkaufs Anfrage Stückliste mit den tatsächlichen Bestellungen. Die Basis ist die Projektbezeichnung von EkAfStkl und den Bestellungen. Diese müssen die gleichen Bezeichnungen haben | Angebotsstückliste, unterer Reiter Einkaufsangebot, Menüpunkt Einkaufsangebot, Drucken |
| Ek Anf Stkl Bestellinfo | as_vergleich_bs | Druck der durch Preise optimieren ermittelten Bestellwerte | Angebotsstückliste, unterer Reiter Einkaufsangebot, Menüpunkt Einkaufsangebot, Vergleich |
| ABC Verbrauchsanalyse | ww_hitliste_jahresbedarf_abc | Analyse der Artikelverbrauche nach der Pareto Regel | Artikel, Journal, Hitliste |
| Formelstückliste | stk_gesamtkalkulation_formel | Berücksichtigen der Formeln aus der Formelstückliste | Stückliste, Formelstückliste auswählen, Bearbeiten, Konfigurieren |
| Formelstückliste<br>aus Angebot | stk_gesamtkalkulation_formel_vk | Errechnen eines Angebotes anhand einer Formelstückliste | Angebot, Stückliste |
