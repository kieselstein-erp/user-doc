---
categories: ["Formulare"]
tags: ["iReport"]
title: "Rückwärts-Kompatibilität Jasperstudio -> iReport 5.5.0"
linkTitle: "Kompatibilität"
date: 2023-01-31
weight: 10
description: >
  Rückwärts Kompatibilität des iReport <-> Jasper Studio
---
Es kommt immer wieder vor, dass Reports im Jasperstudio ohne Berücksichtigung der Rückwärtskompatibilität bearbeitet wurden.

Wenn man nun, aus den verschiedensten Gründen, als Report-Designer dann doch den alten iReport verwenden möchte / muss, so sind uns bisher folgende Punkte aufgefallen, die über einen Texteditor im jrxml File geändert werden müssen.

1. Änderungen
    - das Feld textAdjust="StretchHeight" aus JasperStudio muss in iReport isStretchWithOverflow="true" heissen.<br>
    <u>**Wichtig:**</u> Wurde die Rückwärtsübersetzung nicht durchgeführt, geht die Information verloren und damit wurde eventuell der Report teilweise unbrauchbar, da nicht mehr die gesamte Information angedruckt wird.

2. Verschiedene Barcode-Komponenten, die über JasperStudio eingefügt wurden, kann der iReport nicht mehr interpretieren. D.h. diese im JasperStudio aus dem XML entfernen und "nur" abspeichern. Dann kann der Report wieder im iReport geöffnet werden.

Beim Öffnen im iReport erscheint z.B. die Fehlermeldung:<br>
![](iReport_Fehler_1.png)

Man findet im Report.jrxml <br>
![](iReport_Fehler_2.png)<br>
Diesen Block komplett entfernen und durch ein iReport Element ersetzen.
In diesem Falle ist es ein DataMatrix Barcode, welcher in seiner Definition im iReport anders ist, da andere Lib's verwendet werden.

**Zusatznotiz zum DataMatrix Code**.<br>
In den freien Lib's ist bei Dateninhalten mit drei oder mehr * (Sternchen) ein Bug enthalten, der den Code für die Scanner nicht lesbar macht. Dies ist derzeit (2024) nur durch Kauflizenzen der Barcodelibrary behebbar. Daher unser Rat -> QR-Code verwenden.
