---
categories: ["Formulare"]
tags: ["iReport"]
title: "Installation iReport 5.5.0"
linkTitle: "iReport"
date: 2023-01-31
weight: 200
description: >
  Einrichten des iReport
---
Um den iReport mit **Kieselstein ERP** verwenden zu können, darf ausschließlich die Version 5.5.0 verwendet werden.
Diese benötigt selbst wiederum Java 7, welche üblicherweise nicht mehr auf den Rechnern installiert ist.

Wenn du iReport verwenden möchtest, raten wir den iReport nach dem Jasper Studio zu installieren, sodass die Erweiterung .jrxml auf den iReport zeigt und die Erweiterung .jasper auf das Jasper Studio.

Herunterladen von: https://sourceforge.net/projects/ireport/files/iReport/iReport-5.5.0/iReport-5.5.0-windows-installer.exe/download?use_mirror=kumisystems&download=&failedmirror=deac-riga.dl.sourceforge.net

Für das Drucken von QR-Codes benötigst du zusätzlich noch<br>
core-3.2.1.jar<br>
javase-3.2.1.jar<br>
Für alle ehemaligen HELIUM V Anwender, hier muss es auch ein lpclientpc.jar geben, welches noch mit Java 7 kompiliert wurde. Dieses ebenfalls unter zusatz_libs zur Verfügung stellen.

Unsere Empfehlung ist, dass du im Verzeichnis c:\Program Files (x86)\Jaspersoft\iReport-5.5.0\ ein weiteres Unterverzeichnis *zusatz_libs* anlegst, in das diese Dateien installiert werden.
Diese Dateien müssen in den Classpath (Extras, Optionen, Reiter Classpath) mit aufgenommen werden.

Zusätzlich muss die c:\Program Files (x86)\Jaspersoft\iReport-5.5.0\etc\ireport.conf geändert werden, dass beim Aufruf von iReport 5.5.0 das Java 7 verwendet wird.<br>
Wir legen dies üblicherweise unter c:\Program Files (x86)\Jaspersoft\iReport-5.5.0\jre7 ab.<br>
D.h. hier ist das 
> jdkhome="c:\Program Files (x86)\Jaspersoft\iReport-5.5.0\jre7" 
>
zu setzen.<br>
Da das JRE7 in der richtigen Version aktuell schwer zu bekommen ist, haben wir [hier](jre7.zip) eine Version zusammengestellt.


Vor dem ersten Einsatz des iReport solltest du unter Optionen Plugins konfigurieren. Check for iReport Updates ausschalten und **wichtig** unter Optionen, Optionen Register Compiler **Reportverzeichnis zum Compilieren verwenden** anhaken. Der Haken bei keep.java kann entfernt werden.

Dass du auf das Verzeichnis der Reports ein Schreibrecht haben musst, ist selbstverständlich.
Bitte beachte auch, dass, falls die Reports auf einem Netzwerk-Share liegen, dieses Share mit ein Laufwerksbuchstaben zugewiesen sein muss. Mit UNC Pfad kommt der iReport nicht zurecht. 

## daran denken
Um im iReport die Konfigurationen überhaupt speichern zu dürfen, sind Schreibrechte am Verzeichnis erforderlich.

## Report Editor
Nachfolgend Hinweise zu der Arbeitsweise und den Eigenheiten von iReport welche bei der Gestaltung der Reports beachtet werden sollten.

**Hinweis:** Für das Arbeiten mit iReport sollte mindestens eine Zwei-Tasten Maus zur Verfügung stehen. Eine Bearbeitung der Druckformulare mit nur eine Maustaste (MAC) ist nicht möglich.

**Achtung:** Wenn Sie Druckformulare selbst bearbeiten wollen, müssen Sie diese als Anwenderreport ablegen, da ansonsten die bearbeiteten Reports bei einem Update der **Kieselstein ERP** Version gelöscht werden!!
Die Hierarchie der [Verzeichnisstruktur siehe]( {{<relref "/docs/stammdaten/system/druckformulare"  >}} )
Vor dem ersten Einsatz des iReport sollten Sie unter Optionen Plugins konfigurieren Check for iReport Updates ausschalten und '''wichtig''' unter Optionen, Optionen Register Compiler '''Reportverzeichnis zum Compilieren verwenden''' anhaken. Der Haken bei keep.java kann entfernt werden.

Dass Sie auf das Verzeichnis der Reports ein Schreibrecht haben müssen, ist selbstverständlich.

### Grundsätzliche Vorgangsweise beim Bearbeiten eines Reports

1. Wenn nicht vorhanden, ein Verzeichnis für Anwenderreport anlegen, und Originalreports hineinkopieren.
2. Bei bereits vorhandenem Anwenderreport: Backup des Anwenderreport-Verzeichnisses erstellen.
3. Textfelder (Felder oder Parameter) im iReport anpassen.
4. Änderung speichern und mit "Build - Compilieren" werden ihre Änderungen im laufenden System übernommen.
5. Änderung mit **Kieselstein ERP** Client ansehen (den entsprechenden Report Druck aufrufen).

### Feldnamen der Reports
Starten sie das Programm iReport. Gehen Sie auf "Datei -> Öffnen" und laden Sie den entsprechenden Report.

Unter "Ansicht -> Report Felder" und "Ansicht -> Report Parameter" finden Sie die Feldnamen und die Parameter Namen des Reports.

"Felder" werden nur im Detailabschnitt des Reports verwendet. Für jede z.B. Rechnungsposition wird eine Zeile im Detailabschnitt des Reports gedruckt. 
Zugriff auf ein Feld mit dem Namen Gesamtpreis ist über $F{Gesamtpreis} möglich.

"Parameter" sind nur einmal im ganzen Report vorhanden.
Zugriff auf einen Parameter mit dem Namen P_KOPFTEXT ist über $P{P_KOPFTEXT} möglich.

Die Namen sind sprechend gewählt z.B. im Anwenderreport der Rechnung unter:
im Dateiverzeichnis deines **Kieselstein ERP** Server (...) /server/helium/report/rechnung/anwender/rech_rechnung.jrxml
finden Sie den Parameter P_KOPFTEXT, dieser enthält den Kopftext der Rechnung.

Wollen Sie z.B. P_KOPFTEXT ändern, gehen Sie im Report auf das entsprechende Feld, klicken mit der rechten Maustaste darauf und wählen "Eigenschaften".

Unter "Textfeld -> Ausdruck für Textfeld" sehen Sie den Wert des Feldes.
Wenn Sie unten rechts auf "Expression Editor öffnen" klicken, können Sie leicht verschiedene Felder oder Parameter auswählen und mit "Anwenden" übernehmen.

Unter "Allgemein -> Drucken wenn" wird angegeben, wann der Inhalt des Feldes angedruckt werden soll. 
Z.B. new Boolean($F{Positionsart}.equals("Stuecklistenpos"))
gibt an, dass das Feld nur gedruckt werden soll, wenn der Wert des Feldes Positionsart gleich "Stuecklistenpos" ist.

In Textfeld und Allgemein können Sie einige Konstrukte der Java Programmiersprache verwenden, die Datentypen der Felder und Parameter entsprechen Java Datentypen, weitere Informationen dazu finden Sie in der Java Dokumentation.

Wenn Sie Änderungen durchgeführt haben, gehen Sie auf "Build -> Compilieren" und ihre Änderungen werden im laufenden System übernommen.

Bevor Sie Änderungen machen, empfehlen wir ein Backup des Reportverzeichnises zu erstellen, dadurch können fehlerhafte Änderungen zurückgenommen werden.

**Weiterführende Dokumentation:**
Kostenpflichtige iReport Dokumentation zu beziehen unter: <http://www.jasperforge.org/sf/wiki/do/viewPage/projects.ireport/wiki/HomePage> 
iReport Hilfen und Dokumentation sind über Suchmaschinen im Internet auffindbar.

Dies ist nur eine Kurzanleitung wie iReport mit **Kieselstein ERP** Drucken eingesetzt werden kann und stellt keine ausführliche iReport Anleitung dar.

### Breite der Barcodes:
<a name="Breite der Barcodes"></a>
Die Größe der gedruckten Barcodes richtet sich nach der Größe des definierten Feldes. Der Barcode wird so gedruckt, dass er sowohl von der Breite her, als auch von der Höhe her in die gedachte Umrahmung des Feldes passt. Zugleich wird das für den Barcode erforderliche Strich/Lücken Verhältnis beibehalten.

Anmerkung: Die Angabe erfolgt in 1/72" (Punkt bzw. Dezidot) Für die Umrechnung von Punkten auf mm verwenden Sie als Näherung bitte einen Multiplikator von 0,353. So ergeben 220 Punkte eine Breite von 77,66mm

In der Praxis hat sich bewährt, eine Höhe von 10mm (28Punkte) zu verwenden und die Breite ausreichend breit zu definieren.

(100/72 = Inch * 25,4 = mm = 1/72*25,4 = 0,35277)

Bitte bedenken Sie, dass ein gut lesbarer Barcode zu anderen Strichen und Kanten einen ausreichenden Abstand haben muss. Als Mindestabstand ist die Breite zweier Zeichen zu empfehlen. 

Bitte bedenken Sie auch, dass Barcodescanner üblicherweise eine maximale Erfassungsbreite von 70 mm haben. D.h. mit einem Rand von wenigen Zeichen darf ein Barcode maximal 220 Punkte breit sein.

### Definition der Barcodetype
<a name="Barcodetype"></a>
Die Definition der Barcodetype wird über den Formulareditor vorgenommen.

Üblicherweise wird der Code 128 oder der QR-Code verwendet. Bitte beachte dazu auch [Drucken von Barcodes]( {{<relref "/docs/installation/11_barcode">}} )

### Welche Parameter können wo verwendet werden.
Alle Parameter sind in den vordefinierten, von uns gepflegten Reports enthalten. Sollten Sie in Ihrem eigenen (Anwender) Report Parameter vermissen, so öffnen Sie bitte den original Report, gehen Sie auf Ansicht, Reportparameter. Hier sehen Sie nun alle definierten Felder, Variablen und Parameter. Sie können den gewünschten Parameter nun mit der rechten Maustaste kopieren, in Ihren eigenen Report wechseln und dort mit rechter Maustaste und Einfügen in Ihre Reportparameterliste aufnehmen.

**ACHTUNG:** Die Parameter werden nur innerhalb der gleichen Reports unterstützt. Das Einfügen ungültiger Parameter in einen Report kann zum Abbruch des Programms führen.

### Drucken des Pageheaders aus dem Summary heraus, wenn im Summary ein Seitenumbruch ist.

Dies ist im Konzept des JasperReports nicht direkt vorgesehen. Als Workaround gehen Sie bitte wie folgt vor:

Definieren Sie eine neue Gruppe mit Namen Summary und als äußerste Gruppe definieren. D.h. sie muss in der Ansicht der Reportgruppen als erstes stehen. Die Summary-Gruppe mit Header Höhe 0 und Footer Höhe 78 anlegen.
Die Felder aus dem original Summary in den Footer der (neuen) Summary-Gruppe (=SummaryFooter) verschieben.

Das original Summary auf Höhe 0 setzen.

Anmerkung: Sind in den Detailpositionen Felder mit Höhe 0 enthalten, werden sie beim Einfügen der neuen Gruppe nicht mitverschoben. Diese müssen manuell an die richtige Position gebracht werden, also an die richtige Position in der richtigen Gruppe per Hand nachziehen. Beispiel: $F{Lerrzeile} im Detail des Rechnungsreports.

**Dringende Empfehlung:**<br>
Bilder, die auf einem Report ausgedruckt werden sollten, dürfen im Dateinamen keine Leerzeichen enthalten. Der Dateiname sollte nur aus Kleinbuchstaben und _ bestehen. Er darf nur mit Buchstaben beginnen, im weiteren Namen dürfen Ziffern vorkommen, aber nicht an erster Stelle.

### Definition der Abmessungen des Reports:
Die Abmessungen eines Reports / eines Formulares, z.B. einer Etiketten können im Reportgenerator unter Ansicht, Reporteigenschaften definiert werden. Definieren Sie hier Abmessungen, Ausrichtungen und Ränder.<br>
**WICHTIG:** Es hat sich bewährt, den Drucker immer im Porträt bzw. Hochformat zu definieren, auch wenn ein Formular verwendet wird, welches Breiter als Hoch ist. Um dies für die Reportvorlage richtig definieren zu können, müssen Sie die Höhe etwas höher als die Breite angeben. Die eigentliche Höhe des Etikettes definieren Sie dann mit der Höhe des Druckbereiches im jeweiligen Druck-Band (Detail, bzw. Title). Siehe dazu auch [Etikettendruck]( {{<relref "/docs/stammdaten/system/etikettendruck" >}} ).

Wenn Sie die Abmessungen des Reports verringern, so achten Sie bitte unbedingt darauf, dass auch die Höhen der einzelnen Elemente der Dokumentenstruktur (pageHeader, Detail usw.) angepasst werden müssen. Sind diese zu groß, erhalten Sie Fehlermeldungen wie:

"1. The detail section, the page and column headers and footers and the margins do not fit the page height."

Gehen Sie in diesem Falle in die verwendeten Elemente (Bands) und klicken Sie in die Band height. Geben Sie hier einen Wert ein, der passt (mm -> DeziDots = mm/0,353) oder höher ist. Beim Verlassen des Feldes wird dieser Wert dann automatisch auf die richtige Höhe gesetzt.

### Der Druck des Lieferscheins ist nach unten verschoben
Wenn man ein Feld mit einer bestimmten Höhe aus einem Report löscht, dann bleibt der dafür reservierte Platz bestehen, da dieser über die Bandbreite jenes Bandes bestimmt wird, in dem sich das Feld befindet. Um also den entstehenden Leerraum zu entfernen, der durch das Löschen des Feldes entsteht, muss man die Bandbreite entsprechend anpassen.

Beispiel: Es wird ein Feld "P_BEZEICHNUNG" mit Höhe 24 Pixel aus dem Band "Title" entfernt. In der Folge muss die Bandbreite des Bandes "Title" ebenfalls um 24 Pixel verringert werden.

Seit meiner letzten Änderungen werden die Texte mit Style Informationen gedruckt.

Wird in Ihrem Report ähnliches wie ![](Report_Style.gif) angedruckt, so muss für das jeweilige Textfeld unter Font, *Is styled text* angehackt sein, damit die Formatierungsinformationen aus den Kommentardateien, aus den Textdateien usw. mit angedruckt werden.

![](report_Einstellung.gif)

### Datenexport aus der Druckvorschau
Damit insbesondere Listen in anderen Programmen weiterverarbeitet werden können, wird dringend empfohlen die Spalten und Zeilen auszurichten. Nur so wird erreicht, dass auch in den exportierten Daten die Spalten untereinander stehen. Es reicht **NICHT** dies optisch auszurichten. Es muss dies mit den Jasper Werkzeugen, gleiche Breite **UND** links bzw. rechtsbündig ausgerichtet werden. Auch die Höhen müssen gleich und ausgerichtet sein.

Für den eigentlichen Export verwenden Sie am besten den CSV Export.

Um die Datei in Excel zu übernehmen, benennen Sie nun die Erweiterung der Datei bitte auf .TXT um. Nun mit Excel öffnen, Getrennte Breite, Trennzeichen Komma, Texterkennung "", markieren Sie nun alle Spalten und Kennzeichnen Sie diese als Text und nun Fertigstellen.

So haben Sie die Daten optimiert importiert.

### Drucken von mehrspaltigen Reports z.B. für Etiketten
Mit iReport können auch mehrspaltige Reports definiert werden, so wie sie z.B. für Laseretiketten für Laserdrucker (Zweckform, Herma, ...) benötigt werden. Definieren Sie dazu die Eigenschaften des Reports unter Bearbeiten, Reporteigenschaften und klicken Sie auf den Reiter Spalten.<br>
![](ireport-Spalten1.gif)<br>
Geben Sie hier die gewünschte Anzahl der Spalten ein und definieren Sie auch den benötigten Zwischenraum.

Im eigentlichen Report finden Sie nun die angegebenen Spalten angezeigt. Definieren Sie nun die Inhalte der linken Spalte.

![](ireport-Spalten2.jpg)

**Wichtiger Hinweis:**<br>
Der Druck von Etiketten auf A4 Vorlagen ist nur für den Ausdruck von einmaligen "Listen" zu empfehlen. Gerade für die Schritte bei denen einzelne Etiketten benötigt werden, z.B. Wareneingang, werden Drucker benötigt, welche diese Einzelnen Etiketten auch ausdrucken können.<br>
Das mehrmalige Einlegen von A4-Etiketten hat schon so manchen Laserdrucker ruiniert. Auch diese Formulare sind nur für den einmaligen Durchlauf durch den Laserdrucker gedacht. Wir hatten immer wieder Fälle in denen einzelne Etiketten sich vom Trägermaterial gelöst haben und auf der Trommel des Druckers geklebt sind.

### Praktische Kommandos, Bedingungen im iReport
Nachfolgend eine lose Sammlung praktischer Kommandos für den iReport.

1.  Nur Drucken wenn String Leer ist:<br>
    new Boolean($F{F_POSITION}!=null&&$F{F_POSITION}.trim().length()>0)<br>
    F_POSITION ist hier der String

2.  new Boolean($F{F_POSITION}!=null&&$F{F_POSITION}.trim().length()>0 && $F{F_POSITIONSART}.equals("Ident "))

Beachten Sie bitte, dass auch jeder Abschnitt seine Bedingung hat. So sind die Bedingungen für den PageHeader üblicherweise in diesem abgebildet (<$V{IS_CURRENT_PAGE_NOT_ONE}>) und nicht bei jedem einzelnen Feld.

Funktionen die von **Kieselstein ERP** zusätzlich für die Reportgestaltung im iReport verwendet werden können

Nachfolgend eine Sammlung ergänzender Funktionen für die Verwendung im iReport. Voraussetzung dafür ist, dass das lpclientpc.jar in den Classpath des iReport (siehe Extras, Optionen, Classpath) eingebunden ist.

Alle Funktionen sind in der Klasse com.lp.util.HelperReport gesammelt. Der Aufruf aus iReport entspricht immer dem Muster:

com.lp.util.HelperReport.FUNKTION

-   boolean pruefeObCode39Konform(String sString)
    Sind in dem String nur Code39 druckbare Zeichen enthalten

-   String wandleUmNachCode39(String input)
    Ersetzt Umlaute mit deren Entsprechung ohne Umlaut

-   Double time2Double(java.sql.Time time)

-   String ersetzeUmlaute(String input)
    Wandelt Ö auf Oe usw.

-   String wandleUmNachCode128(String input)

-   String ersetzeUmlauteUndSchneideAb(String input,int maxStellen)

-   String getWochentag(java.util.Locale locale, java.sql.Timestamp tDatum)
    Liefert den Wochentagsnamen in der jeweiligen Sprache

-   String ganzzahligerBetragInWorten(Integer betrag)

-   Integer getCalendarOfTimestamp(java.sql.Timestamp tTimestamp, Locale locale)

-   String getMonatVonJahrUndWoche(Integer iJahr, Integer iWoche, Locale locale)
    Liefert den Monatsnamen eines Jahres und einer Kalenderwoche

-   Integer getCalendarWeekOfDate(Date date)

-   Integer getCalendarWeekOfDate(String sDate, Locale locale)

-   Boolean pruefeEndsumme(BigDecimal bdReportNettoValue, BigDecimal bdHvValue, Double dAbweichung, String listeMwstsaetze, Locale reportLocale)

-   String laenderartZweierLaender(String lkzKunde, String lkzBasis, String uidNummer, java.sql.Timestamp tsEUMitglied)
    Ermittelt die Länderart des Kunden in Bezug auf die Basis (= Mandant)

-   int berechneModulo10(String nummer)
    'nummer' darf nur Ziffern zwischen 0 und 9 enthalten!

-   String berechneModulo10Str(String nummer)

-   Time double2Time(Number zahl)

-   Konvertierung eines Strings in einen anderen Datentyp, ohne Fehlermeldung (Exception):
    BigDecimal toBigDecimal(String bigDecimal) ; // wandelt nach BigDecimal, in Locale "deAT"
    BigDecimal toBigDecimal(String bigDecimal, Locale locale) ; // wandelt nach BigDecimal in angegebener Locale

    BigInteger toBigInteger(String bigInteger) ; // wandelt nach BigInteger mit Radix 10 (Dezimalzahlen)
    BigInteger toBigInteger(String bigInteger, int radix) ; wandelt nach BigInteger mit angegebenen Radix (oktal, hex, ....)

    Integer toInteger(String integer) ; // wandelt nach Integer

    All diesen Funktionen gemeinsam: Bei einem fehlerhaften Format, oder auch null als Parameter wird null anstatt einer Exception zurückgeliefert. Dies muss im Report entsprechend ausgewertet werden.<br>
    Anwendungsbeispiele:
    ```bash
    HelperReport.toBigDecimal("25'100,12", new Locale("de", "CH")) ; // deCH
    HelperReport.toBigDecimal("25.100,12") ; // deAT
    HelperReport.toBigDecimal("-25'987.65") ;
    ```
[Für weitere Helper siehe auch]( {{<relref "/docs/installation/10_reportgenerator/formelsammlung/#aktuell-gibt-es-folgende-helper" >}} )


#### Bei der Komplierung kommt eine ClassNotFoundException Fehlermeldung
Diese Meldung bedeutet, dass der iReport Compiler eine Java Klasse nicht findet. Um die Formulare von **Kieselstein ERP** mit der gewünschten Funktionalität auszustatten, werden viele Klassen direkt von **Kieselstein ERP** zur Verfügung gestellt. Dafür muss der Pfad auf den **Kieselstein ERP** Client mit in den ClassPath des iReport eingebunden werden.
Sie finden dies unter Extras, Optionen, Classpath. Geben Sie hier den Pfad auf den aktuellen **Kieselstein ERP** Client an.
![](iRepot_Einstellungen.png)

#### Fehlermeldung unsupported version
Kommt beim Kompilieren eine Fehlermeldung ähnlich
java.lang.UnsupportedClassVersionError: com/lp/util/report/PositionRpt : Unsupported major.minor version 51.0
so bedeutet dies, dass iReport nicht unter Java 7 ausgeführt wird.

Beachten Sie bitte in jedem Falle, dass Sie iReport nur unter Java 7 genutzt werden kann und dass Sie dafür den Client für Java 7 benötigen.<br>
Die Clients für **Kieselstein ERP** sind für Java 8 kompiliert, was bedeutet dass für Anwender die nur Kieselstein ERP verwenden, das [JasperStudio]( {{<relref "/docs/installation/10_reportgenerator/jasperstudio" >}} ) eingesetzt werden muss. Die Arbeitsweise ist sehr ähnlich.