---
categories: ["Formulare"]
tags: ["Jasper Studio"]
title: "Installation Jasperstudio"
linkTitle: "Jasperstudio"
date: 2023-01-31
weight: 100
description: >
  Einrichten des Jasperstudio
---
Für das Arbeiten mit Jasperstudio ist unbedingt folgendes zu beachten:<br>
Ein Arbeiten mit Jasperstudio unter 16 GB Ram ist faktisch unmöglich. Auch muss von einer entsprechend leistungsfähigen CPU ausgegangen werden. Idealerweise sind sowohl Programme als auch Reports auf einer SSD Platte verfügbar.

## Automatisches Build abschalten
Es ist leider so, dass dieses Build in der Regel die erzeugten Jasper-Dateien zerstört. Daher muss dies unbedingt abgeschaltet werden, bevor die Reports, also die Projekte zugeordnet werden. Es sollte auch das automatische Sichern der Report Sources vor dem Build / Kompilieren eingeschaltet sein.<br>
Hauptmenüleiste, Window, Preferences

![](Workspace.png)

General, Workspace, Build

![](Pfad.png)

![Build und Sichern](Sichern%20und%20automatisches%20build.png)

## Kompatibilität zu iReport 5.5.0
Da das Arbeiten, gerade für komplexere Reports mit Subreports und Rückgabewerten im iReport deutlich einfacher ist, ist es sehr praktisch, wenn die XML-Sourcen der Reports kompatibel zu JasperReport 5.5.0 abgespeichert werden. Es steht damit zwar das automatische Fontsizing zur Anpassung an den verfügbaren Platz NICHT zur Verfügung, aber da Schriften unter 6 Punkt sowieso als nicht geschrieben gelten, benötige ich diese Funktion nicht.<br>
![Kompatibilität](JasperStudio_Compatibility.png)

## Kennzeichnen für Jasperstudio wird instabil
Wenn das automatische Sichern vor dem Kompilieren nicht mehr geht, empfiehlt sich, das Jasper Studio neu zu starten, damit nicht deine Editierarbeit vergebens war.

## Installation
{{% alert title="Report Backup" color="warning" %}}
Wir raten dringend dazu, vor der Installation ein Backup des gesamten Report-Verzeichnisses zu machen. Es ist leider immer wieder vorgekommen, dass durch die Installation die Jasper-Dateien, die eigentlich ausführbaren Dateien sind, zerstört wurden.<br>
Bitte denke auch daran, dass die Workspace-Einstellungen Benutzer-Werte sind. Also, auch wenn der gleichen Rechner/Server genutzt wird. In dem Moment, wo du dich mit einem anderen Benutzer als der einrichtende anmeldest, bitte die Einstellungen prüfen.
{{% /alert %}}

### herunterladen
https://sourceforge.net/projects/jasperstudio/files/latest/download

Achte darauf immer nur eine Version auf dem Rechner installiert zu haben. Die Version vom 29.5.2023, 6.20.5 scheint etwas weniger ressourcenhungrig zu sein.<br>
**ACHTUNG:** Jasperstudio 6.20.6 hat andere Property Definitionen (kann mit den Properties am Root nicht umgehen und hat weitere Bugs). Aktuell verwenden wir 6.20.5


#### 2024-03-25 / 6.21.2
Es stehen die Vorgängerversionen nicht mehr zur Verfügung.
JasperSoft stellt aktuell die 6.21.2 zur Verfügung.
{{% alert title="ACHTUNG:" color="warning" %}}
Diese kann derzeit mit Kieselstein ERP **<u>nicht</u>** nicht verwendet werden.
Versuche die 6.20.6 mit der unten beschriebenen Anpassung der Projekt-Pfade zu verwenden.
{{% /alert %}}

Diese hat das gleiche Verhalten wie die Version 6.20.6. D.h. sie kann mit den Reports auf dem Root nicht umgehen. D.h. insbesondere bei der Bearbeitung von Report in Netzwerkumgebungen muss mindestens eine zusätzliche Ebene, also ein Ordner angesprochen werden. Sonst werden die Properties nicht ausgelesen.

Die Datei findest du unter https://community.jaspersoft.com/files/file/41-jaspersoft-community-edition/
Für den Download muss du dich entsprechend registrieren.

**<u>Hinweis:</u>** Auch für die Installation wird nun eine Registrierung verlangt.

#### 2024-07-01 / 6.20.6
Die Version 6.20.6 steht offensichtlich auch nicht mehr zur Verfügung.

Das bedeutet im Endeffekt. Du musst dir für dein Betriebssystem eine gültige Version der 6.20.5 organisieren.

Wir arbeiten gerade an der Umstellung des Wildfly auf Java 11 und damit verbunden auch auf die Verwendung des aktuellen Jasper Studio 6.21.3. 

#### 2024-10-01 / 6.21.3
Mit der Version 1.0.0 oder höher kann Jasper Studio 6.21.3 verwendet werden.

### einrichten
Die Einrichtung ist so aufgebaut, dass zuerst die Einstellungen im Standard Workspace erzeugt werden und dann in das Report-Verzeichnis verschoben werden. Damit das Jasperstudio das erkennt, ist ein Anlegen, Verschieben, Löschen und Re-Importieren erforderlich. Daher die nachfolgende Beschreibung.<br>
Solltest du mehrere Kieselstein-Installationen parallel betreuen, kannst du durch entsprechende Namensvergabe dies komfortable steuern.

#### Neues Project = Kieselstein anlegen
![](New_Project.png)  

![](Report_Project.png)  

![](Project_Name.png)  

### Project anlegen Variante 2
Nun im Menü Project und dann Project Explorer<br>
![](Project_Explorer.png)  

Wenn noch kein Projekt angelegt, dann auch hier anlegen
im leeren darunter liegenden Feld, Rechtsklick mit der Maus<br>
![](neues_Project.png)  

![](neues_Project_name.png)  

#### Project einstellen
ACHTUNG: Ab der Version 1.0.3 ist die Pfadtrennung in dist und data aktiv. Idealerweise werden von dir nur die anwenderspezifischen Reports bearbeitet, die sich im ?:\kieselstein\data\reports befinden.

- Rechtsklick auf Projektname (Kieselstein) <br>
![](Einstellen1.png)  

Im Einstellungsdialog, Java Build Path, dann der Reiter Libraries und klicke auf Classpath.
Nun die weiteren Jars hinzufügen, also Knopf Add External JARs.<br>
![](Add_external_Jars.png) 

Und nun die drei Files hinzufügen<br>
![](external_files.png) 

core-3.2.1.jar und javase-3.2.1.jar sind für die Bearbeitung von QR-Codes.<br>
kieselstein-ui-swing.jar für die Verwendung der zahlreichen Helperreport-Methoden.<br>
[Die Zusatz-Libs findest du hier]( ./zusatz_lib/javase-3.2.1.jar) und 
[hier]( ./zusatz_lib/core-3.2.1.jar)<br>
Bitte beachte, dass das aktuell zu deiner **Kieselstein ERP** Server Version passende ejb.jar verwendet wird.

Ab der **Kieselstein ERP** Version 1.0.3 wird anstelle der ejb???.jar die kieselstein-ejb-1.0.3.jar verwendet. Du findest diese im lib Verzeichnis des Clients. Kopiere diese idealerweise in das bin Verzeichnis.<br>
Zusätzlich solltest du bei Erweiterungen in den Helper daran denken, dass diese Datei auf die aktuell von dir verwendeten Kieselstein Version aktualisiert werden sollte. Siehe dazu deinen aktuellen Client, ?:\kieselstein\client\lib\kieselstein-ejb-1.?.?.jar 

![](external_file_ejb.png)<br>
die ejb Datei, in der alle von deinem Server unterstützen Helper enthalten sind.<br>

Bitte beachte, dass die hier angeführten Versionsnummern zu deiner Serverinstallation passen müssen.

#### Verbinden der Reportdateien mit dem Projekt
{{% alert title="Automatisches Build abschalten" color="warning" %}}
Unbedingt vorher das automatische Build abschalten.
{{% /alert %}}

dafür erneuter Rechtsklick auf den Projektnamen<br>
![](Systemexplorer.png)  <br>

Nun siehst du, wo die Einstellungsdateien liegen. Z.B.<br>
![](Workspace_Dateien.png) 


Verschiebe diese nun in dein Kieselstein-ERP-Report-Root-Verzeichnis.<br>
![](Verschieben.png)

Nun das oben angelegte Projekt löschen<br>
Also erneut Rechtsklick auf das Projekt und Delete / löschen<br>
![](Projekt_entfernen.png)

und aus dem Reportverzeichnis wieder importieren<br>
<br>
Also erneuter Rechtsklick in das freie Feld im Projekt-Explorer und "Importieren" wählen.<br>
![](Import.png) 

In diesem Import Dialog, wählen General bzw. Allgemein und in dem Ordner den Typ<br>
Existing Project into Workspace.<br>
![](Import_Typ.png) 

Hier nun das Reportverzeichnis deines Kieselstein-ERP Servers angeben.<br>
![](Reportverzeichnis.png) 

Du findest nun im Projektexplorer unter Kieselstein die gesamten definierten Reports.<br>
![](Reports.png)  

**ACHTUNG:**<br>
Wenn die Reports durch eine andere Quelle, z.B. iReport verändert werden, musst du im jeweiligen Detail (Unter-) Baum mit Rechtsklick die Anzeige aktualisieren. Das auch, wenn nur der Inhalt der Reportdefinition, also das jrxml File geändert wurde.

**Hinweis:**<br>
Denke daran die Kompatibilität zu iReport 5.0.0 einzustellen.

**Tipp:**<br>
Die Files bitte immer aus dem Projektbaum des Jasperstudios öffnen und da auf die jrxml Files klicken. Das sind die eigentlichen Sourcen.

### Hinweis:
Ist bereits unter einem anderen User eine Jasper Studio Installation vorhanden, so einfach das Jasperstudio starten. Es kommt der Willkommens Schirm. Dann Menü, Window, new Window. Dann im oberen Reiter den Project Explorer auswählen und dann weiter mit rechter Maustaste und Import.

### Hinweis mehrere Installationen gleichzeitig warten
Wenn mehrere Kieselstein-Installationen faktisch parallel bearbeitet werden sollten, z.B. für verschiedene Firmen oder für die Testinstallation, so kannst du aus der ersten Installation die oben beschriebenen Dateien bzw. Verzeichnisse in die anderen Reportverzeichnisse kopieren.<br>
**WICHTIG:** Es sind keine doppelten Projektnamen erlaubt. D.h. es muss nach dem Kopieren (und vor dem Importieren) die Datei .project (z.B. ?:\kieselstein\dist\wildfly-12.0.0.Final\server\helium\report\.project ) bearbeitet werden. Hier ist der Wert für ![](Gleichzeitig.png)  name entsprechend auf den neuen eindeutigen Projektnamen zu ändern. Danach bitte bei Projekt importieren fortsetzen.<br>
Sollte im Importdialog<br>
![](Projekt_nicht_auswaehlbar.png)  <br>
das Projekt nicht ausgewählt werden können, so gibt es ein Projekt unter diesem Namen bereits. Also die .project Datei entsprechend bearbeiten.

Wichtig:<br>
In .classpath des Projektes stehen die einzubindenden externen Java-Klassen drinnen. D.h. wenn man gemeinsam ein Report-Verzeichnis verwenden möchte, müssen die externen Java-Klassen auch in den exakt gleichen Verzeichnissen liegen.<br>Hier hat sich bewährt, die 
![](classpath.png)  Classpathentry's in UNC Notierung anzugeben.

D.h. man kann das bin Verzeichnis auch dafür nutzen, diese speziellen Java Libs, zentral bei den Reports mit zu hinterlegen und damit sehr klar das Projekt zur Verfügung zu haben.

{{% alert title="Vor dem Kompilieren immer speichern" color="warning" %}}
Leider ist es so, dass, obwohl "Speichern vor Kompilieren" angehakt ist, Jasperstudio auch in der Version 6.20.5 dies nicht zuverlässig macht.<br>
Da das Ding auch immer wieder mal abstürzt, habe ich mir angewöhnt, dass ich immer, **<u>bevor</u>** ich auf "Kompilieren" klicke, auf "Speichern" klicke. Auch das übliche Strg+S geht leider nicht immer.
{{% /alert %}}

### noch einstellen
Ich habe für mich noch folgende Einstellungen getroffen

### Window, Preferences, General, Editors
Prompt to save on close event if still open elsewhere

### Window, Preferences, Jaspersoft Studio, Report Designer
- Abschalten: Automatically exand bands to fit child elements
- Abschalten: Resize band to accomodate pasted elements

### Window, Preferences, Jaspersoft Studio, Report Designer, Rulers And Grid
- Grid Options, schalte ich generell alles aus, lasse aber Show Rulers an und Ruler Measure Unit auf Centimeter.

### Window, Preferences, Jaspersoft Studio, Report Designer, Toolbars
- Elements Alignment ist sehr praktisch

### Window, Preferences, Java, Compiler
Compiler compliance level -> habe ich auf 1.8 gestellt, da derzeit der Server nur in Java 8 compiliert ist

### Window, Preferences, General
![](Run_in_Background_abschalten.png)  <br>
Alway run in background -> abschalten -> üblicherweise kompiliere ich nur den Report, der mich interessiert und nicht laufend alle (was man bei komplexen Programmen dann doch will)

### Empfehlung
Ich richte alle Felder im 10 Pixel Raster aus. Nur wenn der Platz extrem begrenzt ist, z.B. bei Etiketten gehe ich ins 5er Raster oder wirklich pixelweise.
Das hat sich auch für Formulare, welche nach XLS oder CSV exportiert werden sollten, sehr bewährt.

### Wichtig
Damit die Einstellungen gespeichert werden können, muss man auf das Programmverzeichnis entsprechende Rechte vergeben
"c:\Program Files\Jaspersoft\Jaspersoft Studio-6.20.5" 

### Fehler beim Kompilieren finden
Auf den Tab-Problems, den Report suchen und Doppelklick

### Info zu Variablen
Wenn man den Namen einer Variablen ändert, ändert er automatisch alle Verwendungen auch ab.
Im iReport hat er das nicht gemacht. Hat beides seine Vorteile.

### weitere Einstellungen
Wie werden CSV Daten exportiert. Siehe Window, Preferences, Jaspersoft Studio, Exporters und z.B. CSV Exporter. Hier würde ich als FieldDelimiter \t anstatt Komma versuchen.
![](CSV_Export_Delimiter.png)

### Installation weiterer Schriften
Insbesondere für die PDF/A-3 Forderungen dürfen nur freie Schriften verwendet werden.
So wird von Kieselstein ERP in der Zugferd-Rechnung automatisch der freie Font Liberation Sans mit eingebunden.<br>
Nur dieser Font sollte für die Gestaltung deiner Rechnungen inkl. der Kopf- und Fußzeilen verwendet werden.

Dieser zusätzliche Fonts muss unter Umständen in deinem JasperStudio eingerichtet werden. [Siehe dazu](https://community.jaspersoft.com/knowledgebase/best-practices/custom-font-font-extension/)