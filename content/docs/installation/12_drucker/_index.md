---
categories: ["Drucker"]
title: "Drucker einrichten"
linkTitle: "Drucker"
date: 2023-01-31
weight: 1200
description: >
  Drucker unter Linux einrichten
---
Drucker unter Linux einrichten
=====

Ab CentOS 5 können IP Drucker am einfachsten mit printconf eingerichtet werden.
Hier stehen fast alle Druckertreiber usw. zur Verfügung.
Der Port für die Standard IP Verbindung ist 9100. Siehe dazu auch in den Windowseinstellungen in der Konfiguration des Druckeranschlusses unter RAW Einstellungen, Portnummer.

Alternativ / Einfacher kann der WebMin (https://192.168.8.251:10000/), Hardware, Printeradministration verwendet werden. Hier können, ab CentOS 5 fast alle Druckertreiber usw. ausgewählt werden. 