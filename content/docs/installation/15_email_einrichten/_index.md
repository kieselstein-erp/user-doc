---
categories: ["EMail Versand einrichten"]
title: "EMail Versand einrichten"
linkTitle: "EMail Versand einrichten"
date: 2023-01-31
weight: 1500
description: >
  Einrichten des EMail Versandes
---
Nach der Installation sollte oft sehr rasch der E-Mail-Versand eingerichtet werden.

Es sind dabei einige Dinge zu beachten:
1. Sind die Automatikjobs aktiviert SMTP und IMAP und der Automatikjob an und für sich aktiviert
2. hat der lpwebappzemecs überhaupt Rechte, vor allem wenn ein weiterer Mandant eingerichtet wurde
3. sind die E-Mail-Parameter für jeden zu sendenden Mandanten hinterlegt
4. geht der E-Mail-Versand aus System, Parameter, ![](EMail_Test_Versand.png)  

**Hinweis:**<br>
Wenn man beim Testen bereits E-Mail-Versandaufträge erzeugt hat, aber obige Parameter nicht richtig waren, so diese Versandaufträge löschen und neu erzeugen. Es werden einige Dinge intern zwischengespeichert, die die Änderung nicht mitbekommen.

Kommt beim Test-E-Mail-Versand die Fehlermeldung
> nested exception is:<br>
>    javax.net.ssl.SSLException: Unsupported or unrecognized SSL message<br>
>    at com.sun.mail.smtp.SMTPTransport.openServer(SMTPTransport.java:2120)
>
So wurde die falsche Verschlüsselung für den SMTP-Versand ausgewählt. Versuche einfach die andere Verschlüsselung.



## Interner Versanddienst

Integriert im **Kieselstein ERP** Applikations Server steht eine direkte SMTP und IMAP Anbindung für den Versand der E-Mails und gegebenenfalls auch Faxe zur Verfügung.

**Voraussetzungen**

**Kieselstein ERP** Interner Versanddienst E-Mail SMTP und gegebenenfalls IMAP Server mit den entsprechenden Zugangsdaten
- Personal IMAP Zugangsdaten (Reiter Parameter) und E-Mail-Absenderdaten sind eingestellt
- Personal IMAP Abholungsdaten (Inbox) ist eingerichtet

### Funktionsweise des internen Versanddienstes / Mailversand
- a.) E-Mail Versand<br>
Das E-Mail wird mit einem der **Kieselstein ERP** Module erzeugt und durch den Klick auf Senden in den Versandauftrag übertragen.<br>
Hier ist üblicherweise (Parameter DELAY_E_MAILVERSAND) ein Zeitversatz von 5 Minuten eingetragen. 
Nach dem Ablauf dieser Zeit wird das E-Mail mit den eingestellten SMTP Parametern per SMTP versandt.<br>
War der SMPT-Versand erfolgreich, wird das E-Mail in den gesendet Ordner des jeweiligen Sendenden abgelegt.

Also:
- a1.) Versandauftrag erzeugt
- a2.) SMPT Versand
- a3.) IMAP Ablage in gesendete Objekte

Hinweis: Beachte, dass der Versanddienst als Automatikjob ausgeführt wird. D.h. es muss der Automatikjob aktiviert werden. [Siehe dazu]( {{<relref "/docs/stammdaten/system/automatik" >}} ).

Es ist der SMTP Versand von der IMAP Ablage datentechnisch komplett getrennt, daher wird ein per SMTP versandtes E-Mail aus dem Postausgang nach gesendet verschoben. Solange es noch nicht in der IMAP Ablage des jeweiligen Benutzers abgelegt werden konnte, bleibt es hier im Status "Teilerledigt" ![](Versandtstatus_Teilerledigt.gif) stehen.<br>
Erst nachdem auch die IMAP Ablage gemacht wurde, wird der Status auf "Erledigt" = Versandt geändert.<br>
Zusätzlich sieht man in der Statuszeile des Versandauftrages ganz unten den detaillierten Status des jeweiligen Versandauftrages, also gegebenenfalls auch eine entsprechende Fehlermeldung.<br>
Hinweis: Von ev. Fehlermeldungen des vorgelagerten Servers werden nur die ersten 1000 Zeichen abgespeichert und angezeigt.

- b.) E-Mail Empfang<br>
Start des E-Mail Clients ![](E-Mail-Client.JPG) und Klick auf "Neue Nachrichten vom E-Mail Server laden" ![](empfangen.JPG) oder "automatisch abholen" ist angehakt.

Es werden die E-Mails aus dem hinterlegten IMAP Verzeichnis abgeholt. Dieses geht von der Standard Inbox des jeweiligen Benutzers aus. Wir raten hier z.B. ein (Imap) Verzeichnis ToKieselstein anzulegen und dieses entsprechend beim Personal des jeweiligen Benutzers unter Daten, IMAP-Inbox einzutragen.

Bei der Übernahme der IMAP Daten aus dem IMAP Server wird darauf geachtet, dass von den Dateninhalten keine Duplikate übernommen werden. Das bedeutet E-Mails mit gleicher Message-ID werden nur einmal übernommen.

Also:
- b1.) IMAP -- Inbox-Verzeichnis pollen
- b2.) Daten nach **Kieselstein ERP** E-Mail-Datenbank kopieren

### Konfiguration 
![](Parameter_interner_Versanddienst.gif)

Die Parametrierung des internen Versanddienstes erfolgt im Modul System, unterer Reiter Parameter. Gibt man hier zur Einschränkung der Suchergebnisse unter Kategorie Versand ein, so werden die erforderlichen Parameter für den internen Versanddienst gelistet. Solltest du diese Parameter nicht vorfinden, so ist in deiner **Kieselstein ERP** Installation dieses Modul nicht freigeschaltet. Bitte wende dich an deinen **Kieselstein ERP** Betreuer.

### Einfache Konfiguration
Alternativ kann durch Aktivieren des Parameters MAIL_SERVICE_PARAMETER=1 mit anschließendem Schließen und Neustarten des Moduls System in den Reitern 4 (E-Mail) bzw. 5 E-Mail Admin  ![](Mail-Parametrierung.gif) die Parametrierung vorgenommen werden. Dies ist vor allem für deinen IT-Administrator gedacht.<br>
Vom Ablauf her, werden die Parameter im Reiter 4 eingestellt und dann der tatsächliche E-Mail-Versand im Reiter 5 durch Klick auf das E-Mail-Symbol ![](Mail-Parametrierung2.gif) getestet. In diesem E-Mail-Versand werden auch die Fehlermeldungen beim Versand entsprechend zurückgegeben, womit für einen erfahrenen IT-Admin die Parametrierung für die verschiedensten E-Mail-Dienste einfach durchgeführt werden kann.

#### Einrichten des SMTP Servers:
Durch die Parametrierung des SMTP Servers, wird der interne Versanddienst aktiviert.

| Parameter | Beispiel | Beschreibung |
| --- |  --- |  --- |
| SMPTSERVER | smtp.meinefirma.localodermail.firma.local | Servername des Postausgangs Servers (SMTP) (oder IP-Adresse) |
| SMTPSERVER_BENUTZER |  | Falls der Postausgangsserver eine Authentifizierung erfordert ist diese hier einzutragen. Benutzername |
| SMTPSERVER_KENNWORT |  | Kennwort |
| SMTPSERVER_FAXDOMAIN | fax.local | Unterstützt der SMTP-Server eine Faxweiterleitung / Faxgateway so wird diese Funktion durch einen Eintrag der entsprechenden Domain aktiviert. |

#### Anmerkung zum Faxgateway / Faxweiterleitung:
Die Faxnummern werden beim Versand eines Faxes automatisch um diese Domain ergänzt und das Fax wird als E-Mail an den SMTP Postausgangsserver versandt. Der eigentliche Versand des Faxes muss durch den Faxgateway erfolgen.

#### Einrichten der IMAP Anbindung:
Durch die IMAP Anbindung, wird nach erfolgtem SMTP Versand eine Kopie des E-Mails im IMAP Postfach des Benutzers abgelegt. Wenn diese Funktion nicht benötigt wird, sind die Parameter leer zu lassen.

Die Parametrierung ist zweigeteilt:
- a.) die generelle Einbindung, diese findet man ebenfalls unter System, Parameter, Kategorie Versand
- b.) die Definition der Postfächer für jeden Benutzer, diese findet man im Modul Personal, im oberen Modulreiter Parameter.

### Parametrierung IMAP Anbindung

| Parameter | Beispiel | Beschreibung |
| --- |  --- |  --- |
| IMAPSERVER | mail.firma.local | Name des Imapservers (oder IP-Adresse) |
| IMAPSERVER_ADMIN |  | Name des Users, bei dem Mails und Faxe ohne Angabe eines Absenders abgelegt werden |
| IMAPSERVER_ADMIN_KENNWORT |  |  |
| IMAPSERVER_SENTFOLDER | Gesendete Objekte | Name des Ordners für die versandten Mails in der IMAP Struktur |
| MAILADRESSE_ADMIN |  | Diese Adresse wird beim Faxversand verwendet, falls kein Benutzer angegeben ist |

#### Definition der Personal- / Benutzerdaten
| Parameter | Beispiel | Beschreibung |
| --- |  --- |  --- |
| IMAP-Benutzer | office | Benutzerkonto am IMAP Server |
| IMAP-Kennwort | Kennwort | Kennwort für den Benutzer am IMAP Server.Hinweis: Das Kennwort wird nur mit ** angezeigt. |

#### Der IMAP Server ist definiert, es werden aber keine E-Mails versandt?
Der interne Versanddienst von **Kieselstein ERP** funktioniert so, dass zuerst das E-Mail per SMTP Server versandt wird. Hat dies funktioniert, so wird, wenn der IMAP Server definiert ist, eine Kopie davon in das IMAP Konto des Benutzers abgelegt.

Das bedeutet: Bitte prüfe, ob der Zugriff auf den SMTP Server mit den Parametern SMPTSERVER, SMTPSERVER_BENUTZER, SMTPSERVER_KENNWORT richtig konfiguriert ist.

Man sieht im System, unterer Modulreiter Versandauftrag, beim jeweiligen Versandauftrag unten den Status. Interpretiere dies entsprechend.

Werden Parameter am Versandauftrag verstellt / neu gesetzt, so sollte ein im Postausgang wartender Auftrag mit dem grünen Haken ![](Versanddienst_triggern.gif) neu getriggert werden. Es wird dadurch auch die interne Zeitsteuerung neu angestoßen.

Bei mehreren Mandanten achte bitte darauf, dass alle erforderlichen Parameter sowohl unter System als auch im Personal je Mandant und Mitarbeiter, der Versenden darf, definiert werden muss.

### Voraussetzungen, die gegeben sein müssen, um E-Mails richtig weiterreichen zu können
Gerade im deutschsprachigen Raum kommt es immer wieder zur massiven Verwirrung, weil der Inhalt von E-Mails nicht richtig weitergereicht wird. Hier kommt erschwerend dazu, dass am E-Mail-Versand doch einige Systeme beteiligt sind.

- **Kieselstein ERP** -> übergibt das E-Mail an den SMTP Server
- SMTP Server reicht das E-Mail an den Empfänger weiter und bestätigt, dass das E-Mail versandt werden konnte
- **Kieselstein ERP** legt eine Kopie im IMAP Ordner des Versenders ab
- Empfänger öffnet das E-Mail mit seinem Client

Abhängig von dem, was gesandt wurde, verhalten sich die Systeme zum Teil sehr unterschiedlich.

Wichtig:
- Derzeit wird E-Mail-Text, welcher von **Kieselstein ERP** generiert wird, als reiner ASCII Text übergeben. Dieser Text ist nach UTF-8 codiert.
- Die SMTP Server richten sich normalerweise nach der im Betriebssystem des Servers eingestellten Sprache
- Plain Text E-Mails, wie sie von **Kieselstein ERP** gesendet werden, werden in den Clients oft unterschiedlich interpretiert / dargestellt.

Daher müssen, damit die E-Mails mit Umlauten auch richtig beim Empfänger ankommen, alle beteiligten Komponenten mit den Vorschriften [RFC 2047](http://tools.ietf.org/html/rfc2047) und [RFC 6152](http://tools.ietf.org/html/rfc6152) richtig umgehen können.

Ein wichtiger Punkt ist dabei noch die richtige Codierung der mail.xsl Datei. Diese muss für Windows und Linux auf UTF8 erfolgen, für MAC auf ISO-8859-1 (Ansi). Achte darauf diese XSL Dateien ausschließlich mit geeigneten Editoren zu bearbeiten, wie z.B. Notepad++. Zum Thema SMTP Versand [siehe auch](http://de.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol).

### Fax - E-Mail Gateway
Alternativ zur oben beschriebenen Faxweiterleitung ist auch der Versand einer E-Mail an einen Fax-Mail Gateway möglich. Auch hier wird eine E-Mail mit PDF Anhang an eine bestimmte Adresse versandt, es sind jedoch im Betreff die entsprechenden Daten enthalten. Der Aufbau ist wie folgt:

E-Mail Empfängeradresse, so wie unter SMTPSERVER_FAXDOMAIN angegeben.

Betreffzeile: Passwort internationaleNummer; Rückmeldungstext für Bestätigung

Für die Aktivierung dieser Funktion müssen die folgenden Parameter (Kategorie Versanddienst) entsprechend eingestellt sein:
- SMTPSERVER_FAXANBINDUNG: Wert=1
- SMTPSERVER_FAXDOMAIN: fax.xpirio.com
- XPIRIO_KENNWORT: Kennwort für das Gateway

#### Fax - Gateway für Faxmaker
Der Faxversand über den Faxmaker ist ebenfalls möglich.

Angeblich ist die für **Kieselstein ERP** zu verwendende Option immer verfügbar. D.h. es ist für den Faxversand lediglich bei SMTPSERVER_FAXDOMAIN FAXMAKER.COM einzutragen.

In aller Regel macht die Telefonanlage / der Faxmaker die Amtsholung selbst, daher muss der Parameter AMTSLEITUNGSVORWAHL auf ein Leerzeichen gestellt werden. Die Übergabe der Faxnummer mit enthaltenen Leerstellen ist entgegen der Beschreibung von Faxmaker bei der von uns getesteten Version möglich.

### Einrichten E-Mail-Versand per GMail

Um auch über ein GMail Konto E-Mails aus **Kieselstein ERP** versenden zu können, hilft eventuell folgende Vorgehensweise:<br>
Nach erfolgter Anmeldung an deinem Google Konto, suche nach den App-Passwörtern. ![](GMail1.gif) <br>
Wähle hier nun die App-Art E-Mail aus und lasse dir dafür ein Passwort generieren:
![](GMail2.gif)

Dieses Passwort muss nun in den Parametern eingetragen werden, also unter
- System, Parameter, E-Mail
Selbstverständlich ist auch der entsprechende GMail Benutzer einzutragen.![](GMail3.jpg)<br>
Um nun den Versand per E-Mail zu testen, wechselst du in den Reiter E-Mail Admin und versuche direkt, also ohne den Umweg über die Automatik des Versanddienstes, durch Klick auf ![](Test_EMAIL_senden.gif) "Test E-Mail versenden" ein entsprechendes E-Mail rauszusenden. In den darunter angezeigten Einträgen ist ersichtlich, ob dies auch funktioniert hat.

## Der Sende-User
Damit die E-Mails versandt werden können, muss sich ein besonderer User an deiner Kieselstein ERP Installation anmelden können.<br>
Dies ist der User lpwebappzemecs, mit dem Passwort lpwebappzemecs.

**Wichtig:**<br>
Dieser User muss <u>**vor**</u> dem Versuch des Versendens einer E-Mail eingetragen sein. Wurde bereits versucht E-Mails zu versenden und diese gehen im Versandauftrag nicht raus, so hilft nur diese erneut zu versenden. Hintergrund ist, dass auch diese Daten in den Versandaufträgen abgespeichert werden.


Sollte es trotzdem nicht gehen, könnten aus den verschiedensten Gründen noch 0-Byte Files "herumliegen". In diesem Falle, den Wildfly stoppen, die 0-Byte Files aus standalone/data löschen und den Server wieder starten.

Wenn der obige User nicht konfiguriert sein sollte, so steht im Serverlog ungefähr folgender Eintrag:

2024-04-26 16:55:10,479 ERROR [org.jboss.as.ejb3.timer] (EJB default - 4) WFLYEJB0020: Error invoking timeout for timer: [id=3e45d60d-ff9e-4fb3-8473-d083cf1f2077 timedObjectId=lpserver.ejb.ShopTimerFacBean auto-timer?:false persistent?:true timerService=org.jboss.as.ejb3.timerservice.TimerServiceImpl@60767998 initialExpiration=Fri April 26 16:55:10 CEST 2024 intervalDuration(in milli sec)=0 nextExpiration=null timerState=CANCELED info=null]: javax.ejb.EJBException: java.lang.NullPointerException
    at org.jboss.as.ejb3.tx.CMTTxInterceptor.handleExceptionInNoTx(CMTTxInterceptor.java:212)
    at org.jboss.as.ejb3.tx.CMTTxInterceptor.invokeInNoTx(CMTTxInterceptor.java:264)
    at org.jboss.as.ejb3.tx.CMTTxInterceptor.notSupported(CMTTxInterceptor.java:316)

Eventuell steht da noch zusätzlich:<br>
2024-04-26 16:59:10,505 ERROR [com.lp.server.shop.ejbfac.ShopTimerFacBean] (EJB default - 9) Das wechseln des Mandants schlug fehl: com.lp.util.EJBExceptionLP: javax.persistence.NoResultException: No entity found for query
    at com.lp.server.benutzer.ejbfac.BenutzerFacBean.benutzerFindByCBenutzerkennung(BenutzerFacBean.java:545)

## Einstellungen für Office365
Um mit dem Office 365 im Auftrag von (SendAs) senden zu können, muss bei jedem Benutzer unter dessen Adresse man senden will, derjenige eingetragen werden, der im Namen des Benutzers senden will.<br>
Z.B. wenn info@kieselstein-erp.org erlaubt dass von office@kieselstein-erp.org gesendet werden darf, dann muss bei office@ die info eingetragen werden (und nicht umgekehrt, was der deutschen Denkweise entsprechen würde).