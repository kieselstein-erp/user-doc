---
categories: ["Dokumentenscanner"]
title: "Dokumentenscan einrichten"
linkTitle: "Dokumentenscan einrichten"
date: 2023-01-31
weight: 1400
description: >
  Einrichten von Dokumentenscan
---

## Scannen mit NAPS2
-   Stichwort: TWAIN Scanner
-   Herunterladen von <https://www.naps2.com/> und komplett installieren
-   nun gibt es im Verzeichnis das NAPS2.exe. Dies starten und den Scanner wie gewünscht konfigurieren und das Profil unter einem guten einfachen Namen anlegen / speichern
-   nun unter Arbeitsplatzparameter, PFAD_MIT_PARAMETER_SCANTOOL das Programm
```
"C:\Program Files (x86)\NAPS2\NAPS2.Console.exe" -p "TW-Brother MFC-J6910DW LAN" -o
```
- eintragen. -p ist das Profil, welches mit dem naps2 erzeugt wurde.<br>
Macht schön komprimierte PDFs, die eine sehr gute Größe (eigentlich Kleinheit bei Farbe und 300 dpi) haben.

- siehe auch <https://www.naps2.com/doc-command-line.html>