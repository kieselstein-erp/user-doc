---
categories: ["Tipps und Tricks"]
title: "Tipps und Tricks"
linkTitle: "Tipps und Tricks"
date: 2023-01-31
weight: 1900
description: >
  Tipps und Tricks rund um dein ERP System
---
Nachfolgend eine lose Auflistung von Lösungen, die sich im Alltag herauskristallisiert haben.

## Memor1 scannt zwar, die App reagiert aber nicht
Mögliche Ursache: Die Pre- und Post-Codes sind aktiviert.
Siehe dazu 
Am Memor1 = Android, Einstellungen, Scanner Settings, Formating, und Pre- und Postfix
Wenn da Einträge drinnen sind, werden diese mitgesendet aber gegebenenfalls nicht angezeigt.
Äußert sich ev. auch dadurch, dass man, wenn man die Artikelnummer editiert, geht es auf einmal.

## Wann wurden Parameter umgeschaltet
Die Veränderung von Mandanten-Parametern kann unter Umständen dramatische Auswirkungen auf das Verhalten deines **Kieselstein ERP** haben.<br>
Daher wird dies im Server.log dokumentiert.<br>
D.h. du findest auf deinem **Kieselstein ERP** Server unter ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\log\ die Server-Log-Dateien, wobei je Tag eine eigene Datei angelegt wird.

In diesen stehen nun im Klartext die Änderungen an den Parametern.

Zusätzlich kannst du auch ganz gezielt danach suchen, wann welcher Parameter umgeschaltet wurde. Wenn so z.B. ![](Parameter_Aenderung.png) der Parameter ABLIEFERUNG_BUCHT_ENDE umgeschaltet wurde, so steht in der Datei server.log (oder in einer älteren)
```
2024-12-14 10:51:10,727 WARN  [com.lp.server.system.ejbfac.ParameterFacBean] (default task-1) Update ABLIEFERUNG_BUCHT_ENDE, 001, 1, 2024-12-14 10:51:10.726, 11, Los-Ablieferung von Terminal bucht automatisch ein ENDE, Los-Ablieferung von Terminal bucht automatisch ein ENDE
```
Damit sieht man:
|Wert|Bedeutung|
| --- | --- |
| Update | Änderung des nachfolgenden Parameters |
| ABLIEFERUNG_BUCHT_ENDE | Parameter der geändert wurde |
| 001 | Mandant, für den diese Änderung gilt |
| 1 | Wert nach der Änderung |
| 2024-12-14 10:51:10.726 | Datum und Uhrzeit der Änderung |
| 11 | Personal_I_ID des Benutzers, der das geändert hat|
| Los..... | erklärender Langtext aus den Bemerkungen des Parameters|

#### Wer hat diesen Parameter umgestellt?
```batch
select c_kbez from part_partner where i_id = (
	select partner_i_id from pers_personal where i_id=Personal_I_ID);
```
Liefert die Kurzbezeichnung der Person, die diesen Parameter umgestellt hat.

#### E-Mail-Versand geht nicht
- [siehe]( {{<relref "/docs/installation/15_email_einrichten/#der-sende-user" >}} )
- [oder auch]( {{<relref "/insider_tips_und_tricks/#email-versand-des-neuen-mandanten-geht-nicht" >}} )

#### Fehlende Parameter für weiteren Mandanten nachtragen
Bei der Anlage neuer Mandanten werden die Mandanten_Parameter nicht automatisch übernommen. D.h. diese werden nur mit ihren Default-Werten verwendet. Um diese nachzutragen, kannst du folgendes Skript verwenden.<br>
```batch
insert into lp_parametermandant(c_nr, mandant_c_nr, c_kategorie, c_wert, t_aendern, personal_i_id_aendern, c_bemerkungsmall, c_bemerkunglarge, c_datentyp)
with existing_params as (
	select lp.c_nr, lp.mandant_c_nr, lp.c_kategorie, lp.c_wert, lp.t_aendern,
	       lp.personal_i_id_aendern, lp.c_bemerkungsmall, lp.c_bemerkunglarge, lp.c_datentyp,
	       ROW_NUMBER() OVER(PARTITION BY lp.c_nr, lp.c_kategorie ORDER BY lp.t_aendern) as order_crit
	from lp_parametermandant lp
)
select lp.c_nr, lm.c_nr mandant_c_nr, lp.c_kategorie, lp.c_wert, lp.t_aendern,
	       lp.personal_i_id_aendern, lp.c_bemerkungsmall, lp.c_bemerkunglarge, lp.c_datentyp
from existing_params lp, lp_mandant lm
where order_crit = 1
  and not exists (select 7 from lp_parametermandant lp2 where lp2.c_nr = lp.c_nr and lp2.mandant_c_nr = lm.c_nr);
```
