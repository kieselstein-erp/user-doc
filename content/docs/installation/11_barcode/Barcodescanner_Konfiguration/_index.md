---
categories: ["Barcode"]
tags: ["Configuration", "Konfiguration"]
title: "Konfiguration verschiedener Barcodescanner"
linkTitle: "Konfiguration"
date: 2023-01-31
weight: 100
description: >
  Konfiguration verschiedener Barcodescanner
---
Da man immer wieder nach den wesentlichen Konfigurationscodes für die Barcodescanner sucht und die vom Hersteller mitgelieferten Scann-Codes natürlich nicht zur Hand hat, anbei die von uns üblicherweise benötigten Codes, ohne jeglichen Anspruch auf Vollständigkeit etc.

Wir setzen in der Regel Barcodescanner von Datalogic ein. Diese bieten eine sehr gute Leseleistung bei einem angemessenen Preis.

## Genereller Reset des Barcodescanners
Wenn dein Scanner überhaupt nicht mehr das macht, was du möchtest, hilft meist der Reset auf Factory Default. Geht auch das nicht, kannst du es mit fünf Sekunden Halten der Scantaste versuchen. Manchmal hilft auch eine Kombination aus Scanner abstecken. Scantaste drücken und halten und dann wieder anstecken und noch immer einige Sekunden (> 5) halten. Der Scanner meldet sich mit einem Spezialbeep und machte einen kompletten Reset.

## Quickscan
Der Quickscan ist ein Scanner für ein-dimensionale Barcodes, gerne auch Strichcodes genannt. Hinweis: Es ist normal, wenn die Strichcodes nicht vom Bildschirm abgelesen werden können. Hier hilft nur ausdrucken. Am Papier lesen die ein-dimensionalen Scanner deutlich besser.
### Factory Default
ACHTUNG: Dieser Reset ist trotzdem von der Schnittstellen-Konfiguration abhängig, also welche du gerade gewählt hast.
![](Quickscann_Factory_default.png)  <br>
### USB COM Standard
Damit wird die COM-Schnittstelle auf deinem PC emuliert. Dies ist der von uns für die Terminals verwendete Modus.<br>
![](Quickscann_USB_COM.png)  
### USB Wedge
Wenn du den Scanner z.B. für die Inventur-Zählliste nutzen möchtest, muss dieser eine Tastatur emulieren. Das sogenannte Wedge-Interface. Dies wird mit folgendem Code aktiviert.<br>
![](Quickscann_USB_Wedge.png)  <br>
**ACHTUNG:** Du musst dazu auch immer dein Tastaturlayout angeben. D.h. wenn z.B. bei der Losnummer anstelle des / ein - kommt, oder statt dem Z ein Y ist das Tastaturlayout falsch.
#### Countrymode, Tastaturlayout
1. Enter Programming Mode<br>
![](Quickscann_Programmingmode.png) 
2. Deutsches Tastaturlayout<br>
![](Quickscann_Layout_Deutsch.png)  <br>
Und danach wieder Enter/Exit Programming Mode

## Gryphon
### USB COM Standard
![](Gryphon_USB_COM.png)  

### USB Wedge
![](Gryphon_USB_Wedge.png)  

#### Countrymode, Tastaturlayout
1. Enter Programming Mode<br>
![](Gryphon_Programmingmode.png)<br>
2. Deutsches Tastaturlayout<br>
![](Gryphon_Layout_Deutsch.png)<br>
Und danach wieder Enter/Exit Programming Mode

## Es geht absolut nichts mehr
Sollte das tatsächlich passieren, was ein Anwender in den 40 Jahren <u>einmal</u> geschafft hat, so gibt es von Datalogic auch noch den Alladin, mit dem der Scanner komplett zurückgesetzt werden kann.