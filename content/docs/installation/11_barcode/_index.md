---
categories: ["Barcode"]
tags: ["Barcode", "Strichcode", "EAN-Code"]
title: "Arbeiten mit Barcode"
linkTitle: "Barcode"
date: 2023-01-31
weight: 1100
description: >
  Wie mit Barcode umgehen
---
Barcodes sind eine praktische Sache. Vor allem in Verbindung mit Serien- bzw. Chargennummern sind sie maßgeblich für die Sicherheit der Erfassung notwendig.

Grundsätzlich unterscheidet man zwei Typen von Barcodes.
- Die sogenannten eindimensionalen Barcodes, oft auch Strichcode genannt. Dazu gehören auch die oft zitierten EAN Codes (European Article Number)
- die zweidimensionalen Barcodes, dazu gehören vor allem Datamatrix und der QR-Code (Quick Response)

| Barcodeart | Vorteil | Nachteil |
| --- | --- | --- |
| Eindimensional | von annähernd allen Scannern lesbar<br>gute Treffsicherheit des einzelnen zu lesenden Codes|reduzierter Zeichensatz, großer Platzbedarf, mit den Kameras der mobilen Geräte oft nur sehr schwer bis gar nicht lesbar |
|Zweidimensional | nur von dazu geeigneten Scannern lesbar, die meist teurer als die eindimensionalen sind<br>fast alle Zeichen darstellbar, auch deutsche Sonderzeichen usw. | bei mehreren Barcodes neben- / untereinander Treffsicherheit nur mühsam gegeben |

Beide Barcodetypen können mit den Reportgeneratoren erzeugt werden.

**ACHTUNG:** Der Datamatrixcode in Verbindung mit Inhalten in denen mehr als zwei * vorkommen, liefern fehlerhafte Codes. Dies ist nur durch teure proprietäre Barcodelibraries zu beheben. Besser dann gleich auf die Verwendung von QR-Code drängen.

Welche Barcodesequenzen werden vom Zeiterfassungsterminal unterstützt?

Die zentrale Definition aller Barcode-Lead-Ins, die von den **Kieselstein ERP** Geräten verwendet werden, findest du [unter](./Barcode%20Liste%20KES.xls) bzw. findest du die [Definition](./Barcode%20Liste%20KES.pdf) hier.

## HTML BDE Terminal
| Lead In | Bedeutung |
| --- | --- |
| $P | Personal |
| $L | Los	|
| $T | Tätigkeit |
| $M | Maschine |
| $V | Los Kombi AG |
| $U | Auftrag Kombi	|
| $A | Auftrag |
| $KOMMT | Kommt Buchung |
| $GEHT | Geht Buchung |
| $UNTER | Unterbrechung = Pause Buchung |
| $ENDE	| Ende der Tätigkeit des Mitarbeiters |
| $ARZT	| Unterbrechung mit der Sondertätigkeit Arzt |
| $KRANK | Unterbrechung mit der Sondertätigkeit Krank |
| $BEHOERDE	| Unterbrechung durch Behördengang |
| $ANZEIGE | Anzeige des aktuellen Zeitsaldos |

## Zeiterfassungs-Terminal-Software


Drucken von Barcode
===================

Barcodes werden für die rasche und sichere Erfassung von Artikel und Seriennummern sowie zur Erfassung von BDE Daten usw. verwendet. Überall wo Auftragsnummern, Tätigkeiten, Maschinenzeiten oder Artikel erfasst werden müssen, können durch die Verwendung von Barcodes entsprechende Vereinfachungen verbunden mit der Reduzierung der Eingabezeit und der Reduzierung der Erfassungsfehler erzielt werden.
Von **Kieselstein ERP** werden die durch den iReport unterstützten Barcodes verwendet. Üblicherweise wird der Code 39 bzw. der Code 128 für die eindimensionalen Barcodes, welche auch Strichcode genannt werden, verwendet. An zweidimensionalen Barcodes kommen der Datamatrix Code und vor allem der QR-Code zum Einsatz.
Um eine entsprechende Bedienerführung und Bedienungssicherheit zu erreichen, ist der Großteil der von **Kieselstein ERP** verwendeten Barcode mit einem sogenannten LeadIn versehen, welcher die Art des Codes definiert.

## Unterstützte Barcodefunktionen
Derzeit sind folgende LeadIn Zeichen für die verschiedenen Geräte definiert:

| Lead In | Bedeutung | HTML-Terminal | ZE-Terminal | ZE-Stift | Mobil |
| --- |  --- |  --- |  --- |  --- | --- |
| $P | Ausweisnummer des Mitarbeiters | x | x | |
| $L | Losnummer | x | x |  |  |
| $Lnn$MAT | $L Losnummer $MAT <br>Materialentnahmebuchung für das Los |  | x |  |  |
| $T | Tätigkeit (Arbeitszeitartikel) | x |  | |
| $M | Maschinen ID (einzelne Maschine) | x | nur für abweichende Maschine | x | |
| $V | Vorgang bestehend aus Los, Maschinen ID, Tätigkeit oder anstelle der Tätigkeit<br>STOP für das Ende der Maschinenzeit oder <br>FT für die Fertigbuchung des Arbeitsganges| x | x | x | |
| $U | Vorgang bestehend aus Auftrag, Tätigkeit | x | x | x | |
| $W | Vorgang bestehend aus Los, Tätigkeit |  | x |  | |
| $ABLIEFERN | Ablieferungsbuchung |  | x |  | |
| $O | Lagerort/Lagerplatz |  |  |  | x |
| $F | Fehlernummer |  |  |  | |
| $A | Auftragsnummer z.B. auf Packliste | x | x | x | x |
| $B | Bestellung Nummer |  |  |  | x |
| $KOMMT | Anwesenheitszeit Kommt | x | x | x | |
| $GEHT | Anwesenheitszeit Geht | x | x | x | |
| $UNTER | Unterbrechungsbeginn oder Ende | x | x | x | |
| $ENDE | Tätigkeits Ende | x | x | x | |
| $ANZEIGE | Anzeige von Zeitsalden des Mitarbeiters | x |  |  | |
| $STOP | Stop der Maschinentätigkeit |  | x |  | |
| $GROESSEAENDERN | Losgrößen Ändern auf Stückzahl am Terminal |  | x |  | |
| $PLUS | Dient der Zusammenfassung von mehreren Losen zu einer Zeiterfassung. Bei der  Endebuchung dieser Mehrfachen Lose, wird die Arbeitszeit zu gleichen Teilen auf die angegebenen Lose gebucht. Nur für die Erfassung von Fertigungs-Losen möglich. | | |  |  |
| $G | Einzelbuchung auf Los mit Arbeitsgang anstelle der Tätigkeit. Code für Arbeitsgang: $Gxx.yy ... xx Arbeitsgang numerisch, yy Unterarbeitsgang numerisch |  |  |  |
| $X0artikelnummer | Anzeige des Artikelstammblattes |   | (x) |   |
| $X1losnummer | Anzeige des Druckes der Nachkalkulation des Loses | x | (x) |   | 
| $L...$MAT | Materialbuchung auf das Los | | x | |
| $I | Artikelnummer mit Seriennummer oder Chargennummer nach der Artikelnummer muss \| und die CNr bzw. CHG kommen. | | | | x 
| Artikelnummer | Artikelnummer ohne LeadIn und ohne SNR, CHG | | | | x
| VDA4992 | VDA 4992 Mat-Label lt. VDA Norm | | | | x
| Spezialstring | Artikel\<Tab>Menge\<Tab>Charge@ | Spezialartikelerfassungsstring mit Menge und Chargennummer. ACHTUNG: Es dürfen KEINE \<Tab> in der Artikelnummer erlaubt sein | x |


Weitere definierte Barcode LeadIns, welche für zukünftige Verwendungen definiert sind. Diese sind derzeit nicht umgesetzt.

| Lead In | Bedeutung | mobile App |
| --- | --- | --- |
| $ARZT | Arzt Beginn oder Ende |  |
| $KRANK | Krank Beginn oder Ende |  |
| $BEHOERDE | Behördengang Beginn oder Ende |  |
| $DIENST | Dienstgang |  |
| $REISE | Dienstreise Beginn oder Ende |  |
| $UEBER | Überstunden Beginn |  |
| $URLAUB | Urlaubsbeginn oder Ende |  |
| $ und Kundennummer | Kundennummer auf Kundenkarte | x |
| $I123456789012345 | Material Ident danach muss |  |
| $$Pdruckerstring | Übersteuerung des Druckers für den Druck<br>steht aktuell nur für Los-Fertigungs-, Ausgabeliste-, Etikette zur Verfügung | (x) |
| $$Losnummer | Mehrfach Losnummer auf der Ausgabeliste für die Verwendung in der VDA Scan App zur gleichzeitigen Ausgabe und Rücknahme von Chargengeführten Artikeln (Reelid) |

Anmerkung:
Dinge wie $BEHOERDE sollten am ZE-Terminal gebucht werden können, aber nur über Sondertätigkeitsbutton, Combobox

ACHTUNG: Es gibt insbesondere für die kombinierten Barcodes zwei Forderungen:
- über den Barcodescanner der verschiedensten Apps Artikelnummer und Chargennummer in einem Zug erfassen, also z.B. $Innnn|chgnr aber
- wie erfasst man dann den Artikel über einen Wedgescanner<br>
Aktuelle Lösung zwei Barcodes andrucken, d.h. der kombinierte für die schnelle Erfassung, NUR die Artikelnummerm um über Wedge im Client schnell auf den Artikel zu springen.

## Barcode Definitionen für Materialbuchungen
### In der LosMaterialbuchung, bei SNR/CHG Artikeln
Für die Losmaterialbuchung stehen in Kombination mit Barcode folgende Möglichkeiten zur Verfügung:
1. Scan der Artikelnummer im Klartext
2. $Innnnn | Seriennummer bzw. Chargennummer
    - hier kann in dem nun geöffneten Dialog weitergescannt werden<br>
        also z.B. $I4711|1234(CR)$I4711|1235(CR)<br>
        Bitte dabei darauf achten, dass die immer der gleiche Artikel ist.
3. ist man im Seriennummern Feld, so kann auch die Seriennummer direkt gescannt werden. Dabei wird die Menge immer mit einem Stück fix angenommen und daher sofort in die Erfassungsliste übernommen
4. ist man im Chargennummern Feld, so kann auch die Chargennummer direkt gescannt werden. Danach muss zusätzlich die Menge eingegeben werden.


## Von den Barcodes unterstützte Zeichen:

| Barcode | Unterstützte Zeichen |
| --- |  --- |
| Code 39 | 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-.$/+%Space |
| Code 128 | !#$%'()0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ |
| QR-Code | QR-Code unterstützt UTF-8, also, vereinfacht gesagt alle Zeichen, die du in **Kieselstein ERP** eingeben können. |

HINWEIS: Code39 kann keine _______

Wenn nun ein Barcode nicht gedruckt werden kann, so überprüfe bitte die Artikelbezeichnung bzw. den zu druckenden Text auf nicht angeführte Zeichen - vor allem Umlaute.

### Drucken von Barcodes aus anderen Programmen:
Manchmal ist es nützlich, Barcodes aus anderen Programmen (Textverarbeitung) zu drucken.

Wenn du eine passende TrueType Schrift besitzt, so kannst du den Code 39 direkt als Schrift verwenden.

Hier ist zu beachten, dass Barcodes noch zusätzliche Steuerzeichen am Beginn und am Ende des Codes besitzen. Diese müssen üblicherweise mitgedruckt, also in deinen Zeichen enthalten sein.

Der Code 39 muss in ** eingebettet sein (=VERKETTEN("*";D85;"*")). Wenn du also die Zahlen 1234 als Barcode ausgeben möchtest, müssen die Zeichen *1234* mit dem Code 39 TrueType Font ausgegeben werden.

### Erzeugen von VDA / MAT Labels
Eigentlich werden die VDA / MAT Labels durch Variablen in den verschiedenen Reports zusammengestellt. Da man manchmal zum Testen schnell einen Barcode braucht, hier der Link zu einer praktischen Seite. https://label.tec-it.com/de Hier kannst du dir den gewünschten Barcode zusammenstellen. Bitte beachte, dass der Barcode nicht normal gedruckt werden kann. Aber: Die modernen 2D Scanner lesen gerade den QR-Code auch sehr gut vom Bildschirm ab.

**Warum werden manche Artikelnummern nicht als Barcode gedruckt?**

Es kommt z.B. beim Ausdruck der Inventur Zählliste dazu, dass manche Artikel nicht als Barcode angedruckt werden.
Ursache ist hier immer, dass in der Artikelnummer für den Barcode ungültige Zeichen enthalten sind.

![](Ungueltige-Barcodes.gif)

So ist in der zweiten Artikelnummer ein Umlaut enthalten, welcher im Code 39 nicht gedruckt werden kann.

Siehe dazu auch [erlaubte Zeichen in der Artikelnummer]( {{<relref "/docs/stammdaten/artikel/#welche-zeichen-k%c3%b6nnen-f%c3%bcr-die-artikelnummer-verwendet-werden" >}} ).

**Barcodes sind schlecht lesbar, können mit einem CCD Scanner nur mühsam gelesen werden**

Für die Akzeptanz der Zeiterfassung, bzw. der gesamten Datenerfassung mittels Barcodes ist das schnelle und sichere Lesen der Codes von enormer Bedeutung. Um dies zu erreichen, beachte bitte Folgendes:

Die Codes müssen etwas schmäler sein als der CCD-Scanner. Wir setzen üblicherweise Touchscanner ein, also bitte sehr nahe an den Barcode heranführen und dann den Scanknopf drücken. Es hat sich bewährt, während des Scannens eine leichte Bewegung nach unten zu machen.

Zu den Codes selbst: Diese MÜSSEN vor und nach dem eigentlichen Code eine Ruhezone haben. D.h. links und rechts vom Barcode darf im Bereich von ca. 1 cm kein Druck sein. Bitte beachte, dass auch Papierkanten als Linien gelesen werden und daher den Barcode verfälschen können, unlesbar machen.

Oft werden die Barcodes auch in Klarsichtfolien eingelegt. Es gibt nur wenige Klarsichtfolien, die für den Einsatz von Barcodes geeignet sind. Bitte achte darauf, dass die Folien
1. nicht spiegeln und
2. trotzdem einen klaren/scharfen Blick (für den Scanner) auf den Barcode ermöglichen.

Wenn Codes in Folien schlecht lesbar sind, so prüfe bitte zuerst, ob der Barcode direkt vom Papier gut lesbar ist. Ist dies der Fall, so muss eine andere Klarsichtfolie verwendet werden.

Der Scanvorgang sollte bei normalem Licht erfolgen. Bei direkter Sonneneinstrahlung konnten wir bisher keinen Barcode vernünftig einlesen.

Für Einstellung der Barcodescanner [siehe auch]( {{<relref "/fertigung/zeiterfassung/bde_station" >}} ).

#### Können auch QR-Codes gedruckt werden oder Datamatrix-Codes?

A: Ja es können auch QR-Codes gedruckt werden.
Grundsätzlich können auch Datamatrix-Codes gedruckt werden. Leider ist es so, dass in allen verfügbaren Libraries (Stand Februar 2016) Fehler bei der Verwendung von mehreren * (Sternchen), vor allem wenn diese aufeinanderfolgend sind, enthalten sind. Wir raten daher anstatt des Datamatrixcodes den QR-Code zu verwenden. Der QR-Code hat zusätzlich den Vorteil, dass die mobilen Apps für die Decodierung des QR-Codes optimiert sind und diesen sehr schnell lesen können. Bitte beachte, dass dafür zumindest auch Java 8 auf deinem **Kieselstein ERP** Server und auf den Client-Rechnern eingerichtet sein muss.

Im Unterschied zu den 1D-Barcodes, Strichcodes, können im QR-Code bis zu ca. 4300 Zeichen eingebunden und decodiert werden. Dies ist auch abhängig vom zur Verfügung stehenden Platz und der Auflösung des Druckers und der Qualität der ScanEngine des Barcodelesers.

#### Welche Drucker / Druckertreiber sollten für den Druck von Barcodes verwendet werden?
Von **Kieselstein ERP** werden Barcodes über den Druckertreiber direkt an den Drucker gesandt. Das bedeutet, dass die Qualität des Barcodes auch von der Qualität des Druckertreibers abhängig ist. In Kombination mit den Barcodedruckern hat sich bewährt anstelle der original Druckertreiber (ZDesigner) Druckertreiber der Firma [Seagull](http://de.seagullscientific.com/drivers/windows-printer-drivers/) zu verwenden. Achte bitte in jedem Falle darauf, dass die Auflösung deines Druckers ausreichend für die Wiedergabe der Barcodezeichen ist. Die oft angetroffenen 8Dot Drucker haben damit 203 dpi, 12Dot haben 300 dpi. Alles unter 300 dpi liefert meist unzufriedenstellende Ergebnisse.

{{% alert title="Wichtig" color="primary" %}}
Prüfe die Lesbarkeit der Etiketten bevor du mehrere Etiketten auf den verschiedensten Paketen, Lagerplätzen usw. anbringst. Wir haben da schon einige interessante Effekte gehabt. Prüfe jede Etikette auch in der wirklichen Anwendung, bei Sonneneinstrahlung usw..<br>
Es mussten leider schon ganze Lageretikettierungen neu gemacht werden, da dies eben nicht beachtet wurde. Das sollte man vermeiden.
{{% /alert %}}



Detailbeschreibung zur Installation von Barcodedruckern

Da wir immer wieder danach gefragt werden, nachfolgend eine kompakte Beschreibung was bei der Einrichtung von Barcodedruckern im Besonderen zu beachten ist, wobei wir hier von Windows-Betriebssystemen ausgehen. Die Installation in Linux bzw. MAC OSX Systeme setzt immer sogenannte CUPs Treiber voraus. Solltest du Bedarf in dieser Umgebung haben, wende dich vertrauensvoll an uns.

-   Wenn Drucker von mehr als einem Rechner erreicht werden sollten, nutze nur Drucker, die einen vollwertigen Netzwerkanschluss besitzen. Für Linux/OSX Umgebungen, können ausschließlich Drucker mit IP Anschlüssen verwendet werden.

-   Achte auf eine ausreichende Auflösung, wie oben beschrieben bitte mindestens 12Dot

-   Lade die neueste Version der kostenlos zur Verfügung gestellten Druckertreiber der Firma [Seagull](http://de.seagullscientific.com/drivers/windows-printer-drivers/).

-   Installiere den richtigen Treiber, achte dabei auch auf Kleinigkeiten in der Bezeichnung.
    So beachte bitte bei Zebradruckern auf die teilweise unterstützten unterschiedlichen Druckersprachen. Manche Zebradrucker können nur ZPL (Zebra Programing Language) manche nur oder zusätzlich EPL (Epson Programming Language). Nur wenn dein Drucker die richtige Sprache spricht, wird er auch das Gewünschte ausdrucken.

-   Wähle nun die Druckereigenschaften (Rechtsklick auf das Drucker-Symbol unter Geräte Drucker, etwas vom Betriebssystem abhängig)

-   Du siehst nun die Werbeinfo von Seagull und vielen Dank für die guten und stabilen Treiber. Wechsle auf den Reiter **Allgemein** und klicken unten auf **Einstellungen**

-   Nun kommt ein weiteres Fenster, wählen hier den Reiter **Seite einrichten**.

-   Klicke auf Neues (Etikett) oder bearbeite das bestehende durch Klick auf Bearbeiten. Gib  Name und Abmessungen in mm ein.
    Bitte beachte, dass die eingegebenen Etikettenabmessungen um eine Kleinigkeit **größer** (höher / breiter) als in deinem **Kieselstein ERP** Etikett sein müssen.

-   Speichere die Einstellungen durch Klick auf Ok und (im eigentlichen Eigenschaftenfenster) auf Übernehmen.

-   Wähle nun Testseite drucken.
    Es muss der Testdruck exakt am linken oberen Rand der Etikette gedruckt werden.
    Sollte dies nicht so sein, bitte prüfe deine Etikettendefinitionen.

    -   Voraussetzung dafür ist, dass die Startposition deiner Etikette vom Drucker her richtig eingestellt ist.
        Sollte dies nicht so sein, betätige bitte einmal den Formfeed / Seitenvorschub.

    -   Manchmal ist jedoch auch der Positionserkennungssensor falsch oder die Etikettenbahn wurde nicht durch / unter dem Sensor durchgeführt oder in den **Einstellungen** im Reiter **Etikett** ist bei den Einzugsoptionen ein falscher Wert ausgewählt.

Was noch so eintreten kann

-   Der Drucker druckt zu hell.<br>
    Stelle in den **Einstellungen** unter **Optionen** die Helligkeit etwas höher ein. Taste dich bitte schrittweise an den Idealwert heran. Wenn zu schnell auf zu heiß gedreht wird, kann das TTF (ThermoTransfer) Band schmelzen, also abreissen.
    Eine mögliche Abhilfe ist auch die Etikette langsamer zu drucken, reduziere also im gleichen Reiter die Geschwindigkeit.<br>
    Optimiere die beiden Werte für deine Bedürfnisse.

-   Prüfe nach der Einrichtung der Drucker, ob das gewünschte Etikett wirklich in der richtigen Lage und in der richtigen Größe / Position ausgedruckt wird.
    Beachte, dass die Einstellungen der Etiketten Benutzerabhängig hinterlegt werden. D.h. stelle sicher, dass der/die Druckende auch unter dem Namen angemeldet ist, unter dem du die Etikette eingerichtet haben.

#### Kann man Etikettendrucker per Fernwartung einrichten?
Technisch gesprochen ist das kein Problem. Nachdem aber auch bei den Etiketten geprüft werden muss ob diese in der richtigen Lage usw. ausgedruckt werden, empfiehlt es sich dies vor Ort durchzuführen oder von einem fähigen Menschen durchführen zu lassen.

#### Können Etikettenformulare per Fernwartung eingerichtet werden?
Auch das, technisch kein Problem. Wenn dies von dir als Anwender wirklich gewünscht wird, weil z.B. die Reisezeiten einfach zu lange wären, bitte übermittle uns einen Etikettenentwurf, der wirklich auf den Millimeter stimmt. Nur so können wir halbwegs effizient die Etiketten per Fernwartung einrichten. Es muss dazu in jedem Falle jemand vor Ort sein, der den Ausdruck der Etikette(n) überprüft und dann in der Lage ist exakte Angaben zu machen. Unter exakt verstehen wir wirklich, dass die Verschiebung der Felder in mm angegeben wird. Angaben wie etwas größer, ein bisschen nach links signalisieren uns zwar deinen Wunsch, verursachen aber ein, nein nicht so weit, noch größer, also eine Vielzahl an Durchläufen.

## Einlesen von Zahlschein-QR-Codes
Um dies auch einlesen zu können benötigst du einen Wedge-fähigen 2D Barcodescanner.
Bei den von uns verwendeten Gryphon GD4400, muss dieser zusätzlich auf Wedge Send Control Characters = 01 programmiert werden, um auch die im QR-Code enthaltenen Steuerzeichen an die Empfangsroutine durchzureichen.