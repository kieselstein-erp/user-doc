---
categories: ["Knowledge"]
title: "Knowledge"
linkTitle: "Knowledge"
date: 2023-01-31
weight: 4000
description: >
  Praktisches Wissen rund um dein Kieselstein ERP
---
Aufgrund des Wunsches der Mitglieder, eine lose Sammlung an praktischen Bedienungsdingen für Betriebssysteme und Datenbank, die man selten aber doch immer wieder einmal braucht.

Gedacht ist dies für Menschen, die ihre Systeme selbst administrieren und damit auch wissen was sie tun und die Verantwortung dafür übernehmen können und wollen.

## Windows
#### Windows Defender
Als Virenscanner einrichten, dass so aktuell wie möglich

	Signatur Update Intervall
		Registry Hive	HKEY_LOCAL_MACHINE
		Registry Path	Software\Policies\Microsoft\Windows Defender\Signature Updates
		Value Name	SignatureUpdateInterval
		Value Type	REG_DWORD
		Enabled Value	1				1 - 24 Stunden
		Disabled Value	0
	Bearbeiten und Gruppenrichtlinien Editor /Windowstaste, Gruppenrichtlinie
		Computerkonfiguration
			Administrative Vorlagen
				Windows-Komponenten
					Microsoft Defender Antivirus
						Aktualisierung der Sicherheitsinformationen
							Intervall für die Suche nach Updates der Sicherheitsinformationen

#### DNS Puffer neu initialisieren
ipconfig /flushdns

#### Filezugriff
Zugriff von alten Windows-Rechner (XP aber teilweise auch Linux) auf Win7 und höher Freigaben
Auf der Win10 (dorthin, wo man den Zugriff möchte) im Systemsteuerung, Windows Programme und Features, Windowsfeatures, Unterstützung für SMB 1.0 suchen und zumindest den SMB 1.0 Server freischalten.<br>
Danach den Rechner neu starten.

#### laufende Prozesse
netstat -nao<br>
liefert auch die Prozessnumer, um damit über den Taskmanager den richtigen Javaprozess killen zu können.

#### Druckaufträge bleiben hängen.
Passiert komischerweise seit einiger Zeit auf den aktuellen Windows10 usw. Installationen.
Ursache: es scheint eine temporäre Datei hängen zu bleiben.<br>
Lösung:
- Spoolerdienst = Druckwarteschlange beenden
- Drucker abschalten
- c:\windows\system32\spool\PRINTERS
  alle Dateien entfernen.<br>
  Es müssen sich alle Dateien lösche lassen.<br>
  Wenn nicht läuft vermutlich der printfilterpipelinesve.exe noch. Dieses über den Taskmanager beenden.
- Drucker wieder einschalten
- Druckwarteschlange wieder starten
- Wenn keine Druckjobs anstehen, muss dieses Verzeichnis leer sein.

#### Freigaben
net share liefert alle Freigaben dieses Rechners<br>
sieht man auch in der Computerverwaltung, System, freigegebene Ordner, Freigaben und kann da die Freigaben auch löschen

#### Kein Zugriff auf Freigaben möglich
Der Status des Netzwerkes hat sich wieder mal bei einem Update auf öffentliches Netzwerk geändert.

Man geht in die Einstellungen und wechselt dort zu "Netzwerk und Internet". <br>
Unter Status klickt ihr auf "Verbindungseigenschaften ändern". <br>
Anschließend könnt ihr durch die Aktivierung des jeweiligen Schalters von "Öffentlich" in "Privat" wechseln oder umgekehrt<br>

**oft ist das aber nicht genau so, dann kann man**<br>
- Status, weiter auf Ethernet
- da findet man dann das verbundene Netzwerk und da dann auf das Symbol klicken
- und das Netzwerkprofil auf "Privat" ändern
Bei Netzwerkstatus (im Dialog Status) muss dann "Privat" stehen<br>
Dann geht auch der RDP Zugriff wieder.

#### Schnellzugriff Einstellungen
Mit der Tastenkombination "Windows + I" die Windows 10-Einstellungen und wechselt dort zu "Netzwerk und Internet". 

#### Aktuelles Datum und aktuelle Uhrzeit in den Dateinamen einsetzen
[siehe](http://ulrichhanke.de/MicrosoftTipps/Datum_und_Uhrzeit_im_Dateinamen.html)

Manchmal ist es sinnvoll, in einen Dateinamen auch das Datum der Erstellung mit hineinzunehmen. Damit kann man dann die verschiedenen Versionen der Datei sehr einfach und schnell unterscheiden.

> Es gibt im Kommandozeilenmodus von Windows die Variablen<br>
%DATE% = Datum in der Form `TT.MM.JJJJ`<br>
und<br>
%TIME% = Zeit In der Form HH:MM:SS<br>
Darüber hinaus kann man auch noch das Format und die Reihenfolge der einzelnen Teile des Datums und der Uhrzeit bestimmen:<br>
%DATE:~6,4% = Jahreszahl vierstellig<br>
%DATE:~3,2% = Monatszahl zweistellig<br>
%DATE:~0,2% = Tageszahl zweistellig<br>
%TIME:~0,2%= Stundenzahl zweistellig<br>
%TIME:~3,2% = Minutenzahl zweistellig<br>
%TIME:~6,2% = Sekundenzahl zweistellig<br>
Hier ist ein Beispiel:<br>
ren Test.txt Test"%DATE:~6,4%.%DATE:~3,2%.%DATE:~0,2%-%TIME:~0,2%.%TIME:~3,2%-%TIME:~6,2%".txt<br>
ergibt<br>
Test2011.12.05-21.13-37.txt<br>
Man kann mit diesen Variablen auch Ordner erstellen, kopieren oder umbenennen.<br>
Beispiele:<br>
Einen Ordner erstellen md "%date%-%time:~0,2%-%time:~3,2%-%time:~6,2%"<br>
xcopy ordner "%date%-%time:~0,2%.%time:~3,2%-%time:~6,2%"<br>
>

#### USB Stick auf Read Only setzen
Method 1. Manually Remove Read-only with DiskPart CMD

    Click on your "Start Menu", type cmd in the search bar, then hit "Enter".
    Type command diskpart and hit "Enter".
    Type list disk and hit "Enter". (
    Type the command select disk 0 and hit "Enter".
    Type attributes disk clear readonly and hit "Enter".

Zum Setzen: attributes disk set readonly eingeben.

#### Speichertest integriert
mdsched.exe

#### Zuordnung MAC Adressen / IP Adressen
arp -a
	liefert alle seit, ... aufgerufenen IP Adressen und deren MAC Adresse dazu

#### BatchCommandos Übersicht
[siehe](https://www.script-example.com/themen/cmd_Batch_Befehle.php)

#### Autostart abschalten
Autostart auf Win10 abschalten mit Autoruns aus Sysinternals [Download von](https://learn.microsoft.com/de-de/sysinternals/downloads/autoruns)<br>
Als Administrator ausführen<br>
**Vorsichtig** abhaken der nicht gewünschten Funktionen

#### Windows Rebooten mit Sprung ins Bios
Ctrl+left+Shift+Windows und dann Reboot mit der Maus<br>
Geht beim Neustart ins Bootmenü

#### Drucken über Remotedesktop
- Auch Thinstuff druckt normal über den RDP Printer
- geht normalerweise problemlos
- Zum Testen als erstes auf den umgeleiteten Microsoft PDF Drucker drucken<br>
  Dieser legt die Dateien im RDP Aufrufverzeichnis ab<br>
  Geht auch über mehrere Ebenen dann auf die Wurzel der RDP Verbindung
- mit Konica Minolta
  - Es muss der SMB Client & Server 1.0 eingerichtet sein
  - Es sollten die aktuellsten Treiber von Konica Minolta AT!!! verwendet werden
  - Ev. den Drucker LOKAL freigeben
- Win10 / Win11
  - getestet auf Win10 mit Thinstuff und lokaler Rechner Win11

#### Schnelldump der KIESELSTEIN Datenbank
ohne Bilder, Versandaufträge usw.. Also klein und schnell erstellt<br>
"?:\Program Files\PostgreSQL\14\bin\pg_dump.exe" -Fc -h localhost -U postgres --exclude-table-data=public.ww_artikelkommentarspr --exclude-table-data=public.rekla_reklamationbild --exclude-table-data=public.lp_entitylog --exclude-table-data=public.lp_installer --exclude-table-data=public.lp_usercount --exclude-table-data=public.lp_versandauftrag --exclude-table-data=public.lp_versandanhang --verbose -f KIESELSTEIN_SMALL.backup KIESELSTEIN

## Linux
### allgemein

| Zweck | Befehl | Wirkung / Beschreibung |
| --- | --- | --- |
| File finden | find -iname \*hba*.* | findet pg_hba.conf |
| Owner ändern | chown –R root report | Ändert ab report und tiefer (-R) den Owner auf root.<br> Danach können z.B. die Reports über den WinScp einkopiert werden |
| Direkt Rechte ändern | chmod -R 777 verzeichnis | Bedeutet im Verzeichnis dürfen alle alles |
| User einer Gruppe hinzufügen | usermod -aG Gruppe User |
| als root arbeiten | su root | und Passwort |
| als vollwertiger root arbeiten | su - root | nur damit verhält sich das OS, wie wenn man sich als root angemeldet hätte |
| Service ein / ausschalten | chkconfig jboss on oder off | Beim Start wird der Service ausgeführt oder eben nicht |
| Liste der installierten Services | systemctl list-unit-files | |
| Liste filtern | grep xxxx | listet nur Zeilen die xxxx enthalten |
| Editor | vi filename | Mühsam zu bedienender standard Linux Editor<br>vi<br>Commandos mit : einleiten, also<br>:q! -> raus ohne änderungen<br>:wq -> raus und schreiben|
| Editor | nano filename | ausreichend komfortabler Editor. Ist auf den meisten Linuxen installiert |
| Dienste starten / Stoppen | systemctl start/stop/restart Dienstname | übliche Dienstnamen:<br>kieselstein<br>postgresql jeweils mit TAB vervollständigen
| Neues Verzeichnis | mkdir | erzeuge neues Verzeichnis |
| Directory Listing | ls -al | Directory Listing mit allen Infos |
| Verzeichnis leeren | rm * -R | -> im Ubuntu leere das gesamte Verzeichnis |
| Verzeichnis löschen | rmdir Verzeichnis | löscht ein leeres Verzeichnis |
| Verzeichnis umbenennen | mv old new | old auf new umbenennen |
| ausführliches Directory Listing | ls -alh | List Directory alle Attribute Langformat [Human Readable](https://wiki.ubuntuusers.de/ls/)
| Freier Plattenplatz | df -hl | zeigt freien Speicherplatz des lokalen Dateisystems |
| Welche IP hört auf welchem Port | netstat -anlp | listet alle laufenden IP-Listener und die zugehörigen Programme, die da horchen |
| Prozesse auflisten | ps -axf | listet alle Prozesse und deren Aufrufparameter<br>z.B. ps -axf \| grep -i test<br>liefert den Prozessstatus mit den Aufrufparametern (-i = ignore Case beim Wort danach) |
| Prozess beenden | kill -9 PID | Beendet den Prozess (PID) in jedem Falle |
| Aufgabenplanung | crontab -e | bei Ubuntu: /etc/crontab (Ist vi, d.h. mit :wq beenden |
| File laufend anzeigen | tail Filename -f | zeigt andauernd die aktualisierten Inhalte des (Log-) Files an |
| Umgebungsvariablen auslesen | env | |
| Umgebungsvariablen auslesen | env \| grep JAVA_HOME | Zeigt den Wert (Pfad) von Java_Home |

#### pg_hba.conf 
liegt je nach Betriebssystem / Linux wo anders
-	Ubunut	etc/p.....
-	var/lib/p...
-	usr/lib/p...

#### Installation der MS-Fonts (Arial usw.)
> apt-get install ttf-mscorefonts-installer<br>
> Anmerkung: Die EULA muss bestätigt werden. Damit man zum OK kommt, mit die TAB-taste drücken und dann Enter bzw. Space<br>
> sudo fc-cache -fv

#### mount
- a.) das Verzeichnis (/mnt/datastore) muss es geben, sonst mit mkdir anlegen
- b.) das Filesystem muss es auch geben, muss am Linux verfügbar sein
- c.) wenn im PW !, dann mit \ escapen
- d.) mount -t nfs 192.168.0.40:/opt/kieselstein/postgres_datastore_xxxx/ /mnt/datastore
    nfs = Filesystem, der abschließende Slash ist wichtig<br>
WICHTIG: Es muss dies auch freigegeben werden. Dazu im WebMin(Port 10000) unter Netzwerk NFS Exporte eintragen, von welchem Rechner zugegriffen wird

### Debian
ip adress für ifconfig<br>
apt install htop<br>
shutdown -> systemctl poweroff<br>

### Ubuntu

#### Dateien mit Doppelklick starten
Rechte Maus auf die Datei, unten Eigenschaften, Reiter öffnen mit<br>
  nun unter weitere Anwendungen, Anwendung starten auswählen

#### useradd ... neue User anlegen
angelegte User findet man im File /etc/passwd
passwd BENUTZERNAME und dann das neue Passwort eingeben

#### ssh Zugriff nicht eingerichtet (ubuntu 14.04 LTS)
  apt-get install openssh-server .... herunterladen<br>
  danach starten ... /etc/init.d/ssh restart

#### Netzwerkadressen 
##### ändern über Console (wenn installiert)
- nmtui (ist eine Terminal gui für Netzwerkeinstellungen)
- yum install nmtui
##### Grafisch
IP-Adresse kann man im Ubuntu über die Einstellungen ändern

### Suse
Java liegt gegenüber allen anderen Linux Distributionen unter

### CentOS (RedHat)
Datum stellen:
> date MMDDhhmm[[CC]YY][.ss]<br>
> date MMTThhmmJJ
>
hwclock -w ... schreibt die gesetzte Uhrzeit in die Hardwareclock

yum install htop

#### Small-Backup der Kieselstein 
pg_dump -Fc -h localhost -U postgres --exclude-table-data=public.ww_artikelkommentarspr --exclude-table-data=public.rekla_reklamationbild --exclude-table-data=public.lp_entitylog --exclude-table-data=public.lp_installer --exclude-table-data=public.lp_usercount --exclude-table-data=public.lp_versandauftrag --exclude-table-data=public.lp_versandanhang --verbose -f KIESELSTEIN_SMALL.backup KIESELSTEIN

#### Backup der Kieselstein 
pg_dump -Fc -h localhost -U postgres -f ./KIESELSTEIN.backup KIESELSTEIN

#### TCPDUMP
TCPDUMP, also wie laufen denn so die Pakete sehr tief unten
sudo tcpdump 'tcp port 8280' -A -l

## MAC
Wenn man im Finder verschiedene Devices usw. nicht findet, dann am Desktop auf
Gehe Zu (Computer) und dann dieses Device links reinziehen. Ab dem Zeitpunkt ist es da.

Ab OS X Version 8? steht der Launcher zum Starten der Dienste zur Verfügung:<br>
launchctkl start/stop dienst<br>
mit list sieht man alle Dienste und die die eine PID haben laufen<br>
start durch: launchctl load Dienst / Pfad auf plist<br>
stop durch: launchctl unload Dienst / Pfad auf plist<br>

find / -name xxx*.* findet alle Dateien ab Root

Rechtsklick mit der Maus bringt Einsetzen (aus der Zwischenablage)

### Einrichtung
- Zugriff auf die plist: Im Finder zeigen, Paket Inhalt zeigen, Content, info.plist
- Zugriff auf die Info.plist
  Rechts auf Icon, Paketinhalt zeigen, Content, info.plist, rechts-Klick, öffnen mit Texteditor

## SQL, PostgresQL
### Wechsel von pgAdmin 7 auf 8
Es verschiebt sich der Pfad der Runtime. Damit geht gegebenenfalls die nachträglich manuell ergänzte Pfad-Definition nicht mehr Windows, System, erweiterte Systemeinstellungen, Umgebungsvariablen

### Restore
pg_restore -c -h localhost -U postgres -d KIESELSTEIN KIESELSTEIN.backup >> restore.log
Vorher die KIESELSTEIN z.B. über pgadmin oder mit createDB aus ..\database anlegen

### noch einsortieren
Postgres reload im laufenden Betrieb, z.B. wenn IP Adressen nicht freigeschaltet sind
  /etc/postgresql/14/main/pg_hba.conf
  su postgres
  /usr/lib/postgresql/14/bin/pg_ctl -D /var/lib/postgresql/14/main reload

Aufruf von psql.exe<br>
a.) es reicht aus, dem pgadmin das runtime Verzeichnis zu kopieren<br>
b.) bei der Angabe des Hostnamens entscheidet das führende Slash, ob Socket Communication (was im Linux nicht unterstützt wird)
    wenn --host OHNE / angegeben wird, wird eine TCPIP Kommunikation angenommen, die dann nur mehr über das pg_hba.conf gesteuert wird<br>
c.) im Debian und Postgres 14 ist die pg_hba.conf im etc

Postgres Password Reset des Users Postgres [Siehe auch](https://www.postgresqltutorial.com/postgresql-administration/postgresql-reset-password/)
- C:\Program Files\PostgreSQL\12\data\pg_hba.conf
- sichern der pg_hba.conf
- Ändern aller Connection von md5 auf trust
- Restart des Postgres Dienstes
  über die Dienste oder mit pg_ctl -D "C:\Program Files\PostgreSQL\12\data" restart
  The "C:\Program Files\PostgreSQL\12\data" is the data directory.
- Connect to PostgreSQL database server using any tool such as psql or pgAdmin:
  psql -U postgres
  PostgreSQL will not require a password to login.
- Setzen des neuen Passwords für den User Postgres
  postgres=# ALTER USER postgres WITH PASSWORD 'new_password';
- Wiederherstellen der original pg_hba.conf
- Restart des Postgres Dienstes

Rest des PGAdmin Master-Passwords [Siehe auch](https://www.pgadmin.org/docs/pgadmin4/development/master_password.html)
- Beim Anmelde Dialog, bei dem das Master Password abgefragt wird, einfach Reset Master Password anklicken.<br>
Damit werden alle, in einer lokalen SQLite DB abgelegten Passwörter gelöscht.

#### Datentypen
numeric -> BigDecimal -> Sum liefert immer numeric und damit BigDecimal

#### Uhrzeit rechnen
Wenn man Uhrzeit1 von Uhrzeit2 abziehen will, und beides sind Timestamp without timezone, dann select extract(epoch from danach.t_zeit) from pers_zeitdaten
liefert nur Sekunden als numeric, mit denen kann man dann aber einen Sum machen.

#### mehrere Fields mit Trennzeichen zusammenhängen
SELECT string_agg(c_lagerplatz::text, ',') AS Name FROM ww_artikellagerplaetze 
inner join ww_lagerplatz on ww_lagerplatz.i_id=ww_artikellagerplaetze.lagerplatz_i_id
where artikel_i_id=(select i_id from ww_artikel where c_nr='ABCD1234')

#### suchen
Symbol 			Meaning
LIKE '5[%]' 	5%
LIKE '[_]n' 	_n

#### Null-Werte auf definierten Wert setzen
COALESCE

coalesce geht nur, wenn ein Ergebnis kommt, kommt keines, also im PGadmin keine Zeile, dann wirkt dies nicht. Daher immer so aufbauen, dass ein Null-Ergebnis überhaupt theoretisch kommen kann.
> Z.B mit left outer join oder auch mit<br>
select coalesce(max(c_text), 'Referenznummer') from lp_text where ...<br>
was dann immer ein Ergebnis für den max bringt, gegebenenfalls null.

#### Zu Datum Tage dazuzählen
date+1 geht nicht immer<br>
Daher besser:
> (ww_artikelreservierung.t_liefertermin - interval '1' day * COALESCE(part_kunde.i_lieferdauer,2)) as t_liefertermin verwenden

#### Es geht auch aus einem Timestamp
 select DATE(pers_zeitdaten.t_zeit) || ' 00:00:00', * from pers_zeitdaten where i_id = 4003090
 select to_char(t_zeit, 'YYYY-MM-DD 00:00:00'), * from pers_zeitdaten where i_id = 4003090

Mit dem to Char kann noch wesentlich mehr gemacht werden [siehe](https://www.postgresql.org/docs/current/functions-formatting.html)<br>
UND WICHTIG: Will man z.B. zwei Timestamps vergleichen, ob sie am gleichen Tag sind, dann einfach
DATE(pzd.t_zeit) = DATE(pers_zeitdaten.t_zeit)

#### Wochentagsnamen aus einem Timestamp
> select to_char(t_datum, 'Day'), * from pers_betriebskalender 

gibt man als Formater nur Dy an, so kommen die Tagesnamen mit den ersten beiden Buchstaben. In der Regel in Englisch.<br>
Mit substring(to_char(t_datum, 'Day'),1,1) != 'S' dann das Wochenende herausfiltern

#### Tabellen zusammenhängen
Union all<br>
die Spaltennamen durch das as müssen alle gleich sein

#### Anzahl der unterschiedlichen Einträge der Maschinen_I_ID's zählen
"select count(distinct(MASCHINE_I_ID)) from PERS_MASCHINENZEITDATEN where LOSSOLLARBEITSPLAN_I_ID="+$F{lossollarbeitsplan_i_id}+" "+

#### Anzahl der unterschiedlichen Artikel
z.B. im Los<br>
> select count(distinct artikel_i_id) from fert_lossollmaterial<br>
> inner join fert_los on fert_los.i_id=fert_lossollmaterial.los_i_id<br>
> inner join ww_artikel on ww_artikel.i_id=fert_lossollmaterial.artikel_i_id<br>
> where fert_los.c_nr like '24%'<br>
> and ww_artikel.artikelart_c_nr = 'Artikel';

#### Anzahl der unterschiedlichen Stücklisten in den Losen verwendet
> select count(distinct stueckliste_i_id) from fert_los where fert_los.c_nr like '24%';

#### Felder in der Datenbank finden
Man hat immer wieder die Herausforderung, dass man in einer Installation vor zig Jahren, Felder übersteuert hat. Und dann will man auch noch wissen, in welcher Tabelle, denn das gesuchte Feld ist.

In diesem Falle wollte der Anwender die Mindesthaltbarkeit, die man wunderbar im Artikel sieht, auf verschiedenen Formularen angedruckt haben. Es gibt aber diese Mindesthaltbarkeit in der Datenbank nicht. Wie also finden.
- Suchen der Übersteuerung des Textes<br>
```
select * from lp_text where c_text like 'Mindest%' -->> garantiezeit
```
- Suchen in welchem Datenbankfeld das nun tatsächlich enthalten ist
```
SELECT table_name, * FROM information_schema.columns where column_name LIKE '%garant%' and table_name like 'ww_%'
```

#### mit Vorzeichen rechnen
sum(case when Istmenge + Lagerstand < Sollmenge then 0 else 1 end)

## Report
[siehe]( {{<relref "docs/installation/10_reportgenerator/formelsammlung"  >}} )

## Libre Office, Calc
Interessante Funktionen und deren Namen

| Funktion | Beschreibung |
| --- | --- |
| Abrunden | Rundet immer die angegebenen Stellen ab |
| Runden |Rundet mathematisch, vermutlich nicht Bankers Rounding |
| Aufrunden	| Rundet immer auf |
| VRunden | klingt wie Modulo |
| suchen | Suchzeichen und Zelle. Wenn nichts gefunden liefert #WERT! |
| wennfehler | Zelle die zu lesen ist, steht hier ein Fehler (#WERT!) dann wird die Zahl/Wert.. dahinter ausgegeben<br>Z.B. wenn bei Suchen das Zeichen nicht kommt, dann z.B. 0 (=wennfehler(E2;0))	entweder der Wert aus E2, wenn aber Fehler, dann 0 |
| Teil | Substring mit Zelle; Beginn; Länge |
| Verketten | Zusammenhängen mehrerer Stringfelder zu einem String. Nutze ich gerne um SQL Strings zu erzeugen |

## KI, AI
Die sogenannte künstliche Intelligenz

Der Versuch, Menschen in einen Datensatz zu verwandeln, ist Teil einer Kultur des Todes, die alles, was Leben ausmacht<br>
> Spontaneität, fühlendes Erleben, Selbstorganisation und Kreativität,

durch Abstraktion und Berechnung ersetzt.<br>
Ihr Fluchtpunkt ist ein wüstenartiger Planet, auf dem einsam im dunklen Weltall ein blinkender Riesenrechner steht, der anzeigt, wie viel Geld er gerade verdient.

> Fabian Scheidler<br>
> Der Stoff aus dem wir sind<br>
> 2.Auflage 2021

## Datenbank
Die Tabellennamen in der Datenbank sind logisch strukturiert. Somit kann man, mit ein wenig SQL Wissen, sehr einfach die entsprechenden Zusammenhänge heraus finden.<br>

Die Bedeutung der Anfangsbuchstaben der Tabellen sind:
| Tabellenname | Bereich |
| --- | --- |
| anf | Anfrage |
| angb | Angebot |
| as | Angebotsstückliste, inkl. Einkaufsanfragestückliste |
| auft | Auftrag |
| auto | Die Tabellen der Automatikjobs|
| bes | Bestellung |
| er | Eingangsrechnung |
| fb | Finanzbuchhaltung |
| fc | Forecast |
| fert | Fertigung -> Losverwaltung |
| is | Instandhaltung |
| iv | Inseratenverwaltung |
| kue | Küche |
| lp | Allgemeine Tabellen |
| ls | Lieferschein |
| media | Die "Bilder" zu den jeweiligen Belegpositionen |
| part | Partner inkl. Kunden, Lieferanten |
| pers | Personal, Zeiterfassung |
| proj | Projekt |
| rech | Rechnung |
| rekla | Reklamationsverwaltung |
| stk | Stücklisten |
| ww | Warenwirtschaft inkl. Artikelstamm |
| databasechangelog | Das Änderungsprotokoll in der Datenbank aus Liquibase |
