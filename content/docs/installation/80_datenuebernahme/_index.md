---
categories: ["Datenübernahme"]
title: "Datenübernahme"
linkTitle: "Datenübernahme"
date: 2023-01-31
weight: 8000
description: >
  Datenübernahme von anderen Systemen
---
Da auch immer wieder die Frage kommt, wie bekommt man aus anderen ERP Systemen, die Daten in das **Kieselstein ERP**.

Der Klassiker von beliebigen anderen Systemen ist, dass die <u>Stammdaten</u> inkl. Stücklisten per XLS Datei importiert werden. Es ist dafür erforderlich, dass die Daten vom Alt-System entsprechend zur Verfügung gestellt werden.

Eine Übernahme von Bewegungsdaten, also Angeboten bis Lieferscheine und Rechnungen, Bestellungen, Wareneingänge, Fertigungsaufträgen, usw. ist zwar theoretisch möglich, aber immer
mit einem enormen Aufwand verbunden.

Es kommt ergänzend dazu, dass auch die in Dokumentenmanagementsystemen abgelegten Belege übernommen werden sollten, was mit einem enormen Aufwand verbunden ist.

Die Datenübernahme aus dem ERP-System HELIUM V, dessen Fork das **Kieselstein ERP** ist, ist unter gewissen Umständen sehr einfach möglich.

Was ist für die Umstellung von HELIUM V auf Kieselstein-ERP alles vorzubereiten, zu beachten.
Es werden alle Daten auf Knopfdruck, innerhalb kurzer Zeit, nach entsprechender Vorbereitung und von einem guten Backup übernommen. Also:
- Datenbanken (sowohl ERP Daten wie Dokumenten-Daten)
- Formulare

<u>**Wichtige Voraussetzung:**</u><br>
Gültige Softwarepflege / Miete für HELIUM V bis inkl. September 2022

Die Daten gehören laut europäischem Recht dem Anwender, also dir, denn die Arbeit die Daten im ERP-System zu erfassen ist beim Anwender gelegen. D.h. die Leistung der Datenpflege wurde durch den Anwender erbracht.

Sollten obige Voraussetzungen teilweise nicht gegeben sein, so nimm bitte mit uns Kontakt auf, es gibt noch weitere Möglichkeiten. Es ist uns sehr wichtig, dass alle diese Dinge im rechtlich legitimen Rahmen ablaufen.


## Vorgehensweise zur Umstellung
Für die Umstellung von HELIUM V auf dein **Kieselstein ERP** wird wie folgt vorgegangen:
| Nr. | Beschreibung | wer | wann |
| --- | --- | --- | --- |
| 1. | zur Verfügung-Stellung eigenständiger **Kieselstein ERP** Server | M | |
| 2. | Einrichten **Kieselstein ERP** System | C | |
| 3. | Einkopieren der Datenbanken und Formulare | C | |
| 3a. | Prüfung ev. hinzugekommener Datenbankfelder und deren Verwendung im eigenen Unternehmen | C+M | |
| 3b. | Prüfung der Eignung / Geschwindigkeit des **Kieselstein ERP** Servers | C | |
| 4. | Anpassung der Formulare | C | |
| 5. | Einrichten Clients | C+M | |
| 6. | ausführliche Tests | M | |
| 7. | Fehlerreporting | M an C | |
| 8. | Entscheidung wie mit den Fehlern umgehen<br>- Formularanpassung<br>- Programmierung<br>- Umgehung | M + C | |
| 9. | Freigabe der Tests und Festlegung des Starttermins | M + C | |
| 10. | Übertragung und ev. Konvertierung der Datenbank nach **Kieselstein ERP** System | C | |
| 11. | Echtstart | C + M | |
| 12. | Feierlicher Abschluss | alle | |

Bedeutung:
- C ... Consultant
- M ... Mitglied

## Wie prüft man, welche DB-Änderungen gemacht wurden
Wenn neuere Datenbankversionen gegeben sind, müssen die Datenstrukturen und die Inhalte geprüft werden. Hier geht man davon aus, dass anhand der Kieselstein-ERP default Datenbank (.../kieselstein/dist/bootstrap/database) ein Vergleich (https://www.devart.com/dbforge/sql/schemacompare/) mit der bestehenden Datenbank gemacht wird.
Anhand dessen, muss gemeinsam überlegt werden, welche Inhalte bei der Überleitung eventuell verloren gehen würden.<br>
**Anmerkung:** Bisher haben wir zwar leichte Änderungen in den Strukturen bemerkt, welche jedoch, da nicht verwendet, (fast) keine Auswirkungen in den Dateninhalten hatten.<br>
Selbstverständlich ist hier mit entsprechender Umsicht vorzugehen.
**Hinweis:** Mitglieder der Kieselstein-ERP eG können die genaue Umstellungsanleitung im Wiki nutzen.

## Fehlermeldungen und deren mögliche Ursachen
### Falsches Java am Server
Es scheint alles zu gehen, aber beim Drucker der Artikelstatistik kommt (wegen der Grafik) ein schwerer Fehler. Dieser weist in dem langen Fehlerstack auf Grafik / SVG Fehler hin.
Im Server selbst sieht man in den installierten Programmen, dass, vermutlich von früher noch, ![](falsche_Java_Version.png)  installiert ist. Diese bitte deinstallieren und das aktuelle JDK mit FX für das jeweilige Betriebssystem verwenden. Das sieht dann so aus: ![](aktuelle_Java_Version.png)  
Dies passiert gerne, wenn der bisherige ERP Server verwendet wird und das Java nicht überprüft wurde.