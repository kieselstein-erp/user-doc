---
categories: ["Installation"]
tags: ["Docker"]
title: "Installation unter Docker"
linkTitle: "Docker"
date: 2023-01-31
weight: 400
description: >
  Docker nutzen, insbesondere für Tests
---
Gerade für die Tests neuer Versionen empfiehlt die Technik Docker zu nutzen.

Docker ist eine Art Container, in dem dann die spezifischen Einstellungen für diesen Container gemacht werden können, welche die Einstellungen der anderen Docker-Container nicht berühren.
So kann man ganz praktisch die verschiedensten Varianten testen.

Man kann sich das auch wie eine weitere virtuelle Maschine vorstellen, aber ohne das Betriebssystem mehrfach zu benötigen.

## Windows
Die Installation in einer Hyper-V VM ist nur in besonderen Ausnahmefällen möglich. 
Von Docker wird empfohlen, dies NUR auf einer physikalischen Maschine einzurichten.

### Download
siehe https://docs.docker.com/desktop/install/windows-install/

Dafür muss WSL (Windows Subsystem for Linux) eingerichtet sein.<br>
- CMD as Administrator starten
- wsl --install<br>
Richtet ein quasi Ubuntu parallel zum Windows ein<br>
Dauer insgesamt: ca. 15 Minuten<br>
Internet: eine gute Leitung erforderlich<br>
Reboot des Rechners: mehrfach erforderlich<br>

Download: [Docker Desktop for Windows](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)

Danach Docker ausführen und einen entsprechenden Linux User anlegen (Empfehlung gleich wie unter deiner Hauptmaschine)

Laden des Docker Files:
- Gitlab, Kieselstein-docker, Base-Verzeichnis ![](Base_Verzeichnis.png)
- Dockerfile herunterladen
- Ein Verzeichnis Docker anlegen und das heruntergeladene Dockerfile in dieses reinkopieren

Docker Desktop starten
- Create a Dev Environment
- Locales Verzeichnis (Docker) angeben und Continue
- nun werden alle abhängigen Dateien usw. in diesen Dockercontainer geladen. Dauer einige wenige Minuten

https://learn.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment?tabs=dockerce

Dafür braucht man auch noch das Windows Admin Center, für das man sich wiederum registrieren muss.