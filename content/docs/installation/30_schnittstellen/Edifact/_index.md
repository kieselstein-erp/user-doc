---
categories: ["Edifact"]
title: "Edifact"
linkTitle: "Edifact"
date: 2023-01-31
weight: 200
description: >
  Infos rund um Edifact
---
Eine Sammlung unserer Informationen rund um die EdiFact Schnittstelle

Für die EdiFact Anbindung sind unter anderem folgende Einstellungen/Vorraussetzungen erforderlich:
- Kieselstein-Benutzer mit Kennwort mit passenden Rechten für die RestApi-Systemrolle
  - diesem Benutzer ein passendes Personal zuordnen und dort als **Ausweisnummer** den gewünschten Token eintragen (z.B. BEPS-....)
- EDIFACT_USER -> Environment Variable des Wildflyservers mit Benutzernamen dieses Kieselstein Benutzers
- EDIFACT_PASS -> Environment Variable des Wildflyservers mit Kennwort dieses Kieselstein Benutzers
- einen Wildfly Benutzer für die http-Authentifizierung erstellen mit 
```
add-user.sh -a -u '<benutzername>' -p '<passwort>' -g 'submitter'
```

Bei der Anmeldung auf der Website gibt man die Daten des *Wildfly Benutzers zu verwenden* von oben ein.