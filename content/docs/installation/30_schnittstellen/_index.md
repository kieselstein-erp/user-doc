---
categories: ["Schnittstellen"]
title: "Schnittstellen"
linkTitle: "Schnittstellen"
date: 2023-01-31
weight: 3000
description: >
  Schnittstellen zu deinem Kieselstein-ERP System
---
Nachfolgend eine lose Sammlung von teilweise praktischen Schnittstellen zu deinem **Kieselstein ERP**-System

## Telefonanlage
Einfache Übergabe der im **Kieselstein ERP** erfassten Telefonnummern, sodass die Telefonanlage dies automatisch einliest. Womit du auf dem Telefon den Namen des Anrufers siehst.
Baut natürlich auf den sauber erfassten Telefonnummern auf. Siehe [wie Telefonnummern erfassen]( {{<relref "/docs/stammdaten/partner/#wie-erfasst-man-telefon-und-faxnummern-richtig">}} ) .

Schnittstellen
==============

In **Kieselstein ERP** sind eine Vielzahl von Schnittstellen und Datenanbindungen enthalten, welche auch laufend erweitert werden.
Dies beginnt bei Importschnittstellen für Artikel, Stücklisten, Partner, Kunden, Lieferanten und geht bis zu komplexen Anbindungen an verschiedenste weitere vor- oder nachgelagerte Systeme.

## Importe

- Kassen Importer [Oscar und ADS3000]( {{<relref "/fertigung/kueche/kassenimport_definitionen">}} )
- [TrumpfTops]( {{<relref "/fertigung/losverwaltung/trumpf_tops">}} )
- Sachkonten Import in der integrierten Finanzbuchhaltung im CSV Format.
Musterdaten für Österreich und Deutschland siehe Fibu_DE_Muster.csv. Für Schweiz und Liechtenstein siehe Fibu_CH_Muster.csv

### Web-Shop Importe
- Shopify Import in den Lieferschein
- [WooCommerce]( {{<relref "/docs/installation/30_schnittstellen/woocommerce">}} )
 in den Auftrag

### Fileimporter



## Exporte

Bitte beachte ergänzend zu den Exporten, dass grundsätzlich jeder Druck aus **Kieselstein ERP** auch für Exporte, insbesondere im CSV bzw. XLS Format und natürlich im allseits beliebten PDF Format erzeugt werden können.

Bitte beachte den Unterschied zwischen den direkten Exporten der angelieferten Daten (Fields) und dem Export mittels speichern unter.<br>
Beim direkten Export im CSV Format bzw. Export in die Zwischenablage ![](DirekterExportderFields.png)  werden die Daten, so wie sie vom Applikationsserver angeliefert werden, exportiert. D.h. hier können auch Datenfelder enthalten sein die auf deinem Report nicht sichtbar sind, aber vom Applikationsserver angeliefert werden.<br>
**Andererseits:**<br>
Hier können keine errechneten Daten enthalten sein. Egal ob es sich dabei um Summen handelt oder um direkte Datenbankabfragen. Insbesondere wenn Reportvarianten verwendet werden, ist es meist von Vorteil, wenn der Export über den Speichern Dialog ![](ExportMitSpeichernUnter.png)  erfolgt. Hier kann aus folgenden Formaten<br> ![](Exportformate.png)<br>
ausgewählt werden.
| Format | Beschreibung |
| --- | --- |
| PDF | Portable Document Format |
| CSV | Comma Separated Value, Felder sind mit Komma getrennt |
| XLS | |
| ODT | |

Bitte beachte, dass bei diesen Exporten versucht wird, das Druckbild des Reports zu erhalten. Hier kann es von Vorteil sein, wenn man sich einen speziellen Druck für z.B. den XLS Export zusammenstellt. [Siehe auch]( {{<relref "/docs/installation/10_reportgenerator/ireport/#datenexport-aus-der-druckvorschau" >}} )

Darüber hinaus gibt es noch weitere spezielle Anbindungen die nachstehend beschrieben sind.

Bitte beachte ergänzend zu den Exporten, dass grundsätzlich jeder Druck aus **Kieselstein ERP** auch für Exporte, insbesondere im CSV bzw. XLS Format und natürlich im allseits beliebten PDF Format erzeugt werden können. Darüber hinaus gibt es noch weitere spezielle Anbindungen, die hier beschrieben sind.


### Bidirektionale Schnittstellen
Bidirektionale Schnittstellen bestehen meist aus einer Kombination von Modulen bzw. Vorgehensweisen, die erst in Ihrer Gesamtheit den tatsächlichen Nutzen ergeben.
- [MES Systeme](#schnittstellen-zu-mes-systemen)
- [Kassenlösungen](#Kassenlösungen)
- [4Vending]( {{<relref "/einkauf/eingangsrechnung/4vending">}} )
- [CleverCure]( {{<relref "/docs/installation/30_schnittstellen/CleverCure">}} )
- [ProFirst]( {{<relref "/docs/installation/30_schnittstellen/profirst">}} )
- [EDI Import]( {{<relref "/verkauf/forecast/EDI_Importe">}} )
- [EDI Export]( {{<relref "/verkauf/forecast/EDI_Exporte">}} )
- [Magento]( {{<relref "/docs/stammdaten/system/web_services">}} )
- [Trumpf Boost]( {{<relref "/fertigung/losverwaltung/trumpf_fab">}} )

### Schnittstellen zu Elektronik Distributoren
<a name="Elektronik Distributoren"></a>
Nachfolgend eine Beschreibung der derzeit realisierten Schnittstellen zu den verschiedenen Distributoren.
Bitte betrachten Sie dies auch als eine Auflistung von Möglichkeiten, die bereits bestehen, welche wir, für Ihren bevorzugten Distributor gerne erweitern.

### Farnell

Für Farnell wurde die Abfrage der kundenspezifischen Artikelpreise und des Versandes von Bestellungen realisiert.

Abfrage der Artikellieferantenpreise

Grundsätzliches:
**Voraussetzungen**
-   Zusatzfunktionsberechtigung WEB_BAUTEIL_ANFRAGE
-   Parameter ARTIKELWIEDERBESCHAFFUNGSZEIT in Tage
-   API-Request werden am Server getätigt. Das bedeutet Ihr **Kieselstein ERP** Server muss eine Verbindung zum Farnellserver aufbauen dürfen.
-   Gepflegte Daten:<br>
    Lieferantenartikelnummer und gegebenenfalls Herstellernummer.<br>
    **Hinweis:** Als Lieferantenartikelnummer soll auch wirklich diese und nicht z.B. die Herstellernummer angegeben sein.
-   mind. Java 1.8.0_101 am Server

**Einstellung im Lieferanten**
-   Reiter Webabfrage im Lieferanten
-   Default ist <LEER> => kein Eintrag als Weblieferant
-   Auswahl von "Farnell API"
-   Angabe der Mindestparameter:
    -   URL (= <https://api.element14.com/catalog/products>)
    -   Api Key (Registrierung über <https://partner.element14.com/member/register>)
    -   Abgerufene Region (de.farnell.com)
-   Für kundenspezifische Preise
    -   Customer ID (Kundennummer bei Farnell)
    -   Secret Key (erhält man nach Anforderung über Farnell)
![](Webabfrage1.gif)

-   Webabfrage-Eintrag kann wieder entfernt werden, indem auf \<LEER\> zurückgesetzt wird
    ![](Webabfrage2.gif)

**Abfrage im Artikellieferanten** Ist ein selektierter Lieferant im Artikellieferant ein Weblieferant (= hat einen Eintrag im Lieferant, Webabfrage <> LEER) so erscheint im Detail ein neuer Button ![](earth_find.png) "Webabfrage" und die Felder "Letzte Webabfrage" und "Bestand". Mit Klick auf die Weltkugel werden die aktuellen Preise und weitere Daten abgefragt.

**Suchparameter**
1. Artikelnummer Lieferant (des Artikellieferanten, der zum Zeitpunkt der Abfrage gültig ist bzgl. des Gültig-Ab Datums)
2. Herstellerartikelnummer<br>
Sobald und nur wenn ein eindeutiges Ergebnis erzielt wird, werden die Daten des Artikellieferanten aktualisiert. Geht der API-Request nicht durch (Service derzeit nicht verfügbar, fehlerhafte Parameter,...) wird auch nicht aktualisiert, es erscheint eine entsprechende Meldung.

**Aktualisierung Artikellieferant - Regeln**
1. Maßgebend ist jener Artikellieferant, der zum Zeitpunkt der Abfrage gültig ist bzgl. des Gültig-Ab Datums
2. Blieb der Einzelpreis unverändert, so werden die Daten dieses Eintrags aktualisiert. Sonst wird ein neuer Eintrag des Artikellieferanten erzeugt mit Gültig-Ab des Abfragezeitpunkts

**Aktualisierung Artikellieferant - Daten** - Letzte Webabfrage
- Webabfrage Bestand
- Einzelpreis
- Nettopreis: bei Neuerzeugung = Einzelpreis, bei Update entsprechend Rabatt
- Rabatt = 0, bei Neuerzeugung
- Artikelnummer Lieferant
- Wiederbeschaffungszeit (in Tagen)
- Weblink
- Staffelpreise

Bitte beachten Sie, dass es zwischen Ihrer lagergeführten Mengeneinheit und der bei Farnell verwendeten Mengeneinheit durchaus Unterschiede geben kann. Das bedeutet, dass z.B. ein Schrumpfschlauch mit 5 m bei Farnell als 1 Stk mit 5 m geführt wird, Sie dies in Ihrem Artikelstamm in aller Regel in m (und damit in der Stückliste in mm) bewirtschaften werden. Das bedeutet nun, dass diese Übersetzung der Menge auf die Verpackungseinheit beim Artikellieferanten durch den Anwender selbst gepflegt werden muss. Weiters bedeutet dies, dass der Bestand und die Staffelmengen von den Farnell-Mengen (die auf Basis der Verpackungseinheit sind) in die lagergeführten Mengeneinheiten Ihres Artikelstamms umgerechnet werden. Bitte vermeiden Sie daher an dieser Stelle Definitionen wie 1Stk = 1Stk, denn andererseits benötigen Sie sicherlich 1Stk (Farnell Gebinde) = 1Liter z.B. einer Reinigungslösung.<br>
Da die von Farnell vorgegebene Mindestbestellmenge nicht unbedingt einen festen Zusammenhang mit der von Ihnen gewünschten Mindestbestellmenge haben muss, wird auch diese nicht auf die Daten der Schnittstelle gesetzt.

Um Missverständnisse in der Mengendefinition zu vermeiden, raten wir daher von Farnell die (bekannte) Regelung zu verlangen, dass bei unklaren Einheiten Ihre Bestellung auf Halt gesetzt wird, um zu einer Abklärung der Ungenauigkeit von Seiten Farnell abgefordert zu werden.

**Aktualisierung Staffelpreise - Regeln**
1. Maßgebend ist jener Eintrag, der für die aktuell geprüfte Menge zum Zeitpunkt der Abfrage gültig ist
2. Bleibt Menge und Preis unverändert, dann auch der Eintrag
3. Bleibt Menge bestehen und Preis ändert sich, dann
    - a) Gültig Ab < Abfragetag: Bisheriger Eintrag mit Gültig Bis = Abfragetag - 1 und neuer Eintrag mit aktuellem Preis und Gültg Ab = Abfragetag
    - b) Gültig Ab = Abfragetag: Bisheriger Eintrag wird mit neuem Preis aktualisiert
4. Neue Menge führt zu einem neuen Eintrag mit Gültig Ab = Abfragetag
5. Gibt es eine eingetragene Menge nicht mehr, so wird ihr das Gültig Bis = Abfragetag - 1 gesetzt

#### Automatikjob Webabfrage Artikellieferant
Um nun nicht jeden Artikel einzeln aktualisieren zu müssen, gibt es den [Automatikjob]( {{<relref "/docs/stammdaten/system/automatik" >}} ) Webabfrage Artikellieferant.
Die Definition dafür finden Sie im Modul System, unterer Modulreiter Automatik.<br>
Dieser muss aktiviert werden und wird dann zum gewünschten Zeitpunkt einmal täglich, z.B. in der Nacht ausgeführt.<br>
Es werden dabei folgende Funktionen durchgeführt:
- Es werden alle Artikel eines Weblieferanten abgefragt und aktualisiert<br>
Weblieferant = Lieferant mit einem Eintrag im Reiter Webabfrage
- Es gelten bzgl. Aktualisierung die gleichen Regeln wie über den Button im Artikellieferanten
- Wird im Detail des Automatikjobs eine E-Mail-Adresse definiert, so wird nach Durchführung an diese E-Mail-Adresse ein Protokoll gesendet.

Protokoll Beispiel:
```
Protokoll der Durchführung des Automatikjobs Webabfrage Artikellieferant.

Für Lieferant 'Farnell GmbH':
Artikel '1000394NXP': Aktualisiert
Artikel '1000208YAG': Aktualisiert
Artikel '1000795AVX': KEIN ERGEBNIS
Suchparameter: Lieferantenartikelnummer '1657939', Herstellernummer 'null'
Artikel '1000422NXP': MEHRFACHE ERGEBNISSE
Suchparameter: Lieferantenartikelnummer '1301304', Herstellernummer '74HCT14D'
...
```
Mögliche Fehlermeldungen
-   Die Webabfrage für Artikel '0815' wurde vom API-Server nicht zugelassen. Bitte prüfen Sie die Webabfragezugangsdaten im Lieferanten 'XY'<br>
=> Lösung: wahrscheinlich stimmen die Parameter im Weblieferanten nicht.
- Die Webabfrage für Artikel '0815' führte zu keinem (od. mehrfachen) Ergebnis.
    Folgende Suchparameter wurden verwendet: Lieferantenartikelnummer 'AB', Herstellernummer 'CD'
    => Lösung: Prüfung der Lieferantenartikel- und Herstellernummer, z.B. ist keine Lieferantenartikelnummer angegeben oder es wurde darin die Herstellernummer angegeben. Achtung, das sind zwei unterschiedliche Calls gegen den API-Server!

-   Die Webabfrage für Artikel '0815' enthielt ungültige Parameter.
    Bitte prüfen Sie die Einstellungen im Lieferanten oder wenden Sie an Ihren **Kieselstein ERP** Betreuer. Bitte senden Sie uns dazu auch die Protokolldatei.

-   Die Webabfrage für Artikel '0815' führte zu einer unerwarteten Antwort: \<HTTP Statusnachricht\><br>
    => Lösung: Warten und später wieder probieren. Vor allem bei 503 Service unavailable oder 500 Internal Server Error

#### Versenden der Bestellungen im XML Format

Für Farnell steht mit dieser Anbindung auch das Versenden von Bestellungen im XML Format zur Verfügung.
D.h. wenn bei einem Lieferanten die API zu Farnell aktiviert ist, so steht beim E-Mail-Versand der Bestellung neben dem normalen E-Mail-Versand auch der Versand per XML zur Verfügung.![](XML_senden.jpg) D.h. um die Bestellung im für Farnell lesbaren Format (OpenTrans1.0) zu senden, klicken Sie bitte auf Mit XML senden.<br>
Das bedeutet, dass wie bisher die Bestellung als PDF Datei an das E-Mail angefügt wird und zusätzlich die XML Datei als weitere Anhang dazugegeben wird. Beide Dateien werden auch in der Dokumentenablage dieser Bestellung abgelegt.
Es sind dazu folgende Daten erforderlich:
- im Lieferanten muss die Kundennummer hinterlegt sein
- für jeden Artikel muss die "Artikelnummer des Lieferanten" (aus Artikellieferant) gesetzt sein.
Bitte beachten Sie, dass bei den Mengeneinheiten derzeit nur Stk, Meter, Kg, Liter, Stunde und Rolle in das OpenTransFormat übersetzt werden.

Um die E-Mail-Adresse der Bestellung entsprechend vorzubesetzen, nutzen Sie bitte die Übersteuerungsmöglichkeit der [Ansprechpartnerfunktion]( {{<relref "/docs/stammdaten/partner/#%C3%BCbersteuerung-von-email-adressen--faxnummer"  >}} ).
Für die richtige Empfänger-E-Mail-Adresse stimmen Sie sich bitte mit Ihrem Lieferanten ab. Für den Test der Funktion sollte eine E-Mail-Adresse verwendet werden, die auch von Ihrem Lieferanten klar als Test deklariert ist.

#### Wieso werden bei "jeder" Abfrage neue Artikellieferanteneinträge gemacht?
Wie oben beschrieben werden bei Preisänderung zur Dokumentation der Preisentwicklung neue Gültig-ab Einträge angelegt.

#### Was bedeutet Wiederbeschaffungszeit gegenüber der Lieferdauer?
In der Farnell Schnittstelle werden zwei Informationen übergeben.
Einerseits der aktuelle Lagerstand und
andererseits die Wiederschaffungszeit. Hier ist mit Wiederbeschaffungszeit jene Zeit (in Tagen) gemeint, die Farnell / das Werk benötigt, um diesen Artikel wieder zu beschaffen.

## Schnittstellen zu Kassenanbietern
<a name="Kassenlösungen"></a>
Gerade im Handel sind die Übertragung der Artikel an die Kasse(n) und die Übernahme des Tagesprotokolls für die Verbrauchsbuchungen von der Kasse nach **Kieselstein ERP** vor allem zur Aktualisierung des Lagerstandes wesentlich. Sollten Sie weitere Schnittstellen / Funktionen benötigen, so bitten wir um Ihre Anfrage. Wir unterstützen Sie gerne.

### Gastrosoft
Die Schnittstelle zu Gastrosoft besteht im Wesentlichen aus zwei Teilen:
- a.) Export der Artikel mit Beschreibungen und Verkaufspreisen. Dies wird durch eine spezielle Preisliste aus dem Artikelstamm realisiert, mit der die Daten im XLS Format übergeben werden.
- b.) CSV-Kassenimport<br>
Diesen Import finden Sie im Modul Los, Menü Los -> Import -> CSV-Kassenimport.
Auswahl eines Ordners, in dem die zu importierenden CSV-Dateien abgelegt sind. Es werden alle CSV-Dateien darin berücksichtigt.
![](importfenster.PNG)

Folgende Spalten werden benötigt:
- Artikel (= Bezeichnung des Artikels)
- Artikel-Nr.
- Menge
- Rech.-Datum
- Rech.-Nr.
- Pos.-Nr.
- Datum (= Abschlussdatum)
- E-Preis (= Einzelpreis)
- G-Preis (= Gesamtpreis)
- Zahlungsart
- PLU
- Hauptwarengruppe

Es wird jede Datei nacheinander manuell importiert. Es empfiehlt sich vor dem Import eine Prüfung zu machen. Etwaige Fehler werden im Importfenster aufgelistet - wenn es eine spezielle Zeile betrifft mit Zeilennummer. Es wird davon ausgegangen, dass es pro Tagesabschluss eine Datei gibt. Es wird für jeden Dateiimport ein neues Los als freie Materialliste angelegt, mit dem Abschlussdatum des ersten Artikels des Imports als Beginn- und Ende-Datum. Im Projektfeld wird der Tagesabschluss textuell erfasst. Kostenstelle (mit definiertem Sachkonto), Fertigungsgruppe, Montageart muss gegeben sein. Es wird davon jeweils die 1\. verwendet. Als Loslager wird jenes mit niedrigstem Loslager-Sort verwendet.

Jede Zeile (also jeder Artikel) wird als Lossollmaterial im Los angelegt. Ist der E-Preis eines Artikels negativ, wird das Sollmaterial als Lagerzugang gebucht, d.h. die angegebene Menge wird mit negativem Vorzeichen berücksichtigt. Ist ein Artikel lagerbewirtschaftet, muss genügend im Lager verfügbar sein. Ist keine Artikelnummer angegeben, wird jene Artikelnummer für diese Zeile verwendet, die über den Mandantenparameter KASSENIMPORT_LEERE_ARTIKELNUMMER_DEFAULT_ARTIKEL definiert wurde. Im Kommentarfeld des Sollmaterials wird die Rech.-Nr., der G-Preis und die Zahlungsart geschrieben. Zusätzlich wird bei Verwendung des Defaultartikels die PLU und Artikelbezeichnung hinzugefügt.

Ist ein zu importierender Artikel ein Setartikel, so wird nicht der Artikel selbst, sondern seine Positionen als Lossollmaterial importiert. Die Menge in der Setartikelposition wirkt dabei als Faktor zur Menge aus dem CSV. Als Text des Kommentars wird neben der Rech.Nr., G-Preis und Zahlungsart auch angegeben aus welchem Setartikel dieses Lossollmaterial ein Bestandteil war.

Beginnt die Hauptwarengruppe mit "Pfand" (Groß-/Kleinschreibung nicht relevant), so werden diese Artikel beim Import nicht berücksichtigt.

Mögliche Fehlerfälle sind:
- Unbekannter Artikel
- erforderliche Spalte nicht gefunden
- zu geringer Lagerstand für bestimmten Artikel
- Wert einer Spalte konnte nicht in erwartetes Datum oder erwartete Zahl umgewandelt werden
- es konnte keine Montageart, Fertigungsgruppe, Kostenstelle mit definiertem Sachkonto oder ein Lager mit Loslager Sortierung gefunden werden
- Defaultartikel für leere Artikelnummer nicht definiert
- Setartikel enthält keine Positionen

![](importfenster_pruefung.PNG)

### Kassen Importer 
- [Oscar]( {{<relref "/fertigung/kueche/kassenimport_definitionen">}} )
- [ADS3000]( {{<relref "/fertigung/kueche/kassenimport_definitionen">}} )

## Schnittstellen zu MES Systemen
Manche Anwender sind der Meinung, dass MES Systeme an **Kieselstein ERP** angebunden werden sollten. Manchmal gibt es auch die Situation, dass verschiedene Maschinenhersteller die erforderlichen Kommunikationsdaten nicht bzw. nur gegen Einsatz von sehr viel Geld, herausgeben.<br>
Auch wenn wir der Meinung sind, dass eine direkte Integration der vollständigen Fertigungssteuerung in **Kieselstein ERP** deutliche Vorteile bringt, die Komplexität und Mühsamkeit der Abstimmung bestätigt dies, so unterstützen wir unsere **Kieselstein ERP** Anwender selbstverständlich auch in diesen Dingen.

### Hydra
Mit der Hydra-Schnittstelle wurde eine bidirektionale Verbindung zwischen den Losen aus **Kieselstein ERP** und den (Fertigungs-) Aufträgen von Hydra geschaffen.
Bitte beachten Sie bei der Definition Ihrer **Kieselstein ERP** Daten die zahlreichen Beschränkungen, die durch das Hydra System grundsätzlich gegeben sind.

Nachfolgend eine kompakte Beschreibung zur Verbindung mit dem Hydra System. Für weitere Details wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

Das Hydra-Microservice läuft als eigenständiger HTTP-Server (Host + Port in Mandantenparameter HYDRA_SERVER definieren), idealerweise direkt auf dem Rechner auf dem auch Ihr Hydraserver läuft.
Wechselt ein Los in den Status "In Produktion" und entspricht es weiteren Hydra-Kriterien, wird die Ausgabe per HTTP-POST dem Microservice bekanntgegeben. Folgende Kriterien müssen dabei erfüllt werden:
- Zusatzfunktionsberechtigung "Hydra"
- Mandantenparameter HYDRA_SERVER beinhaltet mit Host:Port das darüber erreichbare Microservice
- Los-Fertigungsgruppe ist jene wie in Mandantenparameter HYDRA_FERTIGUNGSGRUPPE definiert

Das Hydra-Microservice holt sich daraufhin selbständig über die Rest-API alle Daten des Loss und wandelt die erhaltenen Daten in die vorgegebene Hydra-Datenstruktur als Hydra-Auftragsdaten um. Diese beinhalten derzeit drei verschiedene Daten-Segmente:
- Auftragskopf: Losnummer, Zusatzstatus, Stücklistenartikelinformation, Kunde, Losgröße, Beginn- und Ende-Termin
- Langtexte des Auftragskopfes: Projekt und Kommentar
- Arbeitsgänge, die wiederum in drei Segmente (AG, AG-Komponenten, AG-Langtexte) unterteilt sind. U.a. mit AG-Nr., AG-Namen, Rüstzeit, Stückzeit, Maschine, Material...
Es werden dabei nur jene Arbeitsgänge berücksichtigt, die als AG-Art jene definiert haben, wie sie in der Konfiguration des Microservices definiert ist.

Pro Los erzeugt das Microservice somit eine temporäre Datei mit den zugehörigen Hydra-Auftragsdaten. Hydra nimmt die Daten über eine definierte Zieldatei entgegen, die von beiden Systemen (Hydra, Microservice) erreichbar ist. Ist diese Zieldatei nicht vorhanden (= Hydra hat die letzte eingelesen bzw. übernommen), schreibt das Microservice eine neue und fasst damit alle bis dahin erzeugten temporären Auftragsdaten darin zusammen und stellt sie so Hydra wieder zur Verfügung.

## Schnittstellen zu Exchange / Outlook
<a name="HV-EST"></a>
Steht aktuell für **Kieselstein ERP** nicht zur Verfügung. Solltest du einen entsprechenden Bedarf haben, freuen wir uns über deine Anfrage.

-   Mit dem Zusatzservice HV-EST, **H**ELIUM **V** **E**xchange **S**ynchronisations **T**ool, können die Daten aus Ihrem Partnerstamm nach **Exchange** übertragen werden.
    Im Wesentlichen werden hier die Firmen, Ansprechpartner mit deren Adressdaten, sowie Telefonnummern und E-Mail-Adressen in einer strukturierten Form in die zentralen Adressdaten des Exchange (LDap-Verzeichnis) importiert.
    Der Import ist so aufgebaut, dass portionsweise die Adressen vom **H**ELIUM **V** **E**xchange **S**ynchronisations **T**ool vom **Kieselstein ERP** Server abgeholt und in das Exchange übertragen werden. Da Exchange nur auf Microsoft-Betriebssystemen zur Verfügung steht, ist die HV-EST auch ein Windows-Programm.

    Die Gliederung / Erkennung, ob eine Partneradresse eine Firma oder ein Ansprechpartner einer Firma ist, erfolgt daran, wenn eine Adresse ein Ansprechpartner einer Firma ist, so wird dies in dieser Struktur im Exchange abgelegt. Ist eine Adresse nicht Mitglied / Ansprechpartner einer anderen Adresse wird dies als reine Firmenadresse betrachtet.
    Zusätzlich werden die übertragenen Adressen gegenseitig gekennzeichnet, sodass Änderungen in Ihren **Kieselstein ERP** Adressdaten beim nächsten Lauf im Exchange aktualisiert werden.
    Da dieser Dienst, wie beschrieben, immer nur Teile überträgt, kann es, je nach Umfang, einige Zeit dauern, bis die Daten synchron sind.

    Bitte beachten Sie, dass aufgrund dessen, dass nur Adressdaten übertragen werden, keine wie immer geartete Unterscheidung zwischen den Mandanten gemacht werden kann.

-   <a name="HV-OST"></a>
Mit dem Zusatzprogramm HV-OST, **H**ELIUM **V** **O**utlook **S**ynchronisations **T**ool, können die Daten aus Ihrem Partnerstamm über Office365 nach **Outlook** übertragen werden.
    Die Einrichtung dieser Funktion setzt administrative Rechte für Ihren Office365 Zugang voraus.
    Das entsprechende Programm (HV-OST.exe) fordern Sie bitte mit der genauen Einrichtungsbeschreibung bei Ihrem **Kieselstein ERP** Betreuer an.
    Die Verbindung zwischen **Kieselstein ERP** und Ihrem Office365 Adressbuch stellt die im Personal des jeweiligen Benutzers eingerichtete E-Mail-Adresse dar.
    Es werden die Kontaktdaten anhand folgender Logik ermittelt:<br>
    - Projekte, in denen der Benutzer als Mitarbeiter oder als "wird durchgeführt von" im Detail eingetragen ist.
    - Angebote, in denen der Benutzer der Vertreter ist<br>
    - Aufträge, in denen der Benutzer als Vertreter oder Auftragsteilnehmer eingetragen ist<br>
    - Rechnungen, in denen der Benutzer als Vertreter eingetragen ist.<br>
    - Kunden, in denen der Benutzer als Provisionsempfänger oder als Sachbearbeiter eingetragen ist.

    Bitte beachten Sie auch den Personal-Parameter "Alle Kontakte" synchronisieren. Mit diesem werden, entgegen obiger Logik, alle Partner in Ihre Outlook-Kontakte übertragen.


## Webshop
Nachfolgend eine Sammlung von nützlichen / wichtigen Informationen rund um das Thema Webshop.
[Zum Thema CleverCure siehe bitte]( {{<relref "CleverCure"  >}} )

#### Lieferschwelle / Lieferung an Endkunden im EU-Ausland
Mit der Lieferung an Endkunden (B2C Geschäft) im EU-Ausland sind je nach Empfänger-Land verschiedene Lieferschwellen verbunden, bei denen Sie gezwungen werden, die Umsatzsteuer mit dem Steuersatz des Empfängerlandes anzugeben und auch die einbehaltene Umsatzsteuer beim jeweiligen Empfängerfinanzamt abzuführen.<br>
Dafür, also die Lieferung an Endkunden, steht das OSS Verfahren zur Verfügung. [Siehe dazu bitte]( {{<relref "/management/finanzbuchhaltung/eu_oss" >}} )

Dies betrifft nur die Umsätze mit Endkunden. Für die Umsätze zwischen Unternehmen (B2B) gilt die Regelung der innergemeinschaftlichen Lieferungen.

Das bedeutet wiederum, dass es für Neukunden im Webshop wichtig ist, dass dieser seine UID-Nummer angibt.

Eine Aufstellung der Lieferschwellen finden Sie z.B. im Portal der [österreichischen Wirtschaftskammer](http://portal.wko.at/wk/format_detail.wk?AngID=1&StID=458268&DstID=0).

#### Wo kann ich definieren, ob Kundensonderkonditionen an den Webshop übermittelt werden?
In System - Parameter finden Sie den Parameter "KUNDESONDERKONDITIONEN_WEBSHOP_VERWENDEN".
Wenn Sie diesen Parameter auf 0 stellen, so werden die Kundensonderkonditionen nicht an den Webshop übermittelt. Bei 1 werden die Sonderkonditionen übermittelt.

