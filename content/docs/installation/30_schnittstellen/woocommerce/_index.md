---
categories: ["Schnittstellen"]
title: "Woo Commerce Import"
linkTitle: "Woo Commerce"
date: 2023-01-31
weight: 300
description: >
  Einlesen von Shopdaten in den Auftrag
---
Im Auftragsmodul steht auch der Shop-Import im WooCommerce Format zur Verfügung.
Laut Informationen von **Kieselstein ERP** eG Mitgliedern kann dieses Datenformat auch von Shopware mit einer kleinen Anpassungsprogrammierung zur Verfügung gestellt werden.

## WooCommerce Import
Dieser Import steht im Modul Auftrag, im Menüpunkt Auftrag, Import, WooCommerce CSV zur Verfügung.
Die Grundidee ist, dass vom Shop täglich Daten abgeholt werden, z.B. über FTP Import und dann täglich diese Daten eingelesen werden.
Da es dabei immer wieder zu erforderlichen Anpassungsarbeiten kommt, sehen wir den manuellen Import als sinnvoller an.

Man findet die Schnittstelle im Modul Auftrag.

![](Woo_Commerce_20Import.png)  

### Zuordnung der WooCommerce Spaltenname:
Wichtig: Die Spaltenbezeichnungen müssen in Englisch übergeben werden. Wir gehen davon aus, dass alle Beträge als Nettobeträge übergeben werden.

Zusätzlich haben wir die Logik implementiert, dass der Brutto-Gesamtbetrag = Order Total Amount aus dem Shop exakt dem anhand der **Kieselstein ERP** Preise errechneten Wert entsprechen muss / sollte. D.h. wenn die Verkaufspreisfindung gleiche Preise ergibt wie die Preise aus dem Shop, so sollten sich die gleichen Summen ergeben.

Hier kommt noch die Herausforderung dazu, dass der Shop ein Brutto rechnendes System ist und **Kieselstein ERP** netto rechnet. Das bedeutet wiederum, dass es zu geringen Cent-Abweichungen kommen kann.

Diese Differenz wird, wenn sie unter 1,00 € ist, ausgeglichen. Ev. ist dafür auch der Rundungsartikel zu definieren.

Definieren Sie dafür den Parameter RUNDUNGSAUSGLEICH_ARTIKEL mit z.B. RUNDUNG.
Der Artikel RUNDUNG selbst muss, nicht lagerbewirtschaftet sein. Der Mehrwertsteuersatz muss mit steuerfrei definiert werden.

"Order Number","Order Status","Order Date","Customer Note",Title,"First Name (Billing)","Last Name (Billing)","Company (Billing)","Address 1&2 (Billing)","City (Billing)","State Code (Billing)","Postcode (Billing)","Country Code (Billing)","Email (Billing)","Phone (Billing)","First Name (Shipping)","Last Name (Shipping)","Address 1&2 (Shipping)","City (Shipping)","State Code (Shipping)","Postcode (Shipping)","Country Code (Shipping)","Payment Method Title","Cart Discount Amount","Order Subtotal Amount","Shipping Method Title","Order Shipping Amount","Order Refund Amount","Order Total Amount","Order Total Tax Amount",SKU,"Item #","Item Name",Quantity,"Item Cost"

| Spalte | Bedeutung |Aktion / Beschreibung |
| --- | --- | --- |
| Title | Anrede | Die Anrede (Title) aus dem Shop wird wie folgt interpretiert:<br>1 ... Herr<br>2 ... Frau<br>alle anderen werden derzeit auf \<leer>, also keine gesetzt.
| First Name | Vorname | Es werden die Spalte First Name in den Vornamen und Last Name in den Familiennamen übernommen |
| Last Name | Nachname | |
| EMail | Kunden Erkennung | Um zu erkennen, ob ein Kunde schon angelegt ist, wird die RE-E-Mail-Adresse herangezogen. D.h. gibt es für die erhaltene E-Mail-Adresse Email (Billing) bereits einen angelegten Kunden, so wird dieser verwendet und kein neuer Kunde angelegt. Wird die E-Mail-Adresse nicht unter RE-E-Mail-Adresse gefunden, so wird der Kunde neu angelegt.<br>Bitte achten Sie darauf, dass aktuell auch die Groß-/Kleinschreibung für die Zuordnung herangezogen wird.|
| Shipping Method Title | Lieferart | Dieser Eintrag wird unter den Lieferarten, genauer einer Bezeichnung aus den Lieferarten gesucht. Wird kein Eintrag gefunden, wird der Import abgebrochen.<br>Hinter der Lieferart muss es auch einen Versandkosten- (Transportkosten)-Artikel geben. Die Versandkosten werden (netto) vom Shop in der Spalte Order Shipping Amount übergeben. Beim Import wird der Verkaufspreis der AB-Position des Versandkostenartikels auf diesen Betrag gesetzt, wodurch die Adresse wie eine Firma behandelt wird.
| SKU | Artikelnummer | muss mit der Artikelnummer deines Kieselstein-ERP Artikelstamms übereinstimmen |
| Quantity | Menge | wird als Positionsmenge in den Auftrag übernommen |
| Item Cost | netto Verkaufspreis dieses Artikels | Bei der Anlage der Auftragsposition wird der Verkaufspreis anhand der Verkaufspreisfindung ermittelt. Weichen der ermittelte Verkaufspreis zu dem vom Shop erhaltenen Item Cost um mehr als 0,01 € ab, wird eine Fehlerzeile hinzugefügt.

Rechnungsadresse / Lieferadresse
- Die Rechnungsadresse wird anhand der erhaltenen E-Mailadresse ermittelt. D.h. ist im **Kieselstein ERP** Kundenstamm die RE-E-Mailadresse bereits vorhanden, so wird diese Adresse als Kunde verwendet. Es erfolgt keine weitere Prüfung mehr.
- Sind in den erhaltenen Shop-Daten die Daten für die Rechnungsadresse und die Lieferadresse gleich, so wird als Lieferadresse, des Auftrags, die Rechnungsadresse verwendet, egal ob die erhaltenen Adressdaten davon abweichend sind.
- Sind in den erhaltenen Shop-Daten die Daten für die Rechnungsadresse und die Lieferadresse ungleich, so wird die Lieferadresse neu angelegt und die Adressart als Lieferadresse gekennzeichnet.

{{% alert title="ACHTUNG" color="warning" %}}
Die RE-E-Mailadresse darf im gesamten Kundenstamm nur **<u>EINMAL</u>** vorkommen. Anderenfalls wird die Adresse erneut angelegt. Auch wenn diese E-Mail-dresse bei verschiedenen Ansprechpartnern definiert ist, kann es zu Fehlinterpretationen kommen.<br>
Nutze gegebenenfalls auch die Groß-/Klein-Schreibung bei den E-Mailadressen (nicht getestet).
{{% /alert %}}

**Info**:<br>
Die erweiterte Suche bei der Kundenauswahl prüft leider die RE-EMail-Adresse NICHT!

### Fehlermeldungen
![](WooCommerce_Import_Fehler1.jpg)<br>
Bitte die angeführte Lieferart definieren. Siehe oben Lieferart bzw. nachfolgend ein Muster wie die Lieferart angelegt werden könnte.<br>
![](WooCommerce_Import_Fehler3.jpg)<br>
Bitte beachte die Zuordnung des Frachtkostenartikels

![](WooCommerce_Import_Fehler2.jpg)<br>
Passiert bei der Vergabe der Debitorennummer für neue Kunden.<br>
**ACHTUNG:** Bitte die automatische Vergabe der Debitorennummer abschalten.

![](WooCommerce_Import_Fehler4.jpg)<br>
Dies passiert gerne, wenn manuell der Auftrag aus dem Shop angelegt wird und danach der Import gestartet wird. Um diese Situation zu entschärfen, wird vom Import automatisch SHOP- vor die eigentliche Bestellnummer aus dem Shop gestellt.

### Parameter
Mit dem Parameter WOOCOMMERCE_PARTNER_ZUSAMMENZIEHEN kann eingerichtet werden, dass Vorname und Nachname beim Import, wenn keine Company gegeben ist, in den PartnerName1 zusammengezogen wird.

### Musterdaten
Musterdaten mit genauer Feldzuordnung siehe als [LibreOffice Vorlage](woocommece_muster.ods) mit Beschreibung bzw. als [CSV](woocommece_import_muster.csv)

### Zusatzinfo
- Die Partnerart ist fix auf Adresse, so wie bei anderen Kunden auch
- Die Kommunikationssprache ist in der Regel die Sprache, in der sich der Kieselstein ERP Benutzer angemeldet hat
- Der Mehrwertsteuersatz kommt aus dem Land, also Inland mit Allgemeine Waren, alle anderen Länder steuerfrei. Für genauere Definition siehe bitte System, Mandant, Vorbelegungen.
