---
categories: ["CleverCure"]
title: "CleverCure"
linkTitle: "CleverCure"
date: 2023-01-31
weight: 100
description: >
  Infos rund um CleverCure
---
Eine Sammlung unserer Informationen rund um die EDI Schnittstelle CleverCure
## Rest-Einsprungspunkt
Der Einsprungspunkt über das https: Protokoll (achte darauf, dass für den https-Port NUR dein externer Partner freigeschaltet ist) ist:<br>
https://deine_externe_EP-Adresse/kieselstein-restapi/services/rest/api/beta/cc?companycode=nnnnn&token=tttttttttttt
- Companycode siehe
- token siehe

## Error 417
Bedeutet, dass (noch) kein Versandweg eingetragen ist. Diesen nachtragen. Dabei darauf achten, dass in der lp_versandwegpartnercc und in der lp_versandwegpartner die gleiche Anzahl an Definitionen gegeben ist.
Die jeweilige ID ist der gemeinsame Schlüssel.

select * from lp_versandwegpartnercc
select lp_versandwegpartnercc.c_kundennummer, part_partner.c_uid, i_soko_adresstyp, * from lp_versandwegpartner
inner join part_partner on lp_versandwegpartner.partner_i_id=part_partner.i_id
left outer join lp_versandwegpartnercc on lp_versandwegpartnercc.i_id=lp_versandwegpartner.i_id
order by part_partner.c_kbez

- Üblicherweise per Hand die neue ID in der lp_versandwegpartner nachtragen
- Irgendwie spielen da die UID Nummern auch noch mit, zumindest für die Zuordnungen
- Knd Lieferantennummer = Werkskennung
- Verkettung im lp_versandwegpartner
- Es muss die Lieferadresse zugeordnet werden

### Auftrag xxx existiert bereits
Diese Fehlermeldung (Error 417) kann leider auch bedeuten, dass ein Auftrag mit dieser Bestellnummer bereits in deinem **Kieselstein ERP** angelegt ist.
- Im Tomcat-Log findest du dazu "nur"
> IP-Adresse - - [14/Sep/2023:16:15:05 +0200] "POST /kieselstein-rest/services/rest/api/beta/cc?companycode=12345&token=20ETMCCMVH13 HTTP/1.1" 417 -<br>
Wichtig ist hier der Zeitpunkt.
>
- Im server.log findest du dazu:
>
2023-09-14 16:15:05,665 WARN [com.lp.server.auftrag.ejbfac.WebshopOrderServiceEjb] (default task-3) Der Auftrag 'JJ/nnnnnnn' existiert mit der Bestellnummer 'xxxxxxxx'. Kein weiterer Auftrag angelegt!
>
Das bedeutet nichts anderes, dass versucht wurde einen Auftrag erneut einzuspielen, obwohl dieser bereits übermittelt wurde.<br>
Info: Die Schnittstelle ist so vereinbart, dass einmal übertragene Aufträge vom CleverCure-Sender **nicht** mehr geändert werden dürfen.<br>
Warum? Wie willst du denn deine Fertigung steuern, wenn dir der Kunde laufend die Auftragsdaten verändert?

## zu klären
Werden versteckte Artikel trotzdem in die AB übernommen, oder kommt deswegen ein Fehler, weil nicht ordentlich unterstützt?

Anbindung Clever Cure
=====================

Mit der Anbindung an CleverCure (siehe dazu auch [CureComp.com](http://curecomp.com)) wurde eine praktische Schnittstelle für einen Teil der Supplychain geschaffen.

Die im Wesentlichen abgebildeten Funktionen sind:
-   Übertragen einer Bestellung aus dem Kundensystem in Ihr **Kieselstein ERP**
-   Senden der Auftragsbestätigung aus Ihrem **Kieselstein ERP** an den Kunden
-   Senden eines Lieferavisos

**Voraussetzungen:** [(Konfiguration siehe unten)](#Konfiguration)
-   **Kieselstein ERP** Webservices
-   **Kieselstein ERP** RestAPI Server
-   HTTPS Zugang bidirektional, abgesichert durch Zertifikate. Eine entsprechende Parametrierung der Firewall ist erforderlich.
-   Parameterdaten wie Kundennummern, Lieferantennummern usw.
-   Kunden-Sonderkonditionen zur Pflege der Kundenartikelnummern

**Ablauf:**

Nach erfolgter Parametrierung und Einrichtung des Systems sieht der Ablauf wie folgt aus:
1.  Vom Kundensystem wird eine Bestellung mit Auftrags-, Rechnungs- und Lieferadresse und den inhaltlichen Positionen gesandt.
    Von der **Kieselstein ERP** CleverCure Schnittstelle werden diese Daten, soweit sie technisch (Achtung: Nicht inhaltlich) in Ordnung sind, gültig entgegengenommen.

2.  Diese Daten werden konvertiert und als Auftragsbestätigung in Ihrem **Kieselstein ERP** angelegt. Zugleich wird ein E-Mail an den CleverCure E-Mail-Empfänger über den Auftragseingang gesandt.
    Details zur Datenzuordnung siehe unten.

3.  Von Ihnen wird die Auftragsbestätigung geprüft und gegebenenfalls korrigiert.
    Bitte achten Sie darauf, dass --> Positionensplitt ....

4.  Drucken Sie nun die Auftragsbestätigung, zumindest in die Vorschau, aus. Entspricht die AB Ihren Vorstellungen, so

5.  senden Sie diese per CleverCure an Ihren Kunden, durch Klick auf den Knopf Auftrag übermitteln ![](Clever1.gif) in den Auftrags-Kopfdaten.
    Wurden bereits Daten an Ihren Kunden gesandt, so:
    - wird dies durch einen grünen Hintergrund im Knopf angezeigt ![](Clever2.gif) und der Zeitpunkt des Versands wird in der Fußzeile angezeigt.
    - sollte die Auftragsbestätigung erneut an den Kunden gesandt werden, so wird nach dem Klick auf den Auftrag übermitteln Knopf die Meldung 
    ![](Clever3.gif)  
    angezeigt, welche mit Ja bestätigt werden muss. Bitte beachten Sie, dass damit eine entsprechende Änderung der Auftragsdaten auch bei Ihrem Kunden vorgenommen wird.
    Info: Neben den üblichen Daten aus **Kieselstein ERP** wird auch das Ursprungsland, welches im jeweiligen Artikel hinterlegt ist, in der Auftragsbestätigung mit zurückgesandt.

6.  Nach erfolgter Beschaffung / Produktion erfolgt die Lieferung der Ware, in der in **Kieselstein ERP** üblichen Art und Weise, Auftragsbezogener Lieferschein, Sicht Auftrag.

7.  Ist die Lieferung fertig zusammengestellt und der Lieferschein ausgedruckt, so senden Sie Ihrem Kunden eine Lieferaviso. Klicken Sie dazu im Lieferschein, Kopfdaten auf Lieferaviso übermitteln ![](Clever1.gif). Auch hier gilt, wenn ein Lieferaviso bereits versandt wurde, wird dies durch den grünen Hintergrund im Lieferaviso Knopf angezeigt. Auch das Lieferaviso kann erneut versandt werden.

**Zuordnung der Daten zwischen CleverCure und **Kieselstein ERP****

| Clever Cure | **Kieselstein ERP** | Bemerkung |
| --- |  --- |  --- |
| <BUYER_PARTY> <PARTY_ID> | Auftragsadresse | Diese muss im Kunden als Lieferantennummer hinterlegt sein. Bitte überprüfen Sie auch die im Kunden hinterlegten UID. Dies ist zugleich auch die Basis für die Zuordnung der Kundenartikelnummern zu Ihren eigenen Artikelnummern. |
| <SHIPMENT_PARTIES> <DELIVERY_PARTY> | Lieferadresse | Es muss dazu die Adresse im Wortlaut übereinstimmen. D.h. die ersten beiden Zeilen der Adresse und die Straße. Gegebenenfalls nicht konvertierbare Zeichen werden ignoriert |
| <INVOICE_PARTY> | Rechnungsadresse | Dazu muss im Kunden die UID Nummer hinterlegt sein. Wenn diese UID nicht übereinstimmt, so wird die Auftragsadresse verwendet. |
| <CC_DRAWING_NR> <CC_INDEX_NR> | Kundenartikelnummer + Revisions- nummer | In der Rechnungsadresse werden in den Soko die Kundenartikelnummern gesucht. Parallel zur Zuordnung wird auch die Revision zwischen CC und dem Feld Revision im **Kieselstein ERP** Artikelstamm geprüft. |
| <LINE_ITEM_ID> | Positions-ID | Essentielle Nummer für die Zuordnung der Position zwischen den CC-Partnern. Für die Durchgängigkeit des Ablaufs von Bestellung bis zum Lieferaviso. Diese muss genau übereinstimmen! Sie finden die Information in der Texteingabe zur Position in **Kieselstein ERP**. |
| <CC_ORDER_UNIT> | Mengeneinheit | Diese muss mit der in **Kieselstein ERP** eingetragenen Kennung übereinstimmen. Siehe dazu: System, Sprache, Einheit. Wenn diese Kennung hier nicht vorhanden ist, so wird die Mengeneinheit Stk. verwendet. Die Information, welche Mengeneinheit bestellt wurde, finden Sie in der darüberliegenden Information. Ändern Sie diese entsprechend ab, um im Lieferavisio eine Übereinstimmung zu erzielen. |
| <TRANSPORT> <INCOTERM> | Lieferart | Auch die Lieferart muss mit der in **Kieselstein ERP** eingetragenen Kennung übereinstimmen. Siehe dazu Lieferadresse, Konditionen, Lieferart bzw. System, Mandant, Lieferart, Kennung. Siehe dazu auch [Incoterms]( {{<relref "/start/04_allgemeine_stammdaten/#incoterms" >}} ) im Modul System unterer Modulreiter Mandant oberer Reiter Lieferart. |
| <DELIVERY_START_DATE> <DELIVERY_END_DATE><ORDER_DATE> | Auftragstermine | In den Kopfdaten des Auftrags in **Kieselstein ERP** wird in den Liefertermin der früheste Positionstermin und in den Finaltermin der späteste Positionstermin eingefügt. Das Bestelldatum entspricht der Information, die im Order-Date.  |
| <DELIVERY_START_DATE> | Positionstermin | Dieser Termin ist der "Wunschtermin" aus der Bestellung des Kunden und kann der Liefersituation entsprechend für das Lieferavisio angepassst werden. |
| <PRICE_AMOUNT> | Nettopreis  | Der erhaltene Preis, welcher mit dem im Artikelstamm / Kundensonderkonditionen für diese Auftragsadresse hinterlegten Preis übereinstimmen muss. |

**Infos / Fehlermeldungen beim Auftragsimport:**

Werden Unstimmigkeiten zwischen den in Ihrem System hinterlegten Daten und den erhaltenen Daten festgestellt, so werden entsprechende Info: Texteingaben in die Auftragsbestätigung vor der jeweiligen Ident/Handeingabe eingefügt. Prüfen Sie diese Fehler, beseitigen Sie diese im Auftrag und vor allem in der Definition Ihrer Daten. So erhalten Sie sehr rasch ein zu / mit Ihrem Kunden abgestimmtes System.

Ein Beispiel hierfür ist, dass eine Artikelnummer in **Kieselstein ERP** gefunden wird, aber keine der CC-Bestellung entsprechende Revisionsnummer vorhanden ist. Diese Information wird als Texteingabe in den Auftrag geschrieben. Eine Vorgehensweise dazu wäre, dass Sie nun einen neuen Artikel anlegen mit der aktuellen Revisionsnummer und diesen in der Position im Auftrag abändern.

Wenn eine Position (Ident) über die Sonderkonditionen nicht gefunden wird, so wird nach der Artikelnummer gesucht, wenn auch hier keine Übereinstimmung besteht, so wird eine neue Position mit der Art Handeingabe angelegt, die alle Informationen zur Bestellposition enthält (Bezeichnung, Menge, Mengeneinheit, LINE_ITEM_ID, Mwst-Sätze, gesuchte Artikelnummer).

Wenn keine Preise/Verkaufspreisbasis für einen Artikel hinterlegt sind, wird der Betrag "0.00" für die Auftragsposition verwendet. Zusätzlich gibt es eine Textposition mit der Mitteilung, dass der Preis nicht ermittelt werden konnte und deshalb auf 0 gestellt wurde.

ACHTUNG: Die LINE_ITEM_ID muss in jeder Position des Auftrags als Texteingabe in folgender Form {"lineitemid" : "1"} enthalten sein!
Verwenden Sie für die Umwandelung einer Handeingabe in einen Artikel den Menüpunkt Bearbeiten, Handartikel in Artikel umwandeln, so bleibt diese Information erhalten.

**Welche Daten werden von der Schnittstelle in der Dokumentenablage des jeweiligen Auftrages abgelegt?**

-   Import Clevercure XML Import_Clevercure<br>
    Dies sind die Originaldaten, die **Kieselstein ERP** von CleverCure erhalten hat

-   Export Clevercure XML Auftragbestätigung_osa_Clevercure<br>
    Diese Daten wurden als Auftragsbestätigung an CleverCure gesandt

**Daten, die zum Export des Lieferavisos abgelegt werden sind:**

-   Export Clevercure XML Lieferaviso_Clevercure<br>
    Diese Daten wurden als Lieferaviso an CleverCure gesandt

**Ergänzende Infos in den Formularen zu CleverCure**

-   Auftragsbestätigung

    Parameter java.lang.String P_VERSANDWEG_BESTAETIGUNG_TERMIN, der Zeitpunkt (Datum&Uhrzeit) an dem die Bestätigung gesendet wurde als Datum formatiert. Null wenn noch nicht bestätigt
    Parameter java.lang.Boolean P_VERSANDWEG, "true" wenn für diesen Auftrag ein Versandweg (Clevercure) festgelegt ist, "false" wenn nicht

-   Lieferschein

    Parameter java.lang.String P_VERSANDWEG_AVISO_TERMIN, der Zeitpunkt (Datum&Uhrzeit) an dem das Aviso gesendet wurde als Datum formatiert. Null wenn noch nicht avisiert

    Parameter java.lang.Boolean P_VERSANDWEG, "true" wenn für diesen Auftrag ein Versandweg (Clevercure) festgelegt ist, "false" wenn nicht

    Feld java.lang.String F_AUFTRAG_BESTAETIGUNG_TERMIN der Zeitpunkt (Datum&Uhrzeit) an dem die Bestätigung gesendet wurde als Datum formatiert. Null wenn noch nicht bestätigt

    Feld java.lang.BOOLEAN F_AUFTRAG_VERSANDWEG "true" wenn für diesen Auftrag ein Versandweg (Clevercure) festgelegt worden ist, ansonsten "false"

<a name="Konfiguration"></a>Konfiguration zu Beginn

**Kieselstein ERP** Webservices - in der Standard-Installation von **Kieselstein ERP** enthalten.

**Kieselstein ERP** RestAPI Server

siehe [Grundeinstellung und Definition RestAPI]( {{<relref "/docs/stammdaten/system/web_links/rest_api/#kieselstein-rest-api">}} )

Zusätzlich gibt es einen **Kieselstein ERP** Benutzer, der die Interaktion von **Kieselstein ERP** und dem RestAPI-Webserver durchführt. Dieser muss auf beiden Seiten definiert werden. 

Im Tomcat passen Sie die Definition restapi.xml nach folgendem Vorbild an:

![](user_restapi_definition.JPG)

## Weitere Definitionen in **Kieselstein ERP**

Legen Sie dazu im Modul Benutzer einen neuen Benutzer an und geben Sie ihm eine Benutzerrolle und hinterlegen den Mandanten. Die Benutzerrolle muss die Rechte AUFT_AUFTRAG_CUD, AUFT_AUFTRAG_R,  PART_LIEFERANT_R, PART_PARTNER_R, WW_ARTIKEL_R besitzen. Hinterlegen Sie beim Benutzer je nach Anforderung eine neue Person (Modul Personal Neu) oder eine bestehende. 

Geben Sie im Modul Personal oberer Reiter Daten jene E-Mail Adresse ein, an die eine Nachricht gesendet wird, wenn in **Kieselstein ERP** ein neuer Auftrag angelegt wurde. Um die Nachricht an mehrere E-Mail Adressen zu verschicken, geben Sie die Adressen mit einem Semikolon (;) getrennt ein.

Im oberen Reiter Detail muss im Feld Ausweis die Identifikationsnummer für das Clevercure-Portal eingegeben werden.

Im Modul Artikel unterer Reiter Grunddaten legen Sie im oberen Reiter Webshop einen neuen Webshop an. Der Name muss ebenso der Definition im restapi.xml entsprechen.

Im Modul System unterer Reiter Sprache, oberer Reiter Einheiten legen Sie die Einheiten PCE (Stück) und KGM (kg) an. Bitte bedenken Sie, falls weitere Übersetzungen der Einheiten aus der Auftragsübermittlung nötig sind, diese hier anzulegen.

**HTTPS Zugang bidirektional, abgesichert durch Zertifikate. Eine entsprechende Parametrierung der Firewall ist erforderlich.**

**Parameterdaten wie Kundennummern, Lieferantennummern usw.**

Für die Definition, für welche Kunden Sie die Clevercure-Schnittstelle verwenden, wenden Sie sich dazu derzeit an Ihren **Kieselstein ERP** Betreuer. 

Für die Definition im Clevercure-Portal, für welchen Kunden Sie eine Auftragsbestätigung/Lieferaviso schicken, geben Sie im Modul Kunde, oberer Reiter Konditionen Ihre eigene Kundennummer im Feld Lieferantennummer ein. 

Kunden-Sonderkonditionen zur Pflege der Kundenartikelnummern

Tragen Sie die Artikelnummern für die Kunden jeweils im Modul Kunde - oberer Reiter Sonderkondition ein. Klicken Sie dazu auf neu, wählen Ihren Artikel aus und geben die kundenspezifische Ident ein. Achtung: Beachten Sie das Feld Gültig ab und tragen hier das gewünschte Datum ein.
Zusätzlich definieren Sie im Modul Artikel bei diesem Artikel die Revisions-Nummer.

VMI Artikel

Es kommt immer wieder die Frage, wie ist im Rahmen von CleverCure mit den VMI Artikel (Vendor Managed Inventory) umzugehen.
Die Antwort dazu ist eigentlich, es ist dies durch entsprechende Verträge zu regeln.
Die Bedeutung von VMI ist im Sinne von CleverCure, dass Artikel quasi sofort, spätestens am nächsten Tag geliefert werden müssen.
Da bedingt, dass Sie als Lieferant an Ihren CleverCure Kunden wissen müssen, welche Artikel das sind. Sie müssen auch wissen, welche Menge Sie zu bevorraten haben.
Sind diese Artikel, durch Ihren Kunden, definiert so müssen im **Kieselstein ERP** entsprechende Lagermindeststände dafür hinterlegt werden, damit Sie immer lieferfähig sind.
Kommt nun ein neuer/zusätzlicher Artikel, aus dem Sinne des VMI über die Schnittstelle, so bedeutet dies nur, dass dieser Artikel nicht Vertragsbestandteil der VMI Vereinbarung ist. D.h. es hätte Ihnen Ihr Kunde rechtzeitig mitteilen müssen, dass er einen weiteren Artikel in die VMI Logik mit aufnimmt. In diesem Falle muss die weitere Vorgehensweise direkt (telefonisch) mit Ihrem Kunden geklärt werden. Dass Sie natürlich versuchen den Kunden entsprechend rasch zu beliefern, ist für alle Beteiligten sicherlich nachvollziehbar.