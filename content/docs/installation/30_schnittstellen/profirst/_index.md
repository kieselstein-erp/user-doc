---
categories: ["Schnittstellen"]
title: "Profirst Anbindung"
linkTitle: "Profirst"
date: 2023-01-31
weight: 400
description: >
  Profirst Anbindung
---
PROfirst Anbindung
==================

Mit der PROfirst Anbindung wird eine Verbindung zwischen dem Konstruktions- und Schachtelprogramm ProFirst und **Kieselstein ERP** geschaffen.
ProFirst wird vor allem in der Blechbearbeitung, wie z.B. Laserschneiden bzw. Plasmaschneiden eingesetzt.
Der grundsätzliche Ablauf ist in der Regel folgender ([siehe auch](#Ablauf)):

1.  Vom Kunden wird ein neues Teil angefragt. Dieses wird in PROfirst konstruiert und somit der Fertigungs-Artikel in PROfirst mit all seinen Eigenschaften angelegt.
    Ist der Fertigungs-Artikel fertig konstruiert, müssen die PROfirst-Daten von **Kieselstein ERP** eingelesen werden. Wählen Sie dafür im Artikelmodul, Pflege, PROfirst Import. Bei diesem Import wird ein direkter Zugriff auf Ihre PROfirst Datenbank gemacht und die Bauteile und deren Bildinformationen ausgelesen.

2.  Die Verdrahtung der Felder zwischen PROfirst und **Kieselstein ERP** ist wie folgt:
    Es wird dabei auch ein Bild des Artikels aus PROfirst nach **Kieselstein ERP** mit übernommen.
    Verdrahtung:

    | ProFirst | **Kieselstein ERP** | Bedeutung |
    | --- |  --- |  --- |
    | Artikelnummer | Stücklisten-Artikelnummer | Dies ist die Artikelnummer der Stückliste unter der der Fertigungs-Artikel angelegt wird. Auf die Länge achten, max. 25 Stellen, alphanummerisch |
    | Material | Stkl-Pos-Artikelnummer | Das Rohmaterial wird in die Stückliste als Materialposition übernommen. Die Abmessungen werden in mm für das umformende Rechteck übergeben. Auf diesen Abmessungen beruhen die Bedarfsberechnungen für das Gesamtmaterial. Die Material-Stärke wird in der Artikelnummer des Materials abgebildet. Das Rohmaterial ist entweder bereits angelegt oder wird beim Import von **Kieselstein ERP** erzeugt. Der Aufbau der Artikelnummer wird wie folgt errechnet: |
    | Maschine | Stkl-Arbeitsplan-Position | Erzeugt eine Arbeitsplanposition der Tätigkeit Autogen oder Plasma oder Laser -schneiden unter Angabe der Maschine auf der der Artikel geschnitten werden soll. Rüst- und Stückzeit kommen aus den Feldern .....und werden im Format hh:mm:ss übergeben. Es sind auch Zeiten > 24 Std. möglich. Die dafür erforderlichen Tätigkeiten müssen definiert sein. Die dafür erforderlichen Maschinen müssen ebenfalls definiert sein. Die Arbeitsgangposition wird mit der Materialposition verbunden. |
    | Bilddatei | Artikelkommentar Fertigungsbild | Es ist im Verzeichnis passend zur Artikelnummer auch eine Bilddatei im xxx (jpg/png) Format abgelegt. Diese wird bei der Stückliste als Artikelkommentar, mit der Markierung bei Fertigung mitdrucken hinterlegt. |
    | Bauteilname | Artikelnummer |   |
    | Kunde | Stückliste Kunde |   |
    | Stärke | Teil der Artikelnummer des Materials |   |
    | Werkstoff | Teil der Artikelnummer des Materials | Hinterlegen des Werkstoffs beim Material |
    | Werkstofftyp | Teil der Artikelnummer des Materials | Bezeichnung des Materials |
    | Form |   | wird nicht verwendet |
    | Menge |   | wird nicht verwendet |
    | Ändern |   | wird nicht verwendet |
    | Fläche (Brutto) | Fläche des Materials | Wird in mm² als Material in die Stücklistenposition eingetragen |
    | Preis NettoField 1 | VK-Preisbasis | Verkaufspreisbasis. Das Datum gültig ab wird mit dem Importdatum vorbesetzt |
    | Knd.Art.Nr.Field 2 | Kurzbezeichnung | Wird üblicherweise als Knd.Art.Nr verwendet und auch bezeichnet |
    | ZeichnungsNrField 3 | Referenznummer | Wird üblicherweise als Zeichnungsnummer verwendet und auch bezeichnet |
    | AnarbeitungField 4 |   | Fremd-arbeitsgang-artikel (siehe unten) |
    | KommissionField 5 | Zusatzbezeichnung2 |   |
    | Feld 6 | Bauform | Wird üblicherweise als Beschreibung verwendet und auch bezeichnet |
    | Bearbeitungszeit | Stückzeit | in hh:mm:ss |
    | Maschine | Maschine | Name (= Inventarnummer) der Maschine auf die die Tätigkeit gebucht werden sollte |
    | Schneidart | Tätigkeit | Artikelnummer der Tätigkeit des Arbeitsgangs, beginnt mit AZ_ gefolgt von der Schneidart. Muss angelegt sein. Es ist dies die Tätigkeit hinter der dann obige Stückzeit und Maschine hinterlegt werden. Das Material des Arbeitsgangs zeigt auf die Materialposition |
    | Gewicht (kg) | Artikel-Gewicht | Gewicht im Artikel im Reiter Technik |
    | Datum | wird nicht genutzt |   |

3.  Nun wird gegebenenfalls das entsprechende Angebot in **Kieselstein ERP** erstellt und an den Kunden übermittelt.

4.  Der Kunde bestellt, in **Kieselstein ERP** wird eine Auftragsbestätigung mit dem Kundenartikel, welches der oben übernommene Fertigungsartikel, der wiederum eine Stückliste ist, erstellt. Natürlich können beliebig viele Positionen im Kundenauftrag erfasst werden.

5.  Mit dem internen Bestellvorschlag werden die anzulegenden Lose ermittelt und erzeugt.
    Nun stehen die zu produzierenden Lose mit deren Arbeitsgängen, in denen wiederum die Maschinen enthalten sind, und den Materialbedarfen in **Kieselstein ERP** zur Verfügung.

6.  Im Modul Los, unterer Reiter offene AGs, oberer Reiter produzieren sind alle angelegten Lose und deren Arbeitsgänge mit Kunde, Los Nr, Material, Maschine und Fortschritt ersichtlich.
    Anhand dieser Liste, oder auch anhand des Journals, Maschine + Material, kann der Werker / Arbeitsvorbereiter entscheiden, welche Fertigungsaufträge / Arbeitsgänge er als Arbeitsvorrat für die nächste(n) Tafel(n) an PROfirst übergibt.
    In dieser Liste finden Sie auch die Anzeige der jetzt produzierbaren Lose. Und wenn Sie mehrere Lose markieren, so wird oben die Summe der Stunden und der Materialmenge, was in der Regel die Fläche der Tafel sein wird, angezeigt. So bekommen Sie ein Gefühl dafür, ob diese Lose aus der Tafel / den Tafeln geschnitten werden können.

7.  Für die Übergabe markiert man nun die gewünschten Lose der Maus und Klick auf ![](ProFirst13.gif) Übergabe an PROfirst.
    Damit werden Stkl-Artikelnummer, Losgröße, Kunde, ... an PROfirst übergeben.
    Zugleich werden die Lose dadurch mit dem Zusatzstatus in PROfirst versehen. Das Los selbst bleibt im Status angelegt.

8.  Nun lesen Sie den Arbeitsvorrat in Ihr PROfirst ein.
    Bitte achten Sie dabei darauf, dass Ihr PROfirst so eingestellt ist, dass vorhandener Arbeitsvorrat immer ergänzt, also erhöht wird.

9.  Nun werden in PROfirst die Bauteile auf die Tafeln geschachtelt.

10. Die geschachtelten Bauteile, mit der Information welche Tafel für welches Los (= Fertigungsauftrag) vorgesehen ist, wird wiederum von PROfirst exportiert.

11. Nun wird der Schachtelplan durch Klick auf .... eingelesen.
    Beim Einlesen werden die im Schachtelplan enthaltenen Artikel/Bauteile so auf die Lose verteilt, dass immer das älteste Los eines Bauteiles verwendet wird. Sollte nicht die gesamte Losgröße in diesem Schachtelplan / der Tafel verwendet worden sein, so wird auch das Los gesplittet, damit wiederum die Gesamtmenge dieses Artikels erhalten bleibt und auch die Information, dass im PROfirst noch offene Lose für dieses Bauteil gegeben sind. Beim Splitting eines Loses wird der Zusatzstatus, dass dieses Los bereits in PROfirst ist, mitübernommen.
    Die Information, welchem Schachtelplan das Los zugeordnet ist, wird beim Los hinterlegt. Es kann auch nach den Schachtelplannummern gesucht werden. Wir gehen davon aus, dass der Dateiaufbau des Schachtelplans mit csvnnnnn.csv gegeben ist, wobei nnnnn die Schachtelplannummer ist. Wird eine Schachtelplannummer eingegeben, die nicht vorhanden ist, erhalten Sie einen entsprechenden Hinweis.

12. Beim Einlesen des Schachtelplans erhalten Sie eine Liste der laut **Kieselstein ERP** lagernden Tafeln dieses Materials. Wählen Sie nun das tatsächlich verwendete Material aus.

13. Da im Schachtelplan auch die Menge definiert ist, die nach dem Schneiden als nutzbare Restmenge übrigbleibt, kann eine Restmengenberechnung durchgeführt werden. D.h. es werden die Los-Sollmengen aller Lose des Schachtelplans der laut Schachtelplan verbrauchten Menge gegenübergestellt und daraus ein Korrekturfaktor ermittelt mit dem die Lossollmengen umgerechnet / korrigiert werden. Damit wird erreicht, dass bei jedem Einlesen eines Schachtelplanes die verbleibende Restmenge einer Tafel zwischen **Kieselstein ERP** und PROfirst übereinstimmt.
    Zugleich wird bei diesem Vorgang nach der Korrektur der Sollmengen das gesamte Material der Lose dieses Schachtelplans ausgegeben, vom Lager in die Lose gebucht.

14. Ist die Tafel nun geschnitten, so muss dies ebenfalls in **Kieselstein ERP** erfasst werden.
    Klicken Sie dazu auf ....
    Es erscheint erneut die Abfrage nach dem Schachtelplan. Nun werden alle Lose dieses Schachtelplans mit deren Sollmengen angezeigt. Es kann nun bei jedem Los angegeben werden, ob eventuell Ausschuss zu erfassen ist. Geben Sie die unbrauchbaren Mengen für das jeweilige Los an und klicken Sie dann auf Erledigt/Buchen.
    Damit werden die angegebenen Lose mit den Losgrößen abzüglich den Ausschussmengen als Ablieferungsbuchung gebucht. Womit die Teile auf Lager sind und z.B. per Lieferschein ausgeliefert werden können.

Anmerkungen:

-   Da von PROfirst der Materialbedarf in x & y Abmessungen übergeben wird, sind die Dimensionen für m² und mm² in den Mengeneinheiten entsprechend auf 2 einzustellen.

-   Beim Erstellen neuer Bauteile in PROfirst achten Sie bitte darauf, dass die Bestellmenge immer 0,00 bleibt. Anderenfalls würden Sie mehr schachteln / produzieren als Sie aufgrund des offenen Auftragsstandes tatsächlich sollten.

-   Sollte ein Kunde einen Auftrag stornieren, so ist neben dem Storno des Loses auch die entsprechende offene Menge des Bauteils zu korrigieren / auf 0,00 zu setzen

Zuordnung der Tafelnummer zur Chargennummer

1.  Bei der Buchung des Wareneinganges werden anstelle der Chargennummern, welche in der Regel bei der Anlieferung nicht sichtbar sind, Tafelnummern erfasst. Wichtig ist hier, dass die Tafelnummern bereits jede Tafel vereinzeln, d.h. für jede Tafel ist eine aufsteigende eigene Tafelnummer zu verwenden. Diese werden in der Zubuchung des Wareneinganges durch Klick auf "Tafelnummer" erzeugen generiert und für jede einzelne Tafel hinterlegt. Damit liegen die Tafeln mit diesen Nummern auf Lager. Die Tafelnummer ist der Ersatz für die Chargennummer. Das bedeutet auch, dass Tafeln, die noch nicht in Chargennummern umgewandelt wurden -->>> FRAGE: Umwandeln oder Ergänzen, in den Lagerinformationen eindeutig ersichtlich sind.

2.  Die Tafelnummer wird in **Kieselstein ERP** in der Version der Charge abgelegt. Die eigentliche Chargennummer wird mit "noch nicht definiert" vorbesetzt.
    Die vergebenen Tafelnummern werden durch die Wareneingangsetiketten ausgedruckt und bei den jeweiligen Tafeln durch Aufkleben auf die Tafeln bzw. deren Begleitpapiere identifiziert. (Z.B. ....) Die Tafelnummer ev. in Kombination mit der Artikelnummer, wird auch in Barcodeform auf der Wareneingangsetikette angedruckt.

3.  Zuordnung der Chargennummern zu den Tafelnummern.
    Durch die Entnahme der Tafel, welche einige Tonnen hat, wird deren Chargennummer sichtbar. Der Werker geht nun an das BDE-Terminal, identifiziert sich mit seiner Ausweiskarte, und tippt auf Tafelnummern zuordnen. Nun scannt er die Tafelnummer ein und tippt die Chargennummer, welche er sich vorher auf dem Tafelnummernetikett notiert hat, in das Terminal ein. Die Chargennummern sind durch Ziffern und normale Zeichen (keine Umlaute, keine Sonderzeichen (vor allem wiederum wegen Barcode)) definiert.

4.  Aus Gründen der Rückverfolgbarkeit sollte die Zuordnung Tafelnummer <-> Chargennummer immer erhalten bleiben. Damit wird, je nach Menge der verarbeiteten Tafeln, die Anzahl der laufenden Stellen der Tafelnummern definiert. Da pro Tag und Schicht und Maschine ca. eine Tafel verarbeitet wird, ist bei drei Maschinen und Dreischichtigem Betrieb von 3285 Tafeln pro Jahr auszugehen. D.h. die Tafelnummern sind mindestens 6-stellig auszulegen. Wir würden der Einfachheit und klareren Zuordnung halber, die Tafelnummern über alle Artikel ziehen. D.h. egal ob das ein Mittelformat mit 2 mm oder ein 4 x 6 m x 10 cm Blech ist, die eine Tafelnummer kommt nach der anderen.

5.  Nachdrucken von Tafel-Etiketten.
    Diese können im Artikel unter Info, Etikett und Angabe der Chargennummer bzw. Tafelnummer jederzeit nachgedruckt werden.

Parametrierung für PROfirst

Die PROfirst Artikeldaten (Parts) werden als Stücklisten Daten nach **Kieselstein ERP** importiert.
Dazu wird von **Kieselstein ERP** direkt auf die Tabellen Ihres PROfirst Servers zu gegriffen. Um diesen Zugriff zu ermöglichen, müssen folgende Parameter definiert werden:

| Parameter | Bedeutung | Beispiel |
| --- |  --- |  --- |
| PRO_FIRST_DBURL | Der Pfad zu Ihrer PROfirst Datenbank. Beachten Sie bitte dazu auch die untenstehenden Anmerkungen | jdbc:jtds:sqlserver://192.168.8.57:1433/profirst |
| PRO_FIRST_DBUSER | Der Benutzer für den Datenbankzugriff. Die Details dazu erhalten Sie von Ihrem PROfirst Betreuer |   |
| PRO_FIRST_DBPASSWORD | Das Kennwort, um auf die Datenbank zuzugreifen |   |
| PRO_FIRST_ANZAHL_ZU_IMPORTIERENDE_DATENSAETZE | Gibt die in einem Importdurchlauf aus PROfirst eingelesenen Stücklisten an. | Steht Default auf 1 um die Verbindung und den Import testen zu können. Erhöhen Sie diesen nach erfolgreicher Parametrierung auf z.B. 50 Datensätze |
| ARTIKEL_MAXIMALELAENGE_ARTIKELNUMMER | Erlaubte Länge der Artikelnummer | 25 (Bitte auf Maximalwert stellen) |
| ARTIKEL_LAENGE_HERSTELLERBEZEICHNUNG | Kein Herstellerbezug | 0 |
| HERSTELLERKURZZEICHENAUTOMATIK | Abschalten | 0 |
| ARTIKELNUMMER_ZEICHENSATZ | Definieren Sie die erlaubten Zeichen. Bitte unbedingt ohne Leerzeichen, zu Ihrer eigenen Sicherheit. | ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_. |
| ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE | Damit werden die Blechdicken in der Artikelauswahl angezeigt |   |
| PRO_FIRST_LEAD_IN_AZ_ARTIKEL | Definieren Sie hier bitte die führenden Buchstaben für die Artikel für die Tätigkeit | AZ_ |
| PRO_FIRST_VERZEICHNIS | In dieses Verzeichnis werden die von **Kieselstein ERP** erzeugten Exportdateien für PROfirst abgelegt. Bitte stellen Sie sicher, dass JEDER Benutzer auf dieses Verzeichnis ein entsprechendes Zugriffsrecht besitzt. Wir raten zur Verwendung von UNC Namen, da diese in den Netzwerken eindeutig sind. Achten Sie darauf, dass das angegebene Verzeichnis auch vorhanden ist. | \\server\PROfirst\CSV\ |
| VERPACKUNGSMENGEN_EINGABE | Da in dieser Branche sehr oft in Tafeln gedacht wird, sollte dieser Parameter auf 1 gestellt werden. Damit erreichen Sie, gemeinsam mit der Hinterlegung der Verpackungsmenge, dass die Tafelanzahl auf den Bestelldaten mit angedruckt werden. |   |
| PRO_FIRST_BILD_PFAD | Pfad, in dem die von PROfirst automatisch erzeugten Bilder abgelegt werden. Diese werden in die Artikelkommentare der Stücklisten übernommen. Dieser Pfad muss aus Sicht des ****Kieselstein ERP** Servers** definiert werden. | Die Einstellung und die Definition in welchen Pfad Ihr PROfirst dies abspeichert, finden Sie unter Extras, Optionen, Bauteil-BMP. Hier bitte die automatische Speicherung anhaken und den entsprechenden Pfad angeben. Als Dateiformat bitte JPG verwenden. Nutzen Sie für bestehende Daten den Knopf für bestehende Bauteile Bilder erzeugen. Beispiel: *\\\\server\\PROfirst\Bilder* bedeutet bei einem Linux-Server aus **Kieselstein ERP** /mnt/profirst/Bilder |
| PRO_FIRST_FREMDFERTIGUNGSARTIKEL | Im PROfirst können auch Fremdarbeitskosten definiert werden. Diese werden im Zusatzfield5 (siehe unten) angegeben. Geben Sie hier die Artikelnummer an unter der diese Kosten in die Stückliste übernommen werden sollten. | AZ_FREMD |
| PRO_FIRST_ARTIKELGRUPPE | Die Artikelgruppe unter der Ihre PROfirst Artikel angelegt werden. Ist insbesondere dann erforderlich, wenn in **Kieselstein ERP** auch die integrierte Finanzbuchhaltung verwendet wird. |   |
| PRO_FIRST_SCHACHTELPLAN_PFAD | Pfad in dem die Schachtelplan-CSV-Dateien gefunden werden. Importierte Schachtelpläne werden in das Unterverzeichnis Old verschoben. |   |
| DEFAULT_ARBEITSZEITARTIKEL | Dieser Arbeitszeitartikel wird in den Arbeitsplänen der Stücklisten für die Tätigkeiten hinterlegt, bei denen in der Bauteildefinition Ihres PROfirst zwar eine Arbeitszeit aber keine Tätigkeit definiert ist. |   |

Die in PROfirst definierbaren Zusatzfelder werden wie folgt übernommen:
- Field1 ... Verkaufspreisbasis des Artikels
- Field2 ... Artikel Kurzbezeichnung, welche üblicherweise für PROfirst Anwender mit Kundenartikelnummer bezeichnet ist
- Field3 ... Artikel Referenznummer, welche üblicherweise für PROfirst Anwender mit Zeichnungsnummer bezeichnet ist
- Field4 ... wird für die Darstellung der Fremdarbeitskosten verwendet. D.h. um diesen Zahlenwert zu übernehmen, muss der PRO_FIRST_FREMDFERTIGUNGSARTIKEL definiert sein. Bitte beachten Sie, dass dessen Mengeneinheit EUR € sein sollte, da ja hier nur Kosten übergeben werden. Diese "Menge" = Kosten werden in die Stückliste als zusätzliche Materialposition übernommen.
- Field5 ... Artikel Zusatzbezeichnung2
- Field6 ... Artikel Bauform, welche für PROfirst Anwender auch als Beschreibung bezeichnet ist
- Field7 ... Artikel Verpackungsart -> dieses Feld wird von der PROfirst Version 9 nicht unterstützt
- Field8 ... Wird von PROfirst Version 9 nicht unterstützt / verwendet und daher auch in **Kieselstein ERP** nicht ausgewertet

Damit die PROfirst Funktionen zur Verfügung stehen, muss zumindest die PRO_FIRST_DBURL definiert sein.

Zusätzlich achten Sie bitte am Profirst Server, dass die TCPIP Verbindungen aktiv / erlaubt sind.
![](ProFirst1.jpg)

Und dass die entsprechenden Ports im SQL Configuration Manager eingestellt sind:
![](ProFirst2.gif)

Testen Sie die Verbindung z.B. durch eine Verbindung mit dem SQL-Manager:
![](ProFirst3.jpg)

Mögliche Fehlermeldungen

Ungültige Zeichen
![](ProFirst4.jpg)
Grundsätzlich werden bei der Übernahme alle Zeichen auf Großbuchstaben umgesetzt. Sollten trotzdem nicht definierte Zeichen enthalten sein erhalten Sie eine Meldung ähnlich der obigen. Ergänzen Sie in diesem Falle die Definition der erlaubten Zeichen Ihrer Artikelnummer. In obigem Beispiel ist der . Punkt nicht definiert.
Der Import wird abgebrochen. Bitte ergänzen Sie die Artikelnummerndefinition oder ändern Sie den Namen in PROfirst

Kunde nicht definiert
![](ProFirst5.jpg)
Generell ist die Definition, dass die Verbindung der Kunden zwischen **Kieselstein ERP** und PROfirst über die Kunden-Kurzbezeichnung erfolgt. Wird kein Kunde mit der in PROfirst eingetragenen Kurzbezeichnung gefunden, so erscheint obige Fehlermeldung und der Import wird abgebrochen. Siehe unten / Kunden Import / ignorieren

Arbeitszeitartikel nicht definiert
![](ProFirst6.jpg)
Um die Arbeitszeitartikel von PROfirst in die Arbeitspläne von **Kieselstein ERP** übernehmen zu können, müssen diese definiert sein. Diese werden entsprechend dem Rulecode aus PROfirst übernommen. Diese Artikel müssen als Arbeitszeitartikel in **Kieselstein ERP** angelegt sein.
PROfirst Bauteile, die im PROfirst keine oder fehlerhafte Arbeitszeit-IDs (kein Rulecode oder Rulecode in Ruletabelle nicht vorhanden) werden auf den im Parameter DEFAULT_ARBEITSZEIT Arbeitszeitartikel gesetzt.

Maschine nicht definiert
![](ProFirst14.jpg)
Die bedeutet, dass für die angegebene Maschine (aus PROfirst) keine Maschine in Ihrem **Kieselstein ERP** mit der entsprechenden Inventarnummer gefunden wurde. D.h. bitte entweder die Maschinendefinition in Ihrem PROfirst richtigstellen (siehe Liste der Bauteile der Maschine) oder die Maschine in Ihrem **Kieselstein ERP** anlegen (Zeiterfassung, unterer Reiter Maschinen). Bitte beachten Sie, dass die Inventarnummer mit der Maschinenbezeichnung in PROfirst übereinstimmen muss. In **Kieselstein ERP** ist die Inventarnummer auf 15 Stellen begrenzt.

PROfirst Artikel und Stücklisten Import durchführen

Sind alle beschriebenen Einstellungen getroffen, so wählen Sie im Artikelmodul bitte Pflege, PROfirst Import.
Damit wird die jeweilige Anzahl an neuen Datensätzen eingelesen.
Bitte beachten Sie, dass wir, so wie es in der Praxis üblich ist, davon ausgehen, dass die Artikel in PROfirst erst abgespeichert werden, wenn diese eindeutig definiert sind.
Da der Import parallel von mehreren Mitarbeitern angestoßen werden kann, aber nicht sollte, würden unter Umständen nicht fertig definierte Artikel importiert. Wir haben uns daher dazu entschlossen, dass ein Artikel nur einmal eingelesen wird.

Aus dem Namen des PROfirst Artikels werden die Artikelnummern gebildet.
Würde sich daraus eine gleichlautende (doppelte) Artikelnummer ergeben, so werden die letzten Stellen durch _nnn hochgezählt. In die Bezeichnung wird der Originalname eingetragen.
Bitte beachten Sie, dass Artikel, deren Namen eine Artikelnummer ergeben würden, die kürzer als die Mindeststellenanzahl, üblicherweise 5, ist, ignoriert werden.

Der Import wird immer so durchgeführt, dass Artikel für Artikel eingelesen und die Stücklisten daraus erzeugt werden. Kommt es nun zu einem Abbruch, wegen eines Fehlers, eine ungültige Definition, so wurden alle Artikel bis zum Auftreten des Fehlers bereits eingelesen und sind bereits in Ihrem **Kieselstein ERP** vorhanden. D.h. wenn nun der Fehler korrigiert wird, so wird exakt bei dieser Fehlerbedingung wieder fortgesetzt und erneut eine bestimmte Anzahl an Datensätzen eingelesen.

Um die direkte Verknüpfung der Stücklisten zwischen **Kieselstein ERP** und PROfirst eindeutig darzustellen, wird die PROfirst PART.IDPART in der Stückliste in den Kopfdaten unter Fremdsystemnr. abgelegt. Damit kann programmtechnisch eindeutig auf den richtigen Artikel in PROfirst zurückgegriffen werden. Für den Import wird von der höchsten in **Kieselstein ERP** hinterlegten Fremdsystemnr. ausgegangen und die danach kommenden Artikel eingelesen.

Wareneingänge nach PROfirst exportieren

Um die im Wareneingang erfassten Tafeln auch an PROfirst zu signalisieren, wurde diese Funktion geschaffen.
Sie finden diese ebenfalls im Modul Artikel, Pflege, Export Wareneingang nach PROfirst.
Es werden damit alle Wareneingänge seit dem letzten Exportlauf an PROfirst über den Umweg einer CSV Datei übertragen.

Bitte beachten Sie, dass dies ein einmaliger Vorgang ist. Nachträgliche Änderungen an den Warenzugangsbuchungen werden NICHT an PROfirst übergeben.
Die Exportdatei wird ebenfalls im definierten PROfirst Exportverzeichnis abgelegt und beginnt mit WE_AB_Datum-Uhrzeit

Um die Daten nach PROfirst zu importieren, starten Sie bitte Ihr PROfirst, wählen dann
 ![](ProFirst7.gif) CAM, CAM Medium.

Klicken Sie nun unten auf Blechverwaltung
![](ProFirst8.gif)

nun wählen Sie
![](ProFirst9.gif)
Tafeln einlesen.

Geben Sie hier den unter PRO_FIRST_VERZEICHNIS definierten Pfad ein und definieren Sie den Import anhand folgender Spalten:
- Tafelnummer
- Material
- ArtikelnummerMaterial
- Staerke
- Breite
- Tiefe
- Chargennummer
- AnzahlTafeln
- Lieferant
- Dokument
- Preis/Kilo
- Werkstoff

Da üblicherweise nicht nur die verschiedenen Standardformate (Klein, Mittel, Groß) sondern auch Sonderformate verarbeitet werden, kann in der Wareneingangsposition die Breite / Tiefe (in mm) angegeben werden. Dazu muss in den Parametern die Dimension der BS-Pos-Einheit auf 2 gestellt werden.

## Export der zu schneidenden Lose / Arbeitsgänge
<a name="Lose an PROfirst exportieren"></a>
Verwenden Sie für die Signalisierung der von Ihnen gewünschten zu schneidenden Lose bzw. Arbeitsgänge im Modul Los den unteren Modulreiter offene AGs und dann den oberen Reiter [Produzieren]( {{<relref "/fertigung/losverwaltung/#welche-lose-k%c3%b6nnen-denn-nun-wie-produziert-werden" >}} ).<br>
Hier filtern Sie zuerst nach dem Material, das Sie schneiden möchten, z.B. durch Eingabe der Artikelnummer des Materials.<br>
![](ProFirst11.jpg)

Sortieren Sie nun die Lose in der Reihenfolge, in der diese gefertigt werden sollten. In der Regel wird das der AG-Beginn sein.
Sind hier Spalten OHNE AG-Beginn, so gibt es für diese Lose keine Arbeitsgänge, was bei importierten Stücklisten nicht sein sollte. Siehe oben.
Für die Sortierung klicken Sie nun auf die Spaltenüberschrift AG-Beginn.
Klicken Sie nun die Lose an, die Sie nach PROfirst zum Schachteln übertragen wollen. Damit Sie ein Gefühl dafür bekommen, ob denn die ausgewählten Lose auf die Tafel geschachtelt werden können, zeigen wir oben die Summe der Abmessungen der Materialien der markierten Lose an. Beachten Sie bitte, dass dies nur die mathematische Addition der in den Stücklisten eingetragenen Flächen ist. Es ist dies in keinem Falle ein wie immer gearteter Schachtelversuch. Nutzen Sie auch die Anzeige heute produzierbar, diese zeigt ob überhaupt ausreichend Material auf Lager ist.
Haben Sie nun alle Lose für die Übertragung markiert, so klicken Sie bitte auf ![](PROfirst_Lose_uebertragen.gif) nach PROfirst exportieren. Damit werden alle markierten Lose in die CSV Exportdatei exportiert.
Der Export wird durch ![](PROfirst_export.jpg) angezeigt.
Der Dateiname hat den Aufbau OAG-Datum-Uhrzeit.csv.

Zusätzlich wird das jeweilige Los mit dem Zusatzstatus P1 markiert.
Hinterlegen Sie hinter diesem Zusatzstatus ein entsprechendes Symbol und den für Sie passenden Begriff. Damit sehen Sie bereits in der Losübersicht, welche Lose zum Schachteln an PROfirst übergeben wurden.
Hinweis: Damit das neue Symbol angezeigt wird, muss der **Kieselstein ERP** Client neu gestartet werden.
Wann diese Daten an PROfirst übergeben wurden, ist je Los im Reiter Zusatzstatus ersichtlich.

Der Datenaufbau sieht z.B. wie folgt aus:
KundeKbez,Losnummer,StklArtikelnummerProFirst,OffeneLosmenge,MaterialProFirst,MaterialHoehe,,,,
Test,16/0000001,696-419-01-01-05,35,S355,25.0

Importieren Sie diese Daten in PROfirst unter CAM Medium, Fertigungsaufträge
![](ProFirst10.gif)Auftrag von PPS ein(lesen).

Bitte beachten Sie, dass dadurch das Los noch immer im Status angelegt ist. D.h. Buchungstechnisch ist das Material noch im Lager. Erst durch die Rückmeldung der Schachtelung wird das Los ausgegeben und damit das Material vom Lager abgebucht.

Einstellung des PROfirst für den Import

![](ProFirst12.jpg)

Für den Ablauf ist wichtig, dass die Verwaltung der offenen, d.h. der zu schneidenden Mengen in beiden Systemen gleich ist.

#### wie können Stücklisten aktualisiert werden?
hierfür stehen zwei Varianten zur Verfügung:

- a.) Aktualisieren einer einzelnen Stückliste. Wechseln Sie dazu im Stücklisten Modul auf die gewünschte Stückliste und wählen nun im Menü Bearbeiten, Stückliste aus PROfirst aktualisieren.
- b.) Aktualisieren aller Stücklisten eines Kunden. Wechseln Sie im Kundenmodul auf den gewünschten Kunden und wählen aus dem Menü, Pflege, PROfirst Import. Bitte beachten Sie, dass hier die Begrenzung auf die maximale Anzahl der zu importierenden Artikel NICHT wirkt. D.h. es werden alle Artikel dieses Kunden eingelesen. Es ist dieser Import jedoch auf ein reines Aktualisieren begrenzt. D.h. es werden nur die bereits in **Kieselstein ERP** eingetragenen Stücklisten aktualisiert. Das Einlesen neuer Artikel muss bitte immer im Artikelmodul, Pflege, PROfirst Import erfolgen (Hintergrund ist die Verwaltung der noch einzulesenden Artikel).

In anderen Worten ist die Bauteilübernahme aus PROfirst wie folgt definiert:

-   Kunde pflegt die bestehenden Artikel / Stücklisten ausgehend von PROfirst zu **Kieselstein ERP**

-   Artikel fügt hinzu: Es werden Bauteile die anhand ihrer PROfirst ID noch nicht im Artikel / Stücklistenstamm von **Kieselstein ERP** sind hinzugefügt = neu angelegt

#### Wie wird mit Verkaufspreisänderungen umgegangen?
Bei jeder Aktualisierung werden auch die aktuellen Verkaufspreise aus PROfirst übernommen und der Preis gültig ab Datum auf das Importdatum gesetzt. Somit sehen Sie im Artikel unter Info, Verkaufspreisentwicklung, wie sich der Preis im Laufe der Zeit verändert hat.

#### Können Kunden vom Import ausgenommen werden.
Ja. Wenn über den Import im Artikelmodul die Fehlermeldung Kundenkurzbezeichnung für xyz nicht gefunden, so wählen Sie Kunde für weitere Importe ignorieren. Damit wird der Kunde in die zu ignorieren Liste mit aufgenommen und beim nächsten Importlauf übersprungen. Sollten Sie die Artikel dieses Kunden später trotzdem importieren wollen, so legen Sie bitte den Kunden an bzw. definieren Sie die entsprechende Kundenkurzbezeichnung eines bestehenden Kunden und wählen Sie dann Pflege, PROfirst Import, wie oben beschrieben. Die nun erscheinende Frage beantworten Sie bitte mit Ja.

### Einfache Beschreibung des grundsätzlichen Ablaufes
<a name="Ablauf"></a>
1.  Importieren Sie alle benötigten Artikel
2.  Legen Sie die Kundenaufträge an
3.  erzeugen Sie die entsprechenden Lose, z.B. mit der internen Bestellung
4.  Bestellen Sie die nötigen Rohartikel, z.B. Blechtafeln
5.  Buchen Sie die Lieferungen im Wareneingang zu.<br>Achten Sie darauf, dass die Blechtafeln entsprechend Chargengeführt sind (es muss die Abfrage nach Chargennummern kommen)
6.  Exportieren Sie im Modul Los, unterer Reiter offene AGs oberer Reiter Produzieren.<br>
    Beachten Sie die produzierbaren Lose nach Sortierung
7.  Schachteln Sie die gewünschten Aufträge im PROfirst
8.  Einlesen eines Schachtelplanes in **Kieselstein ERP**. Damit automatische Zuordnung der Schachtelplannummern zu den Losen.
9.  Schneiden der Schachtelpläne
10. Abliefern der Schachtelpläne = Tafel abliefern unter Angabe des eventuellen Ausschusses
    Dabei Auswahl der tatsächlich verwendeten Tafel