---
categories: ["Praktische Zusatztools"]
title: "Praktische Zusatztools"
linkTitle: "Zusatztools"
date: 2023-01-31
weight: 1800
description: >
  Praktische Zusatztools
---
Wir dürfen immer wieder kleine praktische Tools für unsere Anwender und Mitglieder erzeugen.

Hier eine Auflistung, welche Tools aktuell zur Verfügung stehen

## XLS je Blatt verschlüsselt als PDF ablegen. -> Anwendungsfall, Lohnverrechung im XLS
## Aufgereihte Lieferanten Angebotspositionen als Kieselstein-ERP Lieferantenimport File zur Verfügung stellen, mit Angebotsnummern Ergänzungsscript
## Opticon Reader App
## Bildschirmschoner mit guter HTML Darstellung
## Anwesenheitsliste, Proxy-Server
## Backup-Liste für Starface Telefonanlagen