---
title: "Bedienung des Client"
linkTitle: "Bedienung"
weight: 0550
description: >
  Bedienung deines Kieselstein ERP Clients
---
Eine Zusammenfassung der Bedienung deines **Kieselstein ERP** Java Clients.

Der Client ist ein sogenannter (Rich Fat) Swing Client. Dieser mag auf den ersten Blick etwas veraltet erscheinen, hat aber den enormen Vorteil, dass er in seiner Bedienung wesentlich schneller ist, als z.B. ein reiner Browser-Client.

Tipps und Tricks
================

Mit dieser Seite wollen wir zusätzliche Hilfestellung beim Einsatz von **Kieselstein ERP** leisten.

## Kieselstein ERP Server ist nach IP Änderung nicht mehr erreichbar?
Wird die IP des Rechners mit dem **Kieselstein ERP** Server geändert, so muss der **Kieselstein ERP** Server neu gestartet werden.

## Unterschiedliche Eigenschaften am gleichen Drucker
<a name="Unterschiedliche Eigenschaften am gleichen Drucker"></a>
Ich habe zwei Grundeinstellungen bei meinem Drucker, zwischen denen ich laufend hin- und herschalten muss, z.B. für den Ausdruck von firmeninternen und -externen Dokumenten (Papier mit Firmenlogo)

Der Trick liegt hier darin, faktisch den Drucker ein zweites Mal unter einem anderen Namen zu installieren und diesem die gewünschten (anderen) Einstellungen zu geben.

## Geänderte Fußzeilen werden nicht gedruckt
Es wurden Änderungen an den Fußzeilen vorgenommen, seither kommt nur mehr OK und die Fußzeilen fehlen.

Hier ist der Hintergrund, dass der für die Fußzeilen(=Subreport) im Ausgangsreport vorgesehene Platz zu gering ist. Üblicherweise ist der vorgesehene Platz nicht hoch genug.

Bestimmen Sie anhand des Fußzeilen-Subreports die erforderliche Höhe und korrigieren Sie den Ausgangsreport.

Hinweis: Achten Sie bei Subreports auch darauf, dass Sie die Höhe und Breite so wie auch im Ausgangsreport definieren.<br>
Hinweis: Es wird für die Fußzeilen nur mehr die Fuss.jrxml verwendet.<br>
Siehe dazu auch ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\log\server.log 

## Ich will in den Auswahllisten auch die rundherum befindlichen Daten sehen
Manchmal ist es praktisch z.B. bei Rechnungen nicht nur die eine Rechnung Nr: 24/0000001 zu sehen, sondern auch die rundherum befindlichen Rechnungen. Um dies darzustellen gibt es einen Trick.

- Bestimmen Sie mit dem Direktfilter den gesuchten Datensatz. Z.B. Rechnung 24/0000001\. Nun löschen Sie die Eingabe des Direktfilters wieder ohne Enter zu drücken. Nun wechseln Sie z.B. in die Kopfdaten oder in die Positionen und wechseln sofort wieder zurück in den Auswahlfilter. Durch die Logik, dass **Kieselstein ERP** auf dem ausgewählten Datensatz stehen bleibt, befinden Sie sich auf dem gewünschten Datensatz. Da aber die Filterbedingung inzwischen entfernt wurde, werden alle Rechnungen angezeigt.

## Woran kann man erkennen, ob ein Fax tatsächlich versandt wurde?
<a name="Faxversand"></a>
Um den Versandstatus des Faxversandes zu sehen, starten Sie bitte das System-Modul. Wählen Sie den unteren Modulreiter Versandauftrag. Nun wählen Sie im oberen Modulreiter Gesendet. Nun sollten Sie das gesuchte Fax in der Versandliste sehen. Ist es hier nicht enthalten, so sehen Sie bitte unter Fehlgeschlagen oder Papierkorb nach. Ist das Fax unter Fehlgeschlagen, so kann die Ursache eventuell unter Status ermittelt werden. Ist das Fax unter versendet eingetragen, so wurde uns von der darunter liegenden Faxversandsoftware, welche Betriebssystemabhängig ist, gemeldet, dass das Fax ordentlich versandt wurde.

Nach meinem Client-Update erhalte ich beim Start des **Kieselstein ERP** Clients die Fehlermeldung:<br>
Could not find main class.<br>
![](Start_Fehler.gif)<br>
Dies bedeutet, dass auf Ihrem PC eine falsche Java-Version installiert ist. Aktualisieren Sie Java Virtual Machine. Siehe ( {{<relref "/docs/installation/02_client/" >}} )

## Übersetzung von Texten
In **Kieselstein ERP** können alle Lables (Beschriftungen der Knöpfe, Eingabefelder usw.) dynamisch übersetzt werden. Bei der Verwendung einer PostgresQL Datenbank ist zu berücksichtigen, dass die eingetragenen C_TOKEN <u>**OHNE**</u> endende Leerstellen eingegeben werden.

## Richtige Einstellung der Mehrwertsteuersätze für Deutschland
Da in Deutschland seit 1999 einige Mehrwertsteueränderungen waren, hier die richtige Einstellung der Mehrwertsteuersätze für allgemeine Waren. Eine Luxussteuer, wie sie in Österreich bekannt ist, gibt es in Deutschland derzeit nicht.

![](Deutsche_Mehrwertsteuersaetze.gif)


## Wie kann ich Orte bereinigen
Manchmal kommt es vor, dass Orte mehrfach angelegt, mehrfach eingetragen wurde. Manchmal wurde auch ein Ortsname mit einer Hausnummer versehen, was üblicherweise nicht erwünscht ist.

Wurde z.B.

![](Doppelte_Orte.gif)

angegeben, so kann der Ort Adnet 125c nicht gelöscht werden, da er ja als eigener Ort definiert ist.

Ändern Sie daher im Partner die Definition für diesen Ort / diese Orte in dem Sie die Hausnummer unter Straße eintragen und den Ortsnamen richtig stellen. Danach kann der falsch definierte Ort unter System, System, Ort gelöscht werden.

## Größe der Datenbank
Manchmal ist es praktisch festzustellen warum die Datenbank so groß ist.

Dafür gibt es in der Postgres-Datenbank im PGAdmin ein praktisches Werkzeug.

Gehen Sie in Ihre KIESELSTEIN Datenbank, erweitern Sie diese bis hin zu Tables und klicken Sie diese an.

Nun rechte Maustaste, Berichte, Statistiken

![](Tipps_DB-Statistik_PSQL.gif)

Geben Sie nun noch eine Ausgabedatei an und Sie können diese Statistik komfortabel betrachten, zu uns senden usw.

## Markieren von mehreren Zeilen bei Auswahlmöglichkeiten
<a name="Markieren"></a>
In den Modulen von **Kieselstein ERP** (z.B. Finanzbuchhaltung, Stücklistendruck, etc.) können Sie mehrere Zeilen markieren.

Gehen Sie dazu wie folgt vor:

Mit Klick auf eine Zeile markieren Sie diese.

Halten Sie die Strg-Taste gedrückt um weitere Zeilen zu markieren.

Bestätigen Sie die Auswahl mit Enter.

Sollten Sie mehrere Zeilen, die sich hintereinander befinden, markieren wollen, so gehen Sie wie folgt vor:

Markieren Sie die erste Zeile und halten die Shift ("Groß/Kleinschreib")-Taste gedrückt, nun klicken Sie auf die letzte zu markierende Zeile.

Mit Drücken der Strg-Taste und A markieren Sie alle Zeilen. Zum Beispiel, wenn Sie durch Filter schon alle gewünschten Zeilen zur Auswahl stehen.

Um die gesamte Markierung wieder aufzuheben klicken Sie ohne Tastendruck auf eine andere Zeile. 

Um nur eine Zeile wieder aus der Markierung zu nehmen, halten Sie die Strg-Taste gedrückt und klicken auf die Zeile bei der Sie die Markierung aufheben möchten.

## Tipps zur Verwendung von Etikettendruckern
Für den Ausdruck von Etiketten empfehlen wir den Einsatz von Druckern der Firma Zebra. Für langjährigen Gebrauch ist die Serie Z4 bzw. S4 zu empfehlen.

Für manche Anwendungen mit sehr geringem Etiketten-Aufkommen werden auch die Drucker der Fa. Dymo eingesetzt. Dazu ist anzumerken, dass dies reine Einzelplatzdrucker sind. Die Einrichtung im Netzwerk ist aus unseren Erfahrungen nicht stabil. Dazu kommt, dass das Preis-Leistungsverhältnis nur für ein sehr geringes Etikettenaufkommen optimal ist. Ab ca. 1000 Etiketten im Jahr sind die kleinen Zebradrucker in den Gesamtkosten deutlich günstiger.

Da es natürlich auch zu den Zebradruckern immer wieder Fragen gibt, hier eine kleine Sammlung von Fragen und Antworten.

### Der Drucker druckt ein Etikett und schiebt dann mehrere Etiketten nach
Führen Sie die Kalibrierung durch. Auf Windowsrechnern am einfachsten über die Eigenschaften des Druckers. Hier finden Sie in den Extras unter Aktion, Kalibrierung ausführen. Alternativ kann die Kalibrierung auch direkt am Drucker über das Druckmenü aufgerufen werden.

### Der Drucker druckt ein Etikett und meldet dann "Paper out"
Stellen Sie sicher dass das Papier wie im Deckel des Druckers aufgezeichnet richtig eingelegt ist. Wichtig ist hier auch, dass die seitliche Papierführung sehr knapp aber leichtgängig eingestellt ist. Beachten Sie, dass sowohl die seitliche Führung der Etikettenrolle als auch die Führung des Papierbandes unmittelbar vor dem Thermotransferkopf möglichst exakt ist.

Vor dem Thermotransferkopf ist auf der Innenseite auch der Papiersensor angebracht. Dieser muss sauber sein.

### Das Druckbild ist sehr schlecht
Führen Sie bitte die Kalibrierung durch.

## Weitere allgemein Tipps zur Verwendung von **Kieselstein ERP**
### Server startet nicht mehr
Nach einem Stromausfall startet der **Kieselstein ERP** Server nicht mehr, es gibt unkontrollierte Fehler

Bei unkontrolliertem Abschalten des **Kieselstein ERP** Servers kann es vorkommen, dass unkontrolliert temporäre Dateien, welche eventuell auch teilweise zerstört sein können, stehen bleiben.

Dies wirkt sich dann so aus, dass z.B. der **Kieselstein ERP** Server nicht mehr startet, bei manchen Auswertungen Class not found Exceptions bringt usw..

Wenn dies der Fall ist, so stoppen Sie den **Kieselstein ERP** Server.

Nun löschen Sie aus dem Verzeichnis des **Kieselstein ERP** Servers die Unterverzeichnisse tmp und work. ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\tmp bzw. ?:\kieselstein\dist\wildfly-26.1.2.Final\standalone\data

Nun starten Sie den **Kieselstein ERP** Server wieder

### USV
Benötige ich für den **Kieselstein ERP** Server eine unterbrechungsfreie Stromversorgung?

**Selbstverständlich**

Manche Anwender sind zwar der Meinung dass gespiegelte Platten, eine Datensicherung oder ähnliches ausreichen.

Wir weisen hier ausdrücklich darauf hin, dass jeder **Kieselstein ERP** Server und natürlich auch eventuell eigenständige Datenbankserver mit einer USV (unterbrechungsfreie Stromversorgung) ausgestattet sein müssen.

Wir können dazu von einem Anwender berichten, der ebenfalls der Meinung war, dass eine richtig installierte USV Luxus sei und bei dem ein Ausfall des Servers die Datenbank zerstört hat. Da dies am Abend eines intensiven Tages war, war es enorm wichtig, dass die Daten inkl. des Tages restauriert werden. Wir konnten die Daten mit einem Aufwand von ca. 3x24 Stunden zum Großteil retten. Wir waren jedoch ganz knapp daran aufzugeben.

Bedenken Sie die Kosten für einen derartigen Reparaturversuch und diese Kosten inkl. dem Datenverlust sind nicht versicherbar. Alleine um die Kosten für den Reparaturversuch bekommen Sie drei sehr gute unterbrechungsfreie Stromversorgungen.

### Der Client meldet immer Zeitdifferenzen zum Server.
A: Stellen Sie sicher, dass Ihr **Kieselstein ERP** Server synchron zur internationalen Atomzeit ist. Wenden Sie sich dazu bitte gegebenenfalls an Ihren **Kieselstein ERP** Betreuer.

### Der **Kieselstein ERP** Client schließt sich immer wieder automatisch
A: Der Anwender hatte das Problem, dass immer bei einer längeren Pause, z.B. über Mittag, der **Kieselstein ERP** Client die Verbindung zum **Kieselstein ERP** Server verloren hat.

**Mögliche Ursache I:** Es geht die Netzwerkverbindung zwischen Client und Server verloren. Damit wird die Sitzungs-ID des **Kieselstein ERP** Clients ungültig, wodurch die Verbindung abbricht.

**Was ist zu tun?**

Schalten Sie bitte den Energiesparmodus in der Netzwerkkarte ab. Bitte wenden Sie sich an Ihren zuständigen EDV Betreuer der Ihre Geräte kennt.

**Mögliche Ursache II:** Im Bildschirmschoner war eingestellt, dass der Rechner nach einer gewissen Zeit in den Standbymodus wechselt. Auch dadurch geht die Netzwerkverbindung zwischen Client und Server verloren. Damit wird die Sitzungs-ID des **Kieselstein ERP** Clients ungültig, wodurch die Verbindung abbricht.

**Weitere Möglichkeiten:**
-   Ein Bildschirmschoner der den Rechner in eine Art Ruhezustand versetzt.
-   Die Netzwerkkarte ist defekt und fordert nach einiger Zeit immer wieder eine neue DHCP-Verbindung an
-   Eventuell sollte auch im Ereignislog nachgesehen werden, ob hier weitere Hinweise auf Verbindungsabbrüche, Speicherfehler und ähnliches zu finden ist.

### Geschwindigkeit
Wie kann die Geschwindigkeit von **Kieselstein ERP** bestimmt / gesteigert werden ?
<a name="Geschwindigkeit"></a>

Die Geschwindigkeit von **Kieselstein ERP**, aus der Sicht des Anwenders, der vor dem Client sitzt, hängt von verschiedenen Faktoren ab.

Wir haben versucht hier eine Aufstellung der Punkte zusammenzustellen mit denen eine Abschätzung der Geschwindigkeit insbesondere die Abschätzung von Geschwindigkeitssteigerungen als ungefährer Richtwert, der bitte völlig unverbindlich ist, möglich werden sollte.

1.  Geschwindigkeit der Datenbank
    Diese hängt wiederum von:
    -   Geschwindigkeit der CPU -> steht eine eigene CPU nur für die Datenbank zur Verfügung
    -   Wieviel kann im Ram gepuffert werden -> das hängt wiederum vom
        - verfügbaren Ram,
        - der Größe der Transaktionen,
        - der Menge an Schreiboperationen ab
    -   Die Geschwindigkeit der Platten.
        Dies ist laut unserer Erfahrung für die Datenbank der kritischste Punkt, Speicher ist meistens ausreichend vorhanden
2.  Geschwindigkeit des Applications-Servers
    -   Geschwindigkeit der CPUs -> da der Application Server ein reines Java Programm ist, ist hier die Geschwindigkeit der CPUs ein wesentlicher Faktor. Mit dazu kommt, dass für die unterschiedlichen Aufgaben, weitere Threads gestartet werden. Diese können auf unterschiedlichen CPU-Kernen laufen. Man kann sich das so ähnlich vorstellen, dass jeder Client einen eigenen Thread bekommt.
        Daher ist neben der reinen Taktzeit der CPUs auch die Frage: wieviele CPU Kerne stehen für den Application Server zur Verfügung wichtig.
        Hier kommt mit dazu, steht die CPU nur für **Kieselstein ERP** zur Verfügung oder ist der Server mit anderen Aufgaben belastet. Wenn es um Geschwindigkeit geht, so stellen Sie sicher dass der Rechner ausschließlich für **Kieselstein ERP** (und den Datenbankserver) zur Verfügung steht.
        Hier ist ein weiterer wichtiger Punkt die interne Struktur der Rechner / des Motherboards. Wir haben hier dramatische Geschwindigkeitsunterschiede bei den verschiedensten Herstellern festgestellt.

    -   Größe des für den Applicationserver zur Verfügung stehenden Rams.
        Je mehr Ram genutzt werden kann, desto weniger muss auf die Festplatte ausgelagert werden. Zuviel Ram würde jedoch bewirken, dass der Garbage Collector (das ist die Funktion die übriggebliebenen Speicher wieder hergibt und zyklisch in Java Programmen aufgerufen wird) seltener zusammen räumt, aber dafür zu lange dafür braucht. Hier kann durch Parametrierung des verwendeten Speichers (der wiederum unterschiedliche Arten hat) eine Optimierung vorgenommen werden.

    -   Geschwindigkeit der Verbindung zwischen Applicationserver und Datenbankserver
        Ist diese intern, also raschest möglich, oder geht sie über eine externe (Netzwerk-)Verbindung

    -   Anzahl und Art und Weise bzw. Größe der laufenden Transaktionen.
        D.h. einerseits,

        -   wie ist **Kieselstein ERP** programmiert<br>
            Bei der Programmierung sind wir von einer pesimistischen und transaktionsorientierten Betrachtung ausgegangen. Pesimistisch: D.h. wenn Sie einen Kunden ändern, so geht dies nur, wenn Sie den Kunden für sich selbst, für diesen einen Client sperren können.<br>
            Transaktionsorientiert: D.h. wenn ein Artikel vom Lager in den Lieferschein gebucht wird, so wird dies in einer Transaktion durchgeführt. Sollte es zu Abbrüchen, warum auch immer kommen, so ist dadurch der Zustand vor der Transaktion gegeben. Während der Dauer der Transaktion sind die Daten für die anderen Benutzer gesperrt. D.h. eine angepasste Arbeitsweise trägt zur Geschwindigkeit bei.<br>
            Im Zweifelsfalle haben wir die Verarbeitung in einer Transaktion gewählt. Unter Umständen kann bei manchen Aktionen dies entfernt werden.<br>
            Hier sind wir gerne bereit auf Ihre Vorschläge einzugehen und gegebenenfalls die Transaktions-Verarbeitungs-Art insoweit zu ändern, dass bei gleicher Sicherheit eine schnellere Verarbeitung erreicht wird.

        -   wie arbeiten Sie mit **Kieselstein ERP**
            Werden z.B. am Morgen alle Auswertungen auf zig parallel laufenden Clients gestartet, oder werden umfangreiche Auswertungen / Berechnungen z.B. Bestellvorschlag, Stichtagsbetragungen des Lagers usw. hintereinander gestartet.

    -   Was wir immer wieder feststellen mussten:
        Es werden heutzutage immer mehr Server in virtuellen Umgebungen installiert. Testet man die Geschwindigkeit von **Kieselstein ERP** am Wochenende / Alleine, so ist diese vollkommen ausreichend. Im laufenden Betrieb während des Tages bricht die Performance deutlich ein.<br>
        Die Ursache liegt hier meist daran, dass:
        -   Die IO Geschwindigkeit der Platten/des Plattencontrollers in der virtuellen Maschine völlig überlastet ist und "oben" ansteht. Bitte prüfen Sie dies mit einem geeigneten Programm, z.B. Performance Manager
        -   Die Belastung des Netzwerkcontrollers immer auf 100% ausgelastet ist und daher die Netzwerkpakete im Stau steckenbleiben. Bitte auch überprüfen.

3.  Geschwindigkeit des Clients
    Da auch der **Kieselstein ERP** Client ein reines Java Programm ist, gelten auch hier grundsätzlich die Forderungen an Java Programme.
    -   D.h. je schneller die CPU(s) desto schneller läuft der Client Teil.
    -   Und natürlich: Wieviel Speicher steht für den jeweiligen **Kieselstein ERP** Client zur Verfügung. Rechnen Sie hier mit einem Speicherbedarf je **Kieselstein ERP** Client von mindestens 256 MB (im Ram). Hier kommt noch der Bedarf für das Betriebssystem hinzu, welches üblicherweise nicht unter 512MB angesetzt werden kann. D.h. wenn nur Betriebssystem und ein **Kieselstein ERP** Client laufen und sonst nichts (kein Word, kein Outlook) empfehlen wir mindestens 1GB Ram im Client Rechner zu haben.
    -   Werden mehrere **Kieselstein ERP** Clients parallel eingesetzt, so gehen Sie davon aus, dass für jeden **Kieselstein ERP** Client zusätzlich mindestens 256MB Ram zur Verfügung stehen sollten.
    -   Wichtig ist auch die Größe und die Auflösung des Bildschirms bzw. die verwendete Darstellungsgröße Ihres **Kieselstein ERP** Clients. Je größer die Bildschirmfläche des genutzten **Kieselstein ERP** Clients (Bitte in Pixel), desto mehr muss vom Layoutmanager verwaltet werden (trotz Vektor orientierten Grafikkarten). D.h. wenn rein die Mindestauflösung von 1024x768 verwendet wird ist der Client schneller und stabiler, als wenn sie in maximaler Fenstergröße auf einem 1920x1080 arbeiten.
    -   Entscheidender Faktor ist die Netzwerkanbindung zum **Kieselstein ERP** Server.
        Genauer gesagt die sogenannten Latenz-Zeiten. Je schneller die einzelne Paketanfrage vom Client zum Server und wieder zurück gelangt, desto schneller kann der Client reagieren. Hier ist die richtige Netzwerkparametrierung oft der entscheidende Faktor.
        Zusätzlich kommt hinzu, dass der eingesetzte Netzwerkswitch die geforderte Performance bringen muss. Setzen Sie manageable Switch ein um feststellen zu können, bei welchen Verbindungen Verbesserungsbedarf besteht.

**Wie hängt nun dies alles zusammen?**

Im Client werden einerseits Daten des Servers abgefragt und angezeigt. Hier greift sowohl die Geschwindigkeit des Datenbankservers, des Applikationsservers, der Netzwerkverbindung und auch des Clients. Andererseits werden am Client Daten erfasst, von diesem vorverarbeitet und an den Server gesandt.

Ein Sonderfall sind Auswertungen wie z.B. der Druck einer Rechnung bzw. eine Lagerstandsliste zum Stichtag. Diese Journalauswertung wird komplett am Applikationsserver errechnet und grafisch im Speicher aufbereitet und erst danach an den Client zur Anzeige und zum Ausdruck gesandt. Daraus sieht man, dass auch hier alle drei (mit dem Netzwerk eigentlich vier) Komponenten im Spiel sind und zur gegebenen Geschwindigkeit beitragen.

Je mehr gleichzeitige Benutzer auf **Kieselstein ERP** arbeiten, desto mehr beeinflussen diese auch das Geschwindigkeitsverhalten.

**Noch ein wichtiger Punkt:**

Das "Abwürgen" des Clients, z.B. bei einer lange dauernden Lagerstandsberechnung, bricht den dadurch gestarteten Serverjob nicht ab. Der Server merkt erst am Ende der Berechnung, dass die Daten eigentlich nicht mehr benötigt werden. Lassen Sie daher den Client weiter laufen und starten Sie gegebenenfalls einen zweiten Client. Dieser wird bei gleichem Rechner und gleichem Benutzernamen nicht als zusätzlicher User gewertet.

### Was kann eine Aufrüstung des Servers bringen
Die Geschwindigkeit von **Kieselstein ERP**, aus der Sicht des Anwenders, der vor dem Client sitzt, hängt von verschiedenen Faktoren ab. Als Einleitung siehe bitte obige Aufstellung.

Da immer wieder gefragt wird, was denn ein neuer Server bringen kann, hier eine kurze Zusammenfassung mit unverbindlichen Richtwerten / Faktoren wie sich eine Änderung gegenüber dem Verhalten auswirken kann:

-   Mehreren CPU Kerne werden sich auswirken, insbesondere wenn gleichzeitig mehrere Benutzer darauf arbeiten.
    Ich würde (als Gefühl) einen Faktor von 0,5 je CPU Kern rechnen.
    Beispiel: altes System 2 Kerne, neues System 4Kerne, Steigerung ca. 50%

-   Eine schnellere Taktfrequenz der CPU wird sich auswirken, ich würde auch hier von 0,5 ausgehen.
    CPU Takt alt: 2GHz, CPU Takt neu 3GHz, Steigerung auf ca. 30%

-   Was sich deutlich auswirkt ist, wenn der Server NUR für **Kieselstein ERP** zur Verfügung steht.

-   Schnelleren Platten z.B. SSD und bessere Hardwarestruktur (North & South-Bridge, Rambus usw.) tragen ebenfalls dazu bei

Eine Bremse für die gefühlte Geschwindigkeit können, besonders für die Anwender, "mangelnde" Speicher für die Anzahl Benutzer für gleichzeitige Nutzung auf Terminal Servern sein.

**<u>Wichtiger Hinweis:</u>**

Wir gehen hier von idealen Netzwerkbedingungen aus. Leider mussten wir in der Praxis immer wieder feststellen, dass viel Zeit und Geld in die Hardware investiert wird, aber grundsätzliche Dinge wie interne Domainstrukturen, ordentliche Netzwerkinfrastruktur, Netzwerkverkabelungen die tatsächlich vermessen sind, der Einsatz von Netzwerkswitch, die auch die geforderte Performance bringen hintenangestellt wird. Denken Sie hier auch daran, dass manche Rechner ganz versteckt verseucht sein können. Dies finden Sie nur durch eine umfassende Netzwerkanalyse. Bemerkt wird es oft nur dadurch, dass die Mitarbeiter über ein langsames Verhalten klagen.

### Was kann eine Aufrüstung des Clients bringen ?
Für eine Abschätzung gehen Sie einerseits bei der Steigerung der Verarbeitungsgeschwindigkeit für CPU und Speicher von den für Server angeführten Punkten aus.

GigaBit Netzwerke und Performance der Switch wirken sich eklatant in der gefühlten Verarbeitungsgeschwindigkeit aus.

Zu Geschwindigkeitsvergleichen generell.

Gehen Sie hier nur von reproduzierbaren Messergebnissen aus.

Als Mensch hat man sich an die bessere Geschwindigkeit sofort gewöhnt und nach dem dritten Aufruf z.B. einer Artikelliste ist es schon völlig normal, dass diese so schnell ist.

### Wird ein Virenscanner benötigt?
**Ja für den Client!** Wir raten grundsätzlich dazu, auf jedem Client-Rechner einen geeigneten und aktuellsten Virenscanner einzusetzen. Sollte es auf Clientseite Geschwindigkeitsprobleme geben, so könnte das **Kieselstein ERP** Clientprogramm im Virenscanner ausgenommen werden.

**Bitte nicht am Server!** Am Server raten wir möglichst die gesamte Performance der Hardware für die Geschwindigkeit des Applikationsservers zu verwenden (und nicht um immer wieder die gleichen Dateien zu scannen).

Wie geht das, was ist nun wirklich einzustellen?

-   Installieren Sie den **Kieselstein ERP** Server auf einem Linux-Rechner, z.B. Debian oder Ubuntu oder ähnlichem
-   Stellen Sie in jedem Falle sicher, dass Ihr **Kieselstein ERP** Server in einer friendly Netzwerk-Umgebung läuft. D.h. dass er vor Angriffen jeglicher Art ausserhalb seiner selbst bestmöglich geschützt ist.
-   Müssen, z.B. weil der **Kieselstein ERP** Server aus Firmenpolitik-Gründen auf einem Microsoft-Rechner läuft, Virenscanner und Firewalls installiert werden, so stellen Sie unbedingt sicher, dass die Zugriffe der **Kieselstein ERP** Clients nicht durch die Firewall z.B. des Virenscanners blockiert werden.
    Stellen Sie sicher, dass sowohl Datenbank als auch Application-Server beide mit den entsprechend temporären Dateien, von der ewigen Virenscannerei ausgeschlossen sind.

### Müssen am **Kieselstein ERP** Server Verzeichnisse freigegeben werden?
Mit einer einzigen eventuellen Ausnahme, bitte in keinem Fall!
Bitte stellen Sie sicher, dass das Basis-**Kieselstein ERP** Verzeichnis nicht erreichbar ist. Es könnte bei Freigaben durch unsachgemäße Bedienung bzw. Versehen dazu kommen, dass die ganze **Kieselstein ERP** Server-Struktur gelöscht wird. Das einzige Verzeichnis, welches für geschulte Anwender eingerichtet werden darf ist ab dem Root-Verzeichnis der Reports (ab der Version 1.0.x ?:\kieselstein\data\reports\).<br>
Info: Für den Zugriff der **Kieselstein ERP** Clients ist keine wie immer geartete Freigabe erforderlich. Der Zugriff erfolgt ausschließlich über die Ports.

### Sollte ich meinen **Kieselstein ERP** Client auf einem Netzwerklaufwerk installieren?
Technisch gesehen spricht nichts dagegen. Aus Gründen der Netzwerkbelastung raten wir in der Regel jedoch dazu, die Clients lokal zu installieren.

### Kann ich das Layout speichern?
Ja. [Siehe bitte]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#kann-das-layout-definiert-werden" >}} ).