---
categories: ["Kurzbegriffe"]
tags: ["Drei Buchstaben"]
title: "Kurzbegriffe"
linkTitle: "Kurzbegriffe"
weight: 100
date: 2023-01-31
description: >
  Kurzbegriffe, meist in drei Buchstaben, rund um das Thema ERP
---
Nachdem immer wieder die verschiedensten Schlagworte und vor allem Abkürzungen verwendet werden, hier eine Aufstellung von mir derzeit bekannten 
drei Buchstaben Begriffen aus den verschiedensten Branchen. Wir freuen uns alle, wenn wir dann alle das Gleiche meinen. Gerne weitere Ergänzungen hinzufügen.

Nachdem mit so vielen Begriffen / Abkürzungen um sich geworfen wird, braucht sich niemand zu scheuen, wenn er einen Begriff nicht kennt.<br>
Aber: Fragen darf man in jedem Alter. Es gibt soviele Begriffe die oft auch falsch verwendet / verstanden werden, dass es durchaus zumutbar ist, gemeinsam zu klären, was das jeweilige Gegenüber mit dem Begriff meint.

| Begriff | Bedeutung |
| --- | --- |
| ERP | Enterprise Resource Planning |
| SMD | Elektronik: Surface Mounted Device |
| SMT | Elektronik: Surface Mounting Technologie |
| THT | Elektronik: Through Hole Technology. Bedrahtete Bestückung im Gegensatz zur SMD Bestückung (SMD sehr kleine Bauteile, THT große schwere (Leistungs-)Teile) |
| AOI | Elektronik: Automatikal Optikal Inspection | die automatisiere optische Prüfung der Bestückung von Leiterplatten, im Gegensatz zu menschlichen Prüfung, die NUR Frauen machen (können)|
| MSL | Elektronik: Moisture Sensitivity Level -> für die Verarbeitung von elektronischen Bauteilen dürfen diese nur einen bestimmten Feuchtegrad haben. Dieser wird dadurch ermittelt, dass Bauteile nur eine gewisse Zeit in "normaler" Luft sein dürfen. Sind diese darüber müssen sie neu ausgebacken werden. Es ist nur ein 2-3maliges Ausbacken möglich. Dann gehen die Teile ob der großen Hitze kaputt |
| RoHs | Elektronik: bedeutet im Wesentlichen es dürfen keine bleihaltigen Materialien verwendet werden. Ist jedoch etwas schärfer als nur bleifrei. Es gibt verschieden Normen |
| UL | Elektronik/Elektrotechnik: Underwriters Laboratorys | könnte man mit der VDE-Zulassung für USA übersetzen. Um (elektrische?) Waren in USA in Verkehr bringen zu dürfen wird eine UL-Zulassung benötigt. |
| MHD | Mindest Haltbarkeits Datum |
| MOQ | Minimum Order Quantity |
| EMV | Elektrotechnik/Elektronik: ElektroMagnetischeVerträglichkeit -> Störaussendung und Störfestigkeit (Empfindlichkeit) von Elektrischen / Elektronischen Geräten. Ist Voraussetzung für die Erlangung / Vergabe des CE-Zeichens. |
| CE | Conformitäts Erklärung: Damit klärt der HERSTELLER, dass das Produkt den Harmonisierungsregeln entspricht.<br>Hat das alte VDE/ÖVE Zeichen ersetzt. Hat aber nie die Qualität erreicht. Daher kommt nun das TÜV geprüft |
| MIS | Management Informations System |
| MES | Manufacturing Execution System |
| TÜV | Deutschland: Technischer Überwachungs Verein mit verschiedenen Schwerpunkten je Niederlassung |
| EX | Elektrotechnik/Elektronik: Explosions geschützte Produkte. Haben sehr spezielle Anforderungen an Entwicklung/Konstruktion und Dokumentation und natürlich dann auch an Chargenverfolgung |
| ISO9001:jjjj | Qualitätsstandard, um die Gleichmäßigkeit einer Produktion sicherzustellen |
| ISO9003 | klingt sehr ähnlich, besagt aber nur, dass der Papierprozess für den Handel halbwegs im Griff ist. Für Hersteller / Fertiger ungeeignet, auch wenn es manche machen, damit sie überhaupt was haben. |
| VDA | Verband der deutschen Automobil (Zulieferanten??) Versucht die Vorgaben der Automobilhersteller für die Zulieferer zu standardisieren |
| ODETT-Lable | Definition eines Etikettenformates mit entsprechenden Barcode LeadIns, damit diese automatisch lesbar werden. |
| GL1 | Standardisierungs Firma die versucht zumindest EAN usw. in den Griff zu bekommen |
| EAN | European Article Number [siehe](https://de.wikipedia.org/wiki/European_Article_Number) |
| ILN | International Location Number wird durch GLN ersetzt. Benötigen Firmen wie Aldi, Spar, usw. [siehe](https://de.wikipedia.org/wiki/Global_Location_Number)|
| BI | Business Intelligent |
| BDE | Betriebsdatenerfassung |
| MDE | Maschinen Daten Erfassung aber auch Mobile Daten Erfassung z.B. für unsere mobilen Barcodescanner |
| OS | Operation System (Dos, Windows, MAX OS X, Linux, Debian, Ubuntu, ... ) |
| OSS | OpenSourceSoftware wie z.B. dein **Kieselstein ERP**, und natürlich Linux |
| QS | Qualitätssicherung |
| QA | Quality Assurance |
| CRM | Customer Relationship Management |
| MAWI | Materialwirschaft |
| WAWI | Warenwirtschaft |
| PM | Projektmanagement |
| GDPDU | Grundsätze zum Datenzugriff und zur Prüfbarkeit digitaler Unterlagen [siehe](https://de.wikipedia.org/wiki/Grunds%C3%A4tze_zum_Datenzugriff_und_zur_Pr%C3%BCfbarkeit_digitaler_Unterlagen)<br>Die GDPDU wurde durch die GoDB abgelöst. |
| GoDB | Grundsätze zur ordnungsmäßigen Führung und Aufbewahrung von Büchern, Aufzeichnungen und Unterlagen in elektronischer Form sowie zum Datenzugriff |
| BOM | Bill of Material unsere Stückliste manchmal auch Partslist genannt |
| CNC | Computer Nummeric Controlled | Das sind meist Mehrachs-Maschinen die aus der CAD entsprechende Steuerungsdaten (G-Codes) erhalten und so (mit den Schrittmotoren) entsprechend verfahren können. Diese Programme müssen natürlich wieder gesichert werden usw. |
| UDI | Dieser BarCode wird von FDA (Food and Drugs Administration USA) gefordert -> ist wie die GSIN aber **U**nique**D**evice**I**dentificator -> Datamatrix oder Code128. [siehe](https://de.wikipedia.org/wiki/Unique_Device_Identification)<br>Hinterlegen bei Verpackungs-EAN.  |
| EDI | Electronic Data Interchange. Also alles, was per Dateien oder Datenübertragung ausgetauscht wird. Das geht von der einfachen CSV Datei bis zum EDI-Fact (das eine propritäre Schnittstelle ist). Bei EDI ist immer wichtig zu wissen, welcher Standard. So hat alleine EDI-Fact ca. 5000 Felddefinitionen und davon verwendet jede Branche ein Subset und davon wieder spezielle Ausprägungen.|
| EMS | Electronics Manufacturing Services, also Lohnbestücker |
| SCM | Supply Chain Management |
| CPQ | Configure, price, quotation |
| MPN | Manufacturer Part Number, Hersteller Artikelnummer |
| ROI | Return on Investment |
| LTS | Long Time Support. Wie lange wird diese Softwareversion vom Hersteller gepflegt |

Aus dem Bereich der Finanzbuchhaltung
| Begriff | Bedeutung |
| --- | --- |
| UVA | Umsatzsteuer Voranmeldung |
| SuSa | Summen und Saldenliste, also das, was im **Kieselstein ERP** die Saldenliste ist |

Aus dem Bereich der Erfolgsrechnung
| Begriff | Bedeutung |
| --- | --- |
| BWA | Betriebswirtschaftliche Auswertung eher in DE in Verwendung |
| GuV, G+V | Gewinn und Verlustrechnung eher in AT in Verwendung |
| DB | Deckungsbeitrag in den Stufen 1 bis 7<br>Die Stufen definieren welche weiteren Faktoren / Kostenanteile in der Deckungsbeitragsrechnung mit enthalten sind. |


Und die am häufigsten unklaren Begriffe
| Begriff | Bedeutung |
| --- | --- |
| Kostenstellen | |
| Kostenträger | |

Mein Rat zu den beiden Begriffen. Kläre was dein Gegenüber damit meint. Das ist je nach Branche und auch aus welcher Schule die steuernden Personen kommen, massiv unterschiedlich. Für die Verwendung in deinem **Kieselstein ERP** siehe bei den jeweiligen Kapiteln.
