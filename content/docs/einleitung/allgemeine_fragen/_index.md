---
categories: ["Kieselstein ERP"]
tags: ["allgemeine Fragen"]
title: "allgemeine Fragen"
linkTitle: "allgemeine Fragen"
weight: 400
date: 2023-01-31
description: >
  Allgemeine Dinge rund um dein ERP System Kieselstein ERP
---
Allgemeine Fragen zu **Kieselstein ERP**
====================================

Hier finden Sie eine Sammlung allgemeiner Fragen rund um die Anwendung und vor allem Verwendung von **Kieselstein ERP**.

#### Was ist der Unterschied eines ERPs zu einer Finanz- /Buchhaltungssoftware?
Der Schwerpunkt von Finanz- und Buchhaltungssoftware liegt bei der (steuer-)rechtlich korrekten Abbildung von Buchhaltung, Rechnungslegung und Verwaltung.

Unternehmensführung ist jedoch mehr als das. Die Aufgaben der Geschäftsleitung sind vielfältig von der Entwicklung neuer Produkte, Kundenakquise und Betreuung bis hin zu Teambildung und Mitarbeitersuche. Um ein Unternehmen erfolgreich steuern und führen zu können, benötigt die Geschäftsleitung Werkzeuge, um Entscheidungen treffen zu können. Ein ERP System liefert im Gegensatz zur Finanzsoftware Inhalte aus dem Geschäftsalltag, wie es der Begriff schon sagt: den Ressourcen des Unternehmens.

Oftmals kann man in Buchhaltungssoftware abgekoppelt von der Lagerwirtschaft zum Beispiel Ware dreimal liefern und nur einmal verrechnen; dies kann bei integrierten ERP Systemen nicht passieren. Zu Beginn des Einsatzes einer ERP Software macht dies die Arbeit etwas komplizierter, langfristig gesehen sammelt man mit jedem Vorgang Informationen für die Weiterentwicklung des Unternehmens.

#### Warum alles in einem und nicht in mehreren Programmen erfassen?
Ohne Komplettsoftware schafft man sich Hilfsmittel, die die eigene Arbeit erleichtern. Das können Tabellen, Listen, Karteikarten, manchmal sogar einzelne Software für CRM, Buchhaltung und Lagerverwaltung sein. Aus vielen einzelnen Werkzeugen sammelt man die Inhalte und Daten für Entscheidungen des Alltags: Wann kann das Projekt für Kunde XY geliefert werden? War der Auftrag von Kunde YZ erfolgreich? Wieviel kaufen wir eigentlich beim Lieferanten AB ein, und warum gibt er uns keine Rabatte?

Wenn diese Daten in einem System erfasst und gepflegt werden, so profitieren alle MitarbeiterInnen davon. Alle Abteilungen tragen gemeinsam dazu bei, dass der Alltag einfacher wird.

**Kieselstein ERP** ist die Komplettsoftware für KMU, die alle Geschäftsprozesse in der Software abbilden kann.

Daten müssen nur einmal erfasst werden, doppelte Inhalte werden vermieden, die Pflege erfolgt an einer Stelle.

Je länger man mit dem System arbeitet, umso mehr Informationen und Rückschlüsse kann man aus den Daten ziehen. So kann man aus den Erfahrungen lernen und zur Weiterentwicklung des Unternehmens beitragen. Hat man dies bei der ersten Erfassung bereits im Hinterkopf, profitiert man langfristig, je sorgfältiger Sie Ihre Inhalte pflegen, umso mehr Rückschlüsse lassen sich ziehen.

#### Wozu Lagerwirtschaft und Zeiterfassung?
Ziele von Unternehmen sind vielfältig, dies kann ein langfristiges Bestehen am Markt, Innovationsträger, beste Kundenbetreuung etc. sein. Um die Ziele zu erreichen, muss man nachhaltig wirtschaften. Nachhaltig bedeutet in diesem Zusammenhang, aus Fehlern und Erfolgen zu lernen. Woher will die Projektleitung wissen, dass der Auftrag erfolgreich war, wenn keine Informationen zu Material- und Zeitverbrauch verfügbar sind? Wenn man nicht weiß, dass im Angebot 2h kalkuliert waren und dann aber 20h benötigt wurden?

Wenn das ein- oder zweimal vorkommt, so ist dies eine Lernchance, wenn man diese jedoch nicht nutzt, kann das Fortbestehen des Unternehmens gefährdet werden. Besonders in wirtschaftlich herausfordernden Zeiten ist es somit essentiell, messbare Informationen zu sammeln und an der Weiterentwicklung zu arbeiten.

Die Ansatzpunkte werden in jedem Unternehmen unterschiedlich sein: Mal ist das Lager unstrukturiert, man findet nie etwas, bestellt Teile zu spät, weiß nicht welche Lieferantenreklamationen gerade offen sind. Ein anderes Mal fehlt der Konstruktion die Erfahrung und plant immer zu wenig Zeit für einzelne Arbeitsschritte in der Fertigung ein.

Der Idealfall ist natürlich eine reibungslose langfristige Zusammenarbeit - somit ist es im beiderseitigen Interesse von Mitarbeiter und Geschäftsleitung mit korrekten Zeit- und Materialbuchungen zur Weiterentwicklung und langfristigen Bestehen beizutragen.

#### Warum eine durchgängige Betreuung von AG bis RE?
Die einzelnen Module von **Kieselstein ERP** können als Kreis dargestellt werden. In Kurzform bedeutet das, dass von der Kundenanfrage bis zur laufenden Nachkalkulation alle Schritte abgebildet werden können. Natürlich können je nach Bedarf einzelne Bausteine weggelassen werden. Was ist der Vorteil der durchgängigen Betreuung / Erfassung?

Bei der ersten Kontaktaufnahme des Kunden wissen Sie vielleicht noch gar nicht, welche Produkte interessant sein könnten, der Kunde formuliert seinen Wunsch und gibt Kontaktdaten bekannt. Sie erfassen in **Kieselstein ERP** den Kontakt, eventuell die Adresse und legen damit die Basis zur Kundenbetreuung.

Im weiteren Verlauf meldet sich der Verkauf beim Kunden und klärt die detaillierten Wünsche ab. Nun kann ein Angebot geschrieben werden, man verwendet vorhandene Artikel und eine Neu-Entwicklung.

Mit der Konstruktion entsteht für die Kalkulation eine Angebotsstückliste, so dass der Preis für die Neuentwicklung festgelegt werden kann. Mit der Vorkalkulation wird sichergestellt, dass Material- und Zeitbedarf dem Erlös optimal gegenübersteht. Der Idealfall tritt ein - der Kunde bestellt mit einer kleinen Änderung. Sie wandeln das Angebot in einen Auftrag um und ändern die Stückzahl der Position.

Nun entsteht aus der groben Angebotsstückliste eine Stückliste für die Fertigung. Die Fertigung kann nach dieser Vorlage das Produkt herstellen und erhält durch die Bestellungen des Einkaufs alle erforderlichen Materialien. Durch die durchgängige Erfassung weiß der Einkauf klar welche Materialien benötigt wurden, der Verkauf was in der Konstruktion berechnet wurde und die Fertigungsleitung wie das Produkt zu fertigen ist, welche (Mitarbeiter-/Maschinen-)Kapazitäten notwendig sind. Sobald ein Teil des Auftrags fertig ist, wird ein Lieferschein geschrieben. Der Kunde erhält die erste Teillieferung. Im weiteren Verlauf wird der gesamte Auftrag per Lieferschein geliefert. Nun kann eine Rechnung mit allen Lieferscheinen geschrieben werden. Die Überwachung der Zahlungen und eventuelle Mahnungen erfolgt ebenso innerhalb des Systems. Zu jedem Zeitpunkt werden Informationen zum Beispiel über GoTo Buttons der einzelnen Positionen/Belege gefunden.

#### Warum genau(er) arbeiten?
Je genauer man Inhalte in **Kieselstein ERP** erfasst, umso genauer sind die Auswertungen.

Einige Beispiele: Wenn die Lagerstände stimmen, so werden zum richtigen Zeitpunkt Produkte bestellt, wenn Sie benötigt werden. Wenn man nun Lieferzeiten erfasst, so kann die Beschaffung zeitgerecht bestellen. Wenn man die Adresse und die Kontaktdaten des Lieferanten korrekt eingibt, so werden die Daten in der nächsten Bestellung auch korrekt übernommen, Kollegen wissen welche Ansprechperson zuständig ist und wie sie diese erreichen.

#### Warum sollte keine Handlagerbewegung gemacht werden?
Aufträge bewirken, dass Bedarf an Artikel besteht - egal ob in der eigenen Fertigung oder im Einkauf. Nach der Bestellung werden die Materialien durch den Wareneingang auf Lager gelegt. Diese Zubuchung bewirkt, dass eine Abbuchung möglich ist. Diese Abbuchung kann nun in der Fertigung als Material für die Eigenproduktion verwendet werden oder direkt per Lieferschein oder Rechnung an den Kunden verrechnet werden. Durch die Ablieferungen in der Fertigung werden die Produkte auf Lager gelegt und können nun auch durch Lieferscheine oder Rechnungen wieder an den Kunden geliefert werden. Dadurch wird das Material wieder vom Lager genommen. Die Idee dahinter ist, dass Lagerstände immer korrekt sind. Da die Zubuchungen mit den Abbuchungen übereinstimmen. Wird nun in der Fertigung zum Beispiel durch einen Fehler der Maschine mehr Material benötigt, so soll dies direkt in diesem Fertigungsauftrag erfasst werden. Der Mehrbedarf ist tatsächlich passiert, das Material ist verbraucht. Handlagerbewegungen sind für Sonderfälle und Korrekturen gedacht, im Kommentar erfassen Sie detailliert, warum die Zu- oder Abbuchung erforderlich ist.

Gerade zu Beginn des Einsatzes von **Kieselstein ERP** kann es sein, dass die Lagerstände in **Kieselstein ERP** noch nicht den tatsächlichen Lagerständen vor Ort entsprechen.  Hierzu empfehlen wir jedoch in den meisten Fällen eine laufende Inventur, sobald ein Artikel verwendet wird, wird dieser gezählt und der Lagerstand in der [Inventur]( {{<relref "/docs/stammdaten/artikel/inventur/#kann-man-mit-kieselstein-erp-auch-eine-permanente-inventur-machen" >}} ) von **Kieselstein ERP** erfasst.

Erfasst man nun eine Lagerzubuchung in der Handlagerbewegung so bedeutet das, direkt gesprochen, dass ein Artikel entstanden ist, bei einer Lagerabbuchung, dass ein Artikel verschwunden ist. Im Normalfall kann dies nicht passieren, somit ist es wichtig, die Ursache zu finden, warum ein Artikel nicht lagernd ist, diesen zu suchen etc. - natürlich immer mit dem den Werten entsprechenden Einsatz.

#### Warum ist eine Stückliste einer Materialliste vorzuziehen?
Neben dem Vorteil, dass durch eine Stückliste das gleiche Produkt noch einmal produziert werden kann, wird durch die Ablieferung einer Stückliste das fertige Produkt auf Lager gelegt. Bei einer Materialliste wird lediglich das verwendete Material vom Lager genommen, es entsteht dadurch jedoch kein neues Produkt. Bei der Materialliste werden keine Informationen zum Soll (Material, Zeit) erfasst, somit ist ein Vergleich zwischen Soll und Ist nicht möglich. Je nach Einsatzbereich ist somit zu entscheiden welche Form der Fertigung passend ist. So werden bei Reparaturen oftmals Materiallisten verwendet. Besonders wenn man davon ausgehen kann, dass das Produkt ein zweites Mal gefertigt werden soll, ist eine Stückliste der der Materialliste vorzuziehen.

#### "Nichts darf raus ohne Preis"
Diese Aussage kann von zwei Seiten betrachtet werden: Einerseits ist die Verhandlungsposition mit einem Kunden über Preise, nachdem das Produkt schon geliefert ist, einseitig. Der Kunde kann das Produkt schon verwenden und erhält dadurch eine stärkere Position, als wenn man als Hersteller das Produkt noch nicht liefern könnte. Andererseits muss sich die Preisgestaltung nach einer Kalkulation richten, somit fließt das Wissen aus Konstruktion/Entwicklung/Verkauf/Einkauf in die Preisgestaltung ein.

Wenn nun zum Beispiel der Lieferant den Preis des Basismaterials um 20% erhöht hat, man selbst jedoch die Preise nicht anpasst, kann es schnell passieren, dass keine Erlöse für das Unternehmen erwirtschaftet werden. Auch wenn der Erfolg kurzfristig gut ist, weil der Kunde den Wunsch schnell erfüllt bekommen hat, ist es langfristig gesehen wichtig, dass vor einer Lieferung Preise fixiert werden.