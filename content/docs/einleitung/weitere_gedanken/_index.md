---
categories: ["Kieselstein ERP"]
tags: ["Unternehmen führen"]
title: "Unternehmen führen"
linkTitle: "Unternehmen führen"
weight: 200
date: 2023-01-31
description: >
  Eine Sammlung von Gedanken zur Unternehmensführung
---
Nachfolgend eine Sammlung von Gedanken rund um das Thema Unternehmensführung.

Ich habe in meiner langjährigen Praxis, ich habe immerhin 1984 mit der Programmierung der Stücklistenverwaltung, die damals schon unendliche Hierarchien konnte, begonnen.
D.h. ja ich komme aus der Elektronik Fertigung.

Im Laufe der Jahre und der wechselvollen Geschichte durfte ich immer wieder mit interessanten Menschen zusammenarbeiten.
Auch wenn manche Personen menschlich nicht immer angenehm waren, konnte ich doch von jedem/jeder etwas lernen.

Das Wichtigste war und ist, man muss sich immer die Flexibilität erhalten.
Einerseits geistig, soll heißen, es gibt immer eine zweite oder dritte Sicht auf das "Problem".
Andererseits finanziell. Wenn ich mich finanziell keinen Cent (Pfennig, Groschen, Rappen, Lira) mehr bewegen kann, muss ich, siehe oben, eine Lösung finden.
Für mich sind diese Lösungen immer fair, halten sich an die Vereinbarungen. Ins Kriminal abzugleiten ist dabei nie eine Option, sondern der Anfang vom Ende.

Was hilft sonst noch so:

## Theory of Constraints (TOC)
oder auch Engpass Betrachtung genannt. Diese Theorie geht davon aus, dass man
- a.) herausfinden muss, was dieser aktuelle Engpass ist. Dieser ist zu beseitigen.
- b.) hat man diesen beseitigt, tut sich ein neuer Engpass auf, womit wir wieder bei a.) beginnen

![](Theory_of_constraints.png)  
Siehe dazu auch: https://de.wikipedia.org/wiki/Theory_of_Constraints

## KVP Kontinuierlicher Verbesserungsprozess
Sehe ich persönlich nur dann als erfolgversprechend an, wenn dies auch von der obersten Managementebene aus gelebt und eingefordert wird.<br>
Werden die beteiligten Mitarbeiter:innen nicht ernst genommen, schafft dies enormen Frust und geht bis zur (innerlichen) Kündigung

## Wertschöpfende Tätigkeiten
Was sind in deinem Unternehmen die wertschöpfenden Tätigkeiten, womit wird wirklich das Geld verdient?<br>
Selbstverständlich brauchen wir auch die Geschäftsleitung die steuert, aber den Ertrag erwirtschaften die Mitarbeiter:innen. Da gehört auch ein sauberer Arbeitsplatz mit dazu!

## Saldenliste
Eine Saldenliste haben wir alle. Auch wenn ich diese Vergangenheitsbetrachtung für die Unternehmenssteuerung als ungeeignet betrachte, kann sie trotzdem dazu genutzt werden, den Ist-Stand festzustellen und draus Maßnahmen für die Zukunft abzuleiten.

Wichtig: Die Fibu / Finanzbuchhaltung ist immer die Vergangenheit. Nur in deinem **gut gepflegten** ERP siehst du die Zukunft.

Und ja: Die Pflege der ERP Daten, gerade wenn sich die Termine und Preise laufend ändern, ist mühevoll. Aber: Nur damit ist überhaupt eine Planung möglich.

Bitte auch bedenken: Mit einer offenen Auftragsliste aus einer Tabellenkalkulation hat man schon so machen Kontokorrent-Rahmen (Disporahmen) **verloren**. Im Gegensatz mit einer gut gepflegten offenen Auftragsliste aus einem ERP System, wurden schon öfter die Bedingungen deutlich verbessert.

## Steuerberater / Wirtschaftstreuhänder
Diese Menschen brauchen wir, aber: Ich habe noch keinen Steuerberater / Wirtschaftstreuhänder kennen gelernt, der mein (also dein) Unternehmen in der erforderlichen fachlichen / prozesslichen Tiefe kennt, das ist auch nicht seine Aufgabe. Das bedeutet aber, auch wenn wir die Gedanken / Überlegungen dieser Menschen als qualifizierte Anregung schätzen, dein Unternehmen kennst nur du ausreichend gut. Sollte auch das nicht der Fall sein, dann wir es jetzt sofort Zeit, sich darum zu kümmern (und nicht erst morgen).<br>
Es bedeutet auch, dass du jede Zahl in deiner G+V und in der Bilanz kennen musst. Also wissen musst du, wo diese herkommt. Es ist dein Unternehmen und die Beraterhaftung (ich habe dazu eine leidvolle eigene Erfahrung) greift natürlich nicht bzw. nicht in ausreichendem Umfang.

## Liquiditätsplanung
Wie oben beschrieben, ist die finanzielle Flexibilität sehr wichtig.<br>
Nutze dafür die Liquiditätsplanung. Womit wir wiederum bei den aktuellen und richtigen, also gut gepflegten Daten sind.

Ich persönlich wollte immer wissen, wie mein Kontostand in 2-3 Monaten aussehen wird.

Daran sehe ich auch, was meine kritischen Aufträge / Bestellungen / Lieferanten / Kunden sind. Um das muss ich mich kümmern.

Insofern: Auch die Erfassung der=aller Eingangsrechnungen gehört mit dazu und auch die Hinterlegung zumindest der Lohnsumme.<br>
Siehe dazu auch Vorkalkulation.

## Vollkostenrechnung
| Kosten | Beschreibung |
| --- | --- |
| Kostenstellen | Personen, Maschinen, Bereiche, Fertigungsgruppen, Losklassen |
| Kostenträger | Projekte(klammer), Auftrag, Lose gerne auch an das eigene Unternehmen |
| Kostenarten | Artikelgruppen, Artikelklassen |

### Vorteil
Ein Großteil der entstehenden Kosten und Erlöse wandert, bei sauberer Verwendung, automatisch dorthin wo sie hingehören. Zeitbuchungen, Lagerwirtschaft, Gestehungskosten,...<br>
Es sind die echten Kosten und nicht die von der Buchhaltung ohne Fertigungsbezug irgendwo hin belasteten Kosten

Zusätzlich entsprechende Buchungen auf Lose, welche auch als Mini-Lager betrachtet werden können oder eben auch als Kostenstellen.<br>
Eine derart detaillierte Abbildung ist in einer Buchhaltung faktisch unmöglich.

Thema Lager, bzw. Material am Lager und deren Zuordnung zu den ?? Kostenträgern, Kostenstellen
- a.) Gliederung in Halbfertig-Ware -> Achtung ist immer mit Arbeitszeit
- b.) RHB, Roh-Hilfs-Betriebsstoffe
- c.) Freier Lagerwert / Lagerwert bereits für Aufträge = Kostenträger benötigt<br>
	Ev. Fix buchen und dafür den zusätzlichen Losstatus nützen
- d.) HalbfertigfabrikatsInventur, Work in Progress
- e.) Wareneinsatz

### Thema Mitarbeiter belasten eine andere Kostenstelle
Ist vom Thema her nur die richtige Buchung auf das richtige Los, gegebenenfalls Auftrag.<br>
Ist eine Darstellung, welche Mitarbeiter abweichend von der Heimatkostenstelle gearbeitet haben.<br>

### Wie präsentieren
Dass auch Buchhalter / Chef-Controller das akzeptieren können.

Das Thema ist, wo stellt man das in einer Zusammenfassung dar, dass das ein Kaufmann, ein Buchhalter, ein Controller versteht.<br>
Vielleicht sollte das so etwas wie der KPI Report werden, eben der Vollkostenreport.

Gerade weil das Controlling für die Steuerung des Unternehmens wichtig ist, ist es für ein produzierendes Unternehmen das im Produktionsprozess (zum größten Teil) vollautomatisch mitzuerfassen, und nicht irgendwie nachher in der Fibu zu buchen.<br>
Zusätzlich: Das ERP zeigt mir auch die Zukunft (Aufträge, Forecast aus Forecast bzw. Projekt), die Buchhaltung immer nur die Vergangenheit.

> Kostenstelle = Abteilung, ev. feiner = Aufwand für Instandhaltung Maschine<br>
> > und Unterkostenstellen bzw. Fertigungsgruppe
> Kostenträger = Produkt = Stücklistenlos oder auch der Kundenauftrag

