---
title: "Einleitung"
linkTitle: "Einleitung"
weight: 0200
description: >
  Wozu braucht man überhaupt ein ERP System, das macht doch eh alles mein Steuerberater
---
## Wozu braucht man überhaupt ein ERP System?

Viele Menschen, vor allem Jungunternehmer und Startups fragen sich, mit welchen organisatorischen Werkzeugen sollten man denn sein Unternehmen führen.

Die Antwort ist schlichtweg mit einem **umfassenden ERP System**, wie z.B. das **Kieselstein ERP**.

**Warum?**

Nur wenn (fast) alle (kaufmännischen) Daten in einem System verknüpft sind, kann man den Nutzen durch die Zusammenschau der Daten erkennen.

So ist z.B. die Frage, welchen Deckungsbeitrag erziele ich mit welchem Kunden, in einem guten ERP System relativ einfach zu beantworten. Machen Sie das mal mit einer Tabellenkalkulation.

Oft hören wir auch, das kann ich doch aus meiner Finanzbuchhaltung herauslesen und die macht ja sowieso der Steuerberater, bzw. ich will mich mit diesen Dingen gar nicht befassen.

Gerade das **ich will mich nicht damit befassen** ist der Anfang vom Ende der Selbständigkeit. Sie haben sich ja nicht deswegen selbständig gemacht, um Ihren Angestellten-Job in einer anderen risikoreicheren Umgebung zu machen, sondern, weil Sie ganz besondere Kenntnisse in verschiedenen Bereichen haben und damit auch Geld verdienen wollen.

Wichtig: Zur Selbständigkeit gehört auch, dass Sie sich um die Zahlen kümmern. Es sind Ihre Zahlen, Ihre Gewinne.

Nur Sie können Ihre Zahlen entsprechend interpretieren. Denken Sie immer daran, nur mit entsprechendem Branchen- / Produktknowhow können Ihre Gewinn- / Bilanzzahlen so interpretiert werden, dass damit die erforderliche tägliche Verbesserung in Ihrem Unternehmen erreicht wird.

Der Steuerberater, Wirtschaftstreuhänder hat eine ganz wichtige Funktion, das daily business gehört aber nicht dazu.

Oder in anderen Worten, so wie wir es alle aus der Schule kennen:

wenn die tägliche Mitarbeit gut ist, wird das Jahreszeugnis auch gut sein, also wenn jedes Produkt / Projekt, jeder Kundenauftrag sofort bzw. laufend auf seinen geplanten Erfolg hin überprüft wird, wird Ihre Jahres-Gewinn- und Verlustrechnung ein positives Ergebnis zeigen.

Macht diese Überprüfung nur Ihr Steuerberater, so
- erhalten Sie das Ergebnis zu spät (er ist üblicherweise ein Monat hinter dem täglichen Geschäft)
- es fehlt die Gliederung in die Produkte, in anderen Worten: In Summe ist es so la la, aber die guten Produkte stützen die schwachen Produkte
- es fehlt die klare Erkenntnis welches Ihrer Produkte ein Problem hat. Damit passiert, dass die Produkte mit einem guten Deckungsbeitrag unter Umständen zu teuer sind und somit zum Mitbewerber wandern und die Produkte die Sie zu billig, also unter Deckung, verkaufen, bleiben Ihnen, weil um den Preis macht das keiner Ihrer Mitbewerber.

Um alle diese Dinge in Ihrer Gesamtheit auch für ein kleines 1-2 Mann/Frau Unternehmen zu bewältigen, benötigen Sie ein umfassendes ERP System.

Ja selbst ein Open Source ERP wie Kieselstein benötigt Betreuung und Einführung. Das können Sie selbst machen. Die Unterstützung durch gut ausgebildete Mitarbeiter:innen ist, in Verbindung mit Best Practice, jedoch enorm wertvoll. Beschleunigt den Start sehr deutlich, auch wenn sie Geld kostet.

## Welche Bereiche deckt das Kieselstein ERP ab?
Das Kieselstein ERP kommt, als Fork von HELIUM V, welches auf dem langjährigen Wissen von Logistik Pur entwickelt wurde, aus der Elektronik Produktion.
Über die Jahrzehnte hinweg, mit der Unterstützung durch die Anforderungen vieler Anwender hat es sich zu einem umfassenden ERP System für kleine und kleine mittelständische Unternehmen entwickelt. Die, auch in der Praxis, abgedeckten Branchen sind:
- Elektronik
- Elektrotechnik
- Metallver- und be-arbeitung
- Maschinen und Anlagenbau
- Gerätebau (also eine Kombination aus allen Branchen, typisch für Eigenprodukte)
- Kunststoffverarbeiter
- Lebensmittel und Kosmetik(produkte)
  
Abgedeckte Unternehmensgrößen vom klassischen ein Mann/Frau Startup hin zum Unternehmensverbund mit mehreren eigenständigen Unternehmen und einigen hundert Mitarbeiter:innen.

Die organisatorisch abgedeckten Bereiche sind:
- Einkauf
- Verkauf
- Warenwirtschaft
- Fertigung
- Zeitwirtschaft für Menschen und Maschinen
- Finanzbuchhaltung
- Management

### muss ich damit 100% organisatorisch abdecken?
Nicht wirklich. Gehen Sie pragmatisch vor, oder wie es manchmal zu hören ist: lieber 80% richtig als 100% falsch.

Das bedeutet natürlich auch, dass Sie die Anforderungen an die 80% kennen müssen. Dies lernen Sie nur, wenn Sie sich, täglich (zumindest wöchentlich) mit den Zahlen Ihres Unternehmens auseinandersetzen und diese tatsächlich lesen und verstehen. Damit sehen Sie den nächsten Handlungsbedarf, und es gibt immer einen nächsten Handlungsbedarf. Siehe dazu auch die Theory of Constraints.

## Warum der Name Kieselstein-ERP?
Wirft man einen Kiesel-Stein ins Wasser so breiten sich seine Wellen immer weiter aus. So wünschen wir uns, dass sich auch das **Kieselstein ERP** immer weiter ausbreitet, durch die Unterstützung vieler fleißiger Hände und Köpfe. Zugleich dringt der Stein bis in die Tiefen vor, zeigt also auch das Bestreben, die gesamten Unternehmensabläufe in ihrer gesamten Tiefe zu durchdringen.

Denke aber auch daran, deine großen / wichtigen Kieselsteine rechtzeitig in dein Lebensgefäß zu legen. Damit du rechtzeitig Platz dafür hast. Ist dein Gefäß einmal (mit vielen kleinen Steinen) voll, haben die großen Steine keinen Platz mehr.
