---
categories: ["Kieselstein ERP"]
tags: ["Geschichte"]
title: "Die Kieselstein ERP Geschichte Teil 2"
linkTitle: "Geschichte Teil 2"
weight: 110
date: 2023-01-31
description: >
  Die Kieselstein ERP Geschichte Teil 2
---
# Teil 2

## Wie ging das nun weiter?
1996 wurde die DOS-Clipper87 Lösung mit vielen Zusatz-Libs auf eine Windows 3.11 Lösung portiert.
Sehr rasch zeigte sich, dass uns das nicht weiterbringt und vor allem auch die Stabilität in den vielen Filezugriffen für mehrere User einfach nicht gegeben war.

Also wurde Anfang der 2000er Jahre nach zahlreichen Versuchen beschlossen, auf Java zu wechseln und eine Multi-Tier-Architektur (Mehrschicht Technik) zu verwenden.

So richtig mit der Entwicklung von Logistik Pur Business und dann in HELIUM V umbenannten Produkt wurde dann, dank neuer sehr guter Mitarbeiter und Mitarbeiterinnen, 2004 begonnen.

Im Juli 2005 wurde, zur Unterstützung des Verkaufes in der Bundesrepublik Deutschland in Unterhaching bei München eine eigenständige Vertriebstocher, die Helium 5 ERP Systeme GmbH, gegründet.

Mitte 2010 hat sich [Thomas Ludwig Uhl]( {{<relref "/docs/dank/#thomas-ludwig-uhl" >}} ) mit seinem Team an der Helium 5 ERP Systeme GmbH beteiligt, was nicht nur eine entsprechende Verbreitung bewirkte, sondern aus heutiger Sicht auch die wesentliche Entscheidung brachte, das HELIUM V ERP in seinem Kern unter die **AGPL Lizenz** zu stellen.

Dies brachte einige Open Source affine Kunden.

In ca. 2016 begannen die Bestrebungen, das, doch ziemlich mächtige Paket des HELIUM V auf eine zukunftsfähige Basis, im Sinne der handelnden Personen zu stellen.<br>
Daraus resultierte dann mit 1.April 2020 der Verkauf aller (der beiden) Firmen mit allen Rechten und Pflichten. Wir haben die neuen Eigentümer, nach bestem Wissen bei der Betreuung und Weiterentwicklung unterstützt.

Jedenfalls wurde, von den neuen Eigentümern, alle Rechte und Pflichten mit Wirkung zum 1.Mai 2022 weiterverkauft und die Zusammenarbeit mit uns beendet.<br>
Auch recht:<br>
Wir haben ja zum Zwecke des Pensions- / Rentenantritts verkauft. Hauptsache, das Produkt und damit die Kunden werden weiter betreut.

Mit August 2022 meldeten sich die ersten Kunden zum Thema OpenSource System.

Mit Oktober 2022 ist es, engagierten Kunden, tatsächlich gelungen, den Quell-Code des Kernels unter der AGPL Lizenz zu erhalten.

So wurde die auf Github verfügbare Version von HELIUM V komplett aktualisiert und dann nach Gitlab unter dem Namen **Kieselstein ERP** geforkt.

Bereits im November 2022 haben engagierte und sehr erfahrene Java-Entwickler begonnen, aus den vielen Source-Code Zeilen wieder ein gutes Softwareprodukt zu formen.

So wurde rund um den Kernel nicht nur der gesamte Build Prozess, sondern auch die Entwicklung der wesentlichen Erfassungs-Module, Zeiterfassungsterminal und Mobile Erfassung, gestartet.

Somit konnte Ende Q1/2023 eine erste Version an verschiedene Anwender ausgerollt werden.

Wieviele Anwender / Firmen das **Kieselstein ERP** aktuell produktiv einsetzen, kann von unserer Seite nicht abgeschätzt werden.

Jedenfalls steht damit ein umfassendes mächtiges und auf typische kleine Unternehmen ausgerichtete ERP Lösung, von der Produktion bis zur integrierten Finanzbuchhaltung über CRM, Zeitbewirtschaftung zur Verfügung.

Was uns Stand heute Ende 2024 fehlt, ein entsprechender Hebel, um unser ERP weiter zu verbreiten.

Unser ist hier im Sinne des Genossenschafts- oder Familien Gedanken zu verstehen. Wir freuen uns, wenn diese umfassende Lösung seine Anwender findet, die sich auch des Wertes der eigenen Daten und Prozesse bewusst sind und sich damit der immer enger werdenden Klammern der proprietären Anbieter entziehen.

Wir sehen die zur Verfügung-Stellung des **Kieselstein ERP** auch als einen Beitrag zur Digitalen Souveränität
