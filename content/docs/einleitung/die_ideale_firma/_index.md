---
categories: ["Kieselstein ERP"]
tags: ["Unternehmen führen"]
title: "Die ideale Firma"
linkTitle: "ideale Firma"
weight: 300
date: 2023-01-31
description: >
  Ein theoretischer Gedanke zur idealen Produktionsfirma
---
Die ideale Firma
================

oder auch
wie wir uns den Ablauf in einem Unternehmen vorstellen, welches eigene Geräte herstellt.

Nachdem uns immer wieder Anwender und Interessenten fragen, was denn unsere Empfehlung für einen idealen Ablauf wäre, hier eine kurze und kompakte Zusammenfassung unserer Gedanken zu diesem nicht unkomplexen Thema.

## Voraussetzungen:
- Wir gehen grundsätzlich davon aus, dass die Stücklisten und deren Strukturen richtig sind.
Insbesondere die Strukturen der Stücklisten mit Hilfsstücklisten (z.B. für die Abbildung von Varianten) orientieren sich an den Fertigungsprozessen.
- Die Kundenaufträge sind alle eingepflegt und auch terminlich aktuell.
- Die Wareneingänge werden annähernd sofort gebucht (normalerweise am Tag des Warenzugangs, in Ausnahmefällen am nächsten Arbeitstag).
- Der Lagerstand jedes einzelnen Artikels stimmt, egal ob dies ein teurer Generator ist oder nur eine A3.2 Beilagscheibe oder ein Adernendhülserl.
- Die Wiederbeschaffungszeiten der Artikellieferanten sind bekannt und gepflegt, wobei dies ein laufender Prozess ist.
- Dass die Einkaufspreise, Verpackungseinheiten, Konditionen der Kunden und Lieferanten stimmen, ist selbstverständlich.
- Die tatsächlich für die Fertigung entnommenen Materialien sind akribisch in die Lose nach tatsächlicher Verwendung gebucht.
- Auch alle anderen Materialbewegungen sind erfasst und möglichst auf die richtigen Kostenträger gebucht (Lose, Aufträge, Arbeitsplätze, .....)

## Wie sieht nun der ideale Ablauf aus?
-   es werden laufend / täglich die eingehenden Neuaufträge erfasst.
-   je nach erforderlicher Reaktionsgeschwindigkeit wird täglich / wöchentlich der Lauf der internen Bestellungen (aus den Losen/Fertigungsaufträgen) durchgeführt. Damit erhalten Sie eine Aussage, welche Lose noch anzulegen sind, um den aktuellen Bedarf zu decken. Diese Lose werden auch tatsächlich angelegt, aber, da noch nichts produziert wird, noch nicht ausgegeben.
-   Es wird anschließend an das Anlegen der Lose der Bestellvorschlag neu erzeugt.
    Je nach Zeitfenster der Bedarfe, werden die Bestellungen sofort ausgelöst oder zumindest wöchentlich versandt.
-   Die Lieferterminbestätigungen der Lieferanten werden erfasst und bei Abweichungen werden die sich daraus notwendigerweise ergebenden Terminverschiebungen in Abstimmung mit Verkauf und Kunden im System eingepflegt.
-   Dass die täglich produzierte Menge jedes einzelnen Loses erfasst und abgeliefert werden, muss selbstverständlich sein. Nur damit stimmt der Lagerstand / Lagerwert. Nur dadurch können Sie Ihren Kunden sehr schnell eine Aussage liefern, ja wir sind kurz vorm abschließenden Systemtest oder ähnliches.

Es hat sich in einigen Firmen eingebürgert, dass nur die Lose für die Fertigung der aktuellen Woche / der nächsten Woche angelegt werden. Bedenken Sie, dass für eine Aussage welches Material benötigt wird, alle Fertigungsaufträge = Lose für alle Kundenaufträge, egal wie weit diese in die Zukunft reichen angelegt sein müssen. Nur damit können Sie eine Aussage darüber treffen, welches Material und welche Mitarbeiter- und Maschinenkapazität Sie in Zukunft benötigen. Es ergibt sich draus zwar ein höherer Pflegeaufwand. Sie erhalten damit aber die Sicherheit alle Artikel/Produkte eingeplant zu haben.<br>
Nutzen Sie daher die Unterscheidung zwischen (nur) angelegten Lose und den Losen die Ausgegeben bzw. in Fertigung sind.

### Kapazität:
Dass Zeitmodelle, Urlaube, Zeitausgleiche, geplante Krankenstände usw. zumindest über Ihren Planungshorizont erfasst sein sollten ergibt sich aus der Forderung der Planung.<br>
Dass geplante Maschinenstillstände ebenfalls auf das Maschinenlos (= eine freie Materialliste, auf die alle Kosten der jeweiligen Maschine erfasst werden) eingebucht sind, sollte ebenfalls klar sein.

### Abstimmung der Kapazität mit den Auftragsterminen
Reduziert den Liquiditätsbedarf, da nur das Material beschafft wird, welches auch verarbeitet werden kann.

### Lagermindeststände
Mit den Lagermindestständen wird immer wieder versucht, Pufferzeiten für die Wiederbeschaffung der Artikel zu erzeugen.<br>
Beachten Sie, dass damit entsprechendes Kapital gebunden wird und es immer wieder vorkommt, dass Artikel im Lager veraltern und nicht mehr verkauft werden können. Denken Sie an die Verträge mit Ihren Kunden (wie weit/lange darf vorgefertigt werden usw.). Siehe dazu auch das Buch: Bestände sind böse, von Dr. Thorsten Hartmann (ISBN: 978-3-937960-04-3). Die ist zumindest ein interessanter Gedankengang.

### Wie damit umgehen, dass immer voraus produziert werden muss?
Legen Sie die geplanten Produkte z.B. im Monatsraster manuell, ohne Auftragsbezug oder mit einem Auftragsbezug auf Planauftrag in den Losen an.<br>
Dadurch, dass kein Auftragsbezug (oder bei Planauftrag im Auftrag eigentlich keine Inhalte) gegeben sind, werden Kundenaufträge durch die Lose erfüllt. Sollte nun bei der Internen Bestellung der Bedarf eines Kopflos aufscheinen, sehen Sie, dass Ihre Kunden mehr bestellt haben, als Sie geplant haben. Die Entscheidung wie nun damit umgegangen wird, liegt bei Ihnen.<br>
Kann schnell nachproduziert werden, oder muss der Kundentermin verschoben werden und vor allem, welchen Auftrag können Sie aus vertrieblicher Sicht denn überhaupt verschieben?

Denken Sie daran, dass Ihre Planung z.B. monatlich rollierend aktualisiert werden muss.<br>
Nutzen Sie dafür gegebenenfalls auch das Modul des Forecastauftrags mit der geplanten Erweiterung NUR Auftragsvorplanung, welche die Bedarfe der Kopflose als Grobplanung zur Verfügung stellt und (hoffentlich) durch die Kundenaufträge erfüllt wird.

## Welche Module / Journale helfen / stehen zur Verfügung?
- Aufträge
    -   offene Details mit Lagerstandsdetailbetrachtung
    -   offene Aufträge mit Details
    -   Auszuliefernde Positionen
    -   Lieferplan
- Lose
    -   offene Lose
    -   Auslieferliste
    -   Auslastungsvorschau (auf Mitarbeiterzeiten / Tätigkeiten)
- Zeiterfassung
    - Maschinen, Maschinenbelegung (als Maschinenkapazitätsanzeige)

## Lagerstände
Ein ganz wesentlicher Punkt für jede Materialwirtschaft ist, dass die Lagerstände zu jedem Zeitpunkt richtig sind. So wie in der Finanzbuchhaltung die Buchhaltungskraft dafür verantwortlich ist, dass die Buchungen auf der Bank immer richtig sind, ist die Leitung der Materialwirtschaft dafür verantwortlich, dass der Lagerstand immer richtig ist.

Es helfen die ganzen Berechnungen in deinem **Kieselstein ERP** nichts, wenn die Lagerstände nicht stimmen. Wie sollte man eine verbindliche Zahl errechnen, wenn die Basis falsch ist.

Zu diesem Thema gehört ergänzend dazu, dass es sehr sehr von Vorteil ist, wenn auch die Termine, wann welches Material benötigt wird, gepflegt werden. Dies hilft vor allem deiner Liquidität.

### Die Entwicklung, Konstruktion holt sich einfach Material
Wie sollte man mit diesen Dingen umgehen?

Hier gehören auch so Dinge wie Inventar, Maschineninstandhaltung und ähnliches dazu.

Vorneweg: Es wird davon ausgegangen, dass keiner deiner MitarbeiterInnen Material für private Zwecke entwendet. Solltet ihr hier ein Problem vermuten, auch dafür gibt es Auswertungen.

Wir gehen aber jetzt davon aus, dass dein Team deswegen Material aus dem Lager holt / entnimmt, um seine Aufgaben zu erfüllen. Natürlich kommen hier die ganzen menschlichen Aspekte mit dazu.<br>
Auch die Versuche, das Lager abzusperren und nur xyz hat einen Schlüssel, haben in der Praxis nicht zum Erfolg geführt.<br>
Hier hilft einzig und allein, auch wie bei der unbedingt erforderlichen ehrlichen Zeiterfassung, das Wissen und Verständnis der MitarbeiterInnen dafür, dass im Endeffekt ein sorgfältiges Arbeiten für das Überleben des Unternehmens eine unabdingbare Grundvoraussetzung ist. Wie sollte der Einkauf rechtzeitig Material beschaffen, wenn nicht sicher ist, dass der Lagerstand stimmt.

Wie also diese Themen lösen?
- a.) Inventar (Schreibtische usw.)<br>
Vom Gedankengang sollte das Inventar auch im **Kieselstein ERP** erfasst werden. Bewährt hat sich diese Artikel auf ein eigenes Lager zu erfassen, welches in den Bestellvorschlägen usw. nicht berücksichtigt wird. Es ist dies ein Punkt für eine bereits akribische Erfassung aller Materialien und sollte zur Zeit der Systemeinführung hintangestellt werden.

- b.) Wartungslisten <br>
Dieser Fall ist ja für Messwerkzeug u.ä. interessant. Dafür sollten einzelne Artikel angelegt werden. Für Details siehe bitte [Prüfmittelverwaltung]( {{<relref "/docs/stammdaten/artikel/#pr%c3%bcfmittelverwaltung" >}} ).

- c.) Material und auch Zeiten für die Entwicklung <br>
Egal ob es in deinem Unternehmen eine Entwicklungs- oder eine Konstruktionsabteilung und ähnliche Bereiche gibt. In Bezug auf die Lagerstände sind alle diese Unternehmensbereiche gemeint.<br>
Jedenfalls:<br>
  - Man macht doch eine Entwicklung, eine Neu-Konstruktion für ein neues Produkt / Verfahren usw.
  - Man hat sich dazu auch einen gewissen Kostenrahmen überlegt.
  - Und man will dann wissen, was denn eigentlich diese Entwicklung gekostet hat.

  Wir lösen das normalerweise folgendermaßen:
  - es wird ein Los angelegt. Dieses wird bei der Art (oben) auf Materialliste gestellt.<br>
![](Materialliste.gif)<br>
Das ist faktisch ein Los ohne Stückliste und mit leerem Sollmaterial.
  - es wird das Los sofort ausgegeben
  - nun wird jedes Material, das man für die Entwicklung braucht im Reiter Material mit nachträglicher Materialentnahme ohne Sollmaterial in dieses Los gebucht. ![](zusaetzliche_Materialposition.gif) Hier kann auch eine eventuelle Rückgabe von Material gebucht werden.
  - Es sollten auch die Zeiten für die Entwicklung auf dieses Los gebucht werden.

  Somit hat man am Ende der Entwicklung eine Gesamtkostensicht, was die jeweilige Entwicklung gekostet hat.

  Der einzige Nachteil, der aber für die Entwicklung unrelevant bzw. eher ein Vorteil ist, der Wert der (freien) Materialliste verschwindet dann bei der Erledigung des Loses vollständig (geht in kein Lager o.ä., dafür hat man das Produkt). Zur Abbildung der Rückführung der Entwicklungskosten [siehe]http://localhost:1313/warenwirtschaft/stueckliste/#wie-k%c3%b6nnen-entwicklungskosten-ber%c3%bccksichtigt-werden" >}} ).

- d.) Maschinenkosten, Wartung<br>
Ähnliches wird oft für die Erfassung der laufenden Kosten für die Pflege der Maschinen verwendet.<br>
Auch dafür wird eine freie Materialliste angelegt und das für den Betrieb benötigte Material und auch der Zeitaufwand entsprechend darauf gebucht.<br>
Insbesondere diese Lose müssen regelmäßig überwacht werden. Sie werden von einem bestimmten Mitarbeiter Typ auch gerne als Aufbesserung der Soll-/Ist-Zeit Betrachtung verwendet. Verwenden Sie dafür die Produktivitätsstatistik und gehen Sie menschlich und mit viel Verständnis für Ihre Mitarbeiter, die Sie ja viele Jahre behalten möchten, vor.<br>
Bewährt hat sich für solche Lose eine eigene Losklasse zu verwenden und diese in der Produktivitätsstatistik eigenständig zu betrachten.

### Sollte man auch Schrauben und Beilagscheiben und ähnliches bewirtschaften?
Die Frage war: Wir haben viele Schrauben und Kleinteile in unseren verschiedensten Geräten. Sollte man diese bewirtschaften?

Ja unbedingt, denn wenn für ein Gerät um 25.000,- € eine Beilagscheibe im Wert von 0,01€ fehlt, können Sie das Gerät nicht liefern und somit ist die Beilagscheibe wesentlich.

Aber:
- den Lagermindeststand auf 1-2 Verpackungseinheiten setzen, damit **Kieselstein ERP** rechtzeitig warnt, wenn der Lagerstand ausgeht.
- die Schrauben stehen natürlich in den Verpackungen auf den jeweiligen Arbeitsplätzen. D.h. wenn es z.B. vier Arbeitsplätze sind, sollte der Lagermindeststand auf 4 Packungen gestellt werden.
- immer wenn eine neue Packung vom Lager geholt wird, wird mit einem halben Blick der Lagerstand zwischen **Kieselstein ERP** und dem physikalischen Lagerstand geprüft und der Mehrverbrauch entsprechend als Lagerkorrektur oder auf das Werkstattlos gebucht.
- wenn alle Stücklisten richtig sind, schaut der ideale Prozess so aus, dass der Materialverantwortliche weiß, dass jetzt die Beilagscheiben am Arbeitsplatz ausgehen werden. Bzw. "ja ich habe schon damit gerechnet, dass du wieder Beilagenscheiben holen kommen wirst."

Idealerweise sind alle Schrauben auch in den Stücklisten enthalten und wenn die Entwicklung / das Labor eine Packung braucht, sollte das in jedem Falle gebucht werden (auf das Labor Los, oder als manuelle Handlagerbewegung siehe oben). Das Ziel ist, dass das Lager immer stimmt und dass, auch bei den C-Teilen, man sich darauf verlassen kann, dass immer genug da ist. Und die C-Teile kosten hoffentlich nichts.<br>
In der Inventur sollten man dann schon sehen um wieviele Packungen es differiert und idealerweise dann auch herausfinden warum.

Und ja selbstverständlich: Um eine A3.2 bücke ich mich, klettere aber nicht unter den Tisch. Also bitte mit Vernunft!