---
categories: ["Kieselstein ERP"]
tags: ["Geschichte"]
title: "Die Kieselstein ERP Geschichte"
linkTitle: "Geschichte"
weight: 100
date: 2023-01-31
description: >
  Wie kommt man denn auf die Idee selber ein ERP zu programmieren?
---
## Kieselstein-ERP oder wie kommt man denn auf die Idee  ein ERP "selbst" zu programmieren?

Diese Frage stellte mir ein Messebesucher so ca. 2006, als es die Systems in München noch gab.
Da die Geschichte auch beschreibt, wofür brauche ich denn für mein Unternehmen überhaupt ein ERP System, hier ein kurzer Abriss der Geschichte.

Begonnen hat es eigentlich 1984, wobei viele Dinge bereits vorher schon genutzt / gelebt wurden.
Damals als Fertigungsleiter für elektronische Bildschirmterminals (ja mit Bildröhren usw.)
hatte ich nur Bleistift, Papier und einen CP/M Computer zur Verfügung.

Der bereits vorhandene "Groß"-Rechner der Buchhaltung war für uns in unerreichbarer Ferne, weder Zugänge dazu noch sonstiges. Aber immerhin hatte ich ja den CP/M Rechner.

Auch unser Terminal hatte mehrere Baugruppen und natürlich wurden auf jeder Baugruppe gleiche Bauteile verwendet.
Da auch damals die Materialbeschaffung herausfordernd war (wann ist sie das eigentlich nicht), musste man faktisch täglich, jedenfalls wöchentlich überlegen, bis wann man den welche Stückzahlen an welche Kunden liefern konnte.

Dass in der damals üblichen Papiervariante umzusetzen war mir zu mühsam, daher habe ich das dBase2 auf dem CP/M genutzt und so eine erste sehr einfache Artikel- und Stücklistenverwaltung aufgebaut. Mit der man immerhin schon eine beliebige Stücklistentiefe abbilden konnte und natürlich bei den Artikeln die Lieferanten hinterlegen usw.. Somit habe ich zumindest den Bedarf je Artikel sehr rasch und einfach ermitteln können.

Nachdem ich mich dann 1985 zum ersten Mal selbständig gemacht habe, brauchte ich natürlich eine Ausgangsrechnungsverwaltung und damit auch eine Kundenverwaltung.
Das war für meinen damaligen Haupt-Auftraggeber natürlich auch interessant und so hat sich dieses Ding weiterentwickelt.

In der Zwischenzeit bin ich zurück nach Salzburg und natürlich wurde das nun Mawi genannte Softwarepaket in der eigenen GmbH genutzt.

Ca. 1993 haben wir die Lösung an einen Entwicklungskunden verkauft, der dann 1994 das komplette Sourcerecht des nun Logistik Pur genannten Paketes gekauft hat. Nachdem damit auch mehrere Firmen im In- und Ausland beteiligt waren, sind entsprechend viele Ergänzungen dazu gekommen. 

Auch wurde hier erstmals die Zeitwirtschaft im Produktionsbereich eingerichtet.

1996 bot sich die Gelegenheit mein eigenes Source herauszukaufen und die wenigen Kunden mitzunehmen, womit die Geschichte nun so richtig begann. [Siehe dazu Teil 2]( {{<relref "/docs/einleitung/geschichteii/" >}} )

## Warum sollte ich also in meinem Unternehmen ein ERP einsetzen?

Jeder Unternehmer ist der Kapitän seines Schiffes. D.h. so wie der Kapitän jederzeit wissen muss, wo er ist und wo die Reise hingehen sollte, gilt das auch für den Unternehmer.

Leider ist diese Sicht noch nicht bei allen, vor allem Klein-Unternehmern, angekommen.

### Warum aber muss ich als Unternehmer / Geschäftsführer den aktuellen Status und die Reiseroute kennen?

Gerade in den sogenannten Krisen der letzten Jahre (Finanzkrise 2008, Covid 2020/21, Energie 2022 usw.) hat sich gezeigt, dass die Unternehmer (die dies wirklich leben / verdienen so genannt zu werden) die ihr Unternehmens-Schiff gut gesteuert haben, haben durch die Klippen und Wellen gut durchnavigiert / gut durchgefunden.

Diejenigen die sich nur um Teilbereiche ihrer Unternehmen gekümmert haben, haben es oft nicht überlebt, bzw. mussten sie teils dramatische Einbußen hinnehmen.

Das bedeutet: Ja es ist wichtig, dass das Unternehmen seinen **USP** hat.

Der kann auch darin liegen, dass man an einem günstigen Standort Drehteile herstellt.<br>
An dem günstigen Standort sind die Mieten nieder und die Mitarbeiter sind froh überhaupt eine Arbeit in der Nähe des Wohnortes zu haben. Also auch mit einem geringeren Lohn zufrieden.

Das kann man nützen.

**Aber:**

Trotzdem muss man wissen, mit welchen Teilen / Artikeln / Produkten verdient man Geld und mit welchen nicht.

### Warum muss man das wissen?
Teile / Produkte, die man zu einem hohen Preis verkauft, sind auch für andere (Hersteller / Händler) interessant.

Wenn nun Ihr Kunde eine Möglichkeit findet, genau die gleichen Teile in der gleichen Qualität wo anders billiger zu kaufen, ist die Gefahr groß, dass er das auch macht.

Das bedeutet, es werden dir im Endeffekt nur mehr die Teile übrig bleiben, bei denen du (sehr) wenig verdienst, oder schlimmsten falls drauf legst / dazu zahlst.
Gerade wenn du Zulieferant an große Firmen bist, kommt jedes Jahr die Preisverhandlung. Hier solltest du schon sehr genau wissen, wo ist Luft, um dem Einkäufer seinen Erfolg zu gönnen und wo ist keine Luft.

Dazu ein Beispiel aus der Praxis: Es ging bei einer Fertigung von großen mechanischen Teilen wiedereinmal um den Preis. Der Material-Einkaufspreis ist allen Beteiligten bekannt.
Also kann man ausschließlich beim Produktionsprozess die erforderliche Wertschöpfung verdienen.

Nun hat der ERP-Anwender eine sehr gute Zeiterfassung für die Herstellung dieser Teile gemacht. 

Alle Arbeitsgänge mit den entsprechenden Maschinen gut dokumentiert und die Ist-Zeiten (Mensch und Maschine) erfasst. Somit hatte der Kunde kein Argument mehr, diese Zeiten und damit die Herstellungskosten anzuzweifeln. Damit konnte die Kosten durchgesetzt werden.

Dass dies nur geht, wenn die Qualität des Produktes passt, ist klar. D.h. dass ihr euren Job als Drehereibetrieb oder was immer sehr gut beherrscht, wird heute als gegeben vorausgesetzt.
Die technischen Schwierigkeiten, die es in jeder Branche gibt, sind deinen Kunden meist egal.

Vor allem Einkäufer von Konzernen, haben meist keine Ahnung mehr, was sie überhaupt einkaufen
und die früher gelebte Partnerschaft, lasst uns gemeinsam was Cooles entwickeln, es haben alle etwas davon, ist leider nur mehr in seltenen Fällen gegeben.

Man sieht, neben den technischen Anforderungen gibt es auch eine Menge an weiteren Dingen die du / ihr über eure Produkte und euer Unternehmen wissen müsst.
Hier kommt noch dazu, dass die Unternehmensrechtlichen und Arbeitsrechtlichen Vorschriften vielen Jung-Unternehmern erst dann klar werden, wenn sie zum ersten Mal mit der eigenen Bilanz konfrontiert werden.

Gerade wenn ihr ein Eigentümer geführtes Unternehmen seid.

Die G+V, die BWA, die Gewinn- und Verlust-Rechnung, müsst ihr verstehen.

Es ist dein / euer Unternehmen und nicht das eures Steuerberaters. Neben dem, dass du und nicht der Steuerberater dafür haften, ist es auch euer Kapital (Geld, Zeit, Familienzeit, ....), welches eingesetzt wird.

Das heißt kümmert euch zumindest einmal im Monat um die finanztechnischen Kennzahlen eurer Unternehmen.<br>
Ihr **müsst** von jedem Produkt wissen, wieviel dieses zum Unternehmenserfolg beiträgt.

Wie sieht der aktuelle Unternehmenserfolg wirklich und ehrlich aus?

Wo ist einzugreifen?

Habt ihr Mechanismen eingerichtet, die euch sofort die Probleme bei den Herstellungsprozessen (für den Handle bei den Beschaffungsprozesse) aufzeigen?

Und eines kommt mit den Lieferketten / Materialbeschaffung mit hinzu.

Ihr solltet natürlich heute wissen, welchen Bedarf an Material / Zukaufteilen ihr für die nächsten 3-6 Monate habt.

Das bedingt natürlich, dass es entsprechende Stücklisten gibt.

Dass der Lagerstand immer exakt stimmt, ist eine Selbstverständlichkeit. Die Zeiten mit:
bitte liefern Sie mir morgen einen LKW Zug Stangenmaterial, sind vorbei. Nebenbei ist es aus Einkäufer-Sicht auch nicht klug, so blind beim Materialbedarf zu sein.

Nebenbei bemerkt hatten wir in der Elektronik schon immer Materialbeschaffungszeiten von 60Wochen und mehr und der Schweinezyklus war uns schon 1980 vertraut.

Die Geschichten, was alles passiert ist, die dann zum Untergang des jeweiligen Unternehmens geführt haben sind mannigfaltig. Ein gut geführtes ERP System, das richtig interpretiert wird, hilft, diese traurigen Geschichten in eine Erfolgsgeschichte umzuwandeln.

Ich wünsche allen Unternehmern, dass sie aus Ihren Daten herauslesen können, dass die Ausrichtung des Unternehmens, für die Zukunft
- im Bereich A zu stärken,
- im Bereich B gleich und 
- die Resourcen des Bereichs C besser in den Bereich A verlagert werden.

Auch diese Arbeit gehört zum Unternehmertum dazu, oder willst du dein Haus lieber der Bank geben?

Brauche ich also ein ERP System?

Grundsätzlich Ja!

Eine Ausnahme wäre, wenn du praktisch ein Einzelkämpfer bist und nur 1-2 Kunden hast (auch in Zukunft).

Wenn du aber vorhast, zu wachsen und das Einzelkämpfer Dasein zu verlassen, beginne bereits in der ganz frühen Phase mit einem System, welches mitwächst.

Bedenken dabei immer: Die wesentlichste Resource ist die Zeit, deiner Mitarbeiter:innen und auch deine persönliche.<br>
Leisten dir auch mal die Betrachtung, auf welchen netto Stundensatz du selbst kommen. Wo wolls du hin, was muss dafür geändert werden?

### Erfassung des Erfolges der Verkaufsabteilung!
### Erfassung des Erfolges einer Kreativabteilung wie Entwicklung, Grafik Design uvam.
Ein guter Verkäufer hat kein Problem damit, aufzuzeichnen wieviel Zeit er (es geht nicht um die Minute) in einen Kunden / Interessenten investiert hat.

Daraus kann man, gemeinsam entscheiden wie es mit den verschiedenen Kunden/Interessenten weiter gehen sollte.

Mache die Erfassung der Zeit für die beteiligten Menschen so einfach wie möglich.

Ähnliches gilt für die Kreativabteilungen. Bei den wöchentlichen / monatlichen Auswertungen sieht man wo sich Projekte hinentwickeln.

Ist das in eurem Interesse? Gerade Dinge die zu 90% fertig sind, werden dann, aus verschiedensten Gründen, nicht fertig gemacht, wenn man dem nicht hinterher ist.
Also nur Aufwand, kein Erlös. Ist das in deinem Sinne und im Sinne eurer Kunden?

Wie willst du steuern, wenn ihr den Status / die Position des Projektes nicht kennen?

Ja auch das erfordert Zeit, um die Daten zu erfassen, aber jeder Kapitän macht mehrmals täglich sein Besteck (https://de.wikipedia.org/wiki/Kartenbesteck) oder in anderen Worten:
Willst du wirklich mit 180 im Nebel fahren, oder doch besser nur mit 50, aber dafür sicher ans Ziel.

Übrigens: Verkäufer, die das nicht akzeptieren und das unterlaufen, gibt es viele. Die Statistik zeigt, dass diese Verkäufer auch keinen Erfolg haben.

Um nun alle diese Dinge, welche oft unter den verschiedensten Schlagworten aufgeführt werden, effizient zusammenfassen zu können und nebenher auch noch eine Vielzahl von gesetzlichen Vorschriften zu erfüllen, brauchen es ein umfassendes ERP System.

Bedenkt bitte auch, dass dieses System mitwachsen können muss.

Auch wenn sich Systemwechsel nicht immer vermeiden lassen, jeder Systemwechsel
stellt einen Bruch in der Konsistenz **eurer** Daten dar. Auch wenn die gesetzlichen
Regelungen meist nur 7-10 Jahre Aufbewahrungsfristen fordern. 

Jeder von uns kennt doch den Hilferuf seines Kunden: Ich hatte vor 5-6 Jahren (meist sind es dann 15) bei euch, xx gekauft. Das brauche ich jetzt noch einmal, oder ein Ersatzteil dafür.
- Wo nimmst du die Daten dann her?
- Wie und wer findet überhaupt noch dort hin? 
- Kannst du das alte System überhaupt noch bedienen?

### Noch ein Wort zum Thema Finanzbuchhaltung:
Unterscheide bitte sehr deutlich zwischen der Erfolgsrechnung eines einzelnen Produktes und der Erfolgsrechnung deines Unternehmens.

Die Erfolgsrechnung deines Produktes musst du selbst bzw. fähige Mitarbeiter:innen machen.

Die Korrekturfaktoren dafür (z.B. Vertriebsgemeinkosten) kommen aus der Unternehmensbetrachtung.
Bedenke auch, jeder Artikel, jedes Projekt muss seine eigene Erfolgsbetrachtung haben.

Dies kann kein Finanzbuchhaltungssystem leisten. Das muss euer ERP System leisten.

Wenn ihr ein produzierendes Unternehmen seid, oder auch nur Teile zu einem neuen ganzen zusammenfügt / assembliert, so benötigen ihr ein System, welches mehrstufige Stücklisten unterstützt. Solltet ihr ein System einsetzen, welches dies nicht bis zur Halbfertigfabrikats-Inventur (Work in Progress) unterstützt, baut das System aus. Schlimmsten falls wechseln Sie das System.

Ihr müsst quasi jederzeit wissen, in welchem Fertigungsstatus sich euer Produkt/Projekt mit dem dazugehörigen Material befindet, wieviel Zeit (für das Assemblieren) bereits aufgewandt wurde.

D.h. ja es ist durchaus in Ordnung, die Finanzbuchhaltung einem Fachmann/frau zu überlassen, in einem getrennten System zu machen. Aber:<br>
Jede Eingangsrechnung, jede Warenbewegung ist im ERP zu erfassen. Ausgangsrechnungen sowieso.
Und nur ihr selbst wisst, wie eine Eingangsrechnung zu kontieren ist. Der Steuerberater kann nur raten (was oft ganz gut funktioniert).
Daraus ergibt sich auch, dass ihr eure sehr einfache Erfolgsrechnung der des Steuerberaters gegenübergestellt solltet. Die Unterschiede müssen für euch vollkommen transparent sein.

Wenn nicht, so lange Nachfragen, bis ihr es verstanden habt und ev. erforderliche Maßnahmen ergreifen könnt. **Es ist euer Unternehmen!**

<u>Bedenkt auch:</u> Die monatliche G+V des Steuerberaters kommt, in der Regel 2-3 Monate nachdem das Produkt/Projekt geliefert wurde. D.h. wenn dann erst festgestellt wird, dass da
etwas daneben gegangen ist, ist es um 2-3 Monate zu spät.
Vor allem akzeptiert kein Kunde eine nachträgliche Rechnung für ... wir haben vergessen oder ähnliches.

Darum muss euer ERP diese Information bereits bei der Lieferung (Lieferscheinerstellung) liefern **und die Datenpflege muss dies auch ermöglichen**.

Um nun diesen ganzen Wahnsinn auch halbwegs vernünftig darstellen und anwenden zu können, brauchen ihr ein gut gepflegtes umfassendes ERP System.

Aber nicht alles muss / kann in diesem System enthalten sein.
- So sollten die CAD Daten natürlich aus einer vernünftigen CAD kommen
- Wie sieht es mit EDI Anbindung aus?
- Wenn es eine externe Fibu gibt, wie kommen die Daten dorthin?
Idealerweise malt ihr (mit Unterstützung durch einen guten Consultant/Consulterin) ein Bild aller eurer Unternehmens-Prozesse aufbauend auf meinem Kreis (den auch SAP schon verwendet hat)

Denkt bitte auch daran, die Mitarbeiter:innen in der Produktion / die Maschinen erarbeiten zwar euren Gewinn, aber je effizienter ihr diese einsetzt und das hat nichts mit Druck zu tun, desto mehr guten Output ergibt sich.

Diese Effizienz muss aus der Planung kommen. D.h. die Arbeitsvorbereitung entscheidet wann wieviel wovon gemacht wird und nicht der Mitarbeiter an der Maschine / in der Produktion.<br>
Ja natürlich muss der Arbeitsvorbereiter die Prozesse beherrschen, sonst kann er sie nicht planen.

<u>Oder wie ein Anwender sagte:</u><br>
Durch die Optimierung meines Systems will ich mit der gleichen Mannschaft in aller Ruhe! den Output verdoppeln.<br>
Dass damit auch der Ertrag steigt, ergibt sich von selbst.