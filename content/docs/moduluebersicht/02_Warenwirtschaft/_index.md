---
title: "Featureliste 02 Warenwirtschaft"
linkTitle: "02 Warenwirtschaft"
weight: 200
description: >
  Die Module der Warenwirtschaft: Artikel, Stücklisten
---
Eine Aufstellung aller Module des Bereiches Warenwirtschaft und deren Modulreiter und der dazugehörenden Menüpunkte.

![](Warenwirtschaft.png)

# [Artikel]( {{<relref "/docs/stammdaten/artikel" >}} )
## Artikel
![](Artikel.png)
- Handlagerbewegung
- Warenbewegung
- Material
- Inventur
- Preisliste
- Shopgruppe
- KD-Artikelnr.
- Grunddaten

Der Artikelstamm ist das Herzstück jeder vernünftigen Unternehmensverwaltung ....

Die zwei wichtigsten Preise sind der Verkaufspreis und der Gestehungspreis und damit der DB von dem du lebst!

### Serien- und Chargennummern
- Mit Geräteseriennummern<br>
bzw. MHD in verschiedenen Varianten
### Artikelfreigaben
### Verkaufspreise, Verkaufs-Mengen-Staffelpreise, Kunden-Sonder-Preise
### Kundenartikelnummern
### Lieferanten-Preise-Einkaufskonditionen, Einkaufsmengenstaffeln, Gebindeverwaltung
### Inventur
### Artikel bzw. Chargeneigenschaften
Definition eigener "Datenfelder" je Artikelgruppe
### Dokumentenlink
Ablegen von beliebigen Dokumenten hinter den Artikeln mit einer geführten Zuordnung (Diese Dokumente stehen auch in den mobilen Apps zur Verfügung)
## Dual Use Güter


# [Stücklisten]( {{<relref "/warenwirtschaft/stueckliste" >}} )
## Stücklisten
![](Stuecklisten.png)
- Grunddaten

- hierarchisch mit beliebig vielen Ebenen
- mehrfach gleiche Artikel möglich / wird unterstützt. Auch für Positionsangaben und laufende Nummern
- Arbeitspläne
  - Rüstzeiten / Rüstmengen / Initialkosten
  - Maschinenzuordnung
- Gesamt / Vorkalkulation
- Stücklisten Freigaben
