---
title: "Featureliste 01 Kieselstein ERP"
linkTitle: "01 An/Abmelden"
weight: 100
description: >
  An, Abmelden, Mandantenwechsel
---
Der allererste Block für An-/Abmeldung und Mandantenwechsel

![](An_Abmelden.png)  

## Anmelden
Mit Benutzernamen und Passwort
## Beenden
und Schließen des **Kieselstein ERP** Clients.
## Abmelden, Logout
Abmelden, aber im Client-Programm bleiben
## Mandanten-Wechsel
Ist dann aktiv, wenn der angemeldete Benutzer Zugriff auf mehrere Mandanten hat.
