---
title: "Featureliste 03 Einkauf"
linkTitle: "03 Einkauf"
weight: 300
description: >
  Die Module des Einkaufs: Lieferanten, Anfrage, Bestellung, Eingangsrechnung
---
![](Einkauf.png)
## [Lieferanten]( {{<relref "/docs/stammdaten/lieferanten" >}} )
![](Lieferanten.png)  


## Anfragen

## Bestellungen
### Bestellvorschlag -> Dispo-Lauf

## Eingangsrechnungen
![](Eingangsrechnungen.png)  

### Wiederholende Eingangsrechnungen / Zusatzkosten
### Fibu-Überleitung -> Automatische Buchungen
