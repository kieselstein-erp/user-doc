---
title: "Modulübersicht"
linkTitle: "Modulübersicht"
weight: 8200
description: >
  Welche Module gibt es und welche sind freigeschaltet
---
Verwender des **Kieselstein ERP** haben aufgrund der AGPL Lizenz Zugriff auf alle zur Verfügung stehenden Module. Es macht durchaus Sinn, in der jeweiligen Installation nicht alle zur Verfügung stehenden Module gleich von Anfang an freizuschalten. Hier ist definitiv, weniger ist mehr.

Trotzdem sucht man manchmal nach Lösungen und ob das eigene / eingesetzte ERP dies nicht doch schon könnte.

Daher finden **Kieselstein ERP** Anwender im Modul Benutzer ![](Benutzer.png)  im unteren Reiter Systemrolle, im Modulmenü Journal, ![](RollenUndRechte.png)  <br>
am Ende des Journals eine Darstellung aller derzeit zur Verfügung stehenden Module und ob diese aktiviert sind.<br>
![](Moduluebersicht.png)  <br>

Sollte hier **keine Tabellenberechtigung** angezeigt werden, so müssen diese in der Datenbank nachgetragen werden. Wir weisen eindringlich darauf hin, dass dabei die **lizenzrechtlichen Bedingungen einzuhalten** sind.

## Tastaturbedienung
Ein kleiner Hinweis zur Tastaturbedienung. Die Hauptmenüleiste (mit allen Modulen) erreicht man mit Alt+P.