---
title: "Featureliste 05 Verkauf"
linkTitle: "05 Verkauf"
weight: 500
description: >
  Die Module des Verkaufs: Kunden, Projekte, Angebote, Angebotsstücklisten, (Inserate), Forecast, Auftrag, Lieferschein, Rechnung mit Gutschrift und Proformarechnung
---
Eine Aufstellung aller Module des Bereiches Verkauf

![](Verkauf.png)  

## [Kunden]( {{<relref "/docs/stammdaten/kunden" >}} )
## [Projekte]( {{<relref "/verkauf/projekt" >}} )
### Projektklammer
## [Angebote]( {{<relref "/verkauf/angebot" >}} )
## [Angebotsstücklisten mit Vorlagen]( {{<relref "/verkauf/angebotsstueckliste" >}} )
### Einkaufsangebotsstückliste
### Einkaufsanfragestückliste
## [Inserate]( {{<relref "/verkauf/inserate" >}} )
## [Forecast]( {{<relref "/verkauf/forecast" >}} )
## [Auftrag]( {{<relref "/verkauf/auftrag" >}} )
- Freier Auftrag
- Rahmenauftrag
- Abrufe
- wiederholende Aufträge
## [Lieferschein]( {{<relref "/verkauf/lieferschein" >}} )
- freier Lieferschein
- Auftragsbezogener Lieferschein
- Lieferanten Lieferschein
- Ziellager Lieferschein
- Erstellung von Lieferscheinen mit der mobilen App
## [Rechnung]( {{<relref "/verkauf/rechnung" >}} )
- Sammel-Rechnung
- Anzahlungsrechnung
- Schlussrechnung
- Auftragsbezug
- Abrechnungsvorschlag
### [Gutschrift]( {{<relref "/verkauf/rechnung/gutschrift" >}} )
### [Proformarechnung]( {{<relref "/verkauf/rechnung/proformarechnung" >}} )

