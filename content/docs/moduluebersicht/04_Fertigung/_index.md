---
title: "Featureliste 04 Fertigung"
linkTitle: "04 Fertigung"
weight: 400
description: >
  Die Module der Fertigung / Produktion: Losverwaltung, (Küche), Reklamation, (Instandhaltung), Zeitbewirtschaftung inkl. Mehr-Maschinen, Personal
---
Eine Aufstellung aller Module des Bereiche Fertigung und deren Modulreiter und der dazugehörenden Menüpunkte.
![](Fertigung.png)  

## [Fertigung]( {{<relref "fertigung" >}} )
- [Losverwaltung]( {{<relref "/fertigung/losverwaltung" >}} )
- interne Bestellung
- Produktionsvorschlag
- Auslastungsanzeige

## [Küche]( {{<relref "/fertigung/kueche" >}} )
## [Reklamation]( {{<relref "/fertigung/reklamation" >}} )
## [Instandhaltung]( {{<relref "/fertigung/instandhaltung" >}} )
## [Zeiterfassung / - bewirtschaftung]( {{<relref "/fertigung/zeiterfassung" >}} )
## [Personal]( {{<relref "/docs/stammdaten/personal" >}} )
