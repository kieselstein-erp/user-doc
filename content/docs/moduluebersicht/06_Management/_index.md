---
title: "Featureliste 06 Management"
linkTitle: "06 Management"
weight: 600
description: >
  Die Module des Managements: (Zutritt), Finanzbuchhaltung, Dashboard, 360° Kundensicht
---
Eine Aufstellung aller Module des Bereiche Management

Management
![](Management.png)  

## [Zutritt]( {{<relref "/extras/Zutritt" >}} )
![](Zutritt.png)  
## [Finanzbuchhaltung]( {{<relref "/management/finanzbuchhaltung" >}} )
### ohne integrierter Fibu
### mit integrierter Fibu
![](Integrierte_Finanzbuchhaltung.png)  
## [Dashboard]( {{<relref "/management/dashboard" >}} )
## [360° Kundensicht]( {{<relref "/management/360°_kundensicht" >}} )
