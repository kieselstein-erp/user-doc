---
title: "Stammdaten"
linkTitle: "Stammdaten"
weight: 0500
description: >
  Stammdaten definieren wie sich dein ERP-System verhalten sollte, oder wie ein Anwender sagte, nur mit einer guten Pflege der Stammdaten bringt das ERP System seinen Nutzen.
---
Stammdaten sind üblicherweise
- Artikel
- Stücklisten
- Partner(adressen)
- Kunden
- Lieferanten
- Mitarbeiter:innen

ein weiterer Punkt sind die Grunddaten für die Stammdaten und die grundsätzlichen Daten, wie z.B. Zahlungsziele, Mengeneinheiten usw.
