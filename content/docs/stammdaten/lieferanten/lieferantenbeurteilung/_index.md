---
title: "Lieferantenbeurteilung"
linkTitle: "Beurteilung"
categories: ["Lieferanten", "Qualitätssicherung"]
tags: ["Lieferanten", "Beurteilung"]
weight: 400
description: >
  Beurteilung der Liefertreue und Qualität Ihrer Lieferanten
---
Lieferantenbeurteilung
======================

In vielen Unternehmen müssen schon aus Gründen der eigenen Lieferqualität und oft auch in Zusammenhang mit der ISO 9000 die Lieferanten regelmäßig beurteilt werden.

Dafür steht die Lieferantenbeurteilung zur Verfügung.

#### Wie wird die Lieferantenbeurteilung errechnet?
Die Lieferantenbeurteilung ist als reine Auswertung angedacht.

Anhand der bestätigten Liefertermine und der tatsächlichen Wareneingangsdatum kann die Termintreue bestimmt werden. Dazu kommt die Anzahl der Reklamationen im Zeitraum. Daraus ergibt sich die Qualitätsbeurteilung des Lieferanten.

Hintergrund: Die technische Beurteilung wie sie derzeit in Logistik Pur ist, ist in der Praxis nicht wirklich praktikabel, da der den Wareneingang Buchende sehr oft nicht beurteilen kann, ob die Ware tatsächlich in Ordnung ist oder nicht. Ist sie nicht in Ordnung muss sowieso eine Reklamation an den Lieferanten generiert werden. Damit haben wir

a.) die Anzahl der Reklamationen und 

b.) die Beurteilung der Reklamationen.

Als erste Version für die Beurteilung der Liefertermintreue der Lieferanten wurde diese Auswertung in den Reklamationen implementiert. Siehe daher [Reklamationen]( {{<relref "/fertigung/reklamation" >}} ). Als zusätzliche Bewertung der Qualität des Lieferanten kann das Journal der Reklamationen der / eines Lieferanten verwendet werden. Siehe auch dazu [Reklamationen]( {{<relref "/fertigung/reklamation" >}} ).

#### Wie muss mit gesplitteten Anlieferbestätigungen umgegangen werden?
Manchmal ist es so, dass von Ihnen eine gewisse Stückzahl bei einem Lieferanten bestellt wird, und der Lieferung Ihrer gewünschten Menge nun in Teillieferungen bestätigt.

Wenn nun die Terminbeurteilung für jede Teillieferung erfolgen sollte, so ist es erforderlich, dass die Bestellung in unterschiedliche Positionen aufgeteilt wird.

Beispiel:

Sie bestellen 300Stk

Der Lieferant bestätigt für die KW 33 273Stk

Die restlichen 27Stk werden für KW 36 bestätigt.

Geben Sie nun zuerst in der Lieferterminbestätigung der Bestellung unter AB-Termin den 14.8.2009 ein und ändern dann danach den Termin auf den 2.9.2009 so wird in der Lieferantenbeurteilung das Wareneingangsdatum dem AB-Termin gegenübergestellt und je nachdem ein entsprechender Lieferverzug bzw. eine verfrühte Lieferung ausgewiesen und dadurch dem Lieferanten Beurteilungspunkte abgezogen, obwohl er richtig geliefert hat.

Richtig ist hier, Ihre Bestellung in zwei Positionen aufzuteilen. D.h. die erste Position mit Menge und AB-Termin für die erste Lieferung und die zweite Position mit Menge und AB-Termin für die zweite bestätigte Menge.

#### Welche Termine werden für die Beurteilung herangezogen?
Basis für die Beurteilung der Termintreue ist das Datum des Wareneingangs.

Diese wird:

a.) dem AB-Termin gegenüber gestellt. Achtung: Ist bei Ihnen die Option Ursprungstermin aktiviert, so wird anstelle des (2\. nachträglich geänderten) AB-Termins der Ursprungstermin als Basis für die Beurteilung verwendet.

b.) Ist kein AB-Termin definiert, so wird der Positionstermin verwendet.

c.) Ist der Positionstermin nicht verändert worden, so entspricht dieser dem Liefertermin in den Kopfdaten der Bestellung.

[Siehe dazu auch](../Bestellung/index.htm#bestätigter Liefertermin).