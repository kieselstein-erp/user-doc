---
title: "Lieferantenverwaltung"
linkTitle: "Lieferanten"
categories: ["Lieferanten"]
tags: ["Lieferanten"]
weight: 400
description: >
  Verwaltung der Lieferanten und deren Konditionen
---
Lieferant
=========

#### Wozu Kundennummer im Lieferanten?
Dies ist die Kundennummer des Anwenders, also Ihre Kundennummer, beim Lieferanten. Diese wird automatisch in die Eingangsrechnung übernommen und damit in den Zahlungsvorschlag für das E-Banking verwendet.

#### Was ist ein möglicher Lieferant?
Diese Information dient dazu möglicherweise in Frage kommende Lieferanten bereits vorab zu definieren. Damit kann auch eine Definition der Lieferantendaten von der Artikelseite aus erfolgen. Anhand der Rollenrechte kann dann definiert werden welche Rollen Bestellungen an diesen Lieferanten richten dürfen.

#### Unterschied zwischen Lieferant und Partner
Der Partnerstamm ist die zentrale Adressablage. Wird ein Partner zum Lieferanten, so werden weitere Informationen benötigt. Wie z.B. Zahlungsziel, Kreditlimit oder Kreditorenkontonummer. Diese Konditionen sind je Mandant und Lieferant spezifisch und werden daher in einer ergänzenden Tabelle abgebildet.

#### Lieferant aus Partner erzeugen
 Mit dieser Funktion ![](../Kunde/Kunde_aus_Partner.gif), Sie finden diese in der Auswahlliste des Moduls Lieferant, kann ein bereits bestehender Partner um die Lieferanteninformationen erweitert werden. Dies ist z.B. dann erforderlich, wenn bereits ein Kunde erfasst ist, damit sind automatisch die Partnerdaten dafür erstellt worden und mit diesem Partner wird nun auch eine Geschäftsverbindung im Sinne des Lieferanten eingegangen. **Wichtig:** Dies ist eine Verlinkung. D.h. wenn Sie nun die Adressdaten, genauer die Partnerdaten, des Lieferanten ändern, so werden, da dies nur unterschiedliche Sichten auf die gleichen Daten sind, auch die Partnerdaten und damit die Kundendaten mitgeändert.
Für ein Kopieren des Lieferanten ist die Funktion nicht geeignet.
[Siehe](../Partner/index.htm#Kund, Partner, Lieferant wie gehört das alles zusammen) dazu bitte auch [und](../Partner/index.htm#Kunde, Partner Lieferant, wozu), [und](../Partner/index.htm#Partner in Lieferant wandeln).

#### Unterschied zwischen Kommentar und Bemerkung
[Siehe](../Kunde/index.htm#Kommentar_versus_Bemerkung).
Weiters finden Sie im oberen Modulreiter Kommentar eine Möglichkeit Lieferanten-Kommentare vergleichbar mit [Artikelkommentaren]( {{<relref "/docs/stammdaten/artikel/#artikelkommentar" >}} ) zu erfassen.<br>
Die Kommentarart kann in den Partner-Grunddaten definiert werden. Je nach Definition werden bei der Auswahl des Lieferanten in den Belegen diese Hinweise angezeigt (auch Bilder/PDF).
Ebenso können die Kommentare in den Belegen angedruckt werden. Sollte dies bei Ihren spezifischen Reports noch nicht möglich sein, wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

#### Wozu Warenkonto?
Das Warenkonto dient dem default Vorschlag für die Kontierung einer neuen Eingangsrechnung. Ist hier ein Konto eingetragen, so wird bei der Erfassung einer Eingangsrechnung automatisch dieses Konto vorbesetzt. Somit werden Fehler vermieden und Eingabezeit gespart. Wenn Sie von einem Lieferanten Waren unterschiedlicher Kontierung erhalten, so sollten Sie dieses Feld leer lassen um Fehleingaben zu vermeiden.

<a name="Partner_übernehmen"></a>

#### Wie kann ein bestehender Partner übernommen werden?
Wenn ein bereits bestehender Partner als Lieferant definiert werden sollte, so geht dies am Einfachsten durch einen Klick auf das Icon ![](Partneruebernehmen.jpg) (bestehenden Partner übernehmen). Damit stellen Sie automatisch eine Verknüpfung zwischen Lieferant und Partner her und können den Partner um die lieferantenspezifischen Daten ergänzen.

#### Vergabe der Kreditorennummer?
Dies funktioniert analog zur Debitorennummer. Siehe dazu bitte [dort](../Kunde/index.htm#Vergabe der Debitorennummer). Bitte beachten Sie dazu auch die Anmerkungen zur Erfassung von [Eingangsrechnungen](../Finanzbuchhaltung/Integrierte_Finanzbuchhaltung.htm#Kreditkarten). **Hinweis:** Der Bereich der Kreditorennummern kann im Modul System, Parameter, Kreditorennummern von und bis eingestellt werden und die Länge der Nummer (maximal 6stellig) wird über den Parameter KONTONUMMER_STELLENZAHL_KREDITORENKONTEN eingestellt. 

#### Wann sollte einem Lieferanten eine Kreditorennummer gegeben werden?
Grundsätzlich benötigen Sie die Kreditorennummer nur für die Finanzbuchhaltung. D.h. die Kreditorennummer muss erst vergeben werden, wenn der Lieferant tatsächlich eine Rechnung sendet. Von der Idee her, sollten keine Kreditorennummer für z.B. Abholadressen oder ähnlichem vergeben werden.

<a name="Liefergruppen"></a>

#### Liefergruppen?
Dienen der Definition von Anfragebereichen, z.B. Bleche, Schrauben. Die Liefergruppen sind mandantenabhängig.
Liefergruppen dienen zum effizienten Aussenden von [Anfragen](../Anfrage/index.htm#Liefergruppen) an eine Gruppe von Lieferanten, um von diesen ähnliche Angebote zu erhalten und diese zu vergleichen.

<a name="Liefergruppen anlegen"></a>

#### Wie legt man eine Liefergruppe an?
Im Lieferanten unter den Grunddaten können Liefergruppe anlegt werden und beim entsprechenden Lieferanten im Reiter Liefergruppe hinterlegt werden.

#### Zusammenhang Partner / Lieferant?
[siehe.](../Partner/index.htm#Kund, Partner, Lieferant wie gehört das alles zusammen)

#### Rechnungsadresse
[Siehe](../Kunde/index.htm#Rechnungsadresse).

#### Lieferantenbeurteilung / ISO9000
[siehe.]( {{<relref lieferantenbeurteilung >}} )

#### Abweichendes Umsatzsteuerland ?
[Siehe]( {{<relref "/management/finanzbuchhaltung/fibu_export/#abweichendes-umsatzsteuerland"  >}} ).

#### Wie mit abweichenden Fiskalvertretern, mit anderem Umsatzsteuerbezug umgehen ?
Es gibt manche Lieferanten die sich einer optimierten Umsatzbesteuerung bedienen. Die durchaus Rechtens ist, aber bei manchen Anwendern die Frage aufpoppen lässt, wie denn damit umzugehen ist.
Als Beispiel sei hier die Firma Mouser herangezogen.
Mouser sitzt in USA, Texas, also Drittland.
Mouser hat in Frankreich einen Fiskal Vertreter und eine französische UID (UstID) Nummer, also IG-Erwerb.
Mouser hat in Österreich und Deutschland Vertriebsbüros und hat deutsche / österreichische Bankverbindungen Inlandslieferung ?

Wie ist nun der Lieferant zu definieren, dass diese eigentlich Drittlandslieferung als IG-Lieferung in der Eingangsrechnungsverwaltung verbucht wird?
Es sind dafür folgende Schritte zu setzen:
a.) wir gehen von einem **Kieselstein ERP** Anwender in Deutschland aus
b.) Der Lieferant Mouser wird mit seiner deutschen (Inlands-)Adresse angelegt
c.) bei der UstID Nummer wird die französische UID Nummer eingetragen
d.) bei Mehrwertsteuer stellen Sie diese bitte auf allgemeine Steuer
e.) im Reiter Konditionen setzen Sie bei IG-Erwerb einen Haken
f.) bei integrierter Finanzbuchhaltung gehen Sie nun bitte auf das Kreditorenkonto und definieren die Steuerkategorie mit Ausland EU mit UID
g.) gegebenenfalls hinterlegen Sie bitte das IG-Wareneinsatzkonto bei diesem Lieferanten

#### Mahnsperre
Da die Mahnsperre am Lieferanten in vielen Fällen zu allgemeingültig ist, wird stattdessen die Mahnsperre bei der einzelnen Bestellung verwendet. [Siehe dort.](../Bestellung/index.htm#Mahnsperre)

#### Bestellsperre
Manche Lieferanten müssen für den Einkauf gesperrt werden. Hinterlegen Sie die Bestellsperre im Lieferanten unter Konditionen.

#### Wozu Bestellsperre es gibt ja auch verstecken?
Der Unterschied zwischen versteckten Lieferanten und gesperrten Lieferanten, aber auch Kunden ist, wenn ein Lieferant gesperrt ist, könnte ein Mitarbeiter auf die Idee kommen, diesen (neuen) Lieferanten erneut anzulegen, da er ihn Aufgrund des Versteckt nicht sieht. Den Hinweis beim Anlegen des Lieferanten ignoriert der Anwender und bestellt nun erneut Material bei einem gesperrten Lieferanten.
Bleibt hingegen der Lieferant sichtbar und wird er nur gesperrt, so ist für alle **Kieselstein ERP** Benutzer klar, dass dieser Lieferant bereits bekannt ist, aber dass bei Ihm nicht mehr eingekauft werden darf.
Da die Mahnsperre am Lieferanten in vielen Fällen zu allgemeingültig ist, wird stattdessen die Mahnsperre bei der Bestellung verwendet. Siehe dort.

<a name="Artikellieferanten-Import"></a>

#### Import von Lieferantenartikeldaten?
Unter Lieferanten, Artikellieferanten Import steht ein Import für Artikeldaten dieses Lieferanten zur Verfügung.
Der Aufbau der CSV Datei ist wie folgt:
Ident; Artikelnummer-Lieferant; Bezeichnung-Lieferantenartikel; Einzelpreis; Rabatt; Nettopreis; Preis-ist-gültig-ab; Menge; Mindestbestellmenge; Verpackungseinheit; Rabattgruppe; Herstellerbezeichnung-verwenden; Webshop[; VK_Preisbasis; VK_Preisbasis-gültigab;  [; Staffelmenge; Staffelrabatt; Staffelpreis; Staffel-gültigab]]
Sind für diesen Artikel (Ident) und diesen Lieferanten bereits Daten vorhanden, so werden diese ohne Warnung überschrieben. Sind keine Artikellieferantendaten vorhanden, werden sie neu angelegt. **Hinweis:** Wird keine Ident (Ihre eigene Artikelnummer) übergeben, so wird versucht, anhand der Kombination Lieferant und Lieferantenartikelnummer Ihre eigene Artikelnummer zu ermitteln. Wird diese gefunden, so wird der Import auf diese Artikelnummer durchgeführt. Werden mehrere gefunden, so wird der erste gefundene Artikel verwendet. **ACHTUNG:** Achten Sie auf ausreichende Genauigkeit bei den Preisen. **Kieselstein ERP** erwartet 4-Nachkommastellen. **Wichtig:** Die beiden boolschen Werte, Herstellerbezeichnung-verwenden; Webshop, müssen mit 0 für Falsch und 1 für Wahr übergeben werden. Wichtig ist auch, dass das Datum in TT.MM.JJJJ übergeben wird. **Hinweis:** Menge wird in das Feld Standardmenge importiert.

Die beiden Einträge VK_Preisbasis (Verkaufs-Preisbasis) und VK_Preisbasis-gültigab sind optional.
Sind hier Werte angegeben, so wird beim jeweils gefundenen Artikel die Verkaufspreisbasis mit dem neuen Gültig Ab Datum eingetragen. Damit haben Sie die direkte Möglichkeit, über Änderungen der Lieferanten Preise auch Ihre Verkaufspreise der jeweiligen Lieferantenartikel zu pflegen.

Die Einträge ab Staffelmenge sind optional, es müssen aber, wenn verwendet, alle vier Werte übergeben werden.
Beachten Sie bitte dass für jede Staffelmenge auch die Werte des Artikellieferanten angegeben werden müssen. Aufgrund dessen, dass immer der bestehende Datensatz überschrieben wird, ist es erforderlich, dass in allen Datensätzen des Artikellieferanten richtige Daten enthalten sind.
Folgende Felder müssen immer einen Inhalt haben: Ident, Einzelpreis, Rabatt, Nettopreis, Preis-ist-gültig-ab, Menge, Herstellerbezeichnung-verwenden, Webshop und wenn die Staffeldefinitionen verwendet werden, so müssen alle vier Staffelwerte befüllt sein.

Werden beim Import Fehler festgestellt, so werden nur die ersten dreißig fehlerhaften Daten mit den jeweiligen Zeilennummern angezeigt. Bitte beachten Sie, dass die erste Zeile als Titelzeile interpretiert wird und daher ausgelassen wird. Die angegebenen Zeilennummern beziehen sich auf die echten Datenzeilen. Haben Sie nun die Ursprungsdaten in einer Tabellenkalkulation geöffnet, so ist zu den angegeben Zeilennummern +1 hinzuzuzählen. **Hinweis:** Erscheint die Fehlermeldung:

Fehlerbeschreibung: Es ist ein schwerwiegender Fehler aufgetreten, wichtige Daten sind geloggt.
Das Modul wird jetzt automatisch beendet, bitte wenden Sie sich an Ihren
Administrator. Nach Abklärung mit Ihrem Administrator senden Sie gegebenenfalls
die unter Detail angeführten Informationen mit einer kurzen Beschreibung zum
Auftreten des Fehlers an: support@kieselstein-erp.org
Fehlercode: -1
Java-Version: 10.0-b19
Clientuhrzeit: 27.11.2009 15:53:02 CETUser: 124LPAdmin|chrom|
class javax.ejb.EJBExceptionjava.lang.NumberFormatException: For input string:

so haben Sie einen Boolschen Wert (siehe oben) nicht definiert.

**Hinweis:** Erscheint die Fehlermeldung Beträge stimmen nicht überein, so ergibt sich aus der Errechnung des Einzelpreises - Rabatt[%] ein anderer Betrag als der Nettopreis. Übergeben Sie bei diesem Feld entweder den Rabatt oder den Nettopreis.

<a name="Einkaufspreise erhöhen"></a>

#### Einkaufspreise automatisch erhöhen
Im Lieferantenmodul, Menüpunkt Pflege, EK-Preis steht, wenn Sie das Recht WW_DARF_LAGERPRUEFFUNKTIONEN_SEHEN besitzen, die Pflegefunktion für Lieferanten Einkaufspreise zur Verfügung.
![](Preispflege_Lieferant.gif)
Damit werden für den gewählten Lieferanten alle Einträge im Artikelstamm die Preise zum Stichtag um einen Prozentsatz erhöht werden. Für diese Berechnung wird der zum angegebenen Stichtag gültige Preis des Lieferanten verwendet und mit dem Stichtag als gültig ab der um den Prozentsatz geänderte Preis als neue Position eingetragen.
Es werden auch eventuelle Staffelpreise mit erhöht.

**Hinweis:** Wenn nach dem gewählten Stichtag bereits andere Preisvereinbarungen hinterlegt sind, so werden diese nicht verändert.

#### Lieferanten Umsatzstatistik
Diese finden Sie unter Journal Umsatzstatistik.

Bitte achten Sie besonders auf den Unterschied zwischen der Auswertung nach Eingangsrechnung oder nach Wareneingangspositionen.

#### Lieferanten Liste
[Siehe.](../Partner/Serienbrief.htm#Lieferantenliste)

#### Freigabe, Freigabedatum
Um Zertifizierungspflichtige Artikel liefern zu dürfen, muss der Lieferant freigegeben sein. Dies ist in der Freigabe einzutragen. Weitere Details siehe [Zertifizierungspflichtige Artikel](../artikel/index.htm#Zertifizierungspflichtige Artikel).

#### Zu Lager, Ziellager
In den Konditionen jedes Lieferanten können sowohl das Zubuchungslager als auch das Ziellager definiert werden.

| Button | Beschreibung |
| --- |  --- |
| Zu-Lager | Zubuchungslager, dies ist jenes Lager, welches bei einem neuen Wareneingang für eine Lieferung des Lieferanten vorgeschlagen wird. |
| Ziellager | Ist hier ein Lager eingetragen, so wird bei der Erstellung eines Lieferantenlieferscheines an diesen Lieferanten das angeführte Lager als Ziellager vorgeschlagen. |

<a name="Lieferschein_Lieferkonditionen"></a>

#### Lieferkonditionen für die Lieferschein-Lieferanten anpassen
Im Modul Lieferschein können auch Lieferscheine an Lieferanten erstellt werden, um z.B. Ware an Lieferanten zur Veredelung zu senden oder auch Ware an Lieferanten zurückzusenden. Da diese Lieferanten nun faktisch zwei Konditionen-Definitionen haben, einmal für die Bestellungen, als Lieferant und einmal für die Lieferscheine als (versteckter) Kunde gibt es im Modul Lieferanten, im Reiter Konditionen für diejenigen Lieferanten, bei denen auch Lieferscheine definiert sind, den Sprung in die Lieferkonditionen ![](GoTo_LieferKonditionen.gif). Durch Klick auf diesen GoTo Button gelangen Sie direkt in das Modul Kunde, in die zweite Konditionendefinition für diesen Lieferanten. Definieren Sie hier so Dinge wie, Gewicht am Lieferschein mitdrucken, Anzahl der Lieferscheine, Abbuchungs- und Ziellager, Lieferarten und Spediteur, usw..

#### Wo wirken folgende Felder?
| Feld | Beschreibung |
| --- |  --- |
| Kreditlimit | Reines Informationsfeld ohne Auswirkung |
| Mindestbestellwert | Bei der Überleitung des Bestellvorschlages für einen einzelnen Lieferant erscheint ein Hinweis, wenn der Mindestbestellwert nicht erreicht wird |
| Rabatt | Wird bei Neuanlage in Anfrage/Bestellungskonditionen als allgemeiner Rabatt hinterlegt. |
| Transportkosten | Wird an die Angebots-Vorkalkulation übergeben und dort als Info ausgegeben. |

## Langzeit Lieferantenerklärung, Warenursprung der selbst hergestellten Produkte
Wenn Sie Produkte herstellen und ins Ausland liefern, ist oft auch das Thema welchen Ursprungs sind nun diese Produkte.<br>
Bei Handelswaren ist das in der Regel das von Ihrem Lieferanten angegebene Ursprungsland.<br>
Bei eigenen Produkten geht es oft darum, welcher Wertschöpfungsanteil ist in welchem Land gegeben.<br>
Nutzen Sie dafür die Stücklistenauflösung, mit entsprechender Auswertung der Ursprungsländer Ihrer eingesetzten Waren.<br>
Und das Ursprungsrecht zielt immer auf die Wertschöpfung. D.h. kaufen Sie ein Produkt um 40,- ein, kleben ein Etikett um 0,01 darauf und verkaufen Sie dieses Ding dann um 100,- als veredeltes Produkt, so haben Sie eine Wertschöpfung von annähernd 60%. Damit gilt das als von Ihnen erzeugt/ausreichend bearbeitet und Sie können als Ursprungsland Ihr Land angeben. Die Grenzen für die Wertschöpfung sind im DACH Raum unterschiedlich. So sind diese in der Schweiz/Liechtenstein 40%, in Österreich 50%.