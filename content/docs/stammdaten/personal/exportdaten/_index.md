---
title: "Exportdaten"
linkTitle: "Exportdaten"
categories: ["Personal"]
tags: ["Personal", "Exportdaten", "Lohnverrechnung"]
weight: 900
description: >
  Definition der Exportdaten der Anwesenheitszeiten in die Lohnverrechnung
---
Exportdaten definieren
======================

In **Kieselstein ERP** steht auch die Möglichkeit des Exportes von Daten für Lohnverrechnungsprogramme zur Verfügung. Damit haben Sie eine elegante Möglichkeit die Zeitbuchungen Ihrer Mitarbeiter in komprimierter Form in Ihr Lohnprogramm für weitere Auswertungen zu übernehmen.
Durchführung des Exports siehe.

Der Export baut auf folgenden Basiseinstellungen auf:

Lohnarten

Die Definition der benötigten Lohnarten erfolgt im Modul Personal, unterer Reiter Grunddaten, oberer Reiter Lohnart.

![](Lohnarten_definieren.gif)

| Feld | Bezeichnung |
| --- |  --- |
| Lohnart | Geben Sie hier die gewünschte Nummer der Lohnart ein |
| Bezeichnung | Eine kurze sprechende Bezeichnung der Lohnart |
| Kommentar | Eventuell eine längere Beschreibung der Lohnart |
| Personalart | wählen Sie hier die gewünschte Personalart oder <alle> wenn diese Lohnart für alle ihre Mitarbeiter gilt |
| Ausfallsprinzip   Wochen(Durchrechnungs-zeitraum) | In Österreich wird für die Arbeiter, welche nach Stunden entlohnt werden, für Urlaube und Krankenstände das Ausfallsprinzip angewandt.Definieren Sie hier die Berechnungsbasis der vergangenen Wochen, ab Beginn der Abrechnungszeit. |
| Ausfallsprinzip   Mindest-Üstd. | Definieren Sie ab wievielen Überstunden, wobei hier alle Überstunden addiert werden, innerhalb des oben definierten Durchrechnungszeitraumes, das Ausfallsprinzip zur Anwendung kommt.Um ein Gefühl dafür zu bekommen, verwenden Sie bitte in der Zeiterfassung, Info, Wochenabrechnung. |

Bitte beachten Sie, dass für die Berechnung der Stunden für das Ausfallsprinzip immer die angegebenen Wochenanzahl zusätzlich durchgerechnet werden muss und damit die Berechnungszeiten deutlich länger werden.

Definition der Stundenberechnung für die jeweilige Lohnart

Im Reiter Lohnartstunden definieren Sie nun im wesentlichen wie die Stunden der Lohnart errechnet werden. Also woraus sich diese genau zusammensetzen. Im wesentlichen werden die Stunden der Lohnstundenart errechnet, mit dem Faktor multipliziert und die Stunden auf die gleichen Lohnarten addiert.
![](Lohnart.gif)

| Feld | Bezeichnung |
| --- |  --- |
| Lohnart | Die Lohnart deren Berechnung Sie definieren |
| Faktor | Der Multiplikator, in der Regel 1,00 |
| Lohnstundenart | Einer der verschiedenen Typen der Lohnstundenarten, siehe nachstehende Tabelle |
| Tagesart | üblicherweise leer = für alle Tage, oder mit den Einschränkungen:<br>- Wochentag (Mo-Fr)<br>- Samstag<br>- Sonntag<br>- Feiertag, wobei der Feiertag höherwertiger als die anderen Tage ist |
| Sondertätigkeit  | In Verbindung mit der Lohnstundenart Sonstige Sondertätigkeit können die sonstigen bezahlten / nicht bezahlten Stunden aus den Sondertätigkeiten der Zeiterfassung, welche frei definiert werden können, übergeben werden. Siehe unten. |
| Zeitmodell | Wird ein Zeitmodell angegeben, so erfolgt die Berechnung dieser Zeit nur für dieses Zeitmodell. |
| Schichtzeit  | Sollten, in Verbindung mit dem Zeitmodell, auch die Schichtzeiten ausgewertet werden, so kann die Schichtzeit, in Verbindung mit dem Zeitraum der Schicht genutzt werden. Somit kann z.B. eine Trennung zwischen den Zeitbereichen der 2.Schicht, welche teilweise Steuerbegünstigt und nicht Steuerbegünstigt sind festgelegt werden.Bitte achten Sie darauf, dass das Zeitmodell und die Schichtzeiten zusammenpassen müssen. Von **Kieselstein ERP** wird hier keine Überprüfung vorgenommen. |

**Wozu dient das Zeitmodell?** Gerade wenn verschiedene Schichtzulagen bezahlt werden und diese Daten somit an die Lohnverrechnung weitergegeben werden müssen, wird anhand des Zeitmodells festgelegt, welche Zuschläge bezahlt werden. Geben Sie dafür das zutreffende Schicht-Zeit-Modell an.

![](Lohnstundenart_sonstige_Sondertaetigkeit.gif)

Bedeutung der Lohnstundenart

| Feld | Bezeichnung |
| --- |  --- |
| bezahlte nicht Anwesenheit | Stunden die nicht in Krank- oder Urlaubsstunden enthalten sind, aber bezahlt sind |
| Feiertagsstunden | Die Summe der an Feiertagen gearbeiteten Iststunden |
| Gesamtstunden | Gesamt Anwesenheitsstunden |
| Gutstunden | Die für den Mitarbeiter gebuchten Gutstunden (Personal, Stundenabrechnung) |
| KindKrank | Stunden der Sondertätigkeit KindKrank (Pflegeurlaub) |
| Krankstunden | Stunden der Sondertätigkeit Krank |
| Mehrstunden | Die Zeit zwischen Sollstunden und Normalarbeitszeit |
| Normalstunden | Die Stunden innerhalb der Sollzeit |
| Schichtstunden | Die Stunden die aus dem [Schichtzuschlag](#Schichtzulagen) kommen |
| Sollstunden | Die Summe der Sollstunden |
| Üst 100 st-frei | Überstunden steuerfrei 100% |
| Üst 100 st-pfl | Überstunden steuerpflichtig 100% |
| Üst 50 steuerfrei | Überstunden steuerfrei 50% |
| Üst 50 steuerpfl. | Überstunden steuerpflichtig 50% |
| Urlaubsstunden | Urlaubsstunden |
| Verfügbarer Urlaub Tage | Verfügbarer Urlaub in Tagen zum jeweils Monatsletzten des Abrechnungszeitraums |
| Zeitgutschrift Geht | Die Stunden aus der Zeitgutschrift Geht |
| Zeitgutschrift Kommt | Die Stunden aus der Zeitgutschrift Kommt |

Diese Daten werden aus der Monatsabrechnung genommen. [Definitionen der Stundenarten siehe]( {{<relref "/fertigung/zeiterfassung/ueberstundenabrechnung">}} ).

**<u>Bitte beachten Sie:</u>**

Es empfiehlt sich, vor dem ersten Exportlauf für alle Personalarten die Lohnstundenarten zu definieren.

Ist für eine Personalart keine Lohnstundenart definiert, so werden auch keine Daten erzeugt.

Werden nun z.B. Abteilungsdaten ausgedruckt, so kann dies dazu führen, dass z.B. die Daten für die Arbeiter angedruckt werden, aber die Daten für die Angestellten nicht, da für diese Personalart keine Lohnstundenarten hinterlegt sind.

Ein deutlicher Hinweis auf diese Nicht-Definition liegt dann vor, wenn die Monatsabrechnung nach Abteilung alle Mitarbeiter bringt, aber die Export-Vorschau nur einige wenige oder gar keine.

Prüfen Sie hier in jedem Fall die Einstellung der Lohnstundenarten für die jeweilige Mitarbeiterart.

Denken Sie dabei auch an Ihre Lehrlinge / Azubis, für die eventuell eine eigene Personalart verwendet / definiert wurde.

Sonntagsdefinition / Nachtzuschläge

Wenn reine Zuschlagsdaten exportiert werden sollten, so achten Sie bitte darauf, dass z.B. die Sonntagsdefinition üblicherweise auf den 100%igen Überstunden aufbaut. Nachtzuschläge bauen üblicherweise ebenfalls auf den 100%igen Zuschlägen auf.
Um eine saubere Trennung herbeizuführen, müssen daher die Nachtzuschläge für jeden einzelnen Wochentag mit Ausnahme des Sonntags definiert werden. 

Kann in den Lohnarten gerechnet werden?

A: Wenn damit einfache Rechenoperationen gemeint sind, Ja.
![](Mit_Lohnarten_rechnen.gif)
D.h. Wenn z.B. für die Ermittlung der Ausfallsstunden sowohl die Mehrstunden als auch die Normalstunden herangezogen werden sollten, so definieren Sie einfach diese Lohnart mehrfach mit den unterschiedlichen Lohnstundenarten. Da die errechneten Stunden mit dem Faktor multipliziert werden und dieser auch negativ angegeben werden kann, können auch entsprechend einfache Rechenoperationen durchgeführt werden.

Derzeit werden folgende Exportformate unterstützt:

-   Varial
    Für Varial werden die Stunden im Dezimalformat multipliziert mit 100 übergeben.
    Ein Beispiel: Für die Lohnart des Mitarbeiters ergeben sich für den Zeitraum 25,93 Stunden, so werden diese als 000002593+ an Varial übergeben.
    Beachten Sie bitte auch die Einstellungen der Mandantenparameter.

-   eGecko
    Der Parameter PERSONALZEITDATENEXPORT_ZIELPROGRAMM muss auf EGECKO umgestellt werden -> Die Exportdatei enthält dann eGecko gerechte Daten

-   TEXT
    Hier werden folgende Felder mit Tabs getrennt exportiert:
    Firmennummer
    Personalnummer
    Abrechnungsperiode
    Lohnart
    Datum
    Stunden
    Von (Datum für Krank, Urlaub, Kindkrank)
    Bis (Datum für Krank, Urlaub, Kindkrank)

-   CPU-LOHN
    Hier werden die Daten ebenfalls im CSV Format exportiert, jedoch passend zu [CPU-Lohn](https://www.cpu-informatik.at).
    Musterdaten siehe CPU-Lohn.csv

**Hinweis:**

Allen Programmen ist gemein, dass die Lohnarten frei definiert werden können. D.h. es müssen die errechneten Daten zu den von Ihnen im Zielprogramm definierten und erwarteten Daten passen.

Mandantenparameter die entsprechend eingestellt werden müssen:

| Parameter | Beschreibung |
| PERSONALZEITDATENEXPORT_ZIELPROGRAMM | Dieser Parameter definiert welches Exportformat verwendet wird. Siehe oben |
| PERSONALZEITDATENEXPORT_FIRMENNUMMER | Im Varial sind dies die ersten sechs Stellen die im Exportstring angeführt werden |
| PERSONALZEITDATENEXPORT_ZIEL | In welche Datei sollten die exportierten Daten geschrieben werden. |
| PERSONALZEITDATENEXPORT_PERIODENVERSATZ | Da oft  die Überstunden-Zuschläge um ein Monat versetzt abgerechnet werden, müssen die (Überstunden-) Zuschläge in die darauffolgenden Periode eingelesen werden. So werden z.B. die Überstunden des August in den September eingelesen. Mit diesem Parameter, der sich immer auf das Übertragungsmonat bezieht, wird der Versatz in Monaten definiert. |

<a name="Schichtzulagen"></a>Schichtzulagen bzw. Schmutzzulagen ebenfalls exportieren

Damit können gewisse, vor allem von der Schicht abhängige, Zeiten mit an die Lohnverrechnung übergeben werden.
Haken Sie dafür unter Personal, Grunddaten, Schichtzuschlag beim gewünschten Schichtzuschlag den Zeitdatenexport an.
Damit werden die für diesen Schichtzuschlag errechneten Zeiten auch für den Lohndatenexport zur Verfügung gestellt.
Um die Daten im Export anzuführen wählen Sie bei der Definition der Lohnstundenart Schichtstunden.

Urlaubs und Kranktage exportieren

Für die Lohnstundenart Krankstunden, Urlaubsstunden und Kindkrank werden die durchgängigen Bereiche der Sondertätigkeit (Urlaub) als Datum von Bis, innerhalb des Abrechnungszeitraums (Monat) angegeben. Somit können diese Datumsbereich entsprechend an Ihr Lohnverrechnungsprogramm übergeben werden. Bitte beachten Sie dass derzeit das Von (Datum) Bis (Datum) nur im Textformat übergeben wird. Diese Datumsbereiche werden immer als eigene Zeilen exportiert in denen keine Stunden aber dafür eben die Datumsbereich exportiert werden.

| Firmennummer | Personalnummer | Abrechnungsperiode |  Lohnart |  Datum | Stunden |  Von |  Bis |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |
| PZE001 |  678 |  31.01.2020 |  320 |  31.01.2020 |  46,20 |   |   |
| PZE001 |  678 |  31.01.2020 |  320 |  31.01.2020 |  0 |  07.01.2020 | 13.01.2020 |
| PZE001 |  678 |  31.01.2020 |  320 |  31.01.2020 | 0 |  20.01.2020 |  20.01.2020 |

Durchführung des Exports

Der Export wird im Modul Zeiterfassung, Zeiterfassung, Export aufgerufen.
Hier stellen Sie ein für welche(n) Mitarbeiter, für welche Zeitraum der Export durchgeführt werden sollte. Um den Export durchführen zu können, muss zuerst die Ansicht durch Klick auf aktualisieren ![](Export_aktualisieren.gif) (F5) aktualisiert werden.
Nun sehen Sie die im Detail errechneten Daten für jeden Mitarbeiter des Zeitraumes für jede Lohnart.
Wir haben hier auch die Lohnstundenart jeder Lohnart detailliert ausgeführt, damit Kontrollen leichter durchgeführt werden können.
Für den Export werden die Stunden jeder Lohnart aufsummiert und als ein Wert exportiert. Zusätzlich werden Lohnarten nicht übertragen, deren Gesamtsumme 0,00 (Null) ergibt.
Wurden die Daten entsprechend angezeigt und sind diese so wie von Ihnen gedacht, so kann durch den Klick auf ![](Export_durchfuehren.gif) Export, hier wird vorneweg der eingestellte Name des Exportformates angezeigt, der Export in die eingestellte Datei auf Basis der oben angezeigten Daten durchgeführt werden. Die Parameter müssen wie oben beschrieben entsprechend definiert sein.

Kommt nach Klick auf den Export Button eine Fehlermeldung so öffnen Sie bitte das Detail Fenster.
Üblicherweise sehen Sie hier, warum der Export nicht durchgeführt werden konnte.
So steht hier z.B.:
![](Export_Fehlermeldung.jpg)
Zugriff verweigert. D.h. die Exportdatei konnte / durfte an der gewünschten Stelle nicht erzeugt werden. Handeln Sie bitte entsprechend.

Die weitere Vorgehensweise ist von Ihrem verwendeten Lohn-Programm abhängig. Nachfolgend eine lose Sammlung der uns derzeit bekannten Importvorgänge:

### Varial

Wechseln Sie nun in Ihr Varial Importprogramm und starten Sie den ersten Teil des Imports.

Aus Varial wird ein Importprotokoll erstellt, dieses sollte ähnlich dem unten abgebildeten Protokoll aussehen.![](Varial_Lohnexport_Uebertragungsprotokoll.png)

Der tatsächliche Import ins Varial geht über einen weiteren Zwischenschritt.
Dieser ist nur möglich, wenn das Abrechnungsmonat noch nicht abgeschlossen ist. Führt man trotzdem einen Import durch, so ist Aufgrund des Imports nicht ersichtlich, dass dieser vom Varial NICHT durchgeführt wurde.
Prüfen Sie daher nach dem Import immer, ob die importierten Daten tatsächlich übernommen wurden.

#### Aktualisiert der Lohndatenexport die Gleitzeitsaldi?
Nein, auch wenn die grundsätzliche Berechnung über die Funktion der Monatsabrechnung läuft, wird der Gleitzeitsaldo der jeweiligen Person bewusst nicht aktualisiert.