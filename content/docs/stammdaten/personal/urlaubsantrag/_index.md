---
title: "Urlaubsantrag"
linkTitle: "Urlaubsantrag"
categories: ["Personal"]
tags: ["Urlaubsantrag"]
weight: 300
description: >
  Wie den Urlaubsantrag definieren und einrichten
---
Der EMail Absender des Urlaubsantrages ist die im Personal, Reiter Daten unter Absenderdaten hinterlegte EMail Adresse.

WICHTIG:
Um den Urlaubsantrag auch per EMail zu versenden, **muss** im Client der Urlaubsantragsbutton ![](Urlaubsantrag.png) verwendet werden. Wird nur Neu und Sondertätigkeit Urlaubsantrag verwendet, wird dieser Antrag zwar gespeichert, aber nicht als EMail versandt.

Um den Urlaubsantrag per EMail beantworten zu können, muss im Personal, Reiter Detail bei der Person die den Urlaubsantrag gestellt hat, die persönliche / Private EMailadresse hinterlegt sein.