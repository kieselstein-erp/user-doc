---
title: "Urlaubsanspruch"
linkTitle: "Urlaubsanspruch"
categories: ["Personal"]
tags: ["Personal", "Urlaub"]
weight: 200
description: >
  Urlaubsanspruch
---
Urlaubsanspruch
===============

#### Wie wird der Urlaub richtig eingegeben?
Der Urlaubsanspruch Ihrer Mitarbeiter wird im Personal im Reiter Urlaubsanspruch definiert.

Grundsätzlich ist der Urlaubsanspruch Jahresweise anzugeben. Aufgrund der derzeitigen gesetzlichen Regelungen rechnet **Kieselstein ERP** den Urlaubsanspruch immer im Kalenderjahr.
Der eingetragene Urlaubsanspruch wird immer automatisch in das nächste Kalenderjahr übertragen, wenn nicht für das "nächste" (oder das übernächste) Kalenderjahr der Urlaubsanspruch neu definiert wird. Auch wenn es sich bei dem ersten erfassten Jahr von der Erfassung her um ein Rumpfjahr handelt, so wird von **Kieselstein ERP** der für das (Rumpf-)Jahr angegebene Urlaubsanspruch so wie er definiert ist für die Berechnung verwendet. Eine Aliquotierung wird NICHT vorgenommen. **Hinweis:** Beachten Sie bitte, das die Urlaubsabrechnung von **Kieselstein ERP** auf der derzeit aktuellen gesetzlichen Abrechnung des Jahresurlaubs aufbaut. Das bedeutet der Urlaubsanspruch entsteht immer zum 1.1\. des Kalenderjahres. Siehe dazu bitte auch den Parameter URLAUBSABRECHNUNG_ZUM_EINTRITT mit dem auf die Urlaubsberechnung ab Eintrittsdatum umgeschaltet werden kann. **Hinweis:** In den Softwareversionen ab 2009 wird der Urlaubsanspruch bei Eintritten während des Kalenderjahres aliquotiert, um auch für diese Mitarbeiter während des ersten Jahres eine richtige Anzeige des aliquoten Anspruches zu erreichen. Siehe dazu unten Berechnungsbeispiel. **Hinweis:** In den Softwareversionen ab 2009 erfolgt für die Ansprüche ab 2009 die Berechnung der Urlaubsanspruchsstunden automatisch anhand der hinterlegten Zeitmodelle und der Urlaubstage. D.h. es wird nur mehr der Urlaubsanspruch in Tagen für das gesamte Kalenderjahr eingegeben. Von **Kieselstein ERP** wird bei jeder Urlaubsberechnung der Anspruch von Tagen auf Stunden anhand der hinterlegten Zeitmodelle für das Berechnungsjahr umgerechnet. Um die vor Einführung dieser Rechenmethode manuell eingegebenen Stunden nicht zu verändern, wird diese Berechnung erst ab dem Jahr 2009 durchgeführt.
Für die Berechnung des Anspruches werden immer die Urlaubswochen multipliziert mit den Urlaubstagen pro Woche aus dem Zeitmodell verwendet.
Die Umrechnung der Urlaubswochen auf die Darstellung der Urlaubstage geht immer von einer 5Tage-Woche aus.
Das bedeutet, wenn der Anspruch in Tagen erfasst werden soll und wenn eine abweichende Tagesanzahl pro Arbeitswoche besteht, muss der Anspruch in Wochen entsprechend umgerechnet werden.
**Beispiel:** 4Tage Woche, 35Tage Urlaubsanspruch
35/4=8,75Wochen. **WICHTIG:** Eine gleichzeitige Betrachtung von Urlaubstagen und Urlaubsstunden ist nur für Zeitmodelle gültig, welche die gleiche Anzahl an Arbeitstagen haben, wie im Mandantenparameter URLAUBSTAGE_PRO_WOCHE definiert. Bei allen anderen Zeitmodellen müssen Sie festlegen, ob für Sie die Auswertung nach Urlaubsstunden oder nach Urlaubstagen relevant ist. Anhand dieser Festlegung sind dann entweder die Urlaubsanspruchstage anhand der Wochen (25Tage) oder anhand der Arbeitstage je Zeitmodell einzugeben.
Ein Beispiel:
Im aktuell gültigen Zeitmodell steht der Wert Urlaubstage pro Woche auf 5 Tage / Woche.
Es ist ein Urlaubsanspruch von 25Tagen eingetragen.
a.) 
Der Mitarbeiter hat ein Zeitmodell mit 25Std pro Woche, verteilt auf 3 Arbeitstage.
Die Stundenweise Urlaubsabrechnung ergibt die richtige Stundenzahl, anhand des Jahresanspruches von 125Std.
Die Tageweise Urlaubsabrechnung ergibt eine falsche Anzahl, da ja ein Anspruch von 25Tagen / Kalenderjahr definiert wurde. Wenn Tageweise abgerechnet werden sollte, so müssen 15 Urlaubstage als Anspruch definiert werden.
b.)
Der Mitarbeiter hat ein Zeitmodell mit 25Std pro Woche, jedoch verteilt auf 5 Arbeitstage.
Die Stundenweise Urlaubsabrechnung ergibt die richtige Stundenzahl, anhand des Jahresanspruches von 125Std.
Die Tageweise Urlaubsabrechnung ergibt die richte Tagesanzahl, da auch der Anspruch von 25Tagen / Kalenderjahr richtig definiert ist.

<a name="Urlaubsanspruch"></a>Urlaubsanspruch Stundenweise, Tageweise oder Wochenweise?

Wie bereits oben ausgeführt ist der Urlaub um den bezweckten Wert der Erholung zu haben laut Gesetzgeber Tagesweise zu geben.
Der Urlaubsanspruch wird jedoch in unterschiedlichen Ländern unterschiedlich gehandhabt. Das bedeutet:

| Land | Berechnungsart | Anspruch / Kalenderjahr |
| --- |  --- |  --- |
| Österreich | Wochenweiser Urlaubsanspruch | 5 Wochen |
| Deutschland, Schweiz | Tageweiser Urlaubsanspruch | 24 Tage |

Die grundsätzliche Aufgabenstellung für alle beteiligten Personen ist, wie wird mit wechselnden Zeitmodellen umgegangen, also wieviele Tage Urlaubsanspruch hat ein Mitarbeiter im gesamten Kalenderjahr, wenn er einige Zeit ein Zeitmodell mit drei Arbeitstagen hat und dann z.B. eines mit fünf Arbeitstagen. Hier hilft die wöchentliche Betrachtung.
Daher wird in **Kieselstein ERP** nur mehr der Jahresurlaubsanspruch auf Wochenbasis abgespeichert. Die angezeigten und eingebbaren Urlaubstage sind eine reine Rechenhilfe, welche auf einer fünf Tage Woche beruhen.
Daraus ergibt sich, dass z.B. für 24 Urlaubstage pro Jahr eine Wochenbasis von 4,8 Wochen angezeigt und auch als Grundlage verwendet wird.

Ab dem Urlaubsanspruch für 2009 wird dieser wie folgt berechnet:
 **a.) Stundenweise-Betrachtung:** Relevante Datenfelder:
a1.) Anzahl der Urlaubsanspruchs-Wochen des zu berechnenden Jahres (Kommt aus Personal/Urlaubsanspruch des zu berechnenden Jahres) (im Normalfall 5 Wochen)
a2.) Die Sollstunden der jeweiligen Zeitmodelle (von Montag bis Sonntag ohne Feiertage)

Der einfachste Fall ist hiermit folgender: Wenn jemand das ganze Jahr keinen Eintritt/Austritt hat und immer das gleiche Zeitmodell hat, dann ist der Jahresurlaubsanspruch wie folgt:
            Jahresurlaub in Wochen (a1.) * Sollstunden des Zeitmodells (a2.) = z.B. 5*38,5=192,5

Wenn der Eintritt nun nach dem 1\. Jänner erfolgt, bzw. mehrere Zeitmodelle während eines Jahres verwendet werden, dann werden die Urlaubsansprüche anteilsmäßig addiert: <u>Beispiel:</u> Eintritt am 1.Mai mit Zeitmodell 38,5 Stunden, Zeitmodellwechsel am 8\. August mit 30 Stunden:
(Jahresurlaub in Wochen (a1.) * Anzahl der Tage des Zeitmodells / Anzahl der Tage im Jahr) * Sollstunden des Zeitmodells (a2.)
ergibt:
Zeitraum 1: 31.Mai  bis 7.Aug. (5*  69/365)*38,5 = 36,39
Zeitraum 2: 8.Aug. bis 31.Dez. (5*145/365)*30    = 59,58
                                                                   = 95,97 Stunden Urlaubsanspruch für dieses Kalenderjahr
 **b.)  Tagesweise-Betrachtung:** Diese wird analog zur Stundenweisen Betrachtung berechnet. Es werden jedoch weitere Datenfelder aus der Definition des Zeitmodels herangezogen:
Relevante Datenfelder:
b1.) Urlaubstage pro Woche

Die Formel lautet nun:
(Jahresurlaub in Wochen (a1.) * Anzahl der Tage des Zeitmodells / Anzahl der Tage im Jahr) * Urlaubstage pro Woche (b1.)
 <u>Beispiel:</u> Eintritt am 1.Mai mit 5 Tagen Urlaub pro Woche, Zeitmodellwechsel am 8\. August mit 4 Tagen Urlaub pro Woche
Zeitraum 1: 31.Mai  bis 7.Aug. (5*  69/365)*5 = 4,72
Zeitraum 2: 8.Aug. bis 31.Dez. (5*145/365)*4 = 7,94
                                                               =12,66 Tage Urlaubsanspruch für dieses Kalenderjahr

Anzahl der Tage des Zeitmodells zu obigem Beispiel: 31.5\. - 7.8\. = 1+30+31+7=69

Urlaubsanspruch und vorübergehende Entlassung von Mitarbeitern?

Werden Mitarbeiter zeitweise (5Wochen) abgemeldet und danach wieder im Unternehmen beschäftigt, so ist in aller Regel der Mitarbeiter bei Austritt vollständig abzurechnen. D.h. es werden auch alle Urlaubs- und Gleitzeitansprüche ausbezahlt. Tritt der Mitarbeiter wieder ins Unternehmen ein, so hat er ein neues Eintrittsdatum. Durch die aliquote Berechnung des Urlaubsanspruches ergibt sich daraus, dass sein Urlaubsanspruch für das Rumpfjahr eben aliquot zum neuen Eintritt errechnet wird.
Bitte beachten Sie dazu auch das Thema des Wiedereintrittes z.B. nach Karenz- / Vaterschaftsurlaub.

<a name="Halbtag"></a>

#### Wie ist der Urlaub für Weihnachten und Silvester richtig einzugeben?
Für diese Tage (24.12\. & 31.12.) sind, zumindest in Österreich, einige Regeln zu beachten.
In sehr vielen Unternehmen wird dieser Tag gänzlich freigeben und für die, über die vom Unternehmen freizugebenden Stunden hinaus, anfallenden Stunden wird Urlaub oder Zeitausgleich genommen.
In vielen Kollektivverträgen ist definiert: es ist ab 12:00 frei zugeben.
Bei Mitarbeitern mit festen Zeiten ist dies noch relativ einfach, bei Mitarbeitern mit Gleitzeitmodellen, stellt sich die Frage, von welchem Stundenausmaß auszugehen ist.
Von den Unternehmen wird dies üblicherweise so gehandhabt, dass für diesen Tag ein halber Urlaubstag (ab)gebucht wird und die andere Hälfte des Tages wird vom Unternehmen als bezahlte Nichtanwesenheit gebucht.
Eine sehr pragmatische Lösung ist, dass z.B. für den 24.12\. ein ganzer Feiertag eingetragen wird und für den 31.12\. ein ganzer Urlaubstag gebucht wird. Betrachtet man beide Tage gemeinsam, ist unabhängig auf welchen Wochentag diese beiden Tage fallen, die Abrechnung zum 31.12\. richtig. Muss taggenau gebucht werden, so ist Folgendes zu beachten, wobei es darum geht, dass sowohl die Urlaubstage als auch die tatsächlich freigegebenen Stunden richtig verwaltet werden.
Voraussetzung:
Die Tagesart Halbtag ist in den [Tagesarten](#Tagesarten) definiert und im Betriebskalender sind die Feiertage mit der Tagesart Halbtag eingepflegt.
In den Sonderzeiten des jeweiligen Mitarbeiters ist ein Eintrag mit einem halben Tag Urlaub gebucht.
Der Halbtag ist von den Sollzeiten her so definiert, dass er exakt die halben Sollstunden eines sonst üblichen Arbeitstages (z.B. des Montags) hat.
![](Halbtagsurlaub2009.gif)
Haben alle Tage der Arbeitswoche (des Gleitzeitmodells) eine gleiche Sollzeit, so werden, wie oben dargestellt, die Urlaubsstunden, die Urlaubstage und auch die Sollstunden dieses Tages entsprechend richtig dargestellt.

Kompliziert wird es, wenn die beiden Tage z.B. auf einen Freitag fallen, an dem normalerweise kürzer gearbeitet wird.
Meistens kommt hier der Wunsch / die Forderung dazu, dass bis Mittag gearbeitet werden sollte, trotzdem ein halber Urlaubstag gebucht werden sollte und dass die Gleitzeit und Urlaubsstunden auch noch stimmen sollten.
Wir nur der Halbtag mit einem halben Urlaubstag für diesen Tag eingetragen, so stellt sich die Zeitabrechnung wie folgt dar:
![](Halbtagsurlaub2010.gif)
Wie aus obiger Darstellung ersichtlich, wird die Tagesart Halbtag speziell behandelt.
D.h. es werden die Sollstunden der Tagesart (hier 4,25) mit den Sollstunden eines gleichen Wochentages (hier 4,50) verglichen.
Die Stunden die über die Sollstunden des Halbtages hinaus gehen, werden als Feiertagsstunden gewertet und entsprechend gutgeschrieben. Da für diesen Tag zusätzlich ein halber Urlaubstag gebucht wurde, wird der halbe Tag aufgrund des Tages (in diesem Falle Freitag mit 4,50 Sollstunden) als Urlaubsverbrauch eingetragen.
Üblicherweise wird in vielen Unternehmen gedanklich dieser Tag nur ein halber Urlaubstag betrachtet. D.h. dass die Differenz der Urlaubsstunden zu den nicht geleisteten Sollstunden (bis 12:00) ebenfalls gutzuschreiben ist.
Beispiel zu obiger Darstellung:
Mitarbeiter arbeitet am Freitag üblicherweise von 8:00-12:30
Am Hl. Abend ist ab 12:00 freizugeben.

| Tagesart | Stunden |
| --- |  --- |
| Halbtags Sollstunden | 4,25 |
| abzgl. Ftg. Stunden  | 0,25 |
| abzgl. Urlaubsstd. | 2,25 |
| Noch einzutragende Gutstunden.<br>Diese werden vom Unternehmen dem Mitarbeiter geschenkt. | 1,75 Std |

Buchen Sie diese Gutstunden im Modul Personal, Auswahl des jeweiligen Mitarbeiters in der Auswahlliste und dann im Reiter Stundenabrechnung.

#### Kann auch Zeitausgleich an 24./31\. Dezember eingegeben werden? 
Für den 24./31\. wird in der Regel die Tagesart Halbtag verwendet und dies im Betriebskalender definiert. In den Zeitmodellen muss nun als Sollzeit für den Halbtag 4Std an Sollzeit hinterlegt sein (siehe oben).
Das bedeutet nun, wenn für das Datum ein (ganzer) Tag Zeitausgleich definiert wird, so werden nur die Sollstunden, die ja mit den vier Stunden nur dem halben Tag entsprechen, an Zeitausgleichsstunden (oder auch andere Sondertätigkeit an dem Tag eingetragen ist) gebucht.

#### Es wurde der Urlaubsanspruch geändert, diese Änderungen wirken jedoch nicht?
Wurden im Personal, Urlaubsanspruch neue Daten für die Person hinzugefügt, bzw. bestehende geändert, so werden die daraus abgeleiteten Daten erst bei der nächsten Berechnung des Urlaubsanspruches aktualisiert.

D.h. wenn z.B. der Jahresanspruch geändert wurde, aber der ermittelte Anspruch noch nicht aktualisiert wurde, so muss die Aktualisierung durchgeführt werden. Diese Aktualisierung = Neuberechnung des Urlaubsanspruches erfolgt immer dann, wenn Sie entweder im Personal auf ![](Urlaubsanspruch_jetzt_berechnen.gif) Anspruch aliquot (anteilig) bzw. Ende des Jahres klicken oder wenn in der Zeiterfassung unter Info, Monatsabrechnung eine Monatsabrechnung für das jeweilige Jahr ausgedruckt wird.

Erst danach stimmt die Anzeige mit den tatsächlich hinterlegten Werten überein.

#### Nachträgliche Aufrollung / Änderung von Urlaubsansprüchen über den Jahreswechsel hinweg?
Es kommt manchmal vor, dass man erst bei der Abrechnung der Mitarbeiter feststellt, dass der Jahresübertrag nicht wie gewünscht ausfällt.

Meistens ist die Situation die, dass bei einem Nachdruck der Monatsabrechnung des alten Jahres eine Differenz zum Ausdruck im nun aktuellen Jahr festgestellt wird.

Es könnten die Daten dazu z.B. wie folgt aussehen:

Ausdruck im aktuellen Jahr:

![](Urlaubsanspruch_falsch1.gif)

Ausdruck (zum faktisch gleichen Zeitpunkt) für das vergangene Jahr:

![](Urlaubsanspruch_falsch2.gif)

Wie man sieht, ergibt sich bei der Dezemberabrechnung ein Rest von 29,5 Urlaubstagen. Im Ausdruck des Folgejahres steht aber 31 Tage. Die Ursache dafür ist, dass im Reiter Urlaubsanspruch der "früher" errechnete Resturlaub bis Ende des Jahres auf gesperrt (Gefroren) gesetzt wurde. Das bedeutet, dass, egal welche Werte **Kieselstein ERP** ausrechnet, es ist für die weitere Berechnung der (manuell) hinterlegte Wert zu verwenden.

![](Urlaubsanspruch_falsch3.gif)

Wie ist nun für die Korrektur vorzugehen?

1.) entfernen Sie die Haken bei Gesperrt (Ändern, Haken raus, speichern)

2.) Drucken Sie die Monatsabrechnung zum Ende des alten Jahres zumindest in die Vorschau.
Klicken Sie für die Aktualisierung auf die oberen Aktualisierungspfeile ![](Urlaubsanspruch_falsch4.gif).
Sie finden nun den erwarteten Wert unter Resturlaub.
![](Urlaubsanspruch_falsch5.gif)

3.) Drucken Sie die Monatsabrechnung des aktuellen Jahres. Nun finden Sie auch hier den entsprechenden Übertrag.

![](Urlaubsanspruch_falsch6.gif)

#### Kann solch eine Situation auch auftreten, ohne dass das Gesperrt angehakt ist:
Ja und zwar immer dann, wenn im alten Jahr Änderungen, in diesem Falle beim Urlaub gemacht werden.

Beachten Sie, das generell bei Änderungen in alten Zeiten, für die die Monatsabrechnung bereits gemacht wurde, immer zwingend erfordern, dass jede einzelne Monatsabrechnung von der Änderung bis hin zur aktuellen Monatsabrechnung in der chronologisch richtigen Reihenfolge, also die älteste zuerst und danach die jeweils unmittelbar darauf folgende zumindest in die Druckvorschau gedruckt werden. Nur dadurch wird der monatliche Zwischenstand aktualisiert.

#### Was bedeuten die einzelnen Zeilen in der Urlaubsabrechnung:
![](Urlaubsanspruch.gif)

| Feld | Bedeutung |
| --- |  --- |
| Ab Jahr | Der Urlaubsanspruch wird ab diesem Jahr gerechnet und automatisch beim Jahreswechsel in das neue Jahr übertragen. |
| Jahresanspruch in Wochen | Angabe des Urlaubsanspruches in Wochen. Daneben finden Sie die Angabe des Urlaubsanspruches in Tagen. Beachten Sie für diese wechselseitige Umrechnung dass wir hier von fix 5Tagen für eine Urlaubswoche ausgehen.Geben Sie hier immer den vollen Urlaubsanspruch für das ganze Kalenderjahr ein. |
| Ermittelter Anspr. lt. ZM. in Tagen | Ermittelter Urlaubsanspruch laut Zeitmodell in TagenAufgrund des Eintrittsdatums des Mitarbeiters und seines / seiner Zeitmodelle(s) werden die tatsächlichen Urlaubstage berechnet.Beachten Sie dazu: - im Zeitmodell, Detail, Urlaubstage pro Woche- Parameter: ERWEITERTER_URLAUBSANSPRUCH |
| Tage zusätzlich | Definieren Sie hier die Urlaubstage, welche dem Mitarbeiter zusätzlich gewährt werden. Diese Zahl kann, z.B. bei Datenübernahmen, auch negativ sein. |
| Ermittelter Anspr. lt. ZM. in Stunden | Ermittelter Urlaubsanspruch laut Zeitmodell in StundenAnalog zu ermittelter Urlaubsanspruch in Tagen, jedoch unter Berücksichtigung der Stunden des / der Zeitmodelle(s) für das entsprechende Jahr |
| Stunden zusätzlich | Definieren Sie hier die Urlaubsstunden, welche dem Mitarbeiter für das Jahr zusätzlich gewährt werden. Auch diese Zahl kann negativ sein. |
| Eintritt | Hier wird zu Ihrer Information das letzte Eintrittsdatum des Mitarbeiters angezeigt. |

#### In der Monatsabrechnung wird der Resturlaub nicht richtig übernommen?
Im Reiter Urlaubsanspruch des jeweiligen Mitarbeiters im Modul Personal wird im Detail, neben den Anspruchsdaten der zum Jahresende errechnete Urlaubsanspruch angezeigt. 
![](Personal_Urlaub_gefroren.jpg)
Diese Daten werden bei jeder Monatsabrechnung und bei jedem Klick auf Anzeige des Urlaubsanspruches neu berechnet. Diese Daten sind die jeweilige Ausgangsbasis für den Urlaubsrest des Folgejahres, also der Urlaubsübertrag für das nächste Jahr.
Wurde das darunter angeführte Gesperrt (gefroren) nicht angehakt, so werden diese Daten bei jeder Berechnung (siehe oben) aktualisiert. Sind diese als Gesperrt markiert, so werden diese Daten von **Kieselstein ERP** nicht aktualisiert.
Das kann dazu führen, dass ein eventueller Urlaubsübertrag nicht wie erwartet durchgeführt wird.
Bei den Monatsabrechnungen wird immer der aktuell errechnete Wert angedruckt.
Die Überträge werden jedoch von den gespeicherten (und eventuell eingefrorenen und daher möglicherweise falschen Werten) genommen.

#### Wie ist der Resturlaub zum Start bei Berechnung des Urlaubsanspruchs zum Eintritt einzugeben?
Mit der Berechnung des Urlaubs zum Eintrittsdatum des Mitarbeiters, ist der aus den Vorsystemen zu übernehmende Urlaubsanspruch manchmal nicht sofort ersichtlich. Hintergrund ist, dass sich durch die Verschiebung des Beginns (da zum Eintritt gerechnet werden muss) der Resturlaub ein Jahr früher einzutragen ist.
Beispiel:
Eintritt 1.12.2014
Abrechnungsstichtag 30.9.2021\. D.h. von der Urlaubsabrechnung aus gesehen sind wir noch in der Urlaubsperiode 1.12.2020 bis 30.11.2021\. Also in der Periode 2020.
Daher ist der Resturlaub in der Periode 2019 einzugeben.

#### Der Mitarbeiter hat zuwenig Urlaubstage?
Ausgangsbasis:

Der Mitarbeiter ist am 2.11\. eingetreten.

Er hat einen Jahres-Urlaubsanspruch von 5Wochen

Er arbeitet immer kurze / lange Woche abwechselnd. D.h. kurze Woche = 4Arbeitstage, lange Woche = 5Arbeitstage

Daraus ergibt sich ein Urlaubsanspruch von 3,70 Tagen. Warum ?

Nach obiger Formel 5 x 5 / 365 * 5 = 0,3425 Tage pro langer Woche und 5x5/365*4 = 0,2740

Wir haben 5 lange und 4 kurze Wochen = 5x0,3425 + 4x0,2740 = 2,80 Tage

(Jahresurlaub in Wochen (a1.) * Anzahl der Tage des Zeitmodells / Anzahl der Tage im Jahr) * Urlaubstage pro Woche (b1.)

#### Urlaub in Zeitausgleich wandeln
Es kommt immer wieder vor, dass Mitarbeiter, obwohl gesetzlich nicht zulässig, zuwenige Iststunden haben und dafür den Urlaub "hergeben".
Gedanklich wird das dann meist ähnlich wie, er bekommt statt Urlaub Zeitausgleich, beschrieben.
Es bedeutet dies nichts anderes, dass der Mitarbeiter z.B. einen Gleitzeitsaldo von -40 Std. hat und diesen gleicht er durch den Verbrauch von fünf Urlaubstagen wieder auf.
Die Lösung ist, vereinbaren Sie mit dem Mitarbeiter, dass Sie für eine festzulegende Woche, oder für jeweils einen Wochentag ihm **zusätzlich** zu seinen geleisteten Anwesenheitsstunden auch den Urlaub eintragen. Damit werden die Urlaubstage verbraucht und die Urlaubsstunden fließen in seinen Gleitzeitsaldo ein, womit dieser positiv wird, also die Urlaubsstunden gegen den Gleitzeitsaldo verbucht werden.
Bitte beachten Sie dabei:
- Wenn Sie am Freitag immer kürzer arbeiten sollten, z.B. wegen 38,5Std Woche, so werden, wenn Sie nur Freitage als Urlaub buchen, weniger Stunden verbraucht als an anderen Tagen.
- Wenn Sie komplexe Kolletiv/Tarifverträge haben, können diese Urlaubstage auch Überstunden bewirken, in diesem Falle sollten ev. nur Halbe-Urlaubstage gebucht werden.

#### Erweiterter Urlaubsanspruch
Im Parameter ERWEITERTER_URLAUBSANSPRUCH wird die Art der Berechnung des Urlaubes definiert:

**0 ... Standard**

Steht dieser Parameter auf 0, was der Standard Wert ist, so gilt die oben beschriebene Berechnung des Urlaubsanspruches, welche mit dem Kalenderjahr harmoniert

**1 ... Erweiterter Urlaubsanspruch**

In manchen Unternehmen ist es noch immer üblich, dass Mitarbeiter bei einem Eintritt während der ersten Jahreshälfte für dieses Jahr den vollen Urlaubsanspruch erhalten. Das bedeutet es wird im Eintrittsjahr bei einem Eintritt bis inkl. 30.6. des Eintrittsjahres der volle Anspruch gerechnet. Bei einem Eintritt ab dem 1.7. wird der Urlaub wie üblich aliquotiert.

**Wichtig:**

Tritt ein Mitarbeiter in der ersten Jahreshälfte ein, so ist zur richtigen Berechnung der Urlaubstage die Definition des Zeitmodells für den Mitarbeiter ab 1.1\. des Jahres zwingend erforderlich.

**2 ... Urlaubsanspruch nach TVÖD**

Berechnung des Urlaubsanspruches nach dem Tarifvertrag des öffentlichen Dienstes in Deutschland.

Dieser besagte, dass ein Mitarbeiter xx Tage Urlaubsanspruch für ein ganzes Jahr hat.

Tritt der Mitarbeiter während des Jahres ein, so erhält er 1/12tel des Jahresurlaubsanspruches für jedes volle(!!) Monat, welches er im Unternehmen ist. Der errechnete Anspruch wird kaufmännisch auf eine ganze Zahl gerundet.

Beispiel:

Der Mitarbeiter hat 29Tage Jahresurlaub

Er tritt am 1.5\. ein.

Es gebühren ihm 29/12*8 Urlaubstage = 19,33 Tage. Ergibt gerundet 19Tage

Der Mitarbeiter tritt erst am 2.5\. ein.

Es gebühren ihm 19/12*7 Urlaubstage = 19,91 Tage. Ergibt gerundet 17Tage