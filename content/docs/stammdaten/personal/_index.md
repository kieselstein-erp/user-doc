---
title: "Personalverwaltung"
linkTitle: "Personen"
categories: ["Personen"]
tags: ["Personen", "Mitarbeiter:innen", "Zeitmodell", "Schichtzeit","Kollektivvertrag", "Tarifvertrag"]
weight: 500
description: >
  Verwaltung der Mitarbeiter:innen
---
Personal
========

[Definition der Zeitmodelle]( {{<relref zeitmodell_definition>}} )

[Informationen zu Urlaub und Urlaubsanspruch]( {{<relref urlaubsanspruch>}} )

#### Zusammenhang Partner / Personal?
[siehe.](../Partner/index.htm#Kund, Partner, Lieferant wie gehört das alles zusammen)

#### Warum kann kein neues Personal angelegt werden?
Zum Anlegen neuer Personen benötigen Sie das Recht RECHT_PERS_SICHTBARKEIT_ALLE. Ist dies in Ihrer Rolle nicht definiert, so dürfen Sie nur lesend auf die Daten zugreifen. Zur Rechtevergabe wenden Sie sich bitte an Ihren Administrator.

#### Ich habe eine neue Person angelegt, sehe sie aber nicht
Um die Personen Ihrer Abteilung / Kostenstelle zu sehen, müssen Sie das Recht PERS_SICHTBARKEIT_ABTEILUNG haben.

Um alle Personen des Mandanten zu sehen müssen Sie das Recht PERS_SICHTBARKEIT_ALLE haben.

#### Beim Anlegen einer neuen Person wird immer eine falsche / zu hohe Personalnummer vorgeschlagen
Die Personalnummer einer neuen Person wird immer anhand der höchsten Personalnummer aller auf diesem Mandanten eingetragenen Personen ermittelt. Um alle Personen Ihres Mandanten einzusehen müssen Sie das Recht PERS_SICHTBARKEIT_ALLE haben und zusätzlich die versteckten Personen anzeigen lassen.

#### Ich sehe zwar alle Mitarbeiter meiner Abteilung, kann aber keinen neuen Mitarbeiter anlegen
Um neue Mitarbeiter anlegen zu können benötigen Sie das Recht PERS_SICHTBARKEIT_ALLE.

#### Ich kann keine neue Person mehr abspeichern
Sie erhalten beim Speichern einer neuen Person immer die Fehlermeldung (Details):

*class com.lp.client.frame.ExceptionLP
null; CausedByException is:
PERS_PERSONAL.UK
com.lp.client.frame.delegate.Delegate.handleThrowable(Delegate.java:87)*

Dies bedeutet, dass eine Person mit dieser Personalnummer bzw. diesem Partner bereits angelegt ist.

Wenn Sie diese Person aber nicht sehen, so haben Sie vermutlich kein Recht andere Personen einzusehen.

Prüfen Sie dies bitte im Modul Benutzer, Systemrolle, Rechte. Um alle Personen einzusehen, müssen das Recht PERS_SICHTBARKEIT_ALLE haben.

#### Muss man das Gehalt jedes Monat neu eingeben?
Änderungen in Gehaltsvereinbarungen werden in der Regel nur monatsweise vereinbart. Deshalb kann eine Gehaltsänderung auch nur auf Monatsbasis eingegeben werden. Sie wirkt dann immer ab dem 1\. des eingegebenen Monates. Vereinbarte / Eingetragene Gehälter / Löhne gelten so lange, bis eine neue Vereinbarung eingegeben wurde. Siehe dazu bitte auch [Gehalts-/Lohnkosten Definition](#gehalts-lohnkosten-definition).

#### Sieht jeder das Gehalt?
Einsicht auf das Personalgehalt haben nur Benutzer, welche das Recht PERS_GEHALT_R für das nur lesen bzw. PERS_GEHALT_CUD um auch Änderungen vornehmen zu können besitzen.
Mit dem Recht PERS_GEHALT_R/CUD verbunden ist eine eigene Variante des Kurzbriefes, genannt Kurzbrief Gehalt ![](Kurzbrief_Gehalt.gif) um hier z.B. Vorrückungsschreiben oder ähnlich kritische Informationen ablegen zu können.

#### Was ist der Unterschied zwischen Heimatkostenstelle und Abteilung?
Der Mitarbeiter ist einer Abteilung z.B. Innendienst zugeordnet. Dies ist seine organisatorische Einheit der er zugeordnet ist. Dagegen ist die Heimatkostenstelle jene Organisationseinheit für die er üblicherweise Arbeitet. Z.B. Verkauf Salzburg. Diese Heimatkostenstelle wird als default Vorschlag für den Kostenstellenvorschlag verwendet, wenn keine anderen Daten verfügbar sind.

#### Absenderdaten
#### Telefon, Fax, E-Mail, Homepage? Sind das jetzt die Firmenadressdaten oder die privaten Daten des Mitarbeiters?
<a name="Absenderdaten"></a>
Unter Detail finden sie im Personal die privaten Daten des Mitarbeiters. Also seine Wohnadresse, seine persönliche, private Handynummer, seine private Telefonnummer.

Unter Daten im Bereich Absenderdaten werden die "Firmendaten" Ihres Mitarbeiters definiert. Also unter welcher EMail Adresse er bei Ihnen im Unternehmen erreichbar ist.

![](Absenderdaten.gif)
Bitte unterscheiden Sie klar zwischen den persönlichen Kontaktdaten des Mitarbeiters, welche Sie unter Detail finden und den Absenderdaten des Mitarbeiters in Ihrem Unternehmen, welche Sie unter Daten, Absenderdaten finden.

[Übersteuern der Fax-Absenderinformation](../Installation/Installation_Win_Fax_Email_Dienst.htm#Fax-Absenderinformation übersteuern)

#### Wozu dienen Unterschriftsfunktion und Unterschriftstext?
Mit der Unterschriftsfunktion definieren Sie mit welchem Zusatz diese Person Ihre offiziellen Papiere, z.B. Bestellungen oder Auftragsbestätigungen unterschreibt. Also z.B. i.A., i.V. oder ppa usw.
Mit dem Unterschriftstext beschreiben Sie den Aufgabenbereich den diese Person nach außen hat. Z.B. Einkaufsleitung, Innendienst, usw.. ![](Unterschrift.gif)
Geben Sie hier die Abkürzung für die Unterschriftsfunktion ein zum Beispiel i.V. (en: on behalf of) oder i.A. (en: O/o (order of)).

#### Signaturen
#### Wie definiere ich die Unterschriftstexte der Mitarbeiter / Vertreter?
<a name="Signaturen"></a>
Dies wird im Personal unter Daten definiert.
Tragen Sie hier bei Unterschriftsfunktion den Funktionszusatz für die Unterschrift ein z.B. i.V. oder i.A.
Bei Unterschriftstext tragen Sie die Information für die Position des Mitarbeiters ein. Z.B. Einkaufsleitung, Vertriebsassistenz oder ähnliches. Beide Texte werden bei den Unterschriften unter den offiziellen Dokumenten mit angedruckt.

#### Kann eine Signatur für Texteingaben, Kurzbriefe definiert werden?
Im Modul Personal, Reiter Daten kann unter Signatur jener Text eingegeben werden, der bei Klick auf ![](Signatur_einfuegen.gif) Signatur am Ende des Textes einfügen, eingefügt werden soll. Damit kann z.B. bei einem Kurzbrief immer der gleiche Text einkopiert werden.

#### Welche weiteren Personen spezifische Parameter können definiert werden?
Im Reiter Parameter können weitere Parameter für den Zugriff bzw. die Anzeige und ähnlichem definiert werden.
![](PersonenParameter.jpg)

#### Personen Parameter
<a name="Personen Parameter"></a>
Die Bedeutung der verschiedenen Parameter ist wie folgt:

| Parameter | Beschreibung |
| --- |  --- |
| keine Anzeige in Anwesenheitsliste | Diese Person wird in den Anwesenheitslisten nicht angezeigt.Wird gerne für leitende Mitarbeiter verwendet |
| keine Anzeige am Terminal | Am Terminal wird bei der Einstellung Anmeldeseite diese Person nicht angezeigt |
| keine Überstundenauszahlung |   |
| Telefonzeit mit Telefonzeitstart | Bei Klick auf den Telefonhörer wird gleichzeitig die Telefonzeiterfassung auf den jeweiligen Partner (Kunde/Lieferant) gestartet um so eine effektive Zeiterfassung der vielen kleinen notwendigen Unterbrechungen zu erreichen |
| Start mit meinen offenen Projekten | Beim öffnen des Projektmodules steht die Ansicht auf meine offenen Projekte, d.h. Sie / die Person bekommen / bekommt sofort die Liste der für Sie abzuarbeitenden offenen Projekte. |
| Anwesenheitsliste für alle am Terminal anzeigen | Wenn diese Person am Zeiterfassungsterminal die Liste der anwesenden Personen anzeigen lässt, so wird die Anwesenheit aller Personen angezeigt |
| Anwesenheitsliste für Abteilung am Terminal anzeigen | Ähnlich obigem, es werden jedoch nur die KollegInnen der eigenen Abteilung angezeigt. |
| Darf Kommt am Terminal buchen | Ist dies nicht angehakt, so darf die Person kein Kommt buchen |
| IMAP-Benutzer | Werden EMails durch den **Kieselstein ERP** Versanddienst versendet, so wird nach erfolgreichem SMTP Versand dieses EMail im "gesendete Objekte" des IMAP Servers abzulegen. Dafür muss sich der HELIUM V Server unter dem Benutzer am IMAP-Server anmelden können |
| IMAP-Kennwort | Das passende Kennwort zu obigem Benutzer |
| IMAP-Inbox | Wenn Sie das Zusatzmodul EMail Empfang besitzen werden eingehende EMails dieser Person von diesem IMAP-Ordner abgeholt |
| BCC | Werden offizielle Belege versandt, so wird automatisch auch eine unsichtbare Kopie (BlindCarbonCopy) an diese EMail-Adresse gesendet. Bitte beachten Sie auch die System-Parameter:BCC-Rechnung und BCC-Vertreter |
| generelle WEP Info | Wenn angehakt, bekommt man bei allen BS-Positionen, welche angehakt sind, immer eine WEP-Info-Mail, egeal ob man Anforderer ist oder nicht. |
| Alle Kontakte synchronisieren | Synchronisiert alle Partner über die [HV-OST](../Schnittstellen/index.htm#HV-OST) in die persönlichen Outlook-Kontakte. |

Für weitere Parametrierungen siehe bitte auch Arbeitsplatzparameter.

#### Personen verstecken?
Durch Aktivieren der Eigenschaft verstecken wird eine Person als versteckt gekennzeichnet.

Damit können Sie Personen die in der Auswahlliste normalerweise nicht mehr aufscheinen sollten (z.B. der Administrator) entsprechend kennzeichnen. Bitte beachten Sie dass diese Eigenschaft im Personal von der Eigenschaft im Partner getrennt ist.
Beachten Sie weiters, dass es ein eigenes Rollenrecht gibt, mit dem versteckte Partner angezeigt werden können. Rollen die dieses Recht nicht besitzen, können versteckte Partner / Personen nicht anzeigen.

#### Können die Zeitdaten auch exportiert werden?
Ja. Neben den üblichen Exportfunktionen der **Kieselstein ERP** Ausdrucke steht auch der Export nach Lohnarten zur Verfügung. [Siehe dazu bitte]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ).

#### Wo kann ich die ausbezahlten Überstunden eingeben?
Geben Sie die ausbezahlten Überstunden unter Stundenabrechnung ein. Bitte beachten Sie, dass sich die Felder Mehrstunden bis Überstunden Steuerpflichtig 100% auf die Ausbezahlten Stunden bezieht. D.h. bei der Monatsabrechnung werden diese Stunden vom Gleitzeitsaldo abgezogen. Die Gutstunden werden hingegen dazugerechnet.<br>
Im Ausdruck der Monatsabrechnung werden diese Stunden in das Feld Ausbezahlte Stunden summiert und vom Gleitzeitsaldo abgezogen. Sind bei einer Abrechnung nur Gutstunden eingetragen, so werden hier negative ausbezahlte Stunden angeführt, was zur gewünschten Erhöhung des Gleitzeitsaldos führt.

Achte darauf, dass die eingetragenen Stunden mit deinen tatsächlich ausbezahlten Stunden übereinstimmen. Beobachte das über einen längeren Zeitraum (einige Monate) hinweg, wo du alle Überstunden Arten hast und diese auch ausgeschüttet hast, um zu vermeiden dass nach einiger Zeit festgestellt wird, dass diese Zahlen auseinander laufen.<br>
Egal in welche Richtung eine eventuelle Differenz geht, so ist es zumindest peinlich, wenn nicht sogar teuer.

#### Wie verbuche ich die Zeiten, welche während eines Feiertages gearbeitet wurden?
Der Mitarbeiter bucht seine Anwesenheits- und Projektzeiten wie an jedem anderen Tag auch.
In der Monatsabrechnung erhalten Sie neben den Anwesenheitszeiten auch die Feiertagszeiten ausgewiesen. D.h. dem Mitarbeiter werden sowohl die Feiertagsstunden als auch die tatsächlich geleisteten Stunden angerechnet.

![](Pers_Ma1.gif)

![](Pers_Ma2.gif)

Voraussetzungen:
-   Die Feiertage sind entsprechend gepflegt.
-   Für den Wochentag des Feiertages ist laut Zeitmodell eine Sollzeit definiert.

#### Kennt **Kieselstein ERP** alle Feiertage?
Nein. Aufgrund der unterschiedlichsten Handhabung der Feiertage haben wird die Möglichkeit geschaffen die in Ihrem Unternehmen tatsächlich durchgeführten Feiertage manuell einzupflegen. Es sei hier auch auf die unterschiedlichen Feiertage der verschiedenen Religionen verweisen. Die Pflege der Feiertage finden Sie im Modul Personal, unterer Reiter Betriebskalender.
Hier können Sie auch die in Ihrem Unternehmen üblichen Betriebsurlaube usw. hinterlegen. [Siehe auch.](#Betriebskalender)

#### Definition der Tagesarten
<a name="Tagesarten"></a>
Durch Auswahl des unteren Modulreiter Grunddaten, können im oberen Modulreiter Tagesarten ausgewählt werden. Hier können nun zusätzliche Tagesarten definiert werden. Definieren Sie hier, neben den fest verdrahteten Tagesarten (Sonntag bis Samstag und Feiertag), die von Ihnen zusätzlich benötigten Tagesarten wie z.B. Betriebsurlaub, Tage der offenen Tür usw..
Definition der [Tagesart Halbtag siehe bitte](../personal/Urlaubsanspruch.htm#Halbtag).
Bitte beachten Sie, dass die Tagesart Betriebsurlaub nur für Zeitberechnungen der Lieferungen, Los-Durchlaufzeiten und ähnliches verwendet wird. Für die Berechnung der Anwesenheitszeiten der Mitarbeiter wird diese Tagesart ignoriert. Somit können z.B. Urlaub, auch wenn es ein zentral verordneter Betriebsurlaub ist, entsprechend verbucht und abgerechnet werden. Gegebenenfalls nutzen Sie bitte für das Eintragen der Betriebsurlaube auch die Funktion alle Mitarbeiter.
Das bedeutet auch, dass für ein Datum an dem Betriebsurlaub (BU) definiert ist, für die Berechnung der Pausen diese Tagesart ignoriert wird und anstatt dessen die eigentliche Tagesart, z.B. Dienstag, verwendet wird.

##### Betriebskalender
<a name="Betriebskalender"></a>
Mit dem Betriebskalender (unterer Modulreiter) werden die Feiertage und auch Ihre eigenen besonderen [Tagesarten](#Tagesarten) festgelegt.
Wir haben bewusst auf die automatische Berechnung der Feiertage verzichtet, da diese auch in unserem Kulturkreis sehr länderspezifisch ist. Bitte denken Sie daran, die Feiertage rechtzeitig für die Zukunft festzulegen. Zur Vereinfachung finden Sie im Reiter Vorlage jedoch die Möglichkeit gleichbleibende Feiertage jedes Jahres vorzudefinieren.
Der Betriebskalender ist die Definition an welchen Tagen andere Tagesarten als die üblichen Wochentage gelten. Dies wirkt sich wiederum in der Mitarbeiterzeitabrechnung, aufgrund der Zeitmodelle der Mitarbeiter und in den Zutrittsmodellen entsprechend aus.
![](Betriebskalender.gif)
Unter Religion kann eine Bekenntnis-spezifische Beschränkung der Feiertagsdefinition angegeben werden. D.h. hat ein Mitarbeiter aufgrund seiner Religionszugehörigkeit an diesem Tage frei, so muss für diesen Tag die entsprechende Tagesart (z.B. Feiertag) angegeben werden und zusätzlich angeführt werden, dass dies nur für diese Religion gilt. Haben mehrere, aber nicht alle Religionen an diesem Feiertag frei, so kann dies entsprechend oft angegeben werden. Bei der Definition von Feiertagen welche generell gelten, muss die Religionsdefinition frei bleiben. Dies ist insbesondere in Kombination mit den Zutrittsberechtigungen von Bedeutung.

**Hinweis zu Betriebsunterbrechung.**
Die Tagesart Betriebsunterbrechung, BU, dient nur dazu, diese Tage in der Lieferdauer an Ihre Kunden zu berücksichtigen und in der automatischen Ermittlung des Arbeitsgang Beginns diese Unterbrechnung, z.B. für den Betriebsurlaub, zu berücksichtigen.

**Hinweis zu religionsspezifischen Feiertagen im Zutritt:** Bei der Tagesart (z.B. Feiertag) werden für die Zutrittsberechtigung nur jene Definitionen berücksichtigt, die **OHNE** Religionseinschränkung angelegt wurden. D.h. auch Mitarbeiter, die aufgrund Ihres Bekenntnisses einen Feiertag haben, haben Zutritt, so als wenn diese Definition nicht gegeben wäre.

**Definition jährliche wiederholender Feiertage:**

Im Reiter Vorlage können Sie einerseits bestimmte Tage jedes Jahres als Feiertag definieren (Bsp. 1.1\. = Neujahr). Weiters gibt es Feiertage, die sich auf Grund von anderen Tagen berechnen lassen (Christi Himmelfahrt ist immer 39 Tage nach dem Ostersonntag). Nach der Erfassung können Sie mit einem Klick auf den Button ![](feiertage_button.png) (Feiertage für ein bestimmtes Jahr eintragen) die definierten Feiertage nach der Auswahl des Jahres eintragen.

**Hinweis:** Von **Kieselstein ERP** werden die Feiertage Österreichs mitgeliefert - bitte überprüfen Sie, ob diese für Ihr Unternehmen auch zutreffen bevor Sie das neue Jahr eintragen.

![](feiertag.png)

#### Unter Stundenabrechnung sehe ich keine Zeiten
Dieses Reiter ![](Stundenabrechnung.gif) dient zur Erfassung der bereits abgerechneten Zeiten. Vom Grundgedanken her werden hier die bereits ausbezahlten Stunden eingetragen. Es werden also nur die Stunden eingegeben, welche Sie Ihren Mitarbeitern durch Auszahlung oder sonstige Vergütung bezahlt haben. Diese Zeiten reduzieren den Gleitzeitsaldo Ihrer Mitarbeiter.
Eine Sonderstellung nehmen die Gutstunden und die Qualifikationsprämie ein.
![](Stundenabrechnung_Detail.gif)
Die Gutstunden werden dem Mitarbeiter gutgeschrieben. Die Qualifikationsprämie ist ein Geldbetrag welchen Sie Ihrem Mitarbeiter aus besonderen Gründen ausbezahlen.

#### Der Gleitzeitsaldo wird wann aktualisiert?
Unter ![](Gleitzeitsaldo.gif) finden Sie den aktuellen Gleitzeitsaldo des jeweiligen Mitarbeiters.
Dieser wird immer beim Druck einer [Monatsabrechnung](../Zeiterfassung/index.htm#Monatsabrechnung) aktualisiert.
Daraus ergeben sich zwei Forderungen:
a.) Wenn Zeitdaten aus alten Monaten verändert werden, so müssen die Monatsabrechnungen ab diesem Zeitpunkt neu ausgedruckt (aufgerollt) werden. Damit erhalten Sie zugleich auch die entsprechenden Belege für Ihre Mitarbeiter.
b.) In manchen Unternehmen werden Gleitzeitguthaben nur eine gewisse Zeitlang anerkannt oder immer zu einem bestimmten Zeitpunkt (Quartalswechsel) wieder auf Null gesetzt. Daher können diese Daten auch manuell verändert werden. Um nun eine Neuberechnung zu unterbinden, müssen Sie bitte die Checkbox gesperrt (gefroren) anhaken. Ist der Eintrag gefroren, so wird er von **Kieselstein ERP** nicht mehr verändert.
![](Gleitzeitsadlo_Detail.gif)

#### Wie ist der Gleitzeitsaldo zu verstehen?
Der im Personal im Reiter Gleitzeitsaldo angezeigte Wert entspricht den tatsächlich geleisteten bzw. vom Arbeitgeber zu bezahlenden Stunden abzüglich den Sollstunden usw..
Parallel dazu gibt es die verschiedenen Überstundenarten. Diese werden gerne in Normalstunden umgerechnet dargestellt. Addiert man nun beide Werte, so erhält man die z.B. bei einer Kündigung auszubezahlenden Normalstunden. Oder auch die Stunden, die der Mitarbeiter "abfeiern" darf, bis er wieder einen Null-Saldo hat. 

#### Können Zulagen für verschiedene Arbeitsgänge definiert werden?
Ja.

Dazu müssen zuerst die verschiedenen Zulagen definiert werden, um diese dann den verschiedenen Tätigkeiten zuzuordnen.

Für die Definition der Zulagen gehen Sie im Personal, untere Modulreiter Grunddaten. Nun Modulreiter Zulage.

Legen Sie hier nun die in Ihrem Unternehmen gewährten Zulagen an.

Um die Zulage(n) einem Artikel / einer Tätigkeit zuzuordnen wählen Sie im Personal den unteren Modulreiter Artikelzulage.

Definieren Sie hier die zulagenbehafteten Artikel.

Wurde nun eine zulagenbehaftete Tätigkeit in der Zeitbuchung verwendet, so wird dies in der Monatsabrechnung

![](Schmutzzulage.gif)

in einer eigenen Zeile ausgewiesen.

#### Ausweisnummer
Die Ausweisnummer wird für die Verbindung zwischen Personalkarte und Person verwendet. D.h. wenn in deinem Unternehmen z.B. RFID Tags (Mifare, Legic, ....) verwendet werden, so muss die Seriennummer des jeweiligen RFID Tags, der in der Regel 2x4Byte (Zeichen) lang ist, bei der Ausweisnummer eingetragen werden. Beim [Verlust](#was-ist-bei-einer-änderung-der-ausweisnummer-zu-beachten) kann jederzeit ein anderer RFID Tag verwendet werden. D.h. hier die klare Trennung zwischen Personalnummer, welche auch für die Lohnverrechnung wichtig ist und der Identifikation des Mitarbeiters.<br>
Werden nur Barcodes als Ausweis verwendet, so wird üblicherweise eine fortlaufende mindestens dreistellige Zahl oder 1:1 die Personalnummer verwendet.

#### Warum kann nur eine 3-stellige Ausweisnummer angegeben werden?
Wenn in Ihrer **Kieselstein ERP** Installation auch die [Zeiterfassungsstifte](../Zeiterfassung/Zeiterfassungsstifte.htm) verwendet werden, so muss, aus Gründen der Stiftsoftware bzw. Stiftbedienung, die Ausweisnummer exakt dreistellig angegeben werden.
Bitte unterscheide hier klar zwischen der Personalnummer und der Ausweisnummer.

Die Personalnummer sollte mit deiner Lohnverrechnung übereinstimmen, die Ausweisnummer ist die Übersetzung auf die Personalkarten und Gegebenheiten in deinem/euren Unternehmen.

Es können auch alphanumerische Personalnummern vergeben. Bitte definiere im Mandantenparameter PERSONALNUMMER_ZEICHENSATZ, die verwendbaren Zeichen.

Hinweis:<br>
Der [Andruck der Barcodeetiketten](#Personalliste) erfolgt mit der lesbaren Personalnummer, im Barcode ist jedoch die Ausweisnummer enthalten.

![](Personal_Barcode.gif) Nachdem einige Anwender auch immer nur nach Ausweis suchen, sei hier der Hinweis dass dies meistens mit Ausweisnummer beschrieben ist.

#### Was ist bei einer Änderung der Ausweisnummer zu beachten?
Werden Ausweisnummern geändert, so muss unbedingt darauf geachtet werden, dass vor dieser Änderung die Daten aller Offline-Geräte, welche ja nur die Ausweisnummer abspeichern können, in Ihre **Kieselstein ERP** Datenbank übertragen haben. Erst danach darf die Ausweisnummer geändert werden. Bitte denken Sie gegebenenfalls auch an die Offline fähigen [Zeiterfassungsterminals 5.6](../Zeiterfassung/Zeiterfassungsterminal_5_6.htm) und [Nettop](../Zeiterfassung/Zeiterfassungsterminal_Nettop.htm#Aenderung_der_Ausweisnummer).

#### Beim Druck der Personalliste inklusive Barcodes kommt eine Fehlermeldung
Wenn beim Druck der Barcodeliste ein Fehlermeldung ähnlich 

*Source text : it.businesslogic.ireport.barcode.BcImage.getBarcodeImage(9,"$P"+$F{Ausweisnummer},false,false,null,0,0)* erscheint, so ist in einer der Ausweisnummern ein im Barcode nicht darstellbares Zeichen enthalten.

Zur Behebung drucken Sie bitte die Personalliste (ohne Barcode) aus und überprüfen Sie die unter Ausweisnummer angegebenen Einträge. Für gültige Barcodezeichen siehe [bitte](../System/barcodes.htm#Von den Barcodes unterstützte Zeichen).

#### Beim Druck der Personalliste inklusive Barcodes fehlt der Barcode eines Mitarbeiters
Bitte tragen Sie eine Ausweisnummer (unter Details) ein.

#### Geburtsdatum liegt vor 1970
Wenn Sie einen Mitarbeiter haben, der vor 1970 geboren wurde, geben Sie sein Geburtsdatum manuell in das Textfeld ein z.B. 24.11.1964.

#### Welche Zutrittsklasse wird angezeigt, wenn beim Personal-Journal kein Stichtag angegeben ist?
Hier wird die Zutrittsklasse des Ausdruckdatums angezeigt, also die jetzt gültige Zutrittsklasse.

#### Drucken der Personalliste
<a name="Personalliste"></a>
Die Personalliste gibt ihnen eine Übersicht über alle Mitarbeiter des gewählten Mandanten.

![](personalliste.jpg)

Hier stehen verschiedene Auswahlmöglichkeiten / Auswertungen zur Verfügung:

Sortierung: Wählen Sie die gewünschte Sortierung

Stichtag: Ist dieser nicht angehakt, so werden alle Mitarbeiter, nur in Abhängigkeit von versteckt aufgeführt. Ist Stichtag angehakt, so werden diejenigen Mitarbeiter angeführt, welche zum Stichtag im Unternehmen eingetreten sind, also ein Eintrittsdatum eingetragen haben und zum Zeitpunkt noch nicht ausgetreten sind.

Plus Versteckte: Es können Mitarbeiter als versteckt gekennzeichnet werden. Wenn Sie die Berechtigung besitzen versteckte Mitarbeiter auch anzuzeigen, so können Sie diese mit ausdrucken lassen.

Barcodeliste: Ausdruck einer Liste mit den Barcodes der Mitarbeiterausweisnummern für die Zeiterfassungsbuchungen mittels Barcodescanner / Zeiterfassungsstifte.

**Hinweis:** Die angedruckten Zutrittsklassen werden anhand des Stichtages bestimmt. Ist kein Stichtag angegeben, so wird die Zutrittsklasse zum aktuellen Datum, also dem Datum des Ausdruckes, ermittelt.

#### Wie soll ich bei der Einführung der Zeiterfassung vorgehen, die alten Daten ins **Kieselstein ERP** bringen?
[Siehe bitte.]( {{<relref "/fertigung/zeiterfassung/erster_einsatz">}} )

#### Wie definiere ich Zeitmodelle richtig?
[Siehe bitte.]( {{<relref zeitmodell_definition >}} )

Beachten Sie dazu bitte auch, dass die Sollstunden für die Berechnung der 50%igen Überstunden anhand des Zeitmodells zum 1.des jeweiligen Monates verwendet werden. D.h. auch wenn ein Mitarbeiter z.B. zum 2.5\. eintritt, sollte die Gültigkeit seines Zeitmodells bereits mit 1.1\. definiert sein.

#### In der Monatsabrechnung werden die Sollstunden nicht aufsummiert?
Das hängt unter Umständen vom Zeitmodell ab. Siehe dazu bitte [Sollstunden pro Monat](Zeitmodell_Definition.htm#Sollstunden pro Monat) in der Zeitmodelldefinition und beachten Sie, dass diese Berechnung immer für das Zeitmodell des 1\. des Abrechnungsmonates gilt.

#### Kann ich Bemerkungen zu Personen ablegen?
Ja. Im Personalmodul im Menüpunkt Bearbeiten, Kommentar kann ein eigenständiger Kommentar für diese Person hinterlegt werden. Dieser Kommentar ist vom Partner aus nicht sichtbar, damit hier wirkliche Interna abgelegt werden können.

#### Wo werden Kurzzeichen definiert?
Im Personalmodul, Detail, sollte für jede Person ein Kurzzeichen eingetragen sein.

Die Definition des Kurzzeichens kann bis zu dreistellig sein.

Das Kurzzeichen wird bei den Ausdrucken und vor allem auch bei allen Statusanzeigen des Änderns, Neuanlegens verwendet. Eine Änderung eines Kurzzeichens wirkt sofort in allen angezeigten Daten.

In der Auswahlliste des Personalstammes werden auch die Kurzzeichen angezeigt. D.h. durch Klick auf die Spalte haben Sie eine entsprechende Sortierung und damit eine Übersicht über alle vergebenen Kurzzeichen.

![](Kurzzeichen.gif)

#### Verfügbarkeit
<a name="Verfügbarkeit"></a>
Gerade für die Planung des Mitarbeitereinsatzes / stimmen die Qualifikationen meiner Mitarbeiter mit meinen Aufgaben / erforderlichen Arbeiten überein, ist es erforderlich zu definieren, welcher Mitarbeiter welche Tätigkeiten ausführt und zu wieviel Prozent seiner Anwesenheitszeit er diese Tätigkeit ausführt.
Daraus können, unter Berücksichtigung der geplanten Abwesenheiten wie Urlaub, Krank, Zeitausgleich, usw. die verfügbaren Stunden je Mitarbeiter und Tag berechnet werden.
Siehe dazu z.B. auch [Auslastungsvorschau]( {{<relref "/fertigung/losverwaltung/planung_auswertung">}} ).
Bei der Zuweisung der einzelnen Tätigkeiten, also Arbeitszeitartikel, achten Sie bitte darauf, dass wenn 100% Verfügbarkeit erreicht sind, keine Änderungen mehr vorgenommen werden können, sondern die letzte / eine Tätigkeit gelöscht werden muss und erst danach wieder eine weitere Tätigkeit eingefügt werden kann.

Die Verfügbarkeit kann auch genutzt werden, um die Arbeitszeitartikel Personen zuzuordnen und so zu steuern, wer welche Arbeiten verrichten kann und darf.
Wenn in der Zeiterfassung auf Belege gebucht wird und bei Klick auf den Startknopf (z.B. Modul Auftrag) wird, wenn der Parameter ARBEITSZEITARTIKEL_AUS_PERSONALVERFUEGBARKEIT=1, wird als Arbeitszeit der Artikel mit der höchsten Verfügbarkeit vorbesetzt. Wenn keine Verfügbarkeit definiert ist, dann greift der Parameter DEFAULT_ARBEITSZEITARTIKEL, somit wird der dort hinterlegte Arbeitszeitartikel verwendet.

### Personalkosten in den Nachkalkulationen
<a name="Personalkosten in den Nachkalkulationen"></a>
Für die Kosten der Tätigkeiten in den Nachkalkulationen stehen drei verschiedene Betrachtungen der Ist-Zeiten zur Verfügung.
Diese kann mit dem Parameter: PERSONALKOSTEN_QUELLE bestimmt werden. Es stehen dafür folgende Möglichkeiten zur Verfügung:

| Wert | Bedeutung |
| --- |  --- |
| 0 | Es wird der Gestehungspreis der Tätigkeit herangezogen |
| 1 | Es wird der Kostensatz der Personengruppe verwendet |
| 2 | Es wird der unter Personalgehalt definierte Stundensatz verwendet. |

Bitte beachten Sie, dass die Berechnung der Kosten der Sollzeiten **immer** anhand der bei der entsprechenden Tätigkeit hinterlegten Gestehungspreise basiert. Zu Personalkosten siehe bitte auch [Gehalts-/Lohnkosten Definition](#gehalts-lohnkosten-definition).

### Artikelzuschlag
<a name="Artikelzuschlag"></a>
Wenn für die Erfassung der Ist-Kosten die Personalkostenquellen 1 (Gruppe) bzw. 2 (Stundensatz der Person) verwendet werden, so gibt es immer wieder verschiedene Tätigkeiten, bei denen zwar kein Material als Verbrauch gebucht wird, die aber trotzdem höhere Kosten verursachen als die üblichen Tätigkeiten der Personen. So arbeitet ein Mitarbeiter einerseits auf einem Handarbeitsplatz und andererseits wechselt er auf den Arbeitsplatz zum Schweissen. Wenn man nun nicht detailliert die Maschinen buchen will / kann, so ist es doch so, dass es unterschiedliche Kostensätze sind, die in der Kostenbetrachtung herangezogen werden müssen.
Dafür haben wir den Artikelzuschlag geschaffen.
D.h. hier werden für bestimmte Tätigkeiten, ab einem gewissen Stichtag (gültig ab), zusätzliche Kosten pro Stunde zur Mitarbeiter-Lohnmittelstunde hinzugerechnet.
Dieser Zuschlag wird nur bei der Kalkulation der Ist-Zeiten berücksichtigt.

#### Leistungswert
<a name="Leistungswert"></a>
Im Modul Personal finden Sie im oberen Modulreiter Gehalt auch den Leistungswert in %.
Dieser Faktor gibt an, mit welchem Leistungswert Sie die vom diesem Mitarbeiter geleisteten Stunden tatsächlich berücksichtigen wollen. Dies ist z.B. für Lehrlinge (Azubis) von Bedeutung. Im ersten Lehrjahr hat ein Lehrling / Azubi z.B. einen Leistungswert von 30%. Das bedeutet, dass er für eine Tätigkeit praktisch drei mal so lange brauchen darf wie ein entsprechend ausgebildeter Mitarbeiter.
Bei Einstellung des Parameters PERSONALKOSTEN_QUELLE = 0 (Gestpreis der Tätigkeit) wird der jeweilige Leistungswert in den folgenden Nachkalkulationen entsprechend berücksichtigt. Ist der Leistungswert nicht definiert, so wird er mit 100% angenommen.

-   [Los-Ablieferungsberechnung](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [Los-Nachkalkulation](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [Losstatistik](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [IstStd.Anzeige des Loses](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [Los-Monatsauswertung](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [Auftragsnachkalkulation](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [Zeiterfassung Journal](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

-   [Zeiterfassung Arbeitszeitstatistik](Los-AblieferungsberechnungLos-NachkalkulationLosstatistikIstStd.Anzeige%20des%20LosesLos-MonatsauswertungAuftragsnachkalkulationZE,%20Journal,%20AZ-Statistik)

Beachten Sie bei der Definition des Leistungswertes für verschiedene Monate, dass damit auch die Stundensatz-Definition für das jeweilige Monat vorgenommen wird. D.h. die Stundensätze sind ebenfalls mit dem Leistungsfaktor einzugeben.

#### Personalgruppe
<a name="Personalgruppe"></a>
Unter Daten finden Sie auch die Personalgruppe. Diese dient dazu, Ihre Produktions-Mitarbeiter in diese Gruppen einzuteilen und damit die entsprechenden Kostenfaktoren/Stundensätze für die Berechnung des Wertes der Istzeiten zu definieren.
Die Personalgruppen können im unteren Modulreiter Personalgruppe definiert werden. Legen Sie hier auch die Sätze für die sogenannte Lohnmittelstunde der jeweiligen Personalgruppe fest.

Kollektivvertrag / Tarifvertrag

[Siehe bitte Überstundenabrechnung.]( {{<relref "/fertigung/zeiterfassung/ueberstundenabrechnung">}} )

Aktive und passive Reisezeiten

[Siehe bitte.]( {{<relref "/fertigung/zeiterfassung/reisezeiten">}} )

Bereitschaftsverwaltung

Im Modul Personal finden Sie in den unteren Modulreitern auch den Reiter Bereitschaft.
Damit können Sie die Bereitschaftsstunden Ihrer Mitarbeiter entsprechend erfassen. In der Monatsabrechnung wird bei Bereitschaftsstunden die Bereitschaft als Anhang mit aufgeführt. Grundsätzlich wird eine Bereitschaft durch eine Anwesenheit unterbrochen.
Im Personal im Reiter Bereitschaft werden Bereitschaftsarten und die Bereitschaftsstunden je Tagesart definiert.
Die Bereitschaftsdauer kann grundsätzlich von 00:00 bis zu einer Uhrzeit des Tages oder als gesamter Tag definiert werden.

Die tatsächliche Bereitschaft wird im Modul Zeiterfassung im oberen Reiter Bereitschaft für den jeweiligen Mitarbeiter eingetragen. Da Bereitschaften sich an den Übergabetagen üblicherweise überschneiden, müssen Beginn und Ende mit Uhrzeit angegeben werden.
Die grundsätzliche Idee ist, dass beim Mitarbeiter in der Zeiterfassung definiert wird, von wann bis wann der Mitarbeiter welche Bereitschaftsart hat. Die tatsächliche Bereitschaft, also zu welcher Uhrzeit, an welchem Tag, wird durch die Bereitschaftsart definiert.
Da es immer wieder vorkommt, dass man gerne eine kurze Begründung für eine Bereitschaft angeben möchte kann bei jeder Bereitschaftsdefinition beim Mitarbeiter auch ein Kommentar mit angegeben werden, welcher dann bei der Monatsabrechnung mit angedruckt wird.

**Hinweis:** Sollte wider erwarten eine Bereitschaft tatsächlich um 24:00 enden, so tragen Sie bitte als Ende 00:00 des darauf folgenden Tages ein. Technisch gesehen gibt es 24:00 nicht und 23:59 würde dem Mitarbeiter 0,02Std seiner Bereitschaft abziehen.

### Gehalts-/Lohnkosten Definition
<a name="Gehalts-/Lohnkosten Definition."></a>
Es stehen für die  Erfassung / Berechnung des kalkulatorischen Lohnmittelstundensatzes zwei Versionen zur Verfügung.
- a.) Beaufschlagung des eingetragenen Lohnmittelstundensatzes mit einem Faktor
- b.) Errechnung des Lohnmittelstundensatzes aus den Brutto-Brutto Lohnkosten

Die Unterscheidung welche Berechnung verwendet wird, wird durch den Parameter LOHNSTUNDENSATZKALKULATION_KALENDERWOCHEN definiert. Steht dieser auf 0, so wird nur die Beaufschlagung des eingetragenen Lohnmittelstundensatzes mit einem Faktor für die Bestimmung des kalkulatorischen Lohnmittelstundensatzes verwendet. In diesem Falle dienen die Felder des monatlichen Gehaltes Ihrer Information. 

Hinter beiden Berechnungen steckt auch die Idee, dass eine größtmögliche Transparenz erzielt, ersichtlich, sein sollte.

**Beaufschlagung des eingetragenen Lohnmittelstundensatzes mit einem Faktor**

Bitte beachten Sie hier die Position des Radio-Buttons. Steht dieser bei Faktor, so wird für die Berechnung der Faktor verwendet. Steht dieser bei Kalk.Stundensatz, so wurde dieser manuell eingegeben und der Faktor wird nur zu Ihrer Information angezeigt.

#### Errechnung des Lohnmittelstundensatzes aus den Brutto-Brutto Lohnkosten
<a name="Lohnkosten / Lohnmittelstundensatz"></a>
Mit dieser Funktion wird aus den Brutto-Brutto Gehaltskosten der kalkulatorische Lohnmittelstundensatz errechnet, unter der Annahme von Kalenderwochen, Feiertagswochen, Krankenstandswochen, sowie der Angabe der zusätzlichen monatlichen Sonderzahlungen (die ja doch in jedem Land / Unternehmen anders gehandhabt werden). 

Die Berechnung des Lohnmittelstundensatz aus den monatlichen Brutto-Brutto Lohnkosten erfolgt nach folgender Formel:

Brutto-Brutto-Lohnkosten * (12+Sonderzahlungen) / theoretische Jahres-Ist-Stunden

Die Berechnung der theoretischen Jahres-Ist-Stunden erfolgt nach folgender Formel:

(52Kalenderwochen -- Urlaubswochen -- Krankenwochen -- Feiertagswochen) * Summe der Wochensollstunden = 
theoretische Jahres-Ist-Stunden.

Die Basis für die Sollstunden pro Woche sind die Sollstunden aus dem Zeitmodell welches zum 1\. des ausgewählten Monates gültig ist.

Es ist dies die vereinfachte Berechnung der voraussichtlichen Jahres-Ist-Stunden, mit einer Schwankungsbreite von ca. 1-2Tagen pro Jahr (wegen Feiertagen), was für die Kalkulation ausreichend genau ist. Es wird daher auch die Wochenbetrachtung für diese Berechnung verwendet.

Um dies zu erreichen müssen folgende Parameter eingestellt werden:

| Parameter | Beschreibung | empfohlener Wert |
| --- |  --- |  --- |
| LOHNSTUNDENSATZKALKULATION_KALENDERWOCHEN | Anzahl der Wochen eines Kalenderjahres | 52 |
| LOHNSTUNDENSATZKALKULATION_FEIERTAGSWOCHEN | Anzahl der Feiertage eines Kalenderjahres umgelegt auf Kalenderwochen  | 2 in A |
| LOHNSTUNDENSATZKALKULATION_KRANKWOCHEN | Anzahl der Kranktage eines Kalenderjahres umgelegt auf Kalenderwochen  | 1 |
| LOHNSTUNDENSATZKALKULATION_SONDERZAHLUNGEN | Anzahl der Sonderzahlungen auf Monatsbasis pro Jahr | 2 in A (oder mehr)0,1,2 in D |

Als zusätzlicher Wert wird die Anzahl der Urlaubswochen aus dem Urlaubsanspruch (Jahresanspruch in Wochen) verwendet. Somit ergibt sich, bei obiger Einstellung, für einen Mitarbeiter mit einer 38,5Std Woche an theoretischen Jahres-Ist-Stunden:

(52 - 5 - 1 - 2) * 38,5 = 1.694 Std.

Die Abweichung gegenüber dem tatsächlichen Wert kann im Modul Zeiterfassung, Info, Zeitsaldo überprüft werden. Passen Sie die Daten gegebenenfalls entsprechend an.

![](Kalk_Ist_Jahresstunden.gif)

Somit ergibt sich (Jänner 2014 in Österreich) für ein Bruttogehalt von 2.000,- € ein Lohnmittelstundensatz von 21,70 € und mit einem Korrekturfaktor, z.B. für Stillstandszeiten, Verteilungs- und Ausbildungszeiten und ähnlichem von 20% ein kalkulatorischer Stundensatz von 26,04 €, von denen der Mitarbeiter ca. 8,65€ netto ausbezahlt bekommt (auch während seines Urlaubs usw.)

Info: An der Anzeige des Feldes der Kalk.Ist-Jahresstunden sehen Sie, dass die Berechnung anhand des Brutto-Brutto-Gehaltes und den Kalk.Ist-Jahresstunden erfolgt.

Wird nun mit Neu ein weiterer Datensatz zu einem anderen Monat angelegt, so werden die Daten der jüngsten Gehaltsdefinition als Vorschlagswert übernommen.

In der Anzeige werden die Kalk.Ist-Jahresstunden in Rot dargestellt, wenn die bei der Erfassung gespeicherten Stunden mit den nun aktuell berechneten Stunden nicht übereinstimmen. Sollten diese aktualisiert werden, so kann durch Ändern und Speichern der nun gültige Wert übernommen werden.

Bitte beachten Sie, dass eine Änderung des Kalk.Stundensatzes in den Berechnungen, z.B. Ablieferwert der Lose, erst durch eine erneute Nachkalkulation gegebenenfalls aktualisiert wird.

#### Die Kalk.Ist-Jahresstunden werden nicht angezeigt / in Rot angezeigt
Dies bedeutet, dass die Berechnung noch nicht durchgeführt wurde. Um diese durchzuführen, klicken Sie bitte auf ändern. Dadurch werden für das entsprechende Jahr / Monat die Kalkulatorischen Jahresstunden aus den aktuellen Zeitmodellen errechnet und bei dieser Buchung hinterlegt.

Werden die Kalkulatorischen Ist-Jahresstunden in Rot angezeigt, so hat sich im Hintergrund die Zeitbasis geändert. Das bedeutet, dass diese neu errechnet werden müssen. Auch hier genügt ein Klick auf Ändern um diese neu zu berechnen.

{{% alert title="Wichtig" color="warning" %}}
Damit dies grundsätzlich startet, muss nach der Definition der Parameter der Client neu gestartet werden.
{{% /alert %}}

#### Warum wird dies nicht automatisch gemacht ?
Da dies ja die Basis für Ihre Nachkalkulationen, vor allem für den DBII ist, darf diese Neuberechnung nicht ohne Ihrem Wissen durchgeführt werden.

#### Sind die Gehaltszahlen in der Datenbank sichtbar ?
Ja. Aus Gründen der Transparenz und Nachvollziehbarkeit werden auch die Gehaltszahlen lesbar in der Datenbank gespeichert. Bitte beachten Sie dies, wenn durch Administratoren direkte Zugriffe auf die Datenbank möglich sind.

#### Definitionen für die Liquiditätsvorschau
Um auch die Aufwendungen für die Personalkosten in der Liquiditätsvorschau darstellen zu können, wurden folgende Funktionalitäten mit implementiert:
| Feld | Wirkung |
| --- | --- |
| Gehalt Netto | Wird zum Stichtag Netto fällig |
| Lohnsteuer | Wird zum Stichtag Lohnsteuer fällig |
| Sozialabgaben | Wird zum Stichtag Sozialabgaben fällig |

Diese Daten können im Personal, Grunddaten, Zahltag eingegeben werden.
Die Sozialabgaben errechnen sich aus dem Gehalt BruttoBrutto (also Ihrem gesamten Gehaltsaufwand) abzüglich dem Nettogehalt und abzüglich den Sozialabgaben. Da in Österreich die Sozialabgaben gemeinsam mit der Lohnsteuer fällig werden, kann die Erfassung der Lohnsteuer auch entfallen. In diesem Falle würden die gesamten Abgaben in der Liquiditätsvorschau unter Sozialabgaben angeführt werden.
![](Personal_Zahltage.gif)
Bei der Definition der Fälligkeit des Nettogehaltes gilt die erste Zeile (nach dem Monat). Hier können, wenn Monatsletzter angehakt, die Tage angegeben zu denen das Nettogehalt vorfällig eingeplant werden sollte. Bei den Banktagen werden Feiertage und Wochenende (Sa, So) berücksichtigt.
Sollten Gehälter z.B. bereits zur Monatsmitte bezahlt werden, so bitte den Haken bei Monatsletzter entfernen und den Stichtag für das Nettogehalt mit angeben.
Stichtag Lohnsteuer. Hier wird in A üblicherweise der 15\. für das Folgemonat eingegeben. Zu diesem Termin wird die Bezahlung der Summe der Lohnsteuer aller im Bezugsmonat (November) eingetretenen Mitarbeiter in die Liquiditätsvorschau übernommen.
Stichtag Sozialabgaben. Zu diesem Termin wird die Bezahlung der Sozialabgaben aller im Bezugsmonat eingetretenen Mitarbeiter in die Liquiditätsvorschau übernommen.
Werden bei Stichtag Lohnsteuer bzw. Stichtag Sozialabgaben Negative Tage eingegeben, so wird die Fälligkeit unter Berücksichtigung von Wochenenden und Feiertagen um diese Tage in Bezug zum Monatsletzten errechnet.
Faktor. Die angeführten Beträge (Brutto, Netto, ...) werden mit dem angeführten Faktor multipliziert um auch die Zahlung der Urlaubs- und Weihnachtsgelder (Remureneration) darstellen zu können.
Hinweis:
Bei den Gehältern wird keine eventuelle Aliquotierung errechnet. D.h. sollte ein Mitarbeiter während des Monates Ein- bzw. Aus-treten, so wird trotzdem das für den Monat eingetragene Gehalt in seiner vollen Höhe in die Liquiditätsvorschau übernommen.
Übliche Einstellungen:
Österreich: Bei der Fälligkeit der Nettogehälter -1 und Monatsletzter angehakt (ausgenommen Landesbedienstete/Beamte)
Bei Stichtag Lohnsteuer und Sozialabgaben 15
Faktor 1, und für Juni und November 2 für das 13\. & 14\. Gehalt
Deutschland: Bei der Fälligkeit der Nettogehälter -1 und Monatsletzter angehakt
Bei Stichtag Lohnsteuer 10
Bei Stichtag Sozialabgaben -5
Faktor: In der Regel für alle 12Monate 1

#### Wie wirkt die Überstundenpauschale?
Im Reiter Gehalt des Personals kann auch eine Überstundenpauschale definiert werden.
Dieser Wert bewirkt, dass ab seiner Gültigkeit auf der Monatsabrechnung Überstunden bis zu den angegebenen Stunden als Überstunden abgezogen werden.
Bitte beachten Sie, dass ein nicht Erreichen der Überstundenpauschale gemäß den gesetzlichen Vorgaben, keine Reduktion, keinen negativen Gleitzeitsaldo bewirkt.
Beispiel:
Überstundenpauschale: 10Std
Gleitzeitsaldo für März:   9Std -> bewirkt dass vom Gleitzeitsaldo nur 9Std abgezogen werden, die Gleitzeitstunden gehen NICHT ins Minus
Gleitzeitsaldo für April:  12Std -> bewirkt dass der Gleitzeitsaldo um 10Std reduziert = Überstundenpauschale wird. Die Gleitzeitstunden gehen mit 2Std. ins Plus.

#### Wie wirkt die DRZ-Pauschale?
Im Reiter Gehalt des Personals kann auch eine DRZ-Pauschale definiert werden. DRZ = Durchrechnungszeitraum.
Das bedeutet: In immer mehr Unternehmen werden die Anwesenheitszeiten der Mitarbeiter auf z.B. Jahresdurchrechnungszeiten gerechnet, insbesondere im Zusammenhang mit sogenannten All-In-Verträgen. Daraus ergibt sich, dass der Mitarbeiter eine Überstundenpauschale hat, welche in beiden Richtungen wirkt. Das heisst also, dass zu seinem Wochen-Stunden-Soll in der Monatsabrechnung noch die unter DRZ-Pauschale definierten Stunden hinzugerechnet werden. Also seine Sollzeit für das Monat z.B. um 10Stunden höher ist. Daraus ergibt sich auch, dass der Gleitzeitsaldo auch nur aufgrund der DRZ-Pauschale negativ werden kann, was bei der Überstundenpauschale nicht ist.
Bitte beachten Sie dass dies üblicherweise für einen Durchrechnungszeitraum von 53Wochen gemacht wird. Danach ist der aktuelle Saldo anhand der rechtlichen Gegebenheiten auszugleichen. Dieser Ausgleich muss bitte manuell, im Reiter Gleitzeitsaldo gemacht werden. D.h. in der Regel wird nach der Abrechnung ein eventuell negativer Saldo auf Null gestellt. Denken Sie daran den Haken bei gefroren zu setzen.
In der Monatsabrechnung finden Sie die Pauschale des Durchrechnungszeitraumes unter dem Feiertagssoll angeführt.

#### BCC
Im Reiter Parameter kann eine BCC (Blind Carbon Copy) EMail Adresse angegeben werden. Das bewirkt, dass jedes ausgehende EMail dieser Person, also dieses **Kieselstein ERP** Benutzers auch an die BCC Adresse gesandt wird.
Ist eine BCC Adresse hinterlegt, so wird dies, beim Versand als EMail rechts neben der CC-Adresse angezeigt.

#### Abwesenheitsarten
[Siehe bitte](../Zeiterfassung/index.htm#Abwesenheitsarten).

#### Definition von Fahrzeugen
[Siehe bitte](../Zeiterfassung/Reisezeiten.htm#Definition von Fahrzeugen).....daten sind üblicherweise
