---
title: "Zeitmodelle"
linkTitle: "Zeitmodelle"
categories: ["Personal"]
tags: ["Personal", "Zeitmodell"]
weight: 100
description: >
  Definition der Zeitmodelle
---
Zeitmodell Definition
=====================

Wie geht man bei der Definition der Zeitmodelle vor, was ist zu beachten ?

Mit den Zeitmodellen wird die Verwendung der Anwesenheitszeiten definiert.

Für die Definition der Zeitmodell gehen Sie wie folgt vor.

{{% alert title="Ein Zeitmodell wirkt für den gesamten Erfassungszeitraum" color="primary" %}}
Zeitmodelle wirken immer auch in die Vergangenheit. Das bedeutet: Wenn Änderungen an Zeitmodellen vorgenommen werden, so gelten diese Änderungen für alle in **Kieselstein ERP** gebuchten Daten, die sich auf dieses Zeitmodell beziehen. Werden also nach Änderungen an Zeitmodellen vor allem, aber nicht nur Monatsabrechnungen ausgedruckt, so können, aufgrund der geänderten Zeitmodellvorgaben andere Werte / Gleitzeitsalden errechnet werden.
{{% /alert %}}


{{% alert title="Pausen müssen nachgezogen werden" color="primary" %}}
Werden automatische Pausen verändert, so werden diese Änderungen nicht automatisch in den alten Daten nachgezogen.

Wenn diese nachgezogen werden sollten, so verwende bitte Pflege, Automatikbuchungen in der Zeiterfassung.
{{% /alert %}}

## Definition eines Zeitmodells.

In der Personalverwaltung wählen Sie den unteren Modulreiter Zeitmodelle definieren.

Mit neu erzeugen Sie ein neues Zeitmodell.

![](Zeitmodell_Kopf_Definition.gif)

In der Kennung geben Sie einen Kurzbegriff für das Zeitmodell an und in der Bezeichnung geben Sie die für Sie eindeutig sprechende Bezeichnung des Zeitmodells an. Mit Speichern wird das Zeitmodell erzeugt.

| Feld | Bedeutung |
| --- |  --- |
| Teilzeitmodell | Wird hier ein Haken gesetzt, so werden bei den Saldendarstellungen an den Terminals der Urlaubsanspruch in Stunden anstatt in Tagen ausgegeben. |
| Versteckt | Dieses Zeitmodell sollte in der üblichen Liste der Zeitmodelle nicht mehr aufscheinen |
| Urlaubstage pro Woche | Definieren Sie hier, für die Berechnung des tatsächlichen Urlaubsanspruches, wieviele Urlaubstage = Arbeitstage in diesem Zeitmodell hinterlegt sind. Warum ist diese Hinterlegung erforderlich? Es gibt so manche Konstellation bei der Mitarbeiter auch außerhalb der üblichen 5Tage Woche arbeiten und deren Anspruch trotzdem Wochenweise geregelt ist. Denken Sie dabei z.B. an Kellner, Greenkeeper usw. |
| <a name="Sollstunden pro Monat"></a>Sollstunden pro Monat | Wird hier ein Wert eingetragen, so basiert die Monatsabrechnung für dieses Zeitmodell immer auf den angegebenen Sollstunden, anstelle der automatisch errechneten Sollstunden aufgrund der Kalendertage.Verwenden Sie diese Definition nur, wenn Ihre Verträge mit den Mitarbeitern eine Vereinbarung enthalten, dass für jedes Monat xx Std. (z.B. 174) als Sollstunden zu leisten sind.Beachten Sie bitte, dass bei Verwendung dieser Definition, ein eventuelle Zeitmodellwechsel immer zum 1\. des Monates erfolgen muss. |
| Dynamisch Pause | Wird ein Zeitmodell mit dynamischen Pausen definiert, so wirken die automatischen Pausen ab in die Zukunft.Bitte beachten Sie, dass bei der Buchung der Pausen auf Buchungen die innerhalb der errechneten Pausen liegen, derzeit keinerlei Rücksicht genommen wird. |
| Täglicher Zeitabzug | Der hier eingetragene Wert wird jeden Tag, an dem Zeitbuchungen sind als Minutenfaktor von der gebuchten Istzeit beim ersten Kommt abgezogen. Achten Sie bei Einträgen hier ebenfalls auf die Erfordernis der Vereinbarung mit Ihren Mitarbeitern. |
| Maximale Wochen- Iststunden | Der hier eingetragene Wert bewirkt, dass die maximale Anwesenheitszeit nicht überschritten werden kann. Eine entsprechende Fehlermeldung wird am letzten Tag mit Zeitbuchungen der Woche angezeigt.<br>Hinweis: Die mehr gearbeiteten Stunden sind durch einen Vergleich des Zeitdatenjournal (Monatssumme) mit der Monatsabrechnung ersichtlich.|
| Maximal erlaubte Mehrzeit | Die maximale erlaubte Mehrzeit beschränkt den verbleibenden Gleitzeitsaldo auf den eingetragenen Wert, auch monatsübergreifend.Wird dieser Wert überschritten, so wird der Gleitzeitsaldo auf diesen Maximalwert gesetzt und die Differenz bei den Überstunden abgezogen. |
| Fixe Pausen trotz Kommt/Geht eintragen | Entgegen der üblichen Regelung sollten die fixen Pausen auch wirken, wenn der Mitarbeiter z.B. später kommt oder früher Geht.D.h. wenn z.B. die fixe Pause des Tages von 9:00-9:30 ist und der Mitarbeiter kommt erst um 9:15, so geht er als erstes in die Pause und beginnt seine Arbeit eigentlich erst um 9:30. |
| ZeitgutschriftKommt / Geht | Der Mitarbeiter bekommt für jeden anwesenden Tag für das erste Kommt bzw. das letzte Geht eine entsprechende Zeitgutschrift zu seiner Kommt bzw. Gehtzeit hinzugerechnet. Siehe auch [Gutzeiten pro Tag](../Zeiterfassung/index.htm#Gutzeiten pro Tag). |
| Feiertagssoll addieren | Ist dies nicht angehakt, so werden die Sollzeiten des Feiertags als Istzeiten betrachtet. |
| Schicht | Definition welche Schichtzuschläge gegebenenfalls für dieses Zeitmodell anzuwenden ist. [Siehe](../Zeiterfassung/index.htm#Schichtzuschlaege definieren). |
| Firmenzeitmodell | Ist das Zeitmodell welches für Ihr Unternehmen für die automatische Berechnung der Los-Durchlaufzeiten verwendet wird. Siehe dazu bitte [auch]
( {{<relref "/fertigung/losverwaltung/#kann-der-losbeginn-automatisch-errechnet-werden">}} ) und [auch Auftrag]( {{<relref "/verkauf/auftrag/#wie-wird-der-liefertermin-des-auftrages-errechnet">}} ). |
| unproduktive Zeit als Pause buchen | Wenn angehakt, werden alle unproduktiven Zeiten zwischen Belegen bzw. zwischen Pausen als Pause eingebucht. |
| Feiertag am nächsten Tag | Wird nur für die dritte Schicht verwendet und wenn eigentlich das Schichtmodell um einen Tag versetzt ist.Beispiel: Üblicherweise beginnt in der dritten Schicht, der Montag am Sonntag um 22:00\. Somit beginnt auch der Feiertag am Vortag um 22:00. In manchen Unternehmen ist es aber ersessenes Recht, dass die dritte Schicht des Montags erst am Montag um 22:00 beginnt. Somit verschiebt sich die Feiertagsbetrachtung um einen Tag.Man könnte auch sagen: Üblich ist, der Tag an dem die Nachschicht endet. Mit dem Haken bei Feiertag am nächsten Tag, ist es der Tag an dem die Nachtschicht beginnt. Somit ergibt sich eben eine verschobene Darstellung für den Feiertag.![](Feiertag_am_naechsten_Tag.gif) |

Nachdem Sie die Details des Zeitmodells definiert haben, müssen nun auch die Tagesdefinitionen für jede Tagesart festgelegt werden. Dazu wählen Sie im oberen Modulreiter Zeitmodelltag.
Mit Neu fügen Sie die Tage hinzu. Achten Sie darauf alle Tage an denen gearbeitet werden könnte zu definieren. Denken Sie hier auch an die zusätzlich definierten Tagesarten.

Für jeden Tag können folgende Werte definiert werden.
![](Zeitmodell1.gif)

| Feld | Bedeutung |
| --- |  --- |
| Frühest erlaubte Kommt-Zeit | Ab dieser Uhrzeit wird eine Kommt Buchung akzeptiert. Bucht ein Mitarbeiter vor dieser Zeit, so wird seine Anwesenheitszeit erst ab der eingetragenen Uhrzeit gerechnet. |
| Vortag  | Die Prüfung der frühest erlaubten Kommt-Zeit bezieht sich auf den Vortag. Dies wird insbesondere für die 3.Schicht benötigt. |
| Späteste erlaubte Geht-Zeit | Bis zu dieser Uhrzeit wird eine Geht Buchung akzeptiert. Bucht ein Mitarbeiter nach dieser Zeit, so wird seine Anwesenheitszeit nur bis zur eingetragenen Uhrzeit gerechnet.Beachten Sie, dass bei erlaubten Zeiträumen über Mitternacht und bei frühest erlaubten Kommt-Zeiten jedenfalls auch die frühest erlaubte Gehtzeit definiert sein muss, auch wenn diese andererseits durch die maximal erlaubte Anwesenheitszeit begrenzt wird. |
| längere Anwesenheit akzeptiert ab | In einigen Unternehmen wird davon ausgegangen, dass, wenn Mitarbeiter deutlich länger als die geplante Geht-Zeit im Unternehmen sind, dies auch begründet ist. Darum wird dann die längere Anwesenheit akzeptiert. Das bedeutet: Ist unter "längere Anwesenheit akzeptiert ab" eine Dauer eingetragen, so wird, wenn der Mitarbeiter erst um xx Std nach seiner Spätest erlaubten Geht-Zeit sein Geht bucht, dass dies begründet ist und somit die automatische Begrenzung auf die Späteste erlaubte Geht-Zeit ausser Kraft gesetzt. Bitte beachten Sie, dass für diese Entscheidung nur die Geht Buchung herangezogen wird. Sollte der Mitarbeiter in der Zwischenzeit Pause gebucht haben, so hat dies keine Auswirkung auf die Entscheidung.Die Eingabe erfolgt als Dauer. |
| Sollzeit | Dauer der Sollanwesenheitszeit an diesem Tag.Eingabe in Stunden und Minuten |
| max. Erlaubte Anwesenheitszeit | Wird als Begrenzung der echten Anwesenheitszeit verwendet. D.h. hat hier ein Mitarbeiter z.B. 10Std eingetragen, so wird die maximale Istzeit des Tages auf diesen Wert begrenzt, wenn ein anderer Wert als 00:00 eingegeben wurde. |
| Automatische Pause ab | Ab einer netto Anwesenheitsdauer von hh:mm muss der Mitarbeiter mindestens Mindestpausendauer an Unterbrechungen (=Pause) gebucht haben. Ist dies nicht der Fall, so wird automatisch die fehlende Pausenzeit nachgebucht. Die Nachbuchung erfolgt in der Form, dass ab dem errechneten Maximalen Anwesenheitszeitpunkt die Mindestpausendauer bereits erreicht ist.Beachten Sie den Unterschied bei dynamischer Pause, siehe unten. |
| Mindestpausendauer | Mindestpausen für die Automatische Pause ab, siehe oben. |
| Automatische Pause 2 ab | Ist hier ein Wert > 00:00 eingetragen, so wird ab dieser Gesamtanwesenheitszeit die Mindestpausendauer 2 als Mindestpause erzwungen.Es muss der Eintrag größer als Automatische Pause ab sein.Bitte beachten Sie, dass bei einer Definition der Automatischen Pause 2, wenn diese Anwesenheitsdauer bei der Gehtbuchung erreicht ist,  ebenfalls nur ein Eintrag mit der Mindestpausendauer 2 erfolgt. |
| Mindestpausendauer 2 | Die Mindestpause ab einer Anwesenheit von Automatische Pause 2 ab. |
| Automatische Pause 3 ab | Ist hier ein Wert > 00:00 eingetragen, so wird ab dieser Gesamtanwesenheitszeit die Mindestpausendauer 3 als Mindestpause erzwungen.Es muss der Eintrag größer als Automatische Pause 2 ab sein.Beachten Sie, dass auch hier nur ein Eintrag mit Mindestpausendauer 3 erfolgt. |
| Mindestpausendauer 3 | Die Mindestpause ab einer Anwesenheit von Automatische Pause 3 ab. |
| Rundung bei Kommt-Buchung | Rundung der Kommt Buchung zu Gunsten des Unternehmens auf xx Minuten.Beispiel: Ein Mitarbeiter bucht um 6:58 ein Kommt, so wird bei obiger Einstellung erst ab 7:00 gerechnet |
| Rundung bei Geht-Buchung | Rundung der Geht Buchung zu Gunsten des Unternehmens auf xx Minuten.Beispiel: Ein Mitarbeiter bucht um 16:58 ein Geht, so wird bei obiger Einstellung nur bis 16:55 gerechnet. |
| Runde Sondertätigkeit | Ist dies angehakt und sind Rundung bei Kommt bzw. Geht Buchung eingetragen so werden nicht nur die tatsächlichen Kommt/Geht-Buchungen gerundet, sondern auch alle Unterbrechungsbuchungen wie Unter, Arzt usw.. |
| Schichterkennungszeitpunkt  | Wenn dieses Zeitmodell beim Mitarbeiter als erlaubtes Schichtzeitmodell eingetragen ist, so wird für die automatische Erkennung des tatsächlich gültigen Zeitmodells, welche bei der Kommtbuchung gemacht wird, dieser Zeitpunkt herangezogen.D.h. bei mehreren Schichtzeitmodellen wird jeweils jenes mit dem geringeren zeitlichen Abstand der Kommtbuchung zum Schichterkennungszeitpunkt verwendet. |

Definition des Zeitmodells für die Nachtschicht?

In vielen Betrieben ist es erwünscht, dass im Schichtbetrieb auch für die Nachtschicht exakte Kommt-Geht Grenzen vorgegeben werden. Definieren Sie dafür einfach die früheste erlaubte Kommt-Zeit und die späteste erlaubte Geht-Zeit entsprechend. Z.B. von 21:30 bis 5:30.
Damit wird festgelegt, dass dieses Zeitmodell (exakter dieser Zeitmodelltag) zwei erlaubte Anwesenheitszeitbereiche hat.
1. 00:00-05:30
2. 21:30-24:00

Bitte beachten Sie, dass **Kieselstein ERP** immer tagesorientiert rechnet. Gerade für die Nachtschichten, welche z.B. am Sonntag beginnen ist dies in Zusammenhang mit der Monatsabrechnung von Bedeutung. Definieren Sie die Sollstunden jedes Tag so wie sie wirklich für diesen Tag sind.
Beispiel: Die Montagsnachtschicht beginnt am Sonntag um 21:30\. Die letzte Schicht dieser Woche endet am Freitag um 5:30\.
D.h. für Sonntag darf nur eine Sollzeit von 2:30 Std. eingetragen werden, für Montag bis Donnerstag wird 8:00 eingetragen und für den Freitag eine Sollzeit von 5:30\. Daraus ergibt sich die exakte Abbildung der 40Std. Woche. Wenn nun das Monatsende z.B. auf einen Sonntag oder einen Freitag fällt wird dies, auch für das Monat richtig dargestellt und in die Gleitzeitabrechnung entsprechend mit aufgenommen.
Würden Sie hier nur 5Tage á 8Std. angeben, so könnte es bei manchen Monatsabrechnungen Ungenauigkeiten geben. **Wichtig:**
Um die Nachtschicht eindeutig zu erkennen ist es erforderlich, dass sowohl die früheste erlaubte Kommt-Zeit als auch die spätest erlaubte Geht-Zeit definiert werden. Wenn von der Vereinbarung her, keine Begrenzung der Gehtzeit gegeben ist, so tragen Sie bitte bei der spätesten erlaubten Geht-Zeit eine Minute vor der frühest erlaubten Kommt-Zeit ein. Damit wird **Kieselstein ERP** signalisiert, dass es sich um ein Nachtschichtzeitmodell handelt. **Hinweis:**
Wird nur die frühest erlaubte Kommt-Zeit definiert (ohne Angabe der spätest erlaubten Geht-Zeit), so kann **Kieselstein ERP** nicht erkennen, dass dies ein Zeitmodell für die Nachtschicht ist und verhält sich unter Umständen nicht wie beabsichtigt. Sie erkennen dies an Istzeiten von 24 Std und ähnlichen unlogischen Ist-Zeiten.
Nutzen Sie in diesen Fällen daher die Definition des Vortages bei der frühest erlaubten Kommt-Zeit.
Hinweis2: Beachten Sie bitte gegebenenfalls auch Feiertag am nächsten Tag.

Definition automatischer Pausen

Neben der automatischen Mindestpause können mit dem Reiter Pausen für jeden Zeitmodelltag automatischen Pausen zu festgelegten Zeiten definiert werden. <u>Hinweis:</u> Im Gegensatz zu den festen Pausen (= dynamische Pause nicht angehakt), wird bei der dynamischen Pause die Automatische Pause ab in die Zukunft gerechnet. Das bedeutet dass die Mitarbeiter bei einem Zeitmodell mit dynamischer Pause die Pause immer in die Zukunft gerechnet wird, also ab dem Zeitpunkt automatische Pause ab, wird eine Pause gebucht bis maximal Mindestpausendauer. Zusätzlich wird sichergestellt, dass bei überschreiten der Automatischen Pause ab bis zum erreichen der Automatischen Pause 2 zumindest die Mindestpausendauer (1) abgezogen wird.
Dazu zwei Beispiele:
a.) Automatische Pause ab 6,5 Std. und 9,5 Std. mit Pausen von 0,5 bzw. 0,75 Std.
Einstellung des Zeitmodelltages
![](dynamische_Pause1.gif)
In Worten ausgedrückt, nach 6 Std. muss der Mitarbeiter mit der Pause von maximal 1/2 Std. beginnen.
Nach 9 Std. muss er mindestens 0,5 Std. und maximal 0,75 Std. als Pause haben.

So sehen dann z.B. die Pausenbuchungen aus.
![](dynamische_Pause2.gif)
D.h. es wird die gesamte Pause auf einmal reingebucht.

b.) gleiche Zeitdaten aber der Beginn der Pausen wird um 30 Minuten nach hinten verschoben.
![](dynamische_Pause3.gif)

![](dynamische_Pause4.gif)

Aufgrund der ersten Pause ergibt sich, dass die Automatische Pause 2 nicht erreicht wird und es wird nur 1/2 Std. abgezogen.

**In anderen Worten:** Dynamische Pause bedeutet, dass die Pause nicht rückwirkend gebucht wird, sondern in die Zukunft.
Beispiel
Arbeitszeit lt. Zeitmodell:
Automatische Pause ab 6:00h für 00:30h
Mitarbeiter ist anwesend von 7:00 Uhr bis 13:05
Zeitbuchung MIT dynamischer Pause:
Kommt: 07:00
Unterbrechung Start: 13:00
Unterbrechung Ende: 13:05
Geht: 13:05
Man sieht, dass die Pause von 13:00 bis 13:05 gerechnet wird. Die Anwesenheitszeit des Mitarbeiters sind 6 Stunden

Zeitbuchung OHNE dynamische Pause:
Kommt 07:00
Unterbrechung Start: 12:30
Unterbrechung Ende: 13:00
Geht: 13:05
Man sieht, dass die Pause vollständig abgezogen wird. Die Anwesenheit des Mitarbeiters sind 05:35 Stunden.

Die dynamische Pause ist dann sinnvoll, wenn Teilzeitkräfte so eingeteilt werden, dass diese die maximal mögliche Zeit ohne Pause durcharbeiten. Sollte jedoch eine Teilzeitkraft auch nur eine Minute länger bleiben als sie darf, wird rückwirkend die Mindestpausendauer automatisch abgezogen. Mit der dynamischen Pause wird nur die eine Minute, die die Teilzeitkraft länger geblieben ist, als Pause gerechnet.

#### Können damit auch Geht-Buchungen automatisch verschoben werden?
Nein, Details [siehe bitte](../Zeiterfassung/index.htm#Pausen automatisch verschieben).

<a name="Welche Tage / Tagesarten müssen definiert werden"></a>Welche Tage / Tagesarten müssen definiert werden?

Es sollten immer alle verwendeten Tagesarten in jedem Zeitmodell definiert werden.

Denken Sie daran, dass z.B. aus Gründen einer sehr guten Auftragslage oder auch im Schichtbetrieb, die Mitarbeiter auch an Tagen an denen normalerweise nicht gearbeitet wird, im Unternehmen sind und ihre Zeiten buchen.

Haben Sie nun z.B. Rundung zu Gunsten des Unternehmens oder die Automatische Mindestpause vereinbart, so müssen diese Regelungen für alle Tagesarten an denen auch Buchungen möglich sind, definiert sein, damit **Kieselstein ERP** die anerkannten Stunden und die Mindestpausen auch tatsächlich buchen kann.

Überstundenabrechnung / Kollektivvertrag / Tarifvertrag

[Siehe]( {{<relref "/fertigung/zeiterfassung/ueberstundenabrechnung">}} ).

Wann werden Automatikbuchungen gemacht ?

Bei jeder neuen Geht-Buchung wird geprüft, ob seit der letzten Kommt-Buchung des selben Tages bzw. des Vortages automatische Pausen nachzutragen sind. Ist dies der Fall werden die erforderlichen Buchungen durchgeführt.
Eine nachträgliche Überprüfung auf geänderte Definitionen der Automatikbuchungen erfolgt nicht.
Werden Definitionen von Automatikbuchungen geändert und diese Änderungen sollten auch in bereits bestehenden Daten durchgeführt werden, so muss dafür die Funktion Pflege Automatikbuchungen im Modul Zeiterfassung verwendet werden.

In welcher Reihenfolge werden die Pausen verbucht?

Sind bei einem Zeitmodell sowohl fixe Pausen als auch dynamische Pausen hinterlegt, so werden bei der Geht-Buchung zuerst die fixen Pausen eingetragen. Danach wird auf die Einhaltung der dynamischen Pausen geprüft und diese gegebenenfalls nachgetragen.

Rundung zu Gunsten des Unternehmens

Es kann für jeden Tag auch eine Rundung zu Gunsten des Unternehmens definiert werden.

![](Rundung_zu_Gunsten_des_Unternehmens.gif)

Rundung bei Kommt-Buchung bedeutet: Die beim Mitarbeiter eingetragene Kommt-Buchung wird auf ganze (in unserem Beispiel) 15Minuten zu Gunsten des Unternehmens gerundet. Wenn also um 7:01 eine Kommt-Buchung eingetragen ist, so wird die Anwesenheitszeit erst ab 7:15 gerechnet.

Rundung bei Geht-Buchung bedeutet: Die beim Mitarbeiter eingetragene Geht-Buchung wird auf ganze (in unserem Beispiel) 5Minuten zu Gunsten des Unternehmens gerundet. Wenn also um 16:14 eine Geht-Buchung eingetragen ist, so wird die Anwesenheitszeit nur bis 16:10 gerechnet.

Ist noch zusätzlich Runde Sondertätigkeit angehakt, so gilt diese Regelung auch für alle Arten von stundenweisen Unterbrechungsbuchungen. Auch hier wird der Unterbrechungsbeginn abgerundet, so wie Geht und das Unterbrechungsende aufgerundet, so wie Kommt.

**Wichtiger Hinweis:**

Die Rundung zu Gunsten des Unternehmens wirkt nicht bei Automatikbuchungen. Dies vor allem deshalb, damit alle automatischen (Pause-) Buchungen zum eingetragenen Zeitpunkt stimmen und nicht erst mühevoll mit der Rundungsregel abgestimmt werden müssen.

Werden nun, insbesondere bei Buchungen über Mitternacht, manuell Zeiten nachgetragen, so kann es zu ungerechtfertigten Abzügen kommen. Um diesem zu entgehen, müssen die Automatik Buchungen per Hand erzwungen werden.

![](Rund_Automatik_Buchung.jpg)

Hier wurde der Mitternachtssprung per Hand nachgetragen. Daher ergibt dies bei der Monatsabrechnung eine Rundung auf 23:45 und somit einen ungerechtfertigten Abzug von 15Minuten für den Mitarbeiter.

Um dies zu korrigieren, löschen Sie die Buchungen um 23:59 und am nächsten Tag um 00:00\.

![](Rund_Automatik_Buchung2.jpg)

und fügen Sie kurz vor dem eigentlichen Geht eine weitere Gehtbuchung ein. Dadurch werden die Automatikbuchungen für den Mitternachtssprung nachgetragen.

![](Rund_Automatik_Buchung3.jpg)

Löschen Sie nun die von Ihnen eingefügte Gehtbuchung und Sie erhalten die richtigen Buchungen und Buchungsarten um diesen Mitarbeiter an diesem Tag richtig abzurechnen.

#### Was ist bei Zeitmodelländerungen zu beachten
[Siehe bitte.](../Zeiterfassung/index.htm#Was_ist_bei_Zeitmodelländerungen_zu_beachten)

#### Wieso müssen die Urlaubstage / Woche angegeben werden?
Üblicherweise ergeben sich die Urlaubstage pro Woche aus den Tagen mit Sollstunden.

Wird in Ihrem Unternehmen jedoch im Dreischichtbetrieb gearbeitet, so ergibt sich durchaus, dass manchmal z.B. an Sonntagen Sollstunden hinterlegt sind und sich daraus sechs Urlaubstage ergeben würden, obwohl tatsächlich nur fünf Urlaubstage anzurechnen sind. Achten Sie daher darauf, dass hier die tatsächlich anzurechnenden Urlaubstage pro Woche für das Zeitmodell angewendet werden. Siehe dazu auch [Berechnung des Urlaubsanspruches](index.htm#Urlaubsanspruch).

#### Wie Zeitmodelle für freie Mitarbeiter anlegen?
Freie Mitarbeiter werden üblicherweise nach geleisteten Stunden abgerechnet. D.h. legen Sie einfach ein Zeitmodell an, bei dem zwar Pausen usw. definiert sind, bei dem die Sollstunden aber auf 00:00 gesetzt sind. Dies bewirkt, dass der freie Mitarbeiter Stunden aufbaut. Erhalten Sie nun die Eingangsrechnung des freien Mitarbeiters, so tragen Sie die damit abgerechneten Stunden im Personal unter Stundenabrechnung mit einem Verweis auf die Eingangsrechnung ein. Somit wird der Gleitzeitsaldo entsprechend korrigiert und Sie haben einen praktischen Überblick über die tatsächlich offenen Stunden.
Der Vorteil ist damit auch, dass Sie die Kosten des freien Mitarbeiters damit auch auf den jeweiligen Projekten erfasst haben.

**ACHTUNG:**
Achten Sie auf die Bestimmungen der Scheinselbständigkeit.

#### Wie Zeitmodelle für Vertriebsmitarbeiter anlegen?
Hier geht es üblicherweise darum, dass Vertriebsmitarbeiter einen All Inklusive Vertrag haben und daher in der Regel keine Zeiten erfassen. Trotzdem sollten die konsumierten Urlaubstage auf einfache Weise in **Kieselstein ERP** eingetragen werden.

Definieren Sie daher ein eigenes Zeitmodell aus dem die vertraglich vereinbarten Sollstunden pro Tag ersichtlich sind. Diese Tage sind die Basis für den Eintrag des Urlaubs und die Urlaubsabrechnung.

Da die Mitarbeiter aber keine Zeiten erfassen, tragen Sie bitte bei den Sollstunden pro Monat 0,00 ein. Damit entstehen keine negativen Stundenansprüche aufgrund dessen dass der Mitarbeiter keine Zeiten erfasst. Ob diese Nichterfassung von Zeiten für das Unternehmen / für Sie richtig ist, müssen Sie selbst entscheiden.

#### Warum wird das Zeitmodell rot dargestellt?
Ein Zeitmodell wird rot eingefärbt, wenn keine Zeitmodelltage hinterlegt sind.

#### Wir haben bisher eine Pausenhupe im Einsatz
In einigen Unternehmen ist es üblich mit einer Pausenglocke bzw. Pausenhupe die Pausenzeit anzuzeigen.
Abgesehen davon, dass wir der Meinung sind, dass es für alle von Vorteil ist, wenn ein Mitarbeiter seine Arbeit soweit fertig macht, dass die Maschine(n) während der Pause weiterlaufen können und dass es für alle fairer ist wenn die Pause dann gestempelt wird, sind manche der Meinung dass diese Anzeige der gemeinsamen Pause wichtig ist. Hier hat sich bewährt eine kleine preiswerte elektronische Zeitschaltuhr einzusetzen, welche die Pause anzeigt.