---
title: "Kundenverwaltung"
linkTitle: "Kunden"
categories: ["Kunden"]
tags: ["Kunden", "Lieferadresse", "Rechnungsadresse"]
weight: 300
description: >
  Verwaltung der Kunden und deren Konditionen
---
Kunde
=====

#### Unterschied zwischen Kunde und Partner
Der Partnerstamm ist die zentrale Adressablage. Wird ein Partner zum Kunden, so werden weitere Informationen benötigt. Wie z.B. Zahlungsziel, Kreditlimit oder Debitorenkontonummer. Diese Konditionen sind je Mandant und Kunde spezifisch und werden daher in einer ergänzenden Tabelle abgebildet. [Siehe]( {{<relref "/docs/stammdaten/partner/#kunde-partner-lieferanten-mandanten-wie-geh%c3%b6rt-dies-alles-zusammen" >}} )
und dazu [bitte auch]( {{<relref "/docs/stammdaten/partner/#kunde-lieferant-partner-wozu" >}} ).

#### Kunde aus Partner erzeugen
Mit dieser Funktion ![](Kunde_aus_Partner.gif), sie finden diese in der Auswahlliste des Moduls Kunde, kann eine bereits bestehender Partner um die Kundeninformationen erweitert werden. Dies ist z.B. dann erforderlich, wenn bereits ein Lieferant erfasst ist, damit sind automatisch die Partnerdaten dafür erstellt worden und mit diesem Partner wird nun auch eine Geschäftsverbindung im Sinne des Kunden eingegangen.<br>
**Wichtig:**<br>
Dies ist eine Verlinkung. D.h. wenn Sie nun die Adressdaten, genauer die Partnerdaten, des Kunden ändern, so werden, da dies nur unterschiedliche Sichten auf die gleichen Daten sind, auch die Partnerdaten und damit die Lieferantendaten mitgeändert.<br>
Für ein Kopieren des Kunden ist die Funktion nicht geeignet.<br>
Siehe dazu bitte auch [Zusammenhang Kunde, Partner, Lieferant]( {{<relref "/docs/stammdaten/partner/#kunde-partner-lieferanten-mandanten-wie-geh%c3%b6rt-dies-alles-zusammen" >}} ) [Kunde, Partner, Lieferant wozu]( {{<relref "/docs/stammdaten/partner/#kunde-lieferant-partner-wozu" >}} )  und [Partner in Kunden umwandeln]( {{<relref "/docs/stammdaten/partner/#wie-mache-ich-einen-partner-zum-kunden" >}} ).

Wenn Sie einen Kunden anlegen wollen, der bereits besteht, erfolgt folgende Abfrage:
![](kunde_besteht_bereits.JPG)
- Bei "Ja" wird eine Kopie mit den jeweiligen Daten erstellt. 
- Bei "Überschreiben" wird der bestehende Kunde mit den neu eingegebenen Daten überschrieben.
- Bei "Verwerfen" bleibt der bestehende Kunde unverändert und die aktuell eingegebenen Daten werden verworfen.

#### Unterschied zwischen Kommentar und Bemerkung
<a name="Kommentar_versus_Bemerkung"></a>

#### Im Kunden und auch im Lieferantenstamm finden Sie die Editor-Eingabefelder Bemerkung (unter Kopfdaten) und Kommentar (unter Konditionen).
Dies sind zwei Unterschiedliche Datenfelder. Die Bemerkung aus den Kopfdaten ist auch in den Partnerdaten des Kunden (Lieferanten) enthalten und dient dazu, dass Sie wichtige Informationen für diesen Kunden/Partner, sofort sehen, wenn die Detaildaten des Partners angezeigt werden.<br>
Um nun weitere Vereinbarungen usw. mit diesem Kunden festzuhalten steht im Panel Konditionen das Editorfeld Kommentar zur Verfügung. **Hinweis:** Beachten Sie die [Größenbeschränkung]
( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/freie_texte/#gr%c3%b6%c3%9fenbeschr%c3%a4nkung-der-texteingaben" >}} ) der Bemerkungs-Kommentar-Felder. Verwenden Sie gegebenenfalls anstatt dessen das Modul Projekte ![](Projekte_als_Verkaufs_Aufgabe.jpg)

Weiters finden Sie im oberen Modulreiter Kommentar eine Möglichkeit Kunden-Kommentare vergleichbar mit [Artikelkommentaren]( {{<relref "/docs/stammdaten/artikel/#artikelkommentar" >}} ) zu erfassen.<br>
Die Kommentarart kann in den Partner-Grunddaten definiert werden. Je nach Definition werden bei der Auswahl des Kundens in den Belegen diese Hinweise angezeigt (auch Bilder/PDF).<br>
Ebenso können die Kommentare in den Belegen angedruckt werden. Sollte dies bei Ihren spezifischen Reports noch nicht möglich sein, wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

#### Interessent / Kunde
Wird ein neuer Kunde angelegt, z.B. da Sie Ihm ein Angebot senden, so wird er automatisch als Interessent angelegt. Erhält dieser Interessent nun eine Auftragsbestätigung oder eine Rechnung, so wird automatisch sein Interessenten-Status gelöscht und er somit zum vollwertigen Kunden.

#### Hinweis intern / extern, was ist der Unterschied?
Wenn in diesen Feldern Einträge vorhanden sind, so werden beim Speichern der Kopfdaten von Angebot - Rechnung diese Hinweise angezeigt. Zusätzlich ist angedacht, dass der externe Hinweis auch einmal auf einem Report angedruckt werden könnte. Der interne Hinweis ist nicht für den Druck auf Reports vorgesehen.

#### Default Einstellungen?
Wenn ein neuer Kunde angelegt wird, so werden für folgende Felder folgende Defaultwerte automatisch von **Kieselstein ERP** eingepflegt.

| Feld | Default Wert |
| --- |  --- |
| Kostenstelle | Heimatkostenstelle des angemeldeten Benutzers |
| Provisionsempfänger | Der angemeldete Benutzer |
| Währung | Mandantenwährung |
| Zahlungsziel | Zahlungsziel aus den Vorbelegungen des Mandanten |
| Lieferart | Lieferart aus den Vorbelegungen des Mandanten |
| Spediteur | Spediteur aus den Vorbelegungen des Mandanten |
| Partnerart | Adresse |
| Kommunikationssprache | Die Sprache des angemeldeten Benutzers |
| Mehrwertsteuersatz | Die in den Konditionen des Mandanten definierten Mehrwertsteuer in Abhängigkeit der Länderart |
| Preisliste | Preisliste aus den Vorbelegungen des Mandanten. Üblicherweise wird hier die erste Preisliste aus dem Artikel verwendet, welche immer den höchsten Preis liefern sollte.Um eine zwangsweise Auswahl der Preisliste zu erreichen [siehe]( {{<relref "/docs/stammdaten/system/#keine-preisliste-vorbesetzen" >}} ). |
| LS-Kopien  | Anzahl Kopien aus dem Parameter DEFAULT_KOPIEN_LIEFERSCHEIN |
| RE-Kopien | Anzahl Kopien aus dem Parameter DEFAULT_KOPIEN_RECHNUNG |
| Kreditlimit | Kreditlimit aus dem Parameter KREDITLIMIT des Mandanten, default 1.000,- |
| Mindestbestellwert | Mindestbestellwert aus dem Parameter DEFAULT_MINDESTBESTELLWERT, wenn dieser > 0 ist |

#### Wie kann ein bestehender Partner übernommen werden?
<a name="Partner_übernehmen"></a>
[Siehe Lieferant]( {{<relref "/docs/stammdaten/lieferanten/#lieferant-aus-partner-erzeugen" >}} )

#### Vergabe der Debitorennummer?
<a name="Vergabe der Debitorennummer"></a>
Wenn einem Kunden eine Debitorennummer zugewiesen wird, wird automatisch im Finanzbuchhaltungsmodul das Debitorenkonto mit derselben Nummer und dem entsprechenden Kunden erstellt.
Falls die Debitorennummer schon existiert, erscheint ein Hinweis und man kann entweder eine neue Nummer vergeben oder die selbe Nummer auch diesem Kunden zuweisen.

**ACHTUNG:** In der Finanzbuchhaltung bekommt das Konto den Namen des gerade gewählten Kunden, der Name des "alten" Kunden wird überschrieben.

Es kann jederzeit eine neue Debitorennummer zugewiesen werden. Das Debitorenkonto in der Finanzbuchhaltung bleibt aber auch mit der vorherigen Nummer und Kundennamen bestehen und kann manuell nur gelöscht werden, wenn noch keine Buchungen getätigt wurden. **Hinweis:** Der Bereich der Debitorennummern kann im Modul System, Parameter, DEBITORENNUMMER_VON und DEBITORENNUMMER_BIS eingestellt werden und die Länge der Nummer (maximal 6stellig) wird über den Parameter KONTONUMMER_STELLENZAHL_DEBITORENKONTEN eingestellt. Achten Sie darauf, dass die Definition des Debitorennummernbereiches und die Stellenanzahlen übereinstimmen.

Ein Debitorenkonto kann auch mehreren Kunden zugewiesen werden.

Der Knopf Debitorenkonto anlegen vergibt eine noch unbesetzte Kontonummer nach folgendem Muster: Klasse 3 (abgeleitet aus DEBITORENNUMMER_VON) für Debitorenkonten, die nächsten zwei Stellen betreffen den Anfangsbuchstaben des Kunden A = 01 ... Z = 26 und eine laufende Nummer. Da in manchen Unternehmen der Beginn des Nummernkreises keine glatte Zahl ist, wurde dies so realisiert, dass die ersten drei Stellen des Parameters DEBITORENNUMMER_VON verwendet werden und dazu die oben angeführten Werte dazugezählt werden. Führende Leerstellen werden bei der Erkennung entfernt. Ziffern werden auf den deutschsprachigen Anfangsbuchstaben ersetzt. Alle sonstigen Zeichen werden auf 27 zusammengefasst. Wird der Parameter FINANZ_DEBITORENNUMMER_FORTLAUFEND auf 1 gestellt, wird diese Funktion deaktiviert und die Debitorennummern fortlaufend vergeben.

Automatische Debitorennummern Vergabe

Mit dem Parameter AUTOMATISCHE_DEBITORENNUMMER (=1) kann eingestellt werden, dass ab dem Zeitpunkt ab dem die Rechnungsadresse benötigt wird, für diesen Kunden eine Debitorennummer automatisch erzeugt wird. D.h. Wird ein Kunde als Rechnungsadresse im Auftrag oder im Lieferschein oder als Rechnungsempfänger verwendet, so wird gegebenenfalls eine neue Debitorennummer nach obiger Regel erzeugt.
Die automatische Vergabe der Debitorennummer erfolgt in der Form, dass beginnend ab DEBITORENNUMMER_VON die nächste freie Nummer gesucht wird. Gegebenenfalls wird noch der Parameter FINANZ_DEBITORENNUMMER_FORTLAUFEND in der Form berücksichtigt, dass der Anfangsbuchstabe des Kunden wie oben beschrieben den Bereich für die Suche einer freien Debitorennummer eingeschränkt wird.
Durch die Verwendung der Lücken, kann es vorkommen, dass trotzdem neuere Kunden eine niedrigere Debitorennummer bekommen.
Bitte beachten Sie, dass die Debitorennummer bereits bei der Auftragsanlage automatisch vergeben wird.

#### Wann sollte einem Kunden eine Debitorennummer gegeben werden?
Grundsätzlich benötigen Sie die Debitorennummer nur für die Finanzbuchhaltung. D.h. die Debitorennummer muss erst vergeben werden, wenn der Kunde tatsächlich eine Rechnung erhält. Von der Idee her, sollten keine Debitorennummern für z.B. Lieferadressen oder ähnlichem vergeben werden.

#### Lieferstatistik
In der rechten Spalte der Lieferstatistik wird das Bewegungsmodul angezeigt, welches die Buchung verursachte.
Bei einer Rechnung wird hier RE und die jeweilige Rechnungsnummer und eventuell der zugehörige Lieferschein angezeigt. Bei einer Gutschrift wird GS und die Gutschriftsnummer (in der Rechnungsnummernspalte) und ein ev. zugehöriger Rückschein angezeigt. Zugleich wird die Menge vorzeichenrichtig dargestellt. D.h. Rücknahmen (Ware vom Kunden zu Ihnen) werden negativ dargestellt.
Bitte beachten Sie, dass üblicherweise die Auswertung nach dem tatsächlichen Lieferdatum und nach der Lieferadresse erfolgt.
Für eine Auswertung nach dem Rechnungsdatum haken Sie bitte ![](Lieferstatistik_nach_Rechnungsdatum.gif) Rechnungsdatum an.
Beachten Sie bitte zugleich auch die Auswahl der zugrundeliegenden Adresse, ![](Lieferstatistik_Adresse.gif).

#### Welchen Adressen werden für die Lieferstatistik verwendet ?
Welche Adressen für die Lieferstatistik verwendet werden kann von Ihnen definiert werden. Es stehen dafür folgende Auswertungen zur Verfügung:

| Adresse | Wirkung |
| --- |  --- |
| Lieferadresse | Dies bringt die Positionen, welche direkt an die Lieferadresse verrechnet bzw. geliefert wurden |
| Rechnungsadresse | Dies bringt die Positionen, welche an den gewählten Kunden anhand der Zuordnung Rechnungsadresse erfolgten. Also die Positionen die direkt in den Rechnungen enthalten sind und die Positionen der Lieferscheine, welche diese Adresse als Rechnungsadresse haben (die Lieferscheine die auf die Rechnungsadresse lauten). |
| Statistikadresse | Statistikadresse ist Rechnungen laut Statistikadresse plus die Lieferscheine die als Lieferadresse den gewählten Kunden haben. Also wo die Lieferadresse gleich der Adresse des gewählten Kunden ist. |

#### Zusammenhang Partner / Kunde?
[siehe]( {{<relref "/docs/stammdaten/partner/#kunde-partner-lieferanten-mandanten-wie-geh%c3%b6rt-dies-alles-zusammen" >}} )

#### Grundsätzliche Filterung der Kundenliste
Neben den Direktfiltern auf die Kundennamen usw. steht auch eine Filterung auf 

![](Kundenauswahl_Filterung.gif)

nur Kunden oder nur Interessenten zur Verfügung. Wählen Sie dazu aus der Combobox die gewünschte Einstellung aus.

Mit dem Parameter DEFAULT_KUNDENAUSWAHL kann eingestellt werden, wie die Grundeinstellung dafür ist.

#### Wie wirkt eine Änderung des Mehrwertsteuersatzes des Kunden?
Der im Kunden gespeicherte Mehrwertsteuersatz wird genauso wie die Lieferart, das Zahlungsziel und der Spediteur als **Vorschlagswert** in die jeweiligen Bewegungsmodule übernommen. Das bedeutet: Wird bei einem Kunden der Mehrwertsteuersatz verändert, so wirkt dies nur für die Vorschlagswerte für die danach neu angelegten Bewegungsdaten. Für bereits erstellte Angebote usw. wird der in den Angeboten verwendete Steuersatz **nicht** verändert.

#### ABC Statistik
<a name="ABC"></a>
Als idealtypisch gilt die so genannte Pareto-Verteilung (Pareto-Prinzip: nach Vilfredo Pareto, Italien 1848 - 1923, Entdecker dieses Prinzips) auch 80/20-Regel, d.h. zum Beispiel im Fall der Kundenbewertung (Kundenwert): mit lediglich 20 % der Kunden werden bereits 80 % des Umsatzes erzielt (A-Kunden = hohe Bedeutung), 30 % der Kunden bringen 15 % des Umsatzes (B-Kunden = mittlere Bedeutung) und von 50 % der Kunden kommen nur 5 % des Umsatzes (C-Kunden = geringe Bedeutung).
Quelle: <http://de.wikipedia.org/wiki/ABC-Analyse>
In **Kieselstein ERP** können Sie die Schwellwerte der A und B Verteilung in den Mandantenparametern ändern (KUNDENBEWERTUNG_WERT_A, KUNDENBEWERTUNG_WERT_B). Der Umsatzwert für die C Beurteilung ergibt sich aus dem Rest von A und B auf 100%.

#### Journal Umsatzstatistik
<a name="Umsatzstatistik"></a>
Mit dieser Umsatzstatistik erhalten Sie eine Übersicht über die Umsätze und Deckungsbeiträge Ihrer Kunden, gruppiert nach Partner-Klassen oder Partner-Branchen, verteilt auf Artikelgruppen oder Artikelklassen.<br>
Zusätzlich stehen auch Statistiken über Monate oder Jahre zur Verfügung.<br>
**Hinweis:** In dieser Auswertung werden Artikel die als Nicht Lagerbewirtschaftet gekennzeichnet sind und keine Arbeitszeitartikel sind, automatisch mit einem Gestehungswert von 0,00 gewertet. D.h. die Erlöse dieser Artikel gehen zu 100% in den Deckungsbeitrag ein.<br>
**Hintergrund:** Normalerweise werden diese Artikel nur für zusätzliche Kostenverrechnungen verwendet, welche normalerweise eben keine Gestehungskosten darstellen. Leider kommt es in der Praxis aber immer wieder vor, dass externe Kosten auf diese Artikel gebucht werden. **Hinweis:** In dieser Auswertung werden Artikel die als Nicht Lagerbewertet gekennzeichnet sind **<u>nicht</u>** berücksichtigt.<br>
Hinweis:<br>
Für die zukünftigen Umsätze siehe auch [Auftrags-Umsatzstatistik]( {{<relref "http://localhost:1313/verkauf/auftrag/#auftragseingang-im-zeitraum" >}} ).

#### Auftragsadresse verwenden, Statistikadresse verwenden?
In der Regel wird die Kundenumsatzstatistik nach der Rechnungsadresse ermittelt.
In einigen Betrachtungen ist es sinnvoll, dass anstelle der Rechnungsadresse die bei der [Rechnung]( {{<relref "/verkauf/rechnung/#rechnungsadresse-statistikadresse" >}} ) hinterlegte Statistikadresse verwendet wird. Wurde diese Auswertung gewählt, so wird anstelle der Rechnungsadresse die Statistikadresse ihrer Rechnungen für die Kunden-Umsatzbetrachtung verwendet.<br>
Wurde zusätzlich Auftragsadresse verwenden gewählt, so bedeutet dies, dass bei Positionen / Rechnungen die einen direkten oder indirekten Auftragsbezug haben, die Auftragsadresse für die Kundenumsatzstatistik verwendet wird. Ist bei einer Rechnung kein Auftragsbezug gegeben so wird die Statistikadresse, wenn angehakt, bzw. die Rechnungsadresse verwendet.

#### Wie wird der Deckungsbeitrag eines Kunden errechnet ?
Bei der Deckungsbeitragsrechnung wird grundsätzlich von den im Zeitraum gelegten Rechnungen abzüglich Gutschriften ausgegangen. Diese ergeben die Umsätze, die Erlöse. Dem gegenüber stehen die Aufwände.
Diese werden ausschließlich aus denjenigen Positionen errechnet, die anhand der Warenbewegung direkt oder indirekt in den Rechnungen enthalten sind.
Bei Gutschriften werden die Erlöse negativ betrachtet und es wird versucht anhand der Verbindung der Gutschrift zur Rechnung den ursprünglich ausgelieferten Gestehungspreis zu ermitteln und diesen ebenfalls negativ zu berücksichtigen. Bei Wertgutschriften werden nur die negativen Erlöse herangezogen, da ja keine Warenrückgabe erfolgt.
Bitte beachten Sie, dass nur durch eine reine Kundenzuordnung z.B. auf Losen, dies KEINE Auswirkung in der Deckungsbeitragsbetrachtung des Kunden hat. Erst wenn diese Ware direkt an den Kunden geliefert wird, wirkt dies auch in der Deckungsbeitragsbetrachtung.
Würde z.B. ein Fertigungslos an einen Kunden angelegt, auf dieses entsprechende Aufwände gebucht und das Los dann manuell erledigt und andererseits eine Rechnung mit einem Handartikel an den gleichen Kunden erstellt, so ergibt dies einen Deckungsbeitrag von 100%, da keine tatsächliche Warenbewegung stattgefunden hat.

#### Umsatz nach Statistikadresse
Grundsätzlich wird die Kundenumsatzstatistik anhand der Rechnungsadresse errechnet.<br>
Da es, gerade bei indirekten Umsätzen, oft sehr interessant ist, welche Umsätze Sie mit dem eigentlichen Endkunden erzielen, steht auch die Auswertung nach Statistikadresse zur Verfügung.<br>
Hinweis:<br>
Insbesondere bei Auswertungen nach Statistikadresse entsteht manchmal der Eindruck, dass die Betrachtung des EU = Erstumsatz(es) falsch ist.<br>
Hier ist meist der Hintergrund, dass eine kürzlich / in dem Zeitraum geschriebene Rechnung in der Regel auf eine andere Statistikadresse zugeordnet ist / wurde und daher diese Statistikadresse zum ersten Mal in einer Rechnung verwendet wird, womit der Erstumsatz gegeben ist. Zur Anpassung der [Statistikadresse siehe]({{<relref "/verkauf/rechnung/#rechnungsadresse-statistikadresse" >}} ).

#### Umsatzstatistik über Jahre bzw. Monate
Im Journal der Kundenumsatzstatistik kann neben der Gruppierung nach Artikelgruppen/klassen auch eine Auswertung über den Zeitraum erfolgen.

![](Umsatzstatistik_Gruppierung.gif)

Bei der Auswertung/Gruppierung nach Jahren, werden die letzten 10 Jahre vorgeschlagen. Bei der Auswertung nach Monaten werden die 12 Monate des vergangenen Jahres vorgeschlagen.

Sie erhalten damit für jedes Monat bzw. Jahr eine eigene Spalte mit den entsprechenden Umsatz und Deckungsbeitragswerten.

Um bei der Umsatzauswertung über die Jahre die Sortierung entsprechend steuern zu können, steht

![](Umsatz_10Jahres_Sortierung.gif)

die Definition der Sortierung zur Verfügung. Dabei bedeutet:

| Sortierung | Wirkung |
| --- |  --- |
| Gesamt | Die Umsatzliste wird nach dem Gesamt-10 Jahres Umsatz sortiert |
| Aktuelles Jahr | Die Umsatzliste wird nach dem aktuellen Jahr sortiert. Wobei hier das aktuelle Jahr aus dem Bis-Datum verwendet wird. |
| Vorjahr | Die Umsatzliste wird nach dem Vorjahr sortiert. Auch hier wird das Bis-Datum als Ausgangsbasis herangezogen. |

Beachten Sie bitte, dass für eine reine Sortierung nach Umsatz die Sortierung auf Umsatz und die Kundengruppierung auf keine stehen muss.

Wurde eine Kundengruppierung gewählt, so erfolgt die Umsatzsortierung innerhalb der jeweiligen Kundengruppe.

#### Umsatzstatistik mit Zielerreichung
Ab der Version 0.2.13 kann auch je Kunde das geplante Umsatzziel eingegeben werden. Dieses findest du im Kunden, im Reiter Detail<br>
![](Umsatzziel.png)<br>
Dieses Feld steht nur zur Verfügung, wenn der Benutzer das Recht PART_KUNDE_UMSAETZE_R hat.
Trage hier den geplanten Jahresumsatz für den jeweiligen Kunden ein.<br>
Zusätzlich kannst du ab dieser Version auch für die Regionen, z.B. die Kontinente, die geplanten Umsätze eintragen. Siehe dazu System, Region. Idealerweise ordnest du dann die Länder deinen Regionen zu und siehst somit auch, ob du für deine Region bereits ausreichend Kunden gefunden hast, die die Umsätze erzielen sollten.

Um nun diese Zielerreichnung zu sehen, wählst du bei der Kunden-Umsatzstatistik
- Umsatzziel berücksichtigen. D.h. hier würden auch Kunden aufgelistet werden, die keinen Umsatz aber eine Umsatzziel hinterlegt haben
- und die Sortierung nach Land + Postleitzahl<br>
![](Umsatzziel_Auswerten.png)

Damit erhältst du in der Umsatzübersicht bei jedem Kunden die Angabe seines Umsatzzieles in tausend (Euro, genauer Mandantenwährung) und je Land welche Umsatzzielsumme geplant ist.

Am Ende des Reports findest du 


#### Export der Kundenumsatzstatistik über mehrere Jahre hinweg
Wie üblich kann auch die Kundenumsatzstatistik gespeichert und somit exportiert werden.
Insbesondere wenn sich mehr als 8 Spalten ergeben, wird automatisch die Darstellung auf mehrere Durchläufe aufgeteilt. Dies bedingt, dass der direkte CSV-Export, bzw. der Export über die Zwischenablage nur für die ersten 8Spalten gegeben ist.
D.h. um z.B. die Jahresumsätze von mehr als 8Jahren zu exportieren, teilen Sie dies bitte auf einen Export für die ersten 8 Jahre und nachfolgend für die weitern Jahre auf.

#### Umsatzübersicht
Im Modulreiter Umsatzübersicht erhalten Sie eine Darstellung der Umsätze dieses Kunden der vergangenen 10Jahre. Zusätzlich wird die Anzahl der Rechnungen ausgegeben, damit Sie ein Gefühl dafür bekommen, ob der Kunde viele kleine Rechnungen bekommen hat oder nur wenige aber dafür mit einem entsprechenden Volumen. In der Zeile davor werden die Umsätze der Vorjahre angezeigt. In der untersten Zeile Summe wird die Gesamtsumme der Betrachtung dargestellt.

#### Umsatzanzeige
In den Kunden-Kopfdaten sehen Sie unten die Umsatzanzeige.

![](Kunden_Umsatz_Anzeige.gif)

In dieser Darstellung sehen Sie den aktuellen Umsatz des Kunden des laufenden Kalenderjahres und des Vorjahres. Zugleich wird die Anzahl der gelegten Rechnungen angezeigt. Zusätzlich können Sie wählen, ob die Umsatzbetrachtung auf Basis der tatsächlichen Rechnungsadresse (Kundenadresse in der Rechnung) oder der Statistikadresse ermittelt wird. Im Umsatz sind alle Ausgangsrechnungen jedoch ohne Anzahlungsrechnungen abzüglich der Gutschriften enthalten. Es werden nur Nettowerte angezeigt.

Unterscheidung Umsatz Direkt oder Indirekt, also über die Rechnungsadresse oder die Statistikadresse

Direkt = Kundenadresse = Welche Rechnungen wurden direkt an diese Adresse gelegt.

Indirekt = Statistikadresse = Welche Rechnungen wurden gelegt, welche als Statistikadresse diese Adresse eingetragen haben.

Sie sehen hier also für einen Kunden durchaus unterschiedliche Werte. Üblich ist diese Vorgehensweise, wenn z.B. Hotels über Einkaufsgemeinschaften einkaufen, also die Verrechnung direkt an die Einkaufsgemeinschaft erfolgen muss. Für Sie ist es aber wichtig, dass trotzdem erkannt wird, welchen Umsatz welcher Kunde verursacht hat, denn nur der Kunde (und nicht die Einkaufsgemeinschaft) muss betreut und zum Kauf veranlasst werden.

Für eine detailliertere Anzeige dieser Zahlen steht in der Rechnungsverwaltung die Filter nach Rechungs- bzw. Statistikadresse zur Verfügung.

<a name="Zahlungsmoral"></a>

#### Zahlungsmoral des Kunden
Neben der Umsatzanzeige wird in den Kunden-Kopfdaten auch die Zahlungsmoral des Kunden angezeigt. Für die Berechnung der Zahlungsmoral werden nur Rechnungen im Status bezahlt, also keine offenen und keine teilbezahlten, berücksichtigt.
Zahltage sind Zahlungsdatum abzüglich Rechnungsdatum. In der Standardeinstellung werden die letzten drei Rechnungen der letzten zwölf Monate. Siehe dazu auch System, Parameter, ZAHLUNGSMORAL_ANZAHL_RECHNUNGEN und ZAHLUNGSMORAL_MONATE. Die Umschaltung von Rechnungs- auf Statistikadresse wirkt auch für die Information zur Zahlungsmoral.

#### Monatsrechnung
Ist dieses Feld angehakt, so wird bei der Erstellung einer Ausgangsrechnung der Hinweis ausgegeben, dass der Kunde nur eine Rechnung im Monat erhalten sollte. Diese Funktion dient Kundenvereinbarungen, bei denen der Einkauf durch den Kunden nach Bedarf erfolgt und die Abrechnung ein Mal im Monat erfolgen sollte. 

<a name="Rechnungsadresse"></a>

#### Rechnungsadresse
Im Reiter Rechnungsadresse kann die Rechnungsadresse dieses Kunden zugeordnet werden.
Üblicherweise werden Sie dafür einen bereits bestehenden Kunden übernehmen ![](Rechnungsadresse_aus_Kunde.gif), um immer auf den gleichen Kunden zu zeigen. Dieser Verweis wird für das automatische Vorbesetzen der Rechnungsadresse einer Lieferadresse verwendet. **Hinweis:** Wurde keine Rechnungsadresse beim Kunden hinterlegt, so ist Rechnungsadresse gleich Lieferadresse. Das wiederum bewirkt, dass bei einer Änderung der Lieferadresse, z.B. im Lieferschein, automatisch die Rechnungsadresse mitgeändert wird. Dieses Verhalten deutet darauf hin, dass die Rechnungsadressen der Lieferadressen nicht richtig definiert sind. Bitte korrigieren Sie diese Unstimmigkeiten in Ihrem eigenen Interesse um in Zukunft bessere Daten in Ihrem ERP-System zu haben.

#### Wie wirkt die Adressart?
Im Reiter Kopfdaten kann ![](Kundenadressart.gif) die Adressart dieser Kundenadresse eingestellt werden. Diese Adressart dient einzig Ihrer Information und wird nur, für eine leichtere Übersicht in der Kundenauswahlliste in der ersten Spalte angezeigt.

#### Können die Kunden auch strukturiert angezeigt werden?
Durch aktivieren des Parameters KUNDENAUSWAHL_STRUKTURIERT (stellen auf 1) kann die Verbindung zwischen Rechnungsadresse und Lieferadresse baumartig strukturiert angezeigt werden. In dieser Darstellung wird die Bedeutung der ersten Spalte automatisch errechnet und anstelle der Adressart angezeigt.
Auch hier bedeutet R = Rechnungsadresse, L = Lieferadresse. Diese wird unter der Rechnungsadresse eingerückt dargestellt.
Werden hier Zeilen in Rot dargestellt, so bedeutet dies, dass diese Lieferadresse auf sich selbst als Rechnungsadresse zeigt. Löschen Sie in diesem Falle die bei diesem Kunden hinterlegte Rechnungsadresse, bzw. stellen Sie bitte die Zuordnung richtig.
**Hinweis:**
Bei der Suche nach Firmennamen wird, wenn die Entsprechung für Lieferadressen gefunden wird, immer auch die darüber zugehörige Rechnungsadresse angezeigt.
Wird hingegen nur die Rechnungsadresse gefunden, so werden auch hier die entsprechenden dazugehörenden Lieferadressen mit angezeigt.
![](RE_LF_Adressen.gif)

#### Welche Mehrwertsteuer sollten EU-inländische Kunden hinterlegt haben?
Kunden im EU-Inland (also Kunden die zwar im Ausland sind, dieses Land aber ein EU-Mitglied ist) sollten mit 0% MwSt definiert werden, wenn sie eine UID-Nummer haben.

Für den Fall, dass Ihre Firma ein Finanzamt in diesem Land hat, wird die landesübliche MwSt verrechnet und sollte auch so beim Kunden hinterlegt sein.

Wenn der Kunde keine UID-Nummer besitzt, wird die normale MwSt des Mandantenlandes verrechnet und sollte auch so im Kunden definiert sein.

#### Wie kann ich direkt meinen EMail Client öffnen?
Sind finden neben den EMail Adressen im Kunden und im Lieferanten einen EMail Knopf ![](EMail_Knopf.gif). Durch Klick auf diesen Knopf wird der installierte EMail-Client (Mapi-Client) geöffnet und an diesen die Adresse und die Anrede übergeben.

#### Kann ich direkt die Homepage aufrufen?
Neben der Homepage finden den Homepage Knopf ![](Hompage_Knopf.gif). Durch Klick auf diesen Knopf wird der installierte Browser mit diesem Link gestartet. Dieser ist so aufgebaut, dass wenn am Anfang des Links kein http:// angegeben ist, dieses zusätzlich eingefügt wird. Ansonsten wird die Adresse so wie eingegeben an den Browser übergeben.

#### Kann ich direkt den Kunden anrufen?
Neben der Telefonnummer, sowohl in den Kopfdaten als auch bei den Ansprechpartnern finden Sie neben der Telefonnummer den Telefon-Knopf ![](TAPI_Knopf.gif). Wenn Ihre Tapi-Anbindung eingerichtet ist, wird durch Klick auf diesen Knopf eine telefonische Verbindung mit der gewünschten Nummer hergestellt. Bitte beachten Sie dazu auch die Einstellungen des Parameters AMTSLEITUNGSVORWAHL_TELEFON.

#### Die Kundenadresse hat sich geändert, ich muss noch eine alte Rechnung ausdrucken.
Wurde bei einem Kunden die Adresse, eventuell der gesamte Firmenwortlaut geändert so werden über die Datenbank-Relationen und aus Gründen der Konsistenz sofort alle an diesem Kunden hängenden Belege (Angebot bis Rechnung usw.) mitgeändert. D.h. ab sofort finden Sie den Kunden unter sein nun vergebenen Namen. Sollte es erforderlich sein, dass trotzdem Belege mit alten Daten ausgedruckt werden müssen, so steht dafür die [Dokumentenablage]( {{<relref "/docs/stammdaten/system/dokumentenablage" >}} ) zur Verfügung. Hier sind alle Versionen der ausgedruckten Belege abgespeichert.

#### Kundenliste, Kundenjournal
Finden Sie im Modul Kunde, Journal, Kundenliste.

#### Kann ich meine Kundendaten exportieren?
Ja. Rufen Sie wie oben beschrieben die Kundenliste auf und drucken die Liste in die Vorschau.

Klicken Sie nun auf den CSV-Export ![](Kundenjournal_CSV_Export.gif) und speichern Sie die die Datei mit der Erweiterung .Txt ab. Weitere Vorgehensweise siehe bitte wie unter [CSV-Export]
( {{<relref "/docs/stammdaten/system/drucken/#csv-export" >}} ) beschrieben.

Hintergrund: Das Kundenjournal ist unter anderem dafür gedacht, dass für eine weitere Verarbeitung der Daten diese in einer möglichst breiten und flachen Struktur an eine Tabellenkalkulation übergeben werden kann.

Da der Export über die Druckfunktion erfolgt musste dieses Formular genau in dieser Form aufgesetzt werden.

Wie wird nun diese Datei richtig importiert ?

1.) Speichern Sie diesen Report durch Klick auf ![](Kundenjournal_speichern.gif) und wählen Sie bei Dateityp das CSV Format ![](Kundenjournal_Dateityp_definieren.gif). Geben Sie einen für Sie passenden Namen und den richtigen Speicherort an. In unserem Beispiel wird die Datei auf *c:\mk1.csv* gespeichert. Bedenken Sie dass der Datei automatisch die Endung .CSV gegeben wird.

2.) Starten Sie nun z.B. Excel und wählen Sie Datei öffnen. Gehen Sie in das entsprechende Verzeichnis und setzen Sie den Cursor auf die Datei, in unserem Beispiel auf MK1.csv. Nun benennen Sie die Datei in eine Textdatei um. In unserem Beispiel in MK1.txt. (Hinweis: Nur dadurch wir in Excel die Definitionsmöglichkeit für den Datenimport aktiviert).

3.) Stellen Sie nun den Textkonvertierungsassistenten auf Getrennt und setzen Sie den Importbeginn auf die 2.Zeile

4.) Stellen Sie das Trennzeichen auf , Komma.

![](Kundenjournal_Import1.gif)

5.) Definition des Dezimalzeichens und der numerischen Spalten

![](Kundenjournal_Import2.gif)

Unter weiter kann das Dezimaltrennzeichen und das tausender Trennzeichen definiert werden.

Stellen Sie das Dezimaltrennzeichen auf . (Punkt) und das tausender Trennzeichen auf ' (Hochkomma).

Definieren Sie nun alle üblicherweise nur Ziffern beinhaltenden Spalten als Text.

Diese Spalten sind üblicherweise: Postfach, Lieferantennummer, die Postleitzahlen.

6.) Klicken Sie nun auf Fertigstellen.

Damit haben Sie Ihre Kundenliste importiert.

Mit dieser Kundenliste übergeben wir möglichst alle Datenfelder die für Sie eventuell von Interesse sein könnten.

Diese Felder sind:

| **Feld** | **Beschreibung** |
| --- |  --- |
| Anrede |  |
| Titel |  |
| Name1 |  |
| Name2 |  |
| Name3 |  |
| Kurzbezeichnung |  |
| Strasse |  |
| Land |  |
| PLZ | Postleitzahl |
| Ort |  |
| Telefon |  |
| Fax |  |
| Email |  |
| Homepage |  |
| UID Nummer |  |
| AnsprechpartnerAnrede |  |
| AnsprechpartnerTitel |  |
| AnsprechpartnerVorname |  |
| AnsprechpartnerNachname |  |
| AnsprechpartnerTelefonDW |  |
| AnsprechpartnerEMail |  |
| AnsprechpartnerFaxDW |  |
| AnsprechpartnerMobil |  |
| AnzahlRechnungenHeuer |  |
| AnzahlRechnungenVorjahr |  |
| UmsatzHeuer |  |
| UmsatzVorjahr |  |
| ABC |  |
| AbwUstLand |  |
| Debitorenkonto |  |
| Erloeskonto |  |
| Firmenbuch |  |
| Gerichtsstand |  |
| Interessent |  |
| Sprache |  |
| Kostenstelle |  |
| Kreditlimit |  |
| LandPostfach |  |
| PlzPostfach |  |
| OrtPostfach |  |
| Postfach |  |
| LetzteBonitaetspruefung |  |
| Lieferantennr |  |
| Lieferart |  |
| Mwstsatz |  |
| Partnerart |  |
| Partnerklasse |  |
| Preisliste |  |
| Provisionsempfaenger |  |
| RechnungsadresseAndrede |  |
| RechnungsadresseName1 |  |
| RechnungsadresseName2 |  |
| RechnungsadresseName3 |  |
| RechnungsadresseKurzbezeichnung |  |
| RechnungsadresseLand |  |
| RechnungsadresseLandPostfach |  |
| RechnungsadresseOrt |  |
| RechnungsadresseOrtPostfach |  |
| RechnungsadressePlz |  |
| RechnungsadressePlzPostfach |  |
| RechnungsadressePostfach |  |
| RechnungsadresseStrasse |  |
| RechnungsadresseTitel |  |
| RechnungsadresseUidNummer |  |
| Selektion01 |  |
| Selektion02 |  |
| Selektion03 |  |
| Selektion04 |  |
| Selektion05 |  |
| Selektion06 |  |
| Selektion07 |  |
| Selektion08 |  |
| Selektion09 |  |
| Selektion10 |  |
| Selektion11 |  |
| Selektion12 |  |
| Selektion13 |  |
| Selektion14 |  |
| Selektion15 |  |
| Spediteur |  |
| Waehrung |  |
| Zahlungsziel |  |
| Zession |  |
| Briefanrede | Die ausformulierte Anrede des Ansprechpartners um diese z.B. für ein Direktmailing zu verwenden. |

Zusätzlich werden alle Ansprechpartner des Kunden mit exportiert, wobei der Export so ist, dass für jeden Ansprechpartner ein eigener Datensatz exportiert wird.

Weiters werden die ersten 15Selektion des Kunden mit exportiert, damit Sie für eventuelle Kampagnen diese zusätzlichen Informationen zur Verfügung haben.

Warum wird nicht direkt nach Excel exportiert?

Wenn Sie den direkten Excel Export anstoßen, so werden die Daten genau so exportiert, wie diese am Schirm dargestellt werden. Dies ist in unserem Falle leider nicht zielführend.

<a name="Rabatte mitdrucken"></a>

#### Wie kann das Drucken der Rabatte gesteuert werden ?
Für manche Kunden ist es wichtig Rabatte auf den Ausgangspapieren zu sehen. Andere Kunden wollen / sollen keine Rabatte sehen. Dies kann je Kunde unter Kunde, Konditionen, ![](Rechnungsdruck_mit_Rabatt.gif) gesteuert werden. Ist hier ein Hacken gesetzt, so werden in den offiziellen Papieren die Rabatte angedruckt. Fehlt der Hacken, so werden bereits die Einzelpreise der Position mit berücksichtigtem Rabatt ausgegeben.

#### Zessionsfaktor
Es kann je Kunde der Zessionsfaktor hinterlegt werden.
Mit sogenannten Zessionslisten werden Forderungen, z.B. im Rahmen eines Factoring an die Bank verkauft.
Da es nun vorkommt, dass der Zessionsgeber (die Bank) nicht alle Ihre offenen Forderungen anerkennt, z.B. da dieser Kunde ein mit Ihnen verbundenes Unternehmen ist, können Sie den Zessionsfaktor hinterlegen. Geben Sie hier den Prozentsatz ein, zu dem der Zessionsgeber Ihre Forderung an den Kunden akzeptiert.
Dazu gehört auch: Definition des Zessionstextes, siehe dazu System, Parameter, ZESSIONSTEXT.

Was ist eine Zession?

Zession bedeutet, dass, meistens verdeckt, die offenen Forderungen an ihre / eine Bank verkauft werden. Z.B. eine Factoring Bank. Nun gibt es durchaus Kunden, die von Ihrer Bank nicht akzeptiert werden oder auch Kunden, die ein Zessionsverbot ausgesprochen haben. Daher können Sie mit dem Zessionsfaktor jenen Betrag eintragen, mit dem Ihre offene Forderung an den Kunden von Ihrer Bank akzeptiert wird, um so im Vorhinein bereits die erlaubte Ausnützung Ihres Kontokorrent-Kreditrahmens errechnen zu können.

#### Max. RE-Pos.?, je LF?
[Siehe dazu]( {{<relref "/verkauf/rechnung/#k%c3%b6nnen-alle-lieferscheine-auf-einmal-verrechnet-werden" >}} ).

#### Kunde kann nicht gespeichert werden.
Wenn beim Speichern eines Kunden, z.B. neu aus Partner, in einen neu angelegten Mandanten folgende Fehlermeldung kommt, so prüfen Sie bitte folgende Punkte und stellen Sie sicher, dass hier die Kostenstelle des Mandanten eingetragen ist:

- System, Mandant, Lieferkonditionen, Kostenstelle

- Personal, der angemeldete Benutzer, Detail, Heimatkostenstelle und Abteilung

Die Fehlermeldung zeigt im Detail folgenden Text an:

Die INSERT-Anweisung verstieß gegen die TABLE FOREIGN KEY-Einschränkung 'FK_LP_KOSTENSTELLE_PART_KUNDE_KOSTENSTELLE_I_ID_2'. Der Konflikt trat in der LP-Datenbank, Tabelle 'LP_KOSTENSTELLE' auf.

<a name="Kundennummer"></a>

#### Können auch Kundennummern verwaltet werden?
Der grundsätzliche Gedanke von **Kieselstein ERP** ist, dass Kunden und auch Lieferanten, Partner/Menschen/Firmen sind. D.h. diese Partner haben Namen. Daher wollen wir diese auch mit Namen ansprechen. Nun gibt es aus der Praxis die Situation, dass es, gerade bei großen Ketten z.B. im Lebensmittelbereich, viele gleichlautende Unternehmen gibt, welche sich nur durch den Ort (und das nicht immer) unterscheiden. Hier kommen Kundennummern zum Einsatz.
Um Kundennummern zu verwenden müssen diese im System, Parameter, KUNDE_MIT_NUMMER eingeschaltet (=1) werden.
Öffnen Sie nun das Kundenmodul, so werden im Reiter Konditionen Kundennummern verlangt. In der Kundenauswahl wird die Kundennummer anstelle der Debitorennummer angezeigt. Als vierter Direktfilter kann die Kundennummer anstelle der Debitorennummer eingegeben werden.
Beim Anlegen eines neuen Kunden wird automatisch die nächst höhere Kundennummer vergeben.
Sollten Sie zum Zeitpunkt des Einschaltens der Kundennummer bereits Kunden angelegt haben, so empfiehlt es sich, zuerst für alle bestehenden Kunden die Kundennummer nachzutragen, damit die Vergabe neuer Kundennummern entsprechend automatisch erfolgen kann.
Die Kundennummer kann im Partner, XLS-Import unter KDKundennummer als reine Zahl eingelesen werden.

#### Es werden immer zu hohe / falsche Kundennummern vorgeschlagen?
Dies hat die Ursache in eventuell nur einer falsch vergebenen Kundennummer. Um diese zu finden, gehen Sie wie folgt vor:

a.) Haken Sie plus Versteckte an

b.) Klicken Sie auf die Spalte Kundennummer. Nun ist die Auswahlliste der Kunden nach der Kundennummer sortiert.

c.) Gehen Sie nun ganz ans Ende dieser Liste. Sie finden hier die falschen Kundennummern. Korrigieren Sie diese entsprechend.

<a name="Kundennummer0"></a>

#### Ich würde gerne für manche Kunden eine kurze Kennung / Nummer vergeben?
Nutzen Sie dafür die Kunden-Kurznummer.
Diese finden Sie im Modul Kunden, Reiter Konditionen und hier Generiere Kurznr.
![](Kurznummer.gif)
Durch Klick auf generiere Kurznr. wird eine laufende Nummer erzeugt. Diese ist so aufgebaut, dass das erste Zeichen, die erste Stelle, dem ersten Zeichen des Kundennamens entspricht. Danach wird laufend zweistellig hochgezählt. Zusätzlich kann die Kurznummer auch manuell dreistellig vergeben werden. Bitte beachten Sie, dass beim generieren der Kurznummer eine eventuell bereits vorhandene manuell eingetragene Nummer überschrieben wird.

## Kunden zusammenführen

Beim Kunden im Menü gibt es die Möglichkeit zwei Kunden zusammenzuführen.
"Kunden zusammenführen" bedeutet, dass zwei Kunden zu einem Kunden verschmolzen werden. Dies kann z.B. dann vorteilhaft sein, wenn ein Kunde mehrfach angelegt und bereits verwendet wurde, sodass Abhängigkeiten zu anderen Daten wie z.B. Aufträgen, Rechnungen, usw. entstanden sind, und der Kunde nicht mehr ohne Weiteres gelöscht werden kann. Kunden kann man nur dann zusammenführen, wenn man das dafür nötige Rollenrecht besitzt (<PART_KUNDE_ZUSAMMENFUEHREN_ERLAUBT>). **Wichtig:** Das Zusammenführen zweier Kunden kann nicht rückgängig gemacht werden!

Vorgehen:
1.  In Kunden > Auswahl aus der Liste einen Kunden auswählen (diese Auswahl kann später noch geändert werden)
2.  Kundenmenü > Kunden zusammenführen... (Nun öffnet sich ein neuer Dialog, in welchem die Daten des gerade ausgewählten Kunden auf der linken Seite aufgelistet werden)
3.  Rechten Button "Kunde auswählen..." betätigen um den zweiten Kunden auszuwählen
4.  Zu erhaltende Daten selektieren (Siehe [Erklärung zum Dialog](#Erklärungen zum Zusammenführen-Dialogfenster))
5.  Button "Kunden zusammenführen" (links oben - grüner Haken) betätigen.
    Nach erfolgreichem Zusammenführen wird der Dialog automatisch geschlossen und die Kundenauswahlliste angezeigt.

### Erklärungen zum Zusammenführen-Dialogfenster</a>:
<a name="Erklärungen zum Zusammenführen-Dialogfenster">
Die beiden ausgewählten Kunden werden gegenübergestellt. Zwischen den Kundendaten befinden sich Pfeil-Buttons. Die Daten, die erhalten bleiben, werden farblich hinterlegt.
- Pfeil nach links (<-): Beim Zusammenführen bleiben die Daten des linken Kunden erhalten.
- Pfeil nach rechts (->): Beim Zusammenführen bleiben die Daten des rechten Kunden erhalten.
- Pfeil nach links und rechts (<->) (z.B. Kommentar/Hinweis): Die Daten des rechten Kunden werden an die Daten des linken Kunden angehängt. Wenn allerdings die Höchstzahl der zulässigen Zeichen überschritten wird, werden die Daten des rechten Kunden abgeschnitten.
Sind die Pfeilbuttons nicht aktivierbar, so bedeutet dies, dass die Daten bei beiden Kunden identisch sind.

Oberhalb der gegenübergestellten Kundendaten gibt es einen zentralen Pfeil-Button, welcher betätigt werden kann, um alle Daten des linken Kunden oder alle Daten des rechten Kunden zu übernehmen.

Spezialfeld: Kundesoko<br>
Hier steht die Anzahl der Einträge, die bei dem Kunden als Sonderkonditionen eingetragen sind. Es bleiben im System nur die gewählten Sokoeinträge erhalten. Die Sokoeinträge des zweiten Kunden werden gelöscht.
 **Hinweis:** Es werden immer die blau hinterlegten Felder in den verbleibenden Kunden übernommen.
Siehe dazu auch [Partner zusammenführen]( {{<relref "/docs/stammdaten/partner/#partner-zusammenf%c3%bchren" >}} ).

### Kunden importieren
Siehe dazu bitte [Partner CSV Import]( {{<relref "/docs/stammdaten/partner/import">}} ).

#### Kontaktmanagement
[Siehe]( {{<relref "/docs/stammdaten/partner/kontaktmanagement">}} )

#### Kreditlimit abschalten / aktivieren / anzeigen
Es kann für jeden Kunden ein entsprechendes Kreditlimit im Reiter Konditionen hinterlegt werden. Bei einer Neuinstallation ist diese auf 1.000,- Mandantenwährung eingestellt. Das standard Kreditlimit kann im System, Parameter, Kreditlimit (Kunden) eingestellt werden.

Für die Berechnung des Kreditlimits wird die Summe der offenen Rechnungen und die Summe der offenen verrechenbaren Lieferscheine verwendet. Übersteigt dieser Wert das eingetragene Kreditlimit erfolgt eine entsprechende Warnung.

**Hinweis:**

Ist beim Kunden (Konditionen) kein Kreditlimit definiert, so wird auch keine Warnung angezeigt.

In Firmen, welche den Zahlungseingang nicht pflegen bzw. keinen Rückimport der offenen Posten aus der FiBu Schnittstelle verwenden, ist die Anzeige der offenen Ausgangsrechnungen selbstredend falsch. Daher sollte in diesen Unternehmen die Warnung abgeschaltet werden, also bei den einzelnen Kunden kein Kreditlimit eingetragen werden. Damit für neu angelegte Kunden kein Kreditlimit eingetragen wird, tragen Sie bitte im System unter Parameter für das KREDITLIMIT in der Kategorie KUNDE -1 (minus eins) ein.

#### Anzeige der offenen Ausgangsrechnungen, der noch nicht verrechneten Lieferscheine
Im Kunden werden in den Kopfdaten unter Offene RE, LS die netto Summe der offenen (Teil-)Rechnungen und die netto Summe der noch nicht verrechneten aber verrechenbaren Lieferscheine in Mandantenwährung angezeigt.

![](Kunde_offene_Rechnungen.gif)

#### Abweichendes Umsatzsteuerland ?
[Siehe]( {{<relref "/management/finanzbuchhaltung/fibu_export/#abweichendes-umsatzsteuerland"  >}} )

<a name="Sonderkonditionen"></a>

#### Sonderkonditionen, Kunden-Artikelnummern
Die Sonderkonditionen dienen dafür, für bestimmte ausgewählte Kunden eine gänzlich andere Preisgestaltung zu ermöglichen, als über die Technik der Verkaufspreislisten mit Verkaufsmengenstaffeln. Zusätzlich können hier auch:

-   Kundenspezifische Artikelnummern
    Diese werden unter "Ihre Identnummer" auf den Kundenbelegen (AG-RE) angedruckt

-   Kundenspezifische Artikelbezeichnungen
    Diese werden automatisch in die Zeilen der Artikelbezeichnungen einkopiert und wie ein übersteuerter Artikel-Positions-Text behandelt.

-   Fixpreise in Kundenwährung
    Ist der Kunde nicht in Ihrer Mandantenwährung definiert, so kann bei Fixpreis der gewünschte Verkaufspreis in der Kundenwährung angegeben werden.

hinterlegt werden.

Wird die Definition nur für Artikelgruppen vorgenommen so können nur entsprechende Rabatte angegeben werden.
Wird zusätzlich ein Artikel einer bereits definierten Rabattgruppe angegeben, so hat dies Vorrang vor der Artikelgruppe.

Beachten Sie bitte, dass für Berechnung des Preises anhand der Artikelgruppen die für den Kunden eingestellte Verkaufspreisliste als grundsätzliche Basis der Preisberechnung herangezogen wird. **<u>Hinweis:</u>** Artikel die Nicht rabattierbar sind können in den Sonderkonditionen zwar definiert werden, werden aber für die Rabattberechnungen nicht berücksichtigt. Um dies zu verdeutlichen, werden einzelne Artikel in der Liste der Sonderkonditionen, die nicht rabattierbar sind in rot dargestellt.
Das nicht rabattierbar hat den Vorteil, dass insbesondere in Artikelgruppendefinitionen einzelne Artikel aus der Rabattierung ausgenommen werden können.
<u>**Hinweis2:**</u> Sonderkonditionen und Fremdwährungen.
Gerade wenn Sie mit Kunden, die eine andere Währung als Ihre eigene Mandantenwährung haben, Sonderkonditionen vereinbaren, geht es in aller Regel darum, dass Sie den Preis in dieser fremden Währung definieren, also fixieren wollen. Darum ist bei Fremdwährungskunden nur eine Eingabe der Fixpreise erlaubt. Es bedeutet dies natürlich auch, dass Sie das Währungsrisiko tragen müssen.
Aktuell ist es zusätzlich aber so, dass bei Sonderkonditionen auf Artikelgruppen, trotzdem NUR Rabatte vergeben werden können, welche sich auf die Verkaufspreisbasis beziehen. Hier wird ausschließlich ein Rabatt auf Ihren Listenpreis in Ihrer Währung gegeben, das Wechselkursrisiko also auf den Kunden abgewälzt.

#### Wie können Sonderkonditionen nur für die Artikelnummer und Bezeichnung verwendet werden?
Wenn die Sonderkonditionen nicht zur Preisgestaltung verwendet werden, sondern zur Zuordnung der Kundenartikelnummern zu Ihren Artikelnummern, so finden Sie bei jedem Eintrag in dern Sonderkonditionen die Möglichkeit einen Haken bei ![](wirkt_nicht_Preis.JPG) wirkt nicht in Preisfindung setzen.

Wenn dies angehakt ist, wird der Eintrag von der Preisfindung ausgeschlossen, die Kundenartikenummer + Bezeichnung in den Belege jedoch trotzdem angezeigt. Für die Grundeinstellung des Hakens nutzen Sie bitte den Parameter DEFAULT_KUNDESOKO_WIRKT_NICHT_IN_PREISFINDUNG und setzen diesen auf 1, falls Sie die Haken automatisch setzen wollen. Bitte beachten Sie jedoch, dass diese Funktion erst ab der **Kieselstein ERP** Version 8845 verfügbar ist - eventuell schon vorher bestehenden Einträge müssen entsprechend angepasst werden.

#### Wie wirken die Sonderkonditionen in den Modulen?
Die Ermittlung der Sonderkonditionen bezieht sich immer auf die Rechnungsadresse. D.h. wenn die für den Kunden zutreffenden Preise die in einen Lieferschein eingetragen werden, werden immer aus der in den Lieferschein Kopfdaten angegebene Rechnungsadresse genommen.
Analog wird beim Auftrag vorgegangen. Auch hier kommen die Sonderkonditionen aus der im Auftrag hinterlegten Rechnungsadresse.
In Rechnung bzw. Gutschrift und Angebot kommen die Sonderkonditionen von der Belegadresse.

#### Können die Sonderkonditionen auch vom Artikel aus gepflegt werden?
Es steht auch die umgekehrte Pflege der Sonderkonditionen zur Verfügung. D.h. es gibt im Artikel einen Reiter Sonderkonditionen in dem die Zuordnung des Artikels zu den verschiedenen Kunden dargestellt und bearbeitet werden kann.

#### Wie kann ich Sonderkonditionen vom einen auf den anderen Kunden kopieren ?
Um die Sonderkonditionen eines Kunden auf einen anderen Kunden kopieren zu können gehen Sie wie folgt vor:

-   Wählen Sie den Kunden von dem die Soko kopiert werden sollten

-   markieren Sie die gewünschten Soko-Definitionen / Zeilen und klicken Sie auf ![](Soko_in_Zwischenablage.gif) kopieren in die Zwischenablage

-   Wechseln Sie nun zu dem Kunden, bei dem diese Soko eingefügt werden sollten und klicken Sie auf ![](Soko_aus_Zwischenablage.gif) aus Zwischenablage übernehmen

Damit sind die Sonderkonditionen von einem zum anderen Kunden übertragen. Bitte beachten Sie in jedem Falle dass die Währungen Ihrer Kunden gleich sein müssen.

#### Gibt es eine Übersicht über alle Kunden-Sonderkonditionen?
Siehe dazu bitte: Artikel, Journal, Kundensonderkonditionen.
Filtern Sie hier nach den gewünschten Kriterien.
Für die Sonderkonditionen eines einzelnen Artikels siehe bitte ebenfalls Modul Artikel, den gewünschten Artikel auswählen, Info, Kundensonderkonditionen
Um dies aus der Sicht eines einzelnen Kunden darzustellen, wählen Sie bitte Kundenmodul, den gewünschten Kunden auswählen, Info, Kundenpreisliste

#### Abbuchungs- und Ziellager
[siehe Lieferschein]( {{<relref "/verkauf/lieferschein/#von-welchem-lager-bucht-der-lieferschein-ab" >}} ).

#### Lieferdauer
Das ist die Zeit die die Ware von der Fertigstellung bis zum Eintreffen beim Kunden benötigt. D.h. der angegebene Liefertermin im Auftrag ist der Eintrefftermin, abzüglich der üblichen Lieferdauer ergibt dies den Termin wann die Produktion fertig sein muss - also den Endetermin des Loses. Hinterlegen Sie zum Beispiel bei Kunden aus dem Ausland die längere Lieferdauer um zum Beispiel den Postweg in der Planung berücksichtigen zu können.

#### Reverse Charge
Dieses Feld dient der Erfassung des Vorschlagswertes für die ReverseChargeArt für neue Rechnungen. D.h. mit dieser ReverseCharge Einstellung wird eine neue Rechnung dieses Kunden angelegt. Die tatsächliche Verwendung wird in der Rechnung definiert und kann jederzeit, in Abhängigkeit des Statuses der Rechnung geändert werden.
Bitte beachten Sie auch den Parameter REVERSE_CHARGE_VERWENDEN. Ist dieser auf 0 = abgeschaltet, so steht die Erfassung der Reversechargearten nicht zur Verfügung und es werden die Rechnungen immer ohne Reversecharge erfasst / gebucht.

#### Mindestbestellwert / Mindestauftragswert
In manchen Installationen wird für die meisten Kunden ein Mindestauftragswert vorgeschrieben.
Das bedeutet, dass bei einem Mindestbestellwert (siehe Kunde, Konditionen) von z.B. 150,- € die Differenz vom eigentlichen Auftragswert auf den Mindestbestellwert in der Auftragsbestätigung / dem Angebot automatisch dazugerechnet wird, sodass sich als Nettobetrag der Mindestbestellwert ergibt.
Definieren Sie dafür bitte beim Kunden den gewünschten Mindestbestellwert. Es kann zusätzlich der default Mindestbestellwert in den Parametern (im System) DEFAULT_MINDESTBESTELLWERT angegeben werden. Ist dieser > 0 so wird für jeden neuen Kunden dieser Mindestbestellwert eingetragen. Bitte beachten Sie bei Fremdwährungskunden, dass Sie diesen Wert entsprechend in der Währung des Kunden definieren müssen.
Wird der Mindestbestellwert verwendet, so muss auch der Mindestbestellwert Artikel definiert sein. Damit erhalten Sie eine Statistik wieviel Sie mit dem Mindestbestellwert zusätzlich lukrieren und können dies auch bei der Überleitung in die Finanzbuchhaltung entsprechend kontieren. Bei der Anlage des Mindestbestellwert Artikel beachten Sie bitte, dass dieser im Reiter Sonstiges auf nicht Lagerbewirtschaftet gestellt werden sollte. Hinterlegen Sie die Artikelnummer dieses Artikels im Parameter MINDESTBESTELLWERT_ARTIKEL.
Die Berechnung wurde so gelöst, dass sich der Mindestbestellwert auf den Nettowert der Auftragsbestätigung bezieht. Allfällige allgemeine Rabatte werden NICHT berücksichtigt.
Es wird auch im Angebot der Mindestbestellwert nicht berücksichtigt. Dies sollten Sie gegebenenfalls in Ihren Angebotstexten berücksichtigen.

<a name="Mindermengenzuschlag"></a>

#### Mindermengenzuschlag
Im Gegensatz zum Mindestbestellwert gibt es immer wieder auch die Forderung des Mindermengenzuschlages. Dieser ist in der Regel auch gestaffelt, also je nach Rechnungswert mehr oder weniger hoch. Um den Mindermengenzuschlag verwenden zu können definieren Sie bitte im Modul Rechnung, unterer Reiter Grunddaten, oberer Reiter Mindermengenzuschlag die Werte und Artikel welche zur Anwendung kommen sollten. Beachten Sie bitte, dass die angeführten Artikel in der Regel nicht lagerbewirtschaftet sein werden. Üblich ist auch, dass je nach Rechnungswert die Transportkosten ab einem gewissen Rechnungswert vom Lieferanten, in diesem Falle also Ihnen, dem **Kieselstein ERP** Anwender, übernommen werden.
![](MMZ1.gif)
D.h. definieren Sie bis zu welchem Rechnungswert automatisch welche zusätzlichen Kosten verrechnet werden. So bedeutet obige Definition, dass ab 250,- € der nächst höhere oder kein Mindermengenzuschlag mehr verrechnet wird.
Bitte beachten Sie, dass für jede Neuberechnung einer Rechnung zuvor die in der Mindermengenzuschlagstabelle angeführten Artikel aus der Rechnung entfernt werden und Aufgrund des Ergebnisses dann automatisch wieder eingebucht werden.

Der Mindermengenzuschlag wird nur angewandt, wenn eine Rechnung weder direkt noch indirekt, über den Lieferschein, einen Bezug zu einem Auftrag hat.
Bei Auftrag und Angebot und definiertem Mindermengenzuschlag erscheint bei der Aktivierung des Beleges ein Hinweis. Ob und welche Zuschläge Sie verrechnen muss von Ihnen im Angebot / Auftrag ausgeführt werden.
Wird eine Rechnung angelegt, so wird die Einstellung des Mindermengenzuschlages aus dem Kunden, Reiter Konditionen übernommen. Gegebenenfalls kann er für die einzelne Rechnung, ebenfalls im Reiter Konditionen (der Rechnung) aktiviert oder deaktiviert werden.

Bei der Definition des Mindermengenzuschlages am Kunden beachten Sie bitte, dass eine Kombination von Mindestbestellwert und Mindermengenzuschlag nicht erlaubt ist. Entscheiden Sie sich bitte für einen der Werte.
Zusätzlich kann mit dem Parameter DEFAULT_MINDERMENGENZUSCHLAG definiert werden, ob bei der Neuanlage von Kunden dieser vorbesetzt sein sollte.

Um länderspezifische Mindermengenzuschläge zu erhalten, steht dafür die Länderdefinition im Mindermengenzuschlag zur Verfügung.
![](MMZ_Land.gif)
Das bedeutet, wenn ein Mindermengenzuschlag für ein bestimmtes Land definiert ist, so hat dieser Vorrang vor der allgemeinen Definition.

Wie kann nun eine Definition gemacht werden, die im Inland günstiger / anders ist, als ins Ausland, also in andere Länder.
Definieren Sie einfach den Mindermengenzuschlag ohne Land für die teuren Auslandssendungen und definieren Sie die Inlandszuschläge mit Ihrem Land.

**Info:**<br>
Der Mindermengenzuschlag wird gerne auch für die Verrechnung von Verpackungskosten oder Versandkosten verwendet. Da hier die gleiche Logik üblich ist.

#### Warum unterscheiden sich die Ergebnisse der Kundenumsatzstatistik und der Ausgangsrechnungsstatistik?
Die Unterschiede kommen wie folgt zustande:
In der Kundenumsatzstatistik ist für den Datumsbereich das Datum der Rechnung ausschlaggebend, d.h. Wenn ein LS am 30.4\. erstellt wurde und am 5.5\. verrechnet wurde, ist dieser im Datumsbereich 1.5\. - 31.5 enthalten und zählt noch nicht zum Umsatz des April.
Im Artikelgruppen-Journal wird der Lieferschein nur berücksichtigt, wenn das LS-Datum im Datums-Filter-bereich liegt.
Außerdem enthält die Kundenumsatzstatistik keine 'noch nicht verrechneten'- Lieferscheine, das Artikelgruppen-Journal jedoch schon.
Wenn man diese Unterschiede (und etwaige kleine Rundungsbeträge) vorneweg nimmt (bzw. herausrechnet), dann müssen die Beträge des Ausgangsrechnungsstatistik, Warenausgangsjournal, die Kundenumsatzstatistik und die Artikelgruppenstatistik übereinstimmen.

#### Wie können Provisionsempfänger komfortabel geändert werden?
Es kommt immer wieder vor, dass auch Vertriebsmitarbeiter ausscheiden. Nun sollte dies natürlich nicht dazu führen, dass Angebote nach außen hin noch von diesen Provisionsempfängern geschrieben werden. Da üblicherweise ein ehemaliger Provisionsempfänger durch jemand anderen ersetzt wird, finden Sie im Kunden, im Menü unter Pflege auch Provisionsempfänger ![](Provisionsempfaenger1.gif).
In der nachfolgenden Liste werden alle in Ihrem Kundenstamm zugeordneten Provisionsempfänger aufgelistet.
![](Provisionsempfaenger2.jpg)
Wählen Sie nun den zu ändernden Provisionsempfänger aus und klicken Sie auf den Personalauswahl.
Damit werden alle diesem Provisionsempfänger zugeordneten Kunden auf den nun gewählten Provisionsempfänger geändert.

<a name="Liefersperre"></a>

#### Setzen / löschen von Liefersperren
Leider gibt es auch immer wieder Kunden, die aus den verschiedensten Gründen nicht mehr beliefert werden sollten.
Dies kann im Kunden im Reiter Konditionen im Feld Liefersperre eingetragen werden, bzw. herausgelöscht werden.
Die Liefersperre bewirkt, dass bei jedem Verkaufsbeleg an diesen Kunden ein entsprechender Hinweis erscheint, dass dieser Kunde nicht beliefert werden sollte.
Derzeit ist keine weitere Verriegelung mit Benutzerrechten enthalten. D.h. es liegt in der Verantwortung des Benutzers, ob dieser Kunde, trotz der Liefersperre ein Angebot, eine Lieferung bekommt.
Nutzen Sie gegebenenfalls auch den Parameter KUNDENHINWEIS_AB_MAHNSTUFE.
Zu diesem Thema siehe auch [Mahnsperre]( {{<relref "/verkauf/rechnung/#mahnsperren" >}} ) einer Rechnung bzw. [automatische Liefersperre]( {{<relref "/verkauf/rechnung/mahnen/#k%c3%b6nnen-kunden-ab-einer-gewissen-mahnstufe-automatisch-gesperrt-werden" >}} ).

#### Verwalten von kundenspezifischen Spediteuren, Account Nummern
In einigen Anwendungsfällen ist es erwünscht, dass die Belieferung der Kunden über bestimmte, meist vom Kunden vorgegebene Transportdienstleister erfolgt.<br>
D.h. es werden für den jeweiligen Kunden, bestimmte Spediteure definiert und meist dann auch die Kundennummern Ihres Kunden beim Spediteur als sogenannte Account No. hinterlegt.
Die Definition erfolgt in folgender Reihenfolge:
- a.) Definieren Sie alle Transportdienstleister mit denen Sie / Ihre Kunden zusammenarbeiten. (System, Mandant, Spediteur)
- b.) Im Modul Kunde wird im Reiter Konditionen der übliche Spediteur definiert.
- c.) ergänzend werden im Reiter Spediteur alle für den Kunden vereinbarten Spediteure mit deren Accounting Nummern angeführt.<br>
![](Spediteur.gif)<br>
Zusätzlich kann auch als Information das Gewicht angegeben werden bis zu dem dieser Spediteur Waren liefert.
- d.) Bei der Anlage eines Angebotes bis durch in die Rechnung/Gutschrift wird der unter Konditionen definierte Spediteur übernommen. Im Reiter Konditionen des jeweiligen Moduls kann durch Klick auf den Button Spediteur aus der Liste der für den Kunden definierten Spediteure ausgewählt werden.<br>
![](Spediteur_auswaehlen.gif)<br>
Hier kann zusätzlich auch, durch abhaken von "Nur Spediteure des Kunden", aus allen definierten Spediteuren ausgewählt werden.
- e.) Beim Druck des Beleges (Angebot bis Gutschrift) wird neben dem Spediteur auch die Accounting Nr. mit an den  Report übergeben um diese an geeigneter Stelle andrucken zu können.

#### VK-Preis kommt aus LS Datum
[Siehe]( {{<relref "/verkauf/auftrag/#kunden-vk-preis-kommt-aus-ls-datum"  >}} )