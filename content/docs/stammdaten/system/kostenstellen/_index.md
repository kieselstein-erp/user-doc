---
categories: ["Kostenstellen"]
tags: ["Kostenstellen"]
title: "Kostenstellen"
linkTitle: "Kostenstellen"
date: 2023-01-31
weight: 055
description: >
  Was sind nun wirklich Kostenstellen?
---
Kostenstellen
=========

Nachdem der Begriff der Kostenstellen immer wieder unterschiedlichst verwendet wird, hier der Versuch einer Aufstellung und deren Bedeutung / Umsetzung im deinem **Kieselstein ERP**.

### Profitcenter
Mit den im **Kieselstein ERP** realisierten Kostenstellen wird die Profitcenter Denke umgesetzt. D.h. wenn es im eigenen Unternehmen wirklich getrennte Bereiche gibt, z.B. werden einerseits Fahrräder produziert und andererseits Flugzeugteile, so wären das sehr weit getrennte Bereich, die aber im gleichen Unternehmen abgebildet werden. Das sind unterschiedlichen Proficenter und damit Kostenstellen in deinem **Kieselsein ERP**. Das geht bis zu unterschiedlichem Briefpapier deiner Formulare.

### Jede Maschine ist eine andere Kostenstelle
Dies wird gerne in der Metallverarbeitung verwendet.<br>
Gemeint ist damit, dass man die Maschinen als Kosten betrachtet und darauf verschiedene Dinge buchen kann. Nutze im **Kieselstein ERP** dafür die Maschinenverwaltung aus dem Modul Zeiterfassung.<br>
Wenn es darum geht auch die ganzen Reparatur- und Instandhaltungskosten für die Maschinen zu erfassen, so wird gerne ein Los je Maschine dafür verwendet. D.h. du buchst damit die Zeit für die Instandhaltungsarbeiten aber auch die Aufwände für die Reparaturen auf das Los.<br>
Idealerweise sind natürlich die Standard Wartungsmaterialien (Öl, ..) als Verbrauchsmaterial definiert. Somit stimmen Lager und Kosten (am Los).<br>
Ev. kennzeichnest du das Los noch über die Losklassen als Instandhaltungslos und hast somit weitere Auswertemöglichkeiten.

Der Vorteil ist, dass die Maschine ja nicht nur Kosten und Erlöse hat, sondern auch Eigenschaften, wie Zeitmodelle, einen Leistungsfaktor und ähnliches. Und natürlich wird eine Maschine auch in den Stücklisten eingeplant.

Kostenstellen
=============

Kostenstellen werden in **Kieselstein ERP** in Form der Profitcenterbetrachtung verwendet.

Leider ist der Begriff Kostenstellen mit  so vielen Bedeutungen belegt, dass es erforderlich ist diesen Begriff klar zu definieren.

Möglich Bedeutungen für Kostenstellen und deren Entsprechung in **Kieselstein ERP**.

| Art | Bedeutung |
| --- |  --- |
| Maschine | Maschinen werden gerade in der Metallverarbeitung aber durchaus auch in anderen Bereichen sehr häufig als Kostenstelle bezeichnet, da ja nur Kosten verursacht werden. Der Verdeutlichung halber sprechen wir hier von Maschinen und Maschinengruppen. |
| Aufwandskostenstelle |  |
| Ertragskostenstelle |  |
| Profitcenter | Diese Betrachtung wird in **Kieselstein ERP** abgebildet. Damit kann erreicht werden, dass in einem Unternehmen, einem Mandanten verschiedene Unternehmensbereiche so behandelt werden, als wenn sie eigenständig agieren würden. Beispiel: Ein Unternehmen handelt mit Fahrrädern und mit Motoren. Üblicherweise werden viele Artikel bei den gleichen Lieferanten eingekauft und an die gleichen Kunden verkauft. Trotzdem sind dies zwei völlig unterschiedliche Bereich, die auch getrennt betrachtet werden müssen. Daher stehen für die Kostenstelen in allen offiziellen Modulen jeweils eigene Betrachtungen zur Verfügung um so den Ertrag jeder Kostenstelle = Profitcenter genau analysieren zu können. |