---
categories: ["Parameter"]
tags: ["Parameter"]
title: "Parameter"
linkTitle: "Parameter"
date: 2023-01-31
weight: 050
description: >
  Liste der Parameter
---
Parameter
=========

Die umfangreichen Parametrierungsmöglichkeiten von **Kieselstein ERP** bieten die Möglichkeit Ihre Installation auf Ihre individuellen Anforderungen anzupassen. 
Bitte bedenken Sie vor der Änderung von Parametern, dass dies weitläufige Auswirkungen haben kann.
Generell sollte nach einer Änderung von Parametern, der Client neu gestartet werden. 

Bei Fragen wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer!

Grundsätzlich gelten die Einstellungen der Parameter ab dem Zeitpunkt der Änderung. Wenn Sie nun einen Parameter umstellen, so starten Sie den Client neu und die Änderung ist gültig.
Im Gegensatz dazu gibt es auch zeitabhängige Parameter. Falls ein Parameter zeitabhängig ist, so finden Sie einen weiteren Reiter ![](werte_zum_Zeitpunkt.JPG) Werte zum Zeitpunkt.
Klicken Sie diesen Reiter an und hinterlegen die jeweils gültigen Werte. Definieren Sie zum Beispiel Fertigungsgemeinkosten für den Zeitpunkt ab 1.1.2015.

![](parameter_zeitabhaengig.JPG)

Zusätzlich zu den Parametern des [Mandanten](#Parameter Mandant) gibt es auch noch die [Anwenderparameter](#Parameter Anwender) und die [Arbeitsplatzparameter](#Arbeitsplatz Parameter).
Die Anwenderparameter gelten für die gesamte **Kieselstein ERP** Installation.
Die Arbeitsplatzparameter gelten für den jeweiligen Arbeitsplatz.

Hier finden Sie eine Liste der derzeit von **Kieselstein ERP** unterstützten Parameter:

# Parameter Mandant
<a name="Parameter Mandant"></a>

## Kategorie ALLGEMEIN
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ABSENDERKENNUNG_STRASSE_ORT  | ALLGEMEIN  | 0/1 | Absenderkennung im Format Name, Strasse Lkz-Plz-Ort |
| ADRESSE_FUER_AUSDRUCK_MIT_ANREDE  | ALLGEMEIN  | 0/1 | Anrede mitandrucken, wenn keine Firma vorhanden ist |
| AMTSLEITUNGSVORWAHL  | ALLGEMEIN  |  | Amtsleitungsvorwahl zum direkten Faxversand |
| AMTSLEITUNGSVORWAHL_TELEFON  | ALLGEMEIN  |  | Amtsleitungsvorwahl für Telefon (Direktes wählen/TAPI) |
| ANREDE_MIT_VORNAME  | ALLGEMEIN  | 0/1 | Briefanrede mit Vorname |
| ANZEIGE_ANLEGER_STATT_VERTRETER  | ALLGEMEIN  | 0/1/2 | 0-Zeigt den Vertreter<br>1-Zeigt den Anleger<br>2-Zeigt den Änderer |
| ARTIKELLANGTEXTE_UEBERSTEUERBAR  | ALLGEMEIN  | 0/1 | In den Bewegungsmodulen sind [Artikellangtexte übersteuerbar](../artikel/index.htm#uebersteuerbar) |
| AUSLANDSVORWAHL  | ALLGEMEIN  |  | Vorwahl für Ausland zb. 00 |
| AUSWAHLLISTE_NUR_OFFENE_DEFAULT  | ALLGEMEIN  | 0/1 | Die Standardbelegung der Checkbox "nur offene" in den Auwahllisten |
| AUSWAHLLISTE_VERSTECKTE_ANZEIGEN_DEFAULT  | ALLGEMEIN  | 0/1 | Die Standardbelegung der Checkbox "versteckte anzeigen" in den Auswahllisten |
| BELEGNUMMER_MANDANTKENNUNG  | ALLGEMEIN  |  | Kennung des Mandanten in der BelegnummerACHTUNG: Änderungen an dieser Struktur dürfen nur bei einer Neuinstallation vorgenommen werden. |
| BELEGNUMMERNFORMAT_STELLEN_BELEGNUMMER  | ALLGEMEIN  |  | Anzahl der Stellen der laufenden NummerACHTUNG: Änderungen an dieser Struktur dürfen nur bei einer Neuinstallation vorgenommen werden. |
| BELEGNUMMERNFORMAT_STELLEN_GESCHAEFTSJAHR  | ALLGEMEIN  |  | Anzahl der Stellen des Geschäftsjahres. ACHTUNG: Änderungen an dieser Struktur dürfen nur bei einer Neuinstallation vorgenommen werden.  |
| [BELEGNUMMERNFORMAT_STELLEN_ZUFALL](../Rechnung/index.htm#Zufallsnummer) | ALLGEMEIN  |  | Anzahl der Stellen am Ende der Belegnummer, welche aus einer Zufallszahl bestehen (Bei AG/AB/LS/RE/GS) |
| BELEGNUMMERNFORMAT_TRENNZEICHEN  | ALLGEMEIN  |  | Trennzeichen zwischen Geschäftsjahr und laufender Nummer. ACHTUNG: Änderungen an dieser Struktur dürfen nur bei einer Neuinstallation vorgenommen werden. |
| BEWEGUNGSMODULE_ANLEGEN_BIS_ZUM  | ALLGEMEIN  | 1-31 | Belege dürfen bis zum angegebenen Tag im alten Monat angelegt werden. |
| BEWEGUNGSMODULE_SORTIERUNG_PARTNER_ORT  | ALLGEMEIN  | 0/1 | Sortierung nach Partner und Ort in den Bewegungsmodulen möglich |
| DAZUGEHOERT_BERUECKSICHTIGEN  | ALLGEMEIN  | 0/1 | Den -[Zugehoerigen Artikel](../artikel/index.htm#zugehoeriger Artikel)- in allen Belegen berücksichtigen |
| DEFAULT_ARBEITSZEITARTIKEL  | ALLGEMEIN  |  | Default-[Arbeitszeitartikel](../artikel/index.htm#Arbeitszeit) für BDE (Artikelnummer) |
| DEFAULT_DOKUMENTE_ANHAENGEN  | ALLGEMEIN  | 0/1 | Default-Wert der CheckBox -Dokumente anhängen- im Lieferschein-Versand. |
| DELAY_E_MAILVERSAND  | ALLGEMEIN  |  | Verzögerungszeit des Versands von E-Mails in Minuten |
| DIMENSIONSERFASSUNG_VK | ALLGEMEIN | 0/1 | Aktiviert die Eingabe der Abmessungen in den Verkaufsbelegen |
| DOKUMENTE_MAXIMALE_GROESSE  | ALLGEMEIN  |  | Maximale Dateigröße von Dokumenten die hinterlegt werden in KB |
| DRUCKVORSCHAU_AUTOMATISCH_BEENDEN  | ALLGEMEIN  | 0/1 | Druckvorschau nach Druck beenden |
| EDITOR_BREITE_KOMMENTAR  | ALLGEMEIN  | 400 | Breite des Texteditors bei Kommentaren |
| EDITOR_BREITE_SONSTIGE  | ALLGEMEIN  | 520 | Standardbreite des Texteditors |
| EDITOR_BREITE_TEXTEINGABE  | ALLGEMEIN  | 470 | Standardbreite des Texteditors |
| EDITOR_BREITE_TEXTMODUL  | ALLGEMEIN  | 520 | Breite des Texteditors bei Textmodulen |
| EINKAUFSPREISABWEICHUNG  | ALLGEMEIN  |  |  |
| ENTWICKLUNGSGEMEINKOSTENFAKTOR  | ALLGEMEIN  |  | Gemeinkostenfaktor für Entwicklungkosten |
| FERTIGUNGSGEMEINKOSTENFAKTOR  | ALLGEMEIN  |  | Gemeinkostenfaktor für Fertigungskosten |
| GEMEINKOSTENFAKTOR  | ALLGEMEIN  |  | Allgemeiner Gemeinkostenfaktor |
| GESCHAEFTSJAHRBEGINNMONAT  | ALLGEMEIN  | 1-12 | Monat in dem das Geschäftsjahr beginnt |
| GESCHAEFTSJAHRPLUSEINS  | ALLGEMEIN  | 0/1 |  |
| GESTPREISBERECHNUNG_HAUPTLAGER  | ALLGEMEIN  | 0/1 | 1- Für Berechnung des Gestehungspreises Hauptlager herangezogen 0- Gestehungspreis berechnet sich aus dem Mittelwert der Gestehungspreise aller Läger |
| HTTP_PROXY | ALLGEMEIN  | HTTP Proxy host[:port] | Bitte geben Sie den HTTP Proxy Host (sofern notwendig) ein. Optional kann auch der Port mittels ":portnummer" angegeben werden |
| INLANDSVORWAHL  | ALLGEMEIN  |  | Inlandsvorwahl. z.B. 0 |
| KOSTENSTELLE_IN_VK_BELEGEN_VORBESETZT | ALLGEMEIN | 0/1 | Kostenstelle in VK-Belegen vorbesetzt |
| LAENGE_PARTNER_KURZBEZEICHNUNG  | ALLGEMEIN  |  | Länge des Feldes Kurzbezeichnung in Partner/Kunde/Lieferant. Grösstmöglicher Wert: 40. |
| LEERZEILE_NACH_MENGENBEHAFTETERPOSITION  | ALLGEMEIN  | 0/1 | Am Belegdruck automatisch eine Leerzeile nach einer mengenbehafteten Position einfügen |
| LOGO_IMMER_DRUCKEN  | ALLGEMEIN  | 0/1 | Das Logo auf Belegen immer drucken |
| LOGON_LOESCHEN  | ALLGEMEIN  |  | Anzahl der Tage, nach der Logon-Einträge gelöscht werden |
| LV_POSITION  | ALLGEMEIN  | 0/1 | LV-Position in VK-Belegen anzeigen (Leistungsverzeichnis) |
| MATERIALGEMEINKOSTENFAKTOR  | ALLGEMEIN  |  | Gemeinkostenfaktor fuer Materialkosten |
| MAXIMALELAENGE_DURCHWAHL_ZENTRALE  | ALLGEMEIN  |  | Anzahl der Stellen der Durchwahl der Zentrale |
| MENGE_UI_NACHKOMMASTELLEN  | ALLGEMEIN  | 0/1/3 | Anzahl der Nachkommastellen für Mengenfelder im User Interface (UI) |
| NACHKOMMASTELLEN_DIMENSIONEN | ALLGEMEIN  |  | Nachkommastellen der Dimensionen |
| PARTNERBRANCHE_DEFINIERT_KOMMENTAR  | ALLGEMEIN  | 0/1 | Es werden die Kommentare der entsprechenden Branche angedruckt |
| POSTVERSENDUNGSLAND  | ALLGEMEIN  | 0/1 | Beim Absenderland für P{Adressefuerausdruck} wird das angegebene Land verwendet (anstatt des Landes des Mandanten) |
| PREISERABATTE_UI_NACHKOMMASTELLEN  | ALLGEMEIN  | 2/4 | Anzahl der Nachkommastellen für Preis- und Rabattfelder im UI |
| PREISERABATTE_UI_NACHKOMMASTELLEN_EK  | ALLGEMEIN  | 2/4 | Anzahl der EK-Nachkommastellen für Preis- und Rabattfelder im UI |
| PREISERABATTE_UI_NACHKOMMASTELLEN_VK | ALLGEMEIN  | 2/4 | Anzahl der VK-Nachkommastellen für Preis- und Rabattfelder im UI |
| PREISERABATTE_UI_NACHKOMMASTELLEN_WE | ALLGEMEIN  | 2/4 | Anzahl der WE-Nachkommastellen für Preis- und Rabattfelder im UI |
| PROJEKT_IST_PFLICHTFELD | ALLGEMEIN | 0/1 | Das Projekt ist in den VK-Belegen ein Pflichtfeld. |
| SCHWERWIEGENDE_FEHLER_VERSENDEN  | ALLGEMEIN  | 0/1 | Schwerwiegende Fehlermeldungen an **Kieselstein eG** versenden |
| SIZE_E_MAILANHANG  | ALLGEMEIN  |  | Die maximale Größe aller Anhänge einer E-Mail in KB |
| SOLLZEITPRUEFUNG  | ALLGEMEIN  | 0-3 | [Sollzeitprüfung](../Zeiterfassung/Zeiterfassungsterminal_Nettop.htm#Sollzeitprüfung) in den Zeitdaten:<br>0=Aus,<br>1=Nur Lose,<br>2=Auftragspsition,<br>3=Auftrag gesamt |
| SUCHEN_INKLUSIVE_KBEZ  | ALLGEMEIN  | 0/1 | Wenn nach dem Filterkriterium -Firma- in Partner/Kunde/Lieferant gesucht wird, wird die Kurzbezeichnung ebenfalls durchsucht. |
| UNTERDRUECKE_ARTIKELNR_NULLTAETIGKEIT  | ALLGEMEIN  | 0/1 | Eine Arbeitsplanposition mit diesem Artikel in Kombination mit einer Maschine wird am Fertigungsbegleitschein nicht angedruckt. |
| URL_ANWENDERSPEZIFISCHE_HILFE  | ALLGEMEIN  |  | URL der [anwenderspezifischen Hilfe](../Allgemein/Bedienung_der_Hilfe.htm#anwenderspezifische Hilfe) |
| VERPACKUNGSMENGEN_EINGABE | ALLGEMEIN  | 0/1 | Eingabe der Verpackungsmengen in den Belegpositionen 0 - Keine Verpackungsmengeneingabe1 - Verpackungsmenge kommt im VK aus Artikel-Sonstiges und im EK aus Artikellieferant |
| VERTRETER_VORSCHLAG_AUS_KUNDE  | ALLGEMEIN  | 0/1 | Anzeigen der Vertreter in Angebot- bzw. Auftragskopfdaten aus Kunden |
| VERTRIEBSGEMEINKOSTENFAKTOR  | ALLGEMEIN  | 0/1 | Gemeinkostenfaktor für Vertriebskosten |
| VERWALTUNGSGEMEINKOSTENFAKTOR  | ALLGEMEIN  | 0/1 | Gemeinkostenfaktor für Verwaltungskosten |
| VKPREISEINGABE_NUR_NETTO  | ALLGEMEIN  | 0/1 | In den VK-Belegen kann nur der Netto-Preis eingegeben werden |
| WAEHRUNGKURSAELTERALS  | ALLGEMEIN  | 0/1 |  |
| ZESSION  | ALLGEMEIN  | 0/1 |  |
| ZESSIONSTEXT  | ALLGEMEIN  | 0/1 |  |
| ZUSAETZLICHER_DOKUMENTENSPEICHERPFAD  | ALLGEMEIN  | 0/1 | Dokumente werden zusätzlich in Verzeichnis abgelegt (Achtung: Vertrauliche Dokumente ebenso) |

## Kategorie: ANFRAGE ( {{<relref "/einkauf/anfrage" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ANFRAGE_ANSPRECHPARTNER_ANDRUCKEN  | ANFRAGE  | 0/1 | Ansprechpartner im Adressblock andrucken |
| ANFRAGE_ANSPRECHPARTNER_VORBESETZEN  | ANFRAGE  | 0/1 | Ansprechpartner nach Auswahl des Lieferanten vorbesetzen |
| ANFRAGE_BELEGNUMMERSTARTWERT  | ANFRAGE  |  | Startwert der Belegnummer im neuen Geschäftsjahr |
| MENGEN_IN_STAFFELN_UEBERNEHMEN  | ANFRAGE  | 0/1 | Anlieferdaten mit Mengen >1 werden in die EK-Mengenstaffeln übernommen |
| ANFRAGEDATUM_VORBESETZEN  | ANFRAGE  | 0/1 | Anfragedatum vorbesetzen |

## Kategorie [ANGEBOT]( {{<relref "/verkauf/angebot" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ANGEBOT_AG_STKL_AUFLOESUNG_TIEFE  | ANGEBOT  |  | Tiefe der Stücklistenauflösung am AG Druck.0 = die Stückliste wird ohne Positionen angedruckt1 = die Positionen der Stückliste werden ohne weitere Auflösung angedruckt2 = die Positionen der Stückliste werden mit der ersten Ebene der Unterpositionen angedruckt, usw. |
| ANGEBOT_AG_VORKALK_AUFLOESUNG_MAX_TIEFE  | ANGEBOT  | 0-99 | Maximale Tiefe der Auflösung von Stücklisten und Angebotstücklisten zusammen in der Angebotsvorkalkulation |
| ANGEBOT_AGVORKALK_STKL_AUFLOESUNG_TIEFE  | ANGEBOT  |  | Tiefe der Stücklistenauflösung am AG Vorkalkulation Druck.0 = die Stückliste wird ohne Positionen angedruckt,1 = die Positionen der Stückliste werden ohne weitere Auflösung angedruckt2 = die Positionen der Stückliste werden mit der ersten Ebene der Unterpositionen angedruckt, usw. |
| ANGEBOT_ANSPRECHPARTNER_ANDRUCKEN  | ANGEBOT  | 0/1 | Ansprechpartner im Adressblock andrucken |
| ANGEBOT_ANSPRECHPARTNER_VORBESETZEN  | ANGEBOT  | 0/1 | Ansprechpartner nach Auswahl des Kunden vorbesetzen |
| ANGEBOT_BELEGNUMMERSTARTWERT | ANGEBOT  |  | Startwert der Belegnummer im neuen Geschäftsjahr |
| DEFAULT_ANGEBOT_GUELTIGKEIT  | ANGEBOT  |  | Default für die Gültigkeit eines Angebots in Tagen ab Belegdatum |
| DEFAULT_ANGEBOT_GUELTIGKEITSART  | ANGEBOT  |  | 0 =  Das Angebot gilt zum Ende des laufenden Geschäftsjahres<br>1 = Das Angebot gilt ab Belegdatum für DEFFAULT_ANGEBOT_AGUELTIGKEITs Tage |
| DEFAULT_ANGEBOT_PRUEFESTANDARDDBARTIKEL  | ANGEBOT  | 0/1 | Benutzerabfrage beim Abspeichern einer Ident Position, wenn der DB des Artikels geringer ist, als der Standard DB des Artikels beim Mandanten |
| DEFAULT_LIEFERZEIT_ANGEBOT  | ANGEBOT  |  | Default- Lieferzeit im Angebot in Tagen |
| DEFAULT_MIT_ZUSAMMENFASSUNG  | ANGEBOT  | 0/1 | Default-Wert der Option "[mit Zusammenfassung](../Angebot/index.htm#Mit Zusammenfassung)" in AG+AB |
| DEFAULT_NACHFASSTERMIN  | ANGEBOT  |  | Default-Nachfasstermin in Tagen |
| KONDITIONEN_DES_BELEGS_BESTAETIGEN  | ANGEBOT  | 0/1 | Konditionen müssen vor dem erstmaligen Aktivieren des Angebots bestätigt werden |
| LIEFERANT_ANGEBEN | ANGEBOT | 0/1 | Lieferant in Angebots- und Auftragsposition angebbar. |

## Kategorie [ANGEBOTSSTUEKLISTE]( {{<relref "/verkauf/angebotsstueckliste" >}} )**
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ANGEBOTSSTUECKLISTE_ANSPRECHPARTNER_VORBESETZEN  | ANGEBOTSSTUECKLISTE  | 0/1 | Ansprechpartner nach Auswahl des Kunden vorbesetzen |
| EK_PREISBASIS | ANGEBOTSSTUECKLISTE  | 0/1 | 0=Lief1Preis, 1=Nettopreis |
| DEFAULT_AUFSCHLAG | ANGEBOTSSTUECKLISTE  | 30 | Default-Aufschlag in Prozent, wenn KALKULATIONSART= 3  |
| KALKULATIONSART  | ANGEBOTSSTUECKLISTE  | 0/1 | 1 = Verkaufspreisbezug,<br>2 = Einkaufspreisbezug,<br>3= Einkauf mit Aufschlag (../Angebotstueckliste/index.htm#kalkulationsart) |

## Kategorie [ARTIKEL]( {{<relref "/docs/stammdaten/artikel" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE  | ARTIKEL  | 0/1 | Abmessungen statt Zusatzbezeichnung in der Artikel-Auswahlliste anzeigen |
| AELTESTE_CHARGENNUMMER_VORSCHLAGEN  | ARTIKEL  | 0/1 | Automatisch die älteste Charge vorgeschlagen |
| ANZEIGEN_ARTIKELGRUPPE_IN_AUSWAHLLISTE  | ARTIKEL  | 0/1 | Artikelgruppe in der Artikel-Auswahlliste anzeigen. Wirkt nur, wenn der Parameter ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE=0 |
| ANZEIGEN_ARTIKELKLASSE_IN_AUSWAHLLISTE  | ARTIKEL  | 0/1 | Artikelklasse in der Artikel-Auswahlliste anzeigen. Wirkt nur, wenn der Parameter ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE=0 und ANZEIGEN_ARTIKELGRUPPE_IN_AUSWAHLLISTE=0 |
| ANZEIGEN_KURZBEZEICHNUNG_IN_AUSWAHLLISTE  | ARTIKEL  | 0/1 | Kurzbezeichnung in der Artikel-Auswahlliste anzeigen |
| ANZEIGEN_LAGERPLATZ_IN_AUSWAHLLISTE  | ARTIKEL  | 0/1 | Lagerplaetze in Artikelauswahl anzeigen |
| ANZEIGEN_REFERENZNUMMER_IN_AUSWAHLLISTE | ARTIKEL  | 0/1 | Referenznummer in der Artikel-Auswahlliste anzeigen. |
| ANZEIGEN_ZUSATZBEZ2_IN_AUSWAHLLISTE | ARTIKEL  | 0/1 | Zusatzbezeichnung2 in der Artikel-Auswahlliste anzeigen. |
| ARTIKEL_DEFAULT_CHARGENNUMMERNBEHAFTET  | ARTIKEL  | 0/1 | Artikel ist per Default Chargennummernbehaftet |
| ARTIKEL_DEFAULT_SERIENNUMMERNBEHAFTET  | ARTIKEL  | 0/1 | Artikel ist per Default Seriennummernbehaftet |
| ARTIKEL_LAENGE_HERSTELLERBEZEICHNUNG  | ARTIKEL  |  | Artikel Länge Herstellerbezeichnung |
| ARTIKELGEWICHT_GRAMM_STATT_KILO | ARTIKEL  | 0/1 | Artikelgewicht in Gramm ausgeben |
| ARTIKELGRUPPE_IST_PFLICHTFELD  | ARTIKEL  | 0/1 | In den Artikelkopfdaten ist die Artikelgruppe ein Pflichtfeld (besonders für Erlöskontenfindung bei der Fibu wichtig) |
| ARTIKELGRUPPE_NACH_CBEZ_ODER_CNR_ANZEIGEN | ARTIKEL  | 0/1 | Artikel-Auswahlliste nach Kennung sortieren (0 - Sortiert nach Bezeichnung, 1 - Sortiert nach Kennung). |
| ARTIKELGRUPPE_NUR_VATERGRUPPEN_ANZEIGEN | ARTIKEL | 0/1/2 | 0 - Alle Artikelgruppen anzeigen1 - Nur Vatergruppen anzeigen2 - Alle Artikelgruppen anzeigen, Kindgruppen eingerueckt |
| ARTIKELGRUPPE_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE  | ARTIKEL  | 0/1 | Artikelgruppe statt Zusatzbezeichnung in der Artikel-Auswahlliste anzeigen. Wirkt nur, wenn Parameter ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE=0 |
| ARTIKEL_LAENGE_HERSTELLERBEZEICHNUNG | ARTIKEL  | 0-3 |   |
| ARTIKEL_MAXIMALELAENGE_ARTIKELNUMMER  | ARTIKEL  | 0-23 | Maximallänge der Artikelnummer |
| ARTIKEL_MAXIMALELAENGE_SERIENNUMMER  | ARTIKEL  | 0-40 | Maximallänge der Seriennummer |
| ARTIKEL_MINDESTLAENGE_ARTIKELNUMMER  | ARTIKEL  |  | Mindestlänge der Artikelnummer |
| ARTIKEL_MINDESTLAENGE_SERIENNUMMER  | ARTIKEL  |  | Mindestlänge der Seriennummer |
| ARTIKELNUMMER_AUSWAHL_ABSCHNEIDEN | ARTIKEL |  | Vorbesetzte Artikelnummer bei der Artikelauswahl um eine best. Anzahl von Stellen abschneiden. |
| ARTIKELNUMMER_ZEICHENSATZ  | ARTIKEL  | ABCDEFGHIJKLMNOPQRSTUVWXYZ<br>0123456789-_+ | Bestimmt die in einer Artikelnummer verwendbaren Zeichen |
| ARTIKELSUCHE_MIT_HERSTELLER | ARTIKEL  | 0/1 | Textsuche inkl. Hersteller (Bezeichnung und Nummer) |
| ARTIKELWIEDERBESCHAFFUNGSZEIT  | ARTIKEL  |  | Artikel-Wiederbeschaffungszeit in Kalenderwochen oder Tagen. Zulässige Werte: Kalenderwochen und Tage. |
| AUFGELOESTE_FEHLMENGEN_DRUCKEN | ARTIKEL  | 0/1 |   |
| BEGRUENDUNG_BEI_VKPREISAENDERUNG | ARTIKEL  | 0/1 |   |
| CHARGEN_BEI_GENERIERUNG_NICHT_BERUECKSICHTIGEN  |   |   |   |
| CHARGENNUMMER_BEINHALTET_MINDESTHALTBARKEITSDATUM  | ARTIKEL  | 0/1 | Die Chargennummer beinhaltet in den ersten 8 Stellen das Mindesthaltbarkeitsdatum. (JJJJMMTT) |
| CHARGENNUMMER_MINDESTLAENGE  | ARTIKEL  |  | Mindestlänge der Chargennummer |
| DEFAULT_ARTIKEL_ARTIKELART  | ARTIKEL  |  | Default welche Artikelart bei Neuanlegen eines Artikels |
| DEFAULT_ARTIKEL_AUFSCHLAG  | ARTIKEL  |  |  |
| DEFAULT_ARTIKEL_DECKUNGSBEITRAG  | ARTIKEL  |  | Default Deckungsbeitrag bei Artikel in Prozent |
| DEFAULT_ARTIKEL_EINHEIT  | ARTIKEL  | zb. Stk. | Default Einheit bei Neunalegen eines Artikels |
| DEFAULT_ARTIKELKOMMENTAR_IST_HINWEIS  | ARTIKEL  | 0/1/2 | Default Kommentarart ist -Hinweis bei Artikelauswahl- |
| DEFAULT_ARTIKEL_MWSTSATZ  | ARTIKEL  | ID | Default- Mehrwersteuersatz. Hier wird die Id des Mehrwertsteuersatzes aus der Datenbank (Tabelle LP_MWSTSATZ) eingetragen. Die Id muss vom richtigen Mandanten sein. |
| DEFAULT_ARTIKEL_RABATTIERBAR  | ARTIKEL  | 0/1 | Default- Einstellung der Option -Rabattierbar- |
| DEFAULT_ARTIKEL_SOLLVERKAUF  | ARTIKEL  | 0/1 |  |
| DEFAULT_HANDBUCHUNGSART  | ARTIKEL  | 0/1 | Default-Wert für die Buchungsart in der Handlagerbewegung: 0=Zugang, 1=Abgang, 2=Umbuchung |
| DEFAULT_LAGER  | ARTIKEL  | ID | Lager, das als Voreinstellung verwendet wird |
| DIREKTFILTER_GRUPPE_KLASSE_STATT_REFERENZNUMMER  | ARTIKEL  | 0/1 | Direktfilter-Suche nach Artikelgruppe/Artikelklasse, statt Referenznummer. |
| EK_PREISE  | ARTIKEL  |  |  |
| ERLAUBTE_ABWEICHUNG_INVENTURLISTE  | ARTIKEL  |  | Bringt eine Warnung, wenn Abweichung des Inventurstand größer als der Wert ist |
| FINDCHIPS_APIKEY | ARTIKEL  |  | Bitte geben Sie Ihren Apikey der Findchips API an |
| GENERIERE_ARTIKELNUMMER_ZIFFERNBLOCK | ARTIKEL  | 0/1 | Gibt an, welcher Ziffernblock beim generieren herangezogen wird: 0 = der Erste, 1 = der Letzte |
| GERINGEREN_VKPREIS_VORSCHLAGEN  | ARTIKEL  | 0/1 | Wenn aufgrund der hinterlegten Preisinformationen ein geringerer VK-Preis für einen Artikel möglich wäre, als die VK-Preisfindung liefert, erhält der Benutzer einen entsprechenden Hinweis |
| GESTEHUNGSPREISABWERTEN_AB_MONATE  | ARTIKEL  |  | Ab wievielen Monaten wird der Gestehungspreis abgewertet |
| GESTEHUNGSPREISABWERTEN_PROZENT_PRO_MONAT | ARTIKEL  |  | Um wieviel Prozent wird der Gestehungspreis pro Monat abgewertet |
| GS1_BASISNUMMER_GTIN |   |   |
| GS1_BASISNUMMER_SSCC |   |   |
| HANDLAGERBUCHUNG_MENGE_OBERGRENZE  | ARTIKEL  |  | Grenze der Menge, die mit Handlager bewegt werden darf |
| HANDLAGERBUCHUNG_WERT_OBERGRENZE  | ARTIKEL  |  | Grenze des Werts, der mit Handlager bewegt werden darf |
| HERSTELLERKURZZEICHENAUTOMATIK  | ARTIKEL  | 0/1 | Herstellerkurzzeichen wird automatisch vergeben |
| IMMER_VKPREISDIALOG_MENGENSTAFFEL_ANZEIGEN | ARTIKEL  | 0/1 | Immer VK-Preisdialog Mengenstaffel anzeigen (auch bei Menge 1 bzw. wenn keine Staffeln definiert) |
| INVENTUR_BASISPREIS | ARTIKEL | 0/1 | Inventurbasispreis des Inventurstand kommt aus: Gestehungspreis = 0, EK-Preis = 1 |
| KUNDESONDERKONDITIONEN_WEBSHOP_VERWENDEN | ARTIKEL | 0/1 | 0 = Kundensoko nicht an Webshop übermitteln 1 = übermitteln |
| LAENGE_CHARGENNUMMER_EINEINDEUTIG | ARTIKEL  |  | Wenn die Laenge > 0, dann werden in der Losablieferung eineindeutige CHNrs vorgeschlagen. |
| LAGERBEWEGUNG_MIT_URSPRUNG  | ARTIKEL  | 0/1 | Lagerzugang mit Hersteller/Ursprungsland |
| LAGER_IMMER_AUSREICHEND_VERFUEGBAR | ARTIKEL | 0/1 | [Lagerzubuchungsautomatik](../artikel/index.htm#Zubuchungsautomatik) unzureichender Lagerstand |
| LAGER_LOGGIN | ARTIKEL | 0/1 | Loggt jede Zu- bzw. Abgangsbuchung in LP_PROTOKOLL mit |
| LAGERMIN_JE_LAGER | ARTIKEL  | 0/1 | Lagermindest/Sollstand und [Standort]( {{<relref "/einkauf/bestellung/standorte" >}} ) je Lager |
| LAGERSTAND_DES_ANDEREN_MANDANTEN_ANZEIGEN | ARTIKEL  | 0/1 | Lagerstand des anderen Mandanten anzeigen (in Auswahlliste) |
| LAGERSTANDSLISTE_EKPREIS_WENN_GESTPREIS_NULL  | ARTIKEL  | 0/1 | Wenn der Gestehungspreis 0 ist, dann wird stattdessen der EK-Preis des 1\. Lieferanten in der Lagerstandsliste angezeigt |
| LIEF1INFO_IN_ARTIKELAUSWAHLLISTE  | ARTIKEL  | 0/1 |   |
| MATERIALBEDARFSVORSCHAU_OFFSET_REALISIERUNGSTERMIN | ARTIKEL  | 3 |  |
| MATERIAL_IN_AUSWAHLLISTE | ARTIKEL  | 0/1 |   |
| MATERIALKURS_AUF_BASIS_RECHNUNGSDATUM | ARTIKEL  | 0/1 |   |
| MATERIALZUSCHLAG_STATT_NETTOPREIS_BS_MNG_EHT | ARTIKEL  | 0/1 |   |
| NEUER_ARTIKEL_IST_LAGERBEWIRTSCHAFTET  | ARTIKEL  | 0/1 | Default für Lagerbewirtschaftung bei neuen Artikel |
| PRO_FIRST_ARTIKELGRUPPE | ARTIKEL  |   | Schnittstelle für Blechbearbeitung |
| REKLAMATIONSWARNUNG_NUR_BEI_SNR_CHNR | ARTIKEL  | 0/1 | Reklamationswarnnung bei Artikelauswahl nur bei Snr/Chnr-Artikeln |
| RUNDUNGSAUSGLEICH_ARTIKEL  | ARTIKEL  | IDENT | Muss existieren wenn Muenzrundung durchgefuehrt werden soll.ACHTUNG: Definieren Sie eine neue Artikelgruppe! |
| SERIENNUMMER_EINEINDEUTIG  | ARTIKEL  | 0/1 | Eindeutige Seriennumer über alle Artikel |
| SERIENNUMMERN_FUEHRENDE_NULLEN_ENTFERNEN  | ARTIKEL  | 0/1 | Beim Speichern einer SNR werden führende Nullen entfernt |
| SERIENNUMMER_NUMERISCH | ARTIKEL  | 0/1 | Seriennummern sind numerisch |
| SI_EINHEITEN | ARTIKEL |  | [Einheiten für Si-Wert-Erkennung.](../artikel/index.htm#SI-Wert) |
| SI_OHNE_EINHEIT | ARTIKEL |  | [Si-Wert-Erkennung ohne Einheiten verwenden.](../artikel/index.htm#SI-Wert) |
| STARTWERT_ARTIKELNUMMER | ARTIKEL |  | Startwert für das [Generieren](../artikel/index.htm#Artikelnummerngenerator) einer neuen Artikelnummer |
| STATUSZEILE_KBEZ_STATT_VERPACKUNGSART  | ARTIKEL  | 0/1 | Kurzbezeichnung+ Revision statt Verpackungsart+ Bauform in Statuszeile |
| TEXTSUCHE_INKLUSIVE_ARTIKELNUMMER | ARTIKEL  | 0/1 | Bei der Textsuche wird auch die Artikelnummer inkludiert |
| TEXTSUCHE_INKLUSIVE_INDEX_REVISION | ARTIKEL  | 0/1 | Artikeltextsuche inklusive der Felder Index/Revision |
| URSPRUNGSLAND_IST_PFLICHTFELD | ARTIKEL  | 0/1 |   |
| VERFUEGBARKEIT_IN_AUSWAHLLISTE | ARTIKEL  | 0/1 | Verfuegbarkeit in Auswahlliste anzeigen |
| VERKAUFSPREIS_RUECKPFLEGE | ARTIKEL  | 0/1 | Wenn der Parameter aktiviert ist (1), so wird beim Speichern der Auftragsposition die VK-Basis des Artikels zum Auftragsdatum aktualisiert. |
| VERSION_BEI_SNR_MITANGEBEN | ARTIKEL | 0/1 | Version bei SNR-Lagerbuchung mitangeben möglich |
| VK_MENGENSTAFFEL_ANSTATT_SOKO_MENGESTAFFEL | ARTIKEL | 0/1 | Wenn eine Kunden-Soko gefunden wird und eine passende VK-Mengenstaffel gefunden wird, wird der zusaetzliche Rabatt als Zusatzrabatt in den Belegpositionen angefuehrt. Die Soko-Mengenstaffel ist am Client nicht mehr sichtbar. |
| VKPREISBASIS_IST_LIEF1PREIS  | ARTIKEL  | 0/1 | Als Verkaufspreisbasis wird der Preis des ersten Lieferanten verwendet |
| VK_PREIS_BASIS_MATERIAL | ARTIKEL  | 0/1 |   |
| VKPREIS_STATT_GESTPREIS_IN_ARTIKELAUSWAHL  | ARTIKEL  | 0/1 | Zeigt den ersten berechneten VK-Preis anstatt des Gestpreises in der Artikel-Auswahlliste an |
| WARNUNG_WENN_LAGERPLATZ_BELEGT  | ARTIKEL  | 0/1 | Warnung, wenn Lagerplatz bereits durch einen Artikel belegt ist |
| WERBEABGABE_ARTIKEL  | ARTIKEL  | IDENT | Artikelnummer, welche als Werbeabgabe in die Rechnung eingefügt wird |
| WERBEABGABE_PROZENT  | ARTIKEL  |  | Prozent- Wert der Werbeabgabe |
| WERBEABGABE_VOM_EINZELPREIS  | ARTIKEL  | 0/1 | Werbeabgabe vom Einzelpreis anstatt vom Nettopreis |
| WE_REFERENZ_IN_STATISTIK  | ARTIKEL  | 0/1 | Wareneingangsreferenz in Artikelstatistik andrucken |
| ZUGEHOERIGER_KUNDE_IN_BEWEGUNGSVORSCHAU_ANZEIGEN | ARTIKEL  | 0/1 |   |
| ZUSAETZLICHES_LAGER_IN_AUSWAHLLISTE| ARTIKEL  | 0/1 |   |

## Kategorie [AUFTRAG]( {{<relref "/verkauf/auftrag" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| AB_NACHKALKULATION_NUR_RECHNUNGS_ERLOESE | AUFTRAG | 0/1 | [Fuer AB-Nachkalkulation zaehlen nur Rechnungen bzw. verrechnete Lieferscheine.](Fuer%20AB-Nachkalkulation%20zaehlen%20nur%20Rechnungen%20bzw.%20verrechnete%20Lieferscheine.) |
| ABRUFE_DEFAULT_ABRUFART_AUFTRAG  | AUFTRAG  | DIVISOR/ MANUELL/ REST | Vorbesetzung für die [Abrufart](../Bestellung/index.htm#Divisor) beim Erstellen eines neuen Abrufauftrags |
| ADRESSVORBELEGUNG  | AUFTRAG  | 0/1/2 | 0 = nach Häufigkeit1 = Auftragsadresse = Lieferadresse und Rechnungsadresse anhand Auftragsadresse2 = Lieferadresse anhand Häufigkeit und Rechnungsadresse anhand Auftragsadresse |
| ALLE_LOSE_BERUECKSICHTIGEN  | AUFTRAG  | 0/1 | In der Nachkalkulation werden alle Lose berücksichtigt (1) |
| AUFTRAEGE_KOENNEN_VERSTECKT_WERDEN | AUFTRAG | 0/1 | Auftraege koennen mittels Icon versteckt werden. |
| AUFTRAG_ABVORKALK_STKL_AUFLOESUNG_TIEFE  | AUFTRAG  |  | Tiefe der Stücklistenauflösung am AB Vorkalkulation Druck.0 = die Stückliste wird ohne Positionen angedruckt,1 = die Positionen der Stückliste werden ohne weitere Auflösung angedruckt, 2 = die Positionen der Stückliste werden mit der ersten Ebene der Unterpositionen angedruckt, usw. |
| AUFTRAG_ANSPRECHPARTNER_ANDRUCKEN  | AUFTRAG  | 0/1 | Ansprechpartner im Adressblock andrucken |
| AUFTRAG_ANSPRECHPARTNER_VORBESETZEN  | AUFTRAG  | 0/1 | Den Ansprechpartner nach Auswahl des Kunden vorbesetzen |
| AUFTRAG_AUTOMATISCH_VOLLSTAENDIG_ERLEDIGEN | AUFTRAG | 0/1 | Auftrag automatisch vollstaendig erledigen. Wenn Einstellung = 0 können [Auftraege nur manuell erledigt](../auftrag/index.htm#Erledigtstatus) werden, wenn das Recht AUFT_DARF_AUFTRAG_ERLEDIGEN vorhanden ist. |
| AUFTRAG_BELEGNUMMERSTARTWERT  | AUFTRAG  |  | Startwert der Belegnummer im neuen Geschäftsjahr |
| AUFTRAG_JOURNAL_OFFENE_POSITIONEN_STICHTAG  | AUFTRAG  |  | Die Vorbesetzung des Stichtages beim Druck von Auftrag Journal Offene Positionen in Wochen |
| AUFTRAG_KOPIEREN_LIEFERDATUM_UEBERNEHMEN  | AUFTRAG  | 0/1 | Gibt an ob beim Kopieren von Aufträgen das Lieferdatum und der Finaltermin berechnet wird oder aus dem zu kopierenden Auftrag übernommen wird |
| AUFTRAG_LIEFERADRESSE_ANSP_VORBESETZEN  | AUFTRAG  | 0/1/2 | 0 = nicht vorbesetzen, 1 = erster Ansp. aus Lieferadresse, 2 = Ansp aus Auftragsadresse übernehmen |
| AUFTRAG_RECHNUNGSADRESSE_ANSP_VORBESETZEN | AUFTRAG  | 0/1 | 0 = nicht vorbesetzen, 1 = erster Ansp. aus Rechnungsadresse, 2 = Ansp aus Auftragsadresse übernehmen |
| AUFTRAGSERIENNUMMERN_MIT_ETIKETTEN  | AUFTRAG  | 0/1 | Auftrags Seriennummer fortlaufend ab Start mit Etiketten |
|  |  | AUFTRAGSFREIGABE || --- | || --- | | AUFTRAG  | 0/1 |   |
|  |  | AUFTRAGSVERTRETER_AUS_ANGEBOT || --- | || --- | | AUFTRAG  | 0/1 |   |
| DEFAULT_LIEFERZEIT_AUFTRAG  | AUFTRAG  |  | Default- Lieferzeit im Auftrag in Tagen |
| DEFAULT_UNVERBINDLICH  | AUFTRAG  | 0/1 | Default-Wert für -unverbindlich- in den Auftragskopfdaten |
| ETIKETTENDRUCK_NACH_SCHNELLANLAGE | AUFTRAG  | 0/1 | 0 = gar nicht 1 = sofort nach der Auftragsanlage 2 = vor AG-Auswahl |
| KONDITIONEN_DES_BELEGS_BESTAETIGEN  | AUFTRAG  | 0/1 | Konditionen müssen vor dem erstmaligen Aktivieren des Auftrags bestätigt werden |
| LIEFERADRESSE_IST_AUFTRAGSADRESSE  | AUFTRAG  | 0/1 | Wenn Lieferadresse = Auftragsadresse, dann diese nicht drucken |
|  | LIEFERPLAN_TAGE_IN_ZUKUNFT || --- | | AUFTRAG  | 18 |   |
| MINDESTSTANDSWARNUNG  | AUFTRAG  | 0/1 | Lagermindeststandswarnung im Auftrag |
| OFFENE_AUFTRAGSPOSITIONEN_WERKTAGE_IN_ZUKUNFT | AUFTRAG  | 3 |   |
| TRENNZEICHEN_ARTIKELGRUPPE_AUFTRAGSNUMMER | AUFTRAG  | 0/1 | Trennzeichen zwischen Artikelgruppe und Auftragsnummer. z.B. bei Auftragsschnellanlage. |
|  | WARNUNG_WENN_KEIN_PROJEKT_ANGEGEBEN || --- | |   |   |   |
| WIEDERHOLENDER_AUFTRAG_VERSTECKT  | AUFTRAG  | 0/1 | Wenn ein wiederholender Auftrag neu angelegt wird, ist dieser per Default versteckt |
| ZUSATZSTATUS_VERRECHENBAR  | AUFTRAG  | 0/1 | 0 = RoHs wird angezeigt, 1 = Verrechenbar anstatt RoHs |

## Kategorie [AUSGANGSRECHNUNG]( {{<relref "/verkauf/rechnung" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| FRACHTIDENT  | AUSGANGSRECHNUNG  | 0/1 | Sonderkreis für Frachtartikel |
| RECHNUNG_ANSPRECHPARTNER_VORBESETZEN  | AUSGANGSRECHNUNG  | 0/1 | Den Ansprechpartner nach Auswahl des Kunden vorbesetzen |
| RECHNUNG_BELEGNUMMERSTARTWERT  | AUSGANGSRECHNUNG  | 0/1 | Startwert der Rechnungsnummer im neuen Geschäftsjahr |
| WA_NUR_MIT_LS  | AUSGANGSRECHNUNG  | 0/1 | Warenausgang darf ausschließlich mit LS gebucht werden |

## Kategorie [BESTELLUNG]( {{<relref "/einkauf/bestellung" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ABRUFE_DEFAULT_ABRUFART_BESTELLUNG  | BESTELLUNG  | DIVISOR/ MANUELL/ REST | Vorbesetzung für die [Abrufart](../Bestellung/index.3.gif#Divisor) beim erstellen eines neuen Abrufs |
| AUTOMATISCHE_CHARGENNUMMER_BEI_WEP | BESTELLUNG  | 0/1 | 0 = Deaktiviert1 = Bestellnummer-WareneingangsNr-PositrionsNr |
| BESTELLUNG_ANSPRECHPARTNER_ANDRUCKEN  | BESTELLUNG  | 0/1 | Ansprechpartner im Adressblock andrucken |
| BESTELLUNG_ANSPRECHPARTNER_VORBESETZEN  | BESTELLUNG  | 0/1 | Den Ansprechpartner nach Auswahl des Lieferanten vorbesetzen |
| BESTELLUNG_AUS_BESTVOR_POSITIONEN_MIT_LOSZUORDNUNG | BESTELLUNG  | 0/1 | Bestellvorschlag Loszuordnungen erzeugen (1) |
| BESTELLUNG_BELEGNUMMERSTARTWERT  | BESTELLUNG  | 0/1 | Startwert der Belegnummer im neuen Geschäftsjahr |
| BESTELLVORSCHLAG_VERDICHTUNGSZEITRAUM_TAGE  | BESTELLUNG  |  | Vorschlagswert für den Verdichtungszeitraum des [Bestellvorschlags]( {{<relref "/einkauf/bestellung/vorschlag">}} ) in Tagen |
| DEFAULT_ARTIKELAUSWAHL  | BESTELLUNG  | 0/1 | 0 = Lieferant der Bestellung, 1 = Alle |
| DEFAULT_BESTELLVORSCHLAG_UEBERLEITUNG | BESTELLUNG  | 1/2/3/4/5 | [1 = für jeden Lieferant + gl. Termin eine Bestellung](../Bestellung/Bestellvorschlag.htm#Bestellungen_erzeugen)2 = je Lieferant eine Bestellung anlegen3 = ein Lieferant und ein Termin4 = ein Lieferant und alle Positionen des LF in einer BS5 = Abruf zu vorhandenen Rahmen erzeugen |
| DEFAULT_EINKAUFSPREIS_ZURUECKPFLEGEN  | BESTELLUNG  | 0/1/2 | 0 = nicht [zurückpflegen](../Bestellung/index.htm#Bestellpreis zurückpflegen)1 = Staffelpreis zurückpflegen per default angehakt2 = immer nur Einzelpreiszurückpflegen per default angehakt |
| DEFAULT_EMPFANGSBESTAETIGUNG  | BESTELLUNG  | 0/1 | Empfangsbestätigung im Bestellungsdruck (???) |
| DEFAULT_LIEFERZEITVERSATZ  | BESTELLUNG  |  | Default Lieferterminversatz in den Kopfdaten in Tagen (-1 = Kein Vorschlag) |
| DEFAULT_TEILLIEFERUNG  | BESTELLUNG  | 0/1 | Default ob Haken bei Teillieferung gesetzt ist oder nicht |
| DEFAULT_VORLAUFZEIT_AUFTRAG  | BESTELLUNG  |  | [Default Vorlaufzeit](../Bestellung/Bestellvorschlag.htm#Vorlaufzeit) für den Auftrag in Tagen |
| GUENSTIGER_MELDUNG_ANZEIGEN | BESTELLUNG  | 0/1 | Lieferant waere guenstiger- Meldung in Bestellung anzeigen |
| IN_WEP_TEXTEINGABEN_ANZEIGEN  | BESTELLUNG  | 0/1 | Texteingaben in den Wareneingangspositionen anzeigen |
| KONDITIONEN_DES_BELEGS_BESTAETIGEN  | BESTELLUNG  | 0/1 | Konditionen müssen vor dem erstmaligen Aktivieren der Bestellung bestätigt werden |
| LIEFERANTEN_AUTOMATISCH_SORTIEREN  | BESTELLUNG  | 0/1 | Gibt an ob beim Statuswechsel der Bestellung von Angelegt auf Offen der ausgewählte Lieferant als bevorzugter Lieferant hinterlegt wird |
| MAHNUNG_AUTO_CC  | BESTELLUNG  | 0/1 | Absender des Versandauftrages bekommt eine Kopie |
| MAHNUNGSABSENDER  | BESTELLUNG  | 0/1 | Absender der Mahnung: 0= Administrator, 1=Anforderer |
| MONATSBESTELLUNGSART  | BESTELLUNG  | 0/1/2 | 0 = mit automatischem Wareneingang1 = ohne automatischem Wareneingang, 2 = Bestellungsimport |
| OFFSET_LIEFERZEIT_IN_TAGEN  | BESTELLUNG  |  | Anzahl der Tage ab Bestelldatum bis zur Lieferung |
| PREISUEBERWACHUNG  | BESTELLUNG  | 0/1 | Preisüberwachung in der Liste der Bestellpositionen |
| PROJEKT_STATT_ORT_IN_AUSWAHLLISTE  | BESTELLUNG  | 0/1 | Projekt statt Ort in Auswahlliste |
| TOLERANZFRIST_BESTELLVORSCHLAG  | BESTELLUNG  |  | Die [Toleranzfrist](../Bestellung/Bestellvorschlag.htm#Toleranz) in Tagen die bei der Erstellung eines Bestellvorschlages berücksichtigt wird. Gibt an wie viele Tage zu spät eine Lieferung eintreffen darf. |
| URSPRUNGSTERMIN_FUER_LIEFERANTENBEURTEILUNG  | BESTELLUNG  | 0/1 | Aktiviert den [Ursprungstermin](../Bestellung/index.htm#Ursprungstermin) bei der AB-Eingabe von Bestellpositionen unter Sicht Lieferantentermine |
| WARENEINGANG_LAGERPLATZ_NUR_DEFINIERTE | BESTELLUNG  | 0/1 | Beim Druck des WEP-Etiketts koennen nur mehr Lagerplaetze, welche im Artikel definiert sind, ausgewaehlt werden |
| WEP_PREIS_ZURUECKSCHREIBEN | BESTELLUNG  | 0/1 | WEP-Preis automatisch in Artikellieferant zurueckschreiben. |

## Kategorie [EINGANGSRECHNUNG]( {{<relref "/einkauf/eingangsrechnung" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| 4VENDING_ER_AR_IMPORT_BRUTTO_STATT_NETTO | EINGANGSRECHNUNG  | 0/1 |   |
| AUSGANGSGUTSCHRIFT_AN_KUNDE | EINGANGSRECHNUNG  | 0/1 | [Ausgangsgutschrift an Kunde](Ausgangsgutschrift%20an%20Kunde) |
| EINGANGSRECHNUNG_BELEGNUMMERSTARTWERT  | EINGANGSRECHNUNG  | 0/1 | Startwert der ER-Nummer im neuen Geschäftsjahr |
| EINGANGSRECHNUNG_KUNDENDATEN_VORBESETZEN | EINGANGSRECHNUNG  | 0/1 |   |
| EINGANGSRECHNUNG_LIEFERANTENRECHNUNGSNUMMER_LAENGE  | EINGANGSRECHNUNG  |  | Länge der Lieferantenrechnungsnummer, -1 = Buchstaben können auch eingegeben werden |
| EINGANGSRECHNUNG_ZAHLUNG_MIT_KURSEINGABE  | EINGANGSRECHNUNG  | 0/1 | Kurseingabe bei Zahlung Eingangsrechnung |
| SCHECKNUMMER  | EINGANGSRECHNUNG  | 0/1/-1 | Schecknummer wird bei ER-Druck um 1 erhöht. In ER-Druck nicht sichtbar bei -1 |
| ZAHLUNGSVORSCHLAG_DEFAULT_FREIGABE | EINGANGSRECHNUNG  | 0/1 |   |
| ZAHLUNGSVORSCHLAG_EXPORTZIEL  | EINGANGSRECHNUNG  | 0/1 | Default Export-Zieldatei Zahlungen |
| ZAHLUNGSVORSCHLAG_MIT_SKONTO  | EINGANGSRECHNUNG  | 0/1 | Default mit Skonto bezahlen |
| ZAHLUNGSVORSCHLAG_PERIODE  | EINGANGSRECHNUNG  | 0/1 | Default Zeitraum zwischen den Zahlungsläufen in Tagen |
| ZAHLUNGSVORSCHLAG_SKONTOUEBERZIEHUNGSFRIST  | EINGANGSRECHNUNG  |  | Default Skontoüberziehungsfrist in Tagen |

## Kategorie [FERTIGUNG]( {{<relref "/fertigung" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ABLIEFERUNG_BUCHT_ENDE  | FERTIGUNG  | 0/1 | Los-Ablieferung von Terminal bucht automatisch ein ENDE |
| ABLIEFERUNGSPREIS_IST_DURCHSCHNITTSPREIS  | FERTIGUNG  | 0/1 | Der Preis der Losablieferung ist der Durchschnittspreis |
| ANZEIGE_IN_ARBEITSPLAN  | FERTIGUNG  | 0/1 | 0 = h - ms (Stunden bis Milisekunden)1 = t - m (Tage bis Minuten) |
| AUFTRAG_STATT_ARTIKEL_IN_AUSWAHLLISTE  | FERTIGUNG  | 0/1 | Auftrag statt Artikel in Losauswahlliste.Der Parameter KUNDE_STATT_BEZEICHNUNG_IN_AUSWAHLLISTE ist unwirksam, wenn = 1 |
| AUSGABE_EIGENER_STATUS  | FERTIGUNG  | 0/1 | Es gibt den "Ausgegeben" Status vor "In Produktion" |
| AUSGABELISTE_MATERIAL_NULLMENGEN_ANDRUCKEN | FERTIGUNG  | 0/1 |   |
| AUSGABELISTE_TROTZDEM_DRUCKEN | FERTIGUNG  | 0/1 |   |
| AUSLASTUNGSANZEIGEART | FERTIGUNG  | 0/1 | 0 = Maschine |
| AUSLASTUNGSANZEIGE_DETAIL_AG | FERTIGUNG  | 0/1 | Darstellung der Arbeitsgaenge: 0 = Los u. Artikelnr / 1 = Projekt u Artikelkurzbezeichnung |
| AUSLASTUNGSANZEIGE_EINLASTUNGRASTER | FERTIGUNG  | 60 |   |
| AUSLASTUNGSANZEIGE_PUFFERDAUER | FERTIGUNG  | 15 |   |
| AUSLASTUNGSZEITENBERECHNUNG | FERTIGUNG  | 0/1 | 0 = nur Sollzeiten von AGs die noch nicht Fertig sind / 1 = Sollzeiten x (1 - Erfuellungsgrad) |
| AUTOFERTIG_BEI_ABLIEFERUNG_TERMINAL | FERTIGUNG  | 0/1 | Automatische Fertigbuchung bei vollständiger Ablieferung per Terminal |
| AUTOMATISCHE_ERMITTLUNG_AG_BEGINN | FERTIGUNG  | 0/1 | Automatische Ermittlung des AG-Beginn (in Stunden) 0 = Deaktiviert |
| BEGINN_ENDE_IN_AUSWAHLLISTE  | FERTIGUNG  | 0/1 | Los-Beginn und Ende in Auswahlliste buchbar |
| BEI_LOS_ERLEDIGEN_MATERIAL_NACHBUCHEN  | FERTIGUNG  | 0/1 | Bei Erledigung eines Loses wird das [fehlende Material nachgebucht](../Fertigung/index.htm#Materialnachbuchen) |
| BEWERTUNG_IN_AUSWAHLLISTE | FERTIGUNG  | 0/1 | Bewertung in Auswahlliste anzeigen. |
| DEFAULT_VORLAUFZEIT_INTERNEBESTELLUNG  | FERTIGUNG  |  | Vorbesetzung des Feldes [Vorlaufzeit in der internen Bestellung](../Fertigung/Interne_Bestellung.htm#Vorlaufzeit) in Tagen |
| DEFAULT_VORLAUFZEIT_INTERNEBESTELLUNG_UNTERLOSE | FERTIGUNG  | 7 |   |
| EINRICHTELAGER_ERFASSUNGSTERMINAL | FERTIGUNG  | 0/1 |   |
| ENDE_TERMIN_ANSTATT_BEGINN_TERMIN_ANZEIGEN  | FERTIGUNG  | 0/1 | Ende-Termin anstatt des Beginn-Termins anzeigen |
| FEHLMENGE_ENTSTEHEN_WARNUNG  | FERTIGUNG  | 0/1 | Warnung bei der Los-Ausgabe wenn Fehlmengen entstehen würden |
| FEHLMENGEN_LOSBEZOGEN_SOFORT_AUFLOESEN  | FERTIGUNG  | 0/1 | Losbezogene Fehlmengen sofort aufloesen |
| FEHLMENGE_STATT_PREIS_IN_LOSMATERIAL  | FERTIGUNG  | 0/1 | Fehlmenge statt Preis in Los-Material anzeigen |
| FERTIGUNG_FERTIGUNGSBEGLEITSCHEIN_MIT_SOLLDATEN  | FERTIGUNG  | 0/1 | Fertigungsbegleitschein mit Solldaten drucken |
| FERTIGUNG_GROESSENAENDERUNG_ZUSATZSTATUS | FERTIGUNG  | 0/1 | Zusatzstatus wird nach Losgrößenänderung vom Terminal gesetzt |
| FERTIGUNG_KAPAZITAETSVORSCHAU_ANZAHL_GRUPPEN  | FERTIGUNG  |  | Anzahl der dargestellten Tätigkeitsgruppen |
| FERTIGUNG_KAPAZITAETSVORSCHAU_ZEITRAUM  | FERTIGUNG  |  | Darstellungszeitraum der Kapazitätsvorschau in Tagen |
| FERTIGUNGSBEGLEITSCHEIN_MIT_MATERIAL  | FERTIGUNG  | 0/1 | Fertigungsbegleitschein Material mit andrucken |
| GANZE_CHARGEN_VERWENDEN  | FERTIGUNG  | 0/1 | Nach Möglichkeit immer ganze Chargen im Los verwenden |
| IMMER_CHARGE_FRAGEN  | FERTIGUNG  | 0/1 |   |
| INDIREKTER_AUFTRAGSPOSITIONSBEZUG | FERTIGUNG | 0/1 | Anzeige der Auftragspositonsnummer in der Los-Auswahlliste |
| INT_BEST_VERDICHTEN_RAHMENPRUEFUNG | FERTIGUNG  | 0/1 | Interne Bestellung verdichten mit Rahmenprüfung |
| INTERNEBESTELLUNG_DEFAULTDURCHLAUFZEIT  | FERTIGUNG  |  | Default-Durchlaufzeit in Tagen (für die interne Bestellung) |
| INTERNE_BESTELLUNG_MIT_AUFTRAGSPOSITIONSBEZUG  | FERTIGUNG  | 0/1 | Lose aus interner Bestellung mit Auftragspositionsbezug |
| INTERNEBESTELLUNG_VERDICHTUNGSZEITRAUM  | FERTIGUNG  |  | [Verdichtungszeitraum](../Fertigung/Interne_Bestellung.htm#verdichten) in Tagen |
| ISTZEITEN_GLEICH_SOLLZEITEN  | FERTIGUNG  | 0/1 | Für die Nachkalkulation werden die Istzeiten den Sollzeiten gleichgesetzt. |
| KDC_BUCHT_ARBEITSGANG  | FERTIGUNG  | 0/1 | KDC Losbuchung auf Arbeitsgang |
| KEINE_AUTOMATISCHE_MATERIALBUCHUNG  | FERTIGUNG  | 0/1 | Grundsätzlich keine automatische Materialentnahmen, weder bei Losausgabe noch bei Erledigung |
| KOMMENTAR_AM_FERTIGUNGSBEGLEITSCHEIN  | FERTIGUNG  | 0/1 | Kommentar am Fertigungsbegleitschein drucken |
| KOMMISSIONIERUNG_AUSGESCHLOSSENE_ARTIKELGRUPPEN | FERTIGUNG  | 0/1 |   |
| KOMMISSIONIERUNG_FERTIGUNGSGRUPPE | FERTIGUNG  | 0/1 |   |
| KUNDE_IST_PFLICHTFELD  | FERTIGUNG  | 0/1 | Kunde im Los ist Pflichtfeld |
| KUNDE_STATT_BEZEICHNUNG_IN_AUSWAHLLISTE  | FERTIGUNG  | 0/1 | Kunde statt Artikelbezeichnung in der Los-Auswahlliste anzeigen |
| LOSABLIEFERUNG_GESAMTE_ISTZEITEN_ZAEHLEN | FERTIGUNG  | 0/1 | Für den Ablieferwert zählen nur die Zeiten bis zum Abliefzeitpunkt. |
| LOSAUSGABE_AUTOMATISCH  | FERTIGUNG  | 0/1 | Losausgabe ohne Snr/Chnr-Ausgabedialog. Serien-/Chargennummer werden der Reihe nach verwendet. |
| LOS_BEI_AUSGABE_GESTOPPT  | FERTIGUNG  | 0/1 | Das Los ist nach der Ausgabe automatisch gestoppt |
| LOSAUSGABELISTE2_AUTOMATISCH_DRUCKEN  | FERTIGUNG  | 0/1 | Losausgabeliste2 bei Los-Ausgabe automatisch drucken |
| LOS_BELEGNUMMERSTARTWERT_FREIE_LOSE  | FERTIGUNG  |  | Startwert der Losnummer im neuen Geschäftsjahr |
| LOSBUCHUNG_NUR_SOLLTAETIGKEIT  | FERTIGUNG  | 0/1 | Losbuchung vom Terminal nur auf gültige Solltätigkeit zulassen |
| LOSNUMMER_AUFTRAGSBEZOGEN  | FERTIGUNG  | 0/1/2 | 0= Losnummer fortlaufend1= Auftragsbezogene Losnummer2= Auftragsbezogene Losnummer nach Bereichen |
| MASCHINENBEZEICHNUNG_ANZEIGEN  | FERTIGUNG  | 0/1 | Im Lossollarbeitsplan die Maschinenbezeichnung statt Artikelbezeichnung anzeigen. |
| MEHRERE_LOSE_AUSGEBEN_FERTIGUNGSPAPIERE_DRUCKEN | FERTIGUNG  | 0/1 |   |
| NACHKOMMASTELLEN_LOSGROESSE | FERTIGUNG  |  | Nachkommastellen der Losgroesse / Ablieferung |
| NEGATIVE_SOLLMENGEN_BUCHEN_AUF_ZIELLAGER  | FERTIGUNG  | 0/1 | Negative Sollmengen werden auf Ziellager gebucht. Wenn 0, dann wird das erste Lager der Los-Lagerentnahme verwendet. |
| NICHT_LAGERBEWIRTSCHAFTETE_SOFORT_AUSGEBEN | FERTIGUNG  | 0/1 | Nicht lagerbewirtschaftete Artikel sofort ausgeben, wenn -Materialbuchung bei Ablieferung- in der Stueckliste angehakt ist. Im [Lagercockpit]( {{<relref "/docs/stammdaten/artikel/lagercockpit">}} ) werden bei der Materialumlagerung die nicht lagerbewirtschafteten Artikel ein/ausgeblendet. |
| NUR_TERMINEINGABE  | FERTIGUNG  | 0/1 | Termineingabe ohne Durchlaufzeit |
| PARAMETER_KDC_KEINE_MASCHINENPRUEFUNG  | FERTIGUNG  | 0/1 | 0 = Prüfung der gebuchten Maschine im Arbeitsgang1 = keine Prüfung |
| PRO_FIRST_SCHACHTELPLAN_PFAD | FERTIGUNG  | 0/1 |   |
| RAHMENAUFTRAEGE_IN_FERTIGUNG_VERWENDBAR  | FERTIGUNG  | 0/1 | In den Los-Kopfdaten können auch Rahmenaufträge ausgewählt werden. |
| RANKINGLISTE_ZUSATZSTATUS  | FERTIGUNG  |  | I_ID des Zusatzstatus für Rankingsliste |
| REFERENZNUMMER_IN_INTERNER_BESTELLUNG | FERTIGUNG  | 0/1 |   |
| SOLLSATZGROESSE_PRUEFEN  | FERTIGUNG  | 0/1 | Prüfung der Sollsatzgröße bei der Ablieferung am Terminal |
| TOLERANZFRIST_INTERNE_BESTELLUNG  | FERTIGUNG  |  | Die Toleranzfrist in Tagen die bei der Erstellung einer internen Bestellung berücksichtigt wird. |
| TRIGGERT_TRUMPF_TOPS_ABLIEFERUNG  | FERTIGUNG  |  | Arbeitszeitartikel für [TOPS-Ablieferung]( {{<relref "/fertigung/losverwaltung/trumpf_tops">}} ) in HTML-BDE (Artikelnummer) |
| VERKNUEPFTE_BESTELLDETAILS_ANZEIGEN  | FERTIGUNG  | 0/1 | Verknüpfte Bestelldetails in Losmaterial anzeigen |
| ZUSATZBEZEICHNUNG_IN_AUSWAHLLISTE | FERTIGUNG  | 0/1 | Zusatzbezeichnung in Auswahlliste (Wenn Parameter AUFTRAG_STATT_ARTIKEL_IN_AUSWAHLLISTE=0) |

## Kategorie [FINANZ]( {{<relref "/management/finanzbuchhaltung" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| AUSZUGSNUMMER_BEI_BANK_ANGEBEN  | FINANZ  | 0/1 | 1 = Auszugsnummer muss eingegeben werden0 = wird automatisch generiert |
| BUCHUNGEN_NUR_OFFENE_DEFAULT  | FINANZ  | 0/1 | Die Standardbelegung der Checkbox "nur offene" in den Buchungen.1 = aktiviert, 0 = deaktiviert (Standard) |
| EXPORT_DATEV_BERATER | FINANZ  |  | Beraternummer für DATEV-Export, nicht zu verwechseln mit Mandantennummer |
| EXPORT_DATEV_KONTOKLASSEN_OHNE_BU_SCHLUESSEL | FINANZ  |  | Die erste Stelle der Sachkontonummern, bei welchen der Buchungsschlüssel nicht gesetzt werden darf. Mehrere durch Komma getrennt. |
| EXPORT_DATEV_MANDANT | FINANZ  |  | Mandantennummer für DATEV-Export, nicht zu verwechseln mit Beraternummer |
| EXPORT_DATEV_MITLAUFENDES_KONTO | FINANZ  |  | Das Konto darf in der **Kieselstein ERP** FiBu nicht existieren, es findet nur beim Exportieren von Splittbuchungen Einsatz. Der Saldo dieses Kontos muss in DATEV 0 sein. |
| FIBU_EXPORT_ASCII  | FINANZ  | 0/1 | Gibt an ob FIBU-Export Dateien im ASCII Zeichensatz gespeichert werden sollen |
| FINANZ_DEBITORENNUMMER_FORTLAUFEND  | FINANZ  | 0/1 | 0 = Anfangsbuchstabe des Partners wird für 2+3\. Stelle verwendet1 = fortlaufend, gilt für Debitoren und Kreditoren |
| FINANZ_EXPORT_ANZAHLUNGSRECHNUNG | FINANZ | 0/1 | 0= Anzahlungsrechnungen werden nicht exportiert1= Anzahlungsrechnungen sind im Export enthalten |
| FINANZ_EXPORT_ARTIKELGRUPPEN_DEFAULT_KONTO_AR  | FINANZ  |  | Default-Konto für Positionen ohne Artikelgruppe (Rechnung/Gutschrift) |
| FINANZ_EXPORT_ARTIKELGRUPPEN_DEFAULT_KONTO_ER  | FINANZ  |  | Default-Konto für Positionen ohne Artikelgruppe (Eingangsrechnung) |
| FINANZ_EXPORT_BMD_BENUTZERNUMMER  | FINANZ  |  | Benutzernummer in BMD (2 Ziffern) |
| FINANZ_EXPORT_MIT_BELEGEN  | FINANZ  | 0/1 | Gibt an ob der FIBU-Export die zugehörigen Belege mit im Exportverzzeichnis speichert |
| FINANZ_EXPORT_UEBERSCHRIFT  | FINANZ  | 0/1 | Die Spalten-Überschriften in der Exportdatei angeben |
| FINANZ_EXPORT_UEBERSTEUERTE_MANDANTENWAEHRUNG  | FINANZ  |  | Währung, die die Mandantenwährung für FIBU-Exporte übersteuert. z.B. EUR oder CHF |
| FINANZ_EXPORT_UST_IGERWERB_NORMALSATZ_KONTO  | FINANZ  |  | Umsatzsteuer-Kontonummer für IG-Erwerb Normalsteuersatz |
| FINANZ_EXPORT_UST_IGERWERB_REDUZIERTSATZ_KONTO  | FINANZ  |  | Umsatzsteuer-Kontonummer für IG-Erwerb reduzierter Steuersatz |
| FINANZ_EXPORT_UST_REVERSECHARGE_NORMALSATZ_KONTO  | FINANZ  |  | Umsatzsteuer-Kontonummer für Reverse Charge Normalsteuersatz |
| FINANZ_EXPORT_UST_REVERSECHARGE_REDUZ_KONTO  | FINANZ  | 0/1 | Umsatzsteuer-Kontonummer für Reverse Charge reduz. Steuersatz |
| FINANZ_EXPORT_VARIANTE  | FINANZ  |  | Definiert die Art des Exports. z.B. Kostenstellen oder Artikelgruppen |
| FINANZ_EXPORT_VST_IGERWERB_NORMALSATZ_KONTO  | FINANZ  |  | Vorsteuer-Kontonummer für IG-Erwerb Normalsteuersatz |
| FINANZ_EXPORT_VST_IGERWERB_REDUZIERTSATZ_KONTO  | FINANZ  |  | Vorsteuer-Kontonummer für IG-Erwerb reduzierter Steuersatz |
| FINANZ_EXPORT_VST_REVERSECHARGE_NORMALSATZ_KONTO  | FINANZ  |  | Vorsteuer-Kontonummer für Reverse Charge Normalsteuersatz |
| FINANZ_EXPORT_VST_REVERSECHARGE_REDUZ_KONTO  | FINANZ  |  | Vorsteuer-Kontonummer für Reverse Charge reduz. Steuersatz |
| FINANZ_EXPORTZIEL_DEBITORENKONTEN  | FINANZ  |  | Default Export-Zieldatei Debitorenkonten (zb. c:\...) |
| FINANZ_EXPORTZIEL_EINGANGSRECHNUNG  | FINANZ  |  | Default Export-Zieldatei Eingangsrechnung |
| FINANZ_EXPORTZIEL_GUTSCHRIFT  | FINANZ  |  | Default Export-Zieldatei Gutschrift |
| FINANZ_EXPORTZIEL_KREDITORENKONTEN  | FINANZ  |  | Default Export-Zieldatei Kreditorenkonten |
| FINANZ_EXPORT_ZIELPROGRAMM  | FINANZ  |  | Ziel-Buchhaltungssoftware. z.B. RZL oder BMD |
| FINANZ_EXPORTZIEL_RECHNUNG  | FINANZ  |  | Default Export-Zieldatei Rechnung |
| FINANZ_EXPORTZIEL_SACHKONTEN  | FINANZ  |  | Default Export-Zieldatei Sachkonten |
| FINANZ_OP_PREFIX_GUTSCHRIFT | FINANZ  |  | Prefix Gutschriftnummer bei Import Offene-Posten. |
| FINANZ_OP_PREFIX_RECHNUNG  | FINANZ  |  | Prefix Rechnungsnummer bei Import Offene-Posten - Werte: ? |
| FINANZ_RABATT_KONTO  | FINANZ  |  | Allgemeiner Rabatt wird auf dieses Konto gebucht (wenn definiert) |
| FINANZ_RECHNUNG_WERT0_ERLAUBT  | FINANZ  | 0/1 | 1 ... Rechnung mit Wert 0 erlaubt |
| FINANZ_SAMMELBUCHUNG_MANUELL | FINANZ  | 0/1 | Sammelbuchungen (UVA) werden manuell durchgeführt |
| FINANZ_UVA_ABRECHNUNGSZEITRAUM  | FINANZ  |  | Abrechnungszeitraum der UVAMögliche Einstellungen: Monatlich, Quartal, Jährlich. |
| IMPORT_PLANKOSTEN_DATEI  | FINANZ  |  | Dateiname zu XLS-Spreadsheet mit Plankostendaten |
| INTRASTAT_EXPORTZIEL_EINGANG  | FINANZ  |  | Default Export-Zieldatei Intrastat-Eingang |
| INTRASTAT_EXPORTZIEL_VERSAND  | FINANZ  |  | Default Export-Zieldatei Intrastat-Versand |
| KONTONUMMER_STELLENANZAHL_DEBITORENKONTEN  | FINANZ  |  | Anzahl der Stellen der Kontonummer für Debitorenkonten |
| KONTONUMMER_STELLENANZAHL_KREDITORENKONTEN  | FINANZ  |  | Anzahl der Stellen der Kontonummer für Kreditorenkonten |
| KONTONUMMER_STELLENANZAHL_SACHKONTEN  | FINANZ  |  | Anzahl der Stellen der Kontonummer für Sachkonten |
| LF_RE_NR_BUCHUNGSTEXT  | FINANZ  | 0/1 | 0 = Name des Lieferanteranten1 = Lieferantrechnungsnummer als Buchungstext |
| MAHNUNG_AB_RECHNUNGSBETRAG  | FINANZ  |  | Ab welchem Rechnungsbetrag werden Rechnungen gemahnt (in Mandantenwährung) |
| MAHNUNG_ZAHLUNGSFRIST  | FINANZ  | 0/1 | Zahlungsfrist ab heute in Tagen |
| TOLERANZ_BETRAGSUCHE | FINANZ  |  | Toleranz der Betragssuche im Buchungsjournal (in %). |
| TOLERANZ_SUCHE_NACH_BETRAG | FINANZ  |  | Toleranz in Prozent, wenn nach Betraegen gefiltert wird. |
| UST_MONAT | FINANZ  |  | Anzahl der Monate, um die die UST im vorhinein bezahlt werden muss.[(Liquiditätsvorschau]( {{<relref "/management/finanzbuchhaltung/liquiditaetsvorschau">}} )) |
| UST_STICHTAG | FINANZ  |  | Stichtag an den die UST an das Finanzamt bezahlt werden muss. ([Liquiditätsvorschau]( {{<relref "/management/finanzbuchhaltung/liquiditaetsvorschau">}} )) |
| TOLERANZ_BETRAGSUCHE  | FINANZ  |  | Toleranz der Betragssuche im Buchungsjournal (in %) |
| TOLERANZ_SUCHE_NACH_BETRAG  | FINANZ  |  | Toleranz in Prozent, wenn nach Beträgen gefiltert wird |

## Kategorie [GUTSCHRIFT]( {{<relref "/verkauf/rechnung/gutschrift" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| GUTSCHRIFT_BELEGNUMMERSTARTWERT  | GUTSCHRIFT  |  | Startwert der Belegnummer im neuen Geschäftsjahr |
| GUTSCHRIFT_NENNT_SICH_RECHNUNGSKORREKTUR | GUTSCHRIFT  | 0/1 | Bezeichnung von Gutschriften als Rechnungskorrektur |
| GUTSCHRIFT_VERWENDET_NUMMERNKREIS_DER_RECHNUNG  | GUTSCHRIFT  | 0/1 | Gutschriften und Rechnungen verwenden denselben Nummernkreis |

<!-- ## Kategorie [KÜCHE]( {{<relref "/fertigung/kueche">}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| PFAD_KASSENIMPORTDATEIEN_ADS  | KÜCHE  |  | Pfad zu den ADS- Kassenimport- Dateien des Küchenmoduls |
| PFAD_KASSENIMPORTDATEIEN_OSCAR  | KÜCHE  |  | Pfad zu den OSCAR- Kassenimport- Dateien des Küchenmoduls |
| WARENEINGANG_IN_BESTELLEINHEIT  | KÜCHE  | 0/1 | Wareneingang in Bestellmengeneinheit im WebClient (0 ... in Artikeleinheit, 1 ... in Bestelleinheit) | -->

## Kategorie [KUNDEN]( {{<relref "/docs/stammdaten/kunden" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| AUTOMATISCHE_DEBITORENNUMMER  | KUNDEN | 0/1 | Debitorennummer bei Ausgangsbelegen automatisch anlegen, wenn nicht vorhanden |
| BONITAETSWARNUNGSZEITRAUM  | KUNDEN |  | Bonitätswarnungszeitraum in Monaten |
| DEBITORENNUMMER_BIS  | KUNDEN |  | Definition der Debitorennummern |
| DEBITORENNUMMER_VON  | KUNDEN |  | Definition der Debitorennummern |
| DEFAULT_AM_LS_GEWICHT_ANDRUCKEN | KUNDEN | 0/1 | Default-Wert in Kunden-Konditionen |
| DEFAULT_ARTIKEL_ARTIKELART | KUNDEN |  |  |
| DEFAULT_BELEGDRUCK_MIT_RABATT | KUNDEN | 0/1 | Default- Einstellung der Option -Belegdrucke mit Rabatt- |
| DEFAULT_KOPIEN_LIEFERSCHEIN | KUNDEN |  | Anzahl der Kopien für Lieferscheine |
| DEFAULT_KOPIEN_RECHNUNG | KUNDEN |  | Anzahl der Kopien für Rechnungen |
| DEFAULT_KUNDE_AKZEPTIERT_TEILLIEFERUNGEN  | KUNDEN | 0/1 | Default-Wert für Option -Akzeptiert Teillieferungen- in Kundenkonditionen |
| DEFAULT_KUNDENAUSWAHL  | KUNDEN | 1/2/3 | 1 = Alle, 2 = Kunde, 3 = Interessent |
| DEFAULT_KUNDESOKO_WIRKT_NICHT_IN_PREISFINDUNG | KUNDEN | 0/1 | Default- Einstellung der Option -wirkt nicht in Preisfindung- in der Kunden-Soko eines Artikels |
| DEFAULT_LIEFERDAUER  | KUNDEN | 0/1 | Default- Lieferdauer |
| KREDITLIMIT  | KUNDEN |  |  |
| KUNDE_ERWERBSBERECHTIGUNG_ANZEIGEN  | KUNDEN | 0/1 | Gibt an ob Das Feld Erwerbsberechtigung im Kunden angezeigt wird und die eingetragenen Werte beim Speichern von Belegen überprüft werden. |
| KUNDE_MIT_NUMMER  | KUNDEN | 0/1 | Kunden haben eine [Kundennummer](../Kunde/index.htm#Kundennummer) |
| KUNDEN_POSITIONSKONTIERUNG  | KUNDEN | 0/1 | Postionskontierung wenn 1 (ja) Artikel bestimmt MWST, wenn 0 (nein) gilt MWST des Kunden |
| KUNDENBEWERTUNG_WERT_A  | KUNDEN |  | Prozent- Wert für die [Einteilung](../Kunde/index.htm#ABC) in Klasse A |
| KUNDENBEWERTUNG_WERT_B  | KUNDEN |  | Prozent- Wert für die [Einteilung](../Kunde/index.htm#ABC) in Klasse B |
|  | KUNDENHINWEIS_AB_MAHNSTUFE || --- | | KUNDEN | 1-3 |   |
| KUPFERZAHL  | KUNDEN |  | [Kupferzahl]( {{<relref "/docs/stammdaten/artikel/materialzuschlag" >}} ) für VK-Belege |
| KURZWAHL_BIS  | KUNDEN | 0/1 |  |
| KURZWAHL_VON  | KUNDEN | 0/1 |  |
| NETTOPREISE  | KUNDEN | 0/1 |  |
| NUMMER  | KUNDEN | 0/1 |  |
| OFFENER WE | KUNDEN | 0/1 |  |
| PREISBASIS_VERKAUF  | KUNDEN | 0/1/2 | Definition für den [Verkaufspreis]( {{<relref "/docs/stammdaten/artikel/verkaufspreis" >}} )0 = Verkaufspreisbasis1 = Preisbasis lt. Kundenpreisliste (Es wird als Preisbasis der lt. Preisliste berechnete Betrag verwendet)2 = Verkaufspreisbasis mit Mengenstaffelrabatt im Zusatzrabatt |
| REVERSE_CHARGE_VERWENDEN  | KUNDEN | 0/1 | Reverse Charge verwenden |
| TOURSTATISTIK  | KUNDEN |  |  |
| WIEDERBSCHAFFUNGSMORAL_ANZAHL_BESTELLUNGEN | KUNDEN |  | Anzahl der Bestellungen, die für die Berechnung der Wiederbeschaffungsmoral berücksichtigt werden. |
| WIEDERBSCHAFFUNGSMORAL_MONATE | KUNDEN |  | Anzahl der Monate, die für die Berechnung der Wiederbeschaffungsmoral berücksichtigt werden. |
| ZAHLUNGSMORAL_ANZAHL_RECHNUNGEN  | KUNDEN |  | Anzahl der Rechnungen, die für die Berechnung der [Zahlungsmoral](../Kunde/index.htm#Zahlungsmoral) berücksichtigt werden |
| ZAHLUNGSMORAL_MONATE  | KUNDEN |  | Anzahl der Monate, die für die Berechnung der [Zahlungsmoral](../Kunde/index.htm#Zahlungsmoral) berücksichtigt werden |

## Kategorie [LIEFERANT]( {{<relref "/docs/stammdaten/lieferanten" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| AUTOMATISCHE_KREDITORENNUMMER  | LIEFERANT  | 0/1 | Kreditorennummer bei Belegen automatisch anlegen, wenn nicht vorhanden |
| KREDITLIMIT  | LIEFERANT  |  | Betrag, der das Kreditlimit des Lieferanten definiert |
| KREDITOREN_BIS  | LIEFERANT  |  | Definition der Kreditorennummern |
| KREDITOREN_VON  | LIEFERANT  |  | Definition der Kreditorennummern |
| KURZWAHL_BIS  | LIEFERANT  |  |  |
| KURZWAHL_VON  | LIEFERANT  |  |  |
| LIEFERANTENBEURTEILUNG_PUNKTE_KLASSE_A  | LIEFERANT  |  | Anzahl der benötigten Punkte für die Beurteilungsklasse A |
| LIEFERANTENBEURTEILUNG_PUNKTE_KLASSE_B  | LIEFERANT  |  | Anzahl der benötigten Punkte für die Beurteilungsklasse B |
| LIEFERANTENBEURTEILUNG_PUNKTE_KLASSE_C  | LIEFERANT  |  | Anzahl der benötigten Punkte für die Beurteilungsklasse C |
| NUMMER  | LIEFERANT  | 0/1 |  |

## Kategorie [LIEFERSCHEIN]( {{<relref "/verkauf/lieferschein" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ARTIKELBEZEICHNUNG_IN_LS_EINFRIEREN  | LIEFERSCHEIN  | 0/1 | Bei LS-Erfassung auf -Sicht Auftrag- Artikelbezeichnungen einfrieren |
|  | AUFTRAG_AUS_POSITIONEN_IN_AUSWAHLLISTE || --- | | LIEFERSCHEIN  | 0/1 |   |
| KONDITIONEN_DES_BELEGS_BESTAETIGEN  | LIEFERSCHEIN  | 0/1 | Konditionen müssen vor dem erstmaligen Aktivieren des Lieferscheins bestätigt werden |
| LIEFERAVISO_TAGE_ZUSAETZLICH | LIEFERSCHEIN  |  | Anzahl der Tage die zusätzlich zur Lieferdauer beim Kunden benötigt werden um den Eintrefftermin zu erreichen. Es können auch negative Werte verwendet werden, wenn beispielsweise die Lieferdauer zum Kunden Puffertage enthält. |
| LIEFERSCHEIN_ANSPRECHPARTNER_ANDRUCKEN  | LIEFERSCHEIN  | 0/1 | Ansprechpartner im Adressblock andrucken |
| LIEFERSCHEIN_ANSPRECHPARTNER_VORBESETZEN  | LIEFERSCHEIN  | 0/1 | Den Ansprechpartner nach Auswahl des Kunden vorbesetzen |
| LIEFERSCHEIN_BELEGNUMMERSTARTWERT  | LIEFERSCHEIN  | 0/1 | Startwert der Belegnummer im neuen Geschäftsjahr |
| LIEFERSCHEIN_POSITIONEN_MIT_RESTAPI_VERDICHTEN | LIEFERSCHEIN  | 0/1 |   |
| LS_DEFAULT_VERRECHENBAR | LIEFERSCHEIN  | 0/1 | Default-Wert für -Verrechenbar- in den Lieferschein-Konditionen (1= Verrechenbar, 0=nicht verrechenbar) |
| MINDESTSTANDSWARNUNG  | LIEFERSCHEIN  | 0/1 | Lagermindeststandswarnung im Lieferschein |
| [OFFENE_MENGE_IN_SICHT_AUFTRAG_VORSCHLAGEN](../Lieferschein/index.htm#Auftragspositionen übernehmen)  | LIEFERSCHEIN  | 0/1/2 | Die offene Menge in -Sicht Auftrag- als Positionsmenge vorschlagen bzw. den gesamten Lagerbestand. |
| POSITIONSREIHENFOLGE_AUS_AUFTRAG_ERHALTEN | LIEFERSCHEIN | 0/1 | Positionsreihenfolge des Auftrags erhalten, wenn LS-Position aus Sicht Auftrag erzeugt wird |
| POST_PLC_APIKEY | LIEFERSCHEIN |   | <ClientId>;<orgUnitID>;<orgUnitGUID>;<URL> |

## Kategorie [PARTNER]( {{<relref "/docs/stammdaten/partner" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| DEFAULT_ANSPRECHPARTNER_DURCHWAHL | PARTNER | 0/1 |   |
| GOOGLE_APIKEY  PARTNER | 0/1 |   |
| PARTNERSUCHE_WILDCARD_BEIDSEITIG  | PARTNER | 0/1 | Beidseitige Wildcard im Filterkriterium Firma in der Kunden/Partner/Lieferantenauswahlliste - das bedeutet, dass Teile des Namens eingegeben werden können und bei der Suche nach rechts und links gesucht wird. |
| SELEKTIONEN_MANDANTENABHAENGIG | PARTNER | 0/1 | Selektionen sind mandantenabhaengig |
| URL_ONLINE_KARTENDIENST | PARTNER |   | Wählen Sie zwischen OpenStreetMap oder GoogleMap   |

## Kategorie [PERSONAL]( {{<relref "/docs/stammdaten/personal" >}} )
| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ANWESENHEITSLISTE_TELEFON_PRIVAT_ANZEIGEN  | PERSONAL | 0/1 | Anzeige der privaten Telefonnummer in der Anwesenheitsliste |
| <a name="ARBEITSZEITARTIKEL_AUS_PERSONALVERFUEGBARKEIT"></a>ARBEITSZEITARTIKEL_AUS_PERSONALVERFUEGBARKEIT  | PERSONAL | 0/1 | Arbeitszeitartikel für Zeiterfassung kommen aus der [Personalverfügbarkeit](../personal/index.htm#Verfügbarkeit)Ist dieser gesetzt so wird für die Buchung der Tätigkeit diejenige verwendet, welche den höchsten Prozentsatz (der Anwesenheitszeit) hat. |
| AUFTRAGSBUCHUNG_OHNE_POSITIONSBEZUG | PERSONAL | 0/1 |   |
| AUTOMATISCHE_PAUSEN_AB_ERLAUBTEM_KOMMT  | PERSONAL | 0/1 | Automatische Pausen ab -frühestem erlaubtem Kommt- |
| AUTOMATISCHES_KOMMT  | PERSONAL | 0/1/2 | 0= Aus1= Kommt/ Unterbrechung- Ende wird automatisch gebucht, wenn noch nicht vorhanden2= Kommt wird gebucht, wenn noch nicht vorhanden |
| AUTOMATISCHE_PAUSEN_NUR_WARNUNG | PERSONAL | 0/1 | Automatische Pausen nur Warnung anzeigen, wenn zu wenig gebucht. |
| BDE_MIT_TAETIGKEIT  | PERSONAL | 0/1 | Die HTML [BDE-Station]( {{<relref "/fertigung/zeiterfassung/bde_station" >}} ) benötigt eine Tätigkeit |
| BUCHUNG_IMMER_NACHHER_EINFUEGEN  | PERSONAL | 0/1 | Zeitbuchung immer nachher einfügen. |
| ERWEITERTER_URLAUBSANSPRUCH  | PERSONAL | 0/1/2 | 0 = Standard (aliquot.)1 = Voller Urlaubsanspruch bis zum Eintritt am 30.6. 2 = Berechnung nach TVÖD |
| GUTSTUNDEN_ZU_UESTD50_ADDIEREN | PERSONAL | 0/1 | Gutstunden in Monatsabrechung zu Uestd50Pflichtig addieren |
| LAENGE_ZESTIFTKENNUNG  | PERSONAL |  | Länge der Kennung eines ZE-Stiftes |
| LEAD_IN_AUSWEISNUMMER_MECS  | PERSONAL |  | Lead-In für Ausweisnummer im MECS-Terminal in Verbindung mit einem Barcodescanner. Der Wert ist per Default leer und wird mit $P befüllt, wenn ein MECS-Terminal in Verbindung mit einem Barcodescanner verwendet wird. |
| LOHNSTUNDENSATZKALKULATION_FEIERTAGSWOCHEN | PERSONAL |  | Anzahl der Feiertagswochen für die [Lohnstundensatzkalkulation](../personal/index.htm#Gehalts-/Lohnkosten Definition.). |
| LOHNSTUNDENSATZKALKULATION_KALENDERWOCHEN | PERSONAL |  | Anzahl der Kalenderwochen für die [Lohnstundensatzkalkulation](../personal/index.htm#Gehalts-/Lohnkosten Definition.). |
| LOHNSTUNDENSATZKALKULATION_KRANKWOCHEN | PERSONAL |  | Anzahl der Krankwochen für die [Lohnstundensatzkalkulation](../personal/index.htm#Gehalts-/Lohnkosten Definition.). |
| LOHNSTUNDENSATZKALKULATION_SONDERZAHLUNGEN | PERSONAL |  | Anzahl der Sonderzahlungen für die [Lohnstundensatzkalkulation](../personal/index.htm#Gehalts-/Lohnkosten Definition.). |
| LOSAUSWAHL_TECHNIKERFILTER  | PERSONAL | 0/1 | [Losauswahl über Techniker](../Zeiterfassung/index.htm#Losauswahlliste_für_Techniker) / Gilt nur für Zeiterfassung. |
| LOSBUCHUNG_OHNE_POSITIONSBEZUG_BEI_STUECKRUECK  | PERSONAL | 0/1 | Los-Position in Zeiterfassung ist kein Pflichtfeld, wenn Stückrückmeldung eingeschaltet |
| MAXIMALE_EINTRAEGE_SOLLZEITPRUEFUNG  | PERSONAL | 0/1 | Maximale Einträge an Zeitdaten, die für die Sollzeitprüfung vorhanden sein dürfen |
| MONATSABRECHNUNG_NUR_AB_TAGESIST | PERSONAL | 0/1 |   |
| PARAMETER_ZEITBUCHUNG_AUF_ANGELEGTE_LOSE_MOEGLICH | PERSONAL |   | 1= Zeitbuchungen sind auch auf angelegte Lose möglich |
| PERSONALKOSTEN_QUELLE  | PERSONAL | 0/1/2 | Quelle der Personalkosten:0= Gestehungspreis aus Tätigkeit1= Kosten aus Personalgruppe2= Stundensatz aus Personalgehalt |
| PERSONALNUMMER_ZEICHENSATZ  | PERSONAL |  | Bestimmt die in einer Personalnummer verwendbaren Zeichen. ACHTUNG: Terminals unterstützen nur die Ziffern 0 bis 9! |
| PERSONALZEITDATENEXPORT_FIRMENNUMMER  | PERSONAL |  | [Zeitdatenexport]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ) Firmennummer |
| PERSONALZEITDATENEXPORT_PERIODENVERSATZ  | PERSONAL |  | Periodenversatz im [Lohndatenxport]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ) |
| PERSONALZEITDATENEXPORT_ZIEL  | PERSONAL |  | [Zeitdatenexport]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ) Exportpfad |
| PERSONALZEITDATENEXPORT_ZIELPROGRAMM  | PERSONAL |  | [Zeitdatenexport]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ) Zielprogramm (VARIAL, EGECKO) |
| SALDENABFRAGE_NUR_IST_STUNDEN_DES_AKTUELLEN_MONATS  | PERSONAL | 0/1 | In der BDE-Station und am Terminal wird nur der aktuelle Monats IST-Saldo angezeigt, ohne Überträge aus dem Vormonat. |
| THEORETISCHE_IST_ZEIT_RECHNUNG  | PERSONAL | 0/1 | Theoretische Ist-Zeit Rechnung bei Umspann/Rüstzeiten |
| TOLERANZ_ZEITABWEICHUNG_TERMINAL | PERSONAL | 0/1 |   |
| URAUBSTAGE_RUNDUNG | PERSONAL | 0/1/2/3 | 0 = keine Rundung, 1 = kaufmännisch runden, 2 = generell abrunden, 3 = generell aufrunden |
| URLAUBSABRECHNUNG_ZUM_EINTRITT | PERSONAL | 0/1 | Je nach Gesetzes-Lage.<br>Unsere Empfehlung immer gleichlaufend zum Geschäftsjahr = Kalenderjahr.<br>Eine Umstellung erforder in jedem Falle eine Neudefinition der alten Überlaubsüberträge.  |
| URLAUBSANTRAG | PERSONAL | 0/1 | Ablauf des Urlaubsantrag mit Genehmigung. |
| VON_BIS_ERFASSUNG | PERSONAL | 0/1 | Belege können mit Von-Bis-Zeit erfasst werden. |
| VON_BIS_ERFASSUNG_KOMMT_GEHT_BUCHEN | PERSONAL | 0/1 | KOMMT-GEHT muss bei VON-BIS Zeiterfassung gebucht werden |
| VON_BIS_VERSATZ | PERSONAL |  | Von-Bis Versatz bei Von-Bis Zeiterfassung |
| ZEITBUCHUNG_AUF_ERLEDIGTE_MOEGLICH  | PERSONAL | 0/1 | Zeitbuchungen sind auch auf erledigte Aufträge möglich |
| ZEITBUCHUNGEN_NACHTRAEGLICH_BUCHEN_BIS  | PERSONAL |  | Bis zu welchem Tag des Aktuellen Monats darf man die Zeitbuchungen des Vormonats verändern |
| ZESTIFT_ABSTAND_BUCHUNGEN  | PERSONAL |  | Kommen vom ZE-Stift zwei gleiche Zeitbuchungen, welche innerhalb des angegebenen Zeitabstandes sind, so wird die nachfolgende Buchung verworfen, da davon ausgegangen wird, dass dies eine Fehlbuchung / Fehlbedienung ist. Gleiche Zeitbuchung sind das selbe Los, selbe Tätigkeit, selbe Maschine, selber Mitarbeiter nur der Buchungszeitpunkt differiert. (in Sekunden) |
| ZESTIFT_STUECKRUECKMELDUNG  | PERSONAL | 0/1 | Gutstück um 1 erhöhen, wenn per ZE-Stift auf einen Arbeitsgang gebucht wird. |
| ZUTRITT_DATEN_VORLADEN  | PERSONAL |  | Setzt die Anzahl der Tage fest, für die [Zutrittsdaten](../Zutritt/index.htm#Zutrittdefinition) generiert werden |
| ZUTRITT_DEFAULT_ZUTRITTSKLASSE  | PERSONAL | 0/1 | Default- [Zutrittsklasse](../Zutritt/index.htm#Zutrittdefinition) |

## Kategorie[PROJEKT]( {{<relref "/verkauf/projekt" >}} )

| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| AUFSCHLAG1 | PROJEKT | 0/1 |   |
| AUFSCHLAG2 | PROJEKT | 0/1 |   |
| BUILD_ANZEIGEN | PROJEKT | 0/1 | Build-Nummer in Kopfdaten anzeigen. |
| INTERN_ERLEDIGT_BEBUCHBAR | PROJEKT | 0/1 | Wenn der Parameter auf 1 gestellt ist, dann ist die Zeiterfassung auf intern erledigte Projekte möglich. |
| KURZBEZEICHNUNG_STATT_ORT_IN_AUSWAHLLISTE | PROJEKT | 0/1 | In der Projekt-Auswahl wird anstatt des Ortes die Kurzbezeichnung des Partners angezeigt. |
| KURZZEICHEN_STATT_NAME_IN_AUSWAHLLISTE | PROJEKT | 0/1 | Kurzzeichen statt Name in Auswahlliste. |
| PROJEKT_ANSPRECHPARTNER_VORBESETZEN | PROJEKT | 0/1 | Den Ansprechpartner nach Auswahl des Partners vorbesetzen. |
| PROJEKT_BELEGNUMMERSTARTWERT  | PROJEKT  | 0/1 | Startwert der Projekt-Nummer im neuen Geschäftsjahr |
| PROJEKT_MIT_BETREIBER | PROJEKT  | 0/1 | Projekt mit Betreiber |
| PROJEKT_MIT_TAETIGKEIT | PROJEKT  | 0/1 | Projektzeiterfassung mit zugeordneten Taetigkeiten |
| PROJEKT_MIT_UMSATZ  | PROJEKT  | 0/1 | Geplanten Umsatz in der Projektauswahl anzeigen |
| PROJEKT_OFFSET_ZIELTERMIN  | PROJEKT  | 0/1 | Default-Zieltermin in 90 Tage |

## Kategorie [RECHNUNG]( {{<relref "/verkauf/rechnung" >}} )

| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| AUSFUEHRLICHER_MAHNUNGSDRUCK_AR  | RECHNUNG  | 0/1 | Gibt an ob in Mahnungen die Mahnungsdaten zu sehen sind oder die [Rechnungsdaten + Mahnstufe](../Rechnung/Mahnen.htm#Mahnungsdruck) |
| BRUTTO_STATT_NETTO_IN_AUSWAHLLISTE  | RECHNUNG  | 0/1 | Brutto-Summe statt Netto-Summe in Auswahlliste (RE+ER) |
| DEBITORENNUMMER_IN_AUSWAHLLISTE | RECHNUNG  | 0/1 | Debitorennummer in Auswahlliste |
| EINZELRECHNUNG_EXPORTPFAD  | RECHNUNG  |  | Server-Pfad des [Einzelrechnungsexportes](../Rechnung/index.htm#Einzelrechnungsexport) |
| KONDITIONEN_DES_BELEGS_BESTAETIGEN  | RECHNUNG  | 0/1 | Konditionen müssen vor dem erstmaligen Aktivieren der Rechnung bestätigt werden |
| LASTSCHRIFT_FAELLIGKEIT_NACH_ZAHLUNGSZIEL | RECHNUNG  | 0/1 |   |
| LIEFERSCHEIN_UEBERNAHME_NACH_ANSPRECHPARTNER  | RECHNUNG  | 0/1 | Wenn mehrere Lieferscheine in die Rechnung übernommen werden, dann werden diese nach dem Ansprechpartner sortiert eingefügt. |
| LIEFERSPERRE_AB_MAHNSTUFE  | RECHNUNG  |  | Mahnstufe, ab der eine Liefersperre gesetzt wird |
| MAHNUNGEN_AB_GF_JAHR  | RECHNUNG  |  | Geschäftsjahr, ab dem gemahnt wird. |
| MAXIMALE_ABWEICHUNG_SUMMENAUSDRUCK  | RECHNUNG  |  | Abweichung Summenausdruck |
| MINDEST_MAHNBETRAG | RECHNUNG  |  | Mindestmahnbetrag, ab der eine Sammelmahnung versendet wird. |
| MINDESTSTANDSWARNUNG  | RECHNUNG  | 0/1 | [Lagermindeststandswarnung](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Lagermindeststandswarnung) in der Rechnung |
| RECHNUNG_ANSPRECHPARTNER_ANDRUCKEN  | RECHNUNG  | 0/1 | Ansprechpartner im Adressblock andrucken |
| RECHNUNG_ARTIKELGRUPPE_ANDRUCKEN  | RECHNUNG  | 0/1 | Gibt an ob die Artikelgruppe auf Rechnungen mitgedruckt werden soll |
|RECHNUNG_POSITION_WERT0_PRUEFEN | RECHNUNG  | 0/1 |   |
| RECHNUNGSEMPFAENGER_NUR_AUS_RECHNUNGS_EMAIL | RECHNUNG  | 0/1 |   |
| WIEDERHOLENDE_AUFTRAEGE_ZUSAMMENFASSEN | RECHNUNG  | 0/1 |  |

## Kategorie [REKLAMATION]( {{<relref "/fertigung/reklamation" >}} )

| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| BESTELLUNG_UND_WARENEINGANG_SIND_PFLICHTFELDER  | REKLAMATION  | 0/1 | Bestellung und Wareneingang sind Pflichtfelder. |
| KUNDENREKLAMATION_DEFAULT  | REKLAMATION  | 1/2 | 1 = Fertigung, 2 = Lieferant |
| LIEFERANTENREKLAMATION_BESTELLUNG_MANUELL | REKLAMATION  | 0/1 |   |
| REKLAMATION_BELEGNUMMERSTARTWERT  | REKLAMATION  | 0/1 | Startwert der Reklamationsnummer im neuen Geschäftsjahr |

## Kategorie [STÜCKLISTE]( {{<relref "/warenwirtschaft/stueckliste" >}} )

| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| ANZAHL_STELLEN_LFD_NR_PRODUKTSTUECKLISTE | STUECKLISTE  | 1-4 |   |
| ARBEITSPLAN_DEFAULT_STUECKLISTE  | STUECKLISTE  |   | Artikelnummer des Default-Arbeitsplans einer neuen Stückliste |
| BASIS_GESAMTKALKULATION  | STUECKLISTE  | 0/1 | 0 = übergeordnete Stückliste, 1 = Fertigungssatzgröße |
| BEI_LOS_AKTUALISIERUNG_MATERIAL_NACHBUCHEN  | STUECKLISTE  | 0/1 | Bei Los- Aktualisierung aus Stückliste fehlendes Material nachbuchen |
| DEFAULT_MATERIALBUCHUNG_BEI_ABLIEFERUNG  | STUECKLISTE  | 0/1 | Default-Wert für die Option -Materialbuchung bei Ablieferung- |
| DEFAULT_UEBERLIEFERBAR  | STUECKLISTE  | 0/1 | Default-Wert für die Option -Überlieferbar- |
| DURCHLAUFZEIT_IST_PFLICHTFELD  | STUECKLISTE  | 0/1 | Durchlaufzeit in Stückliste ist Pflichtfeld |
| ERFASSUNGSFAKTOR_UI_NACHKOMMASTELLEN | STUECKLISTE  | 0-3 |   |
| FERTIGUNGSGRUPPE_VORBESETZEN  | STUECKLISTE  | 0/1 | Die Fertigungsgruppe wird mit der ersten Fertigungsgruppe vorbesetzt |
| FRUEHZEITIGE_BESCHAFFUNG  | STUECKLISTE  | 0/1 | 0 = Beginntermin als Eingabe1= Checkbox für [frühzeitige Beschaffung]( {{<relref "/warenwirtschaft/stueckliste/#bestimmte-artikel-sollten-immer-fr%c3%bcher-oder-sp%c3%a4ter-bestellt-werden">}} ) |
| INTEL_STKL_IMPORT_RABATT | STUECKLISTE  |  | Der Rabatt in Prozent, welcher für, durch den Intelligenten Stücklistenimport neu angelegte, Kundensonderkonditionen verwendet wird. |
| LOSE_AKTUALISIEREN  | STUECKLISTE  | 0/1 | 0 = nur Lose der gewählten Stückliste aktualisieren1 = globale Aktualisierung aller Stücklisten |
| PRO_FIRST_ANZAHL_ZU_IMPORTIERENDE_DATENSAETZE || --- | | STUECKLISTE  |   |   |
| PRO_FIRST_BILD_PFAD  | STUECKLISTE  |   |   |
| PRO_FIRST_DBPASSWORD | STUECKLISTE  |   |   |
| PRO_FIRST_DBURL | STUECKLISTE  |   |   |
| PRO_FIRST_DBUSER  | STUECKLISTE  |   |   |
| PRO_FIRST_FREMDFERTIGUNGSARTIKEL | STUECKLISTE  |   |   |
| PRO_FIRST_LEAD_IN_AZ_ARTIKEL | STUECKLISTE  | AZ_ |   |
| PRO_FIRST_SCHACHTELPLAN_PFAD | STUECKLISTE |   |   |
| STRUKTURIERTER_STKLIMPORT  | STUECKLISTE  | 0/1/2/3 | 0 = Abgeschaltet, 1 = [Solid Works]( {{<relref "/warenwirtschaft/stueckliste/importe/solid_works">}} ), 2 = [Siemens NX]( {{<relref "/warenwirtschaft/stueckliste/importe/siemens_nx">}} ), 3= [INFRA]( {{<relref "/warenwirtschaft/stueckliste/importe/infra">}} ) |
| STUECKLISTE_ARBEITSPLAN_ZEITEINHEIT  | STUECKLISTE  | h/min/s | Legt fest, in welcher Einheit die Stückzeit und Rüstzeit angezeigt werden soll. |
| STUECKLISTE_ERHOEHUNG_ARBEITSGANG  | STUECKLISTE  | 10 | Wert, um den die Arbeitsgangnummer erhöht wird, wenn eine neue Position eingefügt wird |
| UNTERSTUECKLISTEN_AUTOMATISCH_AUSGEBEN  | STUECKLISTE  | 0/1 | Unterstücklisten werden automatisch ausgegeben |

## Kategorie VERSANDAUFTRAG

| Bezeichnung | Kategorie | Wert(e) | Beschreibung |
| --- |  --- |  --- |  --- |
| IMAPSERVER  | VERSANDAUFTRAG  |  | IMAP Server für Versandablage. |
| IMAPSERVER_ADMIN  | VERSANDAUFTRAG  |  | IMAP Server Admin Konto für Versandablage (Benutzer) |
| IMAPSERVER_ADMIN_KENNWORT  | VERSANDAUFTRAG  |  | IMAP Server Kennwort des Admin Kontos für Versandablage |
| IMAPSERVER_SENTFOLDER  | VERSANDAUFTRAG  |  | IMAP Server Ordner für gesendete Objekte |
| MAIL_SERVICE_PARAMETER | VERSANDAUFTRAG  | 0/1 | Aus welcher Quelle sollen die Mail-Service-Parameter bezogen werden:0 = XML, 1 = Datenbank**Wenn umgeschaltet wird, bitte den Client neu starten** |
| MAILADRESSE_ADMIN  | VERSANDAUFTRAG  |  | Mailadresse des Administratorkontos |
| SMTPSERVER  | VERSANDAUFTRAG  |  | SMTP Server |
| SMTPSERVER_BENUTZER  | VERSANDAUFTRAG  |  | SMTP Server Authentifizierung: Benutzername |
| SMTPSERVER_FAXANBINDUNG  | VERSANDAUFTRAG  |  | 0 .. nummer@Faxdomain, 1 .. XPIRIO |
| SMTPSERVER_FAXDOMAIN  | VERSANDAUFTRAG  |  | SMTP Server Fax-Gateway Domain. |
| SMTPSERVER_KENNWORT  | VERSANDAUFTRAG  |  | SMTP Server Authentifizierung: Kennwort. |
| XPIRIO_KENNWORT  | VERSANDAUFTRAG  |  | Kennwort für XPIRIO Anbindung |

## Anwender Parameter
<a name="Parameter Anwender"></a>Parameter Anwender

Die Parameter Anwender gelten für die gesamte **Kieselstein ERP** Installation.

| Bezeichnung | Wert(e) | Beschreibung |
| --- |  --- |  --- |
| BELEGNUMMER_MIT_MANDANTKENNUNG |   | Belegnummer mit Mandantenkennung (1 = aktiviert) |
| BELEGNUMMER_STELLEN_MANDANTKENNUNG |   | Länge Mandantenkennung in Belegnummer (wenn aktiviert) |
| MANDANTEN_TRANSFERDAUER |   | Transferdauer zwischen Mandanten (in Tagen) |
| REPORT_CONNECTION_URL | jdbc:postgresql://localhost:5432/KIESELSTEIN<br>jdbc:jtds:sqlserver://localhost:1433/KIESELSTEIN | Verbindungszeichenfolge Report(wird von Ihrem **Kieselstein ERP** Betreuer eingestellt) |
| SNR_CHNR_ALPHABETISCH_SORTIERT |   | Snr/Chnr alphabetisch sortiert(0 = nach Buchungszeitpunkt, 1 = Alphabetisch) |

## Arbeitsplatz Parameter
<a name="Arbeitsplatz Parameter"></a>Arbeitsplatz Parameter

Die Arbeitsplatz Parameter gelten für den jeweiligen Arbeitsplatz. D.h. es wird der Name bzw. die IP-Adresse des Rechners als Schlüssel für die Zuordnung des jeweiligen Arbeitsplatzes verwendet.
Beachten bitte auch, dass externe Geräte wie mobile Barcodescanner, Zeiterfassungsstifte, Zeiterfassungsterminals in den Arbeitsplatz-Definitionen aufgeführt werden.
Eine Besonderheit sind die Zuordnungen der Zeiterfassungsstifte, diese finden Sie im Modul Zeiterfassung, unterer Reiter [ZE-Stifte.](../Zeiterfassung/Zeiterfassungsstifte.htm)

| Bezeichnung | Wert(e) | Beschreibung |
| --- |  --- |  --- |
| ANZEIGEDAUER_DRUCKBESTAETIGUNG |   | Zeit in Sekunden, wie lange der Druckbestätigungsdialog angezeigt werden soll. |
| AUFT_SERIENNUMMERNETIKETTENDRUCK |   | Auftragsseriennummernetikettendruck-Symbol im Auftrag anzeigen. |
| AUFTRAGSABLIEFERUNG_IM_LOS |   | Auftragsablieferung-Symbol im Los anzeigen. |
| BACKGROUND_ENABLED |   | Einstellen ob das Hintergrundbild angezeigt werden soll |
| DATEI_AUTOMATISCHE_STUECKLISTENERZEUGUNG |   | Pfad incl. Dateiname für automatische Stücklistenerzeugung. |
| DEFAULT_MOBIL_DRUCKERNAME_ETIKETT |   | Der Name des Druckers der bei mobilen Anwendungen (REST-API) für das Drucken von Etiketten verwendet werden soll |
| DEFAULT_MOBIL_DRUCKERNAME_SEITE |   | Der Name des Druckers der bei mobilen Anwendungen (REST-API) für seitenweises Drucken verwendet werden soll |
| DOKUMENTE_EINLESE_PFAD |   | Default- Einlesepfad der Dokumentenablage. |
| DRUCKERNAME_MOBILER_LIEFERSCHEIN |   | Der Name des Druckers, auf dem der Lieferschein bei mobilen Anwendungen ausgedruckt werden soll. |
| DRUCKERNAME_MOBILES_ARTIKEL_ETIKETT |   | Der Name des Druckers, auf dem das Artikeletikett bei mobilen Anwendungen ausgedruckt werden soll. |
| DRUCKERNAME_MOBILES_LOSABLIEFER_ETIKETT |   | Der Name des Druckers, auf dem das Losablieferetikett bei mobilen Anwendungen ausgedruckt werden soll. |
| DRUCKERNAME_MOBILES_VERSAND_ETIKETT |   | Der Name des Druckers, auf dem das Versandetikett bei mobilen Anwendungen ausgedruckt werden soll. |
| DRUCKERNAME_MOBILES_WA_ETIKETT |   | Der Name des Druckers, auf dem das Warenausgangsetikett bei mobilen Anwendungen ausgedruckt werden soll. |
| DRUCKERNAME_MOBILES_WEP_ETIKETT |   | Der Name des Druckers, auf dem das Wareneingangspositionsetikett bei mobilen Anwendungen ausgedruckt werden soll. |
| EDITOR_SCHRIFT_GROESSE |   | Die Standard-Schriftgröße im Editor (der vorgeschlagene Wert) |
| EDITOR_SCHRIFTART |   | Die Standard-Schriftart im Editor. |
| EDITOR_ZOOM |   | Standardzoom(Faktor) des Editors in Prozent. |
| FERTIGUNG_ANSICHT_OFFENE_LOSE |   | In der Losauswahlliste werden standardmäßig 0=zu produzierende / 1=offene / 2=alle Lose angezeigt. |
| FERTIGUNG_ZUSATZSTATUS |   | ID des Zusatzstatus im Los für Barcodescanner. |
| KDC100_DEFAULTLAGER |   | Default-Lager des Barcodelesestiftes KDC100. |
| KDC100_DEFAULTZIELLAGER |   | Default-Ziellager des Barcodelesestiftes KDC100. |
| KUE_ESSENSAUSGABE_KUNDEID |   | Zugeordneter Kunde zur Essenausgabestelle. Bitte erfragen Sie die passende ID bei Ihrem **Kieselstein ERP** Betreuer |
| KUE_PARTNERKLASSEID |   | Partnerklasse-Lieferant für Küche-Webclient |
| KUE_PROJEKTSHOP |   | Projekt für Lieferantenlieferschein Webclient |
| KUE_PROJEKTWEB |   | Projekt für Bestellungen Webclient |
| KUE_VATERARTIKELGRUPPEID |   | Vater-Artikelgruppe für Küche-Webclient |
| KUE_WEB_MENURECHTE |   | Menü-Rechte für Küche-Webclient |
| LAGERPLATZ_DIREKT_ERFASSEN |   | ID des Lagers für die Direkterfassung der Lagerplatzes im Artikel. |
| LISTEN_NUR_NACH_CRUD_AKTUALISIEREN |   | Alle Listen werden nur mehr nach Neuanlage/Update/Delete eines Datensatzes aktualisiert. Bitte nur nach Anweisung durch Ihren **Kieselstein ERP** Betreuer verwenden! |
| MAX_ANZAHL_IN_AUSWAHLLISTEN |   | Maximale angezeigte Anzahl an Datensätzen in Auswahllisten. Bitte nur nach Anweisung durch Ihren **Kieselstein ERP** Betreuer verwenden! |
| MELDUNG_MEHRSPRACHIGKEIT_ANGEZEIGT |   | Die Meldung "Mehrsprachigkeit steht auf Ihrem **Kieselstein ERP** nicht zur Verfügung" wurde bereits einmal angezeigt. |
| MONATSABRECHNUNG_NEUES_MONAT_AB |   | Ab diesem Tag beinhaltet das vorgeschlagene Monat der Monatsabrechnung, anstatt des Vormonats, das aktuelle Monat. |
| PFAD_MIT_PARAMETER_SCANTOOL |   | Pfad samt Parameter für den Aufruf eines Dokumenten Scann Programms (Twain) |
| PFAD_MIT_PARAMETER_TAPITOOL |   | Pfad samt Parameter für den Aufruf des Wähl-Programms für die Telefonie. Es wird zusätzlich die zu wählende Telefonnummer übergeben. |
| PORT_FINGERPRINTLESER |   | Port, an den der Fingerprintleser angeschlossen ist. |
| PROGRAMMPFAD_FUER_DOKUMENTENLINK |   | Programmpfad für Dokumentenlink, z.B. explorer.exe |
| RECHTSCHREIBPRUEFUNG |   | Aktiviert die Rechtschreibprüfung auf dem Arbeitsplatz. [Siehe](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Rechtschreibprüfung). |
| TAPI_LINE |   | Line-ID des TAPI-Services. |
| TELEFONWAHL_HTTP_REQUEST |   | HTTP-Request für SNOM Telefonie (-> ###NUMMER### wird durch die eigentliche Nummer ersetzt). |
| TEXTFELD_SELEKTION_BEI_FOKUS | 0/1 | Wenn der Fokus auf ein Textfeld wechselt:<br>0 = Cursor steht am Ende des Textes<br>1 = der ganze Text wird selektiert |

## Ansprechen der Serverdrucker
Gerade wenn von den verschiedenen mobilen Geräten Druckfunktionen erwartet werden, muss trotz allem irgend eine Funktion den tatsächlichen Ausdruck durchführen. Im Falle des **Kieselstein ERP** ist das immer der **Kieselstein ERP** Server. D.h. alle Drucker die vom Server her angesprochen werden können, können auch von den mobilen App's, angesprochen werden und werden mit den üblichen Reportvorlagen definiert.

Sollte dein **Kieselstein ERP** auf einem Linux oder MAC Server laufen, müssen diese Drucker über einen guten CUPS Treiber verfügen.

![](arbeitsplatzparameter_mobiler_drucker.png)  
Das bedeutet, dass auf dem **Kieselstein ERP** Server ein Godex Drucker unter dem Namen Godex RT730i eingerichtet ist und wenn die IP-Adresse des Arbeitsplatzes über die RestAPI ein Artikeletikett anspricht, so wird dieses Etikett auf diesem Drucker ausgedruckt.

### Finden warum ein Druck auf den Arbeitsplatzdrucker über den Server nicht geht
In den Server-Log-Dateien sind Einträge mit HvPrinterJRExport enthalten. Hier sieht man auch, welcher Drucker für den jeweiligen Printjob ausgewählt wurde und welche Druckroutine auferufen wurde und von welchem User und welcher IP-Adresse / Rechnernamen.
Damit sollte man auch eventuelle Unstimmigkeiten auflösen können.

Direkte Druckmethoden sind:
| Methode | Eintrag im Server.log |
| --- | --- |
| Wareneingangsetikette | printWepEtikettOnServer |
| Ablieferetikette | printAblieferEtikettOnServer |
| Artikeletikette | printArtikeletikettOnServer |