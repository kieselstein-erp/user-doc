---
categories: ["Fernwartung"]
tags: ["Fernwartung"]
title: "Fernwartung"
linkTitle: "Fernwartung"
date: 2023-01-31
weight: 100
description: >
  Betreuung Ihrer Kieselstein Installation
---
Fernwartung
===========

Für die rasche Betreuung Ihrer **Kieselstein ERP** Installation ist der Zugang zu Ihrer Installation per Fernwartung unbedingt erforderlich. Es stehen dafür zwei Möglichkeiten zur Auswahl.

**a.) Verbindungsaufbau von Ihnen aus zu uns mittels Fernwartungsprogramm**

Vorgehensweise: Nach Aufforderung unsererseits, meistens Aufgrund einer Frage / Aufgabenstellung durch Sie, stellen Sie die Verbindung mittels Fernwartungsprogramm zu unserem Fernwartungsrechner her. Sie können alle von uns durchgeführten Aktionen am Bildschirm mitverfolgen und gegebenenfalls einschreiten.

**b.) Direkter Zugriff unsererseits auf Ihren **Kieselstein ERP** Server**

Hier können wir uns direkt zu Ihrem **Kieselstein ERP** Server verbinden. Üblicherweise erfolgt auch hier der Verbindungsaufbau nur nach Anforderung durch Sie. Diese Art der Fernwartung ist für beide Seiten die effizienteste, setzt aber ein entsprechendes Vertrauen Ihrerseits (dass wir die Daten richtig verwenden) und unsererseits (dass wir von Ihnen nicht wegen Verbreitung Ihrer Daten belangt werden) voraus.
Aufgrund dieses direkten Zuganges können wir auch außerhalb der üblichen Geschäftszeiten entsprechende Maßnahmen auf Ihrem **Kieselstein ERP** Server für Sie durchführen.

**Geheimhaltungsvereinbarung:**

Wie in unserer Branche üblich, unterliegen alle unsere Mitarbeiter einer entsprechenden Geheimhaltungsvereinbarung, welche durch Dienstverträge oder Zusatzvereinbarungen geregelt sind. Aus diesem Grunde werden Ihre Daten bei uns entsprechend vertraulich behandelt.

Sollten beide oben angeführten Möglichkeiten der Fernwartung, aus durchaus nachvollziehbaren Gründen, nicht möglich sein, so kann eine Betreuung nur vor Ort vorgenommen werden. Bitte bedenken Sie, dass in diesem Falle ein Nachvollziehen Ihrer Aufgabenstellungen für uns nur sehr mühsam möglich ist und daher sehr oft ein Besuch bei Ihnen im Hause erforderlich ist. Die dafür anfallenden Reisespesen und Besuchskosten müssen wir Ihnen zu den jeweils gültigen Sätzen verrechnen.