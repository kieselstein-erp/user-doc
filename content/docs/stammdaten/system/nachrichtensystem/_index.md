---
categories: ["Nachrichtensystem"]
tags: ["Nachrichtensystem"]
title: "Nachrichtensystem"
linkTitle: "Nachrichtensystem"
date: 2023-01-31
weight: 220
description: >
  Nachrichten innerhalb des Kieselstein-ERP-Systems versenden
---
Nachrichtensystem
=================

In **Kieselstein ERP** ist auch ein Nachrichtensystem inkl. Nachrichten Archiv integriert.

Damit können allgemeine Nachrichten versandt werden.

Entwickelt wurde es vor allem um bestimmten Nutzergruppen bei bestimmten Ereignissen entsprechende Informationen zukommen zu lassen, damit diese rasch handeln können.

Die Definition des Nachrichtensystems finden Sie im Modul Benutzer, unterer Modulreiter Nachrichten.

Im ersten Reiter Nachrichtenart finden Sie alle derzeit von **Kieselstein ERP** definierten spezifischen Nachrichten. Also diejenigen Ereignisse auf die **Kieselstein ERP** reagiert.

Die Kennung ist fix vorgegeben. Sie können jedoch definieren, welche Bezeichnung dieses Ereignis bei Ihnen im Hause haben soll und zu welchem Thema es zugeordnet ist. Weiters kann definiert werden, ob am Schirm das Anwenders eine Meldung (Popup) erscheinen sollte und ob das Ereignis archiviert werden sollte.

![](Nachrichten1.gif)

Im oberen Modulreiter Thema sind die Themen ersichtlich. Diese sind aus Gründen der internen Verwaltung fest vordefiniert.

Im oberen Modulreiter Themarolle nehmen Sie nun die Verbindung / Verdrahtung zwischen den Themen und den Systemrollen Ihrer Benutzer vor.

![](Nachrichten2.gif)

So bedeutet z.B. obige Definition dass:

Alle Ereignisse die dem Thema (= TopicFert) Fertigung zugewiesen sind, an alle Personen der Systemrolle Entwickler gesandt werden.

Diese erhalten z.B. folgende Meldung:

#### Können allgemeine Meldungen verschickt werden.
Ja.

Gehen Sie in das Systemmodul, wählen Sie den unteren Modulreiter Nachrichten und klicken Sie oben auf ![](Nachrichten3.gif) Neue Nachricht an Alle.

#### Werden nicht bearbeitete Meldungen bei jedem Login angezeigt?
Nein. Es werden nur Meldungen angezeigt, die seit dem letzen Login noch nicht erstmalig aufgetreten sind. Ihre offenen Meldungen sehen Sie im Systemmodul, unterer Modulreiter Nachrichten.

#### Ich habe mehrere Clients offen, bekommen alle die Meldung?
Nein. Die Meldungen werden immer nur an den ersten Client gesandt, der dem Meldungs-Abonnement entspricht. Sollten Sie mehrere **Kieselstein ERP** Clients geöffnet haben, so erhält die Meldungen nur einer.

Erhalten Sie keine Meldungen, so empfiehlt sich Ihren Rechner neu zu starten, damit eventuell hängen gebliebene **Kieselstein ERP** Sitzungen vollständig geschlossen werden.

#### Kann ich die noch anstehenden Meldungen löschen?
Ja. Es gibt über die JMX Console die Möglichkeit alle Meldungen auf einen Schlag zu löschen.

Das löschen ausgewählter Nachrichten ist nicht vorgesehen.

**Hinweis:** Da die Nachrichten parallel in der **Kieselstein ERP** Datenbank abgelegt werden, bleiben die alten Nachrichten erhalten. Durch obigen Vorgang wird nur das "aufpoppen" der alten Nachrichten unterbunden.

Wie kommen Sie zur JMX Console?

Starten Sie einen Browser und gehen Sie auf <http://Kieselstein-ERP-Server:8080/jmx-console/>

Rollen Sie nun einige Seiten nach unten bis zur Überschrift jboss.mq.destination.

Wählen Sie hier nun das entsprechende Topic (Thema), z.B. [name=FertTopic,service=Topic](http://192.168.0.5:8080/jmx-console/HtmlAdaptor?action=inspectMBean&name=jboss.mq.destination%3Aservice%3DTopic%2Cname%3DFertTopic)

In der nun erscheinenden Darstellung wählen Sie:

![](Nachrichten_loeschen.gif)

removeAllMessages() durch klick auf Invoke.

**Hinweis:** Damit werden die alten noch nicht angenommenen Meldungen insoweit gelöscht, dass Sie dazu nicht mehr aufgefordert werden, also die Meldungen aus dem Nachrichtensystem gelöscht. Die, bereits erfolgte Protokollierung der Nachrichten ist davon nicht betroffen.

**Hinweis:** Sollte die JMX Console nicht gestartet werden können, so ist davon auszugehen, dass diese von Ihrem **Kieselstein ERP** Betreuer abgeschaltet wurde, was insbesondere bei Zugriffen von Extern dringend angeraten wird.

EMail-Versand Nachrichten
=========================

Da es immer wieder die Aufgabe gibt, dass erkannt werden sollte, dass z.B. EMails nicht versandt werden konnten, haben wir ein Nachrichtensystem geschaffen.
Sie finden dieses zusätzliche Modul Nachrichten in der Liste der Module unter Extras, bzw. rechts neben dem System ![](Nachrichten4.jpg).
Grundsätzlich ist dieses Nachrichten System so aufgebaut, dass es Nachrichtenarten gibt, z.B. EMAIL_VERSAND_FEHLGESCHLAGEN. Hier kann unter Detail eine eventuelle firmeninterne Bezeichnung festgelegt werden. Im Reiter können einzelne Mitarbeiter oder auch Nachrichtengruppen zugewiesen werden. Diese Personen bzw. Personengruppen bekommen die jeweiligen Nachrichten zugestellt.
Die Definition der Nachrichtengruppen erfolgt im unteren Modulreiter Nachrichtengruppen.
Weiters finden Sie hier den Verlauf aller Nachrichten.

Ob nun Nachrichten für Sie vorhanden sind, wird in der Statuszeile im vierten Feld von links angezeigt ![](Nachrichten5.gif).
So bedeutet die linke Zahl die Anzahl neuer Nachrichten.

Durch Rechtsklick auf dieses Feld erhalten Sie die Liste der alten Nachrichten an Sie. Hier können Sie zusätzlich eine manuell neue Nachricht an einzelne Benutzer versenden.
![](Nachrichten6.jpg)
Tippen Sie in der Zeile unten den gewünschten Text ein und wählen Sie rechts unten aus den Benutzer (es werden die Kurzzeichen der Personen dargestellt) und drücken Sie Enter um die Nachricht abzusenden.

Das l

Nachrichteneinträge
.... Projektzeitüberschritten ... wird dann an die Empfänger versandt, wenn die Tätigkeit auf diesem Projekt beendet wird und die ![](Nachrichten7.jpg)geschätzte Sollzeit für das Projekt überschritten ist.

Aktuelle Anzeige: Grau ... von mir an einen anderen gesandt
Schwarz ... von jemand anderem an mich gesandt
-> von mir noch nicht gelesen