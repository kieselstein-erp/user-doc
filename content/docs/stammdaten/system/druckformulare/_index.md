---
categories: ["Druckformulare"]
tags: ["Druckformulare"]
title: "Druckformulare"
linkTitle: "Druckformulare"
date: 2023-01-31
weight: 020
description: >
  Druckformulare
---
Druckformulare
==============

In **Kieselstein ERP** finden Sie eine Vielzahl von Druckformularen / Druckvorlagen.
Um dem Anwender die freie Gestaltungsmöglichkeit bei gleichzeitiger Forderung nach jederzeitiger Erweiterbarkeit / Updatemöglichkeit zu erfüllen und die Mehrsprachen- und Mandantenvarianten zu berücksichtigen wurde folgende Vorgehensweise / Verzeichnisstruktur gewählt.

**ACHTUNG**: Ab der Version 1.x.1 wurde die Aufteilung von dist und data auch in den Reports umgesetzt. Dies vor allem, um die Updatefähigkeit der Fremdsprachigen Reports zu erreichen. Für [Details siehe bitte]( {{<relref "/docs/installation/10_reportgenerator/anwenderreports/">}} ).
Vom Gedankengang wurde die Ebene Anwender durch den Bereich data/reports ersetzt. D.h. die Anwenderreports befinden sich nun dort.<br>
Z.B. wurde der Bereich<br>
.../kieselstein/dist/wildfly-26.1.2.Final/kieselstein/reports/allgemein/anwender/...<br>
bzw. vor der 1.x.x<br>
.../kieselstein/dist/wildfly-12.0.0.Final/server/helium/report/allgemein/anwender/...<br>
nach<br>
.../kieselstein/data/reports/anwender/...<br>
verlagert.

Daraus folgt, dass die nachfolgende Beschreibung **nur bis Version 1.0.0** gilt.
![](Report_Struktur.jpg)
**WICHTIG:** Alle Formulare / Definitionen in der **Kieselstein ERP** Schicht werden bei jedem Update Ihrer **Kieselstein ERP** Installation überschrieben, um sicherzustellen, dass in der Anwenderinstallation die aktuellen Formulare verfügbar sind. Formulare und Definitionen in der Anwenderschicht sind Anwenderspezifisch erstellte Definitionen / Vorlagen und werden bei Updates nicht gepflegt / aktualisiert, damit Sie immer Ihre Formulare vorfinden.

Die in **Kieselstein ERP** hinterlegte Logik ist folgende:
Es wird die Formulardefinition wie folgt durchsucht:
Als erstes wird versucht anhand der Mandanten und Sprachdefinition des Beleges ein entsprechendes Formular (einen Report) entsprechend der Sprachebene (dem Locale) zu finden. Ist hier kein Formular eingetragen, wird versucht dieses Formular eine Ebene höher (ausgehend von der Report-Ebene als Wurzel hin zu den tiefer gelegenen Anwender-/ Sprachdefinitionen), also in der Mandanteneben ohne Sprachabhängigkeit zu finden. Ist auch hier keine Formular vorhanden, so wird in der Modulebene Anwender das Formular gesucht. Ist das Formular auch hier nicht eingetragen, so wird in der Modulebene **Kieselstein ERP** das Formular gesucht. Wird auch hier kein Formular gefunden, wird eine entsprechende Fehlermeldung ausgegeben.

Die Modulebene Anwender ist dafür gedacht, dass Anwenderweit, also in Ihrer gesamten **Kieselstein ERP** Installation, ein und das selbe Formular verwendet wird, z.B. für ein Rechnungsjournal, welches zwar nicht dem **Kieselstein ERP** Standard entspricht, aber bei Ihnen für alle Mandanten und Zweigniederlassungen einheitlich sein soll.
In den Formularen gibt es die Sonderdefinition der Firmenlogo Subvorlagen. Diese dienen dazu, Ihr "Briefpapier" direkt auf den Vorlagen mitzudrucken. Diese befinden sich in der Regel im Verzeichnis allgemein, wobei die Mandanten bzw. sprachabhängigen Varianten nach dem selben Schema definiert werden.
Eine weitere Definition sind die Vorlagen für den EMail Versand. Diese werden grundsätzlich unter allgemein abgelegt. Nur die Beleg spezifischen Ausprägungen werden bei den Belegen abgelegt.
Die Namen der Unterverzeichnisse der Mandanten sind die Mandantennummern. Z.B. 001, 002 usw.

### Trennung der Druckformulare nach Kostenstellen:
In **Kieselstein ERP** besteht zusätzlich die Möglichkeit die offiziellen Druckformulare Kostenstellenspezifisch zu trennen. Dies wird z.B. für unterschiedliche Logos je Kostenstelle von Angebot bis Rechnung benötigt.

Dazu kann in der Kostenstellendefinition (System, Mandant, Kostenstelle) ein ![](Kostenstellen_Subdirectory.gif) angegeben werden. Das Subdirectory ist von obiger Schichtenstruktur **neben** der anwender Schicht zu sehen. Bitte beachten Sie, dass abhängig vom Betriebssystem Groß-/Kleinschreibung unterschieden wird.

### Kostenstellen Druckersteuerung
<a name="KostenstellenDruckersteuerung"></a>

**Hinweis:**
Diese Kostenstellendefinition ist dafür gedacht, um für unterschiedliche Kostenstellen unterschiedliche Briefpapiere verwenden zu können.

Wenn von Ihnen für den Druck nur weißes Papier verwendet wird, so reicht es aus wenn nur die Kopf- und Fußlogos je Kostenstelle definiert werden. Sollte jedoch je nach Kostenstelle auf unterschiedliche Drucker(-schächte) gedruckt werden, so ist es erforderlich, dass auch die Reports, auch wenn sie vollständig ident sind, in die Kostenstellen Subdirectorys aufgeteilt werden. Nur damit ist die automatische Druckersteuerung über die Formularnamen möglich.

### Bearbeiten der Druckformulare

Die Formularvorlagen können mit dem iReport Generator bearbeitet werden. 
Der Basispfad (Report) des obigen Diagramms liegt in einer Standardinstallation auf deinem **Kieselstein ERP** Server auf ?:\kieselstein\dist\wildfly-12.0.0.Final\server\helium\report, bzw. /opt/kieselstein/dist/wildfly-12.0.0.Final/server/helium/report.<br>

**Siehe unbedingt Kapitel Reportgeneratoren**

### Größe des Logos
<a name="Größe des Logos"></a>
Achte bei der Verwendung der Logos, dass diese zwar in ausreichender Qualität, aber, von der Dateigröße gesehen, so klein wie möglich sind.<br>
Da beim Email bzw. Faxversand immer eine Kopie des digitalen Drucks in die Dokumentenablage abgelegt wird, wirkt diese Größe für das Wachstum der Dokumente-Datenbank und damit auch für die Größe und Dauer der Datensicherung.<br>
Gerade beim EMail-Versand geht die Größe des Logos auch direkt in die Größe der erzeugten PDF-Datei und damit in die Geschwindigkeit der diversen Übertragungs- bzw. Konvertierungsprogramme ein.

### Subreports
Werden zu Berichten Subreports angelegt, so müssen diese immer in der Ebene des (Haupt-)Reports sein. Davon ausgenommen sind die Subreports der Kopf- und Fußzeilen. Diese sind IMMER unter allgemein abgelegt. Wenn für einen speziellen Report die Kopf- oder Fußzeilen geändert werden müssen, so dürfen diese nur als gewöhnlicher Subreport angelegt werden.

**Beispiel:**

Auftrag und der Subreport 2 sollte geändert werden.
    Ebene report

        Ebene auftrag

> Auftragsbestätigung
>
>     Auftragsbestätigung_Sub1
>
>     Auftragsbestätigung_Sub2
>
>     Auftragsbestätigung_Sub3

Verschieben Sie nun sowohl den Hauptreport und alle Subreports in das Anwender Verzeichnis. Ändern Sie hier den Subreport wie gewünscht ab.

Erstellung von Reportvarianten

Um eine weitere Version eines Ausdrucks in **Kieselstein ERP** zu erstellen, können Reportvarianten definiert werden. Ein Anwendungsfall wäre, dass man eine Variante des Artikeletiketts mit weniger Informationen benötigt. Erstellen Sie dazu eine zweite Version des gewünschten Reports - in diesem Fall des Artikeletiketts (zb. ww_artikeletikett2). Bitte speichern Sie diesen in Ihrem Anwender-Verzeichnis ab.

Wechseln Sie nun in das Modul System, unterer Reiter Medien, oberer Reiter Reportvariante![](reportvariante.JPG).

Sie finden eine Liste der definierten Reportvarianten Ihrer Installation vor. Um nun eine neue Variante zu definieren, klicken Sie auf Neu und füllen die folgenden Felder aus.

![](reportvariante_neu.JPG)

Geben Sie den ursprünglichen Reportnamen, den nun neuen Reportnamen und die Bezeichnung, wie diese in **Kieselstein ERP** in der Auswahl angezeigt werden soll, ein. Nach dem Abspeichern der Einstellungen können Sie im Druck des Etiketts die Variante auswählen indem Sie neben dem Namen des Druckers die jeweilige Bezeichnung zb. Etikett klein auswählen.

#### Kann man das Logo selber definieren?
Es ist auf allen offiziellen Belegen rechts oben auf der ersten Seite ein Logo vorgesehen. Das Standard **Kieselstein ERP** Logo finden Sie unter ?:\kieselstein_wildfly\server\helium\report\allgemein\logo.png

Legen Sie in diesem Verzeichnis ein weiteres Verzeichnis anwender an.

Kopieren Sie an diese Stelle Ihr Firmenlogo im png Format mit dem Namen logo.png.

Ab sofort wird der Report mit Ihrem Logo gedruckt.

HINWEIS: Das Logo.png kann eine Größe bis zu ca. 775x325 Pixel (BxH) in 300dpi Auflösung besitzen.

Hinweis2: Um auf das Logo tatsächlich einzubinden, benötigen Sie noch zusätzlich die Datei logo.jasper im entsprechenden Anwenderverzeichnis.

#### Kann ich Fußzeilen auch selber definieren?
Im gleichen Verzeichnis wie oben beschrieben, steht der allgemeine Report für die Fußzeilen unter  ?:\kieselstein_wildfly\server\helium\report\allgemein\fuss.jasper zur Verfügung.
Bitte denken Sie daran auch diesen in das Anwender Verzeichnis zu kopieren.

#### Welche Dateien benötige ich in meinem spezifische Anwenderverzeichnis um Logo und Fußzeilen zu drucken?
Um auf den Belegdrucken (Angebot - Bestellung) Ihre spezifischen Logos und Fußtexte mitzudrucken benötigen Sie im allgemeinen Anwender Verzeichnis folgende Dateien:

| Reportname | Bedeutung |
| --- |  --- |
| logo.jasper | Logo Reportdatei |
| logo.png | Bild Ihres Firmenlogos / Briefpapiers |
| fuss.jasper | Report für die Fußzeilen |

Bitte beachten Sie, dass diese nur im Verzeichnis report\allgemein\anwender verändert werden dürfen.

<a name="Email Vorlagen"></a>
#### EMail Vorlagen

#### Wo können die Texte für den automatischen EMail-Versand (E-Mail Text und Betreff) verändert werden?
Sie finden die Basis EMail Vorlagen unter report\allgemein die Datei mail.xsl.
Dies ist die Basisvorlage (Style-Sheet) für den Inhalt des Plain-Text Emails, welches an den EMail-Empfänger gesandt wird. 
Anmerkung: An dieses EMail wird ein PDF Anhang angefügt, in welchem das eigentliche Dokument z.B. Angebot enthalten ist.
Beachten Sie unbedingt die Forderungen für den [EMail Versand]( {{<relref "/docs/installation/15_email_einrichten/#voraussetzungen-die-gegeben-sein-m%C3%BCssen-um-e-mails-richtig-weiterreichen-zu-k%C3%B6nnen"  >}} ).

Sie können nun Ihre eigene mail.xsl für allgemeine EMails erstellen oder auch ganz spezielle für den jeweiligen Modulbereich z.B. Angebot.
Wird die Datei mail.xls nach allgemein\anwender kopiert, so ist dies die Standarddatei für Ihre gesamte **Kieselstein ERP** Installation.
Wird die Datei mail.xls unter allgemein in die Unterverzeichnisse der Mandanten kopiert, so gilt sie nur für den jeweiligen Mandanten.
Wird die Datei mail.xls hingegen in das anwender Verzeichnis eines Moduls kopiert, so muss sie auf den Reportnamen mit der Erweiterung xsl umbenannt werden. In anderen Worten, es kann für jede Reportvorlage eine eigene EMail-Vorlage erstellt werden. Diese Datei hat den gleichen Namen wie die Reportvorlage, jedoch die Erweiterung .xsl

Die Anpassung des Betreffs der E-Mail ist vergleichbar mit dem soeben beschriebenen Vorgehen. 
Hierzu erstellen Sie eine Datei im Allgemein-Verzeichnis: betreff.xsl. Dateien für spezielle Reports heißen beispielsweise rech_rechnung_betreff.xsl (siehe oben wie mail.xsl).
Die gleiche Vorgehensweise gilt für übersteuerte Absender, siehe dazu absender_vorlage.xsl
Um nun auch die Namen der an das EMail angehängten Dateien übersteuern zu können, siehe anhang_vorlage.xsl
Zusätzlich gelten die weiter oben beschriebenen Regeln für Mandanten und Sprachen.

Für die Erzeugung von html EMail Dateien mit Verlinkung zu Bildern, z.B. Firmenlogos, verwenden Sie bitte mail_html.xsl als Muster bzw. die Datei mail_signatur.xsl. Bitte beachten Sie in den html Vorlagen, die doppelte html Codierung.

Folgende Parameter können in dieser Datei verwendet werden:

| Parameter | Bedeutung |
| --- |  --- |
| anrede_ansprechpartner | Anredestring für den Ansprechpartner des Beleges. Z.B. Sehr geehrter Herr xy, |
| belegnummer | Z.B. die Angebotsnummer |
| bezeichnung | Bezeichnung des Beleges im Klartext. Z.B. das Angebot Nr. 10/0123456, oder Auftrag usw.Für spezifische Übersetzungen usw. verwenden Sie bitte belegnummer und geben die Bezeichnung des Beleges direkt im XSL an. |
| belegdatum | Z.B. das Angebotsdatum |
| projekt | Das Projekt aus dem Modul, z.B. Angebot |
| bearbeiter | Der beim Modul z.B. Angebot, eingetragene Vertreter ev. mit Unterschrifts- Funktion |
| fusstext | Der Fußtext des Moduls, z.B. Angebot |
| text |  |
| bearbeiter_vorname | Vorname des angemeldeten Benutzers |
| bearbeiter_nachname | Nachname |
| bearbeiter_titel | Titel |
| bearbeiter_telefonfirma | Mandanten-Telefonnummer |
| bearbeiter_telefondwfirma | Durchwahl (Personal, Daten, Absenderdaten, Durchwahl) |
| bearbeiter_email | Firmen-EMail (Personal, Daten, Absenderdaten, E-Mail) |
| bearbeiter_mobil | Firmen Mobiltelefonnummer |
| bearbeiter_telefonfirmamitdw | Kombinierte Telefonnummer ev. mit Durchwahl |
| bearbeiter_direktfax | Direkt Fax aus Personal, Daten, Absenderdaten |
| bearbeiter_faxdwfirma | Faxdurchwahl (Personal, Daten, Absenderdaten, FaxDW) |
| bearbeiter_faxfirmamitdw | Kombinierte Faxnummer ev. mit Durchwahl |
| bearbeiter_unterschriftsfunktion | Unterschriftsfunktion aus Personal, Daten |
| bearbeiter_unterschriftstext | Unterschriftstext aus Personal, Daten |
| firma_name1 | Mandantenname1 |
| firma_name2 | Mandantenname2 |
| firma_name3 | Mandantenname3 |
| firma_lkz | Länderkennzeichen Mandant |
| firma_plz | Postleitzahl Mandant |
| firma_ort | Ort Mandant |
| firma_strasse | Straße Mandant |
| kundenbestellnummer | Kundenbestellnummer, insbesondere wenn Lieferschein, dann aus Auftrag |
| abnummer | Auftragsbestätigungsnummer bei Lieferschein, Packliste etc. |

Die Bearbeitung des Style-Sheets setzt Kenntnisse in der XSLT Programmierung voraus.
Für weitere Infos wenden Sie sich bitte an Ihren **Kieselstein ERP** Händler oder direkt an uns.
Verifizierung des Style-Sheets mit msxsl.exe oder mit Xalan.

Siehe dazu auch ..\report\all\allgemein\mail_html.xsl .. Taglist

#### HTML EMail definieren
Um die Bearbeitung der HTML Vorlagen zu vereinfachen, haben wir einen im **Kieselstein ERP** Client integrierten Editor entwickelt.<br>
Bitte beachte, dass dafür in deiner **Kieselstein ERP** Installation die Funktionalität HTML EMail zur Verfügung stehen muss und dass für die Verwendung der Parameter html_signatur initialisiert sein muss. Idealerweise zeigt der Parameter auf eine öffentlich erreichbare Repräsentation deines / eures Firmenlogos.

Diesen finden Sie im Modul System, unterer Reiter Pflege, oberer Reiter Beleg Pflege und dann im Baum, Pflege, Allgemein, XSL Vorlage ändern.
Durch Klick auf Öffnen wählen Sie die anzupassende XSL Vorlage aus.

![](XSL_HTML_Vorlagen_editieren.png)

Nutzen Sie als Ausgangsbasis die im Verzeichnis allgemein mitgelieferte mail_signatur.xsl
Um dies möglichst allgemeingültig zu halten, wird auch der Firmenwortlaut übergeben.
D.h. für eine gute Umsetzung müssen im Personal, im Reiter Daten folgende Werte eingetragen werden:

-   Firmenzugehörigkeit, also Ihr Mandant
-   Unterschriftsfunktion (i.V., i.A.)
-   Unterschriftstext (Geschäftsleitung, Produktionsleitung)
-   Absenderdurchwahl
-   EMail
-   und wenn gewünscht auch die Mobiltelefonnummer (Handy)

Bitte beachten Sie weiters, dass für eine vollautomatische und gute Formulierung eine entsprechende Abstimmung zwischen den Grunddaten der Kopf- und Fußtexte der Module, der Formularvorlagen und der EMail Vorlagen erforderlich ist.

Um Beleg spezifische Vorlagen verwenden zu können, kopieren Sie das mitgelieferte mail_signatur.xsl in das jeweilige Anwenderverzeichnis. Die Benennung ist wie üblich Reportname ergänzt um das Wort _signatur.xsl. Also für das Angebot z.B. angb_angebot_signatur.xsl.

Sollte bei der Öffnung der XSL Datei die Fehlermeldung ![](HTML_Signatur_Vorlage_Fehler.png)<br>
kommen, so bedeutet dies, dass die Erweiterungen für den **Kieselstein ERP** HTML Editor in dieser Vorlage nicht enthalten sind.

Nach der Übernahme der Vorlage stehen im rechten Bereich die Variablen welche im HTML Text verwendet werden können zur Verfügung. Um diese in den HTML Text zu übernehmen, kopieren Sie den Parameter mittels Doppelklick in die Zwischenablage und fügen diese dann an der gewünschten Stelle mit Strg+V ein.

#### Können die Formularvorlagen und die Vorlagen für die EMail Texte auch direkt am MAC bearbeitet werden?
Ja. Hier empfiehlt es sich, für die Bearbeitung der Reportvorlagen einen Alias einzurichten. [Siehe dazu]( {{<relref "/docs/stammdaten/system/druckformulare/am_mac_bearbeiten">}} ).

#### Können auch Bilder mitgedruckt werden?
Ja, genauso wie die verschiedenen Textmodule und Artikelkommentare mitgedruckt werden können, können auch Bilder mitgedruckt werden. Bitte achten Sie bei der Angabe von Dateinamen aber darauf, dass diese nur Kleinbuchstaben, Ziffern und _ (Unterstrich) beinhalten. Alle anderen Zeichen erschweren die Betriebssystemübergreifende Verwendung in sehr hohem Ausmaß.

#### Können von einer Auswertung auch verschiedene Varianten erzeugt werden?
**Kieselstein ERP** unterstützt auch die Verwendung von Reportvarianten oft auch Reportmutationen genannt. [Siehe]({{< relref "/docs/stammdaten/system/druckformulare/druckformular_varianten.md" >}})

Die dann definierten verschiedenen Reportvarianten können neben der Druckerauswahl für den Report

![](Reportvariante_Auswaehlen.gif)

ausgewählt werden. Durch Klick auf speichern (rechts neben der Reportvariante) wird auch die gewünschte Reportvariante mit gespeichert und so automatisch für den default Druck vorgeschlagen.

Praktisch sind Reportvarianten, wenn von der gleichen Datenbasis unterschiedliche Sichten dargestellt werden sollte. Z.B. Bei einem Angebot dieses ohne Summen, oder bei einer Liste der offenen Aufträge, welche Sie für die Mitarbeiter der Produktion verwenden wollen, diese ohne Verkaufspreise und ohne Deckungsbeiträge zur Verfügung zu stellen.


-----------------------

<a name="Editor Breiteneinstellung"></a>

#### Können die Breiten der Editoren passend zu den Texten eingestellt werden?
Ja. Es  können für verschiedene Arten von Texteingaben auch die Breiten im Texteditor angepasst werden.

Die Anpassung wird im System, Parameter mit dem Parameternamen Breite vorgenommen.

![](Editorbreite.jpg)

Die Standardwerte sind so, dass diese zu den Formularen passen.
| Bezeichnung | Beschreibung |
| --- | --- |
| Editor_Breite_Kommentar | wird für die Kommentare aus den Artikeln verwendet |
| Editor_Breite_Texteingabe | wird für die Texteingaben in den offiziellen belegen verwendet |
| Editor_Breite_Textmodul | wird für die Breite der Texteingaben in die Textmodule (System, Medien) verwendet |
| Editor_Breite_Sonstige | wird für alle anderen Texteingaben verwendet |

#### Können auch direkte Datenbankabfragen mit dem Reportgenerator durchgeführt werden?
Ja. Es können über P_SQLEXEC spezielle Querys abgesetzt werden. Dafür stehen folgende Methoden zur Verfügung:
| Methode | Bedeutung | Muster |
| --- | --- | --- |
| .execute | Ausführen eine SQL Befehls, welcher einen Rückgabewert liefert| fert_fehlteile.jrxml |
| .executes | Ausführen eine SQL Befehls, welcher ein Array von Rückgabewerten  liefert|  |
| .subreport | Liefert ein Subreport-Objekt mit der angegebenen Anzahl an Spalten | |

#### Können die Listendrucke, Auswahllisten, FLR-Drucke angepasst werden?
In **Kieselstein ERP** steht auch der generische Listendruck zur Verfügung. Auch diese Listen können abhängig angepasst werden. Es ist jedoch darauf zu achten, dass bei eventuellen Änderungen, speziell angepasste Listen nachgezogen werden müssen.
Eine Besonderheit ist hier die Verwendung von Boolschen Variablen. Diese werden an die Anwenderspezifischen Listen als String übergeben. Ein Leerstring bedeutet false, ein X im String bedeutet true.
Werden Werte in Prozent übergeben, z.B. Skonto, so wird dies ebenfalls als String übergeben und der Zahlenwert ergänzt um das % Prozentzeichen ausgegeben.