---
categories: ["Druckformulare"]
tags: ["Druckformulare"]
title: "Druckformulare am MAC bearbeiten"
linkTitle: "am MAC bearbeiten"
date: 2023-01-31
weight: 420
description: >
  Wie Druckformulare am MAC bearbeiten
---
Druckformulare direkt am MAC bearbeiten
=======================================

Um die Reportvorlagen am MAC möglichst einfach direkt aus der Oberfläche zu ermöglichen, sollte ein Alias in das **Kieselstein ERP**-Server-Applikation-Paket angelegt werden.

Gehen Sie dazu wie folgt vor:

**<u>Hinweis:</u>**

Die Beschreibung wird für einen Zwei-Tasten Maus erstellt. Insbesondere die Bearbeitung der Formulare ist nur mit einer Zwei-Tasten-Maus effizient möglich.

1.  Rechtsklick auf den **Kieselstein ERP**-Server, Paketinhalt zeigen

2.  Erweitern Sie auf Contents, Resources, server, helium

3.  Rechtsklick auf Report und wählen Sie Alias erzeugen.

4.  Nun erhalten Sie den Reportalias direkt in der **Kieselstein ERP** Struktur. Klicken Sie den Alias an und ziehen Sie ihn z.B. auf den Desktop / Schreibtisch

5.  Nun wird bei Doppelklick auf das ReportAlias Icon die **Kieselstein ERP** Reportstruktur geöffnet. Zur weiteren Vorgehensweise gehen Sie wie unter Druckformulare beschreiben vor.

**ACHTUNG:**

Die Bearbeitung der XSL Dateien für den Email Versand, darf am MAC nur mit dem TextEdit erfolgen. Daher Rechtsklick auf die Datei, öffnen mit anderem Programm, TextEdit auswählen.

**Hinweis:**

Diese Dateien sind in der Codierung Betriebssystem-spezifisch und können daher auf anderen Betriebssystemen nicht verwendet werden.

![](Druckformulare_Alias_am_MAC_erzeugen.jpg)