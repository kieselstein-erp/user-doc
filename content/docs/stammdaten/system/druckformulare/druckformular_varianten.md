---
categories: ["Druckformulare"]
tags: ["Druckformulare"]
title: "Druckformulare hinzufügen"
linkTitle: "hinzufügen"
date: 2023-01-31
weight: 410
description: >
  Weitere Versionen von Druckformularen / Reports erzeugen
---
Formulare hinzufügen
--------------------

Es können Formular-mutationen hinterlegt werden.
Um eine weitere Mutation/Variante eines Reports/Formulars zu erzeugen geht man wie folgt vor:
1. Definition des Ausgangs-Reports z.B. pers_zeitdaten
2. System, unterer Reiter Medien, obererer Reiter Reportvariante
3. Neu, ![](Reportvariante_definieren.png)  
  - Den Ausgangs-Reportnamen in Reportname eintragen.
  - Den gewünschten Reportnamen unter Reportvariante eintragen.
  - Die gewünschte Bezeichnung eintragen. Diese erscheint dann in der Auswahlcombobox der Reportvariante<br>![](Reportvariante_auswaehlen.png)

Hinweis: Für die Auftrags-Packliste Artikelgruppen Steuerung und für die Unterscheidung der Zahlscheine bei der Eingangsrechnung wird dies nicht unterstützt.

Hinweis2: Auch in der Reportvariante stehen nur die originär verfügbaren Fields und Parameter zur Verfügung.
