---
title: "Systemweite Stammdaten"
linkTitle: "System-Stammdaten"
categories: ["Systemweite Stammdaten"]
tags: ["Mandanten", "Textmodule", "Einheiten", "Eigenschaften"]
weight: 900
description: >
  Hier werden grundsätzliche Stammdaten Ihres ERP-System's definiert.
---
System
======

Hier finden Sie allgemeine System Informationen zu **Kieselstein ERP**.
Für statistische Informationen über Ihr System [siehe]({{< relref "/docs/stammdaten/system/systemstatistik" >}})

## Verbindungsmöglichkeiten zu **Kieselstein ERP**

Die Verbindung zum **Kieselstein ERP** Server erfolgt grundsätzlich über RMI. Dies bedeutet für den Anwender, dass vom Client zum Server eine TCP/IP Verbindung bestehen muss. Im Intranet ist dies in den heute üblichen Netzwerken in der Regel bereits installiert. Von außen empfehlen wir zur Anbindung eines Clients an das interne Netzwerk die Verbindung über VPN (virtual private network). VPN Verbindungen werden als Hardware und / oder Software-Lösung angeboten.<br>
Verbindungen wie z.B. Terminalclients sind bei der Verwendung von **Kieselstein ERP** nicht erforderlich. Der Vorteil daraus:
- kein Terminalserver erforderlich
- geringerer Netzwerkverkehr

Für alle weiteren technischen Informationen siehe bitte [Installation]({{< relref "/docs/installation/02_client" >}})

# Grundaufbau

**Kieselstein ERP** ist in der sogenannten Dreischicht-Architektur realisiert. Besteht also aus Datenbank-Server, Applikations-Server und dem eigentlichen Client. Zusätzlich steht als vierte Schicht (Multi-Tier-Model) der Zugriff über Webservices Rest-API, JSon zur Verfügung. Hinweis: SOAP wird nicht unterstützt. Für weitere Dokumentation dazu [siehe]({{< relref "/docs/stammdaten/system/web_links" >}}), bzw. wende dich bitte an deinen **Kieselstein ERP** Betreuer.

### Kann man **Kieselstein ERP** mehrfach starten?
Es ist oft sehr praktisch den **Kieselstein ERP** Client mehrfach zu öffnen. Starten Sie dazu in Windows-Systemen einfach den Client durch erneuten Doppelklick auf das **Kieselstein ERP** Icon ein zweites und ein drittes Mal. Die interne Verwaltung der Rechte und Zugriff ist so gestaltet, dass die Sperren auf den jeweiligen Client wirken.

Beispiel: Wenn Sie im ersten Client den Kunden Müller gerade ändern, so kann der exakt gleiche Kunde Müller im zweiten Client zwar angesehen, aber nicht verändert werden, solange bis sie im ersten Client den Kunden wieder freigegeben (speichern oder verwerfen) haben.

Wichtig: Die in **Kieselstein ERP** realisierte Zwischenablage legt die Daten am Client PC ab. Damit haben Sie die Möglichkeit Daten sehr komfortabel zwischen, auch verschiedenen Clients, auf einem PC zu transferieren.

### Mehrfaches Starten am MAC OS X
Auch am MAC ist ein mehrfaches Starten des **Kieselstein ERP** Clients möglich.

Eine weitere Alternative ist den Client, welcher üblicherweise am Desktop/Schreibtisch liegt zu kopieren, z.B. auf **Kieselstein ERP** Client 2.

Klicken Sie dazu mit der rechten Maus, bzw. Strg+Maus auf den original **Kieselstein ERP** Client und wählen Sie Kieselstein-ERP_Client kopieren.

Klicken Sie dann auf eine leere Stelle des Desktops/Schreibtisches und klicken dann wieder mit der rechten Maustaste bzw. Strg+Maus und wählen Sie Objekt einsetzen. Damit haben Sie Ihren **Kieselstein ERP** Client ein weiteres mal kopiert. Bitte beachten Sie, dass bei Updates jeder Client aktualisiert werden muss.

### Kann **Kieselstein ERP** auch im WLan betrieben werden?
Ja dank der Dreischichttechnik ist dies problemlos möglich. Achten Sie jedoch darauf, dass eine gute Funkverbindung besteht. Für MAC Anwender, es sollten mindestens drei Striche in der Signalstärke angezeigt werden. Wenn die Funkverbindung dauernd abbricht ist die Qualität der Datenübertragung nicht gegeben und Sie werden auch mit **Kieselstein ERP** nicht zufriedenstellend arbeiten können. 

### Wie sollen Firewall und Antiviren-Programme am **Kieselstein ERP**-Server eingestellt werden?
Achten Sie bitte auf die Konfiguration der Firewall und der Antivirus-Programme.

Da für die Kommunikation zwischen Client und Server unterschiedliche Ports verwendet werden, müssen diese in der Firewalleinstellung freigeschalten werden. Kontaktieren Sie für die individuelle Einstellung der Ports bitte Ihren **Kieselstein ERP**-Betreuer. Wenn ein Antivirusprogramm verwendet wird, so ist es wichtig die Verzeichnisse des **Kieselstein ERP** Servers von Scans auszunehmen, da es hier zu erheblichen Geschwindigkeitseinschränkungen kommen kann. Im Idealfall setzen Sie die Scans zu Zeitpunkten fest, wo keine Mitarbeiter mit **Kieselstein ERP** arbeiten und deaktiveren die laufenden Scans. 

[Siehe dazu auch]( {{<relref "/docs/installation/08_nach_der_installation/tipps_und_tricks">}} ).

[Druckformulare]({{< relref "/docs/stammdaten/system/druckformulare" >}})

[Automatik Jobs]({{< relref "/docs/stammdaten/system/automatik" >}})

[Pflegefunktionen]({{< relref "/docs/stammdaten/system/pflegefunktionen" >}})

[Nachrichtensystem]({{< relref "/docs/stammdaten/system/nachrichtensystem" >}})

[Schnittstellen]( {{<relref "/docs/installation/30_schnittstellen">}} )

[Reiter deaktivieren]({{< relref "/docs/stammdaten/system/reiter_deaktivieren" >}})

## Bearbeiten der Systemeinstellungen

Im Modul Systemeinstellungen ![](System_Modul.gif) finden Sie alle Parameter um **Kieselstein ERP** an Ihre Bedürfnisse anzupassen.

In den unteren Modulreitern finden Sie folgende Basis-Funktionalitäten:

![](System_Reiter.gif) mit Ort, Land, Land-Plz-Ort und Einheit. Diese Daten sind Mandanten unabhängig.

![](Gesperrt_Reiter.gif) mit den aktuell gesperrten Datensätzen aller Mandanten.

![](Mandant_Reiter.gif) mit der Definition der Mandanten abhängigen Daten wie Mandantenauswahl, Kopfdaten, Konditionen, Spediteure, Zahlungsziele, Mehrwertsteuer, Kostenstelle und Lieferart.

![](Sprache_Reiter.gif) mit Belegart, Status, Sachbearbeiterfunktion, Positionsart. Dies sind die Basisdaten und deren Übersetzungen. Die Verwendung der Positionsarten ist dann weitergehend in den einzelnen Modulen definiert.

![](Medien_Reiter.gif) mit allgemeinen Medienmodulen, wie Texten oder Bildern und Mediaart 

![](Parameter_Reiter.gif) mit Mandanten und Anwenderparametern. Mit den Einstellparametern für den durch Ihre Anmeldung bzw. Mandantenauswahl gewählten Mandanten. [Einstellung der Parameter siehe](#Einstellung der Parameter).

![](Versandauftrag_Reiter.gif) 

Unter Versandauftrag sehe Sie alle Versandaufträge die über **Kieselstein ERP** abgewickelt wurden.
Im Postausgang finden Sie die noch offenen Versandaufträge.
Im Gesendet finden Sie die bereits versandten Belege.
Im Papierkorb werden die gelöschten Versandaufträge, egal ob diese aus dem Postausgang gelöscht wurden oder aus dem Gesendet eingefügt.<br>
Unter Fehlgeschlagen finden Sie Versandaufträge die nicht zugestellt werden konnten.
Hinweis: Stehen Versandaufträge zu lange im Postausgang, bzw. sind Versandaufträge fehlgeschlagen, so erhält jeder Anwender beim Start eines Modules den Hinweis Fehlgeschlagene Versandaufträge.<br>
Dies hat den Zweck, dass der Anwender auch ohne in den Versandaufträgen nachzusehen diese, oft relevante Information bekommt. In den Statuszeilen des jeweiligen Versandauftrages ist in der Regel auch die Fehlerursache ersichtlich.

#### Anlegen eines Spediteurs

Die Spediteure werden im Mandanten angelegt und sind daher auch Mandantenabhängig. Diese Bezeichnung der Spediteure ist nicht sprachabhängig, da hier eindeutige Firmennamen hinterlegt werden.

### Bearbeiten eines Mandanten
Wenn in Ihrer Installation mehrere Mandanten freigeschaltet sind, so ist zu beachten, dass immer nur derjenige Mandant bearbeitet / verändert werden kann, bei dem Sie aktuell angemeldet (eingeloggt) sind. Die Parameter eines anderen Mandanten können zwar betrachtet aber nicht verändert werden (nur Leserecht).<br>
Wenn Sie die Daten des Mandanten bei dem Sie gerade angemeldet sind bearbeiten, so ist für die Änderung des Kennzeichens Hauptmandant ein besonderes Benutzerrecht erforderlich, welches üblicherweise nur von Ihrem **Kieselstein ERP** Lieferanten eingestellt werden kann.

## Anlegen eines neuen Mandanten / Freischalten von zusätzlichen Modulen
Bei der Installation von **Kieselstein ERP** werden alle Mandanten und deren Module anhand der angegebenen Mandantenlizenzen eingerichtet. Solltest du nachträglich einen Mandaten hinzufügen wollen, so [siehe]( {{<relref "mandant">}} ). Gerne unterstützt dich dabei dein **Kieselstein ERP** Betreuer.

Die Mandantenparameter eines bestehenden Mandanten, wie Anschrift usw. können jederzeit im System unter ![](Mandant_Reiter.gif) geändert werden.

Beim Anlegen weiterer Mandanten werden automatisch alle Pflichtfelder vom Mandanten 001 kopiert. Bitte stellt nach der Freischaltung des neuen Mandanten sicher, dass alle Einstellungen für diesen neuen Mandanten richtig sind.

**Hinweis:**
Können nach dem Freischalten eines neuen Mandanten keine neuen Kunden oder keine neuen Lieferanten angelegt werden, so liegt das meist daran, dass bei der dem Mandanten zugeordneten Person eine Kostenstelle des Urmandanten hinterlegt ist. D.h. bitte prüfen / korrigieren Sie die Kostenstelle des am neuen Mandanten angemeldeten Mitarbeiters, in dem Sie im Personal auf die Person gehen, Detail, ändern. Nun Klicken Sie auf den Knopf Heimat KST. Werden hier die gewünschten Kostenstellen angezeigt, so ordnen Sie die richtige Kostenstelle dem Mitarbeiter zu. Werden keine Kostenstellen angezeigt, so gehen Sie bitte auf System, Mandant, Kostenstelle und legen Sie hier die gewünschten Kostenstellen an. Nun wechseln Sie wieder in das Personal des Mandanten und definieren Sie die Heimatkostenstelle des Mitarbeiters. Nun kann in der Regel auch ein neuer Kunde / Partner angelegt werden.

**Hinweis2:**
Ein ähnliches Verhalten tritt auch auf, wenn bei einem neu freigeschalteten Mandanten die Mehrwertsteuersätze nicht definiert sein. Prüfen Sie bitte gegebenenfalls auch diese Einstellungen unter System, Mandant, MwSt.

**Hinweis3:**
Bricht beim Anlegen des Mandanten **Kieselstein ERP** mit einer Fehlermeldung (id to load is required for loading) so prüfen Sie bitte ob am Mandanten 001, dem Urmandanten, alle Pflichtfelder, im Modul System, unterer Modulreiter Mandant, in allen oberen Reitern definiert sind. Nur wenn dies der Fall ist kann ein neuer Mandant, der ja die Voreinstellungen kopieren muss, angelegt werden.

#### Freischalten oder sperren eines Mandanten
Dies können Sie jederzeit über den Benutzermandanten unter Benutzer vornehmen. D.h. Sie entfernen die Benutzermandanten Zuordnung für diejenigen Benutzer, welche auf den Mandanten keinen Zugriff haben sollten. Achten Sie jedoch darauf, dass immer ein Benutzer, z.B. LPAdmin bleibt, der über Administratorrechte auf allen Mandanten verfügt.

#### Löschen eines Mandaten
Wie oben bereits ausgeführt ist das neu Anlegen eines Mandanten für den Anwender nicht vorgesehen. Aus genau dem gleichen Grunde ist auch das Löschen eines Mandanten nicht möglich. Anstelle des Löschen entfernen Sie bitte einfach die Mandantenberechtigung des Benutzers für den gewünschten Mandanten.

#### Definieren der Standard Bankverbindung dieses Mandanten
Dazu muss zuerst die eigene Bankverbindung definiert sein. Siehe dazu bitte [Finanzbuchhaltung]({{< relref "/management/finanzbuchhaltung#Definition der eigenen Bankverbindungen" >}})

Erst nachdem die eigenen Bankverbindungen definiert sind, kann diese Zuordnung als Vorschlagswert für die Zahlungsbuchungen vorgenommen werden.

#### Optische Kennzeichnung bei mehreren Mandanten
Werden mehrere Mandanten eingesetzt, so ist es unter Umständen für die Anwender von Vorteil, wenn neben der Trennung in der Titelzeile Ihres **Kieselstein ERP** Clients eine zusätzliche optische Trennung hinterlegt wird.
Wir haben dafür die Möglichkeit geschaffen, in der Statuszeile (unten) des Hauptfensters ein Bild zu hinterlegen.
Dieses Bild sollte 320x16 Pixel hoch und im PNG Format sein. Es kann unter System, Mandant, Vorbelegung 2 definiert werden.

### Definition der Mandantenwährung
<a name="Mandantenwährung"></a>
Im System, unterer Modulreiter Mandant, oberer Modulreiter Kopfdaten, kann rechts unten für den ausgewählten Mandanten die Währung entsprechend eingestellt werden. Achten Sie darauf, dass diese Währung von Anbeginn an für Sie richtig eingestellt ist. Wird nachträglich die Mandantenwährung verändert, hat das dramatische Auswirkungen, da damit keine wie immer geartete Umrechnung / Neuberechnung verbunden ist.

Wie kann ich einen Datensatz, der von einem anderen Benutzer gesperrt wurde wieder entsperren.

In den Systemeinstellungen finden Sie den unteren Modulreiter ![](Gesperrt_Reiter.gif).

Hier sind alle aktuellen Sperren aufgelistet.

![](Sperren.gif) 

Zum Sperren und Entsperren ist folgendes zu beachten:

Bei jeder Anmeldung erhält ein Benutzer eine neue UserId. Diese ist **Kieselstein ERP** weit einmalig. Meldet sich ein Benutzer erneut an, so erhält er eine neue UserId. Er kann also seine alten Sperren trotzdem nicht mehr verändern. Dies hat den Zweck, um sich vom gleichen PC mit dem gleichen Client und dem gleichen Benutzer mehrfach anmelden zu können.

Wenn Sie Datensätze entsperren, so beachten Sie bitte, dies nur wirklich aufgrund eines dringenden Bedarfes durchzuführen, wenn Sie also nicht mehr weiterarbeiten können. Anderenfalls könnte es sein, dass Sie einem anderen Benutzer, während des Arbeitens die notwendige Sperre wegnehmen und dadurch seine Eingabe vernichten, wodurch unter Umständen eine stundenlange Arbeit vernichtet werden könnte.

Siehe dazu bitte auch "[Ändern eines Datensatzes](../Allgemein/index.htm#Ändern eines Datensatzes)".

Bitte beachten Sie dazu auch folgendes:

Wenn Sie **Kieselstein ERP** beenden möchten, sich aber noch im Ändern von Daten befinden, diese also noch nicht abgespeichert sind, so sind die Daten gesperrt. Durch das direkte Beenden von **Kieselstein ERP** werden diese Daten nicht freigegeben. Bei einer Neuanmeldung erhalten Sie eine neue UserID, eine neue Sitzungsnummer und können somit diesen Datensatz nicht mehr bearbeiten, außer sie gehen wie oben beschrieben vor. Aus diesem Grunde sollten Sie immer alle **Kieselstein ERP** Module beenden, bevor Sie den gesamten **Kieselstein ERP** Client schließen. Sie können dieses Verhalten aber auch dazu verwenden, einen Datensatz, z.B. den Datensatz eines Kunden, gezielt zu sperren, zu blockieren, damit keine unbeabsichtigte Änderung vorgenommen wird, z.B. um die Veränderung der Kundendaten während Ihres Urlaubs zu verhindern.

#### Anlegen von Orten:
Bitte beachten Sie, dass je Ortsnamen nur ein Ort angelegt werden kann. Eine Besonderheit ist das Verhalten von "ss" und "ß". Dies wird von den üblicherweise verwendeten Datenbanken als gleichwertige Zeichen interpretiert. Deshalb kann ein Ort nur in einer der beiden Schreibweisen angelegt werden. Beispiel: Straßwalchen. Ist dieser Ort bereits angelegt, so erhalten Sie beim Abspeichern von Strasswalchen den Hinweis:<br>
![](FDU.gif)<br>
Was soviel bedeutet, wie dass dieser Ortsnamen bereits angelegt wurde, aber eventuell eben in der anderen Schreibweise.

#### Anlegen von Ländern:
<a name="Anlegen von Ländern"></a>
Unter Land können die Länder mit Länderkennzeichen, Ländernamen, Internationaler Vorwahl, Standardwährung und dem Zeitpunkt des EU-Beitrittes angelegt werden. Bitte beachten Sie, dass das Länderkennzeichen nur maximal dreistellig eingegeben werden kann.
-   Für das Länderkennzeichen gibt es derzeit drei verschiedene Definitionen:
    -   a.) Internationale Unterscheidungskennzeichen für Kraftfahrzeuge. Diese Länderkennzeichen sollten laut Empfehlung der Deutschen Post AG wegen der unterschiedlichen Längen NICHT verwendet werden.
    -   b.) Zwei Buchstaben Code laut DIN EN ISO 3166-1
    -   c.) Drei Buchstaben Code laut DIN EN ISO 3166-1
-   EU-Mitglied von bis. Dieses Datum dient der Steuerung der steuerlichen Behandlung der Eingangs- / Ausgangsrechnungen bzw. der Gutschriften. Ist hier ein Datum angegeben, so werden Belege mit Belegdatum ab diesem Datum und deren Partneradressen (Kunden/Lieferanten) in diesem Land sind, ab diesem Datum als EU-Mitglied behandelt (IG-Erwerb, IG-Erlös). Bitte beachten Sie hier, dass dieses Datum bilaterale Wirkung hat. D.h. selbstverständlich muss auch das Land Ihres Mandanten Mitglied der EU sein.
    So ist. z.B. Österreich seit 1.1.1995 Mitglied der Europäischen Union. 
    Estland, Lettland, Litauen, Malta, Polen, Slowakei, Slowenien, Tschechien, Ungarn, Zypern sind mit 1\. Mai 2004 hinzugekommen, Bulgarien und Rumänien 1.Januar 2007, Kroatien mit 1.Juli 2013.

Für eine sehr übersichtliche Liste der Länderkennzeichen siehe auch [Wikipedia](http://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste).

Bei der Eingabe der Ländernamen beachten Sie bitte, dass die Versendungsstelle, in der Regel Ihr Postamt, die Ländernamen lesen können muss. Also wenn Sie z.B. in Österreich ansässig sind, dann für die USA, Vereinigte Staaten von Amerika und nicht united states of america.

Für eine Liste der aktuell bekannten Länder, Ländernamen, Währungen, Zeitzonen usw. [siehe](Laender_Waehrungen_Zeitzonen.pdf).

Für den Zahlungsvorschlag ist auch die Definition der Sepa-Teilnehmer wichtig.

EU Mitgliedsländer

| LKZ | Land | EU-Mitglied ab | Währung |
| --- |  --- |  --- |  --- |
| BE | Belgien | 1.1.1957 |  |
| BG | Bulgarien | 1.1.2007 |  |
| DK | Dänemark | 1.1.1973 |  |
| DE | Deutschland | 1.1.1957 (Westdeutschland) | EUR |
| EE | Estland | 1.5.2004 |  |
| FI | Finnland | 1.1.1995 |  |
| FR | Frankreich | 1.1.1957 | EUR |
| GR | Griechenland | 1.1.1981 | EUR |
| IE | Irland | 1.1.1973 |  |
| IT | Italien | 1.1.1957 | EUR |
| HR | Kroatien | 1.7.2013 | EUR seit 1.1.2023 |
| LV | Lettland | 1.5.2004 |  |
| LT | Litauen | 1.5.2004 |  |
| LU | Luxemburg | 1.1.1957 |  |
| MT | Malta | 1.5.2004 |  |
| NL | Niederlande | 1.1.1957 |  |
| AT | Österreich | 1.1.1995 | EUR |
| PL | Polen | 1.5.2004 |  |
| PT | Portugal | 1.5.2004 |  |
| CY | Republik Zypern | 1.5.2004 (Europäischer Teil) |  |
| RO | Rumänien | 1.1.2007 |  |
| SE | Schweden | 1.1.1995 |  |
| SK | Slowakei | 1.5.2004 |  |
| SI | Slowenien | 1.5.2004 | EUR seit 1.1.2009 |
| ES | Spanien | 1.1.1986 |  |
| CZ | Tschechien | 1.5.2004 |  |
| HU | Ungarn | 1.5.2004 |  |
| GB | Vereinigtes Königreich | 1.1.1973 - 31.1.2020 |  |

#### BREXIT, EU-Mitglied bis
<a name="Brexit"></a>
Nachdem uns leider Großbritannien verlassen hat, gibt es auch das Feld EU-Mitglied bis
![](EU-Mitglied_bis.gif)<br>
in den Länderdefinitionen. D.h. bitte für den Austritt aus der EU das tatsächlich gefundene Datum eintragen.<br>
Der Austritt bewirkt, dass Rechnungen mit einem Belegdatum nach dem EU-Mitglied bis, wiederum als Drittlandsrechnungen behandelt werden.
Für Anwender mit integriertem Finanzbuchhaltungsmodul [siehe bitte]( {{<relref "/management/finanzbuchhaltung/zusatzinfos/#a-namebrexitabrexit" >}} )

#### Präferenzbegünstigt
Insbesondere unsere Schweizer, Liechtensteiner Anwender, kämpfen mit den Ursprungsbestimmungen, welche im Endeffekt vor allem die Importzölle der Empfänger regeln.
Dafür stellen wir nun die Definitionsmöglichkeit des Präferenzbegünstigten Warenverkehrs zur Verfügung.<br>
Dafür muss einerseits das jeweilige Land als Präferenzbegünstigt definiert werden. Siehe dazu System, System, Land.<br>
Haken Sie hier Präferenzbegünstigt an.<br>
Weiters ist hier wichtig, dass die gelieferten Artikel die jeweiligen Ursprungsländer hinterlegt haben.<br>
Ist nun das Lieferland, also die Lieferadresse, Präferenzbegünstigt und ist das Ursprungsland des Artikels auch Präferenzbegünstigt, so wird bei den Verkaufsbelegen das Präferenzbegünstigt mit übergeben. Ist beim Artikel kein Ursprungsland hinterlegt, so ist automatisch dieser Artikel NICHT Präferenzbegünstigt. Ist z.B. bei einem Angebot bei der Lieferadresse ein Kunde ohne Länderdefinition hinterlegt sind die Angebotspositionen ebenfalls nicht Präferenzbegünstigt.

#### SEPA Ländern
Alle EU Mitgliedsländer, sowie Schweiz und Liechtenstein sind Mitglied im SEPA (Singel Euro Payments Area)

#### Gemeinsames **Postland**
Manche Länder, wie z.B. Liechtenstein haben eine gemeinsame Postorganisation. D.h. wenn eine Versendung in dieses Land erfolgt, so darf das Empfängerland nicht angegeben werden.
Geben Sie in diesem Falle bei dem Land Ihres Stammsitzes das Gemeinsame Postland an.
![](Gemeinsames_Postland.gif)

#### Definition des Rundungswertes
<a name="Rundung"></a>
In manchen Ländern werden bei den Münzen nur größere Einheiten als 1 (0,01) verwendet. Das bedeutet dass im Endkundengeschäft (B2C) die Rechnungen auf diese Beträge gerundet werden müssen. Definieren Sie dies bei den entsprechenden Ländern mit dem Rundungswert.
![](Rundungswert.gif)
Dieser kann zwischen 0,05 bzw. 0,10 eingestellt werden. Die Rundung greift auf den Bruttowert der Rechnung des Landes, wobei egal ist in welcher Währung diese Rechnung erstellt wurde. [Siehe dazu auch]( {{<relref "/verkauf/gemeinsamkeiten/#runden-des-bruttobetrages" >}} ).
Wenn Rundung verwendet wird, so muss auch der in den Parametern unter RUNDUNGSAUSGLEICH_ARTIKEL festgelegte Artikel definiert werden. Dieser ist üblicherweise der Artikel RUNDUNG. Dieser Artikel wird auch dazu benötigt den Rundungs-Erlös auf das jeweilige FiBu Erlöskonto zu buchen. [Siehe dazu bitte]( {{<relref "/management/finanzbuchhaltung/integriert/#wie-ist-das-rappen-rundungs-konto-zu-definieren" >}} )

#### **Anlegen der Zuordnung Land - Postleitzahl - Ort:**
Unter Land-Plz-Ort kann diese Zuordnung erfasst werden. Als Besonderheit kann hier durch die Eingabe des Ortsnamen die Zuordnung zu den Ortsnamen geschaffen werden. D.h. wird ein Ortsname eingegeben und ist dieser noch nicht erfasst, so wird er angelegt, ansonsten wird der Ortsname entsprechend zugeordnet.<br>
Hinweis:<br>
Die Sortierung der Land-Plz-Ort Liste ist bewusst so gewählt, dass diese nach Orten und innerhalb der Orte nach Postleitzahl ohne Berücksichtigung des Länderkennzeichens ist.

#### Wie vorgehen wenn Länder bzw. Orte doppelt angelegt sind?
Wenn neue Postleitzahlen mit deren Orten angelegt werden kann es, auch aufgrund von Tippfehlern oder ähnlichem passieren dass Orte und damit LKZ, PLZ, Ort Kombinationen (Länderkennzeichen, Postleitzahl, Ort) doppelt angelegt werden. Wie z.B.:
![](FalscherOrt1.gif)<br>
Für die Korrektur geht man nun am Besten wie folgt vor:

1.  Korrigieren Sie die Partneradressen. Wechseln Sie dazu in das Modul Partner ![](FalscherOrt2.jpg) und geben Sie im Direktfilter Ort den falschen Ortsnamen ein ![](FalscherOrt3.gif)

2.  Wechseln Sie in den Reiter 2 Detail und korrigieren Sie den Ort auf den richtigen Ortsnamen, genauer die richtige LKZ, PLZ, Ort Zuordnung ![](FalscherOrt4.gif)

3.  Wechseln Sie nun im System in den Reiter 3 Land-PLZ-Ort und wählen ebenfalls den falschen Ortsnamen
     ![](FalscherOrt5.jpg)
    Löschen Sie diesen.

4.  Nun wechseln Sie ebenfalls im Modul System, unterer Reiter System auf 1 Ort wählen ebenfalls den falschen Ort
    ![](FalscherOrt6.jpg)
    und klicken auf Löschen.

#### Ändern der Kopf- und Fußtexte:
In **Kieselstein ERP** gibt es mehrere Möglichkeiten der Gestaltung der sogenannten Kopf- und Fußtexte.<br>
Die hinterlegte Logik ist grundsätzlich folgendermaßen:<br>
Für jedes Bewegungsmodul wird ein vordefinierter Text verwendet, außer er wurde für einen bestimmten Beleg übersteuert.
D.h. es gibt je Bewegungsmodul eine allgemeine Definition und eine individuelle Definition, welche die allgemeine Definition für diesen einen Beleg übersteuert.
Zur Definition der Texte je Bewegungsmodul gehen Sie bitte wie folgt vor:
Für jedes Bewegungsmodul (Angebot - Bestellung) gibt es einen unteren Modulreiter Grunddaten. Hier gibt es wiederum einen Reiter ... Text (z.B. Angebotstext, Rechnungstext). Sie finden hier zumindest die Typen Fußtext und Kopftext. Der Inhalt dieser Text-Typen wird als default / Vorschlagswert in das jeweilige Bewegungsmodul übernommen.
Die Übersteuerung der allgemeinen Kopf- und Fußtexte wird in den jeweiligen Konditionen des Moduls vorgenommen.

**Festlegen des Geschäftsjahreswechsels:**

Unter Parameter kann der Beginn eines Geschäftsjahres definiert werden. Dies wirkt im wesentlichen für die Generierung der Belegnummern. Zusätzlich wird in den Tabellen das ermittelte Geschäftsjahr gespeichert um dies für die jeweiligen Auswertungen zur Verfügung zu haben.
Die Definition teilt sich in zwei Parameter:
![](GF_Wechsel.gif)
Geschäftsjahrbeginnmonat: Tragen Sie hier ein, ab welchem Kalendermonat Ihr Geschäftsjahr beginnt.
Geschäftsjahrpluseins: Wenn die Jahreskennzahlen der Belegnummern mit der Jahreszahl des Geschäftsjahresbeginns korrespondieren sollen, so tragen Sie hier Null ein. Sollte die Jahreskennzahl der Belegnummern z.B. mit dem Ende des Geschäftsjahres korrespondieren, so tragen Sie hier Eins ein.
Wird z.B. für Geschäftsjahresbeginnmonat 7 eingetragen und das Geschäftsjahrpluseins auf Null belassen so bewirkt dies, dass die Belegnummern bis zum 30.6.2006 mit 05/... beginnen (da das beginnende Kalenderjahr des GF Jahres in 05 liegt) und mit 1.7.2006 mit 06/00000001 begonnen wird.

**Durchlaufende Belegnummern**

Haben Sie einen unterjährigen Geschäftsjahreswechsel, so sollten die Belegnummern auch nach einem Wechsel des Kalenderjahres weiterzählen. Für diese Einstellung siehe bitte oben: Festlegen des Geschäftsjahreswechsels.

Definition des Formates der Belegnummern

In den Mandantenparametern kann auch das Belegnummernformat definiert werden. Wir haben vorgesehen, dass Sie die Anzahl der Jahresstellen und die Länge der unterjährigen laufenden Nummer definieren können. Weiters kann das Trennzeichen definiert werden.

![](Format_Belegnummern.gif)

Bitte beachten Sie, dass eine Änderung dieser Parameter sofort wirksam ist. Eventuell bisher erzeugte Belegnummern werden dadurch jedoch **NICHT** verändert. Sollten auch bereits bestehende Belegnummern verändert werden müssen, so wenden Sie sich bitte direkt an uns.

Bei einer Umstellung bedenken Sie bitte, dass die Sortierung der Auswahllisten nach dieser Belegnummer erfolgt. So ist insbesondere bei der Übernahmen von Daten vor 2000 zu empfehlen die Sortierung auf vier Jahresstellen zu definieren.

### Versionen / Voraussetzungen
<a name="Versionen/Voraussetzungen"></a>
**Kieselstein ERP** ist vollständig in der Programmiersprache Java geschrieben. Voraussetzung für den Einsatz ist die Verfügbarkeit der richtigen Java Runtime. Details siehe bitte [Installation]({{< relref "/docs/installation/02_client" >}})

### Einstellung der Parameter
<a name="Einstellung der Parameter"></a>
Parameter werden im Modul System, ![](System.jpg), unterer Modulreiter Parameter eingestellt.

Üblicherweise finden Sie den Parameter am schnellsten über seinen exakten Parameternamen, den Sie im rechten Direktfilter eingeben. Zusätzlich können Sie auch den Kategorien Direktfilter benutzen und so die Auswahl der zur angezeigten Parameter entsprechend einschränken.
Beachten Sie bitte die Trennung zwischen Parametern, diese sind für den jeweiligen Mandanten und Anwenderparameter. Diese gelten global für die gesamte Installation.

[Eine Liste der Parameter und deren Bedeutung finden Sie hier.]({{< relref "/docs/stammdaten/system/parameter" >}})

#### Bedeutung der Parameter Datentypen
Für die Parametrierung Ihres **Kieselstein ERP** Mandanten kann im System unter Parameter das Verhalten von **Kieselstein ERP** entsprechend angepasst werden. Die Parameter selbst sind beschrieben und sind somit selbsterklärend. Die Parameter haben unterschiedliche Datentypen. Die Eingabe dafür ist wie folgt definiert:

| Datentyp | Bedeutung / mögliche Eingabe |
| --- |  --- |
| java.lang.String | Eingabe einer Zeichenkette |
| java.lang.Integer | Eingabe einer ganzen Zahl |
| java.lang.Boolean | Eingabe eines logischen Wertes<br>0 ... falsch<br>1 ... wahr |

#### Direkter Zugriff auf die **Kieselstein ERP** Datenbank
Es kann praktisch sein, direkt auf die **Kieselstein ERP** Datenbank zuzugreifen. Das Passwort ist in der Installationsdokumentation der Datenbank definiert. Siehe bitte dort.

### Erstellung eigener Auswertungen
In **Kieselstein ERP** können unter System, Extralisten eigene Auswertungen erstellt werden.
Aufgrund des Drei-Schichten-Modells und der Datenbank-Unabhängigkeit müssen diese im sogenannten HQL Syntax erstellt werden. Diese Schreibweise ist ähnlich dem SQL Syntax.
Diese Extralisten können den Modulen zugewiesen werden und können, nach dem Neustart des Moduls in der Menüleiste über Extras, NameDerExtraliste aufgerufen werden. Kommt es nun bei der Ausführung einer dieser Extralisten zu eine Fehlermeldung ähnlich der nachfolgenden,<br>
![](HQL-Error.jpg)<br>
so bedeutet dies, dass die Extraliste durch den Hibernate nicht interpretiert werden konnte. In vielen Fällen, so auch in dem hier dargestellten, wurde ein SQL Syntax eingegeben. Korrigieren Sie den Syntax auf HQL. Bei Fragen kontaktieren Sie bitte Ihren **Kieselstein ERP** Händler oder wenden Sie sich direkt an uns <(support@kieselstein-erp.org>). Wir unterstützen Sie gerne.

Das Ergebnis einer Extraliste(n-Auswertung) wird am Schirm dargestellt und kann zusätzlich als CSV-Datei exportiert werden.

#### Wo können die Vorbelegungen für neue Kunden, Lieferanten eingestellt werden?
Im System, Mandant, werden in den Reitern Vorbelegung bzw. Vorbelegung2 die Standard-Werte für neue Kunden bzw. Lieferanten usw. definiert.

### keine Preisliste vorbesetzen
Hier finden Sie auch ![](Vorbelegung_Preisliste.gif) die Definition welche der Verkaufs-Preislisten für einen neuen Kunden verwendet werden sollte. Diese Preislistendefinition greift auch bei allen anderen Funktionen die auf eine Preisliste referenzieren. D.h. wurde für diese keine Preislistendefinition gefunden, wird auf die hier definierte Preisliste zurückgegriffen.
Wenn nun die gewünschte Preisliste bei Neukunden nicht verwendet werden sollte, bitte den Haken entfernen. Das bedeutet, dass nur bei der Neukundenanlage die Preisliste gewählt werden muss.

### Positionsarten anpassen
Im Modul System, unterer Modulreiter Sprache, oberer Modulreiter Positionsart kann die Bezeichnung der jeweiligen Positionsart entsprechend angepasst werden.

Folgende alternative Bezeichnungen haben sich bewährt:
| Positionsart | Bezeichnung |
| --- | --- |
| Ident | Artikel in der Elektronik gegebenenfalls auch Bauteil |
| IZwischensumme | Intelligente Zwischensumme |
