---
categories: ["Status Icons"]
tags: ["Status Icons"]
title: "Status Icons"
linkTitle: "Icons"
date: 2023-01-31
weight: 600
description: >
  Eigene Status Icons hinterlegen
---
In deinem **Kieselstein ERP** kannst du teilweise auch eigene Status definieren. Grundsätzlich haben wir dafür https://www.iconfinder.com/search?price=free&license=gte__1 verwendet.
Einige davon findest du auch unter<br>
Weitere
in produktion_zeiteinteilung.png ... <a href="https://www.flaticon.com/de/kostenlose-icons/produktivitat" title="produktivität Icons">Produktivität Icons erstellt von srip - Flaticon</a>

Nachdem es aber doch viele weitere auch selbst definierbare Status gibt (im Projekt, Sperren im Artikel), die man, insbesondere in der Projektverwaltung abbilden möchte, haben wir den Link angeführt.<br>
Grundsätzlich kann jedes **PNG** im Format 16x16 Pixel als Status-Symbol hinterlegt werden.

Um ein Icon-Bild, das im PNG Format sein muss, zu skalieren haben wir gimp verwendet. Damit auf die nötigen 16 x 16 Pixel gebracht und der transparente Hintergrund gesetzt
- Gimp, Bild, skalieren
- Datei, ... überschreiben und dabei den transparenten Hintergrund anhaken bzw.
- Datei, Exportieren, Dateiname mit Erweiterung .png und dann Hintergrundfarbe speichern abhaken


## Export der aktuell gespeicherten Status Icons
lp_status, Backup, Format: Plain<br>
![](Backup1.png)  

Im Reiter Data / Objects, Data ein, Blob aus<br>
![](Backup2.png)  



Daran denken, nach dem Einspielen der Statusicons den Client neu starten

## Verwendung anderer Icons
Es hat sich in der Zwischenzeit herausgestellt, dass manche Icons besser für die Darstellung geeignet sind. Daher nachfolgend eine Liste von Icons, welche du möglicherweise verwenden möchtest.
| Status | Icon | Modulverwendung |
| --- | --- | --- |
| In Produktion | ![](753869_tools_working_gear_options_preferences_icon.png) | Los |
| Freigegeben | ![](7067457_accept_approved_stamp_icon.png) | Forecast, Aufträge |
| Erfasst | ![](erfasst.png) | Anfrage |

Gerne ergänzen wir diese Liste, bitte um entsprechende Vorschläge.

## Icon Format
Wenn du selber neue Status Icons einspielst, so achte darauf, dass diese im Format **png** sind.

Wenn, auch nach dem kompletten Neustart deines Kieselstein ERP Clients das Icon in der Auswahlliste nicht angezeigt wird, aber sehr wohl in der Detail Ansicht, <br>
![](Status_wird_nicht_angezeigt.png)

so hast du als Format kein PNG verwendet. Bitte das richtige Format verwenden.
