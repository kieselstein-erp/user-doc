---
categories: ["Automatik"]
tags: ["Automatik-Jobs"]
title: "Automatik-Jobs"
linkTitle: "Automatik-Jobs"
date: 2023-01-31
weight: 070
description: >
  Nachtjobs, automatische Jobs
---
Automatik
=========

In einigen Fällen ist es erwünscht, dass täglich, wöchentlich verschiedene Läufe automatisch, hier vor allem während der Nacht, ausgeführt werden. Wir haben diese auch Nachtjob genannten Aufgaben unter System im Reiter Automatik zusammengefasst.
![](Automatik1.jpg)

Grundsätzlich ist diese Darstellung / Anzeige so aufgebaut, dass die Automatik Aufträge in der angeführten Reihenfolge ausgeführt werden. Die Reihenfolge können Sie durch Umreihung mit den Pfeilen ![](Automatik2.gif) vornehmen.
Die automatischen Aufträge starten zum unten angegebenen Zeitpunkt (Startzeit).
Beachten Sie bitte dass diese Zeit den Startzeitpunkt der ersten Aufgabe bestimmt. Ist die erste aktive Aufgabe erledigt, wird mit der zweiten Aufgabe, die als aktiv gekennzeichnet ist, fortgefahren. Damit die Automatik überhaupt ausgeführt wird, muss in der Übersicht im unteren Detailfenster Aktiv angehakt sein.
Unter Detail können die für diesen Lauf spezifischen Parameter eingestellt werden. Unter anderem kann gegebenenfalls hier auch der gewünschte Drucker, auf den z.B. die Liste der Fehlmengen ausgedruckt werden sollte, angegeben werden.

Unter Detail können folgende Werte / Parameter eingestellt werden:
![](Automatik3.gif)

| Feld | Bedeutung |
| --- |  --- |
| Aktiv  | dieser Lauf soll ausgeführt werden |
| Intervall in Tagen | Der Lauf wird nach der Durchführung wieder in "Intervall in Tagen" durchgeführt. |
| Vor Feiertagen ausführen | Ist so gedacht, wenn der Folgetag ein Feiertag ist oder auf ein Wochenende (So, So) fällt, so wird, wenn nicht angehakt der Lauf nicht ausgeführt. Sonst schon. Damit Läufe auch für z.B. den Freitag ausgeführt werden, stellen Sie bitte den Startzeitpunkt auf kurz vor Mitternacht. D.h. wenn der Automatikjob erst nach 12:00 am Freitag starten würde, wird er als bereits auf den "Feiertag" fallend betrachtet und daher nicht ausgeführt. |
| Am Monatsende ausführen | Der Job wird zusätzlich am letzten Tag des Monats ausgeführt. Z.B. wenn er aufgrund der Intervalleinstellung noch nicht ausgeführt werden sollte.D.h. sollte ein Job immer nur am Monatsletzten ausgeführt werden, so ist bei Intervall in Tagen 32 einzustellen. |
| Drucker  | Wählen Sie einen Drucker aus den am Server installierten Drucker aus. Zur Installation der Server-Drucker unter [Linux Systemen]({{< relref "/docs/installation/12_drucker" >}})|
| Intervall in Minuten | Der Lauf wird alle xx Minuten durchgeführt  |

Bitte beachten Sie, dass für den generellen Start des Automatik Laufes in jedem Falle die unter Startzeit angegebene Uhrzeit überschritten werden muss. D.h. wenn Sie den Automatik Job z.B. für Testzwecke sofort starten möchten, so stellen Sie die Startzeit auf jetzt plus eine Minute. Damit beginnen die aktivierten Automatikjobs zu Laufen.
Dies gilt für beide Bereiche, sowohl die Intervalle in Minuten als auch denen in Tagen.

Wie werden Automatikjobs vor dem Wochenende ausgeführt?

Dies hängt vom Startzeitpunkt des Automatiklaufes ab. 
-   Ist der Startzeitpunkt vor 12:00 (Mittag), so wird der Automatiklauf auch am Freitag durchgeführt. Am Sonntag erfolgt kein Lauf des Automatikjobs.
-   Ist der Startzeitpunkt nach 12:00 (Mittag), so wird der Automatiklauf am Freitag nicht durchgeführt. Dafür wird der Automatiklauf am Sonntag durchgeführt, damit am Montag Morgen die Bestellmahnungen bei ihren Lieferanten sind.

Testlauf der Automatikjobs

Stellen Sie bitte die Startzeit auf in einer Minute um die Läufe in der angegebenen Reihenfolge zu starten. Denken Sie daran, die Startzeit danach wieder zurückzustellen.

Protokoll der Durchführung der Automatik Aufträge

finden Sie unter ..\kieselstein_wildfly\standalone\log\server.log direkt in ihrem **Kieselstein ERP** Serververzeichnis.
In diesem allgemeinen Server-Log-File sind auch die Einträge der Durchführung der Automatik-Jobs.
Sollten Sie gezielt nach Ergebnissen suchen, so suchen Sie nach "automatikjob.Automatikjob"

Einstellung des PDF Drucks der Auslieferliste

![](Automatik4.gif)

Hier bedeuten die zusätzlichen Felder:

| Bezeichnung  | Beschreibung  |
| --- |  --- |
| PDF-Pfad Pattern  | {0,date,w} ... 0 = gib mir ein Datumsobjekt welches nach dem Datumsformat (w) ausgegeben wird. Das Date besagt, dass ich vom System ein Datum will.<br>{1} ... Bezeichnung der Reportvariante (Standard, ....)<br>Somit bedeutet obige Einstellung, dass ausgehend vom Root-Verzeichnis, in das Verzeichnis pdf je Kalenderwoche ein Ordner angelegt wird. In den jeweiligen Wochenordner werden alle Report-Varianten des Drucks Auslieferliste eingepflegt. Dafür wird das aktuelle Datum mit TagMonatJahr und dem Namen des Reports angelegt. Z.B.: 31012023_Standard.pdf  |
| Tage bis Stichtag  | D.h. für das Bisdatum der Auslieferliste wird vom Berechnungszeitpunkt die angegebenen Tage dazugezählt.  |
| Archivierungstage  | Die Dateien werden soviele Tage ab Erzeugung aufbewahrt. Die Daten danach werden gelöscht. Es wird nach dem Pfad-Pattern wie oben angegeben vorgegangen. Das bedeutet auch, wird dieses Pattern geändert, müssen die "alten" Dateien manuell entfernt werden.  |

**Hinweise:** Beachten Sie am Linux die Groß/klein-Schreibung. Bitte beachten Sie auch die unterschiedliche Schreibweise für den Ordner (\ für Windows, / für Linux)<br>
Die Verzeichnisstruktur wird gegebenenfalls angelegt.<br>
Beachten Sie, dass der Benutzer unter dem der **Kieselstein ERP** Server gestartet ist auf diesen Pfad schreibenden Zugriff haben muss. Wir empfehlen dies am **Kieselstein ERP** Server in einem eigenen Verzeichnis abzulegen. Für den Zugriff des (**Kieselstein ERP**-) Dienstes muss das Laufwerk immer für den Server verfügbar sein. Wenn dies auf einem anderen Netzlaufwerk abgelegt werden sollte, muss in Linux Systemen das Laufwerk / der Pfad gemounted (mnt) werden.
In Windows-Systemen muss gegebenenfalls der Dienst unter einem Benutzernamen ausgeführt werden.

### Automatik Job startet generell nicht
Details dazu findest du im server.log 
[siehe]( {{<relref "/docs/installation/19_tipps_und_tricks/#email-versand-geht-nicht" >}} ).

#### Informationen dass Automatik-Jobs nicht gegangen ist.
Diese Informationen werden per EMail an den Benutzer lpwebappzemecs gesandt. D.h. dem Benutzer lpwebappzemecs ist eine (versteckte) Person zugeordnet. Ist bei dieser Person unter Daten, Absenderdaten eine EMailadresse eingetragen, so wird ein **Kieselstein ERP** Versandauftrag mit diesem Empfänger (und Absender) angelegt und über die üblichen Techniken versandt.

#### Beim Automatiklauf werden einige / viele EMails an den einem bestimmten Benutzer / an den Administrator gesandt
Dies sind Fehlerinformationen, dass z.B. beim Mahnlauf der Bestell-ABs bei einigen Empfängern, Ansprechpartner der jeweiligen Bestellung, keine EMail Adressen hinterlegt sind. Hinterlegen Sie bitte bei den entsprechenden Adressaten die EMail Adressen, damit diese auch tatsächlich die Mahnungen erhalten können.

##### Finden der Ursache von fehlgeschlagenen Automatik-Jobs.
Jeder Job funktioniert grundsätzlich gleich wie seine manuelle Entsprechung in den einzelnen Modulen. Konnte nun ein Automatik Job nicht durchgeführt werden, so kann die Ursache dafür dadurch herausgefunden werden, dass der Lauf / die Auswertung mit den gleichen Einstellungen wie im Automatik Job manuell gestartet wird.

**Beispiel:**

Die AB-Mahnungen der Bestellungen gehen nicht.

Sie gehen in das Bestellungsmodul, Mahnwesen und starten einen neuen Mahnlauf mit der Einstellung AB-Mahnungen. Nun erscheint die entsprechende Fehlermeldung. Beachten Sie diese und stellen Sie die Ursache ab, dann wird auch der Mahnlauf im Automatik Job funktionieren.

#### Die Automatik startet nicht
Startet ein / kein Automatik-Job so kann das daran liegen, dass diese nicht in der internen Verwaltung eingetragen sind, z.B. aus einem unkontrollierten Abbruch mit erneutem Start des **Kieselstein ERP** Applikationsservers.

Um dies nun erneut zu triggern, gehen Sie in System, Automatik. Klicken Sie in der Übersicht auf Ändern und daran anschließend auf speichern. Durch diese "Änderung" werden die Daten neu erzeugt und somit der Automatik Job gestartet. Bitte beachten Sie auch obige Beschreibung, des möglicherweise nicht Durchführens von automatischen Läufen.

#### Die Automatik bringt keine Daten
Da die Mahnautomatik der Bestellungen keine Mahnungen erzeugte, wurde der Mahnlauf per Hand gestartet und die Mahnungen verschickt. Nun wurde am nächsten Tag geprüft ob Mahnungen erzeugt wurden. Es wurde zwar der Mahnlauf durchgeführt, aber keine Daten erzeugt.

Ursache:

Der Start der Automatik stand auf 21:00\. Das bedeutet, dass durch den manuellen Mahnlauf alle Bestellungen für heute bereits gemahnt wurden und daher im nächtlichen Automatiklauf keine Positionen mehr zu mahnen sind. Stellen Sie daher sicher, dass die Zeitliche- und die Tages-Komponente übereinstimmen. In diesem Beispiel muss der Start des Automatiklaufes auf 01:00 verlegt werden. Damit erhalten Sie für den Mahnlauf ein anderes Datum und damit können die für den "heutigen" Tag fälligen Mahnungen ausgedruckt werden.

#### Automatische PDF Erzeugung für das Arbeitszeitjournal
Beim Aufruf des Automatikjobs für das Arbeitszeitjournal wird nur der Standardreport herangezogen, somit keine Reportvarianten.<br>
Um nun eine Datei pro Tag zu erzeugen, kann z.B. folgendes Pattern verwendet werden: /mnt/Automatikjobs/{0,date,ddMMyyyy}_{1}.pdf<br>
Damit wird für den jeweiligen Tag einen Datei angelegt.<br>
Im Pattern bedeuten die Werte des ersten Parameters:<br>
0 ... Druckdatum<br>
1 ... Name der Reportvariante, also Standard<br>
2 ... Name des Reports<br>
Die Parameter sind optional, um auch alte Reports aufzubewahren. Beachte dazu auch die Einstellung für die Archivierungstage.