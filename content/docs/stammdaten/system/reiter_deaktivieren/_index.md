---
categories: ["Reiter deaktivieren"]
tags: ["Reiter deaktivieren"]
title: "Reiter deaktivieren"
linkTitle: "Reiter deaktivieren"
date: 2023-01-31
weight: 090
description: >
  Deaktivieren, also unsichtbar schalten einzelner Modulreiter
---
Reiter deaktivieren
===================

Es können ausgewählte obere Modulreiter Ihrer **Kieselstein ERP** Installation gezielt deaktiviert werden.

Siehe dazu System, Pflege, Panel sperren

Die Zuordnung der Reiter, welche nicht angezeigt werden sollten, erfolgt über die Resourcen Definition.
Die für die spezifische Sperre erforderlichen Einstellungen wird Ihnen von Ihrem **Kieselstein ERP** Betreuer gerne zur Verfügung gestellt.

Diese Funktion wird nur in den wesentlichen Modulen speziell im ersten unteren Reiter unterstützt. Der erste obere Reiter, welcher in der Regel die Auswahlliste ist, kann nicht ausgeblendet werden.

Für die benötigten Resource-Informationen siehe Quelltext.