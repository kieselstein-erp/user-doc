---
categories: ["Eigenschaften"]
tags: ["Automatik-Jobs"]
title: "Eigenschaften Definition"
linkTitle: "Eigenschaften Definition"
date: 2023-01-31
weight: 060
description: >
  Definition von Eigenschaften für verschiedene Module
---
Eigenschaften Definition
========================

In **Kieselstein ERP** steht für verschiedene Module die Möglichkeit der Definition und damit der Erfassung zusätzlicher Eigenschaften zur Verfügung. Damit können zusätzliche Felder in **Kieselstein ERP** definiert werden.

Die grundsätzliche Definition finden Sie im Modul System, unterer Modulreiter Eigenschaftsdefinition.

Hier können für die Module:
-   Angebot
-   Artikel
-   Artikel, Reiter Technik
-   Auftrag
-   Chargen
-   Kunde
-   Lieferanten
-   Los
-   Partner
-   Personal
-   Projekt
-   Projekthistory, also Projekt Detail

zusätzliche Eigenschaften / Eigenschaftsfelder definiert werden.

Sind Eigenschaften bei den Modulen definiert, so gibt es im jeweiligen Modul einen zusätzlichen oberen Modulreiter Eigenschaften.

Die Definition wird im Modulreiter Beschreibung vorgenommen und lehnt sich stark an die mit dem Layoutmanager übliche Definition der Position von Feldern an. D.h. die Anzeige erfolgt dynamisch, also abhängig von der jeweiligen Darstellungsform werden die Felder und Feldtypen breiter oder weniger breit dargestellt.
Wenn die Eigenschaften von Typ Kundeneigenschaften definiert werden, kann anstatt der Artikelgruppe die Partnerklasse ausgewählt werden. 

Die zielführendste Vorgehensweise ist, wenn Sie sich das Eingabefenster als Gitternetz vorstellen.
So hat z.B. jedes Element diese Gitters eine Breite von 100 und eine Höhe von 10.
Nun setzen Sie Ihre Elemente in dieses Raster.
Die wesentlichsten Felder für die Positionierung sind:

| Feld | Bedeutung |
| --- |  --- |
| Spalten- und Zeilennummer | Geben Sie hier die Position in Ihrem Gitternetz an, beginnend mit 0 links oben |
| horizontale Gewichtung | Die Breite mit einem Vielfachen der Breite eines GitterelementesEs zählt der jeweils höchste Wert in den Definitionen jeder Spalte.Hier hat sich bewährt, in nur einer Zeile, z.B. in der ersten, die Verteilung zu definieren. Bei allen nachfolgenden Zeilen wird dann die horizontale Gewichtung auf 0 belassen |
| vertikale Gewichtung | Die Höhe mit einem Vielfachen der Höhe eines Gitterelementes |
| Feldname | Vergeben Sie hier einen eindeutigen und sprechenden Feldnamen |
| Feldtext | Text der angezeigt wird, z.B. bei Lable, Checkbox oder Combobox |
| Feldtyp | Typ des Feldes siehe unten. |
| Feldbreite | Dies ist die Breite eines Feldes in Zellen.D.h. wird hier zwei eingegeben, so bedeutet dies, dass sich das Feld über zwei Spalten erstreckt. |
| Feldhöhe | Wie Feldbreite, aber für die Zeilen Höhe. |
| Mindestbreite | Diese definiert die Breite des Feldes in Pixel.Hierdurch kann eventuell die sich durch die Gewichtung ergebende Breite übersteuert werden. |
| Mindesthöhe | Analog zu Mindestbreite |
| Abstand oben | Abstand zu oberem Feld in Pixel |
| Abstand unten | Abstand zu unterem Feld in Pixel |
| Abstand links | Abstand / Rand zum linken Feld |
| Abstand rechts | Abstand / Rand zum rechten Feld |
| Pflichtfeld | Steht nur bei Combobox und Textfeld zur Verfügung. Bewirkt dass eine Eingabe ins Eigenschaftenfeld nur dann mit speichern verlassen werden kann, wenn zumindest ein Zeichen (Space) eingetragen wurde. |
| Druckname | Ist hier eine Definition eingegeben, so wird dieses Feld in einem eventuellen Report mit ausgedruckt. Es wird dabei die angegebene Feldbezeichnung angedruckt. |
| Default - Wert | Dieser Wert wird beim Druck automatisch vorbesetzt. Bei Auswertungen in den Reports mittels Scripten kann dieser als Default-Wert verwendet werden. |
| Artikelgruppe / Partnerklasse | Damit kann die Erfassung von Eigenschaften auf die Artikelgruppe (bei Artikeleigenschaften bzw. Chargeneigenschaften) bzw. Partnerklasse (bei Kundeneigenschaften) eingeschränkt werden. |

Mit dem Feldtyp können Sie die Art des Feldes definieren. Es stehen folgende Möglichkeiten zur Verfügung:

| Feldtyp | Bedeutung |
| --- |  --- |
| WrapperLabel | Vergeben Sie diesen Type für die Beschriftung Ihres Feldes<br>Hinweis: Labels werden von **Kieselstein ERP** grundsätzlich rechtsbündig dargestellt. |
| WrapperTextField | Einzeiliges Texteingabefeld, dieses kann eventuell als Pflichtfeld ausgeführt werden |
| WrapperTextArea | Mehrzeiliges Texteingabefeld. |
| WrapperCheckbox | Eine Box mit Bezeichnung zum Anhaken **Tipp:** Da die Checkbox eigentlich aus zwei Teilen, der eigentlichen Box und dem Label dazu besteht hat sich in der Praxis bewährt, dass diese erst in / ab der zweiten Spalte definiert wird. |
| WrapperEditor | Ein Eingabefeld für eine (lange) Texteingabe (3000 Zeichen) |
| WrapperExecButton | Start des definierten Programms, bei dem der eingegebene Wert als Parameter übergeben wird. |
| WrapperPrintButton | Start des in den Eigenschaften definierten Reports. Beachten Sie, dass im Feld Report der Name des gewünschten Reports mit der Endung .jasper eingegeben werden muss.Der Report selbst wird im Reportverzeichnis ../report/panel/ mit den üblichen Steuerungen (anwender, Mandant, Sprache) erwartet.Der Aufbau des Reports ist so, dass an diesen zwei Parameter übergeben werden.P_KEY ... in diesem wird die ID das Datensatzes als String übergeben z.B. um daraus die Artikeldaten zu holenP_PANEL_C_NR ... in diesem wird die Art der Eigenschaften (z.B. Artikeleigenschaften) übergeben.In den Fields finden Sie alle unter den Eigenschaften definierten Felder. Die Feldnamen entsprechen dem unter Feldname angegebenen Texten. Beachten Sie die Groß-/Kleinschreibung. In den Inhalten dieser Fields, welche in den Details angedruckt werden, stehen die für den jeweiligen Datensatz angegebenen Inhalte.Beachten Sie bitte, dass derzeit die Inhalte ausschließlich als String verwaltet werden.Beachten Sie bitte weiters, dass die Feldtypen WrapperLabel, WrapperExecButton und WrapperPrintButton NICHT an den Report übergeben werden.Einen Musterreport finden unter .../report/panel/muster.jrxml. |
| WrapperCombobox | Definieren Sie im Feldtext die in der Combobox zur Auswahl stehenden Werte. Die einzelnen Werte sind durch | getrennt. Eine automatische Berücksichtigung von Leerzeichen ist nicht vorgesehen.Die Combobox kann eventuell als Pflichtfeld ausgeführt werden.Ist die Combobox kein Pflichtfeld, so gibt es die Auswahl <leer>.Ist die Combobox ein Pflichtfeld steht diese Auswahl nicht zur Verfügung.<leer> bewirkt, dass keine Daten angedruckt werden. |

Um die Gestaltung zu erleichtern, haben wir einen oberen Modulreiter Vorschau hinzugefügt. Damit können Sie sofort die Darstellung Ihrer Einstellungen überprüfen.
Nach dem Abschluss der Einstellungen starten Sie das entsprechende Modul neu und Sie finden den zusätzlichen Reiter Eigenschaften mit den definierten Feldern vor.
**Tipp/Hinweis:** Da in der Vorschau nur diejenigen Felder angezeigt werden, die KEINE Artikelgruppe bzw. Partnerklasse hinterlegt haben, geben Sie für die Definition der ersten Eigenschaften bitte KEINE Artikelgruppen/Partnerklassen an. Erst wenn die grundsätzliche Gestaltung klar ist, hinterlegen Sie diese einschränkenden Filter.

Um eine Darstellung ähnlich der untenstehenden zu erhalten, definieren Sie z.B. wie folgt:
![](EigenschaftenMuster.jpg)

| Feldname | Spalten-nummer | Zeilen-nummer | horizontale Gewichtung | vertikale Gewichtung | Feldtyp | Feldtext | Druckname |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |
| TextzeileLable | 0 | 0 | 100 | 10 | WrapperLable | Text |   |
| Textzeile | 1 | 0 | 200 | 10 | WrapperTextfield |  | Textzeile |
| leer | 2 | 0 | 100 | 10 | WrapperLable | <Leerzeichen> |   |
| Eingabegeprüft | 1 | 1 | 100 | 10 | WrapperCheckbox | Eingabe geprüft | Eingabegeprueft |
| BeschreibungLable | 0 | 2 | 100 | 40 | WrapperLable | Beschreibung |   |
| BeschreibungText | 1 | 2 | 300 | 40 | WrapperEditor |  | Beschreibung |

**Hinweis:**

Solange die Felder nicht mit Werten befüllt sind, können sie auch wieder gelöscht werden. Wurde ein Datensatz an die Beschreibung angefügt, ist ein Löschen nicht mehr möglich. Die Änderung der Bezeichnungen hingegen ist jederzeit möglich.

Drucken aus den Artikeleigenschaften

Siehe bitte oben, WrapperPrintButton.

#### Die Technik Eigenschaften werden nicht angezeigt?
Die Ursache dürfte darin liegen, dass die gewählte / zur Verfügung stehende Bildschirmhöhe für eine gute Anzeige nicht ausreicht.

#### Programmaufruf
Wie ist ein Programmaufruf einzurichten?

Beispiel:<br>
Man möchte mit einem Button z.B. eine bestimmte PDF Datei im Artikel anzeigen.<br>
Um dies zu erreichen tragen Sie in den Artikeleigenschaftsdefinitionen folgende Daten ein, bzw. ist wie folgt vorzugehen:
- Definieren Sie den vollständigen Aufrufpfad des Programmes.<br>
Wichtig: Dieser Aufrufpfad, welcher im Feld Befehlsstring einzugeben ist, muss für alle Clients gleich sein. Das bedeutet, dass idealerweise dieses Programm auf einem von allen erreichbaren UNC Pfad eingerichtet sein sollte.
- Wenn nun für alle Datensätze die gleiche Datei angezeigt werden sollte, so muss der Befehlsstring auch die aufzurufende Datei beinhalten
- Sollten hier unterschiedliche Aufrufparameter je Artikel verwendet werden, z.B. unterschiedliche Dateien, so sind diese in den Feldinhalt bei jedem Artikel anzugeben.

**Wichtig:** Idealerweise verwenden Sie für die aufzurufenden / anzuzeigenden Dateien eine in Ihrer IT-Umgebung eindeutige Definition, z.B. die UNC Notierung. So sind Sie von Laufwerksbuchstaben unabhängig. Andererseits bitte sicherstellen, dass bei allen Clients der zugeordnete Laufwerksbuchstabe und seine Bedeutung gleich ist.<br>
Dass die jeweiligen Anwender zumindest einen lesenden Zugriff auf die Datei haben müssen ist selbstverständlich.<br>

Beispiel für den Aufruf einer allgemeinen PDF Datei für alle Artikel:
![](Direkter_Aufruf_mit_dateiangabe.gif)<br>

Beispiel für den Aufruf einer PDF Datei mit der Angabe der jeweils darzustellenden Datei im jeweiligen Artikel:
- Definition des aufzurufenden Programms ![](Programmaufruf.gif)<br>
- Definition im Artikel ![](Dateiangabe_in_den_Eigenschaften.gif)<br>
Beachte bitte in beiden Fällen, dass Programmnamen welche Leerzeichen enthalten, in jedem Falle in "Hochkomma" gesetzt werden müssen.