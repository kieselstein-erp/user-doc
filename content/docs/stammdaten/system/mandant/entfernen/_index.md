---
categories: ["Mandant"]
tags: ["Mandant"]
title: "Mandant entfernen"
linkTitle: "Mandant entfernen"
date: 2023-01-31
weight: 100
description: >
  Einen bestehenden Mandanten entfernen
---
Nachdem es immer wieder zu Unternehmens Um- und Ausgründungen kommt, ist es auch immer wieder erforderlich Mandanten aus bestehenden Datenbanken zu entfernen.

Daher hier eine lose Sammlung, die nicht vollständig sein muss, von Punkten, die bei der Trennung zu beachten sind.

Voraussetzung für diese Arbeiten ist, dass du entsprechend administrative Zugriff auf deine **Kieselstein ERP** Datenbank hast und mit diesen Arbeiten auch vertraut bist.

Hinweis: Wie bereits in einem anderen Kapitel beschrieben, ist die Änderung einer Mandantenkennung (die Mandantennummer ![](mandant_c_nr.png)  ) bei einer bestehenden Installation faktisch nicht möglich.

Grundsätzlich kann man sich darauf verlassen, dass fast alle Tabellen einen Constraint zur lp_mandant.c_nr über das Feld mandant_c_nr haben.<br>
Das bedeutet, eigentlich kann man damit beginnen, dass versucht wird, den gewünschten Eintrag aus der lp_mandant zu entfernen. Dies wird nicht funktionieren, da Abhängigkeiten ([Constrains](https://de.wikipedia.org/wiki/Constraint)) gegeben sind. Damit erhältst du einen Hinweis, in welcher Tabelle auf den Mandanten verwiesen wird. Also muss man die Daten vorher entsprechend löschen. Das wird vermutlich wieder nicht funktionieren, also das davor entfernen. So arbeitet man sich mit Hirn und Verstand auf die tatsächlich zu entfernenden Daten durch.<br>
Hinweis: Das Cascaded Delete war mir bisher immer zu gefährlich, habe ich nie angewandt. Ich verlasse mich lieber auf mein Verständnis der Daten, als auf irgend einen Automaten.<br>
Und warten, dass der Datenbankserver mit der Transaktion fertig wird, muss man sowieso.

Bewährt hat sich, dass man dies auf einer Kopie der Daten durchführt und das so erarbeitete Script dann am Echtsystem, bei abgeschaltetem Wildfly durchführt.

{{% alert title="Wichtig" color="primary" %}}
Ein gezieltes Löschen der Dokumente aus der Dokumentendatenbank ist nicht möglich. Je nachdem welche Daten weitergegeben werden sollten, kann man sich darauf einigen, dass die Dokumentendatenbank neu erzeugt wird. Also mit einer leeren Dokumentendatenbank im abgesplitterten Unternehmen neu begonnen wird.
{{% /alert %}}

Es gibt einige wenige Tabellen für die das nicht gilt. Meist hat dies in der Funktionsweise deines **Kieselstein ERP** keine Auswirkung, ausgenommen ein etwas höherer Speicherbedarf, der Kieselstein Datenbank.

Nachfolgend daher eine Scriptsammlung, die laufend weiterentwickelt wird:
| Tabelle / Beschreibung | Script |
| --- | --- |
| lp_automatikjobs | delete from lp_automatikjobs where not mandant_c_nr in (select c_nr from lp_mandant); |

