---
categories: ["Mandant"]
tags: ["Mandant"]
title: "Mandant anlegen"
linkTitle: "neuer Mandant"
date: 2023-01-31
weight: 700
description: >
  Einen neuen Mandanten anlegen
---
In deinem **Kieselstein ERP** kannst du, mit etwas Datenbankkenntnis auch weitere Mandanten selbst definieren. Du benötigst dafür in jedem Falle das Admin Passwort für deine **Kieselstein ERP** Installation und einen administrativen Zugang zu deiner KIESELSTEIN Datenbank.

Melde dich als Admin an, dieser muss die volle Administratorrolle haben und lege im System, Mandant einen weiteren Mandanten an. Beachte, dass die Kennung des Mandanten von Anfang an richtig vergeben werden muss. Ein Ändern dieser Kennung ist mit bereits eingepflegten Daten faktisch unmöglich.

Nach dem Anlegen des neuen Mandanten ist automatisch nur die Person Admin auch als Benutzer im neuen Mandanten eingetragen. Vergib nun, noch vom alten Mandanten aus, dem Admin die Administratorrolle für den neuen Mandanten.

Um den neuen Mandanten auch grundsätzliche Berechtigungen zu vergeben, müssen diese in der Datenbank eingetragen werden. Verwende dazu das Journal Rollen und Rechte aus dem Modul Benutzer Rollen und Rechte, unterer Reiter Systemrolle ![](journal_rollen_und_rechte.png)  
Hier findest du nach der Auflistung der Rollen und Benutzer usw. auch die Modulübersicht.<br>
![](moduluebersicht.png)  <br>
Dies ist eine Aufstellung über alle Module die es programmtechnisch gibt.<br>
{{% alert title="Hinweis" color="info" %}}
Beachte, dass sich manche Module, Zusatzfunktionen gegenseitig ausschließen. Daher ist eine "kopflose" Freischaltung aller Module kontraproduktiv!
{{% /alert %}}
Trage nun den neuen Mandanten in die Modul und Zusatzfunktionstabellen ein.

Melde dich nun als Admin neu an und wechsle mit dem Mandantenwechsel ![](mandantenwechsel.png)  am neuen Mandanten an.

## Welche Punkte sind nach dem Anlegen eines neuen Mandanten noch zu beachten:
- Prüfe die Parameter des neuen Mandanten gegen den Hauptmandanten. Wähle dafür im neuen Mandanten, System, Parameter und dann den Druck der Auswahlliste ![](auswahlliste.png)  Damit bekommst du einen Vergleich der default Parameter mit den eingestellten Parametern des Hauptmandanten.
{{% alert title="Tipp" color="info" %}}
Achte darauf, dass auch anhand der Belegnummern eine Unterscheidung, z.B. für deine Lieferanten, möglich ist. Siehe dazu auch Parameter Anwender bzw. BELEGNUMMER_MANDANTKENNUNG.
{{% /alert %}}
- Für einen neuen Mandanten muss die Tabelle FB_UVAART ergänzt werden. Nutze dafür das aus dem Spreadsheet erzeugte [Script](UVA_ART.ods), welches du mit der neuen Mandantenkennung anpasst.
- Weiters muss für den neuen Mandanten auch die Tabelle FB_REVERSECHARGEART und die FB_REVERSECHARGEARTSPR ergänzt werden. Auch dafür gibt es ein Spreadsheet, [siehe](./ReverseChargeArtenNachtragen.ods). Auch hier die Mandantenkennung und vor allem auch die in deinem Land für den Mandanten gültigen Bezeichnungen eintragen. Bewährt hat sich auch, nur die tatsächlich verwendeten Reversechargearten anzuzeigen (nicht versteckt).
- Es hat sich bewährt jedem Mandanten ein eigenes grafisches Symbol in der Statuszeile zu geben, damit du beim Arbeiten sofort siehst in welchem Mandanten du bist. Trage dies unter System, Mandant, Vorbelegungen 2 ein. Die Bilddatei muss png sein und 320x16Pixel haben.
- 

Die Systemrolle (Administrator) muss auch das Recht haben, LP_HAUPTMANDANT_U.

Folgende Parameter fehlen bei einem neuen Mandanten:
- RUNDUNGSAUSGLEICH_ARTIKEL
- für eine weitere Liste verwenden den Auswahllistendruck aus System, Parameter aus dem Hauptmandanten heraus. Hier wird mit !! angezeigt, wenn in Bezug auf den Hauptmandanten Parameter in anderen Mandanten fehlen.

Diese müssen über die Datenbank nachgetragen werden. Idealerweise wird dafür ein Script erstellt. [Siehe dazu]( {{<relref "/docs/installation/19_tipps_und_tricks/#fehlende-parameter-f%c3%bcr-weiteren-mandanten-nachtragen" >}} )

