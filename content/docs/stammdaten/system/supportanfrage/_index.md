---
categories: ["Support"]
tags: ["Supportanfrage"]
title: "Supportanfrage"
linkTitle: "Supportanfrage"
date: 2023-01-31
weight: 400
description: >
  Support und Betreuungsunterstützung für Kieselstein ERP eG Mitglieder
---
Supportanfrage
==============

**Bitte um Beachtung!**
Die Unterstützung zu deinem / euren **Kieselstein ERP** steht nur für Mitglieder der Kieselstein ERP eG zur Verfügung. Nachdem die Mitgliedsfirmen die Weiterentwicklung des Systems durch die Mitgliedsbeiträge finanzieren, sehen wir es als selbstverständlich an, dass für eine qualifizierte Betreuung, ein entsprechender Subskriptionsbeitrag geleistet wird.

Wir freuen uns auf weitere Mitglieder und somit auf euren Mitgliedsantrag. Bitte sendet diesen an info@kieselstein-erp.org. Siehe dazu gerne auch https://kieselstein-erp.org/.

Um deine Supportanfrage für euch und uns, so rasch und effizient wie möglich bearbeiten zu können bitten wir folgende Punkte zu beachten:
-   Dringlichkeit:
    Ist es wirklich dringend, d.h. [steht das Unternehmen](#mein-unternehmen-steht), oder geht es "nur" um eine Vereinfachung des Ablaufes, eine Veränderung eines Formulars, oder einfach eine Frage.<br>
    Bitte beachte, dass wir nur einen echten Showstopper, als wirklich dringend akzeptieren können. Es ist niemand in der Lage mehrere Projekte, welche sofort behandelt werden müssen, gleichzeitig qualitätsvoll und effizient zu erledigen.<br>
    Es ist uns gelungen und gleichzeitig immer währende Aufgabe, die Auslieferqualität von **Kieselstein ERP** so hoch zu halten, dass es im Echtbetrieb zu keinem Showstopper kommt.

-   Worum geht es überhaupt?
    Leider erhalten wir viele Anfragen mit das geht nicht, das stimmt nicht und ähnlichen Aussagen.<br>
    Ein tatsächliches Beispiel war ein EMail mit<br>
    Sie rechnen falsch und <br>![](Support1.gif)
    <br>
    diese Aufstellung.<br>
    Eine Stunde später, kam, ich brauche dazu dringend eine Aussage.

    Wir sind, so wie ihr auch, nur Menschen, manchmal gute Detektive, aber sicher keine Hellseher.<br>
    Daher: Liefert uns bitte **konkrete Informationen**

-   Das ist seit dem letzten Update so!<br>
    Auch das eine oft gehörte Aussage.<br>
    Ja es kann vorkommen, dass sich **Kieselstein ERP** aufgrund eines Updates anders verhält.<br>
    Bitte gehe davon aus, dass ein anderes Verhalten, wenn überhaupt, sich aus gesetzlichen Änderungen oder ähnlichen Erkenntnisgewinnen ergibt. Auch wenn wir laufend **Kieselstein ERP** weiterentwickeln, so ist es uns an sehr großes Anliegen, bei der Auslieferung eines Updates, das gleiche Verhalten wie vor der Änderung zu haben, mit der Ausnahme dass Fehler behoben sind.

## Ist dein Kieselstein ERP aktuell
Die Grundvoraussetzung für eine Supportanfrage ist, dass dein Kieselstein ERP auf aktuellem Stand ist. [Siehe dazu]( {{<relref "/docs/installation/01_server/update/" >}} ) [und auch]( {{<relref "/docs/installation/01_server/00_soll_ich_updaten/" >}} ).

## Konkrete Informationen
Schreibe deine Supportanfrage wie einen gute Newsletter, d.h.:

Im ersten Satz kann man erkennen was du von uns möchtest. Also um welches Modul es geht, was du gemacht hast und was dann passiert bzw. welches Ergebnis du erwartet hast.

Dann bitte die ausführliche Beschreibung dazu.

Z.B. Modul Lieferschein, neuen Lieferschein angelegt, bitte hier bereits sehr konkret, war es nur der Neubutton, oder wurde neuer Lieferschein aus Auftrag gewählt.<br>

Was wurde dann gemacht? Kopfdaten gespeichert, also siehe Lieferschein Nr. ....<br>
Hier wurde nun über Position ..... der Artikel (bitte die Artikelnummer oder ein anderer sehr konkreter Hinweis) eingefügt und dann kam die Meldung oder auch den Artikel konnte ich nicht speichern weil oder der Preis ist falsch oder man kann die Seriennummer nicht auswählen usw usf..<br>
Wenn Bilder hilfreich sind, bitte Bilder hinzugeben. Für Windows-Verwender, nutze bitte Alt+Druck um nur den **Kieselstein ERP** Client in die Zwischenablage zu kopieren bzw. noch besser Shift+Windowstaste+S und nur den wirklich wichtigen Bildausschnitt herauszuschneiden und dieses Bild dann mit Strg+V in das EMail Programm zu bekommen. Mach bitte die Bilder klein, aber mit allen Informationen und so konkret wie möglich und

**wie kann das Verhalten nachgestellt werden**.

Ein Verhalten welches nicht nachstellbar ist, können wir leider nicht beheben. Es ist die Information trotzdem für uns wertvoll, da wir auch diese Phänomene sammeln und versuchen sie, zumindest langfristig, abzustellen. Beachten bitte, ein nicht nachstellbares Verhalten, können wir zumindest unmittelbar, nicht beheben.

## Das muss ich Ihnen zeigen
Gerne besprechen, klären wir in einer gemeinsamen [Fernwartungssitzung](#zugriff--fernwartung), in der Regel Teamviewer, worum es nun wirklich geht.<br>
Bitte sorge dafür, dass der [Teamviewer](https://download.teamviewer.com/download/TeamViewer_Setup_x64.exe) auf deinem Arbeitsplatzrechner installiert und Verbindungsbereit ist. <br>Es ist schlichtweg schade um die Zeit, wenn wir jedes Mal, die Installation des Fernwartungsprogramms per Telefon ansagen müssen, nur um dann festzustellen, dass du so ein Programm auf deinem Rechner nicht ausführen darfst. Selbstverständlich ist auch diese Betreuungszeit, auch wenn du nicht wirklich einen Nutzen davon hast, **kostenpflichtig**. 

## Zugriff / Fernwartung
Um das Verhalten analysieren zu können benötigen wir in der Regel einen Zugriff auf dein / euer System. Je effizienter, leichter dieser Zugriff ist, desto schneller können wir helfen. In diesem Zusammenhang sei auch auf unsere Geheimhaltungsverpflichtungen hingewiesen, die wir schon immer hatten / beachtet haben und die mit der [DSGVO]( {{<relref "/docs/stammdaten/dsgvo">}} ) konkretisiert und allgemein bekannt gemacht wurden.<br>
Bitte beachtet, dass wir einen direkten VPN Zugang zu deinem/euren **Kieselstein ERP** Server (eingeschränkt auf nur zu diesen) eindeutig bevorzugen. Dies ist auch der für euch sicherste Weg.

## Ich habe das nun herausgelöscht, ich musste weiterarbeiten

Wir haben für den enormen Druck der in produzierenden Unternehmen derzeit herrscht durchaus Verständnis. Um, eine Fehlermeldung / Supportanfrage eurerseits nachstellen zu können, benötigen wir die Daten die zum nicht gewünschten Verhalten geführt haben. Gibt es diese nicht mehr, können diese Konstellationen nicht mehr hergestellt werden, so ist das Verhalten nicht nachstellbar. Wenn derartige Konstellationen sind, so informiert uns bitte raschest möglich, jedenfalls vor der Änderung. Bei entsprechendem Fernwartungszugang, können wir sehr rasch einen Datenbank-Dump starten um genau diesen Datenbestand zur Verfügung zu haben. Ihr könnt nach dem Start des Dumps bereits weiterarbeiten, wobei dieser in der Regel einige Minuten dauert. Meist ist hier das Thema des Zugriffes welches weit mehr Zeit kostet, als der eigentliche Dump.

## Datensicherung / alte Datenbestände

Manchmal wird behauptet, in der Abrechnung die ich am 2.1. gemacht habe, war das aber vollkommen anders. Wenn ich nun (im März) die exakt gleiche Abrechnung mache, kommt es zu einem anderen Ergebnis. Um dazu eine konkrete Aussage machen zu können, brauchen wir die Datenbank exakt zum Auswertezeitpunkt. Kann diese nicht zur Verfügung gestellt werden, können wir keine Aussage dazu machen. Daher:<br>
**PRÜFE DEINE** Kieselstein-ERP **DATENSICHERUNG REGELMÄSSIG!**

## Deine Antwort
Wir freuen uns wenn ihr uns auch mitteilt, dass unsere Änderung den gewünschten Nutzen gebracht hat. Ein kurzes ***Danke geht***, genügt vollkommen, erleichtert uns die Verwaltung der offenen Supporttickets ungemein und zeigt uns eure Wertschätzung.

## Mein Unternehmen steht!
Was meinen wir mit "es steht dein Unternehmen"?<br>
Das wären Dinge, wie z.B. es können keine Ablieferungen gemacht werden, es können keine Lieferscheine erstellt werden, die Datensynchronisierung mit xxx funktioniert nicht mehr und ähnliches.<br>
Hier sieht man auch die Problematik dieses, es ist dringend.<br>
Bleiben wir beim Beispiel es können keine Lieferscheine erstellt werden.<br>
Es muss für jeden **Kieselstein ERP** Verwender logisch sein, dass um Ware ausliefern zu können, diese verfügbar sein muss.<br>
Verfügbar bedeutet, nicht nur dass ich diese physikalisch in der Hand halte (diese vor mir liegt), sondern dass ein entsprechender Lagerstand im jeweiligen Lager gegeben sein muss.<br>
Ist dies nicht gegeben, muss dafür gesorgt werden, dass die Ware ans richtige Lager gebucht wird.<br>
Wir gehen davon aus, dass dies zumindest bei eurem **Kieselstein ERP** Verantwortlichen bekannt ist und dass dieser auch in der Lage ist, die Ursache warum die Ware nicht in den Lieferschein gebucht werden kann, herauszufinden und dir somit zu helfen.<br>
Um dieses Wissen, Können an euch zu vermitteln, sollten regelmäßig entsprechende Schulungen durchgeführt werden. Diese werden üblicherweise bei euch vor Ort, mit euren Daten, also Artikeln, Adressen, Abläufen, durchgeführt.<br>
Wir bitten um Verständnis, dass diese Punkte<br>
- nicht als dringend
- als Schulung, welche immer in irgend einer Form kostenpflichtig ist

betrachtet werden.

Gerne verweisen wir in den Zusammenhang auch auf die **Kieselstein ERP Akademie** die für Genossenschaftsmitglieder regelmäßig, in der Regel monatlich, zu den verschiedensten Themen abgehalten wird.