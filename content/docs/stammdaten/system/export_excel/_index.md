---
categories: ["Export von Daten"]
tags: ["Datenexport nach Excel(R)"]
title: "Datenexport nach Excel(R)"
linkTitle: "Datenexport nach Excel(R)"
date: 2023-01-31
weight: 210
description: >
  Datenexport nach Excel(R)
---
Export nach Excel(R)
====================

Um die Daten aus den Reportfields nach Microsoft Excel übernehmen zu können und dabei die Formatierungen der Zahlen richtig zu übertragen ist in MS-Excel eine besondere Vorgehensweise notwendig.

Tipp: Wir nutzen in der Regel LibreOffice, welches dies per se richtig macht.

1.  Wählen eine zu übernehmende Auswertung.
    Es wird dies anhand der Hitliste erklärt, kann aber analog auch für alle anderen Auswertungen verwendet werden.

2.  Drucke die Auswertung in die Vorschau

Hinweise zur Übernahme nach Excel(R)

Da wir immer wieder gefragt werden wie man das direkt nach Excel(R) übertragen kann, hier die Vorgehensweise dazu:
Der wesentliche Punkt ist, dass man Excel mitteilen muss, aus welcher Datenquelle der Import aus der Zwischenablage stammt.
D.h. nachdem du die Daten in die Zwischenablage kopiert hast, muss als nächstes ein neues Tabellenblatt geöffnet werden.
Klicke nun mit der rechten Maustaste in das linke, oberste Feld, in das du die Daten importieren möchtest. In der Regel wird das das Feld A1 sein.
Wähle nun unter den Einfügeoptionen<br>
![](Excel_Import1.gif)<br>
die Übernahme von Textdateien, also das Klemmbrett mit dem A.

Sollte die Übernahme, z.B. bei Tausendertrennzeichen u.ä. nicht deinen Wünschen entsprechen, so muss die Verwendung des Textkonvertierungsassistenten definiert werden.<br>
D.h. nach dem ersten Einfügen findest du rechts unterhalb der eingefügten Daten
![](Excel_Import2.gif)<br>
Klicken nun auf den Pfeil und<br>
![](Excel_Import3.gif)<br>
hake den Textkonvertierungs-Assistentenverwenden an.<br>
Definiere hier, in den nachfolgenden Schritten, wie die Daten übernommen werden sollten. Üblich sind hier:

Definiere bitte, insbesondere für die richtige Übernahme von Umlauten und nicht Std.Ansi Zeichen, den UTF-8 Code<br>
![](Excel_Import4.gif)

Die Daten aus der Zwischenablage sind mit Tab(stopp) getrennt.<br>
![](Excel_Import5.gif)

Und gegebenenfalls sind Datumsspalten und auch Zahlenwerte, die Tausendertrennzeichen beinhalten, entsprechend zu definieren.<br>
![](Excel_Import6.jpg)

