---
categories: ["Pflegefunktionen"]
tags: ["Pflegefunktionen"]
title: "Pflegefunktionen"
linkTitle: "Pflegefunktionen"
date: 2023-01-31
weight: 080
description: >
  Reorganisieren, aufrollen von Daten
---
Pflegefunktionen
================

In einigen Fällen ist es erforderlich, Daten aufzurollen.

Aus Sicherheitsgründen sind die Pflegefunktionen mit der Systemrolle WW_DARF_LAGERPRUEFFUNKTIONEN_SEHEN gekoppelt.

Die Pflegefunktionen die vom Anwender ausgeführt werden dürfen sind in den einzelnen Modulen enthalten. Die Pflegefunktionen, welche Sie im Modul System, unterer Modulreiter Pflege finden, dürfen ausschließlich auf Anweisung von **Kieselstein ERP** ausgeführt werden. Werden diese trotzdem ausgeführt, kann es erforderlich werden, dass Daten oder Stati so verändert werden, dass diese wiederhergestellt werden müssen. Wir lehnen jede Verantwortung aus dem Aufruf der Pflegefunktionen im System Modul ab.

Für den Anwender stehen folgende Pflegefunktionen zur Verfügung.

## Artikel
- VK-Preise, Verkaufspreise / Rabatte gezielt ändern. <br>![](Pflegefunktion2.gif)
- Serien-/Chargennummer ändern<br>
Gezielt eine Serien bzw. Chargennummer des gewählten Chargen/Seriennummern geführten Artikels ändern, z.B. wegen Tippfehler
- Paternoster hinzufügen<br>![](Pflegefunktion1.jpg)<br>
Übertragen aller Artikel bei denen der Lagerplatz vom Typ Paternoster hinterlegt ist, in denselben.

## Zeiterfassung
- Automatikbuchungen<br>
Bei Änderungen an den Zeitmodellen, insbesondere mit automatischen Pausen kann es erforderlich werden, dass die bei Geht Buchungen automatisch eingefügten Buchungen neu eingetragen werden. Hier steht die Möglichkeit zur Verfügung, dass für eine / alle Personen die Automatik Buchungen im angegebenen Zeitraum entfernt werden und dann aufgrund der nun bestehenden Zeitmodelle neu eingetragen werden.

## Pflegefunktionen aus dem Modul System:**
Diese dürfen NUR auf Anweisung aufgerufen werden:
- Prüfe Rechnungswert
- Rabattsätze einer Preisliste nachtragen
### Artikel
- Rabattsätze einer Preisliste nachtragen
- Prüfe Lagerabgangursprung
- Prüfe Lagerstände
- Prüfe Belege gegen die Lagerbewegungen auf Konsistenz
- Prüfe Lagerbewegungen gegen die Belegpositionen auf Konsistenz
- Prüfe "Vollständig verbraucht"
- Prüfe Reservierungen
- Prüfe Bestelltliste
- Prüfe Rahmenbedarfe
- Prüfe Auftragseriennummern
- Prüfe Verkaufspreisfindung Staffelmenge
- Prüfe VK-Preise im Lager
- Prüfe verbrauchte Mengen

### Rechnung
- Prüfe Rechungswert

### Fertigung
- Prüfe Fehlmengen
- Lose einer Stückliste nachkalkulieren<br>
Es werden alle vollständig erledigten Lose der angegebenen Identnummer nachkalkuliert. Eventuell fehlerhafte Lose werden übersprungen. Diese werden ins Server-Log eingetragen

### Bestellung
- Bestellstati prüfen
- Bestellpositionsstati prüfen

### Eingangsrechnung
- Aktualisiere Fremdwährungswerte anhand aktueller Kurse