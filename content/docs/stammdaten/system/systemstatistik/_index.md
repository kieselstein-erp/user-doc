---
categories: ["System-Statistik"]
title: "System-Statistik"
linkTitle: "Statistik"
date: 2023-01-31
weight: 240
description: >
  System Statistik, wie entwickelt sich das Datenvolumen usw.
---
Statistik
=========

Um die Entwicklung eurer **Kieselstein ERP** Daten einschätzen zu können, steht im Modul System eine kleine statistische Auswertung der Daten zur Verfügung.

Du findest hier im wesentlichen wieviele Belege mit wievielen Positionen im gewählten Zeitraum angelegt wurden.

Ergänzend sei dazu auch auf die Auswertung der Benutzerstatistik mit der nachfolgenden Zusammenfassung verwiesen.

Die System-Statistik findet man im Modul System, unterer Modulreiter Pflege, oben Menüpunkt Info, Statistik.

Neben der Auswahl des Zeitraum stehen auch die verschiedenen Zeitbereiche zur Verfügung.
![](System_Statistik.jpg)

Eine Gesamtzusammenfassung findest du ebenfalls im Modul System, unterer Modulreiter Gesperrt, oberer Modulreiter Logged in.
![](System_Statistik2.jpg)
Nun den Drucker anklicken und den gewünschten Zeitraum angeben.
Wird hier mehr wie ein Monat angegeben so kommen auf der letzten Seite nachfolgende Infos:
![](System_Statistik3.jpg)