---
categories: ["Dokumentenablage"]
tags: ["Dokumentenablage"]
title: "Parametrierung Dokumentenablage"
linkTitle: "Parametrierung"
date: 2023-01-31
weight: 400
description: >
  Parametrierung der Dokumentenablage
---
Dokumentenablage
----------------

wird gegliedert in

-   interne Dokumente
-   externe Dokumente

## Interne Dokumente
- Dienen der Protokollierung der an den Kunden/Lieferanten/Partner versandten Informationen.<br>D.h. sie sind Kopien der "Papier"-Belege welche versandt wurde. Zweck: Wenn Änderungen an den Anwenderdaten vorgenommen werden, so müssen
  1. wegen Datenkonsistenz die Inhalte auch bereits vollständig erledigter Vorgänge geändert werden (aus Anwendersicht)
  2. wird aber ein altes Dokument erneut ausgedruckt, so müssen die alten Inhalte gedruckt werden. Bedingung ist der Änderungs-Timestamp der Kopfdaten gegenüber dem Timestamp der Abspeicherung des Reportinhaltes. Um die Daten auch nach Texten / Strings durchsuchen zu können wird das iReport Format für die Abspeicherung verwendet.
- Beim Drucken eines Beleges / des Papierdatensatzes wird immer auf das interne Dokument zurückgegriffen. Einzige Ausnahme ist, wenn das Dokument aufgrund einer Änderung neu erstellt wurde. Hier wird, wenn das Dokument **echt** gedruckt wurde, das Dokument als neue Version dieses Vorgangs abgelegt. Echt gedruckt bedeutet, dass das Dokument nicht nur in die Vorschau gedruckt wurde, sondern dass es tatsächlich an den Drucker gesandt, an den Fax- oder EMail- Versand übergeben wurde. Ob das Fax, das EMail tatsächlich raus gegangen ist, ist für die Dokumentenverwaltung nicht relevant.

## Externe Dokumente
Dienen der Ablage der Zusatzinformationen für Vorgänge, aber auch Kunden, Lieferanten, Artikel, Personal usw.

Externe Dokumente müssen zu fast jedem Objekt hinzugegeben werden können. Als Objekt ist hier ... gemeint (auch einzelne Stücklistenpositionen ?, sicherlich aber einzelne Arbeitsplanpositionen)

Dokumente haben eine Dokumentenart und eine Dokumentenklasse und eine Sicherheitsstufe. Mit der Sicherheitsstufe ist die Zugriffsberechtigung verknüpft. Ein neu Abspeichern eines Dokumentes ist jederzeit möglich, bewirkt aber **immer** eine neue Version dieses Dokumentes. Die Sichtbarkeit des Dokumentes ist vom Rollenrecht abhängig.

Jedes Dokument kann Beschlagwortet werden.
-   Auffinden des Dokumentes über
    -   das zugeordnete Objekt
    -   über den Kunden/Lieferanten = Partner des Objektes
    -   Durch Suche nach Schlagwort (kann auch leer sein)
    -   Durch Suche nach DokuArt

Die Suchbegriffe sind kombinierbar und wirken wie ein logisches **UND**.

-   Dokumententypen

Grundsätzlich werden als Dokumententypen PDF, HTML, JPEG, GIF, PNG, jasper, unbekannt unterstützt. Es können aber auch weitere Anzeigearten über externe Viewer eingebunden werden. Hier ist insbesondere an DXF gedacht. Es ist vollkommen klar, dass diese Anzeigeprogramme Betriebssystem (des Clients) spezifisch sind.

Der Zugriff auf die Dokumente muss direkt, über das Objekt, aber auch indirekt über das dem Objekt zugeordneten Kunden / Partner / Artikel in mannigfacher Weise möglich sein.

Die eigentlichen Dokumente kommen in eine eigene Datenbank. Die Verknüpfung ist in der KIESELSTEIN (DB) über eine N:M Tabelle in der auch die Server-ID enthalten ist abgebildet. Damit sind die Massendaten aus der produktiven Tabelle weg und es gibt keine Toten Links. Da wir normalerweise keine Datensätze löschen können (keine Kunden, keine Aufträge, keine Artikel), wird ein nachfolgendes Löschen von Daten in anderen Datenbanken nicht benötigt. In der N:M Tabelle sind auch die für uns wichtigen Gruppen, Rechte, ... enthalten, das ist ja von der Datenmenge her echt wenig. Die N:M Tabelle gibt es für jedes Modul, also Angebot, Auftrag, Kunden, ... .

Zusätzlich wird in der ServerID Tabelle der Zugang zu alternativen DB-Servern, die über einen JDBC Treiber verfügen müssen, eingetragen.

## Scannen mit NAPS2
[siehe]({{< relref "/docs/installation/14_dokumentenscann/_index.md" >}})
