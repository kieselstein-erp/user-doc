---
categories: ["Dokumentenablage"]
# tags: ["Dokumentenablage ablegen"]
title: "Dokumentenablage"
linkTitle: "Dokumentenablage"
date: 2023-01-31
weight: 040
description: >
  Dokumentenablage als nicht löschbares Archiv, ähnlich einem DMS
---

Dokumentenablage
================

In **Kieselstein ERP** ist auch eine praktische Dokumentenablage integriert.
Zur Parametrierung der Dokumentenablage wenden Sie sich bitte an Ihren Administrator bzw.
[siehe]({{< relref "parametrierung" >}})

Bitte beachten Sie, dass die **Kieselstein ERP** Dokumentenablage als Unterstützung Ihrer Verwaltung gedacht ist. Für den Falle eines Rechtsstreites benötigen Sie im Zweifelsfalle das Original. Dieses Original ist in Ihrer Ablage. Die **Kieselstein ERP** Dokumentenablage ist lediglich eine Kopie des Originales und vom Grundgedanken her nicht als Ersatz des Originales ausgelegt.<br>
Die Dokumentenablage gibt es in zwei Ausführungen:
- integrierte Datenbank, welche durchaus einige zig/hundert GigaByte umfassen kann
- externes Filesystem. Ist dann relevant wenn obiges aufgrund der Größe nicht mehr sicherbar ist.

Die Dokumentenablage steht in zwei Ausprägungen zur Verfügung:
- a.) Ablage der offiziellen Belege wie Angebote - Rechnungen, Bestellungen
- b.) Hinterlegen von beliebigen Dokumenten hinter (fast) jedem Datensatz.

Die Ablage der offiziellen Belege erfolgt automatisch, wenn der Beleg tatsächlich gedruckt wird. Wir verstehen darunter, dass er aus der Druckvorschau in die große Vorschau![](Doku_Ablage1.gif), auf Papier![](Doku_Ablage2.gif) oder als EMail![](Doku_Ablage3.gif) bzw. Fax ![](Doku_Ablage4.gif) versandt wurde.<br>
Hinweis: Welche Belege automatisch abgelegt werden sollten ist durch Ihren **Kieselstein ERP** Betreuer einstellbar. Sollten Sie also für Dokumentationszwecke zusätzliche Dokumente automatisch in der Dokumentenablage benötigen, so wenden Sie sich bitte vertrauensvoll an uns.

### Hinterlegen von beliebigen Dateien hinter (fast) jedem Datensatz.
Bei aktivierter Dokumentenablage, finden Sie in sehr vielen der **Kieselstein ERP** Auswahllisten das Dokumenten-Icon ![](Doku_Ablage5.gif) am rechten Rand der Auswahlliste. Durch Klick auf dieses Icon gelangen Sie in die Dokumentenverwaltung genau zum ausgewählten Datensatz, z.B. einer Rechnung.
![](Doku_Ablage6.gif)
Durch Klick aus das Plus neben rech_rechnung werden alle Versionen dieses Beleges dargestellt.
![](Doku_Ablage7.gif)
Diese Darstellung ist so organisiert, dass die neueste Version des Dokumentes immer oben ist.
Am rechten Teil des Bildschirms sehen Sie die Detailinformationen des Dokuments.
![](Doku_Ablage8.gif)

Wurde das Dokumente direkt aus **Kieselstein ERP** erzeugt (z.B. eine Rechnung) so wird im unteren Fenster 
![](Doku_Ablage9.png)
das Dokument inkl. Druckmöglichkeiten dargestellt.
Hier kann die alte Version mit den alten Daten erneut ausgedruckt bzw. z.B. als PDF abgespeichert und versandt werden.
Werden andere Dateiarten abgespeichert, so versucht **Kieselstein ERP** festzustellen ob aufgrund des Dateityps von Ihrem Client aus die Datei mit einem Programm geöffnet werden kann, wenn ja, wird direkt das 
Programm gestartet, ansonsten wird der Datei speichern unter Dialog gestartet. Alternativ verwenden Sie den Button ![](dokuablage_speichern.PNG) um die Datei in ihrem Dateisystem zu speichern.

Dies bedeutet, dass in der Dokumentenablage beliebige Dateitypen abgelegt werden können.
Grafikformate wie png, jpg, tiff und das **Kieselstein ERP** interne Format werden sofort angezeigt. Für alle anderen Dateien wird wie oben beschrieben vorgegangen.
Ebenso können Sie die Datei aus der Dokumentenablage mit klick auf ![](speichern_dokuablage.JPG) in Ihrem Dateisystem abspeichern - es öffnet sich das Fenster zur Auswahl des Ordners.

Anzeige von PDF-Dateien. Diese erfolgt **auch** über einen internen PDF Viewer. Dieser kann leider nicht alle vollumfänglichen Funktionen des je Betriebssystem spezifischen Readers darstellen. D.h. wenn ungenaue Darstellungen in Ihren PDF-Dokumenten gegeben sind, so verwenden Sie bitte die aktuelle Version über den oben beschriebenen Betriebssystem Aufruf (in zugewiesenem Programm öffnen).

Um bei einem beliebigen Dokument eine neue Version zu hinterlegen gehen Sie bitte auf den Ursprungsknoten und wählen danach ändern. Sie erzeugen damit automatisch eine neue Version Ihres Dokuments.
Hinweis: Ein einmal in die Dokumentenablage abgespeichertes Dokument kann nicht mehr gelöscht werden.
Es kann dieses jedoch eine neue Version erhalten bzw. versteckt werden.

Zum Hinzufügen neuer Dokumente zu einem Datensatz, wählen Sie, über die Auswahlliste des jeweiligen Moduls, die Dokumentenablage und klicken dann auf den Neuknopf ![](Doku_Ablage10.gif).
Alternativ können Sie auch Dokumente über die Twain-Schnittstelle ![](Doku_Ablage11.gif) importieren.
Gegebenenfalls muss der Client entsprechend parametriert werden.[siehe]({{< relref "parametrierung" >}})
Bei der Ablage neuer Dokumente werden Sie aufgefordert einen entsprechenden Namen und sowie Belegart und Gruppierung anzugeben. Zusätzlich muss die Sicherheitsstufe entsprechend gewählt werden.
<a name="Belegart"></a>
### Belegart
Belegart und Gruppierung können unter System, Dokumente, obere Modulreiter Belegart bzw. Gruppen definiert werden.

Beim Einlesen neuer Dokumente über den Neu-Knopf kann der verwendete Pfad unter System, Arbeitsplatzparameter, DOKUMENTE_EINLESE_PFAD vorbesetzt werden. Dies bewirkt:
- a.) dass auf diesem Arbeitsplatz immer dieser Pfad bei Klick auf Neu vorgeschlagen wird.
- b.) dass nach dem Einlesen das gewählte Dokument gelöscht wird. Ist der Arbeitsplatzparameter nicht gesetzt wird auch die Datei noch dem Einlesen **nicht** gelöscht. Wird bei vorbesetztem Pfad das Verzeichnis manuell gewechselt, so wird nach dem Einlesen trotzdem die Datei gelöscht.
Diese Funktionalität ist vor allem für das direkte Einlesen von Scannern mit Netzwerklaufwerken gedacht. Achten Sie auch hier darauf, dass die Pfad mit den vorwärts Schrägstrichen angegeben werden müssen, z.B. //Server1/Scan.

Mit dem Parameter DOKUMENTE_MAXIMALE_GROESSE können Sie die maximale Dateigröße von Dokumenten die hinterlegt werden definieren. Geben Sie hier die Größe in KB ein.

## Dokumente per Drag and Drop in die Dokumentenablage ziehen
Die Drag and Drop Funktion der Dokumentenablage ermöglicht das schnelle Hinzufügen von Dateien zu den Belegen in **Kieselstein ERP**. Dazu markieren Sie zum Beispiel in der Auswahlliste den Auftrag zu dem eine Datei hinzugefügt werden soll. Nun klicken Sie auf das Dokumentenablage-Icon ![](Doku_Ablage5.gif), der untere Bereich des Dokumentenablagefensters ist nun dunkelgrau hinterlegt.

Wechseln Sie zurück auf die Datei, die Sie einfügen wollen und ziehen diese in den grauen Bereich.<br>
![](dokumenteneigenschaften_festlegen.PNG)<br>
Nun erscheint ein Dialog um die Eigenschaften der eingefügten Datei zu definieren, füllen Sie die gewünschten Felder aus und übernehmen Sie diese.<br>
Somit ist die Datei, mit dem Auftrag verknüpft, in die Dokumentenablage von **Kieselstein ERP** eingebunden.<br>
Emails werden speziell behandelt. Diese erhalten den Betreff als Dateinamen, somit muss ein E-Mail, das Sie per Drag and Drop einfügen wollen einen Betreff haben. Anhänge von E-Mails werden ebenso in die Dokumentenablage als eigene Dateien eingebunden.<br>
Der Icon der Dokumentenablage in der Auswahlliste wird grün wenn Files hinterlegt sind - so können Sie schnell erkennen, wenn bei Belegen zusätzliche Dateien hinterlegt sind.
Bitte beachten Sie, dass Dokumente aus der Dokumentenablage nicht gelöscht, sondern nur versteckt werden können.<br>
Sobald Sie Schlagworte oder Ähnliches bei der eingefügten Datei (oder beim E-Mail) ändern, wird eine neue Version der Datei in der Dokumentenablage erzeugt.<br>

{{% alert title="Adobe Reader installieren" color="warning" %}}
Laut Info von Anwendern, muss unter Windows der Acrobat Reader als Standard App für die PDF eingerichtet sein, damit diese direkt aus Outlook, genauer aus dem Anhang zu einer Eingangs-EMail übernommen werden können.<br>
Ist dies nicht der Fall, muss der Anhang ins Filesystem (z.B. Desktop) zwischengespeichert werden und von da übernommen werden.
{{% /alert %}}

#### Können auch EMails aus Office 365 übernommen werden?
Ja. Bitten nutzen Sie dafür die Programmversion (und nicht die Browserversion)
Beachten Sie bitte auch, dass aktuell nur die Version 2011 unterstützt wird. Für ältere Versionen nehmen Sie bitte mit Ihrem **Kieselstein ERP** Betreuer Kontakt auf.
Die Version finden Sie unter Start, Office Konto und dann Info zu Outlook
![](Outlook.gif)

#### Welcher Partner soll einem Dokument zugeordnet werden?
In der Regel der "richtige" Partner. Also derjenige Kunde/Lieferant für den das Dokument relevant ist. Sind es allgemeine Unterlagen, die Sie z.B. selbst erstellt haben, so tragen Sie bitte Ihr eigenes Unternehmen als Partner ein.

## Rechte:
Um auch eventuell vertrauliche Dokumente in die Dokumentenablage aufnehmen zu können haben wir die Sicherheitsstufen eingeführt. Damit ein Mitarbeiter ein Dokument der jeweiligen Sicherheitsstufe sehen kann, muss er ein Recht für diese Sicherheitsstufe besitzen. Siehe dazu auch Rollenrechte, DOKUMENTE_SICHERHEITSSTUFE_..

**Hinweis:**

Die Dokumente werden in einer eigenen Datenbank gesichert, welche in Ihr Sicherungskonzept zusätzlich mit aufgenommen werden muss.

**Hinweis:**

Neu und ändern, sowie die Übersicht über alle angelegten Dokumente steht nur mit dem Modul Dokumentenablage zur Verfügung. Ohne diesem Modul können die automatisch durch das drucken abgelegten Dokumente nur über die Auswahlliste einzeln dargestellt werden.

#### Kann ein beliebiges internes Dokument automatisch archiviert werden?
Ja. Es sind alle nicht zu archivierenden Dokumente in der Tabelle LP_DOKUMENTNICHTARCHIVIERT angeführt. Sollte nun ein Dokument archiviert werden, so entfernen Sie einfach den Reportnamen z.B. anf_anfrage.jasper aus der Liste.

Genauso umgekehrt, sollte ein Dokument nicht mehr archiviert werden, so fügen Sie bitte einfach den vollständigen Namen des Reports (inkl. .jasper) in der richtigen Groß-/Kleinschreibung in die Tabelle ein.

Hinweis:

![](Dokumentenablage_kein_Archivierungspfad.png)

Erscheint obiger Hinweis nach einem Druck, so bedeutet dies, dass für ein Dokument kein (programmtechnischer) Archivierungspfad hinterlegt ist. Entweder wird die Archivierung abgeschaltet, oder es muss ein Archivierungspfad dafür gefunden werden. Für eine automatische Archivierung wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.<br>
Wenn die Archivierung für Sie nicht relevant ist, z.B. bei neuen Report-Varianten, wählen Sie Archivierung deaktivieren.

#### Die Dokumente Datenbank wächst sehr schnell.
Vermutlich sind die Logos der einzelnen Drucke sehr groß. [Siehe dazu]
( {{<relref "/docs/stammdaten/system/druckformulare/#gr%c3%b6%c3%9fe-des-logos" >}} )

#### Welche Scanner sollten verwendet werden?
Aus der obigen Beschreibung ergibt sich, dass grundsätzlich zwei Typen von Scannern unterstützt werden:
- a.) Scanner die lokal am Arbeitsplatz über die Twain-Schnittstelle angeschlossen sind.
- b.) Scanner die direkt eine PDF Datei erzeugen und diese in einem Netzwerklaufwerk zur Verfügung stellen.

Die weiteren Ausprägungen der Scanner wie Flachbettscanner, Scann mehrere Seiten in ein Dokument usw. hängt vom Typ und Aufkommen Ihrer einzuscannenden Dokumente ab. Grundsätzlich erscheint uns ein Scanner der mehrere Seiten eines Dokumentes auf einmal einziehen kann und diese auf einem Netzwerklaufwerk zur Verfügung stellt als das praktischste Gerät.
Siehe dazu auch [Dokumentenimporter](#dokumente-importieren)

#### Wie erhält man einen Überblick über alle Dokumente eines Partners?
<a name="Partnerdokumente"></a>
Im Modul Partner finden Sie über die Dokumentenablage einen schnellen Überblick über alle Dokumente, die diesem Partner zugeordnet sind. Gehen Sie dazu wie folgt vor: Wechseln Sie in das Modul Partner. Markieren Sie den gewünschten Datensatz und klicken auf das Icon Dokumentenablage ![](Doku_Ablage5.gif). 
![](partner_dokumentenablage.PNG)
Nun sehen Sie alle Belegarten aufgelistet und darin die Dokumente die zu den Belegen gehören mit deren Versionen.

#### Sehen nun alle Benutzer alle Dokumente?
Nein. Die Sichtbarkeit der Dokumente ist über zwei Arten von Rechte gesteuert, wobei hier noch das Recht für das Modul System hinzukommt.<br>
D.h.: Um die Dokumentenablage des jeweiligen Moduls aufrufen zu können, muss zumindest ein Leserecht für das jeweilige Modul gegeben sein.<br>
Als allgemeines Modul kommt hier noch das Modul System hinzu. D.h. wenn der Anwender auf das Modul zumindest ein lesendes Recht hat (LP_SYSTEM_R) so kann er von dort aus den unteren Modulreiter Dokumentenablage aufrufen und sieht den gesamten Modulbaum von dem aus Dokumente abgelegt wurden.<br>
D.h. wenn ein Modul gestartet werden kann, sieht der Anwender zumindest den Dokumentenbaum.
Damit er/sie Einsicht in das Dokument bekommt muss für das Modul ein lesendes Recht gegeben sein und es muss das Recht für die Dokumentensicherheitsstufe des Dokumentes gegeben sein.
DOKUMENTE_SICHERHEITSSTUFE_0_CU, 1, 2, 3 und 99 für die automatisch erzeugten Dokumente.
[Siehe dazu auch]({{<relref "/management/finanzbuchhaltung/integriert/#sieht-jeder-die-erfolgsrechnung-meinen-gewinn-">}} ).

#### Können Dokumente auch in einem Verzeichnis abgelegt werden?
Die Dokumente, die in der Dokumentenablage gespeichert werden können zusätzlich in einem Verzeichnis abgelegt werden. Stellen Sie dazu den Parameter ZUSAETZLICHER_DOKUMENTENSPEICHERPFAD entsprechend ein. Sie können hier ein Verzeichnis im Dateisystem verwenden. Bitte achten Sie darauf, dass dieses Verzeichnis nur für entsprechende Personen zugänglich / freigegeben ist. Es werden hier ebenso vertrauliche Dokumente abgelegt (je nach Installation zum Beispiel UVA, Rechnungen, etc.).

#### Kann die Sicherheitsstufe vorbesetzt werden?
Wenn einem Benutzer durch seine Systemrolle nur reduzierte Rechte in den Sicherheitsstufen gegeben ist, so wird beim Vorbesetzen neuer Dokumente immer die höchste erlaubte Sicherheitsstufe des Benutzers verwendet.<br>
**Hinweis:** Bei der Übertragung von Bilder durch die RestAPI wird immer die niedrigste Stufe verwendet.

#### Ändern der Version
Wenn die Version oder der versteckt-Status eines Dokumentes geändert wird, so erscheint bei einigen Anwendern der Meldung Pflichtfelder ausfüllen.<br>
![](Pflichtfelder_ausfuellen.png)<br>
Die Ursache dafür sieht man, wenn ![](Details_anzeigen.png)  Details des Dokumentes anzeigen angehakt ist. Hier sieht man unter Anleger, dass dieser leer ist ![](Anleger_leer.png)<br>
Der Hintergrund ist, dass hier das Kurzzeichen der Person, die dieses Dokument angelegt hat angezeigt wird. Ist das Kurzzeichen im Personalstamm leer, ![](Kurzzeichen_leer.png)  so kann auch in der Dokumentenablage keines angezeigt werden, was die Fehlermeldung auslöst. [Siehe dazu auch]( {{<relref "/docs/stammdaten/benutzer/#wieso-wird-beim-ausdruck-angezeigt" >}} )

## Dokumentenpflicht
<a name="Dokumentenpflicht"></a>
In **Kieselstein ERP** steht Ihnen mit der Artikeleigenschaft Dokumentenpflicht, siehe Artikel, Sonstiges ![](Dokumentenpflicht1.gif), eine sehr praktische Funktion zur sicheren Erfassung Ihrer Dokumentenpflichtigen Artikel zur Verfügung.

Der grundsätzliche Gedankengang ist der, dass Sie für Ware welche z.B. mit einem Werksprüfzeugnis, einem Herstellungszertifikat oder Ähnlichem versehen werden muss, möglichst automatisch die Dokumente von Ihrem Lieferanten zu Ihrem Kunden wandern.

Für diese Funktion ist die Dokumentenablage Voraussetzung.

Wird ein Artikel als Dokumentenpflichtig gekennzeichnet, so bewirkt dies, dass bevor eine Wareneingangsposition vollständig erledigt werden kann, auf jeder Wareneingangsposition eines Dokumentenpflichtigen Artikels zumindest ein Dokument hinterlegt sein muss. Wurde kein Dokument hinterlegt, so kann die Wareneingangsposition nicht erledigt werden und somit auch die Bestellung nicht erledigt werden.

Mit der Dokumentenpflicht gekoppelt ist der Versand der Dokumente an die Kunden.

D.h. wird ein Lieferschein mit Dokumentenpflichtigen Artikeln erstellt, so kann beim EMail-Versand des Lieferscheines angegeben werden, ob ![](Dokumentenpflicht2.gif) die Dokumente der Dokumentenpflichtigen Artikel mit an das EMail (an Ihren Kunden) als Anhang mitgesandt werden sollen.

Es werden für jeden Wareneingang immer alle Dokumente, von jedem Dokument jedoch nur die jüngste Version als Anhang dazugefügt. Besteht eine Position aus mehreren Wareneingängen, so gilt dies für jeden Wareneingang.

Die Verkettung zwischen Lieferscheinposition und Wareneingang ist im Warenbewegungsjournal des jeweiligen Artikels ersichtlich. Sollte diese auch auf den Lieferscheinen mit angedruckt werden, so kann diese Information auch im Klartext im Lieferschein angedruckt werden.

Ob Dokumente anhängen per default aktiviert oder deaktiviert ist, kann unter System, Parameter, DEFAULT_DOKUMENTE_ANHAENGEN eingestellt werden.

#### Wie werden die Dokumente bei den Wareneingangspositionen hinterlegt?
Wählen Sie die entsprechenden Wareneingangsposition und klicken Sie auf die Dokumentenablage ![](Dokumentenpflicht3.gif).

![](Dokumentenpflicht4.gif)

In der Dokumentenablage stehen Sie nun auf der entsprechenden Wareneingangsposition. Fügen Sie nun mit Neu ![](Dokumentenpflicht5.gif) oder Scannen ![](Dokumentenpflicht6.gif) ein neues Dokument hinzu.

Geben Sie dem Dokument einen für Sie sprechenden Namen und fügen Sie ein oder mehrere Schlagworte (Stichworte) hinzu.

Speichern Sie nun das Dokument ab.

Sollten Sie eine korrigierte Version für diese Wareneingangsposition von Ihrem Lieferanten erhalten, so wählen Sie das oben beschriebene Dokument und klicken auf ändern ![](Dokumentenpflicht7.gif) um eine neue Version

des gleichen Dokumentes zu erzeugen

Wichtig: Erhalten Sie ein zusätzliches Dokument, z.B. neben der Chargeninfo noch ein Prüfzeugnis, so speichern Sie dies bitte als neues Dokument ab.

Sind bei einer Wareneingangsposition zwei oder mehrere Dokumente hinterlegt, so werden bei einem Dokumentenpflichtigen Artikel alle Dokumente als Anhang im EMail-Versand des Lieferscheines übergeben.

#### Wo werden die Dokumente der dokumentenpflichtigen Artikel  ausgegeben?
Derzeit werden nur beim EMail-Versand des Lieferscheines die Dokumente als Anhang übergeben. Es muss dazu bei Dokumente anhängen ![](Dokumente_anhaengen.gif) der Haken gesetzt sein. Die Standardeinstellung kann mit dem Parameter DEFAULT_DOKUMENTE_ANHAENGEN eingestellt werden.

#### Welche Dokumente werden bei der Dokumentenpflicht ausgegeben?
Es werden immer alle Dokumente eines Wareneingangs ausgegeben. Von jedem Dokument jedoch die jüngste / neueste Version.
| Situation | Beschreibung |
| --- |  --- |
| Wareneingangsposition mit einem Dokument | ![](Dokumentenpflicht8.gif) |
| Wareneingangsposition mit zwei Dokumenten | ![](Dokumentenpflicht9.gif) |
| Wareneingangsposition mit zwei Dokumenten. Vom zweiten Dokument wird nur die jüngere Version (Nr. 1) am Lieferschein mitgedruckt. | ![](Dokumentenpflicht10.gif) |

#### Es werden keine Dokumente angehängt.
Wie oben ausgeführt werden nur bei Dokumentenpflichtigen Artikeln Dokumente der Wareneingangspositionen angehängt. Wenn Sie der Meinung sind, dass Sie alle Dokumente richtig angefügt haben, so prüfen Sie die Kette bitte wie folgt:

-   Gehen Sie bei dem betroffenen Lieferschein in die Lieferschein Positionen

-   positionieren Sie den Cursor auf einen Dokumentenpflichtigen Artikel und wechseln Sie mit dem GoTo Button ![](Dokumente_Artikel_GoTo.gif) in den Artikelstamm.

-   Im Artikelstamm wählen Sie Info (aus dem Menü), Warenbewegungsjournal.

-   Im Warenbewegungsjournal sehen Sie in der rechten Spalte die Abgangsbuchungen. Suchen Sie hier den betroffenen Lieferschein. In der rechten Spalte sehen Sie den zugehörigen Warenzugang. Hinter diesem Zugang müssen die Dokumente hinterlegt sein.

-   Wenn der Warenzugang eine Bestellung ist, so wechseln Sie bitte in die Bestellung, suchen dort den betroffenen Wareneingang und wechseln in die Wareneingangspositionen. Wählen Sie hier die Position mit dem Artikel und klicken auf die Dokumentenablage ![](Dokumentenablage.gif).

-   Hier müssen Dokumente hinterlegt sein.

## Dokumentenlink
<a name="Dokumentenlink"></a>
In vielen Anwendungen ist es sehr praktisch in Ihrer EDV-Fileserver Struktur auch Dokumente abzulegen und die Pfade dazu von **Kieselstein ERP** verwalten zu lassen. Dazu wurde der sogenannte Dokumentenlink geschaffen.

Derzeit steht der Dokumentenlink für die Module 
- Warenwirtschaft
  - Artikel
  - Stückliste
- Einkauf
  - Anfrage
  - Bestellung
  - Eingangsrechnung
  - Zusatzkosten
  - Lieferanten
- Fertigung
  - Los
  - Reklamation
- Verkauf
  - Projekt
  - Angebot
  - Einkaufsangebotsstückliste
  - Auftrag
  - Kunde
- Allgemein
  - Partner
zur Verfügung. Weitere Module können durch **Kieselstein ERP** einfach hinzugefügt werden.

Der Dokumentenlink gliedert sich in zwei Teile.

a.) Definition

Diese finden Sie im Modul System, unterer Modulreiter Mandant, oberer Modulreiter Dokumentenlink.

![](Dokumentenlink_1.gif)

Die Grundidee ist, dass sich je Modul und Beleg/Artikel Pfade für das jeweilige Objekt definieren lassen.

Der von **Kieselstein ERP** erzeugte Pfad wird aus Basispfad+Objekt+Ordner(pfad) zusammengesetzt und der jeweiligen Belegart unter dem Menütext zugeordnet.

Achten Sie bitte bei der Definition der Pfade / Ordner darauf dass

- für Windows Zielsysteme der rückwärts Schrägstrich und für alle anderen System der vorwärts Schrägstrichbzw. bei einer URL _ verwendet wird 

- Verwenden Sie bitte die UNC Notierung, so wie oben dargestellt.

- von **Kieselstein ERP** keine automatischen Schrägstriche hinzugefügt werden. D.h. wenn nach dem Objekt weitere Unterpfade=Ordner definiert werden sollten, so muss unter Ordner z.B. /Unterordner/ angegeben werden.

In den Projekten wird der Bereich der Projekte für den Dokumentenlink mit angeben.

Vermeiden Sie Umlaute in den Pfadangaben. Insbesondere bei gemischten Betriebssystemen

b.) Verlinkung

Nach dieser Definition starten Sie das jeweilige Modul neu. Sie finden nun im Menü unter dem jeweiligen Modulnamen den Eintrag Dokumentenlink mit den von Ihnen definierten Menütexten.

![](Dokumentenlink_2.gif)

Wählen Sie nun den gewünschten Dokumentenlink aus und es wird der Pfad wie oben beschrieben angelegt, wenn er noch nicht vorhanden ist, und es wird der Dateibetrachter Ihres Betriebssystems (für Windows Benutzer der Explorer, für MAC Benutzer der Finder) geöffnet. Der Dateibetrachter steht genau in diesem Pfad und Sie können z.B. per Drag and Drop komfortabel Dateien aus anderen Systemen in diese Struktur einfügen.

Bitte beachten Sie, dass bei Änderungen Ihrer Filestruktur auch diese Verlinkung mitgezogen werden muss.

Die **Berechtigungen** für den Zugriff auf diese Dateien **sind** außerhalb von **Kieselstein ERP** **in Ihrem Netzwerkbetriebssystem abzubilden**.

Der Dokumentenlink kann auch eine URL sein. Wählen Sie dazu die  Option 'Weblink' in der Definition der Dokumentenlinks. Wenn man auf  "Dokumentenlink" klickt, öffnet sich der Browser mit der entsprechenden (internen oder externen) Seite. Somit kann hiermit zum Beispiel ins Firmeninterne Wiki usw. verlinkt werden, was hilfreich ist für weitere Dokumentationen und Zusatzinformationen.

#### Definition von absoluten Pfaden im Dokumentenlink
Ergänzend zum Dokumentenlink können im Artikel auch absolute Pfade je Artikel hinterlegt werden. Dafür definieren Sie im System wie unter [Dokumentenlink](#Dokumentenlink) beschrieben die Belegart und den Menütext. Geben Sie nun zusätzlich an, Pfad absolut ![](Externe_Dokumente.gif). Bitte beachten Sie, dass für den Vorschlagswert des zu hinterlegenden Verzeichnisses der Basispfad verwendet wird.<br>
Nach dem Neustart des Artikelmoduls finden Sie nun einen neuen Reiter ![](Externe_Dokumente1.gif) Externe Dokumente.<br>
Durch Klick auf ändern ![](Externe_Dokumente2.gif) kann der Pfad für diesen Artikel durch Klick auf Auswahlknopf ![](Externe_Dokumente3.gif) der mit Ihrer Menübezeichnung beschriftet ist definiert werden. Es wird dazu der Dateibetrachter Ihres Betriebssystems gestartet, damit die Definition einfach erfolgen kann. Wenn nun die in dem definierten Pfad hinterlegten Dokumente angezeigt / aufgerufen werden sollten, so klicken Sie bitte einfach auf den anzeigen Button ![](Externe_Dokumente4.gif). Damit wird der Dateibetrachter direkt mit dem definierten Verzeichnis gestartet.

Für die Definition des Dokumentenlinks muss der Basispfad definiert werden. Geben Sie hier bitte einen Dummypfad an.

#### Alternativer Dateibetrachter für den Dokumentenlink
Für manche Funktionen, z.B. EPDM-Systeme, reicht der Aufruf des allgemeinen Dateibetrachters nicht aus. Wir haben daher noch die zusätzliche Funktion eines zusätzlichen vorgeschalteten Programms für den Aufruf des Dokumentenlinks geschaffen.
Dafür sind zwei Punkte einzustellen / durchzuführen:
- a.) Haken Sie beim gewünschten Dokumentenlink ![](Externe_Dokumenten5.gif) Pfad aus Arbeitsplatzparameter an.
- b.) Definieren Sie im System, unterer Modulreiter Arbeitsplatzparameter welches Programm beim Aufruf des Dokumentenlinks gemeinsam mit dem Link gestartet werden sollte. Für Windows-User mit standard Installation könnte dies z.B. wie folgt aussehen:
![](Externe_Dokumenten6.gif)

Diese Funktion steht sowohl für die absoluten als auch die errechneten Pfade zur Verfügung.

#### Projekttitel im Dokumentenlink
Insbesondere für die Projektabwicklung ist es oft recht praktisch, dass nicht nur die Projektnummer im Dokumentenlink sondern auch der Titel des Projektes mit enthalten ist. Daher können Sie bei der Belegart Projekt auch ![](Dokumentenlink_3.gif) Mit Titel anhaken. Dies bewirkt, dass zusätzlich zur Projektnummer auch der Titel des Projektes mit in den Ordnernamen aufgenommen wird. Es ist dies so programmiert, dass eine eventuelle Änderung des Titels sich NICHT auf den Ordnernamen auswirkt. Es wird jedoch trotzdem anhand der Projektnummer der richtige Ordner aufgefunden.

#### Können auch Rechte für den Dokumentenlink vergeben werden?
Ja. D.h. per default werden die Dokumentenlinks für alle Benutzer freigeschaltet. Wird jedoch die Combobox Recht mit einem der Dokumentensicherheitsstufen versehen, so können nur Benutzer mit Rollen die diese Sicherheitsstufen haben auf diesen Dokumentenlink aus **Kieselstein ERP** heraus zugreifen.
Bitte beachten Sie, dass dies KEINE wie immer geartete Rechteverwaltung Ihres Filesystems ist. Mit dieser Einstellung wird lediglich die Anzeige des Menüpunktes gesteuert / abgeschaltet. Der tatsächliche Zugriff muss im Filesystem eingerichtet werden. Der Vorteil ist jedoch, dass gegebenenfalls kein Hinweis darauf erfolgt, dass es da überhaupt noch mehr Daten geben könnte.---

#### Dokumente importieren
Um komfortabel Dokumente importieren zu können gibt es den Microservice für den Dokumente Import.

Dieser ist grundsätzlich so aufgebaut, dass der Dateiname die Beleg-Kennung und die Belegnummer enthält, damit der Importer weiß zu welchem Beleg die jeweiligen Daten abgelegt werden müssen.
