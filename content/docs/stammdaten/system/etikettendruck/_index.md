---
categories: ["Etikettendruck"]
tags: ["Etikettendruck"]
title: "Etikettendruck"
linkTitle: "Etikettendruck"
date: 2023-01-31
weight: 030
description: >
  Besonderheiten des Druckens von Etiketten
---
Etikettendruck
==============

Besonderheiten des Etikettendruckes.

Da es beim Drucken, Formatieren und Ansteuern der Etikettendrucker viele Dinge zu beachten gibt, hier eine Sammlung von zu beachtenden Punkten:

Das Formular, die Reportvorlage muss zum Etikett passen. D.h. sowohl die Abmessungen des Reports als auch die Abmessung der Etikette müssen übereinstimmen. Zusätzlich, muss die Abmessung der Etikette dem Drucker mitgeteilt / beim Drucker hinterlegt werden.

Formularvorlage

Achten Sie unbedingt darauf, dass auch Ihr Etikettendruck weiß, welche Etiketten eingelegt sind. Durch diese Einstellung wird auch die zu bedruckende Fläche für den Druckertreiber gesetzt. Wird eine falsche Etikette, also eine falsche Abmessung der Papierabmessungen im Drucker vorgenommen, so erscheint der Druck versetzt (horizontal oder vertikal) oder Ähnliches.

**Einstellungen für den Zebra TLP2844-Z**

![](Einstellung_Zebra_TLP2844Z.gif)

Diese finden Sie unter Eigenschaften, Druckeinstellungen (in Windows-Systemen)

Achten Sie darauf, dass diese Einstellungen für jeden Arbeitsplatz vorgenommen werden müssen.

Reportvorlage

Der Inhalt der Etiketten wird üblicherweise im Dokumentenbereich Title definiert. D.h. die Größe der zu bedruckenden Etikette wird hier in Punkten angegeben. Ein Punkt ist ca. 0,353 mm oder 1/72" (Zoll bzw. Inches).

Beispiel: Einrichten des Etikettendruckes für einen Zebra TLP 2844-Z

1.  Installieren des richtigen Druckertreibers. Hier hat sich für Windows-Systeme der Treiber von seagull bewährt.

2.  Einstellen des Etikettes 62x36mm -> siehe auch Formulardefinition unter Windows 2000 und höher
    Einstellung unter Drucker, Eigenschaften, Druckereinstellungen, Seite einrichten.

3.  Stellen Sie nun mittels Testdruck sicher, dass der Drucker richtig druckt. D.h. mit dem Seagull-Treiber wird TEST mit einem entsprechenden Rahmen herum in die linke obere Ecke der Etikette gedruckt.

4.  Richten Sie nun den entsprechenden Report ein. In unserem Beispiel: ww_artikeletikett.jrxml<br>
    **ACHTUNG:** Bitte ausschließlich Reports im Bereich Anwender verändern!

5.  Nach dem Öffnen des Reports gehen Sie auf Ansicht, Reporteigenschaften.
    Definieren Sie hier Abmessungen, Ausrichtungen und Ränder.<br>
    **WICHTIG:** Es hat sich bewährt, den Drucker immer im Porträt bzw. Hochformat zu definieren, auch wenn ein Etikett verwendet wird, welches Breiter als Hoch ist. Um dies für die Reportvorlage richtig definieren zu können, müssen Sie die Höhe etwas höher als die Breite angeben. Die eigentliche Höhe des Etikettes definieren Sie dann mit der Höhe des Druckbereiches im jeweiligen Druck-Band (Detail, bzw. Title).<br>
    **Trick:** Sollte es gewünscht sein, dass auch im Druckdialog / in der Vorschau die richtige Etikettenhöhe angezeigt wird, so muss die zugehörige Datei *.jrxml mit einen Texteditor bearbeitet werden. Ändern Sie hier den Wert für die Höhe auf die entsprechenden DeziDots ab (pageHeight="102"). Danach muss der Report erneut kompiliert werden. Bedenken Sie dabei, dass die Ränder des Reports ebenfalls zu berücksichtigen sind. D.h. falls Sie eine Fehlermeldung (Report passt nicht mehr in die Definition) erhalten, muss die Höhe des Details (Band) reduziert werden.

6.  Eventuell sollte  bzw. kann hier auch die Ausrichtung des Etikettes entsprechend angepasst werden.
    Da iReport die Ausrichtung automatisch umschaltet, dies aber nicht immer gewünscht / die richtige Ausrichtung ist, können Sie dies in der zugehörigen *.jrxml Datei unter orientation="Portrait" oder Landscape einstellen. Auch nach dieser Änderung muss der Report neu kompiliert werden.

7.  Definieren Sie nun die Höhe des Druckbereiches. (Document structure, detail, Rechtsklick, Band properties)
    - Etikettenbreite = 62mm = 175DeziDot
    - Etikettenhöhe  = 36mm = 102DeziDot
    **Hinweis:** Wenn die Abmessungen des Reports verändert werden, werden eventuell die Höhen der Detail (Band) auf die maximale Reporthöhe reduziert.

8.  Gestalten Sie nun das Etikett wie gewünscht.
    - Hinweis: Sie sollten nur Schriften ab Schriftgröße 6Punkt oder höher verwenden.
    - Hinweis: Der Platzbedarf der Höhe des Textfeldes ist bei 6Punkt + 1 Punkt, ab 8Punkt +2Punkte<br>
    Wird der Platzbedarf geringer angegeben, so sehen Sie zwar im iReport Ihre Eingaben, es wird jedoch nichts gedruckt.

9.  Beim Einsatz von Zebra-Etikettendruckern hat sich ein Rand von 3mm (in den Reporteigenschaften) bewährt.

Formulardefinition unter Windows 2000

Für spezielle Formulare, hier sind benutzerspezifische Papierformate gemeint, müssen ab Windows2000 und höher spezielle Formulare im Druckserver eingerichtet werden.
Dies ist insbesondere bei nicht ausdrücklichen Etikettendruckern der Fall.

1.  Melden Sie sich als Administrator auf dem jeweiligen Client PC an. Es gibt leider einen Unterschied zwischen einem Benutzer mit Administrator Rechten und dem Administrator, weshalb Sie sich unbedingt als Administrator anmelden müssen.

2.  Definieren Sie die gewünschte Etikettengröße im Druckserver. Bei der Angabe der Formularabmessungen für W2k müssen diese in cm angegeben werden. Klicken Sie nach der Definition bitte unbedingt auf Formular speichern.<br>
   **WICHTIG:** Die Formulareigenschaften müssen zum Drucker passen. D.h. Formulare die laut Windows nicht in den Drucker passen, können zwar definiert werden, können aber nie dem Drucker zugeordnet werden.

3.  Ordnen Sie dieses Formular dem Drucker zu.
    Eigenschaften / bzw. Druckeinstellungen, Erweitert und dann die Papiergröße (ist je nach Druckertreiber unterschiedlich bezeichnet) entsprechend dem oben definierten Formular einstellen.<br>
    **WICHTIG:** Damit der Drucker(server) das neue Formular anzeigen kann, müssen Sie nach der Formulardefinition den Druckerordner schließen und danach neu öffnen. Erst danach funktioniert die Zuordnung der Formulare.

**Hinweis:**

Die Formulare sind eine Benutzereigenschaft. D.h. unter Umständen müssen diese pro Benutzer dem jeweiligen Drucker des Benutzers ebenfalls zugeordnet werden.

Den Druckserver erreichen Sie unter Start, Einstellungen, Drucker und nun Datei, Servereigenschaften. Üblicherweise ist der Karteireiter Formulare vorbesetzt.

Tipps zur Behandlung von Etikettendruckern

Nachfolgend eine lose Sammlung von Tipps für die Behandlung von Etikettendruckern, vor allem um die Druckqualität konstant zu halten.

- Legen Sie Papier und Thermotransferbänder **immer** so wie in den Druckern, in den Druckerhandbüchern beschrieben ein.

- Achten Sie darauf, dass die Führung der Papierrollen knapp und gut sitzen (dicht bei der Rolle sind). Diese dürfen nicht klemmen, d.h. das Papier muss leichtgängig gleiten können.

- Für gute Druckergebnisse müssen Papier und Thermotransferbänder gut geführt sein!

- Achten Sie darauf, dass Papier und Thermotransferfolien (TTF-Band) durch die Sensoren gleiten.
    - Das bedeutet bei den Druckern der Serien Z4M bzw. S4M Papier und TTF-Band müssen diese am linken Rand, von vorne gesehen, sein.
    - Bei den Druckern der TLP2844 Serie müssen diese mittig ausgerichtet sein.

- Achte auf eine sehr saubere Aufwicklung der Folien. Es dürfen auf keinen Fall Falten geworfen werden. Gegebenenfalls entferne die bisher aufgewickelte verbrauchte Folie.
- Führe die Sensor Kalibrierung durch. Dies kann bei allen Druckern über das Menü durchgeführt werden. Für die genaue Vorgehensweise siehe bitte jeweiliges Drucker-Handbuch.
- Dass das Etikettenmaterial und die WAX / TTF (ThermoTransFer) Bänder zueinander passen müssen, sollte selbstverständlich sein.

## Etikette wird nur teilweise gedruckt?
Wenn dein Etikettendruck z.B. so aussieht<br>
![](Etikette_fehlerhaft.png)  
<br>dass um unteren Rand einfach Teile von Zeilen fehlen, so kommen in der Regel folgende Ursachen in Frage:
- Voraussetzung, das Etikett ist in deinem Kieselstein-ERP in der Vorschau vollständig
- es wird ein falscher Druckertreiber verwendet. Meine Erfahrung: Es ist besser die Druckertreiber von Seagull Sientific zu verwenden. Denn die Original Druckertreiber (Z-Designer) von Zebra machen gerne Probleme, das gilt analog auch für die CAB Drucker und deren original Druckertreiber.
- die Abmessungen der Etikette müssen auf jeden Arbeitsplatz für jeden Benutzer eingerichtet werden. Diese sollten in der Regel um ein Pixel / einen mm größer sein, als dein Etikett im Reportgenerator definiert ist.

## Orientierung des Etikettendruckes
Auch das ein eigenes Kapitel.

Üblicherweise denkt man sich, dass die Ausrichtung des Etikettes so wie es aus dem Drucker herauskommt ist. D.h. wenn ein Etikette im Drucker eingelegt ist, so ist die Breite derjenige Wert in dem die Zeilen gedruckt werden. Die Höhe ist die Richtung des Vorschubes.
D.h. wenn du nun z.B. eine A6 Etikette ausdruckst, so ist die Breite 105mm und die Höhe 148,5mm
Die Ausrichtung deiner Etikette ist nun eigentlich Landscape.
Da nun die verschiedensten Treiber und die beteiligten Software-Zeile manchmal nicht das machen was wir wollen, empfiehlt sich:
- Richte die Etikette für den Etikettendrucker so ein wie oben beschrieben
- Erstelle den Report wie gewünscht in der Landscape Ausrichtung
- Mache einen Probeausdruck
- Ist nun die Etikette um 90° gedreht, so stelle die Ausdruckrichtung von Landscape auf Portrait um. Achte dabei darauf, dass die Werte in den Feldern Breite und Höhe in deinem Report **gleich** bleiben.<br>
Lässt sich das in der Oberfläche nicht einstellen, definiere die gewünschte Orientierung im jrxml

Was ich auch schon hatte:
- im einen Betriebssystem geht alles wunderbar, im anderen macht der Drucker quasi was er will, schiebt immer viele Etiketten raus usw. 
- Das scheint anfänglich willkürlich zu sein, hängt aber vom Druckertreiber und vom Betriebssystem usw. ab.
- Die Abhilfe die ich gefunden habe: Stelle direkt im jrxml sicher, dass in der Kopfzeile die Orientierung definiert ist. Es gibt immer wieder Reports, die manchmal nicht gehen. Dann scheint das die Ursache zu sein.
- Das ist manchmal auch in den Subreports erforderlich

Mögliche Orientierungen / Syntax
- orientation="Landscape" 
- orientation="Portrait"

## Lebensdauer des Druckkopfes
Insbesondere wenn qualitativ hochwertige Etiketten bedruckt werden, muss man einen entsprechenden Energie-Eintrag auf das (oft Plastik) Etikettenmaterial machen. Dies bedingt, dass mit einer entsprechend hohen Schwärzung gefahren werden muss. Damit werden wiederum die Druck-Köpfe entsprechend belastet.<br>
Versuchen Sie daher die Schwärzung in Verbindung mit der Geschwindigkeit so weit wie möglich zu reduzieren.