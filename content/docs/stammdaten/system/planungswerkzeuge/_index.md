---
categories: ["Planungswerkzeuge"]
tags: ["Planungswerkzeuge"]
title: "Planung"
linkTitle: "Planungswerkzeuge"
date: 2023-01-31
weight: 230
description: >
  Die was wäre wenn Frage
---
Planungswerkzeuge
=================

Nachfolgend eine lose und unvollständige Sammlung von Planungswerkzeugen in Ihrem **Kieselstein ERP**.

## Materialbedarfsprognose
Eine Auswertung welche Artikel bzw. Artikelgruppen in der Vergangenheit benötigt wurden und bei linearer Fortschreibung vermutlich in Zukunft benötigt werden unter Berücksichtigung von offenen Angeboten, Rahmenaufträgen usw.
siehe => Artikel, Journal, Materialbedarfsvorschau

## einen Jahresbedarf schätzen
Wieviel Material und Mitarbeiter:innen Zeit und Maschinenzeit brauche ich dafür.
Das Ziel dieser Betrachtung ist, ein Gefühl dafür zu bekommen welchen Jahresbedarf ihr für die geplanten Produkte habt. Damit kann man mit seinen Lieferanten (die immer auch Partner sind) entsprechende Rahmenverträge verhandeln. Wichtig ist dabei, auch die Verbindlichkeiten zu definieren. Ich habe von einigen Großkunden durchaus erlebt, dass ein Rahmenvertrag nur eine Absichtserklärung ist. Hier gilt es ein vernünftiges Maß / eine vernünftige Vorgehensweise zu finden. Der geschätzte Jahresbedarf hilft auch seine Planung genauer zu machen.
### Wie kommt man zum Jahresbedarf?
Einfach eine Jahresbedarfsstückliste anlegen und hier die gewünschten Jahres-Mengen der jeweiligen Produktstückliste eintragen.
Dann die Stückliste als Ausgabestückliste mit Menge 1 (die Mengen sind ja in den Positionen enthalten) inkl. aller Unterstücklisten anhaken. Schon sieht man den gesamt Jahresbedarf. Für weitere Planungen dies gerne über den Druck und Zwischenablage rüber in eine Tabellenkalkulation.
### Alternative, Nutzung des Forecast-Moduls
Hier die gewünschten Stückzahlen der Fertigprodukte eintragen, wobei im Forecast auch der gewünschte Liefertermin angegeben werden kann. D.h. hier kann man auch z.B. auf Quartals oder Monatsebene planen. Dann Journal Beschaffung, gegebenenfalls sortiert nach Lieferanten.

## wie schnell kann ich xxx Maschinen liefern
Auch hier eine Bedarfs-Stückliste anlegen und dann im Stücklistenmodul, Info, Minderverfügbarkeit. Hier werden auch die lagernden / in Produktion(splanung) befindlichen Halbfertigfabrikate berücksichtigt.
