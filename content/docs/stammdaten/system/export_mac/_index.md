---
categories: ["Export von Daten"]
tags: ["Datenexport am MAC"]
title: "Datenexport am MAC"
linkTitle: "Datenexport am MAC"
date: 2023-01-31
weight: 215
description: >
  Datenexport mit MAC
---
Export für MAC
==============

Um Daten auch am MAC übernehmen zu können, haben wir für Sie folgende Beschreibung erstellt:

Erstellen einer Exportdatei aus **Kieselstein ERP** um diese am MAC in OpenOffice einzubinden:

Voraussetzungen: OpenOffice 3.1 am MAC OS X 10.5

1.  Wählen Sie die zu übernehmende Auswertung.
    Es wird dies anhand der Hitliste erklärt, kann aber analog auch für alle anderen Auswertungen verwendet werden.

2.  Drucken Sie die Auswertung in die Vorschau

3.  Wählen Sie nun den CSV Export.

4.  ![](Export_MAC1.jpg)
    Speichern Sie die Datei am einfachsten auf den Schreibtisch (Desktop)
    **<u>WICHTIG:</u>**
    Bitte die Erweiterung .txt angeben.

5.  Starten Sie nun OpenOffice, Tabellendokument und wählen Sie Datei, öffnen und geben Sie die oben abgespeicherte Datei an.

6.  ![](Export_MAC2.jpg)
    Stellen Sie den Importfilter wie oben angegeben ein (Er sollte automatisch so vorgeschlagen werden)

7.  Sie erhalten die Hitliste in der Tabellenkalkulation Calc geöffnet.

8.  ![](Export_MAC3.jpg)
    Speichern Sie diese mit Datei speichern unter im gewünschten Format ab.