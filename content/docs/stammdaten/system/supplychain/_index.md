---
categories: ["SupplyChain"]
tags: ["SupplyChain"]
title: "SupplyChain"
linkTitle: "SupplyChain"
date: 2023-01-31
weight: 500
description: >
  Open Factory, wird nicht mehr weiter unterstützt
---
Supply Chain
============

Entlang der Produktionslinie kommt es immer wieder zum Datenaustausch in einem Kunden / Lieferantenverhältnis. Kann dieser Datenaustausch elektronisch erfolgen, so hat dies den Vorteil, dass die Daten von der jeweils empfangenden Stelle nicht manuell erfasst werden müssen. Dies bedingt, unter anderem, Vermeidung von (Tipp-) Fehlern, raschere Datenübergabe (Postweg, versus dem Versand elektronischer Dokumente). Dies bewirkt wiederum raschere genauere Information. Dies kann selbstverständlich wesentlich weiter ausgebaut werden.

Diese Supply Chain (Lieferkette) wird auch durch OpenFactory unterstützt. Mit Openfactory steht eine neutrale Clearingstelle für die Übermittlung der Dokumente entlang der Supply Chain zur Verfügung. **Kieselstein ERP** wird diesen, unter anderem auf Web-Services basierenden Standard voll unterstützen.

Verbindung zwischen den Partnern (Lieferant <-> Kunde)

![](SupplyChain_OpenFactory.gif)

1.  Das ERP-System des Kunden stößt z.B. eine Bestellung an.

2.  Der Konverter überträgt die Bestellung in das myOpenFactory-Format

3.  Der Server empfängt die Nachricht und legt diese im Zwischenpuffer des Lieferanten ab.

4.  Der Zwischenpuffer des Server wird vom Konverter des Lieferanten periodisch auf neue Nachrichten abfragt. Neue Nachrichten werden an das ERP-System weitergereicht.

5.  Die Nachricht wird vom ERP-System des Lieferanten verarbeitet.

6.  Der Konverter erzeugt eine Quittung für die Nachricht (Erfolg oder Fehler).

7.  Die Quittung wird über den Server an das Kunden ERP-System weitergeleitet.

8.  Periodisch werden neue Quittungen vom Konverter des Kunden abfragt. Neue Quittungen werden umgewandelt und an das ERP-System weitergereicht.

9.  Die Quittung wird vom ERP-System des Kunden verarbeitet

Dokumente entlang der Lieferkette:

![](Supply_Chain_Dokumente.gif)