---
categories: ["Web Services"]
tags: ["Magento"]
title: "Magentoconnector Installation"
linkTitle: "Magentoconnector"
date: 2023-01-31
weight: 100
description: >
  Installation des Magentoconnectors
---
Magento Connector zu **Kieselstein ERP**

Beschreibung zur Installation des **Kieselstein ERP** Magento-Connector für Ihren Magento Webshop.

## Voraussetzung
- Magento Defaultinstallation, [Downloadlink](https://www.magentocommerce.com/download "https://www.magentocommerce.com/download")
- (für automatische Jobs zukünftig) Magento cronjob ist eingerichtet und wird laufend gestartet, [Link-Anleitung](http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/how_to_setup_a_cron_job "http://www.magentocommerce.com/wiki/1 - installation and configuration/how to setup a cron job")
- Setzen der richtigen Zeitzone und Magentoadminbereich -> System -> Configuration -> General -> Locale Options (Central Europe Standard Time)
- (empfehlenswert) System und Exceptions Log unter System->Configuration->Advanced->Developer->Log Settings zu aktivieren (logs liegen dann unter /var/www/html/magento/var/logs)
- PHP Installation >= 5.2, PHP Soap Extention ist aktiv

## Installation Modul Kieselstein-ERP-Synchronisation
- Kieselstein-ERP-Synchronization-XXX.tgz herunterladen
- Im Magento->Adminbereich unter System->Magento Connect->Magento Connect Manager starten
- Bestehende Version unter "Manage Existing Extensions" mit Uninstall deinstallieren (wenn vorhanden)
- Neue Version mit "Direct package file upload" installieren
- Es erscheint nun im Magento->Adminbereich unter System->Configuration->General der neue Punkt Kieselstein-ERP Connect
    Gegebenenfalls empfiehlt sich ein Ab und wieder Anmelden des Admin's.
    ![](Connector.jpg)**

##Konfiguration Ihrer **Kieselstein ERP** Installation
- Verbindungseinstellungen zu **Kieselstein ERP** im Modul unter Punkt "Kieselstein-ERP- Connect->Basic Webservice Settings" konfigurieren und Einstellungen speichern
- Prüfen ob eine Syncrone oder Asyncrone Verarbeitung gewünscht ist und dies unter "HeliumV Connect->Scheduler" einstellen (Abhängig von der Datenmenge, > 500 Artikel ist asyncron empfehlenswert)
- Manuelle Syncronisierung aus dem Backend durch drücken der jeweiligen Buttons anstoßen und Beobachtung der Logfiles auf Fehler
- Einrichten von Cronjobs in config.xml (magento/app/code/local/Kieselstein-ERP/Synchronization/etc/config.xml) zum wiederkehrenden Anstoßen der Syncronisierungen, Verfügbare Aufrufe sind unter Cronjob.php zu finden
- Steuerklasse anlegen (Sales / Tax / Taxs Rules /... MWST 20%) - wird vom Kieselstein-ERP-Connector automatisch generiert und muss noch dem im Magento vorhandenen Steuersatz zugeordnet werden
- Kundengruppen default auf gewünschte Preisliste von **Kieselstein ERP** umstellen (Bsp. Endkunde) (System - Costumers - Costumer Config. - Create New Acount Options - Default Group
- E-Mail Adressen und Ansprechpartner richtig pflegen (Store E-Mail Adresses)**
- **Achtung:** Settings Tools for Costumer Sync: hier kann automatisch ein E-Mail an alle neu-importierten Kunden geschickt werden mit dem Hinweis, dass jetzt Webshop und Passwort verfügbar sind -> zu Beginn auf nein stellen, wenn der Sie Ihre Kunden über den Start des Webshops informieren möchten, dann "ja" (bitte planen Sie den Webshop-Launch mit Ihrer Marketingabteilung)

## Konfiguration des Connectors
- country: at
- language: de
- shopname: ihr Shop -> Dies muss mit den Einstellungen Ihrer **Kieselstein ERP** Installation unter Artikel, Grunddaten, Webshop, Bezeichnung des Webshops übereinstimmen.
- user/pass: Siehe Ihr **Kieselstein ERP** Client, Hilfe, **Kieselstein ERP** Hilfe, System, Web_Links, WebShop-Interface
- url: http://192.168.8.244:8080/lpserver-ejb/ArtikelFacBeanRest?WSDL

## Bekannte Fehler
- Kommt beim ersten Aufruf des **Kieselstein ERP** Moduls ein 404 Error, so hilft nochmals ein Logout/Login im Adminbereich zu machen und eventuell auch den Browsercache zu leeren und die Seite erneut zu laden.

## Versionen
