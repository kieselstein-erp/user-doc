---
categories: ["Web Services"]
tags: ["Web Services"]
title: "Web Services"
linkTitle: "Web Services"
date: 2023-01-31
weight: 109
description: >
  Web Services, wie RestAPI, JSon
---
Web-Services
============

Um **Kieselstein ERP** auch mit eigenen "Clients" nutzen zu können, stellen wir eine Anzahl von Webservices zur Verfügung. Diese können z.B. für eigene Zeiterfassungsclients, oder für Webshops oder für Abrechnungssysteme verwendet werden. Sie können direkt auf unsere Web-Services zugreifen oder diese über einen Enterprice Service Bus (ESB, z.B. Mule oder Talend) realisieren.

Wir gehen davon aus, dass Sie mit der grundsätzlichen Erstellung und Verwendung von Web-Services vertraut sind.

Derzeit stehen folgende Typen von Zugriffen zur Verfügung:
a.) RestFul / REST
Für die Dokumentation der zur Verfügung stehenden RestFUL Json APIs siehe:
-   direkt auf Ihrem **Kieselstein ERP** Server: http://KIESELSTEIN-ERP-SERVER:8080/api-doc/

Bitte beachten Sie, dass die Implementierung mit dem Apache CXF Format gemacht wurde. D.h. trotz der Rest-Definition stehen die Daten, je nach Aufruf im JSON oder im SOAP Format zur Verfügung.

## Webshop Verbindung / Anbindung
Die Idee hinter der **Kieselstein ERP** Webshopanbindung ist, dass die wesentlichen / alle Daten, die Sie für Ihren Webshop benötigen über Ihr **Kieselstein ERP** gesteuert werden.
Bitte beachten Sie, dass hier nur der Teil der im **Kieselstein ERP** enthaltenen Funktionen zur Verfügung steht. Die eigentliche Anbindung an einen Webshop wie z.B. Mangento muss erstellt werden.

Aus dieser Aufgabenstellung ergeben sich folgende Bereiche:
-   Artikeldaten
-   Kundendaten
-   Kundenbestellungen, also die Überleitung von der Bestellung Ihres WebShop-Kunden in die Auftragsbestätigung in **Kieselstein ERP**

Die Datenübertragung erfolgt nach der BMEcat® 2005 Spezifikation Version 1.2. Siehe dazu auch <http://www.bme.de> für die Artikelstammdaten, sowie nach openTRANS® Version 2.1 <http://www.iao.fhg.de> für den Austausch der Bestellungs- / Auftragsdaten.

Die Verbindung zwischen Webshop und **Kieselstein ERP** ist so, dass immer der Webshop die Daten abfrägt bzw. nach **Kieselstein ERP** überträgt. Dass dafür eine entsprechend gesicherte Verbindung erforderlich ist, erklärt sich von selbst.

Vom Ablauf her gehen wir davon aus, dass nach der erfolgten Basis Datenabfrage regelmäßig, z.B. täglich um 01:00, die Veränderungsdaten des Artikels und der Kunden abgeholt werden.

Die Bestellungsdaten werden aktuell, also annähernd sofort, nach **Kieselstein ERP** übertragen. Von der Importfunktion werden die Kundenbestellungen (aus dem Shop) in **Kieselstein ERP** als angelegte Aufträge oder gegebenenfalls auch als Angebot(sanforderungen) hinterlegt. Diese Aufträge (bzw. Angebot) müssen von Ihnen überprüft und freigegeben werden.

Der Webshop selbst sollte offline fähig sein, d.h. auch ohne ständiger Verbindung zum **Kieselstein ERP** Server funktionieren und hier eben gegebenenfalls die Daten zwischenspeichern.

Der Ablauf im Webshop entspricht den heutzutage üblichen Abläufen.

Eine wichtige Einschränkung ist derzeit, dass wir davon ausgehen, dass entweder per Nachnahme geliefert wird, oder dass es sich um registrierte und bekannte Kunden handelt, deren Bestellungen durch ein zusätzliches Vertragswerk geregelt / gesichert sind.

Als Beispiel für die Einbindung in einen OpenSource Standard Shop haben wir einen Connector für den [Magento Shop](http://www.magentocommerce.com/) entwickelt. Der Magento Connector kann von **Kieselstein ERP** ?????? herunter geladen werden.

Artikeldaten

Die Definition welche Artikel mit welchen Preisen, Beschreibungen, Bildern in welchem Shop dargestellt werden wird im Artikelmodul vorgenommen. Dazu ist folgender Zusammenhang gegeben:

1.  Webshop
    Definieren Sie Ihre verwendeten Webshops.
    Siehe dazu Artikel, Grunddaten, Webshop
    **Hinweis:**
    Der hier eingetragene Name muss auch vom abfragenden Webshop als Webshopname, in Magento = shopname, in der richtigen Groß/Kleinschreibung verwendet werden.

2.  Shopgruppen
    Definieren Sie im Modul Artikel, unterer Modulreiter Shopgruppe Ihre Shopgruppen, in Magento Kategorien genannt, in denen Ihre Artikel im Webshop erscheinen sollen.
    Die Shopgruppen sind hierarchisch aufgebaut. D.h. für jede Shopgruppe kann:
    - die Vatershopgruppe (also die übergeordnete Shopgruppe)
    - ein Referenzartikel um z.B. weitere Beschreibungen und Bilder für diese Gruppe hinterlegen zu können
    - eine Liste von Webshops, für die diese Shopgruppe gültig ist
    definiert werden. Bitte bedenken Sie bei der Definition der Shopgruppen, das Ihre Webshopkunden den Aufbau der Kategorien/Webshopgruppen ohne viel zu überlegen durchblicken kann. 

3.  Preislisten
    Definieren welche Preisliste für welchen Webshop angewandt werden sollten

4.  Kommentarart
    Auch hier definieren Sie, ob eine bestimmte Kommentarart, mit der dann die Langtexte und die Bilder hinterlegt werden, für die Webshops zur Verfügung stehen.
    D.h. damit können Sie eigene Webshop-Beschreibungen für den jeweiligen Artikel erstellen. Zusätzlich können hier mit  Kommentararten auch SEO-relevante Informationen hinterlegt werden.

5.  Verwendung der Artikel für den jeweiligen Webshop durch die Shopgruppe
    Dazu definieren Sie im Artikeldetail in welche Shopgruppe der Artikel fällt.

Um die vom Anwender in **Kieselstein ERP** gepflegten Daten in den Shop zu übertragen frägt der Shop die Daten bei **Kieselstein ERP** über die Webserviceschnittstelle http://KIESELSTEIN-ERP-Server:8080/lpserver-ejb/ArtikelFacBeanRest?WSDL ab. Die regelmäßige Abfrage ist im Server des Shops z.B. durch einen Cron-Job einzurichten. Wir empfehlen, aus Sicherheitsgründen regelmäßig z.B. einmal wöchentlich alle Artikeldaten neu von Ihrem **Kieselstein ERP** Server für den jeweiligen Webshop zu holen.

Zusammenführen von Magento-HeliumV-Connector und **Kieselstein ERP**

![](Webshop1.jpg)

**1.) Benutzer**

Damit der Shop Daten von **Kieselstein ERP** abholen bzw. übermitteln kann muss er sich mit Benutzer und Passwort an **Kieselstein ERP** anmelden. Dazu gehen Sie bitte wir folgt vor:

-   User in Benutzer im **Kieselstein ERP** anlegen z.B. webshop1\. Diesen beim Magento Connector bei User Name bzw. Password entsprechend eintragen.

-   Der Benutzer muss die Rechte haben um seinen Job erledigen zu können.

    -   D.h. Artikel lesen inkl. VK-Preisen lesen

    -   Kunde anlegen

    -   Aufträge anlegen ... wir schlagen vor hier das Aufträge aktivieren definitiv wegzulassen

-   Dem Benutzer ist über Benutzermandant der "WebShop"-Mitarbeiter zugeordnet. Dadurch wird wiederum für die neuen Kunden der Provisionsempfänger vorbesetzt. Für das Anlegen der Aufträge ist dies dieser User unter dem die Aufträge angelegt werden.

-   Die Ausweisnummer des Mitarbeiter-Webshops wird als API Key verwendet, welcher wiederum in der Magento **Kieselstein ERP** Connector Config unter API Key zu hinterlegen ist.
    Ist der API Key nicht definiert, können keine Aufträge angelegt werden.

**2.) Erforderliche Definitionen im Artikel**

-   Webshop .... Die in Artikel, Grunddaten, Webshop hinterlegte Bezeichnung des Webshops muss mit dem Shop Name im Magento **Kieselstein ERP** Connector übereinstimmen (Case Sensitive)

-   Shopgruppen ... Unter Artikel, Shopgruppe müssen Shopgruppen dem obigen Webshop zugeordnet werden. Nur diese werden für den jeweiligen Webshop übertragen bzw. deren Artikel

    -   Im Detail des Artikels hinterlegen Sie die Hauptshopgruppe. Im Reiter Webshopgruppen können Sie dem Artikel weitere zusätzliche Shopgruppen zuweisen.

    -   Referenzartikel ... Liefert Beschreibung und Bilder für die Shopgruppe im Allgemeinen, im Magento Category genannt.

-   Preislisten: Unter Artikel, Preisliste, Webshop wird definiert welche Preisliste in welchem Webshop erscheinen darf.

-   Preise aus den Kunden Sonderkonditionen.

    -   Steht KUNDEN_SONDERKONDITIONEN_IN_WEBSHOP = 0, so werden die Soko-Preise der Kunden nicht übertragen. D.h. es werden nur die für den Webshop definierten Preislisten übertragen

    -   Steht KUNDEN_SONDERKONDITIONEN_IN_WEBSHOP = 1, so werden auch die Soko-Preise der für den Webshop ausgewählten Kunden übertragen. D.h. für jeden Kunden wird zusätzlich zu den bereits definierten Preislisten eine weitere Preisliste im Magento angelegt und dem jeweiligen Kunden zugeordnet. In dieser sind die für den Kunden passenden Verkaufspreise enthalten, welche durchaus eine Mischung aus "normaler" Preisliste und den SoKo Preisen des Kunden sein können.
        **<u>ACHTUNG:</u>** bei vielen Kunden mit Sonderkonditionen kann dies zu einer Überlastung des Magento Shops aufgrund der Anzahl der Preislisten führen.

    In beiden Fällen werden Mengenstaffeln unterstützt. Die Preise werden in Mandantenwährung übermittelt. Sollte eine Verkaufs-Preisliste in einer anderen Währung sein, wird auf Mandantenwährung umgerechnet.

-   Beim Artikel

    -   Im Detail die Haupt-Shopgruppe zuordnen.
        Wird der Artikel in weiteren Shopgruppen = Catalog benötigt, so fügen Sie im Reiter Shopgruppen weitere Shopgruppen hinzu. Bitte beachten: Die Auswertungen z.B. unter Journal gehen nur auf die Haupt-Shopgruppe.

    -   Kommentar:
        Eine der Kommentararten muss für den Webshop freigeschaltet sein (Boolean). Das erfolgt in den Grunddaten - Reiter Kommentarart hier bei Webshop einen Haken setzen.
        Es können beim Artikel je Kommentarart (Webshop) und Typ je ein Datensatz = Bild, Langtext usw. hinterlegt werden.
        **<u>Wichtig:</u>** Der Text hat keine Formatierungszeichen und muss vom Inhalt her zu Ihrem Shop passen. Gleiches gilt für die Bilder.

    -   Referenzartikel: Mit dem Refernzartikel können Sie entweder einen Artikel als Beispiel für weitere Artikel der Kategorie anlegen und diesen auch im Webshop anzeigen lassen, oder nur für die allgemeinen Informationen (wie zum Beispiel Text und Bild) für eine Webshopgruppe (Kategorie in Magento) hinterlegen. Die Entscheidung ob der Artikel auch in der Liste der zu kaufenden Artikel angeführt wird, erfolgt über den Preis. Sobald der Preis 0 ist, wird dieser nur als Vorlage bzw. Beispiel verwendet und kann über den Webshop nicht bestellt werden.

    -   Mehrwertsteuer: Hinterlegen Sie im Artikel die Mehrwertsteuer, dann wird diese auch im Webshop übernommen

    -   SEO Informationen: Mit den Kommentararten SEODescription, SEOKeywords, SEOTitle werden Inhalte für die Suchmaschinenoptimierung des Webshops hinterlegt. 

**3.) Erforderliche Definitionen im Kunden**

-   Kundenselektionen
    Damit wird definiert, welche Kunden an den Shop übertragen werden. Dafür ist in den Partner, Grunddaten, Selektionen bei den gewünschten Selektionen das Webshop anzuhaken.

-   Zusätzlich muss eine gültige EMail-Adresse in den Detaildaten hinterlegt haben. Ist diese nicht gegeben, wird der Kunde nicht an den Shop übermittelt.

-   Info zu Kundepreisliste bei mehreren Partnern.
    Es greift hier immer die Preisliste des letzten Partners.
    Zusätzliche Info: Sollte bei der Einstellung der Preislisten in der Überleitung in die **Kieselstein ERP** Auftragsbestätigung (= Kundenbestellung aus dem Shop) ein Fehler in den Preisen festgestellt werden, so wird dies im Auftrag entsprechend durch eine Textposition angezeigt.

**4.) Kundenbestellung**

Die Übermittlung der Kundenbestellung erfolgt vom Shop aus gesehen sofort. Es wird dadurch umgehen ein neuer Kundenauftrag angelegt. Dieser Auftrag ist im Status angelegt und muss vom **Kieselstein ERP** Anwender aktiviert werden. Damit haben Sie die volle Kontrolle was wirklich bestellt und vor allem bestätigt wird.

**5.) Updatezyklus**

Vom Magento-**Kieselstein ERP**-Connector werden in einem Cron-Job regelmäßig (stündlich / täglich) die geänderten **Kieselstein ERP** Artikelstammdaten abgefragt. Von **Kieselstein ERP** werden diese Änderungen ermittelt und an den Shop übertragen. Somit sparen Sie sich:

-   Die Pflege der Artikelstammdaten: Diese sind: Artikelnummer, Bezeichnungen (3x), Bild, Kommentar, VK-Preise der WebShop Preislisten, Änderungen der Shopgruppen Zuordnungen

-   Die Pflege der Kundenstammdaten: Diese sind: Adressen inkl. Rechnungsadresse, EMail-Adresse, Telefon, Fax, Hompage (Url), Preislistenzuordnung und gegebenenfalls Sonderkonditionen (Soko)

**6.) ServerUrl**

Für die Einstellung der Server-Url, also der Zugriffsmöglichkeit vom Magento Webshop Connector auf Ihren **Kieselstein ERP** Server http://KIESELSTEIN-ERP_SERVER:8080/lpserver-ejb/ArtikelFacBeanRest?WSDL

**7.) Installation des **Kieselstein ERP** - Magento Shop Connectors**

Überlegung

Vorbereitende Arbeiten

Welche Daten möchten Sie als Benutzer = Käufer in Ihrem Shop sehen

Wie werden diese Daten in Ihr **Kieselstein ERP**, vor allem Artikelstamm eingetragen

Bringen Sie diese beiden "Sichten" zur Deckung und Sie erhalten die Informationen die Sie in Ihrem **Kieselstein ERP** Artikelstamm pflegen müssen.

Denken Sie an Shopgruppen, SEO Texte, Bilder, Preise brutto netto in unterschiedlichen Ländern usw.

Bedenken Sie, dass die Artikelbezeichnung im Magento auch für die URL des Artikels verwendet wird. Daraus ergibt sich, dass gleichlautende Artikelbezeichnungen auf gleiche URLs zeigen. Aus diesem Grunde haben wir automatisch am Ende der Bezeichnung die Artikelnummer angefügt.