---
categories: ["Weblinks"]
tags: ["Weblinks"]
title: "Weblinks"
linkTitle: "Weblinks"
date: 2023-01-31
weight: 110
description: >
  Verfügbare Weblinks, innerhalb des Kieselstein-ERP
---
Web-Schnittstellen
==================

Folgende Web-Links werden von **Kieselstein ERP** derzeit unterstützt.
Wir unterscheiden dabei drei verschiedene Kommunikations/Darstellungsformen.

### Im normalen HTML Browser darstellbar:

| Funktion | Aufruf für wildfly |Bemerkung|
| --- | --- | --- |
| Anwesenheitsliste | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=anwesenheitsliste |  |
| Online Datenerfassung | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=bdestation | BDE-Station |
| Quick-Auftrags-zeiterfassung | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=quickze | Zur erstmaligen Erfassung der Login Informationen. Die Logindaten werden in einem Cookie abgespeichert. Löschen der Cookies [siehe](#Loeschen von Cookies). |
| Quick-ZE Direkt Start | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=quickzeiterfassung | Nach Erfassung der Logindaten kann auch direkt mit diesem Link gestartet werden. |
| Quick-ZE neue Login Daten | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=quickze&reenter=true |  Um die Logindaten dieses Users neu zu erfassen |
| WebClients,Küchenmodul | http://KIESELSTEIN-ERP-SERVER:8080/lpkueche/AppKueche.html |   |
| Web-Services | http://KIESELSTEIN-ERP-SERVER:8080/jbossws/services |   |
| Web-Service-REST | http://KIESELSTEIN-ERP-SERVER:8280/kieselstein-rest-docs/ | Dokumentation der RESTful-API, direkt in **Kieselstein ERP** integriert: |
| Web-Service-Ping Test | http://KIESELSTEIN-ERP-SERVER:8280/kieselstein-rest/services/rest/api/v1/system/ping |   |
| WebShop-Interface | http://KIESELSTEIN-ERP-SERVER:8080/lpserver-ejb/ArtikelFacBeanRest?WSDL |   |
| Liste der aktuell zu liefernden Aufträge | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=offeneauftragspositionen<br>Optionaler Parameter: "mandant", also: http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=offeneauftragspositionen&mandant=004 | Tipp:Sollte die Auswertung nicht die gewünschten Ergebnisse liefern, rufen Sie diese bitte im **Kieselstein ERP** Client auf. Vermutlich wird dann klar warum.Stichtag siehe Parameter: OFFENE_AUFTRAGSPOSITIONEN_WERKTAGE_IN_ZUKUNFT |
| Liste der aktuell zu liefernden Aufträge nur für eine Fertigungsgruppe | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=offeneauftragspositionen&fertigungsgruppe=fgr | Liefert nur Aufträge der angegebenen Fertigungsgruppe (fgr). Diese wird im Parameter P_FERTIGUNGSGRUPPE an den Report übergeben |
| Liste der aktuell zu liefernden Aufträge mit Sortierung nach Liefertermin bzw. Abliefertermin | &sortierungLiefertermin=false oder true&sortierungAbliefertermin=true oder false | Werte false oder true<br>Sortierung nach Liefertermin, default false<br>Sortierung nach Abliefertermin, default true |
| Liste der offenen Los-Arbeitsgänge | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=taetigkeit_agbeginnOptionaler Parameter: "mandant", also: http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=taetigkeit_agbeginn&mandant=004 | Tipp: Sollte die Auswertung nicht die gewünschten Ergebnisse liefern, rufen Sie diese bitte im **Kieselstein ERP** Client auf. Vermutlich wird dann klar warum. |
| Liste der offenen Los-Arbeitsgänge für eine bestimmte Tätigkeit | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=taetigkeit_agbeginn&taetigkeit=AZ_00001 | AZ_00001 bitte die gewünschte Tätigkeit angebenEs werden die offenen Tätigkeiten für die nächsten drei Tage aufgeführt |
| und für mehr als 3Tage | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=taetigkeit_agbeginn&taetigkeit=AZ_00001&tage=5 | tage definiert die Tage ab heute auf die der Stichtag gesetzt wird, z.B. 5 Tage |
| Journal Auftrag offene Positionen | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=auftrag_offene_positionenOptionaler Parameter: "mandant", also: http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=auftrag_offene_positionen&mandant=004 | Tipp:Sollte die Auswertung nicht die gewünschten Ergebnisse liefern, rufen Sie diese bitte im **Kieselstein ERP** Client auf. Vermutlich wird dann klar warum. |
|MECS Commandos |  | Spezialkommandos für Terminals der Firma MECS (werden leider nicht mehr produziert) |
|Ausweise holen | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=MECS_AUSWEISE | Liste der Personen mit Ausweisnummern |
|Personalstamm holen| http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=MECS_PERSSTAMM | Liste der Personen die eine Ausweisnummer hinterlegt haben, aber die Personalnummern normiert |
|Saldenabfrage | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=MECS_SALDO&ausweis=STRING_VON_MECSTERMINAL | mit Angabe der gewünschten Ausweisnummer |
|Zeitbuchung an KES Server senden | http://KIESELSTEIN-ERP-SERVER:8080/ze/ze?cmd=MECS_ZEITBUCHEN&record=STRING_VON_MECSTERMINAL  | Damit werden Zeiten vom Terminal im KES gebucht. Format aktuell bitte bei Technik erfragen |

<!-- | Webportal Kundenstückliste(von extern) | https://öffentliche_Adresse:8443/hvpartlist/ |  | -->

Damit der Zugriff auf den **Kieselstein ERP** Server erfolgen kann, müssen die verwendeten Ports in der Firewall des Servers freigeschaltet sein. Wenden Sie sich dazu bitte an Ihren Systembetreuer.
Bitte nutzen Sie für eventuelle Tests bevorzugt Firefox. Der Microsoft Internetexplorer muss für die Tests in seinen Sicherheitseinstellungen umfassend parametriert werden.
Wenn Ihr **Kieselstein ERP** nur per https erreichbar ist, so verwenden Sie bitte anstelle von http://KIESELSTEIN-ERP-SERVER:8080 den https Zugang: https://KIESELSTEIN-ERP-SERVER:8443

Um den Internetexplorer direkt ohne Bedienungsmenü zu starten legen Sie folgende Verknüpfung an:
"C:\Programme\Internet Explorer\iexplore.exe" -k http://KIESELSTEIN-ERP-SERVER:8080/lpkueche/AppKueche.html
Das -k bedeutet ohne Bedienelemente, dahinter folgt die URL der gewünschten Funktion.

Um die HTML BDE-Station ohne dem Aufruf des zusätzlichen Fenster zu starten, geben Sie bitte nach ....cmd=bdestation&done=true an.

Hinweis: Je nach Konfiguration Ihrer Installation kann die Portadresse abweichen.
Den KIESELSTEIN-ERP-SERVER bitte durch den Namen / die IP Adresse Ihres Servers ersetzen.

Bei mehreren **Mandanten** wird der Zugriff auf den Mandanten grundsätzlich über den im Benutzermodul eingestellten Default-Mandanten des Benutzers lpwebappzemecs gesteuert. Sollte ein davon abweichender Zugriff gewünscht werden, so ergänzen Sie die Aufrufe für HTML Browser bzw. Zeiterfassungsterminal um den Parameter &mandant=MMM. MMM ersetzen Sie bitte durch die Mandantennummer.

## Fehler bei der Anmeldung

**WICHTIG:** Denken Sie bitte daran, dass dieser Benutzer auch eine Zugriffsberechtigung für den Defaultmandanten benötigt. D.h. unter Umständen muss der Mandant unter Benutzer und unter Benutzermandant umgestellt werden.
Wenn Fehlermeldungen wie: Fataler Fehler auftreten und diese auch nach mehrmaligem Senden bestehen bleiben, so prüfen Sie bitte diese Einstellungen.
**WICHTIG:** Beachten Sie bitte auch, dass die angegebenen Ausweisnummern im richtigen/zugeordneten Mandanten sein müssen.

**Bei mehreren Sprachen** bzw. bei abweichenden Mandanten-Sprachdefinitionen muss unter Umständen die Sprachdefinition mit übergeben werden. Für die Sprachdefinition ergänzen Sie den Link bitte um &locale=spKK 
spKK steht für [Sprachkurzkennung]( {{<relref "/start/10_mehrsprachigkeit">}} ) z.B: deAT.

## Erforderliche Rechte für QuickZE

Für die Quickzeiterfassung sind mindestens folgende Rechte erforderlich:

| Recht | Text |
| --- |  --- |
| ANGB_ANGEBOT_R | Lesen im Angebot |
| AUFT_AUFTRAG_R | Lesen im Auftrag |
| FERT_LOS_DARF_ABLIEFERN | Darf im Los Ablieferungen durchführen |
| FERT_LOS_R | Lesen in der Fertigung |
| LP_DARF_EMAIL_SENDEN | Darf E-Mails versenden |
| PERS_ANWESENHEITSLISTE_R | Darf Anwesenheitsliste sehen |
| PERS_SICHTBARKEIT_ALLE | Darf alle Personen sehen |
| PERS_ZEITEREFASSUNG_CUD | Schreiben in der Zeiterfassung |
| PERS_ZEITERFASSUNG_REPORTS_SEHEN | Darf Reports -Info- in der Zeiterfassung sehen |
| PROJ_PROJEKT_R | Lesen im Projekt |
| WW_ARTIKEL_R | Lesen im Artikel |
| Lager | denken Sie daran der Rolle auch die entsprechenden Lager zuzuweisenDenken Sie auch an das KEIN LAGER |

**Managementconsole** für die Betrachtung der Auslastung Ihres **Kieselstein ERP** Servers:
Diese steht unter http://KIESELSTEIN-ERP-SERVER:8080/web-console/ zur Verfügung.

Webservices:
Unter http://KIESELSTEIN-ERP-SERVER:8080/jbossws/services erhalten Sie eine Liste der aktuell verfügbaren und freigeschalteten Web-Services, welche von **Kieselstein ERP** angeboten werden.

Bei der Verwendung von Proxys achten Sie bitte darauf, dass Sie, eventuell nur für die internen IP-Adressbereiche, Zugriff auf Ihren **Kieselstein ERP** Server haben.

Es müssen generell Popups für Ihren **Kieselstein ERP** Server erlaubt sein.
Im Safari unter Safari, Popups unterdrücken abschalten.

Im Firefox die Seite erlauben. Dazu die Seite aufrufen, ev. wird nichts angezeigt, dann rechte Maustaste, Seiteninformationen anzeigen, Reiter Berechtigungen und hier für die Site Ihres **Kieselstein ERP** Servers nachfolgende Einstellungen übernehmen.

![](WebLink_FireFox.jpg)

Eventuell müssen die Cookies mehrmals hintereinander (nach Aufrufversuchen) gelöscht werden.

## Löschen von Cookies:

Im Safari / am MAC

1.  Klicken Sie im Safari auf Einstellungen.
    ![](MAC_Cookie_Loeschen1.jpg)

2.  Klicken Sie nun auf den Reiter Sicherheit und dann auf den Button Cookies anzeigen:
    ![](MAC_Cookie_Loeschen2.jpg)

3.  Wählen Sie nun die Cookies Ihres **Kieselstein ERP** Servers.
    In diesem Beispiel ist das wernersmacbook.local
    Markieren Sie alle Cookies und Klicken Sie auf entfernen.
    ![](MAC_Cookie_Loeschen3.jpg)

Im Firefox

1.  Wählen Sie in der Menüleiste Extras, Einstellungen

2.  Wählen Sie den Reiter Datenschutz
    ![](Firefox_Cookie_Loeschen1.jpg)

3.  Klicken Sie auf Cookies anzeigen
    und geben Sie den Namen Ihres **Kieselstein ERP** Servers ein
    ![](Firefox_Cookie_Loeschen2.jpg)

4.  Markieren Sie nun alle Cookies und klicken Sie auf Cookies entfernen
