---
categories: ["RestAPI"]
tags: ["RestAPI"]
title: "RestAPI"
linkTitle: "RestAPI"
date: 2023-01-31
weight: 100
description: >
  RestAPI, Dokumentation der Einsprungpunkte
---
Kieselstein REST API
====================
Mit der REST API steht dir der Zugriff auf viele Funktionalitäten deines **Kieselstein ERP** zur Verfügung. Der generelle Einsprungspunkt ist die IP Adresse deines **Kieselstein ERP** Servers, http://KIESELSTEIN-ERP-SERVER:8080/kieselstein-rest-docs/

![](rest_api.png)  

Hier sind alle auf deinem **Kieselstein ERP** verfügbaren REST API (auch Restfull Service API (Application Programm Interface)) Calls dokumentiert. Bitte beachte, dass diese bei neueren **Kieselstein ERP** Versionen auch neue Funktionalitäten beinhalten werden. Man könnte auch schreiben, laufend erweitert werden.

Um die Funktionalität zu prüfen kann das <u>interactive interface</u> genutzt werden. D.h. mit dem Link auf der ersten Seite "interactive interface" kommst du auf eine Seite aus der du die komplette API testen kannst.

So kann z.B. 
![](rest_local_ping.png)  
mit dem local Ping festgestellt werden, ob der Tomcat Dienst, überhaupt läuft.<br>
So ist die Antwort<br>
![](rest_local_ping_antwort.png)

Mit dem SystemPing kann auch der Zugriff der RestAPI, des Tomcat-Servers, auf den **Kieselstein ERP** Server geprüft werden.<br>
![](rest_system_ping.png)  

D.h. man wählt den gewünschten Punkt der API z.B. unter SystemAPI den Eintrag bei GET /api/v1/system/ping und klickt auf **Try it out** und klickt dann Execute.<br>
Danach werden der Request und und der Response angezeigt, welche dann auch in weiterführenden Programmen, z.B. mobile Apps, das Zeiterfassungsterminal, verwendet werden können.

{{% alert title="WICHTIG" color="primary" %}}
Auch wenn am **Kieselstein ERP** die Datenbankzugriffe bekannt sind, so darf ein schreibender Zugriff auf die Datenbank ausschließlich über die kontrollierenden Module erfolgen, d.h. derzeit ausschließlich über die RestAPI (und natürlich den Client). Mit einem direkten schreibenden Datenbankzugriff kann es bis zur nicht Verwendbarkeit deiner Daten kommen.
{{% /alert %}}



