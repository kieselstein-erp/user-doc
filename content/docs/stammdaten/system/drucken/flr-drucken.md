---
categories: ["Drucken"]
tags: ["FLR-Drucken"]
title: "Auswahllisten Drucken"
linkTitle: "Auswahllisten"
date: 2023-01-31
weight: 400
description: >
  Übersteuern der Auswahllistendrucke
---
Druck von FLR-Listen
====================

#### Wie übersteuert man einen Allgemeinen FLR-Druck?
Um einen FLR- Druck zu übersteuern, benötigt man folgende Informationen:

1.) Die Use-Case-Id der FLR-Liste -> zu finden rechts oben im nicht übersteuerten Report ![](FLR_Druck.png)  
2.) Die Datentypen der einzelnen Spalten -> zu finden in der Methode getTableInfo() der zugehörigen *Handler Klasse FastLaneReaderBean -> praktischer: Erraten

Je nach Anzahl der Spalten werden folgende Feldnamen an den Report übergeben:

Spalte0 (Ist im Allgemeinen FLR-Druck nicht sichtbar, da hier normalerweise der Primary-Key enthalten ist.)

Spalte1

Spalte2

Spalte3

...

Als Muster kann z.B. allgemein/flrdruck2018.jrxml verwendet werden. Um dies für die gewünschte Auswahlliste zu verwenden das 2018 durch die UseCaseID ersetzen und die Spalten wie gewünscht / erforderlich definieren.