---
categories: ["Drucken"]
tags: ["Drucken"]
title: "Drucken"
linkTitle: "Drucken"
date: 2023-01-31
weight: 010
description: >
  Die verschiedensten Druck Möglichkeiten
---
Drucken im **Kieselstein ERP**
===================

Um den üblichen Druckvorgang in **Kieselstein ERP** so rasch wie möglich und so flexibel wie notwendig durchführen zu können wurden folgende Zuordnungsmöglichkeiten geschaffen.

Zuerst aber einige Definitionen / Erklärungen:

Grundsätzlich:

Je größer / verzweigter die Anwenderinstallation, also Ihre Installation, ist, desto komplexer wird auch die Druckaufgabe und vor allem die optimale Steuerung des Ausdruckes möglichst ohne Benutzerinteraktion und mit geringer Netzwerkbelastung.

![](Drucken_Umfeld.jpg)

In dieser Darstellung gibt es also verschiedene Druckertypen, verschiedene Kommunikationswege.

Natürlich will der Anwender eines externen Standortes auf einen bestimmten Drucker in der Zentrale drucken, damit von da aus wieder weitere Aktionen eingeleitet werden können.

Grundsätzlich müssen folgende Druckertypen unterschieden werden:

| Druckertype | Besonderheiten | Bemerkung |
| --- |  --- |  --- |
| Laserdrucker | SchächteFormulare | Keine Durchschläge |
| Endlosdrucker | ev. 2 Traktore | Druckerspezifische Fonts verwenden<br>eigene Druckersprache |
| Etikettendrucker | Thermo Transfer DruckerCutter | Drucker Fonts<br>eigene Druckersprache |

Unterscheidung der Drucker aus der Sicht des Anwenders:

| Druckertype | Eigenschaft | Positionierung |
| --- |  --- |  --- |
| Listendrucker | Schnell, robust, viele Seiten |  |
| Briefpapierdrucker | Erste Seiten, Folgeseiten | Schnell erreichbar, an Zentraler Stelle |
| Etikettenausdrucke | Schneller Ausdruck von Einzeletiketten | direkt am Arbeitsplatz |

Möglichkeiten der Druckerdefinition:

Selbstverständlich sind diese Definitionen wiederum Mandantenabhängig.
- Benutzer / Benutzerstandort spezifisch
- Formular spezifisch
- Arbeitsplatz spezifisch

In **Kieselstein ERP** gibt es von der Anschlusstechnik, von der Verfügbarkeit her gesehen zwei Anschlussarten:
- der Drucker ist dem **Kieselstein ERP** Server bekannt
- der Drucker ist nur auf dem **Kieselstein ERP** Client verfügbar.

Hinweis: Alle Ausdrucke die über den Client erfolgen, werden nur auf den Clientdruckern ausgegeben. Die Server-Drucker sind nur für ganz spezielle Funktionen, wie Automatik-Jobs oder Serverdrucke durch die RestAP gedacht.

Die Steuerung ob ein Druck auf offizielles Briefpapier erfolgen sollte, oder auf weisses Papier erfolgt durch die Ansteuerung der jeweiligen Drucker bzw. der gewünschten Druckerschächte.
Bitte beachten Sie, dass erste Seite auf Briefpapier, Folgeseiten auf Normalpapier vom Drucker(treiber) geleistet werden muss. Eine Trennung in Original auf den einen Drucker und Kopien auf einen anderen Drucker, kann bei den Belegdrucken eingerichtet werden.
In den üblichen Bewegungsmodulen, finden Sie 
Drucke auf offizielles Papier finden Sie immer unter dem linken oberen Menüpunkt, welcher zugleich mit dem Namen des Moduls gekennzeichnet ist. Darunter finden Sie den Menüpunkt Drucken. Dies ist der speziell für den Druck einer offiziellen Rechnung aufbereitete Druck. Sehr oft finden Sie diese Funktion auch im Druckknopf in den Positionsdaten des Moduls.

Druck von Informationen zu einem Datensatz. Die Informationen zu einem Datensatz finden Sie immer unter dem Menüpunkt Info. Also z.B. die Lieferstatistik eines Kunden, die Reservierungen eines Artikels usw.

Informationen über eine Summe von Daten, z.B. über alle Kunden, finden Sie im Menüpunkt Journal.

#### Wozu dient die Lupe?
In jeder **Kieselstein ERP** Liste finden Sie in der rechten oberen Ecke eine Lupe ![](Listen_Drucken_Lupe.gif). Mit dieser Lupe kann die aktuell angezeigt Liste ausgedruckt werden. Bitte beachten Sie, dass dies nur die Daten der aktuellen Liste sind und nichts mit der Druckfunktion des z.B. Angebotes gemein haben. 
Wenn gewünscht kann der [Auswahllistendruck übersteuert]({{< relref "flr-drucken" >}}) werden.

Bedeutung der Druck / Kommunikationsknöpfe

Durch den Aufruf eines Druckes, eines Journals oder ähnlichem gelangen Sie in den sogenannten Druckdialog. Hier finden Sie für jeden Report / jeden Ausdruck folgende Knöpfe, mit denen der Ausdruck nach Ihren Wünschen gesteuert werden kann.

![](Drucken_Leiste.jpg)

Grundsätzlich besteht das Fenster des Druckdialoges aus drei Teilen.

- Der Knopfzeile

- einem Auswahl Dialog in dem Sie die Bedingungen für diesen Druck / diese Auswertung einstellen

- einem kleinen Vorschaufenster

![](Drucken_Refresh.gif) **Aktualisieren**. Mit diesem Knopf werden die im Druckdialog angezeigten Daten aktualisiert. Bitte beachten Sie, dass der Refresh Knopf nur für den Inhalt des Reports, aber nicht für die den Report umgebenden Daten gilt.

![](Drucken_Vorschau.gif) **Vorschau**. Damit werden die Daten im Druckdialog in einem eigenen größeren Vorschaufenster dargestellt. Wurden im Druckdialog noch keine Daten angezeigt, so werden diese vorher ermittelt. Wurden bereits Daten ermittelt, so werden diese <u>**NICHT neu errechnet**</u> um entsprechende Rechenzeit usw. zu sparen.

![](Drucken_Direkt.gif) **Drucken**. Wie Vorschau, jedoch werden die Daten an den Drucker gesandt, der diesem Report hinterlegt ist.

![](Drucken_Email.gif) **EMail**. Wie Vorschau, jedoch werden Sie zur Eingabe eines EMail Empfängers aufgefordert. Der Report wird als EMail mit PDF-Anhang versendet. Konnte Aufgrund der Daten des Drucks ein EMail Empfänger ermittelt werden, so wird dieser vorgeschlagen.

![](EMail_Versenden.jpg)

Für den EMail-Versand stehen zusätzliche Funktionen zur Verfügung.

| Feld | Beschreibung |
| --- |  --- |
| Sendezeitpunkt | Vorgabe eines Sendezeitpunktes.Bleibt dieser leer so wird der Zeitpunkt des Klicks auf Senden zzgl. fünf Minuten (siehe Parameter DELAY_EMAIL_VERSAND) als Sendezeitpunkt verwendet |
| Senden | Erzeugt den Versandauftrag mit den angegebenen Parametern. Wurden Kopien angegeben so erfolgt der Druck der Kopien auf den Kopiendrucker bzw. dem darüber angegebenen Drucker |
| Als ZUGFeRD  | Versendet die Rechnung als PDF im PDF/A-3B Format mit integrierter XML-Datei nach [ZUGFeRD Format Basic](#ZUGFeRD)Bitte beachten Sie, dass dafür Ihre eigene UID-Nummer eingetragen sein sollte und dass Ihre Lieferantennummer beim jeweiligen Kunden eingetragen sein muss. |
| Ohne Kopien  | Obwohl Kopien angegeben wurden, sollten diese nicht gedruckt werden |
| Part (links oben neben An:) | Auswahl der EMailadresse für An: eines ev. anderen Partners.Wenn der Versand an Unternehmen erfolgen sollte und dann einem entsprechenden Ansprechpartner, so achten Sie darauf, dass das Unternehmen ausgewählt wird.Bei der Auswahl eines Unternehmens werden Sie automatisch nach dem Ansprechpartner, wenn vorhanden gefragt.Wird nur ein Partner ausgewählt, so wird dessen EMail-Adresse verwendet. Dies bedeutet auch, wenn Sie unter Partner direkt die Ansprechperson eines Unternehmens verwenden würden, so würden Sie das EMail an die private EMail-Adresse des Ansprechpartners senden. |
| Ansp (zwischen Part und An:) | Auswahl eines anderen Ansprechpartners für An:, basierend auf dem Partner an den das EMail geht, z.B. das AngebotHier wird in der Auswahlliste durch das EMail Symbol ![](Senden_EMail_Ansp_hat_Email.gif) angezeigt, ob bei dem Ansprechpartner auch eine EMail-Adresse hinterlegt ist.  |
| Part (links neben CC) | Analog wie Part für An:, jedoch für CC |
| Ansp (zwischen Part und CC) | Analog wie Ansp für An:, jedoch für CC |
| An: | EMail-Adresse des Empfängers |
| CC | EMail-Adresse des Empfängers der Kopie/Durchschlag |
| Empfangsbestätigung | Wenn angehakt, wird vom Empfänger ein Bestätigungsemail angefordert |
| Absender | Die Absender-EMail-Adresse. Definition siehe [Personal, Daten]( {{<relref "/docs/stammdaten/personal/#absenderdaten" >}} ) |
| Betreff |  |
| Anfügen / Anhang | Mit Klick auf Anfügen wird die ausgewählte Datei der Liste der Anhänge hinzugefügt. |
| Text | Text der im eigentlichen EMail angegeben wird. Dieser kann durch Klick auf ![](EMail_Text_bearbeiten.gif) (Strg+T) bearbeitet werden. Er kann auch für jedes Formular spezifisch an Ihre Vorgaben angepasst werden. Wenden Sie sich dazu an Ihren **Kieselstein ERP** Betreuer. |

### Blättern, Zoomen, PDF Vorschau, Zwischenablage
In der (kleinen) Druckvorschau stehen über der eigentlichen Darstellung des Reports weitere Elemente zur Anzeige zur Verfügung.<br>
![](Steuerung_kleine_Druckvorschau.png)<br>
Hier sind vor allem folgende Buttons interessant:
- ![](PDF_Vorschau.png) Damit wird, **OHNE** dass der Beleg aktiviert wird, dieser in der **Kieselstein ERP** internen PDF Darstellung dargestellt
- ![](Zwischenablage_Vorschau.png) Damit wird für jedes Feld die Funktion Kopieren in Zwischenablage, bei Klick auf das gewünschte Feld aktiviert. Damit können, gerade in Liste durch Klick z.B. in die Artikelnummer diese in die Betriebssystemspezifische Zwischenablage kopiert werden und dieser Inhalt in anderen Programmen oder auch in anderen **Kieselstein ERP** Clients verwendet werden. Um diese Funktion einzuschlaten, muss der Button gedrückt sein.

### RE-EMail
<a name="RE-EMail"></a>
Zusätzlich kann für den Rechnungsausdruck der Vorschlag des EMail-Versandes erzwungen werden.
Tragen Sie dafür beim Kunden in den Kopfdaten im Feld RE-EMail die gewünschte Rechnungsempfänger EMailadresse ein.<br>
Bitte beachten Sie dazu auch die Einstellung des Parameters RECHNUNGSEMPFAENGER_NUR_AUS_RECHNUNGS_EMAIL. Steht dieser auf 1, wird für alle Rechnungen faktisch ein EMail-Versand(Vorschlag) erzwungen. Das hat den Zweck, dass, gerade wenn Sie vollständig auf EMail-Rechnungsversand umstellen möchten, Sie sofort erkennen, welche Kunden noch nicht umgestellt sind, d.h. bei denen die RE-EMail Adresse im Kunden noch nicht hinterlegt ist.

**Info:** Wenn der Knopf *Als ZUGFeRD PDF senden* grau hinterlegt ist ![](ZUGFeRD_disabled.gif), so bedeutet dies, dass zwar die Funktionalität von ZUGFeRD gegeben ist, aber bei diesem Kunden die Basisvoraussetzung, in diesem Falle die Lieferantennummer, nicht gegeben ist, weshalb an diesen Kunden keine ZUGFeRD Rechnung versandt werden kann. Bitte gegebenenfalls Ihre Lieferantennummer bei diesem Kunden entsprechend nachtragen.

![](Drucken_Fax.gif) **Fax**. Wie Vorschau, jedoch werden Sie zur Eingabe eines Fax Empfängers aufgefordert. Konnte Aufgrund der Daten des Drucks ein Fax Empfänger ermittelt werden, so wird dessen Faxnummer vorgeschlagen. [ Siehe auch]( {{<relref "/docs/installation/15_email_einrichten/#fax---e-mail-gateway" >}} ).

Die Bedienung ist analog dem EMail, jedoch auf die Möglichkeiten eines Faxes reduziert.

![](Drucken_Speichern.gif) **Speichern**. Speichert den angezeigten Report inkl. Inhalt in verschiedenen Formaten. Wurden im Druckdialog noch keine Daten angezeigt, so müssen diese erst mit Aktualisieren erzeugt werden.

Die Formate sind:<br>
![](Drucken_Formate.gif)

**Wichtig:** Die Darstellung des Dezimaltrenners orientiert sich an der lokalen (locale) Übersetzung des Zahlen-Patterns. D.h. um eine in deutscher Schreibweise ordentliche Übergabe an eine Tabellenkalkulation zu erhalten, muss für eine Zahl die Formatierung, z.B. #,##0.00   ![](CSV_Zahlen_Formate.gif)  unter Muster angegeben werden.

Um diese Daten vernünftig übernehmen zu können, muss der Import im Unicode Format UTF-8 erfolgen.
Sollte Ihre Tabellenkalkulation diese Konvertierung nicht zur Verfügung stellen, so verwenden Sie bitte [Libre Office](https://de.libreoffice.org/download/download/) in der aktuellen Version und konvertieren Sie damit die Datei für Ihre bevorzugte Tabellenkalkulation. Siehe dazu auch unten.

#### Welche der angeführten (Jasper-) Formate können verwendet werden?
PDF, ODT, XLS, CSV<br>
Hinweis zum xls-Format. Dieser Export stellt die XLS Datei so ein / dar, dass es aussieht wie in Ihrem Druck. Diese Datenstruktur ist für eine weitere Verarbeitung oft ungeeignet. Bewährt hat sich strukturierte Daten im CSV Format zu exportieren. Gegebenenfalls werden speziell angepasste Reportvorlagen benötigt bzw. von uns zur Verfügung gestellt, um einen guten und raschen Import zu ermöglichen.<br>
Um die CSV Datei in Libre Office zu übernehmen muss der Importassistent wie folgt eingestellt werden:<br>
![](Drucken_OpenOffice_CSV_Import.gif)

##### CSV Export
<a name="CSV-Export"></a>
![](Drucken_CSV_Export.gif)
Damit können die unter der Gruppe Detail im Report angeführten Felder in einer CSV Struktur (<u>**C**</u>omma to <u>**S**</u>eparate <u>**V**</u>alues), im Format Feld <TAB> als *.txt Datei exportiert werden, damit diese möglichst einfach in eine Tabellenkalkulation übernommen werden kann. In diesem Export werden **nur** die **Field**-Daten des Reports übertragen. Die Reihenfolge der Spalten werden von **Kieselstein ERP** vorgegeben. Zusätzlich werden bei Texteingaben (Kommentare etc.) die Zeilenumbrüche durch Leerstellen (Space) ersetzt, da einige Tabellenkalkulationen damit nicht richtig umgehen können.

Für den Import in Excel gehen Sie wie folgt vor:

Speichern Sie die Datei mit der Erweiterung .txt ab

Wählen Sie in Excel nun Datei, öffnen und wählen Sie Datei aus

Der TabStop ist das Spaltentrennzeichen.

![](Drucken_Export.gif)

Definieren Sie alle üblicherweise nur Ziffern beinhaltenden Spalten als Text.

Diese Spalten sind üblicherweise: Postfach, Lieferantennummer, die Postleitzahlen.

Klicken Sie nun auf Fertigstellen.

Damit haben Sie Ihre aus **Kieselstein ERP** exportierte Datei importiert.

**Wichtig bei Verwendung von Anwenderreports:**

Die zu exportierenden Felder / Feldnamen werden aus dem (Anwender-) Report geholt.

![](Drucken_CSV_Export2.jpg)

Es werden nur die unter Felder (Fields) angeführten Felder in der angeführten Reihenfolge exportiert.

Die Namen der Felder müssen wie von uns definiert belassen werden.

![](Drucken_TAB_Export.gif) Tab-Export in die Zwischenablage. Mit dieser Exportfunktion werden die Daten in die Zwischenablage des Betriebssystems durch Tab getrennt übergeben. Der Datenaufbau ist wie für den CSV-Export oben beschrieben. Damit haben Sie eine einfache und rasche Möglichkeit dies direkt in eine Tabellenkalkulation zu übernehmen.

Bitte beachten Sie, dass in manchen Formularen umfangreiche Auswertungen / Andrucksteuerungen direkt im Report enthalten sind. Diese werden mit den beiden CSV Exportfunktionen NICHT mit exportiert. Sollten Sie ein direktes Abbild des errechneten Reports benötigen, so nutzen Sie bitte ![](Drucken_Speichern.gif) Speichern.

### Hinweise zur Übernahme in eine Tabellenkalkulation:
Da der Reportgenerator versucht in den Zielformaten möglichst das Aussehen zu erzielen, wie es auch in der Druckvorschau aussieht, dies aber gerade bei weiterführenden Auswertungen oft nicht erwünscht ist, empfiehlt es sich die Datei als *.csv Datei abzuspeichern. In diesem Dateiformat sind keine Formatierungen enthalten. Benennen Sie nun diese Datei vor dem Export auf *.txt um. Danach geben Sie beim Import unter Dateityp, getrennt an, Trennzeichen , (Komma), Texterkennung keine. Markieren Sie nun alle Spalten und definieren Sie als Feldtype Text. Nun haben Sie die einzelnen Spalten bestmöglich importiert.

Alternativ können Sie die Datei auch direkt als CSV Datei importieren. Sie erhalten nun (im deutschsprachigen Raum) alle Daten in einer Spalte, der Spalte A. Markieren Sie diese Spalte durch Klick auf die Spaltenüberschrift. Nun wählen Sie Daten, Text in Spalten. Nun erscheint ebenfalls der Textkonvertierungsassistent. Gehen Sie wie oben beschreiben vor. Für die Konvertierung von Zahlen muss unter Umständen der Dezimaltrenner von , auf . umgestellt werden. Dies kann im Schritt 3 von 3 unter weitere definiert werden.

Die Exportfunktion mittels Speichern ![](Drucken_Speichern.gif) im Report hat den Vorteil, dass eventuell im Report berechnete Werte mit exportiert werden können. Sie hat den Nachteil, dass unter bestimmten Bedingungen die exportierten Spalten nicht immer vollständig sind. Für sichere reproduzierbare Ergebnisse verwenden Sie daher bitte den eigens dafür geschaffenen CSV Export ![](Drucken_CSV_Export.gif).

##### Hinweise zur Übernahme nach Word:
Um einen Ausdruck nach MS-Word zu übernehmen exportieren Sie den Druck bitte zuerst im ODT Format (OpenDocument-Text). Dieses Format kann mit dem Writer von [Libre Office](https://de.libreoffice.org/download/download/) geöffnet werden. Speichern Sie diese Datei dann als *.DOC Datei um sie in MS-Word wiederum weiterverwenden zu können.

##### Übernahme am MAC nach OpenOffice / Libre Office
[Siehe.]( {{<relref "/docs/stammdaten/system/export_mac">}} )

## Hinweise zur Übernahme nach Excel(R)
Da wir immer wieder gefragt werden wie man das direkt nach Excel(R) übertragen kann, hier die Vorgehensweise dazu:<br>
Der wesentliche Punkt ist, dass man Excel mitteilen muss, aus welcher Datenquelle der Import aus der Zwischenablage stammt.<br>
D.h. nachdem Sie die Daten in die Zwischenablage kopiert haben, öffnen Sie ein neues Tabellenblatt.<br>
Klicken Sie nun mit der rechten Maustaste in das linke, oberste Feld, in das Sie die Daten importieren wollen. In der Regel wird das das Feld A1 sein.<br>
Wählen Sie nun unter den Einfügeoptionen<br>
![](Excel_Import1.gif)<br>
die Übernahme von Textdateien, also das Klemmbrett mit dem A.

Sollte die Übernahme, z.B. bei Tausendertrennzeichen u.ä. nicht Ihren Wünschen entsprechen, so muss die Verwendung des Textkonvertierungsassistenten definiert werden.<br>
D.h. nach dem ersten Einfügen finden Sie rechts unterhalb der eingefügten Daten<br>
![](Excel_Import2.gif)<br>
Klicken Sie auf den Pfeil und<br>
![](Excel_Import3.gif)<br>
haken Textkonvertierungs-Assistentenverwenden an.<br>
Definieren Sie hier, in den nachfolgenden Schritten, wie die Daten übernommen werden sollten. Üblich sind hier:
- Bitte definieren Sie, insbesondere für die richtige Übernahme von Umlauten und nicht Std.Ansi Zeichen, den UTF-8 Code
![](Excel_Import4.gif)

- Die Daten aus der Zwischenablage sind mit Tab(stopp) getrennt.
![](Excel_Import5.gif)

- Und gegebenenfalls sind Datumsspalten und auch Zahlenwerte, die Tausendertrennzeichen beinhalten, entsprechend zu definieren.
![](Excel_Import6.jpg)

## Können die Belege gespeichert und nachbearbeitet werden?
Zu dieser Frage, diesem Wunsch sind einige Dinge zu beachten:
- Wenn Sie z.B. eine Rechnung exportieren und danach nachbearbeiten, wozu wollen Sie dies machen
  - um die Rechnung zu verändern ?<br>
  Dieses Vorgehen, welches von manchen Systemen tatsächlich unterstützt wird, erachten wir als äußerst gefährlich, weshalb wir dies auch nicht weiter unterstützen.
  - um statistische Daten zu erhalten, diese für Auswertungen zu verwenden ?<br>Verwenden Sie bitte für den Export von Felddaten den oben beschriebenen CSV Export oder den Tab-Export bzw. die zahlreichen in **Kieselstein ERP** enthaltenen Journale und Auswertungen.
- kann das Aussehen / die Übergabe an z.B. ODT so aussehen wie im Report dargestellt?<br>Hier ist zu beachten, dass die offiziellen Formulare (Anfrage bis Rechnung und Mahnungen usw.) aus historischen Gründen so gestaltet sind, dass technisch gesprochen viele Felder übereinander gedruckt werden, was das gewohnte Druckbild ergibt. PDF-Generator und der eigentliche Druck kommen damit gut zurecht. Die Exporter wie CSV oder auch ODT sind Spaltenweise orientiert. D.h. nur wenn das Formular und seine Felder sehr akribisch ausgerichtet sind und keinerlei überlappende Felder definiert sind, kommt der ODT Export damit klar. D.h. Sie werden in der Regel nicht die Ergebnisse wie erwartet erhalten. Siehe dazu bitte auch oben wozu wollen Sie die Rechnung ergänzen?

#### Wann wird das EMail / das Fax versandt?
Im Sendedialog kann unter anderem auch der Sendezeitpunkt angegeben werden.
![](Sendezeitpunkt.gif)
Wir dieser leer belassen, so werden vom **Kieselstein ERP** Server zum aktuellen Sendezeitpunkt fünf Minuten hinzugerechnet und dieser Zeitpunkt als Sendezeitpunkt verwendet. Dies hat den Vorteil, dass, sollten Sie nach dem Senden bemerken, dass Sie dieses EMail, dieses Fax doch nicht versenden wollten, Sie innerhalb von fünf Minuten, gerechnet ab dem Zeitpunkt des Klicks auf Senden, über System, Versandaufträge, diesen Versandauftrag löschen können. (Basis für den Versandzeitpunkt ist die Zeit des Servers).

Tipp: Wenn Sie ein Email ein Fax sofort versenden wollen, so ist es am Einfachsten, wenn Sie zwar das Datum des Sendezeitpunktes auswählen, aber die Sendeuhrzeit leer lassen. Damit ist der Versandzeitpunkt bereits verstrichen und wird sofort versandt.

#### Was bedeutet ZUGFeRD Versand?
<a name="ZUGFeRD"></a>
Mit dem Versand einer Ausgangsrechnung im Zugferd Format wird die PDF Datei im PDF/A-3B Format mit integrierter XML-Datei nach ZUGFeRD Format Basic versandt. Bitte beachten Sie, dass dafür Ihre eigene UID-Nummer eingetragen sein sollte und dass Ihre Lieferantennummer beim jeweiligen Kunden eingetragen sein muss.
Der Empfänger bekommt somit eine PDF Datei die schreibgeschützt ist.
![](ZUGFeRD1.gif)

Zusätzlich finden Sie untern den Anlagen
![](ZUGFeRD2.gif) die eingebettete XML Datei mit den wesentlichsten Rechnungsdaten.

Um die Anlagen im Acrobat Reader zu öffnen klicken Sie auf den Pfeil am linken Rand ![](ZUGFeRD3.gif) (falls diese nicht schon geöffnet sind), bzw. ziehen Sie die linke Spalte etwas breiter / auf die richtige Breite, bzw. klicken Sie auf eines der Symbole am linken Rand.
![](ZUGfERD_Daten_anzeigen.png)<br>
Mit einem Rechtsklick kann die eingebettete XML Datei gespeichert werden.

Ähnlich ist die Vorgehensweise im Firefox.<br>
Klicke hier auf das ![](Firefox_eingebettete_Daten_vorhanden.png)<br>
![](ZUGfERD_Daten_anzeigen_FireFox.png)<br>
und dann auf die Büroklammer.

Mit diesen Daten kann der Rechnungsempfänger deine Rechnung auf einfache Weise in sein System importieren.<br>
Es ist dies eine sehr einfache Art des EDI (Electronic Data Interchange) also des elektronischen Datenaustausches.<br>
Bitte beachten Sie, dass es keine Garantie dafür gibt, dass der optische Inhalt des PDFs mit dem datentechnischen Inhalt des XMLs übereinstimmen muss. Mit entsprechend krimineller Energie, lassen sich diese beiden Darstellungen durchaus unterschiedlich gestalten. Die eigentliche Gefahr ist eine Veränderung am Wege vom Versender zum Empfänger.

**Hinweis:** Eine nach dem ZUGFeRD Format versandte Rechnung wird in der Dokumentenablage faktisch doppelt abgelegt. Einmal als Kopie der Rechnung und einmal als Kopie der erzeugten ZUGFeRD Datei.

Für den Empfang von elektronischen Rechnungen [siehe]( {{<relref "/einkauf/eingangsrechnung/er_erechnung/">}} )

#### Wie stelle ich eine firmeninterne Vorwahl zur Amtsleitung ein?
Es gibt im System, unterer Reiter Parameter, den Mandantenparameter AMTSLEITUNGSVORWAHL. Diese Vorwahl wird im Druckdialog nicht angezeigt, sondern erst beim Anlegen des Versandauftrages dazugefügt. Das bedeutet im im Modul System unterer Reiter Versandauftrag werden die Faxversandaufträge inkl. der Amtsleitungsvorwahl angezeigt.

#### Wo werden die Absenderadressen für EMail und Fax definiert?
Diese werden im Partner definiert. Siehe [Kommunikation im Partner]( {{<relref "/docs/stammdaten/partner/#kontaktdaten-privat-firmen-mandanten" >}} ).

#### Wo werden die Empfängeradressen für EMail und Fax definiert?
Diese werden durch den empfangenden Partner definiert.
Beispiel: Sie senden eine Rechnung an einen Kunden und haben in den Kopfdaten zusätzlich zur Rechnungsadresse einen Ansprechpartner definiert.

Als EMail Empfänger wird die EMail-Adresse des Ansprechpartners vorgeschlagen. Ist beim Ansprechpartner keine EMail-Adresse hinterlegt, so wird die EMail-Adresse des Rechnungsempfängers vorgeschlagen.

Bei der Fax Nummer wird ähnlich vorgegangen, es greift jedoch zusätzlich die Funktionalität der sogenannten Direkt-Faxnummer. D.h. ist beim Ansprechpartner eine Direktfaxnummer eingetragen, so wird diese Nummer vorgeschlagen. Ist nur die Fax-Durchwahl beim Ansprechpartner hinterlegt, so wird die Faxnummer der Rechnungsadresse herangezogen. Bei dieser wird der Teil Faxdurchwahl der Zentrale abgetrennt, siehe [Wie erfasst man Telefon und Faxnummern richtig]( {{<relref "/docs/stammdaten/partner/#wie-erfasst-man-telefon-und-faxnummern-richtig" >}} ), und durch die Faxdurchwahl des Ansprechpartners ersetzt. Die daraus resultierende Nummer wird vorgeschlagen.

Ist beim Ansprechpartner auch keine Faxdurchwahl definiert, so wird die Faxnummer der Rechnungsadresse vorgeschlagen.

#### Können mehrere Empfängeradressen für das EMail angegeben werden?
Ja. Trennen Sie die unterschiedlichen Adressen mittels ; Strichpunkt. z.B. info@kieselstein-erp.org;office@kieselstein-erp.org

#### Wo kann man Drucker definieren und auswählen (z.B. Als Standarddrucker oder Stockwerkdrucker)?
Die für den Ausdruck auf jedem Client zur Verfügung stehenden Drucker müssen im Client-Betriebssystem definiert werden.

Zusätzlich kann je Client-Computer und Report ein Standarddrucker für den jeweiligen Druck definiert werden.

Grundsätzlich wird bei einem Ausdruck der im Betriebssystem definierte Standarddrucker vorgeschlagen. Sollten nun ein anderer Drucker, z.B. für den Etikettendruck gewünscht werden, so wählen Sie anstelle des vorgeschlagenen Standarddruckers 

![](Drucker_Auswahl.gif)

den entsprechenden Etikettendrucker aus. Um diesen Drucker auf Ihrem Arbeitsplatz dauerhaft diesem Formular zuzuordnen klicken Sie bitte auf ![](Drucker_Speichern.gif). 

Um eine gewählte Druckerdefinition wieder auf Ihren Standard-Drucker zurückzusetzen, wählen Sie bitte ![](Drucker_Loeschen.gif).

Beachten Sie dazu bitte [auch]( {{<relref "/docs/stammdaten/system/druckformulare/#kostenstellen-druckersteuerung" >}} ).

Gleichzeitig mit der Abspeicherung des Druckers werden auch die gewählten Reportparameter abgespeichert. Das bedeutet, wenn Sie einen Report immer in einer bestimmten Einstellung, welche jedoch von den **Kieselstein ERP** Standardeinstellungen abweicht, bei Aufruf vorfinden möchten, so stellen Sie bitte den Report wie gewünscht ein und klicken Sie auf ![](Drucker_Speichern.gif) Speichern.<br>
Beim nächsten Aufruf des Reports werden die Reportparameter mit den von Ihnen gewählten Einstellungen vorbesetzt.

### Besonderheit zur Abspeicherung mit Datumsangaben:
Manchmal will man in der Statistik, z.B. Artikelstatistik die Daten immer ab einem bestimmten Zeitraum sehen, aber immer bis zum aktuellen Datum. Um dies zu erreichen geben Sie das von Datum ein, und lassen das bis Datum leer, bzw. löschen Sie das vorgeschlagene Datum. Speichern Sie nun die Druckeinstellungen und Vorbelegungen ab.

Wenn der Druck nun neu aufgerufen wird, wird das Bis Datum immer mit dem aktuellen Datum vorbesetzt.

Hinweis:

Dies Funktionalität steht nur für diejenigen Auswertungen zur Verfügung, bei denen das Bis Datum auch tatsächlich gelöscht werden kann.

#### Wieso ist der Speichernknopf neben der Formularauswahl grün?
Damit wird signalisiert, dass für die Einstellungen des Formulares bereits Einstellungen getroffen wurden. D.h. es werden Werte verwendet, die eventuell von den vorgeschlagenen Werten abweichen können. ![](Drucker_Auswahl_gespeichert.gif)

Für den Fall, dass das Auswertungsergebnis nicht Ihren Erwartungen entspricht, prüfen Sie daher bitte, ob Ihre/die Einstellungen so sind, dass auch tatsächlich Daten zu den gewählten Werten erzeugt werden können.

#### Mein neuer Drucker ist in der Auswahlliste nicht enthalten.
Die Drucker werden nur beim Start des **Kieselstein ERP** Clients geladen. D.h. wenn in der Auswahlliste der Drucker Ihr, vermutlich soeben neu definierter Drucker noch nicht enthalten ist, muss Ihr **Kieselstein ERP** Client neu gestartet werden.

#### Können auch unterschiedliche Formulare hinterlegt werden?
Ja es können je Formular weitere Mutationen / Varianten der Formulare hinterlegt werden.

Dazu finden Sie neben dem Druckerauswahlknopf die Formularauswahl.

![](Drucken_Formularauswahl.gif)

Diese ist üblicherweise mit <Standard> (dem Standard Formular) vorbesetzt. Es können nun für jedes Formular weitere Mutationen eingerichtet werden. [Siehe dazu.]({{< relref "/docs/stammdaten/system/druckformulare/druckformular_varianten.md" >}})

Bitte beachten Sie, dass beim Speichern der Druckeinstellungen die Formularauswahl mit abgespeichert wird. D.h. es wird eine Verbindung zwischen Drucker und Formular hergestellt. Dies ist gerade bei Etikettendrucken sehr praktisch, da ja in der Regel jedes Etikett auf einen anderen Drucker gedruckt wird.

#### Können auch verschiedene Druckerschächte angesteuert werden?
Die Aufgabenstellung hier lautet üblicherweise, dass die erste Seite z.B. auf das offizielle Firmenpapier und die Folgeseiten auf ein weißes oder ein anderes Formular gedruckt werden sollten. Üblicherweise kommen heutige Druckertreiber mit einer Funktionalität, in der eingestellt werden kann, dass das erste Blatt eines Ausdruckes auf einen bestimmten Schacht geht und die Folgeseiten auf einen anderen Schacht. Wenn dies für Sie die richtige Funktionalität ist, so ist die Vorgehensweise wie folgt:
- installieren Sie einen eigenen / zusätzlichen Drucker(treiber) für diese Art von offiziellen Formularen
- definieren Sie diesen so, dass die erste Seite auf Ihr Briefpapier geht und die Folgeseiten auf z.B. weißes Papier.
- Definieren Sie diesen Druckertreiber als den zu verwendenden Drucker z.B. beim Druck der Rechnung.<br>
Info: Sollte Ihre Drucker dies nicht unterstützen, so gibt es inzwischen auch verschiedene Softwareprodukte, welche dies Aufgabe (print on different paper trays) erledigen.

Zusätzlich stehen für die offiziellen Belege zwei unterschiedliche Druckerdefinitionen zur Verfügung.
Damit kann definiert werden auf welchen Drucker das Original und auf welchen Drucker die Kopien gedruckt werden.
![](Kopien_Drucker1.gif)
So bedeutet diese Einstellung dass sowohl das Original als auch die Kopie auf den Drucker (HP LaserJet 4000) gedruckt werden.

Einstellung für unterschiedliche Drucker:
![](Kopien_Drucker2.gif)
Bei dieser Einstellung wird das Original auf den Farbdrucker (= Drucker (HP7610)) und die Kopie(n) auf den LJ4000 der der Kopiendrucker ist, gedruckt.

Diese Einstellung wird ebenfalls mit dem Einstellungen-Speichern Knopf mit abgespeichert. Somit können Sie dies, am Besten ohne den Kopien-Einstellungen (also leer), für die jeweiligen Formulare wie gewünscht hinterlegen.

#### Können auch zweiseitige Drucke genutzt werden?
Mit der Funktionalität des Kopien-Druckers können auch zweiseitige gerne auch doppelseitige Drucke genutzt werden.
D.h. für den doppelseitigen Druck stellen Sie bitte sowohl den Drucker für das Original auf den Original Drucker. Wenn nun die Kopien auf den physikalisch gleichen Drucker, aber als eigene Einstellung gespeichert werden, so wird von **Kieselstein ERP** ein eigener Druckauftrag an diesen Drucker erzeugt und somit werden auch ungerade Seitenanzahlen bei zweiseitigen Drucken möglich.
Bitte beachten Sie auch hier, dass, wenn der Kopienzähler bereits vorbesetzt sein sollte, dieser VOR dem Speichern gelöscht werden muss, da Sie sonst automatisch für jede Rechnung diese Kopienanzahl, egal wie sie vom Kundenstamm her definiert ist, aufgrund der Übersteuerungslogik verwenden würden.

#### Woher kommt der EAN-Code / der Barcode?
In **Kieselstein ERP** steht Ihnen bei allen Drucken die Verwendung eines Barcodes zur Verfügung.
Üblicherweise wird dies für den Ausdruck von Artikelnummern, Losnummer, Auftragsnummern usw. verwendet.
EAN-Code:
Der EAN-Code ist einer unter einer Vielzahl von verschiedenen Barcodes.
Die EAN steht für International Article Number (früher European Article Number) und ist eine Produktkennzeichnung für Handelsartikel. Die EAN ist eine Zahl, bestehend aus 13 oder 8 Ziffern, die zentral verwaltet und an Hersteller auf Antrag vergeben wird und auf Handelsartikel (z.B. im Lebensmittelhandel) aufgedruckt wird.
Jeder Barcode hat seine spezifischen Merkmale und seine spezifischen Einsatzgebiete.
Von **Kieselstein ERP** wird üblicherweise der Code 128 verwendet, da dieser den Vorteil hat, dass damit Großbuchstaben, Ziffern und einige wenige Sonderzeichen in relativ kompakter Form als Strichcode kodiert ausgegeben werden können.

Definition der Barcodetype [siehe]( {{<relref "/docs/installation/10_reportgenerator/ireport/#definition-der-barcodetype" >}} )

Als sehr praktischer Barcode, der vor allem von mobilen Geräten wie Handys, Mobile Telefonen, Tabletts u.ä. sehr gut und sehr schnell unterstützt wird hat sich der QR-Code, also ein zwei dimensionaler Barcode sehr bewährt. Selbstverständlich kann auch dieser Barcode von Ihrem **Kieselstein ERP** erzeugt werden. Der wichtigste Punkt dazu ist, dass die beteiligten Barcode-Lesegeräte diesen Code auch lesen können müssen.

#### Was ist bei der Einrichtung der Barcodedruckertreiber zu beachten?
[Siehe bitte](barcodes.htm#Barcodedruckertreiber).

#### Was passiert, wenn die EMail-Adresse geändert wird, während jemand im Druckmenü ist?
Aus Geschwindigkeits- und Netzwerktraffic- gründen werden die Versanddaten nur beim Aufruf des Druckes / des Druckdialoges ermittelt. Das bedeutet, wenn, während Sie im Druckdialog stehen, die EMail oder Fax Daten Ihres Kunden, Ihres Ansprechpartners verändert werden, so werden diese Informationen erst durch den erneuten Aufruf des Druckens aktualisiert.<br>
Bitte beachten Sie, dass der Refresh Knopf nur für den Inhalt des Reports, aber nicht für die den Report umgebenden Daten gilt.

**Tipp:** Wurde eine Faxnummer oder eine EMail Adresse geändert, so schließen Sie bitte den Druckdialog und rufen danach den Ausdruck erneut auf.

#### Drucken auf Briefpapier / unterschiedliche Seiten usw.
Die Ansteuerung unterschiedlicher Druckerschächte können Sie in **Kieselstein ERP** über verschiedene Drucker realisieren. D.h. wenn z.B. die Rechnung auf Briefpapier, aber die Packliste auf weißes Papier gedruckt werden soll, so muss auf jedem Arbeitsplatz ein Drucker installiert werden, welcher die Ausdrucke automatisch an den richtigen Druckerschacht sendet. Das bedeutet also, dass Sie den Standarddrucker (=weißes Papier) und einen eigenen Drucker(treiber) für das Briefpapier installiert haben müssen. Für den Druck der Rechnung wählen Sie im Druckdialog von **Kieselstein ERP** den passenden Briefpapierdrucker aus und klicken auf speichern. Damit wird für Ihren Client-Computer diesem Formular der Briefpapierdrucker zugeordnet und die Rechnung an diesen Drucker gesandt.
Einrichten des selben Druckers mit unterschiedlichen Eigenschaften siehe [Tipps und Tricks](../Client/tipps_und_tricks.htm#Unterschiedliche Eigenschaften am gleichen Drucker).

#### Es wurde ein neuer Drucker eingerichtet, er ist aber in der Druckerauswahl nicht sichtbar?
Aus Performance Gründen werden die am Client installierten Drucker nur beim Start des Clients eingelesen. D.h. wenn ein neuer Drucker nach dem Start des **Kieselstein ERP** Clients installiert wurde, so ist dieser nicht sichtbar. Damit er sichtbar wird, beenden Sie bitte den **Kieselstein ERP** Client und starten Sie ihn erneut. Nun ist er sichtbar.

#### Das hinterlegte Firmenlogo ist nicht sichtbar?
Üblicherweise werden farbige Logos auf Briefpapier gedruckt. D.h. es darf bei Ausdrucken das Logo nicht mitgedruckt werden. Wird der Ausdruck aber per Fax oder EMail versendet so ist dies trotzdem anzuführen.
Wenn nun, z.B. für Testzwecke das Logo trotzdem angeführt werden soll, so schalten Sie dies in den Mandantenparametern (System, Parameter) unter LOGO_IMMER_DRUCKEN ein (Wert = 1).

#### Wie können AGB mitgesandt bzw. mitgedruckt werden?
<a name="AGB mitdrucken"></a>
Im Modul System, unterer Reiter Mandant, oberer Reiter AGB kann ein entsprechendes PDF hinterlegt werden, welches bei den unterschiedlichen offiziellen Belegen mitgedruckt bzw. als EMail-Anhang mit gesandt werden kann.

#### Warum werden manche Drucke automatisch gestartet und andere erst manuell?
Wenn ein Druck über Kriterien verändert werden kann, wird er erst manuell gestartet, damit Sie nicht unnötig auf Reports warten müssen, die Sie nicht bestellt haben und um dem Server überflüssige Arbeit zu ersparen. Alle anderen Drucke kommen automatisch.

#### Warum ist das Datum am Druck 'falsch'/nicht deutsch geschrieben?
Wenn es sich um einen Belegdruck handelt, kann es sein, dass der Kunde/Lieferant eine andere Sprache hinterlegt hat. Überprüfen Sie auch, ob Sie mit der richtigen Sprache eingeloggt sind (Sichtbar ist das in der Titelleiste von **Kieselstein ERP** 'UI-Sprache:' [siehe auch]( {{<relref "/start/10_mehrsprachigkeit">}} ).)

#### Im Druck werden Formatierungsbefehle angezeigt, im **Kieselstein ERP** Client sind diese nicht ersichtlich
Es kann bei sehr alten Texten aus Ihren Daten vorkommen, dass intern die Formatierungen noch falsch abgespeichert wurden. Um diese zu korrigieren, bearbeiten Sie bitte die Texte, indem Sie die Formatierungen entfernen, danach neu eintragen und danach speichern. Durch das Speichern werden die internen Formatierungsanweisungen richtiggestellt und damit auch beim Ausdrucken entsprechend dargestellt.

![](Falsche_Formatierungen.jpg)

#### Druck von Etiketten
Für den Druck von Etiketten ist es in manchen Firmen üblich, A4 Etikettenvorlagen mit z.B. 24Etiketten (Zweckform, Herma, ...) zu verwenden. Diese A4 Vorlagen haben den Vorteil, dass sie mit üblichen Laserdruckern ausgedruckt werden können. Sie haben aber den gravierenden Nachteil, dass praktisch kein einzelnes Etikett ausgedruckt werden kann.

Warum werden einzelne Etiketten benötigt?

In der Regel werden die Artikel etikettiert. Das hat den Zweck, zu erkennen, wie alt Artikel sind, um so immer die ältesten Artikel zuerst zu verwenden. Damit erreichen Sie, dass das in **Kieselstein ERP** implementierte FiFo Prinzip unterstützt wird und sie so automatisch die Zuordnung der Wareneingänge zu den Warenausgängen und umgekehrt erhalten.

Um dieses FiFo Prinzip beibehalten zu können, muss nach der einmal erfolgten Etikettierung / Auszeichnung jeder Wareneingang sofort gekennzeichnet werden. Das bedeutet es muss für jeden Artikel eines Wareneingangs ein einzelnes Etikett ausgedruckt werden. Wenn Sie dies nun mit den 3x8 Etiketten-Formularen auf einem Laserdrucker machen, ist das Etiketten-Formular spätestens nach dem dritten Druckvorgang unbrauchbar, weil schwarz, im Drucker hängen geblieben usw.. D.h. spätestens zu diesem Zeitpunkt kommen Drucker zum Einsatz, die einzelne Etiketten drucken können. Wir haben uns daher dafür entschieden nur den Druck einzelner Etiketten zu unterstützen. Bei der Entscheidung welcher Drucker und welche Etiketten für welchen Anwendungsfall am Besten geeignet ist, unterstützen wir Sie gerne.

#### Was ist bei der Einstellung der Etikettendrucker zu beachten?
Unter anderem achten Sie bitte darauf, dass das entsprechende Etiketten-Format nicht nur im Etikettenformular eingestellt ist, sondern unbedingt auch in den Druckeinstellungen des Etikettendruckers. Beachten Sie dies unbedingt auch dann, wenn Etiketten gewechselt werden, wobei der Wechsel von Etiketten-Typen / Größen in aller Regel vermieden werden sollte. Werden laufend Etiketten unterschiedlicher Formate gedruckt, so sollten mehrere Etikettendrucker verwendet werden.

Unter Windows finden Sie für die Zebradrucker (mit Seagull-Treibern) die Etikettendefinitionsmöglichkeiten in den Druckereigenschaften, oberer Reiter Allgemein, dann Druckeinstellungen, Reiter Seite einrichten. Definieren Sie hier ein neues Etikett mit für Sie sprechendem Namen oder definieren Sie zumindest das User Etikett so, dass die Definition zu den tatsächlichen Abmessungen Ihrer Etikette passt.

#### Überleitung von Ausdrucken / Listen in ansprechende Grafiken
Durch die CSV Exportfunktion ![](Drucken_CSV_Export.gif) jedes Ausdruckes, kann auch der Dateninhalt jedes Ausdruckes für weiterführende Auswertungen verwendet werden. Zur Verwendung von Pivot-Tabellen [siehe](Pivottabelle_erzeugen.pdf).

#### Wie passen die Breite des Texteditors und die Breite des Ausdrucks zusammen?
[Siehe bitte](../Allgemein/freieTexte.htm#Breite_Texteditor_Einstellen).

#### Die Vorwahl der Faxnummer wird doppelt / falsch übergeben.
<a name="Vorwahl richtig angeben"></a>
Die Faxnummer des Partners wurde z.B. mit  +4 9 /36 8xx xx angegeben.

Die Telefonnummer wurde ähnlich angegeben und wurde in der Ansicht des Lieferanten/Kunden in rot bzw. blau dargestellt.

Wurde nun ein Faxversand angestoßen, so wurde die Ländervorwahl doppelt angegeben.

Z.B. 0049 04 9 /36 8xx xx

Ursache, da die Ländervorwahl nicht erkannt wurde, dies wird auch durch die rote Telefonnummer angezeigt, wird automatisch die richtige Vorwahl des Landes vor der Faxnummer dazugesetzt.

Korrigieren Sie die Ländervorwahl, sodass sie der Definition des Landes entspricht. Nun wird die Telefonnummer richtig angezeigt.

![](Falsche_Faxnummer1.gif)

Kommt von

![](Falsche_Faxnummer2.gif)

Hier muss das Leerzeichen zwischen 4 und 9 entfernt werden.

Die internationale Vorwahl muss so eingestellt werden wie unter System, System, Land für das jeweilige Länderkennzeichen definiert.

![](Falsche_Faxnummer3.gif)

#### Der Versanddienst bleibt bei Serienemails immer wieder stecken. Was tun?
Der **Kieselstein ERP** Versanddienst übernimmt die Daten aus den **Kieselstein ERP** Versandaufträgen, konvertiert diese und schickt diese weg.<br>
Der Versanddienst prüft die **Kieselstein ERP** Versandaufträge jede Minute auf neue Aufgaben und führt diese dann entsprechend aus.<br>
Wird nun eine große Anzahl von Serienemails / Serienfaxen versandt, so wird durch die Konvertierung in die verschiedenen Formate eine entsprechende Rechnerleistung und auch entsprechend Speicher für diese Aufgabe benötigt. So wird z.B. für den Faxversand der Acrobate-Reader gestartet und mit diesem das Fax entsprechend gedruckt und dem jeweils weiterführenden Dienst übergeben. Ist nun der Rechner entsprechend wenig leistungsfähig, so kommt es immer wieder vor, dass der Versanddienst so langsam wird, dass man den Eindruck hat, er sei abgestürzt. Die Ursache liegt jedoch in der Leistungsfähig des den Versanddienst ausführenden Rechners und seiner Leistungsfähigkeit.<br>
Wenn Sie öfters große Massenmailings machen, sollten Sie überlegen einen entsprechend leistungsfähigen Rechner mit dieser Aufgabe zu betrauen.

<a name="EMail-Versand fehlgeschlagen"></a>

#### Ich bekomme die Meldung dass mein EMail-Versand fehlgeschlagen ist
Es wird bei jedem Öffnen eines neuen Moduls geprüft ob der Versand Ihrer EMails durchgeführt werden konnte. (Bitte prüfen Sie die Versandaufträge!)
![](Versandauftrag_Fehlgeschlagen.jpg)
Sind EMails unter Postausgang hängen geblieben bzw. mit Fehlgeschlagen beendet worden erscheint eine entsprechende Meldung.
Diese gliedert sich in die eigenen nicht versendeten bzw. Fehlgeschlagenen und die allgemeinen nicht versendeten bzw. Fehlgeschlagenen, damit Sie auch einen Kollegen der dies nicht mitbekommt unterstützen können.
Die Vorgehensweise dazu ist immer, dass Sie im Anschluss an diese Meldung die Ursache eruieren und beseitigen und dann
- die noch nicht versendeten automatisch versandt werden oder von Ihnen gelöscht werden
- bzw. die Fehlgeschlagenen faktisch zur Bestätigung aus den Fehlgeschlagenen gelöscht werden.
Die Versandaufträge finden Sie im Modul System, unterer Modulreiter Versandauftrag. Sollten Sie selbst keine / zu wenig Rechte besitzen um diese durchzuführen, bitten Sie Ihren Hausinternen HELIUM V Verantwortlichen dies für Sie durchzuführen.

Bei Anzeige der Meldung wählen Sie bitte wann Sie erneut daran erinnert werden möchten. Diese Info bleibt solange erhalten, als der **Kieselstein ERP** Client geöffnet ist. D.h. wird ein neuer Client gestartet, erscheint die Meldung erneut.

Die Anzahl der jeweiligen Versandaufträge errechnet sich aus dem Wunschsendezeitpunkt plus 5Minuten. D.h. wird ein EMail nicht innerhalb von 5Minuten, ausgehend vom Sendezeitpunkt, der üblicherweise jetzt plus 5Minuten ist, versandt, kommt bei der nächsten Öffnung eines Moduls obenstehende Meldung.

**Warum ist das so?** Hintergrund ist immer, dass man zeitnah darüber informiert werden möchte, dass der Versand eines EMails nicht funktioniert hat. Nun kann es auch vorkommen, dass schnell noch EMails versandt werden, nach Hause gegangen wird und dann zum Schluss trotzdem jemand darüber informiert werden muss, dass der Versand, warum auch immer nicht funktioniert hat. Daher zeigen wir diese Meldung für alle Benutzer an.

#### Kann ein Serien-HTML-EMail aus **Kieselstein ERP** erzeugt werden ?
Aus **Kieselstein ERP** kann ein Serienemail erzeugt werden. Siehe dazu Partner, Serienemail.