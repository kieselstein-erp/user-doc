---
title: "Anreden"
linkTitle: "Anreden"
categories: ["Partner"]
tags: ["Partner", "Anreden"]
weight: 200
description: >
  VerwaltungDefinition der Anreden
---
Anreden in **Kieselstein ERP**
===================

#### Wozu das Ganze?
Der Sinn der Anrede Definitionen ist, dass von **Kieselstein ERP** automatisch Ihre Partner (Kunden, Lieferanten) richtig angesprochen werden. Nun sind hier in verschiedenen Ländern verschiedene Arten üblich. Wir haben daher den Stil der Anreden zusätzlich sprachabhängig gemacht. Siehe dazu weiter unten.

Die verschiedenen Anreden werden vor allem auf den Druckformularen für die richtige Anrede verwendet.

Sie können im Partnermodul die entsprechende Anrede für diesen Partner, diese Adresse überprüfen.

Die unterschiedlichen Arten und ihre Verwendung sind:

|  |  | Auftragsformular | Beispiel Firma | Beispiel Ansprechpartner zu Firma (als Adresse) | Beispiel Einzelkunde(als Ansprechpartner) |
| --- |  --- |  --- |  --- |  --- |  --- |
| Adresse |  |  | Albrecht-Dürer-Straße 182008 Unterhaching |  | Herr Ing. Werner Hehenwarter |
| Ländername |  |  | Deutschland |  |  |
| Anrede | Anrede |  | eMundo GmbH | Herrn Dipl.Inf. Günther Palfinger | Herrn Ing. Werner Hehenwarter |
| Briefanrede | Briefanrede |  | Wenn kein Ansprechpartner für diese Firma / diesen Beleg verfügbar ist, dann erfolgt die Briefanrede mit:Sehr geehrte Damen und HerrenAnsonsten entsprechend der Regel für den Ansprechpartner | Sehr geehrter Herr Dipl.Inf. Günther Palfinger | Sehr geehrter Herr Ing. Werner Hehenwarter |
| Direkte Anrede | Fix: Titel Name1 Name2 |  | eMundo GmbH | Herr Dipl.Inf. Palfinger Günther | Herr Ing. Werner Hehenwarter |
| Anrede mit Titel | Titel Anrede |  | eMundo GmbH | Günther Palfinger | Herr Ing. Werner Hehenwarter |
| Vollständige Anrede | Fix: Anrede Titel Name2 Name1 |  | eMundo GmbH | Herr Dipl.Inf.Günther Palfinger | Herr Ing. Werner Hehenwarter |
| Einfache Anrede ohne Titel | Fix: Name2 Name1 |  | eMundo GmbH | Günther Palfinger | Werner Hehenwarter |

Die Unterscheidung der Art der Anrede wird über die Anrede im Partner-Detail ![](anrede_Art.gif) gesteuert.
Wird hier Herr oder Frau angegeben, so erfolgt eine persönliche Anrede, wird hier Firma oder \<leer> angegeben, so erfolgt die Firmenanrede.

Der Adressblock in den Formularen gliedert sich in vier Teile:

|  | Beispiel Firma | Beispiel Einzelkunde |
| --- |  --- |  --- |
| Adressat | eMundo GmbH | Herr Ing. Werner Hehenwarter |
| Ansprechpartner 1) | Herrn Dipl.Inf. Günther Palfinger |  |
| Straße und Ort mit Postleitzahl bzw. Postfach | Albrecht-Dürer-Straße182008 Unterhaching | Fichtlstr. 55321 Koppl |
| Ländername | DEUTSCHLAND |  |

Beim Beispiel Einzelkunde bleibt der Ländername leer, da im Beispiel der Versand vom Österreichischen Mandanten aus erfolgt.

1.) Ansprechpartner: Bitte beachten Sie hier, dass für den Ansprechpartner eines Kunden die Regeln des Ansprechpartners gelten und diese mit der Kundenadresse entsprechend obiger Reihenfolge verknüpft werden.

Beispiel Endkunde

![](anrede_Endkunde.gif)

Beispiel Firmenkunde

![](anrede_Firmenkunde.gif)

Beispiel Ansprechpartner

a.) Aus der Ansprechpartnersicht beim Kunden

![](anrede_ansp_a.gif)

b.) aus der Sicht als Partner

![](anrede_Ansp_b.gif)

Der Adressblock wird in den Bewegungs-Formularen (Angebote, ...) als P_KUNDE_ADRESSBLOCK automatisch zur Verfügung gestellt.

Der Zeilenaufbau ist wie oben angeführt.

Mit P_MANDANTADRESSE werden Anrede, Länderkennzeichen, Adresse in einer Zeile, als Absenderinfo zur Verfügung gestellt.

Mit P_BRIEFANREDE wird die Briefanrede zur Verfügung gestellt.

Hinweis: Da es in den unterschiedlichen Ländern üblich ist, die Ansprechpartner unterschiedlichst anzusprechen wird auch dies von der Anredeautomatik unterstützt. D.h. wenn Ihre Zielsprache entsprechend ist, werden auch die üblichen Formen unterstützt.

Derzeit gibt es für die Briefanrede folgende Formen, ausgehend von obigem Beispiel des Ansprechpartner Palfinger:

| Sprache | Briefanrede |
| --- |  --- |
| Deutsch | Sehr geehrter Herr Dipl.Inf. Günther Palfinger |
| Englisch | Dear Mr. Palfinger |
| Französisch | Monsieur |
| Italienisch |  |

Grundsätzlich orientiert sich die Gestaltung der Anschrift an der DIN 5008 (Stand April 2005), welche definiert:

1.  Keine Leerzeilen in der Anschrift

2.  Keine Länderkennzeichen

3.  Der Ort wird in der Landessprache geschrieben

4.  Der Ort wird in Versalien (Großbuchstaben) geschrieben

5.  Das Bestimmungsland steht ohne Leerzeile in deutscher Sprache (das bedeutet für **Kieselstein ERP** in der Mandantensprache) in Versalien (Großbuchstaben) direkt unterhalb des Bestimmungsortes.

<u>Beispiel:</u>

    Madame

    Clodette Linton

    Rue du Cheval 13

    53454 LIEGE

    BELGIEN

#### Können Anreden ergänzt werden?
Ja.

In den von uns ausgelieferten Anreden sind Herr, Frau, Firma enthalten. Für manche Anwender ist z.B. die Anrede Familie noch wichtig. Bitten Sie in diesem Fall Ihren **Kieselstein ERP** Betreuer dies in der Basis-Anredetabelle zu ergänzen. Die eventuell gewünschten Übersetzungen können dann wiederum über das Modul Partner, unterer Modulreiter Grunddaten, oberer Reiter Anrede in der sprachabhängigen Bezeichnung ergänzt werden.