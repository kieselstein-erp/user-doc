---
title: "Telefonanbindung"
linkTitle: "Telefonanbindung"
categories: ["Partner"]
tags: ["Partner", "Telefonanbindung"]
weight: 200
description: >
  Anbindung von ausgehenden Telefoniefunktionen
---
Telefonanbindung
================

In **Kieselstein ERP** steht auch die direkte Wahl aus der Adresse zum Telefon-Partner zur Verfügung.
Üblicherweise wird dies mit der so genannten Tapi-Schnittstelle (TelefonApplikationInterface) realisiert.
Zusätzlich steht auch ein sogenannter HTTP Request zur Verfügung. Siehe dazu.

Überall wo die Tapi-Anbindung zur Verfügung steht, finden Sie neben der Telefonnummer das Telefonsymbol ![](KontaktManagement6.gif) um den Gesprächspartner direkt anzurufen, ohne die Telefonnummer neu eintippen zu müssen.

Für die Wahl der richtigen Nummer achten Sie bitte darauf, dass:
a.) die Telefonnummer des Partners entweder international eingegeben ist (+43-6225-28088-0)
b.) Ihrem Mandanten über das Länderkennzeichen die richtige Kennung für das Land eingetragen ist. Damit wird entschieden, ob eine internationale oder eine nationale Vorwahl verwendet werden muss.
c.) Dass die Mandantenparameter

| Parameter | Bedeutung |
| --- |  --- |
| Amtsleitungsvorwahl | Vorwahl um mit Ihrer Telefonanlage eine Amtsleitung zu holen. In sehr vielen Fällen ist dies die 0.<br>ACHTUNG: Wirkt nicht bei TELEFONWAHL_HTTP_REQUEST |
| Inlandsvorwahl | Umsetzung des internationalen Ländercodes auf die tatsächliche führende Ziffer(n).Z.B. +43-6225-28088-0 wird 06225-.. |
| Auslandsvorwahl | Umsetzung des internationalen Ländercodes auf die tatsächliche führende Ziffer(n).z.B.: +49-711-21720058-0 wird 0049-711-... |

richtig eingestellt sind.

Für die eingehende TAPI Integration siehe bitte [HVTE](Telefonanbindung_eingehend.htm) (**H**ELIUM **V** **T**elefonie **E**ingehend)

Tapi
----

Für die Einrichtung der Tapi Schnittstelle sind einige Voraussetzungen und Parametrierungen durchzuführen, welche in Abstimmung mit Ihrem Telefonanlagenbetreuer und Ihrem **Kieselstein ERP** Betreuer durchgeführt werden. Diese müssen auf dem einzelnen Arbeitsplatz eingerichtet werden.

Parametrierung:

Aktuell steht die TAPI Anbindung nur (mehr) für Windows Betriebssysteme zur Verfügung. Es wird dazu die getrennt verfügbare HVTE verwendet.
Der Arbeitsplatzrechner muss Zugriff auf dieses Windows-Programm haben. Die Parameter müssen dem entsprechend (siehe Beispiel) eingegeben werden. Von **Kieselstein ERP** wird zu diesem Aufruf am Schluss noch die zu wählende Telefonnummer mit übergeben.
Es werden alle Zeichen (außer den Ziffern 0-9) aus der Telefonnummer entfernt.

D.h. mit der von uns zur Verfügung gestellten [HVTE](Telefonanbindung_eingehend.htm) **H**ELIUM **V** **T**API **E**ingehend steht auch die Möglichkeit der Wahl über Tapi zur Verfügung. D.h. nach erfolgter Konfiguration der HVTE muss noch die Wahl über die HVTE aus Ihrem jeweiligen **Kieselstein ERP** Client heraus konfiguriert werden.
Nutzen Sie dazu den Arbeitsplatzparameter PFAD_MIT_PARAMETER_TAPITOOL:
![](TapiLineDefinition_HVTE.gif)
Beachten Sie bitte dass der unter Wert angegebene Pfad zum entsprechenden Rechner passen muss, also von da aus tatsächlich die entsprechende HVTE aufgerufen werden kann.

Hinweis:

Die Tapi Anbindung ist so, dass davon ausgegangen wird, dass Telefon und Rechner eine logische Einheit bilden. D.h. Rechner und Telefon stehen nebeneinander und bei Anruf sollte das Telefon neben ihrem Rechner die Verbindung aufbauen.
In diesem Zusammenhang bedenken Sie bitte auch, dass bei der Verwendung von Remote-Sitzungen die HVTE in benutzerspezifischen Verzeichnissen eingerichtet werden muss um jedem Benutzer sein Telefon zuweisen zu können.

Http-Request
------------

Mit dem Http-Request werden die Daten für den Telefonanruf per http-Aufruf direkt an Ihr Telefon übergeben. Üblicherweise sind dies VoIP (Voice over IP) Telefone, wie z.B. snom. Wir haben dafür den Arbeitsplatzparameter <TELEFONWAHL_HTTP_REQUEST> geschaffen.

Auch hier ist die Idee, dass neben jedem Arbeitsplatz ein dazugehörendes Telefon steht. 

Wichtig: Um den Aufruf absetzen zu können, muss in Ihrem Snom Telefon die normalerweise per default eingerichtete Benutzerberechtigung entfernt werden. Solange Ihr VoIP Telefon in einem gut geschützten internem Netzwerk steht sollte das kein Problem darstellen.

Um den Benutzer zu entfernen, gehen Sie in die Administrationsoberfläche Ihres Snom Telefons, unter Erweitert, QoS / Sicherheit und löschen Sie den unter HTTP Server eingetragenen Benutzer und dessen Passwort.

![](Snom_Benutzer_loeschen.gif)

Klicken Sie anschließend auf speichern.

Der Aufrufstring für Snom 3er Serie sieht wie folgt aus:

<http://Snom-IP-Adresse/index.htm?NUMBER=###NUMMER###&DIAL=Wählen>

Snom-IP-Adresse ersetzen Sie bitte durch die IP-Adresse des jeweiligen Telefons,

der Bereich ###NUMMER### wird von **Kieselstein ERP** durch die zu wählende Nummer ersetzt.

Da der Aufrufstring frei definiert werden kann, können selbstverständlich alle anderen Telefone / Telefonanlagen, welche mit einem derartigen Request gesteuert werden können von **Kieselstein ERP** angesteuert werden.

Http-Request mit Telefonanlage
------------------------------

Ergänzend, erweiternd zu obigem, ist bei einer Einbindung der Telefone in eine Telefonanlage (z.B. Starface) ein Zugriff auf das Telefon in Abstimmung mit der Telefonanlage erforderlich. Hier muss man sich üblicherweise mit den Parametern passend zur Anlage anmelden.

D.h. der Aufrufstring für die vorgelagerte Anmeldung sieht wie folgt aus:

**|Benutzer|Password|http://Snom-IP-Adresse/command.htm?NUMBER=###NUMMER###&DIAL=Wählen**

z.B.: |admin|0000|http:/192.168.8.17/command.htm?NUMBER=0###NUMMER###&DIAL=Wählen

Snom-IP-Adresse ersetzen Sie bitte durch die IP-Adresse des jeweiligen Telefons, der Bereich ###NUMMER### wird von **Kieselstein ERP** durch die zu wählende Nummer ersetzt.
Benutzer ersetzen Sie bitte durch einen auf dem Telefon eingerichteten Benutzer.
Password ist das Passwort des Benutzers auf dem Telefon.

**WICHTIG:** Die Amtsleitungsvorwahl greift hier bewusst NICHT, da dies in der Regel von der Telefonanlage gemacht wird. Gegebenenfalls tragen Sie die benötigte Amtsleitung vor den ersten ###NUMMER... ein.

Starface

Bei Starface finden Sie in der Administrationsoberfläche, Telefone, erweiterte Einstellungen die durch die Telefonanlage vergebenen Benutzer und Passwörter.
Alternativ kann für eine Starface-Telefonanlage auch der Aufruf des Starface-Clients verwendet werden.
Üblicherweise erstellen Sie dafür eine Cmd-Shell (Batch) Datei in der
    start /MIN /I "Stf-Call" "c:\Program Files (x86)\STARFACE Client\STARFACE Client.exe" PlaceCall %1%2%3%4%5%6%7%8%9
enthalten ist.
Tragen Sie die Batchdatei wie oben beschrieben unter dem Arbeitsplatzparameter PFAD_MIT_PARAMETER_TAPITOOL ein.
![](Tapi_Starface.jpg)
In diesem Beispiel ist der Inhalt der caller.cmd wie oben (start ....) beschrieben.

Tipp: Es muss zuerst der Aufruf / die Verbindung Ihres Arbeitsplatztelefones mit der Starfaceanlage definiert sein. D.h. in der Taskleiste muss ![](Starface.gif) das Starface-Icon entsprechend die Verbindung zu Ihrer Telefonanlage haben. Dieses erhalten Sie durch Installation des [ STARFACE-Clients](https://knowledge.starface.de/display/wiki62/STARFACE+Client+for+Windows).
Bitte beachten Sie, dass ab Version 6 der Starface der Starface UC Client verwendet werden muss. Dieser ist entsprechend zu Parametrieren (Menü: Starface, Einstellungen). Der Aufruf lautet entsprechend StarfaceUcClient.exe /PHONENUMBER=. Bitte vor der Exe die entsprechende Pfadangabe mit dazu geben. "C:\Programme\Starface\ ....."

nfon Cloud Telefonanlage

Von nfon wird unter anderem auch eine sogenannte Telefonanlage in der Cloud zur Verfügung gestellt. Werden bei dieser SNOM Telefone verwendet, so ist der Benutzername die Durchwahl des Benutzers. Das Password entspricht der voicemailpin. Beides kann in der Administrationsoberfläche definiert werden.

Ob Sie mit den angenommenen Benutzer und Passwort tatsächlich Zugriff auf Ihr Telefon bekommen, prüfen Sie am Besten über einen Browser, einfach durch aufruf von <http://Snom-IP-Adresse>. Danach kommt die Abfrage von Benutzer und Password. Hier geben Sie die ermittelten Daten ein. Sie müssen nun in der Administrationsoberfläche Ihres Telefons sein.

Pimphony
--------

Die Telefonanlagen von Alcatel bzw. NextiraOne werden oft über die sogenannte Pimphony Schnittstelle angesteuert. Bis zu Windows-XP 32Bit emuliert diese auch eine Tapi-Schnittstelle. Ab Windows7 64Bit müssen folgende Voraussetzungen gegeben sein, damit die Pimphony direkt angesprochen werden kann.

1.  Pimphony Version 6.6., Build 2710 vom 5.10.2012 oder höher (vermutlich) muss installiert sein.
    Für die Installation zuerst alle alten Pimphony Programme deinstallieren und danach den Tapi Treiber 64Bit (install_tsp_64.exe) und dann das Install.exe ausführen.

2.  Den Pfad auf das Pimphony Programm = aocphone.exe herausfinden. Ist üblicherweise: c:\Program Files (x86)\Alcatel_PIMphony\aocphone.exe

3.  Diesen in **Kieselstein ERP**, System, Arbeitsplatzparameter, PFAD_MIT_PARAMETER_TAPITOOL für den jeweiligen Rechner eintragen.

4.  Die Amtsleitungsvorwahl(en) harmonisieren. Entweder aus **Kieselstein ERP** verwenden oder den der Pimphony. Wenn aus dem Pimphony, dann in **Kieselstein ERP**, System, Parameter, AMTSLEITUNGSVORWAHL_TELEFON auf ein Leerzeichen (Space) stellen.

5.  Stellen Sie sicher, dass das Pimphony beim Start von Windows mitgestartet wird.

Hinweis: 

Für die Neu-Installation werden die Telefonpasswörter benötigt.

Phoner
------

Einige Telefonanlagen unterstützen zwar kein Tapi und kein http, können aber z.B. mit dem dialer.exe unter Windows(R) ferngesteuert werden. Da die Dialer.exe nicht wirklich gut per Kommandozeile gesteuert werden kann, kann das Freewaretool [Phoner](http://phoner.de/download_de.htm) verwendet werden.
Installieren Sie dieses Programm auf Ihrem Rechner und tragen Sie die Aufrufparameter für das Programm mit absoluter Pfadangabe unter System, Arbeitsplatzparameter, PFAD_MIT_PARAMETER_TAPITOOL für den jeweiligen Rechner ein.

Teams
-----

Um mit Teams ausgehende Telefonanrufe aus **Kieselstein ERP** machen zu können, muss neben der Installation von Teams und der Anbindung von Teams an einen Telefonie-Service-Provider auch die Übergabe der Standard Telefonie über das Betriebssystem eingerichtet sein.
Für Windows gehen Sie wie folgt vor:

1.  Einrichten der Telefonie-App auf Ihrem Windowsrechner, also an welches "Telefon" sollte die Rufnummer übergeben werden.

2.  Windowstaste (Startbutton), Einstellungen (das Zahnrad)

3.  Suchen Sie nach Standard-Apps

4.  Wählen Sie hier die Telefon-App.
    Sollte diese nicht verfügbar sein, so klicken Sie auf Standard Apps nach Protokoll auswählen
    Suchen Sie danach nach TEL
    ![](Teams1.gif)
    und klicken auf Standard wählen und wählen
    ![](Teams2.gif)
    erneut Teams.
    Damit haben Sie definiert, dass Teams für die ausgehende Telefonie verwendet wird.

5.  Nun tragen Sie in den Arbeitsplatzparametern für den Parameter TELEFONWAHL_TEL_REQUEST den Wert:
    tel://###NUMMER###
    ein.
    ![](Teams3.gif)

6.  Somit wird bei Klick auf den Telefonhörer diese Telefonnummer an Teams übergeben und es erscheint der Dialog
    ![](Teams4.jpg)
    den Sie mit Klick auf Anrufen bestätigen.