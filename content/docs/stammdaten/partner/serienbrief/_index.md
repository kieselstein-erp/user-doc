---
title: "Serienbrief"
linkTitle: "Serienbrief"
categories: ["Partner"]
tags: ["Partner", "Serienbrief", "Mailings"]
weight: 200
description: >
  Serienbriefe nach verschiedensten Kriterien erstellen
---
Serienbrief
===========

Im Modul Partner findest du im unteren Modulreiter Serienbrief ein komfortables Werkzeug zur Aufbereitung von sogenannten Massenaussendungen, wie z.B. Newsletter.

Die Serienbrieffunktion ist eng mit den Selektionen verknüpft, welche bei Kunden, Lieferanten und Partnern hinterlegt werden können.

Zusätzlich erhält man im Menüpunkt Info, Empfängerliste eine praktische Liste, welche selbstverständlich auch exportiert werden kann, aus der alle Empfänger ersichtlich sind und mit welcher Kommunikationsart du diese Empfänger erreichst.

#### Wozu dienen die Selektionen
Mit den Selektionen können Sie Partner nach Gruppen definieren / zusammenfassen. Die verfügbaren Selektionen können im Modul Partner, unterer Modulreiter Grunddaten, Modulreiter Selektion definiert werden. Legen Sie hier die von Ihnen gewünschten / benötigten Selektionen an. Z.B. Maschinenbau, Transporteur, Weihnachtskarten, ... .

Die Selektionen werden hauptsächlich für gezielte Mailings verwendet um individuelle, personifizierte Massenmailings zu ermöglichen.

Die Selektionen werden manchmal auch als Merkmale bezeichnet.

Im Reiter negative Selektionen, wählen Sie die Selektionen aus, die ausgeschlossen werden sollen.

Weiters können Sie Selektionen als logisches Oder oder Und miteinander verknüpfen, so erhalten Sie zum Beispiel alle Maschinenbau-Unternehmen, aber ohne Weihnachtkarten-Empfänger. Oder können genauer alle Maschinenbau-Unternehmen, die mehr als 100 Mitarbeiter haben selektieren.

![](selektion_und_oder.JPG)

**Hinweis:**

Im Gegensatz zu den reinen Partnerdaten, welche mandantenunabhängig sind, sind die Daten der Selektionen für jeden Mandanten definierbar. Damit können unterschiedliche Selektionen für den jeweiligen Mandanten definiert und hinterlegt werden.

<a name="Serienbriefe"></a>

#### Wie kann ich gezielte Aussendungen (Mailings) an ausgewählte Kunden / Partner versenden? Serienbriefe:
Dafür wurde die Funktionalität Serienbriefe erstellt. Diese finden Sie im Modul Partner.

Mit den Serienbriefen können Sie anhand der gewählten Kriterien Mailings, per Brief, Fax oder EMail an ausgewählte Partner versenden. Damit können Sie eine gezielte Ansprache bestimmter Kundengruppen erreichen.

Zum Erstellen eines Serienbriefes gehen Sie wie folgt vor:

Im Modul Partner wählen Sie den unteren Modulreiter Serienbrief.

Legen Sie nun einen neuen Serienbrief an. In den Kopfdaten vergeben Sie einen entsprechenden Namen für diesen Serienbrief / diese Marketingaktion. Über die Auswahl der Ansprechpartnerfunktion können Sie gezielt Ihre Adressaten (z.B. Einkauf, Geschäftsleitung) auswählen / ansprechen.

Mit "Wenn kein Ansprechpartner ..." können Sie definieren, dass ein Kunde der keinen passenden Ansprechpartner hat, trotzdem in die Mailingliste mit aufgenommen wird.

Über die Eingabe (A*) PLZ können entsprechende Postleitzahlenfilter gesetzt werden. So bringt z.B. 5 nur die Treffer deren Postleitzahl mit 5 beginnt.

Wird Kunden angehakt, so werden alle Kunden aufgelistet.

Wird Interessenten angehakt, so werden (ev. zusätzlich) alle Interessenten (siehe Kunden) aufgelistet.

Plus Versteckte liefert zusätzlich alle als versteckt markierten Kunden/Interessenten/Partner.

Unter dem Modulreiter Selektionen können Sie die gewünschten Selektionen für dieses Mailing angeben. Wurden hier keine Selektionen eingegeben, so werden alle Partner verwendet.

Im Modulreiter Text geben Sie den gewünschten Text für Ihre Aussendung ein.

Wenn Sie nun im Modulreiter Kopfdaten ändern wählen, und dann auf aktualisieren klicken, so erhalten Sie wiederum die Partner, auf die diese Filterkriterien zutreffen.

Durch einen Klick auf aktualisieren erhalten Sie eine Liste der Partner die mit diesem Mailing angeschrieben werden. Wenn erneut auf aktualisieren geklickt wird, so wird die Liste (ev. mit den neuen Kriterien) am Anfang der Darstellung erneut angezeigt.

Löschen leert das Darstellungsfenster.

Im Darstellungsfenster 

![](Serienbriefliste.gif)

sehen Sie, bei welcher Kommunikationsform der Kunde angeschrieben wird und bei welcher nicht. Achten Sie hier darauf, dass für alle gewünschten Kunden die entsprechenden Kommunikationsdaten (Fax, EMail) hinterlegt sind.

Verwenden Sie dafür gegebenenfalls auch die Empfängerliste unter Info.

#### Anschreiben von Kunden die einen gewissen Umsatz haben
Haken Sie in den Kopfdaten des Serienbriefs alle Kunden an ![](Serienbrief4.gif) und definieren Sie nun Ab Umsatz, bis Umsatz mit dem gewünschten Zeitraum.

![](Serienbrief5.gif)

Damit erhalten Sie nur die Kunden, die im angegebenen Zeitraum den entsprechenden Umsatz gehabt haben.

Für die Trennung in Kunden mit einem Umsatz bis z.B. 10.000,- € und darüber führen Sie diese Funktion zweimal aus, wobei einmal bis Umsatz und das andere mal Ab Umsatz entsprechend definiert ist.

<a name="Empfängerliste"></a>

#### Wie kann man eine Liste aller Partner / Kunden / Lieferanten ausgeben?
Über die Serienbrief-Funktionalität kann man direkt eine Empfängerliste als *.csv-Datei speichern, und diese z.B. in Excel importieren.

Erstellen Sie, wie [hier](#Serienbrief) beschrieben einen Serienbrief und wählen Sie die gewünschten Partner, Lieferanten oder Kunden aus. Sie brauchen keinen Text erstellen, da Sie nur an der Empfängerliste interessiert sind.

Unter Menü Info > Empfängerliste bekommen Sie eine Liste mit der gerade ausgewählten Partner, Lieferanten und Kunden.

#### Ich brauche eine Liste mit allen Ansprechpartnern und allen Firmen
Auch hier steht die Serienbrief-Funktion zur Verfügung.

Wählen Sie ![](Serienbrief1.gif) alle Partner mit zugeordneten Firmen.

Dies bewirkt, dass grundsätzlich von den Partneradressen ausgegangen wird. Ist ein Partner aber ein Ansprechpartner bei einem Unternehmen, so wird als Anschrift die Anschrift des Unternehmens verwendet und nur der Partner nur als Ansprechpartner angeführt. Ist ein Partner bei mehreren Unternehmen als Ansprechpartner eingetragen, so erscheint dieser entsprechend oft, da er ja für diese Firmen entsprechend zuständig ist.

<a name="Lieferantenliste"></a>

#### Lieferanten Liste
Manchmal benötigt man eine Liste aller Lieferanten.

Diese erhalten Sie am Einfachsten über die Funktion Serienbrief.

D.h. starten Sie bitte das Modul Partner ![](Partner.jpg), wechseln Sie in den unteren Modulreiter Serienbrief ![](Serienbrief2.gif).

Legen Sie nun einen neuen Serienbrief an, z.B. Lieferanten und definieren Sie unter Kopfdaten, dass nur Lieferanten (alle und mögliche) verwendet werden sollen.

![](Serienbrief3.gif)

Speichern Sie die Serienbrief Definition ab.

Unter dem Menüpunkt Info, Empfängerliste erhalten Sie eine Aufstellung aller Lieferanten und deren Adressen.

#### Ausdrucken von Serien-Etiketten
Verwenden Sie dafür ebenfalls die Empfängerliste, mit der gewünschten Serienbrief Definition und wählen Sie dann bei der Formularvariante z.B. Etikette1 ![](Serienbrief6.gif)

#### Wie kann ich meine Partnerdaten nach Outlook exportieren?
Z.B. für die Synchronisation mit einem Handy ist es erforderlich, dass die Partnerdaten in Outlook übernommen werden. Neben der Verwendung der [HV-EST](../Schnittstellen/index.htm#HV-EST) steht auch die Möglichkeit der Verwendung des Serienbriefes zur Verfügung.

1. definieren Sie einen [Serienbrief](#Serienbrief) der die gewünschten Partner beinhaltet.

2. wählen Sie im Serienbrief, Info, Empfängerliste und exportieren Sie diese Liste im CSV Format ![](PartnerExport1.gif).

3. Im Speicherndialog vergeben Sie einen sprechenden Namen z.B. Outlookexport.txt. Bitte auf die Erweiterung txt achten.

4. Starten Sie eine Tabellenkalkulation (z.B. Excel oder Open Office) und öffnen Sie die Datei.

5. Beim Importformat wählen Sie Spalten durch Tabstop getrennt.

6. Nun haben Sie eine Liste mit allen exportierten Daten. Entfernen Sie die nicht benötigten Spalten.

7. Speichern Sie die die Datei als CSV (Trennzeichen getrennt)

8. Starten Sie Outlook (Express), Wählen Sie Datei, Importieren, Anderes Adressbuch.

9. Wählen Sie Textdatei mit Kommas als Trennzeichen

10. Ordnen Sie nun die Felder zu

![](PartnerExport2.jpg)

D.h. es muss nun die Übersetzung der Feldnamen aus **Kieselstein ERP**, genauer aus der Exportdatei der Empfängerliste bzw. den von Ihnen in der Tabellenkalkulation benannten Spaltenüberschriften auf die Felder in Outlook durchgeführt werden.

11. Klicken Sie auf Fertigstellen um die Daten in Ihr Outlook Adressbuch zu übernehmen.

#### Kann man auf doppelte EMail-Adressen prüfen?
Ja, mit Hilfe der Serienbrieffunktion und einer Tabellenkalkulation. Gehen Sie dazu wie folgt vor:

Partner, unterer Reiter Serienbrief, neuer Serienbrief z.B. Herbst-Aussendung

Dann alle Partner anhacken und den Serienbrief speichern.

Nun gehen Sie auf Info, Empfängerliste und klicken Sie auf CSV Export ![](CSV_Export.gif). Speichern Sie nun die Datei im CSV Format mit der Erweiterung .TXT (z.B. Aussendung.txt). Öffnen Sie diese Datei nun mit einem Tabellenkalkulationsprogramm, im Dateityp Textdatei und Import mit Trennzeichen TAB. Nun sortieren Sie die Tabelle nach der EMail-Adresse. Sie sehen alle doppelten. Ganz elegant ist, wenn man in einer zusätzlichen Spalte eine Formel einbaut, die dies automatisch prüft =WENN(H1=H2;1;"") dann erhalten Sie die jeweils mehrfache Adresse gekennzeichnet. H ist die Spalte in der die EMailadresse steht.

#### Wie kann das EMail-Anschreiben des Serienbriefes gestaltet / gesteuert werden?
Im Serienbrief kann im Menü unter Bearbeiten EMailtext der Text dieses EMails definiert werden.

Die Vorbelegung dieses Textes kommt wieder aus der allgemeinen Definition für die EMail-Texte.

Eine Besonderheit stellt hier die richtige / persönliche Anrede des Ansprechpartners dar.

Es wird dafür der Platzhalter ###BRIEFANREDE### verwendet. Dieser Platzhalter wird durch die in **Kieselstein ERP** übliche Briefanrede (Sehr geehrter Herr Ing. Werner Hehenwarter) ersetzt. Somit kann direkt im EMail Ihr Ansprechpartner persönlich angesprochen werden.

#### Wie erstellt man einen Newsletter?
<a name="Newsletter"></a>
Der Versand eines Newsletters erfolgt über die Serienbrieffunktion. Gehen Sie dazu wie folgt vor:

Definieren Sie beim Ansprechpartner, wer den Newsletter erhalten soll. Setzen Sie dazu einen Haken bei Newsletterempfänger.

![](newsletterempfaenger.PNG)

Falls bei dem Ansprechpartner keine E-Mail Adresse hinterlegt ist, so erscheint folgende die Fehlermeldung.

![](fehler_newsletter.PNG)

Pflegen Sie die E-Mail Adresse ein und klicken erneut auf speichern.

Nun wechseln Sie in den Reiter Serienbrief im Modul Partner und erstellen einen neuen Serienbrief. Setzen Sie auch hier den Haken für Newsletter.

![](newsletter.PNG)

Anschließend wählen Sie die gewünschte Gruppe aus (Kunde/Partner/Lieferant/...), bei Bedarf können Sie auch nach Land, Branche etc. filtern.

Speichern Sie den neuen Serienbrief und überprüfen die Adressauswahl über Info: Empfängerliste.

Gehen Sie nun [für den Versand](#Serienbriefe) wie oben beschrieben weiter vor. Bzw. [siehe auch](#der-newsletter-geht-auch-an-abgemeldete-empfänger)

#### Was mache ich, wenn ich von der Ansprechpartnerfunktion keine Empfänger beim Unternehmen habe?
Es kann passieren, dass eine Nachricht an alle Unternehmen zb. an die Geschäftsführung versandt werden soll. Wenn nun aber kein Ansprechpartner mit der Funktion Geschäftsführer hinterlegt ist, so würde das Unternehmen die Nachricht nicht erhalten. 

Setzen Sie in diesem Fall einen Haken bei ![](serienbrief_wennkeinAP_1.JPG).

Das bewirkt, dass wenn kein Ansprechpartner mit der entsprechenden Funktion gefunden wird, der 1\. gereihte als Empfänger verwendet wird.

#### HTML Serienemails
<a name="HTML Serienemails"></a>

#### Kann ich Serienbriefe im HTML-Format per EMail versenden?
Es steht auch die Funktion Serienbriefe im HTML Format senden zur Verfügung. Bitten Sie Ihren **Kieselstein ERP** Betreuer diese für Sie freizuschalten.
In der Auswahlliste der Serienbriefe finden Sie, nach der Freischaltung, den HTML-Neu-Button ![](HTML-Serienbrief.png). Durch Klick auf diesen Knopf wird ein neuer Serienbrief, welcher als HTML Serienbrief gekennzeichnet ist angelegt. Sie erkennen dies auch am etwas anderen Aussehen des Textes, welcher im unteren Bereiches der Kopfdaten des jeweiligen Serienbriefes dargestellt wird.
Hier finden Sie einen einfachen Standard HTML Editor mit dem Sie direkt die HTML Texte erstellen und externe Links einbinden können.
Da die Erstellung von schönen HTML Seiten oft auch sehr komplex sind und oft von (externen) Grafikabteilungen gemacht werden, haben wir auch die Möglichkeit eines direkten Importes geschaffen. Klicken Sie auf ![](HTML_Serienbrief_import.gif) HMTL Text importieren um HTML Daten, welche Sie vorher in der Zwischenablage Ihres Betriebssystems abgelegt haben, zu übernehmen. Bei diesem Import wird geprüft ob es sich wirklich um einen HTML Inhalt mit all seinen Formatierungen handelt. Wenn nicht erscheint ein entsprechender Hinweis. Bitte beachten Sie, dass einige EMail-Programme obwohl HTML Emails angezeigt werden nur Text zur Verfügung stellen.
Beim Versand des HTML EMails werden, im Gegensatz zu den normalen Serienbriefen, keine Anhänge erzeugt, sondern direkt HTML Dateien an die jeweiligen Empfänger versandt.

Unterschied zwischen selbst erstelltem HTML Text und importiertem HTML.

-   In den selbst erstellten HTML Text können Links und Links auf öffentlich verfügbare Bilder eingefügt werden. Der direkte Import von Bildern ist nicht vorgesehen. Der HTML Text wird automatisch um die Briefanrede (am Beginn des Textes) und um Ihre Absenderdaten ergänzt. Für die Anpassung der Absenderdaten wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

- Für das mit ![](HTML_Serienbrief_import.gif) importierte HTML wird davon ausgegangen, dass es sich dabei um ein vollständiges HTML handelt, welches Sie z.B. in einem entsprechenden (Grafik-)Programm erstellt haben. Daher kann dieses nicht mehr bearbeitet werden. Um nun trotzdem z.B. die Briefanrede in diese Datei zu bekommen stehen folgende Variablen zur Verfügung:
    - P_BRIEFANREDE ... die Formulierung für den Brief z.B. Sehr geehrter Herr Müller,
    - P_ANSPRECHPARTNERTELEFON ... die Telefonnummer des Ansprechpartners
    - P_ANSPRECHPARTNERFAX ... die Faxnummer des Ansprechpartners
    - P_ANSPRECHPARTNEREMAIL ... die EMailadresse des Anpsrechpartners
    - P_BETREFF ... der Betreff des Serienbriefes
    - P_NAME1 ... Ansprechpartner Nachname
    - P_NAME2 ... Ansprechpartner Vorname
    - P_NAME3 ... Ansprechpartner Abteilung
    - P_ANREDE ... Ansprechpartner Anrede
    - P_ANREDE_FIX ... Titel, Vorname, Nachname
    Um diese Variablen im HTML zu verwenden müssen diese in @@ eingebettet sein. So wird z.B. @@P_BRIEFANREDE@@ durch die Formulierung Sehr geehrter Herr Müller, ersetzt. Damit können Sie das fixe HTML auch entsprechend persönlich gestalten.

#### Bedienung des HTML Editors
Der Editor ist wie üblich zu bedienen. Beim Einfügen von Links ![](HTML_Editor1.gif) und Bildern ![](HTML_Editor2.gif) beachten Sie bitte, dass **beide** für den Empfänger des EMails zugreifbar sein müssen. Dies sind sie in der Regel, wenn die Bilder öffentlich im World Wide Web angezeigt werden können. Beim Einfügen von Bildern werden Sie auch nach Breite/Höhe gefragt. Diese können in Pixel angegeben werden, können aber auch frei gelassen werden und danach das Bild mit der Maus in seiner Größe entsprechend angepasst werden.

#### Kann ich aus den Empfängern des Serienbriefes auch die Wiedervorlage befüllen?
Wenn auf Ihrem **Kieselstein ERP** die Zusatzfunktion Kontagmanagement aktiviert ist, so steht in den Kopfdaten des Serienbriefes auch die Überleitungsfunktion für die Wiedervorlage zur Verfügung. D.h. mit Klick auf ![](Wiedervorlage_befuellen.gif) Kontakte zur Wiedervorlage anlegen, werden für alle Partner-Adressen des Serienbreifes die danach abgefragten Daten in der Wiedervorlage eingetragen. Damit können Sie sehr effizient alle Adressen des Serienbriefes in die Wiedervorlage übernehmen und so anhand der Wiedervorlage die angeschriebenen Adressen direkt nachakquirieren.

#### Wie neue Preislisten / Unterlagen versenden ?
Für den Versand von allgemeinen Unterlagen wie z.B. Preislisten schlagen wir folgende Vorgehensweise vor:
- Veröffentlichen der Preisliste auf einer deiner Webseiten, jedenfalls an einer vom Web aus erreichbaren Position
- nutzen den HTML Serienbrief um ein passendes Anschreiben im HTML Format zu erstellen.
- Verweise hier mit einem Link auf das gewünschte Dokument

Vorteil dieser Vorgehensweise. Du sendest nur ganz wenige wirkliche Daten und kannst gegebenenfalls auch Tracken wer sich dein neues Dokument tatsächlich angesehen hat.

### Der Newsletter geht auch an abgemeldete Empfänger
Die Frage des Anwenders war, warum geht bei nachfolgender Serienbriefeinstellung der Newsletter trotzdem an den abgemeldeten Ansprechpartner raus?

Der Hintergrund ist, dass die Hakerl
![](Newsletter_hakerl01.png)  
logische Oder sind.

D.h. diese Definition bewirkt, dass das EMail an alle geht, die Kunden oder Interessenten sind ODER die im Newsletter angemeldet sind.
Darum erhält ein abgemeldeter Ansprechpartner nach wie vor den Newsletter EMail.
![](Newsletter_abgemeldet02.png)  

Für den Test, für z.B. den nächsten Versuch würde ich, vor dem echten Versand, die Empfängerliste
![](Newsletter_empfaengerliste01.png)  <br>
verwenden.<br>
Auch für die Kontrolle in beiden Richtungen: Bekommen den Newsletter alle die ihn bekommen sollten und die die ihn nicht wollen eben nicht.

Hinweis: Ist bei einem Ansprechpartner, einem Partner (Kunden, Lieferanten usw.) der Newsletter nicht definiert ![](Newsletter_nicht_definiert.png)  <br>
so bekommt dieser, auch wenn nur Newsletter angehakt ist, KEINE Newsletter, da ja der Stand nicht definiert ist. Das bedeutet, wenn man sich dazu durchringt diese Definition zu verwenden, dann muss es auch, so wie es in der DSGVO definiert ist, für jeden Ansprechpartner definiert werden. Laut DSGVO musst du auch noch einen Nachweis dafür haben, dass dir der Partner dafür die Erlaubnis mit eine *Double Opt In* erlaubt hat.

Um nun den Versand der Newsletter **nur** anhand der Einstellung Newsletter angemeldet ![](Newsletter_angemeldet.png)  vorzunehmen muss
![](Newsletter_alle_angemeldet.png)  alle Partner und Newsletter angehakt sein.<br>
Bitte beachte, dass damit NUR die Ansprechpartner / Partner einen Newsletter bekommen, bei denen auch ein Newslettergrund angemeldet eingetragen ist.

#### Kann ich Anhänge mitsenden?
Der Wunsch des Anwenders war, dass mit dem Serien-EMail neue Preislisten z.B. als PDF-Datei versandt werden sollten. Um diese Funktionalität zu erreichen, kann man z.B. wie folgt vorgehen. Als Beispiel versenden wir den Link auf den Mitgliedsantrag der **Kieselstein ERP eG**:
- Erstellen der (PDF-)Datei
- Hochladen auf den eigenen Web-Server
- Erstellen des HTML Serienbriefes im Modul Partner, unterer Reiter Serienbrief, oberer Reiter Auswahl, Klick auf HTML Serienbrief ![](HTML-Serienbrief.png) (Sollte dieser Button nicht zur Verfügung stehen, so fehlt die dafür erforderliche Zusatzfunktionsberechtigung -> einschalten (lassen))
- Erstellen des EMail Textes -> **ACHTUNG:** Bevor du lange Texte abspeicherst, speichere zuerst den leeren Serienbrief und bearbeite ihn danach. Als Parameter / Variablen können die oben definierten verwendet werden, wie z.B. @@P_BREIFANREDE@@
- Füge nun einen Link zu deiner PDF-Datei auf deinem Webserver ein, indem du ein passendes Wort markierst und dann auf Link ![](HTML_Link_einfuegen.png) klickst.<br>
![](HTML_Link_definieren.png).
- Nun noch die üblichen Serienbriefdefinitionen vornehmen, und die Prüfung der Empfänger durch Info Empfängerliste vornehmen.

#### Wo kann man Newsletter An-/Abmelde Gründe definieren.
Siehe dazu bitte Partner, Grunddaten, Newsletter Grund.<br>
Hier können beliebig viele Anmelde bzw. Abmelde Gründe eingegeben werden, z.B. Hat sich bei einer Messe angemeldet. Entscheidend ist im Endeffekt ![](Angemeldet.png)  ob das angemeldet angehakt ist.