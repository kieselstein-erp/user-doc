---
title: "HTML Serienbrief"
linkTitle: "HTML"
categories: ["Partner"]
tags: ["Partner", "Serienbrief", "Mailings", "HTML"]
weight: 250
description: >
  HTML Serienbrief erzeugen
---
Eine kurze Beschreibung, mit welchen Mitteln und Tools man einen passenden HTML Serienbrief für das **Kieselstein ERP** System erzeugen kann.

Für die Aktivierung der HTML Funktion [siehe]

1. Erstellen der Vorlage unter Libre-Office-Writer
2. Speichern unter HTML
3. öffnen mit einem brauchbaren HTML Editor. Ich nutze den alten Microsoft Expression Web 4 Editor.
4. Ausbessern der Umlaute und Sonderzeichen
5. Bilder wie gewünscht positionieren, Größe angeben (in Prozent) und **wichtig** absolute Links auf im Web verfügbare Grafikdateien angeben. Es werden diese Bilder als externe Inhalte übertragen. Eingebettete Bilder sind nicht möglich.<br>
**Info:** Die Prozentangaben sind immer problematisch, da jeder Browser, EMail Viewer dies anders darstellt. Besser ist, wenn die Bilder so richtig 1:1 verwendet werden können. Also die Größen bereits passend zum EMail definiert sind.
6. Den HTML Inhalt, im MS Expression Web 4 Code genannt, vollständig in die Zwischenablage kopieren.
7. Den HTML-Serienbrief im **Kieselstein ERP** anlegen, also Partner, unterer Reiter Serienbrief, in der Auswahlliste ![](HTML-Serienbrief.png), dann die Kopfdaten erstellen und speichern.<br>
Nun auf ändern klicken, womit das HTML Import Icon freigeschaltet wird und damit den HTML Text aus der Zwischenablage in den Serienbrief einfügen.

**WICHTIG:**<br>
Unbedingt den Versand komplett durchgängig bis hin zum ausgehenden EMail prüfen. Da sind so viele Konverter und Programme beteiligt, die das alle unterschiedlich machen.

## Welche Einstellungen sind für HTML EMail einzurichten
- Es muss in den Zusatzfunktionsberechtigungen der EMAIL_CLIENT aktiviert sein
- Definition der [XSL EMail Vorlagen]( {{<relref "/docs/stammdaten/system/druckformulare/#html-email-definieren" >}} ) [bzw.]( {{<relref "/docs/stammdaten/partner/serienbrief/#html-serienemails" >}} )
