---
title: "Partner CSV Import"
linkTitle: "CSV Import"
categories: ["Partner"]
tags: ["Partner", "Import"]
weight: 200
description: >
  Partner Import
---
Partner CSV Import
==================

#### Wozu Import
Mit der Importfunktion haben wir eine elegante und einfache Möglichkeit geschaffen die Partnerdaten im **Kieselstein ERP** mit fremden Daten abzugleichen.

Wir gehen für den Import davon aus, dass die Daten im unten beschriebenen Format angeliefert werden.

Bitte beachten Sie, dass Selektionen und Ansprechpartnerfunktionen bereits angelegt sein müssen. An Anreden sind nur die in der Partner Definition angeführten (Herr, Frau, <leer>) gültig.

Führen Sie auf alle Fälle vor dem eigentlichen Import den Prüflauf durch. Beheben Sie die angestimmten Unstimmigkeiten und importieren Sie danach die Daten.

Bitte beachten Sie, dass wir Partner mit übereinstimmenden Adressdaten als bereits bestehenden Partner betrachten und nur seine Daten ergänzen. Damit ist es möglich beliebig viele Ansprechpartner je Partner zu importieren.

Übereinstimmende Adressdaten bedeutet, dass die Felder Anrede, Name1, Name2, Name3, Straße, LKZ, PLZ, Ort exakt übereinstimmen müssen. Ist das der Fall, so wird dies als bereits angelegter Partner erkannt und die weiteren Daten eingetragen, bzw. bestehende Daten, z.B. Telefonnummern überschrieben.

Der Datenaufbau der CSV Datei ist so, dass in der ersten Zeile die Spaltenüberschriften anzugeben sind. Die Spalten sind in folgender Reihenfolge anzuordnen:

<a name="CSV Import"></a>Anrede; Titel; Name1; Name2; Name3; Strasse; Land; Postleitzahl; Ort; Telefon; Fax; EMail; Homepage; Bemerkung; Selektion; Ansp-Funktion; Ansp-Anrede; Ansp-Titel; Ansp-Vorname; Ansp-Nachname; Ansp-TelefonDw; Ansp-FaxDw; Ansp-EMail; Ansp-Mobil; Ansp-Gültigab; Ansp-Bemerkung; Ansp-Direktfax; Partnerklasse; Branche; ILN; Filialnummer; Uid; Gerichtsstand; Firmenbuchnummer; Postfach; Geburtsdatumansprechpartner; GMT-Zeitversatz; Kontonummer; PostfachLand; PostfachPostleitzahl; PostfachOrt; Kurzbezeichnung; Spediteur; Lieferart; Zahlungsziel; KDLieferantennr; KDKundennummer; LFKundennummer

Ansp- ... steht für Ansprechpartner
Ansp-Gültigab im Format JJJJMMTT (Jahr, Monat, Tag)

Kontonummer:
Es wird, wenn ein Lieferant importiert wird, diese Kontonummer als Kreditorennummer hinterlegt (OHNE PRÜFUNG der Parameter der Nummer). 
Wenn ein Kunde importiert wird, wird diese Kontonummer als Debitorennummer übernommen.

Besonderheit Kurzbezeichnung:
Wenn es im XLS-File als Spalte angegeben wird, dann wird es übernommen, wenn nicht, dann wird die Kurzbezeichnung generiert.

Das CSV Format ist so definiert, dass als Trennzeichen zwischen den Feldern ein ; (Strichpunkt oder auch Semikolon) und eventuell ein " (Doppeltes Hochkomma) zur Texterkennung übergeben wird.
Bitte achten Sie darauf, dass die Datei im Ansi-Format zur Verfügung gestellt wird.

Der Import des Partners wird in zwei Schritten durchgeführt:

1. Prüfung der Daten: Hier wird überprüft, ob alle erforderlichen Daten (Pflichtfelder) in der übergebenen CSV Datei enthalten sind. Nach der Überprüfung erhalten Sie eine Liste der fehlenden Daten.

![](Partner_Import.gif)

Ist die Liste mit Fehlern behaftet, so müssen diese zuerst korrigiert und die Daten dann neu eingelesen werden.

![](Partner_Import2.gif)

Ist die Liste fehlerfrei, so kann auch auf Importieren geklickt werden.

Sollten beim Import der Daten auch Kunden erzeugt werden, so haken Sie bitte erzeuge Kunde an. Dadurch werden neben den Partnerdaten die Daten auch in den Kunden übernommen. Die für den Kunden erforderlichen Pflichtfelder werden, wie bei der manuellen Anlage von Kunden, die Einstellungen aus System, Mandant, Vorbelegungen übernommen.

Zusätzlich zu den Kunden, auch Lieferanten (Daten) erzeugt werden. Setzen Sie dazu den entsprechenden Haken bei ![](erzeuge_Lieferant.JPG).

Die Ansprechpartner Funktion muss so wie die Kennung unter Partner, Grunddaten, Ansprechpartnerfunktion definiert sein.

Haben Sie viele fehlerhafte Einträge die überarbeitet werden sollten, so können Sie den Inhalt des Fehlerfensters mit Strg+A komplett markieren und mit Strg+C in die Zwischenablage übernehmen.

Import im XLS Format

Neben dem oben beschriebenen CSV Format steht auch ein XLS Import zur Verfügung.
Der wesentliche Unterschied ist, dass hier nur die Spaltenüberschriften definieren, in welche Felder welche Spalten übernommen werden sollten.
Grundsätzlich sind die Spaltennamen wir unter [CSV Import](#CSV Import) definiert. Bitte beachten Sie, dass die Spaltenüberschrift für die Straße als Strasse mit ss geschrieben ist. Dies sollte den Import auch bei unterschiedlichen Sprachen deutlich erleichtern.

Können Partnerdaten aktualisiert werden oder werden diese immer neu angelegt?

Beim Import können auch Daten aktualisiert werden. Unbedingte Voraussetzung dafür ist, dass alle drei Namen und die Straße gleich geschrieben sind. Ist dies der Fall, so wird angenommen dass dies der gleiche Partner ist und seine Daten werden aktualisiert, z.B. Ansprechpartner hinzugefügt, anderenfalls wird der Partner neu angelegt. Siehe auch [Zusammenführen](index.htm#Partner zusammenführen).