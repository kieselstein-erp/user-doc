---
title: "Telefonie eingehend"
linkTitle: "Telefonie eingehend"
categories: ["Partner"]
tags: ["Partner", "Telefonie eingehend"]
weight: 200
description: >
  Eingehende Telefonie-Anbindung
---
**Kieselstein ERP** Telefonie Eingehend
============================

Mit diesem nützlichen Werkzeug steht, derzeit nur für Rechner mit einem Windows Betriebssystem, die Verbindung zwischen Ihrer Telefonanlage und Ihrer **Kieselstein ERP** Datenbank zur Verfügung.

Neben dem Zusatzmodul HVTE sind folgende Voraussetzungen für die Verwendung gegeben:
-   Windows7, Windows 10 oder höher in 32 oder 64Bit
-   Tapi Treiber Version 2 oder Version 3 steht zur Verfügung
-   Verbindung zu Ihrem **Kieselstein ERP** Server über den Port 8080
-   Entsprechende administrative Rechte um die HVTE installieren zu können

Einrichtung siehe bitte [unten](#Installation HVTE)

Bedienung

Mehrere Mandanten und mehrere Clients

Da in Ihrem **Kieselstein ERP** der Start mehrerer Clients für den gleichen oder auch unterschiedliche Mandanten zulässig ist, versucht die HVTE entsprechend mit diesen Clients zu kommunizieren.

Finden der Telefonnummern

Wie auch bisher bereits üblich, schreiben Sie bitte die Telefonnummern international. D.h. inkl. Nationaler Vorwahl, Ortsvorwahl und dann der Rufnummer. Z.B. +43-6225-28088-0.
Bitte bedenken Sie, dass die Unsitte die ev. im Inland erforderliche (0) in Klammern mit anzugeben, die automatischen Wahlen verhindert. Wir dürfen in diesem Zusammenhang auch darauf verweisen, dass manche Länder z.B. Italien auch bei Anrufen vom Ausland die 0 verlangen.

<a name="Installation HVTE"></a>Installation
--------------------------------------------

Für die Installation starten Sie bitte die mitgelieferte Datei Setup.exe aus dem jeweiligen x64 bzw. x86 (für 32Bit Betriebssystem) Verzeichnis.
Ev. Aufforderungen nach den Administrator Zugangsdaten beantworten Sie bitte entsprechend.
Je nach eventuell bereits installierter Vorgängerversion kann es erforderlich sein, anschließend an die Installation den Rechner neu zu starten.

Nach erfolgreicher Installation finden Sie auf Ihrem Desktop bzw. unter Programme ("c:\Program Files\**Kieselstein ERP** IT-Solutions GmbH\HVTE\HVTE.exe") das HVTE.exe. ![](TAPI_eingehend0.gif)
Starten Sie dieses Programm. Dieses legt sich nach dem Start in die Taskleiste. Klicken Sie hier mit der rechten Maustaste auf das HVTE Icon ![](TAPI_eingehend1.gif).
Es erscheint beim Aufruf ohne Parametrierungen
![](TAPI_eingehend10.jpg)
Klicken Sie nun auf ok und danach mit der rechten Maustaste auf das HVTE Icon ![](TAPI_eingehend1.gif) in der Taskliste.

Sie erhalten die Auswahlmöglichkeiten
- Anrufliste
- Anzeige Debugfenster
- Einstellungen
- Info
- Beenden

Für die erste Konfiguration wählen Sie bitte Einstellungen.
![](TAPI_eingehend2.jpg)
Wählen Sie den gewünschten Anzeige Stil und in aller Regel sollte dieses Programm mit dem Start des Betriebssystems mitgestartet werden. Haken Sie dies bitte entsprechend an.

Definieren Sie nun im Reiter Verbindung
![](TAPI_eingehend3.jpg)
die für Ihre Telefonanlage konfigurierte TAPI Leitung.

Nun definieren Sie im Reiter **Kieselstein ERP**
![](TAPI_eingehend4.jpg)

Ihren **Kieselstein ERP** Server. Port und SSL bitte nur in Abstimmung mit Ihrem IT & **Kieselstein ERP** Betreuer verändern.

Unter Parameter
![](TAPI_eingehend9.jpg)
kann ein entsprechend Abweichendes Verhalten gegenüber den allgemeinen Regeln Ihrer **Kieselstein ERP** Installation, z.B. wenn verschiedene Telefonanlagen auf die gleiche Datenbasis (Partnerstamm) zugreifen, definiert werden.

Mit Ok werden die Einstellungen übernommen.

Erscheint nach dem OK die Fehlermeldung
![](TAPI_eingehend11.jpg)
so fehlt zumindest die Zuordnung der Tapi-Leitung.
Wichtig: Für die Verwendung der HVTE muss immer ein TAPI-Treiber zur Verfügung stehen.

Test der HVTE

Nach erfolgter Konfiguration sollten Sie die Verbindung beider Welten testen.
Klicken Sie dazu mit der rechten Maustaste auf das HVTE Icon und wählen Anzeige Debug-Fenster

![](TAPI_eingehend5.jpg)

Hier sehen Sie in der oberen Liste das Protokoll der Anrufe und der Verbindungen.
Um die Suche in Ihrer **Kieselstein ERP** Datenbank zu prüfen geben Sie im Feld neben Show Anruf die gesuchte Telefonnummer ein.
WICHTIG: Diese Telefonnummern werden normiert gesucht. D.h. es wird exakt die Nummer, welche Sie ja von der Telefonanlage übermittelt bekommen, gesucht. So würden Sie z.B. 0622528088 nicht finden, wenn die Definition in Ihrem Partnerstamm 06225-28088-0 lautet. D.h. es wird dann eben nach 00436225280880 gesucht.
![](TAPI_eingehend7.jpg)

Sollten unter dieser Telefonnummer mehrere Adressen zu finden sein, so werden diese der Reihe nach angezeigt. Gegebenenfalls nutzen Sie bitte den Scrollbalken um entsprechend die richtige Person zu finden.
Nun kann mit Klick auf Annehmen der Anruf angenommen werden bzw. mit Abweisen dieser eben abgewiesen werden.
Beachten Sie bitte, dass diese Funktionen von der Version des Tapi-Treibers abhängig sind und daher nicht überall zur Verfügung stehen.

**Hinweis:** Sollten die Berechtigungen in Ihrem **Kieselstein ERP** Client nach dem Start erweitert worden sein, z.B. um die Cockpit Funktionalität zu sehen, so muss der **Kieselstein ERP** Client neu gestartet werden.

Mit **Ausblenden** kehrt die HVTE wieder in ihre normale Funktion zurück.

Protokoll der Anrufe und der Kommunikation mit Ihrem **Kieselstein ERP** Server.
![](TAPI_eingehend6.jpg)
Im oberen Teil des Fensters sehen Sie welche Nummern vom Tapi-Treiber übergeben wurden und wie lange die Suche gedauert hat.
Diese Dauer dient auch der Überprüfung der Effizienz Ihrer Netzwerkverbindung bzw. der Geschwindigkeit Ihres **Kieselstein ERP** Servers mit all seinen Komponenten.

Unbekannte Rufnummer

Sollte eine Rufnummer in Ihren **Kieselstein ERP** Daten nicht gefunden werden, so wird dies durch <UNBEKANNT> angezeigt.
![](TAPI_eingehend7.gif)

Anrufliste

![](TAPI_Anrufliste.gif)
In der Anrufliste sehe Sie die jeweiligen Anrufe des aktuellen Tages. Diese Liste wird lokal Tageweise auf Ihrem Rechner abgelegt.
Sie beinhaltet die eingehenden und die ausgehenden Anrufe.
Durch Klick auf das CRM Symbol ![](TAPI_CRM.gif) gelange Sie in den Anrufdialog, von dem aus
- diese Telefonnummer angerufen werden kann ![](TAPI_aus_Anrufliste_waehlen.gif) oder auch
- durch erneuten Klick auf das CRM Symbol ![](TAPI_CRM1.jpg) direkt in das Kunden-Cockpit für diesen Ansprechpartner gesprungen werden kann.

Ist eine Telefonnummer aufgeführt, die nicht aufgelöst werden konnte, also eine unbekannte Telefonnummer,
![](TAPI_direkt_anrufen.gif)
so wird dies durch die Anzeige eines Telefonhörers signalisiert. Hier kann durch Klick auf den Hörer direkt zurückgerufen werden.

Die HVTE auch für ausgehende Anrufe verwenden?

Die HVTE kann auch für ausgehende Anrufe genutzt werden. [Siehe dazu](Telefonanbindung.htm#HVTE_ausgehend). 

#### Was ist für den ersten Start der Telefonnummern Erkennung zu tun?
Nach der erfolgten Einrichtung der HVTE müssen die Telefonnummern für die schnelle Erkennung entsprechend aufbereitet werden. Insbesondere nach einem Update Ihres **Kieselstein ERP** und der ersten Verwendung dieser sehr praktischen Funktion muss die Telefonnummernerkennung initialisiert werden.
Wechseln Sie dazu in das Modul System, unterer Reiter Pflege, und wählen Sie nun im Bereich Allgemein den Button Telefonnummern.
Hinweis: Je nach Menge der Partnerdaten und der Geschwindigkeit Ihres **Kieselstein ERP** Servers kann dies durchaus einige Zeit (1-2Std) in Anspruch nehmen.

#### Wie kann man das Finden der Nummern testen?
Insbesondere durch die Fernwartung ergibt sich manchmal die Notwendigkeit das Finden der verschiedenen Telefonnummern zu prüfen.
Dies kann einerseits mit dem Debug-Fenster erfolgen und andererseits auch über einen Browser-Link
[http://KIESELSTEIN-ERP-SERVER:8080/heliumv/phonenumber?search=12345](http://KIESELSTEIN-ERP-SERVER:8080/heliumv/phonenumber?search=die) = die gewünschte Telefonnummer

Fehlermeldungen
---------------

#### Tapi Line nicht konfiguriert
Bedeutet dass die HVTE keine Verbindung mit dem Tapi Treiber herstellen konnte.
Prüfen Sie bitte Ihre Tapi-Treiber Einstellungen.
Zusätzlich können Sie durch den Aufruf des Windows Boardmittels Dialer die Verbindung zum Tapi-Treiber überprüft werden. Es muss in jedem Falle das wählen über den Dialer funktionieren um auch mit der HVTE den Tapi-Treiber nutzen zu können.

#### TAPI Line konnte nicht registriert werden
Wird beim Starten der HVTE die Meldung,
![](TAPI_Fehler.gif)
TAPI-Line (Name der Verbindung) konnte nicht registriert werden, angezeigt, so bedeutet dies, dass der TAPI-Provider für die eingestellt TAPI-Line einen Fehler gemeldet hat.
D.h. dass von der HVTE aus gesehen, beim Versuch des Verbindungsaufbaues zum TAPI Provider (dem TAPI Programm Ihrer Telefonanlage) von diesem ein Fehler gemeldet wurde und deshalb keine Verbindung zur TAPI Schnittstelle hergestellt werden konnte.
Mögliche Ursache: Die TAPI Line wurde von einem anderen Programm z.B. dem bisherigen Telefon-Programm, belegt.

Telefonanlagen
--------------

Eine Sammlung von Eigenheiten, Informationen zu den verschiedenen Telefonanlagen und Tapi-Treibern

### Starface

Version 5

### Mitel

Bei der Wahl über die Zwischenablage muss unbedingt die Amtsholende Null (0) übergeben werden, da ansonsten die Telefonanlage annimmt, dass dies eine interne Nummer ist und diese auf zwei Stellen kürzt.

Diese in der Schweiz und in Liechtenstein verbreitete Anlage hat in einigen Installationen die Eigenheit, die internationale Schweizer / Liechtensteiner Länderkennung (+41 / +423) zu unterdrücken. Programmieren Sie die Anlage so, dass zumindest die internationalen Vorwahlen der anderen Länder (z.B. für eine **Kieselstein ERP** Installation in Liechtenstein die Schweizer Vorwahl) an die HVTE übergeben wird.