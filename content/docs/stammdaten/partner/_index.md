---
title: "Partnerverwaltung"
linkTitle: "Partner"
categories: ["Partner"]
tags: ["Partner", "Ansprechpartner", "Banken"]
weight: 200
description: >
  Verwaltung der Partner, Ansprechpartner und Banken
---
Partnerdaten sind die Sammlung aller Adressdaten aller Mandanten. Hier sind auch die Personal-Adressen enthalten. Die Darstellung ist von den Rechten abhängig (DSGVO).

Partner
=======

[Serienbriefe, Selektionen]( {{<relref "/docs/stammdaten/partner/serienbrief">}} )

[Geodatenanzeige]( {{<relref "/extras/Geodatenanzeige">}} )

#### Kunde, Lieferant, Partner, wozu?
<a name="Kunde, Partner Lieferant, wozu"></a>
Der Partnerstamm ist in **Kieselstein ERP** die zentrale Adressablage. Hier finden Sie alle je erfassten Adressen. Beginnend von Ihren Interessenten, Kunden, Lieferanten, aber auch die Adressen der Mitarbeiter, die Adressen der Banken und die Ansprechpartner. Dies hat den besonderen Vorteil, dass alle Adressen zentral verwaltet werden. Lediglich die Unterschiede werden in den einzelnen Moduldaten abgespeichert. So gibt es oft die Konstellation, dass ein Kunde zugleich ein Lieferant ist, also ein echter Partner. Wenn Kunden- und Lieferantenstämme getrennt wären, müssten Sie immer zwei Datensätze pflegen. So pflegen Sie nur einen zentralen Datensatz.

**Zusätzlich:**

Der Partnerstamm ist nicht mandantenabhängig. D.h. wird ein Partner vom Mitarbeiter eines Mandanten gepflegt, so steht er automatisch in der nun korrigierten Form für alle Mandanten zur Verfügung.

![](Partner_Grunddaten.gif) im Partner.

Definieren Sie hier allgemeine Daten wie

![](Partner_AnspFunktion.gif)

![](Partner_Branche.gif)

![](Partner_Anrede.gif)

![](Partner_Klasse.gif)

![](Partner_Art.gif)

![](Partner_Kommunikationsart.gif)

Die Eingabe erfolgt immer in einer der folgenden ähnlichen Form:

![](Partner_AnspFunktion_Eingabe.gif)

Unter Kennung geben Sie bitte die Allgemeine Kennung ein. Diese gilt über alle Sprachen hinweg.

Unter UI (UI=User Interface) geben Sie bitte die Bezeichnung für diesen Begriff / diese Kennung ein. Diese wird in der Sprache des Hauptmandanten normalerweise gleich mit der Kennung sein.

Bitte beachten Sie, dass allgemeinere Grunddaten auch unter System ![](Partner_System.gif) zu finden sind.

<a name="Kund, Partner, Lieferant wie gehört das alles zusammen"></a>

#### Kunde, Partner, Lieferanten, Mandanten wie gehört dies alles zusammen?
Der Partnerstamm ist Ihre zentrale Adressensammlung. Hier finden Sie alle in **Kieselstein ERP** erfassten Adressen. Kunde, Lieferanten, Banken, Personal, Ansprechpartner. Dies hat für Sie den Vorteil, dass die Adressdaten nur einmal gepflegt werden müssen. Selbstverständlich ist darauf zu achten, dass nur berechtigte Personen den Partnerstamm pflegen dürfen.<br>
**Wichtig:** Um, gerade für Anwender mit mehreren Mandanten, eine zentrale Pflege zu erreichen, ist der Partnerstamm Mandanten unabhängig. Steht also allen Anwendern die auf Ihr **Kieselstein ERP** Zugriff haben zur Verfügung.<br>
Was ist nun aber, aus der Sicht des Kunden / Lieferanten Teil des Partners und was sind Daten des Kunden / Lieferanten?<br>
Immer wenn die "Adress-Daten", also die Details zu einem Kunden / Lieferanten angezeigt werden, sehen Sie, bis auf wenige Zusätze, die Daten des Partners. Das bedeutet, wenn Sie nun die Straße oder die Bemerkung dieses Partners aus dem Kunden heraus ändern, so ändern Sie die Partnerdaten. Dadurch sind die Änderungen für alle Mandanten und für alle anhängenden Datensätze (Kunden / Lieferanten / Banken / Personal) gültig.

![](Partner.png)

#### Muss ich zuerst einen Partner anlegen und kann erst danach daraus einen Kunden/Lieferanten machen?
Nein. In der Kunden-, Lieferanten-, Bank-, Finanzamt-, Personalverwaltung werden immer die Partnerdaten mitgepflegt. Technisch gesprochen sind Kunden, Lieferanten usw. unterschiedliche Sichten immer auf die gleichen Daten. D.h. wenn ein Kunde erfasst wird, so werden automatisch auch die Partnerdaten für diesen Kunden mit angelegt. Dies gilt in gleicher Form für alle anderen Daten die im Partnerstamm abgebildet sind.<br>
Beachten Sie bitte den Unterschied bei den Ansprechpartnern. Hier ist eine umfassende Änderung nur direkt im Partner möglich.<br>

#### Wie mache ich einen Partner zum Kunden?
<a name="Partner in Kunde umwandeln"></a>
Klicken Sie in der Kundenauswahlliste auf ![](KD_LF_aus_Partner.gif) um einen neuen [Kunden]( {{<relref "/docs/stammdaten/kunden" >}} ) aus einem bestehenden Partner zu erstellen und ergänzen Sie die Kundendaten. **Hinweis:** Für die übliche Neuanlage von Kunden ist es nicht erforderlich zuerst den Partner anzulegen und dann daraus einen Kunden zu machen. Es reicht aus, einfach im Kunden in der Auswahlliste auf Neu zu klicken und die Daten des Kunden einzugeben. Die gleichlaufende Anlage des Partners wird von **Kieselstein ERP** im Hintergrund mitgezogen.

#### Wie mache ich einen Partner zum Lieferanten?
<a name="Partner in Lieferant wandeln"></a>
Klicken Sie in der Lieferantenauswahlliste auf ![](KD_LF_aus_Partner.gif) um einen neuen [Lieferanten]( {{<relref "/docs/stammdaten/lieferanten" >}} ) aus einem bestehenden Partner erstellen und ergänzen Sie die Lieferantendaten. **Hinweis:** Für die übliche Neuanlage von Lieferanten ist es nicht erforderlich zuerst den Partner anzulegen und dann daraus einen Lieferanten zu machen. Es reicht aus, einfach im Lieferanten in der Auswahlliste auf Neu zu klicken und die Daten des Lieferanten  einzugeben. Die gleichlaufende Anlage des Partners wird von **Kieselstein ERP** im Hintergrund mitgezogen.

#### Wie mache ich einen Partner zu einer Person / Mitarbeiter?
Klicken Sie in der Personenauswahlliste auf ![](Neue_Person.gif) Neu. Vergeben Sie nun die Personalnummer und wählen Sie durch Klick auf den Button ![](Person_aus_Partner_uebernehmen.gif) Name den gewünschten Partner aus der Liste der Partner aus. **Hinweis:** Für die übliche Neuanlage von [Personen]( {{<relref "/docs/stammdaten/personal" >}} ) ist es nicht erforderlich zuerst den Partner anzulegen und dann daraus eine Person  zu machen. Es reicht aus, einfach im Modul Personal in der Auswahlliste auf Neu zu klicken und die Daten der Person einzugeben. Die gleichlaufende Anlage des Partners wird von **Kieselstein ERP** im Hintergrund mitgezogen.

#### Was ist der Sinn der Partnerart
Die Partnerart dient der sehr groben Qualifizierung dieser Adresse. Man kann grundsätzlich davon ausgehen, dass alles eine Adresse ist. Hauptzweck ist die einfachere Auswahl aus den in der Anwendung doch sehr langen Partnerlisten. **Hinweis:** Die Partnerart kann unter Partner, Grunddaten, Art definiert werden.<br>
Grundsätzlich keine Definitionen benötigen: Kunde, Lieferant, Bank, Ansprechpartner, Personal. Diese ergeben sich automatisch durch die weiterführenden Definitionen.<br>
Da die Partnerart eng mit dem Modul zusammenhängt, von dem aus der Partner angelegt wird, sollte die Partnerart <u>nicht</u> geändert werden.

#### Was ist der Unterschied in den Partnerarten
Der wesentlichste Unterschied in den Partnerarten sind:

| Partnerart | Bedeutung |
| --- |  --- |
| Adresse | Allgemeine Adresse |
| Ansprechpartner | UID, EORI, ILN, Filialnr., Gerichtsstand und Firmenbuchnummer werden nicht erfasst / angezeigt. <br>Statt dessen wird das Geburtsdatum des Ansprechpartners erfasst. |
| Person | Ihre ursprünglich unter Personal angelegten Mitarbeiter |
| Sonstiges | Bank |

D.h. wenn bei einer Adresse z.B. die UID Nummer nicht erfasst werden kann, so prüfen Sie bitte als erstes die Partnerart und ändern Sie diese gegebenenfalls ab.

#### Partner-Art, Partner-Klasse, Partner-Branche; Wozu dient das, was sind die Unterschiede?
Die Partnerart dient der sehr groben Qualifizierung der Adresse. Zusätzlich wird diese in der Partnerübersicht angezeigt. Durch einen Klick auf die Art kann nach dieser sortiert werden und somit gezielt gewisse Gruppen bearbeitet werden. Die Partnerart kann im Partnermodul unter Grunddaten, Art definiert werden.

Partnerklasse und Partnerbranche sind technisch gesehen zwei gleichwertige Filterkriterien um den Partner datentechnisch zu beschreiben. Sowohl Klasse als auch Branche können ebenfalls im Partnermodul unter Grunddaten definiert werden.

Es empfiehlt sich in der Klassifizierung Partnerbranche Branchenbegriffe oder Ähnliches einzutragen.

Für die Partnerklasse können Sie z.B. Klassifizierungen des Partners vornehmen wie z.B. Distributor, Einzelhändler, Großer Endkunde usw.. Eventuell ist die Partnerklasse auf für Gruppierungen geeignet. Siehe dazu aber auch Kunden-Selektionen.

<a name="Wie erfasst man Telefon und Faxnummern"></a>

#### Wie erfasst man Telefon und Faxnummern richtig?
In **Kieselstein ERP** gibt es bis auf eine Ausnahme, keine grundsätzliche Regel, wie Telefon- und Faxnummern eingegeben werden sollten. Die Ausnahme ist die Kennzeichnung der Durchwahl des zentralen Faxes bzw. der Telefonzentrale. Hier muss ein entsprechendes Trennzeichen (- oder /) eingegeben werden.

Da aber immer öfter die Telefonnummern auch in Mobiltelefone übertragen werden und Sie damit auch aus dem Ausland richtig telefonieren können sollten, empfehlen wir dringend die internationalen Vorwahlen mit anzugeben. Von **Kieselstein ERP** wird dies entsprechend unterstützt. Siehe dazu bitte auch System, Parameter, Auslandsvorwahl bzw. Inlandsvorwahl. Zusätzlich wird überprüft, ob der Beginn der eingegebenen Telefon- bzw. Faxnummer mit der im Land definierten Vorwahl übereinstimmt. Stimmt dies nicht überein, so wird die Telefonnummer in roter Schrift angezeigt.

Für den Faxversand bzw. das Telefonieren mittels Tapi wird die Internationale Vorwahl z.B. +49 gegen die von Ihrem Standort aus definierte Vorwahl z.B. 0049 ersetzt. Ist das Telefonat im Inland, so wird die internationale Vorwahl vollständig durch die Inlandsvorwahl ersetzt.

Wir empfehlen Ihnen die Eingabe der Telefon- und Faxnummern für Sie sprechend bzw. lesbar vorzunehmen. Ein Beispiel:

0043622182250 ist vermutlich für uns alle eine lange Ziffernkolonne, nicht lesbar und nicht merkbar.

+43 6221 8225-0 sagt sehr viel aus

^      ^     ^   ^

!        !      !    +... der Bindestrich trennt ganz klar die Durchwahl der Zentrale von der eigentlichen Rufnummer

!        !      !          die Leerstellen zwischen den Gruppen trennen die Ziffernkolonne in lesbare Teile und zwar

!        !      + ....... die eigentliche Rufnummer

!        + .............. die Ortsvorwahl (Area Code) sollte unbedingt mit eingegeben werden, 

                           da von mehreren Standorten aus gefaxt werden kann und dann die Nummer nicht korrekt ist

\+ ....................... die internationale Ländervorwahl (Countrycode)

Bitte beachten Sie, das das letzte Trennzeichen (- oder /) für die Ermittlung der Rufnummer für die Durchwahlen verwendet wird.

Ein Beispiel: Der Mitarbeiter hat die Telefondurchwahl 301 und die Faxdurchwahl 901 hinterlegt. Die Firmennummern sind wie folgt:

![](Telefon_Faxnummer.gif)

Wird nun ein Beleg an diesen Ansprechpartner gefaxt, so wird die Faxnummer aus der Firmenfaxnummer, jedoch ohne der Ziffern nach dem letzten - (/) gebildet. In unserem Beispiel wird also 0043 6221 8225-901 an das Fax übergeben. Dieses fügt dann eventuell noch eine spezielle Kennung (0) für die Amtsholung hinzu. Die Vorgehensweise für die Ermittlung der persönlichen Telefonnummer des Ansprechpartners ist dazu analog, jedoch mit den Telefonnummern.

**Tipp:** Gruppieren Sie lange Rufnummern in zweier oder dreier Gruppen und trennen sie die verschiedenen Nummernarten mit einem einheitlichen Zeichen. Damit wird die Nummer für Sie und Ihre Mitarbeiter les- und merkbar.

**Wichtig:** Die Erkennung ob eine Durchwahl an die Telefon- bzw. Faxnummer der Zentrale (Firmenadresse) angefügt ist oder nicht ist an das Trennzeichen (-) und an die Länge der Durchwahl der (Fax-)Zentrale gekoppelt. Die maximale Länge dieser Durchwahl kann in den Mandantenparametern unter MAXIMALELAENGE_DURCHWAHL_ZENTRALE (Default=3) eingestellt werden.

Wichtig: Geben Sie die internationale Länderkennung richtig an. [Siehe dazu auch]( {{<relref "/docs/stammdaten/system/drucken/#die-vorwahl-der-faxnummer-wird-doppelt--falsch-%c3%bcbergeben" >}} ).

#### Partner verstecken?
Durch aktivieren der Eigenschaft verstecken ist ein Partner als versteckt gekennzeichnet.

Damit können Sie Partner die in der Auswahlliste normalerweise nicht mehr aufscheinen sollten (z.B. weil Sie keine Geschäfte mehr mit diesem Lieferanten machen) entsprechend kennzeichnen.

Die Eigenschaft des Partners wirkt zugleich für Kunden und Lieferanten.

Für das Personal gibt es eine eigene, vom Partner getrennte Eigenschaft versteckt.

<a name="Bankverbindung / Bank"></a>

#### Bankverbindung / Bank?
Im Partner gibt es sowohl die Bankverbindung eines Partners als auch die Definition der Banken für den jeweiligen Mandanten.

Um einem Partner eine Bank zuordnen zu können, muss zuerst die / seine Bank definiert werden. Wählen Sie dazu im Partnermodul im unteren Modulreiter Bank. Definieren Sie nun mit neu in der Auswahl der Banken eine neue Bank. Hier werden die üblichen Partnerdaten angegeben und zusätzlich die Bank eigenen Definitionen, als da sind: ![](BLZ.gif), ![](BIC.gif).

Beachten Sie bitte, dass für die Bestimmung ab 2014 wenn die Bank / das Land der Bank Sepa-Mitglied ist, es erforderlich ist, dass zumindest das Land der Bank mit angegeben wird. [Siehe unten](#Welcher Ort für die Bank).

In den Daten des Partners können nun unter Bankverbindung ![](Bankverbindung.gif) die Bankverbindungen dieses Partners angelegt werden. Hier sind nun noch die zusätzlichen Daten für das Konto des Partners bei dieser Bank zu definieren. Dies sind Konto und IBAN.

![](Bank_Konto.gif)

Alle weiteren Daten (BIC, BLZ) sind durch die Angabe der Bank definiert.

Für die Definition der Liste der eigenen Bankverbindungen siehe bitte [Finanzbuchhaltung]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen" >}} ).

Beachten Sie bitte, dass die IBAN mittels der Prüfziffer geprüft wird. Sollten in Ihrem System alte / falsche Daten enthalten sein, so müssen diese manuell richtiggestellt werden.

#### Welcher Ort sollte bei meiner Bank hinterlegt werden ?
<a name="Welcher Ort für die Bank"></a>
Insbesondere wenn Sie den Zahlungsvorschlag verwenden, ist es wichtig, dass bei jeder Bank auch ein Ort angegeben wird, damit festgestellt werden kann, ob dieses Land SEPA Teilnehmer ist oder nicht (Hintergrund: es werden im Zahlungsvorschlag nur Daten für diejenigen Länder exportiert, die Sepa-Teilnehmer sind).

Da bei den meisten Bankverbindungen z.B. der Lieferanten nur der Name der Bank und deren Kontodaten angegeben sind, wäre eine exakte Adresszuordnung mit entsprechend aufwändigen Recherchearbeiten verbunden.

Um dies zu vereinfachen legen Sie bitte [im System]( {{<relref "/docs/stammdaten/system/#anlegen-von-l%c3%a4ndern" >}} ) einen leeren Ort mit leerer Postleitzahl an.
Damit wird faktisch nur das Land der Bank definiert, was für den Zahlungsvorschlag ausreicht und andererseits keine Recherchen erforderlich macht.

#### Kontaktdaten, Privat, Firmen, Mandanten
<a name="Kontaktdaten"></a>
Wie können die unterschiedlichsten Daten der Mitarbeiter richtig definiert werden?

Gerade bei Mitarbeitern ergibt sich die Aufgabenstellung, dass diese private Kommunikationsdaten und Firmen(interne)-Kommunikationsdaten haben. Wenn mehrere Mandanten im Einsatz sind, so ist dies unter Umständen auch noch je nach Mandant verschieden.

Alle Kommunikationsarten können im Modul Partner, bei der jeweiligen Person unter ![](Kommunikation.gif) definiert werden.

Grundsätzlich werden folgende Kommunikationsarten unterschieden:

![](Kommunikationsarten.gif)

| Kommunikationsart |
| --- |
| Telefon |
| Fax |
| Handy |
| E-Mail |
| Homepage |
| Direktfax |
| Kurzwahl |

Bitte achten Sie bei den Kommunikationsformen Telefon und Fax auf den Unterschied, ob die Daten aus der Sicht des Ansprechpartners oder der Sicht als direkter Partner z.B. ein Kunde ergeben.

Sind dies die Daten eines Ansprechpartners oder eines Mitarbeiters, so werden hier die Telefon/Fax-Durchwahlen eingegeben (Durchwahl = Telefon/Fax Nebenstellennummer). Anderenfalls werden diese Daten direkt für die Telefon / Fax-Wahl verwendet. Formatierung der Telefon/Fax Nummer siehe [oben](#Wie erfasst man Telefon und Faxnummern).

![](Kommunikations.jpg)

Die Privat Daten des Partners können nur in den Partner bzw. Personal-Detaildaten der Person / des Partners verändert werden.

In der Kommunikationsliste sehen Sie alle Kommunikationsdaten des Partners. Verändert werden können nur die privat Daten und die des angemeldeten Mandanten.

**Hinweis:**

Die Kommunikationsdaten im Reiter ![](Partner_Detailreiter.gif) sind immer die persönlichen, also Privat-Kommunikationsdaten des Partners.

Da Unternehmen keine Privatpersonen sind, finden Sie hier die Kontaktdaten zum Unternehmen. Technisch besteht jedoch kein Unterschied zu Privatpersonen.

Wozu diese Definitionen:

Mit diesen Kommunikationsdefinitionen werden die Absenderdaten Ihrer Mitarbeiter definiert. Damit ist es möglich die entsprechenden Absenderdaten, sowohl in der direkten Kommunikation, als auch in den Formularen zu steuern. Denken Sie an Angebotsformulierungen wie: .... steht Ihnen unter DW 301 Herr Mustermann gerne zur Verfügung. Oder: Ihr Mitarbeiter versendet ein EMail an einen Lieferanten. Natürlich sollte die Antwort des Lieferanten wiederum direkt an Ihren Mitarbeiter erfolgen und nicht an die allgemeine Firmen-EMail-Adresse. Durch die oben angeführten Definitionen können Sie dies entsprechend steuern.
**Hinweis:**

Sind beim jeweiligen Mitarbeiter keine passenden Kommunikationsdaten hinterlegt, so werden die Mandantenkommunikationsdaten verwendet.

**Hinweis:**

Die Detaildaten im Partnermodul entsprechen den Detaildaten im Personalmodul und auch im Kunden- und Lieferantenmodul.

#### Definition der Absenderdaten Ihrer Mitarbeiter
Die Unternehmens-Absenderdaten Ihrer Mitarbeiter werden unter [Personal, Daten]( {{<relref "/docs/stammdaten/personal/#absenderdaten" >}} ) definiert.

#### Erfassung des Postfaches
In **Kieselstein ERP** haben wir berücksichtigt, dass in der Bundesrepublik Deutschland für Postfach- und Straßen-Anschrift unterschiedliche Postleitzahlen angegeben werden müssen. Aus diesem Grund wird für die Straßen-Anschrift die übliche Ortsangabe angeführt und für das Postfach kann zusätzliche eine eigene Postleitzahl mit Ort angegeben werden.

![](Ort_Postfach.jpg)

Das bedeutet, ist ein Postfach angegeben, so werden alle offiziellen Belege auf das Postfach ausgestellt. Ist zusätzlich eine eigene Postleitzahl unter Postfach angegeben, so wird anstelle der Orts-Postleitzahl die Postleitzahl des Postfaches angedruckt.

Für den Fall dass Sie je nach Geschäftsfall einmal die eine oder die andere Adresse verwenden wollen, muss der Partner zweimal angelegt werden.

Die Angabe der Postleitzahl des Postfaches ist als optionale Angabe gedacht, eben für die obigen Adressen. Für alle anderen Länder diese bitte leer belassen.

#### Kann man einen neuen Ort eintragen, wenn man im Modul Partner (Kunde, Lieferant) ist?
Ja! Klicken Sie auf Ort. Im Auswahldialog sehen sie den üblichen Neu-Button. Wenn Sie nun auf neu klicken (Strg+N) kommen Sie direkt ins neu anlegen eines Ortes mit Länderkennzeichen, Postleitzahl und Angabe des Ortsnamens.

#### Partner zusammenführen
<a name="Partner zusammenführen"></a>

#### Es ist der gleiche Partner mehrfach angelegt, wie kann ich das ändern?
Verwenden Sie dafür die Funktion Partner zusammenführen.

**Wichtig:** Wenn nicht nur Partner sondern auch Kunden und Lieferanten zusammengeführt werden sollten, so hat sich bewährt, zuerst die Kunden / Lieferanten zusammenzuführen und danach erst die Partner zusammenzuführen. Wird dieser Vorgang in einem durchgeführt kann es manchmal zu unklaren Konstellationen kommen, die programmtechnisch nicht aufgelöst werden können.

Im Modul Partner finden Sie<br>
![](Partner_zusammenfuehren.gif)

Mit dem nun nachfolgenden Dialog kann bestimmt werden, welche Daten verwendet werden.

![](Zusammenfuehren1.gif)

Die blau hinterlegten Felder sind die Felder, welche nach dem Zusammenführen für den ausgewählten Partner verwendet werden.

Durch Klick auf ![](Zusammenfuehren2.gif) wird das zusammenführen gestartet.

"Partner zusammenführen" bedeutet, dass zwei Partner zu einem Partner verschmolzen werden. Dies kann z.B. dann vorteilhaft sein, wenn ein Partner mehrfach angelegt und bereits verwendet wurde, sodass Abhängigkeiten zu anderen Daten wie z.B. Aufträgen, Kunden, etc... entstanden sind, und der Partner nicht mehr ohne Weiteres gelöscht werden kann. Partner kann man nur dann zusammenführen, wenn man das dafür nötige Rollenrecht besitzen *PART_PARTNER_ZUSAMMENFUEHREN_ERLAUBT*

**<u>Wichtig:</u>** Das Zusammenführen zweier Partner kann **<u>nicht rückgängig</u>** gemacht werden!

Vorgehen:
-   In Partner > Auswahl aus der Liste einen Partner auswählen (diese Auswahl kann später noch geändert werden)
-   Partnermenü > Partner zusammenführen... (Nun öffnet sich ein neuer Dialog, in welchem die Daten des gerade ausgewählten Partners auf der linken Seite aufgelistet werden)
-   Rechten Button "Partner auswählen..." betätigen um den zweiten Partner auszuwählen
-   Zu erhaltende Daten selektieren, siehe Bedienung Dialogfenster Zusammenführen
-   Button "Partner zusammenführen" (links oben - grüner Haken) betätigen.
    Nach erfolgreichem Zusammenführen wird der Dialog automatisch geschlossen und die Partnerauswahlliste angezeigt.

**Bedienung des Dialogfensters Zusammenführen:**
Die beiden ausgewählten Partner werden gegenübergestellt. Zwischen den Partnerdaten befinden sich Pfeil-Buttons ![](Partner_Zusammenfuehren_Pfeil_Button.gif). Die Daten, die erhalten bleiben, werden farblich hinterlegt.
*) Pfeil nach links (<-): Beim Zusammenführen bleiben die Daten des linken Partners erhalten.
*) Pfeil nach rechts (->): Beim Zusammenführen bleiben die Daten des rechten Partners erhalten.
*) Pfeil nach links und rechts (<->) (z.B. Bemerkung): Die Daten des rechten Partners werden an die Daten des linken Partners angehängt. Wenn allerdings die Höchstzahl der zulässigen Zeichen überschritten wird, werden die Daten des rechten Partners abgeschnitten.
Sind die Pfeilbuttons nicht aktivierbar, so bedeutet dies, dass die Daten bei beiden Partnern identisch sind.
Oberhalb der gegenübergestellten Partnerdaten gibt es einen zentralen Pfeil-Button, welcher betätigt werden kann, um alle Daten des linken Partners oder alle Daten des rechten Partners zu übernehmen.

Es gibt zusätzlich die Möglichkeit des automatischen Zusammenführens (Kunde/Lieferant/...): Hiefür sind gegebenenfalls weitere Rollenrechte notwendig *PART_KUNDE_ZUSAMMENFUEHREN_ERLAUBT*, *PART_LIEFERANT_ZUSAMMENFUEHREN_ERLAUBT*).<br>
Zur Erklärung: Es gibt Sonderfälle, bei denen das Zusammenführen der Partner nicht ohne Weiteres durchgeführt werden kann. Z.B.: Wenn zwei Partner zusammengeführt werden sollen, aus denen jeweils ein Kunde erzeugt worden ist. In diesem Fall führt der Versuch des Zusammenführens zu einer Meldung, dass KundeX und KundeY zuerst zusammengeführt werden müssen. Wenn beim automatischen Zusammenführen "Kunde" angehakt wird, dann werden beim Partnerzusammenführen die Kunden aus dem Sonderfall automatisch zusammengeführt, wobei allerdings die Kundendaten nicht gegenübergestellt werden, und so die zu erhaltenden Daten nicht auszuwählen sind. In diesem Fall werden die Daten des linken Kunden übernommen und die des rechten Kunden gelöscht. Wünscht man eine Gegenüberstellung der Kundendaten und eine manuelle Datenauswahl, so kann man die Kunden auch manuell zusammenführen (Siehe Kunden zusammenführen).

Was wird beim Zusammenführen gemacht:

-   Die im Zusammenführen-Dialog selektierten Daten werden in den neuen Partner geschrieben

-   Die Partnerkommunikationsdaten werden in den neuen Partner geschrieben (Direktfax, Fax, Email, Handy, Homepage, Telefon)

-   Empfänger und Sender von bereits angelegten Versandaufträgen werden an den neuen Partner angepasst

-   Existieren Bankverbindungen bei einem der zusammenzuführenden Partner, so werden diese an den neuen Partner angepasst

-   Wurde aus einem der selektierten Partner eine Bank erzeugt, so werden die Bankdaten an den neuen Partner angepasst

-   Ist einer der Partner Teilnehmer an einem Auftrag, so wird diese Teilnahme an den neuen Partner angepasst

-   Sind an den Partner Ansprechpartner geknüpft, so werden diese an den neuen Partner angepasst

-   Wurden bei einem der Partner Kurzbriefe angelegt, so werden diese an den neuen Partner angepasst

-   Wurden bei einem der Partner in der Zeiterfassung Reisen auf den Partner (als Kunden) gebucht, so werden diese Reisen und auch die Logs an den neuen Partner angepasst

-   Wurden auf einen der Partner in der Zeiterfassung Telefonzeiten gebucht, so werden diese Telefonzeiten an den neuen Partner angepasst

-   Wurden Selektionen bei dem Partner definiert, so werden diese an den neuen Partner angepasst

-   Wurde der Partner als Artikelhersteller verwendet, so werden diese Herstellerdaten an den neuen Partner angepasst

-   Wurden aus dem Partner Kunden erzeugt, so werden diese an den neuen Partner angepasst

-   Wurden aus dem Partner Lieferanten oder Rechnungsadressen von Lieferanten erzeugt, so werden diese an den neuen Partner angepasst

-   Wurde aus dem Partner Personal erzeugt oder der Partner als Sozialversicherer oder als Firmenzugehörigkeit beim Personal angegeben, so wird dies an den neuen Partner angepasst

-   Wurde der Partner bei Bestellungen als Lieferadresse ausgewählt, so werden diese Bestellungen an den neuen Partner angepasst

-   Wurde der Partner bei Losen als Fertigungsort verwendet, so werden diese Lose an den neuen Partner angepasst

-   Wurde der Partner bei Projekten als Partner ausgewählt, so werden diese Projekte an den neuen Partner angepasst

-   Wurde der Partner bei Stücklisten als Kunde ausgewählt, so werden diese Stücklisten an den neuen Partner angepasst

-   Zum Schluss wird der nicht mehr benötigte Partner gelöscht, falls alle Abhängigkeiten darauf aufgelöst werden konnten.

**Hinweis:** Es werden immer die blau hinterlegten Felder in den verbleibenden Partner übernommen.

#### Ein Partner ändert seinen Namen. Was ist dabei zu beachten?
Wenn ein Partner, ein Kunde, ein Lieferant, ein:e Mitarbeiter:inn seinen/ihren Namen ändert, so ist dabei grundsätzlich folgendes zu beachten.

-   Ändert sich der Name des Partners, aber der alte Partner bleibt als Partner erhalten. Z.B. Ein Unternehmen wird insolvent und die Nachfolgefirma heißt sehr ähnlich der bisherigen Firma. Ist dies der Fall, so haben wir es mit einem neuen Partner zu tun. Die Statistik, die Debitorennummern usw. müssen auf zwei getrennte Partner laufen. Daher ist ein neuer Partner anzulegen.

-   Wird nur der Name eines Partners, z.B. wegen Umfirmierung oder Verehelichung geändert, so ist der Partner noch immer der gleiche. Er/sie hat nur einen anderen Namen. Hier muss im Partner nur der Name verändert werden. Alle Statistiken, Umsatzdaten, Zeitbuchungen usw. bleiben auf dem Partner erhalten.
    **Wichtig:** In diesem Falle bewirkt die Änderung, dass bei einem eventuellen Ausdruck alter Daten trotzdem die geänderten nun neuen Daten / Namen ausgedruckt werden. Für den Ausdruck alter Belege mit den alten Daten siehe Dokumentenablage.

#### Kann ein Ansprechpartner gelöscht werden?
Jein. D.h. grundsätzlich ist ein Ansprechpartner ein Partner(-Datensatz) wie jede andere Adresse auch. Das hat den Vorteil, wenn Sie z.B. vom Geschäftsführer eines Unternehmens auch die privaten Daten (Adresse, Handynummer etc.) wissen, können diese im Partnermodul direkt diesem Ansprechpartner zugeordnet werden.

Ist ein Ansprechpartner mit einem Partner verknüpft, so kann diese Verknüpfung durch Klick auf ![](Ansprechpartner_loeschen.gif) jederzeit gelöscht werden. Es wird dadurch jedoch nur die Verknüpfung zwischen Partner (z.B. Kunde) und Ansprechpartner gelöscht. Die Adressdaten des Partners bleiben erhalten.

Sind jedoch auf einem Ansprechpartner bereits z.B. Angebote erstellt, so kann dieser Ansprechpartner nicht mehr gelöscht werden. Hier gibt es nun zwei Möglichkeiten:

a.) Es wurde versehentlich ein zweiter Ansprechpartner für die gleiche Person angelegt. Das bedeutet es müssen die Ansprechpartner des Partners (Kunden) zusammengeführt werden.

b.) Der Ansprechpartner ist richtig angelegt hat aber z.B. das Unternehmen verlassen. Da ja hier die alten Angebote an diesen Ansprechpartner erhalten bleiben, muss auch der Ansprechpartner dem Partner weiterhin zugeordnet sein. Hier empfiehlt sich, den Ansprechpartner zu verstecken und die Reihung so zu setzen, dass wieder die aktiven Ansprechpartner vorne in der Auswahlliste erscheinen.

#### Kann ein bestehender Partner als Ansprechpartner verwendet werden?
Wechseln Sie im entsprechenden Modul (Kunde, Lieferant, Partner) in den oberen Modulreiter Ansprechpartner. Klicken Sie nun auf Neu und klicken Sie auf ![](Anspechpartner_auswaehlen.gif). Wählen Sie nun den gewünschten Partner aus.

#### Wie kann der Name eines Ansprechpartners geändert werden?
Gehen Sie im Kunden/Partner auf den oberen Modulreiter Ansprechpartner und stellen Sie den Cursor auf die entsprechende Ansprechpartner Zeile. Es werden nun im unteren Fenster die Details des Ansprechpartners angezeigt.

Hier finden Sie auch den GoTo Button

 ![](Ansprechpartner_aendern.jpg)

Durch Klick auf diesen Button wird das Modul Partner mit dem Ansprechpartner vorausgewählt geöffnet. Wechseln Sie nun auf Detail und ergänzen / ändern Sie den Partner wunschgemäß ab.

#### Der erste Ansprechpartner ist nicht mehr im Unternehmen. Wie wird dies richtig erfasst?
Der erste Ansprechpartner ist ja zugleich der Ansprechpartner, der bei Angeboten, Bestellungen usw. als Ansprechpartner vorgeschlagen wird. Genauer gesagt wird der Ansprechpartner mit der Sortierung 1 vorgeschlagen. Ist nun ein Ansprechpartner nicht mehr im Partner-Unternehmen, so ändern Sie bitte vor dem Verstecken dessen Sortierung durch Klick auf den blauen Pfeil ![](Ansprechpartner_sortieren.gif) so, dass er an letzter Stelle steht. Danach verstecken Sie den Ansprechpartner durch ändern und anhaken von versteckt ![](Ansprechpartner_verstecken.gif).

#### Ein Ansprechpartner ist mehrfach angelegt, kann das korrigiert werden?
Ja. Der Vorgang ist im Partnerzusammenführen enthalten.

Um zwei Ansprechpartner des gleichen Partners (und damit Kunden, Lieferanten) zusammenzuführen, gehen Sie wie folgt vor:

1.  Wählen Sie in der Auswahlliste der Partner den Partner = Firma aus, bei dem Ansprechpartner zusammengeführt werden sollen

2.  Wählen Sie im Menü ![](Partner_zusammenfuehren.gif) Partner Zusammenführen.

3.  Klicken Sie nun im vorbesetzten linken Fenster auf Ansprechpartner ![](Zusammenfuehren3.gif)

4.  Wählen Sie nun den zu erhaltenden Ansprechpartner aus.

5.  Klicken Sie im rechten Fenster auf Partner und wählen Sie den gleichen Partner (Firma) aus, für den das Zusammenführen der Ansprechpartner gemacht werden sollte.

6.  Klicken Sie im rechten Fenster auf Ansprechpartner und wählen Sie den in den zu erhaltenden Ansprechpartner übertragenden Ansprechpartner aus.

7.  Wählen Sie die Felder entsprechend den zu verwendenden Daten wie bei Partner zusammenführen beschrieben.
    ![](Zusammenfuehren4.gif)

8.  Mit Klick auf den ![](Zusammenfuehren2.gif) Haken werden die beiden Ansprechpartner zusammengeführt.

#### Kann beim Partner ein Bild hinterlegt werden?
Gerade in Verbindung mit dem Modul Cockpit wird immer intensiver der Wunsch geäußert, dass man gerne sehen möchte wie den das Gegenüber aussieht. Um das Bild zu hinterlegen wechseln Sie bitte in den jeweiligen (Ansprech-) Partner und wählen dann den Reiter Bild.
Hier kann mit den üblichen Mitteln ein Bild des Partners hinzugefügt werden.
Dieses Bild wird in den Ansprechpartner-Reitern neben der jeweiligen Bemerkung des Ansprechpartners angezeigt.

#### Kontaktmanagement
[Siehe]( {{<relref "/docs/stammdaten/partner/kontaktmanagement">}} )

#### Wo können die Ansprechpartnerfunktionen definiert werden?
Diese können im Modul Partner, unterer Modulreiter Grunddaten, oberer Modulreiter Ansprechpartnerfunktion eingetragen werden.

#### Übersteuerung von EMail-Adressen / Faxnummer
Manchmal ist es erforderlich, dass für bestimmte Formulare diese für den gleichen Partner immer an den gleichen Empfänger gehen, dieser ist jedoch nicht der Ansprechpartner an den dieses Papier gerichtet ist.
Ein Beispiel wäre, dass der elektronische Rechnungsversand immer an eine bestimmte EMail-Adresse Ihres Kunden zu senden ist, ein anderes, dass Sie die Packliste eines Auftrags, abhängig vom Kunden an den jeweils zuständigen Spediteur senden wollen.
Da dies meistens auch noch eine Eigenschaft des Kunden/Lieferanten ist, haben wir das so gelöst, dass Sie dafür spezielle Ansprechpartnerfunktionen definieren können.

Anhand der Packliste sei nun die Vorgehensweise erklärt.

1.  Stellen Sie den Namen des Reports fest, für den dieses Sonder-Ansprechpartnerfunktion definiert werden soll. Drucken Sie dazu den Report in die Vorschau. Sie erhalten z.B.:
    ![](Ansprechpartner_Sonderfunktion_Titel.gif)

2.  Anhand des Fenstertitels entnehmen Sie den Reportnamen. In unserem Fall auft_packliste2.jasper.

3.  Wechseln Sie nun in die Definition der Ansprechpartnerfunktionen und geben Sie diesen Reportnamen unter
    ![](Ansprechpartner_Sonderfunktion_definieren.gif)
    Reportname für übersteuerten Empfänger an. Achten Sie auf die exakte Schreibweise unter Berücksichtigung der Kleinschreibung.

4.  Wechseln Sie nun in den Kunden, Ansprechpartner und definieren Sie einen Ansprechpartner und geben Sie ihm diese (Sonder-) Funktion und hinterlegen Sie eine entsprechende EMail-Adresse bzw. Faxnummer.
    ![](Ansprechpartner_Sonderfunktion_hinterlegen.jpg)

5.  Nun wechseln Sie wieder in den Auftrag und Drucken die Packliste in die Vorschau und klicken danach auf EMail.
    ![](Ansprechpartner_Sonderfunktion_Ergebnis.jpg)

    Es wird die für diesen Report definierte Sonderfunktion bei diesem Kunden verwendet.

Erweiterte Textsuche

Oft kennen Sie zwar den Namen des Ansprechpartners, können sich aber nicht mehr erinnern, zu welchem Unternehmen diese Person gehört. Oder Sie sehen auf Ihrem Telefon eine Rufnummer und wollen wissen, wer denn dies ist.

Um dies einfach festzustellen haben wir die Erweiterte Suche geschaffen.

![](Erweiterte_Suche.gif)

Diese finden Sie in den Modulen Partner, Kunde, Lieferant.

Erfolgt eine Eingabe in der Erweiterten Suche, so werden folgende Datenfelder durchsucht, wobei Groß/Klein-Schribeung ignoriert wird und Leerzeichen ignoriert werden.

-   Name1 (Nachname bzw. 1.Zeile der Firma),

-   Name2 (Vorname bzw. 2.Zeile der Firma),

-   Name3 (bzw. Abteilung der Firma)

-   Straße der Firma

-   alle drei Namen des Ansprechpartners

-   Email des Partners

-   Fax des Partners

-   Telefon des Partners

-   Email des Ansprechpartners

-   Fax des Ansprechpartners

-   Telefon des Ansprechpartners

-   Mobiltelefon des Ansprechpartners

-   Bemerkung beim Ansprechpartner

-   Bemerkung des Partners, nicht jedoch der Kommentar bei Kunde bzw. Lieferant

#### Bemerkung des Partners
[Siehe]( {{<relref "/docs/stammdaten/kunden/#unterschied-zwischen-kommentar-und-bemerkung" >}} ).

#### UID-Nummer, EORI Nummer?
Als Unternehmer sind Sie verpflichtet eine Vielzahl von Erkennungsnummern bei Ihren Geschäftspartnern zu erfassen, anzugeben.

Derzeit können in **Kieselstein ERP** folgende Daten erfasst werden:

| Erfassungsnummer | Bedeutung |
|---|---|
| EU-UID Nummer | Umsatzsteuer Identifikations Nummer für die EU-Mitgliedsländer. Diese muss immer auch das Länderkürzel enthalten (ATU, DE) und sollte ohne Leerzeichen eingegeben werden. |
| EORI Nummer | Die Zoll-Identifikationsnummervor allem für den Warenverkehr mit Drittländern (Schweiz, Liechtenstein, USA usw.) |
| ILN | International Location Number<br>Eindeutige Kennzeichnung der Filialen gerade großer Handelsketten, um diese eindeutig zu identifizieren |
| Filialnr. | Kennzeichnung der Filialen eines Unternehmens innerhalb dessen |
| Gerichtsstand | Das zuständige Gericht wenn Sie mit diesem Partner Geschäfte machen |
| Firmenbuchnummer / HRB Nr | Erfassung der Firmenbuchnummer (A)bzw. der HRB Nummer (D) |

**ACHTUNG:** Auch die Schweiz hat eine UID Nummer. Diese Unternehmens Identifikations Nummer, muss bei jedem Geschäft mit einer Schweizer / Liechtensteiner Firma angegeben werden, hat aber nichts
mit die Umsatzsteuer Identifikations Nummer der EU zu tun.

#### Abteilung des Ansprechpartners
Um die Abteilung eines Ansprechpartners bei Belegen anzudrucken, hinterlegen Sie diese bei der jeweiligen Person im Feld Abteilung ![](abteilung_ansprechpartner.JPG).

#### Wie kann man erkennen bei welchem Mandanten der Ansprechpartner verwendet wird?
Da der Partnerstamm Mandanten-übergreifend ist, werden alle Adressen aller Mandanten angeführt. Im Reiter Ansprechpartner von gibt es nun 2 zusätzliche Spalten Mandant und Art, wie in Referenz zu, hier können Sie erkennen, bei welchem Mandanten die Adresse verwendet wird.

#### Kann in den Belegen nach Ansprechpartner gesucht werden?
[Siehe]( {{<relref "/verkauf/gemeinsamkeiten/#kann-ich-nach-belegen-bestimmter-personen-bestimmter-firmen-suchen" >}} )

#### Wie findet man heraus welche Partner welche Selektion haben?
Dafür stehen zwei Möglichkeiten zur Verfügung:
- a.) Verwenden Sie die Serienbrieffunktion um eine umfangreiche Filterung auf die gewünschten Partner durchzuführen.
- b.) Wenn es um die Bearbeitung der Partner einer bestimmten Selektion geht, so nutzen Sie bitte im Modul Partner den Zusatzfilter Selektion
![](Zusatzfilter_Selektion.gif)

#### Wo ist denn der Partner ansässig?
<a name="MAP_Kartendienst"></a>
Gerade im Vertrieb aber auch im Support ist es oft interessant zu wissen, wo den der Partner geografisch ansässig ist. Wir haben dafür die Funktion Adresse auf Karte in Browser anzeigen ![](Adresse_im_Browser_anzeigen.gif) geschaffen. Damit wird die im Parameter URL_ONLINE_KARTENDIENST hinterlegte Webseite und zusätzlich als Suchabfrage das Länderkennzeichen, Ort, Straße, Hausnummer übergeben und damit angezeigt, wo der Partner zu finden ist. Default ist dieser Parameter auf OpenStreetMaps eingestellt, Sie können jedoch gerne auch einen anderen Suchdienst verwenden.
Dieses Symbol für die Anzeige im Browser haben wir ergänzend bei allen Kunden, Lieferanten, Partner GoTo Buttons hinzugefügt, sodass Sie z.B. ganz praktisch auch direkt aus dem Projekt die geografische Lage anzeigen können.<br>
Warum wird nicht gleich direkt die Karte eingeblendet?<br>
Dies ist neben dem Thema des Platzes (wo anzeigen) vor allem ein Lizenzrechtliches. Ab einer gewissen Anzahl von Anfragen werden von den Kartendiensten Gebühren verrechnet. Sollte diese Einbindung für Sie interessant sein, wenden Sie sich vertrauensvoll an uns. Wir finden eine Lösung.
