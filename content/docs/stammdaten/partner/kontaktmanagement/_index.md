---
title: "Kontaktmanagement"
linkTitle: "Kontaktmanagement"
categories: ["Partner"]
tags: ["Partner", "Kontaktmanagement"]
weight: 200
description: >
  Verwaltung der offenen Kontakte
---
Kontaktmanagement
=================

Zur Verwaltung Ihrer (offenen) Kontakte steht das Kontaktmanagement zur Verfügung.

Sie finden dieses in den Modulen Kunde ![](KontaktManagement1.gif) und Partner ![](KontaktManagement2.jpg).

Anlegen eines neuen Kontaktes

Wählen Sie den Kunden (Partner) und wechseln in den oberen Modulreiter Kontakt ![](KontaktManagement3.gif).

Mit neu ![](KontaktManagement4.gif) wird ein neuer Kontakt angelegt.

Definieren Sie hier

![](KontaktManagement5.gif)

| Titel | Das Schlagwort, die Überschrift worum es bei diesem Kontakt geht.Denken Sie hier daran, dass beim schnellen Lesen in der Wiedervorlage ein für Sie sprechender Begriff enthalten sein sollte. |
| --- |  --- |
| Ansprechpartner | Mit wem wollen Sie (in den Firmen) sprechen.Sie finden neben dem Ansprechpartner auch die Telefonnummer unter der Sie diese Person erreichen und wenn Sie die Tapi-Schnittstelle besitzen klicken Sie einfach auf das Telefonsymbol ![](KontaktManagement6.gif) um den Partner anzurufen. |
| Kontakt von | Ab wann haben Sie mit dem Partner gesprochen.Beim Anlegen eines neuen Kontaktes ist dieser Eintrag mit jetzt vorbesetzt. |
| Kontakt bis | Bis wann, also wie lange haben Sie mit ihm gesprochen. Sie finden neben der Uhrzeit des Kontakt bis ein Uhrensymbol. Ein Klick auf dieses Symbol übernimmt aktuelles Datum und Uhrzeit, sodass die Dauer Ihres Gespräches ersichtlich ist. |
| Kontaktart | Wie haben Sie den Partner kontaktiert.Die möglichen Kontaktarten werden im Partner, Grunddaten, Kontaktart angelegt. |
| Zugewiesener | Wer sollte sich weiterhin um diesen Kontakt kümmern ?Wählen Sie hier eine Person aus Ihrem Personalstamm aus. |
| Wiedervorlage | Wann wollen Sie wieder mit dem Partner sprechen. |
| Langtext | Durch Klick auf öffnen ![](../Bilder/Text_Bearbeiten.png) kann ein Langtext für die Protokollierung des Gespräches angelegt werden. |
| Erledigen | Durch Klick auf ![](KontaktManagement8.gif) wird ein Kontakt erledigt und er erscheint nicht mehr in der Wiedervorlage. |

Liste der bisherigen Kontakte

Wählen Sie den Kunden (Partner) und wechseln in den oberen Modulreiter Kontakt.

Hier sehen Sie die Liste der bisherigen Kontakte, sortiert nach Kontakt (von) Datum.

Wiedervorlage

Um Ihre aktuell offenen Kontakte zu sehen, wechseln Sie auf Partner, Journal, Wiedervorlage.

Hier erhalten Sie eine Liste Ihrer offenen Kontakte, also der Kontakte die Ihnen zugewiesen sind und die noch nicht erledigt sind.

![](KontaktManagement9.gif)

Diese Kontakte sind nach Wiedervorlagetermin absteigend sortiert.

Unter der Spalte Für ist das Kurzzeichen der Person angegeben, die sich um diesen Termin / Kontakt kümmern sollte. Zur Definition der Kurzzeichen [siehe](../personal/index.htm#Kurzzeichen).

Die angezeigte Liste bleibt im **Kieselstein ERP** Client immer im Vordergrund. Durch einen Doppelklick auf den offenen Kontakt wechseln Sie direkt in die Kontaktvorlage des Partners. So können Sie weitere Aktivitäten für diesen Kontakt setzen / festhalten.

Definition der Kontaktart

Um die verschiedenen Kontaktarten wie Besuch, Telefonat, Schreiben usw. festzulegen müssen diese im Partner, unterer Modulreiter Grunddaten, oberer Modulreiter Kontaktart festgelegt werden.