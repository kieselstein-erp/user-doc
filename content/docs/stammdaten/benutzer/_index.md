---
title: "Benutzer"
linkTitle: "Benutzer"
categories: ["Benutzer"]
tags: ["Benutzer", "Systemrollen", "Rechte", "Lagerrechte"]
weight: 600
description: >
  Verwaltung der Systemrollen und deren Rechte, Benutzer, Login Daten, Benutzer <-> Mandantenzuordnung
---
Benutzer, Rollen und Rechte
========
## Modul Benutzer

#### Wie legt man neue Benutzer an und was ist zu beachten? 
Für das Anlegen von Benutzern sollte in folgender Reihenfolge vorgegangen werden.

1.  Legen Sie die Mitarbeiter (ev. auch externe Personen) unter Personal ![](Pesonalmodul.gif) an. Hier müssen zumindest Personalnummer, Name, Kurzbezeichnung und Kurzzeichen definiert sein. Personalart und Heimatkostenstelle werden automatisch vorbesetzt und müssen auch definiert sein.
2.  Legen Sie nun die [Benutzer](#anlegen-eines-neuen-benutzers) an.
3.  Definieren Sie die Rechte der erforderlichen Systemrollen.
4.  Ordnen Sie die Benutzer den Mandanten, Personen und ihren Systemrollen zu. Siehe dazu [Benutzermandant](#benutzermandant).

#### Anlegen eines neuen Benutzers
Wählen Sie das Benutzermodul ![](Benutzermodul.jpg).

Hier müssen für den Benutzer zwei Definitionen vorgenommen werden.

a.) der eigentliche Benutzer

b.) Die Zuordnung des Benutzer zur Rolle, zur Person und zum Mandanten

Wichtig: Bevor ein Benutzer definiert werden kann, muss diese Person / dieser Mitarbeiter im Personalmodul ![](Pesonalmodul.gif) für den jeweiligen Mandanten angelegt werden.

Wählen Sie zuerst den unteren Modulreiter ![](Benutzer_Modulreiter.gif). Definieren Sie nun die Benutzerkennung (den Anmeldenamen) und das Kennwort. Das Kennwort muss mindestens vierstellig sein. Denken Sie bei der Vergabe der Kennwörter an die bekannten Forderungen nach einem sicheren Kennwort.

Nun definieren Sie den default Mandanten für diesen Benutzer. Dies ist der Mandant, bei dem sich der Benutzer üblicherweise anmeldet. D.h. beim ersten Login in **Kieselstein ERP** erfolgt die Anmeldung automatisch an diesen Mandanten.

Die Benutzer Admin und lpwebappzemecs werden für Spezialzugriffe auf Ihr **Kieselstein ERP** benötigt. Bitte belassen Sie diese im System.

Durch diese Definition haben Sie festgelegt welcher Benutzer sich mit welchem Passwort bei Ihrem **Kieselstein ERP** System anmelden darf. Nun müssen Sie noch definieren, welcher Benutzer, bei welchem Mandanten, welche Rolle (Rechte) hat und mit welcher Person er verknüpft ist.

#### Benutzermandant

Wählen Sie dazu im unteren Modulreiter ![](Benutzermandant_Reiter.gif) und ordnen Sie hier dem Benutzer die für ihn zutreffenden Zugriffsberechtigungen und die zugeordnete Person zu.

![](BenutzerMandant_Zuordnung.gif)

Durch die Zuordnung der Person zum jeweiligen Benutzermandanten wird auch das Kurzzeichen usw. definiert. Sollte bei der gewählten Person kein Kurzzeichen zugeordnet sein, so erscheint eine entsprechende Fehlermeldung. Tragen Sie in diesem Falle bitte unbedingt ein Kurzzeichen für den Mitarbeiter ein. [Siehe dazu auch](#Kurzzeichen).

Sie definieren hier als welcher Benutzer (Loginname und Passwort) bei welchem Mandanten, welche Person bzw. Personalnummer ist und welche Systemrolle sie dort hat.

Eventuell müssen Sie noch weitere Rollen definieren. 

#### Rollen / Rechte / neue Rechte
Es kann vorkommen, dass es nach einem Update von **Kieselstein ERP**  neue Rechte gibt. Wo kann man diese hinzufügen?

Bei einem **Kieselstein ERP** Update werden die Rechte für den Administrator automatisch erweitert. Alle anderen Rollen müssen von Ihnen nachgepflegt werden.

Wechseln Sie in das Modul Benutzer und wählen Sie den unteren Modulreiter Systemrolle ![](Systemrolle.gif).

Nun wählen Sie die gewünschte Systemrolle aus und wechseln im oberen Modulreiter auf Rechte. Nun drücken Sie neu. Sie sehen hier die Liste der für diese Systemrolle noch nicht vergebenen Rechte. 

Für jedes Modul gibt es zwei grundsätzlich unterschiedliche Rechte:

| Recht | Bedeutung | Wirkung |
| --- |  --- |  --- |
| CUD | Create Update Delete | dies bedeutet, dass der Mitarbeiter in diesem Modul das Volle Schreib- und Leserecht hat. |
| R | Read | dies bedeutet, dass der Mitarbeiter in diesem Modul nur Lesendes-Recht hat, also keine Daten verändern darf. |

Die weiteren Bedeutungen der Rechte sind selbsterklärend.

![](Rechte_Beschreibung_Muster.gif)

Hinweis: Hat ein Benutzer auch kein Lese(Read) Recht und kein Create Update Delete(Schreib) Recht auf einem Modul, so wird das entsprechende Icon und der Eintrag im Systemmenü ebenfalls nicht angezeigt.

Achtung: Bitte definieren Sie im Reiter Lager auch die Rechte für die Rolle für das jeweilige Lager.

#### Es sind einige Auswertungsstatistiken nicht sichtbar?
Um Auswertungen zu sehen, die unter anderem auch die Deckungsbeitragsberechnung beinhalten, ist das Rollenrecht LP_FINANCIAL_INFO_TYP_1 erforderlich.

Dieses Recht benötigen Sie, um folgende Auswertungen = Menüpunkte zu sehen:

| Modul | Menü | Menüpunkt(e) |
| --- |  --- |  --- |
| Artikel | Journal | Artikelgruppen, Hitliste, Gestehungspreis über VKPreis |
| Los | Journal | Losstatistik |
| Küche | Journal  | Küchenauswertung 1, Küchenauswertung 2, Deckungsbeitragsanalyse |
| Zeiterfassung | Journal | Arbeitszeitstatistik, Auftragszeitstatistik |
| Kunde | Journal | Umsatzstatistik, Kundenliste |
| Angebot | Journal | Angebotspotential |
| Auftrag | Journal | Auftragsstatistik, Erfüllungsgrad, Nachkalkulation |
| Lieferschein | Journal | Umsatzübersicht |
| Rechnung | Journal | Es wird der gesamte Menüpunkt Journal damit (de-)aktiviert |
| Finanzbuchhaltung | Journal | Erfolgsrechnung  |

#### Was ist unter Benutzermandanten zu verstehen?
Mit der Zuordnung des Benutzermandanten wird definiert, welcher Benutzer bei welchem Mandanten welche Systemrolle einnimmt. Also bei welchen Mandanten der Benutzer welche Berechtigungen hat.

Ein neuer Benutzer kann im Modul Benutzer angelegt werden.

#### Der Benutzer Admin
Der Benutzer Admin hat, neben seinen Rechten die über den Benutzermandenten definiert sind, noch höhere Rechte. So kann nur er die Adressdaten des Mandanten ändern.<br>
Bitte achte auch darauf, in deiner Installation das Passwort für den Admin so einzustellen, dass nur wirklich befugte Personen darauf Zugriff haben.

#### Kurzzeichen nicht definiert
#### Wieso wird beim Ausdruck???/??? angezeigt?
Es ist für den Benutzer kein Kurzzeichen definiert.

![](Kurzzeichen_Druck.gif)

Um ein Kurzzeichen für einen Benutzer zu definieren, muss in dessen Personaldaten ein Kurzzeichen hinterlegt sein.
Da in den Personaldaten auch viele Mitarbeiter angelegt werden, die keine **Kieselstein ERP** Benutzer sind, darf das Kurzzeichen nicht zwingend vorgeschrieben werden. Wurde jedoch ein Kurzzeichen definiert, so muss dieses innerhalb des Mandanten eindeutig sein.
Beim Anlegen des Benutzers muss diesem eine Person aus dem Personalstamm zugeordnet werden. Ist bei dieser Person das Kurzzeichen nicht definiert, so erfolgt eine entsprechende Meldung.

Bei der Verwaltung mehrerer Mandanten beachten Sie bitte, dass die Personenzuordnung zu einem Benutzer auch mandantenabhängig ist. D.h. das Kurzzeichen muss bei der Person des default Mandanten des Benutzers hinterlegt werden.

In anderen Worten: Einem Benutzer ist nur eine Person eines Mandanten zugeordnet. Diese dient dazu, dass für diese eine Person immer die gleiche Zuordnung, das gleiche Kurzzeichen, gegeben ist.

#### Wo werden die Benutzerkurzzeichen definiert?
#### wieso wird bei meinen Kurzzeichen??? angezeigt?
Das Benutzerkurzzeichen muss direkt in der Personalverwaltung definiert werden. [Siehe dazu bitte]( {{<relref "/docs/stammdaten/personal/#wo-werden-kurzzeichen-definiert">}} )

#### Wie wird ein Concurrent User berechnet?
Concurrent User oder auch maximale Anzahl der Benutzer zur gleichzeitigen Benutzung von **Kieselstein ERP**.

Bei der Installation deines **Kieselstein ERP** wurde eine große Zahl von gleichzeitigen Benutzern eingerichtet. In der Regel 100. Sollten diese nicht ausreichen, bitte einfach die Benutzer Anzahl im System höher stellen (lassen).

Als ein Concurrent User wird gewertet, wenn sich jemand von einem Rechner aus an **Kieselstein ERP** anmeldet.
Wird vom gleichen Rechner und vom gleichen Benutzer der **Kieselstein ERP** Client mehrfach gestartet, so wird dies trotzdem nur als ein Benutzer gezählt.
Sollte ein Benutzer sich nicht ordentlich abgemeldet haben (Stromausfall, Rechner abgewürgt, etc.) und er hat sich danach erneut angemeldet, so wird dies ebenfalls nur als ein Benutzer gezählt.
Zusätzlich ist die Berechnung der gleichzeitigen Benutzer auf den aktuellen Tag eingeschränkt.

#### Auf welche Lager darf ein Benutzer buchen?
<a name="Lager-Recht"></a>
In **Kieselstein ERP** kann je Systemrolle definiert werden, welcher Benutzer welche Lager bebuchen darf. Damit kann von Ihnen komfortabel gesteuert werden, dass z.B. nur die Mitarbeiter der Qualitätssicherung auch das Sperrlager verwenden dürfen.

Wählen Sie dazu im Modul Benutzer die entsprechende Systemrolle aus und klicken Sie dann auf den Reiter Lager. Geben Sie nun die Läger an auf die die Benutzer der Systemrolle buchen dürfen. Wichtig: Da die Rolle Systemübergreifend ist, müssen bei mehreren Mandaten die Läger der entsprechenden Mandanten angegeben werden.

#### Einschränkung auf Fertigungsgruppen
<a name="Fertigungsgruppen"></a>
Für Produktionsbetriebe ist es oft praktisch wenn einzelne Benutzer nur auf ausgewählte Fertigungsgruppen buchen dürfen. Um dies zu realisieren, ordnen Sie bitte der Rolle dieser Benutzer eine oder mehrere Fertigungsgruppen zu.

Dies bewirkt:
-   Der Benutzer darf nur die Lose dieser Fertigungsgruppe(n) sehen
-   Er darf, wenn er ein Schreibrecht auf die Lose hat, nur Lose dieser Fertigungsgruppen anlegen, wobei bei mehreren freigeschalteten Fertigungsgruppen automatisch die oberste Fertigungsgruppe verwendet wird. Also die die in der Sortierung zuerst kommt.
-   Es wird die Anzeige auf die Reiter, Auswahlliste, Kopfdaten, Material eingeschränkt.
-   Hat die Rolle auch noch das Recht FERT_LOS_DARF_ABLIEFERN, so wird auch noch der Reiter Ablieferung angezeigt.
