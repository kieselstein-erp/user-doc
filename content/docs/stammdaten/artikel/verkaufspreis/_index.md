---
title: "Verkaufspreis"
linkTitle: "Verkaufspreis"
categories: ["Verkaufspreis"]
tags: ["Verkaufspreis", "Mengenstaffeln", "Preislisten", "Verkaufspreisbasis","Fremdwährungspreise", "Sonderkonditionen"]
weight: 100
date: 2017-01-05
description: >
  Verkaufspreise und deren Staffeln, kalenderfähig definieren
---
Verkaufspreise
================

Der Verkaufspreis, ist wie fast alles in Ihrem Kieselstein ERP Kalenderfähig.

Verschiedene Bezugs-Verkaufspreislisten bei den Mengenstaffeln

Verkaufspreis
=============

In **Kieselstein ERP** sind derzeit drei Arten / Wege der Verkaufspreisdefinitionen der Verkaufspreisfindung vorgesehen

Für die Abhängigkeiten der Preise untereinander siehe:

1. Verkaufspreisdefinition anhand von [Preislisten](#1.Preislisten)

2. Verkaufspreisdefinition anhand von [Mengenstaffeln](#2.VK-Mengenstaffeln)

3. Verkaufspreisdefinition anhand von Kunden [Sonderkonditionen](#3.Soko) / Kunden <-> Artikel Preisvereinbarungen, Kundenartikelnummern

Alle drei Definitionen haben gemeinsam, dass die Preise kalenderfähig sind. D.h. jeder Verkaufspreis hat ein Gültigkeitsdatum ab dem dieser Preis gilt. Das bedeutet auch, dass das Belegdatum die Basis für die Ermittlung des gültigen Preises ist ([siehe auch](#ungültiger Verkaufspreis)). Bitte beachten Sie dies in den verschiedenen Darstellungen.
**Weiters:** Alle drei Definitionen bauen auf der Verkaufspreisbasis auf. Als Besonderheit beachten Sie bitte, dass wenn die Verkaufspreisbasis leer (null) ist, der Gestehungspreis als  Verkaufspreisbasis herangezogen wird.
![](../Bilder/warning.png) Dies hat die Eigenschaft, dass damit so genannte lebende Verkaufspreise realisiert werden. Beachten Sie bitte in diesem Fall, dass die Rabatte negativ eingegeben werden müssen, um einen entsprechenden Aufschlag auf Ihre Gestehungskosten zu erreichen.

**WICHTIG: Für eine korrekte Verkaufspreisfindung MUSS die Verkaufspreisbasis richtig definiert werden. Üblicherweise wird hier der teuerste Preis eingetragen. Das ist üblicherweise der Endkundenpreis. Wird hier z.B. 0,00 eingegeben, so haben Sie zwar einen gültigen Preis definiert, das ist aber von der Logik der Verkaufspreisfindung unbedingt zu vermeiden.**

Alternativ steht die automatische Preisfindung auf Basis des Lieferantenpreises zur Verfügung.
D.h. wenn der Parameter VKPREISBASIS_IST_LIEF1PREIS auf 1 gesetzt ist, so wird anstelle der Verkaufspreisbasis der Einzelpreis des bevorzugten (= 1.) Artikellieferanten verwendet.
Dies kommt vor allem dann zum Einsatz, wenn die Verkaufspreise von den Lieferanten getrieben werden und Sie immer mit Aufschlagskalkulationen arbeiten.

Alternativ steht die Verkaufspreisautomatik zur Verfügung. [Siehe dazu bitte](#Verkaufspreisautomatik):

<a name="1.Preislisten"></a>1. Verkaufspreisdefinition anhand von Preislisten

Definieren Sie dazu zuerst die in Ihrem Unternehmen verwendeten Preislisten im Artikelmodul, unterer Modulreiter ![](ModulReiter_Preisliste.gif).
Bitte beachten Sie dass Preislisten aktiv sein müssen um wirksam zu sein.
Preislisten können jederzeit aktiviert werden. Deaktiviert können Preislisten nur werden, wenn keine Zuordnung mehr für Kundenpreislisten und keine Zuordnung mehr zur Vorbelegung im Mandanten gegeben ist.

Im Artikel können nun für jeden Artikel die entsprechenden Verkaufspreise definiert werden.
![](Verkaufspreise.gif)
Hier definieren Sie ab wann welche Preise gültig sind. Dies können Fixpreise oder auch Rabatte sein. Der Vorteil des Fixpreises ist, dass Sie damit jeden beliebigen Preis definieren können. Sein Nachteil ist, dass er bei Preisänderungen der Verkaufspreisbasis nicht mitgeändert wird. Alternativ zum Fixpreis kann ein Rabattsatz angegeben werden. Das wird vermutlich Ihre bevorzugte Preisdefinition sein, da damit die Preispflege auf die Pflege der Verkaufspreisbasis reduziert werden kann. **WICHTIG:** Bitte beachten Sie bei der Preisdefinition immer die Gültig ab und die Preisgültigkeitsanzeige.
Die Preisgültigkeitsanzeige definiert, ab welchem Datum die angezeigten und berechneten Preise dargestellt werden sollten. Das Gültig ab Datum definiert für jeden einzelnen Preis, ab wann dieser Preis nun tatsächlich zur Anwendung kommt und zu welchem Datum dieser Preis gespeichert wird. **Weiters:** Die Preisgültigkeitsanzeige wird nur beim Modulneustart auf den heutigen Tag gesetzt. Wurde die Preisgültigkeitsanzeige auf ein bestimmtes Datum gesetzt, so bleibt diese immer gleich, was den Vorteil hat, dass die Preispflege für einen bestimmten Stichtag wesentlich vereinfacht wird. **Hinweis:** Die Preislistennamen werden nur beim Neustart des Artikelmoduls aktualisiert. D.h. wurde die Definition der Preislisten verändert, so muss, aus Performance Gründen, das Artikelmodul neu gestartet werden, damit Sie die aktualisierten Preislisten sehen. **Hinweis2:** Als Alternative zur Berechnungsbasis anhand der Verkaufspreisbasis steht auch der Preis des bevorzugten Lieferanten zur Verfügung. Es muss dazu der Parameter VKPREISBASIS_IST_LIEF1PREIS auf 1 gestellt werden. Nach dem Neustart des Artikelmoduls wird keine Verkaufspreisbasis mehr angezeigt. Anstatt dessen ist der Lief1Preis ein Pflichtfeld.
Mit dieser Einstellung kombiniert wird oft auch die automatische Vorgabe der (negativen) Rabatte je Preisliste verwendet.

#### Alle Verkaufspreise einer Artikelgruppe (und Preisliste) um einen bestimmten Prozentsatz verändern:
Um eine erleichterte Preispflege gewährleisten zu können, kann man alle Artikel einer Artikelgruppe (und Preisliste) um den gleichen Prozentsatz anheben oder erniedrigen. Über das Modul Artikel und den Menüpunkt Pflege, den Punkt Verkaufspreise aktualisieren. Falls dieser nicht angezeigt wird, fügen Sie das Benutzerrecht: WW_DARF_LAGERPRUEFFUNKTIONEN_SEHEN hinzu. Es werden alle Verkaufspreise inklusive der VK-Staffelpreise ab dem gewählten Datum um den gewünschten Prozentsatz verändert.

Genauer:
Es werden **nur** die Preise der Verkaufspreisbasis um den gewünschten Prozentsatz verändert.
Wurde zusätzlich eine Preisliste angegeben, so werden zusätzlich die Fixpreise der gewählten Preisliste um den gewünschten Prozentsatz verändert.
Die in den Preislisten und in den Staffelpreisen angegebenen Rabattsätze bleiben unverändert. Damit werden NUR die auf der Verkaufspreisbasis basierenden Preise verändert.

#### Was passiert mit Preisdefinitionen die auf ein gültig ab später eingestellt sind?
Bei der Preispflege wird nur ein weiteres gültig ab eingetragen. D.h. wurden vor der nun gewünschten Preiserhöhung bereits verbindliche Preise eingetragen, so wirkt die Preiserhöhung nur für den davor nicht definierten Zeitraum.
Beispiel gültig für die Verkaufspreisbasis und damit für alle darauf basierenden Preise.
Gültig ab        VK-Preisbasis
1.1.2021        100,00
1.3.2022        120,00 vorab eingepflegt weil mit Kunden so besprochen etc.

Nun eine allgemeine Preiserhöhung über alle Artikel um 10% zum 1.1.2022, ergibt nun ein zusätzliches
Gültig ab        VK-Preisbasis
1.1.2022        110,00

Bedeutet: Vor der Preiserhöhung hat der Artikel im Zeitraum von 1.1.2022 bis 28.2.2022 nur 100,00 gekostet. Nach der Erhöhung kostet er für den Zeitraum von zwei Monaten 110,00\. Der ab 1.3.2022 vereinbarte Preis bleibt erhalten.

<a name="Mindest-VK"></a>

#### Mindestverkaufspreis (Mindest-VK) was ist das?
Dies ist der unterste Verkaufspreis. Er errechnet sich aus Gestehungspreis + Mindestdeckungsbeitrag.
Beispiel: Gestehungspreis 100,- €, Mindestdeckungsbeitrag 30% -> 100 * (1+30%) = 130,- €

Wird beim Verkauf (Angebot - Rechnung) ein Nettopreis unter diesem Preis eingegeben, so erscheint eine entsprechende Warnung.

[Siehe dazu auch](#Mindest-DB).

<a name="Tiefster-VK"></a>

#### Tiefster Verkaufspreis?
Der tiefste Verkaufspreis dient als Steuerelement für Ihren Verkauf, Ihren Vertrieb. Er errechnet sich aus dem Mindestverkaufspreis + Aufschlag.
Beispiel: Mindest-VK 130,- €, Aufschlag 20% -> 130 * (1+20%) = 156,- €

<a name="Soll-Vk"></a>

#### Soll- Verkaufspreis?
Der Soll-Verkaufspreis ist ein weiteres Steuerungselement für Ihren Verkauf. Er errechnet sich aus dem tiefsten Verkaufspreis + Soll.
Beispiel: Mindest-VK 130,- €, Aufschlag 20%, Soll 40% -> 130 * (1+20%) * (1+40%) = 218,40 €
Gedacht sind die beiden Werte um Ihrem Verkauf eine Unterstützung für die Preisfindung zu geben.
Sie werden derzeit nur bei der Stücklistengesamtkalkulation ausgewiesen.

#### Verkaufspreispflichtig
Grundsätzlich sollte im Bereich des Verkaufes jeder Artikel aus dem **Kieselstein ERP** Artikelstamm einen Verkaufspreis haben.
Darum kommt auch bei der Erfassung einer Verkaufsposition die Meldung Tatsächlich mit Nullpreis speichern.
Da dies z.B. für die kostenlose Beistellung von Verpackungsmitteln (Europaletten) lästig ist, kann durch entfernen des Hakens bei
![](Verkaufspreispflichtig.gif) VK-Preispflichtig diese Meldung abgeschaltet werden.

<a name="2.VK-Mengenstaffeln"></a>2. Verkaufspreisdefinition anhand von Mengenstaffeln

Basierend auf der Verkaufspreisbasis können zusätzlich Mengen und deren Fixpreis bzw. deren Rabatt angegeben werden.

Der VK Preis wird aufgrund von Mengenstaffeln bestimmt, die für einen Artikel hinterlegt wurden.
Für eine Mengenstaffel wird für einen bestimmten Zeitraum entweder ein Fixpreis oder ein Rabattsatz erfasst. Bezugsgröße für den Rabattsatz ist dabei die VK-Preisbasis des Artikels.
Wenn die VK-Preisbasis nicht hinterlegt ist, gilt der gemittelte Gestehungspreis des Artikels am Hauptlager des Mandanten.

Das bedeutet: Wenn im Panel VK-Preise die VK-Basis für einen Artikel geändert wird, werden alle Mengenstaffeln, die sich auf diese VK-Basis beziehen ebenfalls entsprechend geändert.

Die Wartung der VK-Mengenstaffeln erfolgt in einem 1:n Panel (Liste der Mengenstaffeln + Detail). Das Detail ermöglicht die Erfassung und Wartung einer Mengenstaffel, Mengenstaffeln können auch gelöscht werden.

Außerdem verfügt das Detail über eine Preisgültigkeitsanzeige (vgl. VK-Preise). Default ist das aktuelle Datum. Die Preisgültigkeit wirkt sich auf die angezeigte VK-Basis aus.

Der Benutzer gibt den Gültigkeitsbereich einer Mengenstaffel, die Menge und den Preis, der für die Staffel gilt, an. Zu seiner Information werden Gestehungspreis des Artikels und die aktuell gültige VK-Basis angezeigt. Bei der Erfassung der Mengenstaffel darf es keine Überlappungen im Datumsbereich geben, zu einem bestimmten Kalendertag kann also für eine bestimmte Menge maximal ein Preis hinterlegt sein.
Wenn eine neue Mengenstaffel erfasst wird, muss der Beginn nach dem letzten Ende liegen. Wenn das letzte Ende leer (null) war, wird es automatisch ergänzt, sodass eine fortlaufende Folge von Mengenstaffeln entsteht.

Preisauswahl in den Belegen: Wenn die VK-Preisfindung ermittelt, dass die VK-Mengenstaffeln greifen, dann erhält der Benutzer einen entsprechenden Dialog, in dem er ALLE Mengenstaffeln zu diesem Artikel sieht und wählen kann. Außerdem hat er die Optionen Handeingabe Fixpreis und Rabattsatz. Vorauswahl ist die zutreffende Mengenstaffel.

Weiters können Mengenstaffeln auch mit einem Preislistenbezug angegeben werden. Siehe dazu Parameter PREISBASIS_VERKAUF.

| Wert | Bedeutung |
| --- |  --- |
| 0 |  Die Mengenstaffel bezieht sich immer auf die Verkaufspreisbasis. D.h. egal welche Preisliste für den Kunden gilt, es wird bei den Mengenstaffeln immer in Bezug auf die Verkaufspreisbasis der Verkaufspreis errechnet. D.h. es wird kein Unterschied bei unterschiedlichen (Händler-)Preislisten gemacht |
| 1 | Preisbasis lt. Kundenpreisliste. D.h. Es wird als Preisbasis der lt. Preisliste des Kunden erechnete Betrag verwendet.In dieser Einstellung muss je Mengenstaffeleintrag definiert werden für welche Preisliste dieser Eintrag gilt. Auch hier kann definiert werden dass dieser Eintrag auf der Verkaufspreisbasis wirkt oder dass er für alle Preislisten gilt.![](Preisbasis_VK_Mengenstaffel.gif)Für eine klare Trennung verwenden Sie bitte die jeweiligen Preislisten. |
| 2 | Wie 1, jedoch wird der zusätzliche Rabatt in den Zusatzrabatt der Erfassung übernommen, womit Sie Ihrem Händler-Kunden signalisieren können, dass er nicht nur seinen Händlerrabatt bekommt, sondern eben zusätzlich auch die entsprechende Mengenstaffel berücksichtigt wurde |
| 3 | Wie 1 jedoch nur aktive Preislisten |

Wird nun in der Definition der Mengenstaffel der Preislistenbezug **alle Preislisten** ausgewählt, so bedeutet dies, dass die in den Mengenstaffeln angegebenen Rabatte für die jeweilige Kundenpreisliste gilt.

<a name="3.Soko"></a>3. Verkaufspreisdefinition anhand von Kunden Sonderkonditionen / Kunden <-> Artikel Preisvereinbarungen / Sonderpreisvereinbarung

-   Für einen einzelnen Artikel oder

-   für eine Artikelgruppe

Definieren Sie hier die mit Ihrem Kunden vereinbarten Sonderkonditionen (SOKO), ab einer bestimmten Menge, für einen bestimmten Zeitraum.

SOKOs können für einen einzelnen Kunden oder eine Kundegruppe definiert werden. Der Begriff der Kundengruppe ist dabei gleichzusetzen mit der Rechnungsadresse des Kunden. Das bedeutet:
In der Kunden-SOKO Auswahlliste wird der Eintrag des Artikels Rot angezeigt, wenn dieser nicht rabattierbar ist.
SOKOs werden für Kunden + Artikelgruppen oder einzelne Artikel erfasst.

-   Es gilt: Einzelner Artikel > Artikelgruppe.

-   Ein Kunde kann auch ein Konzern sein.

-   Bezugsgröße für den Standardrabattsatz ist die VK-Preisbasis des Artikels. Wenn keine VK-Basis erfasst wurde, gilt der Gestehungspreis des Artikels (siehe VK-Mengenstaffeln).

-   Wenn sich die VK-Basis ändert, ändern sich entsprechende die erfassten SOKOs, der Preis wird zur Laufzeit berechnet und ist nicht in der DB abgespeichert.

-   In der Liste der KundenSOKOs gibt es eine Spalte für Artikel/Artikelgruppe. Eine Artikelgruppe wird zusätzlich durch ein vorangestelltes "G" gekennzeichnet, beim Artikel bleibt das Zeichen leer (nach dieser Spalte kann nicht sortiert werden).

-   Die SOKO greift nur dann, wenn ein Artikel als rabattierbar gekennzeichnet ist (Artikel>Sonstiges).

-   Ablauf der Verkaufspreisfindung Stufe 3:

1.  Hat der Kunde eine SOKO für den Artikel hinterlegt?

2.  Hat der Kunde eine SOKO für die Artikelgruppe des Artikel hinterlegt?

3.  Hat der Kunde eine SOKO für den Artikel für seine Rechnungsadresse hinterlegt?

4.  Hat der Kunde eine SOKO für die Artikelgruppe des Artikel für seine Rechnungsadresse hinterlegt?

Die Wartung der KundenSOKOs sitzt im Modul Kunde. Sie besteht aus 2 Panels:

1.  1:n Liste der KundenSOKOs zu einem Kunden. Im Detail erfolgt die Wartung einer KundenSOKO.

2.  1:n Liste der Mengenstaffeln zu einer KundenSOKO.

Alternativ dazu können die Artikel auch direkt aus dem Reiter Sonderkonditionen aus dem Artikelmodul gepflegt werden.
Bitte beachten Sie, dass Rabatte in den Sonderkonditionen nur für diejenigen Kunden vergeben werden können, die in als Währung die gleiche Währung wie Ihr Unternehmen / Mandant haben.

Verkaufspreisfindung

Bei der Reihenfolge der Verkaufspreisfindung wird in folgender Weise vorgegangen:

1. Gibt es für diesen Artikel und diesen Kunden eine Sonderpreisvereinbarung

2. Gibt es für diesen Artikel und die verkaufte Menge eine gültige Mengenstaffel

3. Gibt es für diesen Artikel und diesen Kunden einen gültigen Preislistenzusammenhang

4. trafen die obigen 3 Bedingungen nicht zu, so wird der 1.Verkaufspreis des Artikels herangezogen.

#### Ich habe eine neue Preisliste hinzugefügt, sehe diese aber nicht
Wenn die Darstellung der Preislisten geändert wird, also neue Preislisten hinzugefügt bzw. aktiviert oder deaktiviert werden, so muss das Artikelmodul neu gestartet werden, damit diese sichtbar werden.

Abhängigkeiten der Preise untereinander:

In **Kieselstein ERP** werden grundsätzlich drei verschiedene Preise unterschieden.
|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
|---|---|
| Einkaufspreis | Zu diesem Preis bestellen Sie Ware bei Ihren Lieferanten |
|Gestehungspreis | [siehe]( {{<relref "/docs/schulung/begriffe/#gestehungspreis--gleitender-durchschnittspreis" >}} )  Der durchschnittliche Preis der lagernden Waren |
| Verkaufspreis | Mit diesem Preis verrechnen Sie Positionen an Ihre Kunden |

Vom Einkaufspreis zzgl. Transportkosten (und eventueller Fixkosten abzüglich allgemeinen Rabatt einer Bestellung) wird der Einstandspreis ermittelt. Mit Einstandswert (Einstandpreis * Menge) + altem Lagerwert wird der aktuelle Gestehungspreis ermittelt. Der Gestehungspreis + dem Mindestdeckungsbeitrag ergibt den Mindest-Verkaufspreis. Wird im Verkauf dieser Preis unterschritten so wird eine entsprechende Fehlermeldung ausgegeben.

#### Aufgrund welcher Adresse wird der zu verwendende Verkaufspreis bestimmt?
Dies ist in aller Regel die erste Adresse des Verkaufsbeleges.

Beim Lieferschein werden die Konditionen jedoch aufgrund der Rechnungsadresse bestimmt, also auch die Regel für die Verkaufspreisfindung.

#### Wie wird der Verkaufspreis in den Belegen durchgereicht?
In der Regel definiert immer der Vorgängerbeleg, welcher Preis in den Nachfolgebeleg übernommen werden sollte. Selbstverständlich kann im jeweiligen Beleg, der gewünschte Preis abgeändert werden.
D.h. wenn Sie in einem Angebot einen Verkaufspreis definiert haben und daraus dann ein Auftrag erzeugt wird, wird für jede Angebotsposition der Verkaufspreis in den Auftrag übernommen.
Selbstverständlich kann im Auftrag dann der Positionspreis, falls erforderlich, abgeändert werden.
Zusätzlich finden Sie in Angebot, Auftrag, Lieferschein auch den Rechen-Knopf ![](VK_Preis_neu_berechnen.gif) mit dem für die Positionen des Beleges die Preise neu errechnet werden können.
Üblicherweise wird auch bei der Überleitung eines Auftrages in den Lieferschein bzw. die Rechnung diese Logik angewandt.
Aufgrund der aktuell massiven Preisschwankungen im Rohmaterial, gibt es für die Überleitung von Auftrag in den Lieferschein, hier anstatt der Preise aus dem Auftrag die aktuell für das Lieferscheindatum gültigen Verkaufspreise, anhand der oben beschriebenen Verkaufspreisfindung neu zu berechnen. Setzen Sie dafür für die Rechnungsadresse im Kunden im Reiter Konditionen bei ![](VK-PREIS_anhand_LS_Datum.jpg) VK-Preis kommt aus LS-Datum den Haken.

<a name="Gestehungspreis anpassen"></a>

#### Kann der Gestehungspreis verändert werden?
Ja. Voraussetzung dafür ist das Recht WW_ARTIKEL_GESTPREISE_CU.
Damit können im Reiter Lager, des jeweiligen Artikels die momentanen Gestehungspreise des Artikels geändert werden. Bitte beachten Sie, dass damit KEINE nachträglichen Gestehungspreisänderungen bereits entnommener Artikel durchgeführt werden können.
Ist für einen Artikel bzw. für das gewünschte Lager noch kein Lagerstand eingetragen, so wird dieser normalerweise in der Liste der Lagerstände des Artikels im Reiter Lager nicht angezeigt. Fügen Sie mit Neu und durch Auswahl des gewünschten Lagers den Lagereintrag hinzu. Nun kann der Gestehungspreis für diesen Artikel für dieses Lager für zukünftige Lagerbetrachtungen / Lagerabbuchungen verändert werden. Bitte beachten Sie, dass der Gestehungspreis bei jeder Lagerzubuchung aktualisiert wird. Ist für einen Artikel noch kein Lagerstand gegeben, so bedeutet dies, dass der Gestehungspreis direkt auf den neuen Einstandspreis gesetzt wird. **Bei lagernden Artikeln wird bei der Änderung des Gestehungspreises aus Gründen der Aufrollung dieser speziellen Durchschnittspreisberechnung, automatisch der gesamte Lagerstand vom Lager genommen und danach mit dem neuen Gestehungspreis wieder zugebucht. Dadurch geht der Bezug auf den tatsächlichen Wareneingang verloren. Verwenden Sie daher diese Funktion mit bedacht.**

<a name="Mindest-DB"></a>

#### Kann ich den Mindestdeckungsbeitrag auch Null setzen?
Ja. Aber **<u>HÖCHSTE Vorsicht</u>**!!!!
Der Mindestdeckungsbeitrag ist Ihre Sicherheit, dass Sie für jedes Produkt den für das Überleben des Unternehmens erforderlichen Beitrag erwirtschaften.
Bitte bedenken Sie dazu folgendes:

1.  Die Ware wird mit dem Einstandspreis zugebucht und dadurch mit dem Gestehungspreis auf Lager gelegt

2.  Die Ware wird in die Fertigungsaufträge bzw. in Lieferscheine / Rechnungen entnommen

3.  Bei den Fertigungsaufträgen wird Arbeitszeit ev. auch Maschinenzeit zu den hinterlegten Kosten zugebucht.

4.  Bei der Ablieferungsmeldung der Lose wird der Gestehungspreis des jeweiligen Loses mit bestmöglicher Genauigkeit errechnet. D.h. auch die Rückmeldung des Loses ans Lager ist "nur" auf Basis Gestehungspreis

5.  Wird dieser Artikel nun verkauft, so wird beim Abspeichern der Position der eingegebene Nettopreis mit dem dem Lieferschein (Rechnung) zugeordneten Gestehungspreis, also Ihren Einkaufs / Herstellkosten verglichen und wird der Mindestverkaufspreis unterschritten erhalten Sie eine entsprechende Warnung.

6.  Die Praxis zeigt, dass immer wieder Ereignisse auftreten, die höhere Gestehungskosten verursachen. Das können sowohl längere Fertigungszeiten sein aber auch oft geänderte Materialpreise. Denken Sie hier z.B. an stark schwankende Stahlpreise, das weite Thema der Material / Kupferzuschläge, erhöhte Transportkosten usw.

7.  Würde nun der Mindestdeckungsbeitrag auf einen zu geringen Wert, im schlimmsten Falle auf Null gesetzt, liefert dieses Produkt zuwenig / keinen Cent an Deckungsbeitrag für Ihr Unternehmen. Überspitzt formuliert: Es wäre für Sie besser und billiger, wenn Sie das Produkt nicht liefern würden.

#### Verkaufspreisfindung auf Basis Lieferantenpreis
Mit dem Parameter VK_PREISBASIS_IST_LIEF1PREIS kann eingestellt werden, dass als Verkaufspreisbasis anstelle der pro Artikel fest hinterlegen Verkaufspreisbasis der Nettopreis des ersten Lieferanten verwendet wird.
Damit erreichen Sie, dass sich die vorgeschlagenen Verkaufspreise immer am Nettopreis Ihres Haupt-Lieferanten orientieren.
Bitte beachten Sie, dass hier auch eventuelle Wechselkurs Umrechnungen enthalten sind. D.h. wenn ein Lieferant z.B. in USD hinterlegt ist, so wird für die Berechnung Ihrer Verkaufspreise dieser USD Betrag auf den Betrag Ihrer Mandantenwährung umgerechnet. **Hinweis:** Wenn diese Funktion verwendet wird, werden die Rabatte in aller Regel als Aufschläge definiert werden müssen, um einen entsprechenden Deckungsbeitrag zu erzielen. Aufschläge können bei den Rabatten durch Angabe negativer Rabatte eingegeben werden.

#### Verkaufspreisfindung auf Basis des letzten Auftragspositionspreis
Mit dem Parameter VERKAUFSPREIS_RUECKPFLEGE  kann als Verkaufspreisbasis anstelle der pro Artikel fest hinterlegen Verkaufspreisbasis der Preis der letzten Auftragsposition verwendet werden. Im Detail bedeutet das, dass beim Speichern der Auftragsposition die VK-Basis des Artikels zum Auftragsdatum aktualisiert wird.

#### Standard Rabatte bei neuen Artikel
In der Definition der Preislisten kann auch ein Standard-Rabatt hinterlegt werden. Dies bewirkt, dass beim Anlegen eines neuen Artikels automatisch diese Rabatte mit Preisgültigkeit ab heute (= Anlagedatum) eingetragen werden.

Diese Rabatte sind nur Vorschlagswerte und können im oberen Modulreiter VK-Preise geändert werden.

Fragen rund um den Gestehungspreis

#### Kann der Gestehungspreis kleiner dem Einkaufspreis sein?
Von der Grundidee her ist der Gestehungspreis immer gleich oder größer dem Einkaufspreis.

Musste von Ihrer Seite jedoch eine Preiserhöhung im Einkauf akzeptiert werden und war noch alte, günstige Ware auf Lager, so gibt es durchaus auch Konstellationen in denen der Gestehungspreis unter dem Einkaufspreis ist.

Hinweis:

Leider kommt es auch immer wieder vor, dass bei Handbuchungen schlampig gearbeitet wird und einfach kein Gestehungspreis für neue Artikeln bei Handzubuchungen eingegeben werden. Später wird dann der Artikel über die Bestellungen oder den Fertigungsauftrag zugebucht und man wundert sich, dass die Preise nicht stimmen.

In diesem Falle können Sie:

- die Handbuchung richtigstellen

- den Gestehungspreis aktualisieren

Die Richtigstellung der Handbuchung ist vorzuziehen, da dadurch die Verknüpfung der Lagerbewegungen (Zugang mit Abgang) erhalten bleibt. Bei der Korrektur des [Gestehungspreises](#Gestehungspreis anpassen) muss ein Lagerdurchgang gebucht werden, daher ergibt sich, dass alle lagernden Artikel abgebucht und dann neu zugebucht werden müssen.

**Preispflege in **Kieselstein ERP** über Export/Import**

Die Preispflege über mehrere Artikel hinweg ist in **Kieselstein ERP** durch einen Export und darauffolgenden Import gelöst. So können Sie schnell Preise aktualisieren ohne jeden Artikel einzeln zu bearbeiten.
![](Preispflege_Menue.JPG)
Wechseln Sie dazu in das Modul Artikel obere Menüleiste Pflege und hier Preispflege-Export (wenn die Berechtigung VK-Preise sehen/ändern und WW_DARF_LAGERPRUEFFUNKTIONEN_SEHEN vorhanden sind). 

![](Preispflege_Export.JPG)

Nun wählen Sie die gewünschte Artikelgruppe / etc. aus und speichern die Exportdatei.
Hier sind Export Filter nach: Artikelgruppen mit Untergruppen, Artikelklasse mit Unterklassen, Shopgruppen mit Untergruppen sowie Artikelnummer von-bis, sowie der Zusatz versteckte j/n möglich.

Nach dem durchgeführten Export erscheint
![](Preispflege_Export_durchgefuehrt.JPG)
also der Hinweis, wo denn die XLS-Datei abgespeichert wurde.

Der Aufbau der Datei beinhaltet:
Artikelnummer, Bezeichnung, Kurzbezeichnung, Zusatzbezeichnung, Referenznummer, Revision, Index, Einheit, Lieferant, Einzelpreis, Rabatt, Nettopreis, Gültig seit, VK-Basis, Gültig seit, Fixpreis, Rabatt, Errechneter Preis, Gültig ab, Währung, LetzterAbgang, LetzterZugang, Reservierungen Auftrag, Reservierungen Forecast, Reservierungen Forecast (Alle), Artikelgruppe, Artikelklasse, Shopgruppe
Die Preise sind immer zum heutigen Preisgültigkeitsdatum angegeben.
Reservierungen Forecast (Alle) bedeutet, dass dies die Forecastmengen sind, unabhängig von den Call-Off Tag, Call-Off Woche bzw. Forcastauftrag Monaten. D.h. die Abschaltung der Wirkung der Reservierung der Forecastaufträge hat in der Spalte Alle keine Wirkung.

In diesem XLS-File kann nun die VK-Preisbasis und das Gültigkeitsdatum der Basis verändert werden. Außerdem können die Fixpreise/Rabatte und Gültigkeit jeder Preise verändert werden. Wenn in einer Preisliste ein Fixpreis hinterlegt wird, dann hat dieser Vorrang vor dem Rabattsatz. Alle anderen Spalten sind für den Import irrelevant.

Um einzelne Preislisten nicht zu ändern, entfernen Sie dazu die 5 Spalten Fixpreis / Rabatt / Errechneter Preis / Gültig ab / Währung. Einzelne Spalten einer Preisliste können nicht gelöscht werden.
Sobald Sie mit der Überarbeitung fertig sind, speichern Sie die Datei ab.

Importieren Sie die bearbeitete Datei im Modul Artikel obere Menüleiste Pflege, Preispflege-Import.
![](Preispflege_Import.JPG) **Achtung**: Die VKPreisbasis und das Gültigkeitsdatum müssen in ihren Original-Spalten, aktuell sind dies Spalte N und O eingetragen sein.
Wenn die Datentypen in den Spalten nicht korrekt sind, erscheint eine Fehlermeldung und der Import wird abgebrochen und alle Änderungen verworfen. Am besten verwenden Sie die exportierte Datei für den Import also: exportieren der gewünschten Artikel -- speichern die Datei ab -- nehmen die Änderungen vor -- speichern die Datei ab -- importieren diese Datei.

Gerne können einzelne Artikelzeilen entfernt werden und damit werden diese Artikel nicht verändert. Beim Löschen von Spalten bitte mit Bedacht, wie oben beschrieben vorgehen.

#### Kann eine Begründung für Preisänderungen angegeben werden?
Ja. Schalten Sie dazu den Parameter BEGRUENDUNG_BEI_VKPREISAENDERUNG auf 1.
Damit wird bei jeder Preisänderung eine entsprechende Begründung abgefragt.
![](Begruendung_Preisaenderung.jpg)
Diese wird unter Info, Verkaufspreisentwicklung angezeigt.

Diese Begründung muss auch bei der Preispflege durch den VK-Preis-Import angegeben werden.

**<a name="Verkaufspreisautomatik"></a>Verkaufspreisautomatik**

Alternativ zur Version, dass der Lief1Preis als Verkaufspreisbasis verwendet wird, kann auch die VERKAUFSPREIS_AUTOMATIK_LIEF1PREIS zur Verfügung.
Dies bedeutet im wesentlichen, dass immer bei Änderung des Einkaufspreises des bevorzugten Lieferanten automatisch die Verkaufspreisbasis aktualisiert wird, wobei der bei dem Parameter VERKAUFSPREIS_AUTOMATIK_LIEF1PREIS hinterlegte Prozentsatz als Basis für die Umrechnung verwendet wird.
Beachten Sie bitte, dass dies nicht für die Mengenstaffeln gilt.
Die Automatik greift nur für Preisänderungen des bevorzugten Lieferanten bzw. dann wenn ein Lieferant durch Umsortierung zum ersten Lieferanten wird.
Zusätzlich zum Parameter gibt es in der Artikelgruppe die Möglichkeit hier eine andere Verkaufspreisautomatik ![](EK_Preis_Aufschlag.gif) zu definieren bzw. diese für die gewählte Artikelgruppe abzuschalten. Bitte beachten Sie dass die Definition keine Vater- bzw. Kindgruppen berücksichtigt.
Die Logik ist nun wie folgt:
- der EK-Preisaufschlag der Artikelgruppe null = leer, so greift die Definition des Parameteres VERKAUFSPREIS_AUTOMATIK_LIEF1PREIS
- ist ein Wert ungleich -1 eingegeben, so greift dieser Prozentsatz
- ist -1 eingegeben, so bedeutet dies, dass die Verkaufspreisautomatik für diese Artikelgruppe abgeschaltet ist
- wird nun, anhand obiger Regel auf den allgemeinen Parameter VERKAUFSPREIS_AUTOMATIK_LIEF1PREIS zurückgegriffen, so gilt hier analog
- ist dieser ungleich -1 so wird mit diesem Prozentaufschlag vom Netto(Einkaufs)preis die Verkaufspreisbasis errechnet

<a name="ungültiger Verkaufspreis"></a>
#### Ich erhalten einen ungültigen Verkaufspreis
#### Ich erhalten keinen Verkaufspreis
Wie oben beschrieben ist die Basis für die Verkaufspreisfindung das Datum der Preisgültigkeit in Relation zum Belegdatum. D.h. es wird, ausgehend vom Belegdatum (Datum des Angebotes usw.) ein zum gleichen Tag oder davor definierter Verkaufspreis, inkl. Verkaufspreisbasis, Preislistenpreis, Verkaufsmengenstaffel und Kunden Sonderkonditionen gesucht.
Eine immer wieder vorkommende Fehlbedienung ist, dass z.B. ein Angebot heute angelegt wird. Dann wir am Folgetag ein neuer Artikel definiert und ein Verkaufspreis eingegeben. Aufgrund dessen wird für den neuen Artikel die Preisgültigkeit ab heute, also dem Folgetag gesetzt und somit für das Angebot, das ja ein Belegdatum von gestern hat, kein Verkaufspreis gefunden.
Unser Rat daher, bitte beachten Sie bei der Festlegung Ihre Verkaufspreise auch ab wann diese nun tatsächlich gültig sind.

#### Können auch Preislisten in Fremdwährungen definiert werden?
Ja. Bei der Definition der [Preislisten]( {{<relref "/docs/stammdaten/artikel/#wo-k%C3%B6nnen-verkaufspreislisten-definiert-werden" >}} ) kann auch definiert werden, dass diese Preisliste in einer abweichenden Währung definiert ist.<br>
Hier können sowohl Fixpreise als auch Rabatte angegeben werden. Bitte beachten Sie, wenn hier Fixpreise angegeben werden, liegt das Währungsrisiko bei Ihnen.