---
title: "Paternoster"
linkTitle: "Paternoster"
categories: ["Paternoster"]
tags: ["Paternoster"]
weight: 700
description: >
  Artikel in Hochregalen, sogenannten Paternostern lagern
---
Paternoster
===========

In **Kieselstein ERP** steht Ihnen auch die Möglichkeit der Anbindung weiterer Lagersysteme zur Verfügung.

Da die Kommunikation mit diesen Systemen grundsätzlich sehr typenabhängig ist, müssen diese von uns für Sie parametriert werden. [Siehe unten](#parametrierung)

Nach der erfolgten Parametrierung steht Ihnen im Modul Artikel unter Grunddaten, oberer Modulreiter Lagerplatz die Zuordnungsmöglichkeit des Paternosters zur Verfügung. Definieren Sie also welcher Paternoster mit welchem Lagerplatz korrespondiert.

Zusätzlich erhalten Sie im Artikel die Anzeige des aktuellen Lagerstandes im Paternoster.

Die Aktualisierung der Daten des Paternosterlagers kann im Modul System unterer Modulreiter Automatik, Auswahl von Alle Lagerstände von den Paternostern einlesen parametriert werden. Hier wird, entgegen den anderen Automatikjobs das Zeitintervall in Minuten eingegeben. Bitte beachten sie, dass:

a.) die Abfrage durch das Update des Paternosterlagers den Server belastet,

b.) die Abfrage zentral vom Server aus erfolgt.

Bitte beachten Sie auch die Reaktionsgeschwindigkeiten der externen Lagersysteme, die sehr oft noch mit alten Betriebssystemen usw. ausgerüstet und daher entsprechend langsam sind.

Derzeit werden folgende Systeme unterstützt:

- Hänel Leanlift, Kommunikation per FTP, MP100d

Systeme in Vorbereitung:

- Kardex

- Megamat

Für die Parametrierung wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer (PJ10/0015097)

Hinweis: Für die eventuell erforderliche File-Kommunikation auf anderen Servern muss, wenn **Kieselstein ERP** auf Linux-Rechnern läuft das entsprechende Netzwerk-Verzeichnis gemountet werden.

Abfrageintervall / Aktualisierung

Die Abfrage des Paternosters erfolgt nach dem im Automatikjob eingestellten Intervall, üblicherweise 60Minuten.
Die Wartezeit von **Kieselstein ERP** ist so eingestellt, dass in der Regel 30Sekunden gewartet wird, bis der Paternoster antwortet.
Die Abfrage ist aber auch so aufgebaut, dass immer der komplette Lagerstand im Paternoster abgefragt wird.
Es ist nun so, dass die Antwort des Paternosters deutlich länger dauern kann als nur 30Sekunden.
Dies bedeutet für Sie, dass für die erste Abfrage die Antwort des Paternosters zu spät kommt. Mit der zweiten Abfrage (nach einer Stunde) werden die Lagerstandsdaten der ersten Abfrage importiert. D.h. die importierten Daten stellen von Seiten der Abfrage her die Lagersituation im Paternoster von vor einer Stunde dar.
Dieses Zeitverhalten kann durch Änderung des Abfrageintervalls im Automatikjob verkürzt werden, sollte aber nicht unter 15' gesetzt werden.

**Hinweis:**

Es darf pro Lagerplatz und Paternosterkennung nur eine Zuordnung geben, da ja der Paternoster einen (besonderen) Lagerplatz repräsentiert.

## Parametrierung
Die Liste der unterstützen Paternoster muss in der Tabelle ww_paternoster und der zum jeweiligen Paternoster gehörenden ww_paternostereigenschaft definiert werden.<br>
Diese Definitionen müssen direkt in der jeweiligen Tabelle vorgenommen werden.

### Paternoster definieren
Die Paternoster müssen in der Tabelle WW_PATERNOSTER definiert werden.

Zur Zeit unterstützte Typen: Leanlift

Analge Musterscript:
- INSERT INTO ww_paternoster(<br>
i_id, c_nr, c_bez, c_paternostertyp, lager_i_id, personal_i_id_anlegen, t_anlegen, personal_i_id_aendern, t_aendern)<br>
VALUES (1, 'Leanlift', 'Paternoster Leanlift', 'Leanlift', 12, 11, '2024-01-31 00:00:00', > 11, '2024-01-31 00:00:00');

Die Lager_ID bezieht sich auf das Lager das mit dem Paternoster verbunden werden sollte. Das ist in der Regel das Hauptlager.

### Paternostereigenschaften eintragen
Die Paternostereigenschaften, werden in der Tabelle ww_paternostereigenschaft definiert.

Für Leanlift:
| Kommando | Bedeutung |
| --- | --- |
|	DIR_CMD | Kommandoverzeichnis des FTP (wenn nicht am gleichen Rechner, dann UNC Pfad eintragen) |
| DIR_RESPONSE | Antwortverzeichnis |
| WAITRESPONSE | optional Wartezeit in Sekunden (default = 5) |

Musterscripte:
- INSERT INTO ww_paternostereigenschaft(i_id, paternoster_i_id, c_nr, c_wert)<br>
    VALUES (1, 1, 'DIR_CMD', '/mnt/paternoster/mp100d/in/');

- INSERT INTO ww_paternostereigenschaft(i_id, paternoster_i_id, c_nr, c_wert)<br>
    VALUES (2, 1, 'DIR_RESPONSE', '/mnt/paternoster/mp100d/out/');

- INSERT INTO ww_paternostereigenschaft(i_id, paternoster_i_id, c_nr, c_wert)<br>
    VALUES (3, 1, 'WAITRESPONSE', '120');
