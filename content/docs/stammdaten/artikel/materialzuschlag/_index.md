---
title: "Materialzuschlag"
linkTitle: "Materialzuschlag"
categories: ["Materialzuschlag"]
tags: ["Kupferzuschlag", "Kupferbasiskurs", "LME"]
weight: 400
description: >
  Wie mit Materialzuschlägen arbeiten, z.B. Kupferzuschlag
---
Materialzuschlag
================

In vielen Branchen und es werden immer mehr, werden verschiedene Artikel gehandelt, die Materialien beinhalten, die zu Tages- / Wochen- / Monats-Kursen an der Börse gehandelt werden.

D.h. beim Artikel selber wird Material und Materialgewicht angegeben. 
Die verschiedenen Vertragsvereinbarungen und Gestaltungen der Ein- und Verkaufspreise bedingen, dass sowohl von der Verkaufs- als auch von der Einkaufsseite her auf den Materialzuschlag Rücksicht genommen werden muss.

Grundsätzlich ist der Materialzuschlag so definiert, dass er zum vereinbarten Einkaufspreis eines Artikels dazugerechnet wird. Als Besonderheit kommt hier noch dazu, dass bei verschiedenen Materialien und bei verschiedenen Lieferanten ein gewisser Aufschlag bereits im vereinbarten Preis enthalten ist und nur die Abweichungen zu diesem Preis als Zuschlag ausgewiesen werden.

Auch beim Verkauf wird der Materialzuschlag zum jeweiligen Verkaufspreis hinzugerechnet. Details der jeweiligen Berechnung siehe bei den Beschreibungen.

Um die Zuschlagsthematik detaillierter zu beschreiben, wird dies nun anhand des Kupferzuschlags genauer erklärt.

Material:

Die Definition der Materialien finden Sie in **Kieselstein ERP** im Modul Artikel, unterer Modulreiter Material.

Definieren Sie hier Ihre Zuschlagsbehafteten Materialien und deren Zuschlagssätze ab einem bestimmten Datum.

Bitte beachten Sie, dass entgegen der Börsenkurse hier die Sätze pro kg angegeben werden müssen (jeweils in Ihrer Mandantenwährung).

![](Materialzuschlag_Kurs.gif)    (Kupferkurs)

Definition des Materialgewichts:

Definieren Sie für jeden Artikel sein Material und sein Materialgewicht.

Wählen Sie dazu den Artikel aus und wechseln Sie in den Reiter Technik.

Geben Sie hier das Material an und das Materialgewicht in g/Mengeneinheit, bei Kupfer sind dies in aller Regel Leitungen, also in g/m. Üblicherweise wird das Kupfergewicht der Leitungen von den Lieferanten in kg/km angegeben, kann also direkt übernommen werden.

![](Materialzuschlag_Definition.gif)

Damit kann von **Kieselstein ERP** der aktuelle (Kupfer-) Zuschlag berechnet und ausgepreist werden.

Bitte beachten Sie, dass dies der Zuschlag ohne Berücksichtigung der Kupferzahl des Lieferanten ist.

Definition der Lieferanten Zuschlagszahl

Im Modul Lieferanten, wählen Sie den Lieferanten aus und wechseln in den oberen Modulreiter Konditionen.

Hier geben Sie die Kupferzahl zur Basis pro ein Kilogramm ein.

![](Materialzuschlag_Kupferzahl.gif)

**Hinweis:** In der Regel wird die Kupferzahl von den Lieferanten zur Basis 100kg angegeben.

Die Kupferzahl im Lieferanten wird benötigt, da die in den Einkaufspreise der Lieferanten eingerechenten Kupfer-Standardfaktoren je Lieferland unterschiedlich sind. 

So ist derzeit in A der Faktor 1,3 üblich und in D der Faktor 1,5

Mit diesen Definitionen kann nun je Artikel und Lieferant sein Kupferaufschlag unter Berücksichtigung der Lieferantenkupferzahl berechnet werden.

Im Artikel sehen Sie, wenn für einen Artikel Material mit Zuschlag definiert ist, folgende zusätzliche Felder:

![](Materialzuschlag_Artikellieferant2.gif)

Dies bedeutet: Aufgrund der Definition des Materialgewichts und des Zuschlagssatzes abzüglich der Lieferanten-Kupferzahl ergibt sich für diesen Artikel pro (Lager-)Mengeneinheit ein Zuschlag von 0,098722 € pro Meter.

Daraus wird der im Moment gültige tatsächliche Nettopreis von 0,2841 errechnet.

Wird hier das Wort Zuschlag in Rot angezeigt, ![](Materialzuschlag_Artikellieferant1.gif)
so bedeutet dies, dass zwar laut Artikel-Material Zuschlagssätze definiert sind, eine der weiterführenden Definitionen aber nicht vollständig sind. Z.B. ist beim  Lieferanten keine Kupferzahl hinterlegt, es fehlt das Materialgewicht oder Ähnliches.

Anzeige im Bestellwesen

![](Materialzuschlag_Bestellung.gif)

In der Bestellposition wird das Material und der Zuschlag mit angezeigt. Der Name des Materials wird nur bei der Auswahl angezeigt. Der Zuschlagsbetrag wird basierend auf dem Bestelldatum errechnet und kann vom Benutzer nicht verändert werden.

Zu Ihrer Information wird der Zuschlagsbetrag auch in den Wareneingangspositionen mit angezeigt. Für die Bestellung und auch die Warenzugangsbuchung wird immer der Nettopreis / Gelieferter Preis, welcher immer inkl. dem Zuschlag ist, verwendet.

#### Welche Definitionen müssen vorgenommen werden, um den Kupferzuschlag verwenden zu können?
Es sind folgende Definitionen erforderlich:

-   Im Artikel muss das Materialgewicht eingetragen sein (Reiter Technik)

-   Im Artikel muss das Material definiert sein (Reiter Technik)

-   Im Artikel, unterer Modulreiter Material muss der Preis für den Zuschlag zur Basis 1kg definiert sein

-   Im Lieferanten muss im Reiter Konditionen unter Kupferzahl die Kupferbasis des Lieferanten eingetragen sein. Geben Sie hier den beim Lieferanten bereits berücksichtigten Preis auf Basis 1kg des Materials ein.
      - **Hinweis:** Üblicherweise wird diese Zahl in € pro 100kg angegeben. Also ist die Kupferzahl durch 100 zu dividieren und dann einzutragen.
      - **Hinweis2:** Ist keine Kupferzahl gegeben, so muss hier zur Berechnung des Materialzuschlages 0 eingegeben werden.

Materialzuschläge variabel im Verkauf

Die Materialzuschlagsrechnung wurde für den Verkauf um die Möglichkeit von Aufschlägen ergänzt. Die Berechnung erfolgt nach folgender Formel:

Materialzuschlag laut Materialzuschlagstabelle
plus Aufschlag aus Artikel in €
Daraus Summe:
plus Aufschlag aus Artikel in %
ergibt Materialzuschlagsfaktor -> dieser ist mit dem Materialgewicht zu multiplizieren und ergibt den Materialzuschlag für den Verkauf.

Zum Beispiel:
Kurs MK 6,23 € /kg
+
Aufschlag 0,77 € /kg Variabel
+
Aufschlag 5% Variabel
=Summe 7,35 € /kg
Dies ist nun die tatsächliche Kupferzahl für die Berechnung des Zuschlags. Für die Errechnung wird nun noch die Kupferzahl (aus dem Parameter oder aus dem Kunden) abgezogen.

D.h. (7,35 - 1,5) x Gewicht ergibt Materialzuschlag für die Verkaufsposition

![](Materialzuschlag_Artikellieferant3.gif)

Für ein Beispiel aus der Praxis sei hier auf https://www.kabeltronik.de/de/kabeltronik/info-download/copper-surcharge
![](kupferzuschlag.png)  

verwiesen.

Um die Informationen in Ihren Verkaufsbelegen anzudrucken, wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.
In den Report Feldern F_ARTIKEL_KURS_MATERIALZUSCHLAG und F_ARTIKEL_DATUM_MATERIALZUSCHLAG wird der Kurs und das Datum zum Anlagezeitpunkt (und Belegdatum) der Position übernommen. 
Wenn ein Auftrag aus dem Angebot bzw. Auftrag/Lieferschein Auftrag/Rechnung erstellt wird, dann wird der Kurs/Datum des Materialzuschlages jeweils mitkopiert.
Wenn einem Lieferschein/Rechnung eine neue Position hinzugefügt wird (ohne Auftragsbezug) dann wird der aktuelle Kurs/Datum zum Belegdatum hinterlegt.

### Verrechnung nach Vorgänger Beleg oder zum Rechnungsdatum?
<a name="Verrechnungsstichtag"></a>
Mit dem Parameter MATERIALKURS_AUF_BASIS_RECHNUNGSDATUM kann gesteuert werden, ob der Materialzuschlag jeweils vom Vorgänger kommt, d.h. es wird, wie oben beschrieben, bei der Erstellung des Angebotes, genauer des ersten Dokuments der Position, der Materialzuschlag fixiert und dann in den weiteren Belegen durch kopieren übernommen.
**Hinweis:** Dies gilt auch bei der Übernahme von Abrufaufträgen aus Rahmenaufträgen. [Bitte beachten Sie auch]( {{<relref "/verkauf/gemeinsamkeiten/#wie-wird-in-den-verkaufsbelegen-mit-dem-materialzuschlag-umgegangen" >}} ).

Steht der Parameter MATERIALKURS_AUF_BASIS_RECHNUNGSDATUM auf 1, so wird erst durch das Drucken (Aktivieren) der Rechnung der Materialzuschlagskurs auf Basis des Rechnungsdatums ermittelt und zur Berechnung des Rechnungswertes herangezogen. Bitte beachten Sie, dass sich dadurch ein eventuelle Verkaufswert eines Lieferscheines, aber auch der offenen Auftragspositionen verändern wird. Es bedeutet dies auch, dass Angebote und Aufträge keinen Materialzuschlag ausweisen. Dies geschieht üblicherweise dann, wenn es mit Ihren Kunden entsprechend umfangreiche Vereinbarungen gibt und die Materialzuschläge global vertraglich definiert sind.

### Steuerung der Kupferzahl bei Kunden
Üblicherweise wird der Kupferzuschlag für Kunden von Ihrem Unternehmen ausgehen fest eingerichtet. Sie können dies im Parameter KUPFERZAHL eintragen. Bitte beachten Sie auch hier, dass die Basis €/kg ist. So ist also für Kupferzahl 150 1,50 € (genauer Ihre Mandantenwährung) einzugeben.
Da doch einige Ihrer großen Kunden definieren, zu welchen Kupferbasen Sie beliefert werden wollen, kann nun zusätzlich pro Kunde, im Reiter Konditionen, die abweichende Kupferzahl je Kunde definiert werden.
Bitte beachten Sie, dass im Gegensatz zum Zuschlag EK (Einkauf) im Zuschlag VK (Verkauf) der Wert des Parameters KUPFERZAHL mit berücksichtigt wird.

#### Welchen Verkaufspreis muss ich nun im Artikel hinterlegen
Es muss in der VK-Preisbasis immer der Preis inkl. der Verkaufs-Kupferzahl (System, Parameter, Kupferzahl) hinterlegt werden. Bitte beachten Sie: Haben Sie bei einzelnen Kunden eine abweichende Kupferzahl hinterlegt, so gilt diese Kupferzahl als weitere Berechnungsbasis für die Materialzuschlagsberechnung. Gegebenenfalls muss die sich daraus ergebende Differenz durch andere Verkaufspreise, z.B. in den Kunden-Sonderkonditionen, abgebildet werden.

#### Was bedeutet inkl. bei der Kupferzahl
Manche Kunden / Lieferanten vereinbaren mit Ihnen Fixpreise, in denen der Materialzuschlag für z.B. Kupfer bereits enthalten ist. Dies wird durch anhaken von inkl. (Kupfer) erreicht. D.h. bei diesem Einkaufs-/ Verkaufspreis kommen keine Preiskorrekturen aus der Materialkomponente des Artikels mehr dazu.

#### Materialnotierungsdefinition unterschiedlich je Kunde
In einigen Anwendungen besteht die Aufgabe, dass je Kunde unterschiedliche Materialnotierungen, vor allem bei Kupfer, gefordert sind.<br>
Dafür haben wir die zusätzliche Funktion der Materialnotierung geschaffen. Diese finden Sie im Modul Kunde, oberer Reiter Materialnotierung.<br>
Die Vorgehensweise für die erweiterte Definition ist wie folgt:<br>
- a.) Im Artikel wird das Material (Kupfer) im Reiter Technik hinterlegt. Dies ist die Definition für die "normalen / üblichen" Kunden. D.h. für die Kunden die sich nach Ihren Lieferbedingungen richten müssen.
- b.) Nun definieren Sie bitte im Kunden im Reiter Materialnotierung die Übersetzung dazu. Also das wenn z.B. im Artikel Kupfer eingetragen ist, dann ist für den Spezialkunden z.B. die Del Notierung zum Vortag usw. zu verwenden. Hier wird für dieses Material auch die Basis definiert und gegebenenfalls auch der Material inklusive Preis.

Wichtig: Damit die normalen Anzeigen und Berechnungen im Artikel stimmen und damit auch in der Rechnung der dann ermittelte Materialpreis / Kupferpreis angezeigt wird, muss bitte auch hinter dem Material des Artikels (dem normalen Kupfer) ein Materialzuschlag hinterlegt sein. Gegebenenfalls ist er 0,00, aber definiert.

#### Wo sehe ich wann welche Materialnotierungen vorgenommen wurden?
Von **Kieselstein ERP** werden alle Änderungen an der Materialnotierung automatisch mitprotokolliert.<br> Eine Liste der Änderungen finden Sie im Module Artikel, unterer Reiter Material, und dann Menüpunkt Info, Änderungen. Hier wird aufgelistet, wer wann welche Änderung am ausgewählten Material vorgenommen hat.

#### Kann das Material auch ohne Kurse verwendet werden?
Ja. Auch für **Kieselstein ERP** Anwender ohne der Zusatzfunktion Materialnotierung steht der Reiter Material und damit die Definition der Materialien zur Verfügung. Dies wird gerne für die grundsätzliche Definition des verwendeten Materials aus dem ein zu fertigendes Teil zu erstellen ist verwendet.
D.h. Sie könnten im Modul Artikel, unterer Reiter Material, mit Neu die erlaubten / verwendeten Materialien z.B. Stahl-Qualitäten definieren.<br>
Im jeweiligen Artikel kann dann im Reiter Technik das entsprechende Material ausgewählt werden. Womit für den Benutzer klar ist, aus welchem Material der Artikel ist.<br>
Zusätzlich kann durch Einschalten des Parameters MATERIAL_IN_AUSWAHLLISTE, das Material in der Auswahlliste mit angezeigt werden, womit der **Kieselstein ERP** Benutzer sofort die entsprechende Information hat.

## Wie vorgehen für die erste Definition
1. Mandantenparameter einstellen.<br>Kupferzahl in AT 1.3, in DE 1.5
2. Lieferant, Konditionen Kupferzahl angeben. Ist in EUR pro kg, z.B. ist in DE die Kupferbasis 150 pro 100kg
3. Hinweis: Der Wert des Zuschlag EK wird anhand des bevorzugten Lieferanten errechnet. Voraussetzung dafür ist, dass beim bevorzugten Lieferanten auch die Kupferzahl angegeben ist.