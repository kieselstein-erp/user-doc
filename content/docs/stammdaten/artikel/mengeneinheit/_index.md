---
title: "Mengeneinheiten"
linkTitle: "Mengeneinheiten"
categories: ["Mengeneinheiten"]
tags: ["Mengeneinheiten", "Einheiten umrechnen", "Dimensionen"]
weight: 200
description: >
  Definition der verwendeten Mengeneinheiten mit mathematischen Umrechnungsfaktoren.
---
Mengeneinheit
=============

Grundsätzlich wird in **Kieselstein ERP** die Mengeneinheit des Artikels als die lagergeführte Mengeneinheit verwendet. Alle Preise und Angaben beziehen sich auf diese Mengeneinheit. Bitte beachten Sie dass alle anderen Darstellungen nur umgerechnete Werte sind. Auch wenn z.B. in der Bestellung Einkaufspreise in kg eingegeben werden, so werden auch diese Zahlen in die lagergeführte Mengeneinheit umgerechnet und mit diesen Werten abgespeichert.

Die Mengeneinheiten können im Modul System ![](../../system/Modul_System.gif), unterer Modulreiter Sprache, oberer Modulreiter Einheit definiert werden

In **Kieselstein ERP** können Sie beliebige Mengeneinheiten definieren.

Folgende Mengeneinheiten müssen jedoch immer vorhanden sein und können deshalb nicht verändert werden.

| ME |  Bedeutung |
| --- |  --- |
| Stk | Stück |
| h | Stunden |
| min | Minuten |
| s | Sekunden |
| m | Meter |
| mm | Millimeter |

Basis ist immer die Mengeneinheit des Artikels. Also die Mengeneinheit in der der Artikel lagerbewirtschaftet wird.

Dimension

Zu jeder Mengeneinheit können Dimensionen angegeben werden.
Die Dimension definiert die Erfassung zusätzlicher Dimensionswerte für die Stücklistenposition.
Es stehen folgende Dimensionen zur Verfügung:

| Dimension | Bedeutung |
| --- |  --- |
| 0 | keine zusätzliche Erfassung |
| 1 | Erfassung der Länge einer Stücklistenposition |
| 2 | Erfassung der Breite und Tiefe einer Stücklistenposition |
| 3 | Erfassung von Breite, Tiefe und Höhe einer Stücklistenposition |

## Umrechnungsfaktoren

Zusätzlich können Umrechnungsfaktoren angegeben werden. Diese Faktoren können ausschließlich für die Umrechnung innerhalb gleicher Maßeinheiten verwendet werden. Siehe SI-Einheiten.

Beispiel: Die Mengeneinheit des Artikel ist m². Sie wollen die Stücklistenposition aber in mm² erfassen.

Sie definieren deshalb die Umrechnung zwischen den Mengeneinheiten wie folgt:

| Ausgangsmengeneinheit | Zielmengeneinheit | Umrechnungsfaktor |
| --- |  --- |  --- |
| mm² | m² | 1.000.000 |
| g | kg | 1.000 |
| kg | Tonnen | 1.000 |

Ist nun der Artikel in Tonnen definiert, so kann in der Stücklistenposition als Positionsmengenart zwischen g, kg oder Tonnen gewählt werden. Von **Kieselstein ERP** werden die entsprechenden Umrechnungsfaktoren entsprechend behandelt.

**Hinweis:**

Beachten Sie bitte, dass die Erfassung der Mehrdimensionalitäten in den Stücklistenpositionen immer in der Mengeneinheit der Stückliste erfolgt.
Ein Beispiel: Eine Blechtafel wird in m² definiert. In der Stücklistenposition wird als Mengeneinheit der Position mm² gewählt, so erfolgt die Erfassung von Breite und Tiefe in mm.
Voraussetzung: Der Artikel Blechtafel ist in m² definiert und die m² sind mit Dimension 2 angelegt.

Auch die Umrechnung von Zeiteingabeeinheiten in der Stückliste ist mit dieser Definition möglich.

| Ausgangsmengeneinheit | Zielmengeneinheit | Umrechnungsfaktor |
| --- |  --- |  --- |
| s | min | 60 |
| min | h | 60 |

D.h. Ein Arbeitszeitartikel ist im Artikelstamm in Stunden angelegt, aber die Erfassung der geplanten Zeiten erfolgt, in der Stückliste in der Erfassung der Positionen (welche eigentliche nur die Materialien sind) in Sekunden. So werden bei der Überleitung auf die Zieleinheit des Artikels die Umrechnungen mit voller Rechengenauigkeit durchgeführt.

## Bestellmengeneinheiten

In vielen Branchen ist es üblich, Artikel in anderen Mengeneinheiten zu bestellen, als der Artikel dann in der eigentlichen Lagerwirtschaft bewirtschaftet wird. Wir haben dafür die Umrechnung von Lagermengeneinheit auf Bestellmengeneinheit im Artikel geschaffen.

![](Mengeneinheiten_Umrechnungsfaktoren.png)

Damit kann die Umrechnung ausschließlich für das Bestellwesen eingetragen werden. Als Beispiel sei hier die Beschaffung von 3mm Stahlblech angeführt. Ein Quadratmeter Stahlblech hat ca. 24kg. D.h. der Artikel wird bei Ihnen im Unternehmen in m² bewirtschaftet. Da die Stahlhändler üblicherweise in kg rechnen, hinterlegen Sie in den Details der Blechtafel, dass ein m² 24kg hat. Also (ein) m² = 24 kg, so wie oben angegeben.
Ist bei einem Artikel die Bestellmengeneinheit angegeben, so kann in der Bestellung alternativ die Menge (das Gewicht) in Bestellmengeneinheit eingegeben werden. Ebenso kann der Preis des Artikels in der alternativen Bestellmengeneinheit eingegeben werden. **Wichtig:** Alle Buchungen und auch das Abspeichern der Daten erfolgt immer in der Lagermengeneinheit. Es können sich daraus eventuell Aufgrund der Umrechnung entsprechende unrunde Zahlen ergeben.
**Zusatzinformation:** Da die Artikel in der Lagermengeneinheit am Lager geführt werden, sind selbstverständlich auch die Inventurpreise in Bezug auf die Lagermengeneinheiten zu sehen.
D.h. für einen Meter Stahl(Profil/Flachstahl) ist der Meterpreis und nicht der kg Preis anzugeben.

#### Kann die Bestellmengeneinheit auch für Elektronik-Komponenten verwendet werden?
Diese Frage wird in der Praxis immer wieder gestellt. Oft ist der Hintergrund, dass eine Rolle SMD Widerstände mit 5.000Stk á 0,0214 € ja nur einen Wert von 107,- € darstellt und daher aus Sicht des Lagerwertes ausreichend genau ist.
Hierzu muss man wissen, dass grundsätzlich ein SMD Widerstand, so wie viele andere elektronische Komponenten auch, in der Stückliste immer in der Mengeneinheit Stück geführt werden muss, denn die Stücklistenposition, das Bauteil, an exakt der richtigen Stelle der Schaltung gibt es nur in ein Stück. Das bedeutet, dass ein SMD Widerstand in Stück zu bewirtschaften ist.
Hier kommt dazu, dass die Anzahl der Widerstände auf den Rollen je nach Hersteller sehr unterschiedlich ist, also alleine schon aus diesem Grund die Angabe von 1/1000 Rolle fachlich nicht geeignet ist. Zusätzlich hat ein Elektroniker, ein Lagermitarbeiter eine Vorstellung wieviel 2.400 Widerstände sind. Wieviel aber 0,48 Rollen sind, ist ihm nicht ersichtlich.
Ein weitere Punkt ist, dass, z.B. für Prototypen, auch immer wieder geringere Mengen als eine Rolle eingekauft werden. Damit würde die Darstellung völlig unklar.
Selbstverständlich können im Artikellieferanten die mit dem jeweiligen Lieferanten vereinbarten Verpackungseinheiten und Mindestbestellmengen hinterlegt werden. Dies sind jedoch nur Überwachungswerte. D.h. es erscheint in der Bestellung eine Warnung, wenn Sie z.B. 4.900 Stk bestellen möchten, die Mindestbestellmenge aber 5.000 ist. Sie haben natürlich die Möglichkeit auch nur 4.900 oder eben nur 50Stk, für einen Prototypen, zu bestellen.
Beachten Sie dazu bitte auch die Besonderheit der Verpackungsarten in Verbindung mit den Einkaufsmengenstaffeln

Verkaufseinheiten

Neben dem oben angeführten Umrechnungsfaktor für die Bestellmengeneinheit steht zusätzlich die (Verkaufs-) Verpackungsmenge im Artikelreiter sonstiges zur Verfügung. Im Gegensatz zur Bestellmengeneinheit ist  die Verpackungsmenge nur ein Hilfsmittel für die Erfassung der Vielfachen der verkauften Mengen. Sie kann, gemeinsam mit der Verpackungs-EAN-Nr. für zusätzliche Informationen auf den Lieferpapieren verwendet werden.

### Dimensionen im Verkauf

[Siehe dazu bitte]( {{<relref "/verkauf/gemeinsamkeiten/#k%c3%b6nnen-auch-abmessungen-dimensionen-eingegeben-werden" >}} )#

### Breite, Tiefe, Höhe, Länge; Was ist was?

Da es auch hier immer wieder zu Unklarheiten kommt, die Definition dazu.
- a.) Länge wird nur bei eindimensionalen Artikeln verwendet. Z.B. eine Stange Rundmaterial
- b.) Ein zweidimensionaler Artikel (ein Blatt Papier oder eine Blechtafel) hat eine Breite und eine Tiefe. Die Breite ist das Maß von links nach rechts, wenn der Artikel in Gebrauchslage vor Ihnen liegt. Also bei einem Blatt A4 210mm. Die Tiefe ist das Maß von vorne nach hinten des Artikels in der Gebrauchslage. Also bei einem Blatt A4 297mm.
Wichtig: Liegt das A4 Blatt quer vor Ihnen und ist das die gewünschte Gebrauchslage, so ist die Breite 297mm, die Tiefe 210mm. Siehe dazu auch Verpackungsbranche.
- c.) Ein dreidimensionaler Artikel (eine Schachtel Karton) hat die Breite und Tiefe wie oben beschrieben und die Höhe in vertikaler Richtung. Das wäre bei einem Blatt Papier ca. 0,5mm.

**Hinweis:** In der Verpackungsbranche hat sich entgegen obiger technischer Definition trotzdem die Länge Breite Höhe durchgesetzt. Es wird hier allerdings davon ausgegangen, dass wenn das Produkt in Gebrauchslage ist, die längere Seite die Länge ist. So würde, ergänzend zur obigen Beschreibung ein Blatt A4 Papier IMMER eine Länge von 297mm haben, egal wie Sie es beschreiben (Hauptsache es liegt). Für die Umbenennung der Feldnamen im Artikel, Reiter Sonstiges bitten Sie Ihren **Kieselstein ERP** Betreuer dies für Sie einzutragen.

#### Fremdsprachige Mengenarten / Texte
Die Mengenarten können in jeder konfigurierten Sprache gepflegt werden. Bitte beachten Sie, dass ein Großteil dieser Übersetzungen nur bei einem Neustart des Moduls geladen werden, um die Netzwerkbelastung entsprechend gering zu halten.

Beispiel hierfür ist die Mengenart.

Um diese zu definieren / zu übersetzen melden Sie sich bitte am **Kieselstein ERP** in der gewünschten Sprache an.

Wählen Sie nun System, Sprache, Einheit. Definieren Sie nun die gewünschten Texte für die Mengeneinheiten unter UI-Bezeichnung. Bitte bedenken Sie, dass auf den Formularen üblicherweise nur für vier Zeichen Platz vorgesehen ist.