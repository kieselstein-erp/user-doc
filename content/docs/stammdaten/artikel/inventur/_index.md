---
title: "Inventur"
linkTitle: "Inventur"
categories: ["Inventur"]
tags: ["Zählliste"]
weight: 300
description: >
  Erstellen, Zählen, Durchführen der Inventur
---
Inventur
========

Vorneweg eine Begriffsdefinition.
Im Gedanken von **Kieselstein ERP** wird eine Inventur durchgeführt und ergibt eine Liste von inventierten Materialien, Baugruppen, also zum Inventurzeitpunkt lagernden Artikeln.
Unter Inventar verstehen wir, zumindest im österreichischen Sprachgebrauch, Büroeinrichtung, Lagerschränke und ähnliches. Also Einrichtungs-Gegenstände aber auch Maschinen.

Bei unseren Schweizer und Liechtensteiner Anwendern ist die Definition, dass die Inventur die faktisches Arbeit des Zählens ist, mit der dann Wert und Menge des Inventars, also der zum Inventurzeitpunkt am Lager befindlichen Artikel, bestimmt wird.
In weiterer Folge wird in dieser Beschreibung immer vom österreichischen Sprachgebrauch ausgegangen.

Ab wann mach die Durchführung einer Inventur wirklich Sinn?
Gerade Unternehmen die neu starten, sind der Meinung, dass man durch das Zählen des Lagerstandes sofort alles hat, was für eine gute Lagerwirtschaft notwendig ist.
Wenn das nun ein reines Handelsunternehmen ist, mag das stimmen. In dem Moment, in dem Stücklisten verarbeitet werden, ist es aber so, dass für die komfortable Lagerbewirtschaftung auch die Materialbuchungen in die Lose stimmen müssen. Hier ist der komfortabelste Weg natürlich, dass die Stücklisten so weit wie möglich richtig sind.

#### Durchführung einer Stichtagsinventur
Die Stichtagsinventur dient der Erfassung des aktuellen Lagerstandes zu einem bestimmten Stichtag.
Mit **Kieselstein ERP** ist es uns gelungen die Funktionalität der sogenannten fliegenden Zwischeninventur und der Stichtagsinventur zu vereinen. Zum Thema [permanente Inventur siehe dort](#kann-man-mit-kieselstein-erp-auch-eine-permanente-inventur-machen).

Grundsätzlich gliedert sich die Inventur in folgende Schritte:
- Zählen des Lagerstandes
- Eingeben / erfassen des Lagerstandes in **Kieselstein ERP**
- Errechnen des Lagerstandes zum Inventurzeitpunkt

**<u>Wichtig:</u>** Beim Anlegen einer neuen Inventurliste müssen Sie auch das Stichtagsdatum für die Inventur angeben. Auf dieses Datum werden die Inventurmengen anhand der Artikelbewegungen rück- / vorgerechnet.<br>
Beachten Sie bitte auch, dass für die Berechnung der Lagerbuchungen eines Zeitraumes **immer der tatsächliche Buchungszeitpunkt** und nicht das Belegdatum ausschlaggebend sind.

1.  Im Modul Artikel finden Sie einen unteren Reiter Inventur.
    Unter dem Menüpunkt Info finden Sie die Zählliste. Drucken Sie sich die Zähllisten für Ihr Lager / Ihre Läger aus. Es ist eine Blindzählliste inkl. Code128 für die Artikelnummer.
    Mit dieser Liste werden die tatsächlichen Lagerstände erfasst.

2.  In der Auswahl wird eine neue Inventur angelegt und gespeichert.
    Unter Inventurliste werden die aktuellen Lagerstände (WICHTIG: hier ist der Lagerstand jetzt zum Zeitpunkt der Erfassung gemeint) nach Lagern getrennt eingegeben. Beim Speichern des (korrigierten) Lagerstandes werden die entsprechenden Lagerkorrekturbuchungen sofort durchgeführt.<br>
    Die Korrekturen werden in das Inventurprotokoll eingetragen.

3.  Nach der Erfassung aller Artikel (siehe dazu auch Info, nicht erfasste Artikel)  gehen sie auf Abschließen und klicken Sie auf Inventur durchführen ![](Inventur_Durchfuehren.gif). Jetzt werden die Lagerstände für den gewünschten Inventurstichtag errechnet und in den Inventurstand eingetragen. Bitte beachten Sie dass die Durchführung der Inventur VOR dem Inventurstichtag nur in Ausnahmefällen sinnvoll ist.

Inventurprotokoll und Inventurstand können ausdruckt werden. Hier stehen Ihnen vielfältige Sortiermöglichkeiten, wie zum Beispiel nach Lagerplatz zur Verfügung.
Der Inventurstand kann auch manuell verändert werden. Beachten Sie dazu bitte, dass dies KEINE Rückwirkungen auf den tatsächlichen Lagerstand hat. **Wichtig:** Die oben beschriebene Buchung wird auch zeitrichtige Buchung der Inventur genannt.
Das bedeutet, nachdem ein Artikel vom Lager, z.B. mittels Lieferschein entnommen wird, so wird anschließend der Artikel gezählt. Dieser Inventurstand wird in der Inventur erfasst. Danach wird ganz normal die Lagerbewirtschaftung fortgesetzt. Am Ende der Inventur wird trotzdem, gerade wegen der zeitrichtigen Buchung, der richtige Inventurstand zum 24:00 errechnet.
Dies hat für den Anwender den Vorteil, dass für die Inventur nicht Lager bzw. Lagerbereiche gesperrt werden müssen. Es muss lediglich darauf geachtet, dass alle ein- bzw. ausgelagerte Ware auch rechtzeitig verbucht wird. Somit kann die Inventur während des laufenden Geschäftes durchgeführt werden, z.B. wenn einzelne Mitarbeiter gerade Leerlauf hätten.

![](Inventurstand_ablauf.JPG)

Anders erklärt, zwischen dem Zählen und dem Inventurstand (egal ob vor oder nach dem Stichtag) werden die Lagerbewegungen berücksichtigt. Als Mitarbeiter zählt man den Artikel, so wie er derzeit im Lager ist, alle Buchungen in **Kieselstein ERP** (wie zum Beispiel Wareneingänge, Los-Material-Verbrauch) werden dementsprechend gebucht. Wichtig ist in diesem Zusammenhang, dass in der Zeitspanne während der Mitarbeiter den Artikel zählt und der Erfassung im **Kieselstein ERP**, keine Buchungen des Artikels gemacht werden, die sich auf den Lagerstand auswirken.

#### Wie stellt man sicher, dass die Lagerstandsliste mit dem Inventurstand übereinstimmt?
Manche Unternehmen wollen / müssen jedes Monat eine aktuelle Monats / Quartalsbilanz legen. Um diese gültig starten zu können, wird oft eine Inventur herangezogen, die dann mit dem Lagerstand zum Inventurstichtag übereinstimmen sollte / muss.
Dabei ist folgendes zu beachten:
Es ist in der Buchungslogik von **Kieselstein ERP** Systemimmanent enthalten, dass Lagerbuchungen IMMER zum aktuellen Zeitpunkt, also dem Zeitpunkt wann die Buchung durchgeführt wird, gebucht werden. Damit ergibt sich in Verbindung mit obiger Forderung, dass:
Wenn Inventurstand und Lagerstand zum Stichtag übereinstimmen sollten, so müssen alle Lagerbuchungen die mit der Inventur verbunden sind, vor dem Inventurstichtag abgeschlossen sein.
Beispiel: Eine Inventur zum 31.7.. Es müssen alle Inventurbuchungen zum 31.7\. durchgeführt sein. Das sind sowohl die klassischen Inventurbuchungen als auch z.B. die Buchungen setze Lagerstand auf 0,00 bei der Durchführung der Inventur.
D.h. unter der Voraussetzung dass Inventur und Lagerstand auch zum Stichtag übereinstimmen sollten, muss die **Inventur spätestens am Inventurstichtag komplett abgeschlossen werden**!

#### Kann man nachträglich Differenzen vor allem in deren Wert begründen ?
Wurde eine Inventur ev. mit Ausbuchen der nicht gezählten Artikel zu einem späteren Zeitpunkt als dem Inventurstichtag durchgeführt, so kann, siehe oben, der Lagerstand zum Stichtag nicht mit dem Inventurstand, der ja ein errechneter Wert ist, übereinstimmen. Um ein Gefühl für die Differenz zu bekommen nutzen Sie bitte die Inventurliste (Menüpunkt Info) und geben als Zähldatum einen Tag nach dem Inventurstichtag ein. Das Bis-Datum leeren Sie bitte. So bekommen Sie alle die Inventurbewegungen, die nach Ihrem Inventurstichtag noch gemacht wurden und die deshalb nicht in der Lagerstandsliste zum Inventur-Stichtag enthalten sein können.

![](Inventurliste_Aenderungen_nach_Stichtag.gif)

Bitte beachten Sie dazu unbedingt auch , dass VOR dem Ausbuchen der nicht gezählten Artikel diese, wie bei der Sicherheitsabfrage beschrieben, für alle jetzt heute lagernden Artikel gilt und nicht, wie oft angenommen, für die damals lagernden Artikel.

#### Wie macht man eine Stichtagsinventur?
Grundsätzlich besteht eine Stichtagsinventur aus dem Zählen der vorhandenen Artikel und der Eingabe dieser Mengen in das System. In vielen Unternehmen wird dies in Form einer Stichtagsinventur mit hohem Aufwand durchgeführt. Bitte prüfen Sie immer die Möglichkeit einer sogenannten Permanenten-Inventur, welche den Personaleinsatz dramatisch reduziert, aber auch gewisse organisatorische Voraussetzungen erfordert. Als weitere Möglichkeit sei auf die fliegende Zwischeninventur verwiesen. **<u>Stichtagsinventur:</u>** a.) **Vorbereitende Arbeiten** Es empfiehlt sich etliche Tage/Wochen vor dem eigentlichen Inventurstichtag bereits mit dem Zählen zu beginnen. D.h. von freien Kräften, oder auch zusätzlich angestellten Mitarbeitern usw. werden die in den Lagern befindlichen Artikel gezählt. Das Ergebnis der Zählung wird auf den einzelnen Artikelbehältern notiert. Wichtig: Dies setzt voraus, dass auch die Entnahmen ab diesem Zeitpunkt akribisch auf den Behältern notiert werden. Damit reduzieren Sie den Zeitdruck zum Inventurzeitpunkt.
b.) **die Durchführung** Wichtig: Es dürfen **KEINE** Buchungen sowie Entnahmen während der Stichtagsinventur durchgeführt werden. Siehe dazu auch: Worauf ist noch zu achten.
Nun müssen alle Artikel, die noch nicht gezählt wurden in sogenannten blinden Zähllisten erfasst werden. Diese Listen enthalten die Artikel und deren Bezeichnung sowie die Lagerplätze aber keine Mengenangaben und sind üblicherweise nach Lagerplätzen sortiert. In diese Listen sind handschriftlich die gezählten Werte einzutragen. Für eine eventuelle Steuerprüfung kann es sein, dass im schlimmsten Fall diese Listen herangezogen werden. Diese sind also entsprechend aufzubewahren (BAO132).
c.) **die Erfassung** Die von den Mitarbeitern gelieferten Zähllisten sind nun im Inventurmodul entsprechend zu erfassen. Am Einfachsten ist hier die Eingabe mittels Barcodescanner. D.h. es wird einfach der Barcode des Artikels der Zählliste gescannt, das Programm steht damit auf dem richtigen Artikel und sie brauchen nur mehr den aktuellen Lagerstand einzugeben. Für den Fall, dass ein Artikel auf mehreren Listen erfasst wurde oder nacherfasst wurde, ist zu beachten, ob dies eine Korrekturbuchung oder eine Ergänzungsbuchung ist. Dies wird vom Programm entsprechend unterstützt.
Wenn Sie mit der Erfassung fertig sind, so muss unbedingt geprüft werden, ob auch tatsächlich alle lagernden Artikel erfasst wurden. Nicht erfasst Artikel sollten noch nacherfasst werden und gegebenenfalls der Inventurlagerstand auf Null gebucht werden.
d.) **die Nachbearbeitung** Nach der Erfassung muss die Inventur in den aktuellen Lagerstand übernommen werden. Damit ist die eigentliche Inventur abgeschlossen und die üblichen Lagerbuchungen, also das ausliefern und einbuchen der Waren kann wieder durchgeführt werden. Nun ist die Inventur zu bewerten / abzuwerten und eventuelle Korrekturen usw. anzubringen. Hierbei ist zu beachten, dass Korrekturen in Inventurpreis und Menge **NICHT** auf den aktuellen Lagerstand wirken.

Möglichkeit den Zählaufwand zu reduzieren:
Es werden nur 80% des Warenwertes, was üblicherweise 20% der Lagerpositionen entspricht gezählt. Der Rest wird laut EDV in die Inventur übernommen.
Nutzen Sie für eine permanente Inventur auch den Andruck des Lagerstandes aus der Los-Ausgabeliste. Hier kann der Materialverantwortliche mit einem Blick feststellen ob der gebuchte Lagerstand mit dem physikalischen Lagerstand übereinstimmt.

#### welche vorbereitenden Arbeiten sollten noch vor der eigentlichen Inventur durchgeführt werden?
Gerade in Produktionsbetrieben ist eine gute Inventurvorbereitung schon die halbe Inventur. D.h. es muss unbedingt vor dem Beginn des Zählens klar sein, welche Teile überhaupt gezählt werden sollten. D.h. wo befinden sich buchungstechnisch die Artikel? Sind sie im Los oder noch im Lager und ist das den Mitarbeitern bekannt, also welche Teile nicht in der Inventur mitgezählt werden dürfen, da sie in der Halbfertigfabrikatsinventur erfasst werden.
Wie kann diese Situation entschärft werden?
a.) Überprüfen Sie ob alle Lose die erledigbar sind wirklich erledigt sind. Drucken Sie dazu die Halbfertigfabrikatsinventur sortiert nach Auftragsnummer aus. Hier sehen Sie noch offene / nicht vollständig erledigte Lose, deren auslösende Aufträge bereits erledigt sind. Bereinigen Sie diese Liste bestmöglich.
b.) Drucken Sie nun die Liste der auflösbaren Fehlmengen aus und lösen Sie diese Fehlmengen bestmöglich auf.
c.) Stellen Sie sicher, dass alle Lose in ihrem Ablieferstatus, mit den abgelieferten Mengen der jeweiligen Tatsache entsprechen.
d.) Drucken Sie erneut die Halbfertigfabrikatsinventur aus und kennzeichnen Sie die in den jeweiligen Losen gebuchten Teile als auf Los gebucht. Diese dürfen in der Inventur nicht mitgezählt werden und sollten auch während der Inventurbuchung nicht verändert werden. Es empfiehlt sich auch, während des Zählens keine Losablieferungen zu machen, solange bis die Inventur abgeschlossen ist. Wir raten, die nächste Losablieferungsbuchung erst am Folgetag der Inventur durchzuführen, damit eine ausreichend genaue Stichtagsbetrachtung möglich ist.
f.) Hier hat sich auch bewährt, darauf zu achten, dass alle Lose erledigt oder angelegt sind. Von der Idee her sollte die Halbfertigfabrikatsinventur kein einziges Los zeigen.
Das erreichen Sie indem die teilerledigten Lose in der Losgröße auf die bereits abgelieferten Mengen reduziert werden und für die noch zu produzierenden Mengen neue Lose, welche im Status angelegt bleiben bis die Inventur abgeschlossen ist, angelegt werden. Damit können alle Artikel gezählt und erfasst und bei gleichen Artikeln auch zusammengezählt werden.

#### Worauf ist noch zu achten?
Es kommt immer wieder vor, dass vor der Inventur z.B. ein Lieferschein angelegt wird, dann die Zählung der Artikel gemacht wird und dann nach der Inventur erst die eigentliche Buchung durchgeführt wird. Da für die Inventur der Erfassungszeitpunkt herangezogen wird, für die Darstellung der Bewegungsdaten aber das Belegdatum (z.B. des Lieferscheins) ausschlaggebend ist, ergibt sich automatisch, dass die Mengen die normalerweise aus der Artikelstatistik (durch den Anwender) herausgelesen werden NICHT stimmen können. Wir zeigen daher den ev. fehlerhaften Buchungszeitpunkt in Rot an.
Es bedeutet dies auch, dass beim Zählen der Inventurmengen, also der tatsächlichen Inventur KEINE bereits angelegten Lieferscheine bzw. Rechnungen bzw. Wareneingänge mit einem Datum vor dem Zählzeitpunkt gegeben sein sollten. Gegebenenfalls ändern Sie das Belegdatum auf nach der Inventur ab.

<a name="Schritte der Inventur"></a>

## Die Schritte einer Inventur in **Kieselstein ERP**

Für die Inventur wählen Sie im Artikel den unteren Modulreiter Inventur.

1.  Legen Sie eine neue Inventur an und definieren Sie den Stichtag der Inventur.
    Der Inventurstichtag ist das Bezugsdatum auf den die Lagerbewegungen rück- bzw. vor-gerechnet werden.
    Eine Inventur kann nicht vor dem Inventurstichtag abgeschlossen werden.

2.  Drucken der Zählliste, Info, Zählliste.
    Geben Sie hier an, ab welcher Werte (Gestehungswert, Gestehungspreis) die Artikel auf der Zählliste erscheinen sollen.
    In der Zählliste werden die erfassten Mengen und gegebenenfalls Serien- bzw. Chargennummern eingetragen.
    Hinweis: Artikel die mit (C) gekennzeichnet sind sind Chargenbehaftete Artikel. Mit (S) gekennzeichnet Artikel sind Seriennummerntragend. Hier müssen jeweils die Serien- bzw. Chargennummern erfasst werden.

3.  Erfassen Sie nun alle inventierten Artikel
    Wählen Sie dazu den Reiter Inventurliste und geben Sie die erfassten Artikel ein.
    Sie können dafür entweder den Neu-Knopf ![](Inventur_Neue_Position.gif) oder wenn Sie über einen Barcodescanner verfügen auch mittels Barcodeerfassung ![](Inventur_Barcodeerfassung.gif) die Daten eingeben.
    Bitte beachten Sie, dass die Lagerstandsänderung sofort bei Eingabe der erfassten / gezählten Menge gebucht wird. Diese Änderungen werden im Reiter Inventurprotokoll angezeigt. Wird ein Artikel des gleichen Lagers mehrfach erfasst, so erscheint eine entsprechende Meldung. In der Regel gehen wir davon aus, dass eine erneute Erfassung die Korrektur der bisherigen Erfassung ist.

4.  Sind Ihrer Meinung nach alle Artikel erfasst, so prüfen Sie dies durch den Druck der Liste der nicht erfassten Artikel (Info, nicht erfasste Artikel)
    Hier sehen Sie alle Artikel die noch nicht in der Inventurliste erfasst wurden. Gegebenenfalls hacken Sie "nur Artikel mit Lagerstand > 0 (größer 0)" an um eine Liste der noch nicht gezählten Artikel, welche aber derzeit noch einen Lagerstand haben zu erhalten.
    Hier besteht die zusätzliche Möglichkeit, dass für die inventierten Artikel welche Seriennummern- bzw. Chargennummern geführten sind, die noch nicht gezählten Serien- bzw. Chargennummern herauszufiltern.

5.  Sind nun alle zu zählenden Artikel gezählt und eingegeben, so muss nun der Inventurlauf durchgeführt werden.
    Gehen Sie dazu bei der entsprechenden Inventur auf Abschließen und hier auf ändern.
    Bitte beachten Sie unbedingt die Behandlung der nicht inventierten / nicht erfassten / nicht gezählten Artikel.
    ![](Inventur_Lagerstand_Null_setzen.gif). Wird diese Option gesetzt, so werden mit den Inventurlauf **<u>ALLE</u>** nicht erfassten Artikel auf einen Lagerstand von 0 gesetzt.
    Beachten Sie, dass dieser Lauf NICHT zurückgenommen werden kann. Bitte beachten Sie auch, dass es um die jetzt nicht lagernden Artikel geht (und nicht um die damals nicht lagernden)
    Sind alle Einstellungen richtig, so wird durch Klick auf ![](Inventur_durchfuehren.gif) der Inventurlauf gestartet.

6.  Nach der Durchführung der Inventur finden Sie unter Inventurstand die für den Inventurstichtag errechneten Lagerstände und Inventurpreise.
    Diese Mengen und Preise können bearbeitet / abgewertet werden. Beachten Sie dazu, dass:
    a.) diese Änderungen keine Auswirkungen auf Ihr tatsächliches Lager haben
    b.) die gesetzlichen Vorschriften.
    c.) die veränderten Mengen auch in der Artikelstatistik angezeigt werden.
    Die abgewerteten Preise werden zum Stichtag=Inventurdatum berechnet. Hier liegt die gleiche Berechnung wie für die Stichtagsbetrachtung des Lagerstandes zugrunde.

## Abwertung der Inventur

Ein Inventurstand kann nur einmal abgewertet werden.<br>
Als Basis für die Inventurabwertung dient der unter Inventurstand eingegebene Preis. Dieser wird anhand der Abwertungsformel abgewertet.<br>
Abwertungsformel: GESTEHUNGSPREISABWERTEN_AB_MONATE<br>
GESTEHUNGSPREISABWERTEN_PROZENT_PRO_MONAT<br>
Das bedeutet: Ein Artikel der GESTEHUNGSPREISABWERTEN_AB_MONATE Monate keine Bewegung hatte, wird für jedes weitere Monat um GESTEHUNGSPREISABWERTEN_PROZENT_PRO_MONAT %te abgewertet, solange bis er einen Preis von 0 hat.<br>
Zur Durchführung der Abwertung klicken Sie im oberen Modulreiter Inventurstand auf ![](Inventur_Abwertung.gif), Inventur-Preise abwerten.<br>
Für die Betrachtung der Abwertungen werden alle Lagerbewegungen mit Ausnahme der Handlagerbewegung und der Inventurbuchungen berücksichtigt. Also nicht nur Abbuchungen sondern auch Zubuchungen.<br>

Eine zusätzliche Variante der Abwertung ist über die Abwertung einzelner Positionen umgesetzt.
Wechseln Sie in den Reiter Inventurstand, markieren die Zeile des Artikels, den Sie abwerten wollen.<br>
Im unteren Bildschirmbereich befindet sich die Eingabe des Prozentsatz für die Abwertung und klicken auf Speichern.<br>
![](abwertung_prozent.JPG)<br>
Zur Dokumentation erfassen Sie die Begründung für die Abwertung im Kommentarfeld.
Im Anschluss daran wird in der Inventurstand-Liste der Basispreis, die Abwertung und der daraus resultierende Inventurpreis angeführt.<br>
![](darstellung_abwertung.JPG)<br>
**Achtung:** Wenn die Inventur durchgeführt wird, dann wird die Abwertung der vorherigen Inventur (siehe obere Beschreibung, allgemein Abwertung aller Artikel) übernommen (außer sie ist 0 oder NULL) und der Inventurpreis sofort abgewertet. Wenn nach der Inventurdurchführung die Preise abgewertet werden, dann werden hierbei nur Einträge berücksichtigt, bei denen die Abwertung(%) geändert wurde, da der Rest bei 'Inventur durchführen' schon abgewertet wurde.<br>
Lagerbewirtschaftete Artikel werden generell nicht abgewertet. Hintergrund: Manchmal möchte man, obwohl man den Artikel nicht bewirtschaftet, diesen trotzdem in seiner Inventur und damit in der Bilanz wiederfinden. Daher werden auch die nicht lagerbewirtschafteten Artikel bei der Inventur gezählt und so mit erfasst.

### Sieht man die Abwertung im Lager?
Nachdem die Abwertung der Inventur nur für die Preise der Inventurliste gilt, sieht man in den üblichen Lagerlisten, den abgewerteten Preis nicht. Dies hat den Vorteil, dass du immer deinen Gestehungspreis zu den damaligen Einkaufsbedingungen zur Verfügung hast und somit erkennen kannst, was dein Artikel damals gekostet hat.

Wenn du die Lagerliste mit den abgewerteten Gestehungspreisen sehen möchtest, bitte beim Druck der Lagerstandsliste mit abgewertetem Gestehungspreis ![](mit_abgewertetem_Gestehungspreis.png)  anhaken.<br>
Wichtig: Es wird hier die Abwertung basierend auf der aktuellen Situation errechnet, womit sich ein Unterschied zur Inventur ergeben kann.

### Neuberechnung der Inventurpreise, Gemittelter Gest-Preis / EK-Preis
<a name="Inventurpreise"></a>
Bei der Durchführung der Inventur wird je nach Einstellung des Parameters INVENTUR_BASISPREIS (Gestehungspreis = 0, EK-Preis = 1) wird der aktuelle Preis als Inventurpreis in den Inventurstand übernommen. Liegt nun z.B. zwischen dem Stichtag der Inventur und der tatsächlichen Durchführung ein zu langer Zeitraum, oder wurde die Abwertung mit falschen Parametern durchgeführt, so kann es vorkommen, dass der Wunsch nach neuerlicher Ermittlung der Inventurpreise besteht.

Unter Inventurstand wird durch Klick auf ![](inventurpreis_neu_berechnen1.gif) Inventurpreise neu errechnen, die Neuberechnung der Inventurpreise nach Beantwortung von 

![](inventurpreise_neu_berechnen2.jpg)

gestartet.

Hier wird nach folgender Regel vorgegangen:

a.) es wird der im Moment der Berechnung aktuelle Gestehungspreis übernommen.

b.) ist dieser Preis kleiner als der Nettopreis des bevorzugten Lieferanten (und ist ein Lieferant definiert), so wird der Nettopreis des Lieferanten übernommen

c.) ist kein Lieferantenpreis definiert so wird der Einstandspreis der jüngsten Ablieferung vor dem Inventurstichtag gesucht und wenn dieser größer als der Gestehungspreis ist wird dieser Ablieferungs = Fertigungspreis als Inventurpreis übernommen.

Zum Schluss wird die Sperre der Inventurabwertung wieder entfernt, damit der Abwertungslauf erneut gestartet werden kann.
Bitte beachten Sie, dass mit dieser Funktion alle bisherigen Inventurpreise der gewählten Inventur überschrieben werden.
In kurzen Worte: Setzt den Inventurpreis auf den Lief1preis, bzw. wenn dieser nicht vorhanden ist auf den letzten Preis der Losablieferung vor dem Inventurdatum bzw. wenn dieser nicht vorhanden ist, auf den gemittelten Gestpreis des betreffenden Lagers.

Neuberechnung der Inventurpreise alternative Variante, Gestpreis zum Inventurdatum

Setzt den Inventurpreis auf den zum Inventurdatum gültigen Gestehungspreis.
Für die Unterscheidung beachten Sie bitte den Tooltiptext.

Neuberechnung des Inventurpreises auf Basis EK-Preis

Durch Klick auf den Button ![](inventurpreise_neu_berechnen3.jpg) EK-Preis zum Inventurdatum werden die Inventurpreise auf den Preis der letzten Losablieferung bzw. letzten Wareneingangspositionspreis vor dem Inventurdatum gesetzt. Ist keine Losablieferung / Wareneingangsposition vorhanden, so wird der Lief1Preis = EK-Preis verwendet. Wenn es auch diesen nicht gibt, dann wird der Inventurpreis des Artikels nicht verändert.

Welche Preise werden für die Inventurbewertung verwendet?

Grundsätzlich wird für die Bewertung der Inventur der Gestehungspreis als Preisbasis verwendet, da dies der lagertechnisch richtige Preis ist. In einigen Unternehmen ist es üblich, dass die Inventur zum letzten bekannten Einkaufspreis bewertet wird. Dies wird mit dem Parameter INVENTUR_BASISPREIS definiert. Wird dieser auf 1 gestellt, so wird für die Errechnung des Inventurpreises der aktuell gültige Lief1Preis (= aktuell hinterlegter Einkaufspreis des bevorzugten Lieferanten) verwendet. Wenn kein Einkaufspreis hinterlegt ist, wird der Gestehungspreis verwendet. Dies wird NUR für Einkaufsartikel und nicht selbst gefertigte Stücklisten angewandt. Für eigengefertigte Stücklisten wird trotzdem der Gestehungspreis herangezogen.

FAQ zur Inventur:

Bei der Durchführung einer Inventur gilt es alle Waren exakt zu erfassen. Hier ist die Trennung zwischen Waren in der Fertigung (Halbfertigfabrikate) und lagernden Waren besonders wichtig. Wenn wir hier von Inventur sprechen, so sind damit nur Waren, welche sich im Lager befinden gemeint. Beachten Sie dazu bitte unbedingt die [Halbfertigfabrikatsinventur]( {{<relref "/fertigung/losverwaltung/#halbfertigfabrikatsinventur" >}} ) und die [Lagerabstimmung]( {{<relref "/fertigung/losverwaltung/lagerabstimmung" >}} ).

#### Bei einigen Artikeln gibt es Lagerstand und Fehlmengen, wie gehe ich damit um ?
Es sollten vor jeder Inventur unbedingt die Fehlmengen aufgelöst werden.
Um festzustellen welche Fehlmengen noch aufgelöst werden können / sollen, wählen Sie bitte aus dem Fertigungsauftrag (Los) Journal, Fehlmengen. Hacken Sie hier "Nur Artikel mit Lagerstand" an. Sie erhalten eine Liste von Artikeln mit Lagerständen, welche zugleich Fehlmengen sind. Lösen Sie diese Fehlmengen auf. Siehe dazu bitte [Nachträgliche Materialentnahme]( {{<relref "/fertigung/losverwaltung/#nachtr%c3%a4gliche-material-entnahme" >}} ) im Los.<br>
Sollte bei einzelnen Artikeln Fehlmengen aufgelöst werden, so benötigen Sie auch dazu die Fehlmengen verursachenden Lose. Gehen Sie dazu im Artikel auf Info, Fehlmengen. Gehen Sie auch hier in die jeweiligen Lose und dann auf Material. Suchen Sie hier den Artikel mit der Fehlmenge. Sie erkennen dieses Positionen am F am rechten Rand der Materialliste.

#### Was wird in der Artikelstatistik angezeigt?
In der Artikelstatistik wird der Inventurstand zum Stichtag der Inventur angezeigt. Das bedeutet auch, dass wenn von Ihrer Seite der Inventurstand verändert wird, wird dieser veränderte Wert angezeigt.

#### Ich habe mich bei der Erfassung verzählt/vertippt. Wie kann die gezählte Menge verändert werden?
Wechseln Sie in den Reiter Inventurliste, wählen Sie den Artikel aus und klicken Sie unten auf ändern. <br>
Geben Sie nun die richtige Menge ein.<br>
Ist eine Reduktion der Menge nicht mehr möglich, so wurde in der Zwischenzeit der Artikel bereits wieder verbraucht.

**Hinweis:**<br>
Beachten Sie, dass bei der Erfassung immer der aktuelle Lagerstand einzugeben ist.

Ein Beispiel:
- Inventurstichtag 31.12.2011
- Durchführung der Zählung 20.12.2011 ... Es werden fälschlich 22 Stk. eingegeben.
- Feststellung des Fehlers am 23.1.2012
- Inventur ist noch nicht durchgeführt. 

Es hätten anstatt der 22 Stk. nur 11 Stk. eingegeben werden sollen. Hier kommt dazu, dass vom Inventurstichtag bis zum 23.1\. bereits 5 Stk. verbraucht wurden.

D.h. es ist der heutige Lagerstand zu erfassen, also nicht die 11 Stk. zum 20.12. sondern die 6Stk. zum 23.1..

Beachten Sie dazu auch die Anzeige des voraussichtlichen Inventurstandes unter der Inventur Erfassungsmenge.

![](Voraussichtlicher_Inventurstand.gif)

**Hinweis:**

Wird der Fehler erst nach der Durchführung der Inventur festgestellt, so muss der Lagerstand per Hand (Handlagerbewegung) korrigiert werden und der Inventurstand gleichlautend angepasst werden.

#### Kann man mit **Kieselstein ERP** auch eine permanente Inventur machen?
<a name="Permanente Inventur" class="question"></a>
Der Sinn einer permanenten Inventur ist, dass einmal während eines Geschäftsjahres jeder Artikel einmal gezählt wird. Bei entsprechender Sorgfalt kann davon ausgegangen, dass vom Zählzeitpunkt bis zum Inventurstichtag nur unwesentliche Lagerabweichungen zum theoretischen Lagerstand auftreten.<br>
Durch die Anfangs beschriebene Eigenschaft der Zeitpunkt exakten Inventur und dadurch dass die Veränderungslagerbewegung sofort gebucht wird, kann mit **Kieselstein ERP** auch eine permanente Inventur durchgeführt werden.<br>
Hierbei ist lediglich zu beachten, dass ein bereits in der Inventurliste enthaltener Artikel in seinem letzten Inventurstand verändert werden muss und nicht ein zweites Mal in die Inventurliste eingetragen wird. Bei mehrmaligen Änderungen werden die Lagerveränderungen trotzdem in das Inventurprotokoll geschrieben. D.h. die Vorgehensweise ist wie folgt:
- a.) Erfassen der Artikel
  - a1.) kommt bei der Erfassung der Artikel in der Inventurliste keine Fehlermeldung so direkt den gezählten Lagerstand eingeben
  - a2.) kommt es beim Abspeichern des (erneut inventierten) Artikels zur Meldung, dass der Artikel bereits eingetragen ist.<br>
![](Inventur_Doppelter_Artikeleintrag.jpg)<br>
so wählen Sie, am Besten über den Direktfilter Artikelnummer<br> ![](Inventur_Artikelnummer_finden.gif) <br>
den bereits inventierten Artikel aus und ändern die erfasste Inventurmenge durch klick auf ändern ![](Inventureintrag_aendern.gif) auf die heutige Erfassungsmenge ab.<br>
Im Inventur-Protokoll werden die entsprechenden Änderungsbuchungen protokolliert.<br>
Verwenden Sie auch hier die Erfassung der laufenden Inventurmengen die Barcodeerfassung der Inventur ![](Inventur_Barcodeerfassung.gif), in der der Ergänzungsdialog deutlich einfacher abgehandelt ist. Gegebenenfalls verwenden Sie bitte ergänzend die Inventurerfassung per [mobilem Barcodescanner]( {{<relref "/extras/Mobile_App">}} ).

Um zum Ende des (Geschäfts-)Jahres sicher zu sein, dass alle Artikel inventiert wurden, drucken Sie unter Journal, nichterfasste Artikel eine Liste der noch nicht inventierten Artikel mit Lagerstand, oder auch Artikel die keinen Lagerstand haben, aus. Siehe dazu auch [Schritte der Inventur](#die-schritte-einer-inventur-in-kieselstein-erp) ab Punkt 4.

#### Alternative zur Permanenten Inventur
Um sich das laufende Zählen der Artikel zu ersparen und trotzdem einen qualitativ verlässlichen Lagerstand zu bekommen, kann, in Produzierenden Unternehmen, folgende Vorgehensweise verwendet werden:
- Auf den Losausgabelisten wird immer auch der resultierende Lagerstand nach der Entnahmebuchung angedruckt. Prüft nun der/die MitarbeiterIn diese Menge bei der Entnahme, ist dieser bewirtschaftete Artikel gezählt
- Gleiche Vorgehensweise bei der Lagerentnahme für Aufträge anhand der Auftragspacklisten
- Alle Artikel die sich nicht bewegt haben, findet man in der Ladenhüterliste, auf die man sowieso ein besonderes Auge haben sollte.

#### Wie werden Seriennummern tragende Artikel erfasst?
Bei der Inventur von Seriennummern tragenden Artikeln geht es darum, jede einzelne Seriennummer zu erfassen. D.h. es wird der Artikel mit Menge 1 und seiner Seriennummer erfasst, um so eine Gegenüberstellung zwischen den lagernden und den in **Kieselstein ERP** erfassten Artikeln zu haben.<br>
Um dies möglichst einfach zu machen haben wir dies in der Barcodeerfassung der Inventur hinterlegt. Die Barcodeerfassung für die Inventur steht unter ![](Inventur_Barcodeerfassung.gif) zur Verfügung.<br>
Im bestehenden Inventurliste-Schnelleingabe-Dialog können auch mehrere Seriennummern eingegeben werden: (z.B.: 10000-10010).<br>
Sollten schon Seriennummern dieser Liste inventiert worden sein, dann erscheint eine Abfrage, ob diese bereits inventierten Seriennummern ausgelassen werden sollen.

Sind alle Artikel erfasst, also auch jeder einzelne Seriennummern-tragende-Artikel, so verwenden Sie bitte das Journal nicht erfasste Artikel. Hier sind auch die Seriennummern aufgeführt, die bisher noch nicht erfasst wurden. Sind hier noch Seriennummern angeführt, so prüfen Sie bitte ob diese nicht versehentlich übersehen wurden. Sind diese tatsächlich nicht lagernd, so müssen diese mit der Inventurmenge von 0 erfasst und damit vom Lager ausgebucht werden. Die Funktion "Setze Lagerstand der nicht inventierten Artikel auf 0" wird für Serien- und Chargengeführte Artikel aus Sicherheitsgründen nicht angewandt.

#### Wie werden Chargen geführte Artikel erfasst?
Die Inventur von Chargen geführten Artikel geht im wesentlichen nach der gleichen Logik wie die Erfassung der Seriennummern tragenden Artikeln. Auch hier geht es darum jede einzelne Charge zu erfassen. daraus ergibt sich auch, dass Mengenkorrekturen bei vorhandenen Chargen sofort durchgebucht werden. Beachten Sie bitte auch hier, dass vor der Durchführung der Inventur die nicht erfassten Artikel und damit die nicht erfassten Chargen geprüft und gegebenenfalls ausgebucht werden müssen. Die Darstellung zum Beispiel bei der Artikelstatistik wird pro Charge angeführt, ist zum Beispiel die Menge der Charge A von 3 auf 2 geändert worden, so finden Sie den Eintrag -1 in der Artikelstatistik.

### Inventurliste CSV Import
In der Inventurliste gibt es den Button 'CSV-Import'![](csv_import.PNG). Hier können Sie Teile der Inventur per csv-Import übernehmen.
Die CSV-Datein muss 2 bzw. 3 Spalten haben: 
1.) Artikelnummer
2.) Menge (Dezimaltrenner muss ein Punkt sein)
3.) ev. Chargen bzw. Seriennummer

Der Spaltentrenner ist ein Semikolon (;)

Wenn die Artikelnummer nicht vorhanden ist oder die Menge nicht konvertiert werden kann oder es bereits einen Eintrag in der Inventurliste gibt, dann wird die Zeile ausgelassen und geloggt. Nicht lagerbewirtschaftete Artikel werden beim Import ignoriert und ebenfalls im Protokoll vermerkt.<br>
Der Import kann nur für eine Inventur eines einzelnen Lagers gemacht werden.

Hinweis: Theoretisch können sich durch den Import bei einzelnen Positionen negative Inventurmengen ergeben.

### Lagerauswahl bei der Inventur

Bei der Erfassung der Einträge, wird das Lager in folgender Form vorbesetzt: Bei der Schnellerfassung ist, wenn die Inventur nur für ein Lager definiert ist, nur dieses Lager auswählbar.<br>
In der Inventurliste wird immer das zuletzt verwendete Lager vorbesetzt. Wenn eine Inventur für ein Lager durchgeführt wird, so wird dieses Lager vorbelegt, der Parameter DEFAULT_LAGER wird nicht berücksichtigt.

#### Kann die Durchführung der Inventur zurückgenommen werden?
Wenn Sie versehentlich auf Inventur durchführen geklickt haben, finden Sie im Reiter Pflege die Möglichkeit diese zurückzunehmen.

![](inverntur_zurücknehmen.png)

Um anschließend auch die mit der Durchführung der Inventur entstandenen Protokoll-Einträge zurückzunehmen, klicken Sie auf diesen Menüpunkt.

Im Anschluss geben Sie das gewünschte Datum an, ab welchem die Einträge wieder zurückzunehmen sind.

![](protokolleintraege_zuruecknehmen.JPG)

ACHTUNG: diese Pflege ist nur innerhalb kurzer Zeit nach der Durchführung der Inventur möglich! Sobald Materialien verbraucht wurden, kann die Inventur nicht mehr zurückgenommen werden!

#### Im Inventurstand werden negative Mengen angezeigt
Trotz aller eingebaut Sicherheitsmaßnahmen ist es möglich im Endeffekt negative Inventurstände zu erreichen. Hier eine kurze Beschreibung wie es dazu kommt, damit Sie diese Situationen möglichst vermeiden und wie diese korrigiert werden können.
![](inventurstand_negativ.JPG)

Beispiel:
- Es wird ein Artikel z.B. NT001
- zum 27.11\. 9:25:30 mit einer Menge von 0 in die Inventur aufgenommen.
- am 28.11\. 7:15:29 werden 40 aus der Bestellung mit einem Wareneingangsdatum vom 27.11\. zugebucht
- am 08.12\. 11:25:11 werden 40 abgebucht.

Diese Buchung bedeutet, dass zwar am 27.11\. null Stück gezählt wurden, aber mit der später / zu spät durchgeführten Buchung für den 27.11\. wurde eine Menge vor der Inventur dem Lager zugebucht, die laut der danach durchgeführten Inventurbuchung gar nicht auf Lager war. Daher ergibt sich in dieser Situation der negative Inventurstand.<br>
Da wir diese Situation nicht vermeiden können, haben wir uns entschlossen, dies so zu belassen und die ehrlich sich daraus ergebende negative Menge zu buchen.

Für die Korrektur gehen Sie bitte wie folgt vor. Wir nutzen dafür obiges Beispiel:<br>
In der Artikelstatistik wird angezeigt:<br>
![](Statistik_fuer_Inventur.gif)<br>
Hieraus ist ersichtlich, dass zwischen vermeintlicher Buchung, welche für alle Betrachtungen herangezogen werden muss und tatsächlicher Buchung ein Tag Unterschied liegt. Wenn genau zwischen diesen Datum eine Inventurbuchung gemacht wurde kann es zu diesem Verhalten kommen.
In unserem Beispiel lautet die Korrektur, Änderung des Warenzugangsdatums auf den 28.11., da ja die 40Stk erst NACH der Inventur zugebucht wurden.<br>
Da die zugebucht Menge bereits wieder verbraucht wurde, müsste die Losausgabe zurückgenommen werden. Dies könnte mühsam werden.<br>
Die Alternative ist:
-   Rücknahme des Inventurlaufes
-   Wechsel in den Reiter Inventurliste
-   Positionierung auf den Artikel. Hier sehen Sie nun, dass unten als voraussichtlicher Inventurstand -40 angezeigt wird (diese Information stand zum Zeitpunkt der Erfassung noch nicht zur Verfügung).
-   Klicken Sie nun auf ändern und geben Sie den **JETZT** gültigen Inventurstand ein. Damit wird dieser mit dem heutigen Datum (genauer Zeitpunkt) gespeichert und so der Inventurstand korrigiert.

Um alle betroffenen Artikel zu finden, klicken Sie bitte im Reiter Inventurstand auf die Spalte Menge und gehen (bei aufsteigender Sortierung) ganz an den Anfang der Tabelle.<br>
Sind alle Artikel korrigiert, führen Sie die Inventur erneut durch.<br>
**Anmerkung:**<br>
Da sich die Belegbuchungen immer nur auf ein Datum beziehen und da dieses Datum auch gewählt bzw. verändert werden kann, denken Sie z.B. an eine Wareneingangsbuchung die für den vergangenen Freitag durchgeführt werden sollte, achten Sie idealerweise darauf, dass zum Tag des Zählens keine Warenzugänge gemacht werden.

#### Kann das Inventurdatum geändert werden?
So lange die Inventur noch nicht durchgeführt wurde, kann das Datum der Inventur angepasst werden. So können Sie den Stichtag der Inventur auch nach dem Anlegen der Inventur nochmals ändern.

#### Zu welchem Stichtag/Zeitpunkt wird die Inventur betrachtet?
Die Inventur wird zum angegebenen Datum um 23:59:59 betrachtet.<br>
Zusätzlich wurde obige Meldung in einen Hinweis geändert, was bedeutet, dass auch Inventurmengen erfasst werden können, die eigentlich eine negative Inventurmenge ergeben würden.

#### Wie werden nachträgliche Wareneingangsbuchungen behandelt, deren Wareneingangsdatum vor dem Inventurdatum liegen.
Unter der Voraussetzung, dass die Inventur noch nicht durchgeführt wurde, zählt das Wareneingangsdatum.<br>
Bitte beachten Sie beim Eintragen von Korrekturen, dass diese mit dem aktuellen Datum abgespeichert werden. D.h. diese müssen den Wareneingang mit beinhalten.<br>
Ein Beispiel aus der Praxis:
- a.) es wird im Zeitraum vom 27.12.-29.12\. Inventur gemacht. Zu diesem Zeitpunkt werden keine weiteren Warenentnahmen oder Warenzubuchungen (Wareneingänge) gebucht.
- b.) Vom 30.12\. bis 2.1\. des Folgejahres wird nicht gebucht
- c.) die in der Zwischenzeit eingetroffenen Wareneingänge werden, vor allem auch wegen Übereinstimmung mit der Buchhaltung (die Eingangsrechnungen sind ja auch noch im alten Jahr), mit dem Wareneingangsdatum vom 30.12\. eingebucht
- d.) Nun wird die Inventur durchgeführt / abgeschlossen. D.h. in der Inventur ist der Wareneingang zum 30.12\. noch mit enthalten.
Bitte beachten Sie bei der Durchführung der Inventur dass wenn "setze nicht gezählte Artikel auf 0,00" bzw. "setze nicht inventierte Serien/Chargennummern auf 0,00"  angehakt ist, bei allen nicht gezählten Artikel mit dem Durchführungsdatum eine Lagerentnahmebuchung (also mit Heute) gemacht wird. Daher ist es wichtig, dass die Liste der noch nicht inventierten Artikel immer unmittelbar vor der eigentlichen Durchführung der Inventur geprüft wird.

#### Können die Preise der Inventur nachträglich geändert werden?
Unterscheiden Sie hier bitte sehr sehr deutlich zwischen der Änderung der Preise für die Bewertung der Inventur im [Reiter Inventurstand](#Inventurpreise) und den Preisen mit denen ein sich aus der Inventur ergebende Zubuchung bewertet / korrigiert werden. Dies ist dann erforderlich, wenn Inventurzubuchungen erfolgt zu einem Zeitpunkt sind, zu dem noch keine Gestehungspreise der Artikel bekannt waren. Dies ist oft dann der Fall, wenn neue Artikel erfasst werden und dann die Lagerstände per Inventur eingepflegt werden. Da die neuen Artikel weder Lieferanten noch Gestehungspreise haben stehen zum Zeitpunkt der Inventur-Lagerstands-Buchung auch keine Einstandspreise zur Verfügung. Die schlussendliche Wirkung davon ist, dass man bei den verschiedenen Nachkalkulationen feststellt, dass Artikel keine Einstands und somit keine Gestehungspreise habe, weil "damals" keine Preise bekannt waren.
Dafür haben wir nun die Möglichkeit geschaffen, dass im Reiter Inventurprotokoll für Zubuchungen, also positive Mengen, der Einstandspreis dieser Buchung nachträglich verändert werden kann. Diese Änderungsbuchung wird der Aktualisierungslogik des Gestehungspreises unterworfen. Ob diese Änderung des Einstandspreise eine Wirkung auf den aktuellen Gestehungspreis hat, hängt von den nachfolgenden Entnahmebuchungen ab. Siehe dazu bitte auch Gestehungspreis im Artikel.

Wie wird nun diese Änderung durchgeführt?
Wählen Sie in einer noch nicht durchgeführt (= abgeschlossenen) Inventur den Reiter Inventurprotokoll.
Wählen Sie nun die zu ändernden Artikel(-preis). Klicken Sie auf ändern ![](Inventurpreis_aendern.gif). Wenn die Inventurbuchung auf diesen Artikel eine Zubuchung ist, so kann der Preis geändert werden. Ist es eine Abbuchung, so erscheint eine entsprechende Fehlermeldung (kann nur bei einer Zubuchung geändert werden).

#### Inventur in Zusammenhang mit importieren Losdaten / Kassendaten?
Da der Kassenimport die Kassenlagerbewegung mit aktuellem Datum (= heute) bucht, ist für die Inventur wichtig, dass alle Lagerbewegungen vor dem Inventurstichtag importiert sind. Würden die Daten später importiert, sind das natürlich Lagerbewegungen nach dem Inventurstichtag womit die Inventur unrichtig wäre.

#### Doppelte Lagerstände während der Inventur
Diese Situation kann sich bei falsch erfassten Serien- bzw. Chargennumern ergeben. D.h. während der Inventur wird die richtige (zum Inventurzeitpunkt gültige) Chargennummer erfasst. Eingebucht wurde aber eine andere Chargennummern.<br>
Man hat also während der Erfassung der Inventur beide Chargennummern auf Lager.
Erst durch das Ausbuchen der falschen Chargennummer entspricht der Lagerstand wieder den tatsächlich lagernden Mengen.

Beispiel:<br>
von der Charge ABCD sind 100 Stk(m, ..) auf Lager<br>
Man inventiert aber Charge ABCDE mit 100 Stk.<br>
Also hat man insgesamt 200 Stk auf Lager.

Man erkennt nur im Journal der nicht erfassten Artikel (Inventur, Info, Nicht erfasste, Artikel, nur in Inventurliste enthaltene (Snr/Chr-)Artikel welche Chargen / Seriennummer (noch) nicht gezählt sind. So lange gibt es "falsche" Lagerstände. Sollte sich die Inventur länger hinziehen, empfiehlt es sich diese Liste aktuell zu halten, also täglich die "doppelten" Chargennummern (die tatsächlich gezählten und die falsch damals eingebuchten) zu bereinigen.

Gleiches Thema ist natürlich bei den Seriennummern gegeben.

#### Zubuchung neuer Artikel nach dem Inventur Stichtag
Wenn neue Warenbewegungen nach dem Inventurstichtag erfolgt sind und diese Artikel nicht inventiert wurden, also nicht in der Inventurliste enthalten sind, und du bei der Durchführung der Inventur das alle nicht inventierten Artikel löschen angehakt hast, so werden diese Artikel wieder ausgebucht, da du diese nicht gezählt hast.

**Daher: Immer die nicht erfassten Artikel prüfen, bevor du den Lauf zur Ausbuchung startest.**

