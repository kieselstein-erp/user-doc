---
title: "Inventur mehrere Standorte"
linkTitle: "mehrere Standorte"
categories: ["Inventur"]
tags: ["mehrere Standorte"]
weight: 100
description: >
  Inventur über mehrere Standorte hinweg
---
Wenn eine Inventur über mehrere Standorte hinweg durchgeführt werden muss,
gilt es vor allem zu beachten, dass die Betrachtung der Mengen wirklich gleichlautend
durchgeführt wird, da es sonst zu falschen Inventurkorrekturbuchungen kommen kann.

Die Situation:
- es sind zwei Mandanten beteiligt
- Mandant 001 liefert Ware an Mandant 002 zur Lohnfertigung (Verlängerte Werkbank)
- um das Material auch im M001 zu verwalten, werden auch im M001 entsprechende Lose angelegt und bebucht
- M002 hat eine eigenen **Kieselstein ERP** Installation, die Datenbanken sind nicht verbunden

Ablauf der Lieferung von M001 an M002
1. Es werden Produktionslose angelegt, deren Abbuchungslager das Lager-002 ist. Damit erreicht man auch den erwarteten Lagerstand bei M002, also eine Mengenüberwachung.
2. Es wird ein Lieferschein mit Ziellager Lager-002 bei M001 erstellt
3. Damit wird das Material (des Lieferscheines) von Hauptlager nach Lager-002 umgebucht. Somit ist das Material im Lager-002
4. Nun werden die Lose im M001 ausgegeben. Damit wird das im Los benötigte Material von Lager-002 (im M001) abgebucht. D.h. der Lagerstand ist bereits um die theoretischen Los-Bedarfe reduziert! [Siehe](#empfehlung)
5. nun wird die Ware physikalisch an M002 versandt und von diesem in sein Lager eingebucht
6. im M002 wird die Ware in die Produktion gegeben, die Lose, nach seinen Regeln ausgegeben

Durchführen der Inventur
- Die Erwartungshaltung ist, dass man von M002 eine Lagerliste bekommt, die dem gesamten Materialbestand der durch M001 beigestellten Ware beinhaltet.<br>
Also alles was M001 gehört, aber bei M002 liegt.<br>
Dafür wird von M002 das Material im Lager, aber auch das Material in der Produktion erfasst.
- Diese Liste wird an M001 übermittelt<br>
**ACHTUNG:**<br>
Wird nun diese (Lagerstands-)Liste mit dem Lagerstand des Lager-002 bei M001 verglichen, bzw. direkt als Inventurbestand eingespielt, so differiert diese um die von M001 ausgegebenen Lose, da diese Los-Ausgabe-Mengen ja bereits vom Lager-002 bei M001 entnommen wurden.<br>
D.h. für die Kontrolle muss auch bei M001 das Material auf den Losen des M001 (Fremdfertiger, EMS Dienstleister) gebuchten Materials addiert werden.<br>
Erst damit erhält M001 eine vom Wert her gleichwertige Liste wie er sie von M002 bekommen hat.<br>
Erst mit diesen Werten darf er prüfen und eventuelle Korrekturmengen buchen.<br>
<br>
D.h. der Lagerstand für das Lager-002 bei M001 ist:<br>
Lagerstand von M002 - Menge in offenen Losen bei M001 -> Lagerstand des Lager-002 bei M001

## Empfehlung
Da es bei oben beschriebener Vorgehensweise immer wieder zu Fehlbetrachtungen kommt und diese, insbesondere wenn eine Verrechnung zwischen den beteiligten Firmen kommt, dann zu fehlerhaften Lagerständen führt, raten wir zu folgender Vorgehensweise. Diese kannst du gerne auch für weitere Sub-Lieferanten anwenden, egal ob diese in deinem Eigentum sind oder nicht.

Vom liefernden Mandanten (M001) wird das gelieferte Material solange als in der Verfügung von M002 betrachtet, bis dieser das Material zurückgibt. Entweder in Form von verarbeiteten Teilen / Baugruppen, oder eben das Material 1:1.<br>
Das bedeutet, wenn gefertigte Baugruppen zurück kommen, buchst du eine entsprechende Losablieferung. Die Stückliste ist so eingestellt, dass **erst damit** die Materialentnahme (vom Lager-002) erfolgt. Somit hast du auch immer den Überblick, welches Material noch in der Verantwortung von M002 ist. Zum Zeitpunkt der Inventur musst du nur, eventuell vorher gelieferte Baugruppen bzw. Rohmaterial noch einbuchen. Du hast sofort den richtigen Lagerstand. Die Lagerverwaltung für M002 liegt und bleibt damit auch bei M002, was es insgesamt deutlich einfacher macht.<br>
Die Einstellungen dafür sind:
- in den Stücklisten, Kopfdaten ![](Materialbuchung_bei_Ablieferung.png)  Materialbuchung bei Ablieferung
- Voreinstellbar mit dem Parameter DEFAULT_MATERIALBUCHUNG_BEI_ABLIEFERUNG

Eventueller Nachteil dieser Betrachtung: Wenn zum Zeitpunkt der Lieferung von M002 das Material nicht am Lager-M002 gebucht ist, muss der Wareneingang entsprechend handeln. Auch bucht idealerweise der Wareneingang damit die zurück erhaltenen Baugruppen, was eventuell in verschiedenen Firmen nicht erwünscht ist.