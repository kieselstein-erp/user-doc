---
title: "Freigabe"
linkTitle: "Freigabe"
categories: ["Artikel Freigabe"]
tags: ["Freigabe"]
weight: 600
description: >
  Artikel freigeben und sperren
---
Artikel Freigabe
================

Ergänzend zur Stücklistenfreigabe steht auch die Artikelfreigabe als zusätzliche Funktionalität zur Verfügung.
Bitte beachten Sie, dass die Artikelfreigabe nur in Verbindung mit der Stücklistenfreigabe verwendet werden kann.

Die Artikelfreigabe dient vor allem dazu, bestimmte Eigenschaften von Artikeln von verantwortlichen Personen zu definieren und diese Eigenschaften nach der Freigabe nicht mehr ändern zu können. Damit verbunden ist, dass für die Freigabe und auch die Rücknahme der Freigabe von Artikeln das Benutzerrecht WW_FREIGABE erforderlich ist.

Wird ein Artikel neu angelegt, so ist er per default nicht freigegeben.
Wird die Freigabe des Artikels zurückgenommen, muss auch eine Begründung mit angeben werden.
Sowohl Freigabe / Rücknahme der Freigabe und die Begründung sind im Artikel Änderungsprotokoll ersichtlich.

Wird die Freigabe zurückgenommen, so wird überprüft, ob dieser Artikel in freigegebenen Stücklisten enthalten ist. Wenn ja kommt ein Hinweis. In welchen Stücklisten dieser Artikel enthalten ist, ist unter Info, Verwendungsnachweis ersichtlich. Wird die Freigabe eines Stücklistenartikels zurückgenommen, wird automatisch auch die Freigabe der Stücklisten ebenfalls zurückgenommen.

Info: Hat der User kein Recht die Stücklisten zurückzunehmen kann er aus diesem Grunde auch die Freigabe des Artikels nicht mehr aufheben.

Bei einem Freigegebenen Artikel sind folgende Reiter nur mehr ReadOnly:
Detail, Technik, Ersatztypen

Für die Übersicht über den Freigabestatus der jeweiligen Stückliste und deren Positionen siehe bitte im Stücklistenmodul, Info, Freigabe.

Wenn eine Stückliste freigegeben wird, es greift hier das Recht STK_FREIGABE_CUD, so wird geprüft, ob alle in dieser einen Stückliste enthaltenen Positionen freigegeben sind. Ist die Stücklisten Position eine (Unter-)Stückliste, so muss auch diese Stückliste freigegeben sein. Hat die Stücklisten Position Ersatztypen auf der Stücklistenposition, so müssen alle Artikel der erlaubten Ersatztypen freigegeben sein. Sind bei dem Artikel der Stücklistenposition Ersatztypen hinterlegt, so müssen auch hier alle Artikel freigegeben sein.

Umgekehrt: Wird eine Stückliste freigeben, muss geprüft werden, ob auch alle Artikel freigegeben sind. Wenn nicht, kommt eine Warnung mit trotzdem freigeben oder abbrechen.

**Hinweis:** Es wird immer nur die eine Ebene der jeweiligen Stückliste aber mit Ersatztypen auf der Stücklistenposition bzw. über den Artikel betrachtet.
Wenn man nun eine Freigabe zurücknehmen will, muss man faktisch die übergeordneten auch zurücknehmen.

Für die Freigabe eines Artikels klicken Sie im Detail des Artikels auf das grüne Freigabehakerl ![](Artikel_Freigabe1.gif). Wenn ein Artikel bereits freigegeben ist, so sieht man wann diese Freigabe von wem durchgeführt wurde.

In den Stücklistenformularen Ausgabestückliste, Stückliste mit Preis und Stückliste stehen zusätzlich die Informationen je Stücklistenzeile über den Freigabestatus des Artikels zur Verfügung (ArtikelFreigabeZeitpunkt, ArtikelFreigabePerson).

**Achtung: Handartikel sind nie freigegeben!**
Eine Stücklistenposition ist dann freigegeben, wenn der Artikel und gegebenenfalls die (Unter-)Stückliste freigegeben sind. Zusätzlich müssen, wenn auf der Positionen Stkl-Pos-Ersatztypen sind, diese ALLE freigegeben sein. Wenn für den Stkl-Pos-Artikel Ersatztypen gegeben sind, müssen auch diese ALLE freigegeben sein.

#### Wie erkennt man freigegebene Artikel?
Freigegebene Artikel werden in der Artikelauswahlliste aber auch in den Stücklisten-Positionen durch einen ![](Artikel_Freigabe.gif) grünen Haken angezeigt. Da nach diesem Status auch sortiert werden kann, finden Sie bei Stücklisten sehr rasch die noch freizugebenden Artikel.

#### Was ändert sich an der Stücklistenfreigabe?
Mit der Zusatzfunktion Artikelfreigabe wird bei der Freigabe von Stücklisten auch geprüft, ob alle in der jeweiligen Stückliste enthaltenen Artikel freigegeben sind. Da auch die Stücklisten Artikel sind, wird somit die Freigabe idealerweise von unten nach oben, also von der untersten Baugruppe der Stücklistenhierarchie hin jeweils übergeordneten Stückliste durchgeführt.
Sollte nun eine Stückliste freigegeben werden, deren Artikel noch nicht alle freigegeben sind, so erscheint die Warnung:
![](Stuecklisten_Freigabe.jpg)
In aller Regel werden Sie dadurch die Stückliste nicht freigeben und mit dem Freigabe-Report (Info, Freigabe), bzw. der Sortierung nach dem Freigabestatus die entsprechenden Artikel freigeben.

Wird die Freigabe eines Artikels zurückgenommen, so wird damit automatisch auch die Freigabe der Stückliste zurückgenommen.
Gegebenenfalls erscheint die Meldung:
![](Stuecklisten_Freigabe_zuruecknehmen.jpg)
womit eben auch die Stücklisten in der der Artikel verwendet wird, Ihren Freigabestatus verlieren.

Bitte beachten Sie, dass diese Funktion nicht rekursiv nach oben alle Freigaben auflöst. Dies liegt bitte in Ihrer Verantwortung als **Kieselstein ERP** Anwender.

#### Wo sieht man wann ein Artikel, eine Stückliste freigegeben wurde?
Siehe im Artikel Info, Änderungen. Hier wird auch der bei der Rücknahme der Freigabe anzugebende Grund protokolliert.