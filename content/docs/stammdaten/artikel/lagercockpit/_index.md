---
title: "Lagercockpit"
linkTitle: "Lagercockpit"
categories: ["Lagercockpit"]
tags: ["Lagercockpit"]
weight: 1100
description: >
  Ein- und Umlagerung im Artikel / Wareneingang
---
Lagercockpit
============

Mit dieser Funktion wird die Thematik der Ein- und Umlagerung abgebildet.

Der Grundgedanke ist, dass einige wenige Personen für die gesamte Lagerwirtschaft in Ihrem Unternehmen zuständig sind und dass diese auch die Verteilung der erhaltenen Waren auf die Lose/Fertigungsaufträge buchen und zusätzlich auch die Umlagerungen vom Los zurück an das Lager oder an ein anderes Los vornehmen. 

**Voraussetzungen:**

Diese Funktion muss für Ihre Installation freigeschaltet sein.

Sie müssen das Recht WW_LAGERCOCKPIT besitzen.

Es muss ein Lager der Lagerart Wareneingang definiert sein. 
**Hinweis:** Denken Sie daran, dass für die Umbuchungen auch das Recht auf das Lager Wareneingang erforderlich ist.

Im Lagercockpit gibt es drei Reiter:

## Auswahl:
Hier sehen Sie alle im Lager Wareneingang, genauer im Lager der Lagerart Wareneingang, lagernden Artikel.
Der angezeigte Lagerstand ist der Lagerstand des Artikels im Wareneingangslager.
Wählen Sie hier den umzubuchenden Artikel aus.

## Buchungen:
Hier sehen Sie, wie sich der Artikel auf die Lose verteilt und eine Zuordnung der dann ev. verbleibenden Menge.
![](Lagercockpit1.jpg)
Durch Klick auf Entnahme ![](Lagercockpit2.gif) bzw. mit Strg+B wird die Entnahmebuchung auf das Los durchgeführt.
Hier kann zwischen Buchung auf Los oder Lager ausgewählt werden.
Wird die Buchung durchgeführt, so wird sofort die Warenbewegung ausgeführt und die Zeilen verschwinden aus der Liste.
D.h. es empfiehlt sich, für die Arbeiten der Lagerumbuchung, zuerst das Journal WE-Lager-Verteilungsvorschlag auszudrucken und auf diesem die tatsächlich durchgeführten Umbuchungen zu notieren um diese dann direkt am Rechner durchzuführen.
Bitte beachten Sie, wenn die gesamte Menge des im Wareneingangslager befindlichen Artikel aus dem Wareneingangslager in die Lose oder in die anderen Lager umgebucht wurden, so verschwindet dieser Eintrag (Artikel) aus der Auswahlliste.

## NLB-Artikel:
In dieser Liste sehen Sie die Wareneingangspositionen, welche nicht Lager bewirtschaftete Artikel sind. Durch einfachen Klick auf den Buchen Knopf ![](Lagercockpit5.jpg) werden diese Wareneingangspositionen als verräumt gekennzeichnet und sind damit in der Liste nicht mehr enthalten.

![](Lagercockpit4.jpg)

Die verräumte Wareneingangsposition wird in der Bestellung im Reiter Wareneingangsposition in grün angezeigt.

Ein Zurücknehmen des Verräumens ist derzeit nicht vorgesehen.

## Materialumlagerungen:
Diese Liste sind die Materialpositionen der Lose, bei denen Umlagerungsbuchungen durchgeführt werden sollen.
D.h. entweder ist zuviel oder zu wenig Material am Los gebucht.
Ein Zuviel am Los kann z.B. durch eine Änderung der Sollmenge durch die Stückliste zustande gekommen sein.
Dieses Zuviel wird durch eine negative Menge und in Grün dargestellt.
![](Lagercockpit3.jpg)
Üblicherweise werden nur Artikel angezeigt, die auch tatsächlich auf die Lose gebucht werden können. D.h. diejenigen Lospositionen bei denen die Sollmenge ungleich der Istmenge (der bereits auf das Los gebuchten Menge) ist und bei denen in den Lagern der Lagerart Hauptlager, Normallager und Halbfertiglager, ein Lagerstand eingetragen ist, bzw. die Positionen die vom Los zurückgegeben werden können.
Wird der Haken bei nur Artikel mit Lagerstand entfernt, werden alle Los-Positionen angezeigt, bei denen die Soll-Menge nicht gleich der Ist-Menge ist.

Das bedeutet, bei den Rückgaben, welche in grün angezeigt werden, 
![](Lagercockpit_Umlagerung.gif)
stehen bei der Materialumbuchung zwei Möglichkeiten zur Auswahl:
![](Lagercockpit_Umlagerung_buchen.jpg)
- a.) Sie buchen dies an das Lager zurück
- b.) Sie buchen dies auf ein anderes Los um.

Klicken Sie dazu die neben den Auswahlknöpfen befindlichen Radioknöpfe an und wählen Sie entsprechend Lager bzw. Los aus. Beim Lager wird automatisch das Lager des ersten Lagerortes des Artikels vorbesetzt.

**Hinweis:** Bitte bedenken Sie dass diese Materialumlagerung bei jeder Aktualisierung die Daten aus den Lospositionen herausrechnen. Das bedeutet somit auch, dass jede Umbuchung sich sofort in der Liste wiederspiegelt und sich dadurch die Liste entsprechend dem nun aktuellen Stand ändert, also Positionen verschwinden / auftauchen.

**Tipp1:** Stellen Sie bei allen Lieferanten das Zu-Lager auf das Lager Wareneingang. Damit wird bei der Wareneingangsbuchung automatisch das richtige Lager vorgeschlagen.

**Tipp2:** Schalten Sie für die Wareneingangsbuchenden Personen das Recht FERT_DARF_FEHLMENGEN_PER_DIALOG_AUFLOESEN ab, also entfernen.

**Tipp3:**
Stellen Sie den Parameter BEI_LOS_AKTUALISIERUNG_MATERIAL_NACHBUCHEN = 0\. Damit wird bewirkt, dass bei der Aktualisierung der Stücklisten nicht automatisch die Korrektur der Ausgabemengen in den Losen gebucht werden. Schalten Sie zusätzlich den Parameter DEFAULT_MATERIALBUCHUNG_BEI_ABLIEFERUNG = 1\. Damit bei der Ausgabe der Lose die Materialpositionen im Lager verbleiben und somit die Fehlmengen im Reiter Materialumlagerung sichtbar werden.

## Journale:
-   Unter Auswahl steht das Journal WE-Lager-Verteilungsvorschlag zur Verfügung. Damit kann vor dem Buchen eine Liste der vom Wareneingangslager in die Produktion bzw. in das Lager zu buchenden Teile ausgedruckt werden, zur effizienten Verteilung der Artikel.

-   Unter Materialumlagerungen steht das Journal Materialverteilungsvorschlag zur Verfügung.
    Auch diese Liste dient der effizienten Materialverteilung zwischen Produktion und Lager.

#### Wie wird im Lagercockpit mit nicht lagerbewirtschafteten Artikel umgegangen?
Wenn der Parameter NICHT_LAGERBEWIRTSCHAFTETE_SOFORT_AUSGEBEN auf 1 gesetzt ist, so werden bei der Materialumlagerung die nicht lagerbewirtschafteten Artikel ausgeblendet.
Es gibt nun einen Parameter NICHT_LAGERBEWIRTSCHAFTETE_SOFORT_AUSGEBEN =0.
Nicht lagerbewirtschaftete Artikel werden sofort ausgeben, wenn 'Materialbuchung bei Ablieferung' in der Stückliste angehakt ist. Im Lagercockpit werden bei der Materialumlagerung die nicht lagerbewirtschafteten Artikel ein/ausgeblendet.
Wenn die Lose per Stückliste aktualisiert werden, dann werden die nichtlagerbewirtschafteten Artikel trotzdem nachgebucht, auch wenn der Parameter BEI_LOS_AKTUALISIERUNG_MATERIAL_NACHBUCHEN = 0 ist

#### Auftrags Nachkalkulation über Lagercockpit hinweg?
Wenn das Lagercockpit verwendet wird, wird bei der Auftragsnachkalkulation die Handumbuchung entsprechend aufgelöst, d.h. es wird bei einer Handlagerbewegung nach dem allerersten Urpsrung gesucht. Dies wird in der Auftragsnachkalkulation und in der Warenzugangsreferenz (welche die Los-Gesamtkalkulation verwendet) berücksichtigt.

#### Wie erhält man einen schnellen Überblick über umzulagernde Artikel aus der Fertigung?
Im Reiter Materialumlagerung finden Sie eine Möglichkeit nach Alle Artikel, nur Rücknahmen aus Fertigung und nur Artikel mit Lagerstand zu filtern.<br>
![](Ruecknahmen_Fertigung.png)
Auch im Report Journal Material Verteilungsvorschlag besteht die Filtermöglichkeit und Sie erhalten der Auswahl entsprechende Liste mit dem Verteilungsvorschlag.