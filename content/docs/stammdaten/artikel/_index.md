---
title: "Artikel"
linkTitle: "Artikel"
categories: ["Artikel"]
tags: ["Lager", "Lagerplätze", "Gestehungspreis", "Einkauf", "Verkauf"]
weight: 100
description: >
  Wozu brauche ich überhaupt eine Artikelverwaltung?
  Damit man auch mal eine Statistik hat, seine Lagerstände sieht, weiß was man in Zukunft produzieren / einkaufen sollte, zu welchen Preisen eingekauft und verkauft wurde und von welchen Artikeln man denn so überhaupt lebt. Und vieles anderes mehr.
---
Artikel
=======

## Was sind eigentlich Artikel?
Artikel sind das Herzstück eines jeden ERP Systems. Egal ob es um Beschreibungen der Artikel geht, sowohl für z.B. Dienstleistungen, aber auch gehandelt Waren oder selbst hergestellte Produkte, oder um Lagerstände, um Einkaufs-, Verkaufs-, Lagerpreise und -werte. Wann wurde was eingekauft oder verkauft, mit oder ohne Chargen- bzw. Seriennummern und vieles andere mehr.

## Wie lege ich neue Artikel an?
Worauf ist zu achten?
- beginne mit kurzen z.B. fünfstelligen Artikelnummern
- Definiere in Bezeichnung und Zusatzbezeichnung den Artikel so genau, dass das gesamte Team damit weiß was wirklich gemeint ist. Schreibe dabei keine Romane sondern kurze knackige Definitionen.
- bedenke dabei, ein Artikel ist alles, Einkaufsartikel, Verkauf, Stückliste, Rohware, Tätigkeit usw..
- Es muss in jedem Falle sichergestellt sein, dass es einen Artikel nur einmal gibt und nicht den gleichen Artikel unter verschiedenen Artikelnummern mehrfach. Sollte dies, z.B. aus eine Alt-Daten-Übernahme der Fall sein, muss dies **sofort** bereinigt werden.
- Denke den Artikel im Sinne der Fertigung
  - D.h. Stangenmaterial wird zwar in kg eingekauft, aber in m bewirtschaftet und in mm in den Stücklisten verwendet. Zugleich ist, normalerweise, die Verpackungseinheit auf die Länge der Stange gestellt. Zusätzlich wird eventuell die Verschnittmenge für automatische Mehrbedarfsberechnungen gesetzt.
  - Stücklisten sind immer auch Artikel
- 

## Allgemeines zu Artikeln
#### Finden von Artikeln
In **Kieselstein ERP** stehen bei der Artikelauswahl immer folgende Direktfilter zur Verfügung.
![](Artikelnummer_Auswahl.jpg)
Für die Textsuche steht die kombinierte Textsuche zur Verfügung. D.h. mit obigen Werten erhalten Sie alle Artikel die in den Texten an beliebiger Stelle wid in Groß- oder Kleinschreibung und an einer anderen Stelle der Texte 1206 enthalten haben.

Zusätzlich können hier, wie in allen Direktfiltern von **Kieselstein ERP** auch, die Suchkombinationen verwendet werden.

| % | für beliebige Zeichen |
| --- |  --- |
| _ | für EIN beliebiges Zeichen an dieser Stelle (_ = Unterstrich) |

verwendet werden. 

Beispiel:

| Suchstring | findet |
| --- |  --- |
| Kabel_aum%1 | Kabelbaum 123 |
| V%kabel%L=__m | Verbindungskabel L=19mVerbindungskabel Lang am Bagger L=17mResolverkabel L=10m |

Um eine Bezeichnung in den Artikelkommentaren zu finden, verwenden Sie bitte den Knopf ![](Kommentarsuche_artikel.JPG) Suche nach Kommentar. Danach öffnet sich ein Fenster indem Sie nach Wörtern in den Kommentaren suchen können.

#### Sortierung nach SI-Wert
Gerade in der Elektronik ist es sehr praktisch wenn die elektronischen Bauteile (Widerstände, Kondensatoren, Drosseln, usw.) aufsteigend nach deren Werten zur Basis der SI-Einheit (Internationales Einheitensystem) aufgelistet werden. Für die Sortierung des Artikelstamms nach diesen Werten haken Sie dazu das Feld nach SI-Wert sortieren an.
Erhalten Sie dabei eine Sortierung wie die nachfolgende, so sind die Texte noch nicht sortiert. ![](SI_unsortiert.gif)
In diesem Falle müssen die SI-Werte über System, Pflege, SI-Werte nachgetragen werden. Danach sieht die Sortierung wie folgt aus.
![](SI_sortiert.gif)
Aufgrund der Sortierung nach SI-Werten gibt es natürlich auch Anzeigen wie die folgende:
![](SI_sortiert2.gif)
D.h. die Werte werden, egal welche Schreibweise für Sie die richtige ist, entsprechend dem absoluten Werten eingereiht.

#### Kann direkt auf einzelne SI Werte zugegriffen werden?
Manchmal ist es wünschenswert, dass nur spezifische Werte angezeigt werden. D.h. abhängig von der Einstellung ob die Suche mit oder ohne SI-Werten erfolgen sollte, wird der Bereich des Textes entsprechend interpretiert.
Suche nach einem Widerstand mit 0,086k Ohm.
Ergebnis 1 mit SI-Wert Suche:
![](Suche_mit_SI-Wert.gif)

Ergebnis 2 ohne SI-Wert Suche:
![](Suche_ohne_SI-Wert.gif)
Da die textliche Bezeichnung ja 0k086 lautet.

Welche SI-Einheiten werden unterstützt, wie erfolgt die Ermittlung des Wertes?

Für die Ermittlung des Wertes werden die Bezeichnung plus der Zusatzbezeichnung plus der Zusatzbezeichnung2 von links (Bezeichnung) beginnend nach einem Text welcher aus Ziffern und einer der SI-Einheiten besteht durchsucht. In dieser Wertedefinition dürfen KEINE Leerstellen enthalten sein oder sonstige Trennzeichen sein. Ausgenommen davon . (Punkt) und , (Komma) welche beide als Dezimaltrenner interpretiert werden.

Als SI-Einheiten werden derzeit 

R ... Ohm ... Hinweis: Wegen der möglicherweise gegebenen Kollision mit Exa bitte das R für die Entsprechung von Ohm verwenden.

F ... Farad

unterstützt.

Für die wissenschaftliche Notierung der Größen werden folgende Zehner-Potenz Notierungen unterstützt.

| Zeichen | Begriff | Wertebereich | Bemerkung |
| --- |  --- |  --- |  --- |
| k | Kilo | 10 ^ 3 | Achten Sie auf den Kleinbuchstaben k |
| M | Mega | 10 ^ 6 | Achten Sie auf den Großbuchstaben M |
| G | Giga | 10 ^ 9 |  |
| T | Tera | 10 ^ 12 |  |
| P | Peta | 10 ^ 15 |  |
| E | Exa | 10 ^ 18 |  |
| Z | Zetta | 10 ^ 21 | Achten Sie auf den Großbuchstaben Z |
| Y | Yotta | 10 ^ 24 | Achten Sie auf den Großbuchstaben Y |
|  |  |  |  |
| m | Milli | 10 ^ -3 | Achten Sie auf den Kleinbuchstaben m |
| µ | Mikro | 10 ^ -6 | (AltGr+M) |
| n | Nano | 10 ^ -9 |  |
| p | Piko | 10 ^ -12 |  |
| f | Femto | 10 ^ -15 |  |
| a | Atto | 10 ^ -18 |  |
| z | Zepto | 10 ^ -21 | Achten Sie auf den Kleinbuchstaben z |
| y | Yokto | 10 ^ -24 | Achten Sie auf den Kleinbuchstaben y |

Bei der Angabe der Zehnerpotenzen beachten Sie bitte generell die Groß-/Kleinschreibung. So liefern Werte wie 22K keine Ergebnisse, da das Großbuchstaben K nicht definiert ist. 22k hingegeben genau diesen Wert.
In der Sortierung werden die Werte die nicht errechnet werden konnten am Ende der Tabelle angeführt.
D.h. finden Sie eine Sortierung vor wie z.B. die nachfolgende ![](SI_falsch.gif) so liegt die Ursache darin, dass ein nicht definiertes Zeichen für die SI-Wert Berechnung / die Zehnerpotenz angegeben wurde.
Damit bei der Eingabe der Bezeichnungen, welche ja immer inkl. der Werte ist, auch erkannt wird, wird der ermittelte Wert direkt angezeigt.
Wird z.B. der Artikel laut obigem Beispiel von 1K auf 1k korrigiert, so erscheint der SI Wert rechts neben der Bezeichnung. ![](SI-Wert_Eingabe.gif)

<a name="SI-Wert"></a>

#### Wie wird bei den SI-Werten mit m für Meter usw. umgegangen?  Es kann mit den Parametern SI_EINHEITEN, SI_OHNE_EINHEIT gesteuert werden, wie sich **Kieselstein ERP** verhalten sollte.
Üblicherweise steht SI_OHNE_EINHEIT auf 1\. Das bedeutet, dass z.B. 3k3 bedeutet es sind 3.300 (Ohm, Meter, Amper, Volt, Gramm, usw.)
Durch das Abschalten von SI_OHNE_EINHEIT (= setzen auf 0) müssen die von Ihnen gewünschten / benötigten Einheiten im Parameter SI_EINHEITEN definiert werden (SI = wissenschaftliche Einheiten wie Meter, Volt, Farad, Gramm, usw.) .
Das bedeutet: verwenden Sie z.B. Werte wie nm (Nano-Meter) so sind die beiden Parameter wie folgt zu definieren:
SI_OHNE_EINHEITEN ... 0
SI_EINHEITEN ... R,F,H,m
Dies bewirkt, dass auch die Wellenlänge des Lichts, z.B. für LEDs angegeben werden kann.
![](SI_WERT_mit_Einheiten.gif)

SI_EINHEITEN: Folgende Buchstaben sind bereits als Si-Prefixe in Verwendung, und können nur als Einheit genutzt werden, wenn der Parameter SI_OHNE_EINHEIT auf 0 steht:
Y, Z, E, P, T, G, M, k, m, µ, u, n, p, f, a, z, y
Es wird zwischen Groß- und Kleinschreibung unterschieden.

SI_OHNE_EINHEIT: Ist dieser Parameter auf 1 gestellt, werden auch Si-Werte erkannt, welche keine Einheit haben (zB.: 3k, 3m3).
Bei der Einstellung 0 werden nur noch Werte erkannt, welche eine Einheit nach dem optionalen Si-Prefix haben. Dadurch können Einheiten im Parameter SI_EINHEITEN definiert werden, welche sich mit einem Si-Prefix den gleichen Buchstaben teilen (z.B.: m = Meter & m = milli).
Zusammenfassung:
1 erkennt: 3k, 5M, 3nF, 18H, 3k3, 3m = 0,003
0 erkennt: 3kF, 5MR, 3nF, 18H, 3,3kR, 3m = 3 Meter (wenn "m" als Einheit definiert ist, sonst wird nichts erkannt)

#### SI-Werte auch für Quarze Hz sortieren
Um auch die Quarze nach den SI-Werten sortieren zu können, tragen Sie in den Parametern SI_OHNE_EINHEIT 0 ein und definieren Sie unter SI_EINHEITEN R,F,H,Hz.
Damit werden auch die Hz (Hertz) als Einheit erkannt.

Definition von [Mengeneinheiten](mengeneinheit.md)

#### Alternative Artikelsuche
In manchen Bereichen ist es erwünscht die Auswahlliste der Artikel auf Artikelgruppen bzw. Artikelklassen einzuschränken.<br>
Stelle Sie dazu unter System den Parameter DIREKT_FILTER_GRUPPE_KLASSE_STATT_REFERENZNUMMER auf 1 und starten Sie das Artikelmodul neu. Nun kann anstelle der Referenznummer die Artikelklasse bzw. Gruppe als einschränkendes Direktfilter eingegeben werden. Bitte beachten Sie, dass hier nur die Vatergruppen (also übergeordneten) der Artikelgruppen angeführt werden für eine bessere Übersichtlichkeit. Wenn eine Vatergruppe ausgewählt wird, werden alle Artikel mit der Vatergruppe bzw. dessen Artikelgruppe die Vatergruppe als Ursprung haben, angezeigt.
Ev. nutzen Sie alternativ den Parameter ARTIKELGRUPPE_NUR_VATERGRUPPEN_ANZEIGEN und stellen diesen auf 2 um eine strukturierte Sicht in der Auswahl-Combobox der Artikelgruppen zu erhalten.

#### wie lang ist die Bezeichnung?
Die Länge der Artikelbezeichnungen (Bezeichnung, Zusatzbezeichnung, Zusatzbezeichnung2) ist üblicher Weise auf 40Stellen eingestellt.
Dies hat vor allem den Grund, dass in allen Listen und Anzeigen für die Länge der Artikelbezeichnung auch ausreichend Platz sein muss.
Nun haben einige wenige Anwender Mühe damit das Auslangen zu finden. Daher kann mit dem Parameter ARTIKEL_LAENGE_BEZEICHNUNG die Länge der Felder auf maximal 80Stellen erweitert werden.
Bitte beachten Sie, dass ev. Formulare für die vollständige Darstellung ergänzt werden müssen, bzw. dass auf Ihren mobilen Geräten unter Umständen einfach kein Platz dafür ist.

#### Gültige Zeichen der Artikelnummern
Definieren Sie die bei Ihnen gültigen Zeichen für die Artikelnummern unter System, Parameter. Am Einfachsten geben Sie im linken Direktfilter das Wort Zeichen ein und drücken Enter.
Nun Ändern Sie den Parameter ARTIKELNUMMER_ZEICHENSATZ wie gewünscht ab.

**WICHTIG:** Achten Sie unbedingt darauf, dass Sie nur Zeichen verwenden, welche auch von dem von Ihnen verwendeten Barcode gedruckt werden können. Üblicherweise sind die **Kieselstein ERP** Formulare mit dem Code 39 ausgeführt. [Siehe dazu auch]( {{<relref "/docs/installation/11_barcode">}} ).

**WICHTIG2:**
Bitte beachten Sie, dass das Zeichen $ für die Barcodesteuerung (Leadin) verwendet wird. Aus diesem Grunde sollte $ in der Artikelnummer nicht verwendet werden.

 **WICHTIG3:**
Bitte beachten Sie, dass das Zeichen \~ für die automatisierte Generierung von internen bzw. Einmalartikeln verwendet wird. Aus diesem Grunde sollte \~ in der Artikelnummer nicht verwendet werden.

#### Wie gestalte ich meine Artikelnummer am sinnvollsten
Unsere Empfehlung einfach und transparent und schnell erfassbar. Oder etwas ausführlicher:<br>
Die Struktur von Artikelnummern sollte so sein, dass die ersten zwei bis vier Stellen eine sehr grobe Gruppierung Ihrer Artikel sind. Danach kommt eine nummerische laufende Nummer, welche wenn gewünscht, gerne von **Kieselstein ERP** automatisch erzeugt werden kann.

Es war viele Jahre üblich, dass eine Artikelnummer sprechend sein muss. Sprechend bedeutet, dass jede Stelle der Artikelnummer eine spezielle Bedeutung hat. Einige Anwender gingen auch dazu über, dass die Herstellerartikelnummer in der eigenen Artikelnummer mit verpackt war. Dies resultierte dann in 40 und mehrstelligen Artikelnummern.<br>
Wir raten zur Verwendung von möglichst kurzen Artikelnummern, welche im wesentlichen in der Struktur KKKnnnn aufgebaut ist.<br>
KKK steht für eine sehr grobe Strukturierung der Artikelart, Einkaufsteil mechanisch, elektrisch, Zwischenprodukt, Fertigprodukt.<br>
nnnn steht für eine fortlaufende Nummerierung des Artikels.

Man hört nun, vor allem von sehr erfahrenen Einkäufer / Lageristen, das geht überhaupt nicht, wir sind nicht flexibel genug und man findet nichts.

**Was ist nun der Vorteil der kurzen Nummer?**<br>
Wenn die Struktur ähnlich der oben beschriebenen ist, hat die Nummer zwei Bereiche. Eine grobe Klassifizierung und eine laufende Nummer. D.h. der Mensch muss nur zwei kleine Blöcke lesen. Das kann er sicher und zuverlässig mit einem Blick erfassen. D.h. er erkennt sehr schnell ob das tatsächlich der richtige Artikel ist.

Das finden, geht in modernen Systemen wie **Kieselstein ERP** sowieso über die beschreibenden Texte, d.h. man hat mit sehr wenigen Eingaben sofort die 2-3 in Frage kommenden Artikel.

Und, was oft vergessen wird: Wenn Sie eine sehr lange Artikelnummer haben, manche denken da an 40 und mehr Stellen, so kann jede Stelle eine Bedeutung haben (sonst würde die Nummer ja nicht so lang sein). D.h. es muss jedes Mal die ganze Nummer mühsam Zeichen für Zeichen verglichen werden. Denke Sie dann z.B. an das Eintragen von Mengen in blinde Zähllisten, noch dazu von Menschen die normalerweise mit Ihrem Lager nichts zu tun haben dürften. Ist dann auch noch die Unsitte eingerissen, dass in der Artikelnummer Groß UND Kleinschreibung erlaubt ist, wer weiß denn dann noch ob das ein kleines l oder ein großes I ist ? Noch dazu wenn der Druck nicht 100%ig ist usw..

D.h. die Sicherheit der Lagerbewirtschaftung wird mit kurzen Artikelnummern eindeutig besser und dass in einer Artikelnummer keine Leerstellen und keine Kleinbuchstaben erlaubt sein sollten, ist für alle die damit länger zu tun haben, selbstverständlich. Datentechnisch können auch kyrillische Schriftzeichen in der Artikelnummer verwendet werden. Ob das bei uns sinnvoll ist, ist eine andere Sache. Wir raten zu keine Leerstellen, nur Großbuchstaben, keine bzw. nur die sinnvollen Sonderzeichen, welche von Ihrem bevorzugten Barcode auch problemlos dargestellt werden können.

Und die große Ausnahme:<br>
Sind Sie reiner Lohnfertiger, d.h. Sie produzieren ausschließlich Artikel die nur an einen einzigen Kunden gehen und dieser Kunde hat seine eigenen Artikelnummern, dann verwenden Sie bitte die Artikelnummern Ihres Kunden. Ev. vorne ergänzt um ein kleines Kennzeichen, welcher Kunde das ist und danach kommt 1:1 die Kundenartikelnummer.

Zusätzlich:
Erweitern Sie die (Artikel-)Referenznummer zur Zeichnungsnummer und aktivieren Sie die Anzeige der Zeichnungsnummer in allen Belege indem der Parameter REFERENZNUMMER_IN_POSITIONEN auf 1 gestellt wird. Bitten Sie Ihren **Kieselstein ERP** Betreuer ev. die Formulare um den Andruck dieses Feldes zu erweitern

#### wir haben mehrere Artikelnummern, was tun?
Vermeiden Sie dieses Dilemma wie der Teufel das Weihwasser. Lässt es sich trotzdem nicht vermeiden, nutzen Sie die Felder Kurzbezeichnung bzw. Referenznummer. Ihr **Kieselstein ERP** Betreuer beschriftet diese Felder gerne für Sie mit z.B. Alte Artikelnummer oder ähnlichem. [Siehe auch]( {{<relref "zusammenfuehren">}} )

Achten Sie in jedem Falle darauf, dass ein Artikel immer nur als ein Datensatz angelegt ist. Sollten tatsächlich der Fall eingetreten sein, dass versehentlich ein Artikel zwei Mal angelegt wurde, so führen Sie dies besser heute als morgen [zusammen]( {{<relref "zusammenfuehren">}} ).
Haben Sie den physikalisch gleichen Artikel mehrfach angelegt, stimmen Ihre Lagerstände nicht und es stimmt auch die Beschaffung nicht.

Ein tatsächlich in der Praxis eingetretener Fall. Es werden verzweifelt nicht ganz billige Teile gesucht. Da diese unter zwei verschiedenen Artikel(nummern) angelegt waren, hat man nicht gesehen, dass am zweiten Artikel ausreichend Bestand ist. So wurde, der Liefertermin an den Kunden nicht eingehalten, die Teile mit Express und Zuschlagskosten beschafft. Das alles kann man sich ersparen.

#### Wo gebe ich Zeichnungsnummern ein?
Auch hier, eigentlich ist die Zeichnungsnummer die Teilenummer, also die Artikelnummer. Wenn Sie wirklich zwei Nummernkreise verwalten müssen, so nutzen Sie das Feld Kurzbezeichnung oder Referenznummer und bitten Sie Ihren **Kieselstein ERP** Betreuer dies für Sie entsprechen zu benennen.

#### Kann ich Zeichnungsnummern überall verwenden?
Da gerade im Bereich der Lohnfertigung sich die doppelte Verwaltung der Artikel- und der Zeichnungsnummern nicht umgehen lässt, haben wir mit dem Parameter REFERENZNUMMER_IN_POSITIONEN die Möglichkeit geschaffen, diese Nummerierung parallel zu den Artikelnummern zur Verfügung zu stellen.
Bitte beachten Sie, dass die Referenznummer nicht eineindeutig ist. D.h. es wird nicht geprüft ob es diese Referenznummer, die ja dann generell als Zeichnungsnummer benannt sein wird, schon einmal in Ihrem **Kieselstein ERP** Artikelstamm gibt. D.h. Wenn Sie die Referenznummer als Zeichnungsnummer verwenden und diese auch so benannt ist, kann durch aktivieren des obigen Parameters diese faktisch überall verwendet werden bzw. steht sie in den Reports zur Verfügung. Denken Sie daran für die Anzeige in der Artikelauswahlliste auch den Parameter ANZEIGEN_REFERENZNUMMER_IN_AUSWAHLLISTE zu aktivieren.

#### Können Artikel auch wieder gelöscht werden?
Ja, aber.<br>
In der Regel haben einmal angelegte Artikel bereits Abhängigkeiten. D.h. zu einem Artikel wurden, zum Teil automatisch, weitere Informationen wie Abmessungen, Kommentare, Artikellieferanten usw. angelegt. Zusätzlich wurde ein Artikel deswegen angelegt um ihn in Stücklisten, Bestellungen usw. zu verwenden. 
Ein in weiteren Modulen verwendeter Artikel, kann aus Datenbankgründen nicht mehr gelöscht werden. Um diesen aber trotzdem in der täglichen Arbeit nicht mehr zu sehen, setzen Sie diesen Artikel bitte auf versteckt.
Sie benötigen dazu das Recht: LP_DARF_VERSTECKTE_SEHEN.
Um nun versteckte Artikel wieder anzuzeigen, muss in der Artikelauswahlliste lediglich auf ![](Plus_Versteckte.gif) geklickt werden. Es ist dafür ebenfalls obiges Recht erforderlich. **<u>Hinweis:</u>** Wurde ein Artikel (nur) versehentlich mit der falschen Artikelnummer angelegt, so kann diese Artikelnummer komfortabel einfach durch Überschreiben auf die richtige Nummer geändert werden.

#### Anlegen von (fast) gleichen Artikeln
Wird in der Auswahlliste des Artikelmoduls auf ![](Artikel_kopieren.gif) geklickt, so starten Sie damit das Kopieren eines fast gleichlautenden Artikels. Wählen Sie nun den zu kopierenden Artikel aus.<br>
![](Artikel_kopieren.jpg)<br>
Im nun erscheinenden Dialog geben Sie die neue Artikelnummer oder wählen "G" um automatische eine neuen Artikelnummer zu erzeugen. In der aufgelisteten Tabelle sind diejenigen Felder / Bereiche aufgeführt, die kopiert werden sollten.<br>
Diese Funktion ist so gedacht, dass einmalig die zu kopierenden Daten definiert und diese Einstellung abgespeichert (Klick auf speichern) wird. Damit werden für den nächsten Kopiervorgang die gleichen Felder zum Kopieren vorgeschlagen.<br>
Wurde "mit Kommentar" ebenfalls angehakt, so werden zusätzlich noch die Kommentardateien des Artikels mitkopiert.<br>
Wenn Sie den Haken bei Eigenschaften setzen, so werden die Artikeleigenschaften kopiert, wenn der Haken nicht gesetzt ist, so werden die Basiswerte (Einstellung für das jeweilige Feld in den Artikeleigenschaften) bei den Artikeleigenschaften eingetragen.<br>
Bei den VK-Preisen wird die letztgültige VK-Preisbasis kopiert und das GültigAb auf Heute gesetzt. Es werden nur die aktuell gültigen aktiven Preislisten kopiert. Auch bei den Staffelpreisen wird nur der letztgültige Preis pro Staffel mit Gültigkeit=Heute kopiert. Zukünftige Preisdefinitionen werden nicht kopiert, wenn zukünftige Preise definiert waren, erscheint ein Hinweis.

**Hinweis:** Um die gewählten Einstellungen abspeichern zu können, ist das Recht LP_DARF_GRUNDDATEN_SEHEN und das Recht WW_ARTIKEL_CUD erforderlich.

#### Können Einmalartikel angelegt werden?
Ja, [siehe bitte]( {{<relref "/fertigung/losverwaltung/#einmalartikel" >}} )

#### Können doppelt angelegte Artikel wieder zusammengeführt werden?
Ja, [siehe bitte]( {{<relref "zusammenfuehren">}} ).

#### Können Kommentare nachträglich mitimportiert werden?
Ja, verwenden Sie dazu im Reiter Kommentar den Knopf ![](Artikel_Kommentare_kopieren.gif) Kommentare von anderem Artikel kopieren. In der anschließenden Abfrage nur Clientsprache oder alle Sprachen können Sie wählen, ob eben nur die Kommentare in Ihrer Sprache oder alle beim zu kopierenden Artikel hinterlegten Kommentare in allen Sprachen kopiert werden sollten.
Insbesondere wenn Sie die Kommentare aller Sprachen kopieren so denken Sie bitte daran, dass gegebenenfalls auch die Kommentare in den anderen Sprachen entsprechend angepasst werden müssen.

#### Wie behalte ich beim Anlegen neuer Artikel die Übersicht über die Artikelnummernstruktur.
Dieses Thema ist vor allem bei strukturierten Artikelnummern interessant. Diese Aufgabenstellung ist vor allem in der Elektronik / Elektrotechnik relevant.
Wenn ein neuer Artikel angelegt wird, so sollte man bereits die gewünschte Artikelnummer kennen um den Artikel von der Nummernstruktur her im richtigen Bereich anzulegen. Um dabei die Übersicht zu behalten, verwenden Sie anstatt des Neu(er Artikel) das Kopieren. Hier werden Sie zur Auswahl des zu kopierenden Artikels aufgefordert. Zugleich wird damit die Artikelnummer des bestehenden Artikels vorgeschlagen. So können Sie die gewünschte Artikelnummer sehr einfach definieren.

#### Im Artikel besteht die Möglichkeit Hersteller anzulegen. Welchen Sinn / Funktion erfüllt die zusätzliche Eingabe des Partners beim Hersteller?
In **Kieselstein ERP** wird klar zwischen Hersteller und Lieferant(en) eines Artikels unterschieden. Gerade in der Elektronik Branche werden oft Artikel verwendet, die von der Bezeichnung her identisch sind, aber Ihr Kunde schreibt einen bestimmten Hersteller vor. Durch die Angabe des Herstellers können auch diese Artikel unterschieden werden. D.h. für jeden gleichlautenden Artikel wird für jeden Hersteller ein eigener Artikel angelegt. Bitte beachten Sie dazu auch die Ersatztypenverwaltung im Modul Stücklisten. Unabhängig davon hinterlegen Sie für die einzelnen Artikel jeweils Lieferanten, hier kann der Hersteller auch Lieferant sein, meist gibt es jedoch einen Hersteller und X Lieferanten.
Die Länge der Herstellerbezeichnung können Sie mit dem Parameter ARTIKEL_LAENGE_HERSTELLERBEZEICHNUNG definieren.

#### Wo kann ich einen neuen Hersteller anlegen?
Wählen Sie dazu im Artikelmodul den unteren Modulreiter Grunddaten und dann den oberen Reiter ![](Hersteller.gif)

Legen Sie nun mit Neu einen neuen Hersteller aus oder ändern Sie die Zuordnung entsprechend ab.

#### Es ist der gleiche Hersteller mehrfach angelegt, wie kann ich das ändern?
Verwenden Sie dafür die Funktion "Hersteller zusammenführen". Diese finden Sie im Artikelmodul, unterer Modulreiter Grunddaten, oberer Modulreiter Hersteller. Im im Menü Grunddaten wählen Sie Hersteller zusammenführen. Im Dialogfenster wählen Sie den Quell-Hersteller, der in den Ziel-Hersteller übergeführt wird.

#### Wo können HerstellerNr und Bezeichnung des Herstellers erfasst werden?
Im Modul Artikel, oberer Reiter Bestelldaten kann der Hersteller, die Herstellernummer und Herstellerbezeichnung eingetragen werden. Mit Klick auf Hersteller können Sie den Hersteller des Artikels aus der Liste der vorhandenen Hersteller auswählen.
 ![](Hersteller_Bestelldaten.JPG)

#### Wo werden die Herstellernummer und Bezeichnung des Herstellers angedruckt?
Im Anfrage/Bestellungsdruck werden diese als Artikelnummer und Bezeichnung des Lieferanten verwendet, wenn im Artikellieferant ![](Lieferant_Herstellerverwenden.JPG) Herstellerbezeichnung verwenden angehakt ist.

#### Kann ich nach der Herstellernummer suchen?
In der Auswahlliste der Artikel steht der Direktfilter Lieferantennr. zur Verfügung. Hier kann auch nach der Herstellernummer bzw. Teilen davon gesucht werden.

#### Ich möchte die Herstellernummer auch in der Auswahlliste sehen
Aktivieren Sie dafür den Parameter ANZEIGEN_HERSTELLER_IN_AUSWAHLLISTE auf 1 und gegebenenfalls auch den Parameter ARTIKELSUCHE_MIT_HERSTELLER = 1 (Textsuche inkl. Herstellernummer und -bezeichnung) und starten Sie danach das Artikelmodul neu.

#### Warum sind im Warenbewegungsjournal nur Spaltensummen vorhanden, bzw. warum ist das kein Lagerstand?
Weil das Warenbewegungsjournal durch 2 Datumswerte eingeschränkt werden kann und die Summen dann nicht mehr den Lagerstand bilden.

<a name="zugehoeriger Artikel"></a>

#### Wozu dient "zugehöriger Artikel"
Dies ist quasi eine Erinnerungsfunktion in den Belegen. D.h. wenn Sie einen Artikel auswählen, bei dem ein dazugehöriger Artikel hinterlegt ist, so erhalten Sie in allen Verkaufsmodulen die Meldung. Zu diesem Artikel gehört .... . Sie können nun sofort die Übernahme veranlassen oder dies trotzdem überspringen. Einen zugehörigen Artikel definieren Sie im Ursprungsartikel im Modulreiter ![](Modulreiter_Sonstiges.gif).

Da in verschiedenen Fällen ([siehe Auftrags Nachkalkulation]( {{<relref "/verkauf/auftrag/nachkalkulation">}} ) eine direkte Verbindung zwischen der pauschal angebotenen Leistung und der intern kalkulierten Zeit / Tätigkeit gegeben ist gibt es die Möglichkeit einen Umrechnungsfaktor vom Ausgangsartikel zum dazugehörigen Artikel zu hinterlegen. Tragen Sie diesen Umrechnungsfaktor bitte unter ![](Zugehoerig_Multiplikator.gif) Multiplikator (das rechte Feld) ein. Damit wird sofort die passende Menge / Stundenanzahl ausgehend von der pauschal verkauften Dienstleistung hinterlegt.

Umgekehrt stellt sich auch oft die Forderung, dass bei Lieferung einer bestimmten Stückzahl auch noch die Verpackungsmittel als eigene Position mit angeführt werden müssen. Dafür haben wir die Möglichkeiten des zugehörigen Artikels um den Divisor ![](Divisor.gif) und die Rundung auf Ganze ![](AufGanzeAufrunden.gif)(Verpackungsmittel) aufzurunden ergänzt.
D.h. wenn z.B. 190Stk eines Produktes in eine Box passen, so definieren Sie bitte als zugehörigen Artikel die Box, schreiben die 190Stk in den Multiplikator, haken 1/Multiplikator an und haken auch auf Ganze aufrunden an. Wenn Sie nun z.B. 220Stk in den Lieferschein schreiben, so werden automatisch 2Stk der Box als zusätzlich zu buchen vorgeschlagen.

#### Wie kann ich einen zugehörigen Artikel wieder löschen?
Klicken Sie erneut auf ![](Button_Zugehoeriger_Artikel.gif) und in der Auswahlliste auf ![](Button_loeschen.gif) (löschen).

#### Wie kann ein Arbeitszeitartikel definiert werden?
<a name="Arbeitszeit"></a>
Um einen Artikel als Arbeitszeit zu definieren, muss die Artikelart auf Arbeitszeit gestellt werden. Dadurch wird automatisch die Mengeneinheit auf Stunden (h) gestellt. Denken Sie daran, den Artikel auf Nicht Lagerbewirtschaftet zu stellen (Reiter Sonstiges) und definieren Sie auch den Gestehungspreis des Artikels für die Deckungsbeitragsberechnungen bzw. Vorkalkulationen

#### Welchen Artikellieferanten muss ich eingeben, wenn ich meine eigene Arbeitszeit als Artikel anlegen will?
Als Lieferant sollte hier der eigene Mandant verwendet werden. Wichtig ist jedoch, dass für die Bewertung der Arbeitszeit der Einkaufspreis des ersten Lieferanten verwendet wird. Unabhängig vom Namen des Lieferanten.

#### Wo gibt man die Kosten einer zu verkaufenden Einheit Arbeit (in **Kieselstein ERP** ist das ja ein eigener Artikel) ein?
In **Kieselstein ERP** ist unter anderem auch die Arbeitszeit ein Artikel. Also kann für die Artikeldefinition die gesamte Palette des Artikelstammes verwendet werden. Um die Kosten einer Arbeitseinheit, wir sprechen hier auch von den Kosten der sogenannten Lohnmittelstunde, zu definieren, wird der bevorzugte Lieferant dieses Artikels verwendet. Also der Einkaufspreis des Artikels des ersten Lieferanten. **<u>Hinweis:</u>** Arbeitseinheiten können derzeit nur zur Basis Stunden definiert werden.

#### Bei Arbeitszeit von eigenem Personal, wo die Arbeitsschritte / Arbeitsabläufe als Artikel angelegt wurden, muss man Stunden sozusagen auf Lager legen.?
Dies ist eine Möglichkeit. Besser ist jedoch, Artikel die keinen Warenzugang haben auf nicht Lagerbewirtschaftet zu setzen. Sie finden diese Eigenschaft im Artikel unter Sonstiges.
Hinweis: Die Eigenschaft eines Artikels ob Lagerbewirtschaftet oder nicht kann nur dann verändert werden, wenn der Artikel auf keinem der Läger einen Lagerstand hat.

#### Reine Mannzeit, nur zur Info?
Diese beiden Werte dienen der Steuerung des Verhaltens des Arbeitsplanes. [Siehe dazu]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan/#hinweis-zu-reine-mannzeit"  >}} )

![](Reine_Mannzeit_NZI.gif) 

#### Nicht Lagerbewirtschaftet wozu ist dies?
Siehe oben, aber auch ganz allgemein. Es gibt immer wieder Artikel, von denen Sie gerne eine Statistik haben möchten, aber die nicht mit der üblichen Lagerbewirtschaftung gekoppelt sein sollten. Kennzeichnen Sie diese(n) Artikel als nicht Lagerbewirtschaftet. Mögliche Artikel: Arbeitszeiten Ihrer Mitarbeiter, Tagesdiäten, Reisekilometer, Softwareprodukte, usw.
**Hinweis:** Die Eigenschaft Lagerbewirtschaftet kann nur dann umgeschaltet werden, wenn der Artikel auf allen Lägern des Mandanten einen Lagerstand von 0 (null) hat und wenn bisher noch keine Lagerrelevanten Buchungen durchgeführt wurden. Wurde der Artikel einmal in einem Wareneingang, Handlagerbuchung, Lieferschein o.ä. verwendet kann nicht mehr umgeschaltet werden. Der Hintergrund ist in der nachfolgend beschriebenen Problematik (Alte Info) zu finden, da immer wieder Anwender zwischen den beiden Stati hin- und hergeschaltet haben.
Haben Sie einen Artikel bisher nicht lagerbewirtschaftet und sollte dieser nun Lagerbewirtschaftet werden, so schlagen wir vor, den neuen Artikel entsprechend anzulegen, ev. zu kopieren und bei dem alten Artikel unter Sonstiges den neuen Artikel als Ersatzartikel zu definieren.

Alte Info:
Was passiert, wenn nun so ein nicht lagerbewirtschafteter Artikel auf lagerbewirtschaftet umgestellt wird und danach alte Wareneingänge bebucht bzw. geändert werden.?
Da in diesem Falle die Buchung auf ein anderes Lager erfolgen würde, erscheint die Fehlermeldung UPDATE_AUF_LAGER_NICHT_MÖGLICH. Hintergrund: Es würde nun eine Aktualisierung der (alten) Lagerbuchung erfolgen. Diese wurde ja, da nicht lagerbewirtschaftet nicht gemacht und ist somit nicht vorhanden und kann daher auch nicht geändert werden.

#### keine Lagerzubuchung wozu ist dies?
Insbesondere in Installationen in der Elektronik verwendet man gerne globale, Hersteller unabhängige, Artikel für die Planung und dann bei der Ausführung, also der Überleitung der Stücklisten in das Los und im Endeffekt dann in die Bestellung werden exakte Hersteller Artikel verwendet. Somit ergibt sich implizit, dass auf die Herstellerunabhängigen Artikel keine Lagerzubuchung und damit auch keine Bestellung erfolgen darf. Dafür ist diese Eigenschaft gedacht. Ist bei einem Artikel dies angehakt, so kann darauf keine Lagerzubuchung gemacht werden, somit wird der Artikel auch nie einen Lagerstand haben. Zusätzlich wird dies beim Bestellvorschlag angezeigt, wobei es theoretisch im Bestellvorschlag keine Artikel die keine Lagerzubuchung haben geben darf. Wird so ein Artikel trotzdem in eine Bestellung eingetragen, so kann diese Bestellung nicht aktiviert / versandt werden.

#### Verleih wozu ist dies?
Dies dient der Definition von sogenannten Verleihartikeln. [Siehe dazu bitte]( {{<relref "/verkauf/auftrag/termin_in_stunden_minuten/#kann-der-verleih-auch-anders-verrechnet-werden-" >}} ).

#### Welches Lager darf von den Mitarbeitern bebucht werden?
[Siehe]( {{<relref "/docs/stammdaten/benutzer/#auf-welche-lager-darf-ein-benutzer-buchen" >}} )

#### Lager und Lagerplatz, wo ist der Unterschied?
Der Unterschied zwischen diesen beiden Begriffen ist ein gravierender.
Ein Lager hat mehrere Lagerplätze. Nur auf einem Lager können Lagerstände gebucht werden. In diesem Lager liegen die Artikel dann in den verschiedenen Lagerplätzen. Üblicherweise werden Lager für verschiedene Standorte oder Klassifizierungen verwendet.
So haben Sie einen Artikel bei Ihnen in der Zentrale. Daneben gibt es noch ein Außenlager in dem z.B. die Ware in Paletten gelagert liegt. Wenn nun die Ware im Hauptlager zu wenig wird, wird Ware vom Außenlager genommen und ins Hauptlager umgebucht.
Für Lager sind viele Anwendungsfälle möglich, wie z.B. Lager eines Fahr-Verkäufers, eines Servicetechnikers, Konsignationslager usw.. Jedes dieser Läger hat seine eigenen Lagerplätze in dem der Artikel in diesem Lager liegt.
Nur ein Lager hat einen Lagerstand, ein Lagerplatz ist nur die örtliche Zuordnung des Artikels in diesem Lager.

#### Lagerort oder Lagerplatz, wo ist der Unterschied?
Wir haben in **Kieselstein ERP** den Begriff Lagerplatz gewählt, um einen klaren Unterschied zum Lager herauszustreichen. Inhaltlich sind für uns Lagerplatz und Lagerort identisch.

#### Kann nach dem Lagerplatz gesucht werden?
In den Auswahllisten des Artikels kann durch Verwendung des Zusatzfilters ![](Zusatzfilter.gif) mit dem Kriterium Lagerplatz ![](Kriterium_Lagerplatz.gif) nach dem Namen des Lageplatzes gesucht werden. Ev. ist für Sie auch die Aktivierung der Lagerplatzanzeige durch den Parameter ANZEIGEN_LAGERPLATZ_IN_AUSWAHLLISTE hilfreich.

#### Kann ein nicht Lagerbewirtschafteter Artikel einen Lagerplatz haben ?
Die Eigenschaft "Nicht lagerbewirtschaftet" eines Artikels bedeutet nur, dass Sie für diesen Artikel, z.B. Beilagscheiben, keine Lagerstände, keine Lagerbewegungen verwalten wollen. Trotzdem kann es erforderlich sein, dass für diesen Artikel für verschiedene Lager Lagerplätze definiert werden. Aus diesem Grund haben wir die Möglichkeit der Definition der Lagerplätze NICHT verhindert.

#### Werden die Lagerstände bei Mehrlagerverwaltung zusammengezählt ?
Jein. D.h. in der Artikel Auswahlliste und in der Schnellinfo im Artikeldetail werden die Lagerstände für die Anzeige zusammengezählt, mit Ausnahme der Lager bei denen Konsignationslager angehakt ist. Wenn Sie in den Reiter Lager wechseln, so sehen Sie die Lagerstände je Lager detailliert auch mit den Gestehungspreisen je Lager.

#### Kann die Anzeige von Null-Lagerständen unterdrückt werden ?
Wenn in den Grunddaten des jeweiligen Lagers auch bei Leerstand anzeigen ![](auch_Leerstand_anzeigen.gif) angehakt ist, so wird der Lagerstand dieses Lagers auch angezeigt wenn nichts am Lager ist. Ist Leerstand anzeigen nicht angehakt, so wird die Liste der Lager im Modul Artikel, Reiter Lager entsprechend kürzer wenn auf den verschiedenen Lagern keine Lagerstände gegeben sind.

#### Gibt es einen Hinweis wenn ein Lagerplatz von einem anderen Artikel bereits belegt ist?
Wenn der Parameter WARNUNG_WENN_LAGERPLATZ_BELEGT aktiviert ist, so erhalten Sie bei der Auswahl eines Lagerplatzes umgehend einen Hinweis, wenn dieser Lagerplatz bereits von einem anderen Artikel besetzt ist. Dies dient der Information und hat sonst keine weiteren Auswirkungen.

#### Können Lagerplätze automatisch freigegeben werden?
Manchmal ist es gewünscht, dass frei gewordene Lagerplätze tatsächlich freigegeben werden. Das bedeutet, wenn der Gesamtlagerstand eines Artikels auf einem Lager 0 (Null) wird, so sollte zugleich auch die Lagerplatzzuordnung gelöscht werden. Stellen Sie dafür den Parameter LAGERPLAETZE_BEI_LIEFERUNG_UND_LAGER_NULL_LOESCHEN auf 1\. Damit wird, sofort bei einer Lieferung (Rechnung / Lieferschein), also der Entnahmebuchung in den Lieferschein / Rechnung, wenn der Lagerstand Null wird der Lagerplatz dieses Lagers für den gebuchten Artikel wieder gelöscht und steht somit für andere Artikel zur Verfügung.

#### Wo kann ich weitere Lager definieren, wo lege ich ein neues Lager an?
#### Lagerdefinition
Lager können im Module Artikel, unterer Modulreiter Grunddaten definiert werden. 
Bitte beachten Sie, dass bei den relevanten Benutzerrollen das Rechte um auf dem jeweiligen Lager buchen zu können eingetragen sein müssen [zur Beschreibung]( {{<relref "/docs/stammdaten/benutzer/#auf-welche-lager-darf-ein-benutzer-buchen" >}} ).

![](Lager_Definieren.gif)
Hier können folgende Definitionen vorgenommen werden:

| Lager  | Name des Lagers |
| --- |  --- |
| Lagerart | Derzeit wird zwischen Hauptlager, davon darf es nur eines geben und Lagerart Normal unterschieden.Die Lagerart Sperrlager wird an die Reports Rollierende Planung, Wiederbeschaffung und Los-Ausgabeliste für Auswertungen übergeben. [Siehe dazu auch]( {{<relref "/docs/stammdaten/artikel/serienchargennummern/#k%c3%b6nnen-einzelne-chargen-gesperrt-werden" >}} ). Die Lagerart Halbfertiglager wird für verschiedene Auswertungen (Auftrag, Journal, offene Positionen, Lagerstandsdetailauswertung) verwendet bzw. werden diese gerne in besonderen Reports für die [Darstellung des Warenflusses]( {{<relref "/warenwirtschaft/stueckliste/#fremdfertigungsst%c3%bcckliste" >}} ) genutzt. Alle weiteren Lagerarten werden in zukünftigen Funktionen verwendet. |
| Bei interner Bestellung berücksichtigen | Der Lagerstand dieses Lagers wird bei der internen Bestellung mitgerechnet. Oder umgekehrt: Ist dies beim gewählten Lager nicht angehakt, so wird der Lagerstand bei der internen Bestellung NICHT mitgerechnet. |
| Bei Bestellvorschlag berücksichtigen | Wie oben, jedoch für den Bestellvorschlag |
| Sortierung Losausgabe | Die Lager werden in der angegebenen Sortierreihenfolge als Abbuchungslager beim Anlegen der Lose dem Los zugeordnet. |
| Versteckt | Ein Lager, welches nur einmal bebucht wurde, kann nicht mehr gelöscht werden. Da es trotzdem immer wieder vorkommt, dass man Lager nicht mehr bebuchen will, kann hier die Anzeige eines Lagers global abgeschaltet werden. Entgegen der Regel "Darf Versteckte sehen", wird diese Funktion bei den Lägern NICHT unterstützt, da dies unter Umständen entsprechende Auswirkungen in der Bilanz usw. haben kann. |
| Kundenkonsignationslager | Damit wird bewirkt, dass der Lagerstand dieser Lager bei folgenden Betrachtungen NICHT berücksichtigt wird.- Lagerstand in Artikelauswahl/Detail- Im Report Auftrag/Offene Positionen,- Rollierende Planung und- im Report Wiederbeschaffung |
| Ladeadresse | Bei der Ladeadresse kann eine Partneradresse hinterlegt werden. Damit können Sie im Lieferschein z.B. Ihrer Spedition angeben, wo denn tatsächlich die Ware abgeholt werden sollte. |

#### Lagerart Halbfertiglager wozu?
Diese Lagerart dient vor allem der Steuerung des Warenflusses an Ihre Sub-Lieferanten, Fremdfertiger.
Sehr oft ist es so, dass Teile von der Produktion fertiggestellt werden und von den Mitarbeitern an einen bestimmten Platz gelegt werden. Trotzdem sollte der Disponent, der für die Auftragsabwicklung verantwortliche auf einen Blick sehen, welche Teile den nun eigentlich wirklich fertig für den Versand an den Fremdfertiger sind.
Hier hat sich bewährt, dass mehrstufige Stücklisten gemacht werden um den genauen Warenfluss abzubilden. Die Stücklisten die nach der Produktion im eigenen Hause weiter zum Veredler gebracht werden müssen, sind so eingestellt, dass die Ablieferbuchung an ein Lager der Lagerart Halbfertig gebucht wird. Somit kann man jederzeit sehen, welche Artikel von Ihrem (hauseigenen) LKW zum Beschichter usw. mitgenommen werden sollten. Hier wird gerne auch noch die Kombination mit der Artikelgruppe / -klasse verwendet um dann auch noch die unterschiedlichen Typen der Fremdbearbeitung abbilden zu können.

#### Konsignationslager wozu?
Um die Eigenschaft von Lagerbeständen, vor allem wenn Ware zwar in Ihrer Verwaltung, aber nicht in Ihrem Eigentum ist, also z.B. noch Ihrem Kunden gehört und vor allem wenn Sie üblicherweise NICHT darauf zugreifen sollten, wird mit dem Haken Kundenkonsignationslager dargestellt.
Anwendungsfälle sind: Beistellware vom Kunden, Ware die Sie für Ihren Kunden erzeugt haben, ihm schon verkauft und verrechnet haben, aber diese Ware noch in Ihrem Gebäude / Verwaltung ist.

Ware die Ihnen gehört und beim Lieferanten liegt, ist in Ihrem Eigentum. Daher ist das Lieferantenlager KEIN Konsignationslager.

#### Können Lager abgeschaltet / gelöscht werden?
Siehe bitte oben, versteckt.

#### Es wurden neue Lager definiert, ich kann diese aber nicht auswählen?
Nach der Definition von neuen Lagern **müssen** diese auch für den jeweiligen Benutzer freigeschaltet werden. [Siehe dazu bitte]( {{<relref "/docs/stammdaten/benutzer/#auf-welche-lager-darf-ein-benutzer-buchen" >}} ).

#### Lagerart Sperrlager, Schrottlager. Wie wird das richtig verwendet?
Da doch immer wieder Fragen auftauchen wie man das Sperrlager verwenden sollte und auch was der Unterschied zum Schrottlager ist, hier unsere Überlegungen zur Verwendung dieser Lagerarten.<br>
Das Sperrlager ist im wesentlichen für die Qualitätssicherung gedacht. Hier kann, durch die Rollenrechte angegeben werden, wer (die QS) auf das Sperrlager Zugriff hat.
Andererseits ist es auch so, dass Ware die im Sperrlager ist, (noch) nicht als defekt zu betrachten ist, sondern sie ist eben noch nicht, z.B. von der QS beurteilt worden. Daher betrachten wir die Ware im Sperrlager als grundsätzlich verfügbar. Es sollte damit auch die QS dazu angehalten werden, Artikel die im Sperrlager sind, rasch zu prüfen und freizugeben, oder sich zu entscheiden, dass diese Artikel eben nicht den Qualitätsanforderungen genügen.<br>
Ähnliches gilt für das Schrottlager.<br>
Das bedeutet, dass im Artikel Detail im Feld Lagerstand die Summe aller lagernden Mengen dieses Artikels über alle Lager angezeigt wird.<br>
Die Info über den Lagerstand des Sperr- / bzw. Schrott-Lagers wird nur in entsprechenden Auswertungen berücksichtigt / ausgegeben.<br>
Sollten Sie in den verschiedenen Listen / Formularen die Angabe der z.B. am Sperrlager lagernden Menge benötigen, so kann dies gerne im Formular selbst implementiert werden. Bitte wenden Sie sich an Ihren **Kieselstein ERP** Betreuer.

Bitte beachten Sie für den Bestellvorschlag, die interne Bestellung die Bedeutung von bei Bestellvorschlag berücksichtigen bzw. bei interner Bestellung berücksichtigen.
Die für Sie richtige Übereinstimmung mit der Lagerart, muss vom Anwender entsprechend durchgeführt werden. Siehe oben.

#### Welche Lager werden für den Lagerstand in der Artikelauswahlliste herangezogen?
Hier werden die Lagerstände aller Lager ausgenommen den Konsignationslagern herangezogen.
Gibt es in Ihrer **Kieselstein ERP** Installation mehrere Mandanten und einen gemeinsamen Artikelstamm, so entscheidet zusätzlich die Zusatzfunktion [Getrennte Lager]( {{<relref "/docs/stammdaten/artikel/mehrmandanten/#zentraler-artikelstamm-und-getrennte-lager(" >}} ) ob die Lager der unterschiedlichen Mandanten eigenständig oder gemeinsam betrachtet werden.

#### Lagerstand bereits verrechneter Ware
<a name="Lagerstand bereits verrechneter Ware"></a>
Die Frage des Anwenders war, wie mit Ware umgehen, die für den Kunden zwischengelagert wird und auf Zuruf / Anforderung des Kunden ausgeliefert wird.<br>
D.h. es wird für den Kunden die Jahresmenge eingekauft und in Ihrem Hause auf Lager gelegt. D.h. Sie verrechnen diese Einlagerung und andererseits schreiben Sie für die Lieferung aus dem KundenKonsignationslager entsprechende Lieferscheine. Somit haben Sie jederzeit einen Überblick wieviel noch da ist, obwohl Ihnen die Ware nicht mehr gehört.

Wie geht das nun im Detail?
-   Legen Sie bitte ein neues Lager an. Wichtig ist, dass Sie dafür das Lager als Konsignationslager kennzeichnen.
    Bitte denken Sie daran, den Benutzern / Rollen die entsprechenden Berechtigungen für das neue Lager zu geben (Modul Benutzer, Systemrolle, Reiter Lager)
-   Sie kaufen nun die Jahresmenge des Artikels und buchen es in Ihr Hauptlager! ein
-   Nun schreiben Sie einen sogenannten Ziellagerlieferschein, gerne mit Bezug zum Kundenauftrag
    (Lieferschein aus Auftrag. Dann sofort, also bevor Sie Positionen erfassen, in den Reiter Kopfdaten und als Ziellager das gewünschte Kundenlager angeben)
    Damit wird die Ware vom Hauptlager in das Ziellager = Kundenlager umgebucht
-   Diesen Lieferschein können Sie direkt an den Kunden verrechnen, also die gesamte Jahresmenge
-   Am Kundenlager liegt die Ware die Ihrem Kunden gehört.
    Liefern Sie nun, im Auftrag des Kunden, Ware aus dem Kundenlager aus, so schreiben Sie einen Lieferschein, dessen Abbuchungslager auf das Kundenlager zugreift (und nicht auf das Hauptlager)
-   Diesen Lieferschein erledigen Sie manuell, z.B. wenn der Kunde bestätigt hat, dass die Ware angekommen ist.
    Der Vorteil ist, dass Sie damit jederzeit wissen wieviel Ware im Hause ist, die dem Kunden gehört und<br>
    es ist dies auch ein Inventur/Bilanz/Versicherungsthema.<br>
    D.h. in der Inventur, darf dieses Material nicht mehr aufscheinen, da es ja bereits an Ihren Kunden verkauft ist, aber<br>
    für die Versicherung Ihres Lagers müssen Sie wissen welche Werte auch in Ihren Kundenlagern liegen, Sie haben ja die Verantwortung dafür.

Für manche Anwender werden in diesen Fällen die am Kundenlager eingelagerten Artikel mit speziellen Kundenetiketten / Dokumenten gekennzeichnet. Sollte in dieser Richtung Bedarf bestehen, so wenden Sie sich vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

Anmerkung:<br>
In Verbindung mit der Fertigung und der Verwendung von Beistellteilen des Kunden gehen Sie in gleicher Weise vor. Definieren Sie aber im Los bzw. schon in der Stückliste, dass sich das Los nur vom Kundenlager bedienen darf. [Siehe dazu]( {{<relref "/warenwirtschaft/stueckliste/#abbuchungsl%c3%a4ger" >}} ) und auch [Kundenmaterial verarbeiten]( {{<relref "/fertigung/losverwaltung/#kundenmaterial-verarbeiten" >}} )

#### Was wird bei Materialentnahme auf das jeweilige Los zuerst abgebucht.
Das hängt von der Einstellung des Loses ab. [Siehe dazu bitte]( {{<relref "/fertigung/losverwaltung/#l%c3%a4ger-eines-loses" >}} ).

#### Wenn ich einen neuen Auftrag anlege und in diesen Artikel eintragen will, kommt die Meldung: "Für diesen Artikel wurde kein Einzelverkaufspreis hinterlegt". Wo lege ich den Einzelverkaufspreis im Artikelmodul an?
In der Artikelverwaltung muss bei dem jeweiligen Modul der Einzelverkaufspreis hinterlegt, für diesen Zeitraum, abgespeichert sein. Die Verkaufspreise werden anhand der Verkaufspreisbasis und den sich darauf beziehenden Verkaufsrabatten, welche auch negativ (Aufschläge) sein können, definiert. Bitte bedenken Sie, dass alle Verkaufspreise kalenderfähig sind. [Siehe dazu auch]( {{<relref "/docs/stammdaten/artikel/verkaufspreis" >}} ).

#### Wie kann ich als Anwender die Artikeldaten, Bezeichnungen, Artikelnummern, Einkaufs- und Verkaufspreise automatisch einpflegen.
Dies ist teilweise Branchen abhängig. Z.B. in der Elektrotechnik gibt es in der Öffentlichkeit bekannte sogenannte Brutto-Brutto Preise. Das ist der Preis den der Häuslbauer an den Elektriker bezahlt. Beim Import (z.B. Excel usw.) wird der Listenpreis (=Brutto-Brutto) als Einzel(Einkaufs)preis und als Verkaufspreisbasis übernommen. Dazu wird die Lieferantenrabattgruppe ebenfalls übernommen. Über die Verkaufsrabattgruppen oder Artikelgruppen werden die Verkaufsrabatte gesteuert, über die Lieferantenrabattgruppen die Einkaufspreise.<br>
Siehe dazu auch [Importfunktion](#import-von-artikeldaten).

#### Kann man die Lieferantenpreise automatisch erhöhen?
<a name="Preise extern pflegen"></a>
[Siehe bitte]( {{<relref "/docs/stammdaten/lieferanten#einkaufspreise-automatisch-erh%c3%b6hen" >}} )

#### Was ist der Lief1Preis?
<a name="Lief1Preis"></a>
Der sogenannte Lief1Preis ist der Einkaufspreis Ihres bevorzugten Lieferanten. D.h. es ist dies der in der Sortierung der Artikellieferanten oben, an erster Stelle, stehende Lieferant Innerhalb des Gültigkeitsdatums. Bitte beachten Sie, dass beim zentralen Artikelstamm, diese Definition Mandantenübergreifend wirkt.<br>
Um diese abzuschalten, also den Lief1Preis beim zentralen Artikelstamm trotzdem, entgegen einer Konzerndenke, für jeden Mandanten eigenständig zu betrachten, definieren Sie bitte den Parameter GETRENNTE_EINKAUFSPREISE mit 1.
[Siehe dazu auch]( {{<relref "/verkauf/rechnung/preise_in_der_rechnung/#einkaufspreis-lief1preis" >}}

#### Kann ich Preise auch extern Pflegen?
Oft ist es so, dass ein bestimmter Bereich des Artikelstammes in seinen Preisen verändert werden sollte. Auslöser sind oft Änderungen der Lieferantenpreise. Hier wird von einigen Anwendern folgende Vorgehensweise bevorzugt:
1. Pflegen der Lieferantenpreise, siehe dazu Modul Lieferant, Preispflege.
2. Exportieren einer Preispflege-XLS Datei aus dem Artikelstamm, Pflege, Preispflege-Export.<br>In dieser Datei sind zur Info auch die Einkaufspreise Ihres bevorzugten Lieferanten, die aktuellen Gestehungspreise usw. enthalten.
3. Bearbeiten Sie die Verkaufspreise dieser Datei in einem Spreadsheet Programm Ihrer Wahl, z.B. LibreOffice
4. Rückimport der Preispflege XLS Datei und damit Aktualisierung der Verkaufspreise
Bitte beachten Sie, dass die Struktur der XLS Datei **nicht** verändert werden darf.

**Hinweis im Zusammenhang mit Materialzuschlägen wie z.B. Kupferzuschlägen.** Wurden für Artikel Materialzuschläge mit Börsekursen definiert, so sind die Einkaufs- und Verkaufspreise <u>**IMMER**</u> ohne Materialzuschlag (Kupfer), weil der Materialzuschlag über das Materialgewicht und den Börsenkurs täglich anders ist. Wichtig ist hier, dass die Lieferanten den Kupferbasiskurs (130 oder 150 je nach Lieferant / Land) bekanntgeben UND den tatsächlichen Einkaufspreis. Wir haben schon öfter erlebt, dass Lieferanten nur die Preise inklusive Materialzuschlag bekannt geben und sich so höhere Preise (weil das Kupfer so teuer ist) herausholen wollten.<br>
Das bedeutet natürlich auch, dass in der Preispflege bei den Lieferantenpreise NUR die Preise ohne Materialzuschlag eingepflegt werden dürfen.

#### Wo können Verkaufspreislisten definiert werden?
<a name="Preisliste definieren"></a>
Wählen Sie im Artikel unterer Modulreiter Preisliste. Hier können nun neue Preislisten angelegt werden, bestehende Preislisten umgereiht bzw. deaktiviert oder aktiviert werden.
Bitte beachten Sie, dass die Anzeige der geänderten Preislistendefinition nur nach einem Neustart des Artikelmoduls aktualisiert wird. Aus Platzgründen ist die Anzeige der aktiven Verkaufspreislisten auf 10Listen eingeschränkt.
Wir hier ein Standard Rabattsatz definiert, so wird, immer beim Bearbeiten der Verkaufspreise dieser Rabattsatz vorgeschlagen.
Bitte beachten Sie, dass neue Rabattsätze, ev. auch neuer Preislisten, nicht automatisch bei allen Artikeln eingetragen werden.

#### Wann genau ist der Moment, dass ein (bestellter) Artikel auf Lager ist?
Ein Artikel wird mit der Lagerzubuchung auf Lager gelegt. Er ist solange auf Lager, bis er wieder entnommen wird.<br>
D.h. durch die Betätigung des Speicherns wird die Lagerbewegungsbuchung gestartet. Konnte sie durchgeführt werden, so wurde, in diesem Moment der Artikel zugebucht. Konnte sie nicht durchgeführt werden, so wird der gesamte Vorgang aufgerollt und der Zustand wieder hergestellt, wie er vor der Buchung war (Transaction Rollback).<br>
Beispiel: Wenn die Menge in einer Wareneingangsposition eingegeben wird und danach speichern gedrückt wird, wird exakt diese Menge auf Lager gelegt. Aber: Wurde nur die Menge verändert, so wird natürlich die Differenzmenge auf Lager gebucht. Dies kann auch eine Abbuchung sein. In diesem Fall wird vorher geprüft, ob die Menge dieser Zubuchung noch tatsächlich verfügbar ist.

#### Wieso kann ich in der Artikelnummer keine Leerzeichen eingeben.
Da die Leerstellen bei den Ausdrucken (z.B. Artikel-Etikette) optisch nicht erkannt werden können, sind diese auch bei der Eingabe nicht erlaubt. Zusätzlich sollten die Artikelnummern nur Zeichen enthalten, die auch in den üblichen Barcodes (Code 39)  mit ausgedruckt werden können.

#### Welche Zeichen können für die Artikelnummer verwendet werden?
<a name="Erlaubte Zeichen in der Artikelnummer"></a>
Die erlaubten Zeichen für die Artikelnummer können im System, Parameter, ARTIKELNUMMER_ZEICHENSATZ definiert werden. Bitte beachten Sie in jedem Fall ob die gewünschten Zeichen auch mit dem bei Ihnen verwendeten Barcode gedruckt werden können. Siehe dazu auch [Drucken von Barcodes]( {{<relref "/docs/installation/11_barcode" >}} ).

#### Wie organisiere ich meine Läger, meine Lagerplätze?
Es hat sich in der Praxis bewährt, wenn die Lagerplätze nach einem Koordinatensystem beschriftet werden.

Also z.B. A/II/30/d wobei die verschiedenen Stellen folgende Bedeutung haben.

A .... die Kennzeichnung des Regals

II .... die laufende Nummer der Regalreihe in einem Regal

30 ... der Regalboden, hier so organisiert, dass einfach der Abstand des Regalbodens vom Gebäudeboden gemessen wird, was den Vorteil hat, dass man jederzeit Zwischenböden einziehen kann.

d .... eine eventuell erforderliche zusätzliche Unterteilung innerhalb des Regalfaches.

![](Lagerort.jpg)

Der Vorteil ist, dass damit von jedem Mitarbeiter die Artikel sehr rasch gefunden werden können und da alle relevanten Listen mit der Lagerplatz Information versehen werden können und diese auch danach sortiert werden können und so eine einfache Optimierung der Wege erreicht wird.<br>
Denken Sie bei der Organisation des Lagers auch immer an die Wege Ihrer Mitarbeiter und an die optimale Ausnützung des zur Verfügung stehenden Platzes.<br>

#### Wo können Lagerplätze definiert werden?
Im  **Kieselstein ERP** werden die Lagerplätze organisiert verwaltet. D.h. es können den Artikeln nur Lagerplätze gegeben werden, welche vorher von berechtigten Benutzern definiert wurden. Dies hat den Vorteil, dass eine klare Struktur, z.B. wie oben beschrieben, eingefordert wird.

Die Lagerplätze jedes Lagers werden im Artikel, unterer Modulreiter Grunddaten, oberer Modulreiter Lagerplatz definiert. Legen Sie hier die Lagerplätze je Lager an.

Im Artikel, oberer Modulreiter Lagerplatz kann nun für jeden Artikel ein Lagerplatz je Lager definiert werden.

Bitte beachten Sie den Unterschied zwischen der Zuordnung eines Artikels zu einem (anderen) Lagerplatz und die Änderung der Bezeichnung des Lagerplatzes. Wird die Bezeichnung des Lagerplatzes geändert, so werden alle Lagerplätze der Artikel die auf diesen Lagerplatz verweisen verändert.

Lagerplätze können auch in den Modulen Handlagerbewegung und Wareneingangsposition erfasst werden. Hier steht zusätzlich die Möglichkeit des Löschens eines Lagerplatzes zur Verfügung.

#### Können Lagerplatz Etiketten gedruckt werden?
Ja. Gehen Sie im Modul Artikel, unterer Modulreiter Grunddaten, oberer Modulreiter Lagerplatz.

Wählen Sie den gewünschten Lagerplatz aus und klicken Sie auf das Druckersymbol.

#### Lagermindestbestand <-> Lagersollbestand wozu brauch ich das?
<a name="Lagermindeststand"></a>
Diese Schwellwerte sind für den Bestellvorschlag wichtig. Wenn bei der verfügbaren Menge des / der Läger der Lagermindestbestand unterschritten wird, wird auf den Lagersollbestand aufgefüllt. Das bedeutet für Sie, dass damit gesteuert werden kann, wie oft eine Bestellung ausgelöst wird, aber auch wie groß die jeweilige Bestellung dieses Artikels ist, um immer ausreichen auf Lager zu haben, aber auch um eine entsprechend günstige Anzahl an Bestellungen zu haben.<br>
Für die Definition von Lagermindest- und Lagersollständen je Lager kann der Parameter LAGERMIN_JE_LAGER gesetzt werden. Bitte beachten Sie dazu auch die [Standortverwaltung]( {{<relref "/einkauf/bestellung/standorte" >}} ).

#### Kann der Lagermindeststand importiert werden?
Wenn der Lagermindeststand je Lager definiert ist, so können die Lagermindeststände je Lager importiert werden.<br>
Sie finden dazu im Menü Artikel, XLS-Import Lagermindest/-sollstand.<br>
![](Lagermindeststand_Import.gif)<br>
Der Dateiaufbau ist wiederum so, dass die Spaltenüberschriften definieren welche Spalte dies ist.<br>
Es können / müssen folgende Spalten definiert sein:<br>
Artikelnummer, Lager, Mindeststand, Sollstand<br>
Artikelnummer und Lager müssen definiert sein, Mindeststand und Sollstand können definiert sein.<br>
Die angegebenen Artikelnummern und die angegebenen Lager müssen vorhanden sein. Bei Mehrmandantensystemen erfolgt der Import im jeweiligen Mandanten.

#### Fertigungssatzgröße
Für verschiedene Fertigungsabläufe ist es von Vorteil, wenn immer feste Größen produziert werden. D.h. ergibt die Bewegungsvorschau, dass der Lagermindestbestand unterschritten wird, so wird in aller Regel auf den Lagersollbestand aufgefüllt. Ist eine Fertigungssatzgröße definiert, so wird ein ganzzahliges Vielfaches dieses Wertes in der Internen Bestellung der Losverwaltung vorgeschlagen.

#### maximale Fertigungssatzgröße
Es ist oft nicht sinnvoll / möglich Lose mit sehr großen Stückzahlen zu fertigen. D.h. oft wird z.B. die mögliche Tages- / Wochenproduktion als sinnvolle Losgröße herangezogen.
Um nun im internen Bestellvorschlag dies automatisiert abbilden zu können, kann in diesem Feld die gewünschte maximale Fertigungssatzgröße definiert werden.
Das bedeutet: Wenn eine neue interne Bestellung erzeugt wird, werden nur mehr Stücklisten mit einer Losgröße <= der maximalen Fertigungssatzgröße angelegt, bzw. ausgesplittet.
Die gilt auch, wenn man manuell einen neuen Eintrag in die interne Bestellung hinzufügt.
Beim verdichten werden nun nur mehr die Einträge verdichtet, deren Mengen zweier Zeilen größer der max. Fertigungssatzgröße wäre.
So würden zum Beispiel bei einer max. Fertigungssatzgröße von 50 und zwei Einträgen mit jeweils 35, die beiden Einträge bestehen bleiben.

#### externer Arbeitsgang
<a name="externer Arbeitsgang"></a>
Im Reiter Bestelldaten kann ein Tätigkeits-Artikel, also ein Arbeitszeit-Artikel, auch als externer Arbeitsgang gekennzeichnet werden.
Dies bewirkt zwei Dinge:
- a.) Ist dies ein Zukaufsartikel für eine Fremdfertigung, so bewirkt dies, dass durch Zubuchung des Materials, also der Lohnveredelung, das jeweilige Los fertig ist. Somit wird, wenn die Position als Wareneingang gebucht wird, gleichzeitig die Ablieferung des Loses gebucht. [Siehe dazu auch]( {{<relref "/fertigung/losverwaltung/fremdfertigung/#externer-arbeitsgang" >}} ).
- b.) Ist dieser Artikel die Tätigkeit der Fremdbearbeitung selbst (z.B. Beschichten) so wird bei der Ausgabe des Loses geprüft, ob es für diese Tätigkeit auch eine Zuordnung zu einer Materialposition gibt. Ist diese Zuordnung nicht gegeben, so erscheint bei der Ausgabe des Loses ein entsprechender Hinweis.<br>
![](Fremdarbeitsgang_ohne_Material.jpg)

Bei der Kennzeichnung des externen Arbeitsganges<br>
![](externer_Arbeitsgang.gif)<br>
kann auch aktiviert werden, dass die sogenannte Sollsatzprüfung bei einer Erledigung am Terminal NICHT durchgeführt wird. D.h. wenn Sie z.B. Transportkosten als externen Arbeitsgang mit in das Los aufnehmen (müssen), so werden diese in der Regel ja erst nach Fertigstellung des Produktes zugebucht. Daher darf am Terminal bei der Ablieferbuchung diese Materialposition, also der externe Arbeitsgang, nicht geprüft werden. Somit können von der Fertigung die Ablieferbuchungen gemacht werden, es bleibt jedoch die Fehlmenge auf einem derart gekennzeichneten Arbeitsgang erhalten.

##### Anzeige externer Arbeitsgang nicht verfügbar
Steht im der Artikel des externern Arbeitsganges, der ja nur reine Dienstleistung ist, die Auswahl des externern Arbeitsganges ![](Externer_Arbeitsgang_fehlt.png)  nicht zur Verfügung, so ist der Artikel Chargen oder Seriennummern geführt.<br>
Beachte: **Ein externer Arbeitsgang darf keine Chargenführung beinhalten.**

#### WEP-Info an Anforderer?
Sende ein EMail an den Anforderer der Bestellung wenn ein Wareneingang gebucht wird. [Siehe dazu]( {{<relref "/einkauf/bestellung/#bei-einem-bestimmten-artikel-sollte-der-anforderer-informiert-werden-dass-die-ware-gekommen-ist">}} )

#### gemeinsame Betrachtung je Bestellung
Wenn dieser Artikel mehrfach in einer Bestellung angeführt ist, so wird die Gesamtmenge (dieser Bestellung dieses Artikels) für die Preisfindung, vor allem aus der Einkaufsstaffel herangezogen und als Bestellpreis vorgeschlagen. Zusätzlich wird der Bestellpreis dieses Artikels, auch bei manuellen Anpassungen, für alle Positionen dieses Artikels dieser Bestellung immer auf den gleichen Wert gestellt. [Siehe]( {{<relref "/einkauf/bestellung/#es-werden-gleiche-artikel-zu-unterschiedlichen-lieferterminen-bestellt-man-will-aber-den-besseren-preis-bekommen" >}} ).

#### Detailprozentmindeststand
Ist in Ihrer Installation auch die Berechnung des Rahmendetailbedarfes freigeschaltet, so finden Sie im Reiter Bestelldaten neben dem Lagermindeststand auch den Detailprozentmindeststand. Wird hier ein Prozentfaktor eingegeben, der auch größer 100% sein kann, so wird anschließend an die Berechnung des Rahmendetailbedarfes der Lagermindeststand auf den ganzzahligen Wert der sich aus dem Rahmendetailbedarf gerundet auf eine ganze Zahl ergibt gesetzt.<br>
![](DetailProzentMindeststande.gif)<br>
Wird anschließend der Bestellvorschlag erzeugt, so wird der sich aus dem Rahmendetailbedarf ergebende Lagermindeststand für die Berechnung herangezogen.<br>
Achten Sie bei der Verwendung der Automatikjobs darauf, dass der Rahmendetailbedarf vor der Errechnung des Bestellvorschlages durchgeführt wird.<br>
D.h. hier wird der Lagermindeststand bei jedem Lauf der Berechnung des Rahmendetailbedarfes, z.B. täglich, automatisch überschrieben.<br>
Um diese Funktion für den einzelnen Artikel abzuschalten, entfernen Sie bitte den Eintrag unter Detailprozentmindestsatz.

#### Wozu dient die Materialangabe unter Technik?
<a name="Materialzuschlag"></a>
Verschiedene Artikel beinhalten Materialien, welche zum Zeitpunkt des Verkaufes zum Börsenpreis gehandelt werden. Zusätzlich kann das Material als Leistungsfaktor für die Maschinen verwendet werden.

Weitere Details siehe bitte [Materialzuschlag]( {{<relref "/docs/stammdaten/artikel/materialzuschlag" >}} ).

#### Wie pflegt man die EK-Preise richtig?
In **Kieselstein ERP** können beliebig viele Lieferanten für einen Artikel definiert werden.
Definieren Sie im Modulreiter Artikellieferant die Lieferanten des Artikels. Sie können hier für jeden Lieferanten die Artikelnummer und die Bezeichnung des Artikels beim Lieferanten hinterlegen. Zusätzlich kann hier der Preis, inkl. Rabatt und Netto-Preis für den Einzelartikel hinterlegt werden. Für Staffelmengen steht der Reiter EK-Staffelpreise zur Verfügung. Hier werden die Staffelpreise für den ausgewählten Lieferanten eingetragen. Bitte beachten Sie, dass die Preisbasis der Einzelpreis des Artikellieferanten ist. Im Modulreiter EK-Staffelpreise kann daher dieser Preis nicht verändert werden. Die Staffelpreise können sowohl über die Rabatte als auch über die Nettopreise gepflegt werden. Die Pflege über die Rabatte hat den Vorteil, dass nur der Einzelpreis verändert werden muss. Die Staffelpreise ändern sich damit automatisch mit. Die Preise werden in der Lieferantenwährung eingepflegt.
Bitte beachten Sie dazu auch die automatische Preisrückpflege durch das Bestellmodul.
Zusätzlich [siehe bitte](#kann-man-die-lieferantenpreise-automatisch-erhöhen).

**Hinweis:**<br>
Um einen neuen Artikellieferanten anlegen zu können, ist auch das Recht LP_DARF_PREISE_SEHEN_EINKAUF erforderlich.

#### Wie definiert man Verpackungseinheiten eines Lieferanten?
Im Artikellieferant kann nun, wenn keine Bestellmengeneinheit in den Kopfdaten definiert ist, eine Verpackungsmengeneinheit ausgewählt werden, welche als Umrechnungsfaktor die Verpackungseinheit verwendet. So kann zum Beispiel abgebildet werden, dass 1 Verpackung gekauft wird und nicht 100Stk., die einer Packung entsprechen.
[Siehe dazu auch]( {{<relref "/einkauf/bestellung/#wir-bekommen-die-artikel-st%c3%bcckweise-angeliefert-verwalten-diese-aber-in-meter-und-haben-die-preise-in-m-kg-wie-geht-das" >}} ).

#### Wie definiert man Verpackungseinheiten für die Fertigung?
Im Reiter Bestelldaten finden sie das Feld Fertigungs-VPE ![](Fertigungs_VPE.JPG) geben Sie hier die Verpackungseinheit ein.

Wenn Sie hier zum Beispiel 2 eintragen und im Los wird eine Menge des Artikels von 2,1 benötigt, so wird die Menge auf die VPE aufgerundet. Das bedeutet, dass die Menge im Los auf ein Vielfaches der Fertigungsverpackungseinheit gerechnet wird, in diesem Beispiel auf 4\. Verwendet wird diese Eingabe zum Beispiel für Schachteln oder andere Verpackung.

#### Wie können von bis Mengen für die Staffelmengen definiert werden?
Im Reiter Staffelpreise bzw. ist für die Definition auch immer die Menge erforderlich. Das bedeutet, dass dieser Preis ab dieser Menge gilt.

Ein Beispiel:

| Staffelmenge   | Staffelpreis |
| --- |  --: |
| 10 Stk | 10,00 € |
| 25 Stk |   8,50 € |

Das bedeutet: Ab 10,000 Stk bis zu 24,999 Stk gilt der Preis von 10,- €. Ab 25 Stk gilt der Preis von 8,50 €.

Die Anzahl der Mengenstaffeln ist beliebig, sollte natürlich in einem für Sie überschaubaren Maße sein.

#### Standardmenge, Mindest Bestellmenge, Verpackungseinheit, Wiederbeschaffungszeit
<a name="Mengen des Artikellieferanten"></a>
![](ArtikellieferantMengen.gif)

Jeder Lieferant hat üblicherweise seine eigenen Verpackungseinheiten und Lieferzeiten. Daher können in **Kieselstein ERP** diese Daten je Artikellieferant definiert werden.

| Feldname | Beschreibung |
| --- |  --- |
| Standardmenge | Diese Menge wird beim Anlegen einer neuen Bestellposition dieses Artikels bei diesem Lieferanten vorgeschlagen |
| Mind.Bestellmenge | Wird die mindest Bestellmenge bei einer Bestellposition unterschritten, so erscheint eine entsprechende Warnung |
| Verpackungseinheit | Ist die bestellte Menge kein ganzzahliges Vielfaches der Verpackungseinheit, so erscheint eine entsprechende Warnung |
| Wiederbeschaffungszeit | Definieren Sie hier, wieviele Wochen Ihr Lieferant für die Lieferung dieses Artikels benötigt. Diese Werte werden in der Wiederbeschaffungsanzeige im Auftrag verwendet und im Bestellvorschlag um die später bestellbaren Positionen entfernen zu können. |

#### Wie soll die Wiederbeschaffungszeit erfasst werden?
Geben Sie bei der Wiederbeschaffungszeit nur dann eine Zeit (Tage/Kalenderwochen) ein, wenn Ihnen diese tatsächlich bekannt ist. Ansonsten belassen Sie diese bitte leer, womit Sie in den Auswertungen sehen, bei welchen Artikeln keine Wiederbeschaffungszeiten definiert sind. Geben Sie vor allem realistische oder eher pessimistische Zeiten an. Lieber eine Woche zu früh bestellt.<br>
Da sich oft diese Zeiten auch aus den tatsächlichen Beschaffungen ergeben, kann im Artikelmodul, Menüpunkt Pflege, ![](Pflege_Wiederbeschaffung.gif) die Wiederbeschaffungszeit anhand der letzten Bestellungen aktualisiert werden. Für die Berechnung der Wiederbeschaffungsmoral werden die Parameter
WIEDERBSCHAFFUNGSMORAL_ANZAHL_BESTELLUNGEN und WIEDERBSCHAFFUNGSMORAL_MONATE herangezogen. D.h. es werden maximal die letzten Anzahl Bestellungen berücksichtigt, aber nicht weiter wie Monate zurück.<br>
Zusätzlich wird die Wiederbeschaffungszeit für den jeweiligen Artikellieferanten
![](WBZ.gif)als Ø (Alt+157) WBZ: angegeben.

#### Kann ein Link auf die Webseite des Artikels des Lieferanten hinterlegt werden?
Geben Sie neben dem Artikellieferanten den Link auf das Web-Dokument dieses Artikel dieses Lieferanten an. Damit haben Sie jederzeit Zugriff auf diese Daten, solange diese von Ihrem Lieferanten unter der abgespeicherten Adresse zur Verfügung gestellt werden.

#### Wo sehe ich eine kompakte Übersicht über die Einkaufspreise eines Artikels?
Bitte nutzen Sie Info, Lieferantenpreisvergleich. Hier bekommen Sie eine Liste aller Artikellieferanten dieses Artikels mit allen entsprechenden Mengenstaffeln.
Der je Mengenstaffel günstigste Preis und die schnellste Wiederbeschaffungszeit sind grün hinterlegt

![](Lieferantenpreisvergleich.jpg)

#### Kann ein Einkaufs-Preisentwicklung eingegeben werden?
Gerade um den Jahreswechsel aber auch zu anderen Gelegenheiten ist es immer wieder erforderlich, dass Einkaufspreise vor allem für die Zukunft definiert werden. Mit **Kieselstein ERP** können Sie mit unterschiedlichen Gültig Ab Terminen diese zeitliche Entwicklung darstellen.

Geben Sie daher die Einkaufspreise analog zu den angeführten an.

![](Artikellieferant_mehrere_Einkaufspreise.gif)

#### Wo sieht man die Entwicklung der Einkaufspreise eines Artikels?
Das hängt davon ab, was man darunter versteht und auch wo die Daten gepflegt werden.
a.) Die Entwicklung der echten Einkaufspreise, also der Waren die Sie tatsächlich bei Lieferanten bezogen haben
Diese sehen Sie in der Artikelstatistik, gegebenenfalls wird die Anzeige auf den Bereich Einkauf eingeschränkt.
![](Einkaufspreisentwicklung_aus_der_Artikelstatistik.jpg)

b.) Die verschiedensten angefragten Artikel, also die Angebote die von den verschiedenen Lieferanten gelegt wurden.
Siehe dazu, ebenfalls im Artikel, Info, Anfragestatistik
![](Einkaufspreisentwicklung_anhand_der_Anfragen.jpg)
Hier wird zusätzlich der günstigste Preis und die kürzeste Lieferzeit in grün angezeigt.

c.) Die manuelle Pflege der Einkaufspreise.
D.h. es werden, gerne auch für die Preise in der Zukunft, die Artikellieferantenpreise beim jeweiligen Artikel durch mehrfache Angabe des gleichen Lieferanten mit jeweils unterschiedlichem Gültig ab definiert. ![](Einkaufspreisentwicklung_anhand_der_Artikellieferantenpreise.gif)

d.) Wozu Preise in der Zukunft ?
Gerade im Projektgeschäft muss man heute Preise für seine Lieferung in 6 oder 12Monaten abgeben. Wenn Sie nun in der glücklichen Lage sind, heute schon relativ verbindliche Preise für diesen Zeitraum zu haben, stehen sowohl in der Stücklistengesamtkalkulation als auch in der Angebotsvorkalkulation Auswertungen zum geplanten Realisierungstermin zur Verfügung.

#### Bei manchen Bestellungen sollten Einkaufsrabatte aufscheinen und bei anderen nicht.
Sie können dies durch die Eingabe der Einkaufspreise steuern. Wenn bei den Einkaufspreisen Rabatte angegeben werden, so werden diese auch ausgedruckt. Wurden keine Rabatte angegeben, so werden diese nicht mitgedruckt.

#### Wie legt man Artikel in Fremdwährungen an?
Wenn Artikel in Fremdwährungen eingekauft werden, so gelten die Preisvereinbarung in dieser Währung. D.h. die Währungsschwankungen wirken sich auf Ihre Einstandspreise aus, der Preis in z.B. USD bleibt stabil.

Einkaufspreise werden in der Währung des Lieferanten definiert. D.h. um Artikeleinkaufspreise in Fremdwährungen zu definieren muss der Lieferant in der entsprechenden Währung definiert werden.

#### Wie hinterlegt man, wenn ein Lieferant einen Artikel NICHT liefern kann?
Es kommt immer wieder vor, dass Artikel bei denen man annimmt dass diese ein Lieferant liefern kann, dann von diesem doch nicht geliefert werden können, z.B. aus technologischen Gründen, weil die Spannweiter seiner Maschine zu gering ist oder ähnliches.
Damit man nun diesen Lieferanten nicht immer wieder um das gleiche frägt, kann beim Artikellieferant dieser Artikel als (von diesem Lieferanten) ![](nicht_lieferbar.gif)nicht lieferbar angehakt werden. Um eine genauere Begründung / Info mit angeben zu können, kann daneben ein entsprechender Kommentar noch eingepflegt werden. Zusätzlich wird die nicht-lieferbar-Zeile eines Artikellieferanten in Rot angezeigt.
Es bewirkt dies, dass beim Bestellvorschlag diese Lieferanten des jeweiligen Artikels nicht herangezogen werden. Werden Gruppenanfragen gemacht, so wird dieser Lieferant ebenfalls übersprungen.

#### Wie finde ich zur Anfrage zurück?
Um zur Anfrage zu springen, mit der ein Artikellieferanteneintrag gemacht wurde, verwenden Sie bitte den Anfrage GoTo Button im Artikellieferanten.
[Siehe dazu auch]( {{<relref "/einkauf/anfrage/#zur%c3%bcckpflegen-der-in-den-lieferdaten-erfassten-lieferanten-angebote" >}} )

#### Wie wird der Mindest-VK errechnet?
[Siehe]( {{<relref "/docs/stammdaten/artikel/verkaufspreis/#mindestverkaufspreis-mindest-vk-was-ist-das" >}} )

#### Was bewirken Aufschlag und Soll?
Diese dienen einer theoretischen Abschätzung von Verkaufspreisen auf Basis des Gestehungspreise. Zur Auswirkung des Aufschlags [siehe]( {{<relref "/docs/stammdaten/artikel/verkaufspreis/#tiefster-verkaufspreis" >}} ) und zur Auswirkung des Soll [siehe]( {{<relref "/docs/stammdaten/artikel/verkaufspreis/#soll--verkaufspreis" >}} ).

#### Wo werden Seriennummern verwaltet?
[Siehe]( {{<relref "/docs/stammdaten/artikel/serienchargennummern" >}} ).

#### Wo werden Chargennummern verwaltet?
[Siehe]( {{<relref "/docs/stammdaten/artikel/serienchargennummern" >}} ).

#### Wozu Dokumentenpflicht?
[![](Dokumentenpflicht1.gif) Siehe]( {{<relref "/docs/stammdaten/system/dokumentenablage#dokumentenpflicht" >}} )

#### Dokumente der Artikel verlinken
Gerade aber nicht nur in der Elektronik Branche hat man immer das Thema, dass viele faktisch gleiche Artikel angelegt werden. So ist die Spezifikation für einen 10kOhm Widerstand exakt die gleiche wir für einen 10MOhm Widerstand. Will man nun für diese Artikel nur eine Dokumentation in der Dokumentenablage hinterlegen, so wählt man einen Basis-Referenzartikel aus. Bei diesem werden die entsprechenden Datenblätter hinterlegt. Nun kann man bei den weiteren Artikeln, des gleichen Typs, im Reiter Sonstiges unter ![](Referenzartikel.png) Referenzartikel diese Querverlinkung hinterlegen. Dies bewirkt, dass in der Dokumentenablage des verlinkten Artikel eine weitere Unterebene angezeigt wird, in der die Dokumente des referenzierten Artikels sichtbar sind.

#### Was bedeutet die Fehlermeldung "Der zugehörige Artikel kann nicht gespeichert werden"?
![](Zugehoeriger_Artikel.jpg)

Diese Meldung bedeutet, dass ein Verkettungsfehler in den zugehörigen Artikeln vorliegt.

Bereinigen Sie diesen Fehler, die Verknüpfung der Dazugehörigen Artikel finden Sie unter Sonstiges im Artikel, danach können Änderungen auf diesem Artikel gespeichert werden.

#### Lagerumschlagshäufigkeit
Die Lagerumschlagshäufigkeit gibt an, wie oft ein durchschnittlicher Lagerbestand eines Produktes in einem bestimmten Zeitbereich komplett aus dem Lager entnommen wurde. Er wird wie folgt ermittelt:

                                Lagerabgang im Zeitraum

Umschlagshäufigkeit = ----------------------------

                              durchschnittlichen Lagerbestand

Der durchschnittliche Lagerbestand wird vereinfacht durch das arithmetische Mittel aus Anfangs- und Endbestand ermittelt. D.h. also

                                                Anfangsbestand + Endbestand

durchschnittlicher Lagerbestand = --------------------------------

                                                                    2

Die durchschnittliche Lagerdauer ergibt sich, indem man das Jahr mit 360Tagen ansetzt und durch die Lagerumschlagshäufigkeit dividiert.

(Frei nach Wikipedia)

#### Materialzuschläge auf Wareneingängen / Warenausgängen, welche Preise sind zu buchen?
Das Ziel der Definition der Materialzuschläge ist, die Kursschwankungen der Börsennotierten Materialien, wie z.B. Kupfer oder spezielle Legierungen, in der Form abzufangen, dass die zusätzlichen Materialkosten an den Endkunden weiterverrechnet werden.

Die übliche Vorgehensweise ist hier so, dass die Verkaufspreise zu den standard Materialfaktoren (z.B. bei Kupfer zur Kupferzahl 100) definiert werden. Nun werden täglich / wöchentlich die Materialzuschlagsfaktoren gepflegt. Aus den hinterlegten Materialgewichten und den Materialzuschlagsfaktoren kann über die fakturierte Menge der Materialzuschlag errechnet werden. Dieser wird auf der Rechnung in einer eigenen Zeile ausgewiesen. Dadurch ergibt sich einerseits dass Sie die vereinbarten Verkaufspreise stabil belassen können und andererseits doch die Schwankungen der Werkstoffbörse an Ihre Kunden weitergeben können.

**WICHTIG:**

Bei der Buchung des Wareneinganges muss der Materialzuschlag zusätzlich zum Positionspreis angegeben werden bzw. im Positionspreis enthalten sein. Nur dadurch ist sichergestellt, dass die Gestehungspreise der Artikel den echten Kosten entsprechen.

#### Sortierung der Artikel?
Die Sortierung der Artikel nach Artikelnummern ist abhängig von der eingesetzten Datenbank.

Wird eine PostgresQL Datenbank eingesetzt, so werden Hochkomma und Bindestrich richtig einsortiert. Es werden allerdings die Umlaute am Ende des Alphabetes eingereiht.

#### Zeichnungen hinterlegen?
<a name="Zeichnungen hinterlegen"></a>
Im Artikelkommentar können sowohl Texte als auch Bilder hinterlegt werden. Bei den Kommentaren kann auch angegeben werden, bei welchen Modulen (Angebot, Fertigung, ..) diese mitgedruckt werden.

#### Zeichnungen im PDF Format hinterlegen
Gerade die Zeichnungen sind für die Fertigung von Teilen, Geräten sind sehr wichtig.
In der Regel sind diese in anderen CAD Programmen erzeugt worden.
Wählen Sie den Kommentar und den Dateityp z.B. PDF aus und ziehen Sie die PDF mit Drag & Drop in das PDF Anzeige Fenster, um die Zeichnung zuzuordnen.
Es werden die ersten 3000 Zeichen eines textlich auswertbaren PDFs in den Texten der Kommentare gespeichert und können damit mit der Kommentarsuche komfortabel durchsucht und gefunden werden.

### Artikelkommentar
<a name="Artikelkommentar"></a>

#### Artikelhinweise hinterlegen?
Im Artikelkommentar kann zwischen ![](Hinweis_Artikelauswahl.gif) gewählt werden.

-   Mitzudrucken bei
    Mitzudrucken bei bedeutet, dass dieser Kommentar bei den gewählten Modulen mit ausgedruckt wird.
    Es können dies sowohl Texte als auch Bilder sein.
    Bitte beachten Sie, dass jeweils nur das chronologisch erste Bild, der chronologisch erste Text ausgedruckt wird.
    Für Fertigungsbegleitscheine kann das PDF mitgedruckt werden, die Druckauflösung kann hierzu eingestellt werden - wenden Sie sich dazu bitte an Ihren **Kieselstein ERP** Berater.

-   Hinweis bei Artikelauswahl
    Wird Hinweis bei Artikelauswahl gewählt, so wird damit definiert, dass bei Verwendung des Artikels im angegebenen Modul der eingegebene Text in einer Hinweismeldung erscheinen soll. Damit können Sie spezifische Nachrichten an den Benutzer bei der Auswahl exakt dieses Artikels, z.B. bei der Auswahl in einer Bestellung senden.
    Sind hier Bilder, durchaus mehrere, hinterlegt, so werden diese dem Benutzer präsentiert und er kann durch diese durchblättern.

-   Anhang
    Mit der Einstellung Anhang wird definiert, dass beim EMail-Versand die angefügte Datei als Anhang mit gesandt wird.
    Das impliziert, dass diese Einstellung nur beim EMail-Versand funktioniert

Weiters können Sie hier auch Kommentare definieren, die in der Artikelauswahl als Tooltip angezeigt werden. Legen Sie dazu in den Artikel - Grunddaten eine neue Kommentarart mit einem sprechenden Namen an und setzen den Haken bei ![](tooltip_artikelauswahl.JPG) Tooltip in der Artikelauswahlliste. Nun können Sie bei den gewünschten Artikeln einen neuen Kommentar mit dieser Kommentarart hinzufügen. Wenn Sie nun in der Artikelauswahlliste mit der Maus über die Zeile des Artikels fahren, wird der dort eingegebenen Text als Tooltip angezeigt.
![](tooltip_artikelauswah_beispiell.JPG)
Für die Einstellung, welche Dateigröße angehängt werden kann verwenden Sie bitte den Parameter DOKUMENTE_MAXIMALE_GROESSE. Bitte beachten Sie, dass für die Art "unbekannt" eine fixe Maximalgröße von 1MB festgelegt ist.

#### Kann auch ein fester Pfad für Fertigungszeichnungen hinterlegt werden?
Wird in der Kombination, Mitzudrucken bei, Fertigung auch noch Dateiverweis mit angehakt,
![](Dateipfad_auf_Artikelkommentar.gif)
so kann der Pfad auf eine PDF Datei angegeben werden, deren Inhalt als Vorlage für die Weitergabe der PDF Daten dient. Dies wird z.B. gerne in Solidworks, Inventor, u.ä. Umgebungen verwendet, bei der die Datei mit Ihrem vollständigen Pfad, vom Server aus gesehen, auf die durch das Solid Works / Inventur, aktualisierte PDF Zeichnung zeigt. Somit wird immer die aktuelle Zeichnung mit den Fertigungspapieren mit ausgedruckt.
Bitte beachten Sie, dass für den **Kieselstein ERP** Server diese Datei zugreifbar sein muss. In Windowsumgebungen (für den **Kieselstein ERP** Server) bedeutet dies, dass der Dienst unter einem Benutzer laufen muss, ähnliches gilt für Linux bzw. MAC Umgebungen. Zusätzlich muss, gerade wenn auf Netzwerklaufwerke zugegriffen wird, dies auch vom Client aus in der UNC Notifikation angegeben werden.
Sollten die Dateien nicht angezeigt werden, so finden Sie im Server-Log entsprechende Hinweise.

#### kann man eine Datei auf den Dateiverweis ziehen?
Als weitere Version der Artikel-lang-Beschreibung, also des Artikelkommentars wird auch der Dateiverweis mit Drag & Drop unterstützt. D.h. um einen Dateiverweis anzulegen, klicken Sie im Reiter Kommentar auf Neu und wählen die gewünschte Kommentarart aus. Nun ziehen Sie die zu verlinkende Datei einfach mit der Maus in den oberen Bereich der Artikelkommentardefinition, also dem Bereich oberhalb der Art.
![](Dateiverweis_droppen.jpg)
Damit wird automatisch angenommen, dass Sie einen Dateiverweis anlegen möchten und sowohl die Verlinkung als auch Anhang und der Haken bei Dateiverweis gesetzt.

Bitte beachten Sie, dass ein Drop (fallen lassen) auf die Zeile mit Art, bzw. darunter ein Einfügen dieses Artikelkommentars bedeutet
![](Artikelkommentar_droppen.jpg)

#### Woher erkennt man, dass Artikelkommentare, Bilder, Hinweise hinterlegt sind?
Wenn Kommentare beim Artikel hinterlegt sind, egal welcher Art, wird im Reiter Detail des Artikels dies durch das Icon ![](artikelkommentar_vorhanden.JPG) angezeigt.

#### Können die Kommentare auch im Detail angezeigt werden?
Mit der Kommentarart kann definiert werden, dass bestimmte Kommentare sofort im Artikel-Detail angezeigt werden.
Setzen Sie dazu in den Artikel, Grunddaten, Kommentarart den Haken bei ![](Anzeige%20im%20Artikel-Detail.gif) anzeige im Artikel Detail.
Damit werden die Inhalte der Kommentare mit den so markierten Kommentararten, in der Reihenfolge der Kommentare und mit der Kommentarart als Überschrift angezeigt.

#### Wie kann man nach Inhalten der Kommentare suchen?
Um in den Inhalten der Artikelkommentare zu suchen nutzen Sie bitte die Kommentarsuche ![](Artikelkommentar_suchen.gif) in der Artikelauswahl.
Hier werden auch die textlichen Inhalte der PDF Dateien, soweit verfügbar mit angezeigt.

#### Anstelle der Kommentare werden im Detail Lagerstand, Verfügbar, in Fertigung angezeigt
Ist in Ihrer Installation die Funktion Herstellerverwaltung aktiviert, so wird im Detail des Artikels anstelle der Kommentare die Übersicht der Lagerstände der Ersatztypen angezeigt.

#### Können die Artikeltexte übersteuert werden?
Wenn der Mandantenparameter ARTIKELLANGTEXTE_UEBERSTEUERBAR aktiviert ist (=1), so kann im jeweiligen Modul (Angebot-Bestellung) bei klick auf Änderung der Ident spezifischen Texteingabe der Artikelkommentar des jeweiligen Modules in die Texteingabe kopiert werden. Dadurch dass es nur eine Kopie ist, können hier die gewünschten Änderungen vorgenommen werden.
Bitte beachten Sie, dass hier die Regel so definiert wurde:
Ist der in der Position erfasste Texte leer, also auch keine Leerzeichen, so wird der Text aus dem Artikelkommentar angedruckt. Ist ein Text definiert, so wird der an dieser Position eingegebene Text gedruckt. Das bedeutet auch, wenn Sie versehentlich Text eingegeben haben, der da eigentlich nicht sein sollte, so löschen Sie die gesamte Eingabe einfach heraus. Umgekehrt, wenn der Artikeltext nicht gedruckt werden sollte, so muss zumindest ein Leerzeichen eingegeben werden.

#### Bilder für den Ausdruck hinterlegen?
Der Ausdruck von Bildern wird in **Kieselstein ERP** für die Standard Bildformate wie jif, jpeg, png, tiff unterstützt. Haken Sie dazu wie unten beschrieben die entsprechenden Mitzudrucken bei an. Denken Sie daran, dass die Bilder von der Idee her kleine Abbildungen des Produktes sein sollten um Ihren Kunden / Lieferanten entsprechend zu informieren. Lediglich für die Fertigung sind auch Zeichnungen angedacht. Bilder können nur bei der Einstellung "Mitzudrucken bei" hinterlegt werden. Wenn von einem Bild die Breite größer als die Höhe ist, dann wird dies um 90 Grad gedreht.
Wird ![](Default-Bild.gif) Default-Bild mit angehakt, so wird dieses Bild in der Detailansicht des Artikels ebenfalls angezeigt.
Es kann bei Bildern ein Kommentar hinterlegt werden. Dieser Artikelkommentar wird in der Auswahlliste der Kommentare angezeigt, sodass man den Bildinhalt auch textlich beschreiben kann.

#### Wozu unterschiedliche Artikelkommentararten?
Mit den unterschiedlichen Kommentararten, diese können im Artikel unter Grunddaten, Kommentarart definiert werden, haben Sie die Möglichkeit spezifische Kommentare für den Artikel zu hinterlegen. Denken Sie hier z.B. an einen Handelsartikel. Hier muss einmal eine schöne Beschreibung für den Verkauf gemacht werden, welche den Kundennutzen in den schönsten Farben schildert. Dann muss eine Beschreibung für den Lieferanten erstellt werden. Diese muss den Artikel exakt spezifizieren, sollte aber z.B. dem Lieferanten nicht unbedingt den Kundennutzen transparent darlegen. Dann ist noch eine Beschreibung für die Wareneingangsprüfung zu erstellen. Dies hat bestimmte Messverfahren durchzuführen, Messwerte zu überprüfen usw.. D.h. wenn Sie entsprechende Artikelkommentararten definiert haben, können Sie diese Daten komfortabel in den Artikelstamm einpflegen und von **Kieselstein ERP** werden diese Daten entsprechen bei den definierten Papieren automatisch mitgedruckt.
Die selbe Vorgehensweise ist bei den Hinweisen gegeben.

#### Kann auch direkt aus der Zwischenablage einkopiert werden?
Ja. Wählen Sie dazu erst die Art des Kommentars, z.B. png, gif oder jpeg und klicken Sie dann auf dem Knopf ![](Kommentar_aus_Zwischenablage.gif) aus Zwischenablage übernehmen neben der Dateiauswahl. Damit wird der grafische Inhalt der Zwischenablage direkt eingefügt.

#### Können Dateien einfach in den Kommentar reingezogen werden?
Ja. Klicken Sie im Reiter Kommentar einfach auf Neu und ziehen Sie eine Datei mit den Endungen GIF, JPG, JPEG, TIFF, PNG, PDF direkt auf das Dialogfenster und lassen Sie die Datei dort fallen. Von **Kieselstein ERP** wird anhand der Endung der Typ erkannt und im Vorschaufenster dargestellt. Definieren Sie nun noch die Kommentarart und wo der Kommentar mitgedruckt / angezeigt werden sollte.

#### Warenverkehrsnummern?
Warenverkehrsnummern können im Artikel unter Sonstiges hinterlegt werden.

Wenn beim Klick auf ![](Warenverkehrsnummer.gif) keine Warenverkehrsnummern angezeigt werden, so wurden die Warenverkehrsnummern, welche von den Statistischen Zentralämtern zur Verfügung gestellt werden, noch nicht importiert. Siehe dazu bitte [Intrastatmeldung]( {{<relref "/management/finanzbuchhaltung/intrastat" >}} ). Es steht jedoch trotzdem die Möglichkeit zur Verfügung, die Warenverkehrsnummer bei jedem Artikel ![](Warenverkehrnummer2.gif) einzugeben. Diese werden, da nicht in der Warenverkehrsnummerntabelle enthalten, in Rot angezeigt. Sie werden jedoch trotzdem mitgedruckt.

#### ECCN?
Die Export Control Classification Number für Exportgüter für US-amerikanischen Ursprungs kann im Artikel Reiter Sonstiges über der Warenverkehrsnummer erfasst werden. 

#### Pflegefunktionen im Artikel
[Siehe bitte]( {{<relref "/docs/stammdaten/system/pflegefunktionen" >}} ).

<a name="Verschnittbasis / Verschnittfaktor"></a>

#### Verschnittbasis / Verschnittfaktor ?
Im Artikel können unter Bestelldaten Verschnittbasis und Verschnittfaktor [%] definiert werden.
Der Verschnittfaktor in % definiert um welchen Prozentsatz die Menge der Stücklistenposition bei der Überleitung der Stückliste in den Fertigungsauftrag erhöht werden soll.<br>
Die Verschnittbasis löst das Stangenproblem.<br>
Das bedeutet:<br>
Wenn in der Verschnittbasis die Länge des Stangenmaterials eingetragen wird, so wird davon ausgegangen, dass für die Überleitung der Stückliste in einen Fertigungsauftrag auf diese Länge Rücksicht genommen werden muss.<br>
Es wird die Menge so umgerechnet, dass um so viel mehr Stangen (Material) bestellt werden, dass ausreichend ganze Stangen vorhanden sind.

Ein Beispiel:<br>
Sie benötigen für eine Stücklistenposition ein Material mit 3,1m Länge. Es ist nur Stangenmaterial mit 6m verfügbar. D.h. die Verschnittbasis des Artikels ist auf 6m eingestellt. Es wird nun ein Los mit 50Stk aufgelegt.<br>
50 x 3,1 m = 155m würden ohne Verschnittbasis für dieses Los reserviert werden. Aufgrund der Verschnittbasis werden aber<br>
50 x 6m = 300m reserviert.

Ein weiteres Beispiel:<br>
Materiallänge 1,6m, Stangenmaterial 6m, Losgröße 50Stk<br>
50 x 1,6 m = 80m<br>
Aus einer 6m Stange gewinnen Sie 3,75 Stk, also 3Stk. Das ergibt, dass 17 x 6m = 102m eingekauft werden müssen.

Das bedeutet die Logik reserviert so viel, wie ganze Stangen aus dem Material zur Verschnittbasis gewonnen werden können.

#### Stangen und Tafel Bedarf berechnen
Alternativ zur Verschnittbasis gibt es auch die Möglichkeit die Berechnung anhand der im Reiter Technik definierten Abmessungen vorzunehmen.<br>
Stellen Sie dazu den Mandantenparameter VERSCHNITT_NACH_ABMESSUNG auf 1.<br>
Es bedeutet dies, dass für die Berechnung der tatsächlich benötigten Mengen auch die Dimension der Mengeneinheit des Artikel und die Abmessungen der Stücklistenposition berücksichtigt werden. Bitte beachten Sie, dass bei mehreren Positionen, die den gleichen Artikel verwenden, der Verbrauch pro Position errechnet wird.<br>
Bei der Berechnung der benötigten Menge wird bei zweidimensionalen Artikel versucht zu ermitteln wieviele Stück man aus einer ganzen Tafeln gewinnen kann. Diese Stückzahl pro Tafel wird dann für die Berechnung der benötigten ganzen Tafeln herangezogen und so die tatsächlich benötigte Menge errechnet.<br>
Es wird dabei versucht welche Variante (horizontal oder vertikal, also Drehen um 90°) eine größere Stückzahl ermöglich. Die jeweils größere Stückzahl wird herangezogen.

Hinweis: Ist ein einpassen der benötigten Tafeln in die Abmessungen des Artikels nicht möglich, so wird angenommen, dass in der Fertigung der Artikel zusammengestückelt wird und man somit eine entsprechend größere Tafel erhält. In diesem Falle gehen dann die größeren Bedarfe in die Mengenberechnungen ein.

[Bitte beachten Sie dazu auch]( {{<relref "/einkauf/bestellung/#kann-auch-in-abmessungen-bestellt-werden" >}} ).

#### Restmengenverwaltung
Auch dieses Thema ist für viele Metallverarbeiter sehr wichtig. Im Endeffekt bedeutet es, dass z.B. bei der Verarbeitung von Stangenmaterial immer wieder Reststücke anfallen, die zwar für den aktuellen Auftrag nicht verwendet werden können, aber auch nicht als Ausschuss betrachten werden dürfen. Selbstverständlich ist dieses Thema bei Flächen (Blechtafeln) entsprechend komplex. Aktuell sind dafür folgende Lösungsansätze bekannt:
- a.) Eine grafische Resteverwaltung in denen auch die Konturen des Restes hinterlegt sind und so der Anwender auswählen kann, welchen Rest einer bestimmten Tafel er für das jeweilige Los verwendet. Eine Lösung dieser Art ist z.B. in [ProFirst]( {{<relref "/docs/installation/30_schnittstellen/profirst" >}} ) oder in [TrumpfFab]( {{<relref "/fertigung/losverwaltung/trumpf_fab">}} ) bzw. TrumpfBoost enthalten, zu denen von Seiten **Kieselstein ERP** eine bidirektionale Schnittstelle gegeben ist.
- b.) Eine vereinfachte Variante, die von der Überlegung ausgeht, dass der in **Kieselstein ERP** bei den verschiedenen Bestellvorschlagsberechnungen verwendete Lagerstand immer ein sicherer Lagerstand ist. D.h. schlimmstenfalls kauft man eine Stange Stahl zu viel und vermeidet damit teure Produktionsstillstände. Dieser Ansatz ist in Ihrem **Kieselstein ERP**, in Verbindung mit der Mehrlagerverwaltung abgebildet.<br>
Es sind dafür folgende Einstellungen vorzunehmen:
    - Definieren Sie ein Reste Lager. Dieses darf in den beiden Bestellvorschlägen NICHT berücksichtigt werden
    - Hinterlegen Sie dieses Lager im Parameter RESTMENGEN_LAGER
    - Bei den Artikeln, bei denen die Reste in das Restelager gebucht werden sollten, definieren Sie die Verschnittbasis
    - Zusätzlich empfiehlt sich, dass der Parameter ABLIEFERUNGSPREIS_IST_DURCHSCHNITTSPREIS aktiviert wird um über alle Losablieferungen vor allem nach der Erledigung des Loses einen Ablieferpreis zu erhalten, der auch die zurückgegebene Restmenge berücksichtigt.

    Diese Einstellung bewirkt nun, dass
    - bei der Materialentnahme die Verschnittbasis als Grundlage für die Bedarfsberechnung herangezogen wird und somit z.B. immer ganze Stangen entnommen werden.
    - bei der letzten Ablieferbuchung, genauer der Erledigung des Loses, werden die Restmengen errechnet und als nachträgliche negative Materialentnahme auf das RESTMENGEN_LAGER gebucht.
    Somit wird das Los vom Wert her entlastet und zugleich "stimmt" der Lagerstand des Hauptlagers, also des Lagers, welches als Basis für die Bestellvorschläge verwendet wird.

    Ergibt sich nun der Fall, dass Sie Material aus dem Restelager für ein Los verwenden möchten, so buchen Sie, selbstverständlich nach entsprechender Prüfung, die benötigte Menge per Nachträglicher Materialentnahme vom Restmengenlager direkt in das Los.

#### Verschnittmenge
<a name="Verschnittmenge"></a>
Mit der Verschnittmenge definieren Sie den zusätzlichen Verbrauch zum Start jeder Produktion für dieses Material.<br>
Sie können dies auch mit der sogenannten Rüstmenge in der Stücklistenposition vergleichen. Hier wird es jedoch am Artikel definiert.<br>
Ein Beispiel aus der Elektronik-Fertigung:<br>
Der SMD Automat benötigt um seine Feeder richtig einzustellen meist an die 50Stk (von den billigen Bauteilen). D.h. bei jedem Los, welches gestartet wird, werden 50Stk weggeworfen. D.h. sie definieren für diese Bauteile eine Verschnittmenge von 50Stk.

#### Wo kann eine Überproduktion bzw. Ausschuss definiert werden ? 
Bei der Produktion muss ein gewisser Faktor von Ausschuss berücksichtig werden um die vom Kunden bestellte Menge fertigen und im weiteren Verlauf die Stückzahl liefern zu können.
Somit muss die bestellte Menge um einen Faktor erhöht werden. Diese Überproduktion definieren Sie im Modul Artikel, oberer Reiter Bestelldaten.<br>
![](ueberproduktion_definieren.JPG)<br>
Der hier eingegebene Faktor wirkt bei der Überleitung bei der Berechnung der Mengen in der internen Bestellung. Hier wird die Losgröße um den eingegebenen Faktor erhöht. 

#### Wozu dient die Referenznummer im Artikel Detail?
Damit können Sie eine zweite / alternative Artikelnummer für den Artikel vergeben. Nach dieser Artikelnummer kann in der Artikelauswahl ebenfalls gesucht werden. Manche Anwender übersetzen die Referenznummer auf Zeichnungsnummer und haben So ein eindeutiges Feld für diese wichtige Nummer und haben damit zusätzlich die Verwaltung nach Zeichnungsnummern gewonnen. 

Hinweis: Die Referenznummer muss nicht eindeutig sein. D.h. es kann durchaus die gleiche Referenznummer für verschiedene Artikel vergeben werden.

#### Artikel Status / Sperren für bestimmte Bereiche?
<a name="Artikel_sperren" class="subquestion"></a>
Unter dem oberen Modulreiter Sperren können Sperren für verschiedene Bereiche für diesen Artikel angegeben werden. Definieren Sie vorher unter Grunddaten, Sperren die in Ihrem Unternehmen benötigten Artikelsperren.<br>
![](Artikel_Sperr_Status.gif)<br>
Mit Einkauf, Verkauf, Fertigung und Stückliste können Sie die Sperren für den jeweiligen Bereich definieren. Zusätzlich steht die Sperre Allgemein für die Sperre des Artikels in allen Bereichen zur Verfügung. Beachten Sie die Verbindung mit den definierten Ersatztypen.
Ist bei einem Artikel eine Sperre hinterlegt, so erscheint bei der Übernahme des Artikels in einem Modul des jeweiligen Bereiches der Hinweis, dass dieser Artikel gesperrt ist.
In der Artikelauswahlliste wird die Sperre durch das gewählte Icon (16x16Pixel) oder wenn kein Icon definiert ist, durch ![](Artikel_gesperrt.gif) angezeigt.
Wird ![](BDE_Sperre_durch_Fertigung.gif) Sperre durch Fertigung angehakt, so bedeutet dies, dass diese Sperre beim Scann des [Los-Sperr-Codes]( {{<relref "/fertigung/zeiterfassung/bde_station/#sperrcode" >}} ) an der HTML BDE Station gesetzt wird.

##### Automatische Sperre
<a name="Automatische Sperre"></a>
Um nun bei neuen Artikeln automatisch eine Sperre setzen zu können, kann ![](Sperre_bei_Neuanlage.gif) default bei Artikelanlage angehakt werden.<br>
Dies bedeutet, dass automatisch, wenn ein neuer Artikel angelegt wird, diese Sperre(n) für den neu angelegten Artikel gesetzt werden. Dies hat insbesondere den Vorteil, dass neue Artikel z.B. von der Technik definiert werden, bevor sie aber verwendet werden dürfen, müssen diese auch noch einen entsprechenden Freigabeprozess durchlaufen.

#### können auch andere Sperrsymbole definiert werden?
Ja. Du benötigst dafür PNG Bilder mit einer Auflösung von 16x16 Pixel. Bitte beachte die Lizenzrechtlichen Einschränkungen, also freie Bilder oder diese kaufen.

Ordne das gewünschtes ICON durch Klick auf Datei und die entsprechende Auswahl dem jeweiligen Sperrstatus zu. Damit wird in der Auswahlliste dieses Symbol für die Sperre angezeigt. In der Regel ist dafür ein Neustart des Clients erforderlich.

#### Ersatzartikel
Unter Sonstiges kann für jeden Artikel auch ein Ersatzartikel definiert werden.
Je nach Sperr-Status des Artikels wirkt dies in unterschiedlicher Weise:
- a.) Der Artikel ist nicht allgemein gesperrt:
Das bedeutet dies ist nur eine Information, welche weiteren Artikel anstelle dieses verwendet werden könnten.
- b.) Der Artikel ist allgemein gesperrt.
Das bedeutet, dass anstelle dieses Artikels der definierte Ersatzartikel verwendet werden sollte.

Wird ein gesperrter Artikel verwendet, so beantworten Sie bitte die nachfolgende Frage entsprechend.<br>
![](Ersatzartikel_verwenden.jpg)<br>
**Hinweis:** Damit der Hinweis des Ersatzartikels erscheint, muss eine zum Modul passende Sperre definiert sein.
Beachten Sie bitte den Unterschied zu den Ersatztypen auf der Stücklistenposition.

#### Ersatztypen
Gerade in der Elektronikbranche hat man oft die Herausforderung dass die Komponenten / Bauteile herstellerspezifisch verwendet werden müssen. Siehe [dazu auch]( {{<relref "/warenwirtschaft/stueckliste/ersatztypen" >}} ). Nun kann im Reiter Ersatztypen die Zuordnung für den Vorschlag entsprechend hergestellt werden. Andererseits ist es oft hilfreich, wenn man gleich bei der Artikelauswahl sieht welche Lagerstände und Verfügbarkeiten denn auch bei den anderen Ersatztypen gegeben sind. Diese Übersicht finden Sie im Reiter Detail des Artikels.
D.h. ausgehend von der Annahme, dass alle gleichen Artikel, welche sich nur durch die Herstellerkennung unterscheiden, die gleiche Artikelnummer haben, werden hier alle Hersteller dieses Artikels angezeigt mit den Herstellerkurzzeichen, dem aktuellen Lagerstand und der freien Verfügbarkeit.<br>
So könnte die verkürzte Ansicht im sächlichen Hauptartikel z.B. wie folgt aussehen:<br>
![](Lagerstand_Ersatztypen.gif)<br>
D.h. für diesen SMD Kerko sehen Sie im unten angeführten Informationsfenster links die Herstellerkurznummer, daneben den Lagerstand und rechts die freie Verfügbarkeit.

#### Statistik der Artikel, Wo sehe ich die Bewegungen am Artikel
Es stehen dafür zwei verschiedene Journale zur Verfügung
- a.) Artikelstatistik. Diese finden Sie unter Info, Statistik.
Sie beinhaltet eine chronologische Auflistung der Artikelbewegungen, wobei sie so gestaltet ist, dass die jüngsten Buchungen zuerst kommen.
Bitte beachten Sie bei der Artikelstatistik insbesondere folgende Einstellungen:

| Einstellung | Angehakt | nicht angehakt |
| --- |  --- |  --- |
| Eingeschränkt | Es werden nur maximal 50 Datensätze angezeigt | Es werden alle Datensätze laut den anderen Kriterien angezeigt |
| Handlagerbewegung berücksichtigen | Es werden auch die Buchungen der Handlagerbewegungen angezeigt | Die Buchungen der Handlagerbewegungen, z.B. manuelle Zubuchungen während der Systemeinführung, werden nicht angezeigt. |
| Mit Bewegungsvorschau | Zusätzlich zu den Vergangenheitsdaten der Artikelstatistik werden auch die Zukunftsdaten anhand der Bewegungsvorschau angezeigt. D.h. es werden dann auch alle geplante Zu- und Abgänge dargestellt. | Es werden nur die Statistikdaten angezeigt. |
| Buchungsdetails | Es werden auch die Details, also die historischen Daten zu den Buchungen angezeigt.Siehe bitte unten. |   |
| mit Vorgängern | Es werden, ausgehend vom ausgewählten Artikel die Daten der Artikel angezeigt, bei denen der ausgewählte Artikel als Ersatzartikel (siehe Sonstiges) eingetragen ist. Die Verkettung mit dem Vorgänger wird bis an den Ursprung durchlaufen. Damit haben Sie über mehrere [Versionen]( {{<relref "/docs/stammdaten/artikel/versionen" >}} ) eine Gesamtübersicht. |   |

- b.) Das Warenbewegungsjournal. Dieses finden Sie unter Info, Warenbewegungsjournal
Aus dem Warenbewegungsjournal sind die aufgrund der Fifo bzw. Chargen bzw. Seriennummern Buchungszusammenhänge zwischen Zugangs- und Abgangsbuchungen ersichtlich. Das bedeutet: Wurden Ihre Artikel auch tatsächlich nach dem Fifo Prinzip vom Lager entnommen, so sehen Sie hier eine eindeutige Verknüpfung der Zu- und Abgänge.<br>
Aus dieser Liste sind auch die Verbrauchsbuchungen jedes Warenzugangs ersichtlich. D.h. sollte eine Warenzugangsbuchung verändert werden, dies ist aber im ersten Versuch nicht möglich, so liegt es meist daran, dass die Zubuchung schon verbraucht wurde. Die Ursache kann aus dem Warenbewegungsjournal herausgelesen werden.

#### Zählt man die seit der Inventur gebuchten Mengen zusammen, stimmen diese nicht mit dem Lagerstand überein!?
Diese Situation ergibt sich aus der System bedingten Eigenheit, dass Lagerkorrekturen "zurückgegeben" werden. Die Lagerbuchungen also so sein müssen, als wenn sie nie stattgefunden hätten.<br>
Ein klassisches Beispiel dazu ist:
1.  am 10.12. werden 2Stk in ein Los gebucht
2.  am 22.12. wird ein Lagerstand von 1Stk gebucht und als Inventur eingetragen
3.  am 23.12. wird die auf das Los entnommene Menge auf 1Stk reduziert.
    Bei dieser Reduktion wird die Entnahmebuchung so eingestellt, als wenn am 10.12. bereits nur ein Stück entnommen worden wäre.

Der Hintergrund ist, dass wenn dieses eine Stück in einem anderen Los verbaut würde und Sie danach die Einstandspreise der Bestellung ändern, so muss sich diese Änderung selbstverständlich bis in das Los auswirken. Würden wir dies nicht so machen, so würden die Korrekturen der Einstandspreise nicht durchgreifen.<br>
Siehe dazu bitte auch unsere Empfehlungen zur Durchführung der Inventur.

Um nun diese Zusammenhänge darzustellen, nutzen Sie bitte den Haken bei Buchungsdetails.
Hier werden die zusammengehörigen Artikel in ihren Buchungsgruppen dargestellt, wodurch die eigentliche Ursache ersichtlich ist.

![](Artikelstatistik_mit_History.jpg)

So ist aus dieser Darstellung ersichtlich, dass die eigentliche Entnahme am 10.3. VOR der Inventur vom 31.3. stattgefunden hat und diese am 19.7. korrigiert wurde.

Ermittlung der Gestehungspreise bei "nicht lagerbewirtschaftet"

Gerade bei nicht lagerbewirtschafteten Artikeln ist es je nach Unternehmensausrichtung manchmal erforderlich, dass diese Artikel trotzdem Gestehungspreise haben.

Siehe dazu auch Parameter GESTPREISBERECHNUNG_HAUPTLAGER. Wenn dieser auf Ja (=1) gestellt ist, so wird für die Berechnung nur der Lagerwert des Hauptlagers herangezogen, wenn der Artikel lagerbewirtschaftet ist. Ist der Artikel **NICHT** lagerbewirtschaftet, so wird der Gestehungspreis des Lagers KeinLager verwendet.

**Hinweis:**

Nur wenn der Artikel nicht lagerbewirtschaftet ist, steht Ihnen bei der Auswahl des Lagers im oberen Modulreiter Lager, auch das KeinLager zur Verfügung. Beachten Sie bitte, dass bei nicht lagerbewirtschafteten Artikeln der jeweilige Gestehungspreis nur von diesem Lager genommen wird.

Ist der Parameter GESTPREISBERECHNUNG_HAUPTLAGER auf Nein (=0) gestellt, so wird der Gestehungspreis für allgemeine Betrachtungen immer aus dem Durchschnittspreis aller Läger berechnet. Ist hier der Artikel nicht Lagerbewirtschaftet, so ergibt sich aufgrund dieser Durchschnittsberechnung immer ein Gestehungspreis von 0 (Null).

Anzeige der Artikelauswahlliste

Die Darstellung der Artikelauswahlliste kann über folgende Parameter definiert werden.

| Parameter | Beschreibung |
| --- |  --- |
| VKPREIS_STATT_GESTPREIS_IN_AUSWAHLLISTE | Bewirkt, dass anstelle des Gestehungspreises die Verkaufspreisbasis angezeigt wird.Zusätzlich ist für die Anzeige des jeweiligen Preises auch das entsprechende Recht erforderlich, also LP_DARF_PREISE_SEHEN_EINKAUF bzw. LP_DARF_PREISE_SEHEN_VERKAUF |
| ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE | Wenn dieser Parameter aktiviert ist, so bedeutet dies, dass anstelle der Zusatzbezeichnungen die Abmessungen, siehe Reiter Technik, angezeigt werden. |
| ANZEIGEN_ARTIKELKLASSE_IN_AUSWAHLLISTE | Wenn dieser Parameter aktiviert ist, so bedeutet dies, dass anstelle der Zusatzbezeichnungen die Artikelklassen angezeigt werden. (Wirkt nur, wenn der Parameter ABMESSUNGEN_STATT_ZUSATZBEZ_IN_AUSWAHLLISTE=0 und ANZEIGEN_ARTIKELGRUPPE_IN_AUSWAHLLISTE=0.) |
| ANZEIGEN_KURZBEZEICHNUNG_IN_AUSWAHLLISTE | Wenn dieser Parameter aktiviert ist, so wird die Kurzbezeichnung in der Artikel-Auswahlliste angezeigt. |
| ANZEIGEN_LAGERPLATZ_IN_AUSWAHLLISTE | Wenn dieser Parameter aktiviert ist, so wird der Lagerplatz in der Artikel-Auswahlliste angezeigt. |
| LIEF1INFO_IN_ARTIKELAUSWAHLLISTE | Ist dieser Parameter aktiviert und besitzt der Benutzer das Recht LP_DARF_PREISE_SEHEN_EINKAUF, so werden zwischen dem Gestehungspreis und dem Sperren-Icon drei zusätzliche Felder angezeigt.Kurzbezeichnung des bevorzugten LieferantenLief1Preis = Einkaufspreis des bevorzugten Lieferanten ohne Mengenstaffel und immer in MandantenwährungWBZ = Wiederbeschaffsungszeit des bevorzugten Lieferanten |

Bei Änderungen dieser Parameter ist ein Neustart des Clients erforderlich.

### Reservierungen, Lieferdauer, freigegebene / nicht freigegebene Aufträge bzw. Forecast
<a name="Reservierungen, Termine"></a>
Artikel werden immer zum Termin der Auftragsposition reserviert.
Die Definition für die Auftragsposition ist, analog zu den Bestellungen eintreffend in diesem Falle jedoch eintreffend bei Ihrem Kunden. D.h. für die Produktion / Anlieferung der Ware ist vom Auftragspositionsliefertermin die Lieferdauer abzuziehen.
Für Aufträge werden immer die Reservierungen eingetragen. In den verschiedenen Auswertungen wird gegebenenfalls berücksichtigt ob ein Auftrag freigegeben ist oder nicht. So sehen Sie dies z.B. in der Bewegungsvorschau, dass neben den schon freigegebenen Auftragspositionen auch die noch nicht freigegebenen Auftragspositionen angeführt werden.
ACHTUNG: Bei den CallOff-Tag bzw. Woche wird, wenn diese nicht freigegeben sind, auch keine Reservierung eingetragen. Somit scheinen nicht freigegebene CallOff auch in den Reservierungen und somit in der Bewegungsvorschau auf.
Für die Berechnung des Abliefertermines wird die Lieferadresse des Forecast bzw. des Auftrags verwendet.
Siehe dazu auch [Berechnung der Termine]( {{<relref "/fertigung/losverwaltung/#berechnung-des-beginns-der-durchlaufzeit-und-des-endes--abliefertermins" >}} ).

#### Wo sehe ich die Artikel / Bauteile die vom Termin her kritisch sind?
[Siehe dazu bitte]( {{<relref "/fertigung/losverwaltung/planung_auswertung/#auslieferliste-umfangreich" >}} ).

##### Was bedeuten die Symbole in der rechten Spalte der Artikelauswahlliste?
In der rechtesten Spalte der Artikelauswahlliste werden [Artikelsperren ![](Artikel_gesperrt.gif)](#Artikel_sperren) und [Reklamationen ![](Artikel_Reklamationen.gif)]( {{<relref "/fertigung/reklamation/#anzeige-der-reklamierten-artikel-im-artikelstamm" >}} ) am Artikel angezeigt.

#### Ich würde gerne die Artikelgruppe bzw. die Artikelklasse als Pflichtfeld definieren?
Aktivieren Sie dafür den Parameter ARTIKELGRUPPE_IST_PFLICHTFELD bzw. ARTIKELKLASSE_IST_PFLICHTFELD.
Damit wird erzwungen, dass bei der Bearbeitung im Modulartikel im Reiter Detail die Artikelgruppe bzw. die Artikelklasse definiert werden muss.
Bitte beachten Sie auch, dass für die Verwendung der integrierten **Kieselstein ERP** Finanzbuchhaltung in der Regel die Artikelgruppe ein Pflichtfeld sein muss. [Siehe dazu auch.](#Externe Dokumente)

### Ladenhüter

In der Auswertung der Ladenhüter werden jene Artikel gelistet, die im angegebenen Zeitraum keine Lagerbewegung hatten, bzw. die nicht länger als im angegebenen Zeitraum auf Lager liegen.
In der Liste der Ladenhüter erhalten Sie den Lagerstand zum Ende-Zeitpunkt der Betrachtung und zum aktuellen Gestehungspreis.

Warum aktueller Gestehungspreis und nicht abgewerteter Preis ?
Da es um die Lagerbewertung geht, muss man die tatsächlichen Kosten des Artikels kennen, sonst würden, bei entsprechender Abwertung, die teuren Artikel, wenn sie z.B. länger als ein Jahr liegen, Wertmäßig nicht mehr aufscheinen. Nur mit dem tatsächlichen Gestehungspreis kann entscheiden werden, was mit einem Ladenhüter gemacht werden soll. Z.B. an den Lieferanten zurückgeben, weiterverkaufen, oder an den auslösenden Kunden weiterverrechnen.
![](Ladenheuter.gif)

Was bedeuten die einzelnen Spalten in der Ladenhüterliste:

| Kurzzeichen|Beschreibung|
| --- |  --- |
| AG | Artikelgruppe |
| AK | Artikelklasse |
| Artikelnr. | Artikelnummer |
| Bezeichnung | Bezeichnung des Artikels |
| 1) | Lagerstand zum Zeitpunkt des Ausdruckes |
| Lagerstand zum Datum | Lagerstand zum Bis-Datum des Ladenhüterausdrucks. D.h. welcher Lagerstand war am Ende der Betrachtung des Ladenhüterstandes auf Lager. Im Vergleich zum heutigen Lagerstand ( siehe oben 1) )  sehen Sie ob das Material in der Zwischenzeit verbraucht wurde. |
| Gest.Preis | Gestehungspreis zum Ausdruckzeitpunkt nicht abgewertet |
| Lagerwert | Lagerwert zum Bis-Datum des Ladenhüterausdrucks |
| Fehlmenge | aktuelle Fehlmenge für die Fertigung zum Zeitpunkt des Ausdruckes |
| Reserviert | aktuelle Reservierungen zum Zeitpunkt des Ausdruckes |
| RahmenRes | aktuelle Rahmenreservierungen zum Zeitpunkt des Ausdruckes |
| RahmenDet. | aktuelle Rahmendetailbedarfe zum Zeitpunkt des Ausdruckes |
| freie Menge | aus dem Lagerstand abzgl. Fehlmenge, Reserviert, Rahmenreserviert resultierende MengeDiese verbleibt nach Abarbeitung obiger (Fertigungs-) Aufträge noch auf Lager |
| Einstandspreis | Preis aus der Warenzubuchung, meistens Wareneinstandspreis zzgl. ev. Transportkosten |
| freier Lagerwert | freie Menge * Gestehungspreis |
| Art | Enthält die Artikelart: S ... Stückliste, H ... Hilfsstückliste, Z ... Arbeitszeitartikel |
| V | Wird angedruckt, wenn dies ein versteckter Artikel ist |
| Lagerwert ohneStücklisten | Sehr oft wird eine Trennung in reines Material und bereits (eigen-)gefertigte Ware benötigt. Im Lagerwert ohne Stücklisten sind nur die Artikel enthalten, welche nicht als Stückliste definiert sind. |

Hinweis: In der Ladenhüterliste sind Artikel die nicht als Lagerbewirtschaftet gekennzeichnet sind, nicht enthalten.

Um festzulegen welche Lagerbewegungen für die Betrachtung als Ladenhüter von Bedeutung sind, stehen folgende Auswahlmöglichkeiten zur Verfügung, wobei von der Grundidee her nur Lagerentnahmen berücksichtigt werden.
![](Ladenhueter_Auswahl.gif)

| Feld | Beschreibung |
| --- |  --- |
| Fertigung berücksichtigen | Buchungen die von Lager auf Lose gingen werden für die Betrachtung ebenfalls berücksichtigt. |
| Zugänge aus Fertigung berücksichtigen | Losablieferungsbuchungen werden, da die Produktion der Baugruppen bewusst veranlasst wurden, ebenfalls in der Form berücksichtigt, dass eine derartige Buchung keine Betrachtung als Ladenhüter veranlasst. Beachten Sie bitte, dass negative Lagerentnahmen z.B. aufgrund von negativen Sollmengen im Los hier nicht betrachtet werden, davon nicht betroffen sind.  |
| Handlagerbewegungen berücksichtigen | Die Verbrauchsbuchungen der Handbuchungen werden berücksichtigt. |
| plus Versteckte | Artikel die als Versteckt gekennzeichnet sind, werden ebenfalls mit ausgewertet. |

**Wichtig:** Wenn Sie z.B. ein Fertigungsbetrieb sind, so ist die Betrachtung inkl. Fertigung berücksichtigen für Sie die richtige Einstellung. Das bedeutet, dass Lagerbewegungen von Lager an Fertigung als gültige Lagerbewegungen betrachtet werden. Somit werden, wenn Buchungen von Lager an Los im Betrachtungszeitraum waren, diese NICHT als Ladenhüter betrachtet.
In analoger Weise wird bei Handlagerbewegungen berücksichtigen vorgegangen.
Was bedeutet der angegebene Zeitraum ?
Mit dem Zeitraum wird definiert für welchen Buchungszeitraum Sie die Ladenhüterbetrachtung durchführen. Also alle Artikel, nach den obigen Bedingungen, werden dann als Ladenhüter betrachtet, wenn es im Zeitraum KEINE Bewegung von Lager an Verkauf gegeben hat (nach den oben beschriebenen Festlegungen).
In anderen Worten:
Die Ladenhüter Auswertung ist heute den 22.6.2011 für den Zeitraum vom 1.1.-31.3. gültig. Eine Bewegung mit einem Belegdatum (z.B. Lieferscheindatum) vom 1.4\. ist nach obiger Definition somit <u>**ebenfalls**</u> ein Ladenhüter. Es werden bei der Ladenhüterberechnung immer alle aktuell angelegten Artikel betrachtet.

#### Lagerwert zum Stichtag und offene Lieferscheine
<a name="Lagerwert zum Stichtag und offene Lieferscheine"></a>
Die Frage des Anwenders war genau: Welchen Einfluss haben noch nicht verrechnete Lieferscheine auf den Lagerwert. Die exakte Antwort lautet:
Wird Ware mittels Lieferschein ausgeliefert, so wird die Ware dadurch vom Lager abgebucht. Daher verringert sich der Lagerwert entsprechend. Sind nun zu einem Stichtag, z.B. Monatsabgrenzung, noch nicht verrechnete Lieferscheine vorhanden, so müssen diese in einer Gesamtbetrachtung entsprechend berücksichtigt werden. Da es nun Lieferscheine gibt die zwar noch offen sind, aber nicht verrechnet werden können, kann dies in den Lieferscheinen unter Kondition angegeben werden und im Lieferscheinjournal für den Druck entsprechend ausgegeben werden.

#### Lagerwert zum Stichtag und offene Wareneingänge
<a name="Lagerwert zum Stichtag und offene Wareneingänge"></a>
Ähnlich wie oben können sich auch noch nicht geprüfte Wareneingänge auf den Lagerwert auswirken. Der Unterschied ist hier, dass bei der Warenzugangsbuchung immer der Bestellpreis, welcher der genaueste bekannte Preis ist, verwendet wird. Kommt es jedoch bei der Rechnungsprüfung zu entsprechenden Änderungen der Einstandspreise, z.B. wegen hoher Transportkosten, oder Bestellungen zu 0,- Preisen, so kann sich der Lagerwert zum Stichtag dadurch entsprechend ändern. Dies kann soweit gehen, dass ein Ausdruck zu einem früheren Zeitpunkt, an dem die Korrektur des Wareneinstandspreises noch nicht durchgeführt worden war, und der Ausdruck zum gleichen Stichtag, wo aber die Korrektur bereits eingebucht ist, unterschiedliche Lagerwerte ergeben.

#### Wie wird der Gestehungspreis für die Lagerstandsliste zum Stichtag errechnet?
<a name="Gestehungspreisberechnung zum Stichtag"></a>
Für die Ermittlung des Gestehungspreises zu einem bestimmten Stichtag wird der Gestehungspreis, der dem Stichtag (24:00) nachfolgenden Abbuchung, verwendet.

Hat nun ein Artikel vom letzten Abgang bis zum Zeitpunkt der Betrachtung zwar Warenzugänge, aber keine Abgänge, so können sich durch die verschiedenen Einstandspreise Änderungen am Gestehungspreis ergeben, welche sich im nachträglichen Ausdruck eines Lagerwertes zum Stichtag auswirken. Dies basiert auf der Logik, dass bei jedem Warenzugang der aktuelle Gestehungspreis neu berechnet wird. Das hat zur Folge, dass der Lagerwert, gerechnet über den aktuellen Gestehungspreis immer stimmt. Da der Gestehungspreis jedoch nur für die Warenentnahmebuchungen protokolliert wird, steht, unter obiger Situation, nur der aktuelle Gestehungspreis mit all seinen Änderungen für die Ermittlung zur Verfügung. Sind jetzt gravierende Änderungen durch Neueinkäufe im Gestehungspreis gegeben, wird sich dies auch auf die nachträglichen Betrachtungen auswirken.

Wir raten daher, Lagerstandslisten immer möglichst zeitnahe für den entsprechenden Stichtag auszudrucken und entsprechend aufzubewahren. Werden diese Listen später ausgedruckt, so kann es, wegen des oben beschriebenen Mechanismus zu Abweichungen kommen. Um die Aufbewahrung der Lagerstandslisten etwas zu erleichtern, werden diese in die automatische Dokumentenablage mit aufgenommen.

**Hinweis:**

Die Bewertung von Artikeln deren Gestehungspreis 0,00 ist hängt zusätzlich noch vom Parameter LAGERSTANDSLISTE_EKPREIS_WENN_GESTPREIS_NULL ab. Steht dieser auf 1, so bedeutet dies, dass wenn der Gestehungspreis 0,00 ist, wird anstatt dessen der Einkaufspreis des bevorzugten Lieferanten verwendet. Damit wird Ihre Lagerware gegebenenfalls eben zu Einkaufskonditionen bewertet.


## Zentraler Artikelstamm
<a name="Zentraler Artikelstamm"></a>
[siehe Mehrmandanten]( {{<relref "/docs/stammdaten/artikel/mehrmandanten" >}} )

Bedeutung der einzelnen Informationsfelder im Artikeldetail:

![](ArtikelLagerstandInfo.gif)

Im Artikeldetail finden Sie, bei Darstellung in der Mandantensprache, folgende Informationsfelder. Diese Felder sind Verdichtungen von Daten und dienen dem schnellen Überblick, der Beschaffungssituation dieses einen Artikels.

Die Bedeutung der Felder im einzelnen ist:

| Feld | Bedeutung | Detailinfos finden Sie unter: |
| --- |  --- |  --- |
| Lagerstand | Lagerstand aller Läger dieses Mandanten | Modulreiter Lager |
| Reserviert | Summe aller Reservierungen auf Aufträge und Lose | Info, Reservierungsliste |
| Fehlmenge | Summe der Fehlmengen für Lose | Info, Fehlmengenliste |
| Verfügbar | Lagerstand - Reserviert - Fehlmenge = Verfügbar | Info, Bewegungsvorschau |
| Detailbedarf | offener Rahmendetailbedarf. [Siehe dazu]
( {{<relref "/verkauf/auftrag/#rahmendetailbedarf" >}} ) | Info, Rahmendetailbedarf |
| In Fertigung | Erwartete Losablieferungen aufgrund offener Lose | Fertigung, offene Lose dieses Artikels |
| Bestellt | Summe aller offenen Bestellungen | Info, Bestellt-Liste |
| Rahmenreserviert | Offene Reservierungen für Rahmenaufträge | Info, Rahmenreservierungen |
| Rahmenbestellt | Offene Rahmenbestellungen | Info, Rahmenbestellt-Liste |

Hinweis:<br>
Bei aktivierter Übermengenverwaltung kann es auch negative Fehlmengen geben, womit dieser Wert (minus minus) für die Verfügbarkeit zum Lagerstand hinzugezählt wird. [Siehe]( {{<relref "/fertigung/losverwaltung/#%c3%bcbermengen-verwaltung" >}} )


### Unterschied: Rahmendetailbedarf und Rahmenreservierung.
In der Rahmenreservierung sind die einzelnen Artikel enthalten, welche direkt in einem Rahmenauftrag benötigt werden. Im Rahmendetailbedarf werden die Artikel angeführt, welche indirekt über die Stücklistenauflösung für den Rahmenauftrag benötigt werden.

Anzeige von offenen Rahmenmengen mit 0, [siehe]( {{<relref "/verkauf/auftrag/#anzeige-von-offenen-rahmenmengen-mit-0" >}} )

## Artikelgruppe / Artikelklasse
Die im Artikel Detail hinterlegbaren Filtermöglichkeiten Artikelgruppe bzw. Artikelklasse müssen vor der Verwendung in den Grunddaten des Artikels definiert werden.
Vom Grundgedanken her werden diese beiden Filtermöglichkeiten gleich verwendet. Sie dienen vor allem in der Kunden-/Lieferanten- Umsatzstatistik dazu die verwendeten Artikel in die entsprechenden Kategorien zu unterteilen.
Mit der Artikelgruppe werden zusätzlich noch die Erlöskonten für die Überleitung in die Finanzbuchhaltungsprogramme und eventuelle Sonderkonditionen für die Kunden definiert. [Siehe dazu bitte]( {{<relref "/management/finanzbuchhaltung/fibu_export/#erl%c3%b6skontenfindung" >}} )

Mit den Artikelgruppen/-klassen stehen auch sogenannte Vatergruppe/-klassen zur Verfügung. Diese dienen dazu Artikelgruppen wiederum in übergeordnete Gruppen zusammenzufassen. Um nun in der direkten Suche sowohl die Artikel der Vatergruppe als auch der Untergruppe(n) aufgelistet zu erhalten, empfiehlt es sich, in den Artikelgruppen des jeweiligen Hierarchiebaumes eine gleichlautende Bezeichnung zu verwenden.
Am Beispiel von Musikinstrumenten:
> **Blas**instrument
>> Blech**blas**instrument
>>
>> Holz**blas**instrument
>>
> **Trommel**
>> Marsch**trommel**
>>
>> Bass**trommel**
>>
Somit finden Sie bei der Eingabe im Direktfilter Artikelgruppe sowohl die Vatergruppe als auch die dazugehörenden Untergruppen.

Beachten Sie dazu auch die Auswertung der Artikelgruppen unter Journal im Modul Artikel.<br>
In diesem sehen Sie die verdichtete Darstellung der jeweiligen Vatergruppe, des Gesamtumsatzes der direkt und indirekt der Vatergruppe zugeordneten Artikel und darunter eingerückt den Umsatz der darunter liegenden Gruppe usw..

Bitte beachten Sie für die Art der Anzeige Artikelgruppen in der Artikelauswahlliste auch den Parameter ARTIKELGRUPPE_NUR_VATERGRUPPEN_ANZEIGEN. Hier hat sich die Einstellung 2, also hierarchische Darstellung sehr bewährt.

**Bedeutung der Einstellungen in der Artikelgruppe**

 ![](Artikelgruppe_Definitionen.gif)

Für jede Artikelgruppe können verschiedene Einstellungen durchgeführt werden.
Diese haben folgende Bedeutung:

| Benennung | Beschreibung |
| --- |  --- |
| Kennung | Die Basiskennung der Artikelgruppe |
| Bezeichnung | Die Bezeichnung der Artikelgruppe in der Sprache des angemeldeten Clients |
| Vatergruppe | Die übergeordnete Artikelgruppe dazu.Siehe oben. |
| Rückgabe | Für diese Artikelgruppe wird im Verleihgeschäft auch die Rückgabe gebucht. [Siehe dazu]( {{<relref "/verkauf/auftrag/termin_in_stunden_minuten" >}} ). |
| Konto | Das Inlandserlöskonto für diese Artikelgruppe.[Siehe dazu]( {{<relref "/management/finanzbuchhaltung" >}} ). |
| Zertifizierung | Für diese Artikelgruppe muss der Lieferant zertifiziert sein.[Siehe dazu](#Zertifizierungspflichtige Artikel). |
| VK-Warnmeldung im LS unterdrücken | Ist dies angehakt, so werden beim "Schreiben" des Lieferscheines keine Warnmeldungen ausgegeben, welche aus dem Thema der Verkaufspreisfindung kommen. |
| EK-Preis Aufschlag in % | Ist Einzelpreis nicht angehakt und ist ein EK-Preisaufschlag definiert, so wird bei der Änderung, beim Neueintrag eines Einkaufspreises der Nettopreis, also Ihr echter Einkaufspreis, erhöht um den EK-Preis Aufschlag in die Verkaufspreisbasis übertragen.Ist Einzelpreis angehakt, wird anstatt dessen der Einzelpreis, der normalerweise dem Listenpreis entspricht um den EK-Preis Aufschlag erhöht in die VK-Preisbasis übernommen. |
| Einzelpreis | Dieses logische Feld definiert wie sich der obige EK-Preis Aufschlag verhalten sollte.Siehe oben. |
| keine VK-Rabatte drucken | Für die Artikel dieser Artikelgruppe werden, obwohl der Kunde auf Belegdruck mit Rabatt gestellt ist, keine Rabatte an die Belege übergeben. |
| Serien/Chargennummer | hier kann die default Vorbelegung von Serien-/Chargennummer abhängig von der Artikelgruppe bei der Anlage neuer Artikel definiert werden. |
| Kostenstelle | Dient der Vorbelegung der Kostenstelle bei der Auftragsschnellanlage, anhand der Artikelgruppe des Musterartikels |
| Kommentarartikel | Ergänzend zu den Kommentaren des Artikels selbst, werden die Kommentare aus dem Kommentarartikel der Artikelgruppe gedruckt.D.h. es kann für alle Artikel einer Artikelgruppe eine gemeinsame Beschreibung geben oder z.B. ein Wareneingangshinweis. |
| Fremdfertigung | Artikel dieser Artikelgruppen werden bei der vorab Fehlmengenbetrachtung nicht mit berücksichtigt. [Siehe dazu]( {{<relref "/fertigung/losverwaltung/#was-bedeutet-das-kleine-f" >}} ). |

#### Wozu soll ich nun die Artikelgruppe und wozu die Artikelklasse verwenden?
Die etwas trockene Antwort auf diese Frage ist, Artikelgruppe und Klasse dienen der horizontalen und der vertikalen Sortierung Ihrer Auswertungen.

In anderen Worten:<br>
Man will einen Artikel z.B. eine Schraube einmal den Einkaufsteilen zuordnen und das andere Mal den mechanischen Teilen. Einmal will ich wissen welches Einkaufsvolumen man bei den Einkaufsteilen hatte und das andere Mal welchen Umsatz man mit mechanischen Teilen hatte.<br>
Das bedeutet ich ordne z.B. die Schraube der Artikelklasse mechanische Teile zu und ordne sie auch der Artikelgruppe Einkaufsteile zu.<br>
Nun kann ich z.B. in der Kundenumsatzstatistik auswerten, mit welchem Kunden ich welche Umsätze mit den mechanischen Teilen gemacht habe oder andererseits welche (Verkaufs-)Umsätze ich direkt mit Einkaufsteilen gemacht habe. Siehe dazu auch [Kundenumsatzstatistik]( {{<relref "/docs/stammdaten/kunden/#journal-umsatzstatistik" >}} ).

Ein anderes Beispiel, welches insbesondere bei einer guten Verbindung in die Finanzbuchhaltung wichtig ist, ist die Betrachtung der Artikel in Richtung Erlöskontenfindung und eben z.B. nach anderen Klassen. D.h. wenn Sie z.B. Dienstleistungen erbringen und auch Ware verkaufen, so müssen einerseits die Dienstleistungen auf das eine Sachkonto gebucht werden und die Warenlieferungen auf ein anderes Sachkonto (Details zur Buchhaltung siehe bitte dort). Nun gliedern sich Ihre Produkte aber auch z.B. nach Produktgruppen. In den Produktgruppen haben Sie wieder Dienstleistungen und Waren getrennt. Ich will / muss aber z.B. wissen, mit welchen Produktgruppen mache ich bei welchen Kunden welche Umsätze und Deckungsbeiträge. Auch hier ist die eine Auswertung horizontal und die andere vertikal, oder zwei verschiedene Filter für den gleichen Artikel.

Shopgruppe

Wird ihr Webshop durch **Kieselstein ERP** gespeist, bitte fordern Sie dazu gegebenenfalls die Beschreibung der **Kieselstein ERP** API für Webshops an, so ist oft die Situation gegeben, dass mehrere Webshops betrieben werden und dass nicht alle Artikel auf jedem Webshop angeboten / verkauft werden. Zur Definition was in welchem Webshop dargestellt werden sollte dient die Shopgruppe.

Definieren Sie zuerst in den Artikel Grunddaten Ihre Webshops.

Dann wählen Sie im Modul Artikel den unteren Modulreiter Shopgruppe und definieren eine neue Shopgruppe.

Geben Sie dieser einen für Sie sprechenden Namen.

Nun wählen Sie den Modulreiter Webshop und definieren für welche Webshops diese Shopgruppe angewandt werden soll.

Als nächsten Schritt definieren Sie die Artikel die in der jeweiligen Shopgruppe erscheinen sollten direkt in den Kopfdaten der Artikel.

<a name="Artikelnummerngenerator"></a>Automatisch neue Artikelnummern generieren

Für die Erzeugung von Artikelnummern ist es oftmals praktisch diese von **Kieselstein ERP** erzeugen zu lassen.
Durch Klick auf ![](Generieren.JPG) bei der Anlage neuer Artikel können Sie automatisch weitere neue Nummern erzeugen lassen. Damit diese neuen Artikelnummern trotzdem eine gewissen Struktur erhalten muss die jeweilige Basisnummer des gewünschten Nummernkreises angelegt werden.
Sollten z.B. Ihre Artikel mit K10- beginnen und dann vierstellig weiterzählen, so muss die erste Artikelnummer mit K10-0001 manuell angelegt werden. Wenn nun eine weitere Artikelnummer im Nummernkreis K10 angelegt werden soll, so muss lediglich der Nummernkreis ![](Artikelnummernautomatik2.gif) definiert werden. Klicken Sie danach auf den Knopf links der Artikelnummer (...) und Sie erhalten automatisch die nächst höhere Nummer mit K10-0002\.
Bitte beachten Sie dazu auch die definierten Längen der Artikelnummern. Diese werden im System, Parameter eingestellt (ARTIKEL_MAXIMALELAENGE_ARTIKELNUMMER, ARTIKEL_MINDESTLAENGE_ARTIKELNUMMER).

In dem Parameter STARTWERT_ARTIKELNUMMER kann ein Bereich eingegeben werden. Wenn man nun auf den Button Generiere Artikelnummer klickt und wenn die Artikelnummer leer ist, dann wird der Startwert aus dem Mandantenparameter STARTWERT_ARTIKELNUMMER verwendet und eingetragen ![](Generiere_artikelnummer.JPG).

Länge der Artikelnummer

Mit den Parametern .. kann die mindest-Stellenanzahl bzw. die maximale Stellenanzahl der Artikelnummer definiert werden.
Bitte beachten Sie dass derzeit die Gesamtlänge der Artikelnummer auf 25Stellen begrenzt ist.
D.h. bei Verwendung der Herstellerartikelnummer darf die Gesamtzahl der Artikelnummer 25 nicht überschreiten.
![](Maximale_Artikelnummern_ueberschritten.jpg)
Obiger Hinweis erscheint, wenn diese Bedingung nicht gegeben ist. Korrigieren Sie in diesem Falle die beiden Parameter (ARTIKEL_MAXIMALELAENGE_ARTIKELNUMMER und ARTIKEL_LAENGE_HERSTELLERBEZEICHNUNG), sodass der Gesamtwert 25 nicht überschreitet.

### Import von Artikeldaten
<a name="Import"></a>
Im Modul Artikel finden Sie unter Artikel, CSV-Import die Möglichkeit neue Artikel in den Artikelstamm zu importieren. Derzeit steht für den CSV Import von Artikeln folgende Felder im CSV Format zur Verfügung.

Artikelnummer; Bezeichnung; Kurzbezeichnung; Zusatzbezeichnung; Zusatzbezeichnung2; Artikelart; Mengeneinheit; Artikelgruppe; Artikelklasse; Referenznummer; MwSt-Satz; Verkaufspreisbasis; VK-Basis gültig ab; Fixpreis1; Rabattsatz1; Gültig ab1; Fixpreis2; Rabattsatz2; Gültig ab2; Fixpreis3; Rabattsatz3; Gültig ab3; [Revision; Index; Chargenbehaftet; Seriennummernbehaftet]

Das wichtigste Element ist die Artikelnummer. Ist ein Artikel dieser Nummer noch nicht vorhanden, so wird er angelegt. Ist er bereits vorhanden, so wird der Artikel derzeit nicht importiert.

Achten Sie bei der Erstellung der Daten für den Import auf folgendes:
| Feldname | Beschreibung |
| --- |  --- |
| Artikelnummer | Nur gültige Zeichen laut Parameter ARTIKELNUMMER_ZEICHENSATZ verwenden |
| Mengeneinheit | Muss vorhanden sein (z.B. Stk). Am Einfachsten ermitteln Sie diese in dem Sie im Detail eines Artikels im Ändern die Mengeneinheiten aufklappen und den entsprechenden "Text" für die Bezeichnung der Mengeneinheit auswählen. |
| Artikelgruppe | Muss angelegt sein, siehe Artikel, Grunddaten, Artikelgruppen, Kennung |
| Artikelklasse | Muss angelegt sein, siehe Artikel, Grunddaten, Artikelklassen, Kennung |
| Artikelart | Hier stehen Artikel oder Arbeitszeit zur Verfügung |
| MwSt-Satz | Hier muss die Bezeichnung des Mehrwertsteuersatzes angegeben werden. Am Einfachsten ermitteln Sie diesen in dem Sie im Detail eines Artikels im Ändern die Mehrwertsteuersätze aufklappen und den entsprechenden "Text" für die Bezeichnung des Satzes auswählen.Hinweis: \<leer> ist nur ein Platzhalter und keine gültige MwSt Bezeichnung. Bitte z.B. Allgemeine Waren verwenden. |
| Verkaufspreisbasis |  |
| VK-Basis gültig ab | TT.MM.JJJJ |
| Fixpreis1 | Fixpreis der Preisliste 1 in der aktuellen Anzeige unter Artikel, VK-Preis |
| Rabattsatz1 |  |
| Gültig ab1 | TT.MM.JJJJ |
| Fixpreis2 | Fixpreis der Preisliste 2 in der aktuellen Anzeige |
| Rabattsatz2 |  |
| Gültig ab2 | TT.MM.JJJJ |
| Fixpreis3 | Fixpreis der Preisliste 3 in der aktuellen Anzeige |
| Rabattsatz3 |  |
| Gültig ab3 | TT.MM.JJJJ |
| Revision | Optional (wenn dann alle vier zusätzlichen Spalten)Revision des Artikels |
| Index | Index des Artikels |
| Chargenbehaftet | 0 ... nicht Chargen geführt, 1 ... Chargen geführt |
| Seriennummernbehaftet | 0 ... nicht Seriennummern geführt, 1 ... Seriennummern geführt.<br>Wenn Seriennummern geführt, dann kann es nicht Chargen geführt sein. |

Sind Fehler im Import vorhanden, so werden entsprechende Meldungen ausgegeben. Beheben Sie diese. Erst wenn keine Fehler mehr eingetragen sind, wird der Knopf für das Importieren freigeschaltet. 

![](Artikelimport_Fehlermeldung.jpg)

Können die Artikel importiert werden, so wird "Keine Fehler gefunden" angezeigt und durch Klick auf den Importknopf wird der Import gestartet.

Erhalten Sie Fehlermeldungen wie:

![](Artikelimport_Fehlermeldung1.jpg)

so prüfen Sie bitte unbedingt die Feldtrenner. Diese müssen auf ; (Semikolon) gestellt sein.

#### Gibt es eine Alternative zum CSV Import der Artikeldaten?
Anstelle des CSV-Imports kann auch eine Datei im xls Format, welche z.B. im LibreOffice erstellt wurde, genutzt werden.

Folgende Felder können im XLS-File vorhanden sein, wobei zumindest die Artikelnummer vorhanden sein muss: (Alle Werte/Zahlen müssen auch als solche formatiert sein)

Artikelnummer    Artikelart    Kurzbezeichnung    Bezeichnung    Zusatzbezeichnung    Zusatzbezeichnung2    Referenznummer    Index    Revision    Hersteller    Mengeneinheit    UmrechnungsfaktorBestellmengeneinheit    Bestellmengeneinheit    Artikelklasse    Artikelgruppe    Verkaufspreisbasis    Artikellieferant    ArtikelnummerLieferant    ArtikelbezeichnungLieferant    EinkaufspreisLieferant    RabattLieferant    NettopreisLieferant    LagerplatzHauptlager    Breite    Hoehe    Tiefe    Gewicht    Materialgewicht    Material    LagerMindeststand    LagerSollstand

Fertigungssatzgroesse    Verschnittfaktor    Ueberproduktion    Mindestdeckungsbeitrag    Warenverkehrsnummer    Chargennummerngefuehrt    Seriennummerngefuehrt    Lagerbewirtschaftet
Wiederbeschaffungszeit    Mindestbestellmenge    Verpackungseinheit    Standardmenge

Kommentarart    Kommentar    MitdruckenAngebot    MitdruckenAnfrage    MitdruckenStueckliste    MitdruckenAuftrag    MitdruckenBestellung    MitdruckenFertigung    MitdruckenLieferschein    MitdruckenWareneingang    MitdruckenRechnung    MitdruckenEingangsrechnung

Gestehungspreis

Achtung: der Gestehungspreis kann nur importiert werden, wenn der Lagerstand des Artikels aller Läger 0  ist.

Preiseinheit 

VK-Preis/EK-Preis + Gestehungspreis wird durch diese Preiseinheit dividiert

BreiteText RasterStehend RasterLiegend StromverbrauchTypisch StromverbrauchMaximal AufschlagBetrag AufschlagProzent Bauform Verpackungsart Hochsetzen Hochstellen Polarisiert AntistaticMassnahmen Reach Rohs Automotive Medical UL Farbcode ECCN

Mwstsatzbez    VerkaufsEAN     VerpackungsEAN    Verpackungsmenge

Die Anordnung der Spalten hat keinen Einfluss auf den Import.
Eine Spalte die nicht definiert ist, bewirkt keine Änderung im Artikelmodul.
Wenn der Inhalt einer Spalte leer ist, erfolgt kein Import, eventuell vorhandenen Daten werden nicht überschrieben.

**Hinweis:** Musterdateien dazu finden Sie neben dem Report-Root-Verzeichnis auf Ihrem **Kieselstein ERP** Server.

Zusatzinfo: Wenn im Importdialog
![](Artikel_Import.jpg)
der Haken bei bestehende Artikel überschreiben geändert wird, wird erneut eine Prüfung auf die erhaltenen Daten durchgeführt. Daher kann die Reaktion auf das setzen / löschen des Hakens je nach Menge der Daten etwas dauern.

#### Nach dem Import sehe ich teilweise komische Zeichen
Der Import muss im Format UTF 8 erfolgen. (Eigentlich spricht man hier von Kodierungen von denen es leider sehr viele gibt.) Wird der Import in anderen Formaten/Kodierungen durchgeführt, so kann es sein, dass im Client in der Auswahlliste dies richtig angezeigt wird ![](Zeichenkodierung1.jpg) aber unten in den Details wird ![](Zeichenkodierung2.jpg) ein Fragezeichen bzw. wird beim Druck ein Kästchen ![](Zeichenkodierung3.gif) für unbekanntes angezeigt.
Korrigieren Sie diese ungültige Zeichenkodierung mit einem Texteditor der diese Konvertierungen durchführen kann, z.B. Notepad++.
**Kieselstein ERP** verwendet deshalb UTF8 um dies auf allen Plattformen richtig darstellen zu können.

#### Können Artikellieferanten importiert werden
Ja, [siehe]( {{<relref "/docs/stammdaten/lieferanten/#import-von-lieferantenartikeldaten">}} ).

#### Wo sehe ich Renner und Penner, Hitliste, die meist verkauften Artikel?
Unter Journal, Hitliste finden Sie die entsprechende Auswertung.
Damit steht Ihnen auch annähernd die sogenannte ABC Analyse der Artikel zur Verfügung. Siehe dazu auch [Wikipedi*a*](http://de.wikipedia.org/wiki/ABC/XYZ-Analyse). Hier wird wiederum die Parettoregel angewandt. A-Artikel sind jene die einen Wertanteil im Einkauf von 70-80% haben. B-Artikel einen von 15-20%, C-Artikel einen von 5-10%.
Die XYZ Artikel, also die Unterscheidung auf Dauerartikel, Saisonartikel bzw. Sonderangebote ist in unserem Umfeld bei einer guten Stücklistenabdeckung nicht erforderlich, da die tatsächlichen Bedarfe sich aus den Kundenaufträgen und der daran angehängten Stücklistenauflösung in den Fertigungsaufträgen (Lose) ergeben. Das Ziel einer guten Fertigungssteuerung muss sein, bei relativ niedrigem Lagerstand eine optimale Verfügbarkeit der Produkte zu erreichen.

#### Verbrauchsanalyse, ABC Artikel
Für eine qualifizierte Steuerung des Einkaufes wird auch gerne eine ABC Analyse auf den Wareneinsatz herangezogen.<br>
Mit einer Formularvariante der Hitliste (Artikel, Journal, Hitliste), die wir Ihnen gerne zur Verfügung stellen, sehe Sie Ihre Artikel nach der Pareto-Regel sortiert und in ABC Gruppen untergliedert.<br>
Zusätzlich werden offene Bestellmengen, offene Reservierungen und offene Fehlmengen mit angeführt, sowie die Reichweiten des Lagerstandes und der offenen Bestellungen.
Damit sehen Sie, ob Ihr Einkauf das Material / das Lager im Griff hat.
[Siehe]( {{<relref "/docs/installation/10_reportgenerator/reportsammlung">}} )

#### Zusammengehörige Artikel
Dieses Zusatzfunktionsberechtigung ist eine reine Sicherheitsfunktion um zu erreichen, dass die richtigen Artikelkombinationen in den Aufträgen zusammengestellt werden. Diese wird nicht weiter unterstützt.

#### Wieviel Tonnen Stahl wurden dieses Jahr verarbeitet?
Diese Fragen werden gerne zum Jahresende gestellt. D.h. diese Auswertung finden Sie, unter der Voraussetzung, dass bei den Artikel die Bestellmengeneinheitsübersetzung von z.B. m(Meter) auf kg hinterlegt ist, ebenfalls in der Hitliste. Dafür muss diese nach Artikelgruppe sortiert sein. Am Ende jeder Artikelgruppe werden auch die Gesamt-Gewichtsmengen angedruckt.
![](Hitliste_mit_Gewicht.gif)

#### Wo finde ich die Auswertung nach Artikelgruppen?
Unter Journal, Artikelgruppe finden Sie die entsprechende Auswertung.

Hier werden die Artikel gruppiert nach Vatergruppe und Untergruppen dargestellt. In der übergeordneten Vatergruppe sind jeweils die Umsätze der enthaltenen Untergruppen mit enthalten.

In der Artikelgruppe unbekannt sind diejenigen Artikel enthalten, welche keiner Artikelgruppe zugeordnet sind.

#### In der Statistik, in der Hitliste werden Einstands Gestehungspreise mit 0,00 ausgewiesen, wo kann ich das ändern ?
Die Einstandspreise und daraus die Gestehungspreise werden bei der jeweiligen Zugangsbuchung eingetragen. Um Einstandspreise gezielt zu korrigieren, muss die verursachende Buchung gefunden werden. Dies geht am Einfachsten über die Artikelstatistik. D.h. auswählen des Artikels und Info Statistik. Gegebenenfalls ist der Zeitraum entsprechend einzustellen. Nachdem Sie die auslösende Buchung herausgefunden haben, wechseln Sie bitte in das entsprechende Modul und korrigieren Sie die Preise entsprechend. Z.B. Bestellung, Wareneingang, Wareneingangsposition. Hier kann es unter Umständen erforderlich sein, erledigte Wareneingänge wieder zurückzunehmen. Eventuell sind auch entsprechende Berechtigungen usw. erforderlich.

<a name="Handlagerbewegung_aendern"></a>

#### Wie kann der Preis einer Handbuchung geändert werden.
Auch eine Handbuchung kann nachträglich geändert werden. Wählen Sie im Artikel den unteren Modulreiter Handlagerbewegungen und wählen Sie die zu korrigierende Handbuchung aus. Verwenden Sie dafür die Direktfilter. Nun Klicken Sie auf Ändern und korrigieren Sie den Preis wie gewünscht.

**ACHTUNG:**

Dadurch können auch Lagerwerte zu Stichtagen, Werte von Halbfertigfabrikatsinventuren usw. verändert werden.

#### Bei der Handlagerbewegung wird immer das falsche Lager vorgeschlagen
Das für die Handlagerbewegung vorgeschlagene Lager wird in den System-Parameter unter DEFAULT_LAGER definiert.
Hinweis: Es ist hier die ID des Lagers einzutragen. Diese teilt ihnen ihr **Kieselstein ERP** Betreuer gerne mit.

#### Wieso werden bei der Handlagerbewegung bei der Lagerauswahl nur wenige / keine Läger angezeigt?
Da man bei der Handlagerbuchung bei der Abbuchung vom Lager oft auch die Lagerstände des Artikels am Lager erkennen will, werden bei der Lagerabbuchung als auch bei der Umbuchung bei Von Lager nur diejenigen Läger angezeigt, die auch einen Lagerstand haben. Es wird hier jedes Lager mit dem entsprechenden Lagerstand angezeigt.

#### Kann die Lager-Bewegung der Handlagerbewegung vordefiniert werden?
Ja. Stellen Sie bitte den Parameter DEFAULT_HANDBUCHUNGSART entsprechend ein.

#### Journale die zusammenstimmen.
Manchmal stellt sich die Frage, ob bestimmte Journale nicht zusammenstimmen / die gleichen Ergebnisse liefern sollten. Wir haben hier einige Journale ausgewählt, die für diese Betrachtung oft herangezogen werden und deren Unterschiede beschrieben:

a.) Verkauf
| Journal | Beschreibung |
| --- |  --- |
| Hitliste | Die Hitliste liefert nur Artikelumsätze zum Belegdatum. Daraus können sich zwei Unterschiede ergeben:a.) zum Rechnungsjournal: es kann hier Abweichungen aufgrund des Lieferschein / Rechnungsdatum geben (LS wurde vor dem Betrachtungszeitraum erstellt)b.) In der Hitliste fehlen generell die HandeingabenHinweis: Gutschriften (Wert- und Menge) werden in der Hitliste berücksichtigt. |
| Warenausgangsjournal | Beinhaltet Rechnungspositionen und noch nicht verrechnete Lieferscheinpositionen. Der Betrachtungszeitraum ist immer das Belegdatum. |
| Kundenumsatzstatistik | Bezieht sich auf das Datum der Rechnung / Gutschrift und ist ohne den Lieferschein-Positionen die (noch) nicht verrechnet wurden. |
| Rechnungsjournal | Ebenfalls nach Belegdatum der Rechnung und beinhaltet auf Wunsch auch die Gutschriften. |

b.) Einkauf
| Journal | Beschreibung |
| --- |  --- |
| LieferantenumsatzstatistiknachWareneingangsposition | Liefert die Umsätze aus den Wareneingängen auf Basis des Wareneingangsdatums und des Einstandspreises.Im Einstandspreis sind auch die Fixkosten bzw. Transportkosten anteilsmäßig enthalten. |
| Wareneingangsjournal | Beinhaltet die Wareneingänge zur Basis des Wareneingangsdatums und des Einstandspreises. |
| LieferantenumsatzstatistiknachEingangsrechnung | Liefert die Umsätze aus den Eingangsrechnungen auf Basis der Nettowerte der Eingangsrechnungen und zum Eingangsrechnungsdatum |
| Eingangsrechnungsjournal | Ebenfalls nach Eingangsrechnungsdatum der Eingangsrechnung. |

Alle Journale liefern die Umsätze in Mandantenwährung.

Manchmal stellt sich die Frage, ob / warum bestimmte Journale nicht zusammenstimmen / nicht die gleichen Ergebnisse liefern

<a name="Verwendungsnachweis"></a>

#### Sehe ich auch wo welche Artikel verwendet werden?
In welchen Stücklisten welche Artikel verwendet werden sehen Sie im Artikel unter Info, Verwendungsnachweis. Hier werden alle Stücklisten und deren Mengen aufgeführt, in denen der gewählte Artikel vorkommt.

Zusätzlich werden hier neben den verschiedenen Stücklistenarten auch die Angebotsstücklisten mit angeführt. Hier wird unter Stückliste die Angebotsstücklistennummer (AS 09/1234567) angeführt.

Als zusätzliche Information kann der Verbrauch der jeweiligen Stückliste eines bestimmten Zeitraums mit ausgegeben werden. Haken Sie dazu ![](Verwendungsnachweis1.gif) an und geben Sie den gewünschten Zeitraum an.

Im Verwendungsnachweis werden auch die Artikel aus den Arbeitsplänen der Stücklisten mit angeführt. Diese sind an führender Position im Formular mit einem A gekennzeichnet.

Unter letzte Verwendung wird angeführt, wann die jeweilige Stückliste zuletzt an Kunden verkauft wurde bzw. zuletzt in Lose verbraucht / verbaut wurde. Damit haben Sie einen raschen Überblick, welche Stücklisten tatsächlich von z.B. Ihrer Artikel Änderung relevant betroffen sein könnten. Bitte beachten Sie, dass die Ermittlung des Datums der letzten Verwendung derzeit nicht durch den für den Verbrauch angegebenen Zeitraum begrenzt wird.
Um nun auch die übergeordnete Stückliste bis rauf in das eigentliche Gerät / Produkt, also die Kopfstückliste, zu sehen, haken Sie bitte noch mit Hierarchie an. Damit wird die Verwendung des jeweils übergeordneten Produktes mit angezeigt.
![](Verwendungsnachweis.jpg)

Lagerbewegungen mit Ursprungsland und Hersteller

In einigen Branchen ist es erforderlich, dass auf jeder Warenbewegung das Warenursprungsland und der Hersteller angegeben werden. Aktivieren Sie dazu den Mandanten Parameter LAGERBEWEGUNG_MIT_URSPRUNG.

Nun kann bei jeder Warenzubuchung der Hersteller und das Ursprungsland angegeben werden.

Um einen Hersteller zu definieren, muss dieser im Modul Artikel, unterer Modulreiter Grunddaten, oberer Modulreiter Hersteller definiert sein. Da ein Hersteller auch immer ein Partner ist, muss bevor die Definition eines Herstellers erfolgen kann, dieser als Partner definiert sein.

Um nicht für alle Hersteller einen Partner anlegen zu müssen, können Sie z.B. einen Partner Hersteller allgemein anlegen und diesen für die Partnerzuordnung verwenden. Daraus ergibt sich auch, dass die weiteren Adressdaten des Herstellers nicht zur Verfügung stehen.

Von unserer Seite ist dies so angedacht, dass jeder Hersteller ein eigenständiger Partner ist, da es durchaus auch vorkommt, dass Sie direkt mit den Herstellern in Kontakt treten und diese daher auch eine Adresse, Ansprechpartner usw. haben.

Für die Angabe des Ursprungslandes werden die unter System definierten Länder verwendet. Aus Zolltechnischen Gründen wird hier meistens die DIN EN ISO 3166-1 mit den zweistelligen Buchstabencodes verlangt. Das bedeutet: Definieren Sie bitte die Länderkennzeichen entsprechend.

Diese Daten stehen für jede Warenbewegung, soweit angegeben, zur Verfügung und können auch mit ausgedruckt werden. Der eigentliche Druck muss in den Formularen eingerichtet werden. Wenden Sie sich dazu bitte an Ihren **Kieselstein ERP** Betreuer.

## Artikellieferant
### Fixpreis, Gültig ab, Gültig bis
#### Gültig bis
<a name="Lieferantenpreis_gueltig_Bis"></a>
Gültig bis ist dafür gedacht, dass in der Verkaufspreis-Findung eine Warnung ausgegeben werden kann, wenn der Preis Ihres bevorzugten Lieferanten nicht mehr gegeben ist, also z.B. die Bindefrist Ihres Lieferanten abgelaufen ist.

Die Warnung wird bei der Verkaufspreiserfassung ausgegeben.

Beim Angebot wird geprüft, ob die Preisgültigkeit bis zum Ende der Angebotsgültigkeit gegeben ist, bei allen anderen Belegen wird das Belegdatum herangezogen.

## Bewegungsvorschau
Für jeden einzelnen Artikel steht auch die Bewegungsvorschau zur Verfügung. Hier sehen Sie die geplanten Warenbewegungen des Artikels sortiert nach Termin. Aufgrund des aktuellen Lagerstandes und der jeweiligen positiven oder negativen Lagerbewegung ergibt sich der fiktive Lagerstand zum Termin.

Um die Berücksichtigung oder nicht Berücksichtigung der Läger für die Bewegungsvorschau zu signalisieren, wird mit IB signalisiert, dass dieses Lager bei der Berechnung des Internen Bestellvorschlages berücksichtigt wird. Mit BV wird signalisiert, dass dieses Lager bei der Berechnung des Bestellvorschlages berücksichtigt wird.

Das Verhalten des jeweiligen Lagers kann im Artikel, Grunddaten, Lager definiert werden.

Anbindung externer Lagersysteme

Siehe dazu [Paternoster]( {{<relref "/docs/stammdaten/artikel/paternoster" >}} ).

Standardbezeichnung

Oft ist es einfach praktisch, dass möglichst alle Artikel nach dem gleichen Schema benannt werden. Wir haben daher in den Artikel Grunddaten die Möglichkeit geschaffen, Standardbezeichnungen für die Artikel zu hinterlegen.

Definieren Sie hier die verschiedenen gewünschten Texte für Ihre Artikel.

Sind hier Texte für die Bezeichnungen hinterlegt, so werden im Artikel Detail anstelle der Beschriftung der Bezeichnungsfelder, Buttons angezeigt. Durch Auswahl eines hinterlegten Texte über den jeweiligen Button wird der Text in die Bezeichnungszeile übernommen. ![](Standardbezeichnung.gif). ** ACHTUNG:** Ein eventuell bereits eingegebener Text wird damit überschrieben.

#### Externe Dokumente
<a name="Externe Dokumente"></a>
[Siehe bitte]( {{<relref "/docs/stammdaten/system/dokumentenablage/#definition-von-absoluten-pfaden-im-dokumentenlink" >}} )

#### Artikel von zertifizierten Lieferanten, Zertifizierungspflichtige Artikel
<a name="Zertifizierungspflichtige Artikel"></a>
In verschiedenen Branchen, z.B. der Luftfahrt-Branche ist zwingend gefordert, dass Ihre Zukaufsartikel nur von zertifizierten Lieferanten geliefert werden dürfen. Das bedeutet, dass ohne Zertifikat keine Wareneingangsbuchung bei Zertifizierungspflichtigen Artikeln erfolgen dürfen.
Um nun einen Artikel als Zertifizierungspflichtig zu kennzeichnen, haken Sie in der Definition der Artikelgruppen (Artikel, Grunddaten, Artikelgruppen) die Zertifizierung an.
![](Zertifizierung1.gif)

Im Lieferanten können Sie dessen Freigabe im Reiter Konditionen unter ![](Zertifizierung2.gif) hinterlegen. Bei bereits hinterlegten Freigaben sehen Sie rechts daneben, wer wann diesen Lieferanten freigegeben hat.
Wichtig: Wird die Freigabe befüllt, muss auch ein Datum eingetragen werden. Wird die Freigabe gelöscht, so muss auch das Freigabedatum gelöscht werden.

Die bewirkt nun, dass:

-   wird eine Anfrage oder eine Bestellung an einen nicht zertifizierten (freigegebenen) Lieferanten gerichtet und werden zertifizierungspflichtige Artikel bei diesem Angefragt bzw. bestellt, so erscheint bei jedem dieser Artikel ein Hinweis, dass der Lieferant nicht zertifiziert ist.

-   wird versucht auf einen nicht zertifizierten Lieferanten ein Wareneingang zu buchen, so wird dies mit einem entsprechenden Hinweis verweigert. Die Idee ist, dass der Lieferant vor der Lieferung seiner Teile, noch rechtzeitig das Zertifikat mit der (Erst-)Lieferung beistellt.

 Zusätzlich steht unter Journal, Lieferantenliste eine Auswertung zur Verfügung, mit der Sie eine Übersicht über Ihre Lieferanten mit deren Freigaben erhalten. In dieser Liste werden auch die Liefergruppen der Lieferanten angeführt, und für welche der Liefergruppen der Lieferant eingetragen ist. **Hinweis:**
Es werden nur Positionen überprüft, die Ident-Positionen also Artikel sind. Handeingaben werden nicht geprüft. Um eine saubere Überprüfung zu erreichen, sollte daher die Verwendung von Handeingaben in der Bestellung abgeschaltet werden. Setzen Sie dazu unter Bestellung, Grunddaten, Bestellpositionsart die Handeingabe auf versteckt. **Info:**
Diese Funktion wird automatisch aktiviert, wenn die Artikelgruppe als Pflichtfeld (Parameter: ARTIKELGRUPPE_IST_PFLICHTFELD) aktiviert ist. Das bedeutet, dass damit bei jedem Wareneingang erzwungen wird, dass ein Artikel einer Artikelgruppe zugewiesen ist.

#### Die Artikelliste wird teilweise in blau angezeigt, was bedeutet das?
Damit wird signalisiert, dass dieser Artikel nicht Lagerbewirtschaftet ist.

#### Abschalten der Lagerbewirtschaftung
Da es sich in der Praxis herausgestellt hat, dass es immer wieder erforderlich ist, dass Artikel von Lagerbewirtschaftet auf NICHT Lagerbewirtschaftet umgestellt werden können müssen, haben wir diese Funktion eingebaut.
Bitte beachten Sie, dass diese Funktion **<u>auf keinen Fall rücknehmbar</u>** ist.
Es wurden dazu auch zwei Sicherheitsabfragen eingebaut. Erst nachdem beide entsprechend beantwortet wurden, wird die Lagerbewirtschaftung des Artikels abgeschaltet.

#### Anzeige der Verkaufspreisentwicklung
Unter Info, Verkaufspreisentwicklung sehen Sie im Artikelmodul für den ausgewählten Artikel alle Änderungen die an den Verkaufspreisen des Artikels vorgenommen wurden.

#### Protokollierung der Änderungen der Artikelkopfdaten
Unter Info, Änderungen sehen Sie die Änderungen welche an den wichtigsten Feldern des Artikelstammes durchgeführt wurden. Diese sind: Artikelnummer, Kurzbezeichnung, Bezeichnung, Zusatzbezeichnung, Zusatzbezeichnung2, Revision, Index, Einheit, Umrechnungsfaktor zur Bestellmengeneinheit, Bestellmengeneinheit, Artikelart, Hersteller, MwSt Bezeichnung.

#### Können aus dem Artikel direkt Stücklisten erzeugt werden?
Ja. Sie finden im Artikeldetail, direkt neben der Artikelnummer die mit ![](Stueckliste_erzeugen.gif) Stückliste erzeugen beschrifteten Knopf. Durch Klick auf diesen wird für den Artikel eine Stückliste mit Standardeinstellungen angelegt. Und Sie werden automatisch in das Modul Stücklisten geführt, in den Reiter Kopfdaten der soeben neu erzeugen Stückliste, um diese entsprechend fertigzustellen.

**Hinweis:**

Wird an dieser Stelle ![](In_Stueckliste_springen.gif) mit dem Goto-Button angezeigt, so ist hinter diesem Artikel bereits eine Stückliste definiert. D.h. durch Klick auf den Goto Button kommen Sie direkt in die Kopfdaten der Stückliste.

#### Werbeabgabepflichtig?
Dieser Artikel ist Werbeabgabepflichtig. Zur Beschreibung dieser Spezialität des österreichischen Steuerrechtes [siehe]( {{<relref "/verkauf/rechnung/#werbeabgabe" >}} ).

#### Was ist ein kalkulatorischer Artikel ?
<a name="Kalkulatorischer Artikel"></a>
Mit kalkulatorisch stehen Artikel für Angebot oder Auftrag zur Verfügung, welche in aller Regel nur zu Kalkulationszwecken in den Auftrag/das Angebot eingepflegt werden.
Kennzeichnen Sie dazu den Artikel im Reiter Detail als ![](Kalkulatorischer_Artikel.gif) kalkulatorisch.
Dies bewirkt, dass diese Artikel zwar in ein Angebot bzw. einen Auftrag eingefügt werden können, diese werden jedoch NICHT mitgedruckt, da diese Artikel als Plankostenbetrachtung gesehen werden.
Ein Beispiel:
Sie bieten Ihrem Kunden eine Anlage zu einem Pauschalpreis an.
Trotz des Pauschalpreises haben Sie natürlich eine Kalkulation hinterlegt, welche ergibt dass sich dieses Angebot rechnen sollte. Selbstverständlich wollen Sie diese Kalkulatorischen (Plan-) Kosten in der Angebotsvorkalkulation sehen, der Kunde sollte diese Werte auf seinem Angebot nicht sehen.
Daraus ergibt sich auch, dass diese Artikel immer einen Verkaufspreis von 0,00 haben. Wir unterdrücken daher auch die Null-Preis und Mindestpreis Meldungen.
Erfolgt nun die Auftragserteilung so werden diese Artikel vom Angebot mit in den Auftrag kopiert.
Bei der Überleitung vom Auftrag in den Lieferschein in die Rechnung werden diese nicht mehr mit übernommen, da diese Kosten z.B. durch den Zeitaufwand Ihrer Mitarbeiter oder durch Buchungen auf Fertigungslose usw. entstehen.
Für eine komfortable Verwendung siehe bitte [Auftragsnachkalkulation]( {{<relref "/verkauf/auftrag/nachkalkulation" >}} ) bzw. [dazugehöriger Artikel](#zugehoeriger Artikel).
Bitte beachten Sie gegebenenfalls die Voraussetzungen für den [Abrechnungsvorschlag]( {{<relref "/verkauf/rechnung/abrechnungsvorschlag/#bei-der-%c3%bcberleitung-erscheint-die-fehlermeldung-dass-diese-abgebrochen-wird" >}} ).

#### gibt es eine Verbindung zwischen dazugehörigem und kalkulatorischem Artikel?
Ja, insbesondere für die Auftragsnachkalkulation ist diese von Bedeutung. Siehe bitte [Nachkalkulation]( {{<relref "/verkauf/auftrag/nachkalkulation" >}} ).

#### Manche Mitarbeiter sehen verschiedene Journale im Artikel nicht?
Um z.B. Lagerstandslisten, Hitlisten usw. einsehen zu können, sind oft auch die Rechte LP_DARF_PREISE_SEHEN_EINKAUF / LP_DARF_PREISE_SEHEN_VERKAUF erforderlich. Dies hängt vor allem damit zusammen, dass der Mitarbeiter der dieses Auswertungen sehen darf, eben auch die Werte / Preise sehen muss. Hat er das Recht nicht um die Preise zu sehen, werden daher auch die Journale nicht angeführt.
Beachten Sie in diesem Zusammenhang auch das Recht LP_FINANCIAL_INFO_TYP_1\. Mitarbeiter die dieses Recht haben, sehen im Artikel-Journal auch noch die Journale:
Artikelgruppen, Shopgruppen, Hitliste und Gestehungspreis über MinVK / VKpreis

Da in den verschiedensten Anwendungen natürlich das Serien-/Chargennummern Journal wichtig ist, auch wenn die Mitarbeiterin keine Preise sehen darf, steht wenn der Mitarbeiter die Rechte LP_DARF_PREISE_SEHEN_EINKAUF / LP_DARF_PREISE_SEHEN_VERKAUF nicht hat, das Serien-/Chargennummern Journal trotzdem zur Verfügung.

#### Können Artikeletiketten gedruckt werden?
Es können sowohl Etiketten eines einzelnen Artikels als auch mehrere Artikel gedruckt werden.
Aus der Artikelauswahl können durch Klick auf ![](Artikeletikett_drucken.gif) bzw. drücken von STRG+Shift+P die Artikeletikette des ausgewählten Artikels gedruckt werden. Alternativ steht dafür auch Info, Etikette zur Verfügung.<br>
Mit Journal, mehrere Etiketten drucken, können mehrere Artikel (von - bis Artikelnummer usw.) gedruckt werden. Es sind dafür die Rechte LP_DARF_PREISE_SEHEN_EINKAUF und LP_DARF_PREISE_SEHEN_VERKAUF erforderlich.

#### Prüfmittelverwaltung
<a name="Prüfmittelverwaltung"></a>
Es ist immer wieder erforderlich, dass die eingesetzten Messmittel mittels Prüfmittel entsprechenden Eichungen unterzogen werden, dass diese kalibriert werden bzw. entsprechende Wartungen durchgeführt werden.
Dies ist oft damit kombiniert, dass diese Artikel auf eigenen Lagern / Lagerplätzen liegen und eben intern oder extern geeicht werden. Zusätzlich sollte ersichtlich sein, welche, meistens externen Arbeiten, an diesen Messmitteln durchgeführt wurden.
In **Kieselstein ERP** können Sie diese Aufgabe wie folgt lösen:
Definieren Sie im Modul Artikel, Grunddaten, Artikelgruppe eine Gruppe für diese Art von Produkten. Diese Definition dient nur der einfacheren Bedienung und hat auf die eigentliche Funktion keinen Einfluss.
Legen Sie für jedes Messmittel einen eigenen Artikel an. Verwenden Sie dafür einen sprechenden Artikelnummernkreis z.B. MM (für Messmittel). Vergeben Sie beim ersten Artikel z.B. eine fünfstellig Nummer um danach den [Artikelnummerngenerator](#Artikelnummerngenerator) verwenden zu können.
Nach erfolgter Definition des Messmittels wechseln Sie in den Reiter Sonstiges und definieren das Wartungsintervall in Monaten.
Dadurch erscheint der Artikel automatisch im [Journal nächste Wartungen](#Journal nächste Wartungen).
Wurde die Kalibrierung / Wartung für das Messmittel durchgeführt, so tragen Sie dies beim kalibrierten Messmittel im Reiter Sonstiges unter Letzte Wartung mit dem Kalibrierdatum ein.
Nach der Speicherung erscheint neben dem letzten Wartungsdatum auch das Kurzzeichen der Person die das Wartungsdatum eingetragen hat.
D.h. hier ist die letzte Wartung ersichtlich. Für die Anzeige der davor liegenden Wartungsdatum verwenden Sie bitte Info, Änderungen.

Das Kalibrierungsprotokoll sollte als Dokument dieses Artikels in der Dokumentenablage, [Belegart]
( {{<relref "/docs/stammdaten/system/dokumentenablage/#belegart" >}} ) Kalibrierung, abgelegt werden. **Hinweis:** Der hier verwendete Begriff Messmittel gilt natürlich für jegliche Art von Betriebsmitteln, wiederholenden Prüfungen usw.

### Journal der nächsten fälligen Wartungen:
<a name="Journal nächste Wartungen"></a>
Dieses finden Sie im Artikel, Journal, nächste Wartungen.

![](naechste_Wartungen.gif)

In diesem sind alle Artikel enthalten, bei denen ein Wartungszeitraum enthalten ist mit deren nächsten Prüfterminen.

#### Wann genau erscheint ein Artikel im Journal nächste Wartungen ?
Dies ist ein Journal für die bereits durchzuführenden Wartungen. D.h. in diesem Journal erscheinen diejenigen Artikel, die anhand dem Datum der Letzten Wartung plus dem Wartungsintervall in Monaten zum Zeitpunkt des Drucks des Wartungsjournals gewartet werden sollen. Das bedeutet:
- ist bei einem Artikel zwar ein Wartungsintervall aber kein Datum der letzten Wartung hinterlegt, so wird er in der Liste angeführt.
- Ist bei einem Artikel das nächste Wartungsintervall überfällig, so wird er ebenfalls in der Liste mit angeführt.

### Lagerzubuchungsautomatik bei unzureichendem Lagerstand
<a name="Zubuchungsautomatik"></a>
Besonders beim Start des Einsatz von **Kieselstein ERP** stellt sich die Frage, wie damit umgegangen wird, dass man Lieferscheine / Rechnungen schreiben muss, die Lagerstände in **Kieselstein ERP** aber noch nicht stimmen. Einerseits können Sie per Handlagerbuchung die benötigten Artikel zubuchen, den Gestehungspreis dafür festlegen und so den Lagerstand korrigieren.<br>
Die andere Möglichkeit ist, dass diese Lagerzubuchung automatisch erfolgt. Hierzu wird der Parameter LAGER_IMMER_AUSREICHEND_VERFUEGBAR auf 1 gesetzt.<br>
Das bewirkt, dass wenn kein ausreichender Lagerstand gegeben ist, im Hintergrund der Lagerstand entsprechend angepasst wird.<br>
**Achtung:** Die Reihenfolge der Einstandspreispreisfindung, wenn ein Artikel aufgrund des Parameters IMMER_AUSREICHEND_VERFUEGBAR zugebucht wird, ist wie folgt:
1. Es wird der Gestehungspreis zum Belegdatum (des LS gesucht) und nur dann verwendet, wenn dieser weder NULL(=Nicht vorhanden) oder 0 ist. 
2. Wenn kein Gestehungspreis zum Zeitpunkt gefunden wird, dann wird der aktuelle Gestehungspreis verwendet.
3. Wenn der Gestpreis NULL oder 0 ist, dann dann wird der Lief1Preis zum Belegdatum gesucht.
4. Wenn dieser wieder NULL oder 0 ist, dann wird der VK-Preis verwendet.

Ab dem Zeitpunkt ab dem die Lagerwirtschaft aktiv verwendet wird, wird die Lagerzubuchungsautomatik wieder abgeschaltet indem der Parameter auf 0 gestellt wird.

#### Wo erhält man einen Überblick über den Wareneinsatz?
Sie finden im Modul Artikel im Menü Journal den Report Indirekte Wareneinsatzstatistik, diese Auswertung zeigt den kundenbezogenen Materialeinsatz.
Arbeitszeitartikel sind nicht enthalten, da dies Zeiten sind und keine Ware.
Somit beinhaltet die Statistik alle Materialien, die über Fertigung und Lieferscheinen für diesen Kunden gebucht wurden. Die Gegenüberstellung von Einkaufspreis, Gestehungspreis und Kunden-Verkaufspreis auf Basis 1Stk ermöglicht Ihnen einen schnellen Überblick über die Werte.
Zur Nachverfolgbarkeit wird der Wareneingangsbeleg (Bestellung, Losablieferung) angeführt. Wenn der Ursprung eines Artikels in LS/RE/GS eine Losablieferung ist, dann wird das gesamte Material des Loses auf dessen Ursprung geprüft. Hier erfolgt eine Auflösung über alle Ebenen der Lose, sobald ein Artikel aus einem Los kommt, wird auch die Ebene tiefer überprüft. 

#### Wie kennzeichnet man Vorzugsteile?
Zur Kennzeichnung von Artikel als Vorzugsteil definieren Sie im unteren Reiter Artikelgrunddaten, oberer Reiter Vorzugsteil die Grundeinstellungen. Somit erhalten Sie eine Combo-Box in den Artikel-Kopfdaten. Wenn Sie hier nun Vorzugsteile definiert haben, so wird in der Auswahlliste der Artikel eine Spalte mit den Informationen angezeigt.
##### Wozu wird diese Kennzeichnung benötigt, wo wirkt sie?
Die Kennzeichnung von Artikeln als Vorzugsteile dient vor allem Ihrer internen Steuerung der Verwendung der Artikel.<br>
D.h. wenn neue Stücklisten von der Technik / Entwicklung erstellt werden, so hat man oft die Qual der Wahl aus mehreren annähernd gleichwertigen Artikeln den "richtigen" auszuwählen. Hier kann die Vorzugsteile Definition helfen. D.h. wann immer möglich sollten die Vorzugsteile zum Einsatz kommen.<br>
Beachten Sie bitte, dass mit dem Vorzugsteil auch der automatische Ausdruck der Los-Ablieferetikette über das [Zeiterfassungsterminal]( {{<relref "/fertigung/zeiterfassung/ze_terminal/#ablieferung-mit-info-text" >}} ) gesteuert werden kann.

#### Wie findet man am schnellsten die Kundenartikelnummern zu einem Artikel?
Im Reiter Detail von Artikel finden Sie in der Icon-Leiste einen GoTo Button ![](goto_kundenartikelnummer.png).
Wenn Sie nun auf diesen Button klicken, so wechseln Sie in den Reiter Kd.Artikelnummer und die Artikelnummer des vorherigen Artikels ist als Filter vorbelegt. So erhalten Sie sofort eine Liste der Artikel, die eine Kundenartikelnummer zu dem Artikel hinterlegt haben. 
Nutzen Sie auch den Reiter Sonderkonditionen im Artikel.

#### Wo erhält man einen Überblick über Artikel, die nicht in Stücklisten verwendet werden?
Sie finden eine Liste i Modul Artikel/Journal/"Artikel ohne Stkl-Verwendung". Hier sind alle Artikel der Artikelart Artikel enthalten (keine Arbeitszeitartikel), die nicht in Stkl-Positionen verwendet werden. Wenn der Artikel selbst eine Stkl ist, scheint dieser nur in der Liste auf, wenn er fremdgefertigt wird.

#### Was bedeutet Kommissionieren?
Diese Definition wird derzeit nur für die ergänzende Definition der Linienabrufe aus dem Modul Forecast verwendet. [Siehe bitte]( {{<relref "/verkauf/forecast/#kann-ich-artikel-als-laufend-zu-liefern-kennzeichnen" >}} ).

## Gebindeverwaltung
=================================================
<a name="Gebindeverwaltung"></a>

Mit der Gebindeverwaltung steht für den Wareneingang die Abbildung der Gebinde zur Verfügung. Das bedeutet dass:
- in den Artikel Grunddaten die Basisdefinition für die verwendeten Gebinde erstellt werden muss (oberer Reiter ![](gebinde_reiter.PNG))
- der gebindegeführte Artikel chargengeführt sein muss (Artikel oberer Reiter Sonstiges Haken bei Chargennummer Zwangseingabe setzen![](chargennummer_zwangseingabe.PNG))
- im Artikellieferanten je Gebinde ein eigener Eintrag und somit eine eigene Lieferantenartikelnummer und ein eigener Preis gegeben ist ![](gebinde_definition_artikellieferant.PNG)
- die Gebindemenge für den Artikellieferanten wird als Vorschlagswert aus dem Gebinde übernommen, kann jedoch bei gleichen Gebinden und unterschiedlichen Lieferanten auch unterschiedlich sein, obwohl die Gebindebezeichnung gleich ist ![](gebindemenge_vorschlag.PNG).
- Obwohl einzelne Artikellieferanten gebindegeführt sind, müssen nicht alle Artikellieferanten des Artikels gebindegeführt sein.

In der Bestellung gibt es für die Gebindeanzahl/-menge einen eigenen Mengenerfassungsdialog. 

![](Gebinde_dialog_bestellung.PNG)

Hier wird aus der Gebindeanzahl und dem gewählten Gebinde die Positionsmenge errechnet. In der Bestellung werden zusätzlich Gebindetext, -menge, -anzahl mit angedruckt. 

Im Gebinde-Dialog gibt es einen Button "kein Gebinde" mit dem das Gebinde zurückgesetzt werden kann. Mit Klick auf das Icon ![](gebinde_icon.PNG) können Sie den Gebindedialog erneut aus der Bestellposition aufrufen.

Da die Gebindemenge je Bestellposition unterschiedlich sein kann, wird diese auch in der Bestellposition mit hinterlegt.
Bei der Warenzubuchung (Wareneingangsposition (WEP)) wird ein eigener Gebindezubuchungsdialog aufgerufen. In diesem wird die Basis Chargennummer und die Anzahl der Gebinde angegeben. Die sich daraus ergebende Warenzugangsmenge wird aus Gebindemenge und Gebindeanzahl errechnet. Zusätzlich wird je Gebinde eine eigene Chargennummer erzeugt. D.h. an das Ende der Chargennummer wird noch eine fortlaufende Zahl (durch Bindestrich getrennt) angefügt. Aus diesem Grund ist die Gebindechargennummer um 5Zeichen kürzer als in den Parametern definiert.
Für die Änderung von Warenzubuchung die Gebinde sind, muss die jeweilige Chargennummer aus dem Gebinde- / Chargendialog herausgelöscht werden.
**Hinweis:** Dies ist eine Funktion die nur am **Kieselstein ERP** Client implementiert ist und die Eineindeutigkeit der Chargennummer nur innerhalb des einen Wareneingangs prüft. Eine weiterführende Prüfung ist aufgrund der vielen Sonderfälle, denken Sie an Kabeltrommeln die an den Kunden geliefert werden und davon kommt ein Teil zurück, nicht sinnvoll. Wir vertrauen hier auf die sorgfältige Buchung durch die Bedienenden.
Die im Artikel/Lager angezeigte Gebindemenge (-größe) wird anhand des ersten Wareneingangs auf dieses Gebinde ermittelt.
**Info:** In der Bestellung selbst wird in der Position die bestellte Gebindeanzahl mit gespeichert. Dadurch ergibt sich aufgrund der Positionsmenge die Gebindemenge. Dies bedeutet auch, sollte im Artikellieferanten in der Zwischenzeit die Gebindemenge verändert werden, wird dadurch die Gebindemenge der Bestellposition und damit gegebenenfalls des Wareneingangs NICHT verändert. Für eine Richtigstellung der Gebindemenge muss die Bestellposition neu in die Bestellung übernommen werden.
Für den Ausdruck der Wareneingangsetiketten und der Artikeletiketten für ausgewählte Chargennummern steht auch die Information der Gebindebezeichnung und der Gebindemenge zur Verfügung.

**Info:** Die Gebindeverwaltung kann aufgrund ihrer Logik auch für eine einfache Form der Restmengenverwaltung verwendet werden.

Wichtig: Die Gebindemenge bezieht sich immer auf die Mengeneinheit des Artikels. Insofern ergibt sich, dass Standardmengen, Verpackungseinheiten, Mindestbestellmengen entsprechend aufeinander abgestimmt sein müssen.

#### Ich kann beim Artikellieferanten kein Gebinde auswählen?
Unter der Voraussetzung, dass in den Artikel Grunddaten bereits Gebinde definiert sind, prüfen Sie bitte ob bei dem Artikel der Haken bei auf Chargennummer Zwangseingabe (Reiter Sonstiges) gesetzt ist.

#### Muss jeder Artikellieferanten ein Gebinde zugewiesen erhalten?
Nein. Es sind auch Mischformen von Artikellieferanten mit und ohne Gebindedefinition möglich.
Bitte beachten Sie, dass für die Vorschlagswerte des Gebindes immer der zu oberst gereihte Artikellieferant mit dessen Gebinde verwendet wird. Insofern zieht sich der bevorzugte Lieferant, das bevorzugte Gebinde auch hier durch. 

#### Im Bestellvoschlag wird rechts ? bzw. v angezeigt ?
Das bedeutet, dass
| Zeichen | Bedeutung |
| --- | --- |
| ? |hier ist im Artikellieferanten eine Gebindedefinition gegeben, aber in der Bestellvorschlagsposition ist noch keine Gebindedefinition gemacht worden. |
| v | die geforderte Gebindedefinition wurde bereits gemacht |
| | es ist keine Gebindedefinition erforderlich   |

#### Sind Gebinde auch für Handeingaben möglich?
Leider nein, da Gebinde immer Chargennummernführung erfordern, steht dies für Handeingaben nicht zur Verfügung.

#### Wie werden Gebinde im Wareneingang gebucht?
Aufgrund der Definition in den Bestellpositionen erkennt die Wareneingangsbuchung welche Art von Gebinde zugebucht werden sollte. Beachten Sie bitte dass immer nur ganze Gebinde zugebucht werden können. Sollten Sie Gebinde abweichend von den bestellten erhalten, so müssen diese über eine neu angelegte Bestellposition zugebucht werden.
  ![](wareneingang_gebinde_chargen_liste.PNG)
  ![](wareneingang_gebinde.PNG)

#### Können Gebinde auch in Rahmen- und Abrufbestellungen verwendet werden?
Aufgrund der Komplexität, die, vor allem bei der Bedienung von Gebinden bei der Erfüllung der Abrufe gegeben ist, haben wir vorerst die Möglichkeit der Gebindeverwaltung in den Rahmen- und Abrufbestellungen nicht implementiert. Bei Bedarf wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

#### Wie wird ein freier Zuschnitt erfasst?
Um für einen Artikel andere Mengen als durch die Gebinde definiert bestellen und als ein "Sondergebinde" zubuchen zu können, muss ein Artikellieferant ohne Gebindedefinition angelegt sein. Hier ist es ja auch oft so, dass dieser Zuschnitt (das Sondergebinde) eine andere Artikelnummer als die normalen Gebindeartikel hat. [Bitte beachten Sie dazu auch]( {{<relref "/einkauf/bestellung/#kann-auch-in-abmessungen-bestellt-werden" >}} ).

#### Wie sieht die Überleitung von Gebinden ins Los aus?
Wie für alle Chargengeführten Artikel werden die lagernden Chargen mit deren Mengen aufgeführt und Sie wählen durch Klick auf die jeweiligen (Gebinde-)Chargennummer das entsprechende Gebinde aus. 

![](Gebinde_Chargen_Losausgabe.jpg)

#### es sollten keine Chargen ausgegeben werden?
Klicken Sie einfach direkt auf Übernehmen, ohne vorher eine Charge ausgewählt zu haben. Damit bestätigen Sie dass die übernommenen Mengen, aktuell dann eben keine, korrekt sind und der Rest wird, nach Rückfrage, als Fehlmenge ins Los eingetragen.

Lagercockpit

[Siehe]( {{<relref "/docs/stammdaten/artikel/lagercockpit" >}} )

### EAN, GTIN, SSCC, GS1

#### Was bedeutet GLN?
Die GLN = Global Location Number, ersetzt die ILN unter der gleichen Bedeutung. Die GLN dient der weltweiten eineindeutigen Identifikation von physischen Lokationen, also Adressen, und juristischen Personen. Sie wird in Österreich von der GS1 Austria verwaltet.

#### Was ist die GTIN?
Die GTIN = Global Trade Item Number, ersetzt die EAN (European Article Number), bei gleicher Bedeutung. Auch die Vergabe der GTIN Basisnummern wird von der GS1 verwaltet bzw. vergeben.

#### Können EAN Nummern / GTIN automatisch generiert werden?
Ja. Sie tragen dazu in den Systemparametern unter Parameter die GS1_BASISNUMMER_GTIN ein. Sie erhalten die 7 bzw. 9stellige Basisnummer ebenfalls bei der GS1.
Bitte beachten Sie, dass wir **immer** von einer 13-stelligen GTIN ausgehen.
Manche Kunden verlangen eine 14stellige GTIN mit führender 0\. Beachten Sie bitte, dass dies eigentlich eine 13stellige GTIN ist, der "optisch" eine 0 vorangestellt wird.

Sie finden im Modul "Artikel" Reiter "Sonstiges" neben der Bezeichnung der Verkaufs-EAN-Nr. ein Button "G" ![](GTIN_erzeugen.gif).
Dieser ist verwendbar, wenn die GTIN Basisnummer definiert ist. Durch Klick auf den Button wird mit der definierten Basisnummer eine neue GTIN13 generiert, die sich wie folgt zusammen setzt:
<Basisnummer><Artikelreferenz><Prüfziffer>
Basisnummer ... 7- oder 9-stellig
Artikelreferenz ... laufende Nummer, je nach Länge der Basisnummer 5- oder 3-stellig
Prüfziffer ... siehe Definition durch GS1

D.h. für eine 9-stellige Basisnummer können maximal 999 GTIN13-Nummern generiert werden.

Generierung
Es wird für jede verwendete Basisnummer und Mandant ein Zähler initialisiert. Bei der ersten Generierung wird 1 vergeben. Die restlichen Stellen werden mit '0' aufgefüllt. Hat man die Höchstgrenze erreicht (z.B. 999) und will wieder eine GTIN erzeugen, so wird nach Lücken gesucht. Werden keine freien Lücken mehr gefunden, so wird eine entsprechende Fehlermeldung ausgegeben, die darauf hinweist, dass erst existierende GTIN gelöscht werden müssen, um neue generieren zu können.

WICHTIG:

-   die neu generierte Nummer wird erst übernommen, wenn im Reiter "Sonstiges" auf "Speichern" geklickt wurde!

-   hat man eine Nummer generiert und speichert diese nicht ab oder drückt nochmals auf generieren, so ist die eine Nummer vorerst nicht vergeben, da der Zähler bei jeder Generierung inkrementiert wird. Auf diese Lücken wird erst nach Erreichen der Höchstgrenze zurückgegriffen.

-   will man solche Lücken manuell vergeben ist auf die richtig zu vergebende Prüfziffer zu achten!

-   bei jeder Generierung wird geprüft ob bereits ein Artikel mit einer Verkaufs-EAN-Nr. existiert, die der gerade genierten entspricht, wenn ja wird die nächste Lücke gesucht.

-   jeder, der die entsprechenden Schreibrechte am Artikel hat, kann diese EAN-Nr. ändern, jederzeit

-   Achtung auf gemeinsamen Artikelstamm und/oder mehrere Mandanten. Es wird dies aktuell nicht unterstützt und geprüft, da der Parameter mandantenabhängig und die Prüfungen auch
    dementsprechend sind. D.h. in diesem Fall könnten GLEICHE NUMMERN generiert werden, wenn die gleiche Basisnummer verwendet wird!

#### Kann eine SSCC erzeugt werden?
Wenn der Parameter GS1_BASISNUMMER_SSCC definiert ist, so kann, z.B. für die Lieferscheinetiketten oder die Los-Etiketten auch eine SSCC = Serial Ship Container Codes erzeugt werden.
Geben Sie bei dem Parameter die Erweiterungsziffer (X) und die 9-stellige Einleitung (YYYYYYYYY) des 18-stelligen SSCC ein, also XYYYYYYYYY.
Im Druck des Formulares gibt es nun einen HelperReport mit dem eine richtige SSCC errechnet werden kann. Es wird dazu die Packstücknummer an die Methode übergeben und somit auch **Kieselstein ERP** Mandantenweit eine eindeutige Packstücknummer erzeugt. In Kombination mit der Basisnummer ergibt sich daraus eine Weltweit eineindeutige 18-stellige Transportnummer. Dies wird üblicherweise in der Lieferscheinversandetikette oder in den Losetiketten verwendet. Hier wird von **Kieselstein ERP** eine eindeutige laufende Nummer zur Verfügung gestellt, welche in Kombination mit der Basisnummer zu einer eineindeutigen SSCC zusammengestellt wird.

#### Materialbedarfsvorschau
Nachdem immer wieder die Frage nach den Planzahlen für das kommende Jahr für die Zukunft entsteht, haben wir die Materialbedarfsvorschau, welche Sie im Artikel, Journal finden geschaffen.
Die oft gestellte Frage ist, welches Material wird aus verschiedenen Sichten in Zukunft gebraucht werden. D.h. ausgehen von den Daten der Vergangenheit bzw. den offenen Angeboten, Rahmen bzw. angelegten Aufträgen, wird einerseits der "Umsatz" der Artikelgruppen und daraus der anhand der Stücklistenauflösung resultierenden Materialbedarfe ermittelt.
![](Materialbedarfsvorschau.jpg)
Damit erhalten Sie eine entsprechende Prognose, welche Artikel Sie in Zukunft eventuell benötigen werden.
Bitte beachten Sie dass diese eine 1:1 Darstellung ist und eventuell saisonelle Schwankungen 1:1 in die Zukunft übernommen werden.

Warum werden keine offenen Aufträge mitgerechnet.
Diese sind bereits in den Losen und damit in den Bestellungen berücksichtigt. Würden wir offene Aufträge ebenfalls mitrechnen, hätten Sie doppelte Daten. Aus diesem Grunde werden offene Rahmenauftragsmengen mit berücksichtigt.

Das Ergebnis dieser Berechnung wird in zwei Sichten dargestellt:
a.) Die errechnete Zahlenbasis gegliedert nach Artikelgruppen und aufgeteilt auf die vier Betrachtungsweisen
![](Materialbedarfsvorschau2.gif)
Dies dient vor allem auch Ihrer Überprüfung, ob die 1:1 Annahme, dass das im Folgejahr gleich sein wird, auch wirklich stimmt.
Nutzen Sie auch die Filter auf ausgewählte Kunden und oder Artikelgruppen

b.) Der Materialbedarf im Detail
![](Materialbedarfsvorschau3.gif)
Also anhand der verkauften Artikel und deren Stücklistenauflösung werden die entsprechenden Detailmengen errechnet.
Über den Gesamtwert bekommen Sie ein Gefühl für den Materialeinsatz.
Zusätzlich wird in der letzten Zeile die benötigte Gesamtzeit als Richtwert mit angegeben.
Richtwert, da natürlich die Losgrößen nur in Ihrer Gesamtheit betrachtet werden können und daher sich die Rüstzeiten entsprechend verändern werden.

#### Materialentnahmestatistik
Natürlich braucht man immer wieder mal die Aussage, welcher Materialeinsatz ist denn in welchem Monat / Zeitraum gewesen.
Dafür gibt es im Modul Artikel, Journal, die Warenentnahmestatistik.

**Wie ist nun diese Auswertung zu lesen?** ![](Warenentnahmestatistik1.gif)

**Kann es einen negativen Abgang geben?** In der ersten Betrachtung wird man hier wohl behaupten, dass es keinen negativen Abgang geben kann, bzw. was ist denn das überhaupt.
Wenn man nun jedoch die Logik der Losausgabebuchung mit berücksichtigt, so kann es unter folgenden Situationen durchaus zu negativen Abgangsmengen kommen.
a.) Es wird das Los vor dem Betrachtungszeitraum ausgegeben
b.) während des Betrachtungszeitraumes wird ein Teil des Loses wieder an das Lager zurückgebucht. Da Lose grundsätzlich nur Entnahme / Abgangsbuchungen machen, ergibt sich dass diese Entnahme / dieser Abgang eben negativ wird. Z.B. weil der Status des Loses von Ausgegeben / in Produktion wieder auf angelegt zurückgeändert wird.

Warum ist das wichtig und richtig?
Betrachten Sie ein einzelnes Projekt. Nun wird in vielen begleitenden Controllingsystemen der Lagerwert der einzelnen Projekte betrachtet. Somit müssen diese Projekte auch wieder entlastet werden. Darum wird der Abgang unter Umständen eben negativ.

#### Kann in der Warenentnahmestatistik auch der Einstandspreis angegeben werden?
Ja auch dieser Preis wird in der Lagerbewirtschaftung mit protokolliert und kann daher auch in der Warenentnahmestatistik ausgegeben werden.
Bitte Sie gegebenenfalls Ihren **Kieselstein ERP** Betreuer um Anpassung des Reports.

#### korrigierte Lagerbewertung da Lieferung ab Werk
Insbesondere wenn eine Stichtagesgenaue und akribische Betrachtung der Warenwerte zum Bilanzstichtag gemacht wird / werden muss, ergibt sich immer wieder die Frage, welche Waren wurden zwar erst im neuen Jahr / Periode zugebucht, sie sind aber noch im alten Geschäftsjahr / in der alten Periode dem Lagerwert zuzurechnen.
Um diese Unterschiede darzustellen, haben wir im Wareneingangsjournal eine Auswertung integriert, welche die Wareneingänge aufzeigt, welche mit ihrem Lieferscheindatum vor dem Betrachtungszeitraum sind.
So finden Sie im Wareneingangsjournal nach jedem Wareneingang gegebenenfalls einen Hinweis
![](AnlieferungVorBetrachtung1.gif) mit Anlieferung vor Betrachtung. Das bedeutet, dass das Lieferscheindatum VOR dem Auswertungsdatum des Wareneingangsjournals liegt. Im angegebenen Beispiel bedeutet dies, dass die Auswertung ab 1.1.2018 gemacht wurde und dass das angegebene Lieferscheindatum (des Lieferanten) mit 21.12.2017 ist. Durch die Angabe EXW (Lieferung ab Werk des Lieferanten) sollten Sie diesen Warenwert in Ihren Lagerwert mit übernehmen. Daher finden Sie am Ende des Wareneingangsjournals eine entsprechende Zusammenfassung aller Positionen die vor der Betrachtung geliefert wurden und deren Lieferart.
![](AnlieferungVorBetrachtung2.gif)
D.h. obige Zusammenfassung bedeutet, dass für die Bilanz Ihr Lagerwert zum Stichtag 31.12\. um diesen Betrag zu erhöhen ist.

#### Wie bringt man Verkaufspreise vom einen zum anderen Mandanten?
Es ist immer wieder gegeben, dass gemeinsame Mandanten initial gleiche Artikel-Verkaufspreise haben. Beim Start mit dem neuen Mandanten hat man dann die Situation, dass im neuen Mandanten keine Verkaufspreise vorhanden sind, da das ja eine neue Firma ist.
Um diese nun zu übertragen nutzen Sie bitte die Pflegefunktion der Verkaufspreise. D.h. Sie exportieren im ursprünglichen Mandanten, es wird meist der 001 sein, die Verkaufspreise und importieren diese aus der gleichen Datei im anderen Mandanten. Also, Artikel, Pflege, Preispflege-Export. Alle Artikel oder nur die gewünschten exportieren.
Dann in den anderen Mandanten wechseln und hier Artikel, Pflege, Preispflege-Import aus der gleichen Datei die gewünschten Verkaufspreise importieren.
Für die Übernahme von Artikellieferanten und deren Preisen kann der Artikel XLS Import verwendet werden. Für die zur Verfügungstellung der Basisdaten unterstützt Sie gerne Ihr **Kieselstein ERP** Berater mit einer entsprechenden Datenbankabfrage.

#### Was bedeuten die Farben in der Artikelauswahlliste?
In der Artikelauswahlliste werden folgende Farben angezeigt:
| Farbe | Bedeutung |
| --- | --- |
| blau | dieser Artikel ist nicht lagerbewirtschaftet |
| hellgrau | dieser Artikel ist versteckt |
| orange | für diesen Artikel gibt es auch Forecast Positionen |

#### Wozu Sonderkonditionen beim Artikel definieren?
Oft ist es praktischer die Kunden mit besonderen Preis Konditionen vom Artikel aus zu definieren als umgekehrt. Weitere Details zu [Sonderkonditionen siehe]( {{<relref "/docs/stammdaten/kunden/#sonderkonditionen-kunden-artikelnummern" >}} ).
Bitte beachten Sie, dass Rabatte in den Sonderkonditionen nur für diejenigen Kunden vergeben werden können, die in als Währung die gleiche Währung wie Ihr Unternehmen / Mandant haben.

#### Können für bestimmte Artikel weitere Eigenschaften definiert werden?
So wie es für die Artikel allgemein einen eigenen Reiter Eigenschaften gibt, so steht zusätzlich für den Reiter Technik eine Definitionsmöglichkeit für weitere technische Daten zur Verfügung. Die Definition selbst erfolgt wie unter [Eigenschaften Definition]( {{<relref "/docs/stammdaten/system/eigenschaften" >}} ) beschrieben. Bitte beachten Sie, dass als Besonderheit hier die zusätzlichen Felder nur angezeigt werden, wenn die genutzte Bildschirmhöhe ausreicht um diese Felder auch darzustellen.

#### Artikel Freigabe
Für die Behandlung der [Artikelfreigabe siehe bitte]( {{<relref "/docs/stammdaten/artikel/freigabe" >}} ).

#### Artikel Dual Use
[Siehe dazu bitte]( {{<relref "/docs/stammdaten/artikel/dual_use" >}} ).

#### Kaufen oder selber machen ? Make or Buy Entscheidung
<a name="Make or Buy"></a>
Für diese oft recht komplexe Frage haben wir eine Unterstützung in der Form implementiert, dass Artikel die bereits einmal selbst produziert wurden, mit den ev. hinterlegten Einkaufspreisen mit Mengenstaffeln verglichen werden. Sie finden dies im Artikel unter Journal, Make or Buy. Bitte beachten Sie, dass dies eine Auswertung auf Vergangenheitsdaten, also den tatsächlich bei der Produktion entstandenen Kosten ist. Unterstützung für die ev. zu treffende Entscheidung vor einer ersten Produktion finden Sie in der Stücklisten-Gesamtkalkulation.
![](Make_Or_Buy.jpg)
Das bedeutet:
Es werden alle Stücklisten die in dem Zeitraum (Losablieferungen) produziert wurden und bei denen zumindest ein Artikellieferant mit einem zur Preisgültigkeit gültigem Einkaufspreis hinterlegt ist ausgewertet.
Zusätzlich kann die Auswertung nach Artikelnummer, Artikelklasse und Artikelgruppe eingeschränkt werden.
Die Darstellung ist, dass Sie im rechten Bereich den produzierten Artikel finden.
In der Mitte finden Sie eine Liste von Losen, mit der jeweiligen Abliefermenge und dem Ablieferpreis. Sind mehrere Lose im Betrachtungszeitraum gegeben werden auch die gesamt abgelieferte = produzierte Menge und der durchschnittliche Ablieferpreis als Summe angeführt.
Im rechten Bereich finden Sie eine Liste der Lieferanten für den jeweiligen Artikel mit Mengenstaffel, Einkaufspreis, Wiederbeschaffungszeit.
Je Lieferant werden alle Mengenstaffeln mit den jeweiligen Einkaufspreisen dargestellt.
Ist mehr wie ein Lieferant für den jeweiligen Artikel gegeben, so wird der günstigste Einkaufspreis, je Mengenstaffel grün hinterlegt.
Die schnellste Wiederbeschaffungszeit je Mengenstaffel wird Gelb hinterlegt dargestellt.
Ist der Einkaufspreis kleiner dem durchschnittlichen Ablieferpreis, so wird der Block (Mengenstaffel, Einkaufspreis, Wiederbschaffungszeit) orange unterstrichen, was signalisiert, dass die Beschaffung bei diesem Lieferanten günstiger wäre als den Artikel selbst zu produzieren. Bitte beachten Sie, dass hier keine zusätzlichen Umwegkosten wie Transport o.ä. berücksichtigt sind.