---
title: "Mehrmandanten"
linkTitle: "Mehrmandanten"
categories: ["Mehrmandanten"]
tags: ["zentraler Artikelstamm", "Mandanten übergreifende Bestellung", "gemeinsames Lager"]
weight: 500
description: >
  Was ist bei Mandanten-übergreifender Verwaltung zu beachten.
---
Mehrmandanten
=============

## Zentraler Artikelstamm
<a name="Zentraler Artikelstamm"></a>
In **Kieselstein ERP** steht für Anwender mit mehreren Mandanten auch das Modul Zentraler Artikelstamm zur Verfügung. Damit können die Artikeldaten und vor allem die Lagerstände zentral verwaltet werden. Alle anderen Module bleiben nach wie vor Mandantenabhängig. Siehe dazu auch [Mandanten übergreifende Bestellung / Aufträge](#Mandanten übergreifende Bestellung / Aufträge).

Der Grundgedanke dieser Funktionalität ist, dass Artikeldefinition, Lagerwirtschaft und Wareneinkauf vom Hauptmandanten gemacht wird. Lediglich der Verkauf wird, aus steuerlichen oder aus organisatorischen Gründen, auf verschiedene Mandanten aufgeteilt. Daher ist die Verkaufspreisdefinition trotz der zentralen Artikelstammdefinition für jeden Mandanten getrennt ausgeführt. Wird diese zusätzliche Funktion verwendet, so erfolgt der Zugriff für die Artikel und für die Lagerbuchungen immer auf den Hauptmandanten.

Bitte beachten Sie, dass in der Anzeige der Artikellieferanten auch bei den anderen Mandanten immer die Artikellieferanten des Hauptmandanten angezeigt werden, auch wenn keine neuen Lieferanten hinzugefügt werden können. Daher können die Daten der Artikellieferanten und der EK-Staffelpreise nur im Hauptmandanten geändert werden.

Hinweis: Bei Mandanten mit zentralem Artikelstamm erfolgt die Definition der Artikellieferanten und die Warenbeschaffung vom Hauptmandanten aus. D.h. hier sollten keine echten Warenzubuchungen erfolgen. Stellen Sie bitte die Benutzerrechte des Mandanten entsprechend ein.

Da immer wieder gewünscht wird, dass nur der Artikelstamm selbst zentral ist, aber auch der Einkauf für den jeweiligen Mandanten eigenständig erfolgen sollte, wurde ermöglicht, dass auch in den nicht Hauptmandanten Artikellieferanten angelegt werden können. Um nun deutlich darzustellen, für welchen Mandanten die Definition des Artikellieferanten ist, wird auch der Mandant des Artikellieferanten angezeigt.<br>
![](ArtikellieferantBeiZentralerArtikelstamm.gif)<br>
**Hinweis:** Da trotz zentralem Artikelstamm die Trennung der Lieferanten in die Mandanten aufrecht bleibt, jedes Unternehmen (= Mandant) hat seine eigenen Preisvereinbarungen mit dem Lieferanten, muss beachtet werden, dass, trotz eventuell gleichlautender Lieferantennamen, die Lieferanten nach Mandanten getrennt verwaltet sind. Daher werden bei Bestellungen NUR die Lieferantendaten des Lieferanten des gleichen Mandanten verwendet.<br>
Bei der Verwendung des Bestellvorschlages bedenken Sie bitte, dass hier der bevorzugte Lieferant nur dann verwendet wird / werden kann, wenn dieser dem angemeldeten Lieferanten entspricht.
Für die komfortable Übertragung des Lieferanten eines Mandanten in den anderen siehe [bitte unten](#Artikellieferanten übertragen).<br>
**Hinweis:** Um die Lagerbuchung bei zentralem Artikelstamm zu ermöglichen, werden immer nur die Lager des Hauptmandanten angezeigt. Das Wesen des zentralen Artikelstamms ist ja auch, dass gemeinsame Lager verwendet werden.

Zur Übersicht über die einzelnen Lagerstände können Sie den Parameter LAGERSTAND_DES_ANDEREN_MANDANTEN_ANZEIGEN aktivieren. Dieser bewirkt, dass in der Auswahlliste des Artikel-Moduls auch die Lagerstände der anderen Mandanten angeführt wird.

**Info:** Wenn sich die Buchhaltungssysteme der Mandanten unterscheiden (Z.B. bei abweichenden Ländern), so ist eine Verwendung des zentralen Artikelstamms in Kombination mit der integrierten Finanzbuchhaltung bzw. dem Fibu-Export möglich. Es müssen dafür gegebenenfalls die abweichenden Kontierungen der Artikelgruppen je Mandant angepasst werden.

### Zentraler Artikelstamm und Artikelsperren
Wenn man mit einem zentralen Artikelstamm arbeitet, so müssen die möglichen Sperren im Hauptmandanten des Artikels definiert werden.<br>
Damit stehen diese Sperren auch in den anderen Mandanten zur Verfügung.
D.h. es können jederzeit auch von den anderen Mandanten Artikel gesperrt werden. Die Artikelsperre wirkt immer Mandantenübergreifend.

### Zentraler Artikelstamm und Bestellvorschlag
Wenn man mit einem zentralen Artikelstamm arbeitet, so kann auch der Bestellvorschlag mandantenübergreifend durchgeführt werden. 

Danach wird in der Bewegungsvorschau, in der Auswahlliste des Bestellvorschlags und dem Druck zur Information der Mandant angeführt.

Wenn ein neuer Bestellvorschlag erzeugt wird, dann werden in der Bewegungsvorschau die Lagerstände aller Mandanten bzw.die Bedarfe aller Mandanten berücksichtigt.

Beim Überleiten in Bestellungen werden nur Bestellvorschlags-Einträge des aktuellen Mandanten berücksichtigt und bestellt wird bei dem Lieferanten, bei dessen Mandanten man angemeldet ist.

### Fehlmengenauflösung bei anderem Mandanten
Mit Hilfe des Menüpunkts im Lieferschein: Bearbeiten/ Fülle Fehlmengen eines anderen Mandanten nach, wird:<br>
Wenn die Summe der Fehlmengen des ZielMandanten größer als der Lagerstand des Hauptlagers des Zielmandanten ist und beim Quellmandanten ein Lagerstand >0 ist, das fehlende vorhandene Material per Ziellager-Lieferschein vom Quell zum Ziel-Mandanten gebucht (Quell-Lager ist Hauptlager des Quell-Mandanten, Ziellager ist das Hauptlager der Ziel-Mandanten).

![](fehlmengen_mandant_aufloesen.JPG)

Dies geschieht für alle Fehlmengen des Ziel-Mandanten, unabhängig davon, ob das Material in Zukunft aufgrund der Bewegungsvorschau des Quell-Mandanten selbst benötigt wird.

### Zentraler Artikelstamm und unterschiedliche Mandantenwährungen
Bei der Verwendung des zentralen Artikelstammes ist grundsätzlich darauf zu achten, dass, wenn sich die Anwender aus einem gemeinsamen Lager bedienen, die Währungen der beteiligten Mandanten gleich sind.<br>
**Hintergrund:** Die Gestehungspreisberechnung der einzelnen Artikel erfolgt immer in [Mandantenwährung]( {{<relref "/docs/stammdaten/system/#definition-der-mandantenw%c3%a4hrung" >}} ).<br>
Wenn Ihre Mandanten unterschiedliche Währungen haben, so muss unbedingt die zusätzliche Funktion Getrennte_Lager aktiviert werden. Wenden Sie sich dazu an Ihren **Kieselstein ERP** Betreuer.

### Zentraler Artikelstamm und getrennte Lager
<a name="Getrennte Lager"></a>
Gerade bei unterschiedlichen Mandantenwährungen ist es wesentlich, dass entgegen oben gesagtem trotz des zentralen Artikelstammes die Lager getrennt verwaltet werden. D.h. hier reduziert sich der zentrale Artikelstamm auf die gemeinsame Verwendung der Definition des Artikels.

Verkaufspreise, Gestehungspreise, Einkaufskonditionen, Lager und Lagerstände, sowie Lagerplätze sind in jedem Fall getrennt.

### Inventur für jeden Mandanten getrennt
Ist dies gewünscht, muss die Zusatzfunktion getrennte Lager aktiviert werden und natürlich die Rechte für die Lager entsprechend hinterlegt sein.<br>
Ist im anderen Mandanten der Reiter Inventur nicht sichtbar, so fehlt diese Zusatzfunktion für diesen Lieferanten.

### Mandanten übergreifende Bestellung / Aufträge
<a name="Mandanten übergreifende Bestellung / Aufträge"></a>
Bei mehreren Mandanten mit zentralem Artikelstamm ist es sehr praktisch, wenn die Bestellungen für das jeweils andere Unternehmen faktisch automatisch erstellt werden.
Dafür sind folgende Parametrierungen erforderlich:
- a.) es müssen die Partner der Mandanten bei den jeweiligen Mandanten als Kunde bzw. Lieferant angelegt sein. Bitte achten Sie dabei darauf, dass dies durch neu aus Partner übernommen werden.
Achten Sie auch darauf, dass die Zuordnung eineindeutig ist.
- b.) Es muss der Benutzer für die automatischen Buchungen in den Mandanten definiert sein.
- c.) die beteiligten Kunden / Lieferanten müssen die gleiche Währung haben. [Siehe](#Währungen).
Der übliche Ablauf ist:
  - 1. Sie erhalten die Bestellung eines Kunden und legen die Auftragsbestätigung an. Üblicherweise mit Termin unverbindlich. Siehe dazu auch [Auftrag, Wiederbeschaffung.]( {{<relref "/verkauf/auftrag/#wiederbeschaffung" >}} )<br>
  Wir gehen für das Beispiel davon aus, dass dies im Mandanten 001 gemacht wird. Die Stücklisten werden im Mandanten 003 und 002 gefertigt.
  - 2. Es wird, im Mandanten 001, der Bestellvorschlag durchgeführt und somit die Ware als beim Mandanten 003 zu bestellen aufgelistet.<br>Sie erzeugen daraus die entsprechende Bestellung.
  - 3. **<u>Ändern mandantenübergreifende Bestellung</u>**<br>
<a name="Ändern mandantenübergreifende Bestellung"></a>
Aktivieren Sie die Bestellung, entweder durch den klassischen Ausdruck, oder durch Rechtsklick auf das Druckersymbol im Reiter Positionen der Bestellung.<br>
Dadurch wird die Bestellung im Mandanten der über den Lieferanten verknüpft ist angelegt. In unserem Beispiel im Mandanten 003.<br>
**WICHTIG:** Dadurch kann die Bestellung, genauer der Inhalt der Bestellung, nur mehr durch den Auftrag im anderen Mandanten verändert werden. Eine Änderung der Bestelldaten durch den Besteller ist nicht vorgesehen (Sie haben ja eine verbindliche Bestellung bereits an Ihren Lieferanten versandt)<br>Ab nun kann der Inhalt der Bestellung nur mehr über den beim liefernden Mandanten automatisch angelegten Auftrag verändert werden. Dies kann bis zum Storno beider Belege gehen.<br>
Natürlich kann ein Wareneingang auf die Bestellung mit anschließender Eingangsrechnung in die Bestellung gebucht werden. Ab diesem Zeitpunkt sind auch Änderungen im Auftrag des liefernden Mandanten gesperrt.
  - 4. Durch das automatische Anlegen des Auftrages im anderen Mandanten, in unserem Beispiel 003, sind die Bestellpositionen mit den gewünschten Lieferterminen im Auftrag vorhanden.<br>
Der Auftrag befindet sich, abhängig ob eine Automatikregel gegeben ist oder nicht, nun im Status angelegt (Automatikregeln siehe unten).
  - 5. Passen Sie den Auftrag ihren Wünschen gemäß an.<br>
Durch das Aktivieren (drucken usw.) des Auftrages wird zusätzlich die Bestellung des bestellenden Mandanten (001) aktualisiert. Es werden die Bestellpositionen mit Mengen und Preisen angepasst und die Lieferterminbestätigungen mit der Auftragsnummer und den Terminen aus der AB eingetragen.<br>Der Auftrag kann solange verändert und neu bestätigt werden, bis in der Bestellung Wareneingänge gebucht sind. Ab diesem Status kann der Auftrag in seinen Solldaten nicht mehr verändert werden.<br>
**Hinweis:** Änderungen in den Bestellungen werden protokolliert. Siehe Info, Änderungen.
  - 6. Lieferung:<br>Die Lieferung erfolgt wie üblich durch das Erstellen eines Lieferscheines / einer Rechnung mit Auftragsbezug.
  - 7. Wareneingang:<br>Die Buchung des Wareneinganges erfolgt idealerweise über den Reiter ![](Ware_unterwegs.gif) Waren bestellt/unterwegs. Hier sehen Sie die Positionen Ihrer Bestellungen für die der andere Mandant bereits Lieferscheine erstellt hat. Sind diese auch aktiviert (offen oder verrechnet), so ist davon auszugehen, dass diese Ware tatsächlich an Sie versandt wurde. Übernehmen Sie hier mit ![](Ware_unterwegs_LS_uebernehmen.gif) alle Positionen des gewählten Lieferscheines bzw. mit ![](Ware_unterwegs_pos_uebernehmen.gif) die markierten Positionen als Wareneingang.<br>
  Der Vorteil dieser Lösung ist, dass
        - ein eventueller Zeitversatz zwischen den Standorten klar abgebildet werden kann
        - dass an beiden Standorten das Material gezählt wird und eventuelle Unstimmigkeiten offensichtlich werden und somit geklärt werden können.
  - 8. Sollte dieser Geschäftsfall beendet werden müssen, so sind Auftrag und Bestellung auf beiden Seiten manuell auf Erledigt zu setzen. Dies kann sowohl für einzelne Auftrags- / Bestellpositionen als auch für den gesamten Auftrag / die gesamte Bestellung gemacht werden.

**Hinweis:** Wenn auch Materialzuschläge behandelt werden, so müssen natürlich Lieferant und Kunde im jeweiligen Mandanten die gleiche Kupferzahl hinterlegt haben. Die Materialzuschläge sind beim zentralen Artikelstamm für alle Mandanten gleich.

#### Ändern mandantenübergreifende Bestellung
Siehe oben 3.

#### Wie lange kann die Bestellung / die Auftragsbestätigung verändert werden?
Das hängt im wesentlichen davon ab, in welchem Status die beteiligten Module sind.
- D.h. solange die Bestellung nicht aktiviert ist (Status angelegt), kann diese verändert werden
- Wurde die Bestellung aktiviert, so ist der Auftrag im Status angelegt.<br>
An diesem kann Menge und Preis der Positionen verändert werden. Der Termin kann nicht verändert werden
- Wird nun der Auftrag aktiviert, so werden die Daten als Lieferterminbestätigung in die Bestellung eingetragen (Reiter Sicht Lieferantentermine)
- In der Bestellung kann nun nur mehr die Menge und der Liefertermin geändert werden.<br>
Die Menge kann auch auf 0 gesetzt werden. Wird die bestellte Menge auf einen Wert gestellt, der die bereits gelieferte Menge unterschreitet, bzw. auf 0,00, so wird damit automatisch die Auftragsposition erledigt. Sind alle Auftragspositionen erledigt, wird automatisch auch der gesamte Auftrag erledigt, womit auch die Bestellung nicht mehr verändert werden kann.
- Zusätzlich: Der Liefertermin wird durch die Bestellung definiert. Daher kann in den Auftragspositionen dieser nicht verändert werden.

#### Können hier auch automatische Berechnungen von Preisen und Mengen erfolgen?
Für automatische Berechnungen / Ergänzungen der Bestellungen haben wir Regeln vorgesehen. Stellen Sie dazu den Parameter AUTOMATIKREGEL entsprechend ein.
Für die AUTOMATIKREGEL=1 ist folgendes definiert:

1.  Wenn der bestellte Artikel im anderen Mandanten eine eigengefertigte Stückliste ist, dann wird die Auftragspositionsmenge auf die nächsthöhere Fertigungssatzgröße des Artikels aufgerundet und der VK-Preis als Verrechnungspreis an den Besteller verwendet.

2.  Ist der bestellte Artikel keine eigengefertigte Stückliste (bei dem Mandanten der die Auftragsbestätigung erstellt), so wird auf die Verpackungseinheit des bevorzugten Lieferanten (Lief1) aufgerundet und der Einkaufspreis des Lieferanten verwendet.
    Wird jedoch festgestellt, dass für die errechnete aufgerundete Menge zuwenig auf Lager ist, aber für die bestellte noch ausreichend Ware vorhanden ist, so wird nur die bestellte Menge bestätigt

3.  Ist zu wenig auf Lager, so wird die oben errechnete Menge bestätigt und zum Positionsliefertermin wird die Wiederbeschaffungszeit aus dem Lief1 hinzugerechnet.

4.  Es erfolgt eine Überprüfung, ob bei jeder Position der maximale Veränderungswert (AUTOMATIK_POSITIONSSCHWELLWERT in Mandantenwährung) eingehalten wird. Ist dies der Fall, so werden Mengen und Termine in die Bestellung des auslösenden Mandanten rückbestätigt und der Bestellstatus auf "Bestätigt" gesetzt. Zugleich wird die Auftragsbestätigung auf Offen gesetzt.
    Wird der maximale Veränderungswert in einer Position überschritten, so bleibt der Auftrag im Status angelegt und muss von einem berechtigten Benutzer angepasst und bestätigt (= ausgedruckt)werden.

**Hinweis:** Ist die Auftragsfreigabe freigeschaltet, so wird der Auftrag automatisch freigegeben.

##### Wie werden die Termine errechnet?

1.  Vom Besteller wird sein Wunsch Eintrefftermin im Bestellungskopf bzw. in den Bestellpositionen eingegeben

2.  Bei der Überleitung in die Auftragsbestätigung wird die Handlingdauer (siehe Parameter) abgezogen. Ergäbe dies einen Liefertermin vor heute, so wird der heutige Tag als Liefertermin angegeben

3.  Ist die Ware lagernd, so wird der unter 2\. errechnete Liefertermin als Basis verwendet

4.  Ist die Ware nicht lagernd, so wird angenommen, dass heute die Ware noch beim Lieferanten bestellt wird. Somit wird als frühest möglicher Liefertermin heute plus Wiederbeschaffungszeit des bevorzugten Lieferanten (Lief1) verwendet.<br>
    Ist dieser Termin vor dem unter 2\. errechneten Termin, so wird der Termin nicht verändert,
    anderenfalls wird er auf den danach errechneten Termin verändert
    **Hinweis:** Ist beim Artikellieferanten keine Wiederbeschaffungszeit hinterlegt, so werden 14Tage als standard Lieferzeit von diesem Terminberechnungsautomaten angenommen.

5.  Ausgehend von den oben errechneten Terminen wird in der Bestellung der jeweilige Positionstermin zuzüglich der Handlingdauer errechnet und eingetragen.

#### Wird das eine Multi-Site Verwaltung?
Für eine Multi-Site-Verwaltung gehören so Dinge wie Beleglose Rechnungen zwischen den Standorten, jeder Mandant / User darf beim anderen sich aus dem Lager bedienen, es können jederzeit Produktionsaufträge vom Mandanten 001 zum Mandanten 002 umgelagert werden, mit dazu. 

Weiters werden hier die sehr breiten Themen der Inter-Company Verrechnung sehr wesentlich.

Insbesondere die Punkte der Verlagerung von Produktionsaufträgen von einem zum anderen Mandanten sehen wir sehr kritisch, es geht ja nicht nur um das Verschieben eines Auftrages, sondern auch um die Anpassung der Stückliste mit dem Arbeitsplan, also den Maschinen, an den neuen Standort. Aus unserer Sicht kann da nicht einfach per Software der Fertigungsauftrag verschoben werden, sondern es muss auf die gesamte Infrastruktur Rücksicht genommen werden.

Es ist daher derzeit nicht geplant, eine umfassende Multi-Site-Lösung anzubieten.

#### Sehe ich auch ob Ware unterwegs ist?
Unter Ware unterwegs verstehen wir, dass von einem Mandanten bestellte Ware, durch den anderen bereits ausgeliefert wurde, diese aber noch nicht eingetroffen ist. D.h. die Ware ist beim liefernden Mandanten bereits in den Lieferschein gebucht und somit auch die Reservierung am Auftrag erledigt, aber, aufgrund der Transportwege ist sie beim anderen Lieferanten noch nicht eingetroffen. Dies zeigen wir im Bestellmodul im unteren Reiter Ware bestellt / unterwegs an. D.h. hier sehen Sie nicht nur die Ware die bereits am Transportweg ist, sonder auch die, die beim liefernden Mandanten noch offen ist.<br>
![](Ware_unterwegs.jpg)<br>
Da auch der Status des Lieferscheines mit angezeigt wird, sehe Sie, wie in obigem Beispiel, dass der liefernde Mandant gerade dabei ist den Lieferschein zu erstellen.<br>
Bitte beachten Sie, dass es für die bestätigten Positionen durchaus auch Teillieferungen geben kann. Es werden in diesem Falle dann die Bestellpositionen mehrfach angezeigt.

#### Kann damit auch der Wareneingang gebucht werden?
<a name="Waren unterwegs zubuchen"></a>
Ja. Im Reiter Ware bestellt / unterwegs können ausgewählte Positionen bzw. die Positionen des gewählten Lieferscheines direkt in die jeweilige Bestellung gebucht werden. Bitte beachten Sie, dass selbstverständlich die Kontrolle der vom anderen Mandanten erhaltenen Artikel und deren Mengen trotzdem erforderlich sein kann.
![](Ware_unterwegs2.jpg)

Mit dem Klick auf ![](Ware_unterwegs3.gif) werden alle Positionen des gewählten Lieferscheins in die jeweilige Bestellung übernommen. Mit dem Klick auf ![](Ware_unterwegs4.gif) können die ausgewählten Positionen sehr selektiv übernommen werden.

#### Wie können Lieferanten eines Mandanten in einen anderen übertragen werden?
<a name="Artikellieferanten übertragen"></a>
Der Grundgedanke hier ist, dass es einerseits sehr praktisch ist, wenn man in unterschiedlichen Werken die Lieferanten per Klick von einem zum anderen Mandanten übertragen kann. Andererseits ist dies natürlich sehr massiv von den Rechten im jeweiligen Mandanten abhängig.<br>
D.h. es kann der Artikellieferant eines anderen Mandanten kopiert werden. Melden Sie sich dazu am gewünschten Mandanten an, wechseln im Artikelstamm zu dem Artikel und wählen im Reiter Artikellieferant denjenigen Eintrag eines anderen Mandanten, den Sie kopieren möchten und klicken auf ![](Artikellieferant_eines_anderen_Mandanten_kopieren.gif) Eintrag kopieren. Damit wird faktisch der Eintrag aus dem anderen Mandanten in Ihren Mandanten übernommen. Voraussetzung ist, dass über die Verknüpfung der Partneradresse der Lieferant in beiden Mandanten sich auf den gleichen (den einen) Partner bezieht. Sollte das kopieren wegen unterschiedlicher Partneradressen eigentlich des gleichen Lieferanten nicht möglich sein, so müssen zuerst die Partner des einen Lieferanten [zusammengeführt]( {{<relref "/docs/stammdaten/partner/#partner-zusammenf%c3%bchren" >}} ) werden.

Geht es z.B. darum, dass der Artikellieferant von einem zum anderen Mandanten verschoben werden sollte, so gehen Sie bitte in der Form vor, dass zuerst der Artikellieferant in den neuen Zielmandanten kopiert wird. Springen Sie dann mit dem GoTo Button ![](auf_den_Artikellieferant_eines_anderen_Mandanten_springen.jpg) gehen zu Eintrag in den Artikellieferanten im anderen **Kieselstein ERP** Client des anderen Mandanten und verändern / löschen Sie diesen wunschgemäß.

#### Wie erkennt man ob das ein Mandanten übergreifender Beleg ist?
Für die Erkennung ob dieser Auftrag, Lieferschein, diese Bestellung Mandantenübergreifend ist, wird, solange dies ein freier Beleg ist, in der ersten Spalte der Auswahl ein **M** angezeigt welches signalisiert, dass dieser Beleg Mandantenübergreifend ist. Da es im Modul Lieferschein auch vorkommen kann, dass einfach ein Lieferschein an einen anderen Mandanten, ohne Mandantenübergreifendem Auftrag, erstellt wird, wird dies durch ein kleines **m** in der ersten Spalte angezeigt.

**Hinweis:**
Bitte achten Sie darauf, dass für die Mandanten nur jeweils eine Adresse angelegt ist. Nur weil die Namen Ihrer verbundenen Unternehmen gleich sind, ist das noch lange kein anderer Mandant. Ein anderer Mandant ist es nur, wenn dieser Partner im System unter Mandanten aufscheint.
Siehe dazu auch, Partner, auf den jeweiligen Mandanten springen, Reiter Referenz zu.

<a name="Währungen"></a>

#### Unsere Mandanten haben unterschiedliche Währungen
Wenn Ihre Mandanten in unterschiedlichen Währungen arbeiten (müssen), so ist darauf zu achten, dass die gegenseitige Währungsdefinition gleich ist. Das bedeutet, dass der Lieferant des Ausgangsmandanten und der dazugehörende Kunde des Zielmandanten, welche ja die wechselseitige Beziehung darstellen, in der gleichen Währung sein müssen. Die Währungen der Mandanten selbst können unterschiedlich sein. Achten Sie in jedem Falle darauf, dass vom jeweiligen Mandanten / Land aus gesehen die [Wechselkurse]( {{<relref "/management/finanzbuchhaltung/#wo-werden-die-kurse-nachgepflegt" >}} ) richtig eingestellt sind. 

#### Eine Bestellposition kann nicht erledigt werden?
Wenn man bei einer mandantenübergreifenden Bestellung auch das Preise erfasst setzen will, kommt die Meldung
![](Bestellstatus.jpg)
diese Aktion kann aufgrund des Status des Beleges nicht ausgeführt werden.

Aufgrund der direkten Verbindung zwischen Bestellung und Auftragsbestätigung des anderen Mandanten kann der Status nur geändert werden, wenn auch der Auftrag noch nicht vollständig erledigt ist.
Um nun herauszufinden welcher Auftrag beim anderen Mandanten dies ist, wechseln Sie in der Bestellung in den Reiter Sicht Lieferantentermine, wechseln auf die Position und sehen unten die
![](AB_Nummer.gif) AB-Nummer der damit verbundenen Auftragsbestätigung.
Wechseln Sie nun im anderen Mandanten in die jeweilige Auftragsbestätigung. Hier ist nun vermutlich dieser Auftrag im Status erledigt.
![](Auftrag_Enterledigen.jpg)

#### wie verhält sich der Lief1Preis?
Es wird bei der Mehrmandanten-Lösung mit zentralem Artikelstamm davon ausgegangen, dass es trotz getrennter Mandanten ein zentraler Einkauf für alle Artikel gegeben ist. D.h. für die Ermittlung des sogenannten [Lief1Preises]( {{<relref "/docs/stammdaten/artikel/#was-ist-der-lief1preis" >}} ), wird der aktuell gültige Preis OHNE Mandantenzuordnung herangezogen.

### Besonderheiten für Installationen mit Mehrmandanten
Nachfolgend eine stichwortartige Sammlung von Besonderheiten in der Anzeige bzw. in den Reports bei mehreren Mandanten

-   Anzeige der Verkaufsmengenstaffeln mit Bezug auf die Verkaufspreislisten sind Mandantenabhängig. Zugleich werden die Preise des anderen Mandanten angezeigt, wenn die Rechte dafür gegeben sind. Damit man leichter sieht, dass das die Preise eines anderen Mandanten sind, werden diese Mengenstaffeln in hellgrau dargestellt.