---
title: "Artikel Versionen"
linkTitle: "Versionen"
categories: ["Versionen"]
tags: ["Versionen"]
weight: 800
description: >
  Wie mit Artikelversionen umgehen, ich habe ja noch einige alte in der Fertigung
---
Artikel und Stücklisten Versionen
=================================

Nachdem unsere Anwender immer wieder vor der Herausforderung stehen, wann neue Artikel und oder Stücklisten für bestehende Artikel / Stücklisten anzulegen, hier eine Zusammenfassung der von uns empfohlenen Vorgehensweise.

#### Wann ist ein neuer Artikel anzulegen ?
Ein neuer Artikel ist immer dann erforderlich, wenn der neue Artikel NICHT vollständig anstelle des bisher verwendeten Artikels eingesetzt werden kann. Z.B. weil eine Bohrung an einer anderen Stelle ist oder die Abmessungen verändert wurden usw.. Hier kommt meist die Überlegung mit dazu, muss der alte Artikel noch auf Lager bevorratet werden um ev. Ersatzteile für alte noch im Feld befindliche Geräte zur Verfügung zu haben usw.. Denken Sie hier bitte auch an die Verpflichtung als Hersteller, dass Ersatzteile entsprechend lang nach der Auslieferung von Geräten geliefert werden können müssen.

#### Wie kann ein Zusammenhang hergestellt werden?
Oft hat man sich an die Verwendung eines Artikels gewöhnt und nutzt immer diesen. Um die damit einhergehende Gefahr von falschen Artikeln in den Belegen inkl. Stücklisten und Losen hintanzuhalten verwenden Sie bitte aus dem Modul Artikel, Reiter Sonstiges den Button Ersatzartikel. Dies bedeutet, dass am alten Artikel derjenige Artikel als Ersatzartikel / Ersetzt durch eingetragen wird, der nun neu verwendet werden sollte. Dies kann durchaus mehrere Generationen von Ersatzartikeln betreffen.
Der Vorteil dieser Funktion ist auch, dass Sie in der Artikelstatistik Gesamtbetrachtungen über alle Artikel und seine Vorgänger (mit Vorgängern) ansehen können und so, unabhängig von der Version, eine Gesamt-Einkaufs/Verkaufsstatistik des Artikels sehen.

#### Wie macht man das nun effizient?
Abhängig davon ob ein neuer Artikel oder eine neue Stückliste anzulegen ist, gehen Sie etwas unterschiedlich vor.
Ist nur ein neuer Artikel anzulegen, so wählen Sie aus der Artikelauswahlliste des Artikelmoduls, Artikel kopieren ![](Artikel_kopieren_button.gif)
Idealerweise haken Sie hier auch Ursprungsteil verlinken ![](Ursprungsteil_verlinken.gif) an. Damit wird am Ausgangsartikel automatisch obiger Zusammenhang, des Ersatzartikels eingetragen.

Muss eine neue Stückliste angelegt werden, so wählen Sie in der Auswahlliste der Stücklisten, Stückliste kopieren ![](Neue_Stueckliste_erzeugen.gif).
Er erscheint der gleiche Dialog wie für Artikel kopieren. Geben Sie auch hier die neue Artikelnummer der Stückliste ein.
Beim kopieren der Stückliste wird nicht nur der neue Stücklistenartikel angelegt, sondern es werden die Positionen, der Arbeitsplan und die weiteren Daten der Stückliste ebenfalls kopiert.

#### Wie geht man nun am effizientesten bei mehrstufigen Stücklisten vor?
Gerade wenn man Geräte / Baugruppen bestehend aus mehreren Ebenen hat, ist folgendes Feature sehr praktisch.
Es ist in aller Regel so, dass man eine Stückliste ja deswegen kopiert, weil einzelne Positionen auszutauschen sind, anderes verwendet werden usw.
Hier kommt immer wieder dazu, dass bei einem komplexen Stücklistenbaum der übergeordnete Hierarchiebaum neu aufgebaut werden muss, nur weil sich in einer Unterebene ein Teil geändert hat.
D.h. nachdem Sie die neue Stückliste erzeugt haben sind ein oder mehrere Artikel ebenfalls neu anzulegen.
Um dies so einfach wie möglich zu gestalten haben wir die Funktion neue Version anlegen ![](Neue_Stuecklistenpositionsversion_erzeugen.gif) geschaffen.
Mit dieser Funktion werden die für die Erstellung einer neuen Version eines Artikels oder einer (Unter-)Stückliste erforderlichen Schritte möglichst automatisch ausgeführt.
D.h. mit Klick auf neue Version erzeugen wird:
- die Artikelnummer des neuen Artikels der neuen Stückliste abgefragt
- der neue Artikel angelegt
  - ist der Artikel eine Stückliste wird die komplette Stückliste wie oben beschrieben kopiert
- die ursprüngliche Stücklistenposition gegen den neuen Artikel ausgetauscht

D.h. es muss "nur mehr" die Bezeichnung und die sonstigen Daten des neuen Artikels entsprechend angepasst werden.

Tipp

Bitte versuchen Sie den Versionswildwuchs massiv einzudämmen. Ein gutes Beispiel ist hier die Automobilindustrie. Änderungen gibt es nur wenn unbedingt erforderlich oder bei neuen Fahrzeugreihen. Denken Sie bei neuen Version auch immer an die damit verbundenen Dinge wie neue Preislisten, andere Dokumentation technisch und oder kaufmännisch usw.

#### Wie sollte die Artikelnummer mit den Versionen aufgebaut werden?
Wenn nun mehrere Versionen im Feld sind, oder wenn Sie z.B. kritische Kunden bzw. den Zoll zu berücksichtigen haben, so kann es durchaus wichtig sein, dass die ursprüngliche Artikelnummer immer erhalten bleibt. D.h. wenn Sie z.B. den Artikel ABCD haben, so könnte die allererste Version ABCD_00 lauten. Kommt es nun zu einer Änderung, die einen neuen Artikel bedingt (siehe oben) so gibt es eben den Nachfolger / die neue Version ABCD_01.
Es kann nun auf den Papieren, von Angebot bis Bestellung dies in der Form angedruckt werden, dass als eigentliche Artikelnummer ABCD und darunter Index: 01 angedruckt wird.
Gegebenenfalls bitten Sie Ihren **Kieselstein ERP** Betreuer um die Anpassung.

#### Reicht auch die Änderung unter Index / Revision ?
Das hängt davon ab.
Sowohl Index als auch Revision sind nur textliche Einträge in den Artikeldaten.
Die durchgeführten Änderungen werden jedoch protokolliert und können somit auch genutzt werden.
Die Änderungen finden Sie z.B. im Artikelstammblatt. Hier werden der Zeitpunkt der Änderung und die Änderung selbst angezeigt.
D.h., wenn diese reine Chronologie für Sie ausreicht, z.B. da Sie immer zuerst die alten Versionen ausliefern dürfen und erst danach die Produktion usw. der neuen Artikel starten, kann dies eine durchaus praktikable Variante sein.