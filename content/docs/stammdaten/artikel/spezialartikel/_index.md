---
title: "Spezialartikel"
linkTitle: "Spezialartikel"
weight: 1200
description: >
  Beschreibung der Verwendung spezieller Artikel
---
Es gibt in deinem **Kieselstein ERP** auch einige wenige Artikel die eine besondere Bedeutung haben.

Welche Artikel-Nummern dies sind findest du in der Regel unter System, Parameter.

Diese Artikel werden für besondere Funktionen, z.B. Rundungen, verwendet. D.h. sind diese nicht definiert, angelegt, so kann die diese Verwendende Funktion auch nicht ausgeführt werden und es erscheint eine entsprechende Fehlermeldung.

![](Rundungsartikel_nicht_definiert.png)  

Üblicherweise sind diese Artikel nicht Lagerbewirtschaftet. Für den Rundungsartikel muss auch der Mehrwertsteuersatz auf Steuerfrei gestellt werden.

## Warum werden solche Artikel benötigt?
Ein Beispiel:<br>
Es werden Daten von einem Shop importiert, so rechnet der Shop Brutto (inkl. MwSt). Dein **Kieselstein ERP** rechnet aber netto. So kann es vorkommen, dass geringe Differenzen entstehen. Da aber die Rechnung aus deinem ERP den exakt gleichen Betrag wie deine Shop-Software ausweisen muss, müssen diese Differenzen ausgeglichen werden, wozu eben dieser Rundungsartikel verwendet wird. Dies hat auch den Vorteil, dass dieser Artikel auf ein spezielles Konto buchen könnte, womit die Abweichungen gesammelt in der Buchhaltung ersichtlich sind.

## Welche Definitionen gibt es
### RUNDUNGSAUSGLEICH_ARTIKEL
- Der Rundungsartikel wird weiters für die Rappen / Centrundung verwendet.
- Weiters wird er für den Ausgleich bei den Setartikeln verwendet.
- Er wird für den Woo-Commerce Import verwendet.

{{% alert title="ACHTUNG" color="warning" %}}
Der default Wert des RUNDUNGSAUSGLEICH_ARTIKEL ist "RUNDUNG". Da die Rundung bei jeder Neu-Aktivierung einer Rechnung aus der Rechnung entfernt wird um den richtigen Nettowert berechnen zu können, hat dies auch den Effekt, dass ein Artikel der Rundung benannt wurde immer aus der Rechnung gelöscht wird. Gib daher dem Artikel eine andere Artikelnummer.
{{% /alert %}}


## MINDESTBESTELLWERT_ARTIKEL
Artikelnummer des Artikels, welcher eingefuegt wird, wenn der Mindestbestellwert nicht erreicht wird.

### WERBEABGABE_ARTIKEL
Artikelnummer, welche als Werbeabgabe in die Rechnung eingefügt wird.

## KASSENIMPORT_LEERE_ARTIKELNUMMER_DEFAULT_ARTIKEL

