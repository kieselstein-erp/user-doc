---
title: "Zusammenführen"
linkTitle: "Zusammenführen"
categories: ["Zusammenführen"]
tags: ["Zusammenführen"]
weight: 850
description: >
  Artikel zusammenführen, mach aus mehreren Einen
---
Zusammenführen von Artikeln
===========================

Ist es trotz aller Vorsicht dazu gekommen, dass ein und der exakt selbe Artikel unter zwei verschiedenen Artikelnummern angelegt wurde, so können diese Artikel auch wieder zusammengeführt werden.
Auch hier gilt, eine Rücknahme der Zusammenführung ist nicht möglich.

Für das Zusammenführen muss der Benutzer das Recht WW_ARTIKEL_ZUSAMMENFUEHREN haben.

Im Gegensatz zum Zusammenführen von Partner, Kunden, Lieferanten, ist das Zusammenführen der Artikel, nicht zuletzt aufgrund der Komplexität etwas anders aufgebaut.

![](Zusammenfuehren.jpg)

D.h. Sie wählen in der Auswahlliste der Artikel zuerst den Artikel der in einem anderen Artikel aufgehen sollte.
D.h. es wird links der Quell-Artikel definiert und rechts der im Endeffekt verbleibende Ziel-Artikel. D.h. der Quell-Artikel ist nach der Aktion nicht mehr vorhanden.

Unter der Definition der beiden Artikel finden Sie das Fehler und das Warnungen Fenster.
Einträge im Fehler-Fenster bewirken, dass das Zusammenführen nicht möglich ist.
Einträge im Warnungen-Fenster müssen unbedingt beachtet werden, verhindern aber das Zusammenführen nicht.

Es werden folgende Fehlersituationen abgeprüft:

Es werden folgende Warnungen ausgegeben:

Das Zusammenführen wird im verbleibenden Zielartikel unter Info Änderungen protokolliert. Siehe dazu bitte den Eintrag ![](Artikel_Zusammengefasst.gif) Zusammengef(asst)