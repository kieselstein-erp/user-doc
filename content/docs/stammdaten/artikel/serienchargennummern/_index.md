---
title: "Serien- / Chargennummern"
linkTitle: "Snr/CHG"
categories: ["Artikel"]
tags: ["Seriennummern", "Chargennummern"]
weight: 600
description: >
  Seriennummern und Chargennummern generieren, verwalten
---
Serien- und Chargennummern
==========================

## Wozu dienen Seriennummern?
Seriennummern dienen der eindeutigen Identifikation eines einzelnen Produktes. Seriennummern werden üblicherweise für Geräte verwendet. Insbesondere um Garantieansprüche und dergleichen eindeutig feststellen zu können. Sehr praktisch und hilfreich sind Seriennummern auch im Zusammenhang mit der Erfassung des Lebenslaufes eines Gerätes, der Geräte Historie.

## Wozu dienen Chargennummern?
Chargennummern dienen der eindeutigen Kennzeichnung von Produkten die aus ein und dem selben Fertigungsauftrag stammen. Dies wird z.B. für Farben, Chemikalien, Medikamente, Härteprozesse usw. verwendet. Der Hintergrund ist, dass trotz gleicher Fertigungsparameter geringfügige Abweichungen zustande kommen können, oder dass bis zum Endverbraucher hin erfasst werden muss, wann, wer, mit welchen Maschinen usw. das Produkt hergestellt hat.

Der verwaltungstechnische Unterschied zwischen Seriennummern und Chargennummern ist im wesentlichen, dass Seriennummern immer die Menge 1 Stk. haben und Artikel mit Chargennummern auch eine zugehörige Menge haben.

## Wie wird ein Artikel als Seriennummern tragend gekennzeichnet?
Setzen Sie im Artikel, Sonstiges, die Seriennummern Zwangseingabe.

## Wie wird ein Artikel als Chargennummern tragend gekennzeichnet?
Setzen Sie im Artikel, Sonstiges, die Chargennummern Zwangseingabe.
Hinweis: Ein Artikel kann entweder Seriennummerntragend oder Chargennummerntragend sein. Beides ist nicht möglich.

## Wo werden Seriennummern/Chargennummern verwaltet?
Ist ein Artikel Serien-/Chargennummern tragend, kann eine Lager Zu- oder Abbuchung nur mit der Angabe der Serien/Chargennummer erfolgen. Für jede Seriennummer wird ein eigener Eintrag in der Warenbewegungstabelle erzeugt und in dieser die Bewegung (z.B. Wareneingang aus Bestellung) und die Seriennummer abgelegt. Für jede Chargennummer wird ebenfalls ein eigener Eintrag in der Warenbewegungstabelle erzeugt und in dieser die Bewegung und die Chargennummer abgelegt.

Bitte beachten Sie, dass durch die Seriennummernverwaltung nur lagernde Seriennummer ausgeliefert werden können.

## Erfassung der Seriennummern:
Werden Seriennummern behaftete Artikel gebucht, so werden Sie zur Eingabe der Seriennummer(n) aufgefordert. Die Menge des Artikels und die sich aus den eingegebenen Seriennummern ergebenden Menge müssen übereinstimmen.
Bei der Eingabe von mehreren Seriennummern kann , (Komma) als Trennung zwischen zwei Seriennummern verwendet werden und - (Bindestrich) für die von bis Angabe. Seriennummern müssen am Ende immer numerisch sein, können am Beginn jedoch auch alphanumerisch sein. D.h. Formate wie AB3C5X123 sind durchaus zulässig.
Wenn Sie selbst Seriennummern generieren können, so bedenken Sie bitte, dass möglichst keine Sonderzeichen verwendet werden, damit wird die Seriennummernerfassung mittels Barcodescanner wesentlich einfacher. Zugleich bedeutet die Erfassung mittels Barcodescanner, dass die Fehlerrate gegenüber der manuellen Eingabe drastisch reduziert werden kann.

## Wieso kann ich in einer Seriennummer keinen Bindestrich (-) eingeben?
Wie oben bereits angeführt ist der Bindestrich - als Eingabehilfe für von - bis gedacht.
Um die Seriennummernverfolgung möglichst eindeutig zu ermöglichen haben wir daher nur Ziffern und Buchstaben aber keine Sonderzeichen erlaubt. Sonderzeichen werden in vielen Supply Chain Situationen zur Steuerung z.B. der Erfassung verwendet.
Siehe dazu Seriennummern Erfassungsdialog ohne Komma.

## Definition der Länge von Seriennummern:
Mit dem Parametern ARTIKEL_MAXIMALELAENGE_SERIENNUMMER und ARTIKEL_MINDESTLAENGE_SERIENNUMMER definieren Sie die Länge der Seriennummern.
Bitte beachten Sie, dass alternativ dazu auch eine abweichende Länge der Seriennummer direkt beim jeweiligen Artikel im Reiter sonstiges ![](Seriennummernlaenge_am_Artikel_definieren.gif) definiert werden kann.

## Gültige Zeichen für die Seriennummern
Diese sind A bis Z, 0 bis 9, Punkt(.), Schrägstrich (/), Unterstrich(_)

## Gültige Zeichen für die Chargennummern
Diese sind A bis Z, a bis z, 0 bis 9, Punkt(.), Schrägstrich (/), Prozent (%), kaufmännisches Und (&), Unterstrich (_), senkrechter Strich (|)

#### Wie mit Seriennummern mit Sonderzeichen umgehen?
Für die Erfassung von Seriennummern mit Sonderzeichen wurde der Seriennummerndialog entsprechend ergänzt. D.h.
![](Seriennummer3.gif)
Mit dem Parameter SERIENNUMMER_BEGINNT_MIT kann definiert werden, mit welcher Startsequenz Ihre eigenen Seriennummern beginnen.
Dieser Vorgabewert kann bei der Seriennummernerfassung jederzeit überschrieben werden. Durch den Klick auf das Blatt mit Plus ![](Seriennummer4.gif) wird die nächste freie Seriennummer erzeugt. Ein mehrfacher Klick auf das Blatt mit dem Plus erzeugt entsprechend viele Seriennummern, welche, nur als Anzeige, oben in der Dialogbox erscheinen.

Ist Komma angehakt, so gilt die oben beschriebene Regel für die Erzeugung von einzelnen aber auch mehreren Seriennummern. Wird Komma abgehakt, so erscheint
![](Seriennummer5.gif)<br>
Seriennummer von und Seriennummer bis. Hier gilt einzig die Regel, dass die Seriennummern insofern zusammenpassen müssen, dass nummerisch von von nach bis gezählt werden kann.<br>
Beachten Sie dazu bitte auch den Parameter SERIENNUMMER_NUMERISCH = 1, welcher nur Seriennummern die aus Zahlen bestehen zulässt.
Mit dem Parameter DEFAULT_SNR_DIALOG_KOMMA kann definiert werden, wie die Voreinstellung der Erfassung nach Komma oder eben nicht gesetzt sein sollte.

## Eineindeutige Serien-/Chargennummern
Mit dem Parameter LAENGE_CHARGENNUMMER_EINEINDEUTIG definieren Sie, ob Chargennummern eineindeutig sind. Wenn der Parameter gesetzt ist, dann wird bei einer neuen Losablieferung die nächste gültige Chargennummer vorgeschlagen und bei der Zubuchung sichergestellt, dass nur numerische Chargennummern verwendet werden.
Mit dem Parameter SERIENNUMMER_EINEINDEUTIG definieren Sie, dass Seriennummern eindeutig über das ganze Unternehmen hinweg sind. Setzen Sie zum Generieren von Seriennummern zusätzlich den Parameter SERIENNUMMER_NUMERISCH auf 1, somit wir die nächste gültige Seriennummer vorgeschlagen.

## Mindesthaltbarkeit:
Bei vielen Produkten, vor allem der Lebensmittelbranche, ist zusätzlich zur Chargennummer auch das Mindesthaltbarkeitsdatum anzugeben. Wir haben dies so gelöst, dass in diesem Fall neben dem Mindesthaltbarkeitsdatum immer auch eine Chargennummer angegeben werden muss.

#### Was passiert, wenn eine lagernde Seriennummer noch einmal zugebucht wird ?
Da dies nicht zulässig ist kommt eine entsprechende Fehlermeldung.

![](Seriennummer_ist_schon_auf_Lager.jpg)

#### Abschalten der Chargenführung / Seriennummernführung?
Vom Grundgedanken her muss bei der Definition eines Artikels entschieden werden ob dieser Seriennummern- bzw. Chargennummern- geführt ist.<br>
Daher ist eine Änderung ob ein Artikel Seriennummern geführt ist oder nicht, dann nicht mehr möglich, wenn bereits eine Lagerbuchung auf diesen Artikel erfolgt ist. [Siehe dazu bitte](#Abschalten Seriennummern tragend).<br>
Gerade das nachträgliche Aktivieren von Seriennummern beschreibt dies sehr gut, da ja hier
- für jede Lagerbuchung nachträglich Seriennummern vergeben werden müssten und
- die richtige Zuordnung zwischen zugebuchter und abgebuchter Seriennummer praktisch im nachhinein nicht möglich ist.
Hier kommt dazu, dass hinter den Seriennummernbuchungen die Logik steckt, welche besagt, dass eine Seriennummer immer die Menge 1 hat. Daher wären auch sämtliche Lagerbuchungen zu ändern.

Bei der nachträglichen Aktivierung des Chargennummern ist dies vom Thema her ähnlich, da jedoch Chargennummern auch verschiedene Mengen haben, haben wir hier eine Möglichkeit geschaffen, diese nachträglich zu aktivieren bzw. wieder zu deaktiveren.
Bei der nachträglichen Aktivierung der Chargennummernführung wird für die bestehenden Lagerbuchungen in der Chargennummer "keine Chargennummer" eingetragen. Bei der Deaktivierung wird jeder Chargennummerneintrag des gewählten Artikels gelöscht.

#### Buchung von Chargennummern
<a name="Chargennummer"></a>
Die Buchung von Chargennummern erfolgt immer durch Klick auf den Button ![](Chargennummer1.gif) Chargennummer. Sie erhalten hier eine Auswahlliste

![](Chargennummer2.jpg)

mit der der gesamte Erfassungsdialog für die Chargen durchgeführt werden kann.

Für die Zubuchung von Chargen, z.B. bei einem Wareneingang, geben Sie bitte in das linke untere Feld direkt die Chargennummer ein und daneben erfassen Sie die zugebuchte Menge. Durch Klick auf ![](Chargennummer3.gif) oder durch drücken der Enter-Taste wird die Eingabe in das obere Feld übernommen.

Bei einer Fehleingabe kann oben die Zeile markiert werden und durch Klick auf das Rote X wird die Zeile gelöscht.

![](Chargennummer6.jpg)Mit Klick in das Mengenfeld kann die eingegeben Menge geändert werden.

**WICHTIG:**

Die Eingabe der Menge im Tabellenfeld muss mit Enter bestätigt werden.

Nur dadurch wird die Menge geändert. Sie sehe dies auch in der Anzeige der gesamt erfasst Menge.

Über der Mengenspalte sehen Sie die Gesamtsumme der erfassten Mengen.

Haben Sie alle Chargen erfasst, so klicken Sie bitte auf Übernehmen.

Bei der Abbuchung von Chargen steht zusätzlich der ![](Chargennummer4.gif) Neuknopf zur Verfügung. Damit erhalten Sie eine Liste der derzeit lagernden Chargennummern und deren Menge. 

![](Chargennummer5.gif)

Wählen Sie die gewünschte Charge aus und übernehmen Sie diese durch Klick auf Übernehmen.

Haben Sie nun alle Chargenpositionen erfasst, so klicken Sie auf Übernehmen. Damit wird automatisch die Menge auf die eingegebene Menge aufgrund der Chargen gesetzt.

Durch Klick auf das X rechts oben kann die jeweilige Erfassung abgebrochen werden.

Bitte beachten Sie, dass Positionen mit Chargennummern nicht mehr geändert werden können. Für eine Änderung z.B. der Menge löschen Sie bitte die Position und buchen Sie diese danach erneut ein.

<a name="Seriennummer"></a>

#### Buchung von Seriennummern
Die Buchung von Seriennummern erfolgt immer durch Klick auf den Button ![](Seriennummer1.gif) Seriennummer. Sie ist von der Bedienung her gleich wie die Chargennummer, mit dem Unterschied, dass je Seriennummer die Menge auf 1 fixiert ist und daher nicht angezeigt wird.

Um rasch mehrere Seriennummern übernehmen zu können, können mehrere Zeilen wie in Ihrem Betriebssystem üblich markiert werden.

![](Seriennummer2.jpg)

Im Gegensatz zu den Chargennummern können Artikel mit Seriennummern wieder korrigiert werden, also einzelne Seriennummern wieder ans Lager zurückgegeben, bzw. vom Lager entnommen werden.

Zusätzlich können bei der Erfassung von Zugängen (Wareneingang) auch Seriennummernbereiche eingegeben werden. Also für z.B. 100 Stk. 10000-10099. Dies wird vom Dialog sofort in 100 einzelne Seriennummerneinträge aufgelöst.

Für die Erfassung der Seriennummern können auch sogenannte Wedge CCD Scanner eingesetzt werden. Diese werden in die Tastatur eingeschliffen und senden als Scanncode immer Seriennummer+CR. Somit scannen Sie einfach die Seriennummern der erhaltenen Geräte ab und erhalten so die Liste der erhaltenen Seriennummern.

#### Wo kann ich Seriennummern bzw. Chargennummern korrigieren?
Wenn bei der Eingabe von Seriennummern ein Fehler passiert ist, so können Sie diese Buchung zurücknehmen und neu erfassen. Sollte das nicht mehr möglich sein, da weiter Buchungen erfolgt sind, gibt es die Möglichkeit im Modul Artikel, Menüpunkt Pflege die Seriennummer zu korrigieren.
Wählen Sie dazu in der Auswahl den zu ändernden Artikel aus und klicken auf Serien-/Chargennummern ändern.
Durch Klick auf ![](snr.jpg) öffnet sich die Auswahl der vorhandenen Seriennummern (plus deren etwaigen Versionen)
In der folgenden Erfassungsmaske sehen Sie die aktuelle Seriennummer und Version.
Die ursprüngliche SNR/CHNR wird bei Auswahl als Vorschlag in die zu ändernde SNR/CHNR übertragen.
Geben Sie im darunterliegenden Feld die korrigierte Seriennummer und Version ein.
![](snr_vers_aendern.JPG)
Mit dem Klick auf speichern Sie die Änderungen ab und Sie erhalten eine Information wieviele Buchungen geändert wurden.
![](Buchungen_geaendert.JPG)

**ACHTUNG:** Diese Pflegefunktion ist wirklich als Pflegefunktion gedacht. Insbesondere wenn eine Seriennummer offiziell geändert wird, muss die alte SNr abgebucht werden und die neue Zugebucht werden.
Sollte dies für Sie relevant sein und eine Verlinkung der Seriennummern gewünscht werden, so wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer mit dem Stichwort PJ20155.

#### Können einzelne Chargen gesperrt werden?
<a name="Chargen sperren"></a>
Jein. D.h. es ist nicht vorgesehen einzelne Chargen oder auch Seriennummern zu sperren, aber:<br>
Buchen Sie die gesamte Menge oder auch eine Teilmenge einer Charge in ein Lager um, welches nur von bestimmten Mitarbeitern verwendet werden darf, so ist diese Menge für alle anderen nicht mehr verwendbar. Ist das Lager nun von der Lagerart Sperrlager, so wird der Lagerstand, normalerweise, in den Übersichten usw. nicht berücksichtigt. Sollten Sie die gesperrte Charge, z.B. durch Nacharbeit o.ä. wieder verwenden wollen / können, so kann, durch einen Mitarbeiter der eine entsprechende Rolle mit Berechtigung auf dieses Lager hat, die Charge wieder in das Los oder auch in Lieferscheine usw. verwendet / gebucht werden.

#### Können Anmerkungen zu einzelne Chargen angegeben werden?
Um bei Chargen auch Anmerkungen hinzufügen zu können, kann die sogenannte Version verwendet werden. Stellen Sie dafür den Parameter VERSION_BEI_CHNR_MITANGEBEN auf 1.
Eine mögliche Anwendung ist, gerade in der EMS Branche, dass die SMD Rollen vereinzelt werden und so auch hinterlegt wird wo welche Charge zwischengelagert wird.

#### Wo findet man die Seriennummern bzw. Chargennummern Historie?
Verwenden Sie dazu im Modul Artikel das Serien-/Chargennummern Journal.<br>
![](Seriennummern_Journal.jpg)<br>
In der Regel wird dafür nur die Serien- bzw. Chargennummer eingegeben.<br>
Wenn die Serien/Chargennummer exakt bekannt ist, so kann diese in das obere Feld eingegeben werden. Wenn nur Teile der Serien/Chargennummer bekannt sind, so aktivieren sie die untere Zeile (Serien/Chargennr enthält) und geben Sie bitte den bekannten Teil der Nummer ein. Ev. auch nur Teile der Versionsnummer, welche Sie im Feld Version erwarten, z.B. für eine bestimmte Softwareversion.<br>
Beachten Sie bitte, dass auch bei der Eingabe einer Seriennummer 2018-0442 keine Auflösung in einzelne zu suchende Seriennummern erfolgt. Es wird nach dem eingegebenen Text gesucht.

Bei Verwendung des Moduls [Geräteseriennummern]( {{<relref "/fertigung/losverwaltung/geraeteseriennummern" >}} ) kann auch noch der Ausweis des verwendeten Gerätes mit ausgewählt werden. D.h. Sie erkennen hier in welchem Gerät und unter welcher (Haupt-) Geräteseriennummer diese Unterbaugruppe (die Ihr Service gerade bekommen hat) verbaut wurde und somit entsprechende Garantieansprüche und ähnliches sehr rasch klären.

#### Kann definiert werden wie neue Artikel angelegt werden?
Für die Anlage neuer Artikel ist es oft praktisch, dass von vorne herein definiert wird, dass ein Artikel Serien- oder Chargennummern tragend ist.
Nutzen Sie dafür die Parameter ARTIKEL_DEFAULT_CHARGENNUMMERNBEHAFTET bzw. ARTIKEL_DEFAULT_SERIENNUMMERNBEHAFTET.
Zusätzlich kann anhand der Artikelgruppe definiert werden, wie ein Artikel bei der Neuanlage angelegt wird. Beachten Sie bitte, dass die Definition der Artikelgruppe Vorrang vor der Definition durch die Parameter hat. D.h. wenn die Artikelgruppe auf Seriennummernbehaftet gestellt ist, ist es egal wie die beiden Systemparameter eingestellt sind. Steht die Artikelgruppe jedoch auf ohne, so wirken die oben angeführten Systemparameter.

#### Wie finde ich schnell Serien- bzw. Chargennummern?
Sie können dafür auch den unteren Modulreiter Warenbewegungen im Artikelstamm verwenden. Hier kann auch nach Teilen von Seriennummern gesucht werden.

### Abschalten Seriennummern tragend
<a name="Abschalten Seriennummern tragend"></a>

#### Wie kann ich einen Artikel doch von Seriennummern geführt auf nicht Seriennummern geführt setzen?
Wie oben beschrieben ist die Umschaltung bei bereits erfolgten Lagerbuchungen nicht mehr möglich.<br>
Wir haben dies jedoch so erweitert, dass wenn alle bisherigen Lagerbewegungen gelöscht wurden, kann diese Eigenschaft des Artikels doch noch geändert werden.<br>
Wichtig: Es müssen dafür alle Historischen Bewegungsdaten des Artikels gelöscht werden.
Die Vorgehensweise um einen Artikel umzuschalten ist wie folgt:<br>
- a.) Prüfen Sie in der Artikelstatistik ob Lagerbewegungen angezeigt werden. Sollten Lagerbewegungen angezeigt werden, so müssen diese gelöscht werden. Denken Sie bitte auch an die Anzeige der Handlagerbewegungen bzw. an Inventurdaten.
- b.) Sind nun alle Lagerbewegungen entfernt, so wechseln Sie im Artikel auf den Reiter Sonstiges und schalten wie gewünscht von Seriennummern tragend auf nicht Seriennummerntragend bzw. umgekehrt.

### Erzeugen eines Nachfolgers mit Seriennummerntragend
Trotz der oben dargestellten Thematik ergibt sich immer wieder der Wunsch, dass man einen nicht Seriennummern geführten Artikel in einen Seriennummern geführten Artikel umwandeln will / muss.<br>
Die Praxis hat uns gezeigt, dass es hier noch die zusätzliche Herausforderung gibt, dass Artikel mit der bisherigen Eigenschaft (nicht Seriennummern geführt) auf Lager liegen oder in Lose verbaut sind und dass eben erst bei den neuen produzierten / eingekauften Artikeln diese Seriennummern ins Lager zugebucht / verbaut werden sollen. D.h. man muss beide Typen der Artikel auf Lager haben.<br>
Daher gibt es ab der Version 1.0.6 die Möglichkeit einen nicht Seriennummern geführten Artikel in einen Seriennummern geführten umzukopieren oder vice versa.<br>
Dabei werden folgende Schritte automatisch durchgeführt:
- Umbenennen der Artikelnummer des Ausgangsartikels auf hinten -SN\# (<u>S</u>erien<u>n</u>ummer laufend raufgezählt, so dass es keine doppelten Artikelnummern gibt. Sollte es theoretisch nur einmal geben. Es ist jedenfalls nicht dafür gedacht laufend hin und her zu schalten. Wenn aber das # > 9 würde, wird abgebrochen)
- Verstecken des Ausgangsartikels
- vollständiges kopieren des Ausgangsartikels auf einen neuen Artikel, sodass dieser identisch ist inkl. Dokumenten, Kommentare und ev. Verlinkung auf die Referenzdokumente. Da gehören auch alle Verkaufspreise (auch die vergangenen), Staffelpreise, Sokos mit Mengenstaffeln mit dazu.
- Setzen des Ersetzt durch am Ausgangsartikel (Reiter sonstiges)
- Ändern aller Stücklisten-Positionen in denen der Ausgangsartikel verwendet wurde auf den neuen Artikel.
- Ändern aller angelegten, offenen Anfragen und Bestellungen auf den neuen Artikel (sieht ja optisch gleich aus). Bei den Aufträgen/Bestellungen nur die Positionen deren Status offen ist. Erledigte nicht ändern. Teilerledigte müssen vom Anwender selbst angepasst werden.
- Ändern aller Lose im Status angelegt, sowohl der Materialposition als auch der Stücklisten Zuordnung. Dies hat den Vorteil, dass noch nicht verbrauchte Artikel ohne Seriennummern am Lager bleiben und im Los / Lieferschein verbraucht werden können.
- Ändern aller Angebote im Status angelegt oder offen
- Ändern aller Aufträge im Status angelegt oder offen. Für die Teilgelieferten sieht dann der Anwender die Artikelnummer des Ausgangs-Artikels und muss / kann selber entscheiden was er machen will. Die ohne SNr ausliefern oder die AB ändern. Das kann damit soweit gehen, dass ein Teil ohne und eine andere Menge mit SNr geliefert wird. Dafür Änderung der AB erforderlich.
- Eintragen der Änderung in das Änderungsprotokoll auf dem neuen Artikel ... wurde erzeugt aus .... Im Ausgangsartikel sollte das setzen des Ersatzartikels schon protokolliert werden.

Das bedeutet, wenn du bei einem z.B. nicht Seriennummerngeführten Artikel diesen auf Seriennummerngeführt ändern willst, so erscheint die Frage
![](Seriennummerngefuehrt_aendern.png)
Beantwortest du diese mit Ja, so werden oben beschriebene Aktionen ausgeführt.<br>
D.h. im Endeffekt hast du nun zwei Artikel in deinem Artikelstamm.<br>
![](Aenderung_der_Seriennummerneigenschaft.png)<br>
Den Artikel davor, welcher in seiner Nummer hinten um -SN1 ergänzt ist, versteckt ist und der mit dem neuen Artikel verlinkt ist. Siehe dazu Reiter Sonstiges, Ersatzartikel.<br>
Damit kannst du in der Artikelstatistik durch Auswahl von Vorgänger die Gesamtstatistik der Artikel einsehen.

#### Wie kann eine sichere Verwendung z.B. für die Medizinbranche und ähnliche Produkte zum Zwecke der Rückverfolgbarkeit eingerichtet werden?
Nachdem von unseren Anwendern immer wieder um eine Beschreibung geben wird, wie für ähnliche Produkte vorzugehen ist, nachfolgende Zusammenfassung:

- Es sollte gewährleistet werden, dass aus dem Sperrlager keine Waren entnommen werden können.
- der Administrator kann jederzeit neue Rollen definieren, somit sollte auch gewährleistet sein, dass die/alle Anwender sich normalerweise NICHT mit administrativen Rechten anmelden sondern mit den ihnen zugewiesenen Rollen
- Neue Rollen definieren Sie im Modul Benutzerrechte und dann unterer Reiter Systemrolle. Im dann oberen Reiter Rechte, bitte die entsprechenden Rechte vergeben und auch die Lager auf die diese Rolle einen Zugriff haben darf.
- Das default Lager sollte auf ein Prüflager / Quarantäne Lager eingerichtet werden. Nur nach einer Freigabe werden die Waren dann in das Auslieferlager umgebucht.<br>
D.h. hier geht es darum, dass jeder Wareneingang zuerst auf das Prüflager gebucht werden sollte.<br>
Stellen Sie dafür bitte für jeden Lieferanten sein Zu(Buchungs)-Lager auf das Prüflager.<br>
Zugleich sollten Sie, damit bei neuen Lieferanten das Prüflager automatisch vorgeschlagen wird, das Prüflager als Hauptlager definieren. D.h. im Modul Artikel, unterer Reiter Grunddaten, oberer Reiter Lager und hier zuerst das bisherige Hauptlager auf Lagerart Normal umstellen und dann das Prüflager Lager als Hauptlager deklarieren.
- Kann in **Kieselstein ERP** ein Haltbarkeitsdatum aufgeführt werden?<br>
Es gibt die Möglichkeit die MHD Verwaltung zu aktivieren. Stellen Sie dazu den Parameter CHARGENNUMMER_BEINHALTET_MINDESTHALTBARKEITSDATUM auf 1\.<br>
Jedoch **ACHTUNG:** Damit wird für JEDEN Chargengeführten Artikel ein Mindesthaltbarkeitsdatum erzwungen.<br>
Auswertungen dazu finden Sie im Modul Artikel, Journal, Mindesthaltbarkeit
- Wir müssen die Rückverfolgbarkeit der Waren anhand der Chargennummer nach Versendung an jeden Kunden gewährleisten können. Könnten Sie bestätigen, dass bei jedem Lieferschein oder jeder Rechnung von Waren mit einer Chargennummer die Chargennummer erfasst werden muss und so auch klar ist, welche Charge an welchen Kunden versendet wurde? Wie kann **Kieselstein ERP** hier eine exakte Übersichtliste generieren?<br>
Ja, wir garantieren, dass, wenn ein Artikel als Chargengeführt gekennzeichnet ist, es keine Lagerbewegung ohne der Angabe einer Chargennummer möglich ist.<br>
**Dazu der wichtige Hinweis:** Diese Artikeleigenschaft kann jederzeit von einem Berechtigten umgeschaltet werden. Diese Änderung wird protokolliert.<br>
**Daher wichtig**: Es dürfen nur ausgewählte Personen die Artikel definieren.

- Im Worst Case Szenario, dass mit einer Charge einen Rückruf gestartet werden muss, wie sieht die schnellste Lösung über **Kieselstein ERP** hier aus?<br>
Wählen Sie im Artikel, den Menüpunkt Journal, Serien-/Chargennummern. Geben Sie die gesuchte Chargennummer bzw. Teile davon ein. Sie erhalten sofort eine vollständige Übersicht über alle Warenbewegungen die mit dieser Chargennummer erfolgt sind. Darin sind alle Warenzugänge und alle -abgänge mit Lieferanten/Kunden, Lieferscheinnummern, Lieferdatum usw. enthalten. Aus dem Journal heraus können Sie mit einem Klick direkt z.B. in den Lieferschein springen.

#### Vorab drucken von Seriennummern
Gerade in der Produktion von eigenen Geräten / Produkten will man immer wieder auch eigene Seriennummernetiketten, Typenschilder und ähnliches ausdrucken.
Hier gibt es zwei Vorgehensweisen:
- a.) Bei der Losablieferung wird im Seriennummerndialog die Funktion generiere nächste Seriennummer genutzt. Damit die Seriennummer für diese einzelne Ablieferung festgelegt und anschließend die Ablieferetikette mit der Seriennummer ausgedruckt.
- b.) Es müssen vor der Fertigung bereits die Seriennummern definiert sein. D.h. es sind vorab bereits Seriennummern zu drucken, von denen allerdings Ihr **Kieselstein ERP** nichts weiß.<br>
Dafür gibt es wiederum zwei Möglichkeiten:
- b1.) die Artikeletikette
![](Seriennummernetikette_drucken1.jpg)
D.h. über ein speziell eingerichtetes Etikettenformular, geben Sie einerseits die gewünschte Startnummer unter LfdNr.: an und andererseits die Anzahl der gewünschten Etiketten unter Exemplare.
Die benötigte Kombination von Artikelnummer und laufender Nummer, mit den verschiedensten Steuerzeichen nach GS1 usw. können im Etikettenformular eingerichtet werden.

- b2.) Die Los-Etikette
Unter Los, Drucken, Etikette steht ebenfalls ein Etikettendruck zur Verfügung.
![](Seriennummernetikette_drucken2.jpg)
Nutzen Sie dafür das per default als Report 2 benannte Formular. Tragen Sie hier ebenfalls die Anzahl der gewünschten Etiketten unter Exemplare ein. Unter Kommentar geben Sie die Start-Seriennummer für den Druck ein.
Auch hier gilt, dass der tatsächliche Inhalt Ihres Seriennummernetikettes Ihren Wünschen gemäß gestaltet werden kann.

##### Welche der beiden Versionen sollte verwendet werden?
Das hängt von den auf den Etiketten anzudruckenden Daten ab. D.h. wenn Sie z.B. auch Kundendaten, Auftragsnummern, Kundenlogos und ähnliches benötigen, so steht dies beim Druck aus dem Los heraus meist deutlich einfacher zur Verfügung.

**Tipp:** Um schnell Herauszufinden für welchen Artikel welche Seriennummer die zuletzt zugebuchte ist, nutzen Sie im Artikel den unteren Modulreiter Warenbewegungen. Nun filtern Sie auf die gesuchte Artikelnummer und sortieren nach der Spalte Snr/Chargennr.
![](Seriennummer_finden.jpg)

### Hierarchische Chargennummern
<a name="Hierarchische Chargennummern"></a>
Gerade in Fertigungsbetrieben, bei denen eine durchgängige Chargenverfolgung auch über mehrere Fertigungsebenen hinweg erforderlich ist, sind hierarchische Chargennummern eine praktische Funktion.
Dafür steht die Funktion AUTOMATISCHE_CHARGENNUMMER_BEI_LOSABLIEFERUNG zur Verfügung.
Steht dieser Parameter auf 2, so wird bei der Losablieferung auf Klick automatisch die im Los verbrauchte Chargennummer als Basis verwendet und um eine fortlaufende Nummer ergänzt.
Die Länge der fortlaufenden Nummer kann mit dem Parameter LAENGE_HIERARCHISCHE_CHARGENNUMMER definiert werden.

Um dies umzusetzen sind folgende Voraussetzungen erforderlich, welche bei der Erzeugung der fortlaufenden Chargennummer geprüft werden.
Wichtig: Die errechnete Chargennummer kann jederzeit durch den Anwender übersteuert werden. Die Verantwortung für die tatsächlich verwendete Chargennummer liegt bitte beim Anwender.

Voraussetzungen:
- die Stückliste ist als "hierarchische Chargennummer" (verwenden), in den Stücklisten Kopfdaten gekennzeichnet. Dies ist nur sichtbar, wenn die Stückliste Chargennummernbehaftet und der Parameter AUTOMATISCHE_CHARGENNUMMER_BEI_LOSABLIEFERUNG =2 eingestellt ist.<br>
In diesem Falle ist per default "hierarchische Chargennummer" angehakt

- Bei der Losablieferung muss nach wie vor der Anwender im Chargennummern-Dialog auf "generiere Chargennummern" klicken.<br>
Nun wird geprüft, ob es genau eine Materialposition gibt, die chargennummernbehaftet ist und von diesem Artikel nur eine Charge auf das Los gebucht worden ist.<br>
Ist dies nicht gegeben (keine Charge, mehrere Chargen, mehrere Artikel die Chargengeführt sind), so erscheint eine entsprechende Fehlermeldung und die Erzeugung der Chargennummer ist nicht möglich.

- D.h. es ist entweder das Los richtigzustellen, in dem z.B. auf die Chargenreinheit geachtet wird, oder die Chargennummer manuell einzugeben.<br>
Bei der manuellen Eingabe sollte auf eine deutliche Trennung zu den automatisch erzeugten Chargennummern geachtet werden.

- Entspricht das Los den formalen Anforderungen, so wird die vorhandene Chargennummer verwendet und um einen Punkt "." und die fortlaufende Nummer, welche vorne mit Nullen aufgefüllt wird, ergänzt.<br>
**ACHTUNG:** Der Benutzer kann zu jeder Zeit die erstellte Chargennummer übersteuern (auch wenn eine Generierung nicht möglich ist).<br>
<u>Info:</u> Jedes mal wenn auf den Button gedrückt wird, erscheint eine neue Nummer, wodurch es zu Lücken in den vorhandenen Chargennummern kommen kann

Ein Beispiel für die Anwendung ist, wenn z.B. die Anlieferung der Rohware in Coils erfolgt und diese, je nach Bedarf in unterschiedlicher Tiefe weiterverarbeitet werden. Mit der hierarchischen Chargennummer ist somit auf einen Blick sofort ersichtlich aus welcher Basis Charge das (fertige Teil) erzeugt wurde.<br>

Beispiel:
| Rohware(Coil) | Zwischenprodukt(Trommel) | Endprodukt(Ring) |
| --- |  --- |  --- |
| 90/555 | 90/555.123455 | 90/555.123455.123456 |

Der Vorteil ist, dass man damit immer die ursächliche Chargennummer der Rohware erkennt. Somit ist auch die Zuordnung für die verschiedenen Zeugnisse deutlich einfacher und für Ihren Kunden nachvollziehbar.
Dies bedingt zugleich Chargenreinheit in der Produktion. D.h. es kann nur eine Charge auf das Los ausgegeben werden.

| Rohware | Zwischenprodukt | Fertigprodukt | Chargennummer | Bemerkung |
| --- |  --- |  --- |  --- |  --- |
| Coil2 |  |  | 90/556 |  |
|  |  | Ring1 | 90/556.123457 | Direkt aus der Rohware gefertigt |
|  | F6116 |  | 90/556.123458 |  |
|  |  | F6116-FZ | 90/556.123458.123459 | Über das Zwischenprodukt gefertigt |