---
title: "Lager Doppelbuchung vermeiden"
linkTitle: "Lager Doppelbuchung vermeiden"
categories: ["Lager Doppelbuchung"]
tags: ["Lager Doppelbuchung"]
weight: 900
description: >
  Wie kann man bei externen Fertigungsprozesse die Lagerdoppelbuchung vermeiden
---
Wie kann die Lagerdoppelbuchung bei veredelten Artikeln verhindert werden?
========

Insbesondere in der Metallverarbeitung ist es üblich, dass Teile
vom Lieferanten L gelasert werden und vom Lieferanten Z Verzinkt werden.

Da nun die Daten aus der CAD kommen und die CAD nur ein Blechteil kennt, ist der gesamte Fertigungsprozess (dieser einen Blechplatte) nicht abgebildet.

Dies ist bei 250 beteiligten Baugruppen, je Anlage, durchaus verständlich.

Eine saubere und nachvollziehbare Lösung wäre, für diesen Fertigungsprozess eigene Stücklisten und damit Lose anzulegen. Der besondere Vorteil dieses Ansatzes wäre, dass man sehr klar erkennt welche Teile man in welchem Fertigungsstatus zur Verfügung hat.

Gerade bei Anlagen, welche in der Regel immer Unikate sind, aber aus sehr vielen verschiedenen Teilen bestehen, wird in der Praxis der Weg gewählt, dass die Teile als Eins betrachtet werden, egal in welchem Fertigungsstand sie sind. Meist ist es auch so, dass der Kostenfaktor des Veredelns in der Anlagen Gesamtsumme vernachlässigt werden kann, aber man braucht trotzdem eine Bestellung an den Verzinker und auch die Rückmeldung, welche Teile gekommen sind.

D.h. in der Praxis werden die Laserteile per Bestellung beim Lieferanten L bestellt und die Wareneingänge geprüft und zugebucht.<br>
Dann wird die Bestellung an den Lieferanten L kopiert und auf den Lieferanten Z umgeändert. Die Einkaufspreise werden gleich belassen.<br>
Damit gehen die gelaserten Teile zum Verzinker. Nun kommt die veredelte Ware und es wird erneut der Wareneingang gebucht. Da das die gleiche Artikelnummer ist, hat man nun den doppelten Lagerstand was für die Lagerwirtschaft inakzeptabel ist (auch wenn das viele Unternehmen so machen).

Die Lösung dafür ist, dass z.B. der Warenzugang des zweiten Veredelungsschrittes auf ein anderes Lager gebucht wird. Dieses Lager kann beim Lieferanten hinterlegt werden, womit das faktisch automatisch geschieht. Man sollte dieses zusätzliche Normal-Lager so benennen, dass alle Beteiligten wissen, dass dieses eben dies das  Lager aufgrund des Veredelungsschrittes ist.<br>
Ob man nun die Teile aus dem erste externen Fertigungsschritt auf ein Zwischenlager legt oder die aus den Folgefertigungsschritten ist eher Geschmackssache. Wenn die ersten Teile auf einem Zwischenlager liegen und man die Teile mit einem Lieferschein an den Lieferanten sendet, 
hätte dies den Vorteil, auch über diesen Prozessschritt mehr Informationen zu haben. Also zu sehen was noch halbfertig ist und in den Losen dann die fertigen fremdgefertigten Teile zu verbrauchen.

Was wir jetzt noch finden sollten, ist, wie wir die Kosten des externen Veredelns auf den Wareneingang bekommen.<br>
Ev. damit, dass wir für diese Lieferung den Betrag Fixkosten o.ä. haben. D.h. der Einkaufspreis bleibt wie vom Laser-Lieferanten definiert und dann kommt vom Verzinker ja nur die Rechnung für das Verzinken von  xx kg Material, was in der Regel ein Pauschalbetrag ist.<br>
Wenn wir das nun als "Transportkosten" zuweisen, wird das Verzinken vom Wert her auf die einzelnen Teile verteilt, was genau dem entspricht wie es in der Praxis gemacht wird.

**Wichtig:** eine weitere mehrstufige Abbildung würde ich so nicht machen.

Das Lager des ersten Fertigungsschrittes könnte Halbfertig lauten.

Wichtig ist bei dieser Vorgehensweise, dass der Wertzuwachs im Einkaufspreis berücksichtigt ist. D.h. der Einkaufspreis der zweiten Stufe ist auch der Einkaufspreis der ersten Stufe und es kommen über die Transportkosten die pauschalen Gesamtkosten der zweiten Stufe dazu.

Ev. könnte man dem Lieferanten der zweiten Stufe ein spezielles Kennzeichen geben, sodass auf der Bestellung zwar die Teile aber keine Preise angedruckt werden. Dafür aber die geplanten Gesamtveredelungskosten (den Transportkosten).