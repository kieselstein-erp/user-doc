---
title: "Dual Use"
linkTitle: "Dual Use"
categories: ["Dual Use"]
tags: ["Dual Use"]
weight: 1000
description: >
  Dual Use bezeichnet man Waren die sowohl für zivile als auch für militärische Zwecke eingesetzt werden können.
---
Dual Use
========

Als Dual Use Waren werden diejenigen Waren gekennzeichnet, welche sowohl für zivile als auch für militärische Zwecke verwendet werden können.
Daher sind beim Verkauf dieser Waren Länderabhängige umfangreiche Vorschriften zu beachten.
Bitte beachten Sie dazu gegebenenfalls auch das Thema des präferenzbegünstigten Warenverkehrs / Ursprungserklärungen.

Mit diesem Zusatzmodul können Artikel im Reiter Sonstiges, als Meldepflichtig und oder als Bewilligungspflichtig gekennzeichnet werden.
Dies bewirkt, dass in der Auswahlliste der Artikel diese beiden Informationen mit angezeigt werden.
In der Gesamtkalkulation der Stücklisten wird diese Information auf den einzelnen Positionen mit angedruckt.
Und in der Angebotsvorkalkulation werden ebenfalls die melde- bzw. bewilligungspflichtigen Positionen entsprechend gekennzeichnet.

Bitte beachten Sie, dass keine wie immer geartete programmtechnische Verriegelung implementiert sind.
Da diese Regeln auch immer von der aktuellen internationalen politischen Lage abhängig sind, sind sie einer entsprechend häufigen Veränderung unterworfen.
Gegebenenfalls nutzen Sie bitte zur Dokumentation der erteilten Bewilligungen die Dokumentenablage bei den jeweiligen Belegen (Angebote, Aufträge, ..).
Es kann sich durchaus als praktisch erweisen, hier eine eigene Belegart in der Dokumentenablage zu definieren.