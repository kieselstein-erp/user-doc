---
title: "DSGVO"
linkTitle: "DSGVO"
categories: ["DSGVO"]
tags: ["Datenschutz Grund Verordnung"]
weight: 390
description: >
  Datenschutzgrundverordnung und das DSGVO Modul
---
## DSGVO Modul

Um die Anforderungen der DSGVO leichter erfüllen zu können, steht diese DSGVO Modul zur Verfügung.

Damit werden insbesondere folgende Punkte abgedeckt:
-   Übersicht über die in **Kieselstein ERP** verarbeiteten Daten und deren Relevanz in Bezug zu personenspezifischen Merkmalen
-   Beauskunftung
-   Anonymisierung
-   Re-Anonymisierung nach dem eventuellen Einspielen eines Backups

Das DSGVO Modul ist im Personalmodul angeordnet und steht bei vollen Rechten auf das Personalmodul zur Verfügung. Hinweis: Dieses Modul muss für Sie freigeschaltet werden. Wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.<br>
Im Personalmodul finden Sie einen unteren Modulreiter DSGVO<br>
Hier finden Sie alle Partner-Adressen die in Ihrem **Kieselstein ERP** Datenbestand gespeichert sind.
Suchen Sie hier die zu beauskunftende Person, üblicherweise wird über die erweiterte Suche nach Fragmenten des Namens oder der EMail Adresse gesucht. Bitte denken Sie auch daran, dass die als versteckt markierten Adressen mit-durchsucht werden sollten. Gegebenenfalls nutzen Sie bitte auch die Gelegenheit und führen Sie doppelt und dreifach gespeicherte Adressen zusammen und bereinigen Sie Ihren Datenstamm. Siehe dazu bitte zusammenführen.

## Datenschutzgrundverordnung
Am 4\. Mai 2016 wurde die "Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27\. April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten, zum freien Datenverkehr und zur Aufhebung der Richtlinie 95/46/EG (Datenschutz-Grundverordnung)" kundgemacht.

Die Datenschutz-Grundverordnung trat am 25\. Mai 2018 in Geltung. Bis dahin müssen alle Datenanwendungen an die neue Rechtslage angepasst werden.

Um diesen Anforderungen gerecht zu werden raten wir, aus Sicht von **Kieselstein ERP**, folgende Punkte zu beachten, bzw. dürfen wir Ihnen nachfolgende Informationen dazu geben.

Wir weisen ausdrücklich darauf hin, dass wir weder Rechtsanwälte noch sonstige Unternehmens-Berater sind. Wir übernehmen keine wie immer geartete Verantwortung / Haftung für diese Informationen. Weder auf Vollständigkeit noch auf Richtigkeit oder ähnliches.

Im englischsprachigen Raum wird die DSGVO GDPR genannt.

Bitte denken Sie daran, dass die DSGVO für alle Daten Ihres Einflussbereiches / Ihres Verantwortungsbereiches gilt. Egal ob diese in Ihrer IT gespeichert sind, oder ob diese nur ein zeitlich sortierter Papierordner sind. Egal ob diese in Ihren Firmenräumlichkeiten gelagert / gespeichert werden oder ob diese irgendwo in einer externen Cloud abgelegt sind. Sie sind für diese Daten und auch für die Verfügbarkeit der Daten verantwortlich.<br>
Bitte verwenden Sie gute Benutzernamen und qualitativ hochwertige Kennwörter.<br>
Denken Sie daran, dass andere Menschen ev. böswilligen Nutzen aus Ihren Daten ziehen wollen. D.h. jegliche Kommunikation bei der jemand mitlesen könnte, wie z.B. EMail und die kritische Daten enthält / enthalten könnte, muss verschlüsselt sein. Sie haften dafür, wenn Ihre Daten von jemanden unerlaubter Weise mitgelesen werden. Es gibt hier mehr Möglichkeiten als uns allen lieb ist.

## Parametrierungen in **Kieselstein ERP**
Um die DSGVO in Ihrem **Kieselstein ERP** umzusetzen sollten Sie den Parameter ANWESENHEITSLISTE_TELEFON_PRIVAT_ANZEIGEN auf 0 stellen.

### Einstellungen der Rechte / Benutzerrollen
Bitte achten Sie darauf, dass möglichst nur eine Person / ein Benutzer das Administratorenrecht Ihres **Kieselstein ERP** besitzt. Schränken Sie vor allem die Rechte bezüglich Finanzbuchhaltung und Personalverwaltung soweit wie möglich ein. Insbesondere das Recht PERS_DARF_PERSONALDETAILDATEN_SEHEN bewirkt, dass auch Geburtsdaten, Stundenabrechnungen, Gehälter und ähnliches angezeigt werden. Gerade in kleinen Unternehmen herrscht die Unsitte vor, dass "wir sind eh alle gleich" alle Administratoren sind.
**Unser Tipp:** Einigen Sie sich darauf wer für die Personalagenden zuständig ist und nur dieser Benutzer erhält tatsächlich das Recht auf die Personaldetaildaten. Gehen Sie in den Belangen der Finanzbuchhaltung ähnlich vor.<br>
Denken Sie auch daran, dass in den Ausdrucken der Zeiterfassung durchaus DSGVO relevante Daten enthalten sein können. D.h. vergeben Sie nur Rechte für Mitarbeiter die diese Daten auch tatsächlich benötigen und die Sie auf die DSGVO / Geheimhaltung usw. verpflichtet haben. D.h. die Rechte: <br>
PERS_ZEITERFASSUNG_MONATSABRECHNUNG_DRUCKEN, PERS_ZEITERFASSUNG_REPORTS_SEHEN, PERS_ZEITERFASSUNG_REPORTS_DRUCKEN bitte neben den anderen Personalrechten mit Bedacht vergeben.

### Welche Standardanwendungen werden durch **Kieselstein ERP** abgedeckt
Im Sinne der DSGVO sind nachfolgende Standardanwendungen aufgeführt. [Siehe dazu bitte auch](https://www.wko.at/service/wirtschaftsrecht-gewerberecht/Datenschutz_durch_Standard-_und_Musteranwendungen_in_der_W.html).
Von den in der Datenschutz durch Standard- und Musteranwendungen in der Wirtschaft genannten 32 Standardanwendungsfällen sind für die Wirtschaft vor allem folgende von Bedeutung:
#### SA001 "Rechnungswesen und Logistik"
Behandelt die Verarbeitung und Übermittlung von Daten im Rahmen einer Geschäftsbeziehung mit Kunden und Lieferanten einschließlich automationsunterstützt erstellter und archivierter Textdokumente (z.B. Korrespondenzen) in diesen Angelegenheiten und erfasst folgende Personengruppen:
    Kunden, Lieferanten bzw. Empfänger und Erbringer von Lieferungen oder Leistungen
    Sachbearbeiter/Kontaktpersonen beim Auftraggeber
    an der Geschäftsabwicklung mitwirkende Dritte
    Kontaktpersonen beim Kunden/Lieferanten oder an der Geschäftsabwicklung mitwirkenden Dritten
    bloße Zustell-, Lieferungs-, Rechnungsadressaten und dergleichen
    Fremdkapitalgeber
    Gesellschafter

#### SA002 "Personalverwaltung für privatrechtliche Dienstverhältnisse"
Befasst sich mit der Verarbeitung und Übermittlung von Daten für Lohn-, Gehalts-,Entgeltsverrechnung, Aufzeichnungs-, Auskunfts- und Meldepflichten aufgrund von Gesetzen oder Kollektivverträgen oder Arbeitsverträgen einschließlich automationsunterstützt erstellter und archivierter Textdokumente (z.B. Korrespondenzen) in diesen Angelegenheiten, wobei als davon betroffene Personengruppe erfasst werden:
    Arbeitnehmer; arbeitnehmerähnliche Personen; Leiharbeiter; freie Dienstnehmer; Volontäre; Lehrlinge; Ferialpraktikanten und ehemalige Beschäftigte
    (aktuelle und ehemalige) Organe (und deren Mitglieder) und
    sonstige Funktionsträger juristischer Personen und Personengemeinschaften.
    Seit der Novelle BGBl II Nr. 105/2011 fallen auch (bestimmte) personenbezogene Daten von Bewerbern unter die SA002, wenn diese vom Betroffenen angegeben wurden.
    **Tipp:** Erfassen Sie die Religionsbekenntnis des Mitarbeiters nur, wenn er dieses zur Erlangung seines Feiertages (gilt nur für Österreich) tatsächlich angibt.

#### SA003 "Mitgliederverwaltung"

#### SA007 "Verwaltung von Benutzerkennzeichen"
Wird die Systemzugriffskontrolle und Verwaltung von Benutzerkennzeichen für die Auftraggeber -- Datenanwendungen und Verwaltung der Zuteilung von Hard- und Software an die Systembenutzer einschließlich automationsunterstützt erstellter und archivierter Textdokumente (z.B. Korrespondenzen) in diesen Angelegenheiten als Standardfall in der Datenverwendung erklärt. Davon erfasster Personenkreis ist nur die Kategorie der Systembenutzer.

#### SA022 "Kundenbetreuung und Marketing für eigene Zwecke"
Scheint in der Verordnung als Standardanwendungszweck die Verwendung eigener oder zugekaufter Kunden- und Interessentendaten für die Geschäftsanbahnung betreffend das eigene Lieferungs-oder Leistungsangebot einschließlich automationsunterstützt erstellter und archivierter Textdokumente (z.B. Korrespondenzen) in dieser Angelegenheit auf und erfasst dabei folgende Personengruppen:
    Eigene Kunden;
    Interessenten, die an den Auftraggeber selbst herangetreten sind;
    Kontaktpersonen beim Kunden/Interessenten;
    potenzielle Interessenten, deren Adressen von Adressverlagen zugekauft (gemietet) oder selbst ermittelt wurden

#### SA032 "Videoüberwachung"

#### SA033 "Datenübermittlung im Konzern"

#### SA037 "Melde- und Kontrollsysteme zur Bekämpfung von Geldwäscherei und Terrorismusfinanzierung"
Das bedeutet dass die Verwendung von **Kieselstein ERP** in die vier Standardanwendungen (SA001, SA002, SA007, SA022) einzustufen ist.<br>
Details dazu siehe bitte auch Modul DSGVO, Info, Personen spezifische Merkmale.

#### Werden sensible Daten verarbeitet?
Laut DSGVO ist die Verarbeitung von "sensiblen Daten" untersagt.<br>
Dieses Verbot gilt ausschließlich in folgenden Fällen **nicht** (d.h. die Verarbeitung sensibler Daten ist in folgenden Fällen zulässig):
- Vorliegen einer ausdrücklichen Einwilligung
- **Vorliegen einer gesetzlichen Grundlage** einschließlich Kollektivverträge und Betriebsvereinbarungen, zur Ausübung von Rechten aus dem Arbeitsrecht, dem Recht der sozialen Sicherheit und des Sozialschutzes
- **Gesundheitsvorsorge, Arbeitsmedizin** Da könnte auch ein Hinweis zur Gesundenuntersuchung o.ä. enthalten sein. Denken Sie hier immer positiv und Sie werden einen Grund finden, warum das für Ihre Mitarbeiter wichtig ist.

Im Sinne der DSGVO werden derzeit an sensiblen Daten verarbeitet:
- Das Religionsbekenntnis für die Bestimmung des gesetzlichen Feiertages. D.h. wenn der Mitarbeiter sein evangelisches Religionsbekenntnis zum Erhalt der / des evangelischen Feiertages freiwillig bekannt gibt, so kann dies unter Religion hinterlegt werden.
    **Unser Tipp:** Hinterlegen Sie nur wenn der Mitarbeiter darauf besteht sein Religionsbekenntnis und hinterlegen Sie diese Einwilligung in der Dokumentenablage.
- Das Geburtsdatum des Mitarbeiters für die Lohnabrechnung usw. (gesetzlicher Auftrag)
- Seine Krank und Pflege-Tage/Stunden (gesetzlicher Auftrag)
- das Geburtsdatum eines Ansprechpartners / Partners (kein Pflichtfeld)
    Deaktivieren Sie diese Erfassung

#### Kann Teamviewer im Sinne der DSGVO eingesetzt werden?
Auch wenn die Firma Teamviewer natürlich ihr Produkt verteidigt, sehen wir folgende Punkte als problematisch an:<br>
Teamviewer erlaubt jederzeit eine Rückverfolgung zu den handelnden Personen(merkmalen). Die Kette stellt sich wie folgt dar:
- Teamviewer Lizenz mit Namen usw.
- Zuordnung der Lizenz zu den Teamviewer-IDs, welche aus den MAC-Adressen der Rechner errechnet werden
    Alleine schon damit sind entsprechende Profile möglich
- Verwaltung der Kennwörter für einen Fernwartungszugriff ohne vorherige Freigabe

Leider haben wir auf eine Anfrage bei Teamviewer zu deren Ansicht bzgl. DSGVO nur die Info bekommen, dass wir rechtzeitig über deren Status dazu informiert werden.<br>
Bitte beachten Sie auch die Arbeitsschutzrechtliche Situation beim Einsatz dieses Werkzeugs.
Bedenken Sie bitte auch, wie leicht ein Mitarbeiter hiermit alle ihre Geheimhaltungsbestimmungen umgehen kann.

Hinweis: Auch wir setzen Teamviewer ein. Insbesondere sind unsere Zeiterfassungsterminals auf Windows-Basis für die Betreuung mit Teamviewer mit einem konfigurierten Fernwartungszugriff ausgestattet. Von uns werden die Terminals in der Regel so ausgeliefert, dass diese mit Ihrem **Kieselstein ERP** Server kommunizieren und dass der verwendete Benutzer KEINEN Zugriff in Ihr Firmennetzwerk hat.

#### Welche Personenspezifische Merkmale werden von **Kieselstein ERP** gespeichert
Für diese Detailinfos, verweisen wir auf den Ausdruck der Personen spezifischen Merkmale aus dem Modul DSGVO

#### Privacy by Design
Die Grundeinstellungen von **Kieselstein ERP** entsprechen dieser Forderung. Leider verwenden viele, vor allem kleine Unternehmen die Rolle Administrator für alle Benutzer (wir sind alle gleich, jeder darf von den anderen alles wissen). Bitte bedenken Sie, dass es besser ist, wenn jedeR die Rechte bekommt die er/sie für seine/ihre Arbeit benötigt. Das schützt Sie und Ihre Daten und weckt keine zusätzlichen Begehrlichkeiten. Denken Sie auch daran Ihre Arbeitsplatzrechner so einzustellen, dass diese einen automatischen Anmeldeschutz haben.<br>
Von den Datenstrukturen her ist **Kieselstein ERP** so programmiert, dass für die Erfassung von Personen / Ansprechpartnern, der Name als einziges Merkmal ausreicht.

#### Detailübersicht der verarbeiteten personenspezifischen Daten
Sie finden im Modul Personal, unterer Reiter DSGVO, im Menüpunkt Journal, Module und personenspezifische Merkmale, eine Aufstellung der von den jeweiligen Modulen verarbeiteten personenspezifischen Merkmale. Sollten Sie dieses Zusatzmodul nicht besitzen, wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

#### Beauskunftung
Sie finden im Modul Personal, unterer Reiter DSGVO, eine Liste aller Partner welche in Ihrem **Kieselstein ERP** angelegt sind.<br>
Info: Die Partner-Adressen haben keine spezifische Mandantenzuordnung, d.h. Sie finden hier alle in Ihrer Datenbank angelegten Adressen.<br>
Für eine Beauskunftung wählen Sie den gewünschten Partner aus und gehen im Menü auf Info, Beauskunftung.
Hier stehen zwei Auswertungen zur Verfügung:
- Kompakte Info, mit der Liste in welchen Modulen Daten dieses Partners gespeichert sind
- umfassende Auswertung, mit einer Detailaufstellung in welchen Modul-Datensätzen Daten gespeichert sind. Also z.B. bei welchem Angebot der Partner als Ansprechpartner hinterlegt ist.

#### Brauche ich neben der Beauskunftung aus **Kieselstein ERP** auch noch ein Verarbeitungsverzeichnis?
Selbstverständlich. Auch wenn eine großen Anzahl an Daten und Prozessen in **Kieselstein ERP** abgebildet sind. Wir haben bisher keinen Anwender kennengelernt, der nicht in irgend einer Form noch zusätzliche strukturierte Daten aufbewahrt hat. Egal ob dies nun Kundenordner, Personalakten, XLS oder odt Dateien sind. All diese Dateien sind zu beurteilen, im Verarbeitungsverzeichnis aufzulisten und in ihrer Datenrelevanz einzuschätzen.

#### Massendaten / personenspezifische Daten
Diese Unterscheidung ist für das Zutreffen der DSGVO relevant.<br>
Z.B. Wenn Sie nur Ihre Saldenliste an Ihren Unternehmensberater senden, so sind das anonymisierte Massendaten. Im Sinne der DSGVO sind diese nicht schützenswert. Natürlich sollten Sie dies aber trotzdem nur verschlüsselt übertragen, man kann da ja einiges herauslesen.<br>
Andererseits, wenn Sie Ihre Buchhaltung, auch in Papierform, Ihrem Steuerberater übergeben, so sind darin auch die Debitorenkonten enthalten. Das sind strukturierte Daten. Also vom Grundgedanken her personifizierte Daten, weil Sie ja auch an Privatpersonen bzw. Einzelfirmen liefern könnten. Dies geht soweit, wenn Namen auch in GmbH Firmenwortlauten enthalten sind und daraus eindeutig auf eine Person rückgeschlossen werden kann, sind das personifizierte Daten.<br>
**ABER:** Es gibt die gesetzliche Buchführungs- und Aufbewahrungspflicht. Diese ist höherwertig als die DSGVO. D.h. ja Sie müssen das verarbeiten. Jedoch können Sie aus dem Titel der Buchhaltung keine Begründung ableiten, warum Sie das Geburtsdatum benötigen. Einzig wenn Sie Geschäfte mit Minderjährigen machen, könnte dies ein Thema sein. Sollte dies für Sie zutreffen, so setzen Sie sich bitte sehr intensiv mit den Rechten dieser sehr schützenswerten Personengruppe auseinander.<br>
**Zu beachten:** Natürlich ist es selbstverständlich, dass die Lohnverrechnung personenspezifische Daten verarbeitet. Daraus ergibt sich auch, dass jegliche Kommunikation mit Ihrem Auftragsverarbeiter (Steuerberater der die Lohnverrechnung macht), über geschützte Kanäle erfolgt. Also Personendaten nur per verschlüsseltem EMail usw.
**Gilt das auch für Bilanzen?** Im ersten Blick würde man sagen, nein, das sind ja Massendaten, aber: da werden natürlich auch die Organe der Geschäftsführung angeführt. Also verschlüsselt sowieso und bei der Beauskunftung berücksichtigen.

#### Darf ich meinen Partnern / Kunden EMails senden?
Wenn Sie in laufenden Geschäftsbeziehungen sind, so ist die normale EMail Kommunikation Teil Ihres Geschäftes / Business-Use-cases. Das bedeutet, solange Sie hier "normale" Daten wie Angebote, technische Details oder ähnliches austauschen, ist das legal. Kritisch wird es, wenn es um persönliche Daten wie Geburtsdatum, das letzte Strafmandat, ist schon seit xx Wochen Krank, geht. Diese sensiblen Daten sollten Sie weder mit anderen austauschen und schon gar nicht speichern.<br>
So ist auch die Information über das Leistungsangebot Ihres Unternehmens unter vorvertragliche Phase einzustufen (Geschäftsanbahnung) das dürfen Sie jederzeit machen, wenn der Partner Sie um Information gebeten hat. Dass er Sie um Information gebeten hat, sollten Sie nachweisen können.

#### Wie geht man mit Daten die von den Mitarbeitern erzeugt / abgelegt werden um?
In Zusammenhang mit den Daten, die von Mitarbeitern in Ihrem Unternehmen an- und abgelegt bzw. erzeugt werden gibt es einige Dinge zu beachten.<br>
Nachfolgend eine, sicherlich nicht vollständige Sammlung, unserer Empfehlungen / Erfahrungen.

Bitte beachten Sie, dass hier sowohl Datenschutzrechtliche als auch Arbeitsrechtliche Gesetze und Regeln zur Anwendung kommen. Wir verweisen auch hier darauf, dass wir weder Steuerberater, noch Jurist, noch ein Unternehmensberater sind. Rechtsverbindliche Auskünfte holen Sie sich bitte bei den entsprechenden Stellen.

**Oberste Regel:**
Alle Daten die Ihre Mitarbeiter auf Geräten in Ihrer Einflusssphäre speichern, sind Daten des Unternehmens. Das Speichern privater Daten ist ausdrücklich verboten.<br>
Warum? Private Daten dürfen Sie nicht einsehen. Nun hat der Mitarbeiter in seinem EMail Postfach aber Kunden und Private Daten gemischt. Sie haben keine Chance eine Beauskunftung nach DSGVO zu machen, weil Sie die Daten nicht einsehen dürfen.

**Tipp:** Stellen Sie diese Punkte, neben den vielen anderen, z.B. Geheimhaltung, in einem Dokument zusammen und lassen Sie sich die Einhaltung dieser Punkte / Regeln jedes Jahr von Ihren Mitarbeitern unterschreiben.<br>
Will ein Mitarbeiter Ihre Regeln nicht akzeptieren, so überlegen Sie sich, wer die arbeitsrechtlichen Strafen bezahlt.<br>
**Info:** Es haftet am Schluss immer der Geschäftsführer mit seinem Privatvermögen.

#### Wann sind Daten zu löschen ?
Das kommt darauf an. D.h. wenn:
-   ein anderes Recht dies verlangt, z.B. Buchhaltungs Aufbewahrungsfristen, das gilt auch für die Dokumente der vorvertraglichen Phase
-   Wenn zu einem Vorgang Kosten entstanden sind.
    So verursacht z.B. eine Beauskunftung Kosten. Egal ob diese weiterverrechnet wurden oder nicht. Aufgrund dieser Tatsache können Sie der Ansicht sein, dass auch hier die Buchhalterischen Aufbewahrungsfristen gelten.

so dürfen diese Daten nicht gelöscht werden. D.h. prüfen Sie vor dem Löschen / Anonymisieren immer ob hier andere, höherwertige Rechte diesen Vorgang verbieten. Erst nachdem Sie sicher sind, dass es keinerlei Recht gibt, welches Ihnen die Aufbewahrung der Daten vorschreibt, müssen Sie löschen / anonymisieren.<br>
Aus diesem Grunde ist das anonymisieren in **Kieselstein ERP** auch im DSGVO Modul, für welches entsprechende Rechte erforderlich sind, angesiedelt.

#### Können in **Kieselstein ERP** Daten gelöscht werden ?
Theoretisch ja, aber wenn diese mit anderen Dokumenten verknüpft sind, ist ein Löschen, also eine physikalische Entfernung aus der Datenbank nicht mehr möglich. Verwenden Sie stattdessen anonymisieren.<br>
Hinweis: Im Falle dass eine Datensicherung eingespielt werden muss, müssen bereits gelöschte / anonymisierte Daten nachgelöscht/nachanonymisiert werden.<br>
Wir raten daher, die Daten nur zu anonymisieren, da beim Löschen die Daten nicht nachgelöscht werden können.<br>
Denken Sie daran, dass die Protokolldatei der Anonymisierung zusätzlich auf einem völlig anderen Backupmedium gespeichert werden muss. Hier ist eine zweite Datensicherungsstrategie anzuwenden.

#### Was passiert mit den Daten des Testsystemes ?
Bitte beachten Sie, dass von der DSGVO natürlich auch die Daten in Ihren Testsystemen betroffen sind / betroffen sein können. D.h. löschen / anonymisieren / beauskunften Sie bitte auch diesen Datenbestand.

#### Was ist den so grundsätzlich zu beachten ?
Es geht darum dass personenbezogene Daten nicht falsch verwendet werden / nicht in falsche Hände gelangen. Denken Sie an den gerade (April 2018) aufgeflogenen Facebook Daten Skandal.<br>
D.h. im wesentlichen sollten Sie immer Herr/Frau Ihrer Daten sein.<br>
Nun ist das Verständnis vieler Menschen welche Daten denn wo durch die Gegend gesandt werden oft nur sehr rudimentär oder überhaupt nicht vorhanden.<br>
Auch wird die kriminelle Energie mit der manche Zeitgenossen versuchen Daten zu stehlen von uns allen unterschätzt.<br>
D.h. Sie müssen Maßnahmen ergreifen dass die Daten grundsätzlich nicht wegkommen, bzw. dass Sie merken wenn Daten weggekommen sind.<br>
Dazu werden folgende Maßnahmen eingesetzt:
- Alles was an Daten Mobil unterwegs ist sollte durch ein Kennwort geschützt sein. Also alles was Sie auf Notebooks, Handys, Tablets, USB-Sticks, Datensicherungen, usw. gespeichert haben.
- Alles was elektronisch über ungeschützte Kanäle unterwegs ist muss geschützt sein. Bedenken Sie bitte dass ein EMail wie eine Postkarte ist. JedeR kann mitlesen.
- Es sollte niemand unerlaubt Zutritt zu Ihren Standgeräten bekommen. D.h. die Serverräume sind verschlossen und die Client/Arbeitsplatzrechner gehen nach sehr kurzer Zeit in einen Modus über, sodass man nur mit Passwörtern weiter kommt.

Oder in anderen Worten:
In der DSGVO sind unter anderem sechs Grundsätze für die Verarbeitung personenbezogener Daten geregelt:
-   Rechtmäßigkeit, Verarbeitung nach Treu und Glauben, Transparenz
-   Zweckbindung (Verarbeitung nur für festgelegte, eindeutige und legitime Zwecke)
-   Datenminimierung ("dem Zweck angemessen und erheblich sowie auf das [...] notwendige Maß beschränkt")
-   Richtigkeit ("es sind alle angemessenen Maßnahmen zu treffen, damit *unrichtige* personenbezogene Daten unverzüglich gelöscht oder berichtigt werden")
-   Speicherbegrenzung (Daten müssen "in einer Form gespeichert werden, die die Identifizierung der betroffenen Personen nur so lange ermöglicht, wie es [...] erforderlich ist")
-   Integrität und Vertraulichkeit ("angemessene Sicherheit der personenbezogenen Daten [...], einschließlich Schutz vor unbefugter oder unrechtmäßiger Verarbeitung und vor unbeabsichtigtem Verlust, unbeabsichtigter Zerstörung oder unbeabsichtigter Schädigung")

Laut der DSGVO darf nur gespeichert werden, was für die aktuelle Datenverarbeitungstätigkeit tatsächlich notwendig ist. Zusätzlich muss ein Verarbeitungsverzeichnis geführt werden, es sei denn:

- die Datenverarbeitung birgt kein Risiko für die Rechte und Freiheiten der betroffenen Personen
- die Verarbeitung erfolgt nur gelegentlich
- es werden keine sensiblen Daten verarbeitet

Bitte beachten Sie, dass Ihre Webseiten bzw. elektronische Kommunikation / EMail, Newsletter besonders im Augenmerk liegen. [Siehe dazu auch](https://www.webpunks.co/dsgvo-und-webseiten/#about).

#### Verarbeiten wir Ihre Daten im Sinne eines Auftragsverarbeiters, wie im Artikel 28 ausgeführt ?
Nein. Es wird von Seiten **Kieselstein ERP** im Sinne des Artikels 28 keine Auftragsdatenverarbeitung vorgenommen. Wir benötigen Ihre Daten im Anlassfall zwar um verschiedene Problemlösungen zu erarbeiten, z.B. zur Fehlerbehebung oder zur Entwicklung neuer Module bzw. Funktionalitäten. Die eigentliche Datenverarbeitung erfolgt aber immer auf den von Ihnen verwendeten Servern. D.h. liegt Ihre **Kieselstein ERP** Installation auf einem Server dessen Hardware bei Ihnen im Hause steht, so erfolgt die Verarbeitung auf diesem Rechner und Sie müssen bei einer Beauskunftung / Löschung diesen Rechner und seine Daten mit berücksichtigen (siehe dazu gerne auch das zusätzliche DSGVO Modul, welches Sie bei der Beauskunftung dramatisch unterstützt). Liegt Ihre **Kieselstein ERP** Installation in einer Cloud oder ähnlichen Struktur, so muss sichergestellt sein, dass die Behandlung Ihres Servers ebenfalls der DSGVO entspricht. In diesem Falle müssen Sie mit Ihrem Provider entsprechende Verträge abschließen.<br>
Selbstverständlich unterliegen wir schon immer der Geheimhaltung und unser gesamtes Team ist angewiesen nur die Daten welche zur Erfüllung der Aufgaben erforderlich sind zu verwenden. Sie können beruhigt davon ausgehen, dass wir auch unsere Hausaufgaben bezüglich der technischen und organisatorischen Maßnahmen zur Sicherheit der uns anvertrauten Daten unternommen haben. Wir bitten um Verständnis, dass diese nicht offengelegt werden. Sollten Sie dazu ausführlichen Bedarf haben, können wir Ihnen diese gerne, nach Unterzeichnung einer entsprechenden Vertraulichkeitsvereinbarung, zusenden.