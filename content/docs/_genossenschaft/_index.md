---
title: "Kieselstein ERP eG"
linkTitle: "Genossenschaft"
weight: 0100
description: >
  Wer bzw. was ist die Kieselstein ERP Genossenschaft?
---
Die Kieselstein ERP e.G. kümmert sich um die konsolidierte Weiterentwicklung des Kieselstein ERP Systems.
Dazu haben sich eine Anzahl von Firmen als Mitglieder aus den verschiedensten, vor allem produzierenden, Branchen zusammengeschlossen.

Weitere Mitglieder sind herzlich willkommen. Siehe dazu Mitgliedsantrag, Statuten, Mitgliedsbeitrag.

## Einige Stimmen von den Mitgliedern, die teilweise Jahrzehnte lange Anwender sind
- H: ein gutes umfassendes und doch übersichtliches Programm
- 