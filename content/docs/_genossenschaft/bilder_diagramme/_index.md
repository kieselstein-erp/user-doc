---
title: "Bilder zum Kieselstein ERP"
linkTitle: "Bilder"
weight: 2
description: >
  Bilder und Diagramme rund um das Kieselstein ERP System
---
Das **Kieselstein ERP** System ist ein Fork des jahrelang bewährten, Betriebssystem unabhängigen ERP-Systems HELIUM V, mit Stand September 2022.

Es umfasst folgende Module / Bereiche:

## Warenwirtschaft
- Artikelstamm
- hierarchische Stücklisten
## Einkauf
- Anfrage
- Bestellung
- Eingangsrechnung
- Lieferanten
## Produktion
- Fertigungsaufträge
- Zeitbewirtschaftung
- Personalstamm
- Reklamationsverwaltung
- Küchenmodul
## Verkauf
- Projektverwaltung
- Angebotswesen
- Angebotsstücklisten
- Aufträge, inkl. Rahmen- und Abrufaufträgen und wiederholenden Aufträgen
- Forecast
- Lieferschein
- Rechnung
- Kundenstamm
## Management
- Finanzbuchhaltung in zwei Ausführungen
  - nur Export in Standard Buchhaltungsprogramme (Datev, BMD, RZL, ....), sowie Import der OP-Listen
  - vollständig integrierte einfache Buchhaltung bis hin zur G+V/BWA bzw. Bilanz
  - SEPA Import = Import der Kontoauszüge nach ISO, damit fast volautomatische praktische Zahlungsbuchung
- Dashboard mit zahlreichen praktischen Auswertungen, alle anpassbar
- 360° Kundencockpit
## System
- Partner und Banken
- Benutzerverwaltung mit Rollen und Rechten
- allgemeine Systemeinstellungen
<!-- 
## test
![](Uebersicht_Kreis.png)  

Als Source siehe ..\user-doc\content\docs\_genossenschaft\bilder_diagramme\Uebersicht.odg  -->

## dazu einige Bilder
| Modul | Icon |
| --- | --- |
| Mandanten | hat_gray.svg |
| --- | --- |
| Artikel | nut_and_bolt.svg |
| Stückliste | text_code_colored.svg |
| --- | --- |
| Lieferanten | address_book2.svg |
| Anfrage | note_find.svg |
| Bestellung | shoppingcart_full.svg |
| Eingangsrechnung |hand_money.svg |
| --- | --- |
| Fertigung | factory.svg |
| Küche | office-building.svg |
| Reklamation | exchange.svg |
| Instandhaltung | houses.svg |
| Zeiterfassung | clock.svg |
| Personal |worker.svg |
| --- | --- |
| Kunde | handshake.svg |
| Projekte | briefcase2_document.svg |
| Angebot | presentation.svg |
| Angebotsstückliste | note_add.svg |
| Inserate | news.svg |
| Forecast | document_chart.svg | 
| Auftrag | auftrag.svg |
| Lieferschein | truck_red.svg |
| Rechnung | calculator.svg |
| --- | --- |
| Zutritt | id_card.svg |
| Finanzbuchhaltung | books.svg |
| 360° Kundensicht | control_tower.svg |
| Dashboard |gauge.svg |
| --- | --- |
| Partner | businessmen.svg |
| Benutzer | user1_monitor.svg |
| System | toolbox.svg |
| Nachrichten | messages.svg |
| Geodaten |dot-chart.svg |

