---
title: "Kurzfassung des Kieselstein ERP"
linkTitle: "Kurzfassung ERP"
weight: 1
description: >
  Kürzest-Beschreibung des Kieselstein-ERP Systems.
---
Das Kieselsein ERP System ist ein Fork des jahrelang bewährten, Betriebssystem unabhängigen ERP-Systems HELIUM V, mit Stand September 2022.

Es umfasst folgende Module / Bereiche:

## Warenwirtschaft
- Artikelstamm<br>
  Das zentrale Herzstück Ihres ERP Systems!<br>
  Verkauf, Einkauf, Lager, geleitender Lager Durchschnittspreis (Gestehungspreis), Beschreibungen, Bilder, Dokumentation<br>
  Freigabe
  Serien- und Chargennummern, Gebindeverwaltung<br>
  Statistiken, zahlreiche Auswertungen
- hierarchische Stücklisten
  - beliebig viele Ebenen (Tiefen) zur Abbildung Ihres Produktionsprozesses bis hin zu Fremdfertigung (verlängerte Werkbank)
  - Soll- / Vor-Kalkulation inkl. Stücklistenauflösung
  - Phantom Stücklisten (Hilfs-Stücklisten)
  - Set-Artikel als Einkaufs- bzw. Verkaufs-Set
  - Formelstücklisten als automatischer Fertigungsstücklisten Konfigurator, frei konfigurierbar
## Einkauf
- Anfrage
  - Gruppenanfrage mit Gruppen Auswertungen
  - Anfragevorschlag, Überleitung in Gruppenanfragen
- Bestellung
  - Rahmen und Abrufbestellung
  - Bestellvorschlag mit Lieferoptimierung
  - Wareneingänge mit Überleitung zur Eingangsrechnung
- Eingangsrechnung
  - Splittbuchung
  - Kontierungsberechtigung
  - Zahlungsvorschlag
  - Auftragszuordnung
- Lieferanten
## Produktion
- Fertigungsaufträge
  - interner Bestellvorschlag = errechnen der tatsächlich zu fertigenden Baugruppen / Unterstücklisten
  - Geräte-Seriennummern
  - Stückrückmeldung
  - Mitlaufende / Nach-Kalkulation
  - Statistik der Kosten- und Zeitentwicklung
- Zeitbewirtschaftung
  - Anwesenheitszeiten
  - Zeiten auf Projekt, Angebot, Aufträgen, Fertigungsaufträgen, Telefonzeiten, Reisezeiten, ..
  - Monatsabrechnungen mit Überstunden-Auswertungen, automatischer Versand
  - Überleitung zur Lohnverrechnung
  - Urlaubs- und Gleitzeit-Verwaltung
  - Maschinenverwaltung mit Maschinenzeitmodellen, Mehrmaschinen
  - Produktivitätsstatistik Mensch bzw. Maschine
- Personalstamm
  - Rechtestruktur zur Abbildung der DSGVO
  - beliebig viele Zeitmodelle inkl. Vorgaben nach Arbeitsrecht
  - Kollektiv / Tarif-Verträge
  - Schichtzuschläge, Personengruppen, privat & Dienstfahrzeuge
- Reklamationsverwaltung
- Küchenmodul
## Verkauf
- Projektverwaltung
- Angebotswesen
- Angebotsstücklisten
  - Angebotsstücklisten Schnellerfassung
  - Einkaufsangebotsstücklisten inkl. XLS Lieferantenanfragen
- Aufträge, inkl. Rahmen- und Abrufaufträgen und wiederholenden Aufträgen
  - erweiterte Auftrags-Projektsteuerung inkl. Liquiditätsplan
  - Auslieferliste
  - umfassende detaillierte mitlaufende bzw. Nach-Kalkulation
- Forecast
  - verschiedene Importformate
  - Prognose des Materialbedarfes anhand der Forecast-Positions-Termine und der Stücklisten inkl. hierarchischer Auflösung
- Lieferschein
  - Sammellieferschein
  - verketteter Lieferschein
  - Lieferantenlieferschein
  - Vorschlag der auszuliefernden Positionen (anhand Aufträge)
- Rechnung
  - Sammelrechnung
  - Anzahlungs- und Schlussrechnung
  - Proformarechnung
  - Gutschrift / Rechnungskorrektur
  - Zahlungsverwaltung
  - Mahnwesen
  - Provisionsabrechnung
- Kundenstamm
  - Konditionen
  - Sonderkonditionen nach Artikel
  - Zahlreiche Auswertungen und Statistiken
  - Deckungsbeitrag je Kunde
  - beliebig viele Ansprechpartner je Kunden
  - Rechnungs- und Lieferadressen
## Management
- Finanzbuchhaltung in zwei Ausführungen
  - nur Export in Standard Buchhaltungsprogramme (Datev, BMD, RZL, ....), sowie Import der OP-Listen
  - vollständig integrierte einfache Buchhaltung bis hin zur G+V/BWA bzw. Bilanz
    - automatische Verbuchung der ER, AR, GS<br>nach umfangreichem anpassbarem Regelwerk
    - SuSa, UVA, Erfolg, Periodenübernahmen, Ausziffern, manuelle Buchung, ...
  - SEPA Import = Import der Kontoauszüge nach ISO, damit fast volautomatische praktische Zahlungsbuchung
- Dashboard mit zahlreichen praktischen Auswertungen, alle anpassbar
- 360° Kundencockpit
## System
- Partner und Banken
- Benutzerverwaltung mit Rollen und Rechten
- allgemeine Systemeinstellungen
## allgemeines
- freie Benutzeranzahl (keine Begrenzung)
- alle angeführten Module frei aktivierbar
- **keine** Lizenzkosten, bitte rechnen Sie mit Installations- bzw. Betreuungskosten
- beliebig viele Mandanten mit oder ohne zentralem Artikelstamm
- ca. 700 Standard Formulare / Auswertungen
- deutsch und englisch im Standard enthalten, weitere Sprachen durch zur Verfügungstellung der Übersetzungen kurzfristig integrierbar
- bewährtes Drei-Schichten-Modell
- Client und Applikationsserver laufen nativ unter Windows(R), Linux, iOS (MAC)
- Quellcode (Source) unter AGPL lizenziert und unter https://gitlab.com/kieselstein-erp verfügbar
- Wird durch die Kieselstein ERP eG gepflegt und weiterenwickelt

![](menueleiste.png)