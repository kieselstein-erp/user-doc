---
title: "Rechtevergabe"
linkTitle: "Rechte"
weight: 0600
description: >
  Lose Sammlung Rund um das Thema Rechtevergabe
---
Da immer wieder Fragen rund um das Thema Rechtevergabe für die verschiedensten Rollen auftreten, hier eine unvollständige Sammlung der Fragen zu Rechten und deren Wirkung.

Wir haben diese anhand der Module zusammengefasst. Das bedeutet auch, dass ein Recht natürlich in mehreren Modulen seine Wirkung zeigen kann.

## Artikel
### anlegen neuer Artikellieferanten
Um einen neuen Artikellieferanten anlegen zu können, muss der User auch das Recht LP_DARF_PREISE_AENDERN_EINKAUF haben.
Gut ist, wenn er auch das Recht LP_DARF_PREISE_SEHEN_EINKAUF hat, dann kann er auch die Staffelmengen bearbeiten.
