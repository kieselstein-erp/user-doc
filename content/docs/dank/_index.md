---
title: "Dank"
linkTitle: "Dank"
categories: ["Dank"]
tags: ["Dank"]
weight: 9000
description: >
  Dank
---
Das **Kieselstein ERP** würde es ohne die Unterstützung der verschiedensten Menschen und Organisationen nicht geben.

## Thomas Ludwig Uhl
30.12.1969 - 21.8.2015<br>
Tom war von Mitte 2010 bis zu seinem Freitod 2015 Mitgesellschafter der Helium V ERP Systeme GmbH.
- Er war der Autor des ersten deutschsprachigen Linux Buches
- Er war **<u>der</u>** Open Source Pabst für den deutschsprachigen Raum und darüber hinaus.
- Er war Gründungsmitglied der Lisog die heute noch in der OSBA Open Source Business Alliance <https://osb-alliance.de/> sehr aktiv ist
- Er war uns immer um (Licht-)Jahre in seinem sehr schnellen Denken voraus.
- Er hat uns damals dazu gebracht HELIUM V unter der AGPL als Open Source Produkt zu lizenzieren. Nur darum gibt es heute, 12 Jahre später, das **Kieselstein ERP**.

## Microtech GmbH
Dank auch an die Mikrotech GmbH unter dem Geschäftsführer Benjamin Bruno, die der Open Source Gemeinschaft den Quellcode des HELIUM V Kernels in der Version vom September 2022 mit Anfang Oktober 2022 kostenlos zur Verfügung gestellt haben. Es sind das doch einige zig-tausend Menschstunden, die in dem Projekt enthalten sind.

## Technik
Danke den Menschen der Technik / Softwareentwicklung, die in sechsmonatiger Arbeit, den Quellcode so reorganisiert haben, dass ein vollautomatisches Build unter Gitlab für alle verfügbar ist.

## Mitglieder
Ein großer Dank gilt auch den Mitgliedern der Kieselstein ERP eG, die durch ihr Engagement und ihre Mitgliedsbeiträge den Start des Projektes Kieselstein-ERP unter der AGPL überhaupt ermöglichen.

## Anwender, Benutzer
Ein Dank gilt auch allen Menschen, mit denen wir bisher zusammenarbeiten durften und dürfen, die uns mit Ihren Überlegungen und Aufgaben immer wieder zu neuen besseren Lösungen anspornen. Ohne diesen Wünschen, wäre der enorme Funktionsumfang nicht entstanden. Vielen Dank für das große Vertrauen.

## Open Source Community
Das **Kieselstein ERP** würde es nicht geben, wenn nicht eine Vielzahl von Open Source Produkten, auf denen das **Kieselstein ERP** aufbaut, kostenlos zur Verfügung stehen würden.

Ohne Anspruch auf Vollständigkeit sind dies:
- OpenJDK, Java11 FX von Azul.com
- Wildfly
- postgresQL inkl. PGadmin bzw. DBeaver
- Jasper Report Engine
- Liquibase
- intellij bzw. Eclipse
- die freien Linux Betriebssysteme, insbesondere Debian und Ubuntu