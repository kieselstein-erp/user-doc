---
title: "Glosar"
linkTitle: "Glosar"
categories: ["Glosar"]
tags: ["Glosar"]
weight: 10000
description: >
  Glosar, eine Sammlung von gebräuchlichen Begriffen und Schlagwörter im ERP Umfeld
---
Da im ERP Umfeld die unterschiedlichsten Begriffe für oft das Gleiche verwendet werden, hier eine sicherlich unvollständige Sammlung mit ihren Entsprechungen im **Kieselstein ERP**.

Wir weisen ausdrücklich darauf hin, dass dies nur eine kleine Hilfe für die Einordnung darstellen sollte. Jedes ERP System hat seine Spezialitäten, welche bei derartigen Aufstellungen per se nicht berücksichtigt werden können. So sind auch die Anwendungsfälle für kleinere Unternehmen, bis zu einigen 100 MitarbeiterInnen andere als für Multinationale Konzerne mit vielen Standorten usw.. Es unterscheidet sich sehr oft auch die Denkweise in den Unternehmer geführten Firmen.

Natürlich unterscheiden sich die Betriebskosten von proprietären Systemen, zu denen die den Open Source Gedanken von vorneherein konzipiert haben.

## Module
| Modul | KES | SAP(R)|
| --- | --- | --- |
| Personal | PERS | HR |
| Personalabrechnung | ZT | PY |
| Personaladministration | BN | PA |
| Personalzeitwirtschaft | ZT | PT |
| Qualitätsmanagement | | QM |
| Instandhaltung | | PM |
| Produktionsplanung | Los | PP |
| Materialwirtschaft | WW | MM |
| Vertrieb | AG-RE | SD |
| Finanzwesen | Fibu | FI |
| Controlling | siehe | CO |


## Weitere Begriffe
| Bezeichnung in KES | Analogie in SAP(R)|
| --- | --- |
| Hilfsstückliste | Phantomstückliste |

[Siehe auch]( {{<relref "/docs/einleitung/allgemeine_fragen/kurzbegriffe" >}} )<br>
[Teilweise entnommen aus ](https://www.tu-chemnitz.de/wirtschaft/sapr3/hr_wbt/content/SAP_HR_LO01_1.html)<br>
[Und auch aus](https://iob-solutions.ch/uebersicht-sap-s-4-hana-modulen/)<br>
