---
title: "Architektur"
linkTitle: "Architektur"
weight: 8100
description: >
  Kieselstein ERP ist in Multi Tier Technik aufgebaut
---
Das **Kieselstein ERP** ist in der Multi Tier Architektur, auch Mehrschicht Technik genannt, aufgebaut.

![](Architektur.png) 

Die eigentlichen ERP Daten sind in einer eigenen Datenbank, gemäß dem Gedanken, klein, schlank und schnell.

Für die Dokumentenablage gibt es zwei Realisierungen.
- Die reine Dokumentendatenbank.<br>
  - Das hat den Vorteil, dass man sich nicht weiter um das Filesystem kümmern muss und auch z.B. der Administrator nicht aus Versehen Teile der Dokumentendatenbank löscht und sie somit zerstört.
  - Der Nachteil ist, dass diese Datenbank sehr groß werden kann und dass damit die Sicherung so lange dauern kann, dass die Nacht zu kurz wird.
- Die fileorientierte Dokumentendatenbank.<br>
In dieser Konstellation wird nur die Verwaltung der abgelegten Dateien (BLOBs) in der Dokumentendatenbank vorgenommen. Die eigentlichen Daten sind in einer Filestruktur abgelegt, die, so ohne weiteres, für niemanden lesbar ist.
  - Dies hat den Vorteil, dass die Sicherung der sehr großen Dokumentendatenbank über z.B. rsync erfolgen kann und somit nur die veränderten, in der Regel nur die neuen Dateien, in das Sicherungsmedium übertragen werden müssen. Damit ist diese Sicherung in kurzer Zeit durchgeführt, auch bei einer entsprechend großen Datenmenge.
  - Der Nachteil ist, dass theoretisch ein unbedachter Administrator, jemand anderer sollte darauf sowieso keinen Zugriff haben, Teile davon herauslöschen könnte, womit die gesamte Struktur zerstört wäre. Da dieser Fall bisher noch nie eingetreten ist, können wir auch nicht sagen, ob ein Rücksichern der damit fehlenden Teile, dieses Problem beheben würde.
