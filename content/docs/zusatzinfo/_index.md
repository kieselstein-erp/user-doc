---
title: "Zusatzinfo"
linkTitle: "Zusatzinfo"
categories: ["Zusatzinfo"]
tags: ["Zusatzinfo"]
weight: 8000
description: >
  Sammlung von weiteren Infos, die noch eingearbeitet werden müssen
---
Interessante Parameter:
Es gibt eigene Parameter für Lagermindeststandswarnung je Verkaufs-Modul

Standardbelegung für versteckte, nur offene usw.

Urlaubstage Rundung

Ziffernblock

- Belegnummern Format historisch
- Selbstkosten
- Unterstücklisten automatisch ausgeben
- Termineingabe ohne Durchlaufzeit

Anwenderparameter
- Anzeigedauer Druckbestätigung
- Auftrags Seriennummern Etikettendruck Symbol anzeigen
- Auftragsablieferung im Los
- Datei automatische Stücklistenerzeugung
- Lagerplatz direkt erfassen
- Listen nur nach CRUD aktualisieren
- Push Benachrichtigung