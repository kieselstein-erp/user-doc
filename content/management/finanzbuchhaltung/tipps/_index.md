---
categories: ["Finanzbuchhaltung"]
tags: ["Tipps"]
title: "Tipps"
linkTitle: "Tipps"
weight: 800
date: 2023-01-31
description: >
 Tipp(s) rund um das Thema Finanzbuchhaltung
---
## Jahres UVA, Gefahr dass Daten auseinander laufen

Hintergrund:
Einerseits bin ich bei einigen wenigen Anwendern mit der sogenannten Jahres-UVA beschäftigt. D.h. es werden alle abgegebenen UVAs (Umsatzsteuer Voranmeldung) mit den nun im Kieselstein ERP erfassten Daten verglichen.<br>
Hierzu ist wiederum der Hintergrund, dass man, manchmal, aus den verschiedensten Gründen, Buchungen in Zeiträumen macht, für die eigentlich die UVA schon abgegeben ist. Mit der Abgabe der Dezember UVA, welche in DE, mit Dauerfristverlängerung bis zum 10.2. erfolgen muss (in AT bis 15.2. in CH/LI bis Ende März des Folgejahres), ist die letzte Chance das ohne Bilanzkorrektur, die dann immer unangenehm ist, zu machen.<br>
Zusatzinfo: Damit das nicht passiert, haben wir beim Druck (dem wirklichen Druck) eingebaut, dass der UVA Zeitraum verriegelt wird. Dies kann man aber mit den nötigen Rechten auch als Anwender zurücknehmen und damit in alten Perioden buchen, denn als richtiger Buchhalter (und nicht Buchhaltungshilfskraft) weiß man was man macht. Wenn das nun nicht gemacht wird, ist keine Sicherheit gegeben.<br>
Unsere Empfehlung ist, auch wenn man NUR das Buchungsjournal an den STB übergibt, trotzdem die UVA des Monates echt auszudrucken, da damit die Verriegelung eingetragen wird und man nicht versehentlich im alten Monat bucht.

Wurde diese Aufgabe, die Meldung der monatlichen UVA, an den STB (= Steuerberater) delegiert und wird ihm dafür "nur" das Buchungsjournal übersendet so hängt es von der Einstellung des Exports ab, ob es hier zu Differenzen kommen kann.<br>
Das bedeutet: Wenn der Export des Buchungsjournals nach dem **Buchungsdatum** erfolgt so bekommt der Steuerberater eventuelle Änderungen in alten Perioden gar nicht mit.
Wird das nach **Gebucht am** gemacht, sind auch ev. spätere Änderungen mit enthalten. ![](Export_Buchungsjournal1.jpg)<br>
Wenn der Export nach Buchungsdatum erfolgt, raten wir dringend, die Jahressaldenlisten zu vergleichen (zwischen der SuSa des STB und eurer). Nur damit findet ihr eventuelle Unstimmigkeiten. Nichts blöder als, wenn beide Seiten glauben alles richtig gemacht zu haben, aber bei der nächsten Steuerprüfung festzustellen, dass hier eine (gedankliche) Lücke ist / war.

## Änderung der Belegnummern
Werden während des laufenden Geschäftsjahres Belegnummernstukturen verändert, z.B. die Länge der Belegnummer, so greift die automatische Auszifferung der Debitoren-/Kreditorenkonten nicht mehr. D.h. es müssen entweder auch die Belegnummern in der Fibu nachgezogen werden, oder die Belege manuell ausgeziffert werden.

## Es werden in der Erfolgsrechnung keine Prozente angezeigt
Gerade wenn Überschriften für die Erfolgsrechnung definiert wurden, ist wichtig das 100% auf die Summe dieses Blocks zu setzen.

## Materialvorausrechnung
Ist KEINE Anzahlungsrechnung.
Sondern der Einkauf von Material, dass sich der Lieferant auf Lager legt.
Dafür reduziert er dann die Rechnungen für die echten Lieferungen.
Wichtig ist als Unterscheidungsmerkmal, dass das Material bereits euch, also dem **Kieselstein ERP** Anwender gehört.
Üblicherweise bucht man dann die große Materialeinkaufsrechnung auf ein eigenes Material-Voraus-Konto.
Die "eigentlichen" Warenrechnungen müssen dann ja so transparent gestaltet sein, dass die eigentliche Lieferung und der Anteil um Vorausmaterial getrennt ausgewiesen werden.
Der Vorausmaterialanteil ist abzuziehen. Mit diesen Werten ist die Eingangsrechnung anzulegen.
D.h. die eigentliche Warenrechnung inkl. MwSt auf das Materialeinsatzkonto.
Die Reduktion des Materialvorauskontos als Eingangsrechnungsgutschrift mit dem entsprechenden MwSt Wert auf das Material-Voraus-Konto. Damit wird
der offene Wert auf dem Konto reduziert, die Vosteuer entsprechend transparent dargestellt.

## Reverse Charge, innergemeinschaftlicher Einkauf/Erwerb
Nachdem diese Frage immer wieder mal auftaucht, hier eine knappe Zusammenfassung unseres aktuellen (April 2024) Wissensstandes.

Die Frage ist: Ich beziehe als österreichisches Unternehmen von einem deutschen Unternehmen Dienstleistung. Der deutsche Unternehmer liefert das als Reverse Charge Leistung.<br>
Wie ist dies in Österreich zu verbuchen?

Laut [WKO](https://www.wko.at/steuern/umsatzsteuer-auslandsgeschaeften#heading_Basisinfos__Warenlieferungen_und_Dienstleistungen_von_auslaendischen_Unternehmen) wird dies als Innergemeinschaftlicher Erwerb betrachtet. D.h. für das empfangende Unternehmen macht es, für die UVA, keinen Unterschied, ob dies als Lieferung oder als Leistung zu betrachten ist. D.h. es ist in der Kontendefinition die UVA Art auf "IG Erwerb Umsatz Normalsteuer" mit der dazupassenden Variante (derzeit 20%) zu stellen bzw. eben reduzierter Steuersatz.<br>
Man könnte auch sagen, mit Erwerb sind sowohl Lieferung als auch Leistung gemeint.<br>
<u>Anmerkung:</u> Es gibt auch einen Artikel des [deutschen Bundestages](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.bundestag.de/resource/blob/586404/8d9882ff2427a3f33e0faf6ac3a3d4e0/wd-4-184-18-pdf-data.pdf&ved=2ahUKEwj07J3Qid2FAxU62AIHHSy2D8QQFnoECDkQAQ&usg=AOvVaw3afDlexfakmwQKU8SdhSU1) in dem eine quasi gleichlautende Definition vorgenommen wurde.


## Buchung eines Sachbezuges mit MwSt
Ein Sachbezug, z.B. ein Dienstfahrzeug ist vom Grundgedanken her in der Lohnverrechnung mit anzuführen. Hier kommt dazu, dass der Bruttobetrag des Sachbezuges beim Netto-Gehalt abzuziehen ist. Was wiederum dazu führen muss, dass die abzuführende Umsatzsteuer auch auf der UVA aufscheinen muss.<br>
Da aus Sicherheitsgründen bei der UVA Erstellung eine Reihe von Prüfungen durchgeführt wird, muss daher der Sachbezug extra erfasst werden.<br>
Da dies meist manuelle Buchungen sind, ist das Erlöskonto der Sachbezüge wie folgt einzustellen:<br>
![](Definition_Konto_Sachbezug_Erloese.png)

Hinweis:<br>
Achten Sie beim Erhalt Ihrer Lohnverrechnungsdaten darauf, dass Mehrwertsteuerbehaftete Beträge extra zu buchen sind. Üblicherweise trifft das für Diäten / Reisespesen und eben Sachbezüge zu.

## Konto nicht in UVA Berücksichtigen
Manchmal muss man erzwingen, dass bestimmte Konten in der UVA Prüfung ignoriert werden.
Eine der Möglichkeiten ist, das jeweilige Konto auch als Bankkonto zu definieren.

## 