---
categories: ["Finanzbuchhaltung"]
tags: ["Schlussrechnung"]
title: "Schlussrechnung"
linkTitle: "Schlussrechnung"
weight: 700
date: 2023-01-31
description: >
 Verbuchen der Schlussrechnung in der integrierten Finanzbuchhaltung
---

## Fehlermeldung beim Verbuchen einer Schlussrechnung 
Tritt beim Verbuchen der Schlussrechnung einer Eingangsrechnung eine Fehlermeldung auf<br>
![](Schlussrechnung_Fehler.png)<br>
so klicke in jedem Falle auf Detail

Hier steht in der Regel die Ursache für den Fehler.

Steht hier eine Summenungleichheit, z.B.:<br>
![](Schlussrechnung_Summeungleich.png)

so liegt unter Umständen die Ursache darin, dass eine der Anzahlungsrechnungen mit Skonto bezahlt wurde.<br>
Hinweis: Laut Steuergesetzgebung, ist eine Anzahlungsrechnung nur ein Leihen von Geld und daher nicht Skontoabzugsberechtigt.

Wie nun also vorgehen?

Wenn eine Eingangs-Anzahlungsrechnung wirklich mit Skonto bezahlt werden sollte, so einfach nur den skontierten Betrag erfassen und diesen **<u>ohne</u>** Skontoabzug bezahlen.
In den Text der Eingangsrechnung den angedruckten Wert hinschreiben und wurde mit Skonto bezahlt.

Alternative:<br>
Die Skontozahlung über das Durchläuferkonto ausgleichen.<br>
Hier gegebenenfalls über eine Splittbuchung den Vorsteuerbetrag entsprechend berücksichtigen.

Jedenfalls dürfen die Eingangs-Anzahlungsrechnungen nicht mit Skonto bezahlt werden.
