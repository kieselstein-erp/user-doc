---
categories: ["Finanzbuchhaltung"]
tags: ["Key Performance Indicators"]
title: "Key Performance Indicators"
linkTitle: "KPI"
weight: 500
date: 2023-01-31
description: >
 Die Key Performance Indicators
---
KPI Key Performance Indicators
==============================

Da unsere Anwender immer öfter nach den Kennzahlen fragen, mit denen sie Ihre Unternehmen führen sollten, hier eine Sammlung von Punkten die unserer Meinung nach wichtig sind um ein Unternehmen überhaupt führen zu können. [Für die Beschreibung der Funktion KPI siehe](#KPI Auswertung).
Wir gehen hier von einem produzierenden Betrieb aus, die Kennzahlen für reine Handelsbetriebe werden in den grundlegenden Bereichen gleich sein, in den Analysebereiche der Erfolge der Vertreter mögen Sie durchaus detaillierter sein müssen.

Bitte betrachten Sie dies auch als eine Sammlung von Anregungen ohne jeglichen Anspruch auf Vollständigkeit.

Controlling oder Kontrolle?

Gerade im deutschsprachigen Raum werden diese beiden Begriffe gerne vermischt. Bitte beachten Sie, dass es aber immer um Controlling geht, also die Steuerung Ihres/r Unternehmens. In diesem Zusammenhang ist in gewissen Bereichen auch immer wieder Kontrolle notwendig. Aber es geht um die Steuerung.

Und: Leider müssen wir Unternehmer immer wieder erkennen, dass manche Aufgaben nicht delegierbar sind und wenn wir diese nicht wahrnehmen, uns hinter zuviel anderer Arbeit verstecken, werden Dinge nicht gemacht. Bedenken Sie immer auch, es ist Ihr Unternehmen und nicht das eines Bankdirektors, eines Investors usw. (Zumindest in der Anwendergruppe die bis heute unsere Softwarelösungen einsetzt).

Was ist wichtig?

Das allerwichtigste ist, dass Sie sich auf die Daten verlassen können die Ihnen Ihr **Kieselstein ERP** präsentiert. Leider gilt insbesondere in der Unternehmenssteuerung, Shit in, Shit out. Es lässt sich nicht treffender darstellen, auch wenn es kein feines Wort ist.

Wie erreicht man nun, dass man sich auf die Zahlen verlassen kann?

In der ersten Darstellung ist das ganz einfach: Übertragen Sie Ihrem MitarbeiterInnen die Aufgabe und<br>
jetzt wird's schon schwieriger, prüfen Sie ob die MitarbeiterInnen diese Aufgabe auch so wie von Ihnen gedacht umsetzen.<br>
Werden die vereinbarten Maßnahmen nicht umgesetzt, so wird zwei Mal nachgeschult, wobei Sie bei jeder Schulung darauf achten sollten, dass IhrE MitarbeiterIn dies auch tatsächlich so umsetzen will/kann (und nicht nur ja, ja gesagt hat). Ist nun auch das dritte Mal kein Können oder Wollen erkennbar, müssen daraus die Konsequenzen gezogen werden und auch an die anderen Mitarbeiter kommuniziert werden. Das ist definitiv nicht lustig, eigentlich ist es sehr traurig, ist aber die einzige Maßnahme, die dem Rest Ihres Teams zeigt dass Sie es mit den vereinbarten Maßnahmen schon ernst meinen.<br>
**Wichtig:** Bleiben Sie Menschlich!!

Ein Beispiel:
- Können Sie sich auf Ihre Lagerstände verlassen?
- Nehmen Sie sich täglich drei Artikel, gehen Sie zu den unmöglichsten Zeiten ins Lager und zählen Sie die Artikel. Stimmen diese mit dem in Ihrem **Kieselstein ERP** System gezählten Mengen zusammen? Wenn dies nicht stimmt, wobei Sie hier wohlwollend Schwankungen / Ungenauigkeiten zu Gunsten Ihrer MitarbeiterInnen übersehen. Aber: Denken Sie auch an die Werte. Es macht keinen Sinn Beilagscheiben zu zählen (ausgenommen diese fehlen gleich Schachtelweise). Aber Artikel im Wert von einigen 100,- € stimmen immer auf das Stück genau. Wenn nicht: Warum lieber Lagerverantwortlicher stimmt das nicht?

Leider hat die Praxis gezeigt, dass es dieser Kontrollen bedarf, insbesondere dann, je komplexer Ihr Produkt ist, je mehr Zwischenstufen = Halbfertigfabrikatsstufen es tatsächlich gibt.

Welche Werte / Indikatoren sollten nun regelmäßig geprüft / verfolgt / überwacht werden?

Bitte trennen Sie hier zwischen der Überwachung der Zahlen aus Ihrem **Kieselstein ERP** und der Überprüfung ob die durch Ihr **Kieselstein ERP** errechneten Zahlen auch stimmen. Machen Sie auch Querprüfungen um den menschlichen Faktor auszuschließen.

| Thema | Ziel | Fußnote | Wie protokollieren | Aufzurufen unter | Siehe dazu auch |
| --- |  --- |  --- |  --- |  --- |  --- |
| Lagerwerte | entwickelt sich Ihr Lagerwert wie geplant | 1) | Stichtagswert | Artikel, Journal, Lagerstandsliste Alle Artikel |   |
| Lagerwert fertige Halbfertigfabrikate |   |   | Stichtagswert | Artikel, Journal, Lagerstandsliste nur Stücklisten |   |
| Ladenhüter | soweit wie möglich reduzieren |   | Stichtagswert | Artikel, Journal, Ladenhüter |   |
| Material- und Arbeitswerte in der Produktion | Welche Werte sind permanent in der Produktion gebunden |   | Stichtagswert | Los, Journal, Halbfertigfabrikatsinventur |   |
| Angebots Ausgang |   |   | Monatswert | Angebot, Journal, alle Angebote oder auch im Angebot und der Reiter Umsatzübersicht |   |
| Auftrags Eingang |   |   |   | Auftrag, Journal, alle Aufträge oder auch der Reiter Umsatzübersicht |   |
| Auftrags Nachkalkulation | Stimmen die erzielten Deckungsbeiträge mit den geplanten Deckungsbeiträge überein | 2) |   | Auswahl des Auftrags in der Auswahlliste, Info, Nachkalkulation für den einzelnen Auftrag oder auch Journal, Auftragsstatistik |   |
| Ablieferung aus den Losen | Wurde von Ihrer Produktion der geplante Ausstoß erzielt |   |   | Los, Journal, Ablieferungsstatistik |   |
| Bestellausgang |   | 3) |   | Bestellung, Journal, Alle |   |
| Wareneingang |   |   |   | Bestellung, Journal, Wareneingang |   |
|   |   |   |   |   |   |
| Zeitbuchungen auf nicht produktive Lose / Tätigkeiten | Sind die Zeitbuchungen auf nicht produktive Tätigkeiten im Rahmen, oder finden auch Sie 20.000 Std. Maschinen reinigen in einem Geschäftsjahr? |   |   | Zeiterfassung, Info, Produktivitätsstatistik, Alle, nur anwesende |   |
| Stunden Produktivität | Werden Sie und Ihr Team effizienter?Gegenüberstellung des Ausgangsrechnungsumsatzes zu den anwesenden Stunden |   |   | a.) Zeiterfassung, Info, Mitarbeiterübersicht, Alle, nur anwesende, die Gesamtstunden aller Mitarbeiter<br>b.) Rechnung, Journal, Alle Rechnungen Umsätze (oder auch Modulreiter Umsatzübersicht)Umsatz pro Monat zu Stunden pro Monat |   |
|   |   |   |   |   |   |
| Eingangsrechnungseingang |   |   |   | Eingangsrechnung, Journal, Alle oder auch Reiter Umsatzübersicht |   |
| Ausgangsrechnungsausgang |   |   |   | Rechnung, Journal, Alle oder auch Reiter Umsatzübersicht |   |
| offene ERs |   |   |   | Eingangsrechnung, Journal oder auch Reiter Umsatzübersicht |   |
| offene REs |   |   |   | Rechnung, Journal, offene Rechnungen, oder auch Reiter Umsatzübersicht |   |

- 1) Trennen Sie hier zwischen freiem Lagerwert und gesamtem Lagerwert. Die Idee muss sein, einen (freien) Lagerstand von 0,- € zu haben und jederzeit lieferfähig zu sein. Es ist dies zwar ein frommer Wunsch, gibt aber sehr plakativ die Richtung vor.
- 2) Insbesondere diese Auswertung, die für jeden Auftrag der mehr als 5,-€ Deckungsbeitrag und mehr als 50,- € Umsatz bringen sollte, gemacht werden sollte, ist der Auslöser zum Aufdecken vieler Ungereimtheiten. Diese Auswertung ist die Basis für Ihren Unternehmenserfolg. **Jeder!** Auftrag muss seinen Anteil zum Unternehmenserfolg beitragen.<br>
Es kann schon sein, dass die Entwicklung neuer Produkte einen geplanten negativen Deckungsbeitrag hat, wozu ist man Unternehmer. Aber auch diese neuen Produkte müssen in ihren Entwicklungskosten im geplanten Rahmen bleiben, wenn nicht (nicht jeder kann sich einen Flughafen leisten) warum haben Sie und Ihr Team sich so verschätzt, die Veränderung, ev. auch bewusst, herbeigeführt.
- 3) Trennen Sie bei den Bestellungen zwischen freien und Abruf-Bestellungen und offenen Rahmenbestellungen

Monatswert:<br>
Diese Werte stellen einerseits immer die Stichtagsbetrachtung dar. Andererseits kommt es vor, dass z.B. ein Auftrag eines vergangenen Monats von einem Kunden nachträglich geändert wird. Daraus kann es sich ergeben, dass aus der nun aktuellen Betrachtung eines alten Monates sich ev. andere Summen ergeben, da ja der Kunde rückwirkend seinen Auftrag geändert hat.

Sollten diese Prüfungen täglich, wöchentlich oder monatlich durchgeführt werden?

Unsere Meinung dazu: Machen Sie es von der Zuverlässigkeit abhängig. D.h. stimmt das Lager eigentlich immer, so ist die Überprüfung nicht unbedingt täglich durchzuführen. Sie sollten aber immer wieder Stichproben machen, um sicher zu sein, dass dem auch wirklich so ist. Stimmt das Lager nicht, also in einem nennenswerten Ausmaß, so prüfen Sie täglich, völlig überraschend zu möglichen und unmöglichen Zeiten.

Welche Prüfungen kann man versteckt / indirekt einbauen?

Ein gutes Beispiel ist immer den Lagerstand permanent zu prüfen bzw. den einzelnen Mitarbeitern die schriftliche Verantwortung dafür zu übertragen.<br>
D.h. wenn Material in die Fertigung gebracht wird, so übernimmt der Mitarbeiter der damit weiter arbeitet die Verantwortung dafür, der Lager-Verantwortliche wird damit von der Verantwortung für dieses Material entlastet. Wenn das nun bei Ihrem Unternehmen nicht selbstverständlich sein sollte, so nehmen Sie die Los-Ausgabeliste, geben diese zum an die Fertigung gelieferten Material und lassen sich die Übernahme des vollständigen Materials vom Vorarbeiter unterzeichnen.

Eine ähnliche Maßnahme kann mit der Packliste aus den Aufträgen erreicht werden.

In beiden Listen / Aufstellungen sind immer auch die Lagerstände nach der Entnahme angeführt. D.h. hier hat zugleich der Lagerverantwortliche die Möglichkeit eine permanent mitlaufende Inventur zu machen, wobei hier .... wenn noch 1.000Stk da sein sollten, so reicht ein kurzer Blick ob es 100, 500 oder ca. 1.000 sind. Sollte nichts mehr da sein, und es sind noch zig Stück da, so stimmt was nicht. Denken Sie hierbei bitte vor allem an falsche Stücklisten, zu wenig entnommene Teile, doppelte Lagerplätze usw.. Ja es gibt auch den Diebstahl. In aller Regel ist es, gerade bei Start-Ups / jungen Unternehmen, jedoch ein organisatorisches Problem.

Loben Sie Ihre MitarbeiterInnen

Wenn der Ausstoß der Fertigung gut war, z.B. endlich im Plan oder immer gut über Plan (je nachdem was Sie erreichen möchten / müssen) so loben Sie Ihre MitarbeiterInnen für die tolle Leistung.

Sollte man die Kennzahlen öffentlich machen?

Einige unserer Anwender veröffentlichen einige wenige der wichtigsten Kennzahlen. Zu denen gehört immer der Lagerwert.

Nutzen Sie die Liquiditätsvorschau

Und denken Sie dabei auch an die Möglichkeit der Plankosten.

Bekämpfen Sie TimWood!

Mit dieser palaktiven Bezeichnung finden Sie einen Ausdruck für die Gefahren zur Entstehung unnötiger Kosten. Haben Sie ein ernstes Auge auf diese Vorgänge!

- T ... Transport
- I ... Inventary ... Bestände
- M ... Motion ... (zuviel) Bewegung
- W ... Warten (auf ...)
- O ... Over-production ... Über Produktion
- O ... Over-engineering ... Falsche Technologien / falsche Prozesse aber auch Detailverliebtheit, die in einer Serienfertigung nichts verloren hat
- D ... Defects ... Ausschuss, Nacharbeit, Garantiekosten

Lagermindeststände

sind praktisch um die Lieferfähigkeit hoch zu halten, binden Kapital, dass Sie sinnvoller einsetzen können und verdecken die Strukturprobleme Ihrer Fertigung. Reduzieren Sie die Lagermindeststände. Auch wenn wir die theoretische Losgröße ein Stück für hoffnungslos übertrieben halten, so ist es eine gute Vorgabe um Möglichkeiten zu Strukturverbesserungen aufzuzeigen.

<a name="Sicherheit und Vertrauen"></a>Sicherheit und Vertrauen in der Buchhaltung

Aus unserer langjährigen Erfahrung möchten wir Ihnen folgende Gedankengänge und Empfehlung mitgeben.<br>
Hier kommt leider mit dazu, dass noch immer gilt Vertrauen ist gut, Kontrolle muss aber sein.
Gerade in KMU (kleinen und mittelständischen Unternehmen) ist es doch immer oft so, dass wenige Mitarbeiter sehr umfassende Funktionen innehaben / innehaben müssen.<br>
So ist es durchaus üblich, dass der Einkäufer sich um den strategischen Einkauf kümmert, die einzelnen Komponenten beschafft und da er dabei auch immer die Liquiditätssituation im Blick haben muss, bekommt er auch noch die Steuerung der Zahlung der Eingangsrechnungen aufgetragen.<br>
Damit man auf der sicheren Seite ist, wird die Freigabe der Eingangsrechnungen von einer anderen Person gemacht.<br>
Leider kommt hier manchmal und hoffentlich nicht in Ihrem Unternehmen die Not oder auch die kriminelle Energie mancher Menschen zum Vorschein.<br>
Daher die Frage: Wie kann man in dieser Konstellation, es wird doch jede Eingangsrechnung geprüft, noch Geld abzweigen.<br>
Es war uns dies selber viele Jahre nicht klar, es ist aber ganz einfach. Nachdem nur die Eingangsrechnungen geprüft werden, braucht nur von den Hauptlieferanten eine Eingangsrechnung fingiert werden. Dies ist in Zeiten von Photoshop und Co sehr einfach. Diese wird im System erfasst. Damit ist sie eine offizielle Eingangsrechnung die auch zu bezahlen ist. Also wird diese Eingangsrechnung von der prüfenden Person auch freigegeben. Nachdem der Einkäufer, die Buchhalterin ja auch die Zahlungen auf die diversen Konten veranlasst und nur die freigegebenen Eingangsrechnungen bezahlt werden können / dürfen, wird die entsprechende Überweisung gemacht und bei der besonderen Eingangsrechnung ein anderes Empfängerkonto angesprochen. Damit haben Sie selber / die prüfende Person die Falsch-Zahlung freigegeben. Damit ist das Geld schlicht weg.<br>
**Wie können Sie sich dagegen schützen?**<br>
Bitten Sie vor allem Ihre Lieferanten zumindest einmal im Jahr um eine Kontoabstimmung. Diese muss beinhalten, den Umsatz im Zeitraum z.B. im vergangenen Jahr und die aktuell / zum Stichtag offene Posten. Dies muss mit Ihren Buchungen / Ihrer Buchhaltung auf den Cent / den Rappen übereinstimmen.<br>
Gerade bei Eingangsrechnungen, muss die Buchhaltung gut begründen, warum Sie dafür keine Bestellung hat, wo es doch um Warenlieferungen (die auch Leistungen sein können) geht. Wie will man denn was nachkalkulieren, wenn keine Zubuchungen gemacht werden?<br>
**Welcher Personenkreis sollte sich schützen?**<br>
- Geschäftsführer die nicht Eigentümer sind
- Verantwortliche die den Kreis Bestellung, Eingangsrechnung, Zahlung delegiert haben.

Verwenden Sie zwei Buchungssysteme?<br>
Wenn Sie diese Konstellation haben, also ein ERP System mit Warenwirtschaft und Eingangsrechnungen und natürlich als **Kieselstein ERP** Anwender auch das komplette Bestellmodul und andererseits eine ausgelagerte Buchhaltungslösung z.B. beim Steuerberater, Wirtschaftstreuhänder, so führen Sie diese Prüfung für beide Systeme durch. Aus Sicht der offenen Posten ist sicherlich die Buchhaltung führend. Wir haben hier jedoch auch schon bei Steuerberatern sehr interessante Dinge gesehen. Es ist Ihr Unternehmen.

**Warum das Ganze?**<br>
Es wurden hier leider schon deutlich sechsstellige Eurobeträge veruntreut. Davor sollten Sie sich schützen.

Zusatzinfo: Dass Sie zusätzlich laufend die Ladenhüter und die nicht benötigten Materialien im Blick haben ergibt sich von selbst, oder wie schon oben beschrieben. Dass der Lagerstand stimmt ist in der Warenwirtschaft so selbstverständlich, wie in der Buchhaltung, dass die Bank stimmt. **Zu jederzeit!**

<a name="KPI Auswertung"></a>KPI Auswertung
-------------------------------------------

Gemeinsam mit der Funktionalität Liquiditätsvorschau kommen auch die Module (sehr) [einfache Erfolgsrechnung]( {{<relref "/management/finanzbuchhaltung/einfache_erfolgsrechnung"  >}} ) und KPI.<br>
Sie finden diese im Modul Finanzbuchhaltung, Journal.

Die KPI Auswertung ist für eine Übersicht über wenige wesentliche Kennzahlen gedacht.<br>
Zusätzlich ist diese Auswertung, welche keine wie immer gearteten Anspruch auf Vollständigkeit hat, so aufgebaut, dass es eine Art übergeordneter Report ist, in welchem verdichtete Zahlen dargestellt werden, welche wiederum aus den darunter liegenden Reports kommt. Das hat den enormen Vorteil, dass speziell für Sie zusammen gestellte Auswertungen auch in den KPIs berücksichtigt werden können.

Derzeit kann die KPI Auswertung folgende darunter liegenden Auswertungen berücksichtigen:
-   Auftragseingang, getrennt in freie Aufträge und Abrufe und Rahmenaufträge
-   Rechnungsausgang inkl. Gutschriften
-   Fertigungs-Ablieferstatistik
-   Fehlerarten aus den Reklamationen
-   Geleistete Arbeitsstunden auf allen Arten von Belegen mit Zeitbuchungen
-   offene Eingangsrechnungen
-   offene Ausgangsrechnungen

Der Aufruf erfolgt mit der Angabe des Zeitraums von - bis, welcher in der Regel mit den letzten sieben Tagen vorbesetzt wird, womit Sie am Montag den Erfolg der letzten Woche zur Verfügung haben.

Zusätzlich steht die KPI Auswertung als sogenannter Automatikjob zur Verfügung. Damit wird die z.B. wöchentliche Auswertung als PDF in einem zu definierenden Verzeichnis abgelegt. Zusätzlich können diese PDFs auch per EMail versandt werden. Denken Sie bitte daran, dass das PDF nicht verschlüsselt versandt wird. Mehrere EMail-Empfänger per ; Strichpunkt / Semikolon getrennt.

Für die Aufnahme weiterer Auswertungen in die KPI Zusammenfassung wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Berater.