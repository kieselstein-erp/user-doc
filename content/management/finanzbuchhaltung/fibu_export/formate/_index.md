---
categories: ["Finanzbuchhaltung"]
tags: ["Formate"]
title: "Export Formate"
linkTitle: "Export Formate"
weight: 900
date: 2023-01-31
description: >
 Export Formate
---
Fibu Export
===========

In **Kieselstein ERP** steht Ihnen auch eine entsprechende Exportschnittstelle für Ihre Finanzbuchhaltungsdaten zur Verfügung, damit diese möglichst komfortabel in Ihre gewünschte Ziel-Fibu übernommen werden können. Diese Exportfunktion ist jedoch nur dann verfügbar, wenn Sie nicht die noch wesentlich komfortablere Funktion der integrierten Finanzbuchhaltung verwenden.

Es stehen verschiedene Exportformate, welche nachfolgend beschrieben sind, zur Verfügung.

1. RZL Format

Hier ist das spezielle RZL Format sowohl für Eingangs- als auch für Ausgangsrechnungen realisiert. Da dies ein sehr eigenwilliges Format ist, kann es von anderen Programmen nicht verwendet werden.

2. Datev-CSV

| Feldbezeichnung  | Typ | Max-Länge | Mussfeld | Befüllt mit |
| --- |  --- |  --- |  --- |  --- |
| Umsatz (mit Soll/Haben-Kz) | N | 14 | ** | Rechnungsbetrag BruttoSoll=-, Haben=+ (dahinter) |
| BU-Schlüssel | N | 2 |  |  |
| Gegenkonto (ohne BU-Schlüssel) | N | 9 | ** | Debitoren/Kreditorenkonto |
| Belegfeld1 | A | 12 |  | RE/GS Nr. |
| Belegfeld2 | A | 12 |  | AR, GS, ER |
| Datum | D | 8 | * | TTMM |
| Konto | N | 9 | * | Erlös-/Aufwandskonto |
| Kostfeld1 | A | 8 |  |  |
| Kostfeld2 | A | 8 |  |  |
| Kostmenge  | N | 11 |  |  |
| Skonto  | N | 12 |  |  |
| Buchungstext | A | 30 |  |  |
| EU-Land und UStID  | A | 15 |  | Kunden/Lieferanten UID-Nr. |
| EU-Steuersatz | N | 4 |  |  |
| Währungskennung | A | 3 |  | Mandantenwährung |
| Basiswährungsbetrag  | N | 14 |  |  |
| Basiswährungskennung  | A | 3 |  |  |
| Kurs | N | 11 |  |  |

3. BMD Format

Der BMD Export kann auch für den CSV Export verwendet werden.

Dieses Format ist wie folgt aufgebaut:

Trennung der Felder: ; (Semikolon). 

Dezimaltrenner: ,

Nach- und Vorkommastellen durch Dezimaltrenner getrennt

Datumsformat: JJJJMMTT (Jahr im Jahrtausendformat, Monate, Tage)

Splittbuchungen werden NICHT abgebildet

Felder:

| Überschrift | Beschreibung / Inhalt | Bemerkung |
| --- |  --- |  --- |
| Satzart | Satzart zwingend 0 |  |
| konto | Debitorenkonto |  |
| buchdat | Buchungsdatum jjjjmmtt |  |
| gkto | Gegenkonto, = Erlöskonto oder Aufwandskonto |  |
| belegnr | Belegnummer |  |
| belegdat | Belegdatum |  |
| kost | Kostenstelle |  |
| mwst | Steuerprozentsatz | Prozentsatz der Steuer |
| steucod | USt-Steuercode | (1) |
| bucod | Sollbuchung | 1 ... Sollbuchung (RE),2 ... Habenbuchung (ER) |
| betrag | Bruttobetrag | Sollbuchungen (RE) positiv,Habenbuchungen (ER) negativ |
| steuer | Steuerbetrag | bei Kunden negativ, bei Lieferanten positiv |
| periode | Buchungsperiode, das "Geschäftsjahr" |  |
| kurs | Der Name des Rechnungskurses in ISO Code (EUR, USD) |  |
| fwbetrag | Fremdwährungsbetrag |  |
| fwSteuer | Fremdwährungssteuerbetrag |  |
| text | Buchungstext | Bei RE eine Bemerkung (z.B. die Bestellnummer) dazu, bei ER die interne ER-Nummer |
| symbol | Buchungssymbol | RE, ER, GS |
| extbelegnr | Externe Belegnummer | z.B. ER Lieferantenrechnungsnummer |
| zziel | Zahlungsziel | netto Zahltage |
| Skontopz | Skonto % |  |
| skontotage | Skonto Tage |  |
| Skontopz2 | Skonto % 2 |  |
| skontotage2 | Skonto Tage 2 |  |
| vertnr | Vertreter Nummer | Personalnummer der dem Kunden zugeordneten Provisionsempfängers |
| auftkz | Anzahlungsrechnung | (2) |
| auftnr | Projektnummer = Auftragsnummer | wenn Anzahlungsrechnung, sonst leer |
| zmart | ZM Art IG Lieferung | (3) |
| verbuchkz | Verbuchungskennzeichen | Fix auf A |

Anmerkungen:

(1) Der USt-Steuercode dient zur Kennzeichnung der UST-Steuerart
| UST-Steuerart | Bedeutung |
| --- | --- |
| 00 | Vorsteuer oder keine Steuer|
| 01 | Ist-Umsatzsteuer |
| 03 | Soll-Umsatzsteuer |
| 08 | Einkaufskonten für innergem. Erwerb, wenn kein VSt.-Abzug besteht |
| 09 | Einkaufskonten für innergem. Erwerb, wenn VSt.-Abzug besteht |

(2) Anzahlungsrechnungskennzeichen:
| Kennzeichen | Bedeutung |
| --- | --- |
| leer | keine Anzahlungsrechnung |
| 01 | Teilrechnung |
| 02 | Storno der Teilrechnung |
| 03 | Anzahlungsrechnung |
| 04 | Storno der Anzahlungsrechnung (wird derzeit nicht unterstützt) |
| 05 | Schlussrechnung |

(3) Durch dieses Kennzeichen wird die Art des IG-Geschäfts näher bestimmt:
| Kennzeichen | Bedeutung |
| --- | --- |
| 0 | ig. Lieferung oder Verbringung |
| 1 | Werkleistung |
| 2 | Warenbewegung |
| 3 | Dreiecksgeschäfte |
| 9 | Sperre für ZM |

Hinweis: Ausgangsgutschriften werden mit negativen (invertierten) Beträgen, aber wie eine RE gebucht.

4. Lexware Format

Die Datenübergabe an Lexware wird anhand der Daten welche für BMD erstellt werden über eine eigene Exportbeschreibung realisiert. Wir haben dafür folgende Besonderheiten eingebaut.

- a.) Lexware kennt keine negativen Buchungen, daher werden eine Soll- und eine Haben-Spalte übergeben in der die jeweiligen Beträge immer positiv eingetragen sind.

- b.) um auch die Lieferantenrechnungsnummer der Eingangsrechnungen an Lexware durchzureichen wird diese in der Textzeile hinter dem Partnernamen mit angegeben

- c.) Beachten Sie bitte, dass die jeweilige Kurzbezeichnung des Partners (Kunde/Lieferant) als Text = Name übergeben wird.