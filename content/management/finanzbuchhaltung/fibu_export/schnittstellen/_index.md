---
categories: ["Finanzbuchhaltung"]
tags: ["Schnittstellen", "Export"]
title: "Finanzbuchhaltung"
linkTitle: "Schnittstellen"
weight: 900
date: 2023-01-31
description: >
 Schnittstellen / Exporte der ERP Daten zu fremden Buchhaltungssystemen
---

Neben der integrierten Finanzbuchhaltung (siehe) stehen auch Schnittstellen zu üblichen Finanzbuchhaltungssystemen zur Verfügung.
Die Grundidee dahinter ist:
a.) Erfassen der Rechnungen und Gutschriften, sowohl Ausgangs- wie Eingangsrechnungen im **Kieselstein ERP**
b.) Export der erfassten Rechnungen in die externe Buchhaltung, inkl. Belege
c.) Buchung der Bank in der externen Buchhaltung
d.) Rückimport der offenen Postenliste und damit im ERP erkennen, welche Rechnungen schon bezahlt bzw. offen sind.
Sollten die externe Buchhaltung beim Steuerbrater sein, raten wir dringend trotzdem das Zahlungswesen und damit das Mahnungswesen selbst zu machen.
Gegebenenfalls auch dafür den SEPA Import / SFirm nutzen

Es werden folgende externe Buchhaltungssysteme unterstützt
Da die Export-Definitionen neben der Aufbereitung durch den Server über eine XSL Definition laufen können auch weitere Buchhaltungssysteme angebunden werden.

## DATEV
Vom Datev gibt es wiederum mehrere Versionen. Auch sind die Kontenrahmen meist sehr starr und gerade österreichische Firmen wundern sich, warum z.B. das Bankkonto nicht 2803 heißen kann.
Datev kennt sogenannte Automatikkonten. Das sind Konten, welche die Ust / VST Buchung automatisch machen. D.h. daher darf für diese Konten kein Buchungsschlüssel übergeben werden.
Darum müssen diese im Firmenparameter: EXPORT_DATEV_KONTOKLASSEN_OHNE_BU_SCHLUESSEL entsprechend definiert werden.

### Finanz Online
mit Übergabe der Belege
Vorteil, du kannst direkt deine Daten beim Steuerberater einspielen, es gibt keine EMail Kommunikation mehr dazwischen. Damit sind die Geheimhaltungs / DSGVO Themen deutlich reduziert.

Weitere notwendige Einstellungen für DATEV Online, damit auch die Belege übertragen werden.


### Datev im Hause

## BMD

## RZL
Der große Nachteil der RZL Schnittstelle ist, dass die RZL eigene Buchungslogik in der Schnittstelle bereits nachprogrammiert werden muss. Wenn imer möglich vermeiden Sie die RZL Schnittstelle.





An weiteren Buchhaltungssystem wurden bisher realisiert:
- Lexware
- 



## Export des Buchungsjournals aus der integrierten Finanzbuchhaltung 
Finanzprüfung
Übergabe an Steuerberater