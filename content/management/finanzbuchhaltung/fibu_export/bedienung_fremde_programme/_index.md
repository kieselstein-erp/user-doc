---
categories: ["Finanzbuchhaltung"]
tags: ["Fremdprogramme"]
title: "Bedienung Fremdprogramme"
linkTitle: "Bedienung"
weight: 900
date: 2023-01-31
description: >
 Bedienungshinweise für die Übertragung der Daten in fremde Buchhaltungsprogramme
---
Soweit uns bekannt ist, keinen Anspruch auf Richtigkeit oder Vollständigkeit

Bedienung der fremden Finanzbuchhaltungsprogramme
=================================================

Hier haben wir die Eigenheiten und die Bedienung der unterschiedlichen Finanzbuchhaltungsprogramme für den Import / Export der **Kieselstein ERP** Daten beschrieben.

1.) RZL
-------

#### Wie sind die von **Kieselstein ERP** erzeugten Fibu-Daten in meine RZL Buchhaltung zu importieren?
Wählen Sie beim Import das Format mit Trennzeichen. Als Trennzeichen wird von **Kieselstein ERP** das Semikolon (;) verwendet.

Als Zeichensatz wählen Sie Windowsformat.

Der Importpfad muss passend zu den oben definierten **Kieselstein ERP** Exportpfaden in Ihrer RZL Installation eingestellt werden.

Für den ersten Import wählen Sie die Funktion Probelauf. Erst wenn dieser fehlerfrei durchgelaufen ist, wählen Sie bitte den eigentlichen Import.

Bedenken Sie bitte, dass bei jedem Import Fehler, Abbrüche passieren können. Aus diesem Grund empfiehlt sich, einen Import in Ihre Buchhaltung nur mit einer entsprechenden Datensicherung vorzunehmen.

#### Beim Import der Personenkonten erfolgt eine Fehlermeldung.
Es erfolgt die Fehlermeldung: Ungültiges Zeichen in Zessionsnummer.
Die Ursache ist hier, dass in Ihrem RZL Programm nur die alte 12-Spaltige Importroutine zur Verfügung steht. Hier steht eine geänderte XSL Definition zur Verfügung um die Daten auch in die älteren RZL Programme importieren zu können. Wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

Prüfung der CSV Dateien

Um Fehler im **Kieselstein ERP** Export / Import in RZL zu finden, kann die erzeugte CSV Datei sehr einfach in eine Tabellenkalkulation importiert werden. Bei der Überprüfung der Saldengleichheit ist zu beachten, dass die Ausgangsrechnungen und die Ausgangsgutschriften für Soll - Haben - Ust einen Saldo von 0 über alle Buchungen haben müssen, hingegen wird bei den Eingangsrechnungen Soll - Haben + Ust gerechnet. Dies wird dem RZL Importprogramm durch unterschiedliche Ust-Codes (1 = Vorsteuer, 2 = Umsatzsteuer) mitgeteilt.

Dateninhalt des RZL Exportes

In den beiden Buchungszeilen sind bei RE und GS die Firmennamen der Kunden enthalten.

Beim ER Export ist in der OP_Numer die Lieferantenrechnungsnummer eingetragen und unter Belegnummer die ER-Nummer aus Ihrer **Kieselstein ERP** Datenbank. Unter Buchungstext1 wird die unter Text eingegebene Beschreibung übergeben und unter Buchungstext2 wiederum der Lieferantenname der Eingangsrechnung.

Kostenstellen

Um die Kostenstellen auch in die verschiedenen Finanzbuchhaltungsprogramme exportieren zu können, ist es erforderlich, dass die Kennung der Kostenstellen numerisch sind. (Gilt zumindest für RZL und BMD).

Ust-Code

Für RZL muss je Land der sogenannte Ust-Code (Umsatzsteuercode) definiert werden. Tragen Sie diesen, gemäß RZL Definition, unter System, Land, FIBU Ust-Code, ein.

2.) Datev
---------

#### Wie sind die von **Kieselstein ERP** erzeugten Fibu-Daten in meine Datev Buchhaltung zu importieren?
- a.) Importformate definieren
Unter Extras Ascii Formate können unter dem Reiter Importformate die Importformate definiert werden. Es wird das Datenformat Buchungssätze, Formatname Buchungssätze verwendet.
![](datev3.gif)

- b.) Nach der einmaligen Definition gehen Sie für den eigentlichen Import auf
Buchen, Stapelverarbeitung
Rechts den Knopf ASCII-Import wählen.
**ACHTUNG:** Wählen Sie unter Datenformat Buchungssätze und unter Formatbezeichnung den oben definierten Formatnamen Buchungssätze.

**Wichtig:** Abhängig davon ob Ihre **Kieselstein ERP** Dateien mit oder ohne Kopfzeilen exportiert werden (siehe System, Parameter, FINANZ_EXPORT_UEBERSCHRIFT), muss der

"Import ab Zeile" auf 1 ... wenn ohne Kopfzeilen oder 2 ... wenn mit Kopfzeilen gestellt werden.

Der Zeichensatz muss auf ANSI gestellt werden.

![](datev4.gif)<br>
Überprüfen Sie nun die Einstellungen durch Klick auf Ergebnisvorschau.

Diese sollten wie folgt aussehen.<br>
![](datev5.gif)

Stellen Sie nun die richtige Buchungsperiode für den Import ein, 
in dem unter "Vorlaufdaten für Buchungssätze" der zum Export passende Zeitraum eingegeben wird.

![](datev6.gif)

Zusätzlich sollte noch, Originalbestand nach Import löschen angehakt sein.

Klicken Sie nun auf ok um den Import durchzuführen und das Import-Definitionsfenster zu schließen
Sie erhalten die Meldung "Der Vorlauf wurde erfolgreich importiert".

![](datev8.gif)
Erhalten Sie Fehlermeldungen, so stimmen die Formate nicht überein. Überprüfen Sie die Importformate.

Sie finden nun einen neuen Stapelverarbeitungsdatensatz in der Liste der Stapelverarbeitungen

![](datev9.gif)
Verarbeiten Sie nun den Stapel wie üblich durch Klick auf Verarbeiten.

Getestet auf Version Datev V.5.61 (2009)

**Hinweis:** Datev kennt bei der Übergabe der Daten nur ein Vorzeichen. Damit wird gesteuert ob der Betrag ins Soll oder ins Haben gebucht wird. Eine negative Soll-Buchung ist von Seiten Datev nicht möglich. Dies wurde von Seiten Datev GmbH auch nach Rückfrage so bestätigt.

Definition in der neuen Datev Version (ab 2012):

Wählen Sie den neu angelegten / vorhandenen ASCII-Import und wechseln auf Feldauswahl. Folgende Definition muss dem Export von **Kieselstein ERP** entsprechen:

![](datev_versionneu.JPG)

3.) BMD
-------

#### Wie sind die von **Kieselstein ERP** erzeugten Fibu-Daten in meine BMD Buchhaltung zu importieren?
Wählen Sie in Ihrer BMD das Importprogramm pr08a. Menüpunkt Fibu, Buchungsprogramme, Verbuchen Schnittstellen automatisch.

![](BMD_pr08a_1.gif)

Die Frage nach der Art der Bearbeitung bestätigen Sie bitte mit Fibu Allgemein

![](BMD_pr08a_2.gif)

Für den Datenimport bestätigen Sie das Erfassungsfile, welches standardmäßig auf Jahr/fib/buerf steht. D.h. um einen automatischen Import zu erhalten, stellen Sie bitte den Dateinamen für die Exportdateien ebenfalls auf diese Datei. Z.B.: *c:\\BMD\\2010\\FIB\\buerf* (System, Parameter, FINANZ_EXPORTZIEL_EINGANGSRECHNUNG)

Nach der Bestätigung der Daten (in jeder Zeile mit Enter), erhalten Sie eine Maske mit 

![](BMD_Import.gif)

Wählen Sie hier zuerst 4 - Ansehen Erfassungsfile. Wenn diese Daten glaubwürdig sind, wählen Sie 1 - Verbuchen aus Erfassungsfile.

**<u>ACHTUNG:</u>**

Es muss hier einmalig der Parameter Code für Gegenbuchung auf 1 gesetzt werden.

Drücken Sie dazu in obiger Maske die Taste F6 (allgemein).

Wählen Sie nun 

![](BMD_Parametrierung1.gif)

und drücken Sie Enter (Return).

![](BMD_Parametrierung2.gif)

Stellen Sie nun den Code für Gegenbuchung auf 1, was soviel bedeutet wie Einzelgegenbuchung.

Dies bewirkt, dass bei jeder Importzeile in den Sachkonten eine eigene Buchung eingetragen wird.

Speichern Sie die neue Einstellung nun mit F2 (speichern und Ende) ab, damit sie für den nächsten Programmaufruf entsprechend vorbesetzt ist.

Die Parameter Auswahlmaske verlassen Sei mit ESC.

**Ansehen der erfassten Daten**

Um die erfassten Daten in einem Journal anzusehen, wählen Sie Fibu, Buchungsprogramme Buchen(pr03) und dann J - Journal ansehen.

Dass die Kontierungen in BMD zu den Kontierungen in **Kieselstein ERP** passen müssen erklärt sich von selbst.

Wir raten dringen, immer vor dem Import der Daten eine entsprechende Sicherung Ihrer Buchhaltungsdaten zu erstellen. Sie können dazu das in BMD integrierte Programm Ausspielen bzw. Einspielen verwenden. Sie finden diese Programme unter Fibu, Datenschnittstellen, Importschnittstellen bzw. Exportschnittstellen.

Diese Beschreibung wurde für die Version 5.5 erstellt.

**Hinweis**: Wird bei der Anzeige des Erfassungsfiles, eine leere Datei angezeigt, so wurden vermutlich die Spaltenüberschriften nicht aktiviert.

Stellen Sie daher in System, Parameter, den Parameter FINANZ_EXPORT_UEBERSCHRIFT auf 1.

**Hinweis:** Von **Kieselstein ERP** wird für die Übergabe der sogenannte variable Satzaufbau verwendet.

**Bedienung der fremden Buchhaltungsprogramme zum Erzeugen der offenen Postenliste für den Import.**
-----------------------------------------------------------------------------------------------------

### <a name="OP-Liste_Datev"></a>- 1. Datev

Direkt in den Export mit (Menüpunkt) Bestand, Export, ASCII
Nun einstellen auf:
- Datenformat Geschäftspartner (Offene Posten)
- Formatname Geschäftspartner (Offene Posten)
- Exportdatei auswählen. Hier am Besten den Pfad auf eigene Dateien
- Dateiname z.B. op.csv
    - Wichtig: Zeichensatz auf ANSI
- Konto von bis .... nur Debitoren anhaken
- Postenumfang nur Offene Posten
- Raffung ... gerafft. (Das bedeutet, pro Rechnung nur eine Zeile)
![](Fibu_Datev_OP_Export.gif)

Seit ca. 2012 gibt es eine neue Version von Datev, mit der die zu exportierenden Felder frei definiert werden können/müssen.

Da wir für den Import der offenen Posten bestimmte Felder in einer bestimmten Reihenfolge erwarten, hier eine komprimierte Beschreibung wie diese Einstellungen vorzunehmen sind.

**HINWEIS:**

Werden die Default-Einstellungen von Datev verwendet, so führt dies unter Umständen dazu, dass die noch offenen Rechnungen nicht erkannt werden. Daher werden diese fälschlicherweise als bezahlt verbucht, obwohl dem laut Ihrer Buchhaltung nicht so ist.

Die Vorgehensweise zur Definition eines neuen Exportformates ist daher wie folgt:
1.  Wählen Sie in der Buchführung bei den Debitoren die OPOS-Liste<br>
    ![](Datev_OPOS_Liste.jpg)
2.  Nun wählen Sie aus der Menüleiste: Bestand, Export, ASCII
3.  Nun geben Sie als (neues) Exportformat z.B. **Kieselstein ERP** an und klicken unten auf Weiter
4.  Stellen Sie die grundsätzlichen Exporteinstellungen wie folgt ein und klicken Sie dann auf weiter:<br>
    ![](Datev_Exportdefinition1.jpg)<br>
5.  Wählen Sie nun die zu exportierenden Felder und stellen Sie diese auch in der entsprechenden Reihenfolge zusammen.<br>
    ![](Datev_Exportdefinition2.jpg)<br>
    Achten Sie darauf, dass die Reihenfolge der Felder wie angegeben ist.
6.  Klicken Sie nun unten auf die Diskette ![](Datev_Exportdefinition3.gif) um die Formatdefinition zu speichern.
7.  Nun wählen Sie Exportieren (unten neben der Diskette) um die Daten zu erzeugen. Akzeptieren Sie den Vorschlagswert.![](Datev_Exportdefinition4.jpg)
8.  Nun wechseln Sie in Ihr **Kieselstein ERP**, in das Modul Finanzbuchhaltung, unterer Reiter Export und wählen in der Menüleiste Export, Offene Posten Import.<br>
Geben Sie den obigen Pfad an, welchen Sie in der Regel im vorgeschlagenen Pfad (Dokumente) und dann in den Unterverzeichnissen Daten, RWDAT, Export finden.<br>
Wählen Sie hier die oben angegebene Datei aus. In unserem Fall die op.csv
9.  Beachten Sie die eventuell ausgegebenen Warnungen

#### Beim Import werden alle Rechnungen als bezahlt verbucht?
Siehe oben. Mit ziemlicher Sicherheit wurde die op.csv Datei falsch aus dem Datev exportiert.