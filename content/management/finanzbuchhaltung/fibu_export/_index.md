---
categories: ["Finanzbuchhaltung"]
tags: ["Export"]
title: "Export"
linkTitle: "Export"
weight: 900
date: 2023-01-31
description: >
 Die verschiedenen Export Funktionen und Formate
---
Bitte unterscheide ob mit integrierter Fibu oder ohne

Export der AR/ER/GS Daten in eine fremde Finanzbuchhaltung
==========================================================

Wenn Sie das Modul Finanzbuchhaltung der **Kieselstein ERP** nicht besitzen, so besteht die Möglichkeit Exportdaten für verschiedene Fremdbuchhaltungen zu exportieren.

Wechseln Sie dazu in das Modul Finanzbuchhaltung, wählen Sie im unteren Modulreiter Export.

Hier sehen Sie eine Übersicht über die bisherigen Exportläufe.

Vorbereitende Überlegungen für den automatischen Export der Rechnungen / Gutschriften von **Kieselstein ERP** in Ihre Finanzbuchhaltung.

Das grundlegende Thema ist hier immer wieder die automatische Erlöskontenfindung für die Ausgangsrechnungen.

Import der offenen Postenliste [siehe](#Import_OP-Liste).

### Erlöskontenfindung

<a name="Erlöskontenfindung"></a>

Wir kennen zwei Arten wie die Erlöskontenfindung erfolgt:
- a.) Kostenstellen<br>
Hier definieren die Kostenstelle der jeweiligen Rechnung auf welches Erlöskonto gebucht wird. Zusätzlich kann für Sonderfälle die Kontierung der einzelnen Rechnung mit dem oberen Modulreiter Kontierung übersteuert werden und so eine manuelle Aufteilung der Erlöse vorgenommen werden.
- b.) Artikelgruppen<br>
Beim Export nach Artikelgruppen kann für jeder Artikelgruppe ein Erlöskonto zugeordnet werden. Damit wird erreicht, dass in Ihrer Finanzbuchhaltung eine ähnliche Struktur in den Erlöskonten vorliegt, wie bei den Artikelgruppen. Damit werden die Erlöse je Artikelgruppe auf die unterschiedlichen Länder verteilt gebucht.

In aller Regel wird die Aufteilung der Erlöse nach den Artikelgruppen verwendet.

Bitte beachten Sie dazu auch die Aufteilung auf [Lieferung und Leistung](index.htm#Lieferung und Leistung).

Wie werden Erlöse in EU-Länder, Drittland etc. behandelt?

Um die richtige Zuordnung auf unterschiedliche Länderkonten zu erreichen steht die Übersetzung der Erlöskonten nach Länderarten zur Verfügung. D.h. in Ihrer Definition z.B. Artikelgruppen hinterlegen Sie immer das Inlandskonto für dies Artikelgruppe.

Beim Konto selbst hinterlegen Sie welches Konto für eine andere Länderart bzw. für ein bestimmtes Land verwendet werden soll.

Beispiel für die Artikelgruppe Drehteile
| Erlöskonto | Konto |
| --- |  --: |
| Erlöskonto Inland für Drehteile | 4020 |
| Erlöskonto EU mit UID-Nr für Drehteile | 4120 |
| Erlöskonto Drittland für Drehteile | 4220 |

Legen Sie zuerst alle Erlöskonten unter Sachkonto an.

Bei der Artikelgruppe hinterlegen Sie unter Konto ![](Fibu_Export_Artikelgruppen_Definition.gif) das Inlandskonto.

Nun definieren Sie noch die Übersetzungen für die Auslandskonten / Länderarten.

Sie wählen das entsprechende Konto, wechseln auf Konto-Länderart 

![](Fibu_Export_Laenderart_Uebersetzung.gif)

und definieren, welches Konto anstelle des Inlandskontos für dieses Konto bei einer entsprechenden Art des Landes verwendet werden sollte.

Sollte für ein bestimmtes Exportland zusätzlich noch ein anderes Konto verwendet werden, so definieren Sie dies in gleicher Form unter Konto-Landzuweisung.

Bei der Überleitung der Daten wird so vorgegangen, dass zuerst eine Übersetzung für das Land gesucht wird. Ist dafür keine Übersetzung vorhanden, so wird anhand der Länderart eine Übersetzung gesucht. Ist auch diese nicht vorhanden, so erscheint eine entsprechende Fehlermeldung.

**Anmerkung:**

Für die Eingangsrechnungen ist die Frage welches Aufwandskonto verwendet werden sollte relativ leicht beantwortet, da die Kontierung direkt in den Kopfdaten der Eingangsrechnung vorgenommen wird. Beim Export der Daten wird genau dieses Konto angegeben.

#### Wie wird die Länderart bestimmt?
<a name="Länderart"></a>
Die Bestimmung der Länderart richtet sich im wesentlichen nach der Definition des Landes unter System, System, Land.
![](Fibu_Export_Laenderdefinition.png)  

| Länderart | Beschreibung |
| --- |  --- |
| Inland | Das Länderkennzeichen (LKZ) entspricht dem eigenen Länderkennzeichen |
| EU-Mitglied | Ist unter EU-Mitglied seit ein Datum eingetragen, so wird ab diesem Zeitpunkt dieses Land als EU-Land angenommen (solange das leer ist bzw. das Belegdatum innerhalb dieses Bereiches ist) |
| Drittland | treffen obige Definitionen nicht zu so wird Drittland angenommen |
| EU-Mitglied ohne UID Nummer | Ist das Land des Kunden zwar ein EU-Land, ist aber beim Kunden keine UID Nummer hinterlegt, z.B. für Privatkunden, so wird diese Länderart angenommen. |

Zusätzlich wird die Länderart vom Land der Lieferung bestimmt. D.h. wird z.B. eine Rechnung an einen Kunden im Inland gestellt, aber die Lieferadresse ist z.B. im EU-Ausland, so ist die Länderart EU-Ausland.<br>
Da Lieferungen an eine abweichende Adresse meistens trotzdem im gleichen Land sind, wurde die Ermittlung der Debitoren / Kreditorennummern so erweitert, dass wenn Rechnungsland und Lieferland im gleichen Land sind, die Debitorennummer und auch die Länderartübersetzung von der Rechnungsadresse verwendet werden.<br>
Zusätzlich wird hier auch das übersteuerte Umsatzsteuerland berücksichtigt, was bedeutet, dass der steuerliche Sitz Ihres Kunden ausschlaggebend ist und nicht mehr die postalische Adresse.

#### Wo sieht man die Länderart bzw. Länder Übersetzungen
Drucken Sie aus den Sachkonten, Journal, Konten die Übersicht über die Konten aus. Hier werden auch alle Übersetzungen der Länderarten bzw. der Länder mit angeführt. Bitte beachten Sie, dass die rot angedruckten Konten eine Definition des Kontos auf sich selber ist, welche NICHT erforderlich ist.

#### Übersteuerte Länderart
Da, Aussage eines Steuerberaters und Wirtschaftstreuhänders, das Umsatzsteuerrecht außer kompliziert nur kompliziert ist, und es immer wieder zu ziemlich ungewöhnlichen Steuerfällen bei Lieferungen ins Ausland und Verrechnung im Inland oder umgekehrt kommt, haben wir uns dazu durchgerungen, diese Definition an den Anwender zu delegieren.<br>
D.h. es kann im Lieferschein definiert werden, ob die Länderart anhand der oben beschriebenen Automatik, welche vor allem aus der Landdefinitionen kommt, bestimmt wird oder ob diese anders behandelt werden sollte.

Dazu ein kleines Beispiel fürs Verständnis:

Sie liefern als Österreichisches Unternehmen Waren nach Wien-Schwechat, welche dann nach Dubai weitergesandt werden. Sie senden dies aber im Auftrag eines österreichischen Unternehmens.
Abhängig davon wer den weiteren Transport durchführt, die Spedition in Ihrem Auftrag oder im Auftrag des Kunden, oder der Kunde selbst usw. ist das mal einen Inlandslieferung mit 20% Öst.MwSt und einmal eine Drittlandslieferung welche steuerfrei ist. Aus den Adressen der Rechnung und des Warenempfänger alleine kann dies nicht eindeutig ermittelt werden.<br>
D.h. in derartigen Fällen, haben Sie mit der übersteuerten Länderart die Möglichkeit korrigierend, je nach momentaner Ansicht des Finanzamtes, einzugreifen und die Umsatzsteuer entsprechend auszuweisen.

Info: In den Fällen, in denen es um die Entscheidung Buchung auf das Debitorenkonto der Rechnungs- oder der Lieferadresse geht, wird, bei der übersteuerten Länderart, der Länderartvergleich herangezogen. D.h. ist die, ermittelte Länderart der Rechnung gleich der übersteuerten Länderart des Lieferscheines, so greift das Debitorenkonto der Rechnungsadresse. 
Sind diese nicht gleich, greift das Debitorenkonto der Lieferadresse.

Bitte beachten Sie, dass bis auf obige Ausnahmen, die Automatik funktionieren sollte.

#### Kann die übersteuerte Länderart nachträglich umgeschaltet werden?
Da die übersteuerte Länderart in die Findung des richtigen Mehrwertsteuersatzes eingreift, kann diese nur solange geändert werden, solange im jeweiligen Beleg keine Positionen eingetragen sind.

#### Kann die übersteuerte Länderart vorbesetzt werden?
Ja. Es kann sowohl im Kunden, Reiter Konditionen, als auch im Auftrag, in den Kopfdaten, die für den Lieferschein gewünschte Länderart definiert werden.<br>
Bitte beachten Sie, dass für eine übersteuerte Länderart IMMER ein Lieferschein erforderlich ist.

#### Kann Drop Shipping abgebildet werden?
Ja. Bei Drop Shipping, also dem sogenannten [Streckengeschäft](https://de.wikipedia.org/wiki/Streckengeschäft) geht es im wesentlichen darum, dass ein (Zwischen-) Händler Ware bei Ihnen kauft und Sie diese direkt an den (End-)Kunden senden.<br>
Solange Händler und Endkunde im gleichen Land sind, ist dies eine ganz gewöhnliche Lieferung. <br>
Interessant wird es, wenn der Endkunde in einem anderen Land sitzt als der Händler. 
Hier sind insbesondere die Regeln für die Umsatzsteuer entsprechend zu beachten.

Wie hier vorzugehen ist, sei an folgenden Beispielen dargestellt:

Grundsätzlich: Die Darstellung wird immer aus Ihrer Sicht, also der des **Kieselstein ERP** Anwenders gemacht:
- a.) Händler ist im Inland, Kunde ist im Ausland<br>
![](Dropshipping_a.png)  
Das bedeutet, dass Sie die Ware mit Lieferschein an den Endkunden senden. Da aber die Rechnung an einen inländischen Kunden geht, muss die Rechnung mit der inländischen Mehrwertsteuer gelegt werden. Da üblicherweise bei der Rechnungslegung, wenn Lieferland und Rechnungsland nicht gleich sind, die Lieferadresse als Basis für die Mehrwertsteuer und für die Verbuchung der Erlöse herangezogen wird, muss hier die Länderart, im Lieferschein in den Kopfdaten, auf Inland gestellt werden. Damit wird erreicht, dass dies als Inlandserlös verbucht wird (da Sie das ja an Ihren Händler verkaufen).

- b.) Händler ist im Ausland, Kunde ist im Inland<br>
![](Dropshipping_b.png)  
Hier muss Ihr ausländischer Händler die Inlandsmehrwertsteuer tragen. Sollten Sie über diesen Händler viele Inlandslieferungen abwickeln, könnten Sie ihm empfehlen, dass er sich eine inländische UID Nummer, also eine Inlands-Steuernummer zulegt, damit kann er direkt die entsprechende Mehrwertsteuerabrechnung mit dem Inlandsfinanzamt machen.

#### Welche vorbereitenden Arbeiten sind für den Rechnungsexport erforderlich?
- a.) Stellen Sie sicher, dass alle Kunden des Zeitraums ein Debitorenkonto besitzen. Dies geht am leichtesten mit dem Warenausgangsjournal, nach Kunden sortiert. Achten Sie darauf, dass das Debitorenkonto mit Ihrem Finanzbuchhaltungsprogramm übereinstimmt.
- b.) Definieren Sie die Art des Exportes unter System, Parameter, FINANZ_EXPORT_VARIANTE entweder
    - Kostenstellen
    - Artikelgruppen

- c.) Definieren Sie das Exportformat unter System, Parameter, FINANZ_EXPORT_ZIELPROGRAMM
    - RZL
    - Datev-CSV
    - BMD / CSV

- d.) Stellen Sie sicher, dass alle Artikelgruppen ein Erlöskonto zugeordnet haben bzw. dass alle Kostenstellen ein Sachkonto zugeordnet haben.

#### Können auch die Belege mit exportiert werden?
Ja. Stellen Sie dazu den Parameter FINANZ_EXPORT_MIT_BELEGEN auf 1\. Es werden damit alle Dokumente die an einem Beleg hängen mit exportiert. Gibt es von einem Dokumente mehrere Versionen, so wird immer nur die letzte Version exportiert.

Die Dokumente werden in den Pfad der Exportdatei kopiert.

Sind dort bereits Dokumente mit dem gleichen Namen vorhanden, so werden diese ohne Rückfrage überschrieben.

Sind in der Dokumentenablage mehrere gleichlautende Dokumente eingetragen, so bleibt im Endeffekt immer nur das letzte Dokument bestehen, da die Vorgänger automatisch überschrieben werden.

Die Belege werden erst nach der Erzeugung der Exportdatei aus der Dokumentenablage in das Filesystem übertragen.

Dokumente die im Jasper Format vorliegen, z.B. Ausgangsrechnungen, werden in PDF Dokumente konvertiert. Es werden diese Dokumente 1:1 exportiert. D.h. sind bei Ihren Drucken keine Logo-Informationen mit angegeben, so sind auch in den PDF Dateien keine Logos enthalten.

Achten Sie bitte bei der Übertragung der Daten darauf, dass danach der Inhalt des Ordners gelöscht wird.

#### Welche Export Formate werden unterstützt?
Siehe dazu bitte [Exportformate]( {{<relref "/management/finanzbuchhaltung/fibu_export/formate">}} ).

Start des Exportlaufes

Mit Neu, oder Finanzbuchhaltung, Export starten Sie einen neuen Exportlauf.

Fehlermeldung

![](Finanzamt_Nicht_definiert.jpg)

Wenn beim Start des Exportlaufes diese Fehlermeldung angezeigt wird, so wurde für den verwendeten Mandanten das Finanzamt nicht vollständig definiert. Wenden dich in diesem Falle bitte an deinen **Kieselstein ERP** Betreuer damit er die Definition richtig stellt.

**Hinweis zur Richtigstellung:**
Erscheint diese Meldung, so ist das Finanzamt in der Fibu noch zu definieren. Hat der Anwender das Fibu Modul nicht, so kann er dies nicht selbst durchführen. Das Einfachste scheint mir zu sein, dass kurz das FibuModul eingeschaltet wird, das Finanzamt eingepflegt wird und dann die Funktionalität wieder abgeschaltet wird. 

**Hinweis:** Es werden derzeit nur zwei Finanzämter im EU-Raum akzeptiert.

Fehlermeldung

![](Fehler1.gif)

Bedeutet:

In den Sachkonten ist das Konto auf das die Ust/Vst gebucht werden soll, nicht definiert.

Siehe Fibu, Sachkonten, Kopfdaten.

In Kostenstellen-Definitionen nur Erlöskonten hinterlegen.

Wenn:

![](Konto_Laenderart_uebersetzung.gif)

Bedeutet, dass für ein Erlöskonto keine Länderartübersetzung definiert ist.

Hintergrund: Die Erlöse werden nach Kostenstellen auf die Konten gebucht.

Pro Kostenstelle kann ein Erlöskonto angegeben werden. Für die zusätzlichen Länderarten müssen daher Übersetzungen für z.B. Drittland, IG-Erlös usw. angegeben werden.

Im Modul Finanzbuchhaltung finden Sie die Sachkonten. Wählen Sie das entsprechende Konto aus (in unserem Fall 4020) und wählen Sie ![](Konto_Laenderart.gif). Nun definieren Sie mit Neu die jeweiligen Kontoübersetzungen für die unterschiedlichen Länder. 

Sachkontenübersetzung: Aus Kontoauswahlliste Konto auswählen und dazu die zu übersetzenden Länderarten angeben.

Gilt nur für die Erlöskonten, d.h. die Konten der AR/GS

Länderartübersetzung nicht / falsch definiert

Konto-Länderart-Übersetzung nicht definiert. Leider ein sehr häufiger Fehler.

![](Fibu_Export_Artikelgruppenzuordnung_falsch_definiert.jpg)

Hintergrund:

Durch den üblicherweise verwendeten Export der Ausgangsrechnungen anhand der Erlöskonten der Artikelgruppen, kommt es leider immer wieder zu Fehlbedienungen.

**Wichtig:** Wenn Artikel definiert werden, so kann aufgrund des Artikels nicht definiert werden, in welches Land der Artikel verkauft wird. Erst wenn der Artikel mit einer Rechnung verbunden ist, wurde damit das Zielland festgelegt.

Das bedeutet, es muss für die Erlöskonten der Artikelgruppen **<u>immer</u>** das Inlandskonto angegeben werden.

Auf dem Inlandskonto ist dann die [Länderartübersetzung](#Übersetzung_Erlöskonten) entsprechend zu hinterlegen.

**Hinweis:**

Da während des Exports immer wieder Warnungen und Hinweise auftreten, empfiehlt es sich, für den Exportlauf parallel einen zweiten **Kieselstein ERP** Client zu starten.

So können Sie den Hinweis im einen Clientfenster stehen lassen und im zweiten Client die Ursache korrigieren.

![](Export2.gif)

Bedeutet, dass die Erlöskontierung nicht durchgeführt werden konnte, da der Kostenstelle kein Erlöskonto zugeordnet ist.

Zur Definition des Erlöskontos wechseln Sie bitte in die Systemparameter, Mandant, Kostenstelle und definieren Sie das Erlöskonto für diese Kostenstelle.

Siehe dazu auch Arten der Erlöskontofindung.

![](Beleg_nicht_aktiviert.gif)

So aktivieren / drucken Sie die Rechnung bitte aus, oder stornieren Sie diese.

![](Export3.jpg)

Dem Kunden der Rechnung ist keine Postleitzahl für den Ort zugeordnet. Bitte korrigieren Sie die Anschrift der Rechnung.

Bitte beachten Sie dabei, dass nur die Definition der Postleitzahl unter Postfach nicht ausreichend ist. Für den Export muss unbedingt auch der Ort mit Länderkennzahl angegeben werden.

![](LKZ_Postfach_ist_zuwenig.gif)

Gegebenenfalls verwenden Sie die gleiche Postleitzahl für den Ort wie für die Postleitzahl des Postfaches.

Export der Debitoren- bzw. Kreditorenkonten über Journal

Die Pfade der Exportdateien können in den Mandantenparametern definiert werden.

FINANZ_EXPORTZIEL_DEBITORENKONTEN

FINANZ_EXPORTZIEL_KREDITORENKONTEN

**Empfehlung:**

Um Festzustellen, für welche Debitoren oder Kreditoren deren Kontonummern noch definiert werden müssen, empfiehlt es sich, vor dem Exportlauf ein Journal der Ausgangs- bzw. Eingangsrechnungen nach Kunden / Lieferanten sortiert auszudrucken. Darauf werden auch die Debitoren / Kreditorennummern angeführt. Bitte tragen Sie bei den Kunden / Lieferanten mit fehlenden Nummern diese im Kunden bzw. Lieferantenstamm nach.

#### Wo werden die Daten abgelegt?
Der Pfad und der Dateiname der exportierten Daten kann in System, Parameter, Parameter Mandant eingestellt werden.

Es können alle fünf Exportdateien getrennt eingestellt werden.

FINANZ_EXPORTZIEL_EINGANGSRECHNUNG

FINANZ_EXPORTZIEL_GUTSCHRIFT

FINANZ_EXPORTZIEL_RECHNUNG

Bitte beachten Sie, dass die Exportpfad für den exportierenden Client gelten.

Für den Fall, dass die Exportdatei nicht geschrieben werden kann erscheint eine entsprechende Fehlermeldung

![](Fibu_Export1.gif)

Stellen Sie gegebenenfalls mit Betriebssystemmitteln die Verbindung zu diesem Laufwerk her, oder brechen Sie den Exportlauf ab. Korrigieren Sie den Pfad und starten Sie danach den Lauf erneut.

Jedes Finanzbuchhaltungsprogramm verlangt seine eigenen Dateierweiterungen. Siehe bitte die Beschreibungen dort.

Einstellung der Exportpfade für MAC Benutzer

Für MAC Benutzer müssen die Exportpfad gemäß Linux Konventionen eingestellt werden. Um diese z.B. am Schreibtisch des Benutzers abzulegen, stellen Sie den Pfad wie folgt ein:

![](ExportPfad_MAC1.gif)

Der Aufbau dieses Pfades besteht aus vier Teilen:
1. Users ... es sollten die Daten Benutzerspezifisch abgelegt werden
2. Benutzername ... Der Name des Benutzers. Diesen finden Sie am einfachsten über das Festplattensymbol, ![](ExportPfad_MAC2.jpg) und dann ![](ExportPfad_MAC3.jpg) finden Sie den Benutzernamen unter dem Schreibtisch.
3. Damit die Datei auf Ihrem Schreibtisch abgelegt wird, geben Sie Desktop an, gefolgt vom eigentlichen Exportnamen.

Wichtig: Achten Sie bitte auf die Vorwärts-Schrägstriche und achten Sie unbedingt auf die Groß-Kleinschreibung.

Eine alternative Möglichkeit ist folgende Vorgehensweise:

![](Export1.jpg)

Starten Sie bitte das Terminal(Fenster)
geben Sie nun bitte: cd Desktop
ein. Bitte auf die Groß-Klein-Schreibung achten
Geben Sie nun bitte: pwd
ein (PWD = Print Working Directory)
Nun sehen Sie genau den Pfad zum Desktop.
Z.B: /Users/wernerhehenwarter/Desktop

![](Export2.jpg)

#### Mit welchem Zeichensatz wird exportiert?
Üblicherweise werden die Dateien im Zeichensatz Windows-ANSI exportiert. Manchen Programme verlangen noch den ASCII Zeichensatz. Dafür stellen Sie bitten in den Parametern den Parameter FIBU_EXPORT_ASCII auf 1.

#### Wo kann ich die Personenkonten exportieren
Die Personenkonten, also Debitoren und Kreditoren können im Modul FiBu unter Journal exportiert werden.<br>
Auch für diesen Export können die Exportpfade in System, Parameter, Parameter Mandant eingestellt werden.<br>
Es können die Pfade für beiden Personenkonten und die Sachkonten getrennt eingestellt werden.
- FINANZ_EXPORTZIEL_DEBITORENKONTEN
- FINANZ_EXPORTZIEL_KREDITORENKONTEN
- FINANZ_EXPORTZIEL_SACHKONTEN

Bitte beachten Sie, dass die Exportpfade für den exportierenden Client gelten.

Dieser Export ist aktuell für BMD implementiert.

#### Kann eine exportierte Rechnung trotzdem geändert werden?
Nein: Eine als exportiert gekennzeichnet Rechnung kann nicht geändert werden. Sie können jedoch den letzten Exportlauf oder eine einzelne Rechnung aus einem Exportlauf zurücknehmen. Dadurch ist die Rechnung nicht mehr als in die FiBu übernommen gekennzeichnet und kann daher geändert werden. 

**Hinweis:** Es ist dafür das Chef Buchhalter Recht (FB_CHEFBUCHHALTER) erforderlich.

Bitte denken Sie daran, danach den Export wieder zu starten.

Die durch die Änderungen eventuell auch in Ihrer FiBu erforderlichen Umbuchungen müssen von Ihnen manuell nachvollzogen werden.

#### Die Sachkonten sind in meinem Finanzbuchhaltungsprogramm zwar grundsätzlich vierstellig, aber die Nuller-Klasse ist kürzer. Wie kann ich dies in **Kieselstein ERP** definieren?
Bitte definieren Sie in **Kieselstein ERP** die Sachkonten ebenfalls vierstellig. Für die Konten der Nuller-Klasse müssen bei der Definition der Sachkonto-Nummern führende Nullen angegeben werden. Diese werden auch in die Exportdateien übergeben. Laut Auskunft von RZL werden diese beim Import unterdrückt, sodass dies wiederum auf die richtigen Konten gebucht wird.

#### Wie wird beim Erzeugen der Exportdatei vorgegangen?
Beim Export der FiBu Daten kann es vorkommen, dass die Exportdatei bereits existiert. Sie erhalten in diesem Falle eine entsprechende Warnung. Mit dieser Warnung können Sie auswählen, ob die bestehende Exportdatei überschrieben werden soll, oder ob Sie den Fibu-Export abbrechen. Wird der Export abgebrochen, so wird der Exportlauf wieder rückgängig gemacht, als wie wenn die z.B. Rechnungen nicht exportiert worden wären.

Bevor Sie überschreiben wählen, müssen Sie sicherstellen, dass die Daten bereits in Ihre Finanzbuchhaltung importiert sind.

Sollte die Datei versehentlich gelöscht / überschrieben werden, so können die Daten nur in der Form wieder hergestellt werden, dass Sie alle FiBu-Export-Läufe bis inklusive diesem Lauf aus **Kieselstein ERP** löschen und danach, für das jeweilige Monat, neu erzeugen.

#### Welcher Zeitraum wird exportiert?
<a name="Welcher_Zeitraum_wird_Exportiert"></a>
Beim Export der FiBu-Daten erscheint die Frage nach dem (Export-) Stichtag.

![](Fibu_Export.gif)

Weiters haben Sie zwei Optionen, mit denen die Behandlung der außerhalb des Exportzeitraumes liegenden Belege definiert werden kann.

Grundsätzlich werden die Bewegungsdaten (RE, GS, RE) nur für das Monat bis zum eingestellten Stichtag exportiert. In unserem Beispiel also die Daten vom 1.5.2006 bis zum 31.5.2006.

Wurde nun ![](FibuExport_Opt1.gif) angehakt, so werden alle noch nicht exportierten Belege ebenfalls mitexportiert.

Wurde zusätzlich ![](FibuExportOpt2.gif) angehakt, so werden diese Belege in **Kieselstein ERP** als Exportiert, also in FiBu befindlich, gekennzeichnet, aber **<u>NICHT</u>** in die Exportdatei mit aufgenommen.

Dies ist manchmal erforderlich, wenn Rechnungen auch in alten Buchungszeiträumen nachgetragen werden mussten und diese nicht mehr in die FiBu übernommen werden sollten.

**HINWEIS:** Beachten Sie bitte, dass ein Großteil der Fremdbuchhaltungen Periodenreinheit bei der Datenübergabe verlangen. D.h. es dürfen jeweils nur die Daten eines Buchungsmonates exportiert werden. Das bedeutet für Sie, dass keine Daten aus alten Buchungszeiträumen mit übergeben werden sollen. D.h. es muss der Haken bei ![](FibuExport_Opt1.gif) entfernt werden.

Beim Stichtag wird automatisch der Letzte des Vormonates vorgeschlagen. Wird hier der Erste eines Monates eingetragen, so werden nur die Daten für diesen einen Tag exportiert, wenn diese Daten nicht schon exportiert waren.

#### Wie sollte beim ersten Export vorgegangen werden?
Bei der ersten Datenübernahme ist es meistens so, dass bereits Daten aus der Rechnungsverwaltung manuell in die Finanzbuchhaltung eingetragen wurden. Damit diese Daten nicht doppelt übernommen werden, sollte der erste Exportlauf so eingestellt werden, dass die Rechnungen (für Gutschriften und Eingangsrechnungen bitte analog vorgehen) bis vor dem gewünschten Exportzeitraum im **Kieselstein ERP** aus exportiert gekennzeichnet werden. Hier sollte "Auch Belege außerhalb des Gültigkeitszeitraumes exportieren" angehakt sein. Danach starten Sie einen zweiten Exportlauf, der dann das Enddatum in der gewünschten Monats-Periode hat. Üblicherweise muss der Haken bei "Auch Belege außerhalb des Gültigkeitszeitraumes exportieren" entfernt werden. Beim Erzeugen der Datei muss die Warnung, dass die Exportdatei bereits existiert so beantworten, dass die Datei überschrieben wird.

#### Abweichendes Umsatzsteuerland?
<a name="Abweichendes Ust-Land"></a>
Bei manchen Lieferanten / Kunden ist es so, dass diese eine zusätzliche Umsatzsteuernummer in anderen Ländern haben.

Ein Beispiel von Österreich aus gesehen: Wir bestellen Bücher bei Amazon.de.

Der Lieferant Amazon hat eine deutsche Adresse, aber eine österreichische Umsatzsteuernummer. D.h. die deutsche Eingangsrechnung ist mit der österreichischen Steuernummer versehen. D.h. laut Umsatzsteuerrecht ist diese Lieferung wie eine Inlandslieferung zu behandeln.

Um dies abzubilden, kann im Kunden und im Lieferanten unter Konditionen, neben der Debitoren-/ Kreditorennummer ein abweichendes Umsatzsteuerland angegeben werden.

Die Erfassung des abweichenden Umsatzsteuerlandes steht nur zur Verfügung, wenn mehrere Finanzämter definiert sind.

**Hinweis:**

Die Definition des abweichenden Umsatzsteuerlandes gilt für den Partner. D.h. wird immer für beide Module Kunde und Lieferant gemeinsam definiert. Sollten der seltene Fall auftreten, dass der Kunde z.B. Umsatzsteuerland Deutschland hat und der Lieferant, des gleichen Partners, Umsatzsteuerland Österreich, so müssen zwei Partner definiert werden.

**Hinweis2:**

Definieren Sie ein abweichendes Umsatzsteuerland nur, wenn auch tatsächlich die Versteuerung in einem anderen Land und in einem anderem Finanzamt erfolgt. Wird z.B. bei einem Kunden, der in einem Drittland ist, als abweichendes Umsatzsteuerland das eigene Land angegeben, so wird dieser Kunde wie Inland behandelt, was in aller Regel falsch ist, aber Ihrer Definition entspricht.

{{% alert title="Hinweis" color="info" %}}
Mit integrierter Finanzbuchhaltung wird hier das Finanzamt des Debitorenen / Kreditorenkontos angezeigt. Eine abweichende Definition zum Personenkonto ist nicht zulässig.<br>
Ohne integrierter Finanzbuchhaltung kann hier das Land passend zum Finanzamt angegeben werden.
{{% /alert %}}

Bitte gegebenenfalls auch das unter [EU-OSS]( {{<relref "/management/finanzbuchhaltung/eu_oss/"  >}} ) beschriebene beachten.

#### Einrichten weiterer Finanzämter
Wenn Sie auch das vollständige Modul Finanzbuchhaltung besitzen, so kann der untere [Modulreiter Finanzamt]( {{<relref "/management/finanzbuchhaltung/integriert/#definition-eines-finanzamtes" >}} ) ausgewählt werden. Definieren Sie hier die weiteren für Sie wesentlichen Finanzämter. Wenn Sie dieses Modul (den Reiter) nicht besitzen, bitten Sie gegebenenfalls Ihren **Kieselstein ERP** Betreuer dies für Sie einzurichten.

#### Wieso werden EU Länder als Drittland exportiert?
Die Definition ob ein Land als EU-Mitglied zu bewerten ist muss im System, Land definiert werden, seit wann dieses Land EU-Mitglied ist. Ist das Land kein EU-Mitglied, oder wurde die Rechnung **vor** der EU-Mitgliedschaft ausgestellt, so wird der Export als Drittland durchgeführt. Erst ab dem Zeitpunkt der (eingetragenen) EU-Mitgliedschaft wird das als Innergemeinschaftliche Lieferung behandelt.

[Bedienung / Eigenheiten der unterschiedlichen Finanzbuchhaltungsprogramme beim Importieren der Daten]( {{<relref "/management/finanzbuchhaltung/fibu_export/bedienung_fremde_programme">}} )

Anmerkung zu IG-Lieferung und IG-Leistung:

Beim Export der Rechnungen, Gutschriften  und Eingangsrechnungen wird nicht zwischen IG-Lieferung und IG-Leistung unterschieden. Es wird immer IG-Lieferung übergeben.

#### Einzelne Rechnungen werden nicht exportiert?
Manche Anwender erstellen sogenannte Null-Rechnungen, wenn z.B. Bemusterungen, Garantieleistungen o.ä. erbracht wird. Null-Rechnungen können von Buchhaltungsprogrammen in aller Regel nicht verarbeitet werden und werden daher auch nicht exportiert. [Siehe dazu auch](#Null-Rechnungen).

Löschen einzelner Rechnungen aus dem Export.

Manchmal ergibt sich die Notwendigkeit, dass einzelne Belege verändert werden müssen.

Daher können einzelne Belege auch aus älteren Exportläufen herausgelöscht werden.

WICHTIG: Da Sie hier Änderungen an bereits in die Finanzbuchhaltung übernommenen Daten vornehmen, liegt es in Ihrer Verantwortung die Daten konsistent zu halten. Zusätzlich ist für den Benutzer das Chefbuchhalterrecht erforderlich.

Für das Herauslöschen bestätigen Sie bitte folgende Meldung

![](Fibu_Export_Loeschen.gif)

Wird danach

![](Fibu_Export_Loeschen_Kein_Recht.gif)

angezeigt, so bitten Sie Ihren **Kieselstein ERP** Administrator Ihnen das ![](Fibu_Export_Chefbuchhalterrecht.gif) Recht zu erteilen.

Wichtig: Beachten Sie, dass Ihre Rechnung / Ihr Beleg je nach Einstellung des [Exportzeitraumes](#Welcher_Zeitraum_wird_Exportiert) beim nächsten Export wieder mitexportiert werden kann.

Kostenstellen

Um die Kostenstellen auch in die verschiedenen Finanzbuchhaltungsprogramme exportieren zu können, ist es erforderlich, dass die Kennung der Kostenstellen numerisch sind. (Gilt zumindest für RZL und BMD).

#### Automatische Findung der Erlöskonten
<a name="Übersetzung_Erlöskonten"></a>
Für die automatische Findung der Erlöskonten werden derzeit zwei verschiedene Arten unterstützt.
- a.) Erlöskonten anhand der Kostenstelle<br>
Hier werden für jede Kostenstelle die zugehörigen Erlöskonten definiert. Alle Rechnungen die auf die entsprechende Kostenstelle lauten erhalten diese Kontierung. Zusätzlich kann hier mittels der Kontierung im Rechnungsmodul die Kontierung dieser Rechnung übersteuert werden, was auch für Splittbuchungen unterstützt wird.
- b.) Erlöskonten anhand der Artikelgruppe<br>
Hier werden jeder Artikelgruppe das gewünschte Erlöskonto hinterlegt.

Bitte beachten Sie bei beiden Definitionen, dass für die Definition jeweils nur die Inlandskonten hinterlegt werden. Die Übersetzung der Konten auf die verschiedenen Länderarten erfolgt aufgrund der Lieferadresse und die in der Länderart Übersetzung definierten Konten.

Übersetzung der Erlöskonten nach Länderart

Für jedes Konto, das in der automatischen Erlöskonto Findung verwendet wird, muss für die benötigten Lieferländer eine entsprechende Übersetzung, passend zu Ihrem Kontenplan hinterlegt werden.

Gehen Sie dazu auf das Sachkonto und geben Sie je nach Länderart (EU-Ausland mit Uid-Nummer, EU-Ausland ohne Uid-Nummer, Drittland) das entsprechend richtige Konto an. Das Übersetzungskonto muss vorher bereits unter Sachkonten definiert worden sein.

Übersetzung der Erlöskonten nach Land

Da in manchen Unternehmen eine detailliertere Erlöskontenaufgliederung gewünscht wird, steht zusätzlich zur Länderartübersetzung auch eine Übersetzung des Inlandskontos auf das jeweilige Land des Rechnungsempfängers zur Verfügung. Bitte beachten Sie, dass Sie die Übersetzung nach den steuerlichen Gesichtspunkten durchführen. Von **Kieselstein ERP** wird keine Überprüfung vorgenommen.

Export nach Kostenstellen

Hier wird die automatische Findung der Erlöskonten anhand der der Rechnung zugeordneten Kostenstellen und den bei der jeweiligen Kostenstelle definiertem Erlöskonto vorgenommen.

Export nach Artikelgruppen

Hier werden die Erlöskonten anhand der Artikelgruppen der Rechnungen inkl. Lieferscheine ermittelt.

Bitte definieren Sie das Artikelgruppen Defaultkonto (FINANZ_EXPORT_ARTIKELGRUPPEN_DEFAULT_KONTO_AR) in den Systemparametern. Es empfiehlt sich, für dieses Konto auch alle Länder- / bzw. Länderartübersetzungen zu definieren.

Für die Ermittlung des Erlöskontos eines Artikels wird wie folgt vorgegangen.

Artikelgruppe des Artikels. Hinter dieser Artikelgruppe muss das Inlandserlöskonto definiert sein. Für dieses Erlöskonto muss es gegebenenfalls wiederum Länder / Länderartübersetzungen geben.

Erscheint die Fehlermeldung

![](Export_Fehler_Artikeluebersetzung.jpg)

so bedeutet dies, dass für die Übersetzung der Artikelgruppe keine Länderart Übersetzung bzw. keine Landübersetzung definiert. ist. Tragen Sie dies in der Definition des Sachkontos nach.

Achten Sie darauf, dass in einer Artikelgruppe nur Artikel der gleichen MwSt Gruppe (genauer der Umsatzsteuer) sind. Werden beim Export Einträge vorgefunden, welche für ein Erlöskonto mehrere MwSt-Steuersätze definiert haben, so wird der Export abgebrochen. (Fehlermeldung kein eindeutiger MwSt Code gefunden)

#### Können die allgemeinen Rabatte in ein eigenes Konto exportiert werden?
Ja. Hinterlegen Sie dazu unter System, Parameter, FINANZ_RABATT_KONTO das gewünschte Rabatt-Konto ("Aufwandskonto"), z.B. für die Verbuchung der Ihren Zwischenhändlern gewährten Werbekostenzuschüssen.

Ist in diesem Parameter ein Konto eingetragen, so werden an die Erlöskonten die Beträge ohne Berücksichtigung des allgemeinen Rabattes übergeben. An das definierte Rabatt-Konto wird der Rabatt übergeben. Eventuelle Rundungsdifferenzen werden im Rabatt-Konto berücksichtig.

#### Beim Export kommt eine oben nicht beschriebene Fehlermeldung?
Bitte sehen Sie im Detail nach.

Steht hier: "Konto .... für Allgemeinen Rabatt ist nicht vorhanden"

so ist in den Parametern ein Konto für den allgemeinen Rabatt definiert, aber dieses ist in den Sachkonten nicht eingetragen.

Stellen Sie die Definition richtig.

#### Es werden keine Belegnummern exportiert
Hier ist die Ursache in aller Regel, dass die Trennzeichen für die Belegnummern verändert wurden. Stellen Sie sicher, dass das in den Systemparametern definierte Trennzeichen und das in den Belegen, z.B. Eingangsrechnung, verwendete Trennzeichen das gleiche ist.

#### Wo definiert man, dass Anzahlungsrechnungen nicht exportiert werden?
Im Modul System, unterer Reiter Parameter finden Sie den Parameter FINANZ_EXPORT_ANZAHLUNGSRECHNUNG. Wenn dieser auf 1 gestellt ist, so sind Anzahlungsrechnungen im Export enthalten. Stellen Sie den Parameter auf 0, somit werden Anzahlungsrechnungen nicht mehr exportiert.

#### Was ist für den Export von Anzahlungsrechnungen zu beachten?
Vorneweg: [Bitte beachten Sie](Integrierte_Finanzbuchhaltung.htm#Anzahlung-Schlussrechnung).
Sollte beim Export der Eingangsrechnungen eine Fehlermeldung auftreten und in den Details des Fehlers steht

Anzahlung kann nicht mehrfachkontiert sein! ER 20/0001...

so bedeutet dies eventuell, dass für die Eingangsrechnung zwar ein Anzahlungskonto angegeben wurde, dieses aber nicht als Anzahlungskonto definiert ist, bzw. dass kein Sachkonto für die Anzahlung definiert ist.

D.h. es muss bitte im Modul Finanzamt, Buchungsparameter
-   für die Ausgangsrechnungen das Konto für gegebene Anzahlungen
-   für die Eingangsrechnungen das Konto für erhaltene Anzahlungen

definiert sein.<br>
Beachten Sie auch hier, dass dies die Inlandskonten sind. Für Drittland bzw. IG/EU ist die entsprechende Länderartübersetzung bei den jeweiligen Sachkonten zu hinterlegen.<br>
Zusätzlich sollte beachtet werden, dass für die Eingangsrechnung(sanzahlung), die Auswahl des Kontos deaktiviert ist. Ist nun im Finanzamt unter Gegebene Anzahlung kein Konto definiert, wird auch kein Konto vorgeschlagen. Zugleich erscheint damit Mehrfach Splittbuchung. D.h. in diesem Falle tragen Sie bitte im Modul Finanzbuchhaltung, unterer Reiter Finanzamt, oberer Reiter Buchungsparameter unter Gegebene Anzahlungen das entsprechende Sachkonto an. Für Österreich könnte dies z.B. 3350 sein.

#### Wie können Fremdwährungen mitexportiert werden?
Für das Programm BMD ist ein Export mit Fremdwährungen in ER & AR möglich. Hierzu stellen Sie den Parameter FINANZ_EXPORT_ZIELPROGRAMM auf BMD-NTCS ein.

Um Fremdwährungen zu exportieren muss die Vorlage fb_export_bmd_FW.xsl auf fb_export_bmd.xsl umbenannt werden - wenden Sie sich dazu an Ihren **Kieselstein ERP** Betreuer.

Die Vorlage fb_export_bmd_FWalle befüllt die Fremdwährungsfehler immer auch bei EUR (mit Kurs = 1\.  ACHTUNG: Der Import von BMD NTCS ist nicht mit BMD 5.5 kompatibel.

### Import der offenen Postenliste
<a name="Import_OP-Liste"></a>
============================================================

Um einen runden Ablauf zu ermöglichen steht neben der Übergabe der Daten an die fremde Finanzbuchhaltung auch die Möglichkeit des Imports der offenen Postenliste aus der jeweiligen Finanzbuchhaltung zur Verfügung.

Grundsätzlich finden Sie den Import im Modul Finanzbuchhaltung, unterer Modulreiter Export, nun Menüpunkt Export, Import Offene Posten ![](Fibu_OP_Import1.jpg) zur Verfügung.

Durch Klick auf Import Offene Posten erscheint der Dateiauswahldialog. Wählen Sie nun die von Ihrer Finanzbuchhaltung exportierte Datei aus.

Beim Import der offenen Postenliste werden die bereits exportierten (Ausgangs-) Rechnungen, welche nicht mehr in der offenen Postenliste enthalten sind, als vollständig bezahlt gebucht. Werden Rechnungen in der offenen Postenliste aufgeführt, so wird der Differenzbetrag zwischen Rechnungs-Brutto-Wert und dem erhaltenen Saldo-Betrag als bezahlt gebucht.

Wichtig: Es werden nur die "Rechnungen" als noch offen übernommen, welche durch den Import auch erkannt werden. Fehlerhafte Datensätze werden beim Import übersprungen.

Am Ende des Imports erhalten Sie eine Liste der Datensätze, welche ignoriert wurden.

![](Fibu_OP_Import2.jpg)

Dieses Fenster signalisiert dass:
- a.) ein offener Betrag auf der Rechnung 08/0000146 in der OP-Liste enthalten ist, die Rechnung aber storniert ist.
- b.) Der in der Fibu verbuchte Saldo mit dem offenen Rechnungswert einen negativen Zahlbetrag ergeben würde.
- c.) Die Rechnung 08/0000012 hat in der OP-Liste einen Wert, ist aber mit Null-Werten in **Kieselstein ERP** eingetragen.

Bitte korrigieren Sie unbedingt Ihre Daten, entweder in der Buchhaltung oder im **Kieselstein ERP**.

- d.) Eine Menge an Belegen in der offenen Postenliste, die nicht mit **Kieselstein ERP** zusammenstimmen. Da in den offenen Postenlisten immer weitere Buchungen enthalten sein können, wurde die Schnittstelle so aufgebaut, dass jede für **Kieselstein ERP** bestimmte Rechnung (ein) führende(s) Zeichen enthalten muss. Dieses muss im System unter Parameter definiert werden. Alle Buchungen die diese Zeichen nicht enthalten, werden ignoriert und in dem Fenster als Warnung ausgegeben. Hier bedeutet z.B. 951048(1), dass die Belegnummer 951048, welche aus der Zeile Nr. 1 der Importdatei stammt nicht den Regeln entspricht und daher übersprungen wurde.<br>
Bitte beachten Sie, dass die offenen Posten derzeit nur für die Ausgangsrechnungen zur Verfügung stehen. D.h. Gutschriften werden derzeit nicht berücksichtigt.

### NULL-Rechnungen
**<a name="Null-Rechnungen"></a>
Wichtig:** Rechnungen mit einem Wert von 0 (Null) können in den meisten Finanzbuchhaltungsprogrammen nicht verarbeitet werden. Zusätzlich können diese auch vom offenen Posten Import nicht behandelt werden. Sie erhalten daher auch diese Rechnungen in der Liste der fehlerhaften Rechnungen angeführt. Diese Rechnungen müssen per Hand auf erledigt gesetzt werden.

**Tipp:** Anstelle Null-Rechnungen auszustellen, verwenden Sie die Lieferscheine und erledigen Sie diese [manuell](../Lieferschein/index.htm#Manuell Erledigen).
( {{<relref "/docs/stammdaten/system/#anlegen-von-l%c3%a4ndern" >}} )

Wichtig: Um die offene Postenliste richtig von der Fremd-Fibu nach **Kieselstein ERP** zu übertragen ist wichtig, dass möglichst folgender Ablauf eingehalten wird:
1.  Export der Daten aus **Kieselstein ERP** in eine Datei
2.  Import der **Kieselstein ERP** Daten in die Finanzbuchhaltung
3.  Aufbuchen der Bank
4.  Export der offenen Postenliste aus der Finanzbuchhaltung in eine Datei
5.  Import der offenen Postenliste Datei nach **Kieselstein ERP**

Wichtig ist insbesondere, dass zwischen dem Export der offenen Postenliste (4) und dem Import (5) kein Export (1) der Fibu-Daten gemacht wird. Dies würde zur falsch Interpretation der in der offenen Postenliste nicht vorhandenen Rechnungen führen.

Datenaufbau für den Import der offenen Postenliste:

Für den Import gehen wir von folgendem Datenaufbau aus:

CSV-Format, d.h. der Datensatztrenner ist das Semikolon (;)

Kontonummer;Rechnungs-Nr;Rechnungsdatum;Fälligkeit;Gegenkonto;Betrag Soll;Betrag Haben;Saldo

Nach dem Saldo können noch weitere Daten folgen. Diese Daten werden ignoriert.

Die Rechnungsnummer muss im **Kieselstein ERP** enthalten sein, beachten Sie dazu den Parameter FINANZ_OP_PREFIX_RECHNUNG bzw. FINANZ_OP_PREFIX_GUTSCHRIFT für die Gutschriften.

Es werden nur die Felder Rechnungs-Nr und Saldo ausgewertet. Werden zu einer Rechnung mehrere Einträge gefunden, so wird nur der erste in der Reihenfolge der Daten verwendet. Der Dezimaltrenner für den Saldo ist der Punkt (.).

Zurzeit steht der offene Posten-Import für folgende Programme zur Verfügung:
- a.) [Datev](Export_Bedienung_Programme.htm#OP-Liste_Datev)
