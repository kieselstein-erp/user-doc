---
categories: ["Finanzbuchhaltung"]
tags: ["Finanzbuchhaltung", "integrierte Finanzbuchhaltung"]
title: "Finanzbuchhaltung"
linkTitle: "Finanzbuchhaltung"
weight: 10
date: 2023-01-31
description: >
 Die Module der integrierten und nicht integrierten Finanzbuchhaltung
---
Beschreibung der Finanzbuchhaltung

Wichtige Trennung:
- Integrierte Finanzbuchhaltung
- ohne integrierter Finanzbuchhaltung, trotzdem eine umfassende Übersicht über die Unternehmensentwicklung


Finanzbuchhaltung
=================

[Integrierte Finanzbuchhaltung siehe:]( {{<relref "/management/finanzbuchhaltung/integriert">}} )<br>
[Zahlungen mit SEPA importieren / exportieren siehe:]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag#sepa-import--export" >}} )<br>
[Besondere Zusatzinfos siehe:]( {{<relref "/management/finanzbuchhaltung/zusatzinfos">}} )

#### Welches Finanzamt sollte verwendet werden?
Sie sollten vor dem Start, vor allem der integrierten Finanzbuchhaltung, das ausgelieferte Finanzamt auf das tatsächlich für Sie zuständige Finanzamt ändern.
Bitte beachten Sie, dass ein weiteres Finanzamt nur dann erforderlich ist, wenn Sie in einem anderen Land eine entsprechende Steuernummer / UID Nummer haben.

#### Bilanzierer oder Einnahmen/Ausgaben Rechner ?
Dies ist die wichtigste Entscheidung für Ihr Unternehmen.
Da es in der Verwendung von **Kieselstein ERP** grundsätzlich um transparente, nachvollziehbare Darstellung aller Werteflüsse geht, sind Export der Rechnungsdaten bzw. integrierte Finanzbuchhaltung ausschließlich für sogenannte Bilanzierer gedacht. 

Der wesentliche Unterschied ist, dass beim Bilanzierer die Umsatzsteuerverprobung und die Gewinn Ermittlung nach den gelegten Rechnungen erfolgt, bei Einnahmen-Ausgabenrechner erfolgt diese nach den vereinnahmten Zahlungen.

Die Verwendung von **Kieselstein ERP** für Einnahmen/Ausgaben Rechner = Ist-Versteuerer [siehe]( {{<relref "/management/finanzbuchhaltung/einnahmen_ausgaben_rechner">}} ).

Alle anderen Beschreibungen gehen davon aus, dass Sie **Kieselstein ERP** als Bilanzierer = Soll-Versteuerer verwenden.

Für weitere Erklärungen zu diesem Bereich wenden Sie sich bitte an Ihren Steuerberater, Wirtschaftstreuhänder.

#### Welche grundsätzlichen Kontenbereiche gibt es in der **Kieselstein ERP** Finanzbuchhaltung
Es gibt grundsätzlich zwei (eigentlich drei) Bereiche der Konten.
- Sachkonten und Personenkonten.

Die Personenkonten gliedern sich wiederum in Debitorenkonten (Kunden) und Kreditorenkonten (Lieferanten). Siehe dazu auch [Wozu Personenkonten]( {{<relref "/management/finanzbuchhaltung/integriert/einnahmen_ausgaben_rechner/#wozu-nun-debitoren--kreditoren-konten-wenn-es-doch-ein-ea-rechner-ist" >}} )

In allen Kontennummern gibt es keine zusätzliche führende Nullen.
Das bedeutet: Wenn der Sachkontenrahmen z.B. 4-Stellig definiert ist, so ist er vierstellig. Natürlich ist die Nuller-Klasse mit einer führenden Null. Aber diese Null hat Gewicht, da es ja die Anlagengüter sind.<br>
Aber z.B. das Konto 4000 Warenerlöse aus der 4er Klasse hat auf KEINE Fall eine führende Null. Dies vor allem um Klarheit zu schaffen.<br>
Selbstverständlich können die Personenkonten auch mehr Stellen haben. Auch hier: Es gibt keine führenden Nullen. Die Bereiche (Nummernkreise) der Personenkonten können eingeschränkt werden. So ist in Österreich für die Debitoren üblicherweise der Bereich 2000-2999 und für die Kreditoren der Bereich 3000-3999 reserviert. In Deutschland ist üblicherweise für die Debitoren der Bereich 10000-19999 und die Kreditoren der Bereich 70000-79999 eingestellt.

**Hinweis:** Die Stellenanzahl der Sach- und Personenkonten können in den Systemeinstellungen unter Parameter definiert werden.

### Ermittlung der Erlöskonten
In **Kieselstein ERP** können verschiedene Arten der Erlöskontenermittlung definiert werden. Es steht von der Idee her jeweils nur eine Art zur Verfügung. Die Praxis hat aber gezeigt, dass es auch immer wieder Unterarten / Defaultarten davon gibt.

Die Erlöskontenermittlung muss je Mandant eingestellt werden.

| Ermittlungsart | Unterart |
| --- |  --- |
| ein fixes Erlöskonto | keine |
| Kostenstelle | Hauptkostenstelle |
| Artikel | Kostenstelle, Kunde oder Vertreter |
| Rechnungsposition | Kostenstelle, Kunde oder Vertreter |
| Rechnung | Kostenstelle, Kunde oder Vertreter |
| Kunde | keine |
| Vertreter | keine |
| Sollzeitverteilung der Stückliste, welche der Rechnungsposition hinterlegt ist | Artikel bzw. Rechnungsposition |

Mit der oben beschriebenen Funktion wird das Basiserlöskonto ermittelt. Gerade für die Überleitung der Daten in ein fremdes Finanzbuchhaltungsprogramm ist auch die Trennung der Kontierung anhand der verschiedenen [Länderarten]( {{<relref "/management/finanzbuchhaltung/fibu_export/#wie-wird-die-l%c3%a4nderart-bestimmt" >}} ) erforderlich.

Dies erfolgt nach folgender Regel, wobei als Bezugsadresse immer die Lieferadresse herangezogen wird.

1. Inland / Ausland
Ist das Länderkennzeichen der Lieferadresse gleich dem Länderkennzeichen des Mandanten, so ist es Inland, anderenfalls ist es Ausland.
2. EU Ausland / Drittland
Ist beim Länderkennzeichen der Lieferadresse in der LKZ Tabelle unter EU-Mitglied seit ein entsprechendes Datum eingetragen und noch kein EU-Austritt eingetragen und ist das Belegdatum jünger oder gleich dem EU-Betrittsdatum, so ist es Innergemeinschaftlicher Warenverkehr. Anderenfalls handelt es sich hier um ein Drittlandsgeschäft.
3. Mit oder ohne UID
Beim Innergemeinschaftlichen Warenverkehr wird anhand der beim Kunden hinterlegten UID Nummer unterschieden, ob es sich um eine Lieferung/Leistung mit oder ohne UID Nummer handelt. Diese Unterscheidung wird auch herangezogen, wenn entschieden werden muss, ob eine Lieferung mit oder ohne MwSt erfolgen muss.
<!-- 4. Sonderland
Manche Anwender haben im Ausland eine eigene Steuernummer um die MwSt Problematik bei Endkunden zu entschärfen. D.h. entspricht das Länderkennzeichen der Lieferadresse dem Länderkennzeichen eines der zusätzlichen Finanzämter, so wird wiederum eine eigene Länderkontierung angewendet.
-->

Da oft Konten für die Inlandserlöse wesentlich feiner gegliedert sind, als die Konten für die Auslandserlöse, wird die Konvertierung der Basiserlöskonten in einer eigenen Tabelle, welche Sie ebenfalls im Finanzbuchhaltungsmodul finden, definiert. Geben Sie hier das Basiserlöskonto, die Länderart und das resultierende Konto an.

Wird bei der Fibu Übernahme keine Kontenzuordnung / keine Kontenkonvertierung gefunden, so wird, um die Konsistenz der Buchung grundsätzlich zu wahren, der Erlös der Rechnung auf ein Fehlerkonto gebucht und die Rechnung entsprechend markiert. Es liegt in Ihrer Verantwortung, dass dieses Fehlerkonto immer leer ist, also alle Buchungen bereinigt wurden. Sind Buchungen auf diesem Fehlerkonto eingetragen, so können verschiedene Funktionen nicht durchgeführt werden (Export zu fremden Programmen) bzw. wird ein deutlicher Fehlerhinweis auf den Journalen angedruckt.

Die für die Rechnung beschriebene Erlöskontoermittlung wird analog auch für die Erlöskontoermittlung der Gutschriften verwendet.

**Hinweis zu Rechnungsdatum / Leistungsdatum**

Die Umsatzsteuer ist nach dem Leistungsdatum an das Finanzamt abzuführen. Das bedeutet unter Umständen, dass wenn eine Leistung im Februar erbracht wurde, diese aber erst im März verrechnet wird, so ist (theoretisch und umsatzsteuerrechtlich) die Umsatzsteuer bereits mit der Februarabrechnung abzuführen. Übliche und fast zu 100% verbreitete Praxis ist, dass die Umsatzsteuer mit dem Verrechnungsdatum abgeführt wird. Achten Sie daher unbedingt darauf, dass die umsatzsteuerlich relevanten Belege im richtigen Zeitraum erstellt werden. Gerade für die jährlichen Betrachtungen muss unbedingt, auch aus Gründen der Vergleichbarkeit, darauf geachtet werden, dass alles im richtigen Geschäftsjahr verrechnet wurde. Gegebenenfalls müssen von Ihnen zusätzliche Korrekturbuchungen per Hand durchgeführt werden.

Verrechnen Sie die Rechnungen in dem Wirtschaftsjahr in dem die Lieferung bzw. die Leistung erbracht wurde. Alles andere führt oft zu großer Verwirrung und zu enormen Abstimmbedarf.

Ihr **Kieselstein ERP** verbucht alle Belege mit dem Belegdatum. D.h. auch die Rechnungen werden mit dem Rechnungsdatum verbucht. Eventuelle Korrekturen müssen manuell vorgenommen werden.

**[Beschreibung der in **Kieselstein ERP** integrierten Finanzbuchhaltung]( {{<relref "/management/finanzbuchhaltung/integriert">}} )**

## Export der AR/GS/ER Daten in ein fremdes Finanzbuchhaltungsprogramm

Es stehen grundsätzlich folgende Formate zur Verfügung:
1.  RZL
2.  Datev-CSV
3.  BMD-CSV
4.  Lexware

Der Aufbau der Exportformate kann über eine eigene Definition (XSL) gesteuert werden und ist in weiten Grenzen anpassbar.

Siehe dazu [Fibu-Export]( {{<relref "/management/finanzbuchhaltung/fibu_export">}} ).

## Import von OP-Listen
Um den Aufwand für die Zahlungsbuchung möglichst zu reduzieren, stehen auch Importfilter für OffenePosten-Listen zur Verfügung. Die Logik hinter dieser automatischen Zahlungsbuchung ist folgende:

Basis sind alle Rechnungen die bereits in eine externe FiBu übernommen wurden.

ARs die auf der OP-Liste nicht mehr aufscheinen sind bereits vollständig bezahlt.

Allen anderen ARs haben nur mehr den übergebenen Betrag offen.

Es ist damit eine relativ komfortable Aktualisierung der offenen ARs möglich, auch wenn aus Themen der Gegenverrechnung (Gutschrift) usw. nicht alles automatische übernommen werden kann.

**Wichtig:** Der Anwender muss folgende Reihenfolge einhalten.
1. Daten von **Kieselstein ERP** in die Fibu exportieren
2. Daten in Fibu übernehmen
3. Bank buchen (in der Fibu)
4. OP-Liste aus Fibu erzeugen
5. OP-Liste in **Kieselstein ERP** importieren.

**ACHTUNG:** Es darf auf keinen Fall zwischen dem ersten Export und dem erstem Import ein weiterer Export ausgelöst werden, da dadurch Rechnungen als bezahlt verbucht würden, die in Wirklichkeit noch offen sind.

### Änderung des Importformates der Belegnummern
Mit dem Parameter BELEGNUMMERNFORMAT_HISTORISCH kann auch eine abweichende Belegnummerierung, früher verwendete Belegnummerierung für den offenen Postenimport definiert werden.<br>
Das bedeutet alternativ stehen zur aktuellen Definition der Belegnummern auch die angegeben Struktur zur Verfügung. Bitte beachte, dass mit JJ (oder JJJJ) und ### diese Strukturen definiert werden. Durch Trennung mittels , können auch mehrere Formate angegeben werden.

<a name="Kurse nachpflegen"></a>

#### Wo werden die Kurse nachgepflegt?
#### Was bedeutet kein Kurs hinterlegt?

![](Kurs_Nachtragen.gif)

Gerade nach Datenübernahmen aus Altsystemen kann es vorkommen, dass zwar Währungen übernommen werden konnten, aber die Wechselkurse der Währungen untereinander sind nicht definiert. Wenn Sie nun z.B. bei der Rechnung auf Neu Klicken, so erscheint obige Meldung.
Gehen Sie bitte in das Modul Finanzbuchhaltung, wählen Sie im unteren Modulreiter Währung. Wählen Sie die Währung aus, in unserem Fall ATS, wechseln Sie auf Wechselkurs und geben Sie den Wechselkurs zwischen den Währungen an.
Beispiel siehe Währungsdefinition.

#### Wie wird die Währung richtig definiert?
Für die Definition der Währung gehen Sie am Besten immer von Ihrer [Mandantenwährung](../System/index.htm#Mandantenwährung) aus. D.h. Sie gehen am Modulreiter Währung auf die Mandantenwährung und wechseln danach auf Wechselkurs. Geben Sie hier nun den Wechselkurs zu Ihrer Mandantenwährung an. Am Beispiel des US-Dollars zum Euro aus der Sicht eines Eurolandes gehen wir hier von der Aussage aus:
Für einen Euro erhalten Sie heute 1,185600 US-Dollar.
Oder allgemeiner ausgedrückt:
Für einen Mandantenwährung erhalten Sie x,xxxx Fremdwährung.
Bitte beachten Sie, dass die Daten auch in der umgekehrten Form dargestellt und auch gepflegt werden können. Sie sollten jedoch in der beschrieben Form vorgehen.
![](Waehrung.jpg)

Hinweis: Die Kurse werden zusätzlich in den Bewegungsdaten abgespeichert. Der in den Bewegungsdaten hinterlegte Kurs kann daher nur bei einer erlaubten Änderung der Kopfdaten neu übernommen werden. Aus diesem Grunde kann die Währung nur dann verändert werden, wenn keine Positionen enthalten sind.

Umrechnung / Kehrwert der Kurse

Von **Kieselstein ERP** wird für die Umrechnung von einem Kurs zu einem anderen die gleiche Zahlenbasis, also gegebenenfalls der Kehrwert für die Berechnung verwendet. Dies entspricht auch der Umrechnung wie sie von Bankinstituten vorgenommen wird. Also wenn für EUR zu USD 1,19 hinterlegt ist, so wird für die Berechnung USD zu EUR der USD mit 0,840336... multipliziert und das Ergebnis auf maximal 4Nachkommastellen genau abgespeichert.

Mandantenwährung / Bilaterale Kurse / Beidseitige Fremdwährungen

Werden Kursumrechnungen von einer nicht Mandantenwährung in eine andere nicht Mandantenwährung erforderlich, so erfolgt die Umrechnung immer über die [Mandantenwährung]( {{<relref "/docs/stammdaten/system" >}} ).

Ein Beispiel:

Eine Kundenpreisliste ist in USD definiert, der Kunde benötigt seine Rechnung jedoch in CHF.

Ihre Mandantenwährung ist EUR.

In diesem Fall werden die Verkaufspreise aus dem Artikel von USD in EUR umgerechnet, zum jeweils gültigen Rechnungsdatum, und dann von EUR nach CHF, wiederum zum gültigen Rechnungsdatum.

Geschäftsjahr / Kalenderjahr

Üblicherweise ist das Wirtschaftsjahr eines Unternehmens gleich dem Kalenderjahr. Es gibt für viele Unternehmen gute Gründe, das Wirtschaftsjahr abweichend zum Kalenderjahr zu definieren. Die kann in **Kieselstein ERP** unter System, Parameter, GESCHAEFTSJAHRBEGINNMONAT definiert werden. Damit definieren Sie den Kalendermonat, mit dem Ihr Geschäftsjahr beginnt. Diese Definition wirkt sich auf die Erzeugung der Belegnummern aus. Bei der automatischen Erzeugung der Belegnummer wird jeweils zum Beginn eines neuen Geschäftsjahres mit 1 begonnen. Die Belegnummern enthalten üblicherweise an führender Stelle das Jahr (zweistellig) und dahinter eine laufende Nummer. Ist nun der Geschäftsjahresbeginn abweichend vom Kalenderjahr, so ist auch der Neubeginn der Zählung entsprechend. Zusätzlich kann noch der Wert der Jahresdarstellung beeinflusst werden. Üblicherweise wird bei den Belegnummern das Kalenderjahr in dem das Geschäftsjahr beginnt angeführt. Bei manchen Konstellationen ist aber gewünscht, das das endende Kalenderjahr in den Belegen angeführt wird. Für diesen Fall setzen Sie bitte den Parameter GESCHAEFTSJAHRPLUSEINS auf 1.

Fehlermeldung

![](Fehlermeldung_Zirkularbezug.gif)

Aufgrund der, verketteten Kontendefinition würde das Konto im Endeffekt wieder auf sich selbst verweisen. Korrigieren Sie die Definition des UST Kontos dieses Kontos.

### Definition der eigenen Bankverbindungen
<a name="Eigene Bankverbindungen"></a>
Im unteren Modulreiter Bankverbindungen können die eigenen Bankverbindungen definiert werden. Wir sprechen hier auch von den sichtbaren Bankverbindungen. Um eine eigene Bankverbindung definieren zu können muss **vorher** ein entsprechendes Sachkonto für das Bankkonto definiert werden und die eigentlichen Daten der Bank unter Partner erfasst worden sein. Siehe bitte [Partner](../Partner/index.htm#Bankverbindung / Bank).

Zum Anlegen der eigenen Bankverbindung sind folgende Schritte erforderlich:

1.  Anlegen der Bank im Modul Partner, unterer Modulreiter Bank
2.  Anlegen des Sachkontos Bank im Modul Finanzbuchhaltung
3.  Definition der eigenen Bankverbindung im Module Finanzbuchhaltung, unterer Modulreiter eigene Bankverbindung

![](Bankverbindung_definieren.gif)

SEPA-Verzeichnis: Definieren Sie hier den Pfad aus dem / in den Sie die Daten mit der Kommunikation mit Ihrer Banksoftware übertragen wollen.
In Liquiditätsvorschau: Sollte dieses Bankkonto bei der Ermittlung des Ausgangssaldos für die Liquiditätsvorschau mit berücksichtigt werden, so haken Sie dies bitte an.
Stellenanzahl Kontoauszugsnummer: ![](Stellenanzahl_Kontoauszugsnummer.gif) Definieren Sie hier, wieviele Stellen die erfasste Kontoauszugsnummer haben sollte. Dies ist vor allem dann von Bedeutung, wenn Sie den SEPA Kontoauszugsimport verwenden und um Sicherzustellen, dass möglichst die richtige Kontoauszugsnummer eingegeben wird. Ist hier ein Wert eingegeben, so wird bei der Bankbuchung die Länge der Kontoauszugsnummer geprüft. Stimmt diese nicht mit der hinterlegten Länge überein erscheint die Fehlermeldung Anzahl der Stellen der Kontoauszugsnummer.
![](Stellenanzahl_Kontoauszugsnummer_Meldung.jpg)
Korrigieren Sie gegebenenfalls die erfassten Daten.
Dies ist lediglich eine Information. Die Buchung wird trotzdem durchgeführt.
 **Hinweis:** Wenn dies das standard Geschäftskonto ist, definieren Sie noch im Modul System, unterer Modulreiter Mandant, oberer Modulreiter Vorbelegungen die Bankverbindung.

Bankverbindung verstecken

Um ehemalige Bankverbindungen nicht mehr in der Auswahlliste zu erhalten, können Sie die Bankverbindung verstecken. Wechseln Sie dazu in das Modul Finanzbuchhaltung, unterer Reiter Sachkonto und wählen das Konto der Bank aus. Nun setzen Sie den Haken bei versteckt ![](versteckt.JPG). Ab diesem Zeitpunkt wird die Bankverbindung in der Liste der Bankverbindungen nicht mehr angeführt.

### Mehrere Finanzämter

In stark Export-orientierten Ländern ist es üblich, dass in den Haupt-Exportländern eigene Umsatzsteuernummern bei den Finanzämtern eingerichtet werden. Dies hat den Vorteil, dass die Vorsteuerabzugsberechtigung in diesen Ländern sofort wirksam ist und dass die Kunden mit der landesüblichen Mehrwertsteuer beliefert werden können.

Aus diesem Grund können Sie in **Kieselstein ERP** für jedes Konto ein eigenes Finanzamt definieren. Dieses Definition dient dazu, das Bezugsland der Umsatzsteuer, insbesondere beim Export der Daten, festzulegen.

Wenn Sie auch das Modul Finanzbuchhaltung erworben haben, so können Sie Ihre Finanzämter im [Modul Finanzbuchhaltung]( {{<relref "/management/finanzbuchhaltung/integriert/#definition-eines-finanzamtes" >}} ), unterer Modulreiter Finanzamt definieren. Haben Sie das Modul Finanzbuchhaltung nicht erworben, so können von uns, gegen einen geringen Kostenbeitrag, die von Ihnen benötigten Finanzämter hinterlegt werden.

Beachten Sie bitte, dass beim Anlegen neuer Konten automatisch immer das Hauptfinanzamt vorgeschlagen wird.

## Export der RE, GS, ER zur RZL Fibu:

Ein Beispiel: Sie haben zwei Finanzämter (A, D) mit jeweils einer Steuernummer. Diese sind bei den Konten entsprechend hinterlegt. Ihr Hauptfinanzamt ist A.

Nun erhalten Sie drei Lieferungen und zwar aus:

A, D, NL

Die Lieferung aus A wird beim Finanzamt A als Inlandslieferung versteuert.

Die Lieferung aus D wird beim Finanzamt D als (quasi) Inlandslieferung versteuert.

Die Lieferung aus NL wird beim Finanzamt A (= Hauptfinanzamt) als IG-Erwerb versteuert.

D.h. erfolgt ein Erlös oder ein Erwerb aus dem gleichen Land wie das Land des beim Konto hinterlegten Finanzamtes, so wird es umsatzsteuerlich wie eine Inlandslieferung für das Land des Finanzamtes behandelt. Ist dem nicht so, so bezieht sich die umsatzsteuerliche Betrachtung immer auf das Hauptfinanzamt.

Bitte beachten Sie, dass diese Betrachtung nur dann gültig ist, wenn Sie in anderen Ländern Steuernummern haben. Ist dies nicht der Fall, so muss ja sehr oft, z.B. bei Parkgebühren, die MwSt des besuchten Landes mitbezahlt werden. Die Konten für diese Buchungen sind in der Kontoart auf Steuerbar mit fremder Ust zu stellen. Die zu buchenden Beträge sind damit als Bruttobetrag einzubuchen, da Sie ja bestenfalls die im fremden Land bezahlte MwSt am Ende des Jahres zur Refundierung beantragen können. Ob diese dann tatsächlich refundiert wird, hängt durchaus von Schikanen und Willkür der Beteiligten ab.

#### Muss man auch für ein Konto der Anlagegüter eine Kostenstelle angeben?
Ja. Da es auch für Anlagengüter einen Verantwortlichen, eine verantwortliche Abteilung gibt, ist ist auch für die Anlagenkonten eine Kostenstelle zu definieren. Verwenden Sie am Besten die Kostenstelle der Person die die Verantwortung dafür übernimmt. In einigen Fällen wird das auch die Geschäftsleitung sein.

<a name="Sachkonten definieren"></a>

#### Welche Daten müssen für die Sachkonten definiert werden?
Wenn die Sachkonten nur für die Überleitung der Konten an eine fremde Buchhaltungssoftware definiert werden müssen, so sollten folgende Felder definiert werden.

![](Fibu_Kontoerfassung.gif)

| Lable | Bedeutung |
| --- |  --- |
| Kontonummer | Nummer des Sachkontos. Dies muss mit Ihrer Buchhaltung übereinstimmen |
| Bezeichnung  | Die Kontobezeichnung. Diese sollte mit der Kontobezeichnung Ihrer Buchhaltung übereinstimmen und eindeutig sein. |
| Finanzamt  | Das Finanzamt, für das dieses Konto herangezogen wird. |
| Kontoart  | Muss für die Überleitung in eine fremde Buchhaltung nicht definiert werden. Dies wird üblicherweise in den Konten der Buchhaltung definiert. |
| USt Konto | Diese Definition ist nur für die Schleupen-Schnittstelle erforderlich. Geben Sie hier das Konto an, auf das der Umsatzsteueranteil der Ausgangsrechnung / Gutschrift gebucht werden soll. |

#### Bedeutung der Kontoarten
Die Kontoarten haben folgende Bedeutung

| Kontoart | Nummer |
| --- |  --- |
| Aufwandskonto steuerbar mit fremder Ust | 10 |
| Drittland Erlös | 7 |
| Drittland Erwerb | 6 |
| Erlöskonto steuerbar mit fremder Ust | 9 |
| Innergemeinschaftlicher Erlös | 5 |
| Innergemeinschaftlicher Erwerb | 4 |
| Nicht steuerbar | 1 |
| Reverse Charge | 8 |
| Steuerbares Erlöskonto | 2 |
| Steuerkonto aus IG Erwerb | 13 |
| UST- oder Erwerbssteuerkonto | 12 |
| Vorsteuerabzug erlaubt | 3 |
| Vorsteuerkonto | 11 |
| Sonderland Erwerb | 14 |
| Sonderland Erlös | 15 |

<a name="Lieferung und Leistung"></a>

#### Lieferung und Leistung
Gerade für den Export in den EU-Raum ist diese Trennung entsprechend wichtig. Damit diese richtig definiert wird, ist es erforderlich, dass Sie zwei unterschiedliche Artikelgruppen für (Waren-)Lieferungen und für (Dienst-)Leistungen angeben. Damit können diese Artikelgruppen auch den unterschiedlichen Erlöskonten zugeordnet werden und so der entsprechende Ausweis auf der Umsatzsteuervoranmeldung (UVA) erfolgen.

#### [Liquiditätsvorschau]( {{<relref "/management/finanzbuchhaltung/liquiditaetsvorschau">}} ) / [Einfache Erfolgsrechnung]( {{<relref "/management/finanzbuchhaltung/einfache_erfolgsrechnung">}} ) / [KPI]( {{<relref "/management/finanzbuchhaltung/kpi">}} )
Werden Änderungen protokolliert?

A: Ja, Sie finden im Modul Finanzbuchhaltung, oberer Menüpunkt Info einen Ausdruck Änderungen.

![](aenderungen.png)

Hier werden die Änderungen des gerade ausgewählten Kontos gelistet.

#### Wie kann ein Kassenbuch angelegt bzw. verwendet werden?
Zum Kassenbuch gibt es zwei Ausprägungen:

-   Das unechte Kassenbuch. Dieses steht zur Verfügung, wenn die integrierte Finanzbuchhaltung NICHT verwendet wird. D.h. es werden die Zahlungen auf den Ein-/Ausgangsrechnungen auf eine Art Bank die Kassenbuch heisst gebucht.

-   Das echte Kassenbuch, welches auch direkt im Kassenkonto bucht. Details dazu siehe bitte auch [integrierte Finanzbuchhaltung](Integrierte_Finanzbuchhaltung.htm#Kassenbuch).

Zur Anlage eines neuen Kassenbuches gehen Sie bitte wir folgt vor:
-   Starten Sie das Modul Finanzbuchhaltung
-   Legen Sie in den Sachkonten bitte ein Kassenkonto an
-   Wählen Sie nun im Modul Finanzbuchhaltung den unteren Reiter Kassenbuch und legen Sie mit Neu ein neues Kassenbuch an.
-   Geben Sie diesem Kassenbuch einen für Sie sprechenden Namen und ordnen Sie dem Kassenbuch das zuvor definierte Sachkonto zu.
    Wenn dies Ihre Hauptkasse sein sollte, so haken Sie bitte noch Hauptkassenbuch an.

Um nun die Zahlungen der Eingangs- / Ausgangsrechnungen über diese Zahlungsart abwickeln, so wählen Sie bitte im Reiter Zahlung bei Art, Bar und wählen danach links das gewünschte Kassenbuch.<br>
Info: Das Zahlungsjournal finden Sie im Eingangs- / Ausgangsrechnungsmodul unter Journal, Zahlungsausgang bzw. Zahlungseingang.

Um nun der Darstellung eines Kassenjournals für die Installationen OHNE integrierter Fibu möglichst nahe zu kommen, steht 
- einerseits in den Kopfdaten des Kassenbuches der Druck eines Kassenjournals zur Verfügung und - andererseits kann in den Zusatzkosten eine Zusatzkosteneingangsrechnung mit einem Wert von 0,00 aber einem Zahlbetrag von / an Bank erfasst werden.

Bewährt hat sich, dass für die "Buchungen" Kasse an Bank, bzw. Bank an Kasse, eine Zusatzkosteneingangsrechnung Ihres eigenen Unternehmens mit einem Wert von 0,00 angelegt wird und in diese die Bewegungen mit dem jeweiligen Datum eingetragen werden. Bitte beachten Sie, dass hier ein positiver Zahlbetrag bedeutet Kasse an Bank und somit ein negativer Betrag Bank an Kasse.

D.h. im Kassenjournal werden alle Zahlungen die im Rechnungs- bzw. Eingangsrechnungsmodul auf Bar / Kasse geleistet wurden chronologisch dargestellt.
![](Kassenjournal.gif)