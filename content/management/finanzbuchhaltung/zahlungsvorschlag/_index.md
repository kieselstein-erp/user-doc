---
categories: ["Finanzbuchhaltung"]
tags: ["Zahlungsvorschlag"]
title: "Zahlungsvorschlag"
linkTitle: "Zahlungsvorschlag"
weight: 300
date: 2023-01-31
description: >
 offene und fällige Eingangsrechnungen zum bestmöglichen Zahlungstermin vorschlagen und in deine Banksoftware übertragen
---
Zahlungsvorschlag
=================

In **Kieselstein ERP** können auf einfache Weise die fälligen Zahlungen in das Electronic Banking (Elba) übertragen werden.

Die grundsätzliche Idee ist, zuerst wird ein Zahlungsvorschlag erstellt, dieser wird danach bearbeitet und das Ergebnis wird in das Elba übertragen.

Da für den Import von Daten die Eigenheiten der verschiedenen Elba Programme zu berücksichtigen sind, wurde als erstes Format das von Elba Österreich definierte CSV Format als Exportfilter realisiert.

Als weiteres Exportformat steht die Übertragung nach dem SEPA (Single Euro Payments Area) Standard zur Verfügung. [Siehe](#sepa-import--export). Dieser wurde um die Definition der ISO20022 ergänzt.
Ergänzend steht auch das Lastschriftverfahren nach SEPA Standard zur Verfügung.

#### Erzeugen des Zahlungsvorschlages
Im Modul Finanzbuchhaltung finden Sie den unteren Modulreiter Zahlungsvorschlag. 

Hier sind alle bisher erzeugten Zahlungsvorschläge enthalten.

Durch Klick auf Neu ![](Zahlungs_Neu.gif) werden nun die Parameter für den neuen Zahlungsvorschlagslauf definiert.

![](Zahlungsvorschlag_Neu_Dialog.gif)

Definieren Sie hier die Zahlungsstichtag, das Datum des nächsten Zahlungslaufes und ob die Bezahlung mit Skonto erfolgen sollte aus. Definieren Sie die Bankverbindung über die die Zahlung erfolgen sollte.

Hinweis: Es wird die unter System Mandant Lieferkonditionen definierte Bankverbindung vorgeschlagen.

Bei der Errechnung der zu bezahlenden Eingangsrechnungen werden folgende Regeln angewandt:
- Kann die Eingangsrechnung bis zum nächsten geplante Zahlungslauf mit maximal einem Tag Überziehung der Skontofrist mit Skonto bezahlt werden?
    - Wenn ja, so wird sie nicht in den Zahlungsvorschlag aufgenommen
    - Wenn nein, so wird die Eingangsrechnung als mit Skonto zu bezahlen vorgeschlagen, wenn nicht die nachfolgenden Bedingungen zutreffen.
- Ist die Skontozahlungsfrist bereits überschritten, so gilt:
    - Kann die Eingangsrechnung bis zum nächsten geplanten Zahlungslauf innerhalb der Zahlungsfrist bezahlt werden?<br>
    Wenn ja, so wird sie nicht in den Zahlungsvorschlag aufgenommen.
- Alle anderen Eingangsrechnungen werden als zu bezahlen vorgeschlagen.<br>Das Bezugsdatum für die Berechnung der Fälligkeit der Eingangsrechnung ist das Freigabedatum der Eingangsrechnung.<br>
Dazu wird die Zahlungsfrist anhand des Zahlungszieles mit oder ohne Skonto hinzugerechnet.

- Ist in Ihrem **Kieselstein ERP** das Prüfen der Eingangsrechnungen aktiviert, also der Parameter EINGANGSRECHNUNG_PRUEFEN auf 1, so werden die fälligen Eingangsrechnungen nach den angeführten Punkten errechnet. Noch nicht geprüfte Eingangsrechnungen sind jedoch deaktiviert. D.h. Sie sehen hier einerseits alle fälligen Rechnungen, gerade wegen Skontozahlungen, und andererseits auch, bei welchen Eingangsrechnungen die Prüfung der Freigabe noch ausständig ist.

#### Bearbeiten des Zahlungsvorschlages
Der Zahlungsvorschlag kann nach der Erzeugung bearbeitet werden, z.B. weil einzelne Rechnungen nicht oder nur teilbezahlt werden sollten.<br>
![](Zahlungsvorschlag_Bearbeiten.jpg)<br>
Sie finden unter dem oberen Modulreiter offene Posten die Details des Zahlungsvorschlages.<br>
In der letzten Spalte finden Sie die Bankverbindung. Bedenken Sie hier bitte, dass nur Eingangsrechnungen mit Bankverbindungen exportiert werden, deren Länder am SEPA teilnehmen. Die SEPA Teilnahme eines Landes kann im System, oberer Modulreiter Land, definiert werden.<br>
Wird in der vorletzten Spalte ein Haken angezeigt, so wird diese Eingangsrechnung mit dem angezeigten Betrag exportiert, wenn die Bankverbindung in einem SEPA teilnehmenden Land ist.<br>
Durch Klick auf ![](Zahlungsvorschlag_Bearbeiten_Hacken.gif) kann die Übernahme in die Überweisungsliste ein bzw. ausgeschaltet werden. Um mehrere Eingangsrechnungen von der Überweisung auszunehmen markieren Sie bitte die entsprechenden Zeilen und klicken dann auf den Haken.<br>
Durch Klick auf ![](Zahlungsvorschlag_Bearbeiten_aendern.gif) kann der zu überweisende Betrag geändert werden.<br>
Rechts unten wird unter Gesamtwert die Summe der für die Überweisung freigegebenen Beträge angezeigt.<br>
**Hinweis:** Wird Skonto berechnet, so werden auch die Beträge um den Skontobetrag reduziert.

Der Parameter ZAHLUNGSVORSCHLAG_MIT_SKONTO definiert, dass beim Zahlungsvorschlag Skonto berücksichtigt wird.

Beim Parameter ZAHLUNGSVORSCHLAG_SKONTOUEBERZIEHUNGSFRIST können Sie einstellen wie lange Skonto noch berücksichtigt werden soll (Eingabe in Tagen).

ZAHLUNGSVORSCHLAG_PERIODE gibt den Zeitraum zwischen den Zahlungsläufen in Tagen an.

**Hinweis:**

Werden z.B. Vorauszahlungen nicht exportiert, so sind sehr wahrscheinlich Skontotage definiert, aber keine Netto-Zahlungstage.<br>
Korrigieren Sie daher das [Zahlungsziel](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Zahlungsziel) entsprechend.

Im Zahlungsvorschlag sind auch die offenen Zusatzkosten (Modul Eingangsrechnungen) mit enthalten. Damit haben Sie die praktische Möglichkeit auch Ihre laufenden Zahlungen über den Zahlungsvorschlag abzubilden.

#### Werden auch Gutschriften exportiert?
Nein, Gutschriften werden nicht exportiert, genauer offene negative Beträge werden nicht exportiert. Sollten Sie versehentlich einen Zahlungsvorschlag mit einem negativen Betrag eingetragen haben, so erscheint beim Exportieren des Zahlungsvorschlages die Fehlermeldung
![](Zahlungsvorschlag_Gutschriften_enthalten.gif) Gutschriften enthalten.<br>
Zusätzlich werden die Zeilen mit negativen Beträgen in Rot angezeigt. Die Entscheidung wie mit der Gutschrift und eventueller offener weiterer Beträge umgegangen wird muss Zeile für Zeile von Ihnen in den Eingangsrechnungen getroffen werden. Nutzen Sie dazu auch die Möglichkeit den Zahlungsvorschlag nach den Lieferanten (Spalte Firma) zu sortieren.<br>
Wir empfehlen, dass bereits bei der Erfassung von Eingangsgutschriften diese mit den Eingangsrechnungen, für die Sie die Gutschrift erhalten haben, ausgeglichen werden.

#### Wie kennzeichnet man eine Rechnung als bezahlt?
Im Zahlungsvorschlag gibt es das Feld 'vollständig Bezahlt'  ![](vollständig_bezahlt.JPG).
Wenn dies angehakt ist, wird diese bei einem späteren Zahlungsvorschlagslauf berücksichtigt, d.h. wenn bereits bezahlt, dann scheint die Eingangsrechnung nicht nochmals im Zahlungsvorschlag auf.<br>
Wenn der Haken bei 'vollständig bezahlt' nicht gesetzt ist, dann werden bei einem späteren Zahlungslauf die vorherigen Zahlungen mitgerechnet.<br>
Wenn ein Zahlungsvorschlag gespeichert wird, dann kann dieser nur mehr nach Warnung gelöscht/nochmal gespeichert werden.

#### Sieht man im Zahlungsvorschlag auch übersteuerte Bankverbindungen?
Ja. Es wird, bei einer übersteuerten Bankverbindung einer Eingangsrechnung im Detail über der Bankverbindung der Name der Person angezeigt, welche bei dieser Eingangsrechnung hinterlegt ist.
![](abweichende_Bankverbindung.gif)

#### Welche Bankverbindungen werden verwendet?
Insbesondere unsere Anwender mit internationalen Geschäften haben oft Bankkonten für die Haupt-Fremdwährungen wie USD, GBP, CHF, EUR usw..<br>
Nun werden bei größeren Lieferanten bzw. z.B. Speditionen die Geschäfte durchaus auch über unterschiedliche Währungen abgewickelt.<br>
D.h. es sind beim Lieferanten mehrere Bankverbindungen eingetragen, mit unterschiedlichen Währungen.<br>
![](Bankverbindung_mit_Waehrung.gif)<br>
Das bedeutet, dass beim Erstellen eines neuen Zahlungsvorschlages bei der Auswahl der Bank auch die beim Bankkonto hinterlegte Währung übernommen wird.<br>
![](Zahlungsvorschlag_mit_Waehrung.gif)<br>
Das bedeutet, dass für diesen Lauf
- a.) nur die Eingangsrechnungen mit EUR berücksichtigt werden
- b.) wenn bei einem Lieferanten mehr wie eine Bank definiert ist, wird die Bank mit der passenden Währung verwendet.

Wird keine zur Währung passende Bank gefunden, wird die bevorzugte Bank, also die erste in der Reihenfolge verwendet.
In anderen Worten, wenn in der Lieferantenbankverbindung das Feld Währung definiert ist und die Eingangsrechnung die gleiche Währung hat, wird diese Bankverbindung bevorzugt.

#### Kann ich eine Überweisungsliste ausdrucken?
In der Auswahlliste der Zahlungsvorschläge finden Sie auch das Druckersymbol. Damit kann für den gewählten Zahlungsvorschlag die Zusammenfassung der gewünschten Überweisungen ausgedruckt werden. Ist in Ihrer **Kieselstein ERP** Installation auch die Prüfung der Eingangsrechnungen aktiviert, so werden die noch nicht geprüften, also noch nicht freigegebenen Eingangsrechnungen rot markiert.

#### Exportieren des Zahlungsvorschlages
Für den Export der zur Zahlung markierten Rechnungen wählen Sie den oberen Modulreiter Zahlungsvorschlagsläufe. Durch einen Klick auf ![](Zahlungsvorschlag_Exportziel.gif) wird der Zahlungsvorschlag im CSV Format exportiert. Der Pfad des CSV-Exportes kann unter System, Parameter, Mandantenparameter, ZAHLUNGSVORSCHLAG_EXPORTZIEL eingestellt werden. Dieser Exportpfad muss mit Ihren Elba Einstellungen übereinstimmen (siehe unten).
Bitte beachten Sie, dass der Exportlauf beliebig oft vorgenommen werden kann.
Durch den Klick auf ![](Zahlungsvorschlag_SEPA_Exportziel.gif) SEPA wird der Zahlungsvorschlag im SEPA Format in das in der Bankverbindung definierte Verzeichnis exportiert.
Für die Einrichtung von ELBA für den SEPA XML [Import siehe](#SEPA Auftrags-Import).

#### Ich erhalte die Fehlermeldung
![](Zahlungsvorschlag_Bankverbindung_fehlt.gif)

Dies bedeutet dass bei diesem Lieferanten noch keine Bankverbindung hinterlegt ist. Tragen Sie im Lieferanten seine Bankverbindung ein. Die Basisdaten der Banken müssen unter Partner, Bank eingerichtet werden.

**WICHTIG:**  

Da ausländische Lieferanten oft inländische Bankverbindungen haben, und da die ELBA CSV Schnittstelle nur inländische Banken verarbeiten kann, ist für den gültigen Export der Ort der Bank wesentlich. D.h. Sie erhalten obige Fehlermeldung auch, wenn zwar eine Bankverbindung angegeben ist, aber der Ort der Bank fehlt und daher nicht festgestellt werden kann ob dies eine inländische oder eine ausländische Bank ist.

**Hinweis:**

Hier greift auch die SEPA Definition. D.h. ist ein Land SEPA-Teilnehmer, so wird es, dank IBAN und BIC, wie Inland behandelt und somit exportiert. Ist ein Land kein SEPA-Teilnehmer, so werden die Daten nicht an die Schnittstelle übergeben. [Siehe dazu auch.](../System/index.htm#Anlegen von Ländern)

**<u>WICHTIGER Hinweis:</u>**

SEPA gilt laut dem Rulebook des European Payment Councils vom 12.12.2013, Version 7.1 ausschließlich für Überweisungen in EURO. Für alle anderen Währungen muss die wesentlich teurere Auslandsüberweisung verwendet werden. [Siehe dazu auch ISO20022](#ISO20022).
<http://www.europeanpaymentscouncil.eu/index.cfm/knowledge-bank/epc-documents/sepa-credit-transfer-rulebook-version-70/epc125-05-sct-rb-v71-approvedpdf/>
Seite 16 Punkt 2.4

#### Beim Import kommt die Fehlermeldung 'keine EURO-Belege' zu exportieren?
Wenn der Zahlungsverkehr mit der Bank nach dem SEPA Standard definiert ist, dürfen nur Euro-Beträge übergeben werden. Wenn nun z.B. nur USD Beträge exportiert werden sollten, so muss die ISO20022 Schnittstelle entsprechend auf Swiss Payment eingestellt werden. In dieser Definition sind auch andere Fremdwährungen erlaubt.

#### Kann ein Zahlungslauf mehrfach exportiert werden?
Ja. Ein Zahlungslauf kann beliebig oft exportiert werden.<br>
Bitte beachten Sie die Einschränkungen für Zahlungsvorschlag mit Buchung der Zahlung auf [Geldtransitkonto](#Geldtransitkonto).

#### Wie muss Elba für den CSV-Import eingerichtet werden.
Im Elba 5 finden Sie unter Zahlungsverkehr den Menüpunkt Import.

![](Elba_Import1.gif)

Wechseln Sie in den Reiter Importprofile.

![](Elba_Importprofil.gif)

Hinweis:

Wenn dieser Reiter nicht verfügbar ist, haben Sie im Elba keine Rechte um diese zu definieren. Melden Sie sich daher als Elba-Administrator an Elba an.

Wählen Sie nun ein bestehendes Importprofil aus, oder definieren Sie mit Neu ein neues Profil. Als Dateipfad geben Sie den unter System, Parameter, Zahlungsvorschlag_Exportziel

![](Zahlungsvorschlag_Parameter.gif)

angegebenen Dateipfad an.

![](Zahlungsvorschlag_Importprofil_HV.gif)

Nach dem Abspeichern des neuen Profils definieren Sie durch Klick auf Dateivorschau den genaueren Dateiaufbau, wobei es hier im wesentlichen um die Definition der Trennzeichen geht. Diese müssen als Semikolon (Strichpunkt) definiert werden. Definieren Sie hier auch, ab welcher Zeile der Import beginnt. In der Exportdatei sind, insbesondere für die leichtere Zuordnung, in der ersten Zeile die Feldüberschriften enthalten. D.h. für die Definition der Dateistruktur stellen Sie das Feld Import beginnt ab Zeile ... auf 1 und für den echten Datenimport dann auf 2.

Nach der Definition der Trennzeichen gelangen Sie in die Feldzuordnung.

![](Zahlungsvorschlag_Elba_Import_Feldzuordnungsuebersicht.gif)

Klicken Sie hier zuerst in das linke Fenster, danach in das dazupassende Import Feld des rechten Fensters und dann auf Zuordnung erstellen.

Definieren Sie so alle Felder, sodass Sie im Endeffekt im unteren Fenster folgende Definition erhalten.

![](Zahlungsvorschlag_Elba_Import_Detail.gif)

Die Reihenfolge der Felder ist unwichtig, aber die Zuordnung der Felder zueinander muss stimmen.

Nach der richtigen Definition denken Sie daran, *Import beginnt mit Zeile* **2** einzustellen.

![](Zahlungsvorschlags_Elba_Import_Dialog.gif)

Achten Sie hier auf den Eintrag *Import beginnt ab Zeile* **2.** Zugleich geben Sie beim *Dateiaufbau:* **Einzel-Aufträge** an.

Nun klicken Sie auf Importieren und bestätigen diese Meldung mit Ja, wenn der Import wie oben beschreiben richtig definiert ist.

![](Zahlungsvorschlag_Elba_Import.gif)

![](Zahlungsvorschlag_Elba_Import_Auftragsart.gif)

Sie müssen nun noch die Auftragsart für alle Importe definieren. Dies wird üblicherweise die Überweisung sein.
Wählen Sie die Auftragsart aus und bestätigen Sie mit OK.

Nun wird von Elba die Gültigkeit der Bankverbindungsdaten überprüft.<br>
Im Fehlerfalle erhalten Sie entsprechende Informationen / Dialoge. Bitte korrigieren Sie in diesem Falle Ihre Daten sowohl in diesem einen Auftrag und in Ihrem **Kieselstein ERP** Datenbestand.

Am Ende des gültigen Imports sind die Aufträge als Einzel-Aufträge in ihrem Elba enthalten und können von dort als Einzel-Aufträge weiter behandelt werden.<br>
Das Datum des Imports ist zugleich das Durchführungsdatum der Aufträge.<br>
D.h. es ist grundsätzlich angedacht, den Zahlungsvorschlagslauf, den Elba Import und die Übertragung der Daten an die Bank in einem Zuge zu machen.

#### Wie die Daten nach SFirm übernehmen?
Die SEPA-XML-Exportdaten können auch nach SFirm übernommen werden.<br>
Wählen Sie hier links im Programmbaum, Zahlungsverkehr und klicken Sie oben auf Fremddaten importieren.<br>
Nun sehen Sie alle Aufträge, die je einer Zeile des Zahlungsvorschlages entsprechen.<br>
Haken Sie unten "alle Sammler auf einmal importieren" an, damit alle Buchungen in einem Durchgang importiert werden.<br>

Denken Sie daran, dass nach dem Import die Datei gelöscht werden sollte und beantworten Sie die Frage von SFirm entsprechend.

#### Wie sollte mit Eingangsgutschriften umgegangen werden?
Es kommt immer wieder vor, dass Sie von Ihren Lieferanten Gutschriften erhalten.<br>
Damit im Zahlungsvorschlag die richtigen offenen Beträge (im Sinne der Gutschrift) erscheinen, sollten Sie die Eingangsgutschrift unmittelbar nach deren Erhalt als "bezahlt" verbuchen. [Siehe dazu](../Eingangsrechnung/index.htm#Eingangsgutschrift verbuchen).![](meldung.png)

Wenn in einem Zahlungsvorschlag eine Eingangsrechnungsgutschrift enthalten ist, so wird diese Rot markiert und eine Fehlermeldung angezeigt.
![](Zahlungsvorschlag.png)
In diesem Fall entfernen Sie bitte die Eingangsrechnung aus dem Zahlungsvorschlag und führen den Zahlungsvorschlag erneut durch.

#### In meinem Zahlungsvorschlag ist eine rote Zeile?
Im Zahlungsvorschlag werden jene Beträge die negativ sind und zur Übertragung (Zahlung) markiert sind in Rot gekennzeichnet. Sind solche Zeilen bei der Speicherung des Zahlungsvorschlages noch vorhanden, so kann der Zahlungsvorschlag nicht gespeichert werden, da manche Importprogramme einfach die Vorzeichen verwerfen und dadurch eine Zahlung auf ein Guthaben geleistet würde. Wir haben diese Information in jedem Falle im Zahlungsvorschlag mit aufgeführt, damit Sie, wie oben beschrieben, daran denken, dies auf den jeweiligen Lieferanten zu buchen / die Zahlung einzutragen.

#### Beim Export des Zahlungsvorschlages kommt auf einmal eine Fehlermeldung
Erscheint beim SEPA Export des Zahlungsvorschlages eine Fehlermeldung<br>
![](Sepa_Export_Fehler1.png)<br>
so klicke auf Detail. Hier steht vermutlich ein ähnlicher Hinweis wie:<br>
![](Sepa_Export_Fehler2.png)<br>
Dieser bedeutet, dass eine grundsätzliche Länge überschritten wurde. In diesem Falle sind dies die in der dazugehörenden Eingangsrechnung hinterlegten Kundendaten für E-Banking.
![](Sepa_Export_Fehler3.png)<br>
Bitte beachte: In diese Daten gehören **nur** Belegnummern/ID-Nummern die dir vom Lieferanten mitgeteilt werden. Keine wie immer gearteten Kommentare. Der Vorschlagswert für die Kundendaten für E-Banking ist deine Kundennummer beim Lieferanten.

<a name="Geldtransitkonto"></a>Transitorisches Bankkonto, Geldtransit Konto

Ist in der Definition der Bankverbindung Geldtransitkonto angehakt, so wird beim Speichern des Zahlungsvorschlagslauf auf dieses (Bank-)Konto sofort die Zahlung Ihrer Eingangsrechnungen gegen dieses Konto gebucht. Im Gegensatz zur oben beschriebenen Berücksichtigung der bereits erfolgten Zahlungen, wird diese hier nicht durchgeführt, da ja bereits die Zahlungen verbucht sind. (Der normale Zahlungsvorschlag berücksichtigt bei der Ermittlung der offenen Positionen die bisherigen Zahlungsvorschläge. Der Zahlungsvorschlag auf das Geldtransitkonto berücksichtigt diese nicht.)<br>
Denken Sie daran, unter System, Mandant, Vorbelegungen die richtige Bank als Vorschlagswert einzurichten.<br>
Der Export kann, nach Bestätigung der Warnmeldung mehrfach erfolgen. Die Zahlungsbuchung erfolgt jedoch nur beim ersten Exportlauf.

## SEPA Import / Export

<a name="SEPA"></a>Es können Zahlungsaufträge aus dem Zahlungsvorschlag im SEPA Standard exportiert werden. Für die integrierte Finanzbuchhaltung können auch Kontoauszüge importiert werden und so der Vorgang des Zahlungen-buchens deutlich vereinfacht werden.

**Bitte beachten Sie.** SEPA ist nur für Zahlungen in Euro. Andere Währungen siehe bitte IOS20022

Um, insbesondere für die Eingangsrechnungen einen möglichst einfachen Ablauf zu erreichen, empfiehlt es sich, diese mit dem Zahlungsvorschlag zu exportieren. Die dadurch erzeugten Buchungsdaten werden so gekennzeichnet, dass beim Import des Kontoauszuges diese eindeutig erkannt werden können. Dadurch ersparen Sie sich die Zuordnungsarbeit.

{{% alert title="ACHTUNG" color="warning" %}}
Sie hantieren hier mit XML Dateien welche für versierte IT-Benutzer **Daten im Klartext** enthalten. Das bedeutet, wenn diese Dateien auf allgemein zugänglichen Stellen lagern, können diese auch von allen gelesen werden. So findet man hier z.B. Ihren aktuellen Kontostand, das Nettogehalt des Kollegen, die Geschäftsführervergütung, u.v.a.m.. Bedenken Sie dies, wenn Sie mit diesen Daten arbeiten. Ihr IT-Betreuer oder gerne auch Ihr **Kieselstein ERP** Betreuer unterstützt Sie bei diesen wichtigen Sicherheitsüberlegungen.
{{% /alert %}}

Voraussetzungen für den Sepa Export

Für den Sepa Export und den nachfolgenden Import in Ihr Buchhaltungsprogramm muss für die sichtbaren Bankverbindungen das Sepa Exportverzeichnis definiert sein. [Siehe](index.htm#Eigene Bankverbindungen).

Fehlermeldungen die beim SEPA Export auftreten können:

Die Bank hat keinen Ort hinterlegt.<br>
![](SEPA_Export_Ort_fehlt.jpg)

Sollte die Fehlermeldung kein SEPA Pfad in der Bankverbindung hinterlegt lauten, so definieren Sie diesen bitte unter [Bankverbindung](index.htm#Eigene Bankverbindungen).

Die exportierten Daten werden im angegebenen SEPA Pfad im Unterverzeichnis Sepaexport abgespeichert.
![](Sepa_Export_Bestaetigung.jpg)

Infos zur Datenübergabe / Einzelauftrag <-> Sammelauftrag
Derzeit werden, aus Gründen der Übersichtlichkeit, die einzelnen Zahlungsaufträge exportiert und sollten so auch in Ihre Bankingsoftware übernommen werden.

**Welche Texte werden übergeben ?**
Beim SEPA Export stehen leider Zahlungsreferenz bzw. Verwendungszweck nur alternativ zur Verfügung.<br>
D.h. sind bei der Eingangsrechnung im Feld Kundendaten für E-Banking Daten eingegeben, so werden diese Daten als Zahlungsreferenz übergeben. Ist hier kein Eintrag, so wird der Verwendungszweck mit maximal 140 Zeichen in folgender Reihenfolge befüllt:
1. Lieferantenrechnungsnummer, 
2. Ihre Eingangsrechnungsnummer, 
3. optional falls mit Skonto eine Info darüber, z.B. "EUR 1143.46 -2% Skonto", also der eigentliche Zahlungsbetrag und Angabe des Skontos.

Bitte beachten Sie dazu auch den Parameter EINGANGSRECHNUNG_KUNDENDATEN_VORBESETZEN, mit dem gesteuert werden kann, ob automatisch die Kundennummer aus dem Lieferantenstamm in eine neue Eingangsrechnung übernommen werden sollte oder eben nicht.

<a name="SEPA Auftrags-Import"></a>Einstellungen an ELBA für den SEPA-Import

Zuerst ist ein Profil für den Import anzulegen.
Im aktuellen ELBA 5 finden Sie dies unter
![](Auftrags_Importdefinition.jpg)

Nun klicken Sie auf neu und geben den Pfad zur Sepa-Importdatei an.
Diese finden Sie unter zv_export_sepa.xml im für den Sepa-Import der Kontoauszüge hinterlegten Pfad, mit der Ergänzung Sepaexport.<br>
Der Dateityp wird damit automatisch als XML erkannt, womit alle weiteren erforderlichen Definitionen gegeben sind.<br>
Bitte beachten Sie, dass bei der Angabe des Dateinamens auch Leerzeichen z.B. ganz am Ende mit berücksichtigt werden, wodurch Elba die Datei nicht erkennt und auch nicht findet.<br>
Klicken Sie nun auf Ok um die Definition zu speichern.

Nun kann mit Klick auf Import fortgesetzt werden.<br>
Um den Import als einzelne Aufträge zu sehen, beantworten Sie im ELBA 5 Importassistenten die nachfolgende mit Klick auf Nein,<br>
![](SEPA_Import3.jpg)
da Sie diese als Einzelaufträge importieren möchten. D.h. bitte Import als Aufträge anhaken und gegebenenfalls den Haken bei als Sammler entfernen.<br>
![](SEPA_Auftrags_Art_Import_Definition.jpg)<br>
**Wichtig:** Damit eröffnen Sie dem Benutzer von ELBA die Möglichkeit die aus **Kieselstein ERP** übertragenen Daten in jeglicher Form zu verändern.<br>
Wir sehen dies als Vorteil, insbesondere wenn Sie nach dem Zahlungslauf feststellen, dass Sie eine Zahlung doch nicht durchführen möchten.<br>
**Zusätzliche Info:** Wenn der Zahlungslauf aus **Kieselstein ERP** exportiert wurde, wird damit in **Kieselstein ERP** KEINE Zahlungsbuchung vorgenommen (aus obigem Grund, aber auch weil nicht in jedem Falle sichergestellt ist, dass die Bank Ihren Zahlungsauftrag durchführt). Nutzen Sie daher auch den Import des Kontoauszuges. Mit diesem werden die tatsächlich durchgeführten Zahlungen importiert.

![](SEPA_Import2.jpg)

**Hinweis:** Sollten Sie beabsichtigter Weise eine Zahlung, die über den Import eingespielt wurde, aus Ihrer Bankingsoftware gelöscht haben, so sollten Sie diese auch aus dem Zahlungsvorschlag als nicht exportiert kennzeichnen. Hintergrund: Bei der Berechnung der nächsten Zahlung werden die bereits exportierten Zahlungen berücksichtigt - also würde diese Eingangsrechnung nicht mehr als zu bezahlen aufscheinen.

SEPA Lastschriftverfahren
-------------------------

Um das automatische Lastschriftverfahren verwenden zu können benötigen Sie die entsprechende **Kieselstein ERP** Lizenz. Bitte wenden Sie sich vertrauensvoll an Ihren **Kieselstein ERP** Betreuer, damit diese freigeschaltet wird.
Der Grundgedanke des Lastschriftverfahrens ist, dass gleichzeitig mit den Mahnungen der (Ausgangs-)Rechnungen anhand der Zahlungsziel-Definitionen der Rechnungen auch die Lastschriften definiert werden können. D.h. damit eine Rechnung in den Lastschriftvorschlag aufgenommen wird, muss das Zahlungsziel
- a.) mit Lastschrift definiert sein
![](Lastschrift1.jpg)
- b.) Es muss anhand der Mahnungsfälligkeit (Rechnungsdatum + Anzahl Tage netto + Mahntage (der 1.Mahnstufe) ) die Rechnung fällig sein

Damit die SEPA Lastschrift durchgeführt werden kann, muss bei der Bankverbindung des Kunden das SEPA Mandat eingetragen sein ([siehe](../Rechnung/Gemeinsamkeiten_der_Bewegungsmodule.htm#Zahlungsziel mit Lastschrift)) und<br>
für Sie als Anwender die SEPA Gläubiger ID (System, Mandant, Vorbelegungen 2) auch Creditor ID genannt.

Nun werden beim Mahnlauf auch die Rechnungen mit berücksichtigt, bei denen beim Zahlungsziel Lastschrift angehakt ist.<br>
Diese erscheinen nun im oberen Modulreiter Lastschriftvorschlag beim jeweiligen [Mahnlauf]( {{<relref "/verkauf/rechnung/mahnen">}} ).<br>
Prüfen Sie bitte die aufgelisteten Rechnungen, korrigieren Sie gegebenenfalls den Betrag oder löschen, falls der Zahlungseinzug doch nicht erfolgen sollte, die Lastschrift aus dem Vorschlag.

Durch Klick auf ![](Lastschrift2.gif) SEPA Lastschrift exportieren, werden alle Lastschriften im SEPA Format (Camt ) exportiert.<br>
Die exportierte XML Datei wird im SEPA-Verzeichnis Ihrer [Bank](#welche-bank-wird-für-die-sepa-lastschrift-verwendet) exportiert.

Beim Export der Lastschriften werden die Daten überprüft und bei fehlenden Daten die entsprechenden Fehlermeldungen ausgegeben.<br>
Bitte handeln Sie entsprechend.<br>
![](Lastschrift3.jpg)<br>
So bedeutet obige erste Zeile, dass für den Mandanten noch keine [Lastschrift-Bank](#welche-bank-wird-für-die-sepa-lastschrift-verwendet) definiert ist.<br>
Die zweite Zeile bedeutet, dass beim Kunden aus der Rechnung 65 keine SEPA Mandatsnummer hinterlegt ist. [Siehe]( {{<relref "/verkauf/gemeinsamkeiten/#zahlungsziel-mit-lastschrift">}} ).
<br>
Um den Lastschriftexport durchführen zu können, pflegen Sie bitte die Daten nach und klicken Sie dann erneut auf SEPA Lastschrift exportieren.

Konnte der Export erfolgreich durchgeführt werden, so erscheint die Meldung wohin die Datei gespeichert wurde. Z.B.:
![](Lastschrift4.jpg)

Wechseln Sie nun in Ihre Bankensoftware und spielen Sie diese Datei ein.

Für ELBA ist die Vorgehensweise wie folgt:
- wählen Sie links importieren
- Wählen Sie beim ersten Mal Neu und geben obige Datei an (Trick, diesen Pfad aus der Bankdefinition der Finanzbuchhaltung kopieren)
- Damit ist der Dateiaufbau automatisch auf XML festgelegt
- wählen Sie nun importieren. Sollten hier Fehlermeldungen auftreten, so handeln Sie bitte entsprechend.
  Info: Die Creditor ID muss ohne Leerzeichen in **Kieselstein ERP** erfasst sein.
  nachdem die Korrekturen erfolgt sind, muss die Lastschrift noch einmal exportiert werden. Bitte bestätigen Sie die erscheinende Meldung entsprechend.
![](Lastschrift5.jpg)

Nach dem erfolgten Import finden Sie nun die Lastschrift(en) als Sammelauftrag in Ihrer Bankensoftware. Handeln Sie nun wie Sie auch bisher mit den Übertragungen an die Bank vorgegangen sind.

Info: Da ja ein Einzug von Geld erfolgt, erscheint in den Überweisungsaufträgen der Einziehungsbetrag natürlich negativ (und oft auch in Rot).

#### Welche Bank wird für die SEPA Lastschrift verwendet?
<a name="SEPA Lastschrift Bank"></a>
Dies wird in der Bankverbindung, Modul Finanzbuchhaltung, definiert. Bitte beachten Sie, dass jeweils nur eine Bankverbindung als SEPA-Lastschrift-Bank verwendet werden kann.

#### Welche Mandatsreferenznummer sollte verwendet / eingetragen werden?
Es hat sich hier inzwischen eingebürgert, hier der Einfachheit halber die Debitorennummer Ihres Kunden einzutragen.

#### Kann ein neuer Mahnlauf erstellt werden, wenn noch offene Lastschriften gegeben sind?
Nein. Exportieren Sie bitte alle Lastschriften oder löschen Sie diese aus dem Reiter Lastschriftvorschlag.

