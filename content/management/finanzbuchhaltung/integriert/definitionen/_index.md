---
categories: ["Finanzbuchhaltung"]
tags: ["Definitionen"]
title: "Definitionen"
linkTitle: "Definitionen"
weight: 210
date: 2023-01-31
description: >
 Einstellungen deiner integrierten Finanzbuchhaltung
---
DB, DZ, Lohn müssen Kontoart Abgaben sein
Konto Blätter mit Sortierung nach Zahlungsterminen siehe https://docs.kieselstein-erp.org/docs/installation/10_reportgenerator/reportsammlung/

ACHTUNG: Weitere Abgaben wie Kommunalsteuer, Tourismusabgaben, Rundfunkgebühren und ähnliches darf von der Kontoart her NICHT als (Finanz-) Abgaben definiert werden.
