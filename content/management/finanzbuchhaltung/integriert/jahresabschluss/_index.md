---
categories: ["Finanzbuchhaltung"]
tags: ["Jahresabschluss"]
title: "Jahresabschluss"
linkTitle: "Jahresabschluss"
weight: 980
date: 2023-01-31
description: >
 Arbeiten rund um den Jahresabschluss
---
Jahresabschluss
===============

Nachfolgend eine lose Aufstellung der Arbeiten die rund um den Jahresabschluss auszuführen sind.

Vorab:

Dein Kieselstein ERP ist so programmiert, dass rund um den (Geschäfts-) Jahreswechsel zwar Arbeiten durchzuführen sind. Es können sich diese Arbeiten jedoch über einige Wochen, teilweise Monate hinweg ziehen. So ist es aus Programmgründen nicht erforderlich, die Inventur während des Jahreswechsels durchzuführen.<br>
Auch das vor- und rückdatieren von Rechnungen wird entsprechend unterstützt.<br>
Beachte bitte, dass es durchaus Regelungen in deinen Unternehmen geben kann, die einen raschen Abschluss erfordern. Diese sind organisatorisch bedingt.

Im Sinne der leichteren Lesbarkeit sprechen wir hier nur vom Jahreswechsel. Je nach der Situation in deinem Unternehmen und den steuerlichen Gegebenheiten ist damit der Geschäftsjahreswechsel gemeint.

## Arbeiten bei integrierter Finanzbuchhaltung
Abhängig vom Abgabestichtag deiner letzten UVA (Umsatzsteuervoranmeldung) musst du rechtzeitig alle Buchungen im alten Jahr durchgeführt haben.<br>
Somit ergibt sich für die verschiedenen Länder unterschiedliche Stichtage:
| Land | Stichtag | zu beachten |
|--- | --- | --- |
| AT | 15.2. | in Abhängigkeit des Geschäftsjahres ev. später |
| DE | 10.1. | ohne Dauerfristverlängerung, **immer** zum Kalenderjahr |
| DE | 10.2. | mit Dauerfristverlängerung, **immer** zum Kalenderjahr |
| CH | 28.2. | Beachte ev. Fristverlängerungen |
| LI | 28.2. | Beachte ev. Fristverlängerungen |

Idealerweise kombinierst du diese Arbeiten auch mit den Vorbereitungsarbeiten für die Unterlagen an deinen Steuerberater. D.h auch die Abstimmung der Verrechnungskonten.

Bei dieser Beschreibung gehen wir davon aus, dass das Vorjahr bereits ordentlich abgeschlossen ist. Sollte dem nicht so sein, schließe zuerst das Vorjahr sauber ab. Dass die Bankkonten zu 100% stimmen, ist selbstverständlich.

### Arbeiten noch vor dem Jahreswechsel
- Abstimmung der Verrechnungskonten.<br>
Diese sollten ausgeglichen sein. Gerade bei Verrechnungskonten gegenüber beherrschenden Personen, beachte die gesetzlichen Grenzen. In der Regel gelten gelegte Rechnungen als zugeflossen.

### Arbeiten unmittelbar nach dem Jahreswechsel
- Abschließen aller Bankbuchungen im alten Jahr. Also auch Kredite u.ä.
- Eröffnen der Bankbuchungen im neuen Jahr.

### Erfassung der alten Rechnungen
Damit deine letzte UVA, des alten Jahres stimmt, müssen alle Rechnungen (Eingang, Ausgang, Gutschrift, Ausgangsgutschriften, ...) und alle Buchungen für das alte Jahr erfasst sein.

### Vorbereitung der letzten UVA des Jahres
1. Prüfen der OP-Listen (offenen Posten)
    - sowohl aus Sicht des ERP 
    - als auch aus Sicht der Fibu (Finanzbuchhaltung)
    - gegebenenfalls ist in der Fibu die Übernahme der Debitoren / Kreditoren aus dem davor liegenden Geschäftsjahr erforderlich. Man erkennt dies daran, dass Zahlungsbuchungen auf den Debitoren / Kreditoren Konten für Rechnungen aus dem Vorjahr gebucht sind, aber keine Forderungen / Verbindlichkeiten dafür vorhanden sind.
    - gegebenfalls muss dafür die UVA-Verprobung bis zurück zum Jänner / Januar zurückgenommen werden.<br>**Hinweis:**<br>Da durch die Zurücknahme der UVA-Sperre eine Buchung im gesamten Geschäftsjahr möglich ist, empfiehlt es sich dies am Tagesrand, zu Zeiten in denen sonst niemand in der Fibu bucht, durchzuführen.
    - Aus dem Vergleich der beiden Listen siehst du, welche Buchungen noch gegeneinander auszuziffern sind. Im normalen Ablauf wird die Auszifferung automatisch erkannt. Wenn es aber zu Zahlungsausgleich mit Gutschriften, gegebenenfalls über mehrere Belege kommt, ist die manuelle Auszifferung erforderlich.<br>Hier hat sich bewährt, in der Darstellung des Debitoren / Kreditoren-Kontos das nur offene zu verwenden.<br>Fange bei den offensichtlichen Buchungen an, damit bleiben die etwas komplexeren Buchungen übrig, die dann meist zum Schluss auch logisch aufzulösen sind.
    ![](Ausziffern.png)
    - Nach dem Ausziffern müssen beide Liste für jedes Konto und insgesamt den selben Saldo ergeben, wobei wir hier von Brutto-Summen, also inkl. Mehrwertsteuer sprechen.

2. Jahres-UVA
Der Hintergrund für diese Überlegung ist, dass es im Laufe des Geschäftsjahres doch immer wieder vorkommt, dass Buchungen für bereits an das Finanzamt gemeldete Zeiträume durchgeführt werden müssen. Üblicherweise sollten sich aus solchen Buchungen keine (nennenwerten) Änderungen deiner Umsatzsteuerzahllast ergeben. Sollte dem so sein, ist mein Rat, diese Schuld an das Finanzamt ehestmöglich, z.B. mit der nächsten UVA auszugleichen.<br>
Damit du dir sicher sein kannst, dass du, über das Jahr nichts vergessen hast, machen wir diese Aufstellung der Jahres-UVA.

Du benötigst dafür die monatlichen / quartalsweise Meldungen an dein(e) Finanzamt.<br>
Diese trägst du in eine der Tabellenkalkulationsvorlagen ein. Die Vorlagen findest du unter [http://kieselstein_erp:8080/clients/import-files/](http://kieselstein_erp:8080/clients/import-files/) (Ersetze das Wort kieselstein_erp mit der IP Adresse deines Kieselstein-Servers. Du findest diese auch in der Titelleiste deines Kieselstein ERP Clients ![](Client_Adresse.png))

![](Client_download.png)

![](JahresUVA_Vorlagen_Download.png)

Solltest du auch OSS Meldungen machen müssen, ergänze die Aufstellung entsprechend.

Wichtig:<br>
Trage in die Spalten der Monate 1 - 11 die tatsächlich an das Finanzamt gemeldeten Werte, z.B. aus den gedruckten Elster / Finanzonline Meldungen, ein.<br>
Beachte dass der errechnete Monatssaldo auf den Cent mit dem gemeldeten Wert übereinstimmt.

Hinweis:<br>
Die Vorlage für AT ist auf 20% MwSt aufgebaut, bzw. die Vorlage für DE auf 19%. Sollten sich diese Werte ändern, bitte entsprechend anpassen.


