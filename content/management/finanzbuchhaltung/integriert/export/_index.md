---
categories: ["Finanzbuchhaltung"]
tags: ["Export"]
title: "Buchungsjournal exportieren"
linkTitle: "Buchungsjournal exportieren"
weight: 400
date: 2023-01-31
description: >
 Das Buchungsjournal exportieren
---
Für Überleitung in die Fibu deines Steuerberaters bzw. auch für Finanzamtsprüfungen und ähnliches

Export des Buchungsjournals in ein fremdes Finanzbuchhaltungsprogramm

Diese Funktion steht nur bei integrierter Finanzbuchhaltung zur Verfügung.
Sie finden diese im Modul Finanzbuchhaltung - unterer Reiter Buchungsjournal - oberes Menü Buchungsjournal. Hier finden Sie Export. In diesem Export stehen vor allem das DATEV EXTF-Format und das HV RAW Format, für den Export zur Finanzprüfung, zur Verfügung.
Mit dem Export des gesamten Buchungsjournals werden die Daten z.B. an die Buchhaltungssoftware Ihres Steuerberaters übergeben. Er, als Ihr steuerlicher Berater, übergibt, nach den erforderlichen Abstimmungsarbeiten, die Daten an die Finanz usw. Das Ziel ist, dass nach dem Exportlauf alle Daten in der Buchhaltung des Steuerberaters sind. Ein wichtiges Kriterium ist, dass nach der Übernahme die Saldenlisten übereinstimmen müssen. Also die von **Kieselstein ERP** aus ausgedruckte Saldenliste muss mit der des Steuerberaters unmittelbar nach der Übernahme ausgedruckten Saldenliste übereinstimmen.

## DATEV Export

Der Mandant des Buchungsjournals wird über das Pflichtfeld Kostenstelle definiert.
Der Mandant greift natürlich auch beim Export des jeweiligen Journals.

Es werden alle Buchungen des Buchungsjournals im CSV Format (Strichpunkt getrennt) exportiert.
Der Datensatzaufbau ist 
Lfd_Nr.;WKZ;Umsatz;S/H;BU;Gegenkonto;Belegfeld 1;Belegfeld 2;Datum;Konto;Skonto;Buchungstext;EU-Informationen;Umsatz-BW;Kurs;WKZ-BW;Storniert

WKZ= Währungskennzeichen
BU=Buchungsschlüssel
S=Soll, bezogen auf die Spalte Konto
H=Haben, bezogen auf die Spalte Konto

ergänzt um weitere Informationen wie Stornierte Buchung usw.
Splittbuchungen werden so übergeben, dass für jeden Steuersatz / jedes Konto ein eigener Datensatz übergeben wird 
Der Export erfolgt immer für alle Finanzämter für einen ausgewählten Zeitraum des Buchungsdatums (Belegdatum).

Eine Verriegelung der exportierten Daten erfolgt NICHT.
Es MÜSSEN die Saldenlisten (**Kieselstein ERP** und die des Steuerberaters nach dem Import) übereinstimmen.

Tun sie dies nicht, müssen die Überleitungsfehler warum auch immer gesucht und beseitigt werden. Wir benötigen dafür die Unterstützung des Steuerberaters.<br>
Es werden in den Export zusätzlich drei Filter eingebaut:
- a.) Unterdrückung der Übergabe der automatischen Eröffnungsbuchungen
- b.) Unterdrückung der Übergabe der mit Eröffnungsbuchung gekennzeichneten manuellen Buchungen
- c.) Unterdrückung der Übergabe der Automatik-Buchungen

Dies deshalb, da anzunehmen ist, dass diese Buchungen vom Finanzbuchhaltungsprogramm Ihres Steuerberaters ebenfalls auch automatisch gemacht wird.
Zusätzlich werden auch die stornierten Buchungen exportiert um den GDPDU Forderungen zu entsprechen.

Parameter die hierfür je nach Installation eingestellt werden müssen:<br>
Kategorie FINANZ
- EXPORT_DATEV_BERATER = 
- EXPORT_DATEV_MANDANT= 
- EXPORT_DATEV_KONTOKLASSEN_OHNE_BU_SCHLUESSEL = 
- EXPORT_DATEV_MITLAUFENDES_KONTO =

Zusätzlich muss ein eindeutiger Fibu-MwSt Code definiert werden. Dies ist gegebenenfalls mit dem Import bei deinem Steuerberater abzustimmen.<br>
Die Definition der Fibu-MwSt Code wird unter System, Mandant, oberer Reiter MwSt vorgenommen. Hier muss für jeden verwendeten MwSt-Satz in Kombination mit der ReversechargeArt eine entsprechende Definition vorgenommen werden.<br>
Das könnte z.B. wie folgt aussehen:<br>
- Allgemeiner Steuersatz: ![](FIBU-MWST_Code01.png)
- reduzierter Steuersatz: ![](FIBU-MWST_Code02.png)
Kommt beim Export die Fehlermeldung "es konnte kein eindeutiger FIBU-MWSTCode gefunden werden"<br> ![](Datev_Export_Fibu-Mwst_Code_nicht_eindeutig.png)<br>, so ist obige Definition unvollständig.<br>
Um weitere Details herauszufinden, kann man auch das Ausgangsrechnungsjournal mit Details Erlöse angehakt ausdrucken um die fehlende / doppelte Definition herauszufinden.
