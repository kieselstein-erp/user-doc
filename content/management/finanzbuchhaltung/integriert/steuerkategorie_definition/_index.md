---
categories: ["Finanzbuchhaltung"]
tags: ["Steuerkategorie"]
title: "Steuerkategorie"
linkTitle: "Steuerkategorie"
weight: 200
date: 2023-01-31
description: >
 wie mit den Steuerkategoriesn arbeiten
---
Definition der Steuerkategorie
==============================

Gerade für die Covid Mehrwertsteuerumstellung ist auch eine besondere Behandlung der Ust-Konten in Abhängigkeit der Belegdatum = Leistungsdatum erforderlich. Aus diesem Titel heraus müssen Steuerkategorien und Länderartübersetzungen Kalenderfähig sein. Womit die Gültigkeit zum jeweiligen Datum definiert werden kann.

Eine für nur einen Mehrwertsteuerzeitraum erstellte Steuerkategorie-Definition könnte z.B. wie folgt aussehen:
![](Steuerkategorie1.jpg)

Bitte beachten Sie dazu die Spalte Gültig ab und das Aussehen der letzten Spalte, welche zwischen Löschen ![](Steuerkategorie_loeschen.gif) und Neu ![](Steuerkategorie_neu.gif) wechselt.

Beachten Sie auch das Gültig zum Datum, mit dem die Anzeige der jeweiligen Steuerkategorie gesteuert wird.

Mit Löschen ![](Steuerkategorie_loeschen.gif) wird die jeweilige Definitionszeile des gewählten MwSt-Satzes entfernt. Sollten alle Definitionen für einen Mehrwertsteuersatz entfernt worden sein, so wird automatisch, beim neu Start des Finanzbuchhaltungsmodules und dem Aufruf des Modulreiters Steuerkategoriekonto eine leere Definition mit Gültig ab 1.1.1970 nachgetragen.
Mit Neu ![](Steuerkategorie_neu.gif) wird eine neue Definition des gewählten MwSt Satzes zu einem anderen Gültig ab Datum erstellt. Um neue Definitionen anlegen zu können wählen Sie vorher ändern ![](Steuerkategorie_aendern.gif) (aller) Steuerkategoriedefinitionen.
Um die Erfassung bei Neu möglichst einfach zu gestalten, wird nach dem Klick auf Neu nur das Gültig ab Datum gelöscht, womit Sie zur Erfassung des neuen Stichtages, ab dem die andere Definition gilt, aufgefordert werden.

So könnte z.B. nach erfolgter Definition der MwSt Änderungen ab dem 1.7.2020 die Darstellung für das Gültig zum 1.7.2020 wie folgt aussehen.
![](Steuerkategorie2.gif)

#### Die Buchung der Erlöse / Aufwände egal ob 16 oder 19% gehen auf die gleichen Konten
Definieren Sie einerseits obige Übersetzungen für die entsprechenden Zeiträume
Und definieren Sie auch die Länderartübersetzungen, gegebenenfalls auch für das Inland inkl. der Zeiträume ab denen diese gültig sind.<br>
Für die Klarstellung nutzen Sie bitte gegebenenfalls auch das Journal Konten, den Kontenplan.

Stellen Sie nun die Länderartübersetzung richtig.
![](Zusatz5.jpg)

Nun korrigieren Sie die falschen Buchungen wie z.B.:
![](Zusatz6.gif)

In dem Sie in diesem Falle in die Eingangsrechnung gehen und
![](Zusatz7.gif)
im Menü Bearbeiten, Belegübernahme wählen.
Damit werden die Buchungen storniert und nach den neu definierten Regeln übernommen.

Somit sieht die Buchung wie folgt aus:
![](Zusatz8.gif)

#### Wie stellt man das wieder zurück?
Voraussichtlich ab 1.1.2021 greifen wieder die bisherigen Mehrwertsteuersätze. Das bedeutet, dass in den Kontoländerartübersetzungen die dann neue Gültigkeit ab 1.1.2021 auf das eigene Konto eingetragen werden muss. D.h. eigentlich zeigt die Länderartübersetzung auf sich selbst, womit die Definition wie folgt aussieht.
![](Zusatz10.gif)