---
categories: ["Finanzbuchhaltung"]
tags: ["Integrierte Finanzbuchhaltung"]
title: "Einnahmen- Ausgabenrechner"
linkTitle: "Einnahmen- Ausgabenrechner"
weight: 600
date: 2023-01-31
description: >
 Besonderheiten in der integrierte Finanzbuchhaltung für Einnahmen- Ausgabenrechner
---
Einnahmen / Ausgaben Rechner
============================

Im Gegensatz zum Bilanzierer/Sollversteuerer versteuert der Einnahmen/Ausgaben Rechner (= Istversteuerer) nur die vereinnahmten bzw. geleisteten Zahlungen. Dies gilt vor allem für den Bereich der Ausgangsrechnungen. Bei der Erfassung der Eingangsrechnung hängt es von den unterschiedlichen Gepflogenheiten / gesetzlichen Regelungen ab, ob auch hier nur die geleisteten Zahlungen berücksichtigt werden, oder ob die erhaltenen Eingangsrechnungen nach Eingangsrechnungswert und Datum sowohl für die UVA (Umsatzsteuer Voranmeldung) als auch für die Gewinnermittlung herangezogen werden.

Ist-Versteuerer bzw. Einnahmen- / Ausgabenrechner bzw. Verrechnung nach vereinnahmten Umsätzen.

Die Realisierung der verminderten Funktionalität für die EA-Rechner ist in **Kieselstein ERP** so umgesetzt, dass bei der Bankbuchung immer zwei Buchungen durchgeführt werden.
- a.) Die Buchung Erlöse gegen Debitor und gleichzeitig
- b.) die Buchung Debitor gegen Bank

Diese beiden Buchungen sind zusätzlich noch miteinander gekoppelt, sodass sichergestellt ist, dass bei Änderungen auch immer wieder beide Buchungen gelöscht werden.

Bitte beachten Sie, dass es bei den EA-Rechnern in der Fibu keinen Skontoaufwand, keine offene Postenliste gibt. Diese Daten sind, dank Ihrer **Kieselstein ERP** Rechnungs- / Eingangsrechnungsverwaltung trotzdem verfügbar. Jedoch auf Basis der Rechnungsmodule (siehe Journale) und nicht in der Fibu.

UVA, Sachenkonten Saldenliste usw. funktionieren gleich wie in der Fibu für die Sollversteuerer.

#### Wozu nun Debitoren-, Kreditoren-Konten wenn es doch ein EA-Rechner ist.
Hintergrund ist, dass auf den Debitoren- / Kreditorenkonten auch die Definition der Steuerkategorien hinterlegt sind. D.h. es wird damit bestimmt welcher Debitor in welche Steuerkategorie fällt und somit werden auch die Buchungsregeln bestimmt. Für die Übergabe Ihrer Daten an Ihren Steuerberater genügt die Übergabe der Saldenliste.

#### Wieso sind einige Rechnungen, obwohl noch aus dem alten Geschäftsjahr nicht in der Fibu?
Wenn diese Rechnungen noch nicht (teil-) bezahlt sind, so wurden diese, da wir hier ja von EA-Rechnern sprechen auch noch nicht in die Fibu übernommen und können so jederzeit geändert werden.

#### Wie vorgehen bei der Umstellung von EA-Rechner auf Sollversteuerer?
Diese Umschaltung muss von Ihrem **Kieselstein ERP** Betreuer vorgenommen werden. Für die Abstimmung der Eingangs-/Ausgangsrechnungen müssen die Eröffnungsbuchungen manuell / per Hand nachgebucht werden.

#### Wie kann die Funktionalität des EA-Rechners aktiviert werden?
Bitten Sie Ihren **Kieselstein ERP** Betreuer diese Funktion (ISTVERSTEURER) für Sie freizuschalten.

Bitte beachten Sie auch hier die Trennung der Konten in Sachkonten und Personenkonten.

Einer der Nachteile der Buchhaltung nach Einnahmen / Ausgaben Rechner ist, dass es für die Erlöse keine Trennung nach Erlöskonten gibt, sondern dass, mit Ausnahme der Trennung nach Länderarten alles in einen großen Topf geworfen wird.

<a name="Wozu Personenkonten"></a>

#### Wozu Personenkonten?
Die sogenannten Personenkonten, also Debitoren für die Kunden und Kreditoren für die Lieferanten, dienen dazu Ihre Forderungen (bei den Kunden) und Ihre Verbindlichkeiten (bei den Lieferanten) detailliert für den jeweiligen Debitor/Kreditor darzustellen. Für Sie ist ja neben der Summe der offenen Forderungen insgesamt auch wichtig, bei welchem Kunden Sie welche Werte, also welche Rechnungsbeträge offen haben. Dass dieser Gesamtsaldo der offenen Forderungen sich dann auch in einem Summenkonto in Ihren Sachkonten niederschlägt ist für die Richtigkeit der Buchhaltung essentiell. Aber genauso müssen Sie wissen, bei welchem Kunden welche Ihrer Rechnungen offen sind. Die gleiche Betrachtung gilt analog für die Kreditoren, bei denen Sie Ihre offenen Verbindlichkeiten je Lieferant wissen müssen.

#### Welche Sachkonten sollten angelegt werden?
Bitte beachten Sie, dass in **Kieselstein ERP** in aller Regel nicht der gesamte Kontoplan der Sachkonten angelegt werden sollte, sondern nur die tatsächlich benötigten Konten. D.h. Konten wie Anlagegüter, Finanzamtskonten, Konten der Personalverrechnung (Lohnkonten), Bilanzierungskonten werden in aller Regel nicht benötigt. Im Gegenteil empfiehlt sich viel mehr nur die ganz wenigen benötigten Konten anzulegen. Das sind üblicherweise Warenerlöse in den verschiedenen Steuersätzen und in den verschiedenen Ländern und die Aufwandskonten wie Wareneinsatz, Porti usw. Hier ist es sicherlich so, dass die Anlage möglichst weniger Konten mehr Sicherheit vor allem bei der Erfassung der Eingangsrechnungen darstellt. Bitte legen Sie auf keinen Fall Gruppierungskonten oder ähnliche Strukturierungen an, welche ausschließlich in der Bilanz als Gruppensummen benötigt werden.

####  Wie nutze ich nun die Funktionen von **Kieselstein ERP** um einfachst / kostengünstig die Daten zu meinem Steuerberater zu übergeben bzw. die UVA monatlich selber zu erstellen ?
Voraussetzungen: Es werden die Ausgangsrechnungen, die Gutschriften, die Eingangsrechnungen und die Zahlungen im **Kieselstein ERP** erfasst.

Für die UVA benötigen Sie im wesentlichen folgende Daten:

Zahlungseingänge im Zeitraum getrennt nach
- Inland steuerfrei
- Inland reduzierter Steuersatz
- Inland normaler Steuersatz
- EU-Ausland
- Drittland

Eingangsrechnungseingang im Zeitraum getrennt nach
- Inlands Vorsteuerbetrag
- EU-Ausland mit theoretischem reduzierten Steuersatz
- EU-Ausland mit theoretischem normalen Steuersatz

Wo finden Sie nun diese Auswertungen:
-   Zahlungseingänge:<br>
    Siehe bitte Ausgangsrechnungen, Journal, Zahlungseingang
-   Eingangsrechnungen gruppiert nach Konten:<br>
    Siehe bitte Eingangsrechnungen, Journal, Kontierung und wählen Sie bei Eingangsrechnungen nun Alle. Durch die Wahl des Kontierungsjournales erhalten Sie bereits die Aufteilung der Kontierung für Ihren Jahresabschluss. Während des Jahres bzw. für die Erstellung der UVA sind diese Zahlen lediglich aus Kontroll- und Übersichtsgründen von Vorteil.
-   Alternativ kann bei Bedarf für die Eingangsrechnungen auch das Zahlungsausgangsjournal verwendet werden. Dieses finden Sie in Eingangsrechnungen, Journal, Zahlungsausgang.

**Hinweis:** Wenn das Kontierungsjournal verwendet wird, so werden die Skontoerlöse nicht berücksichtigt. Dies kann zu geringen Differenzen bei Ihrer Jahres-UVA zu Ihren Gunsten führen.

#### Was ist bei der Umstellung von EA-Rechners auf Sollversteuerer zu beachten?
Der wesentlichste Punkt ist, dass zu diesem Zeitpunkt, der immer ein Geschäftsjahreswechsel sein muss, die Aufgrund der Ist-Versteuerung noch nicht Soll-Versteuerten Aufwände und Erlöse nachversteuert / nachgebucht werden müssen. Das bedeutet es ist dies als Eröffnungsbuchung vom Debitoren/Kreditorenkonto gegen das Erlös/Aufwandskonto manuell durchzuführen. Also von der Idee her die gleiche Buchung wie beim Sollversteuerer bei der Jahres/Periodenübernahme nur eben nicht gegen das Sammelkonto der Eröffnungskonten sondern gegen die Aufwands bzw. Erlöskonten. Dies bewirkt automatisch, dass die nachzuversteuernden Umsatzsteuerbeträge auf der Jänner-UVA aufscheinen.

Gemischte Abwicklung der Vorsteuer / Umsatzsteuer
=================================================

Manche Anwender haben auch den Status, dass für Eingangsrechnungen die entrichtete Vorsteuer anhand des Eingangsrechnungsdatums errechnet wird, die abzuführende Umsatzsteuer jedoch anhand der Zahlungseingänge errechnet wird. Wir sprechen hier von sogenannter Mischversteuerung.
Sollte dies bei Ihnen der Fall sein, so bitten Sie Ihren **Kieselstein ERP** Betreuer dies für Sie entsprechend einzurichten.
