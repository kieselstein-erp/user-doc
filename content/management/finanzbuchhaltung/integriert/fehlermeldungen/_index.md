---
categories: ["Finanzbuchhaltung"]
tags: ["Fehlermeldungen"]
title: "Fehlermeldungen"
linkTitle: "Fehlermeldungen"
weight: 995
date: 2023-01-31
description: >
 Fehlermeldungen beim Buchen und deren Ursache bzw. Behebung
---
Fehlermeldungen der Finanzbuchhaltung lesen
=============================

Da immer wieder (Rück-)Fragen kommen, deren Inhalt beispielsweise ist:
- ich kann nicht weiterbuchen, 
- die Eingangsrechnung erfassen, 
- die Ausgangsrechnung nicht ausdrucken
- die Zahlung nicht erfassen

nachfolgend eine lose Zusammenstellung von Fehlern und deren Ursache.

Vorneweg:<br>
Mein Rat zur Einstellung der integrierten Finanzbuchhaltung in deinem **Kieselstein ERP** ist, richte wirklich nur diejenigen Geschäftsfälle ein, die in deinem Unternehmen vorkommen (sollten). Dies gibt dir die Sicherheit, dass deine Rechnungen richtig verbucht werden.

Aus diesem Gedanken sind die Fehlermeldungen positiv zu sehen. Sie schützen dich vor einer eventuellen Fehlbuchung (auch wenn das manchmal lästig ist).

Wichtig: Wenn beim Verbuchen ein Fehlerfenster angezeigt wird, so steht manchmal ein direkter Fehlerhinweis bereits in diesem Fenster.<br>
Zusätzlich haben diese Fenster immer einen **Detail**-Button. Mit Klick auf diesen, werden wesentlich mehr Informationen angezeigt und gerade in der Fibu steht hier meist auch der Auslöser, warum sich die Buchungslogik weigert, die von dir gewünschte Buchung durchzuführen.

**Wichtig:**<br>
Wenn du nun Anpassungen an deiner Steuerkategoriedefinition vornimmst, so stelle sicher, dass dies den Finanz- und Steuer-Gesetzen deines Landes und deines Business entspricht.<br>
Wie mehrfach betont, lehnen wir jegliche Verantwortung ab. **<u>Frage deinen Wirtschaftstreuhänder oder Steuerberater.</u>**

## Fehler 4113
Die Fehlermeldung des Anwenders:<br>
Komme mit der SEPA Buchung nicht weiter.

![](Fehler_4113.png)

Ursache(n):
- Es wurde versucht eine Skontobuchung auf die Rechnung Nummer 2320 zu machen.
- Die Rechnung ist von der Definition her eine Inlandsrechnung (ev. auf ein abweichendes Finanzamt) mit reduziertem MwSt Satz.
- Für diesen Sachverhalt (in DE auch Umsatzsteuer Tatbestand genannt) ist keine Buchungsregel definiert

Prüfung:
- stimmt die Definition des Debitorenkontos für diesen Sachverhalt, insbesondere Finanzamt (wenn mehrere) und Steuerkategorie und Reversecharge(art) -> Wenn nicht, richtigstellen

Lösung:
- Modul Fibu
- Unterer Modulreiter Finanzamt
- das passende Finanzamt auswählen
- durch auswählen der Reversechargeart, danach Steuerkategorie und dann Steuerkategoriekonto auf den jeweiligen Sachverhalt manövrieren
- hier nun die aus dieser Erkenntnis gewonnenen Definition in diesem Falle das Skontokonto hinterlegen.
- Da es in diesem Falle um eine Ausgangsrechnung geht, ist dies ein Skontoaufwandskonto mit dem definierten Ust-Satz von 7%

## UVA Zahllast stimmt nicht überein
Immer wenn die UVA gedruckt wird, sollte man auch die im Formular errechnete Zahllast mit der aus den Umsatz- und Vorsteuerbuchungen errechneten Zahllasten vergleichen.<br>
Diese sollten annähernd den gleichen Betrag haben. D.h. je nach Geschäftsfeld kann diese Differenz, welche aus den Einzel-Berechnungen <-> Summenberechnungen der Ust kommen, einige wenige Cent(Rappen) bis zu einigen wenigen Euro(CHF) ausmachen. Sind diese größer
![](Zahllast_stimmt_nicht_ueberein.png)  
so muss die Ursache dafür gesucht werden.

In diesem Falle wurden auf das UST- oder Erwerbssteuerkonto (Kontoart) auch zusätzliche Zahlungen an / vom Finanzamt gebucht, womit die Differenz gegeben ist.<br>
![](Differenz_Zahllastkonto.png)  <br>
Diese Buchungen müssen bitte auf das FA Zahllastkonto (Kontoart) erfolgen.