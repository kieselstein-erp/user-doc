---
categories: ["Finanzbuchhaltung"]
tags: ["Integrierte Finanzbuchhaltung"]
title: "Integrierte Finanzbuchhaltung"
linkTitle: "integriert"
weight: 100
date: 2023-01-31
description: >
 Die integrierte Finanzbuchhaltung
---
Integrierte Finanzbuchhaltung
=============================

Neben der Möglichkeit des Exportes der Rechnungsdaten steht auch die integrierte Finanzbuchhaltung in **Kieselstein ERP** zur Verfügung.

Diese Modul ist derzeit für Soll-Versteuerer / Bilanzierer bzw. für die Verrechnung nach vereinbarten Umsätzen gedacht. Zusätzlich steht bei entsprechender Parametrierung auch die direkte Verbuchung für [Einnahmen-Ausgaben-Rechner](#EA-Rechner) zur Verfügung.

Für die Vorgehensweise bei der Aktivierung der integrierten Finanzbuchhaltung [siehe]( {{<relref "/management/finanzbuchhaltung/integriert/starten">}} ).

#### Was ist die **Kieselstein ERP** Finanzbuchhaltung und was ist sie nicht?
Das in **Kieselstein ERP** integrierte Finanzbuchhaltungsmodul ist für die Erledigung der Arbeiten der Finanzbuchhaltung während des laufenden Jahres gedacht. Dies geht von der monatlichen UVA bis hin zur Erfolgsrechnung / BWA / Ergebnisübersicht.<br>
Da für die Erstellung einer Bilanz immer ein geprüfter Bilanzbuchhalter verantwortlich ist, sind zwar die Daten aus **Kieselstein ERP** die Basis für die Bilanz, die eigentliche Bilanz muss jedoch vom Steuerberater / Treuhänder / Bilanzbuchhalter Ihres Vertrauens erstellt werden. [Siehe dazu auch Unterlagen für den Steuerberater](Integrierte_Finanzbuchhaltung_starten.htm#Unterlagen für den Steuerberater) und auch [Infos zur Bilanz](Integrierte_Finanzbuchhaltung_starten.htm#Rund um die Bilanz).

{{% alert title="WICHTIGER HINWEIS: Keine Steuerberatung" color="primary" %}}
<a name="keine Steuerberatung"></a>

**WICHTIGER HINWEIS:**
Die **Kieselstein ERP** eG ist **keine Steuerberatung**. Eine für Sie passende steuerliche Beratung erhalten Sie vom Steuerberater / Wirtschaftstreuhänder Ihres Vertrauens. Werden von uns / unseren Mitarbeitern steuerliche Punkte genannt, so stellen diese unsere Meinung zum jeweiligen Thema dar. Sie sind in keinem Falle eine steuerliche Beratung und keine steuerberatende Tätigkeit. Für steuerliche Beratung wenden Sie sich bitte an Ihren Steuerberater.
{{% /alert %}}

<!-- **HINWEIS:** -->
Wir weisen in diesem Zusammenhang auch darauf hin, dass wir für die richtige Parametrierung Ihrer Buchhaltung, welche jederzeit von Ihnen selbst vorgenommen werden kann, keine wie immer geartete Verantwortung, geschweige denn Haftung übernehmen.

[GDPDU, GoDB siehe bitte]( {{<relref "/management/finanzbuchhaltung/godb">}} ).
[Für Zusatzinformationen siehe bitte]( {{<relref "/management/finanzbuchhaltung/zusatzinfos">}} ).

## Die richtige Kontendefinition oder auch Konten Hinterlegung
<a name="Kontendefinition"></a>
Da es immer wieder zu Fragen bezüglich der Hinterlegung von Konten im Rahmen der Umsatzsteuerkonten Zuordnung kommt, hier eine kompakte Zusammenfassung der zu beachtenden Punkte.

Die Konten-Zuordnungen gliedern sich in vier Ebenen:

1.  Artikel
    Der Artikel wird über die Artikelgruppe einem Inlands-Erlös-Konto zugeordnet.
    Achten Sie darauf, dass die Zuordnung nur für Inlandskonten erfolgen darf, die Ihrem Hauptfinanzamt entsprechen.

2.  Sachkonto
    Im Sachkonto im Fibu-Modul werden bei obigem Inlands-Erlös-Konto im oberen Modulreiter Konto-Länderart die entsprechenden Unterkonten für die jeweilige Steuerkategorie und für das jeweilige Finanzamt hinterlegt. Bitte beachten Sie, dass die Steuerkategorien in den Sachkonten (im Gegensatz zu den Personenkonten) NUR für die Definition des Verhaltens bei manuellen Buchungen verwendet wird. Buchungen bei denen Debitoren/Kreditoren beteiligt sind, werden von der Steuerkategorie dieses Kontos gesteuert.

3.  Kunde / Debitor
    Im Kundenstamm wird unter Konditionen die gewünschte Debitorennummer eingetragen. Diese findet sich im Fibu-Modul unter Debitorenkonto wieder. In den Kopfdaten des jeweiligen Debitorenkontos erfolgt die Zuordnung zum zuständigen Finanzamt und der richtigen Steuerkategorie. Bei der Auswahl der Steuerkategorie bedenken Sie, dass hier immer vom gewählten Finanzamt auszugehen ist.

4.  Finanzamt
    Im Modul Fibu, unterer Modulreiter Finanzamt werden beim jeweiligen Finanzamt im oberen Modulreiter Steuerkategorie für jede Steuerkategorie die Abstimmkonten für die mitlaufenden Buchungen der offenen Forderungen / Verbindlichkeiten festgelegt. Es empfiehlt sich, je Steuerkategorie und Finanzamt ein eigenes mitlaufendes Konto zu verwenden.
    Im oberen Modulreiter Steuerkategoriekonto wird definiert, bei welcher Art des Geschäftes (Verkauf / Erwerb) und bei welchem MwSt Satz welches Umsatzsteuerkonto verwendet werden muss. Zugleich werden hier die Skontoaufwands- und Skontoerlöskonten definiert.
    Definieren Sie im Finanzamt, Kopfdaten auch das zu verwendende Formular (Formularnummer) für die Umsatzsteuervoranmeldung.
    | Formular Nummer | Bedeutung |
    | --- | --- |
    | Leer | Standardformular = Österreich |
    | 1 | Formular für die Elstermeldung = Deutschland. Definiere hier auch "Umsatz runden" |
    | 2 | Formular für die Meldung in der Schweiz |
    | 3 | Formular für die Meldung in Liechtenstein |

    **Wichtig:** Achten Sie darauf, dass die Kurzbezeichnungen der Finanzämter unterschiedlich sind, sodass sie aus Bedienungssicht leicht unterschieden werden können.

### Zusammenhänge Erlöskonto, Länderart und Steuerkategorie
<a name="Konto-Länderart-Übersetzung"></a>

![](Steuerkategorie_Kunde_Rechnung.png)

Der Kunde definiert über die Adresse (Postleitzahl und Länderkennzeichen) die Länderart. [Siehe](#auf-welches-debitorenkonto-wird-gebucht). Je nachdem ob eine UID Nummer eingetragen ist, oder nicht, wird die Steuerkategorie ermittelt.
Aus der Rechnung bewirken die Positionen (Artikel) das Erlöskonto, je nach Kunden und dessen Länderart kann eine Länderübersetzung notwendig sein. 
Ergänzend dazu wirkt die Kennzeichnung Reverse Charge - je nachdem ob das Reverse Charge Verfahren zur Anwendung kommt, wird die Steuerkategorie verwendet.

Sie finden die Definitionen hierzu im Modul Finanzbuchhaltung, unterer Reiter Finanzamt, oberer Reiter Steuerkategorie.
Wählen Sie hier die Steuerkategorie aus und klicken auf den Reiter Steuerkategoriekonto.
![](steuerkategorie_konto.JPG)

In der folgenden Maske hinterlegen Sie durch einen Klick auf Bearbeiten und auf das jeweilige Kontofeld die Konten.<br>
Bitte beachten Sie, dass bei Ust. Verkauf nur Konten der Kontoart Ust Konto und bei Vst. Einkauf nur Konten der Kontoart Vst Konto zugeordnet werden können.
Zu Ihrer Sicherheit definieren Sie bitte nur die üblichen Fälle und nicht alle möglichen Kombinationen.<br>
Im Laufe der Arbeit mit der Integrierten Finanzbuchhaltung von **Kieselstein ERP** decken Sie Ihre Geschäftsfälle ab. Sie erhalten eine Fehlermeldung, dass Konten (Erlöskonto nicht definiert) nicht definiert sind, falls Definitionen noch nicht vollständig sind. In diesem Fall prüfen Sie bitte die Eingaben und tragen die Definition nach. Dies dient auch dazu, Flüchtigkeits- oder Tippfehler zu vermeiden.
[Siehe dazu auch](#Zu beachten bei Kontendefinitionen richtigstellen).

Oder in anderen Worten: Der Kunde definiert über das Land die Länderart (Inland, EU mit UID, EU ohne UID, Drittland). Damit wird die Steuerkategorie des Debitors definiert.<br>
Der Artikel definiert über die Artikelgruppe das Inlands-Erlöskonto.<br>
Länderart, Reverse-Charge und Finanzamt definieren die Konto-Länderart-Übersetzung für das jeweilige Inlands-Erlöskonto um zum nun richtigen Erlöskonto zu kommen.

**Wichtig:**
Beachten Sie bitte, dass für Anzahlungsrechnungen nicht die Erlöskonten eventueller Artikel sondern das Anzahlungskonto "Erhaltene Anzahlungen" (siehe Fibu, Finanzamt, Buchungsparameter) als Grundlage verwendet wird. Auch hier greift die Länderartübersetzung.

Für die [Definition der Steuerkategorie]( {{<relref "/management/finanzbuchhaltung/integriert/steuerkategorie_definition">}} ) mit unterschiedlichen Erlöskonten je Mehrwertsteuerzeitraum [siehe bitte]( {{<relref "/management/finanzbuchhaltung/integriert/steuerkategorie_definition">}} ).

#### Auf welches Debitorenkonto wird gebucht?
Viele Menschen gehen davon aus, dass das Debitorenkonto auch der Kunde ist.<br>
Aufgrund der Umsatzsteuer Definitionen ist dem leider nicht immer so.<br>
Zusätzlich kommt dazu, dass, nicht zuletzt für die automatische Erlöskontenfindung, die Steuerkategorie des Debitorenkontos, das Verhalten der Buchung definiert.

Das bedeutet nun, dass:
- solange Rechnungsadresse und Lieferadresse im gleichen Land sind, wird die Debitorennummer der Rechnungsadresse verwendet
- geht die Lieferung in ein anderes Land als die Rechnung, ist also in der Lieferadresse ein anderes Land als in der Rechnungsadresse definiert, so muss auch der Lieferadresse eine eigene Debitorennummer und damit ein eigenes Debitorenkonto hinterlegt werden.<br>Damit wird gesteuert, wie deine Erlöse verbucht werden.

{{% alert title="Hinweis" color="info" %}}
Die gemischte Verbuchung nach unterschiedlichen Steuerkategorien ist derzeit nicht vorgesehen.
{{% /alert %}}


#### Beim Aufruf der manuellen Buchung erscheint unvollständig
Erscheint beim Aufruf der manuellen Buchung die Fehlermeldung: Die Definition des MwSt-Satz 'Steuerfrei' ist unvollständig, so bedeutet dies, dass die Definition der MwstSatzBezeichnung<br>
![](c_druckname_fehlt.jpg)<br>
unvollständig ist. Bitte deinen **Kieselstein ERP** Betreuer um Ergänzung der Daten 
(C_DRUCKNAME)

#### Welche Schritte sind für eine vollständige Definition der Fibu erforderlich?
Da diese Frage immer wieder an uns herangetragen wird, haben wir nachfolgende Zusammenstellung erstellt.
1.  Gehen Sie nach der Beschreibung [integrierte Fibu starten]( {{<relref "/management/finanzbuchhaltung/integriert/starten">}} ) vor.
2.  Beachten Sie die [Kontendefinition](#Kontendefinition) wie oben beschreiben.
3.  Gehen Sie für die Definition des einzelnen Sachkontos nach der Beschreibung [Definition der Sachkonten](Integrierte_Finanzbuchhaltung_starten.htm#Definition der Sachkonten) vor.
4.  Prüfen Sie die [Kontendefinition inkl. Finanzamt](Integrierte_Finanzbuchhaltung_starten.htm#Prüfen der Kontendefinition) und inkl. Steuerkategorien
5.  Prüfen Sie die [UVA](#UVA Verprobung) des ersten Monats

#### Wie werden Eingangsrechnungen richtig erfasst?
[Siehe bitte.](../Eingangsrechnung/index.htm#ER erfassen)

#### Können Konten kopiert werden?
Ja. Wählen Sie dafür das zu kopierende, sehr ähnliche, Konto aus und klicken Sie auf<br>
![](Konto_kopieren.jpg)<br>
Neu aus Konto.<br>
Damit wird der Erfassungsdialog wie bei der Neuanlage eines Kontos geöffnet und es sind alle Daten aus dem vorausgewählten Konto vorbesetzt.
Passen Sie nun die Kontonummer und die Kontobezeichnung an. Natürlich müssen alle Eigenschaften des Kontos geprüft werden.
Info: Das Gültig ab Datum wird mit dem aktuellen Geschäftsjahresbeginn vorbesetzt.

#### Können Sachkonten importiert werden?
[Ja, siehe]( {{<relref "/insider_tips_und_tricks/importvorlagen/#kontenrahmen-importieren" >}} )

#### Wie vorgehen bei der Übernahme bestehender **Kieselstein ERP** Daten in die Finanzbuchhaltung?
Dafür sind mehrere Schritte notwendig um eine entsprechende richtige Einstellung der Daten zu erhalten.

1.  <a name="Abstimmkonto"></a>Prüfen Sie die Steuerkategorie auf vollständige Definition.
    Hier insbesondere auf die Abstimmkonten der Forderungen und Verbindlichkeiten.
    ![](Fibu_Steuerkategorie.gif)

2.  Definieren Sie die benötigten Steuerkategoriekonten für jeden verwendeten / vorkommenden MwSt-Steuerkategorie.
    D.h. es sind für jeden Mehrwertsteuersatz die zugehörigen Umsatzsteuer- und Vorsteuerkonten und die Skontoaufwand bzw. Skontoerlöskonten zu definieren.

    ![](Fibu_Steuerkategorie_Definition.jpg)

    Diese Zuordnung bewirkt, dass die Steuern entsprechend der hier definierten Zuordnung verbucht werden.

3.  Definieren Sie nun die Steuerkategorien aller Debitoren und Kreditoren.
    Dazu werden in den Kopfdaten jedes Personenkontos Land, Postleitzahl, Ort, UID Nr und ev. Reverse Charge angezeigt.
    Achten Sie dabei auf die richtige Zuordnung der Steuerkategorie.

4.  Definieren Sie nun Ihre Sachkonten.
    Achten Sie hier insbesondere auf eine für Sie und Ihren Buchhalter eindeutige Bezeichnung des Kontos und definieren Sie die UVA Art. Sollten die beschreibenden Texte der auszuwählenden UVA Art für Sie nicht sprechend sein oder diese unvollständig sein, so wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer. Er kann diese laut Ihren Angaben, den Angaben Ihres Steuerberaters entsprechend einstellen.
    
5.  Prüfen Sie nun, dass die Regel der Erlöskontenfindung auf Artikelgruppe steht. D.h. der Parameter FINANZ_EXPORT_VARIANTE muss auf Artikelgruppen stehen.

6.  Wir raten den Parameter ARTIKELGRUPPE_IST_PFLICHTFELD auf 1 zu stellen, damit für jeden Artikel auch eine entsprechende Artikelgruppe ausgewählt werden muss.

7.  Prüfen Sie nun in den Grunddaten des Artikels, dass für alle Artikelgruppen auch entsprechende Inlandskontierungen angegeben sind.

8.  Prüfen Sie für die Konten der Artikelgruppen die Länderartübersetzungen / Steuerkategoriezuweisungen.
    Damit wird ganz wesentlich die Buchung auf das richtige Erlöskonto gesteuert in Abhängigkeit des Landes/Steuerkategorie des Kunden.

9.  Erfassen Sie nun eine Ausgangsrechnung und drucken Sie diese aus.
    Wechseln Sie in das zugehörige Warenerlöskonto und prüfen Sie ob die Buchung dort mit allen Steuersätzen angekommen ist.
    Ist diese Buchung aus Sicht der Finanzbuchhaltung richtig, auf den richtigen Konten?

10. Buchen Sie nun eine Zahlung mit Skontoaufwand.
    Wird die Zahlung entsprechend auf der Bank eingetragen und der Skontoaufwand verbucht.
    Gegebenenfalls löschen Sie die Bankbuchung wieder.

11. Erfassen Sie nun eine Eingangsrechnung,
    prüfen Sie ob auch hier die Buchungen inkl. den Steuerbeträgen entsprechend richtig verbucht werden und buchen Sie eine Zahlung der Eingangsrechnung mit Skonto.
    **Hinweis:** Alternativ zu den Punkten 9 bis 11 können Sie auch die [Einzelbelegübernahme](#Einzelbelegübernahme) verwenden.

12. Konnten alle obige Punkte erfolgreich durchgeführt werden, so kann als nächstes die Periodenweise Übernahme der bestehenden Daten direkt in die Fibu durchgeführt werden. [Siehe dazu.](#Periodenweise Belegübernahme)

13. Drucken Sie nun eine Saldenliste aus. Stimmt diese mit den Ausdrucken Ihres Steuerberaters überein?
    Gegebenenfalls müssen noch Eröffnungsbuchungen durchgeführt werden.

14. Drucken Sie eine UVA für die oben übernommenen Daten aus.
    Stimmen die angegebenen Steuerbeträge, [wenn nicht siehe](#Falsche Steuersätze in der UVA).
    Stimmt diese mit den Daten Ihres Steuerberaters überein?

#### Steuerkategorien und mehrere Finanzämter auf einem Mandanten
In dieser Konstellation sind einige Dinge zu beachten.
Ziel der Verwendung von mehreren Finanzämtern ist, dass die Mehrwertsteuerthematik, vor allem die einfache Verrechnung der Vorsteuer mit der Umsatzsteuer bei einem ausländischen Finanzamt erfolgen kann.
Dafür benötigen Sie eine Steuernummer beim ausländischen Finanzamt und ein weiteres freigeschaltetes Finanzamt in Ihrer **Kieselstein ERP** Installation.
Derzeit ist es in Europa noch üblich, dass jedes Land seinen eigenen Mehrwertsteuersatz hat.
Wenn Sie nun Lieferungen und Leistungen umsatzsteuerlich gesehen in einem anderen Land abwickeln, bedeutet dies, dass Sie dafür im jeweiligen Mandanten die Mehrwertsteuersätze nicht nur Ihres Inlandes / Ihres Firmensitzes, sondern eben auch jenes Landes mit abbilden müssen für das Sie eine Umsatzsteuernummer haben.

Ein Beispiel:<br>
Firmensitz Österreich, Umsatzsteuernummer in Deutschland (Finanzamt Bonn)<br>
Wenn Sie nur Waren des Normalsteuersatzes in Deutschland verkaufen, so muss auch der deutsche Mehrwertsteuersatz von 19% definiert werden. Dafür definieren Sie unter System, Mandant, Mwst Bezeichnung eine zusätzliche Mehrwertsteuer z.B. mit der Bezeichnung dt. MwSt 19%. Zusätzlich definieren Sie unter MwSt den Steuersatz für diese Bezeichnung mit 19% und gültig ab 1.1.2007.

Nun definieren Sie unter Finanzamt die jeweils gültigen Konten für die jeweilige Steuerkategorie.<br>
Hier ist wichtig nur für diejenigen Steuerkategorien Umsatzsteuerkonten zu hinterlegen, bei denen Sie auch tatsächlich Umsätze erwarten. So würde in unserem Beispiel nur die Steuerkategorie Inland und unter Steuerkategoriekonto nur die Mehrwertsteuerkategorie dt. MwSt 19% definiert werden. Das bringt für Sie die Sicherheit, dass bei falschen Definitionen der Steuerkategorie - Finanzamts Kombination in Kreditoren- bzw. Debitorenkonten von **Kieselstein ERP** erkannt wird, dass diese Buchung nicht definiert ist und daher die Buchung mit einer entsprechenden Fehlermeldung abgebrochen wird.

Beachten Sie bitte insbesondere bei der Definition von ausländischen / zusätzlichen Finanzämtern, dass die Steuerkategorien sich immer auf das Land des gewählten Finanzamtes beziehen. So ist zu obigem Beispiel die Steuerkategorie Inland für das Finanzamt Bonn für innerdeutsche Lieferungen zu definieren.

Denken Sie daran, die Abstimmkonten, auch Mitlaufende-Konten genannt, für jede benötigte Steuerkategorie zu definieren.

#### Ich erhalten die Fehlermeldung Sachkonto für Kostenstelle .... nicht definiert?
Erhalten Sie bei Druck einer Rechnung nachfolgende Meldung,

        java.lang.Exception: Sachkonto fuer Kostenstelle i_id11,10 Kostenstelle 10 ist nicht definiert
so ist für den jeweiligen Artikel kein Erlöskonto hinterlegt. Bitte definieren Sie den Artikel vollständig und drucken Sie dann die  Rechnung erneut. Beachten Sie dazu auch den Parameter FIBU_EXPORT_VARIANTE

#### Die Fibu bucht ausschließlich in Mandantenwährung.
Eventuelle Fremdwährungen werden vor der Übergabe an die Fibu entsprechend den Kursen umgerechnet. Das hat natürlich den Vorteil, dass Kurs-Fehler sofort auffallen. Auch die Bank wird nur in [Mandantenwährung]( {{<relref "/docs/stammdaten/system" >}} ) gebucht, auch wenn Sie eventuell ein Fremdwährungskonto haben.

**Hinweis:**

Sind in der Bankbuchung Fremdwährungsbeträge enthalten, so wird zu Ihrer Information der Fremdwährungsbetrag angezeigt. Dieser wird aus der Buchung über den bei der Zahlungsbuchung hinterlegten Fremdwährungskurs errechnet.

Siehe dazu bitte auch [Bankkonten in Fremdwährung](#Bankkonten mit Fremdwährung).

#### Wie wird eine falsch gebuchte Ausgangsrechnung, weil Konten/Artikelgruppen falsch eingestellt waren wieder korrigiert?
1. Die Kontenzuordnung richtig stellen
2. a.) die RE in den Status angelegt bringen, durch ändern und dann erneut drucken. Über die interne Verknüpfung wird sichergestellt, dass alle alten Buchungen auf die Rechnung zurückgenommen (storniert) werden und dann beim Drucken entsprechend der neuen Definitionen wieder in die Buchhaltung übernommen werden.<br>
b.) Alternativ und gerade bei bereits bezahlten Rechnungen verwenden Sie bitte [Einzelbelegübernahme](#Einzelbelegübernahme).

#### Wann erfolgt die Verbuchung?
Beim Aktivieren der Rechnung, also beim ersten Druck.
Wird die Rechnung geändert, werden die Buchungen wieder zurückgenommen (storniert) und beim erneuten Druck wieder in die Fibu gestellt.

<a name="Einzelbelegübernahme"></a>

#### Kann eine einzelne Rechnung nachträglich in die Fibu übernommen werden?
Ja. Sie haben in allen Belegmodulen (RE, GS, ER, ZK) auch den Menüpunkt Bearbeiten, Belegübernahme. Durch Klick darauf wird der ausgewählte Beleg aus der Buchhaltung storniert und neu übernommen. Die Stornobuchungen werden normalerweise nicht angezeigt, aber auf jeden Falle protokolliert.
Auch bei der Einzelbelegübernahme werden etwaige Zahlungsbuchungen in die Fibu mit übernommen.

#### Mitlaufende Buchungen
Die offenen Forderungen und Verbindlichkeiten werden durch so genannte mitlaufende Buchungen realisiert. Damit finden sind in den Forderungen- und Verbindlichkeiten-Konten, auch Abstimmkonten genannt, immer den aktuellen Stand, wodurch Ihre Saldenliste aus dieser Sicht immer aktuell ist.

#### kann auf mitlaufende Konten gebucht werden?
nein, auch wenn diese aus reiner Buchhaltungssicht nicht falsch wäre, ist es aus Gründen der Übersichtlichkeit strickt abzulehnen.<br>
Wenn eine Buchung auf ein mitlaufendes Konto durchgeführt werden sollte, so erhalten Sie die Meldung:<br>
![](Buchung_auf_mitlaufend_nicht_erlaubt.jpg)<br>
Warum ist das so?
- a.) Buchungen gegen Debitoren-/Kreditorenkonten bewirken automatisch, dass (von den Nebenbüchern aus) auch auf das mitlaufende Konto, je nach Steuerkategorie gebucht wird.
- b.) Würden Sie, z.B. zum Jahreswechsel einen (großen) Posten als uneinbringlich einfach gegen das mitlaufende Konto ausbuchen, so haben Sie absolut keinen Überblick mehr, was denn nun tatsächlich ausgebucht wurde. Lassen Sie sich von niemandem dazu überreden, dass dies schneller ginge. Wenn auch nur ein Cent/Rappen falsch ist, suchen Sie den Fehler stundenlang und wir sind alle nur Menschen.
[Siehe dazu auch](#Mitlaufende_Buchung).

#### Warum gibt es im Reiter Buchungen keinen Neu-Knopf?
Da hinter der Buchung viel mehr steckt und sehr oft auch verschiedene Buchungsschablonen verwendet werden, verwenden Sie bitte den Menüpunkt Bearbeiten, Manuelle Buchung, bzw. den Menüpunkt Bearbeiten, Splittbuchung.

#### Fehlermeldung: id to load is required for loading
Tritt diese Meldung auf, so ist die Steuerkategorie beim Kunden/Lieferanten im Debitorenkonto DEBITORENKONTO !!!! respektive Kreditorenkonto (noch) nicht gesetzt. Dieses bitte im entsprechenden Personenkonto setzen.

Weitere möglich Fehlerursache, die manchmal bei übernommenen Daten auftritt:

Prüfen Sie ob die Rechnungen (ER/AR) und die beteiligten Lieferscheine alle einem Land zugeordnet sind. Da der Lieferschein das steuerliche Lieferland definiert, kann ohne Land keine Länderartübersetzung durchgeführt werden. Daher die Fehlermeldung.

Verhalten der Fibu:
- Beim Anlegen der Sachkonten bleibt immer das zuletzt gewählte Finanzamt vorbesetzt.
- Gültig ab des Kontos wird mit dem Startdatum des Geschäftsjahres vorbesetzt. Z.B. ab 1.1.2023
- Beim Anlegen der Kunden / Lieferanten wird das Standard Finanzamt eingetragen. Dies kann im System, Mandant, Vorbelegungen2 definiert werden.
- Vorschlagswert der Steuerkategorie des Debitors/Kreditors: Dieses wird anhand des Landes (Länderart) vorbesetzt.

#### Was ist zu tun, wenn der Saldo der Saldenliste nicht 0,00 wird?
Wenn der Saldo der Saldenliste nicht 0,00 ist, so bitte als erstes die Forderungen / Verbindlichkeiten Sammelkonten Zuordnung der Steuerkategorie prüfen, also der Definition der Abstimmkonten.

#### Warum wird der Skontoaufwand negativ im Haben verbucht und nicht positiv im Soll?
Da es für den ermittelten Umsatz wesentlich ist, dass der tatsächlich erzielte Betrag richtig dargestellt wird, haben wir uns dazu entschlossen, den Skontoaufwand mit negativem Vorzeichen ins Haben zu buchen. Wir sind der festen Überzeugung, dass die betriebswirtschaftliche Betrachtung hier Vorrang vor eventuellen Buchhaltungs-Allgemein-Regeln hat.

<a name="Mit Belegen neu starten"></a>

#### Wie mit bereits vorhandenen Belegen starten?
<a name="Periodenweise Belegübernahme"></a>

#### Belegübernahme einer ganzen Periode?

Gerade für die Phase der Einführung der in **Kieselstein ERP** integrierten Finanzbuchhaltung kommt es immer zu der Forderung, dass alle Belege, die noch nicht in der FiBu sind automatisch übernommen werden sollten. Dafür haben wir die Funktion Belegübernahme geschaffen. Sie finden Sie unter Sachkonten, bearbeiten Belegübernahme.

![](Fibu_Periodenauswahl.jpg)

Mit dieser Funktion werden pro Buchungsperiode alle noch nicht in der Buchhaltung befindlichen Belege übernommen. Zusätzlich werden auch die Zahlungen der Periode übernommen.

Bitte beachten Sie, dass nur Belege übernommen werden, die weder im Status storniert noch im Status angelegt sind.

Bei dieser Übernahme werden nur die Buchungen übernommen, die noch nicht in der Fibu enthalten sind. Sollten, z.B. wegen massiven Kurskorrekturen alle Belege der Periode noch einmal neu übernommen werden müssen, so haken Sie bitte "Alle neu übernehmen" an.

Da gerade beim Start einer neuen Buchhaltung doch einiges an Definitionsfehleren enthalten sein können, werden diese detailliert aufgeführt. Bitte beseitigen Sie diese Fehler und gehen Sie die Daten konsequent von Monat zu Monat, von Periode zu Periode durch.

![](Fibu_Periodenuebernahme_Fehlermeldung.jpg)

Sie können vom Fehlerfenster mit dem GoTo Button direkt auf den jeweiligen Beleg springen. In obigem Fall auf die Eingangsrechnung.

Beseitigen Sie die Fehler und versuchen Sie dann erneut die Übernahme für den gewünschten Zeitraum durchzuführen.

#### Was sind Belege ?
Hiermit ist die Gesamtheit der (Ausgangs-) Rechnungen, Ausgangsgutschriften (Wert oder Mengengutschrift) in Deutschland auch Rechnungskorrektur genannt, Eingangsrechnungen, Eingangsgutschriften und auch der Zusatzkosten gemeint.

#### Wieso sprechen wir in der Buchhaltung von Periode und nicht von Monaten ?
Da es in der Buchhaltung auch ein abweichendes Geschäftsjahr / Wirtschaftsjahr gibt, und somit z.B. beim Geschäftsjahresbeginn von 1.5\. das erste Geschäftsmonat der Mai ist, also das 5.Monat aber die erste Periode wird hier grundsätzlich von Perioden ausgegangen.

<a name="Manuelle Buchung"></a>

#### Manuelle Buchungen
Bei Sachkonten, Debitoren-, Kreditorenkonten finden Sie im Menüpunkt Bearbeiten auch Manuelle Buchung.

Damit können einfache Buchungen zwischen zwei Konten durchgeführt werden.

Diese Funktion ist so programmiert, dass Sie solange im Erfassungsdialog bleiben, bis Sie alle Buchungen durchgeführt habe.

![](Fibu_Umbuchung.jpg)

D.h. nach der ersten Buchung sind alle Felder soweit vorbesetzt, dass nur mehr das sich ändernde Konto und der neue Betrag eingetragen werden müssen.

Eine Buchung wird durch Klick auf ![](Fibu_Umbuchung2.gif) Speichern abgeschlossen. Zur leichteren Übersicht werden die bisher durchgeführten Buchungen im unteren Bereich der Erfassungsmaske angezeigt.

Mit den Klick auf ![](Fibu_Umbuchung3.gif) verlassen Sie die Manuellen Buchungen.

Die Default Kostenstelle für die Finanzbuchhaltung kann unter System/Mandant/Vorbelegungen 2 definiert werden.

**Hinweis:**

Bankbuchungen auf Eingangs- / Ausgangsrechnungen müssen derzeit direkt unter Zahlungen bei der jeweiligen Rechnung durchgeführt werden. Oder Sie nutzen dafür den [SEPA XML Import](Zahlungsvorschlag.htm#SEPA Kontoauszug Import).

Ändern einer manuellen Buchung bzw. einer Splittbuchung

In den Konten kann eine erfolgte manuelle Buchung bzw. eine manuelle Splittbuchung auch wieder geändert werden. Stellen Sie dazu den Cursor auf die Buchungszeile und klicken Sie auf ändern ![](Buchung_aendern.gif).<br>
Bei einer manuellen Buchung wird nun die Buchungserfassungsmaske mit den Daten der Buchung vorbesetzt und Sie können die Daten entsprechend ändern. Beim Speichern wird automatisch die vorhergehende Buchung storniert inkl. der dafür erforderlichen Gegenbuchung und die neuen Daten als neuer Buchungssatz eingetragen.

Bei einer Splittbuchung wird die Liste der Buchungen mit den Daten vorbesetzt. Hier löschen Sie die zu ändernde Buchung aus der Liste der Buchungen heraus und tragen die neue(n) Buchungszeile(n) ein.<br>
Beim Speichern wird ebenfalls die vorhergehende Buchung automatisch storniert und die neuen Splittbuchungsdaten eingebucht.

Bedeutung der Buchungsarten

Bei der manuellen Buchung kann auch die Buchungsart festgelegt werden. Insbesondere die Buchungsart Eröffnung hat die Auswirkung, dass in den Kontoblättern die Salden der Eröffnungsbuchungen extra ausgewiesen werden. Alle anderen Buchungsarten haben informativen Charakter. D.h. sie werden gerne für Sortierungen oder ähnlichem genutzt.
Beachten Sie bitte, dass die EB Anzeigen in den Kontoblättern derzeit NUR auf der Buchungsart aufbauen. Auch die EB Spalte der Saldenliste wird anhand der Buchungsart ermittelt.

<a name="Manuelle Buchung mit MwSt"></a>Automatische Buchung der Ust/Vst bei der manuellen Buchung

Eine automatische Buchung der Steuerbuchung ist möglich, wenn folgende Voraussetzungen erfüllt sind:
- Auf dem beteiligten Sachkonto ist eine Steuerkategorie hinterlegt.
Sie finden in den Kopfdaten des Sachkontos die Möglichkeit die passende Steuerkategorie auszuwählen und daraufhin auch die Definition, ob Umsatz- oder Vorsteuer.
![](Steuerkategorie_Sachkonten_UST_VST.JPG)
- Bei der manuellen Buchung darf nur eines der beteiligten Konten eine Steuerkategorie hinterlegt haben.
- Es muss ein Bankkonto bzw. ein Sachkonto beteiligt sein.
Wenn kein Konto eine Steuerkategorie hinterlegt hat, oder beide eine Steuerkategorie hinterlegt haben, so wird die Buchung steuerfrei durchgeführt.

Beispiel Bank an Versicherung:<br>
Inland Betrag: 120,- <br>
100,- auf Versicherungsaufwand<br>
20,- auf VST<br>
-120,- von Bank<br>

Bitte beachten Sie, dass die Definition der Steuerkategorie in den Sachkonten **nur** für diese manuelle Buchung zur Ermittlung des Ust-Kontos von Bedeutung ist.

#### Der Ändernknopf ist grau. Die Buchung kann nicht geändert werden, warum ?
Darf eine Buchung nicht geändert werden, so wird der Ändern-Knopf in grau dargestellt. Gründe warum eine Buchung nicht geändert werden kann sind:
-   Das Geschäftsjahr ist gesperrt.
-   Die Buchung wurde in der UVA Verprobung berücksichtigt [siehe](#Buchung wegen UVA nicht erlaubt).
-   Die Buchung wurde aus den Bewegungsdaten automatisch erstellt. Ändern Sie die Datenquelle, also die Bewegungsdaten.

Buchung in anderer Währung:

Die Auswahl eine anderen Währung, dient rein als Rechenhilfsmittel. D.h. wurde für die Buchung eine andere Währung als Ihre Mandantenwährung ausgewählt, so erfolgt die Erfassung in der gewählten Währung. Die Buchung erfolgt in jedem Fall in Mandantenwährung. Die Umrechnung erfolgt zum Kurs des Buchungstages.

Hinweis: Wird für den Ausdruck des Kontoblattes eine andere Währung als die Mandantenwährung ausgewählt, so wird beim Ausdruck anhand des jeweils hinterlegten Kurses rückgerechnet. Wurde vom Buchungszeitpunkt zum Druckzeitpunkt der Kurs zum Buchungsdatum verändert, so wird es zu Differenzen kommen. Die integrierte Buchhaltung bucht immer nur in Ihrer Mandantenwährung.

Abweichendes Umsatzsteuerland

Bitte beachten Sie auch, dass mit der integrierten Finanzbuchhaltung der Begriff abweichendes Umsatzsteuerland in den Kunden/Lieferanten Konditionen durch die Definition des Finanzamtes direkt im Konto ersetzt wird. Daher wird in den Konditionen nur mehr das Finanzamt des Kontos angezeigt. Eine Änderung darf nur mehr im jeweiligen Personenkonto vorgenommen werden und auch da nur, wenn es noch keine Buchungen auf diesem Konto gibt.

Belegübernahme

Um gerade für den Start mit der **Kieselstein ERP** Buchhaltung eine rasche Möglichkeit zu schaffen, die Belege des aktuellen Geschäftsjahres zu übernehmen, haben wir die Möglichkeit geschaffen, diese Periode = Monatsweise in die Finanzbuchhaltung zu übernehmen.
Sie finden diese Funktion unter Sachkonten, Bearbeiten, Belegübernahme.
Wählen Sie nun das gewünschte Monat des aktivierten Geschäftsjahres aus.
Bitte beachten Sie auch, dass die UVA, eine gültige Saldenliste nur dann ausgedruckt werden darf, wenn die Belegübernahme für die Periode erfolgreich durchgeführt werden konnte.

Bei der Belegübernahme kann es zu einer Reihe von Fehlermeldungen kommen, die alle daher rühren, dass die Voraussetzungen für die Buchungen nicht vollständig erfüllt sind.
In diesen Fehlermeldungen ist unter Detail auch immer der auslösende Beleg angeführt und soweit möglich auch der Buchungsvorgang, der nicht durchgeführt werden konnte.
Hier finden Sie unter Zusatzinformation: RE 11/0012345, Firma xyz, LKZ-PLZ Ort
Und dann weiter unten ..... Debitorenkonto für Kunde xyz ist nicht definiert.
Bitte handeln Sie entsprechend.

Siehe dazu bitte auch oben, [Einzelbelegübernahme](#Einzelbelegübernahme).

Fehlermeldungen die bei der Buchung auftreten können:

Wenn bei der Übernahme von Belegen (Rechnungen, Eingangsrechnungen, Gutschriften, Zahlungen, Skonti) Fehlermeldungen auftreten, so klicken Sie bitte ins Detail. Hier finden Sie nach dem allgemeinen Fehlerdialog immer weitere Details die auf die Rechnung ev. den Lieferschein und ev. auch auf den Artikel hinweisen. Einige Zeilen darunter finden Sie dann ev. noch weitere Hinweise, z.B. Keine Artikelgruppe definiert.

-   Fehlerbeschreibung: Es ist ein schwerwiegender Fehler aufgetreten, wichtige Daten sind geloggt.
    Das Modul wird jetzt automatisch beendet, bitte wenden Sie sich an Ihren Administrator.
    Nach Abklärung mit Ihrem Administrator senden Sie gegebenenfalls die unter Detail angeführten Informationen mit einer kurzen Beschreibung zum Auftreten des Fehlers an: <help@kieselstein-erp.org.com> Fehlercode:4823 Java-Version:20.2-b01 Clientuhrzeit:30.08.202317:42:21MESZ User:2461Admin|ZINN|
    Zusatzinformation:
    Rechnung: 23/0000176 
    Lieferschein: 23/0000211 
    Artikel: 45001

    class com.lp.client.frame.ExceptionLP
    java.lang.Exception: Keine Artikelgruppe für Artikel definiert:45001

-   java.lang.Exception: Beleg: 13/0000005, Art: Rechnung , kein Konto definiert in Buchungszeile 3 (null, null, null, 393, 524.71, HABEN , 0.00, null, null, null, null)
    iId, buchungIId, kontoIId, kontoIIdGegenkonto, nBetrag, buchungdetailartCNr, nUst, tAnlegen, personalIIdAnlegen, tAendern;

    Tritt auf, wenn die Steuerkonten nicht ordentlich definiert sind.<br>
    Prüfen Sie daher folgende Einstellungen:
    -   Parameter FINANZ_EXPORT_VARIANTE muss auf Artikelgruppen stehen
    -   Parameter FINANZ_EXPORT_ARTIKELGRUPPEN_DEFAULT_KONTO_AR ist richtig definiert
    -   Finanzamt, Steuerkategorie die entsprechende z.B. Inland ist definiert
    -   Debitoren / Kreditoren Konto ist vollständig definiert
    -   Sachkonto ist vollständig definiert
    -   Das / die beteiligten Ust/Vst Konten, definiert über die Steuerkategorie, sind vollständig definiert
    -   Das Skontoaufwand / -erlöskonto, definiert über die Steuerkategorien, sind vollständig definiert
-   Beim Anlegen einer Debitoren-/Kreditorennummer kommt eine Fehlermeldung
    class com.lp.client.frame.ExceptionLP
    null
    com.lp.client.frame.delegate.Delegate.handleThrowable(Delegate.java:181)
    com.lp.client.frame.delegate.KundeDelegate.createDebitorenkontoNummerZuKundenAutomatisch(KundeDelegate.java:486)

    Was ist zu tun:
    Definieren Sie bitte unter System, Mandant, Vorbelegungen2 das Standard Finanzamt für diesen Mandanten. Wenn hier kein Finanzamt zur Auswahl steht, so definieren Sie dieses bitte im Modul Finanzbuchhaltung, unterer Modulreiter Finanzamt.

    Sollten Sie das Modul Finanzbuchhaltung nicht besitzen, so wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer, damit er für Sie ein Finanzamt einrichtet.

-   Kein Abstimmkonto definiert für Steuerkategorie: Ausland FinanzamtIId: 15 Dies bedeutet, dass für die angegebene Rechnung in den Steuerkategorie-Definitionen keine Definition für die mitlaufenden Konten angegeben ist. Tragen Sie dies für die [Steuerkategorie](#Abstimmkonto) des Finanzamtes nach.

-   2012-04-16 10:41:43:050 [ 0] ERROR error(LpLogger:73) - Code=-1 javax.ejb.EJBTransactionRolledbackException: id to load is required for loading
    com.lp.util.EJBExceptionLP: javax.ejb.EJBTransactionRolledbackException: id to load is required for loading
    at com.lp.server.rechnung.ejbfac.RechnungFacBean.createZahlung(RechnungFacBean.java:2064)
    Diese Meldung tritt auf, wenn versucht wird eine Zahlungsbuchung zu machen und die zugrunde liegende Rechnung ist noch nicht in der Fibu verbucht und kann auch, aus verschiedenen Gründen, nicht in die Fibu übernommen werden.
    Bestätigen Sie die Meldung und versuchen Sie die Rechnung mit Bearbeiten, Belegübernahme zu übernehmen. Es wird ein Fehler auftreten. Beseitigen Sie diesen und setzen Sie dann mit der Belegübernahme und der nachfolgenden Zahlung fort.

-   class com.lp.client.frame.ExceptionLP
    id to load is required for loading
    com.lp.client.frame.delegate.Delegate.handleThrowable(Delegate.java:181)
    com.lp.client.frame.delegate.RechnungDelegate.verbucheRechnungNeu(RechnungDelegate.java:1288)

    Bedeutet, es ist vermutlich keine Artikelgruppe definiert. Definieren Sie die Artikelgruppe als Pflichtfeld.
    Prüfen Sie zusätzlich den Parameter FINANZ_EXPORT_ARTIKELGRUPPE_DEFAULT_KONTO_AR. Dieser muss auf ein gültiges Erlöskonto zeigen.

-   Bei der Buchung, also dem Drucken/Aktivieren einer (Ausgangs-)Rechnung erscheint: Steuerfreie Buchung auf ein steuerbehaftetes Konto<br>
    ![](Fehler_Steuerfreie_Buchung.jpg)<br>
    Dies bedeutet, dass eine Erlösbuchung ohne Umsatzsteuer auf ein Konto erfolgt, welches als mit Umsatzsteuer definiert ist.
    Dies bedeutet in weiterer Folge, dass sehr wahrscheinlich Ihre UVA nicht stimmen wird. In diesem Falle würden Sie zu viel Umsatzsteuer an Ihr Finanzamt abführen.

    Bitte prüfen Sie die Buchung und korrigieren Sie die Definition der Konten, z.B. Länderartübersetzungen oder für einen extrem seltenen Ausnahmefall, lösen Sie diese Buchung mit einer manuellen Gegenbuchung auf.

**<u>Hinweis:</u>**

Für mehrere Finanzämter empfiehlt es sich, die unterschiedlichen Mehrwertsteuersätze getrennt anzulegen und einzutragen.

Oft haben Sie hier auch die Situation dass ein und der selbe Rechnungsempfänger zum Teil Lieferungen im echten Inland des Haupt-Finanzamtes hat und zum Teil Lieferungen als Inlandslieferung im Auslands-Finanzamt erfolgen. Hier erreichen Sie eine deutliche Buchungssicherheit, wenn die Mehrwertsteuersätze exakt und getrennt abgebildet werden.

Hinweis:

Gerade bei grenzüberschreitenden Geschäften kommt es immer wieder zu gemischten steuerlichen Vorfällen. Beachten Sie bitte, dass die Umsatzsteuerpflicht von der Lieferadresse definiert wird. Aus diesem Grunde kann es erforderlich sein, dass mancher Debitor / Kreditor mit unterschiedlichen Steuerkategorien definiert werden muss, also zwei Mal angelegt werden muss mit unterschiedlichen Kontonummern und unterschiedlichen Steuerkategorien. Bei mehreren Finanzämtern kann es vorkommen, dass auch die Finanzämter mit für die Definition der richtigen Steuerkategorie berücksichtigt werden müssen.

## Definition eines Finanzamtes

Bei der integrierten Finanzbuchhaltung steht Ihnen im Modul Finanzbuchhaltung der untere Modulreiter Finanzamt zur Verfügung.

Definieren Sie hier die Daten für Ihre Verbindung zum Finanzamt, neben den üblichen Adressdaten vor allem Steuernummer, Referatsnummer und gegebenenfalls eine abweichende Formularnummer für ein anderes Umsatzsteuerformular.

Nach der eigentlichen Definition des Finanzamtes sind die Steuerkategorien exakt zu definieren.

### Dokumente beim Finanzamt hinterlegen
Im **Kieselstein ERP** können bei jedem Finanzamt auch Dokumente hinterlegt werden.<br>
Bitte beachte, dass der Dokumentenpfad im System nur dann aufscheint, wenn tatsächlich Dokumente beim Finanzamt, allgemeiner gesprochen beim jeweiligen Beleg hinterlegt sind.


#### Hauptfinanzamt

Das ist jenes Finanzamt, welches unter System, Mandant, Vorbelegungen2, Finanzamt für den jeweiligen Mandanten definiert ist.

Eröffnungsbuchungen / Saldenvortrag

Für den Start der Fibu oder für Jahresübernahmen müssen verschiedene Konten wie Banken aber auch Debitoren/Kreditoren usw. eröffnet werden. Im SKR04 wird auch vom Saldenvortrag aus den Vorjahren gesprochen.
Für eine manuelle Eröffnungsbuchung gehen Sie bitte wie folgt vor:<br>
definieren Sie die Eröffnungskonten für Sachkonten, Debitoren und Kreditoren
Legen Sie die Kontoart des Sachkonto mit Eröffnungskonto fest.<br>
![](EB_Konto_Definition.gif)

Für die eigentliche Buchung wählen Sie nun im Menü Bearbeiten, Manuelle Buchung.<br>
![](Fibu_Umbuchung4.jpg)<br>

![](Fibu_Umbuchung5.jpg)<br>

- Legen Sie das Datum der Eröffnungsbuchung fest. In der Regel ist dies der erste Tag Ihres Geschäftsjahres.
- Stellen Sie die Buchungsart auf Eröffnung.
- Wählen Sie das gewünschte Sachkonto und als Gegenkonto das Eröffnungskonto.
Bei Bank-Eröffnungsbuchungen hat sich bewährt als Auszugsnummer die 0 anzugeben.
- Definieren Sie die Kostenstelle und setzen Sie die Mehrwertsteuerart auf steuerfrei.
- Fügen Sie einen geeigneten Text für die Eröffnungsbuchung an und vergeben Sie durch Klick auf Erzeugen eine automatische Belegnummer.
- Sind nun alle Daten richtig, so klicken Sie auf den grünen Haken um die Buchung durchzuführen.

Um nun weitere Buchungen durchzuführen ändern Sie das entsprechende Konto und tragen die weiteren Daten ein.<br>
**Wichtig:** In der Saldenanzeige nach Auszug, z.B. in den Bankkonten, werden die Eröffnungsbuchungen immer mitgerechnet, egal welche Kontoauszugsnummer hinterlegt ist.<br>
Daher wird bei der Eröffnungsbuchung die Kontoauszugsnummer automatisch mit 0 vorbesetzt und ist nicht änderbar.

#### Die Auszugs-Saldenanzeige stimmt nicht mit dem Ausdruck des Kontoblattes überein?
Ursache: Der Ausdruck des Kontoblattes, sortiert nach Auszugsnummer erfolgt rein nach der Auszugsnummer. Für die Errechnung des Saldos des Auszuges wird, wie oben beschreiben, jede Eröffnungsbuchung mitgerechnet, um hier auch die Möglichkeit zu schaffen, dass keine Auszugsnummer angegeben werden muss.

#### Wie sollten die Auszugsnummern der Bankkonten erfasst werden ?
Die richtige Eingabe der Auszugsnummern ist ein wesentlicher Beitrag zur Erfassungssicherheit. D.h. es **<u>muss</u>** gewährleistet sein, dass man den Ausdruck des Bankkontos neben die Kontoauszüge der Bank legen kann und die Salden auf den beiden Papieren stimmen auf den Cent/Rappen überein. Nur so kann sichergestellt werden, dass alles richtig erfasst ist. **Ein zusätzlicher Hinweis:** Es wird in den Konto-Details also den Buchungen im Feld Saldo der aktuelle Saldo des Kontos beginnend von der Buchung mit der niedrigsten Auszugsnummer bis hin zu der Auszugsnummer auf der der Cursor gerade steht angezeigt. D.h. es kann mit einem Blick geprüft werden, ob der Auszug richtig und vollständig erfasst ist. Auch das zu Ihrer Erfassungssicherheit.

Info:
Stehen keine Auszugsnummern in chronologisch fortlaufender Weise zur Verfügung, so verwenden Sie bitte:
-   entweder eine eigene fortlaufende Auszugsnummer pro Kalenderjahr und schreiben Sie diese auf die Bankbelege
-   verwenden Sie anstatt dessen das Auszugsdatum in der Ansi Schreibweise, also JJJJ-MM-TT (Jahr, Monat, Tag). Wichtig ist, dass die Chronologie erhalten bleibt. Siehe dazu auch Parameter AUSZUGSNUMMER_BEI_BANK_ANGEBEN. Gegebenenfalls ist dieser auf 0 zu stellen, damit **Kieselstein ERP** diesen automatisch aus dem Zahldatum übernimmt.

Info: Im Gegensatz zu den deutschen und österreichischen Bankbelegen stellen die Schweizer Banken keine in sich geschlossenen fortlaufenden Kontoauszugsnummern zur Verfügung. Daher gehen Sie bitte wie oben beschrieben vor.

<a name="SEPA Auszugssaldo stimmt nicht"></a>

#### Der Saldo bestimmter SEPA-Konten stimmt nicht mit dem tatsächlichen Saldo überein?
Um auf die verschiedenen Konten einen SEPA/ISO200022 Import durchführen zu können, müssen diese als Bankkonto definiert sein.<br>
Wird nun die Bankkontendefinition erst nach bereits erfolgten Buchungen nachgetragen, kann es sein, dass auf die bisherigen Buchungen noch keine Auszugsnummer erzwungen wurde. Damit ist die Saldoberechnung für den SEPA Import nicht schlüssig. Tragen Sie in diesem Falle die Auszugsnummern nach bzw. lassen Sie diese durch Ihren **Kieselstein ERP** Betreuer nachtragen.

Können Konten gelöscht werden?

Jein, oder das hängt davon ab.

Grundsätzlich können Konten nur gelöscht werden, wenn auf diesen keine Buchungen, ohne Begrenzung des Zeitraumes, sind. Weiters dürfen darauf keine Verweise erfolgt sein. Bei Debitoren-/Kreditorenkonten sind dies vor allem Verweise aus dem Kunden-/Lieferantenstamm.

Um nun versehentlich bereits angelegte Debitoren-/Kreditorenkonten zu löschen, stellen Sie zuerst sicher, dass kein Kunde-/Lieferant auf dieses Konto zeigt. Dazu gehen Sie in die Kundenverwaltung, sortieren die Auswahlliste nach Kunden oder verwenden den Zusatzfilter um den Kunden mit dem Debitorenkonto anzuzeigen. Nun ändern Sie in den Konditionen die Debitorennummer durch Angabe eines bereits bestehenden Debitorenkontos. Den eventuell erscheinenden Hinweis der mehrfachen Verwendung des Debitorenkontos bestätigen Sie entsprechend. Nun kann das eigentliche Debitorenkonto gelöscht werden.

Die Vorgehensweise für Kreditorenkonten ist analog dazu.

Können Buchungen gelöscht / storniert werden?

Es kommt immer wieder vor, wir sind alle nur Menschen, dass falsche Buchungen gemacht werden. Wir haben daher die Möglichkeit geschaffen, einzelne Buchungen zu stornieren. Um eine Buchung zu stornieren, gehen Sie auf die jeweilige Buchung und wählen Sie im Menü, Bearbeiten, Buchung stornieren. **<u>Wichtig:</u>** Damit wird die Buchung nur als storniert gekennzeichnet und in der normalen Darstellung nicht mehr angezeigt, bzw. bei den Saldenlisten, Berechnungen usw. nicht berücksichtigt.<br>
Sie haben in jedem Konto die Möglichkeit das ![](Buchung_plus_stornierte.gif) plus Stornierte anzuhaken. Damit werden auch die stornierten Buchungen angezeigt. Genauso können im Buchungsjournal die stornierten Buchungen mit Datum, Uhrzeit und Person die diese Buchung storniert hat angezeigt werden.
Beim Storno einer Buchung wird die eigentliche Buchung als storniert markiert und eine Gegenbuchung mit invertierten Beträgen angelegt. Diese wird ebenfalls als storniert markiert. Das Buchungsdatum der Stornobuchung ist gleich dem Buchungsdatum der stornierten Buchung. Das Anlagedatum der Buchungen entspricht dem jeweiligen Zeitpunkt der Durchführung der Buchung.
Beim Export der Buchungsjournale für die Finanz werden die stornierten Buchungen immer mit exportiert.<br>
**<u>Hinweis:</u>** Automatikbuchungen, welche z.B. aus einer Ausgangsrechnung kommen können in der Finanzbuchhaltung nicht storniert werden. Gehen Sie für eine Veränderung dieser Daten in das auslösende Modul, z.B. die Zahlung einer Rechnung und ändern Sie diese dort entsprechend ab.

### Wo können die UVA Arten definiert werden?
<a name="Definition UVA Arten"></a>
Im Modul Finanzbuchhaltung können unter Grunddaten die UVA Arten definiert werden.
Dies ist vor allem für die Definition der Kennzahlen, passend zu Ihren länderspezifischen Kennzahlen und für die länderspezifischen Bezeichnungen gedacht. Stimmen Sie diese bitte mit Ihrem Steuerberater ab.

| UVA-Art Bezeichnung | Kennzahl Österreich | Kennzahl Deutschland | Kennzahl Schweiz | Kennzahl Liechtenstein | Bemerkung |
| --- |  --- |  --- |  --- |  --- |  --- |
| Inland red. Steuer | 029 | 86 | 311 | 311 |  |
| Inland Normalsteuer | 022 | 81 | 301 | 301 |  |
| Anzahlung red. Steuer | 029a | 86 | 311 | 311 |  |
| Anzahlung Normalsteuer | 022a | 81 | 301 | 301 |  |
| EU Ausland mit UID | 017 | 41 | 220 & 289 | 220 & 289 |  |
| Export Drittland | 011 | 43/48 | -- | -- |  |
| Umsatz Reverse Charge | 021 | -- | -- | -- | Das was Sie ins europäische Ausland unter dieser Regel verkaufen. Z.B. reine Dienstleistung. Also Leistung und keine Lieferung. |
| Reverse Charge Leistung | 57->66 | -- | -- | -- |  |
| Reverse Charge Bauleistung | 48->82 |   |   |   |  |
| Reverse Charge Schrott | 32->89 |   |   |   |  |
| IG Erwerb red. Steuer | 073 | 93 | -- | -- |  |
| IG Erwerb Normalsteuer | 072 | 89 | -- | -- |  |
| Gesamtbetrag der Bemessungsgrundlage | 000/070 | 7/47 | 200 | 200 |  |
| Vorsteuerkonto | 060 | 67 | 400 | 400 |  |
| darin enthalten Vst auf KFZ | 027 | -- | -- | -- |  |
| darin enthalten Vst auf Gebäude | 028 | -- | -- | -- |  |
| Vorsteuer Investitionen | -- | -- | 405 | 405 |  |
| Import Drittland | 061 | 62 | 405 | 405 |  |
| Import Drittland Zahlung an FA | 083 | -- | -- | -- |  |
| IG Erwerb | 9/67 | 66 | 400 | 400 |  |
| Werbeabgabe | WA | -- | -- | -- |  |
| Steuerbarer Gesamtumsatz | -- | -- | 299 | 299 |  |

Bitte beachten Sie bei Reverse Charge, dass Umsatz Reverse Charge für Ihre Erlöse gedacht ist, Reverse Charge Leistung, Bauleistung bzw. Schrott werden für die Klassifizierung Ihrer Erwerbe (ähnlich dem IG-Erwerb) **<u>im Inland</u>** benötigt.

#### In der Schweiz, Liechtenstein müssen zwei Vorsteuerarten angegeben werden. Wie damit umgehen?
Das hier geschriebene gilt sinngemäß auch für Liechtenstein.
In der Schweiz muss die Vorsteuer in der Steuerberechnung der quartalsweise abzugebenden Umsatzsteuervoranmeldung (UVA) für Material- und Dienstleistungsaufwand getrennt zu Investitionen und übrigem Betriebsaufwand angegeben werden. Damit ergibt sich, dass für Schweizer Installationen eine weitere UVA Art Vorsteuer Investitionen definiert ist.
Da es in der Schweiz auch den reduzierten Mehrwertsteuersatz von derzeit 2,5% und den normal Mehrwertsteuersatz von derzeit 7,7% gibt, ergibt sich daraus, dass es insgesamt vier Mehrwertsteuerbezeichnungen geben muss. Die beiden Steuersätze, getrennt in Investitionen und Material inkl. Dienstleistung.
![](Vorsteuer_CH_1.jpg)

Das bedeutet, dass es dafür auch entsprechend vier Vorsteuerkonten gibt.
![](Vorsteuer_CH_2.jpg)<br>
Der Vorteil dieser Aufteilung ist vor allem, dass es die Prüfung der verbuchten Vorsteuer, siehe dazu auch Ust-Verprobung, deutlich vereinfacht und eventuelle Buchungsfehler sofort ersichtlich sind.
Für die Berechnung der abziehbaren Vorsteuer, welche auf die Kennzahlen 400 und 405 aufzuteilen sind, greift die Definition der UVA Art. D.h. die Beträge der Konten mit der UVA Art Vorsteuerkonto werden auf die Kennzahl 400 summiert. Analog wird mit der UVA Art Vorsteuer Investitionen vorgegangen, welche auf 405 summiert werden.
Konkret wird diese Einstellung unter dem Journal der Steuerkategorien angezeigt (unterer Reiter Finanzamt, Menüpunkt Journal, Steuerkategorien).<br>
![](Vorsteuer_CH_3.gif)

Die Definition der Inlands Steuerkategorien ohne Reverse Charge sieht damit z.B. wie folgt aus:
![](Vorsteuer_CH_4.jpg)

#### Wie sind reine Einfuhrumsatzsteuer-Rechnungen zu erfassen?
In der Schweiz ist es üblich, dass von der Eidgenössischen Zollverwaltung Rechnungen über die Einfuhr-Umsatzsteuerschuld gelegt werden.
Diese müssen bezahlt werden. Da diese aber für Unternehmer gleichzeitig reine Vorsteuer sind, müssen diese als reine Vorsteuer erfasst werden.
Hier hat sich bewährt, dass diese eine Eingangsrechnung ist, welche z.B. direkt auf das Konto 1170 (bitte beachten Sie die steuerlichen Voraussetzungen dafür) kontiert wird und damit der in der UVA als Vorsteuer zu berücksichtigende Betrag entsprechend gebucht wird.<br>
![](Vorsteuer_CH_5.jpg)<br>
Idealerweise stellen Sie den Lieferanten Eidgenössische Zollverwaltung auf steuerfrei (Lieferant, Kopfdaten, Mehrwertsteuer)

#### Reverse Charge, was ist wofür?
In **Kieselstein ERP** können vier Reversecharge Arten definiert werden.
Umsatz Reverse Charge ist gedacht für Reverse Charge Leistung ins Ausland, also das Equivalent zur IG-Lieferung. Alle anderen sind für das Inland vorgesehen.
[Siehe auch]( {{<relref "/management/finanzbuchhaltung/integriert/reverse_charge">}} ).

#### Welche Texte sind wann anzudrucken?
Nachdem immer wieder die Frage nach den entsprechenden Paragraphen und Texten kommt, hier die offiziellen Definitionen zu den verschiedenen Lieferungen.

§ 19 Abs. 1 zweiter Satz
Für Deutschland gelten nach dem SK03 folgende Steuertexte. Diese sind im Sinne von **Kieselstein ERP** wie folgt zu verstehen:
| Erlösart | Steuertext |
| --- | --- |
| Inlandserlöse | getrennt in 19%, 7% und 0% |
| Erlöse durch Lieferung ins EU-Ausland mit UID | Steuerfreie EU-Lieferungen, § 4,1b UStG |
| Erlöse durch Lieferung ins EU-Ausland ohne UID | (sind immer Umsatzsteuerlich wie Inlandslieferungen zu behandeln) |
| Erlöse durch Lieferungen ins Drittland | Steuerfreie Umsätze § 4 Nr. 1a UStG |

| Lieferung / Leistung / Land | Österreich | Kennzahl | Deutschland | Kennzahl | Schweiz / Liechtenstein | Kennzahl |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |
| Inlandslieferung | keine |   | keine |   | keine |   |
| Reverse Charge Inland Bauleistung | Kennziffer 48, §19 Abs. 1a (Bauleistungen) |   |   |   |   | gibt es nicht |
| Reverse Charge Inland Schrott | Kennziffer 32, §19 Abs. 1d (Schrott, Abfallstoffe, ...) |   |   |   |   | gibt es nicht |
| Reverse Charge Inland Telekom |   |   |   |   |   | gibt es nicht |
| Reverse Charge EU-Ausland Leistung | Gemäß §19 Abs. 1 geht die Umsatzsteuerschuld auf denLeistungsempfänger über. |   |   |   |   | gibt es nicht |
| Reverse Charge Drittland Leistung |   |   |   |   |   |   |
| IG-Lieferung (von Ware)In DE auch gerne EU-Lieferung genannt |   |   |   |   |   |   |

## Wie wird ein neues Geschäftsjahr angelegt?

Wenn die erste Rechnung, der erste Bewegungsbeleg eines Jahres erstellt wird, wird das Geschäftsjahr entsprechend initialisiert. Wenn Sie danach das Modul Finanzbuchhaltung öffnen, steht auch das neue Geschäftsjahr zur Verfügung.<br>
**Hinweis:** Wurde das Fibu-Modul vor der ersten Buchung im neuen Geschäftsjahr geöffnet und erst danach die ersten Buchungen im neuen Geschäftsjahr durchgeführt, so kann noch nicht in das nun zusätzlich zur Verfügung stehende Geschäftsjahr gewechselt werden. In diesem Falle schließen Sie bitte das Fibu-Modul und öffnen es erneut. Sie stehen automatisch auf dem nun aktuellen Geschäftsjahr.

### Wie kann ich in einem anderen Geschäftsjahr buchen / nachsehen?

In den Untermodulen Sachkonten, Debitoren- und Kreditorenkonten sowie im Kassenbuch gibt es im jeweiligen Menü immer den Menüpunkt Bearbeiten, Geschäftsjahr. Hier können alle derzeit in Ihrer **Kieselstein ERP** Installation (automatisch) definierten Geschäftsjahre ausgewählt werden. Ob Buchungen im ausgewählten Geschäftsjahr durchgeführt werden dürfen, hängt von der Geschäftsjahressperre und von den durchgeführten UVA-Verprobungen ab.

#### Bei manueller Buchung wird in das falsche Geschäftsjahr gebucht
Prüfen Sie bitte die Einstellung aus Parameters GESCHAEFTSJAHRBEGINNMONAT bzw. GESCHAEFTSJAHRPLUSEINS im System in den Parametern.

#### Wie wirkt die Trennung nach Kostenstellen in der integrierten Finanzbuchhaltung?
Wenn Sie hier an Saldenlisten, UVA oder ähnliches denken: Hier wirkt die Trennung nach Kostenstellen nicht.<br>
Es empfiehlt sich, bei der Anlage der Konten ein klare und transparente Struktur zu wählen. Viele der Konten sind ja durch die nationalen Regelungen bereits definiert. Andererseits sollten Sie immer nur die Konten anlegen die Sie tatsächlich benötigen, um Buchungsfehler / Buchungen auf falsche Konten zu vermeiden.<br>
Gerade die Verwendung mit mehreren Finanzämtern stiftet hier immer wieder Verwirrung. Achten Sie auf klare Kontenstrukturen.

#### Wie werden Erlöskonten und Debitorenkonten einer Rechnung bestimmt?
Gerade bei unterschiedlichen Ländern in den Liefer- und Rechnungsadressen ist dies immer ein viel diskutiertes Thema.<br>
Zum einfacheren Verständnis ein Beispiel:<br>
Der **Kieselstein ERP** Anwender hat seinen Stammsitz in der Schweiz

|  | Debitorenkonto | Finanzamt | Steuerkategorie |
| --- |  --- |  --- |  --- |
| Auftraggeber Fa. H aus Düsseldorf (Deutschland), | 30800 | egal | egal |
| Lieferadresse Fa. H in Erlinsbach (Schweiz) | 30801 | Bern | Inland |

Um die Rechnung korrekt in der Fibu zu verbuchen sind zwei Schritte erforderlich:<br>
- a.) Bestimmung des Debitorenkontos aus der Rechnungsadresse<br>
- b.) Bestimmung der Steuerkategorie aus der Lieferadresse um die richtige Übersetzung des Erlöskontos (der Artikelgruppe, welches ja das Inlandserlöskonto ist) auf die Inlandslieferung zu machen.<br>

Das bedeutet:

Werden Liefer- und Rechnungsadressen in unterschiedlichen Ländern definiert, so müssen für beide eigenständige Debitorenkonten angelegt werden, auch wenn in aller Regel auf das Debitorenkonto der Lieferadresse keine Buchungen erfolgen, um die richtige Übersetzung der Erlöskonten durchführen zu können.

Es gibt zu diesem Thema noch einen sehr gebräuchlichen Sonderfall, dass zwar Rechnungs- und Lieferadresse unterschiedlich, aber im selben Land sind. Im **Kieselstein ERP** wird dies so gehandhabt, dass in diesem Sonderfall die Debitorennummer der Rechnungsadresse verwendet wird. <!-- Derzeit ist auch hier die Vergabe eines eigenen Debitorenkontos erforderlich. -->
Beachten Sie dazu auch [Länderart](Export.htm#Länderart).

### Kassenbuch
<a name="Kassenbuch"></a>
Unter dem Reiter Kassenbuch (Für österreichische Anwender das Kassabuch) können Kassenbücher definiert werden. Jedes Kassenbuch ist (nur) eine andere Sicht auf das jeweilige Kassenkonto. Das bedeutet, dass im Kassenbuch alle Buchungen des jeweiligen Kassenkontos angezeigt werden. Ob die Buchungen nun über das jeweilige Konto durchgeführt werden oder komfortabel direkt im Kassenbuch ist für die eigentliche Buchhaltung ohne Bedeutung.
Daraus ergibt sich die Voraussetzung, dass vor dem Anlegen eines neuen Kassenbuches das darunter liegende Kassenkonto definiert sein muss.

Funktionen die im Kassenbuch nicht zur Verfügung stehen, aber manchmal notwendig sind:
-   Löschen einer Buchung:
    Um eine Buchung aus dem Kassenbuch zu löschen, gehen Sie bitte direkt ins Kassenkonto. Wählen Sie die gewünschte Buchung aus und wählen Sie im Menü, Bearbeiten, Buchung stornieren.

Buchungen im Kassenbuch

Im Kassenbuch werden die Buchungen auf Debitoren und Kreditoren stets ohne Ust/Vst durchgeführt. Bei einer Verbuchung auf Sachkonten wird die Steuerkategorie aus der indirekten Länderartübersetzung errechnet.<br>
Bei Änderungsbuchungen die mit MwSt behaftet sind, wird der MwSt Satz (genauer die Bezeichnung) aus dem Steuerbetrag errechnet. Bei sehr kleinen Beträgen oder bei gleichen Steuersätzen kann es hier unter Umständen zu nicht ganz exakten Vorbesetzungen des MwSt Satzes kommen.

#### Für welche Buchungen wird überhaupt in der Kasse eine Umsatzsteuerbuchung benötigt ?
In vielen handwerklich geführten Firmen ist es noch immer nicht üblich billige Artikel (unter 100,- € pro Artikel) in der Lagerwirtschaft zu berücksichtigen. Da aber doch immer wieder diese Artikel an Kunden verkauft werden, wird noch gerne ein Handbeleg (Paragon) geschrieben. Der Zahlbetrag wird direkt in bar einbehalten. Natürlich muss diese Barzahlung mit der enthaltenen Umsatzsteuer verbucht werden. Dies wird im Kassenbuch gemacht.

#### Warum können manche Kassenbuchungen nur ohne Umsatzsteuer / Vorsteuer verbucht werden?
Mit der direkten Buchung zwischen einem Bank-/Kassenkonto und einem Debitor / Kreditor ist NUR eine Bewegung von Geld / Zahlungen verbunden. Das bedeutet dass diese Zahlungsbuchung immer ohne Umsatzsteuer zu verbuchen ist.<br>
Hingegen ist ein Aufwand natürlich gegen ein Aufwandskonto, also Sachkonto zu verbuchen. Auf dieser Buchung ist die Vorsteuer zu verbuchen.

Beispiel:<br>
Sie kaufen im Baumarkt Schrauben zum Bruttopreis (inkl. MwSt) von 120,- €.

Wenn Sie nun den Kassenbeleg Kasse gegen Baumarkt verbuchen, so wird nur "notiert", dass Sie dem Kreditor Geld gegeben haben, aber nicht wofür. Daher auch keine Vorsteuer.

Dies ist in jedem Falle FALSCH, da der Aufwand nicht verbucht ist.

Richtig ist:

Sie buchen Kasse gegen Wareneinsatz (oder Hilfsstoffe o.ä.). Damit bewirkt auch diese Buchung, dass dieser Aufwand auf den Sachkonten landet.<br>
Wenn Sie nun trotzdem wissen wollen, welchen Umsatz Sie denn mit dem Baumarkt im Jahr machen, so nutzen Sie bitte die Eingangsrechnungsverwaltung.<br>
Sie erfassen hier den Betrag (der Schrauben) als Eingangsrechnung, mit dem Aufwandskonto und buchen dann die Zahlung gegen das Kassenbuch.

#### Kann die MwSt auch in der manuellen Buchung erfasst werden?
Ja, dies ist Abhängig von der Definition der beteiligten Konten. Siehe dazu bitte Beschreibung [manuelle Buchung](#Manuelle Buchung) mit [MwSt](#Manuelle Buchung mit MwSt).

## UVA Verprobung
<a name="UVA Verprobung"></a>
Die UVA Verprobung prüft bei der Aktivierung ob alle Belege aus Eingangs-/Ausgangsrechnungen bzw. Gutschriften des Zeitraumes auch in der Fibu enthalten sind. Ist dies nicht der Fall, so erscheint eine entsprechende Fehlermeldung. Auf eine Prüfung des Vorhandenseins aller Belege in der Fibu wurde verzichtet, um die Umstellung von Fremd-Fibu auf die integrierte Fibu zu ermöglichen.

Bitte achten Sie auf die durchgängige Durchführung aller UVA-Verprobungen, also, dass ab dem Beginn der Verwendung der integrierten **Kieselstein ERP** Finanzbuchhaltung für jedes Monat / Quartal die UVA Verprobung durchgeführt wurde und die UVA für jeden Zeitraum physikalisch ausgedruckt wurde.

Erscheint beim Ausdruck der UVA

![](UVA_Verprobung_fehlgeschlagen.jpg)

UVA Verprobung fehlgeschlagen, so konnten die angeführten Belege (Rechnungen, Gutschriften, Eingangsrechnungen) nicht in die Fibu übernommen werden. Sorgen Sie dafür, dass die Belege in die Fibu übernommen werden, [siehe Einzelbelegübernahme](#Einzelbelegübernahme) und führen Sie danach die UVA erneut durch.

**Hinweis:**

Bei der Prüfung der UVA Verprobung werden immer alle Belege im Zeitraum überprüft, unabhängig vom eventuell abweichend ausgewähltem Finanzamt. D.h. wenn Sie die UVA für ein weiteres Finanzamt durchführen, so können bei der Überprüfung der Verprobbarkeit trotzdem unstimmige Rechnungen aus einem anderen Finanzamt aufscheinen.

**Hinweis:**

Es wird bei der Verprobung jedoch **<u>nur</u>** der angegebene Zeitraum geprüft. Also in der Regel das Monat / das Quartal für das Sie die UVA ausdrucken. Für die Jahres-UVA wird keine Verprobung und somit auch keine Überprüfung der Belege durchgeführt.

Wichtig: Die **Verprobung** wird erst beim **physikalischen** Ausdruck der UVA vorgenommen und damit wird erst unmittelbar vor dem Ausdruck die Verbuchung der Belege des Zeitraums in der UVA geprüft. D.h. für die Verprobung und die Prüfung muss auf den Druckknopf (Strg+P) geklickt werden.

![](UVA_Verprobung_aktivieren.jpg)

#### Bei der UVA Verprobung erscheint eine Fehlermeldung
Es erscheint z.B. die Fehlermeldung "Ungültiges Konto 4720 in Buchung auf Steuerkonto 3806."

Dies bedeutet, dass in den Buchungen des Steuerkontos 3806 (1) Buchungen des Kontos 4720 enthalten sind. Dieses Konto (4720) hat jedoch eine falsche Definition der UVA-Art. Da hier die Gefahr gegeben ist, dass eine ungültige UVA abgegeben wird, wird die Berechnung der UVA komplett abgebrochen. Stellen Sie die Buchungen und gegebenenfalls die Konten richtig. Erst danach kann (darf) die UVA ausgedruckt werden.

(1) dieses Konto ist im unteren Modulreiter Finanzamt, Steuerkategorie, Steuerkategoriekonto definiert. In der Regel ist es das Konto der Umsatzsteuer für die Erlöse bzw. der Vorsteuer für den Aufwand.

![](UVA_Fehlermeldung.jpg)

![](UVA_Fehlermeldung2.jpg)

{{% alert title="Hinweis" color="info" %}}
Es kann vorkommen, dass an dem einen Tag die UVA geht und am anderen nicht. Der Hintergrund ist oft darin zu finden, dass Umsatzsteuerkonten in den Steuerkategoriedefinitonen doppelt vergeben wurden. Man sieht dies im Journal Übersicht Steuerkategorien auf den letzten Seiten, unter UST Konten auf mehrfach vergeben prüfen. Hier dürfen keine doppelt vergebenen Konten angegeben sein.<br>
![](DoppeltVergebeneUstKonten.png)<br>
 Sollte dies der Fall sein so kann es zu einer Fehlermeldung ähnlich der oben beschriebenen kommen.<br>
![](UVA_Fehlermeldung3.png)<br>
Hier muss die Definition bereinigt werden. Ev. müssen dafür eigenständige Konten angelegt werden.
{{% /alert %}}


#### Nach welchen Regeln wird die Prüfung der Steuerkonten für die UVA Verprobung durchgeführt?
wie oben bereits beschrieben wird vor dem Ausdruck der UVA eine Prüfung auf gültige Buchungen durchgeführt.

Erhalten Sie daher z.B. eine Fehlermeldung wie:

java.lang.Exception: Buchung: Rechnung 23/0000085, Buchung, 2023-08-16
Ungültiges Konto 2000 in Buchung auf Steuerkonto 3519

oder

java.lang.Exception: Buchung: Rechnung 13/0000088, Buchung, 2023-08-02 Ungültiges Konto 3519 in Buchung auf Steuerkonto 4020 so sind Konten falsch definiert.

Die Reihenfolge der Prüfung ist wie folgt:

Für jedes Konto, welches in den Steuerkategorien für den Verkauf definiert ist, werden für das gewählte Finanzamt folgende Buchungen analysiert:
- Alle Buchungsdetails im gewählten Zeitraum die nicht storniert und die keine Automatikbuchungen sind
- Ist das Konto ein Sachkonto, dann wird beurteilt
    - Ist es eines der Ausnahmekonten (siehe unten) --> OK
    - Ist es ein Bankkonto --> OK
    - Ist es ein Kassenkonto --> OK
    - Ist die UVA Art "nicht definiert" --> Fehler
    - Ist die UVA Art "Zahllastkonto" --> Fehler
    - Ist es eine Buchung mit der Buchungsart Eröffnungsbuchung --> OK
- Ausnahmekonten, je Steuerkategorie, sind
    - Alle Konten mit Kontoart UST-Sammelkonto
    - Konto Forderungen
    - Konto Kursgewinn
    - Konto Kursverlust
    - Konto Anzahlung verrechnet
    - Konto Anzahlung erhalten

Wichtig:

Das mitlaufende Konto Forderungen (z.B. 2000) muss zu den in der Steuerkategorie definierten UST-Konten passen.

#### Beim Druck der UVA ist der Haken Verprobung nicht gesetzt
Üblicherweise ist beim Druck der UVA der Haken ![](UVA_Verprobung_angehakt.png) bei der Verprobung angehakt. Ist dem nicht so, so ist durch den Parameter FINANZ_SAMMELBUCHUNG_MANUELL definiert, dass die bei der UVA Verprobung durchgeführten Buchungen manuell, also selbst gemacht werden.
Du verlierst damit die Sicherheitsüberprüfung ob die Konten in sich richtig definiert sind.

#### Wie erhalte ich mehr Informationen zur UVA-Verprobung?
Für die fundierte und umfangreiche Aufschlüsselung und Dokumentation der Buchungen für die UVA-Verprobung finden Sie im Modul Finanzbuchhaltung, Journal die Ust-Verprobung.
Im Ust-Verprobungsblatt werden alle Saldenliste Buchungen, die die UVA betreffen über alle Steuerkategorien hinweg angeführt. 

Der Aufbau ist dabei wie folgt:<br>
In den Spalten werden jeweils die Beträge, der dazugehörige Steuersatz und der sich daraus ergebende Steuerbetrag angeführt. 

Die oberste Gliederung bildet die Steuerkategorie. Wie auch in der UVA wird zu erst die Umsatzsteuer und danach die Vorsteuer angeführt. Innerhalb der Umsatz- und Vorsteuer erfolgt die Aufteilung nach Steuersätzen.

- Steuerkategorie Inland
    - UST
        - UST 10%
            - Erlöskonten
        - UST 20%
            - Erlöskonten

    - VST
        - VST 10%
            - Aufwandskonten
        - VST 20%
            - Aufwandskonten

- Steuerkategorie Ausland EU mit UID
(...)

Es besteht die Möglichkeit, dass die UstVerprobung von der UVA um wenige Cent abweicht. Das kommt daher, dass die UVA die Steuer wie folgt berechnet:

(Saldo der UVA Konten) * (Steuersatz zum Zeitpunkt) = Steuerbetrag<

Die Ust-Verprobung bildet die Summe über die tatsächlichen Steuerbuchungen, sowohl zur Berechnung von Nettobetrag und Ust-Betrag. Die Ust-Verprobung ist also genauer als die UVA selbst.

Die Währung ist immer die Mandantenwährung.

#### Differenz der Zahllastbeträge
Bei der Ermittlung der UVA werden, wenn die Kette der Finanzamts-Sammelbuchungen (Siehe Fibu, Finanzamt, Journal, Steuerkategorien) durchgängig definiert ist, zwei Salden angedruckt. Diese sollten, wie oben beschrieben, bis auf wenige Cent zusammenstimmen. Ist dies nicht der Fall,<br>
![](UVA_Differenz.png)<br>
muss davon ausgegangen werden, dass entweder Konten falsch definiert sind, oder Buchungen in Bezug auf die Umsatzsteuer in falsche Konten erfolgt sind.

Meistens kommt es davon, dass Erlöse, also Rechnungspositionen, manuell verändert wurden. D.h. es wurden zwar die Erlöse anhand der Artikelgruppe auf ein Erlöskonto mit allgemeiner Steuer verbucht, aber der Artikel selbst ist ohne / mit einer anderen Mehrwertsteuer beaufschlagt.

Dies findet man am einfachsten, wenn man in diesem Falle das Warenausgangsjournal für den Zeitraum betrachtet und sich die verschiedenen Mehrwertsteuersätze (Feld F_MWST) ansieht. Meist ist es dann sehr rasch entsprechend logisch.

Die Lösung dafür ist immer, dass man dafür eigene Artikel mit eigenen Artikelgruppen anlegt, die entsprechend in die richtigen Erlöskonten buchen.<br>
**Hinweis:**<br>
Handeingaben buchen IMMER in das mit dem Parameter FINANZ_EXPORT_ARTIKELGRUPPEN_DEFAULT_KONTO_AR definierte Konto.

#### Beim Drucken der Ust-Verprobung erhalte ich eine Fehlermeldung?
Wenn bei der Aktualisierung der Ust-Verprobung eine Fehlermeldung erscheint, so wechseln Sie bitte in die Detail Darstellung.<br>
Hier könnte z.B. stehen:<br>
2015-07-09 15:55:52:496 [ 7185820] ERROR error(LpLogger:105) - Code=4125 java.lang.Exception: Eingangsrechng 15/0100370, Buchung, 2015-06-09
<!-- Konto Kontonr. SOLL                           | HABEN
______________________________________________|________________
Eidg. Oberzolldirektion     40500             | 2.197,30
Vorsteuer Mat/DL             1170     2.197,30|
Kreditoren Lief/Leist        2000             | 2.197,30
______________________________________________|________________
SUMME:                                2.197,30| 4.394,60 -->

| Konto | Kontonr. | SOLL | HABEN |
| --- | --: | --- | --- |
| Eidg. Oberzolldirektion | 40500 |           | 2.197,30 |
| Vorsteuer Mat/DL | 1170 | 2.197,30 | |
| Kreditoren Lief/Leist | 2000 |  | 2.197,30 |
| SUMME:   | | 2.197,30| 4.394,60 |

Dies bedeutet, dass auf dem Konto 1170, welches als (reines) Vorsteuerkonto definiert ist / sein muss, Buchungen aus Eingangsrechnungen enthalten sind. In diesem Falle sind dies reine Kosten von Einfuhrumsatzsteuern, welche wie unter [reine Vorsteuerzahlungen verbuchen](#Reine Vorsteuerzahlungen) beschrieben verbucht werden müssen.

Bitte beachten Sie dass diese Detaillierung aus Gründen der Überprüfbarkeit erforderlich sind.

#### ich bekomme bei der Ust-Verprobung die Fehlermeldung mehrere Mehrwertsteuersätze?
Wenn Sie bei der Durchführung der Ust-Verprobung die Fehlermeldung
![](mehrere_MwSt_Saetze.jpg)
erhalten, so bedeutet dies dass das angeführte Konto in mehreren Steuerkategorien verwendet ist. Damit ist einen eindeutige Auflösung der Steuerbuchungen nicht mehr möglich.
Um herauszufinden, welche Steuerkategoriedefinitionen hinterlegt sind nutzen Sie bitte das Journal Steuerkategorien im unteren Modulreiter Finanzamt.

#### Wie wird ein negativer Skontoaufwand verbucht?
Negativer Skontoaufwand ergibt sich dann, wenn selten aber doch Ihre Kunden auch Mahngebühren bezahlen.<br>
Wie kann diese Überzahlung einer Rechnung verbucht werden?<br>
Sie buchen einfach den erhöhten Betrag auf die Zahlung und erledigen die Rechnung. Somit stimmt die Bank und der mehr bezahlte Betrag wird in das SkontoKonto als negativer Skontoaufwand, also als Erlös verbucht. Das hat bei größeren Beträgen, z.B. aus einer Inkasso Betreibung, den Nachteil, dass Sie dafür Umsatzsteuer abführen. Da dieses Geld aber ein Erlös aus Finanzgebarung ist, ist dieser Erlös nicht Umsatzsteuer pflichtig. D.h. Sie führen Steuern ab, wofür dies nicht erforderlich ist.<br>
D.h. es muss diese Überzahlung vom Skonto-Konto weitergebucht werden z.B. auf 8080 Zinserträge Kunden. D.h. Sie buchen z.B.

| Konto | Bezeichnung | Soll | Haben |
| --- |  --- |  --: |  --: |
| 4420 | Skontoaufwand 20% | 175,59 |   |
| 3540 | Umsatzsteuer 20% | 35,12 |   |
| 8080 | Zinserträge Kunden |   |  210,71 |

Somit wird dieser Finanzerlös unter Berücksichtigung des Umsatzsteueranteils als gesamtes auf z.B. Zinserträge Kunden umgebucht.<br>
Damit nun bei der UVA die Buchung(en) auf das Koto Zinserträge als gültig erkannt werden, aber NICHT in der UVA berücksichtigt werden, stellen Sie bitte die UVA Art dieses Kontos auf Umsatz Inland steuerfrei.
![](Umsatz_steuerfrei.gif)

#### Bei der UVA erscheinen orange Zeilen
Wenn beim Druck der UVA orange Zeilen erscheinen,<br> ![](UVA_Orange_Zeilen_01.png)<br>
so bedeutet dies, dass für die UVA Art der MwSt Steuersatz anhand der Steuervariante nicht definiert ist. Am Einfachsten findest du die betreffenden Konten, indem du die Auswahlliste der Sachkonten nach der UVA-Art sortierst ![](Sachkonten_nach_UVA_ART_sortiert.png) und zur angegebenen UVA-Art runter-scrollst.<br>
![](UVA_Orange_Zeilen_02.png)<br>
Nun sieht man, dass in der Spalte Variante für z.B. das Konto 4002 keine MwSt Variante definiert ist. D.h. in die Kopfdaten wechseln,<br> 
![](UVA_Orange_zeilen_03.png)<br>
und den passenden MwSt-Satz eintragen. Bitte beachten den Unterschied von nicht definiert (=Leer) und Steuerfrei 0%. Danach die UVA aktualisieren.

#### Aufgrund der UVA-Verprobung ist für den Zeitraum keine Buchung erlaubt.
Diese Meldung besagt, dass im Zeitraum oder auch im Zeitraum danach bereits eine UVA-Verprobung durchgeführt wurde. Daher darf keine Buchung in diesem Zeitraum durchgeführt werden. Tritt diese Meldung beim Drucken (Druck in die Druckvorschau) der UVA auf, so wurde nach dem gewählten Zeitraum für den Ausdruck der UVA bereits eine UVA Verprobung durchgeführt. Dies wird neben dem Finanzamt angezeigt.

Um die UVA ausdrucken zu können, müssen alle nachfolgenden [Verprobungen zurückgenommen](#UVA-Verprobung zurücknehmen) werden.

**Hinweis:** Achten Sie darauf, dass die UVA Verprobungen in chronologisch richtiger Reihenfolge gedruckt werden, dann kommt es auch nicht zu diesen Fehlermeldungen.

#### Was ist zu tun, wenn eine Zahlung als nicht UVA Verprobt angeführt wird.
Gehen Sie auf die Rechnung und dann in die Zahlung und führen Sie mit ändern, speichern die Übernahme erneut durch. Zu Ihrer Sicherheit sollten Sie überprüfen, ob die Buchung tatsächlich in den Konten vorhanden ist.

<a name="Buchung wegen UVA nicht erlaubt"></a>

#### Buchung ist aufgrund UVA-Verprobung nicht erlaubt
Erscheint die Fehlermeldung "Buchung zum JJJJ-MM-TT ist aufgrund UVA-Verprobung nicht erlaubt", so bedeutet dies, dass in diesem Zeitraum bereits die UVA an das Finanzamt gesandt, also ausgedruckt wurde. Wenn wirklich trotzdem Buchungen durchgeführt werden müssen, so nehmen Sie bitte die [UVA Verprobung zurück](#UVA-Verprobung zurücknehmen), führen Sie die Buchung durch und drucken Sie danach die UVA für den relevanten Zeitraum erneut aus. Dass eine eventuelle Änderung der Daten in der Finanzamtsmeldung entsprechend zur berücksichtigen ist, erklärt sich von selbst.

#### Beim Druck der UVA erscheint anstatt der Steuer null?
Für diese Felder fehlen die Zuordnungen der Mehrwertsteuersätze zu den Drucknamen. Bitte deinen **Kieselstein ERP** Betreuer um die Ergänzung in der LP_MWSTSATZBEZ.C_DRUCKNAME bzw. eine detaillierte Überprüfung der FB_UVAART. Hier ist insbesondere auf die Vollständigkeit der Definitionen der UVA Arten zu achten. D.h. es müssen alle, in der default Auslieferung enthaltenen UVA Arten z.B. auch für weitere Mandanten gegeben sein. In einem Beispiel haben die C_NR'n für
- Verrechnungskonto
- Vorsteuer betr KFZ
- Vorsteuer betr Gebaeude
- Werbeabgabe<br>
gefehlt, was zu obigen null Werten geführt hat.

#### Wird die UVA auch in der Dokumentenablage abgelegt?
Ja. Ebenso wie die Saldenliste wird jede ausgedruckte UVA in der Dokumentenablage abgelegt. Sie finden diese im Modul System, unterer Modulreiter Dokumente. Öffnen Sie nun den Baum Ihres Mandanten, Ast Finanzbuchhaltung, UVA, dann das entsprechende Finanzamt und das gewünschte Monat / Periode.<br>
![](UVA_Dokumentenablage.png)  

**Hinweis:** Ausgedruckte Saldenlisten werden in ähnlicher Weise abgelegt.

#### Wie sind IG-Erwerbsbuchungen durchzuführen / zu parametrieren?
Innergemeinschaftliche Erwerbsbuchungen zeichnen sich dadurch aus, dass bei der Einfuhr zum Erhalt der Vorsteuereigenschaft die Einfuhrumsatzsteuer zu entrichten ist. Diese wird durch die Vorsteuervergütung wieder rückerstattet. Damit diese beiden Felder gebucht werden, ist einerseits das IG-Vorsteuerkonto zu definieren und andererseits das (IG-) Einfuhrumsatzsteuerkonto.

Erhalten Sie nun eine Eingangsrechnung eines Lieferanten mit UID-Nummer, so wird bei der [Erfassung der Eingangsrechnung](../Eingangsrechnung/index.htm#Reverse Charge, IG-Erwerb) der IG-Erwerb angegeben und damit auch, dass der Steuersatz faktisch nur informellen Charakter hat. D.h. Sie erfassen als Zahlbetrag nur den Nettobetrag der Rechnung.

Zur richtigen Definition ist unter Vst.Einkauf das Vorsteuerkonto und unter Einfuhr-Ust. das Konto für die Einfuhrumsatzsteuer anzugeben.

**Hinweis:**

Damit das bebuchte Aufwandskonto auch in der UVA richtig ausgewiesen wird, muss die UVA Art des Kontos auf IG-Erwerb Normalsteuer oder reduzierter Steuersatz gestellt sein.

Beachten Sie dazu bitte auch [IG-Erwerb](../Eingangsrechnung/index.htm#Reverse Charge, IG-Erwerb).

<a name="Reine Vorsteuerzahlungen"></a>

#### Wie sind reine MwSt / Vorsteuer Zahlungen zu buchen?
Für die richtige Erfassung von reinen Vorsteuerrechnungen, z.B. vom Eidgenössischen Finanzamt, [siehe bitte](../Eingangsrechnung/index.htm#reine Vorsteuerrechnungen).
Bitte beachten Sie dafür zusätzlich, dass in der Definition der Steuerkategorien dafür ein eigenes Vorsteuerkonto erforderlich ist.

<a name="Import Sachkonten"></a>Übertragen von Sachkonten von anderen Installationen

Es besteht immer wieder der Wunsch, dass der jeweils gewünschte / erforderliche Kontenplan importiert werden können sollte.<br>
Die Vorgehensweise dafür ist wie folgt:
1. Export eines bestehenden Kontenplanes<br>
Für den Export, gehen Sie im Modul Finanzbuchhaltung auf den unteren Modulreiter Sachkonten, wählen nun Journal Konten. Nun wählen Sie den integrierten CSV Export ![](CSV_Export.gif) und speichern die Datei in ein für Sie sprechendes Verzeichnis.
2. Import eines Kontenplanes<br>
Gehen Sie im Modul Finanzbuchhaltung auf den unteren Modulreiter Sachkonten.
Wählen Sie nun in der oberen Menüleiste Sachkonten, Sachkontenimport.<br>
![](Sachkontenimport.jpg)<br>
Nun erscheint ein entsprechendes Fenster, in dem eventuelle Fehler aufgeführt sind. Beseitigen Sie diese und starten Sie danach den Importvorgang erneut. Ist die Datei fehlerfrei, so werden durch Klick auf den Knopf importieren die Kontendefinitionen importiert.<br>

Eine Musterdatei für den österreichischen Sachkontenrahmen finden Sie auch auf Ihrer Installations CD bzw. können wir Ihnen diesen gerne zur Verfügung stellen. Ebenso können der SKR03 und der SKR04 in reduzierter Form zur Verfügung gestellt werden.
Für den Import beachten Sie bitte unbedingt, [welche Sachkonten sollte importiert werden](Integrierte_Finanzbuchhaltung_starten.htm#Welche Sachkonten).

## UVA / Umsatzsteuervoranmeldung

Eine Sammlung von Fragen und Antworten, Hinweisen zur Umsatzsteuervoranmeldung.

#### Für welche Zeiträume kann die UVA ausgedruckt werden ?
Die UVA kann monatlich, quartalsweise oder jährlich gedruckt werden. Siehe dazu bitte auch den Parameter FINANZ_UVA_ABRECHNUNGSZEITRAUM.

{{% alert title="ACHTUNG" color="warning" %}}
Für den Betrachtungszeitraum des Quartals wird immer die Periode verwendet. Dies kann bedeuten, dass bei abweichenden Geschäftsjahren die Quartalsbetrachtung versetzt ist.<br>
In anderen Worten: Eine Quartalsbetrachtung macht nur Sinn, wenn auch das **Geschäftsjahr mit dem Kalenderjahr ident** ist.
{{% /alert %}}

#### In der UVA wird bei Steuer null angedruckt und man sieht keine Steuersummen.
<a name="Falsche Steuersätze in der UVA"></a>
Es ist in der Mehrwertsteuerbezeichnungstabelle der Druckname nicht definiert. Bitten Sie Ihren **Kieselstein ERP** Betreuer diese entsprechend zu ergänzen.

Werden bei den Steuersätzen falsche Sätze<br>
![](UstSatz_UVA_0.gif)<br>
oder Null<br>
![](UstSatz_UVA_null.gif)<br>
angedruckt, so ist die Zuordnung des zusätzlichen Mandanten in der MwStSatzBezeichnung (C_DRUCKNAME) falsch definiert. Bitte deinen **Kieselstein ERP** Betreuer diese zur ergänzen / richtigzustellen.
![](Ust_Satz_definieren.gif)

#### In der UVA wird der Umsatz mit null angedruckt
Dies tritt dann auf, wenn z.B. das Formular für die deutsche Umsatzsteuervoranmeldung verwendet wird, aber beim Finanzamt das Umsatz runden nicht angehakt ist.

![](UVA_Elster_nicht_gerundet.gif)

Haken Sie in den Kopfdaten des Finanzamtes Umsatz runden an und aktualisieren Sie den Druck der UVA.

#### In der UVA werden die (Anzahlungs-) Erlöse mit falschem Vorzeichen gedruckt.
Bitten Sie Ihren **Kieselstein ERP** Betreuer dies in der UVA Definitionstabelle richtig zu stellen.

#### Wird die letzte UVA Verprobung angezeigt?
Ja. Um die zuletzt verprobte UVA herauszufinden, gehen Sie am Einfachsten in den Sachkonten auf Journal, UVA und klicken auf aktualisieren. Nun werden Zeitraum und Zeitpunkt der letzten UVA Verprobung angezeigt.

![](UVA_Verprobungs_Anzeige.gif)

Diese Anzeige bedeutet: 2012/1 es wurde die Verprobung für den Januar 2012 am 15.3.2012 um 9:08 durchgeführt.

#### Kann eine UVA Verprobung zurückgenommen werden.
<a name="UVA-Verprobung zurücknehmen"></a>
Ja, wählen Sie in den Sachkonten, bearbeiten, UVA Verprobung zurücknehmen, wählen Sie das gewünschte Finanzamt und bestätigen Sie die Meldung mit ja. Für diese Rücknahme ist das Chefbuchhalterrecht erforderlich.

#### In welcher Währung wird die UVA angedruckt?
Für den Druck der Beträge in der UVA wird die Währung verwendet, welche beim Land des jeweiligen Finanzamtes hinterlegt ist. Ist beim Land keine Währung hinterlegt so wird die  Mandantenwährung verwendet.

Ist eine eventuelle Umrechnung erforderlich, so wird immer der zum Monatsletzten gültige Kurs für die Umrechnung verwendet.

**Hinweis:**

Quartals- und Jahres-UVA werden als eine Summe der monatlichen UVA errechnet, daher greift auch hier der jeweils zum Monatsletzten gültige Kurs.

#### Können die Umsätze der UVA gerundet werden?
Ja. Definieren Sie dazu in den Kopfdaten des entsprechenden Finanzamtes ![](UVA_Umsatz_runden.gif) Umsatz runden. Es werden dadurch die an das UVA Formular übergebenen Umsätze auf ganze Beträge gerundet.

#### Wie kann ich die UVA nachrechnen?
Die Werte in der UVA müssen mit denen in der Saldenliste übereinstimmen. Welche Konten in der UVA für welche Bereiche angedruckt werden, wird durch die UVA Art in den Kopfdaten des jeweiligen Kontos bestimmt.<br>
Um eine Übersicht über alle Zuordnungen zwischen Konto und UVA Art zu sehen, verwenden Sie am Besten den Kontenplan. Sie finden diesen unter Journal, Konten. Wählen Sie dann Sachkonten aus.
Aus der Zusammenschau Kontenplan mit UVA Art und Saldenliste ergibt sich unter Berücksichtigung des Zeitraumes der jeweilige Saldo, welcher in der UVA angedruckt wird.
Beachten Sie bitte:
- weitere Finanzämter
- eventuell abweichende Währungen bei anderen Finanzämtern.

Dazu ein Beispiel von unseren Schweizer Kunden:<br>
Die Saldenliste ist in der Regel in CHF (Schweizer Franken). Hat der Anwender aber noch eine Steuernummer in Konstanz, so wird auch eine UVA nach deutschen Regeln in EUR abgegeben.<br>Beachten Sie, gerade bei Quartalsweiser UVA dass, trotzdem die Umrechnung zwischen CHF und EUR zum jeweils zum Monatsletzten gültigen Kurs durchgeführt wird und dass für Deutschland die Umsätze auf ganze Euro gerundet abgegeben werden müssen.

#### Gibt es eine schnelle Prüfung ob der Druck der UVA mit den automatischen Sammelbuchungen übereinstimmt?
Unter der Voraussetzung, dass **Kieselstein ERP** auch die sogenannten [Sammelbuchungen](#Finanzamtsammelbuchungen) vom UST/VST Konto bis zum Finanzamtszahllastkonto machen soll/darf, kann der errechnete Zahllastbetrag als Vergleich zu den im Formular errechneten Steuerbeträgen verwendet werden. Mit dieser doppelten Berechnung erhalten Sie Sicherheit, dass die für die UVA relevanten Konten richtig definiert sind.<br>
D.h. vergleichen Sie beim Druck der UVA die Beträge Steuerschuld/-guthaben errechnet mit dem Betrag Laut Zahllastkonto. Diese müssen, bis auf wenige Cent/Rappen übereinstimmen. Die Cent-Differenzen ergeben sich aus der unterschiedlichen Methodik der Berechnung der Ust/Vst-Beträge.

Der Wert des auf der UVA angedruckten Zahllastkontos wird wie folgt errechnet:

Am Konto mit der Kontoart Zahllastkonto wird die Summe der Buchungen aus den Konten mit der Kontoart UST- oder Erwerbssteuerkonto abzüglich der Buchungen aus den Konten mit der Kontoart Zahllastkonto für die gewählte Periode errechnet.

Hinweis: Zahllastkonten für den UVA-Druck sind Konten, die als **Kontoart** Zahllastkonto hinterlegt haben. Es kann daher, je Finanzamt, nur ein Sachkonto mit der Kontoart Zahllastkonto geben. Ist auch dieses nicht definiert, wird NULL als Ergebnis zurückgeliefert und somit die Informationszeile des Zahllastkontos nicht angedruckt.

Um eine noch höhere Sicherheit, ob die UVA stimmen kann, zu erreichen empfehlen wir, die ausgewiesenen Umsätze mit dem Rechnungsjournal alle Rechnungen (inkl. Gutschriften), ER-Kontierungsjournal und gegebenenfalls Zusatzkosten-Kontierungsjournal zu überprüfen. Unter der Voraussetzung, dass alle steuerlich relevanten Belege in einem der drei Bereiche erfasst sind, müssen sich die Steuerbeträge aus diesen drei Journalen errechnen lassen. Einzig der Skontoertrag / -aufwand ist als Differenz zulässig.

##### Das Zahllastkonto wird auf der UVA nicht angedruckt
Wird das Zahllastkonto auf der UVA nicht angedruckt, so fehlt die durchgängige Definition für die Weiterbuchung auf das FA-Zahllastkonto.<br>
So sieht man hier<br>
![](FehlendeKontoartenFuerUVA.png)<br>
dass die Sachkonten (Definitionen) für die Kontoarten Vst Sammelkonto, Ust Sammelkonto und UST- oder Erwerbssteuerkonto fehlen. Diese sind notwendig um die Sammelbuchung von den Ust/Vorsteuerkonten bis hin zum FA-Zahllastkonto durchreichen zu können.

So sieht nach vollständiger Definition die Darstellung der Zusammenhänge der FA-Sammelbuchungen (Modul Fibu, unterer Reiter Finanzamt, Menü, Journal, Steuerkategorien) wie folgt aus:<br>
![](KorrigierteKontoartenFuerUVA.png)

##### komische, unvollständige UVA
Erscheint die UVA unvollständig, mit fehlenden Überschriften bzw. Steuerwerten,
![](unvollstaendige_UVA.png)<br>
so hat dies mehrere Ursachen:
- die orange Zeilen bedeuten, dass (Corona Zeit) der anzuwendenden Steuersatz für diese UVA Art noch nicht im Detail definiert ist.<br>
D.h. es muss, in den jeweiligen Konten, die UVA Variante definiert werden, damit das Formular weiß wie es rechnen sollte.<br>![](UstVarianteDefinieren.png)<br>
Bitte auch für das Vorsteuerkonto in dem Falle auf 0% und den Drittlandserwerb mit 0%

- Zusätzlich sind derzeit leider **einmalige** Arbeiten erforderlich. D.h.
  - Man muss die Pflege laufen lassen.<br>Also in der Fibu, Finanzamt, Menüpunkt Finanzamt, Pflege UVA Formular ![](Finanzamt_Pflege.png)
  - für die Verwendung von Elster muss **danach** noch das [Script]( {{<relref "/aenderungsprotokoll/" >}} ) gestartet werden. [Direkt zum Script](/aenderungsprotokoll/Update_UVA_Elster.sql)

#### Wie erfolgt die Abstimmung der Finanzamtszahlungen mit dem Finanzamt?
Zumindest in Österreich, genauer mit dem österreichischen Finanzamt, gibt es die Möglichkeit sich über Finanz Online jederzeit einen aktuellen Stand des Finanzamtskontos anzusehen, auszudrucken. Betrachten Sie diese Daten wie ein Bankkonto. D.h. hier finden Sie die Belastungen durch das Finanzamt und Ihre Zahlungen und somit den aktuellen Kontostand.<br>
Daher empfiehlt es sich auch alle Ihre Meldungen wie DB, DZ, Lohnsteuer, Körperschaftssteuer, Umsatzsteuer über ein gemeinsames Finanzamtszahllastkonto laufen zu lassen und so einen schnellen und guten Überblick der Verbindlichkeiten / Guthaben beim Finanzamt zu haben.<br>
Das bedeutet wiederum, dass z.B. DB/DZ zwar z.B. auf das Konto 3521 buchen, dies aber auf das Zahllastkonto, oft auch Verrechnungskonto Finanzamt genannt, automatisch oder manuell weiterbuchen.
![](Automatikbuchung_FA_Sammelbuchung.gif)
Somit haben Sie auf Ihrem Zahllastkonto spiegelbildlich die Kontobewegungen Ihres Finanzamtskontos und einen klaren Überblick und somit sehr rasch die passende Information, wenn es zu eventuellen Unstimmigkeiten kommt. In der Praxis bewährt hat sich, den Kontostand zumindest jedes Quartal abzustimmen.

Wie oben ersichtlich (Konto 2520 und 2521) werden in die Errechnung des Finanzamtszahllastkontos auch die Vorsteuerkonten aus EU und Reversecharge mit aufgenommen. In der UVA sind aber die Konten 2520, 2521 nicht enthalten. Damit ergibt sich automatisch eine Differenz zwischen dem im UVA Formular errechneten Zahllastwert und den in Ihrem **Kieselstein ERP** verbuchten Wert, was sich z.B. wie folgt auf Ihrer UVA darstellen könnte.<br>
![](UVA_Zahllastdifferenz.jpg)<br>
Die Ursache ist eben die leider weitverbreitete Meinung, dass fremde Vorsteuer (die Vorsteuer eines Finanzamtes, eines Landes in dem Sie kein Umsatzsteuerkonto haben) auch als Vorsteuer zu definieren ist, was falsch ist. [Siehe dazu auch](Zusatzinfos.htm#Fremde Vorsteuer).

#### Wie sind die Konten für Anzahlungsrechnungen im Sinne der UVA zu definieren?
Damit die Anzahlungen in Ihrer UVA entsprechend berücksichtigt werden, müssen für die beiden [Anzahlungskonten](#Anzahlung-Schlussrechnung) (Erhaltene und Erhaltene bezahlt) in der UVA Art Anzahlung Normalsteuer definiert sein.

Siehe dazu auch unterer Modulreiter Finanzamt, Buchungsparameter.
Die Anzahlungskonten für Reverse Charge (in Österreich: Bauleistungen §19 Abs. 1a) wünschen sich viele Steuerberater im Sinne der UVA auf der Zeile Reverse Charge (in Österreich: Kennziffer 21). Dafür definieren Sie bitte für die beiden Reverse Charge Anzahlungskonten (Erhaltene und Erhaltene bezahlt) die UVA Art ebenfalls auf Reverse Charge. Dies bewirkt, dass bereits Anzahlungen in der UVA als Reverse Charge Bauleistungserlös betrachtet werden.
Bitte beachten Sie, dass ev. Skontozahlungen auf Anzahlungsrechnungen, auch wenn dies vom Gesetzgeber nicht so gedacht ist (aber in der Praxis gemacht wird), in dieser Betrachtung berücksichtigt sind. Wichtig: Es müssen beide Konten auf die UVA Art Reverse Charge gesetzt werden.

#### Wie sind die Konten für Anzahlungsrechnungen für andere Steuerkategorien zu definieren?
Die oben angeführte Definition in den Buchungsparametern gilt als Basiseinstellung für Inlandsbuchungen. Wenn Sie nun die Fehlermeldung erhalten, dass für die Steuerkategorie noch kein Abstimmkonto definiert ist, gehen Sie bitte wie folgt vor:
Legen Sie ein neues Anzahlungskonto für die Steuerkategorie z.B. Anzahlungsrechnungen Drittland an und wechseln Sie in das in den Buchungsparametern definierte Basiskonto der Anzahlungen. Nun können Sie im Reiter Konto Länderart das neu angelegte (oder auch schon ein vorhandenes) Konto für die Länderartübersetzung hinterlegen. Klicken Sie dazu auf Neu und wählen das Konto und die dazugehörige Länderart, sowie das Finanzamt aus und klicken auf Speichern.

#### Bei der Verbuchung einer Inlands-Anzahlungsrechnung erhalte ich eine Fehlermeldung?
Wenn Sie die nachfolgende Fehlermeldung Sachkontendefinition(en) möglicherweise fehlerhaft erhalten
![](Anzahlungsverbuchung.jpg)<br>
Die UVA Art des Anzahlungskontos für die gelegten Anzahlungen muss auf nicht zutreffend definiert sein.<br>
Es darf nur das Erhaltene Anzahlungen Konto in der UVA mitgerechnet werden, da die Anzahlungen ja als Istversteuerung zu betrachten sind.

#### Die UVA kann nicht gedruckt werden, wie finde ich die Fehler?
Drucken Sie das Journal der Ust-Verprobung für den gewünschten Zeitraum aus. Hier erhalten Sie entsprechende Hinweise, welche Buchungen eine Aktivierung / Verprobung der UVA verhindern.

#### Was ist bei der Definition eines weiteren Finanzamtes noch zu beachten?
In der Regel hat jedes Land auch seine eigenen Mehrwertsteuersätze.

Um für die UVA die für dieses Land abweichenden Steuersätze verwenden zu können, müssen diese unter System, Mandant, MwSt-Bezeichnung zugeordnet werden. Wird keine Zuordnung getroffen, so wird davon ausgegangen, dass diese Steuersätze für das Standard Finanzamt verwendet werden.

#### Wie erreiche ich Sicherheit, dass alle Beleg tatsächlich in der Fibu sind?
Bevor die UVA ausgedruckt wird, wird eine Prüfung durchgeführt, ob alle Beleg im UVA Zeitraum auch in der Fibu enthalten sind. Gegebenenfalls werden diese nachgebucht.

Fehlen Belege, so kann die UVA nicht durchgeführt werden.

Durch die UVA Verprobung werden die Belege des Zeitraumes gesperrt und können, solange die Verprobung des Zeitraumes gegeben ist, nicht mehr verändert werden. Es erscheint die Meldung

![](Bereits_UVA_verprobt.jpg)

Der Beleg ist bereits in der Finanzbuchhaltung verbucht. Im Detail sehen Sie, dass die Ursache der Sperre die UVA Verprobung ist.

<a name="Sperren"></a>

#### Welche Sperren gibt es ?
Es gibt zwei Arten von Sperren:
1. Die Geschäftsjahressperre. 
Diese greift sehr tief in der Buchungslogik und dient eher der technischen Sicherheit. D.h. wenn alle anderen logischerweise erforderlichen Sperren (siehe 2.\) nicht greifen, greift diese Sperre zusätzlich. Diese Sperre sollte in aller Regel nach der Einreichung der Bilanz beim Finanzamt eingetragen werden, eventuell schon etwas früher.<br>
Hier hat sich auch bewährt, dass die Sperre bereits zum Zeitpunkt der Unterlagenübergabe an Ihren Steuerberater eingetragen wird. Den Nachteil, dass dann zu Verbuchung der Bilanzbuchungen diese Sperre durch deinen **Kieselstein ERP** Betreuer wieder entfernt werden muss, überwiegt der Vorteil, dass in diesem Zeitraum, keine versehentlichen Buchungen in den übergebenen Daten gemacht werden.
2. Die Sperre(n) durch die Umsatzsteuerverprobung.<br>
Diese automatische Sperre greift IMMER, wenn eine UVA echt ausgedruckt wird. Damit sind die Ein- und Ausgangsrechnungen, die Zahlungen und alle weiteren Buchungen des UVA-Zeitraumes als UVA verprobt gekennzeichnet und dürfen daher nicht mehr verändert werden.<br>
Diese Sperre kann von einem Anwender mit Chefbuchhalterrecht (FB_CHEFBUCHHALTER) wieder aufgehoben werden.<br>
Gehen Sie dazu bitte in die Fibu, Unterer Modulreiter Sachkonten. Im Menü wählen Sie nun Bearbeiten, Letzte UVA Verprobung zurücknehmen, wählen das Finanzamt aus und bestätigen die Rücknahme.<br>
Wir gehen hier davon aus, dass der/die Verantwortliche weiß was er/sie buchhalterisch macht.<br>
Auch diese Funktion ist für Ihre Sicherheit. Wenn die UVA beim Finanzamt eingereicht wurde, darf für den Zeitraum nichts mehr verändert werden.

#### Wie erreiche ich Sicherheit, dass im alten Geschäftsjahr nicht mehr gebucht werden kann / darf ?
Wir haben dafür den Menüpunkt: Bearbeiten, Aktuelles Geschäftsjahr sperren
geschaffen.
![](Geschaeftsjahr_Sperren.jpg)

Wählen Sie zuerst das Geschäftsjahr, welches für die Buchungen gesperrt werden sollte, über Bearbeiten, Geschäftsjahr und Anklicken des entsprechenden Jahres.
Nun wählen Sie Bearbeiten, Aktuelles Geschäftsjahr sperren. <u>Hinweis:</u> Für diese Funktion ist das Chefbuchhalterrecht erforderlich.<br>
Bestätigen Sie die beiden Abfragen entsprechend.

Damit wird das ausgewählte Geschäftsjahr für jegliche weitere Buchungen gesperrt. Diese Sperre ist tief unten in der Buchungslogik implementiert um für beide Seiten eine entsprechende Sicherheit zu erreichen, dass, wenn die Sperre gesetzt ist, keine Buchungen, aus welchen Gründen auch immer, in diesem Geschäftsjahr mehr durchgeführt werden können.
{{% alert title="ACHTUNG" color="warning" %}}
Dieser Eintrag kann von Ihnen nicht mehr rückgängig gemacht werden.

Für eine ev. doch erforderliche Rücknahme wenden dich bitte an deinen **Kieselstein ERP** Betreuer.
{{% /alert %}}


Änderung von Kontennummern

Grundsätzlich sind Änderungen von Kontonummern jederzeit möglich.
Durch die interne Logik von **Kieselstein ERP** ist die richtige Verkettung bis hin zum Buchungsjournal sichergestellt. **ACHTUNG:** Eine Änderung der Kontonummer muss jedoch auch Ihren Niederschlag in der Korrektur der bisherigen Buchungsbelege finden. Gehen Sie daher damit entsprechend sorgsam um.

Rechte in der Finanzbuchhaltung

Neben dem üblichen nur Lese Recht, ist für die Definition des Verhaltens das sogenannte FB_CHEFBUCHHALTER Recht erforderlich.<br>
In anderen Worten, ein Anwender der FB_CHEFBUCHHALTER nicht hat, hat nur die unteren Reiter
Sachkonten, Debitoren, Kreditoren, Kassenbuch, Zahlungsvorschlag.
Zusätzlich sind im Menü der Sachkonten, Debitoren, Kreditoren folgende Punkte nicht vorhanden:
- Sachkonten importieren
- Bearbeiten, Belegübernahme
- Aktuelles Geschäftsjahr sperren
- Finanzamtsbuchungen
- Der Menüpunkt Journal fehlt komplett

Zusätzlich<br>
Für die Definition der Finanzämter und damit auch der Steuerkategorien ist ebenfalls das Chefbuchhalterrecht erforderlich.<br>
Ebenso für die Änderung der Kopfdaten der Konten und damit deren Neuanlage.

#### Gibt es einen Unterschied zwischen der Belegübernahme im Beleg z.B. der Eingangsrechnung und der Finanzbuchhaltung:
Ja. Die Belegübernahme in die Buchhaltung löscht (storniert) alle Buchungen die sich auf den Beleg beziehen und bucht diese dann neu in die Fibu. Die Belegübernahme aus dem Modul Finanzbuchhaltung unter bearbeiten Belegübernahme zielt nur die Buchungen nach, die noch nicht in der Fibu sind.<br>
Um diese trotzdem neu in die Fibu zu übernehmen haken Sie bitte trotzdem übernehmen an.

#### Erfassen von Zahlungen
[Siehe dazu bitte]( {{<relref "/verkauf/rechnung/zahlung" >}} ).
Siehe dazu bitte auch [SEPA Kontoauszugsimport](Zahlungsvorschlag.htm#SEPA Kontoauszug Import), sowie Zahlungsvorschlag mit Buchung der Zahlungen auf das [Geldtransitkonto](Zahlungsvorschlag.htm#Geldtransitkonto).

#### Welche Punkte sollen laufend überprüft werden? Checkliste für die Buchhaltung
Damit Sie aus Ihrer Buchhaltung möglichst effizient aussagekräftige Daten erhalten sollten Sie auf folgende Punkte achten:
1. Der Saldo des Bankkontos in der Buchhaltung stimmt immer exakt mit dem jeweiligen auf dem Kontoauszug angedruckten Wert überein. Haken Sie jede Buchung am Beleg ab.<br>
Beachten Sie insbesondere die Kurs/Währungsproblematik.
2. Der Stand einer eventuellen "Hand-"Kasse entspricht immer exakt dem gezählten Geldbetrag in der Kasse.<br>
Sollten Sie keine physikalische Kasse haben, was bei kleinen Unternehmen durchaus praktikabel ist, so erfassen Sie die bar bezahlten Belege als Eingangsrechnung mit einer Zahlungsbuchung gegen Verrechnungskonto (der Eigentümer, ...)
3. Verrechnungskonten z.B. zwischen Unternehmen stimmen immer exakt überein, sind gegengleich.
4. Beachten Sie die Konten, die (fast) immer einen Null-Saldo haben müssen, wie z.B. Gehälter, Finanzamt Zahllastkonto, Sozialversicherung, Abgaben an Kommunen
5. Die UVA Verprobung muss für jedes Monat ab dem ersten Monat des Fibu-Einsatzes gültig durchgeführt worden sein. Drucken Sie die UVA auf Papier, oder speichern Sie diese zumindest über das Druckersymbol als PDF ab.<br>
Zusätzlich wird die ausgedruckte UVA auch in der Dokumentenablage abgelegt (bei tatsächlichem Papierausdruck bzw. dem Speichern als .pdf).
6. Bevor Sie die Umsatzsteuerverprobung auch an das Finanzamt melden, prüfen Sie diese gegen:
    1.  Die Saldenliste
    2.  Das Journal aller Ausgangsrechnungen des Zeitraums, inkl. Gutschriften
    3.  Das Kontierungsjournal aller Eingangsrechnungen des Zeitraums.
    4.  Werfen Sie einen Blick auf die offene Postenliste. Sind alle die Rechnungen ausgeglichen / offen die Sie auch so erwarten.

#### Offene Postenliste?
Die offene Postenliste finden Sie in der integrierten Finanzbuchhaltung im Menü, Info, offene Posten.
![](OffenePosten1.jpg)

Wählen Sie hier
![](OffenePosten2.jpg)<br>
ob nur das einzelne gewählte Konto oder die OP-Listen aller Debitoren bzw. Kreditoren ausgedruckt werden sollten.<br>
Entscheiden Sie dabei auch, ob die Sortierung nach Kontonummern oder nach Namen erfolgen sollte.

Info:<br>
Sie sollten diese Listen auch mit den offenen Postenlisten aus dem ERP-Bereich, also offene (Ausgangs-) Rechnungen und offene Eingangsrechnungen zum jeweiligen Stichtag vergleichen.
Diese finden Sie in den jeweiligen Modulen im Menüpunkt Journal.

#### Wie ist der Zusammenhang zwischen offenen Belegen und der Buchhaltung - wie stelle ich die Offene Posten-Liste "richtig"?
Vergleichen Sie die Liste der Offenen Belege in den einzelnen Modulen (Ausgangsrechnung und Eingangsrechnung) mit den Offenen Posten (Modul Finanzbuchhaltung, oberer Menüpunkt Info - Offene Posten) aus der Finanzbuchhaltung - diese werden im Normalfall übereinstimmen. Im Optimalfall sind alle Eingangs- und Ausgangsrechnungen, sowie Gutschriften erledigt bzw. bezahlt und keine Einträge auf den Offenen Posten- Listen bis auf die laufenden Belege. Dadurch stimmen die im Mahnlauf angeführten Belege und die Salden der einzelnen Konten der Kunden und Lieferanten sind korrekt.<br>
**Wie kann man diesen Zustand erreichen?**
- Überprüfen Sie die offenen Belege aus den jeweiligen Modulen: Können noch Zahlungen verbucht werden? Können Gutschriften gegengerechnet werden?
- Bleiben danach noch Rechnungen übrig, so klären Sie bitte mit Ihrem Steuerberater ob diese als [uneinbringliche Forderungen](../Rechnung/Zahlung.htm#Forderungsausfall) gebucht werden sollen / müssen.
- Das Ergebnis der Zahlungen / Zuordnungen überprüfen Sie mit dem erneuten Ausdruck der Offenen Posten
- Ergänzend können Sie das Konto des jeweiligen Kunden / Lieferanten überprüfen, nur mehr aktuell offene Belege sind auch hier mit dem Haken 'nur offene' sichtbar.
- Falls notwendig können Sie manuelle Buchungen (z.B. aus dem Start der Finanzbuchhaltung mit **Kieselstein ERP**) mittels [AZK](#Ausziffern) verknüpfen
- Vergleichen Sie danach nochmals die Offene Postenliste und die Liste der jeweiligen Belege (AR, ER, GS)
- Durch die Erledigung der Belege werden diese auch im Mahnwesen nun korrekt behandelt.

<a name="Bankkonten mit Fremdwährung"></a>

#### Bankkonten mit Fremdwährungen
Werden bei Zahlungen auf ein Bankkonto Fremdwährungen aus Rechnungen verwendet, so wird diese Information auch beim Ausdruck des Bankkontos (Info, Kontoblatt) für alle Fremdwährungsbuchungen mit angegeben.<br>
Es werden Währung, Umrechnungskurs, Soll- / Habenbetrag angegeben.<br>
Stimmt der Umrechnungskurs nicht mit dem Kurs zum Beleg-/Zahldatum der Währungstabelle überein wird der Kurs in Rot angedruckt.<br>
Um den Vergleich des Fremdwährungsbankkontos mit dem Kontoauszug zu erleichtern, haben wir die Möglichkeit der Währungsdefinition für den Druck geschaffen.<br>
Definieren Sie in den Kopfdaten des Kontos ![](Fibu_Waehrung_Druck.gif) die Währung für den Druck dieses Kontoblattes.<br>
Steht diese auf Leer, so wird die Mandantenwährung verwendet.<br>
Ist hier eine Währung ausgewählt, so werden beim Ausdruck des Kontoblattes die Beträge in diese Währung umgerechnet.<br>
Dies erfolgt nach der Regel:
- sind Belege in der gewünschten (Fremd-) Währung vorhanden, so wird der hinter diesen Belegen hinterlegte Wechselkurs für die Umrechnung verwendet.
- es wird der hinterlegte Umrechnungskurs aus der Währungstabelle für die Umrechnung verwendet.
- Ist für eine Währung keine Umrechnung hinterlegt, wird mit einer schweren Fehlermeldung abgebrochen. Definieren Sie bitte Ihre Kurse entsprechend.

**<u>Wichtig:</u>** Dies ist ein reines Hilfsmittel um die Währungsthematik etwas zu erleichtern. Die in **Kieselstein ERP** integrierte Buchhaltung wird **<u>IMMER</u>** in Mandantenwährung geführt. Daher dürfen nur die Beträge in Ihrer Mandantenwährung für weitere Betrachtungen herangezogen werden.<br>
**Info:** Um die Thematik der reziproken Kursumrechnung zu entschärfen haben wir uns dazu durchgerungen den Kurs mit einer Genauigkeit von 3 Vorkomma- und 13 Nachkommastellen abzuspeichern und mit dieser Genauigkeit auch umzurechnen.<br>
Hier ist vor allem der Hintergrund, dass z.B. in der Schweiz oft der reziproke Kurs von der Bank bekannt gegeben wird und mit nur 6 Nachkommastellen der reziproke Wert bei großen Zahlen nicht ausreichend genau dargestellt werden kann.<br>
So sollten Beträge bis zu 99Millionen in beiden Richtungen richtig umgerechnet bzw. dargestellt werden.

#### Wie kann der Abschluss eines Fremdwährungskontos richtig dargestellt werden?
Beim Jahresabschluss von Fremdwährungskonten ergibt sich immer wieder die Aufgabenstellung, dass der Kontoabschluss ja sowohl in Fremdwährung, als auch in Mandantenwährung "richtig" dargestellt werden sollte.<br>
Hier kommt dazu, dass zum Jahresabschluss der Kurs zum Bankschluss (oder zu einem ähnlichen Datum) eingebucht werden muss, sodass die Entsprechung der Fremdwährung durch die Mandantenwährung zum Kurs zum Jahresschluss übereinstimmen. Mit diesem Betrag (der in Mandantenwährung sein muss) gehen Sie ja dann in die Bilanz.

Aufgrund der Tatsache, dass **Kieselstein ERP** NUR in Mandantenwährung bucht, Sie also immer die in Ihrer Mandantenwährung gebuchten und für Sie maßgeblichen Beträge haben, muss der Abschluss des Fremdwährungs-Bankkontos als letzte Buchung gemacht werden.<br>
Buchen Sie hier mit dem Datum des Monatsletzten (31.12.jjjj) und mit Auszugsnummer 99999999 die Kursdifferenz aus.

Gehen Sie dazu wie folgt vor:<br>
Definieren Sie den Wechselkurs zum Ende des Jahres. Dieser sollte am 31.12\. des endenden Geschäftsjahres und zum 1.1\. des beginnenden Geschäftsjahres gleich sein. Stellen Sie dies im Modul Finanzbuchhaltung, unterer Modulreiter Währung entsprechend ein.<br>
![](Fremdwaehrungs_abschluss1.gif)

Errechnung der Kursdifferenz (hier am Beispiel der Mandantenwährung in CHF):
Drucken Sie das Kontoblatt in Fremdwährung (in unserem Beispiel EUR) aus.<br>
![](Fremdwaehrungs_abschluss2.gif) Dies sind EUR

Drucken Sie das gleiche Kontoblatt in Mandantenwährung aus.<br>
![](Fremdwaehrungs_abschluss3.gif) Dies sind CHF

Anhand des oben definierten Wechselkurses ergibt sich nun, dass
7.808,11 EUR entsprechen zum Kurs des 31.12.2011   9.703,13 CHF
Daraus ergibt sich ein Kursverlust von<br>
> 14.356,97 CHF<br>
> -9.703,13 CHF<br>
> --------------<br>
> 4.653,84 CHF<br>

Buchen Sie diesen Kursverlust mittels manueller Umbuchung.
![](Fremdwaehrungs_abschluss4.gif)

Nun sehen die Ausdrucke des Bankkontos wie folgt aus:<br>
![](Fremdwaehrungs_abschluss5.gif) Dies sind EUR

![](Fremdwaehrungs_abschluss6.gif) Dies sind CHF

Das bedeutet, dass beim Kontroll-Ausdruck für die Bank die letzte Buchung, welche den reinen Kursverlust darstellt, nicht berücksichtigt werden darf.<br>
Die Darstellung für die Bilanz ist richtig. Aus diesem Grund ist darauf zu achten, dass bei der Kontoauszugsnummer die höchst mögliche Nummer (999.999.999) eingegeben wird, damit dies die tatsächlich letzte Buchung ist. **Wichtig:** Die Periodenübernahme in das Folgejahr wird, da ja der Kurs über den Jahreswechsel gleich geblieben ist (nicht verändert werden darf) in beiden Währungen, also auch in der Fremdwährung, richtig dargestellt.<br>
![](Fremdwaehrungs_abschluss7.gif) Dies sind EUR

![](Fremdwaehrungs_abschluss8.gif) Dies sind CHF.

Das bedeutet: Mit dem Wissen, dass die abschließende Kursgewinn- bzw. Kursverlustbuchung für die Überprüfung der Buchungen mit dem Bankkonto nicht verwendet werden können, kann auch die Jahresabschlussbuchung entsprechend richtig dargestellt werden.

Wenn zwischen der letzten Buchung im alten Geschäftsjahr und der ersten Buchung im neuen Geschäftsjahr eine Kursänderung gebucht wird, so ergibt sich implizit, dass damit verbunden auch wiederum eine Kurskorrektur mit dem 1.1\. und Kontoauszugsnummer 0 zu buchen ist, damit die Eröffnung in der Fremdwährung (in unserem Falle EUR) richtig ist.

#### Wann sollten Kursänderungen der Währung gebucht werden?
Kursänderungen müssen IMMER vor der Buchung gebucht werden. Nur damit ist sichergestellt, dass Sie auch die richtigen Kurse in **Kieselstein ERP** eingetragen haben. Beachten Sie bitte, dass nachträgliche Kursänderungen:
1. die davon abhängigen Belege NICHT aufgerollt werden, d.h. die bei der Buchung hinterlegten Kurs bleiben erhalten.
2. Bei reinen Darstellungen in anderen Währungen, wie z.B. das Bankkonto, die Anzeige in der Fremdwährung durch die Kursänderung geändert wird, da ja die eigentlichen Buchungen ausschließlich in Mandantenwährung durchgeführt werden.

#### Kann ich das Bankkonto in Fremdwährung sehen?
#### Wie sollte bei Buchungen auf ein Fremdwährungsbankkonto vorgegangen werden?
Gerade in Export orientierten Unternehmen kommen immer wieder Konten zum Einsatz, die, um das Wechselkursrisiko auszuschalten, in anderen Währungen als die Mandantenwährung geführt werden. Hier treffen zwei Forderungen aufeinander: Einerseits die Forderung nach der buchhalterisch richtigen Buchung in Mandantenwährung und anderseits die Forderung nach dem Überblick über die Salden in Fremdwährung. Dies kann in der **Kieselstein ERP** Finanzbuchhaltung dargestellt werden, wenn folgende Punkte beachtet werden:
- Stellen Sie sicher, dass die Kurse zum jeweiligen Buchungsdatum bereits unter Währungen richtig eingetragen sind.
- Müssen **Wechselkurse geändert** werden, so **müssen** die davon abhängigen Buchungen eventuell **neu eingebucht** werden. Dies trifft vor allem Buchungen, bei denen kein indirekter Wechselkurs zur Verfügung steht. Dies sind manuelle Umbuchungen zwischen den Konten wie z.B. vom EUR Bankkonto auf das USD Bankkonto oder Eröffnungsbuchungen.

Um Kursdifferenzen eines Kontos darzustellen, wählen Sie den Ausdruck (des Fremdwährungskontos) in Mandantenwährung. Hier sehen Sie zum Schluss sowohl den Fremdwährungssaldo als auch den Fremdwährungssaldo umgerechnet in Ihre Mandantenwährung und damit die Kursdifferenz. Diese ist gegebenenfalls entsprechend manuell zu buchen.

#### Abgrenzung von Wareneinsatz
<a name="Abgrenzung Wareneinsatz"></a>
Gerade bei Unternehmen die ihren Warenvorrat langfristig einkaufen, aber trotzdem ein exaktes Controlling auch auf Basis des Wareneinsatzes erfüllen wollen / müssen, ist eine klare Abgrenzung des tatsächlichen Wareneinsatzes erforderlich.

Voraussetzungen:
- a.) jeder Warenzugang wird auf Wareneinsatz, z.B. 5100 gebucht
- b.) zugleich wird jeder Warenzugang über die Wareneingangspositionen der Bestellungen mit seinem richtigen Wert dem Artikel-Lager zugebucht.

Nun werden während z.B. eines Monates Waren auf das Lager gebucht und wieder Waren dem Lager entnommen.

Der effektive Wareneinsatz ergibt sich daher wie folgt:

|  | Beispiel: | Beispielwerte |
| --- |  --- |  --: |
| Wareneinsatz im Zeitraum aus den Konten | Summe aus den Konten 5xxx | + 10.000,- |
| plus Lagerstand zum Beginn des Zeitraumes | Lagerstand im Artikel zum Stichtag 1.1.2023 | +   5.000,- |
| minus Lagerstand am Ende des Zeitraumes | Lagerstand im Artikel zum 1.2.2023 | -   4.500.- |
| tatsächlicher Wareneinsatz im Zeitraum |  |    10.500,- |

Beachten Sie hier auch die damit verbundenen Themen wie z.B. Ladenhüter

**Tipp:**

Oft ist eine Aufteilung in z.B. Rohstoffe, Betriebsmittel, etc. wünschenswert / erforderlich.

Um dies einfach und effizient zu erhalten, gliedern Sie ihre Artikel in Klassen oder Gruppen passend zu der erforderlichen Auswertung, im Idealfall mit gleicher Namensgebung wie in den Konten der Buchhaltung.

#### Abgrenzungen von sonstigen Aufwänden
<a name="Abgrenzung sonstiger Aufwand"></a>
Es wird immer auch die Frage an uns herangetragen, wie sollte man z.B. Dienstleistungsrechnungen unterjährig abgrenzen, wobei zu berücksichtigen ist, dass man sich natürlich die Vorsteuer faktisch sofort zurückholen will.<br>
Ausgehend vom österreichischen Einheitskontenrahmen erfassen Sie die Eingangsrechnung gegen die Bilanzgruppe 0, z.B. 0620 Büromaschinen & EDV. Damit finden Sie einerseits die Vorsteuer am Vorsteuerkonto und somit passend zum Eingangsrechnungsdatum in Ihrer UVA und haben andererseits die abzugrenzende Dienstleistung am Konto 0620 im Soll und somit auch in der Bilanz.<br>
Nun buchen Sie für die monatliche Abgrenzung z.B. 7010 AfA im Soll gegen 0620 im Haben und reduzieren so die Anlagengüter gegen die AfA.

**Info zur Buchung von IG-Erwerben als entsprechend abzugrenzende Aufwände**
Da für die richtige UVA der IG-Erwerbe auch der IG-Erwerbs-Umsatz wichtig ist, müssen die Abgrenzungsbuchungen über zwei Konten abgebildet werden.<br>
D.h. Sie buchen einerseits z.B. den IG-Dienstleistungsaufwand gegen 0621 und somit auch die (IG-)Einfuhrumsatzsteuer gegen die IG-Vorsteuer und definieren die UVA Art des Kontos 0621 als IG-Erwerb (Normalsteuer). Für die monatliche Abgrenzung / Weiterbuchung der AFA definieren Sie z.B. 0622 als "Entlastungskonto IG-Erwerb" und buchen dann analog wie oben beschrieben 7010 AfA im Soll gegen 0622\. Bitte beachten Sie, dass die Konten 0621 und 0622 in der gleichen Bilanzgruppe sein sollten.

#### Abgrenzung von umsatzsteuerbehafteten Erlösen
Für die Abgrenzung von umsatzsteuerbehafteten Erlösen muss im wesentlichen analog zur Buchung der IG-Erwerbe vorgegangen werden, jedoch sind natürlich die Soll-Haben Buchungen vertauscht.

#### Ausziffern
Als Ausziffern bezeichnet man jenen Vorgang, mit dem nicht zusammengehörende Zahlungen eines Debitors oder Kreditors so gekennzeichnet werden, dass von **Kieselstein ERP** erkannt werden kann, dass diese trotzdem zusammen gehören. Das bewirkt, dass in der offenen Postenliste diese Belege als zusammengehörend erkannt werden. Üblicherweise wird dies für Verrechnungen und Gegenverrechnungen ohne Zahlungsfluss und ohne Bankzahlung angewandt.

Es stehen dazu drei Funktionen in den Kontentypen Debitoren bzw. Kreditoren zur Verfügung.

![](Ausziffern_neu.gif) Eine neue Auszifferung anlegen für alle markierten Zeilen

![](Ausziffern_loeschen.gif) Das Auszifferungskennzeichen einer Buchungszeile löschen.

Wenn mehrere Buchungen markiert sind, so kann für alle markierten Zeilen ein neues Auszifferungskennzeichen gesetzt werden.

Die Auszifferungen der Buchungszeilen werden in der Spalte AZK (AusZifferungsKennzeichen) angezeigt.

Zusätzlich wird die Summe der Buchungen auf die sich eine Auszifferung bezieht unten im Detail unter ![](Auszifferungssaldo.gif) AZK Saldo angezeigt ([beachten Sie auch](#Summe)).  Diese Anzeige bedeutet, dass für das Auszifferungskennzeichen 1, das ist die Zeile auf der der Cursor gerade steht ein Gesamtsaldo von 0,00 besteht. D.h. die gesamte Buchung ist ausgeglichen und erscheint somit nicht mehr auf der offenen Postenliste.

Bitte beachten Sie, dass im Unternehmensalltag die Buchungen über Ausgangs- und Eingangsrechnungen erfasst werden sollen, so erhalten Sie Transparenz über Kosten und Erträge und mehr Informationen in den Auswertungen. Manuelle Buchungen werden in den Statistiken nicht in gleicher Form berücksichtigt! Besonders im Sinne der nachhaltigen Unternehmensführung empfehlen wir dringend die Erfassung als Eingangs- bzw. Ausgangsrechnung.

Fälle, wo Auszifferungen nötig sind, wie oben angeführt, bei direkter Verrechnung oder Vorauszahlung oder beim Start der Finanzbuchhaltung mit **Kieselstein ERP** gibt es. Das Setzen von Auszifferungskennzeichen sollte jedoch die Ausnahme sein. Gerade für Zahlungen oder die Buchung von Skonto ist kein Auszifferungskennzeichen notwendig.

#### Automatisches Auszifferungskennzeichen
Sobald eine Buchung im Zuge der Zahlung eine abweichende Belegnummer hat oder ein AZK, werden alle zur (Eingangs-)Rechnung gehörenden Buchungen mitausgeziffert. So wird die Gegenverrechnung von Gutschriften direkt am Konto ersichtlich.

#### Kann über zwei oder mehrere Geschäftsjahre hinweg ausgeziffert werden?
Nein selbstverständlich nicht. Dies ist aus Gründen des Jahresabschlusses / Bilanz absolut undenkbar.

Wie geht man also vor?

Mit dem Abschluss des ersten Geschäftsjahres ergibt sich die offene Postenliste für ein bestimmtes Personenkonto.

Dieses wird durch die [Periodenübernahme](#Periodenübernahme) in das darauffolgende Geschäftsjahr übernommen. Die Periodenübernahme für die Personenkonten ist in **Kieselstein ERP** so programmiert, dass die einzelnen Buchungspositionen eines offenen Beleges in das neue Geschäftsjahr übernommen werden. Dadurch sehen Sie auch im neuen Geschäftsjahr die aus dem/den Vorjahr(en) resultierenden offenen Posten und können so im Jahr der Erledigung die Auszifferung buchen. Wenn dies Beleg aus der Rechnung / Eingangsrechnungsverwaltung von **Kieselstein ERP** sind, geschieht diese Auszifferung automatisch.

#### Wie erhalte ich Überblick über die vorhandenen Auszifferungskennzeichen?
Wechseln Sie auf das Konto, dass Sie näher betrachten wollen und klicken hier auf Info - Kontoblätter. Im Druck des Kontoblatts können Sie nach AZK sortieren![](nachAZKsortieren.JPG).

#### Wie wird Skontoaufwand/-erlös aus umsatzsteuerlicher Sicht behandelt?
Wird ein Skonto-Aufwand bzw. -Erlös verbucht, so wird der für das Skonto relevante Umsatzsteueranteil anhand der Original Ust-Verteilung anhand der Rechnung (ER bzw. RE) neu errechnet. D.h. es werden anhand der Konten und Mehrwertsteuerverteilungen der Originalrechnungen die Ust-Werte des Skontos errechnet und so in die Ust-Konten eingebucht. Bei eventuell auftretenden Rundungsdifferenzen wird der größte Betrag korrigiert.

#### Wie wird der Skontoaufwand/-erlös und die Kursdifferenzen bei Fremdwährungsrechnungen behandelt?
Bei Fremdwährungsrechnungen mit Kursschwankungen und Skontobeträgen wurde folgende Betrachtung gewählt um auch bei Teilzahlungen eine möglichst transparente und vor allem richtige Darstellung in den Konten zu erzielen.

Wichtig ist: Es werden alle Beträge jeweils nur in Mandantenwährung in der Finanzbuchhaltung verbucht.
1. Rechnung wird zum Wechselkurs des Rechnungsdatums verbucht.
2. Die erste Teilzahlung wird zum Wechselkurs des Zahlungsdatums verbucht.
3. Die Schlusszahlung wird zum Wechselkurs der Schlusszahlung verbucht, dabei bleibt ein Teilbetrag offen der als Skonto zu verbuchen ist.
4. Es werden die Zahlungs-Kursdifferenzen, aus dem Gedanken der Währungsverluste errechnet, indem die einzelnen Zahlungen zum Wechselkurs des Rechnungsdatums in die Mandantenwährung umgerechnet werden. Die sich ergebende Differenz zu den umgerechneten tatsächlichen Zahlbeträgen wird als Kursverlust bzw. -gewinn verbucht.
5.  Die sich nun ergebende Differenz zwischen<br>
    \+ Rechnungsbetrag in Mandantenwährung<br>
    \- Zahlungen in Mandantenwährung (Kurs zum jeweiligen Zahlungsdatum)<br>
    \- Wechselkursdifferenzen<br>
Ergibt den tatsächlichen Skontoaufwand. Dieser wird entsprechend auf die Konten verbucht.

<a name="Definition UVA Kontenzuordnung"></a>

#### Definition der Kontenzuordnungen für die UVA / Umsatzsteuervoranmeldung?
**Kieselstein ERP** ist so programmiert, dass die Ergebnisse der Umsatzsteuervoranmeldung parametriert werden können. Dies geht soweit, dass auch derzeit nicht bekannte Umsatzsteuerarten durch deinen **Kieselstein ERP** Betreuer zusätzlich parametriert werden können.

Da allen Umsatzsteuerangaben in DACH innewohnt, dass die Vorsteuerangaben nur als Steuerbetrag anzugeben sind, die Umsatzerlöse aber auch der Höhe nach gemacht werden müssen, ist dies bei der Zuordnung der UVA Arten entsprechend zu berücksichtigen. Das bedeutet, dass für Erlöse die Umsätze, die Konten der Erlöse, zuzuordnen sind, für die Vorsteuer aber nur die Vorsteuerkonten. Ein Sonderfall stellt der Innergemeinschaftliche Erwerb dar, bei dem neben dem Umsatz auch beide Steuern (Einfuhrumsatzsteuer und Vorsteuer aus IG-Erwerb) anzugeben sind.

Bitte beachten Sie dies bei der Definition Ihrer Konten und der Zuordnung der UVA-Arten.

#### Wie die Eröffnungsbuchungen bei bestehender Finanzbuchhaltung übernehmen?
<a name="Übernahme Eröffnungsbuchungen"></a>
Umsteiger von anderen Buchhaltungssystemen haben die Aufgabenstellung, dass vor allem für die Debitoren und Kreditoren die Eröffnungsbuchungen manuell zu übernehmen sind.<br>
Es erscheint uns die einfachste Vorgehensweise, wenn im alten Buchhaltungssystem noch die (hoffentlich) automatische Periodenübernahme vom alten auf das nun in **Kieselstein ERP** zu buchende Geschäftsjahr machen.<br>
Drucken Sie nun die Kontoblätter aller übernommenen Konten aus. So haben Sie eine klare Vorlage, welche Eröffnungsbuchungen für den Start von **Kieselstein ERP** einzubuchen sind.<br>
{{% alert title="Wichtig" color="warning" %}}
Buche jede offene Rechnung einzeln. Lasse dich von **niemanden** dazu überreden (Steuerberater machen das gerne) nur auf die mitlaufenden Forderungen / Verbindlichkeiten Konten zu buchen. Nur wenn die Salden jedes einzelnen Debitor / Kreditors klar ersichtlich sind, hast du eine Chance bei (Tipp-)Fehlern diesen auch zu finden.
{{% /alert %}}

#### Periodenreinheit
Du solltest grundsätzlich auf Periodenreinheit in deinen Buchungen achten. Dies ist einerseits eine dringende Erforderniss um deine monatlichen Vergleiche entsprechend aussagekräftig zu haben und andererseits eine Forderung deines Finanzamtes.
- Das bedeutet, dass du darauf achtest, dass deine Ausgangsrechnungen für das Monat gelegt werden, in dem du deine Leistung tatsächlich erbracht / abgeschlossen hast. Achte darauf, bevor du neue Ausgangsrechnungen für das (neue) laufende Monat anlegst. Achte grundsätzlich darauf, dass du deine Leistungen möglichst rasch fakturierst. Nur so bekommst du rechtzeitig mit, wie sich denn die Zahlungsmoral deiner Kunden entwickelt.
- Das bedeutet auch, dass du deine Eingangsrechnungen von deinen Lieferanten und Dienstleistern in der Form bekommst, dass das Eingangsrechnungsdatum in dem Monat ist, in dem die Leistung erbracht wurde. Sollte dein Lieferant nicht in der Lage, oder nicht Willens sein, das zu erfüllen, suche dir einen anderen Lieferanten.
- Ein möglicher Trick um die Periodenreinheit wieder herzustellen:<br>
Sollte sich einige sehr wenige Male im Jahr die Anforderung ergeben, dass eine Rechnung nicht in der benötigten Periode erstellt / erfasst werden kann, es wird dies eventuell bei Eingangsrechnungen der Fall sein, so kannst du mit einer Buchung z.B. über ein Durchläuferkonto dies entsprechend korrigieren. D.h. du erfasst z.B. die Eingangs-Rechnung  mit dem Datum der Lieferantenrechnung und buchst dann den Aufwand, **ACHTUNG: inkl. Vorsteuer**, gegen das Durchläuferkonto. Nun buchst du die Gegenbuchung mit dem gewünschten Datum (der Periode) mit dem gleichen Betrag und dem gleichen Aufwandskonto ebenfalls gegen das Durchläuferkonto. Im Durchläuferkonto zifferst du die beiden Buchungen gegeneinander aus, damit ist das auch für später nachvollziehbar. Und auch im Aufwandskonto zifferst du die Buchung der Eingangsrechnung gegen die Buchung gegen das Durchläuferkonto entsprechend aus.<br>
<u>Anmerkung:</u> Dass das nur für wenige Sonderfälle praktikabel ist, ergibt sich von selbst. Ich persönlich habe das in den letzten 20Jahren nicht gebraucht.
- Wie mit Eingangsrechnungen, wie z.B. Hotelrechnungen, umgehen, die man von MitarbeiterInnen erst Monate danch bekommt:<br>
Wenn du diese überhaupt noch akzeptierst, so hat sich bisher bewährt, eine (handschriftliche) Notiz mit dem "erhalten am: ....." anzubringen und die Eingangsrechnung mit diesem Datum einzubuchen.

Merke: Solange du dem Finanzamt Geld leihst, war das noch nie ein Problem. Aber zu früh  oder gar ungerechtfertig sich Geld vom Finanzamt zu holen kann echt teuer zu werden. D.h. wenn du den Vorsteuerabzug erst später in Anspruch nimmst, leihst du dem Finanzamt Geld -> daher kein Problem.

### Erfolgsrechnung, Betriebswirtschaftliche Auswertung, Gewinn- und Verlustrechnung
<a name="KER"></a>

Kurzfristige Erfolgsrechnung, Betriebswirtschaftliche Auswertung, Gewinn und Verlustrechnung

Mit den Ergebnisgruppen haben Sie ein Instrument, mit dem komfortabel eine Kurzfristige Erfolgsrechnung (KER), eine Betriebswirtschaftliche Auswertung (BWA), ein Gewinn und Verlust Rechnung (G+V) passend zu Ihren Bedürfnissen erstellt werden kann.<br>
Die grundsätzliche Idee ist, dass, so wie Ihre KER aufgebaut sein sollte, diese in den Ergebnisgruppen definiert werden. Sie finden diese Definition unter Finanzbuchhaltung, unterer Modulreiter Ergebnisgruppen.<br>
Der Aufbau dieser Auswertungen ist immer so, dass einzelne Ergebnisgruppen in übergeordnete Ergebnisse summieren. So werden z.B. die Erlöse Handel und die Erlöse Dienstleistung wiederum in der Hauptgruppe Erlöse zusammengefasst. Diese rechnen weiter in den Rohertrag usw.. Die gewünschte Tiefe der Auswertungen ist unbegrenzt, Sie sollten in der Regel jedoch mit 3-4 Ebenen das Auslangen finden.<br>
Die Idee ist weiters, dass die Salden aller Konten die einer Ergebnisgruppe zugeordnet sind. in diese summiert werden.<br>
Definieren Sie daher zuerst die gewünschten Ergebnisgruppen und ordnen Sie danach Ihre Sachkonten den entsprechenden Ergebnisgruppen zu. Damit Sie einen Überblick erhalten, dass auch alle relevanten Sachkonten in der KER enthalten sind, wird die Ergebnisgruppe jedes Sachkontos in der Saldenliste mit angedruckt.<br>
![](Ergebnisgruppe_definieren.gif)
Für die Definition einer Ergebnisgruppe stehen folgende Felder zur Verfügung:

| Feld | Bedeutung |
| --- |  --- |
| Typ | in aller Regel Ergebnisgruppe und für die Gestaltung Linie und Leerzeile |
| Bezeichnung | Bezeichnung der Ergebnisgruppe (siehe auch Steuerung des Drucks)|
| Summengruppe | in welche übergeordnete Ergebnisgruppe summiert diese Ergebnisgruppe |
| Invertiert | Der aus den Konten ermittelte Saldo wird invertiert und danach angezeigt. |
| Summe negativ | sollte der aus den Konten ermittelte Saldo in die weitere Berechnung mit negativem Vorzeichen eingerechnet werden.Wichtig: Die Summe negativ definiert wie der angezeigte Wert in der übergeordneten Gruppe behandelt wird. |
| Basis für 100% | Definieren Sie die Ergebnisgruppe, welche für die Anteilsberechnungen die 100% Basis darstellt.Diese kann nur für eine Ergebnisgruppe definiert werden. |
| Jahresgewinn | Bei jedem Ausdruck der Erfolgsrechnung wird das monatliche Ergebnis auf das Jahresgewinnkonto verbucht.Definition des Jahresgewinnkontos siehe Finanzamt, Buchungsparameter, Jahresgewinn. |

Die Rechnung ist wie folgt:<br>
Salden -> invertiert -> Gruppenanzeige -> Summe invertiert -> (und wieder von vorne)<br>
Saldensumme aller Konten der Ergebnisgruppe ergibt einen Wert.<br>
Wenn invertiert wird der Wert bevor er weiterbehandelt wird invertiert. Dies gelangt in die Anzeige (Report).<br>
Dieser Wert wird wenn Summe negativ erneut invertiert und gelangt so in die Summengruppe.<br>

Anbei ein sehr einfaches Beispiel:<br>
![](Ergebnisgruppe_definieren1.gif)<br>
Selbstverständlich gehören hier noch die gesamten Aufwände für den Betrieb Ihres Unternehmens mit dazu.<br>
Einige unserer Anwender nutzen die Bilanzgliederung, siehe unten, welche nach den gleichen Regeln aufgebaut ist, um daraus eine monatliche Bilanzdarstellung zu erhalten.

Im Reiter Konten des Unter-Moduls Ergebnisgruppen werden die Konten angezeigt, die dieser Ergebnisgruppe zugeordnet sind.<br>
Hier können auch die entsprechenden Konten zur jeweiligen Ergebnisgruppe zugeordnet werden. Für eine rasche Zuordnung wird auch die mehrfache Selektion unterstützt. Sollten dabei Konten ausgewählt werden, die bereits einer Ergebnisgruppe zugeordnet sind, wird diese Zuordnung überschrieben.<br>
Die Zuordnung welches Konto in welche Ergebnisgruppe aufgenommen wird, wird direkt in der Definition der Kopfdaten des jeweiligen Kontos vorgenommen. ![](Ergebnisgruppe_definieren2.gif)
oder wie oben beschrieben auch aus der Ergebnisgruppendefinition heraus, im Reiter Konten

Den Druck der Ergebnisrechnung finden Sie im Reiter Sachkonten - Journal - Ergebnisrechnung.<br>
Wenn Sie den Haken bei Detail setzen, enthält der Druck auch alle Konten die damit verbunden sind. Im Druck der Bilanz mit Detail werden die Konten nach Kontonummer aufsteigend sortiert angeführt. Beim Detaildruck erhalten Sie bei der Ergebnisrechnung auch eine Information mit Konten die zwar Buchungen im Zeitraum haben, aber weder einer Bilanzgruppe noch einer Ergebnisgruppe zugeordnet sind.

Diese Ergebnisrechnung wird von manchen Anwender auch zur Verbuchung des Jahresgewinns verwendet. [Siehe dazu bitte](#Rappen Rundung).<br>
Der Jahresgewinn kann für die als Jahresgewinn gekennzeichnete Ergebnisgruppe automatisch verbucht werden. D.h. es wird mit dem Ausdruck der Erfolgsrechnung der Erfolg des jeweiligen Monates in das Jahresgewinnkonto verbucht. Somit ist dieser Betrag in Ihrer Bilanz enthalten. Natürlich unter der Voraussetzung, dass das Jahresgewinnkonto in den Bilanzgruppen enthalten ist.<br>
Die Weiterbuchung wird durch ![](Weiterbuchung_Gewinn.gif) in der Zeile des Ergebnisses angezeigt.

### Ein kleiner Tipp für nicht Buchhalter.
Um Sicherheit in der G+V Berechnung zu erhalten, empfiehlt sich für eine erste Darstellung und zur Überprüfung der richtigen Struktur der G+V Gruppen, im ersten Lauf ausschließlich den 100% Haken z.B. bei den Erlösen zu setzen. Mit dieser Definition wird bereits das Ergebnis errechnet. Es ist dabei jedoch die buchhalterische Darstellung zu berücksichtigen. D.h. da die Erlöse immer im Haben und der Aufwand im Soll gebucht werden, der Saldo als Soll - Haben dargestellt wird, wird in einer nicht angepassten Darstellung der Gewinn immer negativ dargestellt.<br>
So haben wir bei uns bei den Erlösen das invertiert angehakt, bei der Summengruppe der Erlöse sind alle drei angehakt (invertiert, Summe negativ und 100%) usw.<br>
Unsere Empfehlung ist, zuerst, ohne irgendwo ein invertiert / negativ gesetzt zu haben die Erfolgsrechnung ausdrucken. Dann anhand dieser die Vorzeichen so einzustellen, dass wiederum das logisch gleiche Ergebnis heraus kommt. Es ist dies leider mathematisch / textlich etwas schwer erklärbar. Orientieren Sie sich gedanklich bitte an obigem Beispiel.

### Vollständigkeit
Um sicherzugehen, dass deine Erfolgsrechnung auch alle Konten umfasst, wird in der Detailanzeige ![](Erfolgsrechnung_Detailanzeige.png) am Ende eine Liste von Konten angezeigt, die im gewählten Geschäftsjahr Bewegungen haben, aber weder 
![](Erfolgsrechnung_fehlende_Zuordnung.png)  einer Ergebnisgruppe noch einer Bilanzgruppe zurgeordnet sind.<br>
Bitte stelle auf jeden Fall sicher, dass alle Konten entsprechend zugewiesen sind.<br>
Einzige Ausnahme, das Bilanzgewinnkonto (wo immer du das darstellst).

### Bilanz, Aktiva Passiva
[siehe]({{<relref "/management/finanzbuchhaltung/integriert/bilanz">}} )

### Sieht jeder die Erfolgsrechnung, meinen Gewinn ?
<a name="Sichtbarkeit Erfolgsrechnung"></a>
Das hängt an den Rechten. D.h. jeder **Kieselstein ERP** Benutzer der zumindest ein lesendes Recht für das Modul Systemsteuerung hat (LP_SYSTEM_R) und ein lesendes Recht für das Modul Finanzbuchhaltungsmodul hat (FB_FINANZ_R) hat damit die Möglichkeit in der Systemsteuerung die Dokumentenablage aufzurufen und so auf den Knoten Finanzbuchhaltung zu navigieren und sich diese erzeugten Daten anzusehen. Umgekehrt: Wenn ein Anwender kein Leserecht auf die Systemsteuerung hat, kann er die Dokumentenablage nicht aufrufen. Hat er kein Leserecht auf das Finanzbuchhaltungsmodul, würde er zwar sehen, dass z.B. die Erfolgsrechnung gedruckt wurde, aber keinerlei Inhalte. D.h. beim Klick auf den Knoten werden, abhängig von den jeweiligen Modul-Rechten die Inhalte angezeigt oder eben nicht.<br>
![](Erfolgsrechnung_Dokumentenablage.gif)<br>
Bitte beachten Sie, dass alle automatisch erzeugten und abgelegten Dokumente die Sicherheitsstufe 99 bekommen. D.h. auch die Erfolgsrechnung, UVA, Saldenliste, Sepa Export sind mit dieser Sicherheitsstufe abgelegt.

#### Kann jeder Anwender der das Fibu Modul öffnen darf die Erfolgsrechnung aufrufen?
Für den Aufruf und somit die Erzeugung der Daten für die Erfolgsrechnung ist das Recht LP_FINANCIAL_INFO_TYP_1 erforderlich.

#### Bilanz Gliederung, Aktiva Passiva
<a name="Bilanzgruppe"></a>
Die Angabe der Gliederungskonten für eine Bilanz ist (aus technischer Sicht) sehr ähnlich der Gliederung für die Gewinn- und Verlust-Rechnung, bezieht sich aber auf die Bilanzkonten.
D.h. um Ihre Bilanz in einer vorab Form direkt aus **Kieselstein ERP** erzeugen zu können, verwenden Sie bitte für die Definition den unteren Modulreiter Bilanzgruppen. Die Auswertung danach finden Sie in den Sachkonten unter Journal, Bilanz. Die Gestaltung der Auswertung erfolgt wie oben unter [Kurzfristige Erfolgsrechnung](#KER) beschrieben.
Bei der Bilanzgruppen können Sie bei Konten wie z.B. Kassa / Bank eine negative Bilanzgruppe angeben. Die Funktion der doppelten Bilanzgruppen ermöglicht die Darstellung der Konten, die je nach Ergebnis in der Aktiva bzw. Passiva der Bilanz aufscheinen. Somit wird der Saldo z.B. bei Kasse/Bank je nach Kontostand als Eigenkapital oder Fremdkapital betrachtet mit der entsprechenden Wirkung in der Bilanzsumme. Andere Sachkonten können nur einer Bilanzgruppe zugeordnet werden.

#### Kippkonto, Bankkonten einmal in der Aktiva einmal in der Passiva
Insbesondere Kontokorrentkonten hat man deswegen um kurzfristig Finanzierungen und ähnliches zu machen. Damit haben diese Konten die Eigenschaft, dass Sie mal im Plus und mal im Minus sind. Aus Gründen der Bilanzbetrachtung ist ein positives Bankkonto Eigenkapital und gehört daher ins Aktiva und ein negative Bankkonto ist Passiva, da Fremdkapital.
Um dies entsprechend abzubilden gibt es in der Definition der Bilanzgruppen die positiven und negativen Bilanzgruppen. D.h. um aus Sicht der Bilanz Ihr Kontokorrentkonto entsprechend darzustellen sind folgende Definitionen erforderlich:

a.) positive Bilanzgruppe<br>
![](Bilanzgruppe_positiv.gif)

b.) negative Bilanzgruppe<br>
![](Bilanzgruppe_negativ.gif)

c.) Zuordnung des Kontos<br>
![](Bilanzgruppe_Kontozuordnung.gif)

#### Eröffnungsbilanz
Insbesondere rund um den Jahreswechsel ergeben sich immer wieder die Fragen, wie man denn die Bilanzkonten vom alten Jahr in das neue Jahr übernimmt.
In Verbindung mit Korrekturbuchungen noch im alten Jahr, siehe dazu auch Geschäftsjahr sperren, kommt es gerne zu Differenzen zwischen den Jahren.
Um nun Sicherheit für die richtige Übernahme zu bekommen, nutzen Sie bitte<br>
![](Eroeffnungsbilanz.gif)<br>
den Ausdruck der Bilanz als Eröffnungsbilanz.<br>
Das bedeutet, dass das Ende des letzten Geschäftsjahres den Eröffnungsbuchungen des laufenden Geschäftsjahres gegenübergestellt wird.

#### Wie sollte der Wareneinsatz in der Ergebnisübersicht berücksichtigt werden ?
Wenn für Ihr Unternehmen der Wareneinsatz und somit auch der Lagerstand ein wesentlicher Faktor ist, so muss der Warenvorrat in der Finanzbuchhaltung abgebildet werden. Siehe dazu auch [Abgrenzungen](#Abgrenzung Wareneinsatz).

#### Wie sollte in der Fibu gebucht werden ?
Um mit Ihrer **Kieselstein ERP** Finanzbuchhaltung klare und nachvollziehbare Aussagen treffen zu können, ist es wichtig, dass die erfassten Daten auch klar und strukturiert in der Finanzbuchhaltung erfasst werden.<br>
Als Beispiel sind hier z.B. die Eröffnungsbuchungen von Debitoren bzw. Kreditoren genannt.<br>
Buchen Sie für jedes Konto die Eröffnungsbuchung. In manchen Fälle hat sich bewährt je Beleg eine Eröffnungsbuchung zu machen, damit nach sieben bzw. zehn Jahren noch nachvollziehbar ist, warum denn genau dieser eine Beleg in die neue Buchungsperiode übernommen wurde. Die Anzahl der Buchungen ist bei modernen ERP Systemen kein Problem, es überwiegt hier eindeutig der Vorteil der Transparenz und der Nachvollziehbarkeit.<br>
Lassen Sie sich auf keinen Fall von Ihrem Steuerberater dazu überreden hier nur eine einzige Sammelbuchung reinzusetzen. Es ist für Sie nicht mehr nachvollziehbar und wenn Sie auch nur einen Tippfehler in der Buchung haben, haben Sie keine Chance diesen je zu finden.

#### Wie werden Barauslagen richtig erfasst?
Bei vielen der **Kieselstein ERP** Anwender werden Barauslagen wie Tankbelege, schnelle Einkäufe in einem Baumarkt, Bewirtungsbelege direkt von den Unternehmerpersönlichkeiten aus der privaten Geldtasche bzw. mit der Privaten- oder Firmen-Kreditkarte bezahlt. Wie können diese nun am Einfachsten und richtig erfasst werden.<br>
Grundsätzlich sollten diese Belege alle als Eingangsrechnung erfasst werden. Damit haben Sie ein entsprechende Übersicht über Ihre gesamten Kosten / Aufwände und können in Zukunft auch Überlegungen anstellen, ob gewisse Dinge nicht günstiger beschafft werden können.<br>
Die Zahlung dieser Eingangsrechnung ist als Bankbuchung darzustellen.<br>
Hintergrund:<br>
Gerade die Unternehmerpersönlichkeiten haben ein Verrechnungskonto zwischen dem Unternehmen und der Person als Privatperson, da immer wieder Geldbewegungen zwischen der juristischen Person des Unternehmens und der Person des Unternehmers laufen.<br>
Dieses Konto, für Deutschland und Österreich im Bereich 34xx, ist wie ein Bankkonto zu betrachten.<br>
D.h. neben der Definition des Sachkontos, definieren Sie bitte auch eine Bank und definieren dies als [sichtbare Bankverbindung]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen" >}} ). Bei der Bezahlung der Eingangsrechnung, aber auch wenn z.B. ein Kunde eine Ausgangsrechnung an den Unternehmer bar bezahlt, so erfolgt die Zahlungsbuchung auf diese Bank. Gegebenenfalls geben Sie bei der geforderten Auszugsnummer 0 ein.

Auch Eingangsrechnungen die per Firmen-Kreditkarte bezahlt werden, behandeln Sie in ähnlicher Weise. D.h. für die / jede Firmenkreditkarte ist ein eigenes Bankkonto mit Definition als [sichtbare Bankverbindung]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen" >}} ) anzulegen.

Wenn an die Mitarbeiter die Beträge direkt zu überweisen sind, verwenden Sie bitte die abweichende Bankverbindung. [Siehe]( {{<relref "/einkauf/eingangsrechnung/#kann-f%c3%bcr-die-zahlung-einer-eingangsrechnung-auch-eine-andere-bankverbindung-angegeben-werden" >}} )

#### Kreditkarten
<a name="Kreditkarten"></a>
Wir buchen die Kreditkartenzahlungen immer als eine Eingangsrechnung / als einen Beleg. Ist das so richtig?

Aus Sicht der klassischen Buchhaltung ist dies richtig, aber!<br>
Wir raten zu folgender Vorgehensweise:<br>
Erfassen Sie JEDE Eingangsrechnung gegen den tatsächlichen Lieferanten. Eine Kreditkarte, PayPal, usw. ist nichts anderes als ein weiteres Bankkonto, gegen das diese Rechnung ausgeglichen wird. Durch diese Vorgehensweise erhalten Sie, gerade was Ihre Lieferanten betrifft, eine deutliche Transparenz.<br>
Natürlich spricht nichts dagegen Belege wie z.B. Post, Hotelrechnungen, Sprit-Rechnungen auf einen gemeinsamen Lieferanten z.B. Tanken zu erfassen. Achten Sie aber darauf, dass Ihre wesentlichen Lieferanten, mit denen Sie regelmäßig Geschäfte machen (könnten), namentlich erfasst sind, in dem Sie in der Eingangsrechnungsverwaltung den Namen des Lieferanten richtig angeben. Je nach Höhe der Umsätze können Sie trotzdem für die Buchhaltung eine Sammel-Kreditorennummer angeben oder diesen eben einzeln als eigenständigen Kreditor anlegen.

#### Wie sind Kreditkartenzahlungen beim Jahreswechsel zu erfassen?
Bitte achten Sie darauf, dass die Kreditkarten (wir verstehen dies als Synonym für jegliche Form von Kartenzahlungen) Konten ebenfalls Perioden rein gebucht werden. Gerade beim Jahreswechsel kommt es vor, dass auf der Auszugsnummer 1 noch Einkäufe aus dem Vorjahr enthalten sind, welche zusätzlich mit einem Zahlungsdatum des alten Jahres versehen sind.<br>
Wichtig ist, dass die Zahlungen im richtigen Geschäftsjahr erfasst werden, genau genommen in dem Geschäftsjahr in dem die Abbuchung des Ausgleichs der Kreditkarte gegen Ihr Bankkonto erfolgt. Gehen Sie hier daher bitte so vor, dass, sollten Zahlungsbuchungen noch im alten Geschäftsjahr sein, so buchen Sie diese z.B. zum 1\. des neuen Geschäftsjahres. Viele Anwender buchen die Zahlungen auch so, dass dies Zahlungsdatum mit dem Abbuchungsdatum der Kreditkarte übereinstimmt. Wichtig ist in jedem Falle, dass der Auszug 1:1 mit dem Auszug des Kreditkarteninstitutes übereinstimmt.

#### Wie sollten Barauslagen der Mitarbeiter gehandhabt werden?
Für einige wenige führende Mitarbeiter könnten diese wie oben für die Unternehmerpersönlichkeiten beschrieben gehandhabt werden. In aller Regel wird es jedoch so sein, dass der Mitarbeiter einen relativ geringen Betrag für Sie kurzfristig auslegt, z.B. eine Hotelrechnung oder eine Besorgung, Briefmarken zum Beispiel und sich erwartet, dass er diese Barauslage umgehend ersetzt bekommt.

Das impliziert, dass dem Mitarbeiter in dem Moment wo er den Beleg präsentiert auch die Barauslage ersetzt wird.

Es gibt nun zwei Möglichkeiten:

- a.) Es gibt keine Kasse und auch kein Kassenbuch.
Dies bedeutet, dass (üblicherweise) vom Inhaber dem Mitarbeiter der Belege "abgekauft" wird. D.h. der Inhaber bezahlt dem Mitarbeiter den Beleg sofort aus seiner persönlichen Geldbörse und der Aufwand wird wie oben beschrieben als Barauslage des Inhabers über dessen Verrechnungskonto verbucht.
- b.) Wenn obiges nicht praktikabel ist, wird eine Kasse eingerichtet und einem Mitarbeiter, der Sekretärin, der Buchhaltung, die Verantwortung für die Kasse übergeben.<br>
Das bedeutet nun, dass dem Mitarbeiter der Beleg mit dem Geldbestand aus der Kasse "abgekauft" wird.

D.h. erfassen Sie den Beleg wie oben beschrieben als Eingangsrechnung mit den entsprechenden Kontierungen usw.. Unter Zahlung wählen Sie die Zahlungsart Bar, wählen das gewünschte Kassenbuch aus und bestätigen, dass damit die Eingangsrechnung bezahlt ist.

Die Einlage von der Bank buchen Sie mit der Kassenbuchung gegen das Bankkonto.

Die Einlage des Inhabers verbuchen Sie mit der Kassenbuchung gegen das Verrechnungskonto des Inhabers.

#### Zusammenfassende Meldung
[Siehe bitte]( {{<relref "/verkauf/rechnung/#zusammenfassende-meldung">}} )

#### Splittbuchung
Neben der Splittbuchung in den Eingangsrechnungen und in den Zusatzkosten steht auch direkt in der integrierten Finanzbuchhaltung die Buchungsart Splittbuchung zur Verfügung. Typische Beispiele für diese Buchung sind Löhne und Gehälter bzw. Reisekosten.
Sie finden diese Funktion im Menü unter Bearbeiten Splittbuchung.
![](Splittbuchung1.gif)

Definieren Sie nun die grundsätzlichen Buchungsdaten wie Datum, Text und Belegnummer.
![](Splittbuchung2.gif)

Nun definieren Sie für jede einzelne Buchung<br>
![](Splittbuchung3.gif)
- welche Kontoart
- welches Konto
- welche Kostenstelle
- den Betrag
- ob dieser im Soll oder im Haben zu buchen ist.<br>
Zur Bestätigung der Datenübernahme klicken Sie auf ![](Splittbuchung4.gif) oder drücken Strg+B.
Stellen Sie so die Buchungen auf die Soll- und Haben- Konten zusammen, bis der Saldo 0 ist und die Soll- und Haben- Summen Ihrem gewünschten Wert entsprechen.<br>
Wurde versehentlich eine falsche Zeile eingetragen, so kann mit ![](Splittbuchung8.gif) diese wieder aus der Darstellung gelöscht werden.

Für eine Lohn- und Gehaltsbuchung sieht z.B. ein typischer Buchungsbeleg wie folgt aus:
![](Splittbuchung5.gif)

Die Entsprechung dazu in Ihrer Splittbuchung (mit den oft angetroffenen geringen Abweichungen in den Kontonummern) sieht in **Kieselstein ERP** nun wie folgt aus:
![](Splittbuchung6.gif)

Haben Soll und Haben die gleiche Summe, ist also der Saldo 0,00
![](Splittbuchung9.gif)<br>
so ist der Haken für die eigentliche Verbuchung freigeschaltet. Klicken Sie auf ![](Splittbuchung7.gif). Damit wird Ihre Buchung komplett übernommen.

#### Können in Splittbuchungen auch 0,00 Beträge eingegeben werden?
Gerade wenn Splittbuchungen für eine Reihe von Buchungen durchgeführt werden, so ist es manchmal praktisch, dass immer die gleichen Konten eingetragen bleiben und die für diese eine Splittbuchung nicht benötigten Konten mit einem 0,00 Betrag versehen werden.

Dies wird von **Kieselstein ERP** in der Form unterstützt, dass diese Beträge zwar eingegeben werden können. Bei der Überleitung in die eigentliche Buchungsroutine werden diese Zeilen jedoch unterdrückt.

#### Splittbuchung Reisekosten
Hier sehen Sie ein typische Beispiel für die Verbuchung von Reisekosten

![](Spliitbuchung_Reiko.jpg)

Der zweimalige Eintrag von 6332 ergibt sich daraus, dass in den Diäten (Verpflegungs-Mehraufwand) in Österreich 10% Ust enthalten sind, die auf das entsprechende Vorsteuerkonto gebucht werden müssen.

#### Können Miet-Erlöse in der Gehaltsbuchung angegeben werden?
Es sollten in der Splittbuchung der Gehälter und ähnlichem keine Umsatzsteuerbeträge mit erfasst werden. Der Hintergrund ist die Prüffunktion der UVA, welche dies als falsche Buchung interpretieren würde.<br>
D.h. bitte teilen Sie die ev. in den Gehaltsabrechnungen enthaltene Mieterlöse (= Ust) auf und erfassen Sie diese als eigene Buchung. Bitte buchen Sie dazu die enthaltene Umsatzsteuer auf das **Umsatzsteuer Sammelkonto**.<br>
![](Sachbezuege_im_Lohn.jpg)<br>

Die Kontendefinitionen der beteiligten Konten sind wie folgt:<br>
a.) 6000 Löhne:<br>
![](Sachbezuege_im_Lohn_1.jpg)<br>

b.) 4850, Mieterlöse:<br>
![](Sachbezuege_im_Lohn_2.jpg)<br>

c.) 3501, Ust Sammelkonto:<br>
![](Sachbezuege_im_Lohn_3.jpg)<br>

Eine weitere Variante stellt sich wie folgt dar:<br>
Splittbuchung der Löhne, mit dem Bezug zu den Mieterlösen die ja auch ein Gehaltsbestandteil sind und darauf aufbauend die Splittbuchung für die tatsächlichen Mieterlöse.<br>
Somit muss das Konto Mitarbeiter Wohnungen immer einen Null-Saldo haben. Der Trick liegt hier darin, dass das Konto 8410 Mitarbeiter Wohnungen für die UVA Art als Umsatz Inland steuerfrei (Erlös ohne Umsatzsteuer) definiert ist. Damit wird der Saldo dieses Kontos, der ja null sein muss, in der UVA unter Umsatz steuerfrei angeführt. Damit ist und das ist ja das Ziel, die Prüfung der UVA Buchung erfolgreich.

Beispiel:
| Konto  | Bezeichnung | Soll | Haben |
| --- |  --- |  --: |  --: |
| 3590 | Finanzamt Lohnabgaben |   |    6.595,35 |
| 6000  | Löhne & Gehälter  | 43.979,56  |   |
| 6500  | Gesetzlicher Sozialaufwand | 11.063,52 |   |
| 6550  | MVK Beitrag  |       601,81 |   |
| 6610  | Kommunalsteuer |    1.668,12 |   |
| 6620  | DB |    2.067,46 |   |
| 6630  | DZ |       180,26 |   |
| 3570  | Kommunalsteuer |   |    1.668,12 |
| 3580  | GKK |   | 20.903,58  |
| 3590  | Finanzamt Lohnabgaben  |   |   2.247,72  |
| 3600 | Verbindlichkeiten Löhne |   | 37.925,96  |
| 8410  | Mitarbeiter Wohnungen |   |      220,00 |

Und dazu nun die Weiterbuchung vom Konto 8410

| Konto  | Bezeichnung | Soll | Haben |
| --- |  --- |  --: |  --: |
| 8410 | Mitarbeiter Wohnungen | 220,00 |   |
| 8411  | Erlöse - Miete Wohnung |   | 131,82 |
| 3501  | Mehrwertsteuer 10% |   |    13,18 |
| 8412  | Erlöse - Betriebskosten Wohnung |   |    62,50 |
| 3502 | Mehrwertsteuer 20%  |   |    12,50 |

#### Wie sollten Verrech. sonstige Sachbezüge Kfz 19% USt. erfasst werden?
Auch für diese Verbuchung gilt das unter Miet-Erlöse geschriebene.
D.h. bitte erfassen Sie die Sachbezüge Ihrer MitarbeiterInnen
- 3790 Lohn- und Gehaltsverrechnung
    - 4947 Verrech. sonstige Sachbezüge Kfz 19% USt.
    - 3806 Umsatzsteuer 19%

als eigenständige Splittbuchung.<br>
Auch hier empfiehlt sich, dies als Splittbuchung zu erfassen, da Splittbuchungen sehr einfach für das nachfolgende Monat innerhalb des Geschäftsjahres kopiert ![](Splittbuchung_kopieren.gif) werden können.

[Zu diesem Thema siehe auch]( {{<relref "/management/finanzbuchhaltung/tipps/#buchung-eines-sachbezuges-mit-mwst">}} )

#### Können Buchungsschablonen hinterlegt werden?
Wir haben auf die Verwendung von Buchungsschablonen verzichtet und stellen stattdessen die Möglichkeit bereits getätigte Splittbuchungen als Vorlagen zu verwenden zur Verfügung. Um z.B. eine weitere Gehaltsbuchung im nächsten Monat zu machen, suchen Sie bitte eine bereits durchgeführte Gehaltsbuchung und klicken im Reiter Buchungen auf "Neue Splittbuchung aus Buchung erstellen" ![](Neue_Splittbuchung.gif). Damit werden alle mit dieser Buchung verbundenen Buchungen als Vorlage in den Bearbeitungsmodus der Splittbuchung übernommen. Definieren Sie Datum, Text und Beleg und korrigieren Sie gegebenenfalls die entsprechenden Beträge auf den einzelnen Konten. Die Vorgehensweise ist wie oben unter Splittbuchung beschrieben.

#### Wie können manuelle Buchungen durchgeführt werden ?
Hier kann zwischen drei verschiedenen Vorgehensweisen für die Durchführung der Buchung unterschieden werden
1.  Manuelle Buchung (ehemals Umbuchung)
    Hier kann nur zwischen zwei Konten umgebucht werden.
    Buchungen zwischen Sachkonten erfolgen immer ohne Mehrwertsteuer, da keine automatische Steuerinformation gegeben ist.
    Buchungen zwischen Sachkonto und Debitor bzw. Kreditor kann auch mit Angabe des Mehrwertsteuersatzes erfolgen.
2.  Splittbuchung
    Hier kann in einer Buchung eine Sammlung von Soll und Haben Konten angegeben werden.
    Es muss Summengleichheit herrschen.
    Insofern könnte hier manuell auch ein Vst / Ust Konto mit Betrag (manuell errechnet) angegeben werden
3.  Buchungen über das Kassenbuch
    Dies ist für die Buchungen kleiner Beträge gedacht, daher können hier auch, abhängig von der Art des Gegenkontos MwSt / VSt behaftete Buchungen durchgeführt werden. Bei Debitoren / Kreditoren werden diese aus den Einstellungen des jeweiligen Kontos geholt, bei Buchungen auf Sachkonto kann Ust oder Vst angegeben werden.

#### Wieso wird bei manuellen Buchungen die Vorsteuer / Umsatzsteuer falsch errechnet?
Die Ursache dafür ist in einer falschen / unvollständigen Definition des jeweiligen Sachkontos zu suchen.<br>
D.h. es tritt dies bei Buchungen Wareneinsatz (oder Miete) gegen Bank auf. Ist nun das Sachkonto (Miete) ohne Steuerkategorie definiert, wird auch keine MwSt gebucht. Es steht auch die Auswahl der Mehrwertsteuerart nicht zur Verfügung. Für die richtige Verbuchung definieren Sie bitte am Ziel-Sachkonto (also z.B. der Miete) die Steuerkategorie mit der MwSt Art ![](Steuerkategorie_Sachkonto.gif). [Siehe dazu auch](#Manuelle Buchung mit MwSt)

#### Wie fehlerhafte Automatikbuchungen korrigieren?
Es kommt leider immer wieder vor, dass z.B. aufgrund fehlerhafter Definitionen vor allem Automatikbuchungen gemacht werden, die dann korrigiert werden sollten. So z.B. wird bei mehreren Finanzämter immer wieder auf das falsche Finanzamt gebucht und damit ist natürlich die gesamte Ust-Thematik falsch.

Um dies nun zu korrigieren gehen Sie am Besten wie folgt vor.
- a.) Wir gehen davon aus, dass Sie den Fehler relativ zeitnah bemerken. D.h. dass in der Regel noch keine Zahlungsbuchungen auf der Ausgangs-Rechnung sind.
- b.) nehmen Sie die Rechnung zurück. D.h. gehen Sie in die Rechnung, wählen Sie ändern und bestätigen Sie die Abfrage nach "Rechnung ändern" mit ja. Damit ist die Rechnung im Status angelegt und aus der Buchhaltung herausgenommen. Sollte es mehrere Rechnungen betreffen, so gehen Sie bitte für die anderen (hoffentlich wenigen) Rechnungen analog vor.
- c.) Stellen Sie nun die Konten richtig.
- d.) Drucken Sie die Rechnungen wieder aus, zumindest soweit, dass diese wieder im Status offen sind.
Sollten Zahlungsbuchungen auf der Rechnung gewesen sein, so müssen diese zuerst zurück genommen und dann auch wieder eingetragen werden.

**Hinweis:**

Bedenken Sie bitte, dass insbesondere bei Rechnungen an Lieferadressen in andere Länder, die Lieferadresse die Buchung, also die Erlöskontenfindung definiert. Hier hilft am schnellsten ein Ausdruck der Rechnung.

#### Gemeinsame Steuerverwaltung
Liechtenstein und die Schweiz haben eine gemeinsame Steuerverwaltung. D.h. Lieferungen von Liechtenstein in die Schweiz und umgekehrt müssen als Inlandslieferung gehandhabt werden.

Dafür sind folgende Definitionen zu machen:
- a.) definieren Sie im Modul System, unterer Modulreiter System, oberer Modulreiter Land, für Liechtenstein als gemeinsames Postversendungsland die Schweiz.<br>
![](Gemeinsame_Steuerverwaltung_Postversendungsland.jpg)
- b.) Bei den Erlöskonten definieren Sie für die Länderartübersetzung zusätzlich die Übersetzung des Landes so, dass Lieferungen in die Schweiz als Buchung auf das Inlandskonto durchgeführt werden. Also für das jeweilige Sachkonto wird beim Inlandskonto noch die Konto-Landzuweisung für die Schweiz durchgeführt.

Obige Beschreibung ist für Liechtenstein. Für die Schweiz ist es vice versa.

#### IG-Erwerbsbuchungen
Bei Innergemeinschaftlichem Erwerb sind zum Erhalt der Vorsteuerabzugsberechtigung ebenfalls Mehrwertsteuerbuchungen erforderlich. Dies ist einerseits die Buchung der Einfuhrumsatzsteuer und andererseits die Buchung der Vorsteuer.

Diese doppelte Buchung wird in der Steuerkategorie definiert.

Definieren Sie dazu die Steuerkategorie AuslandEUmUID (EU-Ausland mit UID Nr.) wie folgt:

Zusätzlich muss die theoretische Steuer bereits ab der Eingangsrechnung definiert werden.

D.h. das Konto hat in der UVA Art IG Erwerb normal oder reduziert definiert.
In der Steuerkategorie werden für die Normalsteuer und die reduzierte Steuer die beiden Konten definiert. Also das Konto für die Einfuhrumsatzsteuer reduzierter Satz und Normalsatz und das Konto für die IG-Vorsteuer reduzierter Satz und Normalsatz.

Erfolgt nun eine Buchung auf dieses Konto, so wird anhand der UVA Art erkannt, dass die UstBuchungen so wie die normalen Ust-Steuerbuchungen mitzubuchen sind.

#### Buchungen mit mehreren beteiligten Finanzämtern
#### Geschäftsfall mit mehreren Finanzämtern
[Siehe bitte]( {{<relref "/management/finanzbuchhaltung/integriert/mehrere_finanzaemter">}} ).

#### Anbindung des integrierten Finanzbuchhaltungs Moduls
Die in **Kieselstein ERP** integrierte Finanzbuchhaltung übernimmt Daten aus den Eingangs- und Ausgangsrechnungen in den Bereich der Finanzbuchhaltung. Programmtechnisch ist dies so realisiert, dass die Übergabe über Schnittstellen erfolgt, welche den jeweiligen Beleg bzw. die Zahlung usw. sofort verbuchen. Das bedeutet:
-   Technisch gesehen ist eine klare Trennung zwischen dem Bereich des Modules der Finanzbuchhaltung und dem restlichen ERP System **Kieselstein ERP** gegeben
-   Für den Anwender stellt es sich, aufgrund der guten Integration so dar, dass es aus einem Guss ist und jede Buchung in den Ein- und Ausgangsrechnungen sofort in der Buchhaltung aufscheint, was auch deutlich zur Buchungssicherheit beiträgt.

#### Wo wird das Standard Finanzamt definiert?
Dies ist jenes Finanzamt, welches im System unter Mandant, Vorbelegungen 2, als Finanzamt eingetragen ist. Es ist dies der Vorschlagswert für die Definition neuer Konten. Welches Finanzamt für welches Konto gilt, wird direkt beim jeweiligen Konto definiert.

<a name="Anzahlung-Schlussrechnung"></a>

#### Anzahlungs- und Schlussrechnungen
Für die Verbuchung von Anzahlungs- und Schlussrechnungen und der automatischen Eröffnungsbuchungen müssen in den Buchungsparametern des Haupt-Finanzamtes die Konten der Anzahlungsrechnungen definiert werden.

Wenn bei der Verbuchung einer Anzahlungsrechnung die Fehlermeldung mit dem Hinweis: Verrechnungskonto Erhaltene Anzahlungen nicht definiert für Anzahlungsrechnung jj/nnnnnnn erscheint, so ist das Konto Verr.Erhalten (Verrechnete Anzahlungsrechnungen Erhalten) nicht definiert.

Bitte beachten Sie, dass in einer Anzahlungsrechnung in aller Regel nur eine Position, die des Anzahlungsbetrages, üblicherweise ist diese eine Handeingabe, enthalten ist. 

Grundsätzlich dürfen in einer Anzahlungsrechnung keine Lieferscheine enthalten sein. Weiters darf es nur eine werthaltige Position geben. Das Gewähren von Skonto ist bei Anzahlungsrechnungen gesetzlich so nicht vorgesehen und auch nicht erlaubt, wird aber immer wieder von Kudnen gemacht. Sollten diese Vorgaben nicht erfüllt sein, erscheint eine entsprechende Fehlermeldung. 

Mit gleicher Logik werden die Anzahlungs- und Schlussrechnungen der Eingangsrechnung verbucht. Für die Parametrierung werden die Definitionen der Verr.Geleistet und Geleistet verwendet.

Bitte beachten Sie auch die [Definition der UVA-Arten](#UVA_Anzahlungs-Schlussrechnung).

**Buchungsparameter**

EB Konten, Konten für die automatischen Eröffnungsbuchungen

| Konto | Beschreibung |
| --- |  --- |
| Sachkonten | Geben Sie hier das EB-Konto für die Eröffnungsbuchungen der Sachkonten an |
| Debitoren | Geben Sie hier das EB-Konto für die Eröffnungsbuchungen der Debitoren an. || Kreditoren |  EB-Konto für die Eröffnungsbuchungen der Kreditoren |

Hinweis: Wir raten für diese drei Arten unterschiedliche Konten zu verwenden um bei Unstimmigkeiten rascher diese Buchungs- / Definitionsfehler zu finden. Theoretisch können auch gemeinsame Konten verwendet werden.

Anzahlungen
| Konto | Kontoart | Beschreibung |
| --- |  --- | --- |
| <a name="Anzahlung_Verr.Erhalten"></a>Erhaltene Anzahlungen(Verr.Erhalten) | Aktives Bestandskonto | Verrechnungskonto gelegte Anzahlungen Konto der an Ihre Kunden gelegten (geleisteten) Anzahlungsrechnungen |
| <a name="Anzahlung_Erhalten"></a>Erhaltene Anzahlungen bezahlt(Erhalten) | Passives Bestandskonto | Konto mit den Zahlbeträgen der gelegten Anzahlungen. Konto ist mit einem Bankkonto vergleichbar. Das ist treuhändisches erhaltenes Geld. Das bedeutet: Im schlimmsten Fall, wenn z.B. das Projekt nicht realisiert werden kann, ist dieses Geld wieder zurückzuzahlen. Fachlich richtig ist dies ein Verbindlichkeiten Konto. |
| Gegebene Anzahlungen (<a name="Anzahlung_Verr.Geleistet"></a>Verr.Geleistet) | Passives Bestandskonto | Verrechnungskonto der von Ihren Lieferanten erhaltenen (und als ER eingebuchten) Anzahlungsrechnungen |
| Gegebene Anzahlungen bezahlt(<a name="Anzahlung_Geleistet"></a>Geleistet) | Aktives Bestandskonto | Konto mit den Zahlbeträgen der erhaltenen Anzahlungen. Konto ist mit einem Bankkonto vergleichbar. Hinweis: Dies ist treuhändisch an Ihren Lieferanten bezahltes Geld. Das bedeutet, eigentlich gehört das noch Ihnen. Fachlich richtig ist dies ein offene Forderungen Konto. |

G+V:
| Konto | Beschreibung |
| --- |  --- |
| Gewinnvortrag |   |
| Jahresgewinn |   |

Musterdefinition für Anzahlungskonten nach SKR 03<br>
![](Anzahlungskonten.gif)<br>
Die Übersetzung auf SKR04 lautet:
- Erhaltene Anzahlungen ... 1190
- Erhaltene Anzahlungen bezahlt ... 1495
- Gegebene Anzahlungen ... 1186
- Gegebene Anzahlungen bezahlt ...

Musterdefinition für Anzahlungskonten nach dem österreichischen Einheitskontenrahmen
![](Anzahlungskonten_AT.gif)

Die Anzahlungen sind sowohl für normale Eingangs- und Ausgangsrechnungen zu definieren als auch für Reverse Charge Rechnungen. Die Denkweise ist bei beiden Arten die gleiche. Beachten Sie bitte die Unterschiede für die [Definition der UVA-Art](#UVA_Anzahlungs-Schlussrechnung).
Ablauf bei der Verbuchung von Anzahlungs- / Schlussrechnungen: 

1.  Es wird eine Anzahlungsrechnung erstellt. 
    Es wird Debitor gegen [Verr.Erhalten](#Anzahlung_Verr.Erhalten) gebucht

2.  Kunde zahlt die Anzahlungsrechnung oder einen Teil davon
    Bank bucht gegen Debitor und macht eine Umbuchung von [Verr.Erhalten](#Anzahlung_Verr.Erhalten) gegen [Erhalten](#Anzahlung_Erhalten) mit dem Teil-Zahlungsbetrag

3.  Schlussrechnung wird erstellt.
    Es wird Debitor gegen Erlöse und [Erhalten](#Anzahlung_Erhalten) gegen Debitor gebucht
    Damit sind alle Zahlungseingänge, also auch die auf die Anzahlungsrechnungen direkt dem Debitor zugeordnet.
    Mit der Schlussrechnung haben die Konten [Verr.Erhalten](#Anzahlung_Verr.Erhalten) und [Erhalten](#Anzahlung_Erhalten), für dieses Projekt, wieder einen Null-Saldo

4.  Rest der Schlussrechnung wird bezahlt. Wird wie eine übliche Ausgangsrechnung gebucht. (Bank gegen Debitor)

Die Umsatzsteuerkonten werden anhand der Erlöskonten ermittelt.
**Info:**
Die Gegenbuchungen der Schlussrechnung für die bereits verbuchten Anzahlungsrechnungen werden aus den Buchungen der mit der Schlussrechnung über den Auftrag verbundenen Anzahlungsrechnungen ermittelt. Das bedeutet: Sollten Fehler in den Anzahlungsrechnungsbuchungen, z.B. durch falsche Parametrierung, enthalten sein, so müssen zuerst die Anzahlungsrechungen durch erneute Belegübernahme richtiggestellt werden. Erst nach der Richtigstellung der Anzahlungsrechnungen wird auch die Schlussrechnung richtig verbucht.

#### Bei der Verbuchung einer Schlussrechnung kommt eine Fehlermeldung
Erhalten Sie beim Drucken / Belegübernahme einer Schlussrechnung eine Fehlermeldung wie die nachfolgende,<br>
> class com.lp.client.frame.ExceptionLP<br>
> java.lang.Exception: !summeHaben.equals(summeSoll)
>
so prüfen Sie bitte ob:
- a.) alle Anzahlungs- und Schlussrechnungen wirklich auf den gleichen Debitor lauten
- b.) bei Anzahlungsrechnungen ob diese im Jahr der Schlussrechnung sind bzw. dass diese im Geschäftsjahr der Schlussrechnung eröffnet wurden.

Bitte prüfen Sie dies sowohl in den Kundendaten als auch in den Debitor-Konten.

#### Kann ich Anzahlungs- und Schlussrechnungen auch in Fremdwährung abwickeln?
Ja. Auch wenn hier das Thema der Kursdifferenzen dies deutlich verkompliziert, verbucht Ihr **Kieselstein ERP** dies nach folgender schematischer Darstellung
![](Anz_Schluss_mit_Fremdwaehrung.gif)

#### Warum kann für einen Auftrag mit offenen Anzahlungsrechnungen keine Schlussrechnung erstellt werden?
[Siehe bitte](../Rechnung/Anzahlung_und_Schlussrechnung.htm#Schlussrechnung bei offener Anzahlungsrechnung). 

#### Wie sollten Vorauszahlungen an Lieferanten verbucht werden?
Wie oben beschrieben können auch Anzahlungsrechnungen und Schlussrechnungen auf der Eingangsrechnungsseite automatisch verbucht werden. [Siehe dazu bitte auch.](../Eingangsrechnung/index.htm#Vorauszahlungen)

Weiters gibt es eine Buchungsart Vorauszahlung (bei der Manuellen Buchung) und bei der Zahlungsbuchung die Zahlungsart Vorauszahlung.<br>
Diese bringt die Buchungen des Vorauszahlungskontos gefiltert auf den Kreditor und die Zahlungen die noch nicht auf andere ER's gebucht sind.<br>
Ist die Vorauszahlung höher als der Rechnungsbetrag wird der Rest als neue Vorauszahlungs-Umbuchung gebucht.<br>
Das hat den Vorteil, dass die noch nicht abgerechneten Vorauszahlungen klar ersichtlich sind.

**Wichtiger Hinweis zur Unterscheidung zwischen Anzahlung und Vorauszahlung:** Wir betrachten jegliche Form von reinen Zahlungen als eine Bewegung von Geld, welche per Definition keinen Umsatzsteueranteil haben.<br>
Das bedeutet: Wenn Sie von einem Kunden Geld / eine Zahlung erhalten und dieses ist für ein Projekt, also eine Aufgabe die Sie für Ihn machen sollten, so ist dies grundsätzlich ein Auftrag und darauf eine Zahlung auf eine Anzahlungsrechnung. Erfassen Sie bitte diesen Vorgang auch genau so. Manche BuchhalterInnen sind der Meinung dass das viel zu kompliziert ist. Die Praxis hat gezeigt, mit allen Abhängigkeiten, denken Sie z.B. nur an die Liquiditätsvorschau oder an die richtige Verbuchung der Mehrwertsteuer, ist die Verwendung der Anzahlungsrechnung der sicherere und weit schnellere Weg. Hier kommt die Übersichtlichkeit dann noch mit oben drauf.<br>
Dies gilt sowohl für die Eingangs- als auch für die Ausgangsrechnung.<br>
Das bedeutet, dass eine Vorauszahlungsbuchung wirklich nur dann erfolgen sollte, wenn eine Zahlung erfolgt, die derzeit keinem "Projekt" (Auftrag, Bestellung, ...) zugewiesen werden kann.<br>
Aus diesem Grunde sind für die Verwendung einer Vorauszahlungsbuchung zum Ausgleich einer Rechnung nur Buchungen ohne Umsatzsteuer erlaubt. Hintergrund: Anderenfalls würde die Umsatzsteuer doppelt verbucht werden, was sicherlich nicht in Ihrem Sinne ist.

Erweiterung der Anzahlungs-/Schlussrechnung

Die wesentliche Umstellung betrifft dass nun die Umsatzsteuer nach den geleisteten Zahlungen auf den Anzahlungsrechnungen errechnet wird. Dies betrifft sowohl die Eingangs- wie die Ausgangsrechnungen.<br>
Sollten Sie bei der Buchung eine Fehlermeldung ähnlich unten stehender erhalten, so korrigieren Sie bitte die Definition des Anzahlungs-(Erlös) Kontos<br>
![](Warnung_Sachkonto_eventuell_falsch_definiert.jpg)

**WICHTIG:** Beachten Sie bitte, dass Anzahlungsrechnungen dafür gedacht sind, dass Ihnen Ihr Kunde Geld treuhändisch übergibt / leiht, damit Sie ihm glauben, dass er Ihr Produkt / Ihre Leistung auch tatsächlich will. Damit ist implizit enthalten, dass mit Arbeiten erst begonnen wird, wenn die Anzahlungsrechnungen auch tatsächlich bezahlt sind. Somit gibt es theoretisch die Situation, dass eine offene Anzahlungsrechnung schlussgerechnet wird nicht.<br>
Die Praxis und oft auch die wirtschaftliche Lage zeigt, dass es auch durchaus zu Fällen kommen kann dass:
- am 20.10\. wird die Anzahlungsrechnung oft zu 100% erstellt
- am 30.10\. ist die Ware / Lieferung / Dienstleistung fertig und wird, obwohl die Anzahlungsrechnung noch immer offen ist, Schlussabgerechnet.
- am 2.11\. (also im Folgemonat) wird nun die Anzahlungsrechnung bezahlt und damit ist vermeintlich der Vorgang abgeschlossen.

Aus der oben beschriebenen Logik ergibt sich, dass die Umsatzsteuer am Zahlungsdatum der Anzahlungsrechnung hängt. Das bedeutet in dieser Konstellation, dass Sie die Umsatzsteuer einen Monat zu spät abführen.<br>
Unsere Empfehlung zur Vorgehensweise ist, da sich diese Konstellation nicht auf das Wesen einer Anzahlungsrechnung bezieht, dies eben über eine normale Rechnung abzuwickeln.<br>
D.h. entweder Sie stimmen sich mit Ihrem Kunden ab und wandeln die Anzahlungsrechnung in eine normale Rechnung um (was nicht 100%ig dem Gedanken des Gesetzgebers entspricht) oder
Sie stornieren die Anzahlungsrechnung und verrechnen die Lieferung über eine normale Rechnung. Gemeinsam mit der Rechnung senden Sie Ihrem Kunden auch das Storno der Anzahlungsrechnung. Variante 2 ist korrekt, erzeugt aber stornierte Rechnungen. D.h. es sollte in jedem Falle auch eine Stornobegründung in der Anzahlungsrechnung erfasst werden.

#### Buchungsjournal
Im Buchungsjournal (unterer Modulreiter im Modul Finanzbuchhaltung) sehen Sie ein Protokoll aller Ihrer Buchungen. Auch hier werden aus Gründen der Übersichtlichkeit die stornierten Buchungen nicht angezeigt. Diese können durch anhaken von plus stornierte jederzeit angezeigt werden.

Im Buchungsjournal stehen drei obere Reiter zur Verfügung:

| Reiter | Beschreibung |
| --- |  --- |
| Buchungen | Hier sehen Sie alle Buchungen in der Form, dass pro Buchung eine Zeile angezeigt wird. |
| Detail | Hier sehen Sie die mit der ausgewählten Buchung verbundenen Einzelbuchungen. Beachten Sie bitte, dass auch die Buchungen auf mitlaufende Konten angezeigt werden. |
| Detailliert | Hier sehen Sie jede einzelne Buchung in einer durchgängigen Form. Also die Buchung inkl. aller dazugehörenden Detailbewegungen. Neben den Direktfiltern Beleg und Text steht hier aus die Suche nach Betrag zur Verfügung. Beachten Sie bitte, dass der eingegebene Betrag mit einer Toleranz von in der Regel 10% gesucht wird, unabhängig vom Vorzeichen. Die Toleranz kann im Mandantenparameter TOLERANZ_SUCHE_NACH_BETRAG eingestellt werden. |

#### Datenträgerüberlassung, Buchungsjournal Export für Steuerprüfung
Selbstverständlich steht, z.B. für eine eventuelle Finanzprüfung, der Export des Buchungsjournals zur Verfügung.<br>
Um diese Daten zu exportieren wählen Sie im Modul Finanzbuchhaltung, den unteren Modulreiter Buchungsjournal.<br>
Dann wählen Sie den Menüpunkt Buchungsjournal und dann Export.<br>
![](Export_Buchungsjournal1.jpg)

Hier definieren Sie nun das Exportformat. In der Regel ist dies für die Finanzprüfung das HV RAW Format.<br>
![](Export_Buchungsjournal2.jpg)

RAW steht hier dafür, dass die Rohdaten exportiert werden. D.h. keine Veränderungen der Buchungen beim Export vorgenommen werden.

Trennzeichen im CSV ist Tabulator. Es gibt KEINE Metazeichen wie Anführungszeichen für Textspalten.
So sehen z.B. die ersten drei Zeilen des exportierten Files unverändert aus:

> Datum Buchung Buchungsart Konto Kontonamen Kontotyp Soll Haben Storniert<br>
01.01.2011 6795 EB 9800 Eröffnungsbilanzkonto Sachkonto 0,01 0<br>
01.01.2011 6795 EB 0120 Datenverarbeitungsprogramme, Software Sachkonto 0,01 0
>
1. Datum ist das Buchungsdatum.
2. Buchung ist die Buchungs-ID aus der **Kieselstein ERP** Datenbank.
Alle Zeilen mit der gleichen ID gehören zur selben Buchung.
3. Es gibt folgende Buchungsarten:
    - BK = Bankbuchung
    - KA = Kassenbuchung
    - EB = Eröffnungsbuchung
    - UB = Umbuchung
    - BU = Buchung
    - SV = Saldovortrag
4. Konto ist die Nummer des Kontos
5. Kontonamen ist die Bezeichnung des Kontos
6. Kontotyp gibt an ob es sich um ein Sachkonto, Debitorenkonto oder Kreditorenkonto handelt.
7. Soll
8. Die Beträge in den Spalten Soll und Haben sind mit optionalem Komma und ohne Gruppierung formatiert.
9. Storniert gibt an ob es sich um eine stornierte Buchung handelt ( = 1) oder nicht (= 0).

Nicht exportiert werden Buchungen auf mitlaufende Konten (offene Forderungen / Verbindlichkeiten), wenn diese automatisch im Zuge eine Buchung auf ein Debitoren-/Kreditorenkonto erstellt wurde.<br>
Bucht man direkt auf ein mitlaufendes Konto (z.B. Konto Forderungen/Verbindlichkeiten...) wird die Zeile sehr wohl mitexportiert. Diese Buchung wird aktuell verhindert.<br>
<!-- Da Buchungen auf mitlaufende Konten nicht per Hand erfolgen dürfen, wird in die Datei lp.log im log Verzeichnis des Servers eine Fehlermeldung geschrieben.

Diese schaut in etwa so aus:
2014-01-30 10:41:24,476 WARN [com.lp.server.finanz.ejbfac.FibuExportFacBean] Direkte Buchung auf mitlaufendes Konto: buchung_i_id = 13; belegnr: 13/0006 ; konto: 2100 Debitoren Ausland

Das bedeutet, dass die Buchung mit der ID 13 und der Belegnummer 13/0006 auf dem Konto 2100, dem Debitor Ausland Sammelkonto, eine Buchung per Hand gemacht wurde.
Diese Buchung kann im einfachsten Fall storniert werden. Dazu sollte aber der Hintergrund der Buchung bekannt sein, da eventuell andere Buchungen nötig sind, um den Fehler zu korrigieren.
Wurde ein Sammelkonto als Ust/Vst Konto definiert (lässt sich im Buchungsjournal>Details sehr leicht erkennen), kann im entsprechenden Beleg (Rechnung/Eingangsrechnung) mit Bearbeiten>Belegübernahme dieser Fehler korrigiert werden.

Nach der Storinierung der entsprechenden Buchungen, sieht man im lp.log nach einem neuen Exportdurchlauf folgendes:

2014-01-30 10:41:24,476 WARN [com.lp.server.finanz.ejbfac.FibuExportFacBean] Direkte Buchung auf mitlaufendes Konto: buchung_i_id = 13; belegnr: 13/0006 ; konto: 2100 Debitoren Ausland ,storniert
2014-01-30 10:41:24,476 WARN [com.lp.server.finanz.ejbfac.FibuExportFacBean] Direkte Buchung auf mitlaufendes Konto: buchung_i_id = 231; belegnr: 13/0006 ; konto: 2100 Debitoren Ausland ,storniert

Es gibt jetzt die ursprüngliche (falsche) Buchung und die Stornobuchung dazu. Das 'storniert' am Ende ist selbstsprechend.
 -->

#### Schweizer Besonderheiten
In der Schweiz und in Liechtenstein ist die Vorsteuer für Investitionen unabhängig von den sonstigen (Waren-) Einkäufen zu erfassen. Aus der Sicht von **Kieselstein ERP** bedeutet dies, dass dies eigene Mehrwertsteuersätze sind.

Definieren Sie daher eine Mehrwertsteuer z.B. Investitionen und den zugehörigen Steuersatz im Modul System, unterer Modulreiter Mandant und obere Modulreiter MwSt Bezeichnung und MwSt.

![](MwStSatz_Investitionen.gif)

Nun wechseln Sie noch in das Modul Finanzbuchhaltung, wählen den unteren Modulreiter Finanzamt, wählen ihr Finanzamt (Eidg. Steuerverwaltung) und definieren für die Steuerkategorie Inland unter Steuerkategoriekonto für die Mehrwertsteuer Investitionen das entsprechende Vorsteuerkonto.

![](MwStSatz_Investitionen2.gif)

#### Werbeabgabe
Um die Werbeabgabe auch im UVA Formular entsprechend mit anzudrucken ist eine spezielle Einstellung der UVA-Formulare erforderlich. Bitten Sie ihren **Kieselstein ERP** Betreuer dies für Sie vorzunehmen. Details zur Werbeabgabe [siehe](../Rechnung/index.htm#Werbeabgabe).

#### Periodenübernahme
<a name="Periodenübernahme"></a>
Unter Bearbeiten, Periodenübernahme steht diese Funktion, welche von einigen auch als Saldovortrag bezeichnet wird, zur Verfügung.
Buchungen die in diesem Lauf der Periodenübernahme durchgeführt werden, werden als automatische Eröffnungsbuchung gekennzeichnet.
Es wird dies in den Konten in einer zusätzlichen Spalte vor der Buchungsart angezeigt.
![](EB_Buchung.gif)<br>
Die automatische Periodenübernahme ist für Debitoren- und Kreditorenkonten vorgesehen. Für Sachkonten führen sie die Periodenübernahme für das jeweilige Konto einzeln durch - siehe auch Beschreibung unten.

Die grundsätzliche Vorgehensweise für die Periodenübernahme ist:
1.  Prüfen Sie die offene Postenliste des zu übernehmenden Geschäftsjahres.<br>
    Hier ist wichtig, dass diese vor der Übernahme richtiggestellt wird. Verwenden Sie dazu auch die Möglichkeit des [Auszifferns](#Ausziffern) der Buchungen.<br>
    Prüfen Sie dies für alle Debitoren- und alle Kreditorenkonten
2.  Führen Sie die Periodenübernahme für alle Debitoren- und Kreditorenkonten durch.<br>
    Gehen Sie dazu in das Geschäftsjahr in dem die Übernahme stattfinden sollte.<br>
    Bitte achten Sie darauf, dass bei den Konten das Häkchen "automatische Eröffnungsbuchung" gesetzt ist.<br>
    **Hinweis:** Zu Ihrer Sicherheit muss für die Übernahme das Vorvergangene Geschäftsjahr gesperrt sein.<br>
    Ein Beispiel: Wenn die Übernahme für 2013 durchgeführt werden sollte, so muss das Geschäftsjahr 2011 abgeschlossen sein und es muss dies durch [Aktuelles Geschäftsjahr sperren](#Sperren) auch in der **Kieselstein ERP** Finanzbuchhaltung eingetragen sein.
    Bei der Übernahme werden eventuell bereits durchgeführten automatischen Eröffnungsbuchungen storniert und aufgrund der neuen Ergebnisse wieder neu hinzugefügt.
    Zusätzlich kann angegeben werden, dass bereits durchgeführte manuelle Eröffnungsbuchungen ebenfalls gelöscht werden.<br>
    Bei den Übernahmen der offenen Eingangs- / Ausgangsrechnungen werden zur Erhöhung der Transparenz jede einzelne offene Rechnung in das neue Geschäftsjahr eröffnet. Damit wird auch erreicht, dass die automatische Auszifferung auch im neuen Geschäftsjahr gegeben ist.
    Die Soll- / Haben-Buchungen werden auf der gleichen Seite wie die ursprüngliche Buchung übernommen. So werden Sie hier Gutschriften mit entsprechenden negativen Beträgen finden. Dies ist so beabsichtigt und dient der höheren Transparenz.
3.  Führen Sie nun die Periodenübernahme für jedes einzelne Sachkonto durch.
    Bei den Sachkonten haben wir dies so gelöst, dass jedes Sachkonto einzeln übernommen werden muss.<br>
    Bitte beachten Sie, dass nur die Bestandskonten eröffnet werden (dürfen).<br>
    Hier kommt die Besonderheit mit dazu, dass die sogenannten mitlaufenden Konten in der Regel aus den Buchungen auf den Debitoren und Kreditoren bebucht werden. D.h. in aller Regel sind hier keine manuellen Buchungen erlaubt.<br>
    Bei den Buchungen auf den Sachkonten wird jeweils der Saldo des Vorjahres auf dem Konto gebildet und dieser als Eröffnungsbuchung in das neue Geschäftsjahr übernommen. Hier lautet die Regel, dass es nur positive Beträge gibt. D.h. ja nachdem ob der Saldo positiv oder negativ ist, wird der Saldo im Soll oder im Haben positiv eingebucht.<br>
    Ein positiver Saldo wird im Soll gebucht, ein negativer Saldo wird positiv im Haben gebucht.
4.  Sollten nach erfolgter Periodenübernahme auf einem Konto trotzdem Buchungen im alten Geschäftsjahr ergeben, so wird auf den Kontoblättern eine Information angedruckt, die besagt, dass die Periodenübernahme nicht mehr gültig ist. Führen Sie in diesem Falle die Periodenübernahme erneut durch.

**<u><a name="Mitlaufende_Buchung"></a>Hinweis:</u>**
Wir gehen davon aus, dass die mitlaufenden Konten keine wie immer geartete manuelle Buchung enthalten.<br>
Werden in den mitlaufenden Konten Korrekturen durchgeführt, müssen diese Korrekturen Manuell in die Folgejahre übernommen werden.<br>
Aus unserer Sicht ist die manuelle Buchung auf mitlaufenden Konten eine zwar schnelle, aber sehr sehr ungenaue Arbeitsweise, die von manchen Steuerberatern gemacht wird, die aber aus Gründen der Transparenz eindeutig abzulehnen ist.

**<u>Praktischer Ablauf:</u>**

Der praktische Ablauf bei einem Sollversteuerer, z.B. GmbH, ist so, dass mit dem ersten Tag des neuen Geschäftsjahres die Bankkonten des alten Jahres abgeschlossen werden und dann für diese Konten die Periodenübernahme gemacht wird. Üblicherweise erhalten Sie in den ersten Tagen noch Rechnungen aus dem vorigen Geschäftsjahr. Eventuell müssen Sie auch noch Rechnungen für Leistungen des alten Geschäftsjahres erstellen. Nachdem diese Arbeiten alle durchgeführt sind, kann erst die Periodenübernahme (siehe oben) für die Debitoren und Kreditoren durchgeführt werden.

**Hinweis:** Da das Mahnwesen aus der Rechnungsverwaltung gesteuert wird und nicht aus der offenen Postenliste, ist aus diesem Grund, im Gegensatz zu vielen anderen Buchhaltungsprogrammen, keine Eile geboten.

**<u>Parametrierung der Eröffnungskonten:</u>**

Sind finden im Modul Finanzbuchhaltung, unterer Modulreiter Finanzamt, oberer Modulreiter Buchungsparameter. Definieren Sie hier die Konten für die Eröffnungsbuchungen der Sachkonten, der Debitoren- und der Kreditorenkonten. Auch hier raten wir im Sinne der Transparenz je Kontoart ein eigenes Eröffnungskonto zu verwenden.

#### Die Periodenübernahme dauert ewig
Wird die Periodenübernahme durchgeführt, so wird diese durch den Klick auf den Knopf "Periodeübernahme ausführen" gestartet. Wurde die Periodenübernahme durchgeführt, so ändert sich dieser Knopf auf OK, was bedeutet, dass die Periodenübernahme gültig ausgeführt wurde.

![](periodenuebernahme_ausfuehren.JPG)   ![](periodenuebernahme_ok.JPG)

#### Können einzelne automatische Periodenübernahmebuchungen wieder storniert werden?
Ja. Bitte nutzen Sie dazu die Funktion Buchung Stornieren aus dem Menüpunkt Bearbeiten, welcher (nur) in den Sachkonten zur Verfügung steht. Gegebenenfalls springen Sie aus den Debitor-/Kreditorkonten mit dem GoTo Button auf die entsprechende Buchung im jeweiligen Sachkonto.

#### Bei der Periodenübernahme kommt der gesamte offene Betrag in der EB. Was ist da falsch?
Das ist so Absicht. D.h. um die Übersichtlichkeit insbesondere bei teilbezahlten Rechnungen zu erhöhen, werden Belege deren offene Posten NICHT 0,00 sind, in den Debitor-/Kreditor-Konten so übernommen, dass der original Rechnungsbetrag und die Teilzahlungen ersichtlich sind. Vom Saldo her gesehen ist das Ergebnis das selbe und der Gewinn an Transparenz ist enorm.

#### Wie kann das Kassenkonto eröffnet werden?
Selbstverständlich muss auch jedes Kassenkonto eröffnet werden. Da direkte manuelle Buchungen auf das/die Kassenkonten (Sachkonto, Bearbeiten, Manuelle Umbuchung) **nicht** zulässig sind, muss die Eröffnungsbuchung im unteren Modulreiter Kassenbuch durchgeführt werden.

#### Auf den Saldenlisten wird bei EB Wert inkonsistent angezeigt
#### Auf dem Kontoblatt wird Inkonsistent angedruckt
Damit wird signalisiert, dass auf dem jeweiligen Konto nach der Durchführung der Periodenübernahme noch Buchungen durchgeführt wurden. Führen Sie in diesem Falle die Periodenübernahme für das jeweilige Konto erneut durch, um eine entsprechende Konsistenz in den Buchungen zu erreichen.

<a name="Start_mit_EB_Buchungen"></a>

#### Wie ist beim Start der integrierten Finanzbuchhaltung mit der Periodenübernahme mit den Eröffnungsbuchungen vorzugehen?
Der Start der **Kieselstein ERP** Finanzbuchhaltung ist immer nur zum Beginn eines Geschäftsjahres sinnvoll.

Für die Eröffnungsbuchungen muss es so sein, dass die offene Postenlisten (Kreditoren / Debitoren) aus dem letzten Tag des alten Geschäftsjahres mit den EB-Buchungen (Eröffnungsbuchungen) des neuen Geschäftsjahres übereinstimmen. D.h. für den Start buchen Sie alle offenen Posten der Debitoren und Kreditoren aber auch der Bestandskonten nach. Wir raten dringend auf die Erzielung einer ausreichenden Transparenz zu achten. Lieber einige Buchungen mehr gemacht und dafür ganz klar sehen warum was wie gebucht wurde, als schnell eine einzige Zahl reingebucht und dann tagelang rumgesucht.
- D.h. es wird jeder Kreditor / Debitor eigenständig in seinem Konto gebucht (und nicht in die mitlaufenden Konten ein einzige Zahl reingebucht).
- Wenn Sie bei einem Kreditor nur sehr wenige (2-3) Rechnungen offen hatten und diese sehr zeitnah bezahlt wurden, kann dies als ein Saldo eingebucht werden. Sind es viele Rechnungen, die sich wenn möglich noch über einen längeren Zeitraum verteilen, so empfehlen wir jede Rechnung einzeln einzubuchen. Sie machen das ja nur einmal und nur beim Start der **Kieselstein ERP** Buchhaltung. Danach macht **Kieselstein ERP** das für Sie. Sie haben damit einen deutlichen Transparenzzugewinn.

**Hinweis:** Das Nachbuchen der Bestandskonten gilt nicht für die mitlaufenden Konten, die aus den Personenkonten (Debitoren, Kreditoren) automatisch gebildet werden.

#### Die Periodenübernahme aller Debitoren / Kreditorenkonten funktioniert nicht?
Wenn die Periodenübernahme aller Debitoren / Kreditorenkonten nicht funktioniert, die Übernahme einzelner Konten jedoch schon, so prüfen Sie bitte den Haken bei Automatische Eröffnungsbuchung in den Kopfdaten der Debitorenkonten. ![](Automatische_Eroeffnungsbuchung.gif) Nur für die Konten, für die diese Eigenschaft gesetzt ist, werden auch die Periodenübernahmen durchgeführt.

#### Kann man beim Kontoblatt die Debitoren- / Kreditorennamen mitdrucken?
Ja wählen Sie beim Druck des Kontoblattes auch GKT Bezeichnung drucken ![](GKT_Bezeichnung_mitdrucken.gif). Damit werden in einer zusätzlichen Zeile die Bezeichnungen des Gegenkontos angedruckt.

#### Verbindung mitlaufende Konten, offene Postenliste, Sachkonten, Eingangs-, Ausgangs-Rechnungen
Um Klarheit zu schaffen, wie sich die Eingangs- Ausgangs-Rechnungen in Bezug auf die durch den jeweiligen Geschäftsjahreswechsel ergebende Abgrenzung verhält, hier der Versuch einer teilweise grafischen Erklärung dieser Zusammenhänge.

Die Erklärung wird hier für die Eingangsrechnung beispielhaft dargestellt. Für Ausgangsrechnungen gilt dies in gleicher Weise, jedoch sind Soll - Haben entsprechend vertauscht.

- a.) Wird eine Eingangsrechnung abgespeichert, so erfolgt sofort die Verbuchung auf
    - das Aufwandskonto
    - das Vorsteuerkonto
    - das Kreditorenkonto und auf das aufgrund der Steuerkategorien hinterlegte Mitlaufende Konto.
- b.) Für die Erklärung des Verhaltens der offenen Postenliste sei nun eine Eingangsrechnung angenommen, die mit 28.12.2012 erfasst wird und die am 3.1.2013 bezahlt wird.<br>
Die Eingangsrechnung wird also am 28.12.2012 mit z.B. 1.000,- € gebucht

| Soll | Haben | im Geschäftsjahr 2012 |
| --: | --: | --- |
| 5000 | 31000 | Aufwand gegen Kreditor |
|      |  3301 | und auf das mitlaufende Konto |

Also ist der Saldo am Kreditorenkonto 1.000,- für das Geschäftsjahr 2012

- c.) Nun wird die Eingangsrechnung am 3.1.2013 bezahlt.<br>
Es wird gebucht

| Soll | Haben | im Geschäftsjahr 2013 |
| --: | --: | --- |
| 31000 |  2800 | Kreditor gegen Bank |
| 3301  |       | und auf das mitlaufende Konto |

Also ist der Saldo am Kreditorenkonto **minus** 1.000,- für das Geschäftsjahr 2013 und damit auch am mitlaufenden Konto, was, wie Sie sicherlich richtig erkannt haben, inhaltlich falsch wäre.

- d.) Wird nun aus der Eingangsrechnungsverwaltung das Journal der offenen Eingangsrechnungen ausgedruckt, so ist diese Rechnung bezahlt.<br><br>
Wird die offene Postenliste aus der Buchhaltung für 2013 ausgedruckt, so ist die Eingangsrechnung überzahlt.<br>
<br>
Warum ?<br>
<br>
Es fehlt die Eröffnungsbuchung, welche auch als Übertragsbuchung (vom einen in das andere Geschäftsjahr) bezeichnet werden könnte, die den offenen Saldo dieses einen Kreditorenkontos vom Vorjahr (2012) in das aktuelle Geschäftsjahr (2013) übernimmt.<br>
<br>
Dies kann z.B. durch die Periodenübernahme (Fibu, Bearbeiten, Periodenübernahme) oder bei neu aufgesetzten Buchhaltungsprogrammen durch manuelle Eröffnungsbuchungen erfolgen.<br>
<br>
Werden die Übertragsbuchungen, also die Eröffnungsbuchungen nicht gemacht, so wird die offene Postenliste und damit die Salden der mitlaufenden Konten immer falsch sein.

Warum ist dieses Verhalten so gegeben?

Ein Jahresabschluss, eine Bilanz, ist immer eine Momentbetrachtung eines Unternehmens zu einem bestimmten Stichtag. Daher sind die entsprechenden Abgrenzungsarbeiten vorzunehmen.

Die einfachste Art ist hier die Abgrenzung durch die offene Postenliste. Sie bedingt jedoch, dass Rechnungen die zum Stichtag noch nicht bezahlt waren in die neue Periode / in das neue Geschäftsjahr übernommen werden müssen. Kann, warum auch immer, eine automatische Übernahme nicht gemacht werden, so müssen die entsprechenden Konten manuell eröffnet werden. [Siehe oben](#Start_mit_EB_Buchungen).

#### Wie mit Überzahlungen umgehen?
Es passiert immer wieder, es arbeiten überall Menschen, dass es bei der Bezahlung von Rechnungen zu Überzahlungen kommt, aus Irrtum, Tippfehlern usw.

Da die generelle Forderung besteht, dass das Bankkonto immer exakt mit dem Kontoauszug der Bank übereinstimmen muss, müssen auch die Überzahlungen gebucht werden.

Die unserer Meinung nach sicherste Möglichkeit mit Überzahlungen umzugehen ist wie folgt:
- a.) Sie buchen den richtigen Betrag in der Zahlung der Rechnung (ob Ein- oder Ausgangsrechnung ist vom Thema her egal)
- b.) den überzahlten Betrag buchen Sie Bank gegen ein Durchläuferkonto
- c.)
    1. wird der überzahlte Betrag mit einer weiteren Rechnung ausgeglichen, so buchen Sie die Zahlung der Rechnung gegen dieses Durchläuferkonto, welches dafür auch als [Bankverbindung](index.htm#Eigene Bankverbindungen) definiert sein muss.
    2. wird der überzahlte Betrag zurücküberwiesen, so buchen Sie Durchläufer gegen Bank

**Wichtig:** Von der Idee her ist das Durchläuferkonto im Saldo immer 0,00 mit Ausnahme der bewusst "unstimmigen" Zahlungen.

Es spiegelt dieses Konto auch wider, was Sie Ihren Kunden noch schuldig sind, bzw. was Sie von Ihren Lieferanten noch erhalten.

Eine Alternative ist, den zu hohen Betrag direkt in der Zahlung der Rechnung zu erfassen und die Rechnung als Teilbezahlt stehen zu lassen.

Wird dann die Überzahlung zurücküberwiesen, so buchen Sie in der Zahlung der Rechnung den Betrag der Überweisung als negativen Zahlbetrag.

Dies hat den Vorteil, dass am Personenkonto (Debitor bzw. Kreditor) der offene Saldo ersichtlich ist.

Wird die Überzahlung mit weiteren Rechnungen ausgeglichen, so muss dieser Ausgleich wiederum über das Durchläuferkonto erfolgen.

#### Es müssen die Salden der Vorsteuerkonto null sein. Was wenn dem nicht so ist?
Wenn die aktuelle UVA ausgedruckt ist und somit die automatische Weiterbuchung der Vorsteuer, Umsatzsteuer auf das Zahllastkonto erfolgt ist, müssen die Vorsteuer bzw. Umsatzsteuerkonten einen Saldo von 0,00 ausweisen. Ist dem nicht der Fall, bedeutet dies, dass nach der Durchführung der UVA die Sperre durch die UVA Verprobung wieder aufgehoben wurde und nachträglich Belege mit MwSt eingebucht / geändert wurden. Drucken Sie die UVA (mit dem echten Druck) für den jeweiligen Zeitraum erneut aus.<br>
Achten Sie vor allem bei der letzten UVA Ihres Geschäftsjahres auf eine entsprechend abgestimmte Jahres-UVA.

#### Finanzamtssammelbuchungen<a name="Finanzamtsammelbuchungen"></a>
Neben den üblichen Buchungsarbeiten auf die einzelnen Aufwands und Erlöskonten sind monatliche Sammelbuchungen erforderlich. Das Verhalten der Sammelbuchungen wird über die Kontoart des jeweiligen Kontos gesteuert. Die Sammelbuchungen selbst werden vor jeder UVA-Auswertung durchgeführt, falls die UVA für diese Periode noch nicht verprobt ist. Das bedeutet auch, dass es einen großen Unterschied darstellt, ob Sie die UVA nur in die große Druckvorschau drucken, damit wird keine UVA Verprobung gemacht, oder ob Sie aus dem Druck-Vorschau-Fenster direkt die UVA ausdrucken, da bei diesem Ausdruck die gewählte Periode verprobt wird.

Die Buchungen selbst sind als Automatikbuchungen gekennzeichnet und können so vom **Kieselstein ERP** eindeutig identifiziert werden.

Da bei der UVA die Sammelbuchungen durchgeführt werden, ist es zwingend erforderlich, dass die erforderlichen Kontoarten definiert sind. Ist dies nicht der Fall, werden entsprechende Fehlermeldungen ausgegeben.

Einstellungen der Kontenarten für die automatischen Finanzamtsübertragsbuchungen: Es werden für die Sammelbuchungen folgende Kontoarten verwendet:

| Kontoart | Beschreibung | Typ |
| --- |  --- |  --- |
| Ust Konto | Umsatzsteuerkonto, also ein Konto das die Umsatzsteuerbuchungen z.B. 20% beinhaltet | Steuerkonten |
| Vst Konto | Vorsteuerkonto, also ein Konto das die Vorsteuerbuchungen z.B. 20% beinhaltet. | Steuerkonten |
| Ust Sammelkonto | Umsatzsteuer SammelkontoHier werden alle Umsatzsteuerkonten aufsaldiert | Steuer-Sammelkonten |
| Vst Sammelkonto | Vorsteuer SammelkontoHier werden alle Vorsteuerkonten aufsaldiert | Steuer-Sammelkonten |
| UST- oder Erwerbssteuerkonto | Hier werden alle Steuer-Sammelkonten aufsaldiert | Finanzamt Steuer-Verrechnungskonto |
| Abgabenkonto | Konten in denen Abgaben enthalten sind. Z.B. Lohnsteuer, DB, DZ | Abgabenkonto |
| FA Zahllastkonto | Das Finanzamtzahllastkonto |  |

Derzeit ist folgender Ablauf für die Finanzamtssammelbuchungen programmiert:<br>
- Es werden die Periodensalden aller Konten der Kontoart Ust Konto auf das Ust Sammelkonto saldiert.
- Es werden die Periodensalden aller Konten der Kontoart Vst Konto auf das Vst Sammelkonto saldiert.
- Es werden die Periodensalden aller Konten der Kontoart Ust Sammelkonto auf das UST- oder Erwerbssteuerkonto saldiert.
- Es werden die Periodensalden aller Konten der Kontoart Vst Sammelkonto auf das UST- oder Erwerbssteuerkonto saldiert.
- Es werden die Periodensalden des UST- oder Erwerbssteuerkonto auf das FA Zahllastkonto saldiert.
- Es werden die Periodensalden aller Konten der Kontoart Abgabenkonto auf das FA Zahllastkonto saldiert.

Oder in anderer Form:
- Ust Konten -> Ust Sammelkonto
- Vst Konten -> Vst Sammelkonto
- Ust Sammelkonto + Vst Sammelkonto -> UST- oder Erwerbssteuerkonto
- UST- oder Erwerbssteuerkonto + alle Abgabenkonten -> FA Zahllastkonto

[Siehe dazu auch](#Zu beachten bei Kontendefinitionen richtigstellen).

#### Können diese automatischen Buchungen auch verhindert werden?
Wenn Sie manuell die UVA-Buchungen vornehmen wollen, so definieren Sie die Kontoarten nur soweit Sie diese automatisch bebucht haben wollen. Das Ende der automatischen Buchung ist somit definierbar in dem weiterführende Konten nicht eingetragen werden. Wenn Sie also kein FA Zahllastkonto definieren, so erfolgt die Buchung nur bis zum UST-oder Erwerbsteuerkonto, wenn kein UST- oder Erwerbssteuerkonto definiert ist, nur bis zu den Ust/Vst Sammelkonten.Deaktivieren der automatischen Sammelbuchung auf Ust und Vst Sammelkonten indem Sie auch hier die Kontoart auf z.B. nicht definiert setzen. Ab nun werden keine automatischen Buchungen der einzelnen Steuerkonti auf Sammelkonten durchgeführt. Bitte achten Sie auf die betroffenen Konten bei der laufenden Verbuchung.

#### Wie kann ich diese Sammelbuchungen manuell machen?
In der Regel ist es so, dass, wenn die Buchungen nicht automatisch gemacht werden, werden diese manuell per Hand nachgebucht. Das hat sicherlich den Vorteil, dass die abgeführten / angegebenen Steuern unter Ihrer Kontrolle sind. Dafür gehen Sie wie folgt vor:
Stellen Sie den Parameter FINANZ_SAMMELBUCHUNG_MANUELL auf 1\. Somit werden bei der UVA Verprobung keine automatischen Sammelbuchungen durchgeführt. Zusätzlich steht nun die Buchungsart "Mwst-Abschluss" zur Verfügung. Buchen Sie die Mehrwertsteuerbuchungen mit dieser Buchungsart. Damit werden diese immer zum Ende der Periode / des Monates angezeigt und, so wie die automatischen Buchungen auch, in Türkis dargestellt.<br>
**Wichtig:** Mwst-Abschluss Buchungen können nicht geändert werden. Für Änderungen löschen und neue Buchung erstellen.<br>
**Info:** Die MwSt-Abschluss Buchungen müssen immer zum Letzten des Monats sein.

#### Bei der Erstellung der UVA erscheinen Fehlermeldungen
Diese Meldungen dienen Ihrer Sicherheit.

Auch aus der Forderung der Finanzamtssammelbuchungen ergeben sich verschiedenste Meldungen, wenn oben beschriebene Bedingungen nicht gegeben sind. Erscheint eine Fehlermeldung, so klicken Sie bitte auf Detail und schieben den Thumb ganz nach oben. Hier sehen Sie den eigentlichen Hintergrund der Meldung.

Maßnahmen bei den Meldungen sind:

| Meldung | Maßnahme |
| --- |  --- |
| Kein Sammelkonto für Kontoart Vst Sammelkonto | Definieren Sie am Vorsteuer-Sammelkonto die Kontoart VST SammelkontoBeachten Sie, dass selbstverständlich auch die VST-Konten mit der Vorsteuer definiert sein müssen. |
| Kein Sammelkonto für Kontoart UST- oder Erwerbssteuerkonto | Definieren Sie das Konto mit der Kontoart UST- oder Erwerbssteuerkonto |
| Kein Sammelkonto für Kontoart FA Zahllastkonto | Definieren Sie das Zahllastkonto |
| Rechnung jj/nnnnnnn, Buchung, 2012-12-06Ungültiges Konto 2000 in Buchung auf Steuerkonto 3602 | Bedeutet: Im Ust/Vorsteuerkonto 3602 sind Buchungen mit Gegenkonto 2000 (mitlaufendes Konto) enthalten.Da das Sachkonto 3602 jedoch als .... (hier gibt es  definiert ist, darf die Buchung nicht durchgeführt werden. Korrigieren Sie die Definition der Steuerkategorien. |
| FB2013/2, Umbuchung, 2013-02-28Ungültiges Konto 3501 in Buchung auf Steuerkonto 3540 | Diese Meldung bedeutet, dass die Struktur Ihrer Konten / Sammelkonten etwas verworren sind.Stellen Sie die Strukturen richtig, beachten Sie dabei auch die in den Finanzamtssteuerkategorien hinterlegten Definitionen.Zur Korrektur können diese Buchungen auch manuell gelöscht werden. |

**Info:** Die Meldung beginnt immer mit ... ExceptionLP und dann in der nächsten Zeile java.lang.Exception: und dann der eigentliche Meldungstext.

**Hinweis:** Wenn Sie Kontenarten ändern und keine automatischen Buchungen erhalten, so ist davon auszugehen, dass die UVA-Verprobung für diesen Zeitraum schon gemacht wurde. Prüfen Sie dies, bzw. nehmen Sie die UVA Verprobung, im Menü, Bearbeiten, letzte UVA Verprobung zurücknehmen, zurück.

#### Wie findet man am einfachsten UVA-Fehler?
Wie bereits mehrfach angeführt raten wir, auch aus Gründen der menschlichen Fehler, vor der eigentlichen UVA das (Ausgangs-)Rechnungsjournal, das Eingangsrechnungs-Kontierungsjournal und gegebenenfalls das Zusatzkosten-Kontierungsjournal für den jeweiligen Zeitraum und immer für alle Rechnungen durchzusehen und auf richtige Verbuchung zu prüfen. Die in den jeweiligen Journalen angegebenen Salden sind die Basis für die Plausibilitätsprüfung Ihrer UVA. D.h. die Umsätze des Ausgangsrechnungsjournals müssen mit einem maximalen geringeren Betrag von 1-3%, welche den Skontoaufwand darstellt, mit dem Journal übereinstimmen. Ist dem nicht so, machen Sie sich bitte auf die Suche. Es ist Ihr Unternehmen und nicht das des Steuerberaters. <br>Gleiches gilt analog für die Vorsteuer und natürlich für die innergemeinschaftlichen (IG) Erwerbe. Beachten Sie die Besonderheit, dass es sich bei den IG Erwerben zwar um Vorsteuern handelt, aber da auch die Einfuhrumsatzsteuer (gedanklich) mit enthalten ist, müssen auch die Umsätze mit angeführt werden.<br>
Stimmen die Salden nicht über, so hilft in der Regel der Ausdruck der Saldenliste des Zeitraumes in der Formularvariante Saldenliste quer. Hier finden Sie auf der letzten Seite eine Zusammenfassung aller Salden die UVA betreffend gegliedert nach UVA Arten. Damit sollte es sehr rasch klar sein, warum welcher Betrag auf der UVA angedruckt wird.<br>
Beachten Sie bitte auch dass die Kontendefinitionen in Ihrer Verantwortung liegen.

**Info:**<br>
Da der Saldo der Vorsteuerkonten wegen der UVA-Sammelbuchungen IMMER 0,00 ist, scheinen diese nur auf, wenn noch KEINE Ust-Sammelbuchungen durchgeführt sind. D.h. wenn die Saldenliste-quer **vor** der UVA ausgedruckt wird, sind die Vorsteuerbuchungen noch sichtbar, danach nicht mehr. Gegebenenfalls löschen Sie diese Automatikbuchung
![](integrierte_Finanzbuchhaltung_VST_Sammelbuchung_loeschen.gif)<br>
und führen danach die UVA-Verprobung erneut durch.

#### Wie kann man die steuerlich richtige / falsche Buchung am schnellsten finden?
Ergänzend zu obigem,<br>
Bitte beachten Sie, dass bei der UVA der aus dem UVA Formular errechnete Zahlbetrag und der Zahlbetrag laut Zahllastkonto, bis auf geringe Rundungsdifferenzen übereinstimmen muss.
Kommt es hier zu nennenswerten Abweichungen, müssen diese gesucht und bereinigt werden.
Eine Ursache ist meistens, dass von **Kieselstein ERP** zwar Hinweise ausgegeben werden (Buchung auf Konto mit Ust, aber ohne Ust, oder umgekehrt) dies aber vom Anwender ignoriert wird.<br>
Spätestens zur Jahres-UVA muss dies korrigiert werden.<br>
Hier hat sich als praktisch herausgestellt, dass man einfach die Kontoblätter aller Erlöskonten in die Vorschau "druckt". Die Kontoblätter sind so gestaltet, dass unstimmige Definitionen aufgezeigt werden. Gerade bei der unterjährigen Mehrwertsteuerumstellung wie im Corona Jahr 2020 kann es hier schon zu Buchungsfehlern gekommen sein.
![](Unstimmige_UST_Saetze.jpg)<br>
D.h. es werden unstimmige Umsatzsteuersätze hinterlegt dargestellt. Bitte prüfen Sie diese.

#### Wie wirken Anzahlungsrechnungen in der UVA?
Bei der Berücksichtigung der Anzahlungsrechnungen haben wir uns dazu durchgerungen, dass diese, von den Terminen her wie normale Rechnungen betrachtet werden.
Die gesetzliche Regelung in Österreich (und in D? und wie in CH/LI?) ist so, dass bei Anzahlungsrechnungen, sowohl Eingangs- als auch Ausgangsrechnungen das Zahlungsdatum ausschlaggebend für die Ermittlung der Umsatzsteuer / Vorsteuer ist. Unter der Annahme, dass bei Anzahlungs- / Schlussrechnungen die beteiligten Partner beiderseits daran interessiert sind, dass die Zahlung rasch fließt, nehmen wir an, dass diese im gleichen Monat wie die Rechnung ist und somit im Endeffekt in der UVA keine Auswirkung hat. Daher wird in **Kieselstein ERP** auch bei den Anzahlungsrechnungen das Rechnungsdatum als Basis für die Ermittlung der UVA verwendet.

Musterdefinitionen für

**Österreich**

![](Definition_Sammelbuchungen_AT.gif)

**Deutschland SKR03**

**Deutschland SKR04**

**Schweiz und Liechtenstein**

![](Definition_Sammelbuchungen_CH.gif)

Kontenarten

In jeder Buchhaltung gibt es grundsätzlich verschiedene Kontenarten. Hier eine kompakte Zusammenfassung, die wir als sehr hilfreich empfunden haben:

| Bezeichnung | Beschreibung | Nummernkreise |||
| --- |  --- |  --- | --- | --- |
|  |  | A | D | CH |
| aktive Bestandskonten | Bestandskonto bedeutet, dass Buchungen auf diesen Konten NICHT erfolgwirksam sind, falls nicht ein Erfolgskonto das Gegenkonto ist. Das aktiv bedeutet, dass eine Erhöhung Ihres Guthabens bei der Bank, Ihres Kassenbestands oder der Zukauf von Anlagevermögen im Soll steht.Jeder Abgang steht daher im Haben. | 0-2 |  |  |
| passive Bestandskonten | Auch hier ist eine Buchung erfolgsneutral. Das passiv bedeutet jetzt allerdings, dass jede Erhöhung im Haben zu buchen ist, jede Verminderung im Soll. Klassisches Beispiel wäre der "Aufbau von Verbindlichkeiten", d.h. ins Haben, oder Zunahme von Eigenkapital. | 3, 9 |  |  |
| Ertragskonten | Jede Erhöhung ist im Haben, jede Verminderung im Soll zu buchen.Nicht verwirren lassen sollte man sich davon, dass es in der 8er Klasse viele Konten gibt die eigentlich nur zur Verbuchung von Aufwänden da sind. Deshalb ist es ein Ertrags, aber auch Aufwandskonto. | 4, 8 |  |  |
| Aufwandskonten | D.h. steigt der Aufwand (Löhne, Materialaufwand) ist ins Soll zu buchen. Sinkt der Aufwand muss ins Haben gebucht werden. | 5-8 |  |  |
| Erfolgskonten | Erfolgskonten Abschluss gegen GuVBestandskonten Abschluss gegen Schlussbilanzkonto (was üblicherweise nicht mehr gemacht wird) | 5-8 |  |  |
| Gewinn- und Verlust-Rechnung (GuV) | In der GuV bilden dann 5-7 den Betriebserfolg, 8 den Finanzerfolg womit wir dann beim EGT (= Ergebnis der gewöhnlicher Geschäftstätigkeit) stehen |  |  |  |

Reverse Charge

Unser aktueller Wissenstand (August 2013) bezüglich Reverse Charge für Österreich, Inland ist wie folgt<br>
Der aktuelle Stand der Ansicht der Finanz ist (das kann sich bitte monatlich ändern)<br>
a.) Wenn irgendwo was von Mehrwertsteuer drauf steht, kann passieren, dass das so gewertet wird, dass da Mehrwertsteuer enthalten ist und dass diese dann, obwohl nur als MwSt Info angedruckt, abgeführt werden muss.<br>
b.) Auf der UVA sind nur die Umsätze betreffend Reverse Charge getrennt anzuführen.<br>
c.) im Gesetz steht nichts von: Die UST muss "doppelt gebucht" werden (so wie die Einfuhrumsatzsteuer).<br>

Das bedeutet:<br>
Inlands Reverse Charge Rechnungen sind als steuerfrei zu erfassen. Sowohl Eingangs-, als auch Ausgangs-Rechnungen.<br>
Die Reverse Charge Umsätze werden auf ein eigenes Konto gebucht und können damit auch auf der UVA als eigener Posten angeführt werden.<br>

Info zu den Eingangsrechnungen:<br>
IG-Erwerb ist der Innergemeinschaftliche Erwerb, das geht nur aus dem EU-Mitgliedsländern die nicht im Inland (Österreich) sind. Inlands-Rechnungen sind immer mit MwSt auszustellen, ausgenommen (und das auch nur in Österreich) wenn diese nach Reverse Charge zu behandeln sind.

**<a name="Summe"></a>Summe der markierten Buchungszeilen**

Im Modul Finanzbuchhaltung haben Sie bei Konten im Reiter Buchungen die Möglichkeit einzelne Zeilen zu markieren und deren Summe anzuzeigen.

Besonders für Transparenz und Fehlersuche ist die Funktion hilfreich.

![](Fibu_summe.PNG)

Mit Klick auf eine Buchungszeile markieren Sie diese.

Halten Sie die Strg-Taste gedrückt um weitere Zeilen zu markieren.

Im unteren Bereich des Fensters erscheint nun eine gelbe Zeile, die die Summer der markierten Zeilen ausgibt.

Sollten Sie mehrere Zeilen, die sich hintereinander befinden, markieren wollen, so gehen Sie wie folgt vor:

Markieren Sie die erste Zeile und halten die Shift ("Groß/Kleinschreib")-Taste gedrückt, nun klicken Sie auf die letzte zu markierende Zeile.

Um die gesamte Markierung wieder aufzuheben klicken Sie ohne Tastendruck zum Beispiel auf eine andere Zeile. 

Um nur eine Zeile wieder aus der Markierung zu nehmen, halten Sie die Strg-Taste gedrückt und klicken auf die Zeile bei der Sie die Markierung aufheben möchten.

#### Wie kann man erkennen, dass manche Aufwandskonten nicht VST berechtigt sind?
Wenn Sie ein Sachkonto neu anlegen, können Sie die Information ohne Ust/Vst anhaken. 

Wird dieses Konto nun in einer Eingangsrechnung verwendet und ein Steuersatz ungleich 0% gewählt, erfolgt ein entsprechender Hinweis.

#### Wie wird der Haft(ungs)rücklass in **Kieselstein ERP** abgebildet?
Der Haftungsrücklass stellt eine Sicherheit für den Auftraggeber in der Baubranche dar. Eine Vereinbarung regelt die Höhe, Dauer und den Umfang des Haftrücklass. Üblich sind 2-3% des Gesamtbetrags der Schlussrechnung. Diese werden vom Kunden einbehalten für etwaige Forderungen auf Grund von Reklamation, Gewährleistung, etc. gegenüber des Bauunternehmens. Um diesen Fall in **Kieselstein ERP** abzubilden, legen Sie ein neues Sachkonto als Bankkonto mit dem Namen "Haftrücklass" an. Wird nun eine Schlussrechnung bezahlt, so buchen Sie mit einer Umbuchung den vereinbarten Betrag (Haftungsrücklass) auf dieses Konto. Somit wird die jeweilige Rechnung als bezahlt verbucht und der offene Betrag bleibt am Haftungsrücklass Konto. Bitte bedenken Sie, dass dieses Konto von Ihnen überwacht werden muss, um die ausständige Zahlung nach Ablauf der vereinbarten Frist zu erhalten.

#### Wie können Schadensfälle verbucht werden.
Wenn Sie aufgrund z.B. von Schäden, die bei Ihrem Kunden entstanden sind, diesem eine Rechnung ganz oder teilweise gutschreiben müssen, so empfehlen wir, diese über eine Gutschrift darzustellen. Dies hat auch den Vorteil, dass im ERP Teil die Umsätze um den Schaden sinken.
D.h. die Kette geht folgendermaßen:
- Legen Sie ein Aufwandskonto für die Verbuchung des Schadens an, z.B. 7800<br>
    Achten Sie darauf ob auf dieses Konto Buchungen mit Umsatzsteuer erfolgen. D.h. für Inlands-Schäden muss dieses Konto in der UVA Art auf Umsatz Inland Normalsteuer gestellt sein
    ![](Schadensfall1.jpg)

-   Legen Sie unter Artikel, Grunddaten, Artikelgruppen, eine Artikelgruppe Schadensfälle an und weisen Sie dieser obiges Konto zu

-   Legen Sie einen Schadensartikel an und geben Sie diesem die Artikelgruppe Schadensfälle
    Beachten Sie, dass dieser Artikel nicht lagerbewirtschaftet ist.

-   Erstellen Sie die Gutschrift, mit oder ohne Rechnungsbezug und führen Sie hier den Schadensartikel an
    Üblicherweise werden Sie Ihrem Kunden der guten Ordnung halber die Gutschrift zusenden.

-   Gleichen Sie nun die ursprüngliche Rechnung mit der Zahlungsart Gutschrift gegen die angelegte Gutschrift aus.
    Achten Sie auf Gutschrift erledigt (je nach tatsächlichem Erledigungsstatus der Rechnung / Gutschrift)

-   Bei der UVA kommt:
    ![](Schadensfall2.jpg)<br>
    Ursache: Es fehlt die Definition der UVA Art. Da in dieser Buchung die Umsatzsteuer enthalten ist, muss natürlich dieses (Aufwands-)Konto in der UVA berücksichtigt werden. Damit erhalten Sie die zu viel bezahlte Umsatzsteuer wieder zurück.

<a name="Zu beachten bei Kontendefinitionen richtigstellen"></a>

#### Was ist zu tun, wenn Konten / Buchungsdefinitionen nicht stimmen / korrigiert werden mussten?
Gerade zum Beginn kann es vorkommen, dass die Buchungseinstellungen unvollständig und leider auch manchmal falsch gemacht werden. Manche Menschen lesen keine Handbücher, sondern probieren halt mal rum. D.h. man stellt dann zu einem Zeitpunkt fest, dass, aufgrund der falschen Definitionen, falsche Buchungen gemacht wurden. Daher sind die Buchungsdefinitionen richtigzustellen.

{{% alert title="ACHTUNG:" color="primary" %}}
**Werden Buchungsdefinitionen geändert, so werden damit NICHT automatisch alle davon betroffenen Buchungen aufgerollt.**
{{% /alert %}}

D.h. was ist alles zu tun am die geänderten Buchungsdefinitionen auch für ev. bereits getätigte Buchungen zu übernehmen.<br>
Vor allem davon betroffen sind die automatisch durchgeführten Buchungen, welche auf diese Regeln, hier vor allem die Definitionen der Steuerkategorien, zugreifen.<br>
Grundsätzlich sind hier zwei Dinge zu unterscheiden:
- a.) kommt von **Kieselstein ERP** eine Fehlermeldung, dass für den Fall xyz (IG Lieferung mit Revese Charge an Empfänger mit UID Nummer) keine Steuerkategorie Definition gegeben ist, so tragen Sie diese nach und versuchen erneut die Rechnung auszudrucken, was bei richtiger Hinterlegung entsprechend funktioniert. Damit ist für die Zukunft dieser Fall definiert.
- b.) man stellt fest, dass verschiedene Definitionen einfach falsch sind, nicht nur dass auf das falsche Konto gebucht wurde, sondern dass die Regel falsch definiert war.<br>
Auch hier ist als erster Schritt natürlich die Regel richtigzustellen. In der Folge sind die Belegübernahmen erneut durchzuführen. Hier können einerseits die einzelnen Beleg (im jeweiligen Modul, Bearbeiten, Belegübernahme) oder auch die Belege einer Periode (FiBu, Bearbeiten, Belegübernahme) erneut übernommen werden. Damit werden die bisherigen Buchungen des Beleges inkl. Zahlungsbuchngen storniert und dann anhand der nun geltenden Regeln neu eingebucht.
- c.) Ein weiterer Fall ist, dass die Definition der Kontenart falsch ist, beachten Sie dazu den Unterschied zwischen Vorsteuerkonto und Vorsteuer behaftetem Konto! In **Kieselstein ERP** kann nur das eigentliche Vorsteuerkonto definiert werden.

Sind nun derartige Definitionsfehler gegeben, so wird, je nach Einstellung der UVA-Verprobungsbuchungen, auch automatische Steuer-Übertragsbuchungen gemacht. Dies werden immer bei Ausdruck der UVA und wenn Verprobung angehakt ist durchgeführt. D.h. wurde nun die Kontoart richtiggestellt so müssen die UVA-Verprobungen zur Richtigstellung der Automatischen Verprobungsbuchungen (deren Belege beginnen mit FB) für den jeweiligen Zeitraum neu durchgeführt werden. Anderenfalls kann es vorkommen, dass alte automatische Verprobungsbuchungen stehen bleiben und insbesondere bei der Datenübertragung an Ihren Steuerberater, die immer ohne diesen Automatikbuchungen durchgeführt werden sollten, so entsprechenden Differenzen in den Saldenlisten kommt.

Wie können solche falschen Definitionen entstehen?<br>
Es beginnt bei der Definition der Artikelgruppe zum Artikel, geht über die Länderartübersetzung bis hin zur Steuerkategorie. Auch ändert sich manchmal die Betrachtungsweise, man will andere Betrachtungen auch in der Finanzbuchhaltung haben usw..

Denken Sie daran, dass nach der Korrektur eventuell durchgeführt Datenexporte erneut vorzunehmen sind.

#### Können die FiBu-Daten exportiert werden?
Ja. Es stehen dafür drei Varianten zur Verfügung.
- a.) Datev
- b.) HV RAW
- c.) RZL-CSV

Der Export der Daten basiert grundsätzlich auf dem Buchungsjournal. Um also Daten aus Ihrer **Kieselstein ERP** Buchhaltung zu exportieren starten sie das Finanzbuchhaltungsmodul, wählen den unteren Modulreiter Buchungsjournal. Nun klicken Sie im Menü auf Buchungsjournal und wählen Export<br>
![](BuchungsjournalExport1.jpg)

Im nun erscheinenden Dialog wählen Sie das Exportformat aus.<br>
![](FibuExport.jpg)<br>
Geben Sie nun den Zeitraum an und welche Buchungen exportiert werden sollten. Beachten Sie dabei die steuerlichen Vorschriften. Der Zeitraum bezieht sich auf das Buchungsdatum.<br>
Je nach Art des Exports müssen die Automatischen Eröffnungsbuchungen (aus der Periodenübernahme) die Automatikbuchungen (z.B. UVA Sammelbuchungen) und die Manuellen Eröffnungsbuchungen mit oder nicht mit übergeben werden. Das bedeutet, sind diese Daten in der Zielbuchhaltung, z.B. aus den Vorjahren, schon vorhanden, so dürfen diese nicht mit übergeben werden.<br>
Beachten Sie bitte auch, dass sich Exportzeiträume nicht überschneiden sollten. Bei der derzeitigen Implementierung des Exports wurde davon ausgegangen, dass in einem Zeitraum in dem bereits Buchungen exportiert wurden, keine Veränderungen mehr gebucht werden. Eine Verriegleung dafür wurde nicht implementiert. Siehe dazu bitte gegebenenfalls [UVA-Verprobungszeitraum](#UVA Verprobung).<br>
Sollten Sie eine Überprüfung Ihrer Buchungen z.B. durch Ihre Steuerberater / Wirtschaftstreuhänder wünschen, so wird dies meist so realisiert, dass die exportierten Daten in das Buchhaltungssystem Ihres Steuerberaters eingespielt wird und dieser dann die importierten Daten verwirft.

Mit Bezeichnung geben Sie noch den grundsätzlichen Dateinamen an und klicken danach auf Exportieren. Den nun erscheinenden speichern Dialog bestätigen Sie entsprechend und warten nun die Dauer des Exportes ab.

**WICHTIG:** Klären Sie bitte vor dem ersten Export bereits ab, welche Daten Ihr externer Steuerberater / Ihre externe Steuerberaterin erwartet. Manche sind der Meinung, dass die Eröffnungsbuchungen aus Ihrem eigenen System übernommen werden müssen, was für uns richtiger ist (denn der Steuerberater hat die Gesamtbuchhaltung und damit auch die Verantwortung dafür), manche sind aber der gegenteiligen Meinung, d.h. es werden auch die Eröffnungsbuchungen aus **Kieselstein ERP** Übernommen. Wenn dies der Fall ist, so müssen beim Export die automatischen und die manuellen Eröffnungsbuchungen angehakt und somit mit übergeben werden.

Die Übergabe der Automatikbuchungen und die Übergabe auch der stornierten Buchungssätze ist vor allem für die Prüfung durch die Finanzbehörde gedacht. Hier ist natürlich wichtig zu sehen, werden Buchungsfehler gemacht bzw. wie war das System eingestellt und wurde es berichtigt.

Bitte beachten Sie auch, dass manche DATEV-Systeme so eingestellt sind, dass diese automatisch die MwSt buchen. Das bedeutet dass diese Einstellung auch im **Kieselstein ERP** in den Kontendefinitionen gegeben sein muss oder zumindest, dass alle Buchungen mit MwSt durchgeführt werden müssen.

**Ergänzender Hinweis zu Eröffnungsbuchungen:**<br>
In **Kieselstein ERP** werden die Eröffnungsbuchungen anhand der Buchungsart (bei der manuellen Buchung) identifiziert. Um nun für den Export der Daten sicher zu sein, dass die auf den Eröffnungskonten gebuchten Konten tatsächlich auch von der Buchungsart EB, Eröffnungsbuchung sind, prüfen Sie bitte vorher die Kontoblätter der definierten Eröffnungskonten. Hier sollten / müssen alle Buchungen Eröffnungsbuchungen sein.<br>
![](EBs_pruefen_fuer_Export.gif)<br>
So bedeuten die beiden EB EB in der ersten Zeile, dass diese Buchung automatisch durch die Periodenübernahme des Kontos übernommen wurde.<br>
Die zweite Zeile mit dem EB nur in der Spalte Art bedeutet, dass diese Buchung manuell gemacht wurde, wobei in diesem Falle Steuerverbindlichkeiten manuell übernommen wurden.

#### Unterschied gebucht am oder Buchungsdatum
In manchen Anwendungen sollten alle Buchungen in einem parallel DATEV System nachgezogen werden. Das bedeutet, dass alle Buchungen auch Änderungen / Stornos usw. zusätzlich im anderen System gemacht / nachgezogen werden müssen. Verwenden Sie dafür das Gebucht am und haken Sie bitte plus Storniert an, damit auch die Stornobuchungen im anderen System nachgezogen werden. Bitte achten Sie darauf, dass keine überlappenden Exportdatum verwendet werden. D.h. die Idee ist, dass immer die Daten, z.B. der letzten Woche, exportiert werden.

#### Welche Daten sind für den DATEV-Export einzustellen
Damit der Export zu den DATEV-Daten Ihres Steuerberaters funktioniert, müssen folgende Punkte gegeben sein.
Dass die Sachkonten, Debitoren, Kreditoren Nummern und deren Bedeutungen komplett ident sind ist Grundvoraussetzung.<br>
Zusätzlich müssen für den Export bzw. für Import bei Ihrem Steuerberater noch folgende Parameter (System) definiert werden:
- Datev-Berater Nr.: Parameter EXPORT_DATEV_BERATER
- Datev-Mandant Nr.: Parameter EXPORT_DATEV_MANDANT
- ExportMitlaufendes Konto Nr.: Parameter EXPORT_DATEV_MITLAUFENDES_KONTO (üblicherweise 9999)
- Umsatzsteuerschlüssel für den reduzierten und den normal-Mehwertsteuersatz. Diese sind im System, Mandant, MwSt, FIBU-MWSTCode zu definieren. Bitte beachten Sie, dass hier nur nummerische Werte (Zahlen) eingeben werden dürfen.

##### Einstellung des Importes in Datev
Der Importer in Datev stellt viele Varianten für den Datenimport zur Verfügung. Das hat den Vorteil dass unterschiedlichste Formate importiert werden können. Der Nachteil ist, dass man das **Kieselstein ERP** Format einstellen muss. Es ist sicherlich von Vorteil, wenn Sie / Ihr Steuerberater sich für den **Kieselstein ERP** Import eine entsprechende Importdefinition erstellt.<br>
Laut unseren Informationen ist der Importer wie folgt einzustellen:<br>
Wählen Sie für die Einstellungen bitte: Bestand, Importieren, ASCII-Daten importieren.
![](Datev_Import_Einstellungen.jpg)

Ob Ihre Einstellungen richtig sind sehen Sie in der Importvorschau, die im unteren Bereich eingeblendet wird.<br>
![](Datev_Import_Einstellungen2.jpg)

**Hinweis:** Gegebenenfalls sollte anstelle des Lieferantennamens die erfasste Lieferantenrechnungsnummer übergeben werden. Stellen Sie dazu bitte den Parameter LF_RE_NR_BUCHUNGSTEXT auf 1\.

#### Welche Daten sind für den RZL-Export einzustellen
Aufbauend auf dem Datev-CSV Export steht auch eine leicht abgewandelte Version für die Übergabe nach RZL zur Verfügung.<br>
Wählen Sie dazu den RZL-Export. Die Festlegungen für die Datev-Parameter gelten analog wie oben bei Datev beschrieben.<br>
Zusätzlich wird der Dateiname fix mit EXTF_ begonnen, damit RZL das Datenformat richtig erkennt.<br>
Im Unterschied zum Datev Format wird das Datum mit TTMM erzeugt.<br>
Der Parameter datev-berater nr muss für den Export zwar eingestellt werden, ist aber für den Import in RZL nicht relevant, da von RZL nicht unterstützt.<br>
Der Parameter datev mandant nr ist auf die Nummer des Mandanten aus dem RZL einzustellen.<br>
Der Parameter export mitlaufendes konto nr sollte auf das allgemeine Fehlerkonto, in der Regel 9999, eingestellt werden.<br>
Die FIBU-MWSTCode (System, Mandant, MwSt) sollten gleichlautend wie die Steuersätze, z.B. 10 für 10% usw. eingestellt werden. -> Bitte stimmen Sie dies mit Ihrem Steuerberater ab.<br>
Der Parameter EXPORT_DATEV_KONTOKLASSEN_OHNE_BU_SCHLUESSEL sollte auf . gestellt werden.

**Hinweis:** Der Export zur Übernahme in ein weiteres Buchhaltungssystem erfolgt in der Regel
- OHNE Eröffnungsbuchungen (2x) und
- OHNE Automatikbuchungen,
da diese vom externen Buchhaltungsprogramm nach dessen Logik durchgeführt werden.<br>
In der Regel werden die Daten auch ohne den Stornobuchungen übergeben.

#### Beim Export nach DATEV / RZL-CSV erscheint eine Fehlermeldung
Der Export wurde im DATEV bzw. im RZL-CSV Format definiert und es erscheint die Fehlermeldung
![](KeinEindeutigerFibuMwstCode.gif)

es konnte kein eindeutiger FIBU-MWSTCode gefunden werden.<br>
Für den DATEV Export ist eine Übersetzung der MWST-Sätze auf die DATEV Schlüssel erforderlich. Diese Meldung besagt dass kein oder mehrdeutige Schlüssel definiert sind. Prüfen Sie im System, unterer Modulreiter Mandant, oberer Modulreiter Mwst, dass für alle verwendeten Mehrwertsteuersätze entsprechende Fibu-MWSTCodes hinterlegt sind.<br>
Derzeit (2014) sind folgende Zuordnungen üblich:
- 19%Mwst ... 9
- 7%Mwst ..... 8

Stimmen Sie diese Daten bitte unbedingt mit Ihrem Steuerberater ab.

Angeblich kommt für den Zeitraum 1.7.2020 bis 31.12.2020 ein anderer Schlüssel zur Anwendung:
- Ust
    - 2 ... 5%
    - 3 ...  16%
    - 4 ...  7%
    - 5 ...  19%
- Vst
    - 6  ... 7%
    - 7  ... 19%
    - 8  ... 5%
    - 9  ... 16%

#### Kein eindeutiger FIBU-MWSTCode gefunden
Beim Aktivieren einer (Ausgangs-) Rechnung erscheint die Fehlermeldung
![](KeinEindeutigerFibuMWSTCode_02.png)  <br>
Es sind doch beides nur Handeingaben.<br>

Hintergrund:<br>
Die Buchung von Handeingaben erfolgt auf ein Erlöskonto. Siehe dazu den Parameter FINANZ_EXPORT_ARTIKELGRUPPEN_DEFAULT_KONTO_AR. Da damit der Erlös auf einem Konto mit einer eindeutigen Umsatzsteuer landet, dürfen keine gemischten Mehrwertsteuersätze gebucht werden. Darum diese Fehlermeldung.

Wenn du wirklich den Fall hast, dass du unterschiedliche Steuersätze mit übersteuerten Texten verwendet werden müssen, so definiere dir z.B. für jeden MwSt-Satz einen Artikel, der nicht Lagerbewirtschaftet ist und der z.B. keinen bezeichnenden Text hat. Wenn du nun diesen Artikel in die Rechnung / Lieferschein / Gutschrift übernimmst, so kann dieser Text übersteuert werden. Somit hast du durch den Artikel mit unterschiedlichen Artikelgruppen die richtige Zuordnung der Erlöskonten und gleichzeitig den gewünschten Text auf deinem Dokument.

#### Können auch Sachkonten, Debitoren, Kreditorenkonten für die Übernahme exportiert werden?
Ja. Es steht bei der integrierten Finanzbuchhaltung auch der Export der Kontendefinitionen zur Verfügung.<br>
Sie finden diese im unteren Reiter Buchungsjournal unter<br>
 ![](BuchungsjournalExport1.jpg), <br>
Buchungsjournal, Export Konten.

Damit der Export in die richtigen Dateien / Pfade erfolgt, definieren Sie diese bitte im System, unterer Modulreiter Parameter und dann die Parameter:
- FINANZ_EXPORTZIEL_SACHKONTEN
- FINANZ_EXPORTZIEL_DEBITORENKONTEN
- FINANZ_EXPORTZIEL_KREDITORENKONTEN
- FINANZ_EXPORT_UEBERSCHRIFT .... mit diesem Parameter kann definiert werden, ob die Spaltenüberschrift in den Exportdateien mit angegeben werden sollte.

Bitte beachten Sie, dass der Export dieser Daten grundsätzlich im ANSI Format erfolgt, damit die fremden Buchhaltungsprogramme dies auch lesen können. Der Parameter FIBU_EXPORT_ASCII wird nur für den Export der Eingangs/Ausgangsrechnungen unterstützt.

Nach erfolgtem Export erscheint eine entsprechende Meldung.<br>
![](Export_Sachkonten.jpg)<br>
Bitte beachten Sie unbedingt, dass vor dem Einspielen / aktualisieren der Buchhaltungsdaten eine vollständige Sicherung Ihrer externen Buchhaltung vorliegen muss.

#### Wie kann eine Rechnung mit Steuer an einen Kunden aus dem Drittland gestellt werden?
Es gibt Gründe, die bewirken, dass eine Ausgangsrechnung an einen Kunden aus einem Drittland mit Steuer ausgestellt werden muss. Wenn dies der Fall ist, so definieren Sie für diesen Kunden die Steuerkategorie im Debitorenkonto als "Inland". Das bewirkt, dass die Rechnung mit Steuer erstellt und verbucht werden kann.

#### Wie kann der Saldo der Vorperiode im Kontoblattdruck angeführt werden?
Wenn Sie den Haken bei Saldo Vorperiode beim Ausdruck des Kontoblatts setzen, so erhalten Sie die Saden der Vorperiode aufsummiert.

#### Was bedeutet bei der UVA-Art bzw. die Kontoart "nicht zutreffend"?
Dies bedeutet, dass keine spezifische Definition einer Kontoart oder UVA Art notwendig ist.

#### Wie ist das Rappen-Rundungs-Konto zu definieren?
<a name="Rappen Rundung"></a>
Unsere Schweizer und Liechtensteiner Anwender haben auch die Herausforderung, dass die Brutto-Rechnungsbeträge auf fünf ganze Rappen zu runden sind. Für die Definition dieser Landesregel [siehe bitte.]( {{<relref "/docs/stammdaten/system/#definition-des-rundungswertes" >}} )

Um die Rappenrundung verbuchen zu können sind folgende Definitionen erforderlich:
1.  Legen Sie das Rappenrundungskonto an z.B. 6948
    1.  Definieren Sie dieses mit Kontoart nicht zutreffend und
    2.  UVA Art Umsatz Inland steuerfrei
    3.  Variante Steuerfrei (ab 1.1.1900)
2.  Definieren Sie eine Artikelgruppe Rundung<br>
    Hinterlegen Sie hier das Rappenrundungskonto (6948)
3.  Legen Sie einen Rundungsartikel an.<br>
    Üblicherweise hat dieser die Artikelnummer RUNDUNG.<br>
    Und stellen Sie den Rundungsartikel auf die Artikelgruppe Rundung.
4.  Definieren Sie in den Parametern den Rundungsartikel

#### Wie ist der Jahres Gewinn-/Verlust zu verbuchen?
<a name="Verbuchen Jahresgewinn0"></a>
Da diese Frage immer wieder an uns herangetragen wird, hier eine kurze Beschreibung der erforderlichen Buchungen.
{{% alert title="Wichtiger Hinweis:" color="primary" %}}
Wir weisen ausdrücklich darauf hin, dass die richtige Verbuchung, die Berechnung des steuerlichen Gewinns, ev. getrennt in einen handelsrechtlichen und finanzrechtliche Aspekt, usw. usf. von **Kieselstein ERP** nicht geleistet wird. Dies liegt in Ihrer Verantwortung. Gegebenenfalls ziehen Sie bitte einen Steuerberater / Wirtschaftstreuhänder zu, um hier Sicherheit (soweit ein Berater denn in die Haftung geht) **zu erhalten.**
{{% /alert %}}

Ein kleiner Exkurs:<br>
Grundsätzlich ist es so, dass jeder Jahresgewinn das Eigenkapital des Unternehmens erhöht.

Vorgehensweise:
- a.) Sie haben, z.B. mit Journal, Ergebnisrechnung den Jahresgewinn ermittelt. ACHTUNG: Je nach Einstellung der Ergebnisrechnung ist das ausgewiesene Vorzeichen entsprechend zu berücksichtigen.
- b.) Schließen Sie die Erfolgskonten gegen das G+V Konto ab. D.h. buchen Sie den Saldo jedes Erfolgskontos (Endbestand) gegen das G+V Konto.

![](G_V_Ermittlungsbuchungen.jpg)

- c.) Nun ist das G+V Konto, je nach Saldo, gegen den Jahresüberschuss/Jahresgewinn zu buchen.

Soweit die Theorie. In der Praxis wird oft auf die oben angeführten Buchungen verzichtet und nur der ermittelte Gewinn verbucht.<br>
Ein Beispiel:<br>
Der Gewinn laut Erfolgsrechnung betrage 300,- €. Das beinhaltet, dass der Saldo des G+V Kontos ebenfalls 300,- ausmachen würde, bzw. dass die nicht gebuchten Saldovorträge insgesamt wiederum einen Saldo von 300,- € ergeben würden. Daher wird nur die Buchung des Jahresgewinns gegen G+V durchgeführt. Der Ausgleich des G+V Kontos ist implizit durch die (nicht durchgeführten) Saldovorträge jedes einzelnen Kontos gegeben.

Das bedeutet, eigentlich muss nur die Verbuchung des Jahresgewinns vorgenommen werden. Selbstverständlich ist die wichtigste Voraussetzung dafür, dass die Berechnung des Jahresgewinns richtig ist. D.h. dass die betroffenen Konten Ihrer Ergebnisrechnung richtig zugeordnet sind.

Zusätzliche Info: Wenn Sie in Ihrer Buchhaltung auch noch direkt die Bilanz aus dem **Kieselstein ERP** ausdrucken, so muss vermutlich der Jahresgewinn extra ausgewiesen werden.<br>
Das bedeutet, dass die Umbuchung vom Gewinn/Verlustkonto des laufenden Jahres auf das Jahres/Gesamtgewinn/Verlustkonto nur mit 1.1\. des Folgejahres gemacht werden darf.

