---
categories: ["Finanzbuchhaltung"]
tags: ["abweichende Länderart"]
title: "abweichende Länderart"
linkTitle: "abweichende Länderart"
weight: 350
date: 2023-01-31
description: >
 abweichende Länderart
---
Wie mit abweichenden Länderarten umgehen?
=========================================

Es kommt immer wieder vor, dass es umsatzsteuerlich durchaus interessante Konstellationen gibt. Daher nachfolgend eine unvollständige und Lose Zusammenstellung von Sachverhalten die uns so im Laufe der Zeit untergekommen sind.

Der wichtigste Punkt dabei ist, dass diese Sonderfälle in der Regel bereits durch den Auftrag definiert werden.

ACHTUNG: Das Umsatzsteuerrecht, ist außer kompliziert nur kompliziert. D.h. ob das für deinen Anwendungsfall steuerlich die richtige Vorgehensweise ist, erfrage bitte von einem fähigen Steuerberater. Unser Rat: Lasse dir das auch schriftlich bestätigen.

Jedenfalls: Das Team rund um das Kieselstein ERP, ist weder Bilanzbuchhalter noch Steuerberater und **lehnt jegliche Verantwortung** für die steuerlich richtige Behandlung **ab**.

### Fall IG Lieferung an anderes EU Land
Situation:
- Der Kieselstein ERP Anwender sitzt in Deutschland
- Der Auftraggeber sitzt z.B. in den Niederlanden
- Der Empfänger der Ware sitzt in Österreich

Die Verrechnung sollte nun mit der UID Nummer des Auftraggebers erfolgen.

Lösung: Definiere im Auftrag in den Kopfdaten die Länderart mit EU-Ausland mit UID-Nr. Damit erfolgt die Buchung auf die Lieferadresse und mit der UID Nummer der Lieferadresse

