---
categories: ["Finanzbuchhaltung"]
tags: ["Bilanz"]
title: "Aktiva Passiva Bilanz"
linkTitle: "Bilanz"
weight: 990
date: 2023-01-31
description: >
 Aktiva Passiva Bilanz
---
Bilanz, Aktiva Passiva
=============================

Da wir nicht Buchhalter immer wieder über diese Frage stolpern, hier ein kleiner Ausflug zu dem Thema. Teilweise entnommen [von](https://studyflix.de/wirtschaft/aktiva-passiva-2348).

![](Aktiv_Passiva_Bilanz.png)

Auf den Posten im Aktiva ist vermerkt, für welche Zwecke das Unternehmen das Geld verwendet (Mittelverwendung). Du siehst hier also das Vermögen. Untergliedert ist diese Seite der Bilanz in das Anlagevermögen (Bsp. Maschinen, Grundstücke) und das Umlaufvermögen (Bsp. Bankguthaben, Rohstoffe).

Die einzelnen Positionen im Passiva zeigen dir, woher das Geld des Unternehmens stammt (Mittelherkunft). Hier ist das Kapital aufgelistet. Diese Seite ist strukturiert nach Eigenkapital (Geld, das dem Unternehmen selbst gehört) und Fremdkapital (Bsp. aufgenommene Darlehen, Verbindlichkeiten; alle Schulden des Unternehmens).

Wenn du alle Posten auf einer Seite zusammenzählst, dann erhältst du die Bilanzsumme. Die Summe im Aktiva entspricht gemäß der Doppelten Buchführung der Summe im Passiva, da du ja die gleiche Geldmenge betrachtest, einmal woher sie kommt (Passiva) und das andere Mal, was du damit machst (Aktiva). 

Aus den Aktivposten bildest du die aktiven Bestandskonten (Bsp. Bank, Kasse, Fuhrpark, Betriebsausstattung; Dokumentation des Vermögens des Unternehmens). Aus den Posten im Passiva leiten sich die passiven Bestandskonten (Bsp. Verbindlichkeiten aus Lieferung und Leistung, Verbindlichkeiten gegenüber Kreditinstituten; Dokumentation des Kapitals des Unternehmens) ab.

Der Anfangsbestand der Bestandskonten befindet sich immer auf der Seite, auf der sich das Konto auf der Bilanz befindet. Aktivkonten haben den Anfangsbestand somit im Soll, Passivkonten im Haben. 

## Unterschied Aktiva Passiva
Aktiva und Passiva unterscheiden sich in einigen Aspekten voneinander. Nachfolgend  wichtigsten Unterschiede.
| Aktiva  | Passiva |
| --- | --- |
| Linke Seite der Bilanz | Rechte Seite der Bilanz |
| Mittelverwendung | Mittelherkunft |
| Vermögen (Anlagevermögen und Umlaufvermögen) | Kapital (Eigenkapital und Fremdkapital) |
| Aktive Bestandskonten | Passive Bestandskonten |

### Aktiva
Im Aktiva ist das Vermögen des Unternehmens aufgelistet. Du erkennst, für welche Zwecke das Unternehmen die Mittel verwendet. 

Sortiert sind die Posten auf der Aktivseite der Bilanz nach ihrer Liquidität. Also einfach gesagt, je schneller du etwas zu Bargeld machen kannst, desto weiter unten steht es. Immobilien stehen zum Beispiel relativ weit oben in deiner Bilanz, weil es einige Zeit dauert bist du ein Haus verkauft hast. Dein Bankguthaben steht sehr weit unten, da du es jederzeit abheben kannst.

Grundsätzlich ist das Aktiva untergliedert in das Anlagevermögen, das Umlaufvermögen und drei Nebenposten.
- Anlagevermögen<br>
Das Anlagevermögen soll dem Unternehmen langfristig dienen. Hier sind alle Vermögensgegenstände aufgelistet, die zum Aufbau, zur Ausstattung und zur Funktionsfähigkeit des Unternehmens notwendig sind.<br>
Aus diesen Bestandteilen setzt sich das Anlagevermögen zusammen:<br>
    Immaterielle Vermögensgegenstände (Bsp. Rechte, Lizenzen, Patente)
    Sachanlagen (Bsp. Gebäude, Maschinen, Grundstücke, Betriebsausstattung)
    Langfristige Finanzanlagen (Bsp. Wertpapiere, Unternehmensbeteiligungen, Aktien)
- Umlaufvermögen<br>
Das Umlaufvermögen dient dem Unternehmen nur kurzfristig. Es sind alle Vermögensgegenstände, die zum Verbrauch, zur Weiterverarbeitung oder zur Rückzahlung notwendig sind.<br>
Es besteht aus diesen Positionen:<br>
    Vorräte (Bsp. Rohstoffe, Betriebsstoffe, Erzeugnisse, Waren)
    Forderungen (Geld, das dir andere schulden)
    Kassenbestand, Bankguthaben, Guthaben bei Kreditinstitutionen
- Nebenposten Aktiva<br>
Die Nebenposten der Aktivseite setzten sich aus dem Rechnungsabgrenzungsposten, aus aktiv latenten Steuern und aus dem aktiven Unterschiedsbetrag aus der Vermögensverrechnung zusammen.<br>
Beim Rechnungsabgrenzungsposten werden alle Ausgaben aufgelistet, die das Unternehmen in diesem Geschäftsjahr gezahlt hat, die Gegenleistung wird aber erst in der Folgeperiode erbracht. Du überweist die Miete zum Beispiel schon im Dezember für Januar.<br>
Aktiv latente Steuern nimmst du in deine Bilanz auf, wenn du eine Steuerentlastung im nächsten Geschäftsjahr erwartest, weil der Steueraufwand in der Handelsbilanz niedriger ist als in der Steuerbilanz.<br>
Den Posten Aktiver Unterschiedsbetrag aus der Vermögensverrechnung brauchst du, da Vermögensgegenstände nicht mit Schulden verrechnet werden dürfen (Saldierungsverbot). 

### Passiva
Im Passiva steht das Kapital des Unternehmens. Du erfasst die Herkunft der Mittel deines Unternehmens. 

Gegliedert ist die Passivseite nach Eigenkapital und Fremdkapital. Letzteres ist wiederum nach der Laufzeit strukturiert.
- Eigenkapital
Unter Eigenkapital verstehst man das Geld des Unternehmens, das ihm wirklich gehört. Es steht dem Unternehmen unbefristet zur Verfügung. Es fallen keine Zinsen an und es muss nicht zurückgezahlt werden.<br>
Das Eigenkapital setzt sich aus diesen Posten zusammen:<br>
    Gezeichnetes Kapital (Stamm- / Grundkapital)<br>
    Rücklagen<br>
    Gewinn- und Verlustvortrag (Jahresüberschuss oder Jahresfehlbetrag, der aus GuV hervorgeht)
- Fremdkapital<br>
Unter dem Fremdkapital sind alle Verbindlichkeiten und Schulden des Unternehmens zusammengefasst. Diese kannst du aufteilen in Rückstellungen, Verbindlichkeiten und in die beiden Nebenposten, den Rechnungsabgrenzungsposten und passiv latente Steuern. 
- Rückstellungen<br>
Rückstellungen sind zukünftige Verbindlichkeiten, deren Höhe und Eintrittszeitpunkt ungewiss ist.<br>
Du führst zum Beispiel einen Gerichtsprozess, der sich bis ins nächste Geschäftsjahr zieht. Du weißt, dass hohe Gerichtskosten auf dich zukommen, aber nicht, wann genau und in welcher Höhe du sie bezahlen musst (Prozessrückstellungen).<br>
Weitere Beispiele für Rückstellungen sind:<br>
    Pensionsrückstellungen<br>
    Steuerrückstellungen<br>
    Instandhaltungsrückstellungen<br>
- Verbindlichkeiten<br>
Unter den Verbindlichkeiten werden sämtliche Schulden des Unternehmens zusammengefasst.<br>
Die Gelder sind zum aktuellen Zeitpunkt noch im Unternehmen vorhanden, sie müssen aber zu einem bestimmten  Zeitpunkt wieder zurückgezahlt werden.<br>
Hier führst du zum Beispiel diese Verbindlichkeiten auf:<br>
    Verbindlichkeiten gegenüber Kreditinstituten (Bsp. Darlehen, Kontokorrentkredite)<br>
    Erhaltene Anzahlungen auf Bestellungen<br>
    Verbindlichkeiten aus Lieferungen und Leistungen (offene Rechnungen)<br>
- Nebenposten Passiva<br>
Die Nebenposten der Passivseite setzen sich zusammen aus dem Rechnungsabgrenzungsposten und den passiv latenten Steuern.<br>
Unter den Rechnungsabgrenzungsposten fasst du alle Einnahmen des Unternehmens zusammen, die es in dieser Periode erhalten hat, aber die Leistung erst im nächsten Geschäftsjahr erbringt.<br>
Die passiv latenten Steuern erfasst du, wenn du in Zukunft eine Zahlungsverpflichtung gegenüber dem Finanzamt erwartest. 
