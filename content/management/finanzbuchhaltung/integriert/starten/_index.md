---
categories: ["Finanzbuchhaltung"]
tags: ["Fibu starten"]
title: "integrierte Fibu starten"
linkTitle: "starten"
weight: 100
date: 2023-01-31
description: >
 Wie mit dem Finanzbuchhaltungsmodul starten
---
Integrierte FiBu starten
========================

Wie wird am besten beim Start der integrierten Finanzbuchhaltung vorgegangen?

Dies ist eine Stichwortsammlung der durchzuführenden Schritte. Für die Durchführung / Aktivierung siehe bitte bei den jeweils einzelnen Punkten.

**Ein wichtiger Hinweis:**

Wir gehen davon aus, dass sie mit den Grundzügen einer Buchhaltung und der Tätigkeit / Wirkung in der Finanzbuchhaltung vertraut sind. Dies ist kein Kurs über Finanzbuchhaltung. [Auch sind wir keine Steuerberater oder Ähnliches.](Integrierte_Finanzbuchhaltung.htm#keine Steuerberatung)
Gerade wenn unterschiedliche Finanzämter zum Einsatz kommen, achten Sie auf die steuerlichen Unterschiede in den Ländern.<br>
Als wesentlich erscheint uns, in Deutschland wird die Umsatzsteuer immer im Kalenderjahr abgerechnet, auch bei abweichendem Geschäfts/Wirtschaftsjahr, in Österreich ist das gleichlautend mit dem Geschäftsjahr, wie das in der Schweiz ist, ist uns derzeit nicht bekannt.

**Hinweis:**

Die **Kieselstein ERP** Finanzbuchhaltung beinhaltet keine Warenbewegungsbewirtschaftung, wie sie manchmal insbesondere für Abgrenzungen angewendet werden. Die Bewegungen des Lagers, finden Sie im Modul Artikel, mit den entsprechenden Lagerauswertungen zum Stichtag bzw. im Modul Fertigung mit der Halbfertigfabrikatsinventur.

Halten Sie die Konten in der Fibu so schlank wie irgend möglich. Auswertungen finden Sie in den ERP Modulen von **Kieselstein ERP**, z.B. Hitliste im Modul Artikel, Kunden Umsatzstatistik mit Deckungsbeitragsauswertung im Modul Kunden, usw.. Siehe dazu auch [Abgrenzungen]( {{<relref "/management/finanzbuchhaltung/integriert/#abgrenzung-von-wareneinsatz" >}} )

## Ablauf der Grundeinstellung der Integrierten Finanzbuchhaltung von **Kieselstein ERP**
1.  Definieren Sie Ihr Standard Finanzamt.
    In dem Modul Finanzbuchhaltung unterer Reiter Finanzamt: Neu und tragen dieses in System, unterer Reiter Mandant, oberer Reiter Vorbelegungen 2 ein.
2.  Prüfen Sie die MwSt Sätze.
    Sie finden die Steuersätze in System, unterer Reiter Mandant, Mwst Stellen Sie diese gegebenenfalls richtig und entfernen Sie nicht benötigte Steuersätze. 
3.  Definieren Sie den Kontenrahmen in Abstimmung mit Ihrem Steuerberater.
    Es ist essentiell wichtig, dass Sie die gleichen Sachkonten verwenden wir Ihr Steuerberater.
    Bestehen Sie darauf. Nur damit ist gewährleistet, dass Sie und Ihr Berater vom Gleichen sprechen.<br>
    Definieren Sie gegebenenfalls die Stellenanzahl der Sachkonten.<br>
    Siehe dazu [Import der Sachkonten](Integrierte_Finanzbuchhaltung.htm#Import Sachkonten).
    Siehe auch: [Welche Konten sollten importiert werden](#Welche Sachkonten).
4.  Verwenden Sie nur Konten die Sie tatsächlich benötigen.<br>
    Völlig unsinnige Konten löschen Sie bitte. Wenn Konten eventuell in der Zukunft verwendet werden könnten, so setzen Sie diese auf versteckt, damit bei der täglichen Arbeit nur die Konten angesprochen werden die benötigt werden.<br>
    Details zur Kontendefinition siehe [Beispiel für die Startdefinition von Sachkonten](#Welche Sachkonten)
5.  Definieren Sie die UVA Art jedes einzelnen Sachkontos und gegebenenfalls ein abweichendes Finanzamt.<br>
    Prüfen Sie die Definition durch Ausdruck des Kontenrahmens im Journal, Konten.
6.  Beachten Sie die Anmerkungen zur Definition von Sachkonten.
7.  Definieren Sie die Steuerkategorien und die Steuerkategoriekonten<br>
    Im unteren Reiter Finanzamt, oberer Reiter Steuerkategorie können Sie die Konten nach den geforderten Kriterien auswählen. Definieren Sie hier nur die Zuordnungen die Sie erwarten, das bringt deutlich Sicherheit.
8.  Definieren Sie in den Steuerkategorien die jeweils mitlaufenden Konten für Forderungen und Verbindlichkeiten<br>
    Hier raten wir je Länderart ein eigenes mitlaufendes Konto zu definieren, Sie gewinnen mehr Übersicht.
9.  Stellen Sie den Buchhaltungsexport auf Artikelgruppen
    Parameter: FINANZ_EXPORT_VARIANTE: Artikelgruppen
10. Definieren Sie das Default Sachkonto insbesondere für die Handeingaben<br>
    Parameter: FINANZ_EXPORT_DEFAULT_ARTIKELGRUPPEN_AR
11. Schalten Sie folgende Parameter ein:
    - ARTIKELGRUPPE_IST_PFLICHTFELD
    - AUTOMATISCHE_DEBITORENNUMMER
    - AUTOMATISCHE_KREDITORENNUMMER
12. Definieren Sie den Zeitraum der UVA ob monatlich (default) oder quartalsweise.
    - FINANZ_UVA_ABRECHNUNGSZEITRAUM
13. Definieren Sie den Zahlenraum und die Stellenanzahl der Debitoren- und Kreditorennummern
    - DEBITORENNUMMER_VON
    - DEBITORENNUMMER_BIS
    - KONTONUMMER_STELLENANZAHL_DEBITORENKONTEN (4 Stellen = 1000) und entsprechende für Kreditoren 
14. Definieren Sie gegebenenfalls ob es fortlaufende oder strukturiert nummerierte Debitoren-/ Kreditorennummern gibt.
    FINANZ_DEBITORENNUMMER_FORTLAUFEND.
15. Sind bereits Buchungsdaten von früheren / Alt-Systemen vorhanden, so definieren Sie Debitoren- und Kreditoren- Nummern gleichlautend mit dem Altsystem / System des Steuerberaters oder Ähnlichem.
16. Definieren Sie bei allen Debitoren / Kreditorenkonten die richtige Steuerkategorie.
    Bei mehreren Finanzämtern ist auch das Finanzamt zu prüfen und gegebenenfalls richtig zu stellen.
17. Definieren Sie für alle Artikel die Artikelgruppen und für alle Artikelgruppen die Erlöskonten
18. Definieren Sie die entsprechenden Länderartübersetzungen der Erlöskonten: im Reiter Sachkonten Konto-Länderart Übersetzung. Das bewirkt, dass wenn eine Buchung aus dem Ausland kommt, die entsprechend angelegten Sachkonten bebucht werden.

19. Prüfen Sie mit der Funktion Belegübernahme ob Sie alle Rechnungen, Gutschriften und Eingangsrechnungen übernehmen können
    - a.) Prüfen Sie aus der Finanzbuchhaltung heraus Monat für Monat ob die Übernahme funktioniert.<br>
         Geht diese nicht, so gehen Sie bitte zum einzelnen Beleg und beheben die Ursache der Fehlermeldung<br>
    - b.) Unter bearbeiten Belegübernahme kann jeder Beleg eigenständig in die Finanzbuchhaltung erneut übernommen werden.<br>
         Hier werden entsprechende Fehlermeldungen unter Detail ausgegeben, die die Ursache der Nicht Übernahme aufzeigen.

20. Prüfen Sie unter Grunddaten, UVA Art, dass alle UVA Arten die jeweils gültige Kennzahl für die (derzeit manuelle) Übertragung ans Finanzamt (Finanzonline bzw. Elster) hat.

21. Prüfen Sie die UVA in Gegenüberstellung mit dem Ausgangsrechnungsjournal und dem ER-Kontierungsjournal und soweit vorhanden dem Zusatzkosten-Kontierungsjournal.<br>
    Es muss dies fachlich zusammenstimmen.

23. Buchen Sie die Eröffnungsbuchungen, vor allem Debitoren Kreditoren aber auch der entsprechenden Sachkonten.
[Siehe dazu auch Übernahme bei bestehender Finanzbuchhaltung]( {{<relref "/management/finanzbuchhaltung/integriert/#wie-die-er%c3%b6ffnungsbuchungen-bei-bestehender-finanzbuchhaltung-%c3%bcbernehmen" >}} )

Bitte achten Sie darauf die Module z.B. Finanzbuchhaltung, Lieferanten nach Änderungen in den Parametern zu schließen und neu zu öffnen, so dass die Änderungen übernommen werden.

#### Definition der Sachkonten
<a name="Definition der Sachkonten"></a>
Beschreibung der Punkte die bei der Definition eines Sachkontos zu beachten sind:

![](Kontodefinition.jpg)

| Feldname | Beschreibung |
| --- |  --- |
| Kontonr. | Die Kontonummer des Sachkontos inkl. führender Null (0). Die Stellenanzahl kann mit dem Parameter KONTONUMMER_STELLENANZAHL_SACHKONTEN definiert werden. |
| Bezeichnung | Textliche Bezeichnung des Kontos. Achten Sie auf die Eindeutigkeit der Bezeichnung. Sowohl für Sie selbst als auch für Ihren Steuerberater. |
| Kontoart | Die Kontoart ist vor allem für die Automatikbuchungen im Rahmen der UVA relevant.Für eine Darstellung der Zusammenhänge siehe bitte [Finanzamtsammelbuchungen](Integrierte_Finanzbuchhaltung.htm#Finanzamtsammelbuchungen). Als zusätzliche Kontoart steht hier Eröffnungskonto (Saldenvortrag) zur Verfügung. |
| Kostenstelle | Für Ihre interne Definition. Hat im Rahmen der integrierten Buchhaltung in **Kieselstein ERP** derzeit keine weitere Bedeutung |
| Ergebnisgruppe | Wenn dieses Konto ein G&V Konto ist, so hinterlegen Sie hier die gewünschte Ergebnisgruppe. [Zur Definition von Ergebnisgruppen siehe bitte dort](Integrierte_Finanzbuchhaltung.htm#KER). |
| Bilanzgruppe | Ist das Konto ein Bilanzkonto, so kann hier die zutreffende Bilanzgruppe hinterlegt werden. [Zur Definition von Bilanzgruppen siehe bitte dort.](Integrierte_Finanzbuchhaltung.htm#Bilanzgruppe)<br>Da Kontokorrentkonten sich je nach deren Kontostand entweder im Eigenkapital oder im Fremdkapital abgebildet werden sollten, gibt es die Bilanzgruppe positiv und dafür auch die Bilanzgruppe negativ. Wurde eine "Bilanzgruppe positiv" ausgewählt, so muss auch eine "Bilanzgruppe negativ" angegeben werden. |
| Finanzamt | Bei welchem Finanzamt wirkt dieses Konto für die UVA Verprobung. |
| UVA Art | Bei welcher UVA Kennzahl wird, falls zutreffend, dieses Konto berücksichtigt.Beachten Sie dazu bitte auch [Definition Steuerkategorien](Integrierte_Finanzbuchhaltung.htm#Definition Steuerkategorien) und [Definition UVA Arten](Integrierte_Finanzbuchhaltung.htm#Definition UVA Arten) und [Definition UVA Kontenzuordnung](Integrierte_Finanzbuchhaltung.htm#Definition UVA Kontenzuordnung) |
| Steuerkategorie | Wenn dieses Konto auch bei manuellen Buchungen Mehrwertsteuer bzw. Vorsteuer mit berechnen und buchen sollte, so geben Sie hier bitte die gewünschte Steuerkategorie an. Definieren Sie in diesem Falle zusätzlich ![](Steuerkategorie_am_Sachkonto.gif) ob Vorsteuer oder Umsatzsteuer gebucht werden sollte. |
| Gültig ab | ab diesem Datum, in der Regel der 1\. des Geschäftsjahres im Anlage-Jahr steht das Konto zur Verfügung |
| Automatische Eröffnungsbuchung | Bei Sachkonten nicht wirksam. Siehe dazu auch [Periodenübernahme]( {{<relref "/management/finanzbuchhaltung/integriert/#perioden%c3%bcbernahme" >}} ). |
| Ohne Ust./Vst. | Dient Ihrer ER-Erfassungssicherheit. D.h. ist dieses Feld angehakt, so erscheint bei der Erfassung einer Eingangsrechnung mit Vorsteuer und Buchung auf dieses Konto eine entsprechende Warnung. |
| (Gültig) bis | Dieses Konto sollte ab dem Datum nicht mehr bebucht werden. Ist eine reine Information|
| allgemein sichtbar | Ist dies nicht angehakt, so ist das Konto nur mit Chefbuchhalterrecht sichtbar |
| Sortierung | Definieren Sie hier die für das Konto gewünschte Sortierung. Z.B. nach Auszugsnummer für Bankkonten |
| manuell bebuchbar | Darf dieses Konto auch durch manuelle Umbuchungen / Splittbuchungen bebucht werden. So dürfen z.B. die mitlaufenden Konten nicht manuell bebucht werden. |
| Währung Druck | Wenn definiert, wird das Konto beim Druck in der gewählten Fremdwährung ausgedruckt. |
| versteckt | Konten die bisher bebucht wurden, nun aber nicht mehr benötigt werden, können hier versteckt werden. Damit sind sie in der üblichen Auswahlliste nicht sichtbar. Dies wirkt auch für die sichtbaren Bankverbindungen|
| Bemerkung | Gerade für den SKR03 und SKR04 gibt es lange Beschreibungen der Bedeutung der verschiedenen Konten. Für diese Beschreibungen und sonstigen Anmerkungen ist dieses Feld gedacht. |

#### Muss für jede Umsatzsteuerart ein eigenes Konto definiert werden ?
Ja. Dies bedingt zwar auf der einen Seite, dass mehr Konten angelegt werden müssen, erhöht aber auf der anderen Seite die Transparenz und die Nachvollziehbarkeit deutlich. So können Kontrollrechnungen z.B. für die UVA sehr einfach anhand der Saldenliste gemacht werden (Erlöse mit 20% müssen die abzuführende Umsatzsteuer ergeben), was in dem Falle der gemischten Umsatzsteuersätze auf einem Konto faktisch unmöglich ist, bzw. nur mit speziellen Auswertungen erfolgen kann.<br>
Zusätzlich wird durch die integrierte Verwendung die Aussage über Erlösgruppen usw. im ERP System dargestellt, woraus deutlich bessere und detailliertere Aussagen getroffen werden können, als mit einer noch so gut gepflegten Fibu möglich wären.

#### Wie kann die Kontendefinition rasch überprüft werden?
<a name="Prüfen der Kontendefinition"></a>
Um die getroffenen Kontendefinitionen überprüfen zu können gehen wir in der Regel wie folgt vor:
1.  Druck des Kontenjournals (unterer Modulreiter Sachkonten, Journal, alle Konten)<br>
    Hier wird geprüft ob:
    -   Stimmen die Kontenarten zu den Konten
    -   Stimmen die UVA Arten (Stimmen jeweils im Sinne des oben beschriebenen, bei der UVA Art achten Sie bitte besonders auf die indirekte Definition für die reine Inlands-Vorsteuer)
    -   Stimmen die Ergebnisgruppen-Zuordnungen bzw. Bilanzgruppen Zuordnungen.
        Sind alle Konten jeweils einer der beiden zugeordnet
2.  Druck einer aktuellen Saldenliste mit der Druckvariante quer.<br>
    Hier werden erneut die unter 1\. angeführten Prüfungen vorgenommen. Man hat hier unter Umständen eine bessere Übersicht ob dies schon richtig zugeordnet ist.<br>
    **Hinweis:** Die bei den Sachkonten angedruckte Steuerkategorie wirkt NUR bei der manuellen Buchung
3.  Prüfung der Definitionen des Finanzamtes (unterer Modulreiter Finanzamt)
    -   Sind Steuernummer und gegebenenfalls Referatsnummer eingetragen
    -   ist die Formularnummer richtig definiert und ist gegebenenfalls Umsätze Runden angehakt
    -   Sind die Steuerkategorien definiert. Definieren Sie nur die für Sie zutreffenden Steuerkategorie, nur so verhindern Sie eventuell Buchungsfehler
    -   Definieren Sie die Buchungsparameter, insbesondere die EB-Konten
    -   Wenn mehrere Finanzämter eingerichtet werden mussten, so definieren / prüfen Sie dies bitte für jedes Finanzamt
    -   Eine Gesamtübersicht über die Steuerkategorien erhalten Sie im Journal Steuerkategorie

#### Welche Arbeiten sind erforderlich, bevor Daten an Ihren Steuerberater übergeben werden?
Oft ist (Jung-)Unternehmern nicht klar, dass der Steuerberater (böse gesagt) nur Steuerberater ist. Von Ihrem täglichen Business hat er keine Ahnung. Daher ist er nicht in der Lage aus unsauberen Daten eine ordentliche Bilanz, eine gute steuerliche Begleitung zu machen. D.h. er ist darauf angewiesen dass Sie ihm die Unterlagen bestmöglich aufbereitet zur Verfügung stellen.

Oft treffen wir auf den Fall, dass in den Anfangsjahren die Buchhaltung der Steuerberater macht und dann auf die Buchhaltung im eigenen Hause umgestellt wird. Da gerade das erste Jahr einiges an Vorbereitungsarbeiten für eine gute Übergabe der Unterlagen an den Steuerberater erforderlich ist, hier eine Zusammenstellung der Punkte die **vor** der Übergabe geprüft und geklärt werden sollten.

1.  Prüfen Sie dass alle Buchungen aus den einzelnen Modulen tatsächlich in der Fibu sind. Führen Sie dazu eine Jahres-UVA im jeweiligen Geschäftsjahr durch. Korrigieren Sie eventuelle Unstimmigkeiten.

2.  Übernehmen / Prüfen Sie die Eröffnungsbuchungen, sodass diese mit den Daten Ihres Steuerberaters übereinstimmen (A).

3.  Gleichen Sie eventuelle Verrechnungskonten ab. Z.B. zwischen Ihrer GmbH und Ihrer Einzelfirma (L).

4.  Prüfen Sie die offene Postenliste der Debitoren gegen die offenen Ausgangsrechnungen zum Stichtag aus dem Modul Rechnungen. Bei Unstimmigkeiten müssen diese aufgelöst werden. <br>
Gegebenenfalls müssen einzelne Buchungspositionen ausgeziffert werden.<br>
**Wichtig:** Um in die Daten eine entsprechende Transparenz zu bekommen sollte jede offene Rechnung mit allen eventuellen Teil-Zahlungsdetails im einzelnen Debitorenkonto gebucht sein. Eventuell kann eine Buchung mit der offenen Gesamtsumme je Debitor gemacht werden. Die von manchen Steuerberatern übliche Vorgehensweise alles als Pauschalsumme einzubuchen ist viel zu grob, da damit keine Chance besteht, eventuelle Unstimmigkeiten zu finden.<br>
Dass die EBs (Eröffnungsbuchungen) brutto, also inkl. Ust. einzubuchen sind, ist selbstverständlich. Es ist Ihnen Ihr Kunde ja den Bruttobetrag schuldig.<br>
Geben Sie bei manuellen Eröffnungsbuchungen die gleiche Belegnummer an, damit die automatische Auszifferung zusammenfinden kann.<br>
Die Buchung muss auf (mehrwert-) steuerfrei gestellt werden.

5.  Prüfen Sie die offene Postenliste der Kreditoren gegen die offenen Eingangsrechnungen zum Stichtag aus dem Modul Eingangsrechnungen. Bei Unstimmigkeiten müssen diese aufgelöst werden.<br>
Gegebenenfalls müssen einzelne Buchungspositionen ausgeziffert werden.<br>
Beachten Sie das Thema der offene Posten aus den Vorperioden, wie unter 3. beschrieben.

6.  Prüfen Sie die Konten die faktisch immer einen Null-Saldo haben müssen (L).<br>
Dies sind z.B. Netto-Löhne und Gehälter, Finanzamts(Zahllast)konto, Sozialversicherungskonten (GKK, AOK), Kommunalsteuer und ähnliche Konten.

7.  Das alle Bankkonten mit dem jeweiligen Saldo auf jedem einzelnen Kontoauszug immer überstimmen ist eine Selbstverständlichkeit (L).

8.  Das die Kassenbücher immer exakt stimmen ist eine Selbstverständlichkeit (L).

9.  Drucken Sie nun die Saldenlisten der Debitoren und Kreditoren.

10. Drucken Sie die Saldenliste der Sachkonten.

11. Bewährt hat sich auch, alle Kontoblätter an den Steuerberater zu übergeben. Damit hat er Einsicht in all Ihre Buchungen.

12. Ebenfalls benötigt er all Ihre Erklärungen die Sie gegenüber dem/den Finanzamt/Finanzämtern abgegeben haben wie UVA, ZM usw. und auch die entsprechenden Buchungsmitteilungen.

Zusätzlich benötigen Sie noch weitere Daten für Ihren Steuerberater wie z.B.:
-   Der Lagerstand zum Stichtag, eventuell mit abgewerteten Ladenhütern.<br>
    Das Thema der Bewertung der Ladenhüter klären Sie bitte mit Ihrem Steuerberater
-   Aktuelle Urlaubsansprüche und Zeitguthaben Ihrer Mitarbeiter.
-   Geben Sie die neu hinzugekommenen Anlagengüter bekannt (Afa)
-   Geben Sie ebenfalls eventuell ausgeschiedene Anlagengüter bekannt.

**Hinweis:**

Eigentlich müssen die OP's aus dem Vorjahr den EB's des laufenden Jahres entsprechen. Ausgenommen davon sind Umbuchungen und Korrekturen die im Zuge der Bilanz durchgeführt wurden.

(A) ... Diese Arbeiten sollten so früh wie möglich durchgeführt werden. Damit erhalten Sie auch während des laufenden Jahres Sicherheit, dass Ihre Buchungen richtig sind. Sie müssen jedoch spätestens zum **A**bschluss gemacht werden.

(L) ... Diese Arbeiten sollten Sie **L**aufend machen und so entsprechende Buchungssicherheit erlangen.

**Hinweis:**

Manche gute Steuerberater wünschen sich, dass sie kein/zusätzlich zum Papier (sondern) das [Buchungsjournal](Integrierte_Finanzbuchhaltung.htm#Export Buchungsjournal) in elektronischer Form erhalten. Es ist damit eine deutlich größere Sicherheit und Transparenz verbunden. Wir können diese Vorgehensweise nur begrüßen.

**Ein Tipp:**

Lassen Sie sich von niemandem dazu überreden, die Bilanz ein oder zwei Monate nach Ende des Geschäftsjahres abzugeben. Mit allen erforderlichen Abgrenzungsarbeiten und das neben ihrem laufenden Tagesgeschäft, ist dies ein Ding der Unmöglichkeit bzw. mit einem derartigen Aufwand verbunden, dass es nicht dafür steht. Wenn Ihre Liquiditätssituation angespannt ist, so müssen Sie mit entsprechenden vertrauensbildenden Maßnahmen und laufendem verlässlichen Reporting Ihre finanzierenden Partner von Ihnen bereits überzeugt haben. Hier nur auf die Bilanz zu setzen ist deutlich zu wenig.

#### Welche Sachkonten sollten importiert werden ?
<a name="Welche Sachkonten"></a>
Steuerberater neigen in der Regel dazu den für den Moment einfachsten Weg zu gehen.
D.h. wenn Sie Ihren Steuerberater fragen, welche Konten Sie den importieren / anlegen sollten, dann erhalten Sie normalerweise die Auskunft, legen Sie den ganzen Sachkontenrahmen der überhaupt definiert ist an.<br>
Das sind z.B. für den SKR03 ca. 1500 Konten, für den SKR04 1300, für den Einheitskontenrahmen 600 usw.<br>
Die Praxis zeigt:<br>
- a.) je mehr Konten Sie haben, desto höher ist die Wahrscheinlichkeit dass auf das falsche Konto gebucht wird.
- b.) die Parametrierung der Konten muss derzeit immer an Ihre Gegebenheiten angepasst werden. <br>
Dies ist für jedes Konto mit einem gewissen Zeitaufwand verbunden, je mehr Konten, desto größer der Zeitaufwand für die Anpassungsarbeit.
- c.) Als Geschäftsführer / verantwortliche(r) Buchhalter(in) müssen Sie die Bedeutung jedes Kontos kennen.<br>
Wer von uns kann schon mit "Minderung der Entnahmen § 4 (4a) EStG (Haben)" gerade am Beginn der unternehmerischen Tätigkeit etwas anfangen.

Also: **Weniger ist mehr.**

**Ein Beispiel für die Startdefinition von Sachkonten:**
1. Erlöskonto "Erlöse Inland" Bereich 4...
2. UST Konto 20% (Kontoart: Ust Konto) Bereich 3... 
3. VST Konto 20% (Kontoart: Vst Konto) Bereich 2...
4. IG Erwerb / IG Erlös 
5. Skontoaufwand Bereich 4... (10%, 20%)
6. Skontoerlös Bereich 5... (10%, 20%)
7. Wareneinsatz Bereich 5... (Bsp. Material, IG Erwerbe, Fremdleistung)
8. Kreditoren / Debitoren automatisch (aber Sammelkonten dazu)
    1. 2000 Kundenforderungen Inland (mitlaufende Konten)
    2. 2001 Kundenforderungen EU (mitlaufende Konten)
    3. 3301 Lieferantenverbindlichkeiten Inland
    4. 3302 Lieferantenverbindlichkeit EU
9. Kontendefinition UVA
    - Finanzamt Zahllastkonto Bereich 3...
    - Verrechnung Finanzamt Bereich 3...
    - Vst Sammelkonto Bereich 2... Art Vst. Sammelkonto
    - Ust Sammelkonto Bereich 3... Art Ust. Sammelkonto
10. Verrechnungskonten für Anzahlungen
    - 2...Gegebene Anzahlungen
    - 3...Erhaltene Anzahlungen

#### Gegebenenfalls hilft Ihnen auch die Definition des österreichischen Einheitskontenrahmens
Der österreichische Einheitskontenrahmen besteht aus 10 Kontenklassen (0 - 9). Dieser wird unterteilt in Konten die gegen das SBK abgeschlossen werden (zumindest theoretisch, macht niemand mehr). Es sind dies die Konten 0-3\. Und Konten die gegen GuV, Gewinn und Verlust, abgeschlossen werden. Die Kontenklasse 9 beinhaltet die Abschlusskonten.<br>
Man spricht hier auch von:<br>
-   Bestandskonten
    -   0 ... Anlagevermögen und Aufwendungen z.B. Grundstücke, Fahrzeuge usw.
    -   1 ... Vorräte
    -   2 ... Sonstiges Umlaufvermögen, z.B. Kassa, Bank
    -   3 ... Verbindlichkeiten, Rückstellungen z.B. Lieferverbindlichkeiten, Umsatzsteuer Zahllast usw.
-   Erfolgskonten
    -   4 ... Betriebliche Erträge
    -   5 ... Materialaufwand, Wareneinsatz
    -   6 ... Personalaufwand z.B. Löhne, Gehälter, Sozialabgaben
    -   7 ... Betriebliche Aufwändungen und Abschreibungen
    -   8 ... Finanzerträge und -aufwände, z.B. Zinsen
-   Abschlusskonten
    -   9 ... Abschluss- und Verrechnungskonten z.B. Kapital, Gewinn - Verlust Konto (GuV), Eröffnungsbilanzkonten

In anderen Sachkontenrahmen ist dies vom Gedankengang sehr ähnlich, wenn auch die Nummernkreise durchaus unterschiedlich sind.

#### Können die (Bilanz-) Umbuchungen des Steuerberaters importiert werden?
Es erreichen uns immer wieder Anfragen, dass die Umbuchungen des Steuerberaters, welche dann auch oft noch einige hundert Umbuchungen sind, importiert werden sollten.<br>
Dieser Import ist in **Kieselstein ERP** sehr bewusst nicht vorgesehen, denn:<br>
Der Wunsch nach dem Import der Umbuchungen ist aus Sicht aller Buchhalter zu 100% berechtigt und nachzuvollziehen aber:<br>
Sie haben mit **Kieselstein ERP** nicht nur eine einfache Buchhaltungssoftware sondern ein komplexes ERP System. Das bedeutet, wenn nun einige hundert Umbuchungen nachzubuchen sind, sind davon mit sehr hoher Wahrscheinlichkeit auch viel Ein- und Ausgangsrechnungen betroffen. Diese wurden nicht oder falsch abgeschlossen usw.. Das nun nur auf der Buchhaltungsseite zu bereinigen hilft zwar in dem Moment für die Bilanz, aber leider nicht langfristig, da aus der ERP Sicht, die Eingangs- und Ausgangsrechnungen, Gutschriften nach wie vor falsch wären.<br>
Die sehr große Gefahr bei einem Import wäre nun, dass mit dem Import die Buchungen schnell schnell übernommen werden, aber die unbedingt erforderliche Bereinigungsarbeit nicht geleistet wird. Aber NUR mit der Bereinigung haben Sie ein sauberes System. Das würde in weiterer Folge bedeuten, dass im Endeffekt die Arbeit des Einpflegens der Umbuchungen (nur in der Buchhaltung) umsonst ist. Da es viele Buchungen sind, doppelt tragisch.<br>
Ein oft vorkommendes Beispiel ist, das Forderungen bzw. Verbindlichkeiten Konto. Dieses ist ein sogenanntes mitlaufendes Konto. D.h. die Buchungen auf diesem Konto ergeben sich automatisch aus den Buchungen auf den Kreditoren / Debitoren. Diese ergeben sich in aller Regel wiederum aus den Rechnungen und den Zahlungen. Nun ist es bei sehr sehr vielen Steuerberatern üblich, was aus rein buchhalterischer Sicht auch so in Ordnung ist, das Forderungen- / Verbindlichkeitenkonto einfach mit einer Gegenbuchung auszugleichen. Aus wie vielen Einzelberichtigungen das nun für Kunde A, Rechnung x besteht, ist dem Steuerberater egal. Aber: In Ihrem Mahnwesen haben Sie das noch mit drinnen. Ihre Artikelumsätze sind falsch. Die Kunden Deckungsbeitragsrechnung hat diese Information nicht usw.. Sie sehen, Ihr Unternehmen ist deutlich komplexer als das was die Buchhaltung abbildet.<br>
Darum unser dringender Rat, bitte bereinigen Sie dies in richtiger ERP konformer Form. Es ist uns bewusst, dass die meisten Buchhalter dafür kein Verständnis haben und die Techniker auch kein Verständnis für die Buchhaltung. Jeder lebt in seiner Welt, aber es müssen beide Welten verbunden werden. Es ist ja immerhin auch Ihr Unternehmen und nicht das des Steuerberaters und am Schluss müssen Sie dafür den Kopf hinhalten.<br>
Wenn Sie dafür Unterstützung brauchen, Ihr **Kieselstein ERP** BetreuerIn hilft Ihnen gerne.

<a name="Rund um die Bilanz"></a>Infos rund um das Thema Bilanz
---------------------------------------------------------------

Nachdem immer wieder auch Fragen rund um die Bilanz an uns herangetragen werden, hier eine lose Sammlung von Fragen, Gedanken und Antworten dazu.

Bitte beachten Sie, dass für eine verbindliche Bilanz diese nur von BuchhalterInnen mit entsprechenden Prüfung erstellt werden dürfen.

Nachfolgende Fragen kommen vor allem auch aus dem Thema einer monatlichen Bilanz.

Wir wünschen Ihnen, dass Sie Ihre Unternehmenssteuerung so im Griff haben, dass dies nicht erforderlich ist. Wenn doch, bedenken Sie: Je kürzer der Zeitraum vom vergangenen Monat / Zeitraum bis zur Auswertung, desto mühsamer ist eine saubere Abgrenzung.

#### Wie mit noch nicht fakturierten Wareneingängen umgehen?
Vom Lieferanten noch nicht fakturierte Wareneingänge können für die Bilanz wie eine Art Eingangsrechnungen die noch nicht erfasst wurden gedacht werden. D.h. auch dieser Betrag ist in Ihrem Lageraufbau bereits enthalten, aber eben von Ihren Lieferanten noch nicht verrechnet.
Das bedeutet auch, dass bei Betrachtungen zum Wareneinsatz anhand der Lagerveränderungen, diese Zahl damit nicht doppelt betrachtet wird.

#### Wo findet man die noch nicht fakturierten Wareneingänge?
Siehe bitte, Bestellung, Journal, Wareneingang.
![](WEP_ohne_ER.jpg)<br>
Hier finden Sie am Ende, WE ohne ER zum Einstands Preis. Zusätzlich sind die Lieferscheine ohne Eingangsrechnungen entsprechend markiert.<br>
Beachten Sie unbedingt auch die Eingangsrechnungen die außerhalb des jeweiligen Auswertungszeitraumes liegen, gerade bei Abgrenzungen.<br>
Und beachten Sie auch die Werte auf nicht lagerbewirtschafteten bzw. nicht Lagerbewerteten Warenzugangspositionen.<br>
Bitte beachten Sie dass dies ein Stichtagswert ist. D.h. für monatliche Bilanzbetrachtungen sind die Änderungen gegenüber dem Vormonat zu buchen.<br>

#### Wie mit noch nicht fakturierten Lieferungen umgehen?
Wenn Sie gezwungen sind monatliche Bilanzen zu machen, so achten Sie, genauso wie bei den Jahresbilanzen darauf, dass Sie die gelieferten und verrechenbaren Waren soweit irgend möglich im Leistungszeitraum verrechnen.<br>
In Verbindung mit der Lagerbewertung, der Wareneinsatzberechnung bleibt dann oft die Frage, was ist mit den Lieferungen die Leihstellungen sind, Ware die bei Ihren Lieferanten zur Veredelung ist und ähnlichem.<br>
Bitte unterscheiden Sie hier grundsätzlich zwischen Lieferungen, also Lieferscheinen die nur eine Umbuchung zwischen Ihren Lagern auslösen und aus diesem Grunde nicht verrechenbar sind und den Lieferscheinen die Buchungen auf ein Lager auslösen, dessen Bestand dem / Ihrem Kunden gehört und daher verrechnet werden sollte. Nutzen Sie selbstverständlich auch die Möglichkeit Rechnungen rückdatieren zu können. Achten Sie dabei auf die Chronologie der Rechnungsdatum.<br>
Ein weiterer wesentlicher Punkt ist, erwarten Sie eine gelieferte Ware zurück (Leihstellung), oder ist / kann sie noch nicht verrechnet werden, oder ist es eine kostenlose Garantieleistung?

#### Was ist bei Änderungen im alten Jahr zu beachten?
Selbstverständlich muss auch bei der Bilanz Summengleichheit bei Aktiva und Passiva gegeben sein.<br>
Gerade wenn viele Abstimmungsarbeiten notwendig sind, die dann meistens auch noch im alten (Geschäfts-)Jahr stattfinden, wird oft vergessen diese ins neue Jahr weiter zu übertragen. D.h. prüfen Sie im Endeffekt nach Änderungen im alten Geschäftsjahr erneut die richtige Übernahme der Buchungen vom alten auf das neue Geschäftsjahr. Hier hilft ev. auch der Ausdruck der Saldenliste im neuen Jahr, in dem angezeigt wird, ob bei einem Konto Buchungen nach der Eröffnungsbuchung gemacht wurden.<br>
Gerade wenn die OP-Listen der Debitoren / Kreditoren ausgeglichen wurden, denken Sie an die erneuten Eröffnungsbuchungen der Debitoren / Kreditoren Konten.<br>
Bei diesen Korrekturen, ändert sich in aller Regel auch der Jahresgewinn. Daher ist auch dieser neu zu übernehmen.

#### Kann man die Abgrenzungsbuchungen auf einmal machen?
Ja. Es empfiehlt sich, ähnlich wie für die Lohnverrechnung die monatlichen Abgrenzungsbuchungen als eine Splittbuchung aufzusetzen und diese jedes Monat entsprechend wieder zu verwenden. Damit denken Sie immer an alle Punkte die für diese Buchung erforderlich sind und haben sofort die betroffenen Konten zur Hand.

#### Aktiva, Passiva, was ist was?
In der Bilanz werden Ihre entsprechenden "Bestände" und Verbindlichkeiten in einer Stichtagsbetrachtung möglichst transparent dargestellt.<br>
So spricht man von Aktiva für die Konten, deren Werte (Saldo) faktisch Ihnen / Ihrem Unternehmen gehören, hingegen stellt die Passiva Ihre Verbindlichkeiten dar.<br>
So finden Sie, im österreichischen Einheitskontenrahmen, z.B. die Unternehmenskredite immer in der 3er Kontenklasse oder auch die Verbindlichkeiten gegenüber Lieferanten.<br>
Hingegen sind die Forderungen an Ihre Kunden in der 2er Kontenklasse abgebildet.<br>
Ein Sonderfall ist in der Regel das Bankkonto. Dies ist je nach Kontostand eine Forderung der Bank an Sie oder es stellt ein Guthaben bei Ihrer Bank dar.

Übliche Definitionen, nach dem österreichischen Einheitskontenrahmen sind:

| Kontenklasse | Bilanzbereich | Bemerkung |
| --- |  --- |  --- |
| 0 | Aktiva |   |
| 1 |   |   |
| 2 | Aktiva |   |
| 3 | Passiva |   |
| 9 | Passiva |   |

Fehlermeldungen
---------------

#### FEHLER_FINANZ_EXPORT_FINANZAMT_NICHT_VOLLSTAENDIG_DEFINIERT
Beim verwendeten Finanzamt fehlt das Land. D.h. bitte hinterlegen Sie zumindest den Ort und damit das Land und die Postleitzahl damit **Kieselstein ERP** sich entsprechend verhalten kann.