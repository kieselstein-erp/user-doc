---
categories: ["Finanzbuchhaltung"]
tags: ["mehrere Finanzämter"]
title: "mehrere Finanzämter"
linkTitle: "mehrere Finanzämter"
weight: 500
date: 2023-01-31
description: >
 Besonderheiten bei mehreren Finanzämtern
---
Geschäftsfall mit mehreren Finanzämtern
=======================================

Es kommt, gerade für unsere Schweizer Anwender immer wieder vor, dass, vor allem als Kundenservice, eine Direktlieferung aus der Schweiz an einen Kunden innerhalb der EU geliefert wird. Wenn nun sowohl eine Schweizer Steuernummer als auch eine UID Nummer innerhalb der EU, z.B. Deutschland gegeben ist, stellt sich die Frage, wie dies verbucht werden sollte, da sowohl das Schweizer Finanzamt als auch das Deutsche Finanzamt die Umsätze in der jeweiligen UVA sehen möchte.

Wie sieht der Geschäftsfall eigentlich aus?

Ware befindet sich in CH ------> wird an einen Kunden z.B. in CZ geliefert (eine Steuernummer in DE (z.B. Konstanz) ist gegeben)

Zolltechnische Bewegung
Ware von CH ---> DE ..... von CH aus Lieferung nach Drittland .... von DE aus Import aus Drittland
Ware von DE ---> CZ ..... von DE aus IG-Lieferung

Genau betrachtet bedeutet dies, dass es sich um zwei Lieferungen innerhalb eines Unternehmens handelt. Diese werden wie folgt abgebildet:
- a.) Lieferung der Ware von CH ---> DE ... Erstellen einer Ausgangsrechnung vom eigenen Unternehmen mit CH Finanzamt an das eigene Unternehmen mit Adresse DE mit CH Finanzamt
- b.) Verbuchung der unter a.) erstellten Ausgangsrechnung als Eingangsrechnung im eigenen Unternehmen mit DE Finanzamt (nun befindet sich die Ware zolltechnisch in DE)
- c.) Lieferung der Ware von DE ---> CZ ... Erstellen der Ausgangsrechnung vom eigenen Unternehmen mit DE Finanzamt an den Endkunden, wobei sich dieser wiederum auf das DE Finanzamt beziehen muss. Dadurch wird die Lieferung für den Endkunden zum innergemeinschaftlichen Erwerb.

Wichtig:
Beachten Sie bitte dass jeweils der Kunde definiert, welches Finanzamt für die Buchung herangezogen wird.

Voraussetzung dafür:
- Sie haben als Kunden Ihr eigenes Unternehmen mit Adresse in DE (Ihr Zolllager) und CH Finanzamt angelegt (für die Erstellung der Ausgangsrechnung unter a.) ).
- Sie haben als Lieferanten Ihr eigenes Unternehmen mit Adresse in DE mit DE Finanzamt angelegt (für die Erfassung der Eingangsrechnung unter b.) )
- Sie haben den Endkunden (CZ) mit DE Finanzamt angelegt
- Beide (Kunde & Lieferant) haben als dahinter liegenden Partner den unter Mandanten angegebenen Partner verwendet.

Eigenheit die sich daraus ergibt:<br>
Der Umsatz ist, da ja zweimal die Ausgangsrechnung erstellt wird, doppelt vorhanden. Dies wird durch die unter b.) erstellte Eingangsrechnung (Drittlandserwerb) wieder ausgeglichen.
In der Buchhaltung muss das so abgebildet werden.<br>
Um dies in den Statistiken entsprechend darstellen zu können, müssen die betroffenen Partner als verbundene Unternehmen gekennzeichnet werden.<br>
In den Statistiken kann dies dann entsprechend ausgewählt werden (mit verbundenen Unternehmen).

Wie werden die beiden internen Rechnungen bezahlt?<br>
Die Rechnung an das eigene Unternehmen mit CH Finanzamt und DE Adresse entspricht ja der Eingangsrechnung mit DE Finanzamt. Damit diese, ohne Bewegung auf dem Bankkonto ausgeglichen werden, stehen zwei Möglichkeiten zur Verfügung:<br>
- a.) Sie buchen die Zahlung der Rechnung und der Eingangsrechnung über ein Durchläuferkonto als Bank
- b.) Sie buchen die Ausgangsrechnung (CH Finanzamt) als bezahlt. Hier wählen Sie als Art (der Zahlung) Gegenverrechnung aus und wählen dann die dagegen stehende Eingangsrechnung an. Damit sind beide Rechnungen ausgeglichen und als vollständig bezahlt verbucht.

#### Wie ist nun die genaue Vorgehensweise?
Gehen Sie bitte wie folgt vor:
- a.) Sie brauchen einen Kunden (I) der den Wortlaut Ihres Unternehmens hat, mit Adresse in DE und Finanzamt CH. Steuerkategorie Drittland (Erwerb)
- b.) Sie brauchen ein Lager in das die Ware vom Hauptlager (CH) ins Zolllager (DE) gebucht wird.
- c.) Erstellen Sie einen Lieferschein lautend auf den Kunden I mit Abbuchungslager Hauptlager und Ziellager Zolllager (CH)
- d.) Verrechnen Sie den (Umbuchungs-) Lieferschein
- e.) Der Endkunde muss sich (auch) auf das Finanzamt DE beziehen, Steuerkategorie EU mit UID
- f.) Erstellen Sie ev. einen Lieferschein an den Endkunden
- g.) Verrechnen Sie den Lieferschein
- h.) Erfassen Sie die Eingangsrechnung lautend auf den Lieferanten eigen DE. Es sollte dies der gleiche Partner sein wie der Kunde "eigen DE". Der Lieferant muss Finanzamt DE haben, damit es ein Drittlands-Erwerb wird.
- i.) Verrechnen Sie die Zahlung zwischen der Rechnung des Kunden "eigen DE" und der Eingangsrechnung (aus h.) )