---
categories: ["Finanzbuchhaltung"]
tags: ["Reverse Charge"]
title: "Reverse Charge"
linkTitle: "Reverse Charge"
weight: 300
date: 2023-01-31
description: >
 Das Reversecharge Verfahren und seine Abbildung im **Kieselstein ERP**
---
Reverse Charge in der integrierte Finanzbuchhaltung
===================================================

Nachdem das Reverse Charge Thema nicht ganz unkomplex ist und es gerne auch vermischt wird, hier eine Zusammenfassung unseres aktuellen Wissensstandes.

Vorne weg: Unterscheiden Sie ganz klar zwischen dem was Sie
-   verkaufen und
    -   was der Reverse-Charge Regelung unterliegt

und dem was Sie
-   einkaufen und
    -   was der Reverse-Charge Regelung unterliegt, was aktuell nur Österreich und hier nur das Inland betrifft.

je nachdem sind diese Dinge auf der UVA in den unterschiedlichen Bereichen anzugeben.

Beteiligte UVA Arten

Erlöse:    Umsatz Reverse Charge<br>
Aufwände: Reverse Charge (Bau-)Leistung, Schrott, Telekom(unikation)

#### Reverse Charge, was ist wofür?
In **Kieselstein ERP** können vier Reversecharge Arten definiert werden.
Umsatz Reverse Charge ist gedacht für Reverse Charge Leistung ins Ausland, also das Equivalent zur IG-Lieferung. Alle anderen sind für das Inland vorgesehen.

#### Welche Texte sind wann anzudrucken?
Nachdem immer wieder die Frage nach den entsprechenden Paragraphen und Texten kommt, hier die offiziellen Definitionen zu den verschiedenen Lieferungen.

§ 19 Abs. 1 zweiter Satz
Für Deutschland gelten nach dem SK03 folgende Steuertexte. Diese sind im Sinne von **Kieselstein ERP** wie folgt zu verstehen
Inlandserlöse                                               getrennt in 19%, 7% und 0%
Erlöse durch Lieferung ins EU-Ausland mit UID    Steuerfreie EU-Lieferungen, § 4,1b UStG
Erlöse durch Lieferung ins EU-Ausland ohne UID (sind immer Umsatzsteuerlich wie Inlandslieferungen zu behandeln)
Erlöse durch Lieferungen ins Drittland                Steuerfreie Umsätze § 4 Nr. 1a UStG

| Lieferung / Leistung / Land | Österreich | Kennzahl | Deutschland | Kennzahl | Schweiz / Liechtenstein | Kennzahl |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |
| Inlandslieferung | keine |   | keine |   | keine |   |
| Reverse Charge Inland Bauleistung | Kennziffer 48, §19 Abs. 1a (Bauleistungen) |   |   |   |   | gibt es nicht |
| Reverse Charge Inland Schrott | Kennziffer 32, §19 Abs. 1d (Schrott, Abfallstoffe, ...) |   |   |   |   | gibt es nicht |
| Reverse Charge Inland Telekom |   |   |   |   |   | gibt es nicht |
| Reverse Charge EU-Ausland Leistung | Gemäß §19 Abs. 1b geht die Umsatzsteuerschuld auf denLeistungsempfänger über. |   | Gemäß §13.B Abs. 2 geht die Umsatzsteuerschuld auf den Leistungsempfänger über. |   |   | gibt es nicht |
| Reverse Charge Drittland Leistung |   |   |   |   |   |   |
| IG-Lieferung (von Ware)In DE auch gerne EU-Lieferung genannt |   |   |   |   |   |   |

**Reverse Charge**

Unser aktueller Wissenstand (August 2013) bezüglich Reverse Charge für Österreich, Inland ist wie folgt<br>
Der aktuelle Stand der Ansicht der Finanz ist (das kann sich bitte monatlich ändern)
- a.) Wenn irgendwo was von Mehrwertsteuer drauf steht, kann passieren, dass das so gewertet wird, dass da Mehrwertsteuer enthalten ist und dass die dann, obwohl nur als MwSt Info ausgedruckt abgeführt werden muss.
- b.) Auf der UVA sind nur die Umsätze betreffend Reverse Charge getrennt anzuführen.
- c.) im Gesetz steht nichts von: Die UST muss "doppelt gebucht" werden (so wie die Einfuhrumsatzsteuer).

Das bedeutet:<br>
Inlands Reverse Charge Rechnungen sind als steuerfrei zu erfassen. Sowohl Eingangs-, als auch Ausgangs-Rechnungen<br>
Die Reverse Charge Umsätze werden auf ein eigenes Konto gebucht und können damit auch auf der UVA als eigener Posten angeführt werden.<br>
 **Info zu den Eingangsrechnungen:** IG-Erwerb ist der Innergemeinschaftliche Erwerb, das geht nur aus den EU-Mitgliedsländern, die nicht im Inland (Österreich) sind. Inlands-Rechnungen sind immer mit MwSt auszustellen, ausgenommen (und das auch nur in Österreich) wenn diese nach Reverse Charge zu behandeln sind.

Ad richtiger Ausweis auf dem UVA Formular inkl. Steuern<br>
Auch wenn die Buchung bei der ER als Steuerfrei zu erfassen ist, so ist für den doppelten Ausweis von Umsatz und theoretisch enthaltener Steuer bei der Variante (der UVA-Art) auf 20% zu stellen

**Hinweis:**
Die Reversechargearten sind "nur" für die automatische Verbuchung der Mehrwertsteuer relevant.<br>
Für den Ausweis der verschiedenen Reversechargearten auf der Umsatzsteuervoranmeldung ist nur die UVA Zuordnung der jeweiligen Konten relevant.

Üblicherweise werden Reversecharge-Eingangsrechnungen OHNE Vorsteuer (steuerfrei) erfasst. Der Ausweis der theoretischen Umsatzsteuer und der dagegen zu buchenden Vorsteuer, erfolgt nur im UVA Formular. Sollten Sie auf dieser doppelten Buchnug bestehen, so kann diese in den [Steuerkategoriedefinitionen]( {{<relref "/management/finanzbuchhaltung/integriert/steuerkategorie_definition">}} ) eingerichtet werden.

Hinweis:<br>
Sollten Sie für Ihre Buchhaltung eine weitere Reversechargeart benötigen, so kann dies von Ihrem **Kieselstein ERP** Betreuer gerne eingerichtet werden. Bitte wenden Sie sich vertrauensvoll an ihn/sie.