---
categories: ["Finanzbuchhaltung"]
tags: ["Liquiditätsvorschau"]
title: "Liquiditätsvorschau"
linkTitle: "Liquiditätsvorschau"
weight: 200
date: 2023-01-31
description: >
 Die Liquiditätsvorschau baut auf den offenen Ein- und Ausgangsrechnungen auf
---
Liquiditätsvorschau
===================

Im Modul Finanzbuchhaltung steht unter Journal die Liquiditätsvorschau als Erweiterungsmodul zur Verfügung.

Definieren Sie hier:
-   den aktuellen Kontostand Ihres Bankkontos
-   den Zeitraum wie weit in die Zukunft die Liquiditätsvorschau berechnet werden soll in Kalenderwochen
-   das Kreditlimit Ihres Bankkontos
-   die Betrachtung nach Zahlungsmoral oder Zahlungsziel
-   die Betrachtung der Eingangsrechnungen nach Rechnungsdatum oder nach Freigabedatum
-   ob eventuell eine Planungsdatei zusätzlich berücksichtigt werden soll
-   welche zusätzlichen Informationen noch ausgewertet werden sollen

Damit die Daten aus der Liquiditätsvorschau eine entsprechende Aussagekraft haben, beachten Sie bitte die [unten angeführten Punkte](#Aussagekraft).<br>
Mit Aktualisieren erhalten Sie eine Liste aller offenen Ein- und Ausgangsrechnungen sowie der Gutschriften, eventuell ER-Zusatzkosten und die sich aus den wiederholenden Aufträgen ergebenden zukünftigen Ausgangsrechnungen, sortiert nach Fälligkeitsterminen.

Der Fälligkeitstermin wird anhand des Belegdatums zuzüglich den Netto-Zieltagen aus dem Zahlungsziel errechnet.

Zusätzlich wird die Mehrwertsteuer ausgewiesen.

![](Liquiditaetsvorschau.gif)

Um insbesondere das Kreditlimit zu speichern nutzen Sie bitte die Speicherfunktion der Druck-Voreinstellungen.

In der Bankverbindung kann definiert werden, dass diese Konto bei der Liquiditätsvorschau berücksichtigt werden soll. 

Wählen Sie dazu im Modul Finanzbuchhaltung, unterer Reiter Bankverbindungen, die gewünschte Bank aus und wechseln in die Kopfdaten.

Nun können Sie einen Haken bei ![](liquiditaetsvorschau_saldo.JPG) Liquiditätsvorschau setzen.

Wenn Sie nun in das Journal wechseln, so finden Sie neben dem Kontostand ein Symbol zum Aktualisieren ![](liquiditaetsvorschau_aktualisieren.JPG).

Mit Klick auf Aktualisieren, wird der aktuelle Saldo des Bankkontos eingetragen. Wenn Sie bei mehreren Konten das Häkchen gesetzt haben, so wird der aktuelle Saldo aufsummiert.

**Hinweis:**

In der Liquiditätsvorschau können die Eingangsrechnungen und die Zusatzkosten auf Basis des Belegdatums (zzgl. Zieltagen) oder nach Freigabedatum betrachtet werden.

Die Berechnung der Liquiditätsvorschau kann nach der statistischen Zahlungsmoral jedes einzelnen Kunden oder nach dem Zahlungsziel erfolgen. (Bei einer Zahlungsmoral die immer Skonto bedingt, wird derzeit der Skontoaufwand nicht berücksichtigt.)

![](Liquiditaetsvorschau2.gif)

Für die Berechnung der Zahlungsmoral wird die gleiche Logik wie beim [Kunden](../Kunde/index.htm#Zahlungsmoral) verwendet.

Konnte für einen Kunden keine Zahlungsmoral errechnet werden, wird das laut Rechnung hinterlegte Zahlungsziel verwendet.

Für die Steuerung der Zahlungseingänge verwenden Sie bitte die Mahnsperre und den Zahlungsplan für die Ausgangsrechnungen.

**Berücksichtigung der Eingangsrechnungen** 

Hier kann die Berücksichtigung nach **Eingangsrechnungsdatum** oder nach **Freigabedatum** erfolgen.

Um auch die geplante Zahlung der Eingangsrechnungen entsprechend steuern zu können, steht die Möglichkeit der Auswahl des Bezugsdatums für die Fälligkeit der Eingangsrechnungen zur Verfügung. Wird das Freigabedatum verwendet, welches insbesondere im Falle zu früher Lieferantenlieferungen von Bedeutung ist, so erfolgt die Annahme der Zahlung entsprechend später. Bei der Berücksichtigung der Abgaben, hat dies keine Auswirkung, da ja für Sollversteuerer, das jeweilige Rechnungsdatum ausschlaggebend ist.

## Plankosten
<a name="Plankosten"></a>
Der Aufbau der Datei für die Plankosten ist wie folgt:

Es ist dies ein Spreadsheet im XLS Format.

Die Spalten sind wie folgt definiert, wobei die Reihenfolge und die Spaltenart fix definiert sind.

Beleg    Datum    Nettobetrag    MwSt Betrag    Beschreibung    Weitere Spalten können hinzugefügt werden.

Der Pfad zur Datei wird im Parameter IMPORT_PLANKOSTEN_DATEI hinterlegt. Bitte geben Sie hier den Pfad und den Dateinamen mit der Endung .xls an.

Um die Plankostendatei mit zu berücksichtigen muss ![](Liquiditaetsvorschau3.gif) mit Plankosten mit angehakt sein.

Die Daten aus den Plankosten werden in Blau dargestellt.

Um eine Trennung zwischen Ein- und Ausgängen zu erhalten muss in der Plankostendatei beim Beleg für den Zahlungsausgang ein ER (wie Eingangsrechnung) an der ersten Stelle des Beleges angeführt sein.

Da in diesen zusätzlichen Plankosten oft auch weitere Berechnungen hinterlegt werden, wurde dies so definiert, dass das erste Blatt des Spreadsheets für die Plankosten ausgewertet wird. Die weiteren Blätter stehen zu Ihrer Verfügung. Eine Musterdatei finden Sie [hier](Plankosten.xls).

An weiteren Auswertungen können berücksichtigt werden:
## mit offenen Aufträgen
![](Liq_mit_offenen_Auftraegen.gif)

Hier werden die offenen Auftragspositionen mit berücksichtigt. Als Zahlungseingangsdatum wird der Finaltermin des Auftrages zuzüglich der Einstellung der Berechnung der Zieltage (Zahlungsmoral oder Zahlungsziel) verwendet.<br>
Zusätzlich werden hier auch noch die Wiederholenden Aufträge in der Form berücksichtigt, dass angenommen wird, dass der Auftrag in seinem Wiederholungsintervall bis zum Ende des Betrachtungszeitraumes erhalten bleibt.<br>
Rahmenaufträge werden nicht berücksichtigt, da deren Verbindlichkeit in der Praxis nicht ausreichend genau bekannt ist.<br>
Ebenso werden Positionen der Forecastaufträge in der Liquiditätsvorschau nicht mitgerechnet.

## mit offenen Angeboten
![](Liq_mit_offenen_Angeboten.gif)

Hier werden die gewichteten Hoffnungsfälle zum Realisierungstermin berücksichtigt. D.h. der Angebotswert multipliziert mit der Auftragswahrscheinlichkeit zum Realisierungstermin. Als Zahlungseingangsdatum wird zum Realisierungstermin die Berechnung der Zieltage hinzugezählt.

## mit offenen Bestellungen
![](Liq_mit_offenen_Bestellungen.gif)

Hier werden:
- a.) die offenen Bestellpositionen so berücksichtigt, dass angenommen wird, diese werden gleichzeitig mit dem Liefertermin bzw. dem hinterlegen AB-Termin vom Lieferanten fakturiert. Hier wird noch das bei der Bestellung hinterlegte Zahlungsziel zur Errechnung der Fälligkeit der Zahlung hinzugezählt.
- b.) die offenen Wareneingangspositionen so berücksichtigt, dass diejenigen, die noch keine Eingangsrechnung hinterlegt haben so betrachtet werden, dass zum Wareneingangsdatum das Zahlungsziel aus der Bestellung hinzugerechnet wird um den Fälligkeitstermin zu errechnen.
Bei beiden Punkten gibt es von der Betrachtung her keine Liefertermine vor dem Auswertezeitpunkt. D.h. Sie finden die Fälligkeiten der Bestellungen immer erst nach dem Zeitraum des Zahlungszieles.
- c.) Bei offenen Bestellpositionen, wird der Saldo der Positionen, abzüglich des Saldos etwaiger Anzahlungsrechnungen in die Liquiditätsvorschau aufgenommen.
- d.) Rahmenbestellungen werden in die Liquiditätsvorschau nicht mit aufgenommen, da deren Fälligkeitstermine nicht wirklich bekannt sind.

## mit Abgaben
<a name="Abgaben"></a>
![](LiqVorschau_mit_Abgaben.gif)

Hier werden:
- a.) Die Nettogehälter, die Lohnsteuer und die Sozialabgaben (BruttoBrutto - Lohnsteuer - Nettogehälter) zu den im Personal unter Grunddaten, Zahltag definierten Terminen und Faktoren übernommen. [Siehe dazu]( {{<relref "/docs/stammdaten/personal/#gehalts-lohnkosten-definition" >}} ). Wichtig ist in den Gehältern, dass auch die Gehälter aller administrativen Mitarbeiter enthalten sind.
- b.) Die abzuführende UST (Umsatzsteuer) und die rückerstattete VST (Vorsteuer) wird anhand der Eingangsrechnungen, Zusatzkosten, (Ausgangs-)Rechnungen, (Ausgangs-)Gutschriften errechnet und zu den unter UST_STICHTAG bzw., UST_MONAT definierten Stichtagen in die Liquiditätsvorschau mit aufgenommen.

**Info:** Es wird bei den Abgaben grundsätzlich davon ausgegangen, dass diese zu den Stichtagen bezahlt werden.

Erscheint die Fehlermeldung:
![](LiqVorschau_Rechnung_Angelegt.jpg)<br>
So bedeutet dies, dass im Berechnungszeitraum mindestens eine noch nicht aktivierte Rechnung vorhanden ist. Um die Liquiditätsvorschau durchzuführen aktivieren oder stornieren Sie die angegebene Rechnung bitte entsprechend.

Für die Berechnung des Datums (Wochen) der Fälligkeit der Umsatzsteuer/Vorsteuer wird der Feiertagskalender in der Form berücksichtigt, dass an Wochenenden (Samstag, Sonntag) und an Feiertagen (egal welche Art von Feiertag) vorfällig (also früher) eingetragen wird.

Die Fälligkeit der Ust wird so errechnet, dass vom Zeitpunkt der Durchführung der Liquiditätsvorschau um UST_MONATE zurückgegangen wird. Ist nun der UST_STICHTAG bereits überschritten, so wird die UST für das Folgemonat errechnet und zum Stichtag in der Liquiditäsvorschau angeführt.

Übliche Einstellungen sind:
- für Österreich: UST_STICHTAG=15, UST_MONAT=2
- für Deutschland, ohne Dauerfristverlängerung: UST_STICHTAG=10, UST_MONAT=1
- für Deutschland, mit Dauerfristverlängerung: UST_STICHTAG=10, UST_MONAT=2

**Hinweis:** Um auch Nebenkosten wie Versicherungen usw. erfassen und daraus die Liquiditätsentwicklung berechnen zu können, sollten die Zusatzkosten im Modul Eingangsrechnungen verwendet werden.

#### Was ist für die Aussagekraft der Liquiditätsvorschau zu beachten?
<a name="Aussagekraft"></a>
Damit die in der Liquiditätsvorschau errechneten Zahlen auch tatsächlich stimmen, Sie Ihre Unternehmenssteuerung darauf abstellen können, ist es essentiell wichtig, dass alle Aufwände und Erlöse im ERP-Bereich von **Kieselstein ERP** erfasst werden. Daten die nicht in diesem Bereich erfasst werden (können) sollten in die [Plankosten](#Plankosten) (Planungsdatei / Steuerkalender) mit aufgenommen werden, um so eine realistische Darstellung der zukünftigen Liquiditätsentwicklung zu erhalten.

Gerade in Unternehmen die die Finanzbuchhaltung selbst durchführen, finden wir leider immer wieder die sehr einseitige Buchhaltungssicht, dass "nur" die Fibu stimmen muss. Dem muss entschiedenst widersprochen werden. Für die Steuerung des Unternehmens, gerade in Hinsicht auf zukünftige Entwicklungen ist deutlich mehr als nur das Verbuchen von Belegen erforderlich. Daher muss auf jeden Fall sichergestellt werden, dass alle Ein-/Ausgangsrechnungen und deren Zahlungen in/passend zu den jeweiligen **Kieselstein ERP** Modulen erfasst werden. Die Liquiditätsvorschau greift in der Auswertung (mit Ausnahme des Standes des Banksaldos) auf keine Informationen aus der integrierten Finanzbuchhaltung zu.

In diesem Zusammenhang weisen wir ausdrücklich darauf hin, dass Anzahlungen als Anzahlungsrechnungen (Ein- oder Ausgang) erfasst werden müssen, damit diese in der Liquiditätsvorschau richtig dargestellt werden. Werden nur Vorauszahlungen (Bank an Debitor) gebucht, so stimmt zwar der Kontostand, es wird jedoch nicht erkannt, dass dies eigentlich Geld ist, welches Sie von Ihren Kunden nur geliehen erhalten haben. D.h. es könnte sein, dass Sie dafür noch Leistung erbringen müssen. 

Wichtig daher: Wenn Sie Zahlungen von Kunden erhalten, die keinem Auftrag zugeordnet werden können, so müssen diese faktisch sofort dem Kunden rücküberwiesen werden (Eine Alternative wäre diese auf ein Konto umzubuchen, welches in der Liq.Vorschau NICHT berücksichtigt wird). Also: Wenn Zahlungseingang z.B. aufgrund von Angeboten, Auftragsbestätigungen erfolgt, dann ist in jedem Falle sofort eine Anzahlungs- (Ausgangs-)Rechnung mit Auftragsbezug zu erstellen. Ist dies nicht möglich, so sollten Sie das Geld sofort zurücküberweisen (oder die Geschäftsleitung informieren dass da zuviel Geld am Konto liegt).

In manchem Branchen wird dies von den Lieferanten leider auch in umgekehrter Weise gehandhabt. Gehen Sie bitte in jedem Falle auch hier analog zum für die Ausgangsrechnungen beschriebenen Vorgang vor.

#### Werden in der Gesamt-Liquiditätsvorschau Brutto oder Netto Beträge dargestellt?
In der Gesamt-Liquiditätsvorschau geht es um den Fluss von Geldbeträgen. D.h. es werden immer die erwarteten Zahlungsbeträge dargestellt, welche Brutto-Beträge sind. Als zusätzliche Information wird meistens auch der enthaltene Mehrwertsteueranteil mit ausgewiesen. Die Abgaben sind selbstverständlich ohne Mehrwertsteuer.

#### was bedeutet mit AB Zahl-/Zeitplan?
Ist diese Option vorhanden und angehakt, so werden die Zahlungsplan und Zeitplandaten aus den Aufträgen und aus den Bestellungen mit berücksichtigt.
Diese werden als
- ZP ... Zahlungsplan
- ZT ... Zeitplan

dargestellt, wobei ZP AB den Zahlungsplan aus der AB (Auftragsbestätigung) bedeutet und ZP BS den Zahlungsplan aus der Bestellung, also Zahlungsausgang.

ZT ... bedeutet die geplanten Materialkosten aus dem Zeitplan der AB. Achten Sie darauf, dass die geplanten Materialkosten sich nicht durch den Zahlungsplan aus der Bestellung doppelt verwendet werden.

Wieso werden die Anzahlungsbeträge der Bestellung doppelt dargestellt und noch dazu mit unterschiedlichem Vorzeichen.
![](erw_Liq_Vorschau1.jpg)

Sieht man sich die Bestellung 15/0000001 an, so sieht man daraus, dass:
- diese eine Anzahlungs-Eingangsrechnung (ER15/0000001) hat. Diese ist in der KW47 fällig
- auf der Bestellung ist bereits ein Wareneingang (BS15/0000001/1) verbucht, aber es ist die ER-Schlussrechnung noch nicht erfasst.
- daher muss vom zu bezahlenden Bestellwert die bereits erfasste Eingangsrechnung wieder abgezogen werden. Daher gibt es die Zeile BS15/0000001/AZ mit dem invertierten Betrag der Anzahlungsrechnung. Hintergrund dafür ist, dass mit dem Wareneingang eine Verbindlichkeit entstanden ist, der (in diesem Falle) noch keine Eingangs-(Schluss-)Rechnung gegenübersteht. Andererseits ist eben ein Teil davon durch die Anzahlungsrechnung bereits bezahlt. Die doppelte Darstellung wurde aus Transparenzgründen gewählt.

Warum ist die Bestellung 15/0000001 um zwei Wochen später fällig obwohl der Wareneingang schon längst erfolgt ist.<br>
Hier ist davon auszugehen, dass frühestens mit dem Tage der Erstellung der Liquiditätsvorschau die Eingangsrechnung dafür erfasst wird. Aufgrund des bei der Bestellung angegebenen Zahlungszieles (in diesem Fall 14Tage) ist der Wareneingang frühestens in zwei Wochen zu bezahlen.

#### Wie werden die Zahlungstermine errechnet?
Die Liquiditätsvorschau ist ganz wesentlich von den Zahlungs- bzw. Fälligkeitsterminen beeinflusst. Diese werden wie folgt ermittelt:

| Belegart | Beschreibung |
| --- |  --- |
| Ausgangsrechnung | Abhängig von der Einstellung Zahlungsmoral oder Zahlungsziel:Bei Zahlungsmoral wird die Zahlung nach den letzten Zahlungen des Kunden errechnet. Ergibt diese kein Ergebnis, wird das vereinbarte Zahlungsziel der Rechnung verwendet.Bei Zahlungsziel werden die Nettotage des Zahlungsziels zum Rechnungsdatum hinzugezählt.Dies gilt sowohl für (normale) Ausgangsrechnungen als auch für Anzahlungs- bzw. Schlussrechnung. Bei der Schlussrechnung wird nur der noch offene Betrag verwendet.Eine Ausgangsrechnung die mit heute (Datum der Liquiditätsauswertung) noch nicht bezahlt aber bereits früher fällig gewesen ist, wird mit heutigem Zahlungseingang angenommen. |
| Eingangsrechnung | Abhängig von Betrachtung nach Eingangsrechnungsdatum oder Freigabedatum werden zum jeweiligen Datum die Nettotage des Zahlungsziels hinzugezählt.Dies gilt sowohl für (normale) Eingangsrechnungen als auch für Anzahlungs- bzw. Schlussrechnung. Bei der Schlussrechnung wird nur der noch offene Betrag verwendet.Eine Eingangsrechnung die mit heute (Datum der Liquiditätsauswertung) noch nicht bezahlt aber bereits früher fällig gewesen wäre wird so angenommen, dass sie heute bezahlt wird. |
| Bestellung ohne Wareneingang | Hier wird angenommen, dass der Lieferant zum Bestell(Wunsch-)termin bzw. zum letzten bekannten bestätigten Termin liefert und mit gleichem Datum seine Rechnung (also Ihre Eingangsrechnung) erstellt. Daher werden zu den Terminen die Nettozahltage des Zahlungsziels der Bestellung hinzugerechnet und damit die Fälligkeit der Zahlung angenommen. Dies wird für die Bestellpositionen ermittelt die noch keinen Wareneingang haben. Ist der Liefertermin bereits überschritten (liegt also vor heute), wird angenommen, dass die Ware heute geliefert wird. |
| Wareneingang ohne Eingangsrechnung(szuordnung) | Sind in den Bestellungen bereits Wareneingänge verbucht, diese aber noch nicht als Preise erfasst MIT einer zugeordneten Eingangsrechnung eingetragen, so wird angenommen, dass Sie die Eingangsrechnung dazu heute (also zum Datum der Liquiditätsauswertung) noch bekommen. Daher werden hier die Nettozahltage anhand des Zahlungsziels der Bestellung von heute weg hinzugerechnet. |
| Wareneingang mit Eingangsrechnung(szuordnung) | Hier wird angenommen, dass der Wareneingang in seinem Wert vollständig in der Eingangsrechnung abgebildet ist. Daher wird dieser nicht berücksichtigt. |
| Bestellungen mit Eingangsanzahlungs-rechnungen | Hier ist der Sonderfall, dass die Bestellung oft noch vollständig offen ist, aber die Anzahlungseingangsrechnung bereits erfasst ist. Es wird dies daher so betrachtet, dass zum Liefertermin der ersten Position der Wert der Anzahlungsrechnung, da ja in den Eingangsrechnungen bereits erfasst, abzuziehen ist. Auch hier werden die Nettozieltage der Bestellung hinzugerechnet. |
| Bestellung Zahlungsplan | Zahlungsplanpositionen die noch nicht als erledigt abgehakt sind, werden zum Termin plus den Nettozieltagen der Bestellung als Fällig betrachtet.Da in der Regel solche Zahlungsplanpositionen nur gemacht werden, wenn Anzahlungs-Eingangsrechnungen dafür vereinbart sind, wird der freie Bestellwert um diese Netto-Beträge reduziert. Achten Sie daher darauf, dass die Bestellungszahlungsplanpositionen gleichlaufend mit der Erfassung der Anzahlungs-Eingangsrechnung erledigt werden.Ist der Termin bereits überschritten, wird angenommen dass Sie die Eingangsrechnung heute erhalten. |
| AuftragZahlungsplan | Zahlungsplanpositionen die noch nicht als erledigt abgehakt sind, werden so angenommen, dass die Rechnung zum gelegten Termin gelegt wird. Daher wird der wahrscheinliche Zahlungseingang je nach Einstellung Zahlungsmoral oder Zahlungsziel wie unter Ausgangsrechnung beschrieben ermittelt.Bitte achten Sie auch hier darauf, dass die Auftragszahlungsplanposition gleichlaufend mit der Erstellung der (Anzahlungs-)Ausgangsrechnung erledigt wird.Ist der Termin bereits überschritten so wird angenommen dass die Ausgangsrechnung heute erstellt wird. |
| AuftragZeitplan | Die zum Termin angegebenen Materialkosten werden so betrachtet, dass Sie zum angegebenen Termin eine Eingangsrechnung erhalten. Als Zahlungsziel wird das default Zahlungsziel für neue Lieferanten (System, Mandant, Vorbelegungen, Lieferanten) angenommen und daher die Nettotage zum jeweiligen Termin hinzugerechnet.Ist der Termin bereits überschritten, so wird angenommen, dass die Eingangsrechnung heute eintrifft. |
| Abgaben | Ust/Vst werden zu den in den Parametern angeführten Werten errechnet (UST_MONAT, UST_STICHTAG) Nettogehälter, Sozialversicherung, Lohnsteuer wird wie im Personal, Grunddaten, Zahltag definiert angenommen. Hier wird angenommen, dass diese Abgaben pünktlich bezahlt werden. D.h. dies ist der einzige Bereich in dem offene Zahlungen aus früheren Perioden nicht auf heute verschoben werden. |

### zusätzliche Plandaten
Liquiditätsvorschau zusätzliche Plandaten

Damit können zusätzliche Plandaten in der Liquiditätsvorschau berücksichtigt werden, welche sich nicht aus den "normalen" Ein-/Ausgangsrechnungen bzw. Personalkosten ergeben.<br>
Für die Berücksichtigung von offenen Aufträgen, offenen Bestellungen bzw. Wareneingängen verwende bitte die zusätzlichen Hakerl (Checkboxen).
Als Muster [siehe]((./Musterdateien//Plankosten.xls)