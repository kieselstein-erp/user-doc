---
categories: ["Finanzbuchhaltung"]
tags: ["Einfache Erfolgsrechnung"]
title: "Einfache Erfolgsrechnung"
linkTitle: "Einfache Erfolgsrechnung"
weight: 900
date: 2023-01-31
description: >
 Sehr einfache Erfolgsrechnung
---
Einfache Erfolgsrechnung
========================

#### Einfache Erfolgsrechnung
Unter Journal, einfache Erfolgsrechnung steht als Erweiterungsmodul in Verbindung mit der Liquiditätsvorschau auch eine sehr einfache Erfolgsrechnung zur Verfügung.<br>
Hier erhalten Sie für das gewählte Geschäftsjahr eine Gegenüberstellung Ihrer Erlöse und Aufwände jeweils je Monat.

**Hinweis:** Dies ist in keinem Falle eine G+V, Gewinn- und Verlustrechnung oder ähnliches. 

Diese Darstellung sollte eine sehr grobe Orientierung über die aktuelle Situation geben.
Für die Darstellung sind folgende Rechte erforderlich:
- Finanzbuchhaltung
- Chefbuchhalter
und das Zusatzmodul Liquiditätsvorschau.

D.h. es werden die monatlichen
- Erlöse
- Aufwände gegliedert in Eingangsrechnungen und Zusatzkosten
- Personalaufwände

dargestellt. Zusätzlich können noch die Lagerstände jeweils zum Monatsletzten und die Halbfertigfabrikatsinventur ebenfalls zum Monatsletzte, gegliedert in Material und Arbeit ausgeführt werden. Bitte beachten Sie, dass die Lagerstände und die Halbfertigfabrikatsinventuren jeweils eine Stichtagsbetrachtung sind und dass daher die Jahresauswertung entsprechend lange dauert.

In den Zusatzkosten werden anhand der Definition der Wiederholungsintervalle auch die geplanten Kosten mit angeführt.

Für die Personalkosten werden das Gehalt BruttoBrutto aus dem Reiter Gehalt im Modul Personal herangezogen. Es werden die Multiplikatorfaktoren (Personal, Grunddaten, Zahltag) berücksichtigt. Es wird aber der Gehaltsaufwand für das jeweilige Monat gerechnet.

Aus Erlöse (Ausgangsrechnungen abzgl. Gutschriften) minus Aufwände ergibt sich der Rohertrag.

Davon abgezogen werden die Personalkosten um den Ertrag pro Monat darzustellen.
Es ist dies eine sehr grobe Näherung, eher die Darstellung einer Tendenz (und in keinem Falle eine Erfolgsrechnung mit AfA usw.)<br>
Sie setzt selbstverständlich voraus, dass möglichst alle Aufwände in den **Kieselstein ERP** Modulen Rechnung, Eingangsrechnung, Zusatzkosten und die Personalkosten erfasst sind.<br>
In den Personalkosten sind keine ausbezahlten Überstunden oder ähnliches enthalten.

![](einfache_Erfolgsrechnung.jpg)