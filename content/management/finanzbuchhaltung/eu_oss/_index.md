---
categories: ["Finanzbuchhaltung"]
tags: ["EU OSS"]
title: "EU OSS"
linkTitle: "OSS"
weight: 1000
date: 2023-01-31
description: >
 One-Stop-Shop - Umsatzsteuer
---
Sammlung wie der One-Stop-Shop (MOOS) für österreichische Anwender, in Verbindung mit der integrierten Finanzbuchhaltung verwendet werden kann.

Siehe dazu bitte auch die Voraussetzungen laut [österreichischem Finanzamt](https://www.usp.gv.at/steuern-finanzen/umsatzsteuer/Umsatzsteuer-One-Stop-Shop.html)

Die wesentlichste Aussage ist, dass der OSS nur für nicht Unternehmer, also Privatpersonen und nur für Waren anzuwenden ist.<br>
Alles andere ist eine IG-Lieferung oder IG-Leistung ev. auch Reverse Charge.

- Zudem sind Umsätze, die über den OSS erklärt werden, in Österreich weder in die UVA noch in die jährliche Umsatzsteuererklärung aufzunehmen.

**Wichtig:**
Man muss sich zuerst für das EU-OSS registrieren. Erst danach kann über FinanzOnline die entsprechende Erklärung abgegeben werden.

## Erklärungszeitraum
Der Erklärungszeitraum ist das Quartal (Kalendervierteljahr):
- Erstes Quartal: Jänner-März
- Zweites Quartal: April-Juni
- Drittes Quartal: Juli-September
- Viertes Quartal: Oktober-Dezember 
Das Unternehmen hat nach Ablauf des Quartals einen Monat Zeit, die Erklärung abzugeben und die Steuer zu bezahlen.

## Was ist in der EU-OSS Erklärung anzugeben?
In der Erklärung sind der Mitgliedstaat, in dem die Steuer zu entrichten ist (Mitgliedstaat des Verbrauchs), die Bemessungsgrundlage für die Steuer (Netto – Umsatz in Euro) und der anzuwendende Steuersatz anzugeben. Wenn Leistungen nicht von Österreich aus, sondern von einer Betriebsstätte in einem anderen Mitgliedstaat ausgeführt werden, müssen Sie auch die Betriebsstätte samt ihrer UID-Nummer angeben. Bei Warenlieferungen ist der Mitgliedstaat anzugeben, in dem die Beförderung der Ware beginnt. Dies erfolgt durch Angabe der Betriebsstätte dieses Mitgliedstaates bzw. wenn das Unternehmen dort keine Betriebsstätte hat, durch Angabe des Abgangslandes.

## Welcher Steuersatz ist anzuwenden?
[siehe](https://ec.europa.eu/taxation_customs/tedb/taxSearch.html)<br>
siehe Rate Structure, hier sollte man schlauer werden bzw. [eine Spreadsheet Aufstellung](https://taxation-customs.ec.europa.eu/document/download/231d5d92-160f-4a7f-a104-5a87aba97735_en?filename=vat_rates_en.xls)
- Beträge sind in EUR anzugeben

[Tatsächliche Realisierung siehe unten](#realisierung)

## Gedanke:
Die nachfolgende Beschreibung ist der Einfachheit halber für Deutschland als Rechnungs- und Leistungsempfänger (also die Lieferadresse) erstellt. Kann analog natürlich auch für die anderen Europäischen Länder angewandt werden.<br>
Der Trick liegt darin, dass man:
- Ein eigenes Land für die verschiedenen EU-Ausländischen Länder für die du die Steuern abführen musst, anlegst. Z.B. Für Deutschland das Länderkennzeichen DEO. Dieses Land hat keinen Eintrag bei EU-Mitglied seit. Es darf auch nur als Inland verwendet werden.
- Nun legst du ein weiteres Finanzamt an, welches sich auf dieses Land bezieht.
- Wichtig: Die Finanzamts UVA Formularpflege benötigt eine spezielle Einstellung (Script)
- nun definierst du die Konten auf die diese "Inlands" Buchungen für das zweite Finanzamt (DE) gehen sollten
- die ausländischen Kunden mit dem neuen Länderkennzeichen anlegen und die Kunden ohne UID anlegt
- die Steuerkategorie der Debitorenkonten (der Kunden) stehen automatisch auf dem normalen Finanzamt und auf der Steuerkategorie Ausland setzen.
- alle MwStSätze der beteiligten = belieferten Länder anlegt und soweit möglichst bei den Kunden hinterlegt
- die Artikelgruppe entsprechend definiert
- nun für das Inlandserlöskonto die Länderart-Übersetzung **<u>nur</u>** für das  oben definierte Land der Steuerabfuhr machen, in unserem Beispiel also DEO. Somit sollte auch eine Fehlermeldung kommen, wenn ein neues Land dazu kommt, das man noch nicht übersetzt hat.
- womit sich auch ergibt, dass man je EU-Ausland für das man die jeweilige UST abführen muss, ein eigenes Sachkonto anlegen muss. Wenn man Normalsteuersatz und reduzierten Steuersatz hat, dann sind es jeweils zwei Konten
    - dafür auch die MwSt-Steuer-Varianten verwenden
    - vermutlich dafür auch eine zusätzliche UVA-Art über die DB anlegen. Z.B. EU-OSS<br>
    Tabellen: fb_uvaart und fb_uvaformular -> die OSS Werte sind in der UVA nicht anzugeben und auch nicht in der ZM
- Dadurch dass du deinen ausländischen (Inlands-) Konten das abweichende Finanzamt hinterlegst, bekommst du auch zwei UVA-Formulare, in denen dann die entsprechenden Steuern angegeben sind.
    - in der UVA als Summe je Land und Steuer
    - in der Ust-Verprobung dann detailliert (ähnlich der neuen UST-IG-Auswertung)
    Info: Für den Vergleich mit der Zahllast, sind die beiden Formularwerte zu addieren.

Damit sollte man die erforderlichen Daten eintragen können.


Vorgehensweise für eine Umstellung bei laufenden Daten:
- Rechnungen und deren Orte identifizieren
- in Land-PLZ-Ort die LKZ des jeweiligen Ortes auf z.B. DEO setzen
- Über die Rechnung auf den Kunden springen
    - hier die MwSt richtigstellen
    - und dann beim Debitorenkonto des Kunden das andere Finanzamt (z.B. DE) hinterlegen und die Steuerkategorie auf Inland stellen
- danach die Rechnung erneut den Beleg übernehmen<br>
![](Abweichendes%20Finanzamt%20beim%20Debitor.png)  

## Realisierung
Entgegen dem oben beschriebenen konnte das in folgender Weise umgesetzt werden
- Finanzamt z.B. OSS anlegen, mit Land DE
- Steuerkategorie einrichten, dass die Ust entsprechend gebucht wird.
- der Kunde wird ganz normal definiert aber mit dem für das Zielland passenden MwSt Satz
- die Länderartübersetzung wird entsprechend eingetragen
- die beiden betroffenen Sachkonten werden auf das Finanzamt DE gesetzt und die Ust Variante auf 19% bzw. 7%
- Der Trick ist, dass nur die beiden Sachkonten auf das Finanzamt OSS gehen
- und das Verbindlichkeitenkonto 2101
- und die beiden Sachkonten mit der DE Ust 3511 & 3512
- somit kann bei der UVA und auch bei der UST Verprobung das jeweilige Finanzamt ausgewählt werden
- für die Abgabe der UVA und der OSS Meldung sind immer beide Auswertungen heranzuziehen

## Fragen
### Kann man die ausländische Vorsteuer bei der EU-Oss abziehen
Laut Google kann die Vorsteuer im IG-Ausland nicht bei der OSS abgezogen werden.

Sollten für dich relevante Beträge auflaufen, so empfiehlt sich eine ausländische UID Nummer zu beantragen.

### Wo kann man in Österreich die OSS erfassen
Siehe Finanzonline, suchen nach OSS, damit gelangst du ins USP (Unternehmensservice Portal) in dem die OSS erfasst (Erklärung einreichen) werden kann.

## Einstellungen
In der Regel gibt es die Herausforderung, dass ein gewöhnlicher Verkäufer, gerade wenn kleine Beträge per Shop verkauft werden, auch die Kundendaten anlegen können muss, ohne dass er/sie einen Zugriff auf das Finanzbuchhaltungsmodul haben darf.

Daher haben wir hier den Weg gewählt, dass die beim Kunden zu hinterlegende Mehrwertsteuer-Bezeichnung auch ein Finanzamt definiert hat. Damit wird nun auch das Finanzamt für den Debitor (also die Abbildung des Kunden im Finanzbuchhaltungsmodul) definiert und die Steuerkategorie automatisch auf Inland (in Bezug auf das Finanzamt) gesetzt. Damit haben wir eine quasi Inlandslieferung mit der deutschen Mehrwersteuer von 19% bzw. 7%

### Mehrwertsteuerdefinitionen
Damit dein Kieselstein weiß welche Mehrwersteuersatz bei welchem Finanzamt anzuwenden ist, musst du dies unter System, Mandant, MwSt Bezeichnung beim Finanzamt hinterlegen.<br>
Du gehst dazu wie folgt vor:
- wähle das ausländische OSS Finanzamt (oder anlegen)
- definiere die OSS MwSt Sätze, in unserem Falle nun DE Allgemeine Waren bzw. DE Lebensmittel
- ordne den OSS MwSt Sätzen das OSS Finanzamt zu
- ordne nun den Inländischen MwSt Sätzen die Übersetzungen auf das andere Finanzamt zu. Solltest du mehrere ausländische OSS Verfahren anwenden müssen, so musst du diese eben mehrfach zuordnen.<br>
Die Zuordnung sieht dann z.B. wie folgt aus:<br>
![](MwSt_Definition.png)<br>

### Kundendefinitionen
Legst du nun einen Kunden für ein OSS Land an, so muss in der Mehrwertsteuerdefinition des Kunden die DE-Mehrwertsteuer (oder des jeweiligen Landes) angegeben werden. Siehe Kunde, Kopfdaten, ![](Kunden_MwSt_Definition.png)

### Anlegen weiterer OSS Finanzämter
Für das Anlegen weiterer OSS Länder kann gerne die Funktion neues Finanzamt verwendet werden.<br>
Damit die jeweilige UVA auch funktioniert, ist aktuell ein kleines Script erforderlich, welches die Daten in die Tabelle FB_UVAFORMULAR einträgt.<br>
Es sollte dies von jemanden gemacht werden, dem die Zusammenhänge in der Datenbank transparent sind.

```bash
-- Erweiterung Fibu UVA Formular für die weiteren Finanzämter
select c_name1nachnamefirmazeile1, * from lp_finanzamt 
inner join part_partner on part_partner.i_id=lp_finanzamt.partner_i_id
where mandant_c_nr='001';

select * from fb_uvaart where mandant_c_nr='001';	-- Die UVA Art ist NICHT vom Finanzamt abhängig

-- Eigentlich müssen "nur" die Einträge der Erlöskonten vom Haupt-Finanzamt kopiert werden
-- HauptFinanzamt = 15	(finanzamt_i_id)
-- OSS für FR = 5600
-- es gibt nur die uvaart_i_id 17 und 18 (Umsatz = Erlöse Reduziert- und Normal-Steuersatz)

-- Nachtragen für FR
INSERT INTO fb_uvaformular(
	i_id, finanzamt_i_id, mandant_c_nr, uvaart_i_id, i_sort, i_gruppe, c_kennzeichen, personal_i_id_anlegen, t_anlegen, personal_i_id_aendern, t_aendern)
	VALUES ((select max(i_id) from fb_uvaformular)+1, 5600, '001', 17, 5, 1, '29', 11, '2024-12-28 12:00:00', 11, '2024-12-28 12:00:00');
INSERT INTO fb_uvaformular	VALUES ((select max(i_id) from fb_uvaformular)+1, 5600, '001', 18, 4, 1, '22', 11, '2024-12-28 12:00:00', 11, '2024-12-28 12:00:00');
```