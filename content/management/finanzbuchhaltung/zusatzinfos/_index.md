---
categories: ["Finanzbuchhaltung"]
tags: ["Zusatzinfos"]
title: "Zusatzinfos"
linkTitle: "Zusatzinfos"
weight: 800
date: 2023-01-31
description: >
 Zusatzinfos rund um das Thema 
---

Zusatzinfos zu Themen rund um die Finanzbuchhaltung
===================================================

Diese Kapitel ist für die Beschreibung von besonderen Fällen und Situationen gedacht und wird laufend ergänzt.
Infos zum Thema Brexit siehe.

Was ist bei der Covid Mehrwertsteuerumstellung zu beachten?
-----------------------------------------------------------

Im Zuge der Corona Krise wurden für Deutschland die Sätze der allgemeinen Mehrwertsteuer für vorläufig sechs Monate von 19% auf 16% und für den reduzierten Steuersatz von 7% auf 5% gesenkt.
In gewissen Bereichen zieht auch Österreich mit einer Senkung der reduzierten Steuer von 10% auf 5% nach.
Es ist meiner Meinung nach damit zu rechnen, dass auch weitere Länder nachziehen, bzw. die Zeiträume verlängert werden.

Was ist dabei zu beachten, bzw. welche Fragen / Konstellationen wurden an uns herangetragen und wie sind diese zu Berücksichtigen:

Wichtig: Das Belegdatum in **Kieselstein ERP** ist auch immer als das Leistungsdatum zu betrachten, wird vor allem für die Ermittlung des jeweils gültigen Mehrwertsteuersatzes herangezogen. Daraus ergibt sich, dass Rechnungen, egal ob Eingangs-, Ausgangs-Rechnung, Zusatzkosten, Gutschriften bzw. Rechnungskorrekturen den zutreffenden Mehrwertsteuersatz, vom Datumsbereich her gesehen, immer anhand des Belegdatums ermitteln. In der Version ab Juli 2020 werden unterschiedliche Steuersätze aufgrund unterschiedlicher Lieferscheindatum berücksichtigt.
D.h. in besonderen Fällen erstellen Sie für Lieferungen bzw. Leistungen eben zwei Lieferscheine. Den ersten im alten Zeitraum (19%) und den zweiten im anderen Mehrwertsteuerzeitraum (16%).
Wichtig auch für die klare Abgrenzung zu welchem Datum / Steuersatz dies zu verrechnen ist.
Es geht bei den Ausgangsrechnungen immer darum, zu welchem Stichtag Ihr Kunde über die Ware / Leistung verfügen kann.

#### Müssen neue Mehrwertsteuerbezeichnungen angelegt werden?
Nein. Die Mehrwertsteuerbezeichnungen sind genau dafür gedacht, dass zeitliche Änderungen, die unseren Regierungen immer wieder mal einfallen, auf einfachste Weise abgebildet werden.
Das bedeutet, lege  **NUR** im Modul System, unterer Reiter Mandant, oberer Reiter MwSt ab 1.7.2020 den / die neuen MwSt Sätze NEU an. Es müssen auch die **alten** MwSt-Sätze **erhalten** bleiben.
**[Siehe dazu auch die unten angeführte Empfehlung](#Definition Mehrwertsteuersätze)**.

#### Kann ich Angebote von 19% auf Aufträge die mit 16% anzulegen sind, so wie üblich übernehmen?
Ja. Von **Kieselstein ERP** werden die Mehrwertsteuersätze entsprechend Ihren Definitionen konvertiert.

#### können aus Aufträgen mit 19% in Lieferscheine die mit 16% zu buchen sind übernommen werden?
Ja. Von **Kieselstein ERP** werden die Mehrwertsteuersätze entsprechend deiner Definitionen konvertiert.

#### Was passiert wenn Ware noch mit 19% versendet wird aber mit 16% abzurechnen ist?
Da es immer um das Leistungsdatum geht, also zu welchem Stichtag kann dein Kunde über die Ware, deine Dienstleistung verfügen, raten wir die Rechnung zumindest im gleichen Mehrwertsteuerzeitraum, also zum gleichen Mehrwertsteuersatz, zu verrechnen.
Gegebenenfalls durchbrechen Sie die Forderung nach der chronologischen Rechnungslegung (nur auf den ersten Blick) und erstellen noch eine Rechnung mit einem alten Belegdatum. Sollte dies erforderlich sein, so schreiben Sie bitte eine deutliche Begründung in, z.B. die Kopfzeilen, der Rechnung, in denen auch das tatsächliche Verrechnungsdatum angeführt ist. Bitte denken Sie immer daran, dass Sie bei der nächsten Steuerprüfung in 10Jahren, sofort und exakt wissen müssen, warum Sie damals (also dann vor 10Jahren) das so gemacht haben und warum Sie der Meinung waren, dass das damals richtig war. Beachten Sie dazu bitte unbedingt auch, dass gegebenenfalls die abzuführende Umsatzsteuer entsprechend anzupassen ist.

#### Was ist wenn mein Kunde mit Skonto bezahlt?
Das Skonto wird mit dem MwSt Satz der Rechnung verbucht.

#### wie ist mit Gutschriften umzugehen, wenn diese für Lieferungen und Leistungen die mit 19% erstellt wurden umzugehen?
Es gilt auch bei Gutschriften, also Rechnungskorrekturen, das Leistungsdatum. D.h. bitte erstellen Sie die Gutschrift mit dem für den MwSt-Satz gültigen Belegdatum. Bitte schreiben Sie unbedingt eine Begründung (siehe oben) und das tatsächliche Gutschriftsdatum in die Gutschrift. Denken Sie daran die Umsatzsteuer entsprechend anzupassen.

#### Zahlung per Vorauskasse, Bestellung 15.12.2020, Ich weiß jetzt schon, dass ich erst am 5.1.2021 liefern kann.
Ich muss die Anzahlungsrechnung am 15.12.2020 nun mit 19% ausstellen.
Erstellen Sie einen Lieferschein mit Lieferscheindatum 1.1.2021, mit einer Handeingabe definieren Sie den Anzahlungsbetrag, der dann mit 19% versteuert wird. Nun binden Sie, entgegen jeder unserer anderen Beschreibungen, diesen Lieferschein in die Anzahlungsrechnung ein. Damit wird er mit 15.12\. aber mit 19% verrechnet.
Versuchen Sie diese Situation zu vermeiden.

#### ich muss noch im Juni eine Lieferung machen und kann diese aber erst im August fakturieren?
Entscheidend ist hier, ab wann der Kunde über Ihre Lieferung / Leistung tatsächlich verfügen kann. Dazu gehört auch, dass er z.B. eine Freigabe über Ihre gelieferte Leistung erteilen muss, bzw. das Gerät erst nach umfangreichen Prüfungen in sein Eigentum übergeht.
Auch hier gilt das Leistungsdatum. Gegebenenfalls ändern Sie das Lieferscheindatum in den passenden MwSt-Satz-Zeitraum und schreiben dazu dass die eigentliche Lieferung schon früher erfolgt ist.

#### Ich habe im Juni eine Teststellung gemacht und kann diese nun nach Freigabe durch den Kunden im Juli fakturieren. Welcher MwSt Satz ist anzuwenden?
Siehe oben. Da der Kunde erst nach Freigabe über die Lieferung/Leistung verfügen kann, ist der für Juli gültige Satz anzuwenden (16%).

#### Mein Lieferant sendet mir eine Rechnung in der verschiedene Mehrwertsteuerperioden abgerechnet werden?
Bei der Ermittlung des Vorsteuerbetrages ist es grundsätzlich so, dass Sie nur die Vorsteuer anrechnen dürfen, welche gültig auf der Rechnung ausgewiesen ist. D.h. es ist eine entsprechende Splittbuchung bei der Erfassung der Eingangsrechnung durchzuführen.

![](Zusatz3.jpg)
So wurde in obigem Beispiel eine Eingangsrechnung z.B. mit Eingangsrechnungsdatum Juli noch mit 19% erfasst. Der exakte Mehrwertsteuerbetrag muss in diesem Falle manuell errechnet werden, da für den MwSt-Satz-Zeitraum die 19% nicht mehr ausgewiesen werden.
Wenn zwei unterschiedliche MwSt Sätze auf der Eingangsrechnung ausgewiesen werden, raten wir die Bezugswerte beider Nettobeträge anzuführen um so eine eindeutige Zuordnung darzustellen.

#### Sollte ich eine Dauerfristverlängerung beim deutschen Finanzamt beantragen?
Das ist, gerade jetzt für die Umstellungszeit, dringend anzuraten. Gegebenenfalls begründen Sie eine spätere Abgabe Ihrer Umsatzsteuervoranmeldung rechtzeitig bei Ihrem Finanzamt.

#### Sollte ich eine abgestimmte Jahres-UVA machen?
Dies ist dringend anzuraten.

#### Wie ist die österreichische Mehrwertsteuersenkung für die Bereiche Gastronomie, Beherbergung und Kultur zu definieren?
Definieren Sie bitte im System, Mandant, Mwst Bezeichnung eine neue Mehrwertsteuerart von Gastro, Beherbergung, Kultur.
![](Zusatz9.jpg)

Definieren Sie anschließend im Reiter Mwst den dafür ab 1.7.2020 gültigen Steuersatz von 5%.
![](Zusatz10.jpg)
Damit werden, vor allem für Eingangsrechnungen ab 1.7\. auch die 5% für Gaststätten und ähnliche Betriebe, die 5% als MwSt Satz mit angeboten
![](Zusatz11.jpg)

Für unsere Anwender der integrierten Buchhaltung, denken Sie bitte auch daran die Definition der Steuerkategorie zu ergänzen.
![](Zusatz12.jpg)

### Empfehlungen:
<a name="Definition Mehrwertsteuersätze"></a>

#### Definition Mehrwertsteuersätze
Nach heutigem Wissensstand sollte für Deutschland folgendes definiert sein:
![](Zusatz4.jpg)

Bitte beachten Sie, dass hinter der Handeingabe eine spezielle Definition steckt. Sollte diese in Ihrer **Kieselstein ERP** Installation nicht mehr vorhanden sein, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer (lp_mwstsatzbez.B_HANDEINGABE=1)

Es sollte die Handeingabe für den Mehrwertsteuersatz ebenfalls definiert sein und damit das passende Vorsteuerkonto (im SKR04 1400). Hier muss dann manuell der MwSt Betrag mit 0,00 eingegeben werden.
![](Zusatz1.jpg)

![](Zusatz2.gif)

[Für die Definition der Steuerkategorie und der Länderarten siehe bitte]( {{<relref "/management/finanzbuchhaltung/integriert/steuerkategorie_definition">}} ):

#### Langfristiger Normalsteuersatz?
Wozu denn das ?
Der deutsche Gesetzgeber ist der Meinung, dass Leistungen die zwar während der Corona Krise verrechnet werden, aber auch in den Zeitraum danach aus Mehrwertsteuersicht reinreichen, dann eben mit dem Steuersatz der zum Ende des Zeitraumes gilt zu verrechnen sind. D.h. es wird z.B. unsere Softwarepflege immer für 12Monate verrechnet, also sind diese, auch wenn sie im Jul 2020 ausgestellt werden mit 19% zu verrechnen.
Dies ist auf den Elsterformularen entsprechend anzugeben.

Natürlich hat das zwei Seiten:
a.) Die Erstellung der Ausgangsrechnung
Dafür müssen

b.) Die Erfassung der Eingangsrechnung
![](langfristiger_Normalsteuersatz.jpg)
Zusätzliches Vorsteuerkonto 19% langfristig anlegen mit gültig ab 1.7.2020

#### Wie ist das im Zusammenhang mit IG Erwerb?
Auch hier ist es so, dass Sie beim Erwerb ja die theoretische Umsatzsteuer angeben müssen. Das bedeutet, es lebe der Amtsschimmel, dass für alle IG-Erwerbe im Zeitraum 1.7.-31.12.2020 ein theoretische Umsatzsteuersatz von 16% auszuweisen ist.

<a name="Fremde Vorsteuer"></a>

#### Wie ist die Vorsteuer aus fremden Ländern zu buchen?
Weil es immer mehr Menschen machen und es ein Finanzstrafvergehen ist.
Ich darf mir nur dann Vorsteuer vom Finanzamt holen, wenn das ein anderer Unternehmer dorthin schon abgeführt hat. D.h. wenn z.B. falsche Werte auf den Belegen stehen, sollte ich den niedrigeren nehmen, wenn ich Vorsteuer von einem anderen EU-Land (oder EWR oder...) ausgewiesen habe, ist das zwar nett, kann aber nur im Zuge der Umsatzsteuer Rückerstattung angefordert werden. Hier wird gerne vom jeweiligen Land massivste Schikane betrieben. Außerdem gibt es Mindestwerte (50,- € rückzufordernder Betrag) ab denen diese Beträge überhaupt behandelt werden.
Zu erfassen ist das immer mit steuerfrei und dem Bruttobetrag, denn es ist reiner Aufwand.
Für Details wenden Sie sich bitte an Ihren Steuerberater.

Wie vorgehen für die Einrichtung der MwSt ab 1.7.2020 ff
--------------------------------------------------------

Der Versuch einer kompakte Beschreibung wie für die Einrichtung der sich durch die mitten im laufenden Kalenderjahr überfallsartig beschlossenen Mehrwertsteueränderungen ergebenden Änderungen. Bitte beachten Sie dazu auch immer die sich doch stetig ändernden Anweisungen der Finanzbehörden.
Besonders beachtenswert finde ich die Anweisung der deutschen Behörden bzgl. Subskriptionen und länger dauernden Dienstleistungen. Nicht nachvollziehbar ist die Ausführung bezüglich Anzahlungsrechnungen für Projekte die voraussichtlich!! in der zweiten Jahreshälfte 2020 abgeschlossen werden.

1.  Drucken Sie die UST-Verprobung. Kommen hier Fehlermeldungen müssen diese zuerst bereinigt werden.
2.  Sollten für die nun neuen unterschiedlichen Steuersätze auch unterschiedliche Konten verwendet werden?
    Ja natürlich und selbstverständlich.
    Gerade in dieser Umstellungsphase hilft Klarheit, Struktur und Transparenz, vor allem auch um sicher zu sein, dass nach bestem Wissen und Gewissen alles verbucht ist.

3.  Einspielen der FB_UVAFORMULAR Definition für alle Finanzämter und alle Mandanten

4.  definieren der 16% Erlös- und Skontokonten

5.  definieren der Mwst-Satz-Bezeichnung Langfristiger Normalsteuersatz gültig ab 1.7.2020 und 19%

6.  definieren der 16% / 5% / 19% Vorsteuerkonten
    Hinweis: Definieren Sie bei den einzelnen Vorsteuerkonten die UVA Art Vorsteuerkonto und die UVA Variante mit 0% Steuer.
    Das Vorsteuersammelkonto darf keiner UVA Art zugeordnet sein.

7.  definieren der Skontoerlöskonten für 16%, 5%, 19% langfristig

8.  definieren der Steuerkategoriezuordnungen ab 1.7.2020

9. definieren der Konto-Länderart bzw. Konto-Land-Übersetzungen ab 1.7.2020

10. definieren der zusätzlichen IG-Erwerbskonten (auch hier muss die Trennung in 16% erfolgen)

11. zuordnen der UVA Varianten für die jeweiligen Erlös-, Vorsteuer-, IG-Erwerbs-Konten

12. Drucken Sie die Saldenliste quer, hier interessiert vor allem die letzte Seite und dann
    die Ust-Verprobung für die jeweiligen Finanzämter und
    die UVA für die jeweiligen Finanzämter.
    Prüfen Sie akribisch ob die Daten in sich schlüssig sind.

Was bedeutet:
![](UVA_Variante1.jpg)
Hier werden die Zeilen deswegen in orange angedruckt, da einerseits Umsätze auf dieser UVA Art definiert sind, aber andererseits keine UVA Variante definiert ist. D.h. hier müssen bei allen Konten die Umsätze in diesem Zeitraum haben und die der UVA Art Bezeichnung z.B. EU Ausland mit UID zugewiesen sind, zumindest die UVA Variante steuerfrei definiert werden.
D.h. es ist bereits in der Auswahlliste der Sachkonten ersichtlich, dass
![](UVA_Variante2.gif)
Für das Konto der UVA Art EU Ausland mit UID keine UVA Variante definiert ist.
Tragen Sie daher in den Kontokopfdaten
![](UVA_Variante3.gif)
die passende UVA Variante ein. In diesem Beispiel Steuerfrei.

Somit ändert sich die Darstellung auf
![](UVA_Variante4.jpg)
In gleicher Weise gehen Sie auch bei den anderen, unvollständig definierten Konten vor.

UVA Art hat keine Entsprechung im UVA Formular bewirkt
![](UVA_Variante5.jpg)
dass dies wie oben dargestellt angedruckt wird. Bitte die UVA Arten bereinigen. D.h. entweder die UVA Art entfernen oder die Tabelle FB_UVAFormular ergänzen.

Ein weiteres Beispiel aus der österreichischen UVA:
![](UVA_Variante6.jpg)

die Ursache für die nicht definierte (MwSt-) Variante finden Sie am einfachsten in dem Sie
![](UVA_Variante7.jpg)
- die Sachkonten wählen
- Konten mit Buchungen im gewählten Geschäftsjahr anhaken (damit kommen nur die Konten mit Werten in diesem Geschäftsjahr)
- die Sachkonten nach der UVA Art sortieren
- nun gehen Sie auf die fehlerhafte (orange hinterlegte) UVA Art. Definieren Sie bei dem Konto bei dem keine Variante definiert ist die entsprechende Variante der Steuersätze.

Wie bekommt man die leere UVA Zeile weg?

![](UVA_Variante8.jpg)
Wird in der UVA, meist bei IG-Erwerb oder bei Vorsteuer auch eine Zeile ohne Variante und mit einem Umsatz bzw. Steuer von 0,00 angezeigt, so bedeutet dies, dass es Konten der UVA-Art (Bezeichnung) gibt, die zwar der UVA-Art zugewiesen sind, in dem UVA Zeitraum keine Buchungen haben und bei denen die MwSt-Variante nicht definiert ist. D.h. bitte tragen Sie die jeweils richtige Variante nach.

Wie die UVA im Elster ab Juli 2020 eintragen?
---------------------------------------------

Im Elster sind eigentlich nur die 19% und die 7% richtig abgebildet. Das bedeutet, dass die Kennziffer 81 für die 19%igen steuerpflichtigen Umsätze zu verwenden ist. Hier wird die sich daraus ergebende MwSt automatisch errechnet. Die steuerpflichtigen Umsätze mit 16% und mit 5% sind in ihrem Nettobetrag unter Kennziffer 35 und deren gesamte Steuer (also die MwSt für die 16% und für die 5% gemischt) in der Kennziffer 36.
Ähnlich ist für die IG-Erwerbe vorzugehen. D.h. die IG-Erwerbe zur Basis 19% sind in der Kennzahl 89 einzugeben (MwSt wird automatisch errechnet). Die IG-Erwerbe mit den theoretischen 16% und mit den theoretischen 5% sind in ihrem Nettobetrag unter der Kennziffer 95 und deren gesamte Steuer unter der Kennziffer 98 anzugeben.

<a name="Brexit"></a>Brexit
---------------------------

Nachdem das vereinigte Königreich nun endgültig die Europäische Union verlässt sind ab 1.1.2021 folgende Punkte zu beachten:

-   Anwender ohne integriertem **Kieselstein ERP** Finanzbuchhaltungsmodul.
    Hier muss in den Ländereinstellung nur das Austrittsdatum mit 31.12.2020 eingetragen werden. [Siehe dazu](../System/index.htm#Brexit).

-   Anwender mit integrierter **Kieselstein ERP** Finanzbuchhaltung müssen bitte folgendes beachten:
    Da durch den Austritt Großbritannien ab 1.1.2021 wiederum als Drittland zu betrachten ist, müssen diejenigen Kunden für die dies zutrifft auch als Drittland definiert werden.
    Das bedeutet, dass, da ja für die bisherigen Buchungen 10(7) Jahre Aufbewahrungspflicht besteht, alle diese Debitoren neu angelegt werden müssen. Genauer: Wenn diese Kunden bisher aufgrund ihrer UID Nr (Umsatzsteueridentifikationsnummer des Kunden) als Innergemeinschaftliche Lieferung / Reverse Charge behandelt wurden.
    Wir empfehlen diese Kunden neu anzulegen, hierbei KEINE UID Nummer mehr anzugeben und auch neue Debitorenkonten zu vergeben. Damit ist der Vorgang für alle Transparent.
    Um dies in den Konten und Kunden-Auswahllisten übersichtlich darzustellen, können diese Kunden und die Debitorenkonten auf versteckt gesetzt werden.
    Bitte bedenken Sie in diesem Zusammenhang auch eventuelle Skontoaufwände, welche, wenn die Ursprungsrechnung noch vor dem 1.1.2021 war, auf die Innergemeinschaftliche Lieferung angerechnet werden muss, was wiederum für die Anlage eines neuen Kunden spricht.

Bitte beachten Sie in beiden Fällen auch, dass die Zusammenfassende Meldung (ZM) entsprechend zu berücksichtigen ist. So ergibt sich aus diesem Titel, dass in jedem Falle (egal ob mit oder ohne integrierter Finanzbuchhaltung) ein neuer Debitor angelegt werden sollte.
Wir raten auch unbedingt dazu, ev. noch nicht verrechnete Lieferscheine aus 2020 mit Rechnungsdatum 2020 abzurechnen. Alles andere würde nur mehr kompliziert werden.

Das Eoss Verfahren
------------------

Ab 1.7.2021 ist das OSS (One Stop Shop) Verfahren für Lieferungen an Endkunden in andere Mitgliedsländer der EU anzuwenden, wenn ihr jährlicher Umsatz über Internetplattformen 10.000,- € / Jahr überschreitet. Daraus ergibt sich, dass für Kunden OHNE UID Nummern, also typischen B2C Geschäften, bei der jeweiligen Rechnungsposition (idealerweise definieren Sie dies bereits ab Angebotsposition entsprechend) die Mehrwertsteuersatz des Empfängerlandes zu hinterlegen ist.
Laut <https://www.wko.at/service/steuern/Mehrwertsteuersaetze_in_der_EU.html> sind aktuell folgende MwSt Sätze in den verschiedenen EU Ländern definiert:

| **Mitgliedstaat** | **Bezeichnung der USt** || **Steuersatznormal** | **Steuersatzermäßigt** |
| --- |  --- | --- |  --- |  --- |
| **Belgien** | taxe sur la valeur ajoutée (TVA) oder belasting over de toegevoegde waarde (BTW) ||        21 |        12/6 |
| **Bulgarien** | Danak varhu dobavenata stoynost (DDS) ||        20 |          9 |
| **Dänemark** | Meromsætningsafgift (MOMS) ||        25 |          - |
| **Deutschland** | Umsatzsteuer (USt) ||        19 |         7 |
| **Estland** | Käibemaks (KM) ||        20 |          9 |
| **Finnland** | Arvonlisävero (ALV) ||        24 |        14/10 |
| **Frankreich** | taxe sur la valeur ajoutée (TVA) || 20  |  10/5,5/2,1 |
| **Griechenland** | Foros Prostithemenis Axias (FPA) ||        24       |      13/6       |
| **Griechenland - Lesbos, Chios, Samos, Kos und Leros** | Foros Prostithemenis Axias ( FPA) ||       17 bis 31.12.2021 |       9/5 bis 31.12.2021 |
| **Irland** | value added tax (VAT) ||        23 |  13,5/9/4,8 |
| **Italien** | imposta sul valore aggiunto (IVA) | 22 ||    10/5/4/0 |
| **Kroatien** | porez na dodanu vrijednost (PDV) |         25 ||         13/5 |
| **Lettland** | Pievienotas vertibas nodoklis (PVN) |         21 ||        12/5 |
| **Litauen** | Pridétinés vertés mokestis (PVM) |         21 ||          9/5 |
| **Luxemburg** | taxe sur la valeur ajoutée (TVA) |         17     ||     14/8/3 |
| **Malta** | value added tax (VAT) oder taxxa fuq il-valur miújud |         18 ||        7/5/0 |
| **Niederlande** | Belasting Toegevoegde Waarde (BTW) |         21 ||           9 |
| **Nordirland** | value added tax (VAT) |  | 20 |         5/0 |
| **Österreich** | Umsatzsteuer (USt) |         20 ||    13/10        5      bis 31.12.2021 |
| **Polen** | podatek obrotowy |         23 ||        8/5/0 |
| **Portugal - Festland** | Imposo sobre o Valor Acrescentado (IVA) |         23 ||         13/6 |
| **Portugal - Madeira, Azoren** | Imposto sobre o Valor Acrescentado (IVA)  | 18Azoren22Madeira || 9/4Azoren12/5Madeira |
| **Rumänien** | Taxa pe valoarea adăugată (TVA) |         19 ||        9/5 |
| **Schweden** | Mervärdesskatt (moms) |         25 ||      12/6 |
| **Slowakei** | Dan z pridanej hodnoty (DPH) |         20 ||      10 |
| **Slowenien** | Zakon o davku na dodano vrednost (DDV) |         22 ||        9,5/5 |
| **Spanien** | impuesto sobre el valor añadido (IVA) |         21 ||       10/4 |
| **Tschechien** | dan z pridane hodnoty (DPH) |         21 ||       15/10 |
| **Ungarn** | Általános forgalmi adó (áfa) |         27 ||       18/5 |
| **Zypern** | Foros Prostithemenis Axias (FPA) |  19 ||     9/5 |

Um die MwSt Sätze anzulegen, wechseln Sie bitte in das Modul System, im unteren Modulreiter Mandant dann auf Ihren jeweiligen Mandanten und dann auf den Reiter Mwst Bezeichnung. Hier legen Sie alle benötigten MwSt Bezeichnungen an. Unsere Empfehlung, dies an der Bezeichnung der österreichischen MwSt anzupassen.
So z.B. Allgemeine Waren DE.
Nach der Anlage der Bezeichnung muss zusätzlich der ab gültige Steuersatz im Reiter Mwst definiert werden.

Um alle europäischen Sätze in einem Durchlauf anzulegen, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

Wichtig:
Wenn deine **Kieselstein ERP** Installation so eingestellt ist, dass die Mehrwertsteuer-Definition direkt aus dem Kunden kommt, so hinterlege beim jeweiligen ausländischen Kunden die entsprechenden Mehrwertsteuersätze. (Parameter KUNDEN_POSITIONSKONTIERUNG = 0)
Ist deine **Kieselstein ERP** Installation jedoch so eingestellt, dass diese aus dem Artikel kommt, weil z.B. Geräte und Lebensmittel verkauft werden, so muss in der aktuellen Version, dies je Belegposition (Angebot - Rechnung) manuell richtig eingestellt werden. Für eine Erweiterung wenden dich bitte vertrauensvoll an deinen **Kieselstein ERP** Betreuer.

Werk- oder Montagelieferung
---------------------------

Nachfolgend eine Information der Wirtschaftskammer Oberösterreich vom Oktober 2021, welche unter Umständen die österreichischen Anlagenbauer betreffen kann.<br>
**Neue Definitionen von Werk- und Montagelieferungen in Deutschland und die Folgen.**

Aufgrund der Rechtsprechung des deutschen Bundesfinanzhofs (BFH) wurden der Umsatzsteuer-Anwendungserlass (UStAE) geändert. Es gibt eine neue Definition einer **Werklieferung** mit schwerwiegenden Folgen für österreichische Unternehmer - es kann zu einer Pflicht zur Umsatzsteuerregistrierung in Deutschland führen.<br>
Man muss zwischen Werklieferung und Montagelieferung unterscheiden. In beiden Fällen besteht die Umsatzsteuerpflicht in Deutschland -- zum Übergang der Steuerschuld kommt es allerdings nur bei der Werklieferung. Im Falle der Montagelieferung ist eine Registrierung zur Umsatzsteuer beim Finanzamt München 2 erforderlich.<br>
Eine Werklieferung liegt vor, wenn "wenn der Werkhersteller für das Werk einen fremden Gegenstand be- oder verarbeitet und dafür selbstbeschaffte Stoffe verwendet, die nicht nur Zutaten oder sonstige Nebensachen sind (vgl. BFH-Urteil vom 22\. 8\. 2013, V R 37/10, BStBl 2014 II S. 128). Leider fehlt eine Definition, wann ein fremder Gegenstand als be- und verarbeitet gilt. Dies ist einerseits der Fall, wenn der fremde Gegenstand im Werk untergeht. Es reicht aber auch eine feste Verbindung der vom Lieferanten beschafften Gegenstände mit dem fremden Gegenstand. Ein bloßes Anschrauben reicht nicht, die Verbindung muss so fest sein, dass der Gegenstand beim Lösen zerstört wird. Die Größe und das Gewicht alleine reicht nicht, um von einer festen Verbindung zu sprechen.<br>
Als Werklieferung könntet zum Beispiel der Einbau eines (für eine bestimmte Produktionsanlage konzipierten) Schaltschrankes in diese Produktionsanlage sein.<br>
Die Lieferung einer Maschine in Einzelteilen, die beim Kunden zusammengebaut wird ohne dass der Kunde selbst Gegenstände beistellt ist also eine Montagelieferung, die eine Registrierungspflicht in Deutschland seit 1.7.2021 zur Folge hat.<br>

Das bedeutet in weiterer Folge, dass du:
1. eine deutsche Steuernummer benötigen, also auch eine DE-UiD Nummer
2. beginnend vom Angebot die Montageleistungen mit der deutschen MwSt von 19% ausweisen müssen
3. diese Info auch in die Finanzbuchhaltung durchgereicht werden muss und damit natürlich auch eine deutsche UVA (Elster) abzugeben ist. Beachte bitte, dass in Deutschland ohne Dauerfristverlängerung dies bis zum 10\. des Folgemonates (also am 10.Februar für den Jänner) zu erfolgen hat. Tipp: Dauerfristverlängerung beantragen.

**WICHTIG:** Bitte beachte, dass im deutschen Umsatzsteuerrecht immer das Kalenderjahr als Bezugsbasis dient, egal wie dein Geschäftsjahr definiert ist.
Nutze in dem Zusammenhang auch, dass du nun für alle deutschen Eingangsrechnungen entsprechend ebenfalls sofort den Vorsteuerabzug geltend machen kannst.

Ergänzend dazu die Info eines deutschen Steuerberaters:

Montage aus EU nach DE: §3 Abs. 7 UstG: Der Lieferant muss eine deutsche UID Nummer haben, in Deutschland beim Finanzamt (München) registriert sein. Die Rechnung muss wie eine deutsche Inlandsrechnung mit 19% Umsatzsteuer ausgestellt werden und selbstverständlich ist, das an das deutsche Finanzamt mittels Elster zu melden.

Unterscheidung ob Montagelieferung oder innergemeinschaftliche Lieferung:

Unser aktueller (Ende 2021) und absolut unverbindlicher Wissensstand dazu ist folgender:<br>
Wird eine Maschine geliefert und diese wird nur angeschlossen, so ist es eine innergemeinschaftliche Lieferung, da sie ohne eine zerstörende Handlung setzen zu müssen, wieder abgebaut werden kann.<br>
Wird hingegen die gleiche Maschine geliefert, aber hier z.B. ein Rohr, eine Verstrebung angeschweißt, so kann die Maschine, ohne diese Verstrebung durchzusägen nicht mehr abgebaut werden, daher ist es eine Montagelieferung und ist auf Basis des deutschen Inlandssteuersatzes zu versteuern und die Ust entsprechend abzuführen.

Wie kann das nun im **Kieselstein ERP** entsprechend dargestellt werden?

Für **Kieselstein ERP** Anwender ohne integrierter Finanzbuchhaltung und mit wenigen deutschen Montagelieferungen ist es vermutlich das einfachste, wenn einfach der benötigte deutsche Mehrwertsteuersatz angelegt wird, und dieser individuell bei der jeweiligen Position hinterlegt wird. Üblicherweise wird es für die Anlagenbauer so sein, dass gegebenenfalls der Kunde bereits auf die entsprechende Mehrwertsteuersatzbezeichnung gesetzt wird.<br>
Für die **Kieselstein ERP** Anwender mit integrierter Finanzbuchhaltung bedeutet diese Forderung des deutschen Finanzamtes, dass die UVA (Umsatzsteuervoranmeldung) auch für das deutsche Finanzamt unter der deutschen Steuernummer abzugeben ist. Das bedeutet, dass ein weiteres Finanzamt anzulegen ist. Das bedeutet auch dass das jeweils richtige Finanzamt dem jeweiligen Kunden / Lieferanten zugewiesen werden muss. Haben Sie nun Kunden die Sie einmal mit Montagelieferung beliefern und das andere Mal mit IG-Lieferung, oder auch Kunden die von Ihren Lieferanten innerhalb Deutschlands beliefert werden, so sind diese Kunden doppelt anzulegen, da sie einmal unter den österreichischen Umsatzsteuerregeln fallen und das andere Mal unter die deutschen Umsatzsteuerregeln fallen.