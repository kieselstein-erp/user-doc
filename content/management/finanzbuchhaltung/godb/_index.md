---
categories: ["Finanzbuchhaltung"]
tags: ["GoDB"]
title: "GoDB"
linkTitle: "GoDB"
weight: 900
date: 2023-01-31
description: >
 Umsetzung der GoDB, bzw. GDPDU
---
GoDB
====

GoDB ... Grundsätze zur ordnungsmäßigen Führung und Aufbewahrung von Büchern, Aufzeichnungen und Unterlagen in elektronischer Form sowie zum Datenzugriff
GDPDU ... Grundsätze zum Datenzugriff und zur Prüfbarkeit digitaler Unterlagen

Nachdem vor allem unsere Buchhaltungsverwender immer wieder nachfragen ob die **Kieselstein ERP** Finanzbuchhaltung der GDPDU bzw. der GoDB entpricht hier unsere öffentliche Erklärung dazu.

Wichtig: Die GDPDU wurde mit 1.Januar 2015 von der GoDB abgelöst.

Zur Info: **Kieselstein ERP** wurde von Anbeginn an unter den Grundsätzen damals noch der GDPDU entwickelt. Da die GoDB nur eine verdeutlichende / klarstellende Arbeitsunterlage des deutschen Finanzamtes ist, wird auch die GoDB von **Kieselstein ERP** erfüllt.

Gerne bestätigen wir, dass wir nach bestem Wissen und Gewissen die Regeln einer ordnungsgemäßen Buchhaltung mit der **Kieselstein ERP** Buchhaltung erfüllen.

Nachfolgende die Beantwortung von immer wiederkehrenden Fragen unserer Anwender zu diesem Thema.

#### Ist die **Kieselstein ERP** Buchhaltung testiert, gibt es ein Testat?
Die **Kieselstein ERP** Buchhaltung ist nicht testiert. Wir verweisen dazu auf das [Schreiben des deutschen Bundesfinanzministeriums vom 14\. November 2014](http://www.bundesfinanzministerium.de/Content/DE/Downloads/BMF_Schreiben/Weitere_Steuerthemen/Abgabenordnung/Datenzugriff_GDPdU/2014-11-14-GoBD.pdf?__blob=publicationFile&v=3) in dem unter Ziffer 12, Absätze 180 bzw. 181 ausgeführt wird:

-   Positivtestate zur Ordnungsmäßigkeit der Buchführung - und damit zur Ordnungs-mäßigkeit DV-gestützter Buchführungssysteme - werden weder im Rahmen einer steuerlichen Außenprüfung noch im Rahmen einer verbindlichen Auskunft erteilt.

-   "Zertifikate" oder "Testate" Dritter ... , entfalten jedoch aus den in Rz. 179 genannten Gründen gegenüber der Finanzbehörde keine Bindungswirkung.

#### Wie ist das Radierverbot umgesetzt?
Wird eine Buchung, wenn denn aus anderen Regeln / Vorschriften erlaubt, storniert, so wird ein Stornobuchungssatz erzeugt, welcher die Originalbuchung aufhebt. Der Original- und der Storno-Buchungssatz werden als storniert gekennzeichnet und normalerweise nicht angezeigt. Es kann dies jedoch jederzeit durch setzen des Hakens plus Stornierte ![](plus_stornierte.gif) sichtbar gemacht werden. Diese Auswahlmöglichkeit steht bei den Drucken der Kontoblätter, dem Druck des Buchungsjournals und den Exportmöglichkeiten des Buchungsjournals zur Verfügung.

#### Unveränderbarkeit der Buchführung
Dieser Gedanke ergibt sich daraus, dass vor allem nach Abgabe der UVA, eines Monatsabschlusses die Buchungen im alten Monat nicht mehr verändert werden dürfen.<br>
Siehe Ausdruck der UVA. Hier ist automatisch der Haken "Verprobung" gesetzt, womit beim Ausdruck der UVA (achten Sie bitte auf den tatsächlichen Druck und nicht den Druck über die große Druckvorschau) der Buchungszeitraum und alle davorliegenden gesperrt werden. Damit ist automatisch keine Buchung in diesem Zeitraum mehr möglich. Zusätzlich wird der Ausdruck der UVA in der Dokumentenablage gespeichert.<br>
Anmerkung: Ein Anwender mit entsprechenden Rechten darf diese Sperre wieder aufheben. Wir gehen davon aus, dass einE BuchalterIn weiss was Er/Sie macht.

#### Wo werden die ausgedruckten Rechnungen abgelegt?
In **Kieselstein ERP** wird jegliches offizielle Dokument, welches von **Kieselstein ERP**  erzeugt und aktiviert wird, in einem rücklesbaren Format in der Dokumentenablage abgelegt.<br>
Diese offiziellen Dokumente sind Anfrage, Bestellungen, Angebote, Auftragsbestätigungen, Lieferscheine, Rechnungen, Gutschriften.<br>
Eingangsrechnungen werden insoweit abgelegt, als die eingescannten Daten manuell oder vollautomatisch der Eingangsrechnung zugeordnet werden können z.B. im PDF Format

#### Wann wird verbucht?
Wird die integrierte Buchhaltung verwendet, so wird bei der Aktivierung der Belege, in diesem Falle nur mehr Eingangsrechnung, (Ausgangs-)Rechnung, Gutschrift (= Rechnungskorrektur) entsprechende Buchungsdatensätze erzeugt. D.h. wird eine Rechnung, eine Gutschrift (= Rechnungskorrektur) in Papier oder EMail-Form aktiviert, so wird/werden mit dieser Aktivierung entsprechende Datensätze erzeugt. Wird die Rechnung verändert muss diese zurückgenommen werden, was abhängig vom Status der Umsatzsteuerverprobung bzw. der Geschäftsjahressperre, möglich ist.<br>
Es werden in jedem Falle entsprechende Stornobuchungssätze veranlasst.
Die Buchungen werden alle in einem Buchungsjournal zusammengefasst dargestellt.

#### Werden auch EMails u.ä. in **Kieselstein ERP** abgelegt ?
Dies hängt vom Anwender ab. Es liegt in der Verantwortung des Anwenders, dass er zusätzliche geschäftlich relevante Belege gemäß den Vorschriften der GoBD in **Kieselstein ERP** als Zusatzbelege z.B. beim Angebot oder beim Kunden entsprechend ablegt .

#### Wie finde ich die Belege ?
Anhand der Belegnummer können die Belege jederzeit aufgefunden werden. Durch einen Klick auf die Dokumentenablage werden diese einfach und komfortabel angezeigt und können auch ausgedruckt werden. Werden Belege aus der Dokumentenablage nachgedruckt, so wird beim Nachdruck ein entsprechender Vermerk dass dies (nur) eine Kopie ist, auf dem Dokument zusätzlich angebracht. Dies dient der sicheren Unterscheidung zwischen Original und Kopie.

#### Kann das Buchungsjournal exportiert werden ?
Das Buchungsjournal kann jederzeit im RAW und im DATEV-CSV Format exportiert werden.
Anmerkung: Ein Buchungsjournal steht nur für die Daten ab Einsatz der integrierten Buchhaltung zur Verfügung.

#### Zugriff auf die Daten durch die Finanz?
Der Zugriff Z1 für Finanzprüfungen kann auf User-Ebene durch den Anwender selbst jederzeit eingerichtet werden.<br>
Der Zugriff Z2 direkt auf das Datenbanksystem kann jederzeit als Gast gewährt werden.<br>
Die Übergabe der Daten Z3 wird durch den Export des Buchungsjournals gewährleistet.

#### Unveränderbarkeit der Daten, Manipulationssicherheit?
Die Unveränderbarkeit der Daten ist solange gewährleistet, als der Anwender selbst das administrative Passwort der Datenbank nicht bekannt gibt bzw. den Zugriff auf den Datenbankserver entsprechend einrichtet.<br>
Aufgrund der Lizenzierung von **Kieselstein ERP** liegt es in der Verantwortung des Anwenders, das administrative Datenbank Passwort bzw. den Zugang zur Datenbank entsprechend sicher einzurichten.

#### Durchgängigkeit der Daten?
Der Anwender ist für die Durchgängigkeit der Daten selbst verantwortlich. Hiermit ist vor allem die vor- und nachgelagerte Belegerfassung gemeint. Also z.B. EMail-Anfragen des Kunden des **Kieselstein ERP** Anwenders sollten im **Kieselstein ERP** System landen. Wird dies vom Anwender nicht durchgeführt, so liegt dies in seiner Verantwortung.

#### Aufbewahrungspflicht?
Auch wenn sich dies manche Anwender immer wieder wünschen, ein Löschen von Daten ist im System nicht vorgesehen. D.h. solange die **Kieselstein ERP** Datenbank zur Verfügung steht (siehe Datensicherung) sind alle Daten in konsistenter und durchgängiger Form vorhanden. Es muss vom Anwender sichergestellt werden, dass die auf dem **Kieselstein ERP** Server gespeicherten Daten und Einstellungen nicht durch unbefugte Handlungen zerstört werden. Wir empfehlen daher den Einsatz von Linux-Systemen und die strickte Geheimhaltung der entsprechenden Passwörter.

#### Dokumentenablage?
Die Dokumentenablage ist eine eigene Datenbank in der nur Dokumente hinzugefügt werden können. Wird an Dokumenten, wir sprechen in der Regel von Belegen, Veränderungen vorgenommen, so wird die neue tatsächlich ausgedruckte Version mit aktuellem Datum und einer neuen Versionsnummer in der Dokumentenablage abgelegt.

Anmerkung1: Die **Kieselstein ERP** Dokumentenablage ist kein Ersatz für ein geprüftes Dokumentenablagesystem. Wir raten daher unseren Anwendern, den Originalbeleg in seiner originalen Fassung aufzubewahren.

Anmerkung2: Die Datensicherungspflicht liegt beim Anwender.

#### Verhindert **Kieselstein ERP** unsinnige Buchungen?
Nein. **Kieselstein ERP** geht davon aus, der/die Buchende wissen nach welchen Regeln zu buchen ist. Es werden bei den Automatikbuchungen je nach Einstellung eine Vielzahl von Vorgaben geprüft bzw. erwartet. Die Prüfung auf buchhalterische Richtigkeit wird nicht durchgeführt. Hier kommt natürlich mit dazu, dass ein guter Buchhalter immer eine entsprechende Buchung machen kann / können muss. So ist eher die Transparenz gegeben.<br>
Auch die Verantwortung welchen Kontenrahmen der Anwender verwendet liegt beim **Kieselstein ERP** Anwender.

#### Sind damit alle Forderungen der GoDB abgedeckt?
Nein. Neben den softwaretechnischen Dingen sind von der GoDB eine Vielzahl von Verfahren / Unternehmensprozessen gefordert. Diese müssen von Ihnen gegebenenfalls umgesetzt werden. Bitte beachten Sie dazu, dass die GoDB aber nur eine interne Handlungsanweisung des Finanzamtes an die Finanzprüfer ist. Sie hat nicht den Status eines Gesetzes. Aber natürlich ist es für Sie im Falle einer Prüfung deutlich besser, je mehr Sie von der GoDB umgesetzt haben. Wir verweisen daher erneut auf das [Schreiben des deutschen Bundesfinanzministeriums vom 14\. November 2014](http://www.bundesfinanzministerium.de/Content/DE/Downloads/BMF_Schreiben/Weitere_Steuerthemen/Abgabenordnung/Datenzugriff_GDPdU/2014-11-14-GoBD.pdf?__blob=publicationFile&v=3).