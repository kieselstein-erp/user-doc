---
categories: ["Finanzbuchhaltung"]
tags: ["Intrastatmeldung"]
title: "Intrastatmeldung"
linkTitle: "Intrastat"
weight: 400
date: 2023-01-31
description: >
 Die innergemeinschaftliche Statistikmeldung an die Statistik Austria bzw. die DeuStat
---
Intrastatmeldung
================

Die Intrastatmeldung finden Sie in der Finanzbuchhaltung ![](Fibu_Intrastat.jpg). Wählen Sie den unteren Modulreiter Intrastat ![](Intrastat.gif).
In der nun erscheinenden Liste sehe Sie die bereits importierten und derzeit gültigen Warenverkehrsnummern.

#### Wer ist Meldepflichtig:
Laut [Statistik Austria](http://www.statistik.at/web_de/frageboegen/unternehmen/aussenhandel_intrastat/index.html) sind ab dem Berichtsjahr 2013 folgende Unternehmen meldepflichtig:
INTRASTAT meldepflichtig sind Unternehmen, deren Wareneingänge aus EU-Mitgliedstaaten bzw. deren Warenversendungen in EU-Mitgliedstaaten wertmäßig die Assimilationsschwelle von 550.000 EUR im Vorjahr überschritten haben. Wird die Schwelle erst im laufenden Kalenderjahr überschritten, sind ab jenem Monat, in dem diese Überschreitung erfolgt, statistische Meldungen abzugeben.
Dieser Wert gilt für 2013\. Ob Sie selbst Intrastatpflichtig sind, bitte z.B. bei Ihrem Steuerberater, dem Finanzamt oder der Statistik Austria bzw. dem Statistischen Bundesamt (www.destatis.de) erfragen.

Hinweis: Seit 2022 sind Unternehmen nur mehr dann meldepflichtig, wenn je Verkehrsrichtung (Versand, Einfuhr) ein Wert von 1.100.000,- € überschritten wird. [Details siehe](https://www.usp.gv.at/laufender-betrieb/statistik/intrastat-meldung.html#BetroffeneUnternehmen)

**Hinweis:**

Beachten Sie, dass von den Finanzbehörden die Übereinstimmung zwischen den in den Umsatzsteuervoranmeldung, den Zusammenfassenden Meldungen und der Intrastatmeldung gemeldeten Werte geprüft werden.

#### Erstellen einer Intrastatmeldung
A: Für die Erstellung der Daten für die Intrastatmeldung empfiehlt es sich, zuerst die Daten zu prüfen und dann erst die tatsächliche Intrastatmeldung durchzuführen. Es stehen dafür unter dem Menüpunkt Intrastat zwei verschiedene Auswertungen zur Verfügung.
![](Intrastat_Menue.gif)
- a.) Intrastatmeldung
- b.) Vorschau

Gemäß obiger Empfehlung führen Sie zuerst die Vorschau aus.
- Wählen Sie Eingang oder Versand<br> ![](Intrastat_Auswahl.gif)<br>
- Geben Sie die durchschnittlichen Transportkosten von der Grenzübertrittsstelle zum Entladestandort (Ihr Unternehmen) an.
- Definieren Sie den gewünschten Zeitraum.

Dadurch erhalten Sie eine Liste aller in die Intrastatmeldung aufzunehmenden Positionen mit 
![](Intrastat_Report.gif)
den Belegnummern, den Artikel, den Warenverkehrsnummern bzw. ??? wenn keine Warenverkehrsnummer definiert ist (siehe dazu Artikel, Sonstiges), den Preisen, Werten inkl. Statistischem Wert und dem Gewicht der Position.<br>
Für eine gültige Intrastatmeldung müssen die Werte wie von den Statistischen Ämtern vorgegeben eingetragen sein.

Führen Sie diesen Vorschau-Lauf für Eingang und Versand durch. 

Danach kann die eigentliche Intrastatmeldung ausgeführt werden.

Erhalten Sie bei diesem Lauf eine Meldung wie
![](Intrastat6.jpg), so ist die oben beschriebene Ergänzung nicht vollständig durchgeführt worden.
Stellen Sie den angegebenen Artikel richtig und führen Sie den Export dann entsprechend durch.

Gleiches gilt für
![](Intrastat9.jpg)

Achten Sie darauf, dass erst mit der abschließenden Meldung <br>
![](Intrastat7.jpg)<br>
die aktuellen Intrastat-Exportdaten erzeugt werden, welche erst nach diesem gültigen Export in das Portal der jeweiligen Statistikbehörde übernommen werden dürfen.

Der Exportpfad wird unter System, Parameter, INTRASTAT_EXPORTZIEL_EINGANG bzw. INTRASTAT_EXPORTZIEL_VERSAND eingestellt.

#### Import neuer Daten der ÖStat
Vom Österreichischen Statistischen Zentralamt werden jährlich neue Warenverkehrsnummern herausgegeben. [Siehe dazu](#KN8 Datei erzeugen). Daraus werden von uns entsprechende Daten exportiert, welche Sie durch Klick auf ![](Intrastat_Import.gif) Intrastat importieren in Ihre Datenbank übernehmen können. Beim Import der Daten werden die alten Warenverkehrsnummern-Daten gelöscht. Die bei den Artikeln hinterlegten Warenverkehrsnummern bleiben selbstverständlich erhalten. **WICHTIG:**
Achten Sie darauf, dass vollständigen Datensätze eingespielt werden. D.h. nach dem Import, müssen für die Gruppen und Untergruppen vollständige Definitionssätze angeführt sein.
![](Intrastat_Musterdaten.gif)

D.h. der Import muss sehr ähnlich der obigen Darstellung ausgeführt sein. Haben Sie hier Kapitelüberschriften ohne Nummer, so wurden die falschen Daten importiert.
Für den Import ab 1.1.2022 muss auch die Besondere Maßeinheit mit vorhanden sein.

##### Download KN8 ab 2023
Ab 2023 ist der Download der KN8 etwas anders organisiert. [Siehe](https://www.statistik.at/fileadmin/pages/1135/WVZ_2023__KN2-_bis_KN8-Codes_mit_Warentext_DE.ods) bzw. suchen über die 
Lupe nach Intrastat und dann der Eintrag Erläuterungen, Hilfestellungen.<br>
Dann etwas weiter runterscrollen bis das passende Warenverzeichnis kommt.

#### ausgewählte Artikel nicht in der Intrastat anführen?
Da die Intrastatmeldung alle im Zeitraum exportierten Artikel anführt, werden auch ev. angegebenen Frachtmittel wie Europaletten usw. in die Intrastatmeldung mit aufgenommen. Da dies ja keine Handelsware im klassischen Sinne ist, will man diese auch nicht in der Intrastatmeldung mit anführen.<br>
Für derartige Artikel verwenden Sie bitte die Warenverkehrsnummer 0000 00 00\. Artikel mit dieser Warenverkehrsnummer werden bei der Versandmeldung nicht berücksichtigt.

Intrastatmeldung Österreich
===========================

Ab 1.1.2022 muss die Intrastatmeldung jedes Monat, für das eine Meldung abzugeben ist, über RTIC hochgeladen werden. Diese Funktion steht in **Kieselstein ERP** zur Verfügung.

Voraussetzung ist dafür auch eine aktuelle Warenverkehrsnummerntabelle. [Siehe](#aktuelle-kn8-dateien)
Bitte denke daran, diese jedes Jahr zu aktualisieren.

Übertragung der Meldung zur Statistik Austria

Nach der Erzeugung der Intrastat-Eingangs bzw. Versandmeldung müssen diese bei der Statistik Austria gemeldet werden.<br>
Dies erfolgt aktuell über das RTIC, also das **R**eporting **T**ool **I**ntra **C**ollect
Starten Sie dafür einen Internet Browser und gehen Sie auf die Webseite der [Statistik Austria](http://www.statistik.at).<br>
Am Startbildschirm befindet sich in der rechten unteren Hälfte auch eine Link-Box mit dem Link zu "Portal Statistik Austria". Der Einstieg in das Reporting Tool erfolgt über die Anmeldung im [Statistik Austria Portal](https://portal.statistik.at/).<br>
Sie benötigen dafür Ihre Zugangsdaten zur Statistik Austria.
![](Intrastat10.jpg)

Hier finden Sie die für Sie freigeschalteten Applikationen.
![](Intrastat11.jpg)
Für die erste Übertragung empfiehlt sich den RTIC Test zu verwenden.

Klicken Sie für die Meldung auf RTIC Test bzw. RTIC.
Wählen Sie anschließend Meldung erstellen.
![](Intrastat12.jpg)
Füllen Sie geforderten Felder entsprechend aus

![](Intrastat13.jpg)
und wählen Sie danach Datenimport.

![](Intrastat14.jpg)
- Durch Klick auf Auswahl wählen Sie die oben erzeugte Intrastat-Datei, Eingang bzw. Versand, aus.
- Das Trennzeichen verbleibt auf ; Semikolon.
- Definieren Sie als Zeichensatz UTF-8
- und geben Sie an, dass die Datei eine Kopfzeile enthält.
- Anschließend klicken Sie auf Daten importieren.

Nun wird rechts angezeigt dass die Datensätze eingelesen werden und nach entsprechender Verarbeitung dass die Daten eingelesen wurden. Gegebenenfalls werden auch entsprechende Fehler angezeigt.

Sie finden nun diese neue Meldung in der Meldungsübersicht.
![](Intrastat14.jpg)

Mit bearbeiten könnte diese Meldung noch einmal verändert werden. Diese Daten werden nicht mit Ihrem **Kieselstein ERP** in irgend eine Form abgeglichen.<br>
Auch können hier entsprechende Fehler und ähnliches eingesehen werden. Bitte prüfen Sie immer, ob in den Meldungen Fehler enthalten sind und leiten Sie Meldungen nur weiter wenn diese Fehlerfrei sind.

Wenn die Meldung in Ordnung ist, klicken Sie auf weiterleiten, wodurch diese Intrastatmeldung verbindlich an die Statistik Austria übermittelt wird.

Damit ist die Intrastatmeldung abgeschlossen.
Denken Sie daran sich entsprechend aus dem Portal abzumelden ![](Intrastat15.gif).

Hinweis1:
Der exakte Pfad der Importdateien kann in **Kieselstein ERP** unter System, Parameter, INTRASTAT_EXPORTZIEL_EINGANG und INTRASTAT_EXPORTZIEL_VERSAND definiert werden.


Intrastatmeldung Deutschland
============================

Unter [Liste der gültigen Warennummern (Sova-Leitdatei) Ausgabe 2022](https://www.destatis.de/DE/Methoden/Klassifikationen/Aussenhandel/Downloads/sovaleitdatei-2022-zip.zip?__blob=publicationFile "zum Download: Liste der gültigen Warennummern (Sova-Leitdatei) Ausgabe 2022 (zip, 2MB, Datei ist nicht barrierefrei)") finden Sie die gültige Warenverkehrsnummern für Deutschland für das jeweilige Jahr.<br>
Diese ist vom Inhalt her, Dank der europäischen Union, ident mit der Österreichischen KN8 Datei. Das bedeutet für unsere Anwender aus Deutschland, bitte verwenden Sie als Musterdatei ebenfalls die [Importdatei KN8](#aktuelle-kn8-dateien)

Unterschied der Meldung zwischen Österreich und Deutschland.
In Deutschland muss auch der Verkehrsweg angegeben werden. Da dieser in der Regel für KMU nicht definiert werden kann, man beauftragt einen Spediteur, wie dieser versendet ist nicht bekannt, wird als Verkehrsweg Kurier verwendet.

#### Onlineerfassung
Die Online-Meldung erfolgt über <https://www-idev.destatis.de/idev/OnlineMeldung>.<br>
Auch hierfür benötigen Sie entsprechende Zugangsdaten. (Die weitere Beschreibung erfolgt für den Gast-Zugang)
- Nach erfolgter Anmeldung wählen Sie
![](Intrastat20.gif)<br>
Intrahandel Formularmeldung und wählen danach den entsprechenden Berichtszeitraum.
- Klicken Sie nun oben auf Meldung,<br>
![](Intrastat21.gif)<br>
bzw. scrollen Sie nach unten bis zur Meldungserfassung.

Mit einem gültigen Zugang kann auch der Import mit CSV Format ![](Intrastat22.gif) gewählt werden und damit die von **Kieselstein ERP** erzeugten Daten eingespielt werden.

Für weitere Informationen wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

### Länder Zusammenfassung

Für die Zusammenfassung nach UID Nummern und Warenverkehrsnummer, verwenden Sie bitte die letzte Seite der Vorschau.<br>
Hier finden Sie auf der letzten Seite eine Zusammenfassung nach Ländern und Warenverkehrsnummern wie Sie der deutschen Erfassung entspricht.

#### Können Artikel von der Intrastatmeldung ausgenommen werden?
Ja. Geben Sie bitte im Artikel bei der Warenverkehrsnummer 0000 00 00 an. Damit wird signalisiert, dass dieser Artikel nicht in die Intrastatmeldung aufgenommen werden sollte. Dies wird z.B. für Dienstleistungen oder Verpackungen benötigt.

#### Umsatzsteuerrecht und EU Ausland, IG Lieferung
<a name="IG Lieferung"></a>
Da wir von unseren Anwendern immer wieder mit Fragen zum Umsatzsteuerrecht konfrontiert werden, hier die Aussage eines Steuerberaters unseres Vertrauens.<br>
Dazu folgende Einschränkungen: Wir sind weder Buchhalter noch Steuerberater. **Dies ist eine in jeglicher Form unverbindliche Information**. Für eine verbindliche Aussage wenden Sie sich bitte an den Steuerberater Ihres Vertrauens. Bitte beachten Sie dabei, dass sehr viele Steuerberater mit diesem Sachverhalt überfordert sind. Es ist auch schon vorgekommen, dass das österreichische Bundesministerium für Finanzen einige Jahre lang eine falschen Auskunft im Sinne des Gemeinschaftsrechtes der Europäischen Union gegeben hat.<br>
Der Wissensstand ist August 2019, österreichisches Recht. Hier kann sich "täglich" etwas ändern.

**Der Anwendungsfall I:** Es gibt drei beteiligte Unternehmen.
- Der Produzent P mit Sitz in Österreich
- Der Lohnverarbeiter L mit Sitz in Italien, welcher von P beauftragt wird
- Der (End)Kunde K ebenfalls mit Sitz in Italien

Alle drei sind Unternehmer mit einer jeweils eigenen UID (Umsatzsteuer Identifikations Nummer im Sinne des europäischen Rechtes)

P stellt Ware her und sendet sie zu L zu weiteren Verarbeitung in seinem Auftrag. L sendet dann weiter an K. P sendet an K die Rechnung.

- grundsätzlich ist das Verbringen eines Gegenstandes innerhalb eines Unternehmens steuerlich unbeachtlich.
    - wenn dieser Gegenstand jedoch in ein anderes EU-Mitgliedsland gebracht wird, dann handelt es sich um den Tatbestand des innergemeinschaftlichen Verbringens (IG-Verbringen)
    - das IG-Verbringen wird gleich behandelt wie eine IG-Lieferung
    - eine Ausnahme davon ist die vorübergehende Verwendung
    - eine vorübergehende Verwendung liegt bei der Lohnveredelung (dh. bei arbeiten am Gegenstand durch einen anderen Unternehmer) vor, wenn der Gegenstand nach Erbringung der sonstigen Leistung wieder zur Verfügung des Auftraggebers(P) in den Mitgliedstaat gelangt, von dem der Gegenstand befördert worden ist
- für obiges Beispiel bedeutet das folgendes:
    - die Ware kommt nach der Lohnveredelung nicht mehr retour nach Österreich
    - somit liegt keine vorübergehende Verwendung vor, sondern ein steuerfreies IG-Verbringen. D.h.
    - Der Produzent P muss sich in Italien umsatzsteuerlich registrieren, d.h. eine italienische UID Nummer haben!
    - Meldung des IG-Verbringens in Österreich (ZM mit eigener italienischer UID also der italienischen UID von P)
    - Erwerbsbesteuerung in Italien (für P)
    - Die Lieferung von L an K ist steuerpflichtig, da Inlandslieferung in Italien (ACHTUNG: Hier greifen dann auch die italienischen Regeln)
- diese unangenehme steuerliche Behandlung kann vermieden werden, wenn K selbst die Bearbeitung beauftragt und nicht P, das wäre dann lediglich eine steuerfreie IG Lieferung nach Italien

Daraus ergeben sich nun zwei mögliche Varianten wie das umsatzsteuerlich korrekt abgebildet werden kann:
- VARIANTE 1: die Gegenstände kommen zurück nach Österreich, zu P
    - D.h. wenn die Gegenstände nach Italien zu L und anschließend wieder zu P gebracht werden, liegt eine umsatzsteuerlich, nicht zu erfassende vorübergehende Verwendung in Italien vor.
    - es ist ein nicht steuerbarer Liefertatbestand in Österreich (keine ZM)
    - P bekommt von L nur eine Eingangsrechnung über eine Lohnveredelung (ohne Ust, Reverse Charge in Österreich)<br>Es wird dafür aber die Ware zweimal sinnlos herumgesandt!
- VARIANTE 2: die Gegenstände kommen zurück nach Österreich zu P und werden dann weiterverkauft
    - eine vorübergehende Verwendung verlangt, dass die Gegenstände zum selben Steuerpflichtigen P zurückgesandt werden.
    - entscheidend ist hier, wann die Weiterverkaufsabsicht entsteht!
        - steht bereits in Italien (bei L) fest, dass die Gegenstände weiterverkauft werden, dann ist bereits wieder keine vorübergehende Verwendung mehr gegeben und es kommt zu einer Registrierungspflicht in Italien.
        - wird die Verkaufsentscheidung erst getroffen, nachdem die Gegenstände wieder in Österreich sind, dann ist wieder eine Anwendung der vorübergehenden Verwendung, und es kommt zu keiner Registrierungspflicht in Italien.<br>
        Hier muss jedoch aus den Lieferpapieren nachvollziehbar sein, dass die Gegenstände wieder in der Verfügungsmacht von P waren.<br>
        Dementsprechend sollte auf den Lieferpapieren von Italien nach Österreich eine Lieferanschrift aufscheinen, die P zuordenbar ist.<br>
        **Info aus der Praxis: Italien sieht dies sehr eng!**

Diese Vorgehensweise ist inhaltlich gleich, wenn z.B. die Ware nicht in Italien, sondern in Deutschland einer Lohnveredelung unterworfen wird.<br>
Gleiches gilt wenn zwar der Lohnveredler in Italien, oder Deutschland ist, der Kunde K in einem Drittland (Schweiz, Liechtenstein, USA).<br>
Es ist immer entscheidend, von welchem Land aus die steuerfreie Ausfuhrlieferung durchgeführt wurde.

**Der Anwendungsfall II:** Es gibt drei beteiligte Unternehmen.
- Produzent P mit Sitz in Österreich
- Lohnverarbeiter L mit Sitz in Italien, der von Ihrem Kunden K beauftragt wird.
- (End)Kunde K ebenfalls mit Sitz in Italien

Alle drei sind Unternehmer mit einer jeweils eigenen UID (Umsatzsteuer Identifikations Nummer im Sinne des europäischen Rechtes)

Der wesentliche Unterschied zu Fall I ist, dass aus Ihrer Sicht, der Lohnverarbeiter L nur eine weitere Lieferadresse Ihres Kunden K ist.<br>
D.h. die Lieferanschrift lautet Kunde K, c/o Lohnverarbeiter L. Für die Adresse L gilt dann unseres Wissens auch die UID Nummer des Kunden K.

Wenn nun der Lohnverarbeiter L für mehrere Ihrer Kunden arbeitet, so muss, wegen der unterschiedlichen Kunden(namen) Ihr Lohnverarbeiter mehrfach angelegt werden, da er gegenüber P ja JEWEILS als unterschiedlicher Kunde K auftritt.

P stellt Ware her und sendet sie zu K per Adresse L zu weiteren Verarbeitung im Auftrag von K. Ob L dann an K sendet, ist für P nicht mehr relevant. P sendet an K die Rechnung.

#### Wie kann die Richtigkeit der Warenverkehrsnummern geprüft werden?
Insbesondere wenn sich die jährliche Pflege der Warenverkehrsnummern ergibt, sind unter Umständen einige Warenverkehrsnummern zu ändern. Üblicherweise gibt es z.B. von der Statistik Austria eine Vergleichsliste welche der Warenverkehrsnummern sich von einem zum anderen Jahr geändert haben. Suchen Sie nach "Gegenüberstellung Warenverkehrsnummer 2020 zu Warenverkehrsnummer 2021".<br>
Dies sind normalerweise sehr wenige Warenverkehrsnummern.<br>
D.h. die Vorgehensweise ist so, dass Sie zuerst die aktuelle Intratstat Definition importieren und danach den Druck der Intrastat-Auswahlliste aufrufen. Hier finden Sie am Ende eine Liste der in Ihrem Artikelstamm verwendeten Warenverkehrsnummern. Darin enthalten sind auch diejenigen die aktuell keine Zuordnung zu einer gültigen Warenverkehrsnummer haben.
![](Pruefung_Warenverkehrsnummer.jpg)

<a name="KN8 Datei erzeugen"></a>Erzeugen einer aktuellen KN8 Datei
-------------------------------------------------------------------

- Herunterladen des gültigen Warenverzeichnisses von [Statistik Austria](https://www.statistik.at/web_de/frageboegen/unternehmen/aussenhandel_intrastat/download/index.html)

- Wählen Sie hier ![](Intrastat_KN8.gif) Warenverzeichnis JJJJ KN2- bis KN8-Codes mit Warentext, im XLSX Format
- Öffnen Sie die Datei, z.B. mit Libre Office und wählen Sie das Tabellenblatt "KN2 - KN8 01-97"
- Löschen Sie die Spalten A (KNxx), sowie die Spalten E und F
- Speichern Sie das Tabellenblatt als CSV Datei ab.<br>
    ![](Intrastat_KN8_2.gif)<br>
    Exporteinstellungen:<br>
    ![](Intrastat_KN8_3.gif)<br>
    **Hinweis:** Um die Daten direkt in Linux Clients importieren zu können, muss diese als UTF8 exportiert werden.

### Aktuelle KN8 Dateien
Damit nicht jeder Anwender die Daten jeweils selbst konvertieren und einrichten muss, haben wir hier auch die aktuellen Intrastat Importdatei KN8 als Zip File zur Verfügung gestellt.

| Jahr | Download |
| --- | --- |
| 2025 | Wird immer Anfang des Jahres von der Statistik Austria zur Verfügung gestellt |
| 2024 | [kn8_2024.zip]( ./KN8_2024.zip) |
| 2023 | [kn8_2023.zip]( ./KN8_2023.zip) |
