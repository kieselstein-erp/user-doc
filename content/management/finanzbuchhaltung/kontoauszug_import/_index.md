---
categories: ["Finanzbuchhaltung"]
tags: ["Kontoauszugsimport"]
title: "Kontoauszugsimport"
linkTitle: "Kontoauszugsimport"
weight: 350
date: 2023-01-31
description: >
 Kontoauszug im SEPA / ISO20022 Format importieren
---
<a name="SEPA Kontoauszug Import"></a>SEPA Kontoauszug importieren
==================================================================

Mit deinem **Kieselstein ERP** können Daten im CAMT053 Format und im CAMT052 Format importiert werden. Wählen Sie im Modul Finanzbuchhaltung den unteren Modulreiter Bankverbindung. Wählen Sie dann die Bank und damit das Konto in das der Kontoauszug importiert werden sollte.
Zur Definition von welchem Pfad Ihr Sepa-Kontoauszug importiert werden sollte, tragen Sie im Reiter Kopfdaten das SEPA-Verzeichnis ein.
![](SEPA_Import1.gif)

Für den eigentlichen Import wechseln Sie in den Reiter 3 SEPA-Kontoauszug.
Klicken Sie hier auf ![](SEPA_Import2.gif) SEPA-Kontoauszug importieren.
Bestätigungen Sie nun<br>
![](SEPA_Import3.gif)<br>
die Importdefinitionen.<br>
Wichtig: Wird im camt.052 Format importiert, so ist dies kein echter Kontoauszug sondern es stellt dies nur eine Übergabe von Buchungsdaten dar, wie Sie diese oft bei Internet Banking Systemen vorfinden. Daher ist für das camt.052 die Angabe der gewünschten, von Ihnen verwendeten Kontoauszugsnummer erforderlich.

Beim camt.053 Format werden hingegen echt Kontoauszugsdaten transportiert in denen auch die Kontoauszugsnummer enthalten ist.<br>
![](SEPA_Import4.gif)

Nachdem Sie die Daten gewählt bzw. bestätigt haben klicken Sie auf Importieren.

Es erscheint nun nach wenigen Sekunden:
![](SEPA_Import5.gif)

Durch diesen Import wurden die Dateien aus dem Importverzeichnis in die **Kieselstein ERP** Datenbank in eine Zwischentabelle zur Verarbeitung übernommen. Die importierten Dateien wurden in das Unterverzeichnis Old verschoben. Um die Rückverfolgbarkeit bei eventuellen Unstimmigkeiten zu ermöglichen, lassen Sie diese Dateien bitte bis zum z.B. Jahres- oder Quartals-Abschluss bestehen.

Bitte beachten Sie, dass wir aus Gründen der sicheren Datenverarbeitung maximal 200 Kontoauszugsdateien in einem Lauf importieren. Sind mehr Dateien in dem Verzeichnis enthalten, sollten Sie bitte das Verzeichnis vorerst bereinigen und danach den Import neu starten. Bereinigen z.B. dadurch, dass alle alten Kontoauszüge aus dem Verzeichnis gelöscht werden. Sollten Sie tatsächlich laufend mehr wie 200 SEPA Kontoauszugsdateien importieren müssen, lassen Sie uns das bitte wissen. Wir finden eine Lösung.

Erhalten Sie beim Import eine ERR: Zeile so handeln Sie bitte entsprechend.
So bedeutet z.B.: ![](SEPA_Import19.gif) , die Salden stimmen nicht ...., dass der Saldo laut Bankkonto und der Saldo der ersten Kontoauszugsimportdatei nicht übereinstimmen, was wiederum die Vermutung nahe legt, dass die Dateien manuell verschoben / adaptiert wurden. Bitte entfernen Sie die überzähligen Dateien.

Sie finden nun, im Reiter 3 SEPA-Kontoauszug, eine Liste von importierten Kontoauszügen.
![](SEPA_Import6.gif)

Durch Klick auf ![](SEPA_Import7.gif) kann der gewählte Kontoauszug als storniert / gelöscht gekennzeichnet werden. Dies nutzen Sie z.B. wenn bereits Buchungen für diesen Kontoauszug gegeben sind.<br>
Durch Klick auf ![](SEPA_Import8.gif) SEPA Kontoauszug verbuchen starten Sie den eigentlichen Zuordnungsvorgang der Bewegungen des Kontoauszuges. Es wird dabei immer mit dem ältesten noch nicht importierten Kontoauszug begonnen.<br>
![](SEPA_Import9.gif)<br>
In der ersten Maske werden alle Bewegungen des Kontoauszugs dargestellt.

Mit weiter wird versucht eine Zuordnung der Bewegungen, also der Buchungen auf Ihrem Kontoauszug zu den offenen Ausgangs-/Eingangsrechnungen bzw. Zusatzkosten zu finden. Im Idealfall können alle Buchungen zugeordnet werden und Sie erhalten ein Bild ähnlich dem untenstehenden:
![](SEPA_Import10.gif)

Sie sehen hier:
- Zahlung: den Text aus dem Kontoauszug
- Buchungsdatum: das Buchungsdatum laut Kontoauszug
- Betrag: den Betrag laut Kontoauszug
- Belege: die diesem Betrag des Kontoauszugs zugewiesenen Belege. Werden hier Kunden bzw. Lieferantennamen angezeigt, so sind dies die Kurzbezeichnungen der Kunden/Lieferanten
- Restbetrag: bleibt für den / die Belege ein Restbetrag
- Erledigt: Ist damit die Zahlung auf diese Rechnung erledigt, abgeschlossen
- Übernehmen: Haken Sie diese Checkbox an, damit die Buchung tatsächlich übernommen wird.

Für jede Zeile wird deren Status beim Import definiert.
| Farbe | Status |
| --- | ---
| Grün | es wurde eine eindeutige Zuordnung für den Betrag zu einem Beleg (Eingangs- / Ausgangsrechnung) erkannt |
| Rot | Es konnte keine Zuordnung gefunden werden |
| Orange | Es konnte keine eindeutige Zuordnung gefunden werden |
| Türkis | es wurden manuell ein oder mehrere Belege zugeordnet |

Hinweis: Bei der Zuordnung von Ausgangsrechnungen wird gegebenenfalls auch die Bankverbindung des Kunden mit eingetragen.

Um die Zuordnung der Belege zu den Zahlbeträgen vorzunehmen bzw. zu verändern klicken Sie bitte in die jeweilige Zeile in die Spalte Belege<br>
![](SEPA_Import18.gif)<br>
Hier finden Sie bis zu 50 mögliche passende Belege und zusätzlich die Zeile manuelle Auswahl.<br>
Der Klick auf Manuelle Auswahl bringt Sie in die Zuordnung dieses Zahlbetrages = SEPA Kontoauszug Suchergebnis<br>
![](SEPA_Import20.jpg)<br>
Hier kann nun, je nach Art der Bankbewegung eine oder mehrere Eingangsrechnungen, eine oder mehrere Rechnungen und auch direkte Buchungen (=Manuelle Buchung) auf (Sach-) Konten vorgenommen werden.<br>
Ein Doppelklick auf die gewünschte Rechnung markiert diese als Gesamtes zu übernehmen. Es wird damit der mögliche Zahlbetrag zugewiesen und wenn dieser 100% dem noch offenen Betrag der Rechnung entspricht auch das Erledigt angehakt. Sollten ein anderer Betrag als der noch offene bezahlt worden sein, bzw. ein anderer als der vorgeschlagene Zahlbetrag der Rechnung zugeordnet werden, so klicken Sie bitte mit der Maus in das Feld Zahlbetrag der markierten Rechnungszeile und geben Sie den tatsächlichen Betrag ein.

Im Reiter Übersicht finden Sie eine Liste der für diese Buchungszeile des Bankkontoauszugs zugewiesenen Rechnungen. Rechts unten wird der noch offene zuzuweisende / zu buchende Betrag angezeigt. Dieser sollte selbstverständlich 0,00 sein. Es wird, bei der Verbuchung des Kontoauszuges daher der erwartete Saldo, da er auch mit dem in den Sepa-Daten enthaltenen Beträgen nicht übereinstimmt in Rot angezeigt.<br>
Haken Sie auch bei den manuellen Zuordnungen das ![](SEPA_Import21.jpg) Übernehmen an, damit diese Buchungen durchgeführt werden. Sind alle Buchungen richtig zugeordnet, so ändert sich die Farbe des Feldes "erwarteter Saldo für Auszug xxxx" auf grün und die tatsächliche Übernahme kann durchgeführt werden.<br>
![](SEPA_Import22.jpg)<br>
Dadurch wurde auch der Weiter Knopf freigeschaltet. Ein Klick darauf startet die tatsächliche Verbuchung des Kontoauszuges. Es werden damit auch alle Zahlungen auf Ein- und Ausgangsrechnungen bzw. Gutschriften, Zusatzkosten und die manuellen Buchungen gebucht.
Zur Bestätigung sehen Sie:<br>
![](SEPA_Import11.gif)<br>
Durch Klick auf Beenden sind Sie nun wieder in der Auswahlliste der Kontoauszüge, in der der soeben übernommene Kontoauszug naturgemäß nicht mehr offen ist.

Um alle bisher importierten Kontoauszüge anzuzeigen entfernen Sie, wie in **Kieselstein ERP** üblich, bitte den Haken bei ![](SEPA_Import12.gif) nur offene. Die bereits importierten Kontoauszüge sind mit einem Haken ![](SEPA_Import13.gif) versehen. Die stornierten sind durch das rote X ![](SEPA_Import14.gif) gekennzeichnet und die angelegten, also noch nicht verbuchten Kontoauszüge sind durch das angelegt Symbol ![](SEPA_Import15.gif) gekennzeichnet.

Werden / wird bei der Ermittlung der Belege keine Zuordnung gefunden, so wird dies
![](SEPA_Import16.gif)
durch orange farbene Zeilen angezeigt. Nehmen Sie hier die Zuordnung manuell vor.

#### Wie wird eine manuelle Zuordnung gemacht ?
Es kommt immer wieder vor, dass Buchungen direkt auf Sachkonten oder eine Buchung auf mehrere Rechnungen u.ä. vorgenommen werden. Hier wird, aus Sicherheitsgründen, keine vollautomatische Zuordnung vorgenommen. Es werden Ihnen jedoch die wahrscheinlichen Rechnungen vorgeschlagen.<br>
Klicken Sie dazu in die Importzeile in die Spalte Belege und wählen ![](SEPA_Import18.gif) Manuelle Auswahl.<br>
Bei Haben-Buchungen wird automatisch der Reiter Eingangsrechnungen vorgeschlagen, bei Soll-Buchungen der Reiter (Ausgangs-)Rechnungen.<br>
Für Buchungen auf Sachkonten klicken Sie bitte auf Manuelle Buchung.<br>
Die Buchung ist, aus Sicht des gewählten Bankkontos mit Haben vorbesetzt. Geben Sie nun das gewünschte (Aufwands-) Konto an. Als Zahlbetrag wird immer der noch offene Betrag für diese Kontoauszugszeile vorgeschlagen. Durch Klick auf ![](SEPA_Import23.gif) übernehmen wird diese manuelle Bankbuchung in der Buchungsart Bank in die Auswahlzeile übernommen. Der offene Restbetrag, dieser Kontoauszugszeile, wird rechts unten angezeigt. Ist dieser 0,00 so wird der Knopf Übernehmen freigeschaltet und es wird / werden diese Buchungszeilen (in Art einer Stapelbuchung) für diese Kontoauszugszeile festgehalten.

#### Kann man in den Listen suchen?
Oft ist es praktisch in den Suchergebnissen, der manuellen Zuordnung, nach Teilen des Kunden-/Lieferanten-Namens o.ä. zu suchen. Da dies ein sehr spezieller Dialog ist, werden hier sogenannte RegEx verwendet, die leider zu den üblichen Eingaben nicht kompatibel sind. D.h. um hier einen Teil des Namens zu filtern muss **\.\*** mit dem nachfolgenden Wort eingegeben werden **und** danach Enter gedrückt werden .
![](Suchen_01.png)

#### Kann der Import unterbrochen werden ?
Der Import zwischen den einzelnen Kontoauszügen kann jederzeit beendet werden. Wurde der Zuordnungsvorgang des einzelnen Kontoauszuges gestartet, so muss dieser beendet oder abgebrochen werden. Beachten Sie bitte, dass beim Abbrechen alle erfassten / zugeordneten Daten verworfen werden.

#### Welche Prüfungen werden vorgenommen, wie erlange ich Sicherheit, dass alles richtig verbucht ist ?
Es müssen grundsätzlich die in den SEPA Daten enthaltenen Daten schlüssig sein. D.h. wenn der Banksaldo und die von Ihnen zugewiesenen Buchungen zusammenstimmen wird der entsprechende Weiter Knopf freigeschaltet. Damit haben Sie die Sicherheit, dass der Saldo Ihres Bankkontos mit dem in Ihrem Fibu-Bank-Konto gebuchten Saldo übereinstimmt.

#### Weiter wird nicht freigeschaltet
![](SEPA_Import17.gif)
Dieses Bild bedeutet unter anderem, dass der im Import enthaltene Anfangssaldo mit dem Saldo Ihres Bankkontos NICHT übereinstimmt.<br>
Zur Behebung stellen Sie bitte den Banksaldo richtig.<br>
[Beachten Sie dazu auch](Integrierte_Finanzbuchhaltung.htm#SEPA Auszugssaldo stimmt nicht).

#### Beim Import erscheint eine Fehlermeldung
Es wird bereits beim Import geprüft, ob die Daten in sich logisch sind.<br>
So können z.B. nur Kontoauszüge importiert werden, die noch nicht in Ihrem Bankkonto vorhanden sind.<br>
Die Kontoauszüge müssen chronologisch und aufsteigend sein. D.h. nach der Auszugsnummer 1 muss die Auszugsnummer 2 kommen usw.<br>
Leider passiert es, dass von den Banken nicht immer absolut richtige Daten gesandt werden. D.h. wird beim Import festgestellt, dass diese unlogisch / nicht schlüssig sind, wird eine Fehlermeldung ausgegeben und diese Daten werden nicht importiert.

**Hinweise zur Fehlerursache:**
Erscheint die Fehlermeldung .... keine fortlaufende Auszugsnr.
![](SEPA_Import_Fehlermeldung1.gif)
So bedeutet dies, dass wir die Soll-Auszugsnummer erwarten aber die Ist-Auszugsnummer bekommen haben.<br>
In diesem Falle bedeutet das konkret, dass der Auszug Nr 172 zweimal in der einzulesenden Datenbasis enthalten war. Abhilfe: den falschen XML Datensatz entfernen.

![](SEPA_Import_Fehlermeldung2.jpg)<br>
Diese Meldung bedeutet, dass die Kontoauszugsnummer bereits vorhanden ist. Suchen Sie in diesem Falle in den XML Dateien (Pfad siehe Ihre Einstellungen) nach dem Text der Auszugsnummer. Unter der Voraussetzung dass in der jeweiligen XML Datei nur ein Kontoauszug enthalten ist, verschieben / entfernen Sie die XML Datei und versuchen Sie den Import erneut.

#### Werden Kontoauszüge übersprungen?
Grundsätzlich werden keine Kontoauszüge übersprungen. Es werden jedoch von den Banken manchmal reine Informations-Kontoauszüge gesandt. D.h. das sind Kontoauszüge OHNE Bewegungsdaten, d.h. diese beinhalten reinen Informationstext. Diese werden von **Kieselstein ERP** erkannt und ignoriert.

#### Welche Auszugsnummern werden verwendet?
Abhängig von der Art des Importes wird:
- beim CAMT053, der ja eine echte Repräsentierung des "papierenen" Kontoauszuges ist, wird die tatsächliche Kontoauszugsnummer, wie in der XML Datei gespeichert, verwendet.
- verwenden Sie für die Aktualisierung Ihrer Bankdaten das CAMT052 Format, welches nur eine Mitteilung der Kontobewegungen im gewählten Zeitraum darstellt, so wird das Datum mit dem der Datenexport gespeichert wurde verwendet.

#### Werden beim Import Lieferanten / Kunden automatisch zugeordnet?
Beim Import der SEPA Daten wird, soweit möglich auch die Bank beim jeweiligen Kunden / Lieferanten hinterlegt.<br>
D.h. üblicherweise sind in den SEPA Daten auch die IBAN und die BIC Ihres Geschäftspartners enthalten.<br>
Ist im Kunden-/Lieferantenstamm diese Kombination enthalten, so wird beim Import automatisch diese Zuordnung verwendet und somit erkannt welcher Partner dies sein sollte.<br>
Wird die übergebene Information nicht gefunden, so muss vom Anwender die manuelle Zuordnung zur Rechnung (ER, RE, GS) gemacht werden. In **Kieselstein ERP** merken wir uns diese Zuordnung von IBAN und BIC.<br>
**WICHTIG:** Offensichtlich wird in den SEPA Daten der Schweizer bzw. Liechtensteiner Banken aktuell keine IBAN und BIC mitgesandt. Daher kann diese Zuordnung auch nicht gemacht werden. Und darum muss auch jedes Mal die Rechnung manuell gesucht werden. Bitte Überzeugen Sie Ihre Bank davon, dass diese Daten für eine effiziente und automatische Verarbeitung erforderlich sind.

#### Unterschied zwischen CAMT053 und CAMT052
- Das CAMT053 Format stellt die XML Abbildung des Kontoauszuges dar, mit Kontoauszugsnummer usw.
- Das CAMT052 Format liefert die Umsätze des gewählten Zeitraumes. 
**Info:**<br>
Stimmen beim Import die Bewegungsdaten aus dem Zahlungsvorschlag nicht mit den im Kontoauszug erhaltenen Daten überein, erhalten Sie eine entsprechende Fehlermeldung.

#### ISO20022 oder SEPA
<a name="ISO20022"></a>
Das XML Format ISO20022 entspricht in wesentlichen Zügen dem SEPA Format. Wir haben dieses Format bereits grundsätzlich vorgesehen.<br>
In diesem Format können vor allem auch andere Währungen, wie z.B. USD, mit exportiert werden.
Die Definition welches Format verwendet wird, wird für die jeweilige Bankverbindung in den Kopfdaten vorgenommen.<br>
Hier kann zwischen SEPA und Swiss Payments unterschieden werden.<br>
Daraus ergeben sich damit wiederum die unterschiedlichen Formate für Lastschrift und Zahlungsauftrag.<br>
Für weitere Details wenden Sie sich bitte direkt an Ihren **Kieselstein ERP** Betreuer.

**Info für ELBA Benutzer:**

Um Kontoauszugsdaten im Format CAMT053 im Elba Offline zu erhalten, muss unter Grundeinstellungen --> Zugangsdaten --> Eigene Konten für das jeweilige Konto, im Reiter Export-Daten der Punkt Kontoauszug camt.053 angehakt sein.<br>
Damit werden automatisch die Daten im angegebenen Verzeichnis mit abgelegt. Bitte beachten Sie bitte, dass diese Daten unter Umständen, je nach Verzeichnis und Zugriffsrechten, von anderen Personen, z.B. Administratoren, gelesen werden können. Ein pfiffiger Admin liest die XML Dateien "mit freiem Auge" aus.
![](Sepa_Export1.jpg)

#### Können ELBA SEPA Daten nachträglich abgerufen werden?
Ja. Manchmal kommt es vor, dass in der Übertragung das gewünschte Laufwerk für die SEPA XML Dateien nicht zur Verfügung steht. Leider liefert das Elba bei der Übertragung keinen Hinweis (es gibt nur eine kurze Notiz im Protokoll, die man sehr leicht überliest) dass die SEPA Daten nicht gespeichert werden konnten. Um nun diese nachträglich abzuholen ist die Vorgehensweise wie folgt:
- a.) im ELBA auf das Konto gehen und rechte Maustaste (auf eine Zeile des Kontoauszugs) und e-Kontoauszug anfordern auswählen
- b.) Konto auswählen und die fehlenden Kontoauszugsdaten (von - bis) angeben
![](SEPA_nachfordern.jpg)

- c.) Auf anfordern klicken und
- d.) dann den Bankenrundruf starten

Import von Onlinebanking im camt.052 Format

Die Vorgehensweise ist von Seiten **Kieselstein ERP** sehr ähnlich dem camt.053 Format. Der wesentliche Unterschied besteht darin, dass die Daten keine Kontoauszüge sind, sondern Tageweise aufbereitet geliefert werden. Es hat sich bewährt, dass vom Anwender, also Ihnen, die importierten Daten Kontoauzügen ähnlich verwaltet werden. Daher die Eingabe der Kontoauszugsnummer beim Import der Dateien.<br>
Für den Export aus Ihrem Onlinebanking stellen Sie bitte den gewünschten Zeitraum ein, wählen als Exportformat CAMT-Format-gebuchte Umsätze und klicken auf speichern bzw. exportieren.
![](SEPA_Import_SPK1.gif)

Hier erhalten Sie in der Regel eine ZIP Datei. Speichern Sie diese z.B. lokal ab (ACHTUNG: an das Löschen denken).<br>
Für Windows User könnte das z.B.
![](SEPA_Import_SPK2.gif) sein.
Daran anschließend sehen Sie alle enthaltenen Dateien
![](SEPA_Import_SPK3.gif)
markieren Sie diese und ziehen Sie diese in Ihr definiertes SEPA Import Verzeichnis.<br>
Nach dem herüberziehen, was leider immer ein kopieren ist, denken Sie daran die ZIP-Datei zu löschen.

Die weitere Vorgehensweise ist wie oben beschrieben.

**Tipp:**<br>
Da bei den Online-Buchungssystemen immer wieder mal für den laufenden Tag Buchungen hinzukommen können, importieren Sie nur bis inkl. gestern. Es könnte sonst vorkommen, dass Sie für einen Tag zwei unterschiedliche Kontoauszüge erhalten, was Sie in jedem Falle vermeiden sollten. Die Verwirrung wäre enorm und wird auch von den Online Bankingsystemen NICHT unterstützt.

**Hinweis:**<br>
Beim Import des Kontoauszuges werden immer auch Sammelaufträge oder Sammler mit übergeben. Diese haben die Eigenschaft, dass nur Betrag und Anzahl der Buchungen übergeben werden. Damit stehen keinerlei Informationen für die weitere Verwertung zur Verfügung. Beantragen Sie daher bitte bei Ihrer Bank, dass diese Sammler abgeschaltet werden und sie alle Buchungen im Detail erhalten.

#### Wie kann der SEPA-Kontoauszugsimport initial gestartet werden?
Um den SEPA Kontoauszug zu starten sind folgende Bedingungen, neben den technischen Anforderungen Voraussetzung:
-   Der älteste eingelesene Kontoauszug muss
    -   im laufenden, also dem gewählten Geschäftsjahr sein
    -   die Kontoauszugsnummer muss die erste in diesem Konto sein oder
        Lückenlos an die letzte Buchung auf dem Bankkonto anschließen
    -   der Saldo auf dem Bankkonto muss exakt dem Startwert des Kontoauszuges entsprechen
-   Gegebenenfalls stornieren Sie alte Kontoauszugsimportdateien
-   Gegebenenfalls buchen Sie den Startwert des Kontoauszuges entsprechend nach

#### Beim Import werden alte Auszugsnummern aufgelistet
Werden beim Import alte Auszugsnummern aufgelistet, so wurden vermutlich zwischendurch diese alten Auszugsnummern erneut angefordert. Da der Import dies erkennt und damit die weitere Verarbeitung verweigert, stehen folgende Möglichkeiten für die Behebung dieser "falschen Bankdaten" zur Verfügung.
- a.) löschen der erhaltenen SEPA Daten ab dieser Importdatei und neu holen der SEPA Daten von der Bank
- b.) theoretisch sollte auch eine manuelle Bearbeitung der Datei möglich sein, sodass die Kontoauszüge in der tatsächlich notwnedigen Reihenfolge zur Verfügung stehen
- c.) manuelles buchen dieses Kontoauszugs
