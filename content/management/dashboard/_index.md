---
categories: ["Management"]
tags: 
title: "Dashboard"
linkTitle: "Dashboard"
weight: 30
description: >
 Eine Sammlung verschiedenster Report-Datenbank-Scripts für eure Spezialwünsche an Auswertungen
---
Das **Kieselstein ERP** Dashboard ist als kompaktes, Benutzer abhängiges, Informationssystem in Form eines Reports gedacht. Von der Idee her stellt man sich seinen **Kieselstein ERP** Client so ein, dass das Modul Dashboard bei jedem Start des Clients automatisch geladen wird.

Somit hat man, täglich seine spezielle Auswertung mit den passenden Informationen zur Verfügung.

Bitte beachte, dass in aller Regel diese Auswertungen auf deine / eure Bedürfnisse angepasst werden müssen.

In dem ausgelieferten Dashboard Report sind z.B. enthalten:
- deine offenen Angebote mit Wert und Wahrscheinlichkeiten
- deine offenen Aufträge
- deine offenen Bestellungen
- deine offenen Projekte
- alle offenen Lieferscheine
- deine offenen Wiedervorlagen

Um nun gegebenenfalls zu erreichen, dass sich das Dashboard automatisch jede Stunde aktualisiert, stellen Sie bitte den Parameter DASHBOARD_AKTUALISIEREN auf die gewünschte Aktualisierungszeit in Minuten, z.B. 60.
