---
categories: ["Management"]
tags: ["Cockpit"]
title: "360° Kundencockpit"
linkTitle: "360° Kundencockpit"
weight: 20
description: >
 Zentrale Sicht auf Kunden und auch Lieferanten
---
360° Kundensicht
================

Mit dem Modul 360° Kundensicht steht Ihnen vor allem eine praktische Zusammenfassung der Kunden / Lieferantendaten zur Verfügung.
Dieses Modul stiftet seinen Nutzen in Verbindung mit der **T**api **E**inbindung. D.h. direkt aus der Telefonnummer des Anrufenden kann in das 360° Kundensicht-Detail des Rufenden mit einem Mausklick gesprungen werden.
![](360_Kundensicht1.jpg)

Die Auswahl steht für eine manuelle Suche Ihrer Partner zur Verfügung.
Über den CRM Klick ![](../Partner/TAPI_CRM1.jpg) aus der [TE]( {{<relref "/docs/stammdaten/partner/telefonanbindung_eingehend">}} ) gelangen Sie direkt auf den passenden Ansprechpartner.
Zugleich sehen Sie im oberen Teil die Adresse und bei entsprechenden Rechten auch die Umsätze des Kunden.
Mit dem GoTo gelangen Sie direkt in den Kunden bzw. Lieferanten.
Darunter angeordnet finden Sie die Liste der Ansprechpartner. Hier stehen die üblichen Möglichkeiten zur Bearbeitung und Verwendung der Ansprechpartner zur Verfügung.

Mit Klick (Alt+3) wechseln Sie in die Telefonzeiten.
![](360_Kundensicht2.jpg)
Die Anzeige ist auf den unter Ansprechpartner voreingestellten Ansprechpartner eingeschränkt. Mit anhaken von Alle Ansprechpartner werden die Telefonzeiten aller Ansprechpartner dieses Kunde/Lieferanten angezeigt.
Zusätzlich kann mit den Direktfiltern nach anderen Ansprechpartner gesucht werden bzw. im Direktfilter Kommentar nach Inhalten in den in den Telefonzeiten erfassten Kommentaren.
Praktisch ist auch, dass sowohl die Dauer der Gespräche und die Person, also IhrE MitarbeiterIn, angezeigt werden.
Weitere Details zur Telefonzeiterfassung [siehe](../Zeiterfassung/index.htm#Telefonzeiten erfassen).

Mit Klick (Alt+4) wechseln Sie in die Aufgaben.
Dieser Reiter stellt auch die sogenannten Kontakte (die Sie im Modul Partner finden) dar.
Auch hier, eine praktische Übersicht über alle Aufgaben die Sie für diesen Ansprechpartner oder für alle Ansprechpartner des Kunden erfasst / hinterlegt haben.

Mit Klick (Alt+5) wechseln Sie in die Projekte.
Hier sehen Sie, in der üblichen Bedienung der Projekte alle Projekte des Kunden / Partners mit den Umschaltmöglichkeiten der Bereiche.
Damit verbunden sind selbstverständliche die Details (Alt+6) zu den jeweiligen Projekten.

Ist in Ihrer HELIUM V Installation auch noch das Zusatzmodul Projektklammer freigeschaltet, so steht auch der Reiter 7 Cockpit (hier ist das Projektcockpit gemeint) zur Verfügung.
Mit diesem Modul erhalten Sie einen Komplettüberblick über alle diesem Projekt angehängten Belege. [Für Details siehe bitte Projektklammer]( {{<relref "/verkauf/projekt/projektklammer"  >}} )
