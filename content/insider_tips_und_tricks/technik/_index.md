---
title: "Technik Infos"
linkTitle: "Technik"
categories: ["Technik"]
tags: ["Technik"]
weight: 10
description: >
  Tipps und Tricks für die Technik / den erfahrenen Consultant
---
Tipps und Tricks für die Technik / den erfahrenen Consultant

## Sicherung der Dokumentendatenbank
### Kann man das Vacuum, genauer die Zeit für das Vacuum der Dokumentendatenbank einsparen?
Hintergrund der Überlegung ist, dass das ja nur eine reine Zuwachsdatenbank ist, aber:<br>
Es sind da auch Tabellen enthalten, die direktz vom Jackrabitt verwendet werden, welche auch dynamische Daten beinhalten. 
Nur weil wir keine Dokumente löschen, bedeutet das nicht das die interne Verwaltung nicht temporäre Daten wieder löscht.

Falls das einmal ein Thema werden sollte, muss man sich das genauer ansehen. Derzeit bitte nicht aus dem Vacuum ausnehmen.

## Reports
### Wie findet man von den flrdrucknnnnn zum rufenden FLR?
Um von einem bestehenden flrdruck zurück zu finden, woher dieser denn aufgerufen wird, siehe: ....\kieselstein\ejb\src\com\lp\server\util\fastlanereader\service\query\QueryParameters.java 

## Ändern von Ressource-Lables
Grundsätzlich ist in jedem ERP wichtig, dass die benutzten Felder in ihrer Bezeichnung möglichst exakt (in einem kurzen Wort) das wiedergeben, was du / der Anwender darunter versteht.
Nun gibt es, insbesondere in den Stammdaten, oft Felder die nicht benötigt werden und andererseits bräuchte man noch ein Feld für z.B. verbleibenden Rest.

Daher hier die Beschreibung wie man die Resourcen der Lables übersteuern kann.<br>
Als Beispiel wollen wir im Artikel im Reiter Technik das Feld *Raster liegend* durch (den Text) *verbleibender Rest* übersteuern.

Beim Start des Clients werden die Übersteuerungstexte geladen. Diese stehen in der Tabelle LP_TEXT. Diese hat die Felder<br>
![](uebersteuerte_lables.png)  
D.h. wenn man in den c_token den Resourcennamen einträgt, so wird anstelle des hinterlegten, sprachabhängigen Resourcennamens, der Inhalt für c_text angezeigt. Das Ganze dann auch noch in Abhängigkeit des Mandanten und der locale (Sprache).<br>
Das bedeutet, es stellt sich nur mehr die Frage wie findet man nun die Resource?
Du kannst diese über die im Source gespeicherten message-Files heraussuchen oder über das kieselstein-ui-swing-x.x.x.jar. Da das client.jar faktisch auf jedem Rechner der **Kieselstein ERP** installiert hat verfügbar ist, sei es an diesem JAR erklärt:
1. gehe in das kieselstein-ui-swing-x.x.x.jar. Windows-Anwender nutzen dafür den Totalcommander mit Strg+Page-down (Öffne das / ein Zip-File, was ein Jar immer ist)
2. nun in der angezeigten Verzeichnisstruktur tiefer rein bis com/lp/client/res
3. suche die Datei(en) messages.properties bzw. die Sprachvarianten davon
4. öffne diese, nur lesend (im Totalcommander mit F3 bzw. Shift+F3)
5. suche nach dem Wort, welches im original angezeigt wird, z.B. ![](uebersteuerte_lables2.png)  Raster liegend.
6. du findest: ![](uebersteuerte_lables3.png)  
7. der linke Teil (vor dem =) ohne eventueller Spaces ist der Resourcennamen. Diesen kopieren und
8. als neue Zeile und unter c_token in die LP_Text eintragen.
9. nun noch mandanten_c_nr und locale_c_nr ergänzen und unter c_text den übersteuerten Text (verbleibender Rest) eintragen und die Tabellenergänzung speichern.
10. um die Änderung sofort in deinem Client wirksam werden zu lassen, gehe nun in deinen Kieselstein Client. Wenn das "geänderte" Modul offen ist dieses schließen und in die Systemsteuerung gehen. Unterer Reiter Pflege und oben auf ![](uebersteuerte_lables4.png)  LP_TEXT neu laden klicken und das betroffene Modul neu öffnen und die gewünschte Änderung prüfen.
Solange die Resourcennamen nicht geändert werden, bleiben deine übersteuerten Texte erhalten.

**Hinweis:** Die Spaltenüberschriften der Tabellen (FLR) und einige andere wenige Resourcen sind im Serverteil gespeichert. D.h. diese findest du unter ??\kieselstein\dist\wildfly-12.0.0.Final\standalone\deployments\kieselstein-x.x.x.ear. Auch hier in das ear hineingehen (Strg+Page-Down) und erneut nun in das ejb-x.x.x.jar reingehen bis com\lp\server\res\\. Auch hier findest du sprachabhängige messages.properties im gleichen Aufbau. Auch hier suchst du nach dem gewünschten Wort und trägst die Resource unter c_token usw. in der LP_Text ein.

**Anmerkung:** Dadurch ändern sich natürlich nicht die Feldnamen der Datenbankfelder. D.h. wenn du nun über einen SQL_Query (P_SQLEXECUTER) den Wert von verbleibender Rest anzeigen willst, musst du nach Raster_liegend suchen und diesen Wert verwenden.

## Wo findet man welche Datenbankfelder?

## welchen PGadmin kann man verwenden?
Grundsätzlich sollte der PGadmin4 erst aber Version 6.x verwendet werden. Leider gibt es hier auch Versionen die zumindest unpraktisch sind.<br>
Ich persönlich verwende den 6.3, dem aber bereits einige praktische Kleinigkeiten fehlen.<br>
Weiters den 6.20.<br>
Anscheinend geht der aktuelle 7.3. auch ganz gut.<br>
Der 6.12 geht nicht vernünftig.

## Die Datenbank wächst und wächst
So wie das **Kieselstein ERP** Stand Juli 2023 programmiert ist, werden gültig versendete Versandaufträge doppelt abgelegt. D.h. diese sind einerseits in der Dokumentenablage und andererseits in den Versandaufträgen mit dem Status versandt in deinen ERP Daten = KIESELSTEIN gespeichert. Du siehst aber nur die Daten aus der Dokumentenablage. Damit wächst deine Datenbank, das Backup dauert immer länger usw.. Daher empfiehlt sich, einen Crone Job einzurichten, der täglich zusammenräumt. Idealerweise ist dies mit dem Backup kombiniert.<br>
Wir konnten bei einem Mitglied die Datenbank von 8GB auf 1,6GB reduzieren, ohne Datenverlust.