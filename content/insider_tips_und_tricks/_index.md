---
title: "Tipps und Tricks"
linkTitle: "Tipps"
categories: ["Tipps und Tricks"]
tags: ["Tipps und Tricks"]
weight: 70
description: >
  Tipps und Tricks für die Technik / den erfahrenen Consultant
---
Tipps und Tricks für den erfahrenen Consltuant

## Geschäftsjahr nicht bebuchbar, obwohl Beginnmonat **nun** richtig eingestellt ist.
Ursache: in der lp_geschaeftsjahrmandant ist der Beginn falsch eingetragen.

## Bei manueller Buchung Meldung, Mwst Satz passt nicht
in LP_MWSTSATZBEZ den c_druckename nachtragen. Dieser muss wiederum zur UVA passen (siehe Parameter)

## Importe generell
Es erreichen uns immer wieder Anfrage, weil ein Import angeblich nicht geht.

Dazu muss man wissen, die Import sind meist Transaktionsorientiert programmiert. D.h. entweder geht der ganze Import durch, oder er wird abgebrochen und der gesamte Import damit aufgerollt. D.h. die Daten stellen sich so dar, als wenn der Import nie durchgeführt worden wäre.

Das bedeutet für den Applikationsserver bzw. den Datenbankserver, dass er die ganzen Zustände zwischenspeichern muss.

Das bedeutet in weiterer Folge, dass jede Zeile mehr, eine entsprechende Verarbeitung erfordert. Was wiederum bedeutet, dass die Verarbeitungszeit je Zeile länger wird. Dieses Verhalten ist quadratisch mit der Anzahl der Zeilen verbunden. Daher dauert z.B. der Import von 100 Zeilen 10Sekunden, so dauert der Import von 200 Zeilen nicht 20Sekunden, sondern 40Sekunden.

{{% alert title="dringende Empfehlung" color="primary" %}}
Wenn der Import nicht durchläuft, also nach 1-2Stunden noch immer kein Ergebnis bringt, bzw. mit einer Fehlermeldung (Timeout) abbricht, Reduziere die Importdatei auf 10% der Datenmenge.<br>
Beispiel aus der Praxis: Der Import der Verkaufspreispflege mit ca. 9.000 Positionen ist nicht durchgelaufen. Hat man dies auf ca. 1000 Positionen begrenzt, geht es innerhalb angemessener Zeit.
{{% /alert %}}

## Lagerplätze mit Mengen
## Palettenlager
Es kommt sehr selten, aber doch, immer wieder der Wunsch, wir wollen auf den Lagerplätzen auch Mengen haben. Es reicht (angeblich) nicht aus, dass man weiß auf welchen Lagerplätze die Artikel liegen, sondern man muss auch wissen wieviele es sind.

Der Hintergrund bei einem Anwender ist / war, dass die Ware auf Paletten liegt und die Paletten eine laufende Nummer haben. Diese Paletten sind wieder im Container Lager (in einen großen Container gehen 32 Paletten rein), oder im Außenlager E oder oder.
Daraus ergibt sich ganz klar, dass ich wissen muss, wo sollte ich hingehen um die 10.000Stk zu holen.

Ein Lösungsansatz dafür ist, dass jede Palette eine Charge ist, ähnlich der Gebindeverwaltung. Damit bucht man eine Palette zu und hinterlegt bei der Charge die laufende Paletten Nummer.
Meist kommt dann auch noch die Frage, und wo genau, an welcher Stelle des Schwerlast-Hochregallagers steht denn nun die Palette. Hier kann man die "Version" der Charge nutzen. D.h. es wird in die Version dann der Lagerplatz, welcher selbstverständlich strukturiert aufgebaut ist, eingetragen. Siehe dazu Parameter: VERSION_BEI_CHG_MITANGEBEN. Für die Anpassung der Formulare, von Packlisten bis, einfach an den **Kieselstein ERP** Betreuer wenden.

## Urlaub für alle Mitarbeiter:innen eintragen
Die Frage war,

wenn man den Urlaub für alle einträgt, wie wird dann Krank usw. berücksichtigt.

Hier ist es so, wenn in den Zeitdaten Einträge sind (Kommt, Geht, ..)
dann wird für den jeweiligen Tag kein Urlaub eingetragen.

Sind aber Sondertätigkeiten vorhanden, wie z.B. Krank, dann wird trotzdem ein Urlaubseintrag gemacht. D.h. gegebenenfalls gibt es dann für diese Tage sowohl einen Urlaubs- und einen Zeitausgleichseintrag.<br>
Oder in anderen Worten: Sonderzeiten werden nicht berücksichtigt.

## Durchmesser-Zeichen eingeben.
Das Durchmesserzeichen kann mit Alt+0216 Ø auf dem nummerischen Block eingegeben werden.<br>
Hinweis: Üblicherweise ist das Durchmesserzeichen bei der Erfassung der Artikelnummern deaktiviert. Es kann dies zwar in den Parametern freigeschaltet werden. Wir raten jedoch dringend davon ab, da die Artikelnummern in der Regel auch in Barcodes gedruckt werden. Diese sind in aller Regel (noch) Strichcode, welche die ganzen Sonderzeichen nicht drucken können. Einzig der QR-Code kann vermutlich auch das Durchmesserzeichen drucken. Das würde eben bedeuten, dass für dich alle Formulare die Barcode drucken auf den QR-Code umzustellen wären.

## Kann man die Mandanten Nummer ändern?
In einer bestehenden Installation bei der schon Kunden, Lieferanten, Artikel, MwSt-Sätze, Belege usw. gegeben sind, ist ein, "ich ändere mal schnell die Mandantennummer" faktisch unmöglich.<br>
Man könnte es eventuell über einen Datenexport mit reinem SQL Query versuchen, in dem man dann die 'alt' auf 'neu' ändert. Wehe wenn man dann ein Feld übersehen hat, dann wird der Import krachen. Also, extreme Vorsicht.

## Bei einer Lagerbuchung kommt Fehler_Abbuchung
Versucht man eine Lagerbuchung, so kommt es in sehr seltenen Fällen auch mal dazu, dass die verschiedenen Lagertabellen nicht übereinstimmen.

D.h. bucht man einen Artikel so erscheint ein schwerer Fehler.

![](Lagerstandsfehler.png)

Klickt man hier auf Details, so steht in der Fehlermeldung<br>
> class com.lp.client.frame.ExceptionLP<br>
> java.lang.Exception: <br>
> FEHLER_ABBUCHUNG_OHNE_URSPRUNGSEINTRAG_NICHT_MOEGLICHFEHLER_ABBUCHUNG_OHNE_URSPRUNGSEINTRAG_NICHT_MOEGLICH, ARTIKEL_I_ID=28248 SNR_CHNR: null ABZUBUCHENDEMENGE=384.0 MENGE_AUS_URSPRUENGEN=383.0000

Wie kann man das reparieren?

Voraussetzung ist, du kennst die Artikelnummer des Artikels den du gerade buchen wolltest, so kann über die Pflegefunktion im System dieser Fehler insofern korrigiert werden, dass der Artikel wieder bebucht weden kann.

Also:
- Gehe auf System, unterer Reiter Pflege
- wähle den oberen Reiter Pflege und dann
- Prüfe Lagerstände<br>
![](Pflege_Lagerstand.png)
- nun die zu prüfende Artikelnummer eingeben<br>
 ![](Pflege_Lagerstand2.png)
- Die Frage nach der Korrektur des Lagerstandes mit Ja beantworten<br>
![](Pflege_Lagerstand3.png)
- Wichtig:
Danach unbedingt den Lagerstand des Artikels inkl. Chargen- und Seriennummern prüfen und gegebenenfalls richtig stellen.

## EMail-Versand des neuen Mandanten geht nicht
Wenn ein weiterer Mandant angelegt wurde, kommt es immer wieder vor, dass der EMail-Versand des neuen Mandanten nicht funktioniert.

Genau genommen geht der gesamte Automatik-Job für den neuen Mandanten nicht, da die Berechtigung des Spezialbenutzers nicht eingetragen wurde.

Man sieht das auch im server.log. Hier steht zuerst try logon > usr: lpwebappzemecs|Kieselsteinserver-Name und in der folgenden Zeile: Das wechseln des Mandants schlug fehlt: ....<br>
![](Fehler_bei_Mandant_wechseln.png)<br>

Die Ursache ist, dass unter Benutzer, Benutzermandant, der Benutzer lpwebappzemecs für den gewählten Mandanten keinen Eintrag, bzw. keine ausreichende Berechtigung hat.<br>
Diese bitte, gleichlautend wie für den funktionierenden Mandanten nachtragen.

## Automatikjob für den Mandanten geht nicht
[siehe oben](#email-versand-des-neuen-mandanten-geht-nicht)
