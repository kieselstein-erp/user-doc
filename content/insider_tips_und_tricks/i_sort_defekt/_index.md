---
title: "Sortierung defekt"
linkTitle: "Sortierung defekt"
categories: ["Tipp"]
tags: ["Sortierung defekt"]
weight: 40
description: >
  Sortierung defekt, wie reparieren
---
In sehr seltenen Fällen kommt es vor, dass die in den Bewegungsdaten enthaltene Sortierung durcheinander kommt.

Man merkt das daran, dass die Reihenfolge der Positionen mit den blauen Pfeilen ![](blaue_pfeile.png) nicht mehr verändert werden kann. D.h. es bleibt quasi die Sortierung stecken, kann nicht mehr weiter verschoben werden.

In anderen Fällen kommt es vor, dass bei der Übernahme in den Nachfolge-Beleg ein Serverfehler gemeldet wird ![](ServerFehler.png).<br>
Im Detail steht dann, ziemlich am Anfang<br>
![](ServerFehler_Detail.png)<br>
returns more than one elements.

## Wie kann man den Sortierungsfehler reparieren
Dafür muss man in die Datenbank gehen und die Detailpositionen des jeweiligen Beleges manuell richtigstellen.<br>

{{% alert title="ACHTUNG" color="warning" %}}
Du arbeitest direkt in der Datenbank, ohne Programmkontrolle.

Durch entsprechende Tippfehler können die Daten zerstört werden.

Wir raten: **Mache vorher ein Backup** und mache das zu Zeiten, an denen sonst keine Arbeiten im System durchgeführt werden, damit du gegebenenfalls das Backup wirklich wieder einspielen kannst.
{{% /alert %}}

Es sei dies am Beispiel des Angebotes 24/0368 des Mandanten 001 erklärt.
1. starten PGAdmin und als postgres mit allen Rechten anmelden
2. Wähle deine Kieselstein Datenbank aus und die entsprechende Tabelle der Positionen der Bewegungsdaten. In unserem Beispiel wollen wir die Angebotspositionen verändern und klicken daher auf angb_angebotposition (ACHTUNG: Das Verhalten ist je nach PGAdmin Version leicht unterschiedlich)
![](Sort01.png)
3. Klicke nun oben auf 2 Query Tool
4. Führe nun eine Abfrage nur auf die Positionen dieses einen Beleges aus, in unserem Falle die Positionen des Angebotes.
> select * from angb_angebotposition where angebot_i_id=
>	(select i_id from angb_angebot where c_nr='24/0368' and mandant_c_nr='001');

Ergebnis:<br>
![](Sort02.png)

5. Du siehst nun in dem Ergebnis der Abfrage, dass der i_sort 2 doppelt vorhanden ist. Dies ist der Auslöser für das Fehlverhalten, die Fehlermeldungen.
6. d.h. du gibst nun einer der Datenzeilen einen neuen I_Sort.<br>
Wir nutzen dafür die Logik, dass jede Datenzeile einer Tabelle immer eine eigene I_ID hat.
Z.B. update angb_angebotposition set i_sort=5 where i_id=29587;
Achte hier darauf wirklich nur diesen einen Datensatz zu verändern.
7. Machst du nun obige Abfrage erneut, so ist jeder I_Sort nur einmal vorhanden.
8. Vermutlich ist nun die Sortierung nicht ganz so wie du sie erwartest, bzw. an den Kunden gesandt hast. D.h. du gehst nun in den betroffenen Beleg und korrigierst die Sortierung wie gewünscht.
9. Gegebenenfalls musst du den Beleg nach der Richtigstellung der tatsächlichen Sortierung noch aktivieren.
