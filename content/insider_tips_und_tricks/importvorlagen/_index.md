---
title: "Importvorlagen"
linkTitle: "Importvorlagen"
categories: ["Import"]
tags: ["Vorlagen"]
weight: 20
description: >
  Eine Sammlung praktischer Importvorlagen um Massendaten ins System zu bekommen
---
Eine Sammlung praktischer Importvorlagen um Massendaten ins System zu bekommen.

Bitte ausschließlich bei entsprechenden System und Datenbankkenntnissen verwenden.

## Einspielen von Zeitbewegungsdaten
[Zeitbewegungen importieren]( ./Zeitdaten_import.ods)<br>
Hier findest du ein Musterfile für die Zeitdatenübernahme.<br>
Üblicherweise wird vom Datenlieferanten / Anwender / Mitglied nur das erste Blatt ausgefüllt.
Wichtig die Formatierung belassen (Also Personalnummer als Text und den Zeitpunkt als Datum mit Uhrzeit)
An Sondertätigkeiten sind KOMMT, GEHT, UNTER, ENDE erlaubt.
Die Personalnummer müssen bitte mit den Einträgen im Kieselstein übereinstimmen. Solltet ihr nur die Ausweisnummern haben, dann gerne. Dann ist die Importer Vorlage entsprechend anzupassen.

Über das zweite Blatt = Importer erhältst du ein entsprechendes Insert-Script. Unbedingt daran denken, am Schluss den Indexeintrag in der LP_PRIMARYKEY zu aktualisieren.

## Kontenrahmen importieren
Um bei aktivierter integrierter Finanzbuchhaltung den Kontenrahmen importieren zu können, kannst du dies nach beiliegender Vorlage machen. Die Datei ist eine Textdatei, mit UTF8 und als Feldtrenner bitte \<Tab>.<br>
Wichtig auch, die Definitionen für die Spalten F_Kontoart, F_Ergebnisgruppe, UVA, Finanzamt müssen vorhanden sein. F_GUELTIG_VON bitte im Ansi Format (yyyy-mm-tt).<br>
Die boolschen Felder mit 1 = angehakt und 0 = nicht angehakt besetzen.<br>
Aufgrund des CSV Formates ist die Spaltenreihenfolge wichtig.
- Vorlage [DE-SKR04]( ./Kontenrahmen_SKR04.csv) bzw. [Vorlage](Kontenrahmen_SKR04.ods)
- Vorlage [österreichischer Einheitskontenrahmen](AT-Einheitskontenrahmen.csv) bzw. [Vorlage](AT-Einheitskontenrahmen.ods)


