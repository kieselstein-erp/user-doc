---
title: "Tipps, Abläufe"
linkTitle: "Tipps, Abläufe"
categories: ["Tipp"]
tags: ["Abläufe"]
weight: 30
description: >
  Tipps und Abläufe
---
Tipps und Abläufe
=================

Eine Sammlung von Anwenderfragen, welche aus Sicht der Module keinem wirklichen Bereich zugeordnet werden kann.

Abbildung von Wartungsverträgen
-------------------------------

Mit welchem Modul / welcher Funktion können in **Kieselstein ERP** Wartungsverträge verwaltet werden?
Beispiel: Kunde Meier hat über mich einen Wartungsvertrag seiner Server gekauft. Es sollten das Ablaufdatum und eine grobe Beschreibung gepflegt werden.

Dafür gibt es verschiedene Ansätze, welche auch mit den sonst üblichen / bewährten Abläufen im Unternehmen zusammen hängen.

-   Monatliche / wiederkehrende Rechnungen, da ja dies solange läuft, bis der Kunde seinen Vertrag kündigt
    wenn das so etwas ist, wie unsere monatlichen / Quartalsweisen Subskriptionsrechnungen, dann würde ich das als Wiederholenden Auftrag anlegen mit einem Verrechnungsbeginn usw.
    Das bedeutet dann, dass man z.B. einmal zum Monatsbeginn im Rechnungsmodul auf Extras, Wiederholende Aufträge verrechnen klickt und automatisch die Rechnungen angelegt bekommt

-   Ist es aber eher so etwas wie Wartung, dann könnte man für jeden Vertrag einen Artikel anlegen, bei diesem im Reiter Sonstiges das Wartungsintervall hinterlegen und auch die letzte Wartung definieren.
    Zugleich kann man am Artikel in der Dokumentenablage die ganzen Belege und Verträge etc. hinterlegen.
    Und man erhält im Artikel, Journal, nächste Wartungen eine Übersicht wann man sich worum kümmern muss. [Siehe](../artikel/index.htm#Prüfmittelverwaltung).

-   Eine dritte Variante wäre das in den Angeboten abzubilden, mit einem Nachfasstermin von z.B. in einem Jahr

-   Man legt sich im Projekt einen Bereich laufende Wartungsverträge an (nach der Bereichsanlage (in den Grunddaten) das Modul neu starten)
    Hier hat man dann z.B. die Kategorie Server und hinterlegt da was alles vereinbart ist.
    Auch hier gibt es Zieltermine usw. nach denen man sortieren kann.
