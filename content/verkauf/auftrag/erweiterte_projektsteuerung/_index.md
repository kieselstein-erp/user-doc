---
title: "Erweiterte Projektsteuerung"
linkTitle: "Erweiterte Projektsteuerung"
categories: ["Auftrag"]
tags: ["Erweiterte Projektsteuerung"]
weight: 600
description: >
  Erweiterte Projektsteuerung im Auftrag, Milestones, Auftrags-Liquiditätsplan  
---
Erweiterte Projektsteuerung im Auftrag
======================================

Mit der Zusatzfunktion erweiterte Projektsteuerung im Auftrag steht Ihnen die Abbildung der Planungs- und Realisierungsaufgaben von der groben Schätzung hin zu den Details inkl. dem jeweiligen Liquiditätsplan zur Verfügung.

Die Planung wird für jeweils einen Auftrag erstellt.
Im Reiter Zahlungsplan geben Sie die Voraussetzungen für die Rechnungslegung zu diesem Betrag und damit die Meilensteine für diesen Auftrag an. Jeder Zahlungstermin kann mehrere Meilensteinvoraussetzungen für seine Erfüllung haben.
Die verfügbaren Meilensteinvoraussetzungen werden im Auftrag in den Grunddaten, Meilensteinvoraussetzung definiert. 

Für die erweiterte Projektsteuerung wird der erste Teilnehmer als Projektleiter angesehen.
Die Funktion kann im Modul System, Sprache, Sachbearbeiterfunktion definiert werden.

**Zahlungsplan:**
Im Reiter Zahlungsplan![](zahlungsplan.JPG) legen Sie die geplanten Zahlungen mit den Terminen und Voraussetzungen für diesen Auftrag an.
Neben der Erfassung der Beträge werden auch die ursprünglichen Beträge protokolliert. Die ursprünglichen Beträge können nur verändert werden, wenn Sie das Recht AUFT_DARF_AUFTRAG_ERLEDIGEN besitzen.
Im Zahlungsplan bzw. in den Meilensteinvoraussetzungen für die jeweilige Erreichung des Zahlungsplanschrittes können die einzelnen Schritte als erledigt angehakt werden. Dies wirkt neben der Liquiditätsvorschau auch im Meilensteinjournal. Wurde ein Zahlungsschritt / ein Meilenstein versehentlich erledigt, so kann durch einen erneuten Klick auf Erledigt dies wieder zurückgenommen werden.
Wenn für einen Zahlungsschritt mehrere Voraussetzungen erfüllt werden müssen, so geben Sie dies bitte im Reiter Voraussetzungen Meilenstein für den jeweiligen Zahlungsschritt an.
Achtung: in der Finanzbuchhaltung/Liquiditätsvorschau gilt: Die Summe aller Zahlungspläne wird von der BS-Pos-Summe abgezogen und nicht nur die Summe der offenen Zahlungspläne.

**Zeitplan:**
Im Reiter Zeitplan ![](zeitplan.jpg) geben Sie den geplanten Aufwand für die Herstellung des Auftrages, des einzelnen Schrittes, ev. in zeitlicher Korrelation zum Zahlungsplan an. 
Dieser ist in zwei Bereich gegliedert: Der geplante Materialbedarf und der geplante Zeitbedarf (Dauer) um bis zum angegebenen Termin die angeführten Arbeiten durchführen zu können. Hier können Material und Dauer eigenständig erledigt werden. Verwenden Sie für das Material das Icon ![](material_erledigt.JPG) und für die Dauer das Icon![](dauer_erledigt.JPG). Selbstverständlich wirkt auch dies in den jeweiligen Journalen.

Mit Klick auf das Icon ![](zeitplan_importieren.JPG) können Sie einen Zeitplan importieren.

**WICHTIG:** Die Verbindung zwischen den geplanten Erlösen und Aufwänden und den durch Bestellungen, Eingangsrechnungen, Lieferscheine, Ausgangsrechnungen bereits fix terminisierten Zahlungsflüssen wird **NUR** über die Erledigung der einzelnen Schritte, im Zeitplan bzw. im Zahlungsplan, durchgeführt. Beachten Sie daher, dass diese Daten übereinstimmen. Zur Überprüfung kann die Liquiditätsvorschau am einzelnen Auftrag, (Info, Projektblatt) verwendet werden.

**Übersicht über den Projektstatus des einzelnen Auftrags:** Unter Info, Projektblatt wird die Projektsituation des Auftrages in drei Bereichen dargestellt:
a.) Zahlungsplan.
Hier sehen Sie, nach Termin sortiert, die Entwicklung der Zahlungsplanschritte

Mit der erweiterten Projektsteuerung sind auch zwei Liquiditätsbetrachtungen verbunden:
b.) Die Liquiditätsbetrachtung am einzelnen Auftrag anhand Zahlungsplan (inkl. Meilensteinenplanung) und Zeitplan mit geplanten Material und Mitarbeiterkosten
c.) Die Gesamt-Liquiditätsvorschau
In dieser sind auch die Personalkosten, die abzuführende Umsatzsteuer, die Sozialabgaben und die Plandaten aus dem einzelnen Liquiditätsbetrachtungen des Auftrags mit enthalten. [Siehe dazu.]( {{<relref "/management/finanzbuchhaltung/liquiditaetsvorschau/#mit-abgaben" >}} )

Projektblatt: Es werden alle Zahlungspläne angezeigt, jedoch die erledigten mit dem Offen-Betrag = 0 und Status = Erledigt. Ausserdem wird die Summe aller Zahlungspläne von der BS-Pos-Summe abgezogen.

Liquiditätsvorschau: Die Summe aller Zahlungspläne wird von der BS-Pos-Summe abgezogen.

Zahlungsplan: Ist im Detail das erledigt Hakerl disabled, so bedeutet dies, dass mehrere Voraussetzungen für die Erfüllung dieses Zahlungsplan-Schrittes erforderlich sind. Wechseln Sie in diesem Falle in den Reiter Meilenstein Voraussetzungen und bestätigen Sie hier die einzelnen Schritte um die vollständige Erledigung zu erreichen.

![](erweiterte_Projektsteuerung1.jpg)

Hinweis: Die Beträge in der Auftrags-Liquiditätsvorschau sind Netto Beträge, im Gegensatz zur Gesamt Liquiditätsbetrachtung, die den tatsächlichen Zahlungsfluss auf Basis der Brutto-Beträge errechnet.

Die Stunden aus dem Zeitplan brauchen in der Gesamt-Liquiditätsvorschau nicht berücksichtigt werden, da die Gehaltsabgaben, welche sowieso immer zu leisten sind, in der Funktion Abgaben mit enthalten sind. D.h. es wird davon ausgegangen, dass eine vernünftige Abstimmung der offenen Planstunden gemacht wird, wodurch sich dann kein Bedarf für fiktive Berechnungen ergibt.
Wichtig ist in den Gehältern, dass auch die Gehälter aller administrativen Mitarbeiter enthalten sind.

Journale
| Journal | Bedeutung |
| --- | --- |
| Meilensteine | liefert eine Liste der offenen Zahlungsplanpositionen mit Terminen und noch nicht erledigten Meilensteinen dazu |
| Materialbedarfe | liefert eine Liste der offenen Materialbedarfe mit den geplanten Terminen |
| Planstunden | bringt eine Gegenüberstellung der unter Zeitplan eingeplanten Dauer zum jeweiligen Termin gegenübergestellt mit den verfügbaren Personalstunden. Die verfügbaren Personalstunden werden in der Form errechnet, dass die geplante Anwesenheitszeit jedes Mitarbeiters laut Zeitmodell mit dem Faktor Verfügbarkeit (im Modul Personal, Reiter Gehalt)  multipliziert mit dem Leistungsfaktor (z.B. für Lehrlinge / Azubis) pro Wochentag angeführt wird. Es werden die Mitarbeiter berücksichtigt, die an den jeweiligen Tagen im Unternehmen eingetreten sind |

Ergänzend zur eigentlichen Projektsteuerung aus dem Auftrag gehört zu diesem Thema auch der Zahlungsplan auf der jeweiligen Bestellung mit dazu.<br>
Das bedeutet: Definieren Sie für Bestellungen bei denen Sie wissen, dass es dafür zu Eingangs-Anzahlungsrechnungen kommen wird, auch die jeweiligen geplanten Zahlungstermine. Sie finden diese in den Bestellungen, Reiter Zahlungsplan.<br>
Auch hier können / müssen bereits erledigte geplante Positionen entsprechend abgehakt werden.

#### Planstunden
Die Darstellung der Planstunden, kann in Verbindung mit den Auftragsteilnehmern sehr praktisch für die Auslastungsplanung genutzt werden.<br>
Da die Planstunden nur auf die Verfügbarkeit der Personen, Personal, Reiter Gehalt, abzielt, kann eine praktische Trennung zum Bereich der Fertigung und der Auslastungsplanung der Fertigung gemacht werden, da die Auslastungsvorschau die verfügbaren Tätigkeiten aus Personal, Reiter Verfügbarkeit nimmt.
![](Planstunden.jpg)

Tipp: Trennen Sie die echten Arbeitszeiten aus der Fertigung mit den sonstigen Zeiten wie Montagen oder ähnlichem. D.h. die echten Fertigungszeiten sollten auf die Lose gebucht werden, die Montage bzw. Konstruktionszeiten aber auf die Aufträge. So gewinnen Sie einerseits einen klaren Überblick über die tatsächlichen Fertigungskosten und andererseits haben Sie im Journal Auftragszeitstatistik, im Modul Zeiterfassung, eine Übersicht welcher Mitarbeiter wieviel auf welchem Auftrag gebucht hat. Ergänzend kann dazu natürlich auch die Produktivitätsstatistik verwendet werden.