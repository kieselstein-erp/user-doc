---
title: "Vom Auftrag zur Rechnung"
linkTitle: "Vom Auftrag zur Rechnung"
categories: ["Auftrag"]
tags: ["Vom Auftrag zur Rechnung"]
weight: 200
description: >
  Kompakte Zusammenfassung der Schritte um vom Kundenauftrag zur Rechnung zu kommen
---
Vom Auftrag zur Rechnung
========================

Da vor allem die neuen Anwender von **Kieselstein ERP** immer wieder Fragen zum Thema stellen, wie komme ich am effizientesten von einem erfassten Auftrag zur Bestellung der Teile, zur Auslieferung und im Endeffekt zur Rechnung, hier eine kurze und kompakte Zusammenfassung der erforderlichen Schritte um dies, bei gut eingestellten Basisdaten Ihres **Kieselstein ERP** zu den gewünschten Daten / Belegen zu kommen.

## 1.) Ablauf ohne Fertigung

Dieser Ablauf ist oft in Handelsunternehmen anzutreffen, kommt aber auch immer wieder in produzierenden Unternehmen zum Einsatz, da auch diese oft reine Handelsprozesse abbilden.

Auftrag -> Bestellvorschlag -> Bestellungen -> Wareneingang bucht auf Lager -> Lieferschein -> Rechnung -> Zahlung

![](ab_ls.jpg)

-   Erfassen des Auftrages mit den Positionen. Die Positionen sind Artikel, die Termine sind die Eintrefftermine beim Kunden. Damit werden die Artikel zu den gewünschten Terminen im Artikelstamm reserviert.

-   Im Bestellmodul, unterer Modulreiter Bestellvorschlag.
    Klicken Sie auf neuen Bestellvorschlag erzeugen.
    Es wird nun der gesamte Artikelstamm durchgearbeitet und für jeden Artikel die Berechnung der Bewegungsvorschau durchgeführt., Vereinfacht gesagt:<br>
    Lagerstand<br>
    \- Reserviert<br>
    \- Fehlmenge<br>
    \+ offene Bestellungen<br>
    --------------------- ergibt<br>
    verfügbare Menge zum Zeitpunkt (Tag),<br>
    unterschreitet diese den Lagermindestbestand, wird auf den Sollbestand aufgefüllt. Diese Menge wird als zu bestellen vorgeschlagen.

-   Im Bestellvorschlag sehen Sie im ersten Lauf die Artikel mit jeweils exakt den errechneten Mengen.<br>
Mit Verpackungsmengen einrechnen wird die Anzahl der zu bestellenden Positionen gegebenenfalls verdichtet.

-   Leiten Sie nun die Positionen des Bestellvorschlages einzeln je Lieferant oder alle auf einmal in die Bestellungen über.

-   Senden Sie die einzelnen Bestellungen an die Lieferanten

-   Verbuchen Sie die Wareneingänge auf den jeweiligen Bestellungen. Damit wandern die Artikel auf Lager.

-   Drucken Sie die Liste der offenen Aufträge aus dem Modul Aufträge, Journal offene Aufträge inkl. Detail aus oder auch Journal offene Details und beachten Sie ausschließlich die lieferbaren Aufträge.<br>
Alternativ, nutze den Ausliefervorschlag im Modul Lieferschein.

-   Wählen Sie nun im Lieferscheinmodul neuer Lieferschein anhand Auftrag, wählen Sie einen der auszuliefernden Aufträge aus.<br>
Übernehmen Sie mit alle lieferbaren Positionen übernehmen, diese Positionen in den Lieferschein und drucken Sie den Lieferschein aus.
Verfahren Sie damit solange bis alle gewünschten Lieferungen durchgeführt sind.<br>
Nutzen Sie dazu gegebenenfalls auch die Funktionalität der Packliste aus dem Modul Auftrag (Auftrag, Packliste im Menü)

-   Verrechnen Sie alle verrechenbaren Lieferschein, durch Start des Moduls Rechnung und klick auf Rechnung aus Lieferschein erstellen. Verfahren Sie so für alle offenen Lieferscheine und selbstverständlich werden die Rechnungen sofort an den Kunden versandt.

-   Für den Zahlungseingang wählen Sie bei der jeweiligen Rechnung den Reiter Zahlungen und buchen so die Zahlungen zu.

## 2.) Ablauf mit Fertigung

Ergänzend zum obigen Ablauf ist zwischen Auftrag und Bestellvorschlag die Auflösung der Auftragspositionen mit Stücklisten eingebunden.

Auftrag -> Bestellvorschlag -> Bestellungen -> Wareneingang bucht auf Lager -> Lieferschein -> Rechnung -> Zahlung

   !                   ^----------------------------------------+                !  ^---------------------------------------+

   + -> Interne Bestellung -> Anlegen der Fertigungsaufträge +               +bucht in Los -> Fertigung -> Losablieferung +

![](AB_LOS_LS.JPG)

-   Sie beginnen unabhängig ob Handelsware oder eigen gefertigte Produkte mit dem erfassen des Auftrages mit den Positionen. Die Positionen sind Artikel, die Termine sind die Eintrefftermine beim Kunden. Damit werden die Artikel zu den gewünschten Terminen im Artikelstamm reserviert.

-   Im Modul Fertigungsauftrag, unterer Modulreiter Interne Bestellung
    Klicken Sie auf interne Bestellung durchführen.
    Es wird nun ebenfalls der gesamte Artikelstamm durchgearbeitet, mit der Einschränkung, dass nur diejenigen Artikel berücksichtigt werden, die eigen gefertigte Stücklisten sind. Vereinfacht dargestellt:<br>
    Lagerstand<br>
    \- Reserviert<br>
    \- Fehlmenge<br>
    \+ offene Fertigungsaufträge<br>
    \--------------------- ergibt<br>
    verfügbare Menge zum Zeitpunkt (Tag),<br>
    unterschreitet diese den Lagermindestbestand, wird auf den Sollbestand aufgefüllt. Diese Menge wird als zu fertigen  vorgeschlagen.

-   In der internen Bestellung sehen Sie die jeweils exakt errechnet Menge der benötigten Fertigungsaufträge / Lose.<br>
    Mit Interne Bestellung verdichten, werden alle innerhalb von z.B. 14Tagen zu produzierenden Mengen auf den frühest erforderlichen Termin zusammengefasst, zugleich werden die Fertigungssatzgrößen berücksichtigt und eine Überprüfung der offenen Rahmenaufträge durchgeführt.

-   Leiten Sie nun die Positionen der internen Bestellung in einzelne Fertigungsaufträge über und legen so die entsprechenden Lose an.

-   Durch die Losanlage wird das benötigte Material reserviert und Sie können für die Beschaffung wie oben beschrieben bei der Erzeugung des Bestellvorschlages fortsetzen.<br>
Hinweis: Die Ausgabe der Lose ist dafür NICHT erforderlich.

-   Mit dem Wareneingang wird wie oben beschrieben die Ware an Lager gebucht.

-   Nun können die Fertigungsaufträge auf in Produktion gesetzt werden (geht auch früher, dann gibts Fehlmengen) -> Siehe dazu auch die Bedeutung der rechten Spalte in den Losen.

-   Ist die Stückliste fertig produziert, so wird mit Ablieferung das fertige Produkt an Lager abgeliefert und Sie können bei den Lieferscheinen fortsetzen.