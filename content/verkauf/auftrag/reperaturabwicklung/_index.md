---
title: "Auftrag"
linkTitle: "Reperaturabwicklung"
categories: ["Auftrag"]
tags: ["Reperaturabwicklung"]
weight: 400
description: >
  Beschreibung einer möglichen Reperaturabwicklung
---
Reparaturabwicklung
===================

Da wir in der Praxis immer wieder gefragt werden, wie Reparaturen, z.B. von Geräten, abgewickelt / abgebildet werden können, hier eine kurze Zusammenfassung wie dies in **Kieselstein ERP** derzeit realisiert werden kann.

**Hintergrund:**

Meistens handelt es sich dabei um Geräte / Produkte die Seriennummern behaftet sind und bei denen man auch aus der Historie ersehen möchte, dass dieses Gerät / diese Baugruppe schon mehrfach repariert wurde.
Zusätzlich weiß man oft nicht, welches Material und wieviel Arbeitszeit für die Reparatur eines Gerätes aufgewendet werden sollte.

Ablauf für eine Reparatur an Gerät X mit Seriennummer 100.

1.  Das Gerät X kommt von Kunden zurück.
    Erstellen Sie einen Lieferschein mit negativer Menge an den Kunden und buchen Sie den angelieferten Artikel auf das Reparaturlager. Ist der Artikel als Seriennummern tragend definiert, so muss die angelieferte Seriennummer angegeben werden.

2.  Für die eigentliche Reparatur wird ein Los angelegt, welches
    -   eine freie Materialliste ist
    -   in dem der Kunde hinterlegt wird
    -   in dem Artikel, Seriennummer und Lieferscheinnummer im Kommentar hinterlegt werden

3.  Wird nun die Reparatur durchgeführt, so buchen Sie die Mitarbeiter- und Maschinenzeiten und auch das gesamte benötigte Material auf dieses Los.

4.  Ist die Reparatur abgeschlossen, so erledigen Sie das Los durch eine Ablieferungsbuchung.

5.  Senden Sie nun das Gerät mit einem Lieferschein aus dem Reparaturlager wieder an den Kunden. Damit ist das Gerät wieder vom Reparaturlager abgebucht. D.h. Sie sehen auch an den Lagerständen des Reparaturlagers, wieviele Reparaturen noch offen sind.

Zusätzliche Verrechnung der Kosten für Material und Arbeit führen Sie gegebenenfalls als Handeingabe (Pauschale) bzw. als Arbeitszeitartikel (Dienstleistung) auf dem Lieferschein / auf der Rechnung an.
Die Beschreibung der durchgeführten Reparatur geben Sie als Texteingabe im Lieferschein mit dazu.
Denken Sie daran, den unter 1\. erstellt Lieferschein manuell zu erledigen.<br>
Alternativ dazu können Sie in beiden Lieferscheinen den Preis/Wert des Gerätes mit anführen und beide Lieferscheine in eine Sammelrechnung einbinden. Damit sieht Ihr Kunde auch klar wann das Gerät bei Ihnen eingetroffen ist und wann es repariert wieder ausgeliefert wurde.

Alternative mit kostenpflichtiger Reparatur

Es kommt immer wieder auch die Frage wie mit Revisionen, also kostenpflichtiger Überholung von Geräten vorgegangen werden sollte.<br>
Von der Idee her ist der Ablauf wie oben beschrieben, jedoch wird vor der Reparatur oft ein Angebot / Kostenvoranschlag an den Kunden gesandt und dann bei Annahme des Kostenvoranschlages auch eine Auftragsbestätigung. In diesen Belegen sind oft Reparatursätze und vielleicht auch auszutauschende Materialien und gegebenenfalls auch die Dienstleistung mit enthalten.
Es ist dies daher bei der Buchung des benötigten Materials entsprechend zu berücksichtigen.
Insbesondere wenn das reparierte Gerät dann auch noch durch den Zoll muss, ist nachfolgend beschriebene Variante zu empfehlen.

### Beispielfall:
Der Kunde hat vor einigen Jahren ein Gerät bekommen. Nun sollte dieses Gerät überholt, also einer Revision unterzogen werden.
- Er sendet dieses Gerät zu Ihnen und bittet um einen Kostenvoranschlag.
- Idealerweise bieten Sie Ihrem Kunden diese Revision mit einem Angebot an, in dem die pauschale Wartung / Revision und die aufgrund Ihrer Analyse erwarteten Ersatzteile und eventuelle zusätzliche Zeiten angeführt sind.
- Ihr Kunde bestellt.
- Sie führen die Reparatur durch.<br>
Gegebenenfalls stellt sich heraus dass noch weitere Teile benötigt werden.
- Das überholte Gerät wird an Ihren Kunden gesandt und die Wartungsarbeiten und die (zusätzlichen) Teile verrechnet.
- Als zusätzliche Erfordernis ergibt sich, dass das reparierte Gerät durch den Zoll muss, womit sie auch den statistischen Wert an der Grenzübertrittstelle benötigen.

Der Ablauf ist nun folgender:

1.  Erfassen Sie das einlangende Gerät mit einen Lieferschein mit negativer Menge bzw. bei Seriennummerntragenden Artikeln haken Sie im Seriennummerndialog Rückgabe an.<br>
Buchen Sie dieses Gerät auf ein Reparaturlager, da es ja nicht in Ihren Besitz übergeht.<br>
Als Verkaufspreis verwenden Sie den Listenpreis des Gerätes für den jeweiligen Kunden.

2.  Ist ein Kostenvoranschlag, ein Angebot für die Überarbeitung / die Reparatur gewünscht, so legen Sie dieses Angebot an und erfassen die Analysezeit bereits auf dieses Angebot.

3.  Kunde bestellt.

4.  Sie erstellen, aus dem Kostenvoranschlagsangebot eine Auftragsbestätigung und senden diese, je nach Gepflogenheit an Ihren Kunden<br>
In der Auftragsbestätigung ist eine
    -   Pauschale Position für die Revision enthalten
    -   die benötigten zusätzlichen Materialien
    -   die geschätzte zusätzliche Arbeitszeit

5.  Sie legen ein Los als freie Materialliste an und verbinden dieses mit dem Auftrag
Idealerweise verbinden Sie den oben zuerst als freien Lieferschein erstellten Lieferschein nun mit dem neu angelegten Auftrag.

6.  Sie führen die Reparatur / Gerätewartung durch<br>
Die für die Standardwartungsarbeiten benötigten Materialien, die im Auftrag in der Position Revision enthalten sind, buchen Sie mit Nachträglicher Materialentnahme ohne Sollposition ins Los (nutzen Sie dazu auch den mobilen Barcodescanner)

7.  Die Materialien die Sie zusätzlich benötigen buchen Sie in einen weiteren Lieferschein. Dieser muss sich auf den Auftrag beziehen.<br>
Wenn das Material geplanter Weise bereits im Auftrag enthalten ist, dann mit Auftragsbezug, sonst als Lieferschein-Position OHNE Auftragsbezug.

8.  Die Arbeitszeit wird direkt auf das Los gebucht.<br>
Theoretisch könnten auch die Zeiten je nach Position auf das Los und auf den Auftrag aufgeteilt werden. In der Praxis ist es aber den produzierenden / reparierenden Mitarbeitern nicht zumutbar, während der Erfassung zwischen diesen kaufmännischen Positionen zu unterscheiden.<br>
Zusätzlich will man sich gerne für die Verrechnung einen entsprechenden Spielraum schaffen, was den von den Stunden nun tatsächlich verrechnet wird, verrechnet werden kann.

9.  Ist die Reparatur abgeschlossen, so wird das Los erledigt.<br>
ACHTUNG: Da dies eine freie Materialliste ist, verschwindet dieser Verbrauch, da durch die freie Materialliste kein Rückfluss des fertigen Gerätes an das Lager erfolgt.

10. Das bedeutet, dass daran anschließend sofort ein Lieferschein des reparierten, überholten Gerätes erstellt werden sollte, mit dem das Geräte mit dem gleichen Preis wie oben, an den Kunden zurückgesandt wird.

11. Muss das Geräte durch den Zoll benötigen Sie vom Wesen her eine Proformarechnung. D.h. Sie binden NUR den zuletzt erstellten Lieferschein in die Profromarechnung ein. D.h. auf der Proformarechnung steht das nun neuwertige Gerät mit dem entsprechenden Verkaufswert.

12. Für die tatsächliche Verrechnung erstellen Sie eine Sammelrechnung mit allen drei Lieferscheinen.
    -   Also dem Lieferschein mit dem Sie das alte Gerät bekommen haben
    -   den Lieferschein in dem die zusätzlichen Dienstleistungen und Materialien enthalten sind
        Ob Sie in diesen Lieferschein auch die Position der Pauschalreparatur aufnehmen oder diese dann extra in diese Sammelrechnung übernehmen ist eine rein organisatorische Frage.
    -   den Lieferschein mit dem das Geräte wieder an den Kunden zurückgegangen ist.

        Diese Vorgehensweise bewirkt dass:
        1. die Warenbewegung, wann ist das Geräte bei Ihnen eingegangen und wann wurde es wieder geliefert
        2. zu welchen Verkaufspreise wurde das Gerät bewertet
        3. was machen die tatsächlichen Reparatur / Wartungskosten aus
        klar und für alle nachvollziehbar ersichtlich ist.<br>
        Da sich die Verkaufspreise der Rücknahme und des Wieder-Versandes aufheben (gleicher VK-Preis mit +/- Menge) steht im Endeffekt auf der Rechnung der vereinbarte Reparaturwert. Für den Zoll haben Sie dies in der Proformarechnung eindeutig ausgewiesen.

13. Nun kann mit der Auftragsnachkalkulation der tatsächliche Erfolg / Deckungsbeitrag dieses Reparatur / Revisionsauftrages ermittelt werden.

Fragen dazu:
Ist es richtig, dass ein Revisions-Artikel eine Stückliste sein muss, damit da ein Arbeitsplan erfasst werden kann und dann ein Los auf den Auftrag gelöst und abgeliefert werden kann?
-> Nein es kann auch auf eine freie Materialliste Arbeitszeit erfasst werden.

Verschrotten / Austausch

Wird beschlossen dass anstatt des defekten Gerätes ein anderes Gerät geliefert wird, so liefern Sie anstelle des defekten Gerätes eben ein Gerät mit einer anderen Seriennummer aus. Führen Sie gegebenenfalls beide Lieferscheine und damit beide Geräte (das defekte und das Austauschgerät) in der Sammelrechnung an. Wenn Sie für die Rücknahme eine eigene Preisliste haben, so geben Sie diesen Rücknahmepreis an und erhalten sofort den Fakturierungswert für den Geräteaustausch.

Bei einem Austausch senden Sie das Gerät mit einem Lieferantenlieferschein an Ihren Lieferanten.

Bei einer Verschrottung buchen Sie das Gerät bitte mittels einer Handlagerbewegung vom Reparaturlager ab und begründen dieses entsprechend. Einige Anwender buchen auch verschrottete Geräte in ein Verschrottungslos, um so im Laufe des Jahres eine eindeutige Kostenkontrolle über die verschrotteten Produkte zu erhalten.

Auftragsschnellanlage - Reparaturauftrag

Um die Abwicklung einer Reparatur möglichst schnell und einfach zu gestalten gibt es die Zusatzfunktion der Auftragsschnellanlage.
Hierbei finden Sie in der Menüleiste der Auftragsauswahl das Icon ![](auftragsschnellanlage.JPG) Auftragsschnellanlage.
Über das Icon Artikeleigenschaften ![](artikeleigenschaft_aendern.JPG) können Sie für den Artikel des Auftrags direkt die Eigenschaften erfassen beziehungsweise bearbeiten.
Nach Klick auf das Icon öffnet sich die Kopfdatenerfassung des Auftrags. Wählen Sie hier den Kunden aus und klicken auf speichern. 
Geben Sie entweder die Bestell- oder Projektnummer ein, ansonsten erscheint die Meldung:
![](Meldung_PJ_BS_Nr.JPG)
Nach dem Speichern erhalten Sie die Artikelauswahlliste, hier werden Musterartikel für bestimmte Arten der Reparaturen angelegt. Definieren Sie dazu für die jeweiligen Artikelgruppen einen Musterartikel und Kennzeichnen diesen mit der Kennung der Artikelgruppe und _MUSTER. 
Wählen Sie hier den gewünschten Artikel aus.

Geben Sie hier die definierten Inhalte (hier können Sie mit Ihrem **Kieselstein ERP** Betreuer eine Art Pflichtfelder bei Erfassung für einen Artikel des Reparaturauftrags definieren) ein.
![](positionsinformationen_abschnellanlage.JPG)
Wenn Sie den Haken bei Packliste drucken setzen, so wird im Anschluss der Druck der Packliste gestartet. Bestätigen Sie die Eingaben mit dem grünen Haken.
Im Anschluss wird ein Auftrag angelegt und Sie können mit der Reparatur und somit der Zeiterfassung beginnen. 
Der Auftrag ist nach Schnellanlage aktiviert und im Status offen. ACHTUNG: Dieser Auftrag ist nicht in der Dokumentenablage abgespeichert. (Dazu bitte die gewohnten Schritte zur Belegaktivierung durchführen)
Die Position im Auftrag wird als neuer Artikel mit den erfassten Informationen, der Artikelgruppe des Musterartikels und der Artikelnummer: Kennung der Artikelgruppe und Auftragsnummer angelegt.
Falls der Musterartikel als Vorzugstyp gekennzeichnet ist, so wird auch dies Information beim neu angelegten Artikel hinterlegt.

## Getrennte Erlöskonten für Reparaturen und allgemeine Lieferungen.

Hier bieten sich zwei verschiedene Ansätze an:

1.  Sie betrachten die Reparaturen wie eine eigene Abteilung. D.h. alles was Reparaturen betrifft muss der Kostenstelle Reparaturen (siehe System, Mandant, Kostenstellen) zugeordnet werden. Das hat den deutlichen Vorteil, dass ganz klar erkannt werden kann, welche Umsätze mit dieser Abteilung / mit diesem Profitcenter Reparaturen erzielt wird.

2.  Anstatt, aber auch zusätzlich zu obigem führen Sie einen Dienstleistungsartikel ein. Dieser hat z.B. die Artikelgruppe Reparaturen und bucht damit auch auf ein eigenes Erlöskonto. Selbstverständlich sehen Sie auch die Artikelgruppenumsätze z.B. in der Kunden-Umsatzstatistik.

Unsere Empfehlung ist eindeutig die Version 1. zu verwenden. Auch erachten wir eine Trennung der Erlöskonten in der Fibu als absolut nicht notwendig. Wozu haben Sie Ihr **Kieselstein ERP**.

Hier kommt mit dazu, dass Sie durch die Kostenstellen/Profitcenter Betrachtung in fast allen Modulen eine Trennung danach durchführen können. Also sehen Sie z.B. auch welche Angebote = Kostenvoranschläge Sie im Bereich Reparaturen gemacht haben.

Wie sollte das Reparaturlager angelegt werden?

Beim Reparaturlager ist darauf zu achten, dass diese Ware in aller Regel nicht Ihnen gehört und auch in den Bestellvorschlägen NICHT berücksichtigt wird. D.h. definieren Sie bitte das Reparaturlager wie folgt:

![](Reparaturlager.jpg)

Ev. sollte das Reparaturlager von der Lagerart her auch als Schrott-Lager definiert werden, was bewirkt, dass die Lagerstände des Schrottlagers nicht in den üblichen Betrachtungen mitgerechnet werden.