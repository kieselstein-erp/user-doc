---
title: "Nachkalkulation"
linkTitle: "Nachkalkulation"
categories: ["Nachkalkulation"]
tags: ["Nachkalkulation"]
weight: 100
description: >
  Nachkalkulation, Auftragsstatus
---
Auftragsnachkalkulation
=======================

Da vor allem die neuen Anwender von **Kieselstein ERP** immer wieder Fragen zum Thema stellen, wie komme ich am effizientesten von einem erfassten Auftrag zur Bestellung der Teile, zur Auslieferung und im Endeffekt zur Rechnung, hier eine kurze und kompakte Zusammenfassung der erforderlichen Schritte um dies, bei gut eingestellten Basisdaten Ihres **Kieselstein ERP** zu den gewünschten Daten / Belegen zu kommen.

Bitte beachten Sie auch den Unterschied zum Auftrags-[Erfolgsstatus](#Erfolgsstatus).

**1.) Auftragsnachkalkulation**

![](Nachkalkulation_AB.JPG)

<a name="Nachkalkulation"></a>

#### Nachkalkulation
![](Auftragsnachkalkulation.gif)

Mit der Auftragsnachkalkulation steht Ihnen ein mächtiges Werkzeug für die Beurteilung des Auftrags / Projekterfolges jedes einzelnen Auftrags zur Verfügung.

Es werden kurz gefasst die Aufwände den Erlösen gegenübergestellt. Durch die Trennung in Material und Arbeits- bzw. Maschinenzeit erhalten Sie die Aufteilung in Deckungsbeitrag I (DBI) und DBII.

Es werden alle an den Auftrag gebundenen Belege angeführt. Diese sind:<a name="Belege"></a>

| Art | Beleg | Inhalt / Bedeutung |
| --- |  --- |  --- |
| ER | Eingangsrechnung | Nettokosten der Auftragszuordnung der Eingangsrechnungen |
| (ER) | Eingangsrechnung | wird in Mat.Ist nicht mitgerechnet, da in AB nicht berücksichtigen gesetzt ist |
| LS | Lieferschein | Positionen des Lieferscheines gegliedert in VK-Ist und Gestehungspreis-Ist |
| RE | (Ausgangs-)Rechnung | Positionen der Rechnung gegliedert in VK-Ist und Gestehungspreis-Ist für direkt dem Auftrag zugeordnete Rechnungen |
| GS | Gutschrift | (Gutschriften die indirekt über die Rechnung dem Auftrag zugeordnet werden können) |
| RS | Rückschein | (negativer Lieferschein der indirekt über den Lieferschein am Auftrag hängt) |
| LA | Losablieferung | Los-Kopfpositionen mit Arbeits- und Materialanteilen, welche über den Verbrauch in LS oder RE dem Auftrag zugeordnet werden können. |
| AB | Auftragsbestätigung | Die Auftragsbestätigung mit den Verkaufssollwerten (Arbeit und Material), den geplanten Aufwänden nach Gestehungspreisbetrachtung und den direkt diesem Auftrag zuordenbaren Zeiten geteilt in Arbeits- und Maschinenstunden.Unter Material Soll werden nur die Gestehungspreise(werte) jener Positionen summiert, die nicht selbst gefertigt werden. Also die Summe der Gestehungspreise alle Positionen mit Ausnahme der Stücklisten, bei denen Fremdfertigung nicht angehakt ist. Die Gestehungswerte dieser Positionen sind ja in den direkten oder indirekten Losen enthalten. |
| ZZ | Fertigungswerte | In dieser Zeile sind jene Gestehungspreise als Material Ist summiert, welche auch in Lieferscheinen enthalten sind, um die doppelte Betrachtung wieder herauszurechnen. |
| DR | Dienstreise die mit dem Auftrag verbunden ist, inkl. aller Reisekosten |
| BS | (Offene) Bestellung mit der Bewertung ob die Artikel / Bestellpositionen über das Lager gehen oder nicht. D.h. Handeingaben und Nicht Lager bewirtschaftete Artikel werden in die Auftragsnachkalkulation mit aufgenommen |

Die Gestehungspreise der Arbeitszeiten werden anhand der Definition der Personalkostenquelle errechnet. [Siehe dazu]( {{<relref "/docs/stammdaten/personal/#personalkosten-in-den-nachkalkulationen" >}} ) und [auch]( {{<relref "/docs/stammdaten/personal/#gehalts-lohnkosten-definition" >}} ).

Die Gestehungspreise der Maschinenzeiten werden anhand der definierten Maschinenkosten zum Zeitpunkt errechnet.

Die Gestehungspreise der Materialien werden zum jeweiligen Lagerentnahme-Zeitpunkt ermittelt.

Die angebundenen Losablieferungen werden anhand der tatsächlichen Lieferungen ermittelt. In den Loswerten sind nur die Mengen berücksichtigt, die auch tatsächlich geliefert wurden. Die angegebenen Zeiten werden anhand der verschiedenen Ablieferungen ermittelt. Kann eine Losablieferung nur teilweise eine Lieferung zugeordnet werden, so wird dies anteilig gerechnet.

**Wichtig:**

In den "Bewertung nach Gestehungspreis Material-Ist"-Positionen der Rechnungen und der Lieferscheine sind auch die Arbeitszeit und Materialwerte aus den Losablieferungen enthalten. Daher wird neben/unter den Material-Ist Werten der Rechnungen / Lieferscheine der Betrag "davon aus Losen" angezeigt. Dieser Betrag muss von den Gestehungskosten der Rechnungen / Lieferscheine abgezogen werden, da die Kosten sonst doppelt berücksichtigt wären. Einmal in Rechnung/Lieferschein und einmal in der Losablieferung.

Hintergrund der doppelten Darstellung:

In Rechnung/Lieferschein können auch andere Artikel als die sich aus der Losablieferung ergebenden enthalten sein. Diese sind in der Nachkalkulation des Auftrags ebenfalls zu berücksichtigen. Daher werden diese getrennt angeführt.

Die Darstellung ist in drei Gruppen gegliedert:

**a.) Bewertung nach Verkaufspreisen**

| Spalte | Bedeutung |
| --- |  --- |
| Soll-Arbeit | Summe der in der AB angeführten Arbeitszeitstunden zum Verkaufspreis unter Berücksichtigung der Rabatte |
| Soll-Material | Summe der in der AB angeführten sonstigen Positionen (Positionen die nicht Arbeitszeiten sind) |
| Ist-Arbeit | Summe der im LS / RE angeführten Arbeitszeitstunden zum Verkaufspreis unter Berücksichtigung der Rabatte des LS und der RE |
| Ist-Material | Summe der im LS / RE angeführten sonstigen Positionen zum Verkaufspreis unter Berücksichtigung der Rabatte des LS und der RE |

Hinweis: Wird neben einem LS ein R angezeigt, so bedeutet dies, dass der LS bereits verrechnet ist.
Wird daneben noch ein weiterer Haken angezeigt, so bedeutet dies, dass die Rechnung auch bereits bezahlt ist.
Demzufolge bedeutet ein Haken neben der RE, dass diese bereits bezahlt ist.

**b.) Bewertung nach Gestehungspreisen**

| Spalte | Bedeutung |
| --- |  --- |
| Soll-Arbeit | Geplanter Zeiteinsatz zur Basis Gestehungspreis zum Anlagezeitpunkt des Auftrags / des Loses anhand der Positionen des Auftrages oder der Soll-Positionen des Loses |
| Soll-Material | Geplanter Materialeinsatz zur Basis Gestehungspreis zum Anlagezeitpunkt des Loses |
| Ist-Arbeit | Tatsächlicher Zeiteinsatz zur Basis Gestehungspreis zum Buchungszeitpunkt des Zeiteintrages anhand der dem Auftrag / dem Los zugeordneten Zeiten |
| Ist-Material | Tatsächlicher Materialeinsatz zur Basis Gestehungspreis zum Buchungszeitpunkt des Materials auf LS, RE, Los |

**c.) Zeiten**

Arbeitszeit (Std)

| Spalte | Bedeutung |
| --- |  --- |
| Soll | Geplanter Zeiteinsatz des Auftrags / des Loses anhand der Positionen des Auftrages bzw. der Soll-Positionen des Loses |
| Ist | Tatsächlicher Zeiteinsatz des Auftrags / des Loses anhand der Zeitbuchungen der Mitarbeiter auf den Auftrag / das Los |

Maschinenzeit (Std)

| Spalte | Bedeutung |
| --- |  --- |
| Soll | Geplanter Maschinenzeiteinsatz des Loses anhand der Soll-Positionen des Loses |
| Ist | Tatsächlicher Maschinenzeiteinsatz des Loses anhand der Zeitbuchungen der Maschinen durch die Mitarbeiter auf das Los |

Wenn der Parameter ALLE_LOSE_BERUECKSICHTIGEN = 1 ist, dann werden die Gest-Werte Material und AZ IST der LA-Zeilen negiert
Es werden alle Lose in der Nachkalkulation angeführt (LO). Für jedes Los gibt es eventuell noch eine zusätzliche Zeile "LI" 

Es wird für jede Lossollmaterial-Position, welche eine Stückliste ist, das Ist-Material ermittelt. Wenn der Ursprung aus einer Losablieferung ist, welche dem selben Auftrag zugeordnet ist, dann wird der Ablieferpreis * IstMaterialMenge * -1 Summiert und in "Material Ist" angezeigt

Wenn dieser Wert 0 ist, dann wird die Zeile nicht angedruckt

**Unterscheidung Arbeitszeit, Nicht Arbeitszeit.**

Es wird dazu die Artikelart verwendet. D.h. nur Positionen die ein Artikel sind und deren Artikelart Arbeitszeit ist, sind Arbeitszeiten. Alles andere ist Material, also auch Handeingaben.

**Arbeitszeit / Maschinenzeit**

Die Unterscheidung zwischen Arbeitszeit und Maschinenzeit ist derzeit nur für die Istzeit-Buchungen vorgesehen.

Wichtig: Damit die in der Nachkalkulation angegebenen Zeiten die tatsächlich verbrauchten Zeiten beinhalten können, müssen die Zeiten der Mitarbeiter und der Maschinen vollständig verbucht werden. Siehe dazu [Produktivitätsstatistik der Mitarbeiter]( {{<relref "/fertigung/zeiterfassung/#produktivit%c3%a4tsstatistik" >}} ).

**Druck Arbeitszeiten detailliert**

Wenn beim Druck der  Nachkalkulation der Haken bei ![](Auftragszeiten_detailiert.JPG) Auftragszeiten detailliert gesetzt ist, so werden auch die Kosten der Tätigkeiten angeführt. Es werden die Gestehungskosten der einzelnen AZ-Artikel angezeigt und berechnet.

**2** **.) Auftragsnachkalkulation über alle Lose**

![](Nachkalkulation_AB_alle_Lose.JPG)

![](AB_Nachkalkulation_alleLose.JPG)

Ergänzend zu der [Auflistung oben](#Belege):

| Art | Beleg | Inhalt / Bedeutung |
| --- |  --- |  --- |
| LO  | Lose | Zeit und Materialbuchungen auf Losen |
| Li | Lieferschein | Losablieferungen die sich aus übergeordneten Losen ergeben |

#### Welche Lose werden in der Auftragsnachkalkulation berücksichtigt?
<a name="Welche Lose werden in der Nachkalkulation berücksichtigt"></a>
In der Auftragsnachkalkulation werden diejenige Bewegungsdaten berücksichtigt, die einen direkten Zusammenhang mit dem Auftrag haben. Das sind also z.B. die Lieferscheine, die mit Bezug auf den Auftrag erstellt wurden.

Bei den Losablieferungen ist der Zusammenhang oft nur indirekt gegeben. D.h.: es wird anhand der Warenbewegungen entschieden, welches Los einen tatsächlichen Bezug zum Auftrag hat. Das bedeutet also, dass Artikel die über die Auslieferung mittels Lieferschein einen Bezug (durch den Lieferschein) zum Auftrag haben, in die Nachkalkulation mit aufgenommen werden. Kommt die Warenzubuchung (der Auslieferung mittels Lieferschein) aus einem Fertigungsauftrag (also Losablieferung) so wird die zur Warenausgangsbuchung korrespondierende Warenzugangsbuchung verwendet. Daraus ergibt sich normalerweise, dass die Artikel die aufgrund der auftragsspezifischen Fertigungsaufträge produziert wurden ausgeliefert werden. Aufgrund des FiFo kann sich aber auch ergeben, dass andere (ältere) Artikelbewegungen verwendet werden.

Diese Logik setzt die Lagerbewirtschaftung voraus. Da diese nur für Artikel gegeben ist, werden die freien Materiallisten der Losverwaltung eigenständig behandelt. D.h. da diese je keine Lagerzubuchung bewirken, werden diese anhand der Auftragszuordnung des Loses zu den jeweiligen Aufträgen hinzugerechnet.

#### Es sind nicht alle Lose in der Auftragsnachkalkulation enthalten?
Wenn die Auftragsnachkalkulation ausgedruckt wird, so sind nur die Lose 972-001 bis 972-014 enthalten. Die (Unter-)Lose 972-999 und 972-998 sind in der Auftragsnachkalkulation nicht angeführt.
Wir hingegen die Losstatistik für den Auftrag 972 ausgedruckt, so werden diese auch angeführt. Was ist die Ursache dafür?
Siehe dazu bitte obige [Beschreibung](#Welche Lose werden in der Nachkalkulation berücksichtigt). Prüfen Sie gegebenenfalls die Warenbewegungen der Ablieferungen der Unterlose (972-999, 972-998). In der Regel werden diese Losnummer, bei Losnummern mit Auftragsbezug, nur dann erzeugt, wenn diese sich aufgrund von Unterstücklisten ergeben. Sie werden daher in der Auftragsnachkalkulation nicht angeführt.
Ein Sonderfall sind zusätzlich manuell angelegte Lose, welche z.B. für eine Nacharbeit verwendet werden. Sind diese Lose freie Materiallisten, dann werden diese bei der Auftragsnachkalkulation berücksichtigt, da es hier keine wertmäßige Lagerzubuchung durch die Losablieferung gibt.
Wichtig: Es werden, auch wenn die direkte Auftragszuordnung nicht aktiviert ist, zwar die indirekten Lose nicht in der Nachkalkulation angeführt. Die in den Unterlosen enthaltenen Arbeitszeiten werden trotzdem mit ausgewiesen.

<a name="ER_Zuordnung"></a>

#### Wie ordnet man Eingangsrechnungen einem Auftrag zu?
Grundsätzlich werden Kosten für einen Auftrag über die Materialbewegungen zugeordnet. Wenn Sie nun Zusatzkosten auf den Auftrag buchen wollen, so können Sie diese im Modul Eingangsrechnung über den Reiter ![](Auftragszuordnung.JPG) hinzufügen. Wählen Sie hier den gewünschten Auftrag und den Betrag.

Wenn Sie den Haken bei keine Auftragswertung ![](keine_Auftragswertung.JPG) setzen, so wird in der Auftragsnachkalkulation die Eingangsrechnung bei Mat.Ist nicht mitgerechnet.

**ACHTUNG:** Bitte unterscheiden Sie ganz klar zwischen Eingangsrechnungen für lagergeführte und NICHT lagergeführte Artikel.<br>
D.h. wird Ware über eine Bestellung auf Lager gebucht und dann mittels Lieferschein oder Rechnung in einen Auftragszusammenhang gestellt, so wird für die Auftragsnachkalkulation automatisch der Gestehungspreis der ausgelieferten Position verwendet und geht damit in die Deckungsbeitragsrechnung ein. Würden Sie nun die Eingangsrechnung der Bestellung auch dem Auftrag zuordnen, ordnen Sie die Kosten ein weiteres Mal dem Auftrag zu, belasten den Auftrag ein zweites Mal. Damit stimmt natürlich die Ist-Betrachtung der Deckungsbeitragsrechnung nicht mehr.

#### Gibt es eine Möglichkeit den laufenden Losfortschritt in der Auftragsnachkalkulation anzuführen?
<a name="Nachkalkulation_Fortschritt"></a>
Wenn Sie im Los die [Fortschrittsbewertung]( {{<relref "/fertigung/losverwaltung/#kann-der-fortschritt-eines-loses-angegeben-werden" >}} ) verwenden, so können die Informationen hieraus in der Auftragsnachkalkulation genutzt und angeführt werden.
Aus der Fortschrittsbewertung kann die theoretische Restdauer hochgerechnet werden (Istzeit / Fortschritt = theoretische Gesamtistzeit linear hochgerechnet).<br>
Daraus kann die Sollzeitüberschreitung in % angeben und die Gesamtsumme der zu erwartenden Zeit errechnet werden.<br>
Eine xx% Sollzeitüberschreitung wird in Rot angedruckt, nicht definierte Bewertungen in Lila.

#### Können alle Lose eines Auftrages neuberechnet werden?
Sie finden eine Checkbox im Report Auftrag/Info/Nachkalkulation ![](alle_betroffenen_Lose_nachkalkulieren.JPG) "alle betroffenen Lose nachkalkulieren".
Wenn Sie hier einen Haken setzen, werden alle betroffenen Lose anhand der Los-Gesamtkalkulation nachkalkuliert und die Preise aktualisiert.

#### Was bedeutet (Material Ist) ... signalisiert den Einkaufswert der Position?
In der Auftragsnachkalkulation werden bei den Verbrauchen (Lieferschein bzw. Rechnung) üblicherweise die Gestehungspreise der jeweiligen Materialien angedruckt. Diese sind in aller Regel die unternehmensweit gesehen richtigen Bewertungen jeder Position. In jenen Fällen, wo durch unstrukturierte Materialwert Buchungen extrem stark schwankende Einstandspreise gegeben sind und das Material nie vollständig verbraucht wird (siehe Gestehungspreisberechnung) kann es vorkommen, dass der Wunsch besteht, für die einzeln isoliert betrachteten Nachkalkulation der Einstandspreis richtiger ist. Um dies zu erkennen wird unter den Gestehungswerten der Einstandswert in Klammern mit angedruckt.

#### Die Auftragsnachkalkulation stimmt nicht, was tun?
Wie [oben](#ER_Zuordnung) beschrieben, ist bei einer manuellen Zuordnung der Eingangsrechnungen darauf zu achten, dass nur nicht lagergeführte Artikel / Eingangsrechnungen zugeordnet werden. Oder in einem Beispiel: Rechnungen für Taxi oder Flugkosten, sollten mit der Eingangsrechnungszuordnung genutzt werden. Alles andere sollte direkt oder indirekt über das Lager geführt werden und benötigt somit **keine** Zuordnung. Wenn Sie aus Gründen der Transparenz die Zuordnung trotzdem vornehmen möchten, nutzen Sie bitte den Haken bei keine Auftragswertung.

#### Können pauschale Werte mit Sollstunden verbunden werden?
In vielen Dienstleistungs-/ Service Unternehmen werden bestimmte Tätigkeiten pauschaliert an Kunden angeboten und verkauft. Natürlich liegen hinter diesen reinen verkäuferischen Werten auch immer wieder Soll-Stunden, die dem Kunden nicht dargelegt werden dürfen, aber in der internen Auftragsnachkalkulation verwendet werden.<br>
Dafür haben wir den [kalkulatorischen Artikel]( {{<relref "/docs/stammdaten/artikel/#was-ist-ein-kalkulatorischer-artikel-" >}} ) geschaffen. In Verbindung mit dem [dazugehörigen Artikel]( {{<relref "http://localhost:1313/docs/stammdaten/artikel/#wozu-dient-zugeh%c3%b6riger-artikel" >}} ) ergibt sich eine praktische Einstellung um Dienstleistungen wie Ware zu verkaufen, aber intern als echte Stunden / Arbeitsleistung nachkalkulieren zu können.<br>
Der Zusammenhang / die Abfolge ist wie folgt:
1. definieren Sie den Pauschalartikel der Verkauft werden sollte
2. definieren Sie beim Pauschalartikel jeweils im Reiter Sonstiges (des Artikels) dass dieser bei der Auftragsnachkalkulation als Arbeitszeitwert (der Sollzeit) betrachtet werden sollte ![](als_AZ_in_Nachkalk_betrachten.gif)
3. definieren Sie unter zugehöriger Artikel die Arbeitsleistung die Ihr Mitarbeiter erbringen sollte und
4. definieren Sie den Multiplikator um vom Pauschalartikel zum dazugehörigen Arbeitszeitartikel (den Sie vorher schon definiert haben und als kalkulatorischen Artikel angelegt haben) zum kommen.<br>
Damit ist die Erfassung der Pauschalen relativ einfach. Ein Beispiel:<br>
Sie verkaufen Ihrem Kunden Zutrittssysteme, deren Montage pro Zutrittsobjekt einen Pauschalbetrag von 35,- € kosten.<br>
Es ist dies so kalkuliert, dass Ihr Mitarbeiter in der Regel vier dieser Zutrittsmodule pro Stunde montieren kann.<br>
D.h. beim Artikel Zutrittsobjekt ist im Reiter Sonstiges unter dazugehört der Arbeitszeitartikel, beim Multiplikator 0,25 und bei Arbeitszeit in AB-Nachkalkulation ein Haken eingetragen.<br>
Der Arbeitszeitartikel ist als kalkulatorischer Artikel (Reiter Detail) definiert.<br>
Damit wird beim Schreiben des Angebotes bzw. des Auftrags bei Auswahl des Pauschalartikels automatisch der kalkulatorische Artikel als zusätzliche Position vorgeschlagen, welche Sie übernehmen. In der Menge sind die geplanten Stunden bereits vorbesetzt. Somit haben Sie sofort eine entsprechende Nachkalkulation über Ihren Kundenauftrag und in weiterer Folge natürlich auch in der Auftragsstatistik.

#### Kann ein kalkulatorischer Artikel auch mit einem Verkaufssetartikel verbunden werden?
Ja. Wichtig ist, dass der kalkulatorische Artikel des Setartikels nicht über das dazugehört (der Artikel) sondern als eigene Position im Setartikel (= Stücklistenmodul) eingebunden wird.
Wichtig ist weiters, dass der kalkulatorische Artikel keinen Verkaufspreis hat und dass er idealerweise an der letzten Stelle, aber in keinem Falle an erster Stelle des Verkaufssetartikels liegt.

#### Wo finde ich eine komplette Auswertung / Nachkalkulation meiner Aufträge?
Im Auftragsmodul finden Sie unter Journal, Statistik eine Auswertung aller Aufträge.
Abhängig von der gewünschten Auswertung hat die Bedeutung des Datumsbereiches immer wieder Erklärungsbedarf.<br>
Hier stehen folgende Datumseinschränkungen zur Verfügung:
| Datum | Bedeutung |
| --- |  --- |
| Belegdatum | Es werden die Aufträge mit Auftragsdatum Von - Bis in die Betrachtung mit aufgenommen |
| Erledigungsdatum | Es werden diejenigen Aufträge ausgewertet, welche im Zeitraum Von - Bis erledigt wurden |
| Liefertermin bis zum | Hier werden diejenigen Aufträge berücksichtigt, welche noch offen sind und bis zum angegebenen Termin geliefert werden sollten.Hier wird nur der Liefertermin aus den Kopfdaten des Auftrages herangezogen.Es dient dies vor allem auch eine Statusübersicht über die offenen Aufträge |
| Stichtag | Es werden alle Aufträge aufgelistet, die noch nicht erledigt sind oder deren Erledigungsdatum nach oder gleich dem Stichtag ist und deren Anlagedatum vor bzw. zum Stichtag ist. Für diese Betrachtung wird dann innerhalb des Auftrages jede Buchung auf den Stichtag hin ausgewertet. |

##### Warum kann kein Liefertermin von / bis angegeben werden?
Hintergrund ist, dass dies die umfassende Nachkalkulation der Aufträge ist. Eine Auftragsnachkalkulation macht aber nur dann Sinn, wenn der Auftrag abgeschlossen, also erledigt ist. Um nun z.B. den Erfolg / Deckungsbeitrag eines Kunden zu errechnen, müssen Aufträge abgeschlossen sein, da ansonsten weitere Kosten auf Aufträge hinzukommen können und somit sich die Darstellung des Deckungsbeitrages vermischt.

#### Wir wird der Erfolg von wiederholenden Aufträgen betrachtet?
Oft werden die wiederholenden Aufträge für laufende Betreuungsleistungen an den Kunden verwendet. D.h. einerseits bezahlt der Kunde regelmäßig den gleichen Betrag und andererseits kümmern Sie / Ihre Team sich um die Pflege der Kundeninstallation wie z.B. Webseiten, Gerätewartungen, Softwarepflege usw..
D.h. einerseits werden die laufenden Rechnungen mit Bezug auf den Wiederholenden Auftrag erstellt und andererseits buchen Sie den gesamten Aufwand, egal ob Material oder Zeiten wie oben beschrieben auf den Auftrag. Damit ergibt sich, dass die Erlöse (die gelegten Rechnungen) den Aufwänden (meist Arbeitszeiteinsatz) gegenübergestellt wird. Wenn nun für die Berechnung der Kosten der Stunden der Mitarbeiter die richtigen Lohnmittelstunden hinterlegt sind, was für eine ordentliche Unternehmensführung sowieso Grundvoraussetzung ist, so sehen Sie auch den tatsächlichen Erfolg dieses wiederholenden Auftrags. Eine reine Hochrechnung der im Auftrag als Soll erfassten Stunden über das Wiederholungsintervall und den Auswertezeitpunkt ist derzeit nicht umgesetzt.

#### Wirkung des Sammelieferscheines in der Nachkalkulation
ACHTUNG: Ist die Zusatzfunktion Sammellieferschein abgeschaltet, werden Lieferscheine und Rechnungen anhand der Zuordnung aus den Kopfdaten auf den jeweiligen Auftrag vorgenommen.
Ist die Funktion Sammellieferscheine aktiviert, so wird davon ausgegangen, dass jede Lieferscheinposition auf den entsprechenden Auftrag zugeordnet ist.
D.h. wurde eine Lieferscheinposition OHNE Auftragsbezug hinzugefügt, scheint diese auch nicht in den Aufträgen auf. Um hier einen Überblick zu
bekommen, siehe Spezialauswertung für den einzelnen Lieferschein, welche unter Menüpunkt Lieferschein, Adress-Etikette, Gestehungspreis hinterlegt ist


<a name="Erfolgsstatus"></a>

## Auftrags Erfolgsstatus
Mit dem Erfolgsstatus steht eine weitere Form der Betrachtung der Auftragsentwicklung zur Verfügung.
Der Erfolgsstatus wurde insbesondere in Verbindung mit der erweiterten Projektsteuerung im Auftrag erstellt, welche wiederum davon ausgeht, dass eine sehr sehr Enge Beziehung zwischen Auftrag und Losen gegeben ist.
Der Erfolgsstatus hat gegenüber der Nachkalkulation (und somit auch der Auftragsstatistik) sehr unterschiedliche Betrachtungsweisen.
Im Erfolgsstatus etwa wird z.B. der Material-, Maschinen-, Arbeitszeiteinsatz der auftragsbezogenen Lose laut Los-Gesamtkalkulation herangezogen. In der Nachkalkulation werden nur die Losablieferungen betrachtet, die sich aus den auftragsbezogenen Lieferscheinen ergeben.
So ergibt sich z.B. eine Differenz im Materialeinsatz dadurch, dass in der Auftragsnachkalkulation nur das anteilige Material (und die Arbeitszeiten) berücksichtigt wird, im Erfolgsstatus aber der gesamte Materialeinsatz herangezogen wird.
Gegebenenfalls kann durch aktivieren des Mandantenparameters ALLE_LOSE_BERUECKSICHTIGEN, dieser steht üblicherweise auf 0, also wenn Sie diesen auf 1 stellen, so werden alle Lose des jeweiligen Auftrags in Ihrer Gesamtheit in der Nachkalkulation berücksichtigt. Somit sind Sie auch in der Nachkalkulation näher am Erfolgsstatus, was jedoch aus Deckungsbeitragssicht nicht unbedingt der Wahrheit entsprechen muss.
