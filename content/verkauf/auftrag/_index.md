---
title: "Auftrag"
linkTitle: "Auftrag"
categories: ["Auftrag"]
tags: ["Auftrag"]
weight: 40
description: >
  Auftragsverwaltung inkl. Rahmen und Abrufaufträgen, sowie wiederholenden Aufträgen als Vorlage einer Verrechnung
---
Auftrag
=======

Statusübergang
![](statusuebergang.png)<br>
[Step by Step]( {{<relref "/verkauf/auftrag/auftrag_step_by_step">}} ) <br>

Anlegen eines Auftrags

Um ein neuen Auftrag anzulegen gehen Sie bitte wie folgt vor:
Klicken Sie auf das Modul Auftrag ![](Auftrag.JPG) in der nun offenen Auswahl klicken Sie auf ![](angebot_neu.PNG) um Daten aus einem bestehenden Angebot übernehmen.

Wenn Sie eine Auftragsbestätigung ohne Angebotsbezug erstellen wollen, so klicken Sie auf Neu ![](neu_button.gif) .

Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den Kunden, Projekt, Bestellnummer, Termine - je nach Bedarf.

Mit einem Klick auf Speichern ![](speichern.JPG) bestätigen Sie die Eingaben.

Wechseln Sie in den Reiter Positionen und geben hier die Positionen des Auftrags ein. 
Um eine neue Position hinzuzufügen klicken Sie auf Neu ![](neu_button.gif) wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#positionsarten">}} )), geben die Informationen (u.a. Menge) ein und klicken auf Speichern ![](speichern.JPG). Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.

Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Konditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für diesen einen Auftrags ab. 

Mit Klick auf ![](drucken.JPG) Drucken (oder oberes Menü Auftrag - Drucken...)erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG), dem Versenden per E-Mail ![](e-mail.jpg) oder speichern ![](speichern.JPG), aktivieren Sie den Auftrag, der Status (siehe oben) ändert sich von angelegt in offen.

#### Reparaturabwicklung
[Siehe]( {{<relref "/verkauf/auftrag/reperaturabwicklung">}} ).

#### Erweiterte Projektsteuerung mit Meilensteinen, Kosten, Liquiditätsplan usw.
[Siehe]( {{<relref "/verkauf/auftrag/erweiterte_projektsteuerung">}} ).

#### Wieso kann ich den Auftrag nicht in den Lieferschein übernehmen?
Siehe dazu [Wieso kann ich bei der Erstellung eines Auftragsbezogenen Lieferscheines den soeben angelegten Auftrag nicht auswählen]( {{<relref "/verkauf/lieferschein/#wieso-kann-ich-bei-der-erstellung-eines-auftragsbezogenen-lieferscheines-den-soeben-angelegten-auftrag-nicht-ausw%c3%a4hlen" >}} ) unter Lieferschein

#### Wie kann ich einen Auftrag direkt in die Rechnung übernehmen?
Siehe dazu [Rechnung direkt aus Auftrag erstellen]( {{<relref "/verkauf/rechnung/#rechnung-direkt-aus-auftrag-erstellen" >}} ).

#### Was bedeute Auftragseingang / offen in der Übersicht
Auftragseingang ist die Summe der Aufträge die von Kunden im Zeitraum (Monat) erteilt wurden. Die daraus noch nicht ausgelieferten Positionen finden sich in der Spalte offene wieder.

#### Wie bzw. wo kann ich den Status eines Auftrages ändern (z.B. von erledigt auf offen) 
Auftrag Kopfdaten, Icon "ändern", System fragt ob der Auftragsstatus auf "Teillieferung" geändert werden soll. Wurde die Änderung durchgeführt so kann der Auftrag manuell wieder auf geliefert bzw. erledigt gesetzt werden, indem der gespeicherte Auftrag wieder über das "Ändern-Icon" bearbeitet wird.

#### Auftragsnachkalkulation
[Siehe]( {{<relref "/verkauf/auftrag/nachkalkulation">}} ).

## Rahmenauftrag
<a name="Rahmenauftrag"></a>
#### Was ist der Unterschied zwischen freie Aufträge, Abrufaufträge und Rahmenaufträge?
Ein Rahmenauftrag ist eine Vereinbarung zwischen Anwender und seinem Kunden innerhalb eines meist längeren Zeitraumes, üblicherweise 12 Monate, Ware an den Kunden zu liefern, wobei die genauen Liefertermine erst später mit den Abrufen (= Abrufauftrag) bekannt gegeben werden.
Der Rahmenauftrag dient dem Anwender zur besseren Planung (z.B. Fertigung größerer Stückzahlen) und zum besseren Einkauf (durch Einkauf größerer Mengen erzielt man besserer Preise) und ermöglicht dadurch dem Kunden einen besseren Preis und oder eine kürzere Lieferzeit / höhere Verfügbarkeit. Der Rahmenauftrag ist also für beide Beteiligen ein wesentlicher Vorteil.
Der Rahmenauftrag selbst wird durch den/die Abruf(aufträge) ausgeliefert. Damit erhält man jederzeit eine Übersicht über den Rahmen und damit über die Vereinbarung.
Freie Aufträge sind Aufträge die weder Rahmenauftrag noch Abrufauftrag sind.
Im wesentlichen verhält sich ein Abrufauftrag wie ein freier Auftrag. Wobei jedoch der Abruf den offenen Rahmen reduziert.
Über die Laufzeit des Rahmenauftrages ist es durchaus üblich, dass sich Änderungen der zu liefernden Artikel ergibt. Denken Sie hier z.B. an die Versionsänderung einer elektronischen Baugruppe, welche sich im Laufe des Jahres von der Version 1.0 zur Version 3.5 hin entwickelt. Im wesentlichen wird die Vereinbarung erfüllt. Da aber einige Artikel zusätzlich in der neuen Version verwendet werden müssen ändert sich auch der Preis usw.

#### Wo finde ich eine Übersicht über die Abrufsituation eines Rahmenauftrags?
Wählen Sie in der Auswahlliste der Aufträge den gewünschten Rahmenauftrag aus und klicken Sie im Menü auf Info, Rahmenübersicht. Darin ist die Aufstellung alle Rahmenpositionen, die Liste der Abrufe, detailliert mit Abruf- und Liefer-Mengen und eine Zusammenfassung mit einer Gesamtübersicht über den Rahmenstatus aller Positionen enthalten.
![](Rahmenuebersicht.jpg)
Damit gewinnen Sie rasch einen entsprechenden Überblick über den Rahmen.

#### Es sollten zu einer Rahmenauftragsposition Abrufpositionen mit unterschiedlichen Lieferterminen angelegt werden
Manche Endkunden bestellen in jeweils einer Abrufbestellung aus **einer** Rahmenposition mehrere Abrufe mit unterschiedlichen Terminen und Mengen und bestehen darauf, dass diese auch in der Abrufauftragsbestätigung entsprechend dargestellt sind.<br>
Um dem gerecht zu werden verwenden Sie bitte die Funktion ![](Rahmenposition.gif) zusätzliche Rahmen(abruf)position überleiten. Damit bleibt die Verkettung erhalten und trotzdem können verschiedene Abruftermine und Mengen einer Rahmenposition in einem Abruf dargestellt werden.
Bitte beachten Sie, dass eventuell erforderliche Änderungen nur mehr in der Sicht Positionen durchgeführt werden können.<br>
**Hinweis:** Da in diesen Anwendungsfällen auch gefordert wird, dass die Positionen in den Abrufen nicht nach Rahmen sondern nach deren Abrufbestellung sortiert sein müssen, muss dafür der Parameter ABRUFPOSITIONSREIHENFOLGE_WIE_ERFASST auf 1 gestellt werden.

#### Nach Auftragserteilung wollen wir Handeingaben in Artikel umwandeln, wie geht das?
Manchen Anwender von **Kieselstein ERP** erfassen in Aufträgen für neue Artikel / Prototypen etc. nur Handeingaben. Bei Auftragserteilung soll diese Handeingabe zu einem Artikel werden.
Dazu markieren Sie die Belegposition der Auftragsbestätigung und klicken auf Bearbeiten - Handartikel in Artikel umwandeln ![](handartikel_in_artikel_umwandeln.png).
Nun geben Sie die neue Artikelnummer ein bzw. generieren diese.<br>
 ![](gewuenschte_artikelnummer.JPG)<br>
Danach wird ein Artikel mit den Eingaben erstellt und eine Reservierung angelegt. **Info für CleverCure Anwender:** Beim automatischen Einspielen der CleverCure Daten werden nicht zuordenbare Artikel als Handeingabe mit entsprechenden erklärenden Texten eingespielt. Hinter diesen Positionen stecken, so wie in allen anderen CleverCure Positionen auch die IDs welche Teil der Abstimmungsdaten der beiden Partner sind. Bei der Umwandelung der Auftragsposition von Handeingabe in einen Artikel bleibt diese Information erhalten. Wird hingegen ein neuer Artikel angelegt und dieser mittels Position Neu bzw. Einfügen in den Auftrag übernommen und danach die Handeingabe gelöscht, so ist die Verbindung nicht mehr gegeben.

Da es auch immer wieder vorkommt, dass Artikel mit unvollständigen Definitionen, von Seite Ihres Kunden oder auch von Seite des **Kieselstein ERP** Anwenders, dazu führen, dass im Import der Artikel nicht als solcher erkannt wird und daher eine Handeingabe im Auftrag angelegt wird, steht auch die Funktion Handartikel durch Artikel ersetzen zur Verfügung.<br>
Beim Ersetzen wird auch die Übereinstimmung der Verkaufspreise geprüft und gegebenenfalls ein entsprechender Hinweis ausgegeben.<br>
![](Handartikel_in_Artikel_Preispruefung.jpg)<br>
Bitte handeln Sie entsprechend.

#### Können in den CleverCure Aufträgen die Positionen als bearbeitet markiert werden?
Auftragsbestätigungen die automatisch über die CleverCure Schnittstelle eingespielt wurden, sind meistens einige 100 Positionen lang. Da, trotz aller Automatissmen, vor der verbindlichen Bestätigung des jeweiligen Auftrages an Ihren Kunden, diese durchgesehen werden müssen, haben wir die Möglichkeit zum Markieren der geprüften Positionen geschaffen.
Daher finden Sie, bei CleverCure Aufträgen, die Möglichkeit eine Position als gesehen markieren zu können. Durch Klick auf ![](Position_markieren.gif) wird die Zeile, werden die Zeilen, entsprechend grün markiert. Hier können auch mehrere Zeilen markiert werden. Ein erneuter Klick dreht den Status in sein Gegenteil um.
Das bedeutet auch, dass ein gültig bearbeiteter CleverCure Auftrag NUR grüne Positionszeilen haben sollte.

#### Mit der offline HVMA sollten nur ausgewählte Positionen an die HVMA übertragen werden
Wenn Sie auch die Zusatzfunktion Offline HVMA im Einsatz haben, kann definiert werden, dass nur bestimmte Auftragspositionen, der Übersichtlichkeit halber, an die HVMA übertragen werden. Um dies zu signalisieren werden diejenigen Positionen in grün dargestellt, welche auch an die HVMA übertragen werden. Durch Klick auf ![](HVMA_Auftragspositionen_uebertragen.gif) kann dies entsprechend aktiviert / deaktiviert werden. Für die Umschaltung wird auch der sogenannte Multiselekt unterstützt.

#### Nach der Meldung "kein Hauptlager des Mandanten erfasst" sind alle Knöpfe grau. Was tun?
Die Ursache dieser Meldung ist eben, dass im Mandanten kein Hauptlager definiert ist, oder dieses nachträglich entfernt wurde. Da die Deckungsbeitragsberechnungen usw. auf dem Hauptlager aufbauen muss dieses definiert sein. Schließen Sie das Auftragsmodul, wechseln Sie in das Artikelmodul, Untere Modulreiter Grunddaten, Obere Modulreiter Lager und definieren Sie ein Hauptlager.

Nun kann das Auftragsmodul wieder gestartet werden.

#### Packliste was ist das, wozu wird diese verwendet und was ist das im Rahmenauftrag?
Die Packliste dient als Unterstützung für die Komissionierung, also für das Bereitstellen der für einen Auftrag auszuliefernden Waren. 

Sie wird oft auch für einen schnellen Überblick über einen Auftrag verwendet, ob dieser Auftrag, die offenen Positionen des Auftrags schon geliefert werden können. Aus diesem Grunde ist die Packliste üblicherweise nach Lagerplätzen sortiert. Die auf der Packliste angeführten Mengen sind die offenen (noch zu liefernden) Auftragsmengen.

Packliste bei Rahmenaufträgen: Auch bei Rahmenaufträgen steht dieser Ausdruck zur Verfügung. Die offenen Mengen sind jedoch die offenen Rahmenmengen dieses Rahmenauftrages. Zusätzlich werden hier jedoch auch die insgesamt offenen Mengen angedruckt. Also die Summe der noch nicht abgerufenen Menge (offene Rahmenmenge) und die Summe der in den Abrufen noch nicht ausgelieferten Menge, damit Sie rasch einen Überblick über die Verfügbarkeiten des Gesamtauftrages bekommen.

Siehe dazu auch Ausliefervorschlag.

#### Können Artikelkommentare auch in der Packliste angedruckt werden ?
Ja. Es muss dafür bei den Kommentaren des Artikels unter Mitzudrucken bei auch die Fertigung angehakt sein.

#### Wie kann der Lieferantenpreis, der Einkaufspreis für einen Artikel im Auftrag dokumentiert werden?
Hier ist die Umsetzung ident zu dem Angebot - [siehe bitte hier]( {{<relref "/verkauf/angebot/#wie-kann-der-lieferantenpreis-der-einkaufspreis-f%c3%bcr-einen-artikel-schon-im-angebot-dokumentiert-werden" >}} ).

#### Rahmendetailbedarf
<a name="Rahmendetailbedarf"></a>
Der Rahmendetailbedarf dient der Darstellung der für die Erfüllung der offenen Rahmenaufträge noch benötigten Materialien / Einkaufsartikel. Er ist daher für den Einkauf eine enorm wichtige Information, insbesondere um festzustellen ob für einen Artikel Rahmenbestellungen getätigt werden sollen bzw. ob die offene Rahmenbestellungsmenge mit den für Kundenaufträge benötigten Materialien übereinstimmt.

Der wesentlichste Punkt ist, dass für jede noch nicht eingeplante Baugruppe die Stückliste über die Hierarchie vollständig aufgelöst wird.

**Wie wird der Rahmendetailbedarf errechnet?**

Ziel ist, die zusätzlich zu den normalen Reservierungen noch erforderlichen Bedarfe der Baugruppen (Stücklisten) zu erkennen, um dem Einkauf die Verwaltung der Rahmenbestellungen zu erleichtern. Aus dem bisherigen Einsatz der Rahmendetails musste erkannt werden, dass nur das gesamte Unternehmen als ein Lager betrachtet werden darf. Die Verknüpfung über Aufträge usw. ist nicht zielführend.
Voraussetzung: Während der Berechnung dürfen keine Losausgaben, Lieferscheine usw. durchgeführt / erstellt werden. Die Berechnung wirkt nur für Artikel. Sonst darf es keine weiteren Voraussetzungen geben.

Die Logik dahinter ist nun wie folgt:
| Bedarfsauslöser | Was ist noch zu liefern |
| --- | --- |
| Rahmenauftrag | offene Rahmenmenge |
| Abruf | offene Auftragsmenge |
| Freier Auftrag | offene Auftragsmenge |

Was kann geliefert werden?
| Art | Menge | Bemerkung |
| --- | --- | --- |
| Lagerstand | Des Hauptlagers |
| Fertigung |  Was wird geliefert werden | Als zu beschaffen bereits mitgeteilt und daraus im Lager bereits berücksichtigt |
| Angelegtes Los | die Losgröße | die Reservierungen |
| Ausgegebenes Los | die Losgröße | die Fehlmengen |
| Teilerledigtes Los | die offene Losgröße | die Fehlmengen |
| Erledigtes Los |  nichts<br>(es wurde schon alles auf das Lager gebucht) | nichts mehr |

**Anmerkung:** Die Ware welche bereits für die Fertigung eingeplant ist, ist wie Lager zu betrachten, also gehören Reservierungen und Fehlmengen direkt zum Lagerstand.

Die Berechnung wird nun zweigeteilt:<br>
I.)<br>
a.) ... Feststellung der noch zu liefernden Baugruppen <br>
         offene Rahmenmengen + offene Auftragsmengen ergibt<br> 
         noch aus den Aufträgen zu liefernde Mengen (je Baugruppe) <br>
b.) ... Abzüglich Lagerstand          (dies ist die Menge die sofort geliefert werden könnte) <br>
c.) ... Abzüglich offene Losmengen (Losgröße - bereits rückgemeldet, <br>
                                                da die rückgemeldeten ja schon wieder auf Lager liegen)<br>
d.) ... Ergibt noch nicht eingeplante Baugruppen<br>

II.)<br>
a.) ... Ermittlung der Einzelbedarfe anhand der Stücklistenauflösung<br>
**Zusätzlich:** Die noch zu liefernden Baugruppen werden nach Termin gereiht, damit die Erfüllung ebenfalls nach Terminen gereiht erfolgen kann, um so auch eine Näherung der Terminsituation zu erreichen. **Hinweis:** Die Verknüpfung bei Rahmen als auch bei offenen Lieferungen erfolgt rein über die Artikelnummer.<br>

Eine etwas vereinfachte Darstellung für die Ermittlung der Rahmenmenge:<br>
+  Rahmenmenge (die Menge der Position aus dem Rahmenauftrag)<br>
-  Menge auf Lager<br>
-  Menge in Produktion<br>
-  Menge bereits geliefert<br>
------------------------<br>
Ergibt offenen Rahmendetailbedarf<br>

**Wieso ist dafür ein Nachtjob erforderlich?**<br>
Bei einer entsprechenden Menge von Rahmenaufträgen und Positionen in den Rahmenaufträgen dauert die Berechnung deutlich länger als 1-2Sekunden.<br>
Da sich die Lagerstände, aber auch Stücklistendefinitionen ständig verändern können, ist es dem Anwender nicht zumutbar, bei jeder Änderung entsprechend lange (in der Praxis einige Minuten) auf die Verbuchung der sofortigen Aufrollung der Rahmendetailbedarfe zu warten.<br>
Da der übliche Kreislauf in Unternehmen ist, dass während des (Arbeits-) Tages die Aufträge erfasst und gebucht werden und am nächsten Tag der Einkauf für die Bestellungen die entsprechenden Informationen benötigt, reicht es, wenn täglich in der Nacht die Rahmendetailbedarfe für das gesamte Unternehmen aufgerollt werden. Der Zeitpunkt dieser Aufrollung kann in den Nachtjobs (System, Automatik) frei eingestellt werden.

#### Ich habe Zeiten auf einen Auftrag erfasst und sehe diese nicht?
Für die Zeitauswertung ist grundsätzlich eine richtige Zeitbuchung erforderlich. Siehe dazu bitte [Zeiterfassung]( {{<relref "/fertigung/zeiterfassung/#ich-habe-zeiten-auf-einen-auftrag--ein-los-erfasst-und-sehe-diese-nicht" >}} ).

#### Wie funktioniert die Vorbelegung des Mwstsatzes in den Auftragpositionen?
Ausschlaggebend ist der Parameter "PARAMETER_KUNDEN_POSITIONSKONTIERUNG" aus den Mandantenparametern im System.<br>
Bedeutung des Parameters:
- 1 ... (ja) Artikel bestimmt MWST
- 0 ... (nein) es gilt MWST des Kunden

#### Offene Aufträge zum Stichtag?
Das Journal der offenen Aufträge beinhalten alle Aufträge, welche am Stichtag oder danach angelegt wurden und vor oder zum Stichtag erledigt wurden. Hier können Sie die Aufträge filtern nach Alle, nur Bestätigte oder nur Unverbindliche (also Liefertermin noch nicht bestätigt). Wenn Sie den Haken bei ![](mit_angelegten_ABs.JPG) mit angelegten Aufträgen setzen, so werden auch Aufträge im Status angelegt sind, angeführt. Angelegte Aufträge sind jedoch noch nicht aktiviert, somit werden für diese Aufträge keine Preise angedruckt.

<a name="Offene Auftragsdetails"></a>

#### Offene Auftragsdetails?
Unter, Journal offene Details können, neben weiteren anderen Auswertungen, die offenen Positionen der Aufträge detailliert ausgewertet werden. Das hier vorgeschlagene Stichtagsdatum (Liefertermin bis) wird anhand des Parameters AUFTRAG_JOURNAL_OFFENE_POSITIONEN_STICHTAG errechnet.
Hier stehen neben den Sortierungen nach Belegnummer, Kunde, Artikel, Projekt die Untersortierung nach Liefertermin zur Verfügung, sowie verschiedene Filter.
Als Besonderheit ist hier die Lagerstandsdetailauswertung mit enthalten.
![](offene_Detail_auswertung.gif)
Diese Auswertung bewirkt zwei zusätzliche Betrachtungen.
- a.) es werden die Lagerstände nach Lagerarten aufgeführt und in folgender Reihenfolge fiktiv verbraucht.
- a1.) Hauptlager und gleichberechtigt die Normallager
- a2.) Lieferantenlager
- a3.) Halbfertiglager

Ergeben sich hier noch immer Fehlmengen, so werden diese in die eventuell nachfolgende Lagerstandsdetailauswertung der Stückliste als offene Menge (= Bedarf) mit übergeben.
Der fiktiver Verbrauch bedeutet, dass die (Stücklistenkopf-)Artikel nach Termin sortiert betrachtet werden und die vorhandenen Lagermengen zuerst vom ersten zu liefernden Artikel und dann vom nächsten usw. verbraucht werden. Somit erhalten Sie auch eine Sicht, wie weit ihr aktueller Lagerstand, ev. mit den Halbfertigkomponenten noch reicht.
- b.) für die Stücklistenartikel bei denen in Lagerstandsdetailauswertung berücksichtigen in den Stücklistenkopfdaten angehakt ist, werden, nur für eine Unterebene, die Stücklistenpositionen mit den Detaillagermengen errechnet und die Lagerstände ebenfalls nach Lagerarten angeführt und in obiger Reihenfolge fiktiv verbraucht.
![](in_Detailauswertung_beruecksichtigen.gif)<br>
Das bedeutet, dass Sie nicht nur die Lagersituation der fertig produzierten Stücklisten sehen, sondern für ausgewählte Stücklisten auch die Situation der Unterkomponenten. Damit kann sehr rasch entschieden werden, ob z.B. der Zusammenbau, das Assembling einer Baugruppe vorgenommen werden kann um ein Fertigprodukt zu liefern.

#### Offene Auftragspositionen?
Unter, Journal offene Positionen können, neben weiteren anderen Auswertungen, die offenen Positionen der Aufträge ausgewertet werden. Das hier vorgeschlagene Stichtagsdatum (Liefertermin bis) wird anhand des Parameters AUFTRAG_JOURNAL_OFFENE_POSITIONEN_STICHTAG errechnet.

#### Sortierung nach Lieferadresse?
In den Auftragsjournalen (Alle, offene, offene Details, offene Positionen) kann bei der Sortierung nach Kunden neben der Auftragsadresse auch nach Lieferadresse sortiert werden. Beachten Sie bitte, dass diese Sortierung nach den Kundennamen erfolgt. Für eine Tourenplanung sollten die Touren bei den Kunden hinterlegt sein um dann dafür eine entsprechende Auswertung machen zu können. Wenn Sie Bedarf in diese Richtung haben, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

#### Wird ein geänderter Verkaufspreis in den Artikelstamm zurückgepflegt?
Da im Verkauf oft besondere Preisvereinbarungen für nur ein Projekt, einen Vorgang getroffen werden müssen, diese aber nicht in die Verkaufspreisvorgabe zurückwirken dürfen, werden die Verkaufspreise im Gegensatz zu den Einkaufspreise nicht in den Artikelstamm zurückgepflegt. Zusätzlich ist es in vielen Anwendungen üblich, dass die Vorgaben der Verkaufspreise nur von bestimmten Personen verändert werden dürfen.

#### Liefertermin, Positionstermin, Finaltermin, Rahmentermin. Was ist was?
-   Liefertermin ist der Liefertermin für alle Positionen des Auftrages, welche keine besonderen Positions(Liefer-)termine haben.
Der Liefertermin der Auftragskopfdaten ist immer der späteste / letzte Liefertermin dieses Projektes
-   Positionstermin ist der Liefertermin einer einzelnen Auftragsposition. Dieser kann nur zum Finaltermin oder davor, bis heute, sein
-   Finaltermin ist eine zusätzliche Information, wann dieser Auftrag / dieses Projekt abgerechnet werden kann. Denken Sie an die Lieferung einer elektrischen Anlage, diese muss nach der Lieferung noch in Betrieb genommen werden. D.h. die Komponenten müssen für die Montage auf der Baustelle sein. Erst nach erfolgter Inbetriebnahme kann das Projekt abgerechnet werden. Dieser geplante Abschlusstermin ist der Finaltermin.<br>
**Hinweis:** Wird Ware im Auftrag zurückgenommen, also mit negativer Menge eingetragen, so ist der Termin der Rücknahme automatisch der Finaltermin. Diese Rücknahme wird als negative Reservierung mit dem Finaltermin in die Reservierungsliste übernommen und bewirkt somit eine Lagerstandserhöhung. Siehe dazu auch unten, Retourenabwicklung.
-   Rahmentermin ist bei einem Rahmenauftrag der Termin bis zu dem der vollständige Rahmen von Ihrem Kunden abgerufen sein sollte.

<a name="Berechnung des Liefertermines"></a>

#### Wie wird der Liefertermin des Auftrages errechnet?
Grundsätzlich wird der Liefertermin von Ihrem Kunden bestimmt. Selbstverständlich liegt es in Ihrer Verantwortung die Realisierbarkeit dieses Termins mit den verschiedensten Werkzeugen zu prüfen und hier gegebenfalls auch in den Auftragskopfdaten das unverbindlich zu setzen.
Um den Liefertermin vorzubesetzen stehen zwei Parameter zur Verfügung:
- DEFAULT_LIEFERZEIT_AUFTRAG, damit wird vom Anlagedatum ausgehend die Anzahl der im Parameter angegebenen Kalendertage hinzugerechnet
- LIEFERZEIT_AUS_ANGEBOT, wenn dieser Parameter aktiviert ist, wird anhand der Arbeitstage des Angebotes ab dem Anlagedatum (also heute) des Auftrags die Anzahl der Arbeitstage hinzugerechnet. Arbeitstage sind die Wochentage Montag bis Freitag, sofern sie kein Feiertag und kein Betriebsurlaub sind. Für diese Berechnung muss ein Firmenzeitmodell definiert sein.
Wenn in Ihrem Angebot Wochen als Lieferzeit definiert sind, so wird diese Anzahl der Wochen zum Anlagedatum hinzugerechnet und der darauffolgende nächste Arbeitstag als Liefertermin angegeben.

#### Termin nur in Tagen oder genauer mit Stunden und Minuten ?
Siehe dazu [Auftragstermin mit Uhrzeit]( {{<relref "/verkauf/auftrag/termin_in_stunden_minuten">}} ).

#### Was bedeuten die verschiedensten Datum, Stichtage, Termine in den unterschiedlichen Auftragsjournalen?
Anbei eine Aufstellung welche Bedeutung die unterschiedlichen Datum / Termine in den unterschiedlichen Auftragsjournalen haben:

-   Erledigte Aufträge
    Das von / bis Datum bezieht sich auf das Erledigungsdatum des jeweiligen Auftrags, egal ob automatisch oder manuell erledigt.
    Der Auftrag muss im Status vollständig erledigt sein und das Erledigungsdatum muss >= von und <= bis sein.

-   Alle Aufträge
    Das von / bis Datum bezieht sich auf das (Beleg-) Datum des jeweiligen Auftrags. Der Status des Auftrags ist egal. Das Beleg-Datum muss >= von und <= bis sein.

-   offene Aufträge
    Hier wird zwischen ohne Details und Mit Details unterschieden.

    -   Ohne Details, d.h. der Haken bei Mit Details ist nicht gesetzt
        a.) Das Belegdatum muss vor dem Stichtag sein
        b.) Liefertermin aus den Kopfdaten muss vor dem Stichtag (<=) sein. Es hätte bis zum Stichtag der gesamte Auftrag geliefert werden sollen
        c.) Die AB darf zum Stichtag noch nicht erledigt sein. D.h. der Stichtag ist vor dem Erledigungsdatum, also das Erledigungsdatum ist NACH dem Stichtag
        Es greifen hier NUR die Termine des jeweiligen Auftragskopfes, da ja nur der jeweils gesamte Auftrag ausgewertet wird.

    -   Mit Details, d.h. der Haken bei Mit Details ist gesetzt
        a.) Das Belegdatum muss vor dem Stichtag sein
        b.) Liefertermin der einzelnen Position muss vor dem Stichtag (<=) sein. Es hätte die Position bis zum Stichtag geliefert werden sollen
        c.) Die gesamte AB darf zum Stichtag noch nicht erledigt sein. D.h. der Stichtag ist vor dem Erledigungsdatum, also das Erledigungsdatum ist NACH dem Stichtag
        Eine Berücksichtigung der Erledigungszeitpunkte der einzelnen Positionen ist derzeit datentechnisch nicht vorgesehen.

-   offene Positionen

    -   Liefertermin von ist nicht besetzt. D.h. es wirkt der Stichtag
        Der Liefertermin der jeweilige Position ist bis zum Stichtag zu liefern und noch offen und der Auftrag / die Auftragsposition ist noch nicht erledigt

    -   Liefertermin von ist ebenfalls angegeben
        Der Liefertermin der jeweiligen Position muss >= dem Liefertermin von und <= dem bis/bzw. Stichtag (Liefertermin bis) sein. Sie muss noch offen sein und der Auftrag ist noch nicht erledigt.

-   Zusätzliche Info zu offene Positionen:

    -   Wenn die zusätzliche Sortierung nach Liefertermin angehakt ist, dann greift zuerst die oben gewählte Sortierung, z.B. nach Kunden und danach die Sortierung nach dem (Positions-)Liefertermin

    -   Wenn die Sortierung nach Abliefertermin angehakt ist, so wird vom Liefertermin der Position noch die Lieferdauer des Kunden abgezogen und dann nach diesem Termin sortiert.
        Haben Sie als Hauptsortierung nach Kunden ausgewählt. so erhalten Sie hier eine praktische Liste, für welchen Kunden was wann zu liefern ist.

-   offene Details
    Es werden die Aufträge aufgeführt, deren Belegdatum vor dem Stichtag ist und die zum Stichtag noch nicht erledigt sind

    -   Zusätzliche Info:
        Wenn Sortierung nach Liefertermin angehakt wird, so wird als Untersortierung vom eingegebenen Positionsliefertermin die Lieferdauer an den Kunden abgezogen und nach diesem errechneten Termin sortiert. Auch diese Liste zeigt eine gute Ausliefer-Übersicht.
        Wenn zusätzlich noch die Lagerstandsdetailbetrachtung angehakt wird, so erhalten Sie zusätzlich dazu die ersten Ebene der jeweiligen Stücklsite und können so sehr einfach prüfen ob für die Fertigung / Auslieferung ausreichend Vormaterial vorhanden ist.

#### Auftragspsitionstermin und Lostermine ändern
Ab der Version 0.2.0 steht in den Auftragspositionen die Möglichkeit zur Verfügung die einzelnen Auftragspositionen in Verbindung mit den darauf referenzierten Losen gemeinsam zu ändern.
Durch Klick auf den Button ![](Positionstermine_verschieben_01.png) Positionstermine verschieben kann die Auftragspositino gemeinsam mit den Losterminen (Beginn und Ende) verschoben werden.<br>
![](Positionstermine_verschieben_02.png)<br>
In Abhängigkeit ob mit dieser Auftragsposition direkt ein Los verbunden ist, oder nicht, erscheint dieser Umfangreiche Dialog.
D.h es werden in einer Änderung sowohl die 


#### Retourenabwicklung
Bei manchen Geschäftsfällen sind auch Retouren abzuwickeln. Das bedeutet, dass Sie Geräte liefern und vom Kunden andere Geräte zurückbekommen. In **Kieselstein ERP** tragen Sie diese Positionen Vorzeichen-richtig in den Auftrag ein. Dies bewirkt, dass die von Ihnen zu liefernden Positionen zum Liefertermin in die Reservierung übernommen werden, und die Retouren zum Finaltermin in der Reservierung gelistet werden. Damit ist diese Warenbewegung auch in der Bewegungsvorschau enthalten und wird bei den Bestellvorschlägen entsprechend berücksichtigt.

<a name="Wiederbeschaffung"></a>

#### Wiederbeschaffung
Mit dieser Auswertung, welche Sie im Auftrag im Menüpunkt Info finden, wird die Wiederbeschaffung der Auftragspositionen über die Stücklistenauflösung ermittelt. Hier werden vor allem die Durchlaufzeiten der Stücklisten und die Wiederbeschaffungszeiten der einzelnen Zukaufkomponenten herangezogen und aufgelistet. Damit können Sie abschätzen, mit welchem Zeithorizont Sie bei Neubeschaffung aller Komponenten rechnen müssen. 
Die Summe der Durchlaufzeiten und die Wiederbeschaffungszeit aus Artikellieferant bewirkt, dass der früheste Liefertermin = morgen 0:00 + höchste Gesamtwiederbeschaffungszeit + Kundenlieferdauer ist.<br>
Die Gesamt-Wiederbeschaffungszeit wird absteigend sortiert.<br>
Nicht berücksichtigt wird der Einfluss der Fertigungszeit also der freien Kapazitäten. Als sehr grobe Information wird die Summe der in den Stücklisten enthaltenen Arbeitszeit angedruckt. Für die Einlastung der Maschinen und Mitarbeiter verwenden Sie bitte die Auslastungsanzeige bzw. das Journal Auslastungsvorschau im Modul Los/Fertigung.<br>
Für die Auflösungstiefe der Widerbeschaffungsvorschau beachten Sie bitte auch den Parameter AUFTRAG_ABVORKALK_STKL_AUFLOESUNG_TIEFE, welcher Default auf 3 Ebenen eingestellt ist.

Bei Installationen mit mehreren Mandanten und gemeinsamen / zentralem Artikelstamm, steht auch die Auswertung über alle Mandanten zur Verfügung.<br>
Da in dieser Konstellation davon ausgegangen wird, dass eine Stückliste nur bei einem Mandanten gefertigt wird, kann Mandantenübergreifend die Wiederbeschaffungszeit errechnet werden.<br>
Wichtig:<br>
Der Parameter ARTIKELWIEDERBESCHAFFUNGSZEIT muss bei allen beteiligten Mandaten gleich eingestellt sein.<br>
Bei der Ermittlung der Wiederbeschaffung wird der aktuelle Lagerstand der Bedarfe ausgehend von dem Mandanten der die Stückliste fertigt angedruckt. Ist dieser geringer als die benötigte Menge, werden, soweit gegeben, die Lagerstände der anderen Mandanten angegeben.
![](Artikel_Wiederbeschaffung.jpg)<br>
Durch die rot hinterlegte Zeile wird signalisiert, dass für den fertigenden Mandanten kein Lagerstand gegeben ist.<br>
Die grün gedruckt Zeile zeigt dass am anderen Mandanten ausreichen Ware vorhanden ist.<br>
Selbstverständlich ist hier noch zu berücksichtigen, dass gegebenenfalls die Ware vom einen zum anderen Mandanten zu liefern ist.

#### Vorschlagswert für nicht definierte Wiederbeschaffungszeiten
Mit dem Parameter DEFAULT_WIEDERBESCHAFFUNGSZEIT kann der Vorschlagswert für nicht definierte (Artikel-Lieferanten-)Wiederbeschaffungszeiten in der Auswertung der Auftragswiederbeschaffung und in der Angebotsvorkalkulation definiert werden. Ob sich dieser Wert auf Kalender-Tage oder Kalender-Wochen bezieht, wird durch den Parameter ARTIKELWIEDERBESCHAFFUNGSZEIT bestimmt.

<a name="Rollierende Planung"></a>

#### Rollierende Planung
Die rollierende Planung dient der Weitergabe des teilweise verbindlichen Forecasts Ihres Kunden an Ihre Lieferanten.<br>
Sie finden diese Auswertung unter Info, rollierende Planung.

Die Idee hinter der Rollierenden Planung ist folgende:
- Sie erhalten von Ihrem Kunden Forecast-Planungen für die nächsten 3-12 Monate.
- Sie legen pro Monat einen Rahmenauftrag an und schreiben in die Projektbezeichnung das Monat. Zugleich setzen Sie den Rahmentermin auf das Ende des Monates. Achten Sei darauf, dass die Auftragsnummern und die Monate chronologisch zusammenpassen.
- Nun tragen Sie in den Monats-Rahmenauftrag die Forecastzahlen Ihres Kunden ein.
Haben Sie nun die Daten für die nächsten 12 Monate erfasst, so sehen Sie in der Auswertung der Rollierenden Planung welche Einzelkomponenten Sie in den nächsten Monaten für die aufgrund der enthaltenen Stücklisten errechneten Materialien benötigen. Drucken Sie diese gruppiert nach Lieferanten aus, so können Sie diese direkt an Ihre Lieferanten weiterleiten.

Ausgangsbasis sind die offenen Rahmenaufträge des ausgewählten Kunden, also des Kunden des Auftrags auf dem der Cursor bei Aufruf der Funktion steht.
Es werden damit vor allem die Mengen für die nächsten 12 Monate errechnet.
Dazu werden alle offenen Rahmenaufträge des Kunden herangezogen und die ältesten 12 offenen dieses einen Kunden berücksichtigt. Sind weniger Rahmenaufträge offen, so werden keine Werte errechnet, sind mehr offen, werden nur die ältesten 12 berücksichtigt.
Der Begriff Monat, dient hier nur der üblichen Verwendungsweise für die rollierende Planung. Die tatsächlichen Zeiträume sind außerhalb von **Kieselstein ERP** zu definieren. Daher wird auch die Projektbezeichnung aus den jeweiligen Auftragskopfdaten als Spaltenüberschrift bei den Mengen mit angedruckt.
Es wird für jede Auftragsposition die komplette Stücklistenauflösung durchgeführt und zum Schluss alle Positionen nach Artikel verdichtet. Zu Ihrer Übersicht werden Lagerstand und aktuell frei verfügbare Menge mit angedruckt. Zusätzlich stehen auch noch die Wiederbeschaffungszeiten der Lieferanten zur Verfügung.

**<u><a name="Tipp Rollierende Planung"></a>Tipp:</u>** Da es sich anscheinend eingebürgert hat, dass die verbindlich zugesagten Drei-Monats Forecasts, dann doch nicht so genau gehalten werden, empfiehlt es sich, die "alten" Planungen aufzubewahren, also im System stehen zu lassen.
Ein Beispiel:
am 30.7\. erhalten Sie die Planung für August, September, Oktober
am 30.8\. erhalten Sie die Planung für September, Oktober, November
am 30.9\. erhalten Sie die Planung für Oktober, November, Dezember
Sie haben aber am 31.7\. die Planung für Oktober verbindlich weitergegeben, Material Ihrer Unterstücklisten usw. eingekauft.
Weicht nun die am 30.9\. erhaltene Planung deutlich von der am 30.7\. erhaltenen Planung ab, so haben Sie normalerweise das Recht, den Mehraufwand Ihrem Kunden in Rechnung zu stellen. Ob Sie dies durchsetzen können steht auf einem anderen Blatt.
Sie können die tatsächlichen Lieferungen, den tatsächlichen Wareneinsatz für einen Zeitraum mit dem Journal [Rahmenerfüllung](#Rahmenerfüllung) auswerten. Und erhalten damit eine Vorstellung davon, wie exakt Ihr Kunde seinen, oft verbindlich zugesagten, Forecast erfüllt.

Im System stehen lassen, bedeutet: Sie erledigen den am 30.7\. für Oktober angelegten Rahmenauftrag, damit er in den Berechnungen usw. nicht mehr wirkt, aber in Ihrer Auslieferungsbetrachtung für den Oktober wird der am 30.7. angelegte Rahmenauftrag für die Rahmenerfüllungsstatistik als Vergleichswert herangezogen.

Tipp: Bitte beachten Sie auch das Modul [Forecast]( {{<relref "/verkauf/forecast">}} ), welches in einer sehr umfassenden Variante inkl. EDI Import und in einer kleinen Ausführung zur Verfügung steht.

#### Wieso stimmen Rollierende Planung und Rahmendetailbedarf nicht zusammen?
Auf den ersten Blick ist man der Meinung, dass die Detailmengen der [Rollierenden Planung](#Rollierende Planung) und die Detailmengen des [Rahmendetailbedarfs](#Rahmendetailbedarf) zusammenstimmen müssten. Dies ist ausschließlich bei gänzlich neuen Produkten = Stücklisten und bei aktualisiertem Rahmendetailbedarf der Fall.

Bitte berücksichtigen Sie, dass im Rahmendetailbedarf der Lagerstand aller Läger und die Mengen in Fertigung berücksichtigt werden (abgezogen). Dies wird bei der Rollierenden Planung, welche dafür gedacht ist, dass Sie den Bedarf an Ihren Lieferanten weitergeben, nicht berücksichtigt.

Für den Rahmendetailbedarf wird zusätzlich der Bedarf nach Terminen sortiert durch Lagerstand und den Mengen in Produktion erfüllt. So kann es durchaus den Anschein erwecken, dass nur ein Teil der Rahmenaufträge im Rahmendetailbedarf enthalten ist. Bei genauerer Betrachtung stellt sich heraus, dass diese Situation sich jedoch durch die terminliche Steuerung ergibt.

Um die Übersicht etwas zu erleichtern, haben wir in der Stücklistenverwendung auch die Reservierungen, Rahmenreservierungen, Fehlmengen, Lagerstände mit angedruckt.

#### Verfügbarkeit
Gerade bei vielen Leihstellungen ist es oft nicht einfach die Verfügbarkeit eines Artikels zum Zeitpunkt zu ermitteln. In dieser Auswertung sehen Sie die benötigte Menge und den fiktiven (theoretischen) Lagerstand zum Zeitpunkt der geplanten Lieferung. Damit können Sie erkennen welche Positionen kritisch in Ihrer Verfügbarkeit sind und entsprechende Maßnahmen einleiten.

#### Rahmenaufträge, Abrufaufträge, freie Aufträge mit unterschiedlichen Positionsterminen
In **Kieselstein ERP** stehen verschiedene Auftragsarten zur Verfügung.

-   Freie Aufträge<br>
    Freie Aufträge sind direkt abzuwickelnde Kundenaufträge.
-   Wiederholende Aufträge<br>
    Mit wiederholenden Aufträgen können Sie Miet / Wartungsrechnungen zu entsprechenden Stichtagen und Wartungsintervallen definieren. Diese Aufträge werden durch die Funktion Wiederholende Aufträge abrechnen in der Rechnungsverwaltung verwendet. Es werden aus den Aufträgen direkt Rechnungen erzeugt. Ein wiederholender Auftrag wird solange verwendet, bleibt solange offen, bis er  manuell erledigt wird.
-   Rahmenaufträge<br>
    Rahmenaufträge dienen der Definition von Rahmenvereinbarungen mit Ihren Kunden. Die Lieferdetails, die abgerufenen Stückzahlen werden durch die Abrufaufträge bestimmt.
-   Abrufaufträge<br>
    sind die Abruf-Lieferungen zu den Rahmenaufträgen. Es ist durchaus üblich, dass es Mengen und Artikel-Unterschiede zwischen den definierten Rahmenaufträgen und den dann tatsächlich eingelasteten Abrufen gibt. Denken Sie z.B. an Versionsänderungen von Baugruppen.

#### In einem Rahmenabruf sollte ein anderer Artikel geliefert werden als im Rahmenauftrag definiert ?
Gerade bei Rahmenaufträgen kommt es immer wieder vor, dass andere Artikel, z.B. neuere Versionen, leicht geänderte Komponenten, vom Kunden in diesen Rahmenauftrag mit einbezogen werden als ursprünglich definiert. Wichtig ist hierbei, dass trotzdem die offene Menge des Rahmenauftrages / der Bezug zur Rahmenauftragsposition gegeben bleibt, da ja die vereinbarte Rahmenmenge und oft auch der Preis, gleich bleiben.

Gehen Sie dazu wie folgt vor:
-   Legen Sie einen neuen Abrufauftrag an
-   Wechseln Sie in Sicht Rahmen und übernehmen Sie die Rahmenauftragsposition, die eigentlich abgerufen wird, also die ursprünglich vereinbarte Position.
-   Gehen Sie nun in die Positionen und **ändern** Sie bei der übernommenen Position die Artikelnummer und die Menge wie gewünscht ab. Beachten Sie, dass die Verkettung zur Rahmenauftragsposition nur dann erhalten bleibt, wenn die Position geändert wird. Wird die Position gelöscht und neu hinzugefügt, so wurde die Verkettung entfernt und dadurch wird die Rahmenposition nicht mehr mitverwaltet, was bewirkt, dass die offenen Rahmenmengen nicht dem entsprechen was eigentlich beabsichtigt wurde.

#### Wieso sieht man in einem Rahmenauftrag keine Lieferscheine und keine Rechnungen ?
Im Reiter Lieferschein / Rechnung werden nur die direkt dem Auftrag zuordenbaren Lieferscheine bzw. Rechnungen angezeigt. Da ein Rahmenauftrag durch Abrufaufträge erfüllt wird, erfolgen auf einen Rahmenauftrag keine direkten Lieferungen. Somit sind auch keine Lieferscheine bzw. Rechnungen direkt dem Auftrag zugeordnet. Die durch die Abrufe gegebene indirekte Zuordnung finden Sie im Menü - Info, Rahmenübersicht.

#### Es sollte ein anderer Artikel als in der Auftragsbestätigung definiert geliefert werden?
Es kommt immer wieder vor, dass in der Auftragsbestätigung ein Artikel mit z.B. Version 1.0 definiert wird. Nun steht zum Lieferzeitpunkt bereits eine neuere Version zur Verfügung. Nun kann man einerseits die Auftragsbestätigung entsprechend abändern. Alternativ steht auch die Möglichkeit zur Verfügung einen anderen Artikel auszuliefern, mit dem die gewählte Auftragsposition erfüllt wird. Um dies zu erreichen gehen Sie bitte wie folgt vor:
a.) Diese Funktionalität steht nur im Lieferschein zur Verfügung
b.) Legen Sie einen Auftragsbezogenen Lieferschein an und wechseln Sie in den Reiter Sicht Auftrag
c.) Übernehmen Sie die Originalposition in den Lieferschein
d.) wechseln Sie in den Reiter Positionen des Lieferscheins und ändern Sie, durch Klick auf den Artikelknopf, den Artikel der mit dieser Auftragsposition ausgeliefert wird.

#### Wie wird ein Auftrag erledigt ?
Üblicherweise wird ein Auftrag durch Lieferungen erfüllt. Wurden alle Positionen geliefert, so wird der Auftrag automatisch auf Erledigt gesetzt. Ein Rahmenauftrag wird dann auf erledigt gesetzt, wenn alle Positionen des Rahmenauftrages abgerufen sind und diese Positionen auch alle ausgeliefert sind. Das bedeutet, dass alle Lieferscheine der Abrufaufträge erledigt sein müssen. 

**ACHTUNG:** Ein Wiederholender Auftrag wird niemals automatisch auf erledigt gesetzt. Er muss manuell auf erledigt gesetzt werden.

Bitte beachten Sie auch den Parameter AUFTRAG_AUTOMATISCH_VOLLSTAENDIG_ERLEDIGEN ([siehe](#Erledigtstatus)).

<a name="Erledigtstatus"></a>

#### Auftrag nur von Berechtigten manuell erledigbar
Der Mandantenparameter (AUFTRAG_AUTOMATISCH_VOLLSTAENDIG_ERLEDIGEN) steuert, ob die Erledigung des Auftrags über die automatischen Statusänderungen gemacht wird, oder der Erledigtstatus manuell gesetzt wird.

Wenn der Parameter auf 0 gesetzt ist, so greift die automatische Auftragsstatusänderung nur bis "Teilerledigt". Für die vollständige Erledigung wird ein eigenes Rollenrecht benötigt. Damit verknüpft ist auch das Recht die Erledigung aufzuheben.

Um den Status auf Erledigt zu ändern bzw. wieder aufzuheben sind für die Menüpunkte 'manuell Erledigen' und 'Erledigung aufheben' das Recht AUFT_DARF_AUFTRAG_ERLEDIGEN und AUFT_AUFTRAG_CUD erforderlich. Zur Protokollierung wird das manuell Erledigen/Aufheben im lp_critical.log angeführt.

Wenn ein Auftrag vollständig erledigt ist, dann dürfen darauf keine Buchungen mehr gemacht werden. Um zu verhindern, das auf einen Auftrag Zeiten gebucht werden: Parameter ZEITBUCHUNG_AUF_ERLEDIGTE_MOEGLICH auf 0 setzen.

#### Wo ist ersichtlich wer wann ein Auftrag erledigt hat ?
Dies wird im Auftrag in der Statuszeile angezeigt. D.h. um diese Information zu sehen, wechseln Sie bitte in die Kopfdaten des Auftrags. Nun werden in der Statuszeile neben dem Status ![](Auftrag_Erledigt_Anzeige.gif) das Erledigungsdatum in das Kurzzeichen des Erledigers angezeigt.

<a name="Wiederholender_Auftrag"></a>

#### Wie wird ein wiederholender Auftrag definiert ?
Für einen wiederholenden Auftrag muss zusätzlich zu den bisherigen Angaben noch das Wiederholungsintervall und der Startzeitpunkt (ab Belegdatum möglich) definiert werden. ![](Wiederholungsintervall.gif)

Damit steuern Sie wann und wie oft eine entsprechende Rechnung automatisch erstellt werden sollte.

Immer wenn in der Rechnungsverwaltung Wiederholende Aufträge verrechnen gewählt wird, wird geprüft ob für diesen Auftrag die aufgrund des Startdatums und des Wiederholungsintervalls definierten Rechnungen verrechnet sind. Sind verschiedene Termine der wiederholenden Aufträge noch nicht verrechnet so werden diese mit laufender Nummer nachgetragen.

**WICHTIG:** Als Rechnungsdatum wird das aufgrund des Wiederholungsintervalls errechnete Datum verwendet.

Das bedeutet, Sie sollten diese Funktion immer zeitnah zu den Wiederholungsintervallen aufrufen um eine entsprechende Durchgängigkeit der Rechnungsnummern und der dazugehörigen Belegdatum zu erreichen.

**Hinweis:** Dies kann durch die Verwendung des [Neu-Datums der Rechnung]( {{<relref "http://localhost:1313/verkauf/rechnung/#neu-datum" >}} ) übersteuert werden. D.h. sollten die automatisch angelegten wiederholenden Rechnungen auf ein bestimmtes Datum hin angelegt werden, so setzen Sie bitte vor Aufruf der Funktion in der Rechnungsverwaltung das Neu-Datum auf den entsprechenden Wert.

**ACHTUNG:** Wird das Wiederholungsintervall geändert und sind bereits Rechnungen auf diesen Auftrag gebucht, so muss auch der Termin ab dem das (neue) Wiederholungsintervall gilt in die Zukunft gesetzt werden.

Eine Änderung des Wiederholungsintervalls von z.B. jährlich auf halbjährlich ohne Änderung des Wiederholungstermins würde bewirken, dass alle fehlenden Rechnungen des neuen Intervalls nachgetragen werden. Sind diese in anderen Geschäftsjahren, so kommt es zu entsprechenden Programmabbrüchen.

**ACHTUNG:** Ist eine Rechnung eines Wiederholungsauftrags ungerechtfertigt erstellt worden, so muss diese durch eine Gutschrift ausgeglichen werden. Bei der Berechnung ob eine Rechnung aufgrund eines wiederholenden Auftrags neu anzulegen ist, werden stornierte Rechnungen nicht berücksichtigt. D.h. wird eine Rechnung, welche mit einem wiederholenden Auftrag verknüpft ist, storniert, so wird, wenn für diesen Termin eine wiederholende Rechnung auszustellen ist, diese Rechnung automatisch wieder angelegt.

**WICHTIG:** Für den wiederholenden Auftrag muss der wiederholende Termin gesetzt werden. Ein wiederholender Auftrag ohne Termin ist nicht zulässig.

**WICHTIG:** Es können neben den automatisch erzeugten Rechnungen eines wiederholenden Auftrags auch manuelle Rechnungen dazugehängt werden, z.B. damit das Gesamtprojekt besser beurteilt werden kann. Hier ist enorm wichtig, dass die Bezeichnung in den Kopfdaten keine Zeitbereichsangabe enthalten darf. So darf es bei einer zusätzlichen Rechnung für einen Quartals-Wiederholenden Auftrag auf keinen Fall lauten: Rechnung für April 2009 - Juni 2009\. Sondern es muss vielmehr lauten Initialgebühren für, oder ein leerer Eintrag usw.

#### Das Wiederholungsintervall kann nicht mehr verändert werden?
Da bei einem Wiederholenden Auftrag das Wiederholungsintervall gemeinsam mit dem Wiederholungstermin die Basis für die Termine der zu erstellenden Rechnung ist, dürfen bei einem Wiederholenden Auftrag, für den bereits Rechnungen erstellt wurden, diese Vorgaben nicht mehr verändert werden.
Wenn die Termine / Intervalle eines Wiederholenden Auftrags verändert werden müssen, (was eigentlich nicht in der Natur eines Wiederholenden Auftrags liegt), so gehen Sie bitte wie folgt vor:
Erledigen Sie bitte den Auftrag. Legen Sie einen neuen Auftrag an, z.B. durch Klick auf das blaue Auftragsbuch ![](Auftrag_kopieren.gif) (kopieren eines Auftrags) in der Auftragsauswahl und ändern Sie nun Termin und Wiederholungsintervall entsprechend.

Vorteil des Wiederholenden Auftrags gegenüber z.B. Jahresrechnungen mit monatlichen Zahlungen:
Aus einem Wiederholenden Auftrag werden z.B. monatliche Rechnungen für Wartungsarbeiten erzeugt. Daraus ergibt sich, dass für jede Monatsrechnung natürlich die einbehaltene Umsatzsteuer abzuführen ist. Wird eine Jahresrechnung gemacht, so muss die einbehaltene Umsatzsteuer sofort abgeführt werden, was von manchen Kunden / Branchen nicht akzeptiert wird.

#### Wie kann in den wiederholenden Aufträgen eine entsprechende Preisanpassung gemacht werden?
Natürlich ändern sich auch bei Wartungs- und Mietaufträgen die entsprechenden Preise, oft auch an den Index gekoppelt. Um nun nicht alle wiederholenden Aufträge, manuell aktualisieren zu müssen gibt es die Preispflege für wiederholende Aufträge.
D.h. die Vorgehensweise ist wie folgt:
a.) Definieren Sie die neuen Verkaufspreise Ihrer Artikel ab z.B. dem nächsten 1.1\. (Preisgültigkeit ab)
b.) Wählen Sie im Modul Aufträge ![](WiederholendeAbPreispflege1.gif) Wiederholende Aufträge zur Preisgültigkeit aktualilsieren.
Geben Sie im nachfolgenden Fenster das zu a.) passende Preisgültigkeitsdatum z.B. zum nächsten 1.1\. an.
![](WiederholendeAbPreispflege2.jpg)
Mit maximaler Preisabweichung legen Sie fest, um welchen Prozentsatz Preise sich maximal verändern dürfen. Dies ist interessant, wenn Sie vom eigentlichen Artikelpreis abweichende Preise in den Positionen definiert haben.

Es sind dafür die Rechte: LP_FINANCIAL_INFO_TYP_1, FB_CHEFBUCHHALTER, AUFT_AKTIVIEREN erforderlich.

#### Was bedeuten die einzelnen Spalten der Umsatzübersicht?
Sie finden in **Kieselstein ERP** in den Verkaufsmodulen jeweils eine Umsatzübersicht. [Siehe dazu auch]( {{<relref "/verkauf/angebot/#was-bedeuten-die-einzelnen-spalten-der-umsatz%c3%bcbersicht" >}} )

Vor der eigentlichen Auswertung muss die Auswertungsart definiert werden. Wählen Sie aus:

![](Auftragsumsatzuebersicht1.gif)

Damit können verschiedene Betrachtungen am Auftrag durchgeführt werden.
| Auswertung nach | Bedeutung |
| --- | --- |
| Belegdatum | Auftragsumsatz / Auftragseingang im Zeitraum |
| Liefertermin | offene Lieferungen / Abrufe zum Liefertermin, Rahmentermin |
| Finaltermin | offene Lieferungen zum Finaltermin, also ev. dem Projektabrechnungszeitpunkt |

Wählen Sie zusätzlich das zu betrachtende Geschäftsjahr.

![](Auftragsumsatzuebersicht2.gif)

Diese Darstellung ist zusätzlich in zwei Bereiche gegliedert:
| Bereich | Bedeutung |
| --- | --- |
| Offene | was wurde im Zeitraum noch nicht ausgeliefert / abgerufen |
| Eingang | Auftragseingang der Auftragsart im Zeitraum |

| Bereich | Auftragsart | Bedeutung |
| --- | --- | --- |
| Offene | Freie | Offene, also noch nicht gelieferte Auftragswerte der angelegten und aktivierten freien Aufträge |
| Offene | Abrufe | Von den angelegten und aktivierten Abruf-Aufträgen wurden die angegebenen Werte noch nicht ausgeliefert |
| Offene | Rahmen | Von den angelegten und aktivierten Rahmen-Aufträge wurde der Wert noch nicht abgerufen. |
| Eingang | Freie | Auftragseingang der freien Aufträge im Zeitraum |
| Eingang | Abrufe | Eingang von Abrufaufträgen aus Rahmenaufträgen im ZeitraumHinweis: das addieren von Abrufen und Rahmen würde im Endeffekt zu einer doppelten Berücksichtigung des Auftragseingangs führen. Wählen Sie die für Sie jeweils passende Betrachtung. |
| Eingang | Rahmen | Auftragseingang der Rahmenaufträge, üblicherweise mit einer entsprechend langen Laufzeit. |

| Begriff | Erklärung |
| --- | --- |
| Freie Aufträge | sind direkte Aufträge, welche ohne Rahmenbezug sind |
| Abrufe | sind Abrufe aus Rahmenaufträgen |
| Rahmen | Rahmenaufträge |

#### Auftragseingang im Zeitraum?
<a name="Auftragumsatzstatistik"></a>
Verwenden Sie für diese Auswertung Journal, Alle Aufträge und definieren Sie Zeitraum und Sortierung. 
Sie finden in **Kieselstein ERP** in den Verkaufsmodulen jeweils eine Umsatzübersicht. [Siehe dazu auch]( {{<relref "/verkauf/angebot/#was-bedeuten-die-einzelnen-spalten-der-umsatz%c3%bcbersicht" >}} )

#### offener Auftragsstand nach Artikelgruppen
Ergänzend zur Vergangenheitsbetrachtung des Kundenumsatzes steht für die erwarteten Umsätze in der Zukunft das Journal der Auftrags-Umsatzstatistik zur Verfügung.<br>
Die Ausgangsbasis ist hier das Belegdatum der Aufträge (und nicht der offene Liefertermin wie aus den Journalen).<br>
D.h. mit diesem Stichtag waren Aufträge mit Belegdatum vor dem Stichtag ![](UmsatzStichtag.png) bereits angelegt **und** es sind von diesen Aufträgen Positionen im Wert von zum Stichtag noch nicht geliefert.<br>
D.h. die Auftragspositionen (Menge x Preis) abzüglich der <= Stichtag bereits gelieferten Positionen, also Basis Lieferschein-/Rechnungsdatum.<br>
Zusätzlich kann in dieser Sicht, ausgewählt werden, ob nur die Rahmenaufträge oder die Freien Aufträge plus Abrufe ausgewertet werden sollten.

#### Adressen im Auftrag
Die Adressen im Auftrag sind als lernendes System aufgebaut. D.h. es werden anhand der in den Aufträgen erfolgten Zuordnungen die nachfolgenden Adressen vorgeschlagen. Diese sind zusätzlich nach Häufigkeiten sortiert, sodass die meist verwendete Adresse immer oben ist. Ist bei einer Zuordnung nur eine Adresse eingetragen, so wird diese direkt zugeordnet, sind bereits mehrere Adressen verwendet worden, so muss vom Benutzer eine entsprechende Auswahl aus den bisher erfolgten Zuordnungen getroffen werden. Durch Eingabe einer neuen Liefer- bzw. Rechnungsadresse wird eine weitere Zuordnung vorgenommen.

Von der Reihenfolge her werden die Adressen von oben nach Unten abgearbeitet.

D.h. Auftragsadresse danach Auswahl der Lieferadresse und danach Rechnungsadresse.

| Adresse | Bezeichnung | Regel |
| --- |  --- |  --- |
| Kunde | Auftragsadresse. Dies ist der Auftragsempfänger | Wird vom Anwender ausgewählt |
| Lieferadresse | Anlieferung der Ware erfolgt mittels Lieferschein an diese Adresse | Wird automatisch von der Auftragsadresse kopiert und gegebenenfalls vom Anwender geändert |
| Rechnungsadresse | Rechnungslegung erfolgt an diese Adresse | Wird Aufgrund des Kundenstamms ermittelt. D.h. die Rechnungsadresse wird anhand des Kunden der die Lieferadresse ist ermittelt. Ist bei der Lieferadresse keine Rechnungsadresse hinterlegt, so ist die Rechnungsadresse gleich der Lieferadresse. |

Achtung: Beeinflusst wird die Adressvorbelegung auch vom Mandantenparameter ADRESSVORBELEGUNG. Folgende Werte haben folgende Auswirkungen:
-   0 = Rechnungsadresse wird nach Häufigkeit (Rechnungsadressen der bisherigen Aufträge zu den Auftragsadressen) vorbelegt
-   1 = Wirkung wie oben beschrieben
-   2 = Lieferadresse wird nach Häufigkeit vorbelegt

Lieferadresse und Rechnungsadresse werden bei Lieferschein anhand Auftrag automatisch in den Lieferschein übernommen. [Siehe dazu auch]( {{<relref "/docs/stammdaten/kunden/#rechnungsadresse" >}} ).

#### Kann der Ansprechpartner einer Lieferadresse definiert werden?
Ja. Sie finden in der Zeile der Lieferadresse auch den Knopf ![](Auftrag_LS_Ansp_auswaehlen.gif) Ansprechpartner.

Die Vorbelegung des Ansprechpartners wird anhand des Parameters AUFTRAG_LIEFERADRESSE_ANSP_VORBESETZEN gesteuert.
- 0 = nicht vorbesetzen,
- 1 = Ansprechpartner anhand der Lieferadresse übernehmen (es wird der erste Ansprechpartner verwendet)
- 2 = Ansprechpartner aus Auftragsadresse übernehmen

#### Auftrag Teilnehmer
Unter dem Modulreiter ![](Auftrag_Teilnehmer.gif) Teilnehmer können die Teilnehmer eines Auftrages definiert werden. Neben dem Teilnehmer wird auch seine Funktion in diesem Projekt definiert. Die Liste der verfügbaren Funktionen kann im System, unterer Modulreiter Sprache, oberer Modulreiter Sachbearbeiterfunktion definiert werden. Diese Funktionsliste ist identisch mit der Definition der Sachbearbeiter im Kundenstamm.

##### Externe Teilnehmer
Oft sind auch externe Teilnehmer für einen Auftrag wichtig. Diese können ebenfalls im Modulreiter Teilnehmer durch Klick auf ![](Externen_Teilnehmer_hinzufuegen.png) Externen Teilnehmer hinzufügen, aus dem Partnerstamm ausgewählt werden.

#### Wartungsauswertung
Die Wartungsauswertung wurde für planmäßige Wartungs- und Überprüfungsaufträge geschaffen.
Die Auswertung finden Sie unter Auftrag, Journal, Wartungsauswertung.
Die Voraussetzungen dafür sind:
Im Auftrag sind Artikelpositionen hinterlegt.
Jedem Artikel ist eine Artikelgruppe hinterlegt, diese wird im Wartungsjournal unter den Prüfungsarten ausgedruckt.
Jedem Auftrag sind für jede Artikelgruppe auch Teilnehmer hinterlegt.
Die Kennungen der Teilnehmerfunktionen müssen mit den Kennungen der Artikelgruppen identisch sein.

Die Durchführung der Wartungsarbeiten wird in den Lieferscheinen abgebildet. Die Zuordnung erfolgt über die Lieferadressen (Standorte).
Konnte die Tätigkeit / Prüfung durchgeführt werden, so wird diese im Lieferschein mit Menge eingetragen. Konnte Sie nicht durchgeführt werden, sollte sie aber trotzdem auf den nächsten Prüfungsintervall verlegt werden, so ist die Menge mit 0 (Null) einzutragen. Sie wird nun im Journal der Wartungsauswertung als nicht durchgeführte Prüfung angeführt.

In Wartungsjournal, werden alle erforderlichen Prüfungen bis zum gewünschten Stichtag aufgelistet. Diese Liste ist nach den geplanten Prüfern sortiert.
Das Wartungsintervall ist beim jeweiligen Artikel unter Sonstiges zu hinterlegen und beschreibt das Wartungsintervall in Jahren. **Wichtig:** Im Wartungsjournal werden nur offene Aufträge berücksichtigt.

Ermittlung des Termins für die nächste Prüfung:
- a.) es gibt noch keine nachfolgende Prüfungen.
Hier wird der im Auftrag hinterlegte Termin verwendet.
- b.) Der Termin für die nächste Wartung / Prüfung wird anhand der zuletzt durchgeführten Prüfung, welche durch die Erstellung des Lieferscheines abgebildet wird, zuzüglich des Wartungsintervalls des jeweiligen Artikels errechnet. Sind Artikel in den Aufträgen angeführt, haben diese jedoch kein Wartungsintervall hinterlegt, so erscheinen diese Artikel automatisch, da ein Wartungsintervall von 0 (Null) angenommen wird.

![](Wartungsauswertung.gif)

Ist die Prüfungsart leer, so bedeutet dies, dass dem Artikel keine Artikelgruppe zugeordnet ist. Bitte im Artikel unter Detail nachtragen.
Scheint eine Prüfung / Wartungsauftrag trotzdem auf der ersten Seite unter dem leeren Prüfer auf, so ist in dem Auftrag für die gewünschten Prüfungsart kein Prüfer definiert.

<a name="Anzeige von offenen Rahmenmengen mit 0"></a>

#### Anzeige von offenen Rahmenmengen mit 0
Zu dieser Anzeige kann es kommen, wenn in dem angezeigten Rahmenauftrag die Rahmenmenge des Artikels nachträglich auf 0 (Null) geändert wurde. Diese Eingabe ist dann erforderlich, wenn auf dem Rahmenauftrag bereits Abrufe eingetragen sind. Diese Position verschwindet aus der Liste der offenen Rahmenreservierungen, wenn der gesamte Rahmenauftrag oder die einzelne Position des Rahmenauftrages (manuell) erledigt wurde.

#### Kann bei einem teilerledigten Auftrag die Lieferadresse geändert werden ?
Ja. Ändern Sie dazu in den Auftrags-Kopfdaten einfach die Lieferadresse.

**Hinweis:** Es werden dadurch die bereits erstellten Lieferscheine nicht geändert. Es bleiben vielmehr die in den Lieferscheinen eingetragenen Adressen wie ursprünglich aus dem Auftrag übernommen erhalten.

Dies hat vor allem den Vorteil, dass während der Laufzeit eines Auftrages die Anlieferadresse, z.B. aufgrund eines Kundenwunsches geändert werden kann.

#### Journal offene Positionen
Im Journal offene Positionen kann auch ein Filter nach Fertigungsgruppen gesetzt werden. Dieser Filter dient dazu, nur die Auftragspositionen zu erhalten, die Stücklisten sind und der gewünschten Fertigungsgruppen zugeordnet sind.

Bei der Auswahl der Fertigungsgruppe(n) können auch mehrere Gruppen angegeben werden.

Bedienen Sie dazu das Auswahlfenster der Fertigungsgruppen wie folgt:

![](Auftrag_Fertigungsgruppen_Auswahl.jpg)

Klicken Sie auf die erste gewünschte Fertigungsgruppe. Drücken und halten Sie nun die Strg-Taste und wählen Sie die weiteren gewünschten Fertigungsgruppen. Bei der letzten Fertigungsgruppe machen Sie bitte einen Doppelklick.

Damit wird das Auswahlfenster geschlossen und die gewählten Fertigungsgruppen werden in der Journal-Filter Einstellung entsprechend angezeigt.

Alternativ können Sie auch nach der Auswahl der letzten Fertigungsgruppe die Entertaste drücken.

#### Auftragsdokument, Dokumente für Auftrag angeben
Oft ist es erforderlich, dass je Auftrag bestimmte Dokumente bei der Auslieferung mit angegeben werden müssen. Um dies von der Auftragsanlage bis zum Lieferschein durchzureichen, wurde die Funktion Auftragsdokument geschaffen.

Um gewünschte Auftragsdokumente zu definieren, gehen Sie bitte in das Modul Auftrag, unterer Modulreiter Grunddaten, oberer Modulreiter Auftragsdokument.

![](Auftragsdokumententypen_definieren.jpg)

Definieren Sie hier die bei Ihnen üblicherweise erforderlichen Dokumente.

Dies bewirkt, dass immer bei der Speicherung der Auftragskopfdaten nach den Dokumenten gefragt wird.

Zusätzlich besteht bei dieser Frage die Möglichkeit, dass Sie definitiv angeben, dass für diesen Auftrag keine Dokumente erforderlich sind.

![](Auftragsdokumente_definieren.gif)

Zusätzlich bewirkt dies, dass beim Druck des Lieferscheins die Dokumenten Bezeichnungen extra mit angedruckt werden. So wird sichergestellt, dass der/die zuständige (Versand-) Mitarbeiter auch die Dokumente tatsächlich mit gibt.

<a name="Lieferscheinpreise_automatisch_aktualisieren"></a>

#### Nachträgliche Aktualisierung der Preise aus dem Auftrag in den offenen Lieferscheinen?
Gerade wenn Aufträge nach Aufwand abgewickelt werden, z.B. Erstbemusterung, Einzelfertigung und ähnliches, so kommt es immer wieder vor, dass zu dem Zeitpunkt, an dem die Lieferscheine erstellt werden, die Verkaufspreise auch nicht annähernd bekannt sind.

Erst nach der Nachkalkulation des Projektes, der Fertigungsaufträge kann der Verkaufspreis ermittelt werden.

Dieser wird nun in den Auftrag nachträglich eingetragen. Um nun diese neuen Verkaufspreise automatisch in die daran angebundenen, aber noch nicht verrechneten Lieferscheine zu übertragen klicken Sie auf, ![](Lieferschein_Preise_aktualisieren.gif) Preise in Lieferschein(en) aktualisieren.

Es werden nun alle Nettopreise in den noch nicht verrechneten Lieferscheinen dieses Auftrags auf den neuen Wert gesetzt. Damit wird die Verrechnung deutlich einfacher.

#### Können die Preise entsprechend der **Kieselstein ERP**-Preisfindung aktualisiert werden?
Ja, mit einem Klick auf das Taschenrechner-Icon ![](rechnung_klein.JPG) werden die Preise im Auftrag entsprechend der Preisfindung aktualisiert. Das bedeutet das VK-Preislisten, Kundensonderkonditionen, Mengenstaffeln etc. berücksichtigt werden. Bitte beachten Sie, dass manuelle Eingaben zb. von Fixpreisen überschrieben werden.

#### Die im Auftrag eingepflegten Preise sollten automatisch die Verkaufspreisbasis für die Zukunft sein.
Der Vorteil dieser Funktion ist, dass durch die, mit dem Kunden neu ausgehandelten Preise, welche im Auftrag erfasst werden, automatisch auch die Verkaufspreise für weitere Lieferungen definiert werden, solange bis wieder ein neuer Preis vereinbart wird. Um diese Funktion zu aktivieren, setzen Sie bitte den Parameter VERKAUFSPREIS_RUECKPFLEGE auf 1.
Er bewirkt dass beim Speichern der Auftragsposition der Artikel dieser Position eine neue Verkaufspreisbasis mit dem gültig ab Datum zum Belegdatum des Auftrags eingetragen wird. Damit steht er für zukünftige z.B. direkte Lieferscheine zur Verfügung. 

#### Wie findet man die Bestellungen eines Auftrags?
[Siehe bitte]( {{<relref "/einkauf/bestellung/#wie-findet-man-bestellungen-eines-auftrags" >}} )

## Können Auftragsdaten importiert werden?
A: Es stehen dafür verschiedene Importformate zur Verfügung:
### CSV Import
Legen Sie dazu den Auftrag an und wechseln Sie in den Reiter 3 Positionen. Verwenden Sie nun den CSV-Import ![](Auftrag_CSV_Import.gif).<br>
Für den Import werden folgende Daten erwartet, wobei nur die ersten beiden Spalten importiert werden.<br>
Artikel;Menge;\<Positionstermin\><br>
Zusätzlich wird, so wie für CSV Dateien üblich, die erste Zeile als Spalten-Überschrift angenommen.<br>
Wird kein Positionstermin angegeben, so wird der Liefertermin der Kopfdaten übernommen. Wird ein Positionstermin angegeben, so muss dieser im Format TT.MM.JJJJ (Tag, Monat, Jahr) angegeben werden.
### SON CSV
Mit diesem Import können mehrere Aufträge gemeinsam eingelesen werden, wobei die Zusammenfassung durch die gleiche Bestellnummer gegeben ist.<br>
Sie finden diesen im Menüpunkt Auftrag, Import, SON CSV<br>
Der Aufbau der CSV Datei ist wie folgt:<br>
| Spalte | Bedeutung | Bemerkung |
| --: | --- | --- |
| 2 | Lieferantennummer | siehe Kunde, Konditionen, Lieferantennummer |
| 5 | Bestelltyp |
| 9 | Abladestelle |
| 12 | Bestellnummer |
| 14 | Artikelnummer | Übersetzung der Kundenartikelnummer aus den Kunden Sonderkonditionen auf die eigene Artikelnummer |
| 16 | Bezeichnung | falls Handeingabe |
| 18 | Liefertermin | Format dd.MM.yyyy |
| 19 | Menge | |

enthalten.

Wenn ein Artikel im Artikelstamm nicht gefunden werden konnte, werden im Auftrag 2 Positionen angelegt:
- ein Handartikel mit der Bezeichnung aus dem CSV
- eine Texteingabe mit "Artikelnummer XXX aus CSV-Import konnte nicht gefunden werden"<br>
Der in den Kopfdaten des jeweiligen Auftrags eingetragen wird, ist der erste importierte Liefertermin für diesen Auftrag = Bestellnummer<br>
Die erhaltene Bestellnummer wird in das Feld Bestellnummer in den Kopfdaten übernommen.

Anmerkungen:<br>
Bestelltyp: Wenn hier FPQ steht (Erstmusterprüfbericht), so wird für der Artikelposition eine Texteingabe mit dem Eintrag FPQ angelegt.
Abladestelle: Der Kunde wird anhand der "Abladestelle" welche in den Kunde/Konditionen Fremdsystemnr. definiert sein muss, gesucht.

## VAT Import
Diese Funktion steht zur Verfügung, wenn der Parameter AUFTRAGS_VAT_IMPORT aktiviert ist.
D.h. wenn der Parameter auf 1 steht (und nach Neustart des Moduls Auftrag), gibt es im Auftrag einen Menüpunkt Auftrag/Import/VAT XLSX.
Mit Klick auf diesen Menüpunkt müssen nun Kunde und Ansprechpartner und das Import-File ausgewählt werden.
Der Aufbau der Datei ist wie von VAT derzeit (April 2020) angeliefert.
Für den Import wesentlich sind die Spalten:
| Spalte | Bedeutung |
| artNr | dieses wird vorne um VAT ergänzt und als Artikelnummer verwendet |
| bestMenge | sies wird als Menge in den Auftrag übernommen |
| kreisKurz | ist die Lieferadresse. Zur Bestimmung welche Lieferadresse dies ist, wird dieser Wert im Feld Filialnr. im Kundenstamm verwendet. Bitte achten Sie in der Erfassung darauf, diesen Wert eindeutig zu hinterlegen |
| bestText | wird zur Indexprüfung herangezogen, siehe unten |
| bestText | hier wird die erste Zeile auch in das Auftrags-Projekt übernommen. Zusätzlich wird daran die Spalte artNr hinzugefügt |
| bestNr | |
| benCd | werden als bestNr / benCd in die Bestellnummer des Auftrags übernommen |
| losNr | wird mit dem Wort Los Nr. ergänzt und in die Zusatzbezeichnung in der Auftragsposition übernommen |
| preisFw | wird als Ihr Verkaufspreis in die Auftragsposition übernommen |
| eTerm | wird als Termin übernommen |

Wird ein Artikel nicht gefunden, so wird der Import abgebrochen.<br>
Stimmen Preis oder Index nicht überein, wird im Anschluss an die Artikelposition eine Texteingabe mit den Details der Fehler eingetragen.

**Indexprüfung:** Aus dem Artikel = VAT+Spalte E steht in der Kurzbezeichnung (ist üblicherweise die Zeichnungsnummer) die Zeichnungsnummer.<br>
In der Spalte N steht diese Zeichnungsnummer und dahinter das Wort Index und dahinter der Index (als Wort/Zeichen)<br>
Diesen Index verwenden wir. Wenn der Index der im Artikel hinterlegten Revision entspricht alles gut.
Steht hier eine andere Revision oder wird das Wort Index bzw. kein Inhalt für Index gefunden wird danach eine Texteingabe mit "Für obigen Artikel wurde Index .... gefunden bzw. kein Index gefunden. Bitte prüfen." eingetragen.

**Änderungen durch den Kunden:** Da vom Kunden immer wieder Änderungen gemacht werden, wird diese Aufgabe wie folgt gelöst:<br>
Wenn es eine Bestellnummer schon gibt, wird der Auftrag mit einem Auftragszusatzstatus markiert. Die Auflösung / Richtigstellung muss vom Anwender vorgenommen werden. Es ist dies von einer Vielzahl von Faktoren abhängig, wie z.B., Sie stehen mitten in der Produktion, es wurde "nur" die Version geändert (ein Loch dazu oder weniger = zuschweißen usw.).

## [WooCommerce]( {{<relref "/docs/installation/30_schnittstellen/woocommerce"  >}} )

#### Wie kann man nachträglich einen Auftrag zu einem Angebot zuordnen?
[Siehe bitte]( {{<relref "/verkauf/angebot/#kann-ein-angebot-nachtr%c3%a4glich-einem-auftrag-zugeordnet-werden-" >}} )

#### Es wurde ein Angebot in einen Auftrag übernommen, es sollte dies jedoch ein Rahmenauftrag sein. Wie kann ich das ändern?
Die Überleitung eines Angebotes in einen Auftrag, bewirkt dass ein Freier Auftrag angelegt wird. Die Auftragsart ist jedoch nur änderbar, wenn keine Einträge im Reiter Positionen enthalten sind. Um nun einen Auftrag in einen Rahmenauftrag zu ändern gibt es drei mögliche Vorgehensweisen:

1.  Sie übernehmen den Auftrag aus dem Angebot durch Klick auf ![](Uebernahme_Angebot_in_Auftrag.gif) Daten aus bestehendem Angebot übernehmen  in der Auswahlliste.

Das hat den Vorteil dass das Angebot erledigt ist und die grundsätzliche Verknüpfung zwischen Auftrag und Angebot gegeben ist. Nun wechseln Sie in die Auftragspositionen, markieren alle Positionen und kopieren diese in die Zwischenablage ![](Kopieren_in_Zwischenablage.gif). Anschließend löschen Sie die Positionen des Auftrags (Shortcut Strg+D).

Nun ändern Sie in den Kopfdaten des Auftrags die Auftragsart auf Rahmenauftrag,

![](Aendern_Auftragsart.gif)

 wechseln in die Positionen und fügen die Positionen aus der Zwischenablage ![](Uebernahme_aus_Zwischenablage.gif) wieder ein.

2.  Sie legen manuell einen neuen Auftrag an mit der Auftragsart Rahmenauftrag. Nun gehen Sie in das Angebot, kopieren dort die relevanten Positionen in die Zwischenablage, gehen in den Rahmenauftrag und fügen die Angebotspositionen ein. Nun wechseln Sie erneut in das Angebot, wählen im Menü bearbeiten Erledigen und geben den Rahmenauftrag an.
3.  Weiters kann mit dem Icon ![](rahmenauftrag_aus_angebot.gif) ein Rahmenauftrag aus einem Angebot erstellt werden.

<a name="Rahmenerfüllung"></a>

#### Rahmenerfüllung
Unter Info, Rahmenerfüllung finden Sie eine Auswertung, welche die oft für Fließfertigungsaufträge benötigten Daten in übersichtlicher Form gegenüber stellt.

D.h. Ausgangsbasis sind die Artikel und Stücklisten, die im Rahmenauftrag enthalten sind.

Es werden hier alle Unterstücklisten bis hin zum Einzelteil aufgelöst und nach Artikelnummern verdichtet.

Sodann werden die Warenverbrauchen (mit Ausnahme der manuellen Lagerkorrekturen (Handbuchungen)) im Zeitraum den Mengen im Rahmenauftrag und den Einkäufen gegenübergestellt.

Zusätzlich wird der Lagerstand zum Bis-Zeitpunkt und die zum Bis-Zeitpunkt frei verfügbare Menge mit angedruckt, damit Sie eine entsprechende Übersicht gegenüber Ihrem Kunden darstellen können.

**Hinweis:**

Diese Auswertung ist eine allgemeine Sicht auf die Lieferungen der im Rahmenauftrag enthaltenen Artikel im Zeitraum. Es ist auf keinen Fall eine Sicht für die tatsächliche Erfüllung eines einzelnen Rahmenauftrags.

Allerdings wird die Rahmenerfüllung gerne für allgemeine Betrachtungen für die Produktion / Lieferung Kundenspezifischer Artikel verwendet.

Unter Lieferungen werden alle Lagerabbuchungen summiert, mit Ausnahme der Handlagerbuchungen.

Beachten Sie auch den [Tipp zur Rollierenden Planung](#Tipp Rollierende Planung).

#### Welche Warenbewegungen werden unter geliefert in der Rahmenerfüllung berücksichtigt?
Unter geliefert werden alle Warenentnahmen berücksichtigt. D.h. alle Entnahmen (auch negative) auf Lieferscheine, Rechnungen, Lose. Losablieferungen, also die Rückmeldungen aus der Fertigung, werden eigenständig angezeigt, damit Sie auch sehen, welche Stückzahlen Sie für Ihren Kunden produziert, aber nicht geliefert haben.

#### Tätigkeitsstatistik
Mit dieser Auswertung, die Sie unter Journal Tätigkeitsstatistik finden, erhalten Sie eine Gegenüberstellung der aufgrund der Stücklisten geplanten Tätigkeiten zu den auf den Aufträgen tatsächlich gebuchten Tätigkeiten.

Der eingegebene Zeitraum bezieht sich auf das Erledigungsdatum des jeweiligen Auftrages.

#### Warum stimmt die Tätigkeitsstatistik nicht mit der Arbeitszeitstatistik überein?
Ja das ist so richtig. Warum ?
In der Tätigkeitsstatistik wird das Erledigungsdatum der Aufträge herangezogen.
In der Arbeitszeitstatistik greift das Datum jedoch auch die Buchungsdatum der Bewegungen zu. Dadurch ergibt sich zwangsläufig, dass es zu Unterschieden kommt, da auf einem im November erledigten Auftrag durchaus auch Buchungen aus dem Oktober entfallen können.

#### Können mehrere Aufträge mit gleichen Inhalten und unterschiedlichen Lieferterminen angelegt werden?
Verwenden Sie dazu den Button ![](mehrere_Auftraege_mit_unterschiedlichen_Lieferterminen_anlegen.gif) Aufträge aus bestehendem mit mehreren Lieferterminen.
Nach Klick auf den Button wählen Sie den Auftrag aus, den Sie kopieren möchten.
Nun erscheint die Frage nach dem Liefertermin für den ersten Auftrag.
Mit Ok bestätigen Sie den gewählten Liefertermin, womit der Auftrag angelegt wird. Daran anschließen erscheint die erneute Frage nach dem Liefertermin. Geben Sie hier einen neuen Liefertermin ein und bestätigen Sie wiederum mit ok, oder beenden Sie die Erfassung mit Abbrechen.

<a name="Auftragstermin verschieben"></a>

#### Auftragstermin verschieben
Um Auftragstermine zu verschieben, steht eine zweigeteilte Funktion zur Verfügung.
Sie finden diese im Auftrag unter Bearbeiten, Termin verschieben.<br>
Das bedeutet:<br>
a.) werden nur die Termine **eines** Auftrags verschoben, so werden die anhängenden Lose mit verschoben. Es wird damit der Liefertermin des Auftrags um die Anzahl der sich aus der Liefertermin-Differenz ergebenden Tage nach vor oder auch zurück verschoben. Alle an dem Aufträge hängenden Lose werden in ihren Terminen (Beginn und Ende) ebenfalls um die Anzahl der angegebenen Tage verschoben. Ebenfalls werden die Reservierungen, Fehlmengen, geplanten Ablieferungen der Lose und des Auftrags mit verschoben. Durch die relative Speicherung der Arbeitsgang-Beginn-Termine wird auch der geplante Beginn der Arbeitsgänge mit verschoben.
![](Termin_verschieben.gif)
Bitte beachten Sie dazu auch die reine Verschiebung der [Lostermine]( {{<relref "/fertigung/losverwaltung/#kann-der-lostermin-nachtr%c3%a4glich-ge%c3%a4ndert-werden" >}} )

**Info:** Sollten, nachträglich Termine nur im Auftrag verschoben werden, so ändern Sie bitte die Termine nur im Auftragskopf bzw. die Positionstermine.

b.) sollten die Termine mehrerer Aufträge verschoben werden, so wählen Sie mehrere Aufträge und klicken auf den Button Aufträge und wählen die gewünschten Aufträge mittels multiselekt und dem grünen Haken aus.
![](mehrere_Auftraege_Termin_verschieben.gif)
Damit wird bei den ausgewählten Aufträgen der Liefertermin und der Finaltermin entsprechend gesetzt. Alle Positionstermine werden ebenfalls auf den Liefertermin gesetzt.
**ACHTUNG:** ev. mit dem jeweiligen Auftrag verbundenen Lose werden **nicht mitverschoben**.

#### Auftragspositionstermin mit Losterminen verschieben
Ab der Version 0.2.9 steht auch das kombinierte Verschieben der Auftrags-Positionstermine in Verbindung mit den Losterminen zur Verfügung.<br>
Wähle dazu im Reiter Positionen die gewünschte Position des Auftrags und klicke dann auf das Termine verschieben Icon ![](Auftragspositionstermin_verschieben_1.png)

Im nachfolgenden Dialog<br>
![](Auftragspositionstermin_verschieben_2.png)<br>
kannst du nun die an der Produktion dieser Position beteiligten Los-Termine mit dem Auftragstermin mitverschieben. Hast du den Parameter LOS_MATERIAL_ZIELTERMIN_ANZEIGEN aktiviert, so muss auch der Material Vollständig Termin mit angegeben werden.<br>
Hier ist zusätzlich eine Prüfung der Termine mit eingebaut um entsprechend mögliche/realistische Termine zu erreichen.<br>
Solltest du in Sonderfällen diese Dinge übersteuern müssen, so muss dies direkt im Los durchgeführt werden.

#### Bei der Übernahme eines Angebotes in den Auftrag werden die Texte der Konditionen nicht mitkopiert.
Ja. Das ist Absicht. Da in einer Auftragsbestätigung in aller Regel andere Kopf- und Fußtexte angeführt werden, als im Angebot werden diese Texte nicht mit übernommen.

#### Kann ein Auftrag auch versteckt werden?
Ja. Diese Funktion wurde speziell für Wartungsaufträge eingerichtet und wird mit dem Parameter AUFTRAEGE_KOENNEN_VERSTECKT_WERDEN=1 aktiviert. Diese Wartungsaufträge sind in der Regel wiederholende Aufträge bei denen Wartungsarbeiten zu bestimmten Zeiträumen durchgeführt werden müssen. Um nun steuern zu können, dass diese langfristig offenen Aufträge nur dann sichtbar sind, wenn sie tatsächlich bearbeitet werden, haben wir die Möglichkeit geschaffen, dass Aufträge versteckt werden können.

Die Umschaltung von Versteckt auf nicht versteckt kann jederzeit bei jedem Auftrag durchgeführt werden. Klicken Sie dazu in den Kopfdaten einfach auf ![](Auftrag_verstecken.gif) verstecken.

Ob ein Auftrag bereits versteckt ist sehen Sie unten. D.h. es wird links von Pönale (Vertragsstrafe) ![](Auftrag_versteckt.gif) Versteckt angezeigt, wenn der Auftrag versteckt ist.

Um nun versteckte Aufträge in der Auswahlliste zu sehen wurde zusätzlich, wie z.B. im Artikel auch, der Direktfilter ![](Versteckte_Auftraege_anzeigen.gif) hinzugefügt. Ob Sie diesen Filter zur Verfügung haben hängt von Ihren Rollenberechtigungen ab.

Hinweis:

In der Quickzeiterfassung und in den Terminals werden versteckte Aufträge nicht angezeigt.

#### Kann die Funktion des Auftrag-Verstecken deaktiviert werden?
Ja, stellen Sie dazu den Parameter AUFTRAEGE_KOENNEN_VERSTECKT_WERDEN auf 0.

#### Wie kann ich reine Kostenpositionen im Auftrag anführen?
Verwenden Sie dazu [kalkulatorische Artikel]( {{<relref "/docs/stammdaten/artikel/#was-ist-ein-kalkulatorischer-artikel-" >}} ).

#### Ich habe einen Artikel im Auftrag und bekomme keine Position?
Wenn die Meldung Es sind keine Positionen für diesen Auftrag vorhanden
 ![](keine_Positionen.jpg)
erscheint (das gilt Sinngemäß auch für Angebote), aber im Auftrag Artikel-Positionen enthalten sind, so sind diese Artikel als kalkulatorische Artikel gekennzeichnet.
Um die Auftragsbestätigung drucken zu können, entfernen Sie bitte im Artikel-Detail den Haken bei Kalkulatorisch ![](kalkulatorisch.jpg) bzw. wählen Sie einen anderen Artikel.

#### Mit Zusammenfassung
[Siehe dazu]( {{<relref "/verkauf/angebot/#mit-zusammenfassung" >}} )

#### Planauftrag
Es gibt immer wieder die Aufgabenstellung, dass Plandaten für die Zukunft dargestellt werden müssen.
Hier geht es meistens darum, welche Volumen sind, z.B. für das nächste Jahr, mit den Lieferanten zu verhandeln, welcher Finanzbedarf besteht und ähnliches.
Für diese Betrachtung kann z.B. der Punkt Wiederbeschaffung im Modul Auftrag, sortiert nach Lieferanten verwendet werden.
D.h. legen Sie einen Rahmenauftrag an, dieser wirkt nicht in den Reservierungen, und tragen Sie die geplanten Stücklisten und Artikel in diesen Rahmenauftrag ein. Nun starten Sie mit Info, Wiederbeschaffung, sortiert nach Lieferanten, die Berechnung der Stücklistenauflösung mit Lieferantendaten. Bei den Lieferanten wird für jeden Artikel der bevorzugte Lieferant des Artikels verwendet.
Da ein stornierter Auftrag jederzeit wieder entstorniert werden kann, kann es durchaus von Vorteil sein, diesen Planauftrag nur für die Dauer der Berechnung zu aktivieren und danach wieder zu stornieren. **Hinweis:** Wenn es auch um Kapazitätsbedarfe geht, so sollte dafür die Stücklistenverwaltung mit Gesamtkalkulation verwendet werden. D.h. tragen Sie in eine (temporäre) Stückliste den benötigten/geplanten Jahresbedarf ein und errechnen Sie mit der Gesamtkalkulation, aus dem Menüpunkt Info, den Kapazitätsbedarf.

#### Sollzeiten und Sollzeitüberwachung im Auftrag
Damit die Sollzeitüberwachung im Auftrag Sinn macht (Parameter: Sollzeitpruefung), müssen im Auftrag auch Sollzeiten eingegeben sein. Manchmal ist es erwünscht, dass diese geplanten Zeiten nicht auf dem eigentlichen Auftragsformular ausgedruckt werden. Geben Sie in diesem Falle Arbeitszeiten mit einem Verkaufspreis von 0,00 ein und unterdrücken gegebenenfalls diese Artikel im Ausdruck der Kunden-Auftragsbestätigung.

#### Wo sehe ich wie ein Auftrag geliefert / verrechnet wurde?
Ein Auftrag wird durch Lieferscheine und oder Rechnungen erfüllt.
Damit ist der Auftrag für sich erledigt, d.h. alle Positionen wurden geliefert oder direkt verrechnet.
Zusätzlich ist in der Auswahlliste im Modul Auftrag der Status ersichtlich.
Grüner Haken = vollständig erledigt
grauer Haken = teil geliefert
usw.

Umgekehrt gibt es in den Lieferscheinen die gleiche Funktionalität, dass nur die offenen, also noch nicht verrechneten angezeigt werden. D.h. es muss lediglich sichergestellt werden, dass alle tatsächlich verrechenbaren Lieferscheine auch verrechnet sind und somit in der Liste der offenen Lieferscheine nicht mehr aufscheinen.

#### Was wird in der Sicht Lieferstatus angezeigt?
Im Reiter Lieferstatus wird die Ausliefersituation für die Auftragspositionen aus Sicht der Lieferscheine dargestellt. 

![](sicht_lieferstatus.JPG)

So erhalten Sie einen schnellen Überblick über die gelieferten, erledigten und noch im Auftrag offenen Positionen.

D.h. es werden alle Lieferscheine mit einem Bezug zum Auftrag angezeigt. Hat die jeweilige Lieferscheinposition einen Bezug zu einer der Auftragspositionen so wird diese Auftragsposition mit der Gesamtmenge der auf die Auftragsposition gelieferten Lieferscheinmengen angezeigt. Um die Lieferscheinpositionen der einzelnen Zeile anzuzeigen wechseln Sie bitte für die jeweilige Auftragsposition in den Reiter Auftragsposition Sicht Lieferschein.

Hinweis: Wurden z.B. einzelne Auftragspositionen manuell erledigt, ohne dass je eine Lieferscheinposition dafür erstellt wurde, scheint diese Auftragsposition auch nicht in der Auflistung auf.

Info: Im Reiter Auftragsposition Sicht Lieferschein wird im untenstehenden Detail auch der Status der Rechnung mit angezeigt. Mit dem GoTo Button können Sie wiederum um in den jeweils betroffenen Beleg Lieferschein bzw. Rechnung springen.

#### Wo erhalte ich eine Gesamtübersicht über den Auftrag?
Im Auftrag gibt es auch den Reiter Lieferschein / Rechnung.
Hier ist finden Sie eine Übersicht über die Verkaufsbelege die direkt oder indirekt mit dem Auftrag verbunden sind, wie z.B. in der nachfolgenden Darstellung.
![](Auftrag_Gesamtuebersicht.gif)
Diese Darstellung ist wie folgt zu lesen:
Direkt mit dem Auftrag ist der LS (Lieferschein) 12/0002864 verbunden. 
Mit dem Lieferschein ist die Proformarechnung 12/000208 und die RE (Ausgangsrechnung) 12/0002344 verbunden.
Hier gibt es noch die weitere Variante, dass für eine Ausgangsrechnung eine GS (Gutschrift) ausgestellt wurde. Diese wird/werden in der dritten Spalte angezeigt.

Hinweis: Hier werden nur die direkt dem Auftrag zugewiesenen Rechnungen, Lieferscheine angezeigt. Somit werden für Rahmenaufträge keine Belege dargestellt.
Für eine komfortable Übersicht nutzen Sie bitte auch die Rahmenübersicht aus dem Menü Info.

#### Wenn ich eine Auftragsposition löschen will, so erscheint eine Fehlermeldung?
Wenn auf der Auftragsposition keine Lieferungen per Lieferschein oder Rechnung durchgeführt wurde, so ist vermutlich die Auftragsposition mit dem Los verknüpft. Um diese Verknüpfung zu lösen [siehe bitte]( {{<relref "/fertigung/losverwaltung/#worauf-ist-beim-storno-eines-loses-zu-achten" >}} ).

#### In der Auftragsauswahlliste werden die Aufträge in verschiedenen Farben angezeigt. Was bedeutet das?
Damit werden folgende Informationen angezeigt:
| Farbe | Bedeutung |
| --- |  --- |
| Rot | Bei dem Auftrag ist Pönale (Vertragsstrafe) gesetzt. |
| Blau | Für diesen Auftrag wurden bereits Eingangsrechnungsverteilungen vorgenommen |
| Violett | beide Punkte treffen zu, also Pönale und Eingangsrechnungszuordnung |

#### Einen Auftrag als jetzt verrechenbar kennzeichnen?
Es ist immer wieder praktisch, den Kollegen, Mitarbeitern zu signalisieren, dass ein Auftrag jetzt abgerechnet werden kann. Dafür haben wir die Funktion ![](Auftrag_verrechenbar.gif) verrechenbar geschaffen. Ein Klick auf den Rechner markiert den Auftrag, welcher im Status offen oder teilerledigt sein muss als verrechenbar. Haben Sie den falschen Auftrag ausgewählt, so setzt ein erneuter Klick auf verrechenbar den Status wieder zurück. Um dies auch in der Auswahlliste anzuzeigen schalten Sie bitte den Parameter ZUSATZSTATUS_VERRECHENBAR">ZUSATZSTATUS_VERRECHENBAR auf 1\. Damit wird anstelle der RoHs Information der Verrechenbarstatus angezeigt. Diese Information steht auch in den verschiedenen Journalen der offenen Aufträge zur Verfügung.

#### Einen Auftrag grundsätzlich als verrechenbar markieren
Wenn Aufträge auch für die Steuerung von z.B. Garantieabwicklungen oder ähnlichem verwendet werden, ist es oft sinnvoll, dass diese bei beim Anlegen, bei der Aktivierung bereits entsprechend gekennzeichnet werden. Dafür steht die Definition in den Auftragskonditionen ob verrechenbar oder nicht verrechenbar zur Verfügung. Da es hier oft auch mehrere Gründe gibt, warum insbesondere ein Auftrag nicht verrechnet werden sollte, können diese unter Auftrag, Grunddaten, Reiter verrechenbar definiert werden.
Ist ein Auftrag nicht verrechenbar, so wird er, wenn keine anderen Farbbedingungen zutreffen, in der Auftragsauswahl in grau dargestellt.
Die Info des Nicht verrechenbar wird in den Lieferschein weitergereicht, jedoch ohne der Begründung.

#### Die Auftragszeile wird in grau dargestellt?
Ein Auftrag wird in der Auswahlliste dann in hell-grau ![](nicht_verrechenbar.gif) dargestellt, wenn er als nicht verrechenbar gekennzeichnet ist. Dies wird in den Konditionen des Auftrags eingestellt. Es können in den Grunddaten unter Verrechenbar mehrere Gründe, warum dieser Auftrag (nicht-) verrechenbar ist eingerichtet werden, sodass für die verrechnende Stelle jederzeit ersichtlich ist, warum der Auftrag nicht verrechnet werden sollte.

#### Kann der Auftragswert als gesamtes übersteuert werden?
<a name="Auftragswert manuell übersteuern"></a>
Es kommt immer wieder vor, dass für einen Auftrag ein pauschaler Betrag vereinbart wurde. Um nun nicht mühsam jede einzelne Position auf den Wert hinzurechnen, wurde die Erfassung des manuellen Auftragswertes geschaffen. Sie finden dieses Feld im Reiter Konditionen des Auftrags. Wird beim manuellen Auftragswert, welcher in der Währung des Auftrages betrachtet wird, ein Betrag eingegeben, so wird versucht den Korrekturwert mit dem Versteckten Aufschlag für jede Werthaltige Zeile des Auftrages abzubilden. Da sich trotzdem oft kleine Differenzen ergeben, wird zusätzlich eine sogenannte Pauschalkorrektur gebildet und beim Auftrag hinterlegt und auch als eigene Zeile in der Auftragsbestätigung mit ausgedruckt.
Bitte beachten Sie:
- die Pauschalkorrektur wird, wenn der Auftrag geändert wird wieder auf ungültig gesetzt
- werden zusätzliche Positionen hinzugefügt, so löschen diese zwar die Pauschalkorrektur, verändern aber den versteckten Aufschlag nicht. D.h. gegebenenfalls ist der nun neue Auftragswert erneut einzugeben.

#### Kann die Null-Preis Abfrage unterdrückt werden?
Wenn bei einer Auftragsposition als Preis z.B. 0,00€ eingegeben wird, erscheint der Hinweis, ob dieser Wert übernommen werden soll.  Der Null-Preis Dialog ist durch das anhaken der Checkbox unterdrückbar. Ist dieser Haken gesetzt, so erfolgt solange das Modul geöffnet ist keine weitere Abfrage zum Null-Preis.

![](nullpreis_abfrage.png)

#### Kann man einen bestehenden Auftrag kopieren und bearbeiten?
Ja, die Daten für einen neuen Auftrag können aus einem bestehenden Auftrag verwendet werden. Verwenden Sie dazu den Button ![](Auftrag_kopieren.gif) Auftrag kopieren.

Auch beim Erstellen von Abrufaufträgen für bestehende Rahmenaufträge hilft das Übernehmen der bestehenden Daten.

Wählen Sie dazu einen schon bestehenden Abrufauftrag und kopieren diesen -- nun werden die Daten (Kopfdaten und Positionen mit Rahmenauftragsbezug) übernommen.

#### Können Etiketten zu einzelnen Auftragspositionen gedruckt werden?
Ja, verwenden Sie dazu das Icon ![](auftragsposition_etikett.JPG) Etikett - zur Anpassung des Ausdrucks nach den Bedürfnissen Ihres Unternehmens, kontaktieren Sie bitte Ihren **Kieselstein ERP** Betreuuer.

#### Auftragsjournale
Für die Aufträge stehen praktische Journale zur Verfügung.

Journal offene Details. Hier steht auch die Auswertung mit Lagerstandsdetailbetrachtung zur Verfügung. Wird dies angehakt, so wird für jede offene Auftragsposition die eine Stückliste ist, das Feld ![](Lagerdetailauswertung.gif) in Lagerstandsdetailauswertung berücksichtigen ausgewertet (Siehe Stücklisten Kopfdaten). Für die Stücklisten bei denen dies angehakt ist, werden auch die Unterpositionen mit in die Auswertung aufgenommen. Hier wird zusätzlich noch eine Verbrauchsberechnung je Position nach Lieferterminen und Lagertypen vorgenommen. Dabei wird wie folgt vorgegangen.

Zum Beginn des Artikels werden die Lagerstände aus dem Artikellager je nach Lagerart übernommen. Für jeden weiteren Eintrag wird der Lagerstand in der Reihenfolge der Lagerarten und dann nach der Sortierung der Lager verbraucht.

Die Reihenfolge der Lagerarten für den Verbrauch ist wie folgt:
-   Hauptlager
-   Normallager
-   Lieferantenlager
-   Halbfertiglager
Das Ergebnis dieser Berechnung könnte z.B. wie folgt aussehen:

![](offeneDetailsMitLagerstandsdetails.gif)

#### Wie können Auftragspositionen schnell erfasst werden?
Hierzu gibt es die Auftrags-Schnellerfassung mittels Barcodescanner: Nach Klick auf das Scannersymbol ![](barcode_schnellerfassung_positionen.PNG) geben die Artikelnummer und Menge ein und speichern mittels Enter-Taste.

#### Wenn ich Ware vom Kunden zurückerhalte, wie kann ich diese in meinen Auftrag so einbuchen, dass der offene Auftragsstand wiederum richtig ist?
Es kommt leider immer wieder vor, dass Ware aus einem Auftrag an einen Kunden geliefert wird und diese wird dann, z.B. wegen Mängel, wieder zurück gesandt. Wie ist nun, neben der [Reklamationserfassung]( {{<relref "/fertigung/reklamation" >}} ) in der Materialwirtschaft damit umzugehen, sodass der offene Auftragsstand wiederum den Tatsachen entspricht und gegebenfalls ein neues Los, eine neue Bestellung ausgelöst wird.
Der Grundgedanke ist, dass es ausreicht einen auftragsbezogenen Lieferschein zu erstellen und in diesem MIT Auftragsbezug eine negative Liefermenge, mit dem Lager Schrottlager bzw. Sperrlager zu buchen. Je nach Auftragsstatus sind dabei folgende Punkte zu beachten:

-   Ist der Auftrag bereits vollständig erledigt und Sie wollen wirklich den Stand dieses Auftrags zurücknehmen, so wählen Sie den Auftrag aus (der Haken bei nur offene muss entfernt sein) und setzen ihn in den Status teilerledigt (Bearbeiten, Erledigung aufheben)

-   Legen Sie nun einen neuen Auftragsbezogenen Lieferschein für diesen Auftrag an (Klick auf das blaue Auftragsbuch in der Lieferschein-Auswahlliste und Auswahl des teilerledigten Auftrags)

-   Wechseln Sie nun in den oberen Modulreiter Sicht Auftrag und entfernen Sie gegebenenfalls den Haken bei nur offene anzeigen. Damit werden alle Positionen des Auftrags dargestellt.

-   Wählen Sie nun die Position aus, die Sie von Ihrem Kunden zurückerhalten haben

-   Klicken Sie auf ändern und geben Sie bitte eine negative Menge für die zurückgenommenen Artikel an.<br>
Wählen Sie gegebenenfalls als Ziellager ein Lager, welches vom internen Bestellvorschlag, bzw. vom Bestellvorschlag NICHT mitgerechnet wird. In der Regel wird das ein Sperr- bzw. ein Schrottlager sein. Definition von Lägern siehe dort.

Damit haben Sie die offene Menge des Auftrages um die zurückgenommene Menge erhöht. So wird, je nach Verfügbarkeit wiederum ein entsprechender Bedarf dargestellt.

## Auftragsfreigabe

Durch setzen des Parameter AUFTRAGSFREIGABE = 1 kann die Auftragsfreigabe aktiviert werden.
Damit wird erreicht dass:
-   ein neu angelegter Auftrag grundsätzlich nicht freigegeben ist
-   nur Berechtigte, siehe Recht AUFT_DARF_AUFTRAEGE_FREIGEBEN dürfen Aufträge freigeben und diese auch wieder zurücknehmen.
-   Bei der Erzeugung des Bestellvorschlages bzw. internen Bestellung werden nicht freigegebene Aufträge NICHT berücksichtigt.<br>
Dies gilt auch für die Berechnungen der verfügbaren Menge zum jeweiligen Stichtag.
-   Aufträge die nicht freigegeben sind können trotzdem ausgedruckt und auch an den Kunden versandt werden. In aller Regel wird hier eine Info, Achtung Auftrag nicht freigegeben o.ä. mit angedruckt werden. Der Status eines nicht freigegebenen Auftrags ändert sich nicht, d.h. er bleibt immer im Status angelegt. Trotzdem wird der Versand in der Dokumentenablage abgelegt.
-   Die Freigabe erfolgt durch Klick auf den ![](Auftrags_Freigabe1.gif) Haken in den Auftrags-Kopfdaten. Wird ein freigegebener Auftrag ausgedruckt so ändert er seinen Status auf offen. Die Rücknahme eines bereits freigegebenen Auftrags erfordert ebenfalls das Auftragsfreigabe-Recht (AUFT_DARF_AUFTRAEGE_FREIGEBEN ). Hat der Benutzer dieses Recht nicht, erscheint nur ein Hinweis, dass der Auftrag bereits freigegeben ist und daher nicht mehr verändert werden darf.
-   Wann und von wem ein Auftrag freigegeben wurde, wird in den Kopfdaten direkt neben dem Freigabehaken ![](Auftrags_Freigabe2.gif) inkl. dem Kurzzeichen des freigebenden Benutzers angezeigt .

#### Kann im Auftrag auch nach dem Titel des zugewiesenen Projektes gesucht werden ?
Wenn Sie das Modul Projektklammer besitzen und der Parameter PROJEKT_TITEL_IN_AG_AB_PROJEKT auf 1 gestellt ist, so wird die Anzeige in der Auswahlliste der Aufträge vorne um den Projekttitel aus dem direkt oder indirekt (über das Angebot aus dem der Auftrag erzeugt wurde) angezeigt. Die Trennung zum Projekt des Auftrags bzw. zur Bestellnummer erfolgt durch das Pipe Symbo ( | ). In der Suche nach den Projekten (aus dem Auftrag) wird dann zusätzlich auch der Inhalt des Projekttitels herangezogen.

#### Unterkostenstellen?
In manchen Unternehmen ist es erforderlich, dass ein Auftrag mehreren Abteilungen / Kostenstellen zugewiesen wird. Dies wird vor allem für die Auftragsplanung benötigt.
Mit dem zusätzlichen Modul Unterkostenstellen kann diese Zuordnung getroffen werden.
Wie üblich wird in den Auftrags-Kopfdaten die Hauptkostenstelle definiert. Im zusätzlichen Reiter Unterkostenstellen können weitere Kostenstellen für diesen Auftrag definiert werden. Diese werden, zur leichteren Orientierung, auch in den Kopfdaten angezeigt.

Die Unterkostenstellen wirken derzeit NUR im Journal offene Aufträge ohne Details. D.h. der Haken bei Mit Details darf nicht gesetzt sein. Wird nun entweder eine Kostenstelle ausgewählt oder Sortierung nach Kostenstelle angehakt, so wird jeder Auftrag mit seiner Kostenstelle **und** mit seiner Unterkostenstelle angeführt.
**Info:** Die Werte des jeweiligen Unterkostenstellen-Auftrags werden zwar auch bei den Unterkostenstellen mit angedruckt. In den Summen werden diese aber NICHT mitgerechnet.
Dass es sich bei dem Auftrag um eine Unterkostenstellenzuordnung handelt wird vor der Kostenstelle durch das Wort Unter angezeigt und die Werte werden in Hellgrau dargestellt.

<a name="Zeitbestätigung"></a>

#### Zeitbestätigung
<!-- Insbesondere in Verbindung mit der [HVMA2 Offline App](../HVMA/index.htm#HVMA2 Offfline), steht die Funktion der [Zeitbestätigung](../HVMA/index.htm#Auftragszeiten unterschreiben) zur Verfügung. -->
Der Zweck ist, dass, gerade auf Montagen / Baustellen usw. die Anwesenheitszeiten Ihrer MitarbeiterInnen von Ihrem Kunden möglichst zeitnah unterschrieben werden sollten. D.h. es werden vom Mitarbeiter die Zeiten mit der Offline App, im ToDo erfasst und mittels Datensynchronisatation übertragen. Durch die Anforderung der Zeitbestätigung für einen spezifischen Auftrag, wird der aktuelle Inhalt dieses Formulars an die ToDo Liste übertragen und kann so dem Kunden-Verantwortlichen präsentiert werden.
Nun wird auf diesem Formular vom Kundenverantwortlichen direkt auf Ihrem mobilen Gerät unterschrieben, damit werden diese "Bilder" bei der nächsten Synchronisierung in die Zeitbestätigung zum Auftrag hinterlegt (siehe) und daran anschließen ein Bestätigungsemail an den Ansprechpartner des Auftrags (der Auftraggeberadresse) mit dem unterschriebenen Beleg und den PDF Rohdaten (welche dem Kundenverantwortlichen präsentiert wurden) übersandt.
Bitte beachten Sie, dass das Formular auch in der gewünschten Sprache erstellt werden kann, es muss dafür lediglich die richtige Kommunikationssprache beim Auftragskunden eingestellt und das Formular in der entsprechenden Sprache vorhanden sein.
Im **Kieselstein ERP** Client finden Sie dieses Formular im Modul Auftrag unter Info, Zeitbestätigung.
Für die Übermittlung an die HVMA werden die Daten OHNE den bereits unterschriebenen Zeiten für den angemeldeten Benutzer verwendet.
Bitte beachten Sie, dass dieser Report in zwei Ausführungen zur Verfügung steht. Einer detailliert als PDF mit Seitenumbrüchen usw. und einer der für die Unterschrift optimiert ist, nur sehr wenige Daten anzeigt und ohne Seitenumbrüche ausgeführt ist.
Zur eindeutigen Kennzeichnung der Zeitbestätigung wird diese mit einer laufenden Nummer versehen, welche bei jedem Ausdruck generiert wird. Beide Belege, der detaillierte PDF Beleg und der Unterschriftsbeleg, haben nur bei dem Aufruf aus der HVMA2 die gleiche laufende Nummer.

#### Woher sehe ich von welchem Lager sich ein Auftrag bedient?
Mit dem Parameter ABBUCHUNGSLAGER_IN_AUSWAHLLISTE kann aktiviert werden, dass in der Auswahlliste der Aufträge auch das geplante Abbuchungslager jedes einzelnen Auftrages angezeigt werden kann.

#### Die Auswahlliste der Aufträge nach Terminen sortiert
In der Auswahlliste der Aufträge wird auch der Liefertermin mit angezeigt. Dieser Termin ist üblicherweise der Liefertermin aus den Kopfdaten des jeweiligen Auftrags. Sind nun in einem Auftrag mehrere Positionen mit unterschiedlichen Lieferterminen enthalten, so wünscht man sich oft, dass der jeweils nächste fällige Termin für diese Liste dargestellt wird. Aktivieren Sie dafür den Parameter AUFTRAGSPOSITIONSTERMIN_IN_AUSWAHL_ANZEIGEN. Damit wird der früheste Positionen der noch nicht erledigten Positionen angezeigt. Auch nach dieser Spalte kann sortiert werden.

#### Wie kann ich eine Auftragsposition aufteilen?
Es kann vorkommen, dass es nötig ist bestehende Auftragspositionen splitten zu müssen, da es z.B. zu Teilmengen mit unterschiedlichen Lieferterminen kommen kann. Dabei kann die Funktion "Aufteilen der Auftragsposition" verwendet werden. Sie funktioniert analog zum [Aufteilen von Bestellpositionen]( {{<relref "/einkauf/bestellung/#wie-kann-ich-mengen-teilungen--mengen-splitt-des-lieferanten-in-meinen-bestellungen-abbilden" >}} ).

#### Alle oder nur markierte Positionen aus Sicht Auftrag übernehmen
Manchmal will man bewusst die Flexibilität in der Anwendung einschränken.<br>
So kann mit dem Parameter SICHT_AUFTRAG_MEHRFACHAUSWAHL gesteuert werden, ob im Modul Lieferschein bzw. Rechnung die Frage ob alle oder nur markierte Positionen übernommen werden sollten deaktiviert werden. Ist der Parameter deaktiviert, so werden immer alle Positionen, welche noch offen sind in den Reiter Positionen übernommen. Selbstverständlich nur für die lagernden Mengen.

#### Kunden: VK-Preis kommt aus LS Datum
Bedeutet: In LS/RE-Sicht Auftrag wird, wenn für die Rechnungsadresse die CheckBox angehakt ist, der zum LS/RE-Belegdatum aktuelle VK-Preis vorgeschlagen und nicht der Preis aus dem Auftrag übernommen.<br>
Wenn man über das blaue Buch alle offenen Positionen liefert, wird ebenfalls der zum LS/RE-Belegdatum aktuelle VK-Preis verwendet