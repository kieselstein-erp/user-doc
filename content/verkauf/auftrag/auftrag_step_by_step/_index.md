---
title: "Auftrag Step by Step"
linkTitle: "Step by Step"
categories: ["Auftrag"]
tags: ["Auftrag Step by Step"]
weight: 300
description: >
  Step by Step
---
Auftrag - Step by Step
======================

Hier finden Sie eine Sammlung von Step by Step zu den verschiedensten Themen rund um den Auftrag.
Es wird dieses Kapitel laufend ergänzt um so eine How To Sammlung zur Verfügung zu stellen.

Ändern eines Auftrages bzw. einer Position im Auftrag
-----------------------------------------------------

Die Frage lautete:
wie kann ich einen Auftrag wieder ändern
- Positionen - Menge ändern
- Positionen - Aufspalten (in 2 Mengen teilen )
- neue Artikel(Versionen) anstatt der alten Artikel(Versionen) austragen

Ob eine Position eines Auftrags geändert werden kann hängt vom Status des Auftrages ab.
Grundsätzlich kann ein Auftrag nur geändert werden, wenn er im Status angelegt ist.
Wichtig: Selbst ein vollständig erledigter Auftrag kann wieder in den Status angelegt gebracht werden, aber: weniger als schon auf die Auftragsposition geliefert wurde, kann in der Auftragsmenge nicht enthalten sein.
Beschreibung der Vorgehensweise anhand eines vollständig erledigten Auftrages:

-   Auftrag in der Auswahlliste auswählen. Dazu den Haken bei nur offene entfernen.
    Menüpunkt Bearbeiten, Erledigung aufheben -> Frage mit Ja beantworten. Je nach Liefersituation ist nun der Auftrag im Status offen oder teilerledigt

-   Den teilerledigten Auftrag (die mit dem grauen Haken) nach offen bringen
    Auftrag auswählen, Reiter Kopfdaten, auf den Ändern Knopf klicken (den Stift) Frage mit Ja beantworten -> Auftrag ist nun im Status offen

-   Den offenen Auftrag bearbeiten und damit in den Status angelegt bringen.
    Entweder im Reiter Kopfdaten und Klick auf den Ändern Knopf und die Frage mit Ja oder Änderungsauftrag beantworten oder die gleiche Funktion im Reiter Positionen.

-   Mengen ändern, Positionen löschen, Positionen hinzufügen.

    -   Mengen können jederzeit geändert werden, jedoch ist die Mindestmenge diejenige die schon geliefert wurde. Einfach auf den Ändern Kopf klicken.
        Um die bereits gelieferten Mengen einzusehen, wählen Sie den Reiter Lieferstatus

    -   Preise können jederzeit geändert werden, gelten aber nur für in Zukunft ausgelieferte Positionen.
        Preise in Lieferscheinpositionen die aus dem Auftrag bereits übernommen wurden, werden absichtlich NICHT verändert.

    -   Positionstermine können jederzeit geändert werden.

    -   Neue Positionen hinzufügen. Einfach mit Neu oder auch mit Einfügen vor der Cursorposition

    -   Position löschen. Dies ist jederzeit möglich, solange die Position nicht geliefert wurde.

-   Spezialfall alte neue Version
    Oft ist es so, dass man die Version 0.0 bestätigt, dann aber wird das im Laufe der Zeit die Version 2.5
    D.h. es sollte der Auftrag auf die neue Version geändert werden. D.h. man hat von der alten Version z.B. bereits eine Teilmenge geliefert und in Zukunft sollten die Lieferungen in der nun freigegebenen neuen Version erfolgen.
    Vorgehensweise dazu:

    -   Alte Version: Die Menge soweit reduzieren, dass sie exakt der gelieferten Menge entspricht (siehe z.B. Menüpunkt Info, Auftragsübersicht oder Reiter Sicht Lieferstatus)

    -   Neue Version: Unter der Position der alten Version eine weitere Positionszeile mit dem Artikel der neuen Version einfügen und die Restmenge eintragen.<br>
**Hinweis:** In diesem Falle würde ich bei der Rückfrage des **Kieselstein ERP** ob der Auftrag geändert werden sollte, mit *dies ist ein Änderungsauftrag* antworten. Damit wird die Versionsnummer des Auftrages hochgezählt.

    -   Weiters gibt es die Möglichkeit, eine ergänzende Textzeile (Freigabe von Kunde erteilt am ....) davor oder danach hinzuzufügen.

    -   WICHTIG: Nach der durchgeführten Änderung muss der Auftrag wieder aktiviert werden.
        Entweder:

        -   Klassische Variante: Drucken und große Druckvorschau oder an den Kunden senden

        -   Schnelle Abkürzungsvariante, wenn KEIN Dokument benötigt wird. Im Reiter Positionen, mit der rechten Maustaste auf den Drucker klicken.

Zusätzlich: Den Liefertermin des gesamten Auftrages können Sie auch im Menüpunkt, Bearbeiten, Termine verschieben verändern. Die einzelnen Positionstermine können in den Positionen gepflegt werden.

Auftragsnachkalkulation stimmt mit Los-Nachkalkulation nicht überein
--------------------------------------------------------------------

Die Frage lautete:
Woher kommt die große Differenz zwischen Losnachkalkulation und Auftragsnachkalkulation

Hintergrund: Der Lieferschein mit dem die Produktion ausgeliefert worden war, hat die vollständigen Positionen zweier Lose ausgeliefert.
Da es aber schnell gehen musst, wurde der Lieferschein vor der eigentlichen Ablieferbuchung erstellt. Es war also das Belegdatum des Lieferscheines VOR der eigentlich Produktionsrückmeldung. Daher musste von **Kieselstein ERP** der aktuell bekannte und daher ev. alte Gestehungspreis verwendet werden.
Hierzu kann gerne auch der Hinweis gegeben werden, dass wenn bei der Auftragsnachkalkulation entsprechend Differenzen zwischen Gestehungspreis eines Lieferscheines und seinem Einstandspreis (der Wert in Klammer) angezeigt werden, wie z.B. in diesem Falle ![](Auftragsnachkalkulation_Gestehungspreisabweichung_zu_Einstandspreis.jpg) so muss, idealerweise über das Warenbewegungsjournal des Artikel die Herkunft des Gestehungspreises geprüft werden.

Stellt sich, so wie in diesem Falle, heraus, dass die tatsächliche Ablieferbuchung NACH der Auslieferung gemacht wurde, so muss entweder, das Auslieferdatum auf nach der Ablieferung ODER das Ablieferdatum vor oder zum Auslieferdatum gesetzt werden.
Meist wird es einfacher sein, das Ablieferdatum auf den gleichen Tag die das Lieferscheindatum zu setzen.