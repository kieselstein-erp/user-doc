---
title: "Auftrag"
linkTitle: "Termin mit Uhrzeit"
categories: ["Auftrag"]
tags: ["Termin in Stunden, Minuten"]
weight: 500
description: >
  Planung mit Uhrzeit, Stunden, Minuten, eventuell inkl. Rückgabe, z.B. für Veranstaltungen, Verleih u.ä.
---
Verleih / Auftragstermin mit Uhrzeit
====================================

Mit dieser Zusatzfunktion können die Auftragstermine mit Uhrzeit angegeben werden. Dies wird z.B. für das Verleihgeschäft / Catering benötigt, da hier die (Leih-)Ware exakt zum Termin bei der Veranstaltung sein muss.

Daraus ergibt sich, dass der Liefertermin und der Finaltermin in den Auftragskopfdaten mit Uhrzeit angegeben werden muss. Die Angabe der Uhrzeit steht damit auch in den einzelnen Positionsterminen zur Verfügung.

Dies bewirkt, dass in alle darauf aufbauenden Sichten wie Reservierungslisten, Bewegungsvorschau die Termine ebenfalls mit Uhrzeit angeführt werden.

Da im Verleih / Catering Geschäft fast immer der Fall gegeben ist, dass Ware geliefert / verliehen wird und dann die gleiche Ware zum Finaltermin wieder zurückgenommen werden soll, steht Ihnen in **Kieselstein ERP** ein entsprechender Automatismus für den automatischen Eintrag, die automatische Aktualisierung der Auftragspositionen zur Verfügung. Klicken Sie nach dem Erfassen der zu liefernden Positionen einfach auf den Knopf Rücknahmepositionen eintragen![](Ruecknahmepositionen_eintragen.gif). Dadurch werden alle eventuell bereits mit negativen Mengen eingetragenen Positionen aus dem Auftrag gelöscht und alle jene mengenbehafteten Positionen zum Finaltermin mit negativer Menge in den Auftrag am Ende dazugefügt, bei deren Artikelgruppe das Feld Rückgabe ![](Auftrag_Rueckgabe.gif) angehakt ist.

Die Packliste wurde so gestaltet, dass zuerst die positiven Mengen und dann die negativen Mengen aufgeführt werden. Dadurch kann die Packliste in zwei Blätter getrennt werden. Eines für die Lieferung und eines für die Rücknahme.

Die Lieferung der Positionen / Leihware an den Kunden erfolgt wie üblich mit einem auftragsbezogenen Lieferschein, in dem Sie die tatsächlich an den Kunden gelieferte Ware anklicken und die gelieferte Menge eintragen.

Die Rücknahme der Positionen erfolgt durch einen weiteren (Rücknahme-) Lieferschein, welchen Sie ebenfalls dem Auftrag zuordnen. Auch hier geben Sie die tatsächlich vom Kunden zurückerhaltene Ware durch Eingabe mit negativer Menge an.

Für die Verrechnung gehen Sie nun abhängig von Ihrer Gestaltung der Preislisten wie folgt vor:
1. Im Verleih- / Ausleih- Lieferschein ist (nur) der Verleihpreis enthalten.<br>
Übernehmen Sie den Verleihlieferschein in die Rechnung und setzen Sie den Rücknahme-Lieferschein auf erledigt. Eventuell vom Kunden nicht zurückgegebene Ware wird per Handeingabe verrechnet.
2. Im Verleih- / Auslieh- Lieferschein ist der Verkaufspreis enthalten.<br>
Im Rücknahme-Lieferschein ist der Differenzpreis zum Mietpreis enthalten. Übernehmen Sie beide Lieferscheine in die Rechnung. Eventuell vom Kunden nicht zurückgegebene Ware wird durch die Differenz der Mengen automatisch zum Verkaufspreis verrechnet.

#### Warum so eine komplizierte Buchung ?
Es ist aus unserer Sicht zwingend erforderlich, dass der Lagerstand zu jedem Zeitpunkt richtig ist. Daher muss die Ware für den Verleih vom Lager gebucht werden und bei Rückgabe eben wieder eingelagert werden.

#### Kann der Verleih auch anders verrechnet werden ?
<a name="Verleih"></a>
Ja. Es steht als zusätzliche Artikeleigenschaft der Verleih(Artikel) zur Verfügung.

Dieser bewirkt, dass bei einer direkten Übernahme von Auftragspositionen in die Rechnung, KEINE Lagerbuchung erfolgt. Hingegen bei einer Übernahme des Verleihartikels in einen Lieferschein erfolgt sehr wohl eine entsprechende Lagerabbuchung.

Damit wird einerseits die Forderung nach einer einfachen Verleihverrechnung erfüllt und andererseits trotzdem die Möglichkeit zur Verfügung gestellt, dass auch als Verleih gekennzeichnete Artikel verkauft werden.

Zusätzlich: Da beim Verleih der Mietpreis ja immer deutlich niedriger als der Mindestverkaufspreis ist, wird bei Verleihartikel keine Unterpreisprüfung vorgenommen.