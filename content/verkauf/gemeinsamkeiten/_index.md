---
categories: ["Bewegungsmodule"]
tags: ["Gemeinsamkeiten"]
title: "Bewegungsmodule Gemeinsamkeiten"
linkTitle: "Gemeinsamkeiten"
weight: 100
date: 2023-01-31
description: >
 Gemeinsamkeiten der Bewegungsmodule
---
Gemeinsamkeiten in den Bewegungsmodulen
=======================================

In **Kieselstein ERP** unterscheiden wir zwischen den Programmmodulen für die Stammdaten und den Programmmodulen für die Bewegungsmodule.

Um die Bedienung möglichst einfach zu gestalten, haben wir versucht einen Großteil der Module mit gleicher Bedienung auszustatten.

-   Stammdaten-Module sind: Partner, Banken, Kunden, Lieferanten, Personal, Artikel

-   Bewegungsdaten-Module werden zusätzlich gegliedert in Verkauf, Einkauf, Fertigung.

    -   Bewegungsdaten-Module des Verkaufs sind: Angebot, Angebotsstückliste, Auftrag, Lieferschein, Rechnung, Gutschrift

    -   Bewegungsdaten-Module des Einkaufs sind: Anfrage, Bestellung

    -   Bewegungsdaten-Module des Fertigung sind: Lose (Fertigungsaufträge), Zeitdaten, Reklamationen

-   Sonderfall Stückliste: Von der fachlichen Sicht sind Stücklisten Stammdaten, aus der Sicht der Bedienung sind Stücklisten wie Bewegungsdaten zu betrachten, da ja hinter einer Stückliste mehrere Positionen, welche die Stückliste definieren, hinterlegt sind.

[Elektronischer Datenaustausch, EDI, Desadv, CleverCure und ähnliches siehe bitte]( {{<relref "/verkauf/rechnung/edi">}} )

Bitte beachte die Aufbewahrungspflichten

### Positionsarten

In **Kieselstein ERP** stehen Ihnen je nach Modul verschiedene Positionsarten zur Verfügung. Da ein Großteil über alle Module gleich sind, werden sie hier zentral beschrieben.

**Info:** Sollten einzelne Positionsarten in verschiedenen Modulen nicht erwünscht sein, so können diese von Ihrem **Kieselstein ERP** Händler gerne abgeschalten werden.

**Hinweis zur Positionsart Position und zur Ermittlung der Mehrwertsteuer:**

Für die Berechnung der Mehrwertsteuer wird der Mehrwertsteuersatz der ersten Artikelposition nach der Positionsart Positionsbeginn verwendet. 

| Positionsart | Angebot | Angebots-stückliste | Auftrag | Lieferschein | Rechnung | Gutschrift | Anfrage | Bestellung | Stückliste | Fertigung | Reklamation |
| --- |  :-: |  :-: |  :-: |  :-: |  :-: |  :-: |  :-: |  :-: |  :-: |  :-: |  :-: |
| Ident | X | X | X | X | X | X | X | X | X | X | X |
| Handeingabe | X | X | X | X | X | X | X | X | X | X | X |
| Texteingabe | X |   | X | X | X | X | X | X |   |   |   |
| Textbaustein | X |   | X | X | X | X | X | X |   |   |   |
| Betrifft |  |  |  |  |  |  | X | X |  |  |  |
| Leerzeile | X |   | X | X | X | X | X | X |   |   |   |
| Seitenumbruch |   |   |   |   |   |   |  X |  X |   |   |   |
| Endsumme | X |  | X | X | X | X | X | X |  |  |  |
| Lieferschein |   |   |   |   | X |   |   |   |   |   |   |
| Ursprungsland |   |   |   |   | X |   |   |   |   |   |   |
| AGStückliste | X |   |   |   |   |   |   |   |   |   |   |
| IntelligenteZwischensumme | X |  | X | X | X |  |  |  |  |  |  |

#### Wie ist die Positionsart "AGStückliste" zu verstehen?
Die Positionsart "AGStückliste" dient der Verwendung von Angebotsstücklisten im Angebot. Damit haben Sie die einfache Möglichkeit "Verkaufsstücklisten" zu erstellen und diese, ohne Artikel- und Stücklistenverwaltung aufzublähen, in einem eigenen Modul zu organisieren. Auch bei den Angebotsstücklisten können die anzudruckenden Angebotsstücklistenpositionen definiert werden.

#### Wie ist die Positionsart "Lieferschein" zu verstehen?
Die Positionsart "Lieferschein" dient der Verwendung von Lieferscheinen in einer Rechnung. Durch die Angabe von mehreren Lieferscheinen kann so eine Sammelrechnung erzeugt werden.

#### Wie ist die Positionsart "Ursprungsland" zu verstehen?
Die Positionsart "Ursprungsland" dient dem Andruck des Ursprungslandes auf der Ausgangsrechnung. Erfolgt die Lieferung in ein Drittland, so wird auch der Text der Warenerklärung mitgedruckt, auf dem gegebenenfalls das Ursprungsland mit angedruckt werden muss.

Hinweis: Der Text der Warenerklärung muss bei jeder Installation an Ihre Gegebenheiten angepasst werden. Eventuell ist auch eine entsprechende Übersetzung erforderlich.

#### Wie ist bei der Positionsart "Seitenumbruch" zu beachten?
Die Positionsart Seitenumbruch dient einem Zwangs-Seiten-Umbruch.

Der Seitenumbruch wirkt nur, wenn danach noch eine Position kommt. Gegebenenfalls muss, wenn der Seitenumbruch eventuell der letzte Eintrag ist, danach noch eine Leerzeile angeführt werden.

#### Wie ist die Positionsart "Endsumme" zu verstehen?
Gerade in Angebot und Auftrag ist es oft so, dass nach dem Kaufmännischen Teil noch eine Reihe von weiteren Informationen anzuführen sind. Wenn nun die Gesamtendsumme erst nach der 10.ten Seite kommen würde, wären wir alle verwirrt. Daher haben wir die Möglichkeit geschaffen, dass gezielt eine Endsumme gesetzt werden kann.

Bitte beachten Sie, dass nach der Endsumme keine Positionen mit Mengen und oder Preisen angeführt werden dürfen.

<a name="Intelligente Zwischensumme"></a>

#### Wie ist die Positionsart "Intelligente Zwischensumme" zu verstehen?
Insbesondere in Branchen im Bereich Bau werden immer wieder Zwischensummen für Positionsbereiche benötigt.
Z.B. für Leistungen an Gebäuden im Erdgeschoss, im ersten Stock usw.
Dafür haben wir die sogenannten intelligenten Zwischensummen geschaffen.
Geben Sie hier die Bezeichnung für den Bereich, z.B. Eingangsbereich, die Positionsnummern von und bis aus der die Zwischensumme gebildet werden sollte und einen eventuellen Rabatt an.
Es können auch Zwischensummen von Zwischensummen gebildet werden um so auch Staffelungen für Gesamtrabatte auf bestimmte Positionsbereiche zusätzlich vergeben zu können. Denken Sie z.B. an einen getrennten Ausweis von Arbeit und Material und dann einen Rabatt für das Material und dann noch einen Gesamtrabatt für den ersten Stock.
Geben Sie dazu einfach die jeweiligen Positionsbereiche von bis entsprechend an, also inkl. der vorgelagerten Zwischensummen.

Die Berechnung der Werte der Zwischensumme erfolgt nur bei der Aktivierung der Rechnung, also beim Druck. Werden zwischendurch Werte der Zwischensumme geändert, so wird dies durch ein gelb hinterlegtes Feld dargestellt, welches den zum letzten Druck beinhalteten Wert anzeigt. Dieser Wert wird erst beim nächsten Druck der Rechnung / des Beleges wieder aktualisiert.![](Intelligente_Zwischensumme.jpg)

Die Formulare sind so gestaltet, dass vor der Von Position die Bezeichnung der Zwischensumme als Überschrift gedruckt wird.<br>
Dort wo die Position Intelligente Zwischensumme eingegeben wurde, wird diese gegebenenfalls mit den Rabatten in Klammern angedruckt, da ja der Wert der Zwischensumme in der Gesamtbetrachtung der Rechnung nicht mitgerechnet werden dürfen.<br>
Wenn nun eine Position "Intelligente Zwischensumme" von einem Auftrag in den Lieferschein übernommen werden soll, so verwenden Sie Sicht Auftrag. Die Zwischensummenposition wird aus Sicht Auftrag dann in die Positionen übernommen, wenn alle von der Zwischensumme umschlossenen Positionen "erledigt" werden können. Als "erledigt" betrachtet werden:<br>
1. mengenbehafteten Positionen mit dem Status erledigt (Ident, Handeingabe) die lieferbar sind.
2. nicht mengenbehaftete Positionen (Texteingabe, Textbaustein, Endsumme, ...)

Die Anordnung der Positionen wird bei der Übernahme beibehalten, so lange wie alle Positionen übernommen werden können. Sobald eine Beginn- oder Endposition einer Zwischensumme nicht in den Lieferschein übernommen werden kann, muss die Intelligente Zwischensumme im Lieferschein angepasst werden.

Wenn die Zwischensumme über einen Bereich, der alternative Positionen enthält, definiert ist, werden diese Positionen nicht für die Preisberechnung herangezogen.
Achtung: Wenn das VON oder BIS einer Intelligente Zwischensummen eine alternative Position ist, fehlen die Start- bzw. Endpositionen und damit erfolgt keine Berechnung mehr.

#### Können für intelligente Zwischensummen Preise vorgegeben werden?
Es kommt immer wieder vor, dass man faktisch für einen Set von Artikel, welcher aber individuell zusammengestellt wurde, einen pauschalen Preis vergeben will.
Dies kann ebenfalls mit der intelligenten Zwischensumme erreicht werden. Oft kommt dies auch in Kombination damit, dass keine Positionspreise angezeigt werden sollten.
Um nun den Wert eine intelligenten Zwischensumme vorzugeben klicken Sie bitte auf den nummerischen Ziffernblock ![](intelligente_Zwischensumme.gif). Im nun erscheinenden Dialog geben Sie den gewünschten Preis für die Gesamtheit der durch die Von Position / Bis Position beschriebenen Artikel/Handeingaben an.
Es wird dabei versucht die Rabatte der Positionen entsprechend so zu steuern, dass die Preisverteilung genau den Betrag ergibt. Da dies, vor allem in Kombination mit unterschiedlichen Stückzahlen mathematisch nicht immer möglich ist, wird eine eventuell verbleibende Differenz durch einen Rundungsartikel dargestellt. Erscheint die Fehlermeldung Rundungsartikel nicht definiert, so muss im System, Parameter Rundungsartikel die Artikelnummer des Rundungsartikels verwendet werden.

### Stornieren von Bewegungsdaten
In **Kieselstein ERP** können aus Gründen des Rechnungslegungsgesetzes einmal angelegte Belege nicht mehr entfernt werden, da im Rechnungslegungsgesetz sinngemäß enthalten ist, dass die Rechnungsnummern fortlaufend sein müssen.<br>
Wir empfehlen daher anstelle z.B. eine Rechnung zu stornieren, diese, wenn sie z.B. falsch ausgestellt wurde, zu ändern.<br>
Ein Beleg kann in **Kieselstein ERP** immer solange geändert werden, solange es keinen Nachfolger dafür gibt. Z.B. wenn die Rechnung noch nicht bezahlt wurde oder sie noch nicht in die Finanzbuchhaltung exportiert wurde, kann sie jederzeit geändert werden. Bitte beachte auf jeden Fall die steuerlichen Vorschriften.<br>
Um einen Bewegungsdatensatz, also z.B. eine Rechnung zu stornieren, muss diese im Status offen sein. Klicken Sie auf die Kopfdaten der Rechnung und wählen Sie dann ![](Gemeinsamkeiten_Stornieren.gif) stornieren. Damit wird diese Rechnung als storniert gekennzeichnet, die darin enthaltenen Artikel, Lieferscheine usw. werden zurückgegeben.
Wenn nun dein Kunde z.B. auch einen Beleg dieser stornierten Rechnung wünscht, so drucke diese bitte erneut aus. Es wird die Rechnung mit dem Hinweis storniert ausgedruckt.
Der Druck des stornierten Beleges ist für Auftragsbestätigung, Rechnung, Gutschrift vorgesehen.

<a name="Storno"></a>

#### Können stornierte Bewegungsdaten wieder verwendet werden.
Ja. Um einen stornierten Bewegungsdatensatz wieder zu verwenden gehen Sie bitte in die Kopfdaten und klicken Sie auf ![](Gemeinsame_Kopfdaten_aendern.gif) ändern.
Die anschließende Frage nach
![](Gemeinsamkeiten_Storno_aufheben.gif)
beantworten Sie bitte mit Ja. Damit werden alle Positionen der Rechnung wieder vom Lager entnommen. Kann die Menge nicht vom Lager entnommen werden, erscheint eine entsprechende Fehlermeldung und das Storno kann nicht aufgehoben werden.

**HINWEIS1**:
Wird ein stornierter Lieferschein wieder aktiviert, so beachten Sie dabei bitte, dass die im Lieferschein enthaltenen Mengen auf 0,00 gesetzt werden, damit bei dieser Aktion keine Lagerbuchungen gemacht werden müssen. D.h. es sind bitte die Mengen entsprechend zu korrigieren.

**HINWEIS2**:
Beim stornieren einer Rechnung wird der Lieferschein auf nicht verrechnet gesetzt und der durch den Lieferschein repräsentierte Wert durch eine Handeingabe dargestellt. Daher muss beim entstornieren der Rechnung der Lieferschein gegebenenfalls wieder in die Rechnung übernommen werden.

#### Durchlaufende Belegnummer oder immer Neubeginn zum Geschäftsjahreswechsel
Üblicherweise wird bei den Belegen, z.B. Rechnung in jedem neuen Geschäftsjahr wieder mit der Nummer 1, ergänzt um die Kennung des Geschäftsjahres begonnen.
In manchen Unternehmen war es bisher üblich diese Nummern auch über den Geschäftsjahreswechsel weiterlaufen zu lassen. Dieses Verfahren hat, gerade für Bilanzierern, einen erheblichen Nachteil.
Der Vorteil des Beginns mit Nr. 1 im neuen Geschäftsjahr und der damit bedingten Kennung innerhalb der Belegnummer ist, dass bei dieser Vorgehensweise auch noch Rechnungen im alten Geschäftsjahr angelegt werden können. Dies wird von **Kieselstein ERP** durch die Rückdatierung der einzelnen Belege bei der Neuanlage unterstützt. Werden hingegen durchlaufende Nummern verwendet, so können nachträglich keine Rechnungen noch für das alte Geschäftsjahr ausgestellt werden. Siehe dazu auch [Rückdatieren]( {{<relref "/verkauf/rechnung/#r%C3%BCckdatieren" >}} ). Siehe bitte auch [Zufallsnummer]( {{<relref "/verkauf/rechnung/#die-kunden-sollten-nicht-erkennen-dass-ich-fast-nur-f%C3%BCr-einen-einzigen-kunden-arbeite">}} ).

#### Können Startwerte für die Belegnummer definiert werden?
<a name="Startwert"></a>
Ja. Es können im System unter Parameter mit dem Suchbegriff Startwert die gewünschten Module gelistet werden. Hier kann der Startwert für die bei jedem Geschäftsjahreswechsel verwendeten Zahl für den ersten Belege des Jahres definiert werden. Üblicherweise beginnen die Rechnungsnummer jedes Geschäftsjahr mit 1 und zählen dann laufend hoch. In manchen Anwendungen ist es gewünscht, dass die Startrechnungsnummer mit einem größeren Wert beginnt. Dies kann unter System, Parameter, RECHNUNG_BELEGNUMMERSTARTWERT definiert werden.<br>
**Wichtig:** Dieser Parameter gilt nur für die Rechnungen und nicht für Gutschriften.

Genauso können die Startwerte für Angebot, Auftrag, Gutschriften und auch Anfragen und Bestellungen gesetzt werden.

**Hinweis:** Durch Definition von -1 wird erreicht, dass die (Rechnungs-) Nummern durchlaufend sind. D.h. im neuen Jahr wird mit der logisch nächsten Nummer fortgesetzt.

**ACHTUNG:** Dies hat den Nachteil, dass gerade in der Zeit des (Geschäfts-) Jahreswechsels keine Rückdatierungen ins alte Geschäftsjahr gemacht werden können, da es sonst logisch gesehen die Belegnummer doppelt gibt.

Daher raten wir diese Funktion auf keinen Fall bei den Eingangsrechnungen zu verwenden, da es hier immer wieder der Fall ist, dass Sie Belege des neuen und des alten Geschäftsjahres zum gleichen Zeitpunkt erfassen müssen.

#### Können Belegnummernkreise definiert werden?
Ja. Verwenden Sie dazu die oben beschriebenen Startwerte.

#### Wie finde ich rasche / einfach eine Belegnummer?
[Siehe bitte]( {{<relref "/start/02_shortcuts/#suche-nach-belegnummern" >}} )

#### Mehrwertsteuer der Position kommt vom Kunden oder vom Artikel?
Mit dem Parameter KUNDEN_POSITIONSKONTIERUNG wird definiert, ob die Mehrwertsteuer je (Rechnungs-) Position aus dem Artikel (=1) kommt oder aus dem Kunden (=0). Default Einstellung ist, dass die Definition des für die Position verwendeten Mehrwertsteuersatzes vom Kunden kommt.

Kommt der Mehrwertsteuersatz vom Artikel, so ist unbedingt erforderlich, dass bei jedem Artikel ein Mehrwertsteuersatz definiert ist. Um auch hier bei der Anlage neuer Artikel einen Standard-Mehrwertsteuersatz definieren zu können, verwenden Sie den Parameter DEFAULT_ARTIKEL_MWSTSATZ. Die hier erforderliche ID des Parameters teilt Ihnen Ihr **Kieselstein ERP** Betreuer gerne mit.<br>
**Hinweis:** Kommt der MwSt-Satz vom Artikel, so wird, wenn der MwSt-Satz des Kunden 0 also steuerfrei ist, der MwSt-Satz der Position trotzdem auf steuerfrei gesetzt.

Beachten Sie dazu auch die Einstellungen unter System, Mandant, Vorbelegungen. Die unterschiedlichen MwSt Sätze.

Beachte bitte auch die zusätzliche Möglichkeit, bei weiteren Finanzämtern, den Mehrwertsteuersatz anhand des Kunden-Finanzamtes zu steuern.

Zu den Mehrwertsteuersätzen in den Positionen, insbesondere bei Änderungen der [MwSt-Sätze siehe auch]( {{<relref "/start/04_allgemeine_stammdaten/#definition-neuer-mehrwertsteuers%c3%a4tze" >}} )

#### Sortierung der Auswahllisten nach Kunde/Lieferant
In der Standard-Einstellung ist aus Geschwindigkeitsgründen die Sortierung der Spalten nach Kunden/Lieferanten abgeschaltet. Mit dem Parameter BEWEGUNGSMODULE_SORTIERUNG_PARTNER_ORT = 1 kann diese Sortierung eingeschaltet werden. Beachten Sie, dass nach dem Umschalten der **Kieselstein ERP** Client neu gestartet werden muss.

#### Verschieben einer Positionen ist nicht möglich.
![](Positionen_verschieben.jpg)

Um Positionen nur zu verschieben, muss der Beleg zuerst in den Status angelegt gebracht werden. Klicken Sie dazu auf ändern, bestätigen Sie di nachfolgende Meldung mit Ja. Dadurch ist der Beleg im Status angelegt. Gegebenenfalls kann das damit eingeleitete Ändern der Daten abgebrochen werden. **Hinweis:** Werden Setartikel verwendet, so können die Setartikel-Positionen nur innerhalb des Sets verschoben werden.
Um den gesamten Set zu schieben muss der Setartikel-Kopf verschoben werden.

<a name="Tabelleneinstellungen, Positionen verschieben"></a>

#### Positionen können in den Bewegungsmodulen nicht verschoben werden?
Manchmal erhalten wir von Anwender die Info dass in verschiedenen Modulen auf einmal die Sortierpfeile weg sind, die Positionen nicht mehr verschoben werden können. Die Ursache ist immer, dass die Positionen des Moduls (z.B. Auftrag) eine Sortierung haben und meist zusätzlich, dass diese Sortierung auch noch gespeichert wurde. Die Lösung ist, einfach die Sortierung löschen und die Speicherung richtig stellen.

Im Detail bedeutet dies folgende Schritte: 
- a.) grundsätzlich können die Positionen eines Auftrags, Angebots usw. mit den Sortierpfeilen ![](Sortierungs_Pfeile.gif) sortiert werden
- b.) werden in den Positionen die normalen Sortierungen durch Sortierungen der Spaltenüberschriften verwendet, so werden die Sortierpfeile nicht mehr dargestellt. Dies ist durch in der jeweiligen Spaltenüberschrift ![](Sortierung_Bezeichnung.gif) ersichtlich.
- c.) Wurde nun auch noch die Spaltendarstellung durch Klick auf Tabelleneinstellungen speichern ![](Tabelleneinstellung_speichern.gif) gesichert, so ist beim nächsten Start des Moduls die Sortierung wieder gegeben und somit sind z.B. die Auftragspositionen nicht mehr sortierbar. Wie ist nun vorzugehen ?
- d.) Um die Sortierung zu löschen
  - d1.) klicken Sie bitte mit der rechten Maustaste in die Spaltenüberschrift. Damit ist die Sortierung gelöscht und die Sortierpfeile werden wieder angezeigt.
  - d2.) Speichern Sie unbedingt diese Einstellung durch Klick auf Tabelleneinstellungen speichern, die nach der ersten Speicherung als ![](Tabelleneinstellung_ueberschreiben.gif) Haken dargestellt werden.

Damit die Tabelleneinstellungen gespeichert werden können, muss der jeweilige Benutzer in jedem Falle ein Schreibrecht auf sein lokales **Kieselstein ERP** Benutzerverzeichnis haben. D.h. für Windowsinstallationen ist dies unter <Benutzerverzeichnis>/.kes/ also z.B. ![](Pfad_fuer_Tabelleneinstellungen.png)<br>
zu finden. In den anderen Betriebssystemen gehen Sie bitte ebenfalls vom Benutzerverzeichnis aus.

#### Einfügen von zusätzlichen Setpositionen in ein bestehendes Set
#### Verändern von Artikelsets in den Belegen

A: Es ist in der Praxis immer wieder erforderlich, dass angelegte Artikelsets, welche oft auch als Verkaufssets bezeichnet und genutzt werden, in einem spezifischen Verkaufsvorgang (Von Angebot bis Rechnung) verändert werden müssen.

Die in **Kieselstein ERP** implementierte Logik von Sets ist, dass das in den Stücklisten definierte Set als Vorschlagswert in den Verkaufsbeleg übernommen wird. Wenn nun das Verkaufsset verändert werden sollte, so kann durch einfügen und löschen das Set angepasst werden. Bei der Überleitung des Sets aus dem Auftrag in den Lieferschein bzw. in die Rechnung wird das im Auftrag hinterlegte Set als Basis verwendet.

Beim Einfügen zusätzlicher Positionen in ein Set und damit auch bei der eventuellen Änderung eines Zubehörteiles stellen Sie den Cursor auf die Setposition, vor der der neue Artikel, die neue Setposition eingefügt
werden sollte und klicken Sie auf einfügen ![](Einfuegen.gif). Wenn der neue Artikel am Ende des Artikelsets sein sollte, so verschieben Sie ihn danach mit den Pfeilen ![](Sortierungs_Pfeile.gif) an die gewünschte Position. Wie bereits oben beschrieben, können die Setartikel Positionen nur innerhalb des Sets verschoben werden.

#### Wie werden die Werte der Setartikel behandelt?
Die Preise der einzelnen Setartikelpositionen werden anhand des jeweiligen Verkaufspreises des einzelnen Setartikels im Verhältnis zum Gesamtwert verteilt. [Siehe dazu bitte auch]( {{<relref "/verkauf/gemeinsamkeiten/#wie-werden-die-werte-der-setartikel-behandelt"  >}} ).

#### Unterschied der Behandlung von Setartikeln zwischen Sicht Auftrag und Position
Werden ausgehend vom Auftrag Setartikel in den Lieferschein bzw. die Rechnung übernommen, so wird der Inhalt/Umfang des Setartikels durch den Auftrag bestimmt. Werden hingegen im Reiter Position Setartikel hinzugefügt, so wird der Inhalt des Setartikels aus den im Modul Stücklisten definierten Positionen übernommen.

### Bestimmte Positionsarten sollten nicht verwendet werden
Für einige Anwender ist es sinnvoll, praktisch, wenn bestimmte Positionsarten in bestimmten Modulen nicht zur Verfügung stehen. So könnte es z.B. die Regel geben, dass Ware die ausgeliefert wird, immer mit einem Lieferschein ausgeliefert werden muss. Oder es dürfen auf Lieferschein und Rechnung keine Handartikel verwendet werden.

Um dies abzuschalten, gehen Sie bitte im jeweiligen Modul in die Grunddaten, wählen Sie Positionsart, klicken Sie auf die gewünschte Positionsart, dann auf ändern und haken Sie versteckt an. Nach dieser Änderung muss der **Kieselstein ERP** Client neu gestartet werden.

#### Zusätzliche Beschreibungen sollten mit meinem Artikel verbunden werden
Manchmal ist es erforderlich, von Vorteil, wenn z.B. zusätzliche Beschreibungen eines Artikels direkt mit dieser Positionszeile  verbunden sind. Dafür haben wir die Möglichkeit der Texteingabe beim Artikel (Ident) geschaffen. D.h. durch Klick auf ![](Text_Bearbeiten.png) gelangen Sie in die Erfassung der Textzeilen mittels Texteditor, welche zu dieser Position gehören. Wird nun diese Position abgespeichert, so ist in der Beleg Übersicht in der rechten Spalte ein Haken sichtbar, wenn diese Zeile eine Texteingabe beinhaltet.<br>
Um den Text zu sehen, ohne den Beleg zu ändern, bewegen Sie bitte die Maus auf die Zeile. Wenn dann die Maus für 1-2Sekunden still steht, wird der Text für ca. 5Sekunden als sogenannter Tooltip angezeigt.<br>
Zur Einstellung der Breite des Texteditors [siehe bitte]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/freie_texte/#wie-passen-die-breite-des-texteditors-und-die-breite-des-ausdrucks-zusammen" >}} )

#### Die Auswahllisten sollten nach Kunde sortiert sein?
Ja dies ist grundsätzlich möglich.

Üblicherweise ist die Sortierung nach Kunde bzw. Lieferant abgeschaltet. Dies ist anhand des ![](Gemein1.gif) weißen Punktes vor dem Namen ersichtlich. Um diese Sortierung nun einzuschalten muss der Parameter (im Modul System) BEWEGUNGSMODULE_SORTIERUNG_PARTNER_ORT auf 1 gestellt werden. Nach dieser Umstellung starten Sie bitte Ihren Client neu.

#### Wenn ich einen Sonderrabatt geben muss, wie mache ich das richtig ?
Werden z.B. in Angeboten Rabatte gegeben, so empfiehlt sich, von Anfang an diese Daten richtig in **Kieselstein ERP** einzupflegen. In diesem Zusammenhang sind zwei Punkte zu beachten.

a.) wie ist der Warenfluss ?

b.) soll, am Ende in der Rechnung bzw. in den Umsatzstatistiken dieser negative Umsatz in der jeweiligen Artikelgruppe/klasse aufscheinen oder unter unbekannt.

a.) Warenfluss:

In aller Regel gewähren Sie dem Kunden den Rabatt, also findet ein Warenfluss von Ihnen zum Kunden statt. Das bedeutet bei der Menge ist eine positive Zahl einzugeben und der Preis ist mit Minus einzugeben.

Würde bei der Menge eine negative Zahl eingegeben, so bedeutet dies, dass Sie die Ware vom Kunden zurückerwarten, z.B. weil er Ihnen im Tausch ein altes Gerät zurückgibt. Bedenken Sie bitte, dass diese Buchungen von **Kieselstein ERP** ab der Auftragsverwaltung berücksichtigt / unterstützt werden. D.h. wenn Sie bei einem Auftrag einen Artikel mit negativer Menge eingeben, so wir davon ausgegangen, dass dieser Artikel zum Finaltermin des Auftrages wieder dem Lager zugebucht wird. Dies wird vor allem in der Bewegungsvorschau und damit in den Bestellvorschlägen entsprechend berücksichtigt.

b.) Artikel oder Handeingabe

Handeingabe ist hier sicherlich das Einfachste. Diese Buchung hat jedoch den deutlichen Nachteil, dass in den Umsatzstatistiken diese Buchung als unbekannte Artikelgruppe/klasse angezeigt wird. In den Artikelstatistiken kann die Handbuchung nicht angeführt werden. Es empfiehlt sich daher hier z.B. je Artikelgruppe einen nicht Lagerbewirtschafteten Artikel einzurichten und diesen im jeweiligen Beleg zu verwenden und die Artikeltexte entsprechend zu übersteuern.

**Hinweis:**

Es ist auch oft die Forderung gegeben für eine Gruppe von Artikeln einen bestimmten Pauschalpreis zu geben. Verwenden Sie dazu bitte die Gruppierung mittels der Positionsart Position mit der Einstellung Positionspreis unterdrücken, bzw. Positionspreis andrucken.

![](Gemein_Positon.gif)

Hier haben Sie die Möglichkeit einen Pauschalpreis zu vergeben. Dies bewirkt, dass die Statistikpreise der Artikel entsprechend anteilig nach Positionswert umgerechnet werden und so richtig in den Statistiken dargestellt werden.

**Hinweis 2:**

Alternativ können auch die Zusatzrabatte in den Positionen (Ident, Handeingabe) verwendet werden.

#### Wie werden Mengen richtig eingeben?
Bei den Ausgangsmodulen, also Angebot, Auftrag, Lieferschein, Rechnung, Fertigung (Los) bedeutet eine positive Menge, dass diese Ware vom Lager abgebucht wird und an den Kunden, in die Fertigung gebucht wird. Im Lieferschein haben Sie die Möglichkeit negative Mengen einzugeben. Dies bedeutet, dass die Menge zurück in das in den Kopfdaten angegebene Lager gebucht wird.

Im Modul Gutschrift, wird ebenfalls Ware zurückgenommen, wenn es sich um eine Mengengutschrift handelt. D.h. hier werden positive Mengen auf das angegebene Lager zugebucht.

Im Modul Bestellung werden im Wareneingang positive Mengen an das definierte Lager zugebucht.

Bei der Losablieferung werden Mengen von der Produktion in das in den Kopfdaten angegebene Ziellager zugebucht.

#### Anzeige des Versandstatus?
In vielen Anwendungen sollte man auch unterscheiden können, ob ein Auftrag tatsächlich an den Kunden versandt wurde, oder ob er nur intern angelegt und gedruckt wurde. Dazu finden Sie in den Auswahllisten neben der Anzeige des Status nun auch die Information über welches Medium der Versand erfolgte. ![](Gemein_Versandstatus.jpg) Es werden hier die gleichen Symbole wie in der Druckvorschau verwendet.

#### Wie wird der Verkaufspreis errechnet?
[Siehe dazu.]( {{<relref "/docs/stammdaten/artikel/verkaufspreis" >}} ) Siehe dazu aber auch [Materialzuschlag](#Materialzuschlag) bzw. [Preisdialog](#Preisdialog).

<a name="Lagermindeststandswarnung"></a>

#### Lagermindeststandswarnung
In den Modulen Auftrag, Lieferschein, Rechnung kann bei Unterschreitung des Lagermindeststandes zum Liefertermin (beim Auftrag), bzw. zum Belegdatum eine Warnung ausgegeben werden.

D.h. wird der Lagermindeststand des Artikels aus jetziger Sicht unterschritten, so erhalten Sie eine entsprechende Meldung.

Hier wird die Summe aller Läger zusammengezählt. Eine Stücklistenauflösung wird nicht durchgeführt.

Für eine Prüfung der Wiederbeschaffung / Lieferbarkeit der Artikel eines Kundenauftrages siehe [Wiederbeschaffung]( {{<relref "/verkauf/auftrag/#wiederbeschaffung" >}} ).

Die Lagermindeststandswarnung für die Module muss unter System, Parameter eingeschaltet werden. Siehe dazu die Parameter  MINDESTSTANDSWARNUNG für die Kategorien, Auftrag, Lieferschein, Rechnung.

#### Verleihtage / Verrechnungsfaktor
Für die Module Angebot, Auftrag, Lieferschein, Rechnung, Gutschrift steht die Zusatzfunktion Verleihtage bzw. Verrechnungsfaktor zur Verfügung.

Diese Funktion kommt aus dem Verleihgeschäft. Hier wird eine Menge x von Geräten/Produkten für eine Dauer von y Tagen verliehen. Um nun die Lagerbewegungen aber auch die Verrechnung richtig definieren zu können haben wir die Funktionalität der Verleihtage geschaffen. Da auch beim Verleihgeschäft Mengenstaffeln, in diesem Falle über die Verleihtage gewährt werden, müssen die zur Verrechnung zur Verfügung stehenden Verleihtage und deren Verrechnungsfaktor im Modul Artikel, unterer Modulreiter Grunddaten, oberer Modulreiter Verleih definiert werden.

Bitte beachten Sie: Werden die Verleihtage verändert / hinzugefügt müssen die abhängigen Module neu gestartet werden, damit die neuen Definitionen übernommen werden.

Die Definitionen sind, wie die Zahlungsziele bzw. die Zeitmodelle einfach definiert. D.h. werden diese verändert, so wirkt die Veränderung auf alle Drucke, Berechnungen usw.. Sowohl in der Vergangenheit als auch für die Zukunft.

Die angegebenen Tage stehen bei der Positionserfassung von Ident = Artikel zur Verfügung.

Wie werden Verleihtage erfasst und berechnet?

Bei der Erfassung von Leihtagen steht im Mengenfeld zwei Felder für die Erfassung zur Verfügung. Im ersten Feld haben Sie die Auswahlmöglichkeit für die Verleihtage und im zweiten Feld für die Menge der verliehenen Geräte / Artikel![](Verleihtage.gif)

Sollten keine Verleihtage mit der Berücksichtigung von Faktoren verwendet werden, so stellen Sie das erste Feld bitte auf \<LEER>.

Für die Berechnung der Zeilensumme wird der bei den Verleihtagen hinterlegte Faktor berücksichtigt. Genauso wird für die Berechnung der Gesamtsumme der beim jeweiligen Verleihtag hinterlegte Faktor mitgerechnet.

**Hinweis:** In den von uns standardmäßig ausgelieferten Formularen sind diese beiden Werte nicht angeführt. Bitten Sie Ihren **Kieselstein ERP** Betreuer, diese für Sie in die Formulare an der richtigen Stelle einzubauen.

#### Wie werden Seriennummern / Chargennummern erfasst? 
Für [Seriennummern siehe]( {{<relref "/docs/stammdaten/artikel/serienchargennummern/#wozu-dienen-seriennummern" >}} )

für [Chargennummern siehe]( {{<relref "/docs/stammdaten/artikel/serienchargennummern/#wozu-dienen-chargennummern" >}} )

#### Wieviele Seriennummern können in einer Lieferscheinposition erfasst werden?
Die Antwort auf diese Frage hängt auch mit der Geschwindigkeit Ihres **Kieselstein ERP** Servers zusammen.<br>
D.h. wenn Seriennummern vom Lager abgebucht werden, werden alle davon betroffenen Seriennummern in einer Transaktion verbucht. Dies dient vor allem Ihrer Sicherheit, dass entweder die Artikel am Lager sind oder im Lieferschein, aber nicht irgendwo dazwischen. Da die Transaktionen zwischengespeichert werden müssen, steigt mit der Anzahl der beteiligten Seriennummern die Zeit für die Buchung exponientiell. Für diese Buchung ist eine Zeit von zehn Minuten vorgesehen, was in der Regel für mehr als 1.000 Seriennummern für eine Position ausreicht.<br>
Sollte dieses Timeout bei einer deutlich geringeren Anzahl von Seriennummern wirken, wenden Sie sich bitte vertrauensvolle an Ihren **Kieselstein ERP** Betreuer. Er/Sie wird Ihnen Lösungsmöglichkeiten vorschlagen.

Für die Anwender mit bidirectionalen EDI Anbindungen sei darauf hingewiesen, dass die Anzahl von übertragbaren Seriennummern-Datensätzen durch die EDI Definition sehr stark limitiert ist.

#### Jahresanzeige
Es besteht manchmal der Wunsch, dass nur die z.B. Rechnungen eines Jahres angezeigt werden sollen. Dies kann im Nummern Direktfilter eingestellt werden. Geben Sie z.B. für das Jahr 2011 nur 11/% ein. Sollte in Ihrer Installation ein anderes Trennzeichen eingestellt sein, so bitte dieses verwenden.

<a name="Zahlungsziel"></a>

#### Zahlungsziel
Die Zahlungsziele werden für jeden Mandanten im Modul System, unterer Modulreiter Mandant, Auswahl des Mandanten (es kann nur der Mandant bearbeitet werden, an dem Sie gerade angemeldet sind) und oberer Modulreiter Zahlungsziel.

Bei der Anlage eines Zahlungsziels ist zu beachten:
Die Berechnung von Fälligkeiten erfolgt nach den Netto Tagen.
Für die Berechnung von Skontozahlungen werden die jeweiligen Skontotage herangezogen.
Achten Sie darauf, dass die Skontotage immer kürzer als die Nettotage sind.
Wenn Sie auch das Modul Zahlungsvorschlag besitzen, so werden diese Werte auch für die Errechnung des Zahlungsvorschlages herangezogen.
Da es bei der Definition von Vorauskasse immer wieder zu Fragen kommt hier die richtige Einstellung dafür:
![](Vorauskasse.gif)
Das bedeutet, die Rechnung ist sofort fällig und gleichzeitig können Sie sich dafür 2% Skonto abziehen.
Beachten Sie diese Einstellung bitte insbesondere für den Zahlungsvorschlagslauf im Modul Finanzbuchhaltung.
Um ein Zahlungsziel beim Zahlungsvorschlag zu berücksichtigen, setzen Sie den Haken bei ![](../Eingangsrechnung/zahlungsvorschlag_beruecksichtigen.JPG) in Zahlungsvorschlag berücksichtigen.

Um einen Stichtag anstelle der Anzahl Tage Netto für ein Zahlungsziel zu definieren, klicken Sie auf Stichtag.
Damit können die bei der Anlieferung von großen Unternehmen oft aufgezwungenen Bedingungen Rechnungsdatum und Fälligkeit zum 15\. des zweitnachfolgenden Monats abgebildet werden.![](../Allgemeine_Stammdaten/zahlungsziel_stichtag.JPG)
Nun können Sie im Feld den Fälligkeitstag und das Folgemonat angeben. Wenn Sie im Folgemonat 0 eingeben, so wird das aktuelle Monat verwendet, bei 1 das nächste Monat und entsprechend weiter. Wenn Sie den Haken bei Monatsletzter setzen, so wird der letzte Tag des definierten Monats verwendet.
Beispiel1:
Rechnungsdatum: 16.3.
Fälligkeit bei Einstellung wie oben: 15.5.

Beispiel2:
Rechnungsdatum: 14.3.
Fälligkeit bei Einstellung wie oben: 15.4.

Da es immer wieder auch zu Mischungen von Stichtag und Skonto kommt, wurde auch folgende Variante unterstützt.
![](Stichtag_und_Skonto_im_Zahlunsgsziel.gif)
3% bis zum 25\. des Folgemonates, 60Tage netto

Wird Folgemonat definiert, so bedeutet dies, dass die Skontotage als Stichtag des Folgemonates gelten. Wenn innerhalb des Datums der Rechnungslegung mit Skonto bezahlt werden sollte, muss beim Folgemonat 0 eingegeben werden.

**Hinweis:** Skontotage von 0 bedeutet, dass bei Bezahlung zum Rechnungsdatum ein Skonto gewährt wird. Wenn kein Skonto definiert sein sollte, muss dieses Feld leer sein.

<a name="Zahlungsziel"></a>

#### Zahlungsziel "Sondervereinbarung"
Legen Sie ein Zahlungsziel "laut beiliegender Sondervereinbarung" an. Bei den Pflichtfeldern (Tage, Skontotage und Skonto) geben Sie 0 ein. In der Rechnung unter dem Reiter Konditionen können Sie nun das Zahlungsziel auswählen und in dem Fußtext die Vereinbarungen angeben.
Wichtig ist, dass Sie die so erstellte(n) Rechnung(en) aus den Mahnungen ausschließen. Hierzu wählen Sie die gewünschte Rechnung aus und setzen unter Bearbeiten eine Mahnsperre. Geben Sie hier das gewünschte Datum ein.
Ein Vorschlag wäre auch die erste Mahnung (meistens 7 Tage) noch beizubehalten um die 1\. Zahlung der Sondervereinbarung zu überwachen und danach die Mahnsperre einzurichten.

Idealerweise geben Sie dann eine Texteingabe / einen Textbaustein nach der Endsumme dazu, der diese Sondervereinbarung genauer beschreibt.

<a name="Zahlungsziel mit Lastschrift"></a>

#### Zahlungsziel mit Lastschrift
Um bei den Rechnungen ein SEPA-Lastschriftmandat andrucken zu können, haken Sie bitte beim gewünschten Zahlungsziel Lastschrift an. Dadurch wird der Lastschriftstext mit den entsprechenden Bankinformationen auf dem Zahlungsziel der Rechnung mit angedruckt.
Der Ablauf für die Lastschrift nach SEPA ist wie folgt:

-   Es ist dies eine sogenannte Basislastschrift

-   Beantragen Sie die Gläubiger-ID und tragen Sie diese in System, Vorbelegungen 2 ein.
    Die Gläubiger-ID kann in Deutschland bei der [Bundesbank](http://www.glaeubiger-id.bundesbank.de)
    und in Österreich bei Ihrer Bank
    beantragt werden

-   Senden Sie das entsprechende Lastschriftschreiben an Ihren Kunden. Dieses finden Sie im Modul Kunden, am jeweiligen Kunden im Reiter Bankverbindung.
    Dieses ist von Ihm freizugeben und an Sie zurückzusenden.
    Tragen Sie danach die Mandatsreferenznummer und das Datum der Zustimmung Ihres Kunden unter erteilt am, ebenfalls im Reiter Bankverbindung ein.
    Wir empfehlen das unterschriebene Schreiben in der Dokumentenablage des Kunden abzulegen.

-   Achten Sie bitte darauf, dass die Gläubiger-ID OHNE Leerstellen erfasst werden muss.

#### Zahlungsziel Betrag bereits erhalten
Wenn Si ez.B. auch einen Webshop betreiben, so wird üblicherweise die Ware nur dann ausgeliefert, wenn der Betrag sicher verfügbar ist. So z.B. per Vorauskasse oder Kreditkarte. Somit sollte beim Zahlungsziel nicht das Zieldatum sondern *Bereits bezahlt* angedruckt werden. Geben Sie dafür bei den Nettozieltagen eine negative Zahl (z.B. -1) ein. Damit wir *Rechnungsbetrag bereits erhalten* angedruckt.

#### Wie erkennen Sie ob alle Voraussetzungen für den Andruck des SEPA-Mandates erfüllt sind?
Diese werden bei der Aktivierung der Rechnung erfasst. D.h. wenn die Rechnung nur in die kleine Vorschau gedruckt wird, so erfolgt noch keine Prüfung, sondern erst beim tatsächlichen Ausdruck, Versand der Rechnung.

<a name="Rundung des Bruttowertes"></a>

#### Runden des Bruttobetrages
In manchen Ländern sind die Bruttobeträge der Rechnungen auf Werte größer 0,01 zu runden.
Der Hintergrund ist hier, dass es für die Barzahlung keine kleineren Münzen als z.B. 5 Rappen (Schweiz) gibt.
Um dies zu definieren hinterlegen Sie bitte im System, unterer Modulreiter System, [oberer Modulreiter Land]( {{<relref "/docs/stammdaten/system/#definition-des-rundungswertes" >}} ) beim jeweiligen Land die gewünschte Rundung.<br>
Die Rundung wird so eingerechnet, dass der sich ergebene Bruttorechnungsbetrag kaufmännisch auf oder abgerundet wird ([siehe dazu auch Rundung]( {{<relref "/verkauf/rechnung/preise_in_der_rechnung/#bank%c3%bcbliche-rundung-bankers-runden" >}} ). Der sich ergebende Differenzbetrag wird, korrigiert um eventuelle Rabattbeträge als reiner Nettobetrag ohne Mehrwertsteuer als letzte (Rechnungs-) Position mit einem Rundungsartikel angedruckt.
Für den Rundungsartikel sollte eine eigene Artikelgruppe definiert werden und so die Summen der Rundungen auf ein eigenes Rundungskonto gebucht werden.<br>
Bitte beachten Sie, dass dieser Artikel:
- Nicht lagerbewirtschaftet
- bei der Mehrwertsteuer als steuerfrei definiert sein muss.

Da die Rechnung doch meistens von Auftragsbestätigung und Angebote abweicht, denken Sie z.B. an Teillieferungen und Teil-Verrechnung, haben wir diese Funktion nur im Rechnungsmodul integriert.

#### Wieso trägt **Kieselstein ERP** dann für gerade Beträge doch einzelne Rappen / Cent ein ?
Es geht grundsätzlich darum, dass der Bruttobetrag auf 0,05 Rappen/Cent stimmt. Wenn also z.B. eine Konstellation wie unten gegeben ist, muss, um den Endbetrag über alle Rabatte und Steuersätze hinweg zu erhöhen eben eine Rundungsposition mit einem Rappen hinzugefügt werden.

![](Rappenrundung_Beispiel.gif)

Da obige Betrachtung bei einigen wenigen Ihrer Kunden negativ aufstoßen könnte, Sie hatten ja 101,- vereinbart und nicht mehr, können Sie im Land definieren ![](Rundungsart.gif) wie gerundet werden sollte. So ist für dieses Beispiel die Einstellung auf 0.05 immer abrunden einzustellen. D.h. obiges Beispiel wird auf 102.50 abgerundet und dem Kunden 4 Rappen geschenkt.

Kostenträger

Als weitere zusätzliche Funktion kann für die Verkaufsmodule (AG-GS) je Position auch ein Kostenträger angegeben werden. Dies wird derzeit für weitere Auswertungen des Warenausgangsjournals verwendet.
Die zur Verfügung stehenden Kostenträger werden im System, Mandant, Kostenträger definiert.

#### Die Zahlungsziele sind verschwunden
Ursache ist, dass bei den Bezeichnungen der Zahlungsziele in Ihrer Client-Sprache Leerzeichen als Bezeichnung eingegeben wurden. Löschen Sie entweder die Leerzeichen oder besser: Geben Sie den sprachlich richtigen Text zum Zahlungsziel ein.

#### Warum wird die Warenerklärung (nicht) angedruckt
Die Warenerklärung oder auch Ursprungserklärung genannt, wird gedruckt, sobald der Empfänger der Waren aus einem Drittland kommt.
Falls die Warenerklärung bei einem EU-Mitglieder angedruckt wird, so überprüfen Sie bitte im Modul System - unterer Reiter System, oberer Reiter Land, ob bei diesem Land in dem Feld EU-Mitglied seit ein Datum eingetragen ist. Falls hier kein Eintrag besteht, geben Sie bitte das Datum des EU-Beitritts ein. Falls bei einem Drittland ein EU-Beitritt eingetragen ist, löschen Sie diesen.

#### Welche Texte werden für Auslandsrechnungen angedruckt und wie werden diese gesteuert?
Aufgrund der stärker werdenden Globalisierung kommen immer mehr **Kieselstein ERP** Anwender in die Situation, dass sie Rechnungen nicht nur ins Inland bzw. das benachbarte Ausland sondern Weltweit in den unterschiedlichsten und oft auch interessanten Konstellationen versenden. Die oberste Voraussetzung ist zu wissen, welcher umsatzsteuerliche Anwendung für Ihre Rechnung gegeben ist.<br>
Üblich war meist die Regelung Rechnung und Lieferung an gleiche Adresse, also definiert (gedanklich) die Rechnungsadresse die steuerliche Behandlung. Also Inland mit allgemeinem Steuersatz (20% in AT, 19% in DE, 7,7% in CH/FL) Ausland mit steuerfrei. Nun kommen aber die Fälle des Reverse Charge dazu, welche nur für die Mitglieder der EU gelten und Rechnungslegungen im Inland mit Lieferung an einen Empfänger im Ausland aber einer Zwischenstation über Ihren Spediteur (Flughafen) der wiederum im Inland ist. Usw.

Dazu nun eine Aufstellung unter welchen Bedingungen welche Texte im **Kieselstein ERP** standard Rechnungsformular angedruckt werden. Bitte beachten Sie dass die Texte selbst Vorschlagswerte sind. Auch diese sind rechtlichen Ansichten und Entwicklungen unterworfen. Lassen Sie sich diese von einem zertifizierten Berater freigeben.

| Beschreibung / Text | Voraussetzung ohne integrierter Fibu | Voraussetzung mit integrierter Fibu |
| --- |  --- |  --- |
| Ursprungserklärung | Waren sind **nicht** im Mandantenland als Ursprungsland definiert  |   |
| IG Warenverkehr | Kunde muss EU-Mitglied sein und die UID Nummer eingetragen haben |   |
| Reverse Charge Inland |   |   |
| Reverse Charge Ausland |   |   |
| Drittland |   |   |

Wodurch wird definiert ob und ab- / bis wann ein Land Mitglied der europäischen Union ist. Bitte beachten Sie auch immer, dass hier die Regelung der hinterlegten europäischen UID Nr zusätzlich gegeben ist.
Siehe [Länderdefinition]( {{<relref "/docs/stammdaten/system/#anlegen-von-l%c3%a4ndern" >}} )
Bitte beachten Sie auch die Möglichkeit der übersteuerten [Länderart]( {{<relref "/management/finanzbuchhaltung/fibu_export/#wie-wird-die-l%c3%a4nderart-bestimmt" >}} ).

#### Wie wird in den Verkaufsbelegen mit dem Materialzuschlag umgegangen?
<a name="Materialzuschlag"></a>
Insbesondere wenn der [Materialzuschlag]( {{<relref "/docs/stammdaten/artikel/materialzuschlag" >}} ) (es geht meist um den Kupferzuschlag) so definiert ist, dass der [erste Beleg den Zuschlagskurs]( {{<relref "/docs/stammdaten/artikel/materialzuschlag/#verrechnung-nach-vorg%c3%a4nger-beleg-oder-zum-rechnungsdatum" >}} ) definiert, sind einige Punkte zu beachten.<br>
Ausgangsbasis ist dafür meist, dass Sie für Ihren Kunden Angebote legen müssen, die eine Art Projektpreis definieren. Damit ist verbunden, dass dieser Preis inkl. Kupferzuschlag für die Laufzeit dieses Projektes gilt. Daraus ergibt sich, dass egal wie der Kurs zum Auslieferzeitpunkt tatsächlich steht, es muss der im Angebot angegebene Kurs bis in die Rechnung durchgereicht werden. Dies klingt einfacher als es ist, denn es gibt natürlich auch immer wieder die Situation, dass im Auftrag bzw. bei der Lieferung zusätzliche Artikel angeführt werden (müssen) und diese haben dann unter Umständen abweichende Kurs. Hier ist auf den Unterschied zu achten, ob eine Position durch kopieren z.B. über die Zwischenablage oder über das Einlesen eines Angebotes (in einen Auftrag) übernommen wurde oder ob manuell eine zusätzliche Position hinzugefügt wurde. Je nachdem wie die Vorgangsweise war, greifen unterschiedliche Daten. Wurde eine Position neu angelegt, so gilt der Kurs welcher zum Zeitpunkt der Anlage der Position anhand des Belegdatums ermittelt wurde. Wurde aber eine Position aus einem anderen Beleg einkopiert, so gilt der bei dieser Position damals definierte Kurs, welcher durchaus eine deutliche Abweichung zum aktuellen Kurs darstellen kann.
Um alle Positionen auf die aktuelle Kursberechnung zu bringen, muss jede Position des Beleges neu / rekalkuliert werden.
Bitte beachten Sie, dass wenn ev. Kurskorrekturen im Materialzuschlag durchgeführt werden, so werden diese in den bereits angelegten Positionen NICHT aktualisiert, wir würden ihre gesamten nach außen kommunizierten Daten verändern. D.h. auch in diesem Falle sind die entsprechenden Belege neu zu berechnen. Nutzen Sie dafür die Funktion Neu berechnen.
Um eventuelle Differenzen zwischen der Erfassung des Materialzuschlages und der aktuellen Berechnung darzustellen, wird im Preisdialog unten der gespeicherte und der aktuelle Zuschlagskurs angezeigt. Bitte wählen Sie den gewünschten für Sie richtigen Kurs. Damit Sie bei einer eventuellen Kursabweichung erkennen können warum dies so ist, wird bei Materialzuschlag ein Tooltip angezeigt in dem die Ausgangsdaten dargestellt werden.
![](Materialzuschlag.gif)
Dies bedeutet, dass es durchaus vorkommen kann, dass auf einem Beleg gleiche Artikel mit unterschiedlicher Kursbasis (unterschiedlichem Bezugsdatum für die Kursberechnung) vorzufinden sind. Die Entscheidung ob dies richtig ist, liegt bei Ihnen. Bitte achten Sie darauf. Wir lehnen jegliche Haftung zu diesem Thema ab.

<a name="Preisdialog"></a>

#### Wieso erscheint bei der Erfassung immer der Preisdialog?
Werden Verkaufspositionen mit Artikeln erfasst, so wird oft, abhängig von der Art der Erfassung, der sogenannte Preisdialog angezeigt. In einigen Unternehmen ist diese Sicherheitsabfrage irritierend. Der tatsächliche Hintergrund ist, dass üblicherweise bei der Neuanlage einer Position in einem Verkaufsbeleg (Angebot bis Rechnung) auch eine Menge von einem Stück vorgeschlagen wird. Dies bewirkt, dass automatisch bereits eine Verkaufspreisermittlung, eine Verkaufspreisberechnung für dieses eine Stück erfolgt. Wird nun die Menge geändert, so ergibt eine erneute Preisberechnung, dass eigentlich ein anderer Preis ausgewählt verwendet werden sollte. ABER: Es könnte auch sein, dass der bereits eingegebene Preis doch der von Ihnen gewünscht ist. Aus diesem Grunde erscheint, als Frage an Sie als Wissenden, der Preisdialog. Also die Frage, welchen Preis möchten Sie denn in dieser Position nun wirklich verwenden. Dieses Verhalten ist bei Änderungen in den Verkaufspositionen durchaus erwünscht. Bei der Neuanlage kann dies, je nachdem ob von Ihnen gedanklich schon ein Preis eingegeben wurde oder nicht, verwirrend sein.
Wir haben daher eine Funktion geschaffen, mit der der Mengenvorschlag abgeschaltet werden kann. Um den Mengenvorschlag abzuschalten setzen Sie bitte den Parameter VK_STANDARD_MENGE auf 0\. Dadurch wird erreicht, dass die erste Preisberechnung erst nach Ihrer Erfassung der gewünschten Menge erfolgt und somit wird die erste Preisfindung ohne Änderungsinfo durch den Preisdialog durchgeführt.

<a name="Schrottrechnung"></a>

#### Wie erfasse ich die Schrottrechnung richtig?
Gerade im Bereich der Elektrotechnik aber auch der Metallverarbeitung wird der anfallende wertvolle Schrott an die Materiallieferanten zurückverkauft. Bitte beachten Sie dabei einige steuerliche Feinheiten.

-   üblicher Weise wird der Schrott von Ihrem Materiallieferanten mitgenommen. Sie erhalten dann von Ihm eine Gutschrift.

-   Diese Gutschrift ist aus der Sicht Ihres Unternehmens ein Erlös aus dem Verkauf von Ware, in diesem Falle also Schrott.
    Das bedeutet, dass Sie in **Kieselstein ERP** eine Ausgangsrechnung erstellen, die dem Wert der Gutschrift Ihres Lieferanten entspricht.
    Für unsere Anwender in Deutschland. Dies ist jetzt wirklich eine Warengutschrift, im Gegensatz zur Rechnungskorrektur.

-   Hinterlegen Sie den Beleg der Gutschrift bei Ihrer Ausgangsrechnung. Diese Rechnung dient nur internen und steuerlichen Zwecken.

Wie mit der Zahlung umgehen?

-   Üblich ist, dass man sich bei der nächsten Rechnung des Materiallieferanten die Schrott-Gutschrift abzieht.

-   D.h. auf der einen Seite gibt es die Eingangsrechnung für die Materiallieferung, andererseits gibt es die (Ausgangs-)Rechnung für den Schrott.
    D.h. als ersten Schritt buchen Sie bitte die offene Ausgangsrechnung (des gutgeschriebenen Schrotts) gegen die Eingangsrechnung für die neue Lieferung.
    Somit reduziert sich der offene Zahlbetrag der Eingangsrechnung entsprechend.

Welche Punkte sollten noch beachtet werden ?

-   Es ist natürlich ideal, wenn Kunde und Lieferant nur ein Partner sind

-   Um den verkauften Schrott in der Buchhaltung auf einem eigenen Konto abzubilden, sollte bitte ein Schrott-Artikel und eine Schrott-Artikelgruppe welche dann auf das eigentliche Schrott-Verkaufs-Erlös-Konto zeigt hinterlegt werden.

-   Beachten Sie die Vorschriften bezüglich Reverse Charge

-   Sollten Sie für die Schrottlieferung einen Lieferschein schreiben ?
    Unsere ehrliche Antwort aus einer Vielzahl von Projekten. Klingt zwar lächerlich aber ja, machen Sie sich die Mühe.<br>
    **Warum?**<br>
    Wir durften bei einem unserer Anwender feststellen, dass die Kunden (die uns den Schrott abkaufen) auch nur Menschen sind und manches vergessen. Wenn Sie nun einen offenen Lieferschein in Ihrer Lieferscheinliste haben und Sie haben schon zwei Monate lang keine Gutschrift bekommen, so sollten Sie Ihrem Geld so schön langsam mal nachlaufen ;-)). Ist kein Lieferschein mit einem ca. Gewicht geschrieben, so vergessen Sie das schlichtweg. Und das waren schon mal deutlich 5stellige Euro-Beträge.

<a name="DirektfilterAnsprechpartner"></a>

#### Kann ich nach Belegen bestimmter Personen bestimmter Firmen suchen?
Ja. Wenn z.B. bei der Rechnung beim Kunden ![](Direktfilter_Ansp.gif) für die Beschreibung @Aa angezeigt wird, so bedeutet dies, 
dass nach Ansprechpartner bei Firma gesucht werden kann. Also wie im Beispiel heh(enwarter) bei Firma hvg. Wenn Sie nur (mehr) den Namen des Ansprechpartners wissen, so reicht die Eingabe von (Teil-)Name@ und die Firma wird einfach weggelassen. Damit werden alle Belege gelistet, der Ansprechpartner diesen Namen(-steil) enthalten.

#### Für uns ist die Rechnungsadresse auch im Lieferschein sehr wichtig, wie vorgehen?
Aktivieren Sie bitte den Parameter RECHNUNGSADRESSE_IN_LIEFERSCHEINAUSWAHL. Somit wird, nach einem Neustart des Lieferscheinmoduls, in der Auswahlliste auch die Rechnungsadresse angezeigt. Zusätzlich werden die Direktfilter um die Filterangabe der Rechnungsadresse erweitert.

<a name="Vertreter"></a>

#### Wo kommt der Vertreter her?
In den Verkaufsbelegen, Angebot bis Gutschrift, muss auch der sogenannte Vertreter definiert werden. Dieser Vertreter wird entweder durch den aktuell am **Kieselstein ERP** angemeldeten Benutzer oder durch den Provisionsempfänger des Kunden definiert. Sollte der Vertreter aus dem Kunden vorbesetzt werden, so muss der Parameter VERTRETER_VORSCHLAG_AUS_KUNDE auf 1 gestellt sein und es muss der Vertreter (in der Personalverwaltung) eingetreten und nicht versteckt sein. Wenn Sie das Modul Zeiterfassung nicht besitzen, dann reicht es, wenn der Vertreter (im Personalmodul) nicht versteckt ist. Wichtig ist, dass der gewünschte Vertreter aktuell im Unternehmen eingetreten ist.
**Hinweis:**
Prinzipiell greift der Parameter nur bei der Auftragsanlage und der Vertreter des Auftrag wird dann in die Folgebelege übernommen.
AUSSER: Wenn ein Vertreter/Provisionsempfänger nicht mehr "eingetreten" ist oder noch nicht eingetreten ist, dann wird der Vertreter aus dem Kunden neu übernommen.

#### Beim Anlegen des Lieferscheines, Angebotes wird kein Vertreter vorgeschlagen?
Bitte stellen Sie sicher, dass der gewünschte Vertreter aktuell als in Ihrem Unternehmen eingetreten anzusehen ist.
Prüfen Sie gegebenenfalls im Modul Personal das Eintrittsdatum der jeweiligen Person.

#### Können die Texte vom Angebot bis in die Rechnung durchgereicht werden?
Stellen Sie dafür den Parameter KOPFTEXT_UEBERNEHMEN auf 1\. Damit wird bewirkt, dass der Kopftext aus dem Angebot in den Auftrag und von diesem in Lieferschein und da weiter in die Rechnung weiterkopiert wird.

#### Wie kann sehr schnell eine Liste von Artikeln erfasst werden?
Gerade in Unternehmen in denen eher gehandelt wird, also Ware vom Lager möglichst effizient versandt werden sollte, hat sich die Verwendung von Barcodescannern sehr bewährt.
Für Lieferscheine und Rechnungen steht daher auch die Erfassung mittels Barcodescanner zur Verfügung. [Siehe dazu bitte]( {{<relref "/verkauf/lieferschein/#wie-k%c3%b6nnen-lieferscheinpositionen-schnell-erfasst-werden" >}} ).<br>
Um diese praktische Funktion wirklich effizient nutzen zu können, ist eine gute Pflege der Stammdaten (Artikel, Verkaufspreise, Preislisten, Kunden mit Zuordnungen) selbstverständlich.

#### Können AGB mitgedruckt werden?
Ja. [Siehe bitte]( {{<relref "/docs/stammdaten/system/drucken/#wie-k%c3%b6nnen-agb-mitgesandt-bzw-mitgedruckt-werden" >}} ).

#### Wie können Rabatte angedruckt werden?
Der Andruck von Rabatten auf den Bewegungsmodulen ist meist von den bedienten Kundengruppen abhängig. Daher kann der Andruck von Rabatten für Kunden aktiviert bzw. deaktiviert werden. Siehe dazu [bitte]( {{<relref "/docs/stammdaten/kunden/#wie-kann-das-drucken-der-rabatte-gesteuert-werden-" >}} ).<br>
Zusätzlich gibt es immer wieder die Aufgabe, dass obwohl Kunden Rabatte sehen sollten, diese bei verschiedenen Artikelgruppen NICHT angezeigt werden sollten.<br>
Beachten Sie dazu bitte die Definition in den Artikelgruppen, keine VK-Rabatte drucken. D.h. wenn beim Kunden das Andrucken der Rabatte aktiviert ist, wird, wenn bei der Artikelgruppe dies angehakt ist, trotzdem der Rabatt nicht angedruckt.

#### Können die Nachkommastellen eingestellt werden?
Ja. Siehe dazu bitte die entsprechenden Parameter im Modul System, unterer Reiter Parameter und hier dann die Parameter:

| Parameter | Bereich | max. Stellen |
| --- |  --- |  --- |
| PREISERABATTE_UI_NACHKOMMASTELLEN | allgemein | 4 |
| PREISERABATTE_UI_NACHKOMMASTELLEN_EK | Einkauf | 6 |
| PREISERABATTE_UI_NACHKOMMASTELLEN_WE | Wareneingang | 6 |
| PREISERABATTE_UI_NACHKOMMASTELLEN_VK | Verkauf | 4 |

Bitte beachten Sie, dass sich Änderungen, insbesondere im Verkauf aufgrund der damit verbundenen mehr- oder weniger Nachkommastellen auch die Berechnungsergebnisse der Zeilensummen verändern können und es somit zu Warnungen im Bereich der Gesamtsummen kommen kann.
Daher empfehlen wir, offene Belege unmittelbar nach der Änderung der Nachkommastellen, neu zu berechnen. D.h. z.B. im Modul Angebot, im Reiter Positionen die Verkaufspreise zu aktualisieren.
Damit wird die Berechnung auf die nun gültige Nachkommastellenanzahl angepasst.
Info: Zeilensummen sind immer auf zwei Nachkommastellen gerundet.

#### Zusammenfassung der Bedeutung der Statussymbole
In **Kieselstein ERP** werden, vor allem bei den Belegen die verschiedenen Status der Belege mit Symbolen angezeigt.
Zusätzlich wird für jedes Symbol seine Bedeutung mittels Tooltip angezeigt, ![](Status_Angelegt_Tooltip.gif) sodass Sie jederzeit nachsehen können, was denn nun die Bedeutung dieses Symbols ist.
Hinweis: Im Modul System, unterer Modulreiter Sprache, oberer Modulreiter Status finden Sie alle Status die in Ihrem **Kieselstein ERP** definiert sind. Hier können auch Symbole ergänzt bzw. geändert werden.

Oft kommt auch die Frage nach dem Ablauf der Status der verschiedenen Module.
Wir verweisen hier gerne auf die Statusdiagramm, die für jedes Modul beschrieben sind.

Vom Ablauf her sind die Status immer fortschreitend und, soweit möglich, bei allen Modulen gleich.
Hier noch einmal eine Aufstellung der in den Verkaufs / Einkaufsbelegen üblichen Status und deren Bedeutung. Als Symbole wurden die bei uns üblichen Symbole verwendet:

| Status | Symbol | Bedeutung | Entsteht bei | Zurück mit |
| --- |  --- |  --- |  --- |  --- |
| Angelegt | ![](Status_Angelegt.gif) | Beleg ist in der Erfassung Ihr Kunde / Lieferant / Mitarbeiter weis noch nicht, dass es diesen Beleg gibt.In der Regel keine Wirkung in den Auswertungen, aber bereits Wirkung im Lager / Verfügbarkeit | Neuanlage des Beleges |   |
| Offen | ![](Status_Offen.gif) | Beleg wurde gedruckt und damit aktiviert. D.h. Ihr Kunde / Lieferant weiss dass er etwas bekommt, dass er etwas liefern sollte | tatsächliches Drucken aus der kleinen Druckvorschau | Ändern im Beleg |
| teilgeliefert | ![](Status_Teilgeliefert.gif) | Teile des Auftrags sind bereits geliefert | Erstellung eines Lieferscheines / einer Rechnung für den Auftrag | Ändern des Auftrages in den Kopfdaten |
| teilbezahlt | ![](Status_Teilbezahlt.gif) | Die Rechnung (RE/ER) wurde zum Teil bezahlt | Erfassen einer Zahlung | Löschen aller Zahlungen auf der Rechnung |
| bezahlt | ![](Status_Geliefert.gif) | Die Rechnung wurde vollständig bezahlt bzw. als vollständig bezahlt akzeptiert (Siehe Skonto) | Erfassen der Zahlung und anhaken als Erledigt | Löschen der letzten Zahlung auf der Rechnung |
| geliefert | ![](Status_Geliefert.gif) | Status im Lieferschein der besagt dass dieser analog zu offen (aus-)geliefert wurde | tatsächlichem Drucken | Ändern des Lieferscheines |
| verrechnet | ![](Status_Verrechnet.gif) | Status im Lieferschein der besagt dass dieser in einer Rechnung (nicht Proformarechnung) verrechnet wurde) | Bei Übernahme des Lieferscheines in die Rechnung | Löschen des Lieferscheins aus der Rechnungsposition |
| erledigt | ![](Status_Erledigt.gif) | Lieferschein wurde manuell erledigt, Auftrag wurde manuell oder automatisch erledigt | Manuelle Erledigung Je nach Auftragsrechten auch automatisch erledigt wenn alle Positionen des Auftrags geliefert wurden | Klick auf ändern in den Kopfdaten bzw. Menü, Bearbeiten, Erledigung aufheben |

Anmerkung: Kleine Druckvorschau, ist diejenige Ansicht, die beim Drucken des Beleges sofort aufgerufen wird. Für das Aktiveren des Beleges wählen Sie einen der oberen Funktionen (große Druckvorschau, Drucken, Speichern, EMail, Fax). Bitte beachten Sie dazu den Parameter GROSSE_DRUCKVORSCHAU_AKTIVIERT.

<a name="Rechtschreibprüfung"></a>

#### Aktivieren der Rechtschreibprüfung
In den Texteditorfeldern steht auch eine Rechtschreibprüfung zur Verfügung.
Die verwendeten Wörterbücher sind aktuell für die drei deutschsprachigen Dialekte ausgelegt und werden zentral am Server verwaltet.
Um auf Ihrem Client-Rechner der Rechtschreibprüfung zu aktivieren, muss diese über die Arbeitsplatzparameter freigeschaltet werden.
D.h. stellen Sie unter System, Arbeitsplatzparameter, für den jeweils gewünschten Rechner den Arbeitsplatzparameter RECHTSCHREIBPRUEFUNG auf 1\. Bitte schließen und starten Sie danach Ihren Client neu.

![](Rechtschreibpruefung.jpg)

Hintergrund: Die Rechtschreibprüfung benötigt sowohl Speicher als auch Rechenleistung. Da insbesondere in der Fertigung das Augenmerk auf andere Dinge gelegt wird, haben wir uns entschlossen dies aktivieren zu können.

<a name="Dimensionen"></a>

#### Können auch Abmessungen, Dimensionen eingegeben werden?
Durch aktivieren des Parameters DIMENSIONSERFASSUNG_VK auch die Erfassung von Abmessungen in den Verkaufsbelegen zur Verfügung.
D.h. wenn die durch den Artikel definierte Mengeneinheit eine Dimension (1-3) hinterlegt hat, können die Abmessungen und die Stückzahl dieser Abmessungen erfasst werden.
Die Abmessungen werden immer in mm angegeben.
Bitte beachten Sie, dass dies nur eine Rechenhilfe ist. D.h. beim Feldwechsel aus der Dimensionenerfassung wird die Verkaufsmenge aktualisiert. Umgekehrt wird eine Änderung der Verkaufsmenge nicht in die Abmessungen übertragen.

So würde z.B. die Erfassung für ein Blech wie folgt aussehen:
![](Dimensionen.jpg)

- Info1: Derzeit ist für Handeingaben diese Rechenhilfe nicht vorgesehen.
- Info2: Diese Berechnung / Umrechnung ist aktuell für m(Meter) und mm(Millimeter) in allen drei Dimensionen vorgesehen. Aus diesem Titel heraus auch, siehe den Unterschied zu lfm (Laufmeter) welche üblicherweise keine Dimension haben.

#### Können allgemeine Verpackungskosten definiert werden?
In einigen Anwendungen entsteht immer wieder der Wunsch, dass man sich automatisch errechnende Verpackungskosten definieren kann. Dies wird immer als prozentualer Aufschlag auf den Nettowert der Lieferung gesehen.
Wir haben daher im Modul Kunde, Reiter Konditionen unten die Erfassung der Verpackungskosten in % geschaffen.
Voraussetzung für die Nutzung der Verpackungskosten ist auch, dass in den Parametern der Parameter VERPACKUNGSKOSTEN_ARTIKEL definiert ist.
Basis der Berechnung ist, wenn eine Rechnungsadresse Verpackungskosten hinterlegt hat, wird beim Aktivieren eines Angebotes, Auftrags bzw. der Rechnung der Wert des Verpackungskostenartikels als Belegnettosumme errechnet und am Ende des Beleges hinzugefügt.
Eine Übernahme vom Auftrag in den Lieferschein ist nicht vorgesehen, vor allem um bei Teil- bzw. abweichenden Lieferungen die jeweils zum Rechnungswert passenden Verpackungskosten errechnen zu können.
Um nun bestimmte Artikel aus der Berechnung der Transportkostenpauschale auszunehmen, wird dafür im Artikel die Eigenschaft Werbeabgabepflichtig (Reiter Sonstiges) verwendet. Bitten Sie Ihren **Kieselstein ERP** Betreuer dies auf Transportkostenfrei umzubenennen. D.h. alle Artikel bei denen Transportkostenfrei gesetzt ist, werden bei der Berechnung der Transportkostenpauschale NICHT berücksichtigt.

#### Wie weit kann das Belegdatum rückdatiert werden?
Das kommt darauf an.
Grundsätzlich können Belege (Anfragen bis Gutschrift) abhängig von den Benutzerrechten und dem Parameter BEWEGUNGSMODULE_ANLEGEN_BIS_ZUM innerhalb eines Geschäftsjahres beliebig datiert werden. Beachten Sie dazu bitte unbedingt die Vorgaben an die Rechnungslegung, also die chronologische Durchgängigkeit der Belegnummern.
Details siehe bitte [Rückdatieren]( {{<relref "/verkauf/rechnung/#r%C3%BCckdatieren" >}} )

<a name="Bilder hinterlegen"></a>

#### Können Bilder in den Belegen eingefügt werden?
Es können in den Belegpositionen hinter annähernd allen Positionsarten auch Bilder hinterlegt werden.
D.h. bei der Erfassung der Belegposition kann durch Klick auf ![](Bildverwendung.gif) Bild, in die Bildverwendung gewechselt werden.
![](Bildverwendung2.jpg)
Hier steht ein einfacher Editor zur Verfügung, mit dem Bilder in Ihre Belege eingefügt werden können. Bevorzugt für die Positionsarten Ident, Handeingabe und Texteingabe.

Um ein neues Bild einzufügen stehen Ihnen nun folgende Möglichkeiten zur Verfügung:

1.  Ziehen Sie eine Bilddatei einfach in das mittlere leere Feld. Hier wird anhand des Dateitypes (Extension) erkannt, welches Dateiformat gegeben ist und damit auch die Art, des Bildes auf diesen Dateityp gesetzt. Bitte beachten Sie, dass nur die definierten Grafikformate übernommen werden können. Also PNG, JPG, GIF, TIFF.

2.  Wählen Sie die Art des Bildes aus, z.B. PNG, so kann ein Bild über den Dateiauswahldialog
    ![](Bildverwendung3.jpg)
    (Klick auf Datei) ausgewählt werden

3.  Wenn Sie aus einem anderen Programm ein Bild in die Zwischenablage gelegt haben, so kann durch Klick auf das Zwischenablagesymbol, direkt neben der Art, ![](Bild_aus_Zwischenablage.gif) dieses Bild 1:1 übernommen werden.

4.  Wenn Sie eine Datei auf den Neu-Button ![](Bildverwendung_Neues_Bild_hinzufuegen.jpg) ziehen, so wird automatisch angenommen, dass Sie ein weiteres Bild für diese Position mit erfassen möchten. D.h. damit ist es wie wenn Sie auf Neu geklickt hätten und dann das Bild z.B. auf die Art gezogen hätten.

Um nur das Bild zu löschen und danach ein neues einzufügen, klicken Sie auf den Radiergummi ![](Bild_loeschen.gif) neben Datei.
Wichtig: Um den gesamten Bildeintrag zu löschen, verwenden Sie bitte das rote X ![](Bildverwendung4.jpg).

Da mit diesem Bild-Zusatz durchaus mehrere Bilder hinter einer Position hinterlegt werden können können Sie mit ![](Bildverwendung5.jpg) den blauen Pfeilen, bzw. der Auswahlbox bestimmen, welches Bild Sie nun gerade bearbeiten. Zusätzlich kann die Reihung durch die senkrechten blauen Pfeile ![](Bildverwendung6.jpg) gesteuert werden. Mit Neu ![](Bildverwendung7.jpg) wird ein weiteres Bild am Ende eingefügt. Ev. mit den senkrechten blauen Pfeilen an die gewünschte Reihenfolge schieben. mit dem roten X ![](Bildverwendung4.jpg) kann ein / das Bild auch wieder entfernt werden.
Bitte beachten Sie, dass es für die Löschvorgänge kein Undo gibt. D.h. gelöscht bedeutet, das Bild ist unwiederbringlich weg. Ev. würden Sie es in einem Backup noch einmal finden.
Mit der Auswahlbox ![](Bildverwendung7.gif) kann die links rechts Ausrichtung des Bildes in Bezug auf die Formulardefinition gesteuert werden. Bitte beachten Sie dabei, dass in den Standardformularen alle Bilder für eine einheitliche Breite definiert sind. Damit kann es auch vorkommen, dass Texteingaben einen anderen rechten Rand haben, als die Bilder. Sollten Sie hier genauere Definitionen benötigen, so wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

**Hinweis:** Da unsere Anwender immer wieder an solchen Stellen PDFs einfügen möchten.
PDF dient der Seitenbeschreibung eines Druckdokumentes. D.h. ein PDF wird immer eine ganze Druck-Seite abbilden. D.h. dies als Teil einer Positionsinformation mit anzugeben, entspricht nicht der Intention eines PDFs. Unser Vorschlag: Wenn Sie einen Bildausschnitt eines PDFs mitsenden möchten, so kopieren Sie den Bildausschnitt, z.B. mit einem Snipping Tool, in die Zwischenablage und fügen dieses Bild, was dann in der Regel ein PNG oder JPG sein wird, durch Klick auf ![](Bild_aus_Zwischenablage.gif) Zwischenablage wie gewünscht ein.

#### Wie kann ein bestehendes Bild ersetzt werden?
Ziehen Sie einfach auf das bestehende Bild das neue gewünschte Bild. Somit wird das Alte durch das Neue ersetzt.

#### Sind hinter einer Position Bilder hinterlegt oder nicht ?
dies wird durch ein unterschiedliches Aussehen des Mediasymbols, also des Filmstreifens signalisiert

der hinterlegten Bilder angezeigt.

-   Nur Anzeige der Position

    -   Kein Bild hinterlegt. ![](Bildverwendung_kein_Bild.gif) Es wird der schmale Filmstreifen angezeigt

    -   Bild hinterlegt. ![](Bildverwendung_Bild.gif) Der Filmstreifen nutzt den ganzen Platz des Buttons

-   Während der Bearbeitung der Position

    -   Kein Bild hinterlegt. ![](Bildverwendung_bearbeiten_kein_Bild.gif) Schmaler farbiger Filmstreifen.

    -   Bild hinterlegt. ![](Bildverwendung_bearbeiten_Bild.gif) Der Filmstreifen nutzen den ganzen Platz des Buttons und ist grün hinterlegt.

#### Kann man auch Texteingaben vor den Positions-Kopfzeilen andrucken?
Ja das ist möglich. Bitten Sie Ihren **Kieselstein ERP** Betreuer um eine entsprechende Anpassung der anwenderspezifischen Reports.

## Welche Bedeutung haben die Zeichen in der ersten Spalte
Die Bedeutungen der ersten Spalte, haben je nach Modul untercshiedliche Bedeutungen
| Zeichen | Modul(e) | Bedeutung |
| --- | --- | --- |
| R | AB, BS | Rahmenauftrag, bzw. Rahmenbestellung |
| A | AB, BS | Abrufauftrag bzw. Abrufbestellung |
| L | LS | Lieferantenlieferschein |
| M | AB, BS, LS | Mandantenübergreifend |
| m | LS | Lieferschein an einen Mandanten <u>ohne</u> mandantenübergreifender Bestellung

## Belegkopf
Wo kommen die Daten des Belegkopfes her?

Die Daten des Belegkopfes kommen ganz allgemein gesprochen aus dem Beleg, z.B. Rechnungsnummer und aus den Personaldaten des Anlegenden bzw. Versendenden des Beleges.

Hier werden sehr oft die sehr persönlichen Daten der Person, mit den Absenderdaten verwechselt. Wichtig ist, dass in den Belegkopf nur die Absenderdaten übernommen werden, also diejenigen aus dem Modul Personal, Reiter 3 Daten, unterer Bereich Absenderdaten.<br>
![](absenderdaten.png)  <br>
![](belegkopf_unsere_daten.png)  <br>
| Andruck im Belegkopf | Quelle<br>Modul | Reiter | Feldbezeichnung |
| --- | --- | --- | --- |
| Unser Zeichen | Personal | 2 Detail | Kurzzeichen |
| Sachbearbeiter:in | Personal | 2 Detail | Anrede, Titel, Vorname, Nachname, nachgestellter Titel |
| Telefon (links) | System, Mandant | 2 Kopfdaten | Telefon, ohne der Durchwahl für die Zentrale |
| Telefon DW | Personal | 3 Daten | Absenderdaten: Durchwahl |
| E-Mail | Personal | 3 Daten | Absenderdaten: E-Mail |
Damit erreicht man, dass für jeden Versender nur einmalig die Daten im Personal richtig eingestellt werden müssen.

**Hinweis:**<br>
Die Daten im Personal, 2 Detail sind die privaten Daten des/der Mitarbeiter:in. Also wo ist er/sie zu Hause, seine private Handy Nummer usw..

**Hinweis2:**<br>
Selbstverständlich sind diese Daten für jedeN Mitarbeiter:inn und jeden Mandanten einzustellen.

**Hinweis3:**<br>
Wenn der EMail-Versand trotzdem nicht geht, denke auch daran, dass die EMail-Parameter und die Automatikjobs für jeden Mandanten eingerichtet und aktiviert sein müssen. 

## Bilder auf eine Belegposition hinzufügen
Dazu gehört auch die Frage, was bedeutet in der rechten Spalte die ausgegebene Zahl?
In den Positionen der Verkaufsbelege wird rechts
![](medien_anzahl.png) die Anzahl der hinterlegten Medien (Bilder) ausgegeben. Das bedeutet, dass in der Zeile mit der 1 auch tatsächlich Bilder hinterlegt sind.

## Aufbewahrungspflichten
Aus der [GoDB Rz. 119](https://ao.bundesfinanzministerium.de/ao/2021/Anhaenge/BMF-Schreiben-und-gleichlautende-Laendererlasse/Anhang-64/anhang-64.html) bzw. der RZ. 120 (= Beispiel zu 119) steht: *Werden Handels- oder Geschäftsbriefe mit Hilfe eines Fakturierungssystems oder ähnlicher Anwendungen erzeugt, bleiben die elektronischen Daten aufbewahrungspflichtig.* Damit ergibt sich, dass du auf deine ERP Daten auch 7 bzw. **10 Jahre** oder länger nach dem letzten Einsatz zugreifen können musst. Siehe dazu auch [Datensicherung]( {{<relref "/docs/installation/01_server/datensicherung/" >}} )

Das gilt, wenn es streng ausgelegt wird, beginnend beim Anfrage EMail deines Kunden / Lieferanten und endet in den Belegen der Verrechnung.

<u>Mein Tipp:</u> Gerade wenn Rechnungen / Lieferscheine storniert werden, wird bei einer eventuellen Prüfung (verständlicher Weise) davon ausgegangen, dass schwarz geliefert wurde. Dokumentiere daher diese Korrekturen sehr genau und genau auf dem Storno-Beleg ev. auch zusätzlich auf dem Nachfolgebeleg. Das Ziel muss sein, dass du bei einer Prüfung die Frage, was war damit, sofort (innerhalb weniger Minuten) beantworten kannst. Das gilt für dich und auch für deine(n) Nachfolger(innen).