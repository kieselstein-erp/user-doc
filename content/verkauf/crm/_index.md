---
title: "CRM"
linkTitle: "CRM"
weight: 80
description: >
 Customer Relationship Management
---
CRM
===

CRM, Customer Relationship Management, Kunden Beziehungs-Verwaltung.

Da viele unserer Anwender immer wieder nach den CRM Tools in **Kieselstein ERP** fragen, hier eine Liste von Punkten mit denen sowohl operatives als auch analytisches CRM betrieben werden kann.

Die Philosophie des CRM stellt den Kunden in den Mittelpunkt um möglichst viele Informationen zu bündeln und in Zukunft Umsätze zu erzielen. Ziel ist unter anderem ein Aufbau einer gesunden langfristigen Beziehung zu beiderseitigem Vorteil. Die Durchgängigkeit durch die Einbindung in das ERP-System **Kieselstein ERP** bewirkt, dass Sie Daten nur einmal pflegen, keine unterschiedlichen Systeme verwenden und somit der Arbeits- und Zeitaufwand gering bleibt.

Besonders für KMU ist somit diese All-In-One Lösung sinnvoll.

Um Informationen zum Kunden zu dokumentieren und daraus weitere Schritte für die Zusammenarbeit abzuleiten, stehen Ihnen in **Kieselstein ERP** eine Vielfalt von Bausteinen zur Verfügung.

Module die in **Kieselstein ERP** am CRM beteiligt sind:

| Modul | Themen |
| --- |  --- |
| [Projekte]( {{<relref "/verkauf/projekt" >}} ) | Erfassung der Kontakte, Verkaufschancen, Verfolgung des Leads, HistorieTerminverwaltung, Zuordnung zu zuständigen Mitarbeitern.Beachten Sie auch die Einstellung des Parameters PROJEKT_MIT_UMSATZ |
| [Angebote]( {{<relref "/verkauf/angebot" >}} ) | Gewichteter Hoffnungsfall, also Wahrscheinlichkeiten wann welche Umsätze realisiert werden |
| [Aufträge]( {{<relref "/verkauf/auftrag" >}} ) | Offene Auftragspositionen, geplante Erlöse, geplante Deckungsbeiträge |
| [Lieferschein]( {{<relref "/verkauf/lieferschein" >}} ) | Noch nicht verrechnete Lieferungen, Rücknahmen |
| [Rechnung]( {{<relref "/verkauf/rechnung" >}} ) | Noch nicht bezahlte RechnungenWarenlieferungen an den Kunden im ZeitraumZahlungsmoralMahnwesen |
| [Kunden]( {{<relref "/docs/stammdaten/kunden" >}} ) | Definition der ZahlungszieleUmsätze im laufenden Jahr, Umsätze im Vorjahroffene Rechnungen, noch nicht verrechnete LieferscheineStatistik aller Lieferungen, Monatsstatistik (Umsatz im Monat)Umsatzauswertung mit Deckungsbeitragsanalyse (Wo sind die Blumenstraußkunden)Dokumente (z.B. Verträge) zum KundenKundenstammblatt mit Kommunikationsdaten, Umsätzen, [Kontakten]( {{<relref "/docs/stammdaten/partner/kontaktmanagement">}} ), Angeboten, Aufträgen, Kurzbriefe |
| [Partner]( {{<relref "/docs/stammdaten/partner" >}} ) | [Serienbriefe]( {{<relref "/docs/stammdaten/partner/serienbrief">}} ), auch für Aussendungen per EMail, Etiketten u.v.a.m.persönliche Informationen zu wichtigen Partnern. Z.B. Privatadressen der Geschäftsführer, persönliche Vorlieben u.ä.[Telefonanbindung]( {{<relref "/docs/stammdaten/partner/telefonanbindung">}} ) CSV Import (für neue Kundendaten) |
| Ansprechpartner | Newsletter-Empfänger, Funktion |
| [Geodatenanzeige]( {{<relref "/extras/Geodatenanzeige">}} ) | Anzeige der Serienbriefdaten in grafischer Form in Google Maps |
| Dashboard | Kompakte zusammenfassende Darstellung speziell für Ihre Bedürfnisse angepasster Auswertungen |
| [360°Kundensicht]( {{<relref "/management/360°_kundensicht">}} ) | Interaktions Plattform mit Ihren Kunden und Lieferanten auch in Verbindung mit der [HVTE]( {{<relref "/docs/stammdaten/partner/telefonanbindung_eingehend">}} ) |

Der besondere Nutzen liegt darin, dass die wesentlichen Informationen, wie Umsätze, Deckungsbeiträge automatisch durch **Kieselstein ERP** erzeugt werden. Denn: Was haben Sie von einem Kunden mit erheblichem Umsatz, aber sehr geringem Deckungsbeitrag.

Andererseits: natürlich müssen immer wieder neue Kunden aktiv akquiriert werden. Dazu nutzen Sie vor allem das Modul Projekte.

**Adressmanagement und Kontaktverwaltung in **Kieselstein ERP****

Im Modul Partner finden Sie zentral alle Adressen (Kunden, Lieferanten, Interessenten, etc.) und deren Ansprechpartner. Bei einem CRM wird betont, dass die Trennung zwischen Firmen und Personen essentiell ist. Auch das bilden wir in **Kieselstein ERP** ab, was eine Kontaktpflege mit Ansprechpartnern auch nach Wechsel des Unternehmens ermöglicht.
Die Selektionen für Serienbriefe grenzen das Zielpublikum für Aussendungen ein, die Kurzbrieffunktion dokumentiert Ihre direkte Kommunikation mit dem Partner. 
Mit Hilfe der Bemerkung können Sie wichtige Informationen erfassen, die beim Aufruf des Partners sofort sichtbar sind. Interaktionen erfassen Sie im Reiter Kommunikation (E-Mail, Fax, Brief, etc.).
Die Geburtstagsliste (in Journal zu finden) leistet besonders für die persönliche Kommunikation mit Ihren Partnern einen wichtigen Beitrag.
Zeit spart auch die Anbindung an eine Telefonanlage -- das Vertippen beim Wählen fällt weg, ein Klick reicht um die Verbindung mit dem gewünschten Partner aufzubauen.
Sollten Sie nun die gewünschte Person nicht erreichen, so besteht mit dem Toll "Wiedervorlage" eine komfortable Lösung zur Verfügung sich und anderen Erinnerungen zu erstellen. 

Mit dem Haken "Newsletterempfänger" pflegen Sie Ihre Kontakte für den Versand eines [Newsletters]( {{<relref "/docs/stammdaten/partner/serienbrief/#wie-erstellt-man-einen-newsletter" >}} ) über die Serienbrieffunktion.

## Information über den Kunden 

Zur Erfassung von Informationen stehen Ihnen die Module Partner und Kunde zur Verfügung. 
Einen Überblick über die Umsätze, Anzahl der (offenen) Rechnungen, Lieferscheine und die Zahlungsmoral erhalten Sie im Modul Kunden. 
Die A-B-C Einteilung ermittelt nach Ihren Definitionen den unternehmerischen Wert eines Kunden und wird ebenso in den Kopfdaten angezeigt. So erkennen Sie am ersten Blick den aktuellen Stand um die weiteren Schritte zu setzen. 
Zur individuellen Betreuung definieren Sie Sachbearbeiter und dokumentieren die Interaktionen zum Beispiel mittels Kurzbriefen. 
Mit der Drag&Drop Funktion der **Kieselstein ERP** Dokumentenablage fügen Sie Dateien, E-Mails etc. direkt hinzu. 

Einen schnellen Überblick über alle Dokumente, die dem ausgewählten Partner zugehörig sind, erhalten Sie über die [Dokumentenablage]( {{<relref "/docs/stammdaten/system/dokumentenablage/#wie-erh%c3%a4lt-man-einen-%c3%bcberblick-%c3%bcber-alle-dokumente-eines-partners" >}} ).

Weiters ist die umfangreiche Definition von Sonderkonditionen bis zur Ebene des einzelnen Artikels möglich. So können Sie zum Beispiel Kundenpreislisten ausdrucken/verschicken. 

Reklamationsmanagement

Im Modul [Reklamationsmanagement]( {{<relref "/fertigung/reklamation" >}} ) können Sie Reklamationen erfassen, verwalten, nachverfolgen und auswerten. Der 8D Bericht leistet einen großen Beitrag zum Qualitätsmanagement Ihres Unternehmens und somit auch zur Optimierung des Umgangs mit Kundenreklamationen.

Ticketing / Leads

Das Modul Projekte kann zum Beispiel für eine Ticketing-Lösung verwendet werden. Sie erfassen Projekte für Kunden, geben Termine ein, zuständige Mitarbeiter usw. die einzelnen Stati und Bereiche können Sie definieren. Eine andere Verwendungsart ist das Lead-Management. Den Status von Leads können Sie ebenso hier dokumentieren und überwachen. Durch die Filtermöglichkeiten in der Auswahlliste erhalten Sie einen Überblick über den Status von/der Projekte/Leads.
Die individuellen Anpassungsmöglichkeiten sprechen Sie am Besten mit Ihrem **Kieselstein ERP** Betreuer ab, dieser findet die passende Lösung für Ihr Unternehmen.

Termine/Terminüberwachung

Integriert in den Modulen finden Sie die unterschiedlichen Ausprägungen der Terminverwaltung. So gewinnen Sie Übersicht über Auftrags- und Liefertermine, Fertigungsauslastungen, optimale Bestelltermine, Mitarbeiterverfügbarkeiten und Vieles mehr. Auch die schon erwähnte Wiedervorlage hilft Ihnen bei der Überwachung offener Punkte. 

Analysen, Controlling, Reporting

Die umfangreiche Einbindung von Reports zu einzelnen Daten (Info) und gesammelten (Journale) ist über alle Module hinweg gegeben. 
Verkaufszahlen / Reklamationszahlen / Umsätze / Deckungsbeiträge etc. können abgerufen werden. 
Diese Werkzeuge wie Statistiken und Auswertungen schaffen eine Basis für kurzfristige und langfristige Optimierung, Weiterentwicklung und Wachstum Ihres Unternehmens.

Welche Werkzeuge stehen für eine Beurteilung der Entwicklung der Kunden aber auch des Unternehmens zur Verfügung?

In **Kieselstein ERP** steht eine Vielzahl von Auswertungen für die Entwicklung sowohl Ihres eigenen Unternehmens, als auch der Kunden bzw. eines einzelnen Kunden zur Verfügung.
Abhängig von der Art und Weise Ihrer Datenerfassung, stehen hier auch noch die Auswertungen bis zum Deckungsbeitrag I, II, III zur Verfügung, getreu dem Motto, Umsatz macht Arbeit und Deckungsbeitrag macht Freude.
Welche Auswertungen könnten nun dafür herangezogen werden:

| Modul, Menüpunkt | Beschreibung |
| --- |  --- |
| Fibu: Journal, Einfache Erfolgsrechnung | eine sehr sehr einfache Erfolgsrechnung, aber mit Personalkosten, Lagerständen und ähnlichem |
|  Fibu: Journal, Erfolgsrechnung | Salden des gewählten Monats, im Vergleich zu den Salden des Vorjahres<br>Kumulierte Salden von GF-Jahr Beginn zu kumulierte Salden des Vorjahres |
| Fibu: Journal, Liquiditätsvorschau | die Liquiditätsvorschau mit den verschiedenen zusätzlichen Betrachtungen<br>Hat natürlich mit den Umsatzentwicklungen nichts zu tun. |
| Fibu: KPI | Key Performance Indikator<br>Für Ihr Unternehmen. Muss in der Regel auf Ihr Unternehmen angepasst werden. Liefert Auftragseingang, Fertigungs-Ablieferung, Reklamation, Rechnungsausgang im Zeitraum, üblicherweise die letzte Kalenderwoche |
|  Rechnung: Journal Umsatz | liefert die monatlichen Umsätze des aktuellen Jahres und des Vorjahres und wenn gewünscht die Entwicklung der offenen Posten je Monatsletztem |
| Kunde: Journal Umsatzstatistik | entweder als Betrachtung eines Zeitraumes mit Auflösung nach Artikelgruppen oder -klassen oder auch als Mehrjahresentwicklung |
| Kunde: Einzelnen Kunden auswählen, Info Lieferstatistik | liefert die einzelnen Komponenten die im Zeitraum geliefert wurden. Achtung auf eingeschränkt, bringt nur die 50 jüngsten Datensätze (ev. den Haken entfernen) |
| Kunde: Einzelnen Kunden auswählen, Info Lieferstatistik und Monatsstatistik anhaken | Liefert eine Auflistung der Umsätze des Kunden pro Monat für den gewünschten Zeitraum |
| Kunde: Einzelnen Kunden auswählen, Info Stammblatt | Liefert unter anderem auch die jährliche Entwicklung<br>Eine ähnliche Umsatzdarstellung finden Sie im Reiter 8 Umsatzübersicht |
| Artikel, Journal, Artikelgruppe | Eine Auswertung der Umsätze und Deckungsbeiträge im Zeitraum mit Details zu Artikel(Haupt-)Gruppen |
| Artikel, Journal, Hitliste | Eine Auswertung der verkauften Artikel im Zeitraum mit verschiedenen Sortierungen z.B. nach dem höchsten Deckungsbeitrag, dem höchsten Verkaufswert u.ä.. |
| Dashboard | Eine Sammlung von für Sie relevanten Informationen sowohl zur Umsatzentwicklung als auch zu einzelnen Modulen. |

Bitte beachten Sie, dass die Verfügbarkeit der Module auch von den in Ihrer **Kieselstein ERP** Installation freigeschalteten Modulen abhängig ist.