---
categories: ["Angebotsstückliste"]
tags: ["Einkaufsangebotsstückliste"]
title: "Einkaufsangebotsstückliste"
linkTitle: "Einkaufsangebotsstückliste"
weight: 20
date: 2023-01-31
description: >
  Angebotsstücklisten mit Einkaufsmengenstaffeln erfassen und auswerten
---
Einkaufs-Angebotsstückliste
---------------------------------------------------------------------

Mit dem Zusatzmodul der Einkaufs-Angebotsstückliste steht Ihnen in **Kieselstein ERP** ein komfortables Werkzeug für die Vorkalkulation mit Mengenstaffeln zur Verfügung. Klicken Sie dazu im Modul Angebotsstückliste auf den unteren Modulreiter Einkaufsangebot.

Für die ergänzende Funktion des weiteren Moduls der [Einkaufs-Anfrage-Stückliste siehe bitte]( {{<relref "einkaufsanfragestueckliste" >}} )

In den Kopfdaten definieren Sie den Kunden inkl. Ansprechpartner, das Projekt und bis zu 5 verschiedene Mengen (Satzgrößen) für die Stückliste.
Unter Positionen pflegen Sie die entsprechenden Artikel bzw. Handeingaben ein und vergeben für jede Position den entsprechenden Einkaufspreis je Mengeneinheit (Stück) passend zur entsprechenden Menge(nstaffel).

![](EK-Angstkl2.gif)

In der Zeile Preis wird die Preiseingabe für die entsprechende Mengenstaffel signalisiert.

Unter Volumen sehen Sie das Volumen der jeweiligen Position, welches sich aus der Multiplikation der Menge mit der jeweiligen Satzgröße ergibt.

In der darunter liegenden Zeile geben Sie nun die Einkaufspreise für das jeweils benötigte Volumen an.

Tragen Sie gegebenenfalls auch die weiteren Werte ein.

Da es immer wieder vorkommt, dass zu einzelnen Positionen auch längere Kommentare hinterlegt werden müssen, können mit dem oberen Modulreiter Kommentar für die ausgewählte Positionszeile zwei Kommentarfelder befüllt werden.

Auf Wunsch werden diese beim Ausdruck mit angeführt.

#### Kann eine Einkaufs-Angebotsstückliste importiert werden?
Ja.

Auf für die Einkaufsangebotsstückliste steht mit dem Icon ![](csv_import.PNG) ein CSV Import zur Verfügung.

Der Datenaufbau dafür ist folgender:

Ident;Menge;Bezeichnung;Mengeneinheit;Position;Referenzbemerkung

|Spalte | Beschreibung |
| --- |  --- |
| Ident | Artikelnummer des Artikels |
| Menge | Menge dieser Position |
| Bezeichnung | Bezeichnung des Artikels, wirkt nur, wenn die Artikelnummer im Artikelstamm nicht gefunden wurde. Z.B. für Handeingaben  |
| Mengeneinheit | Mengeneinheit der Stücklistenposition.Dies muss eine gültige und passende Mengeneinheit sein und wirkt nur wenn Handeingaben importiert werden |
| Position | Text für das Feld Position |
| Referenzbemerkung | Text für das Feld Referenzbemerkung |

**Hinweis:** Die zu übernehmenden Artikel müssen im Artikelstamm angelegt sein. Ist dem nicht so, so wird die Position trotzdem übernommen und als Handeingabe eingetragen.

Zusätzlich steht für das **Kieselstein ERP** auch der [Intelligente Stücklisten-Import]( {{<relref "/warenwirtschaft/stueckliste/importe/intelligent">}} ) zur Verfügung.
