---
title: "Angebotsstückliste"
linkTitle: "Angebotsstückliste"
categories: ["Angebotsstückliste"]
tags: ["Angebotsstückliste"]
weight: 35
description: >
  Verkaufsstücklisten (nur) für Angebote erstellen, inkl. Mengenstaffeln
---
Angebotsstückliste
==================

#### Was unterscheidet die Angebotsstückliste von der Stückliste
Stücklisten dienen als Grundlage für die Fertigung eines Artikels und sie sind Artikel orientiert. Das bedeutet, dass die Stückliste als Artikel bereits definiert ist.
Wenn es noch unklar ist, ob Sie den Auftrag erhalten und Sie Ihren Artikelstamm nicht unnötig erweitern wollen, ist die Angebotsstückliste die richtige Wahl.
Bei der Angebotsstückliste können Sie bei der Angabe von bereits vorhandenen Artikeln bzw. von Handeingaben den Gestehungspreis manuell hinzufügen, ohne die Daten im Artikel zu verändern.

Module der Angebotsstücklisten
------------------------------

In Ihrer Ausprägung gibt es derzeit drei verschiedene Angebotsstücklisten:

-   die klassische [Angebotsstückliste](#Angebotsstückliste), [Schnellerfassung]( {{<relref "schnellerfassung" >}} )

-   die [Einkaufsangebotsstückliste]( {{<relref "einkaufsangebotsstueckliste" >}} )

-   die [Einkaufs-Anfrage-Angebotsstückliste]( {{<relref "einkaufsanfragestueckliste" >}} )

## Die klassische Angebotsstückliste

#### Preiserfassung in der Angebotsstückliste
In der Angebotsstückliste können grundsätzlich zwei Arten von Preis erfasst werden.
1. Der Verkaufspreis für diese Position pro Stück (Mengeneinheit). Dieser wird bei der Positionsart Ident(Artikel) anhand der beim jeweiligen Artikel hinterlegten Verkaufspreise ermittelt.
2. Der Gestehungspreis dieser Position ebenfalls pro Mengeneinheit.
Die Differenz der beiden Eingaben stellen den Deckungsbeitrag dieser einen Position dar.

Bitte beachten Sie, dass die Übernahme in die Angebotsstückliste nur für die Verkaufspreise erfolgt. Eine automatische Übernahme der Gestehungspreise in die Verkaufspreise ist nicht gewollt, um ihnen die erforderliche Freiheit bei der richtigen Kalkulation Ihrer Produkte zu belassen.

#### Übernahme des Wertes der Angebotsstückliste in ein Angebot
Wird die Angebotsstückliste in ein Angebot übernommen, so wird der Verkaufswert der Angebotsstücklistenpositionen aufsummiert und dann als kalkulatorischer Wert und bei neu übernommenen Angebotsstücklisten auch als vorgeschlagener Verkaufspreis in die Angebotsposition übernommen.
Bitte beachten Sie, dass in der Angebotsvorkalkulation beide Preise, also der Gestehungspreis und der Verkaufspreis aufgelistet werden um Ihnen die Kostenermittlung möglichst transparent darzustellen.

#### Kann die Angebotsstückliste in die Stückliste importiert werden?
Nutze dafür die direkte Übertragung der Angebotsstückliste in die Stückliste. [Siehe]( {{<relref "/verkauf/angebotsstueckliste/#wie-wird-eine-angebotsst%c3%bcckliste-in-eine-st%c3%bcckliste-%c3%bcbertragen" >}} )

**<a name="kalkulationsart"></a>Kalkulation in der Angebotsstückliste**

Der Parameter KALKULATIONSART (1 = Verkaufspreisbezug, 2 = Einkaufspreisbezug, 3=Einkauf mit Aufschlag ) steuert die Kalkulationsmöglichkeiten. 

**Kalkulationsart 1: Verkaufspreisbezug**

In der Kalkulationsart 1 erfassen Sie rein die Verkaufspreise der jeweiligen Artikel. D.h. es ist (nur) eine Zusammenstellung von Komponenten, welche Sie als eine Stückliste Ihrem Kunden anbieten.

**Kalkulationsart 2: Einkaufspreisbezug**

Wenn der Parameter auf 2, steht Ihnen eine komfortable Möglichkeit zur Verfügung um vom Einkaufspreis ausgehend mit Aufschlägen das Angebot zu kalkulieren. In den Grunddaten können Sie die Aufschläge definieren, die sich entweder auf Material oder auf Zeit beziehen. 
Info: Setzen Sie den Haken bei Material, so bezieht sich der Aufschlag auf Materialpositionen. Wenn Sie den Haken nicht setzen, auf Zeitpositionen.

Sie erhalten einen neuen oberen Reiter Aufschlag, hier finden Sie die Eingabe Maske für die Aufschläge.

 ![](agstkl_aufschlag.jpg)
Nun können Sie die Beträge der Aufschläge einzeln eingeben und erkennen sofort die Auswirkungen auf den Verkaufspreis. Die Materialpreise, Arbeitszeit und damit zusammenhängende Aufschläge sind einzeln angeführt und das Ergebnis wird berechnet. Weiters erkennen Sie, welche Aufschläge definiert sind. 
Das gemeinsame Arbeiten an Angeboten wird dadurch erheblich erleichtert und die Nachvollziehbarkeit erhöht. Besonders nützlich ist die Kalkulation für weitere Verhandlungen mit den Auftraggebern, da Sie einen Überblick über die einzelnen kalkulierten Aufschläge zum aktuellen Zeitpunkt haben.

<a name="Einkaufspreis"></a>

#### Wo kommt bei der Kalkulationsart 2 der Preis her?
Sie haben bei der Kalkulationsart 2 die Möglichkeit, je Angebotsstückliste zu definieren, welche Preisbasis für die "Einkaufs-"Kalkulation verwendet wird.

Wurde in den Kopfdaten
![](agstkl_ekpreisbasis.jpg)
Lief1Preis definiert, so bedeutet dies, dass zur Basis des Datums der Angebotsstückliste der Preis des bevorzugten Lieferanten, der sogenannte Lief1Preis verwendet wird.
Steht diese Auswahl auf Nettopreis, so kann der Preis je Angebotsstücklistenposition eingegeben werden.

Ähnliches gilt für die Kalkulationsart 3\. Auch hier bedeutet, wenn die EK-Preisbasis auf Lief1Preis steht, dass der Preis direkt aus dem Lief1Preis kommt, unter Berücksichtigung des Gültig-ab des jeweiligen Lieferantenpreises.

**Kalkulationsart 3: Einkauf mit Aufschlag**

Wenn der Parameter auf 3 steht, so wird in der Anzeige der Positionen auch der gelieferte Wareneingangspreis (= letzter Einkaufspreis) und das Wareneingangsdatum (= letztes Einkaufsdatum) angezeigt. So können Sie zum Beispiel erkennen, dass die Lieferung der zweiten Position im Beispiel das letzte Mal 2011 erfolgte und eventuell eine Anfrage bei Ihrem Lieferanten sinnvoll sein kann.
![](kalkulationsart3.JPG)
Weiters kann pro Position ein Aufschlag definiert werden, der Vorschlagswert wird im Parameter DEFAULT_AUFSCHLAG in Prozent definiert. Sie können diesen in jeder Position entsprechend anpassen.
![](kalkulationsart3_aufschlag.JPG)

Der Gesamtaufschlag je Material und Arbeitszeit, wie in der Kalkulationsart 2 beschrieben, ist ebenso in der Kalkulationsart 3 verfügbar. Ist der Parameter DEFAULT_AUFSCHLAG auf -1 gestellt, dann wird vom Kunden-VK-Preis der Aufschlag rückgerechnet. Die VK-Maschinenkosten können in den Maschinenkosten definiert werden (Modul Zeiterfassung, unterer Reiter Maschinen, oberer Reiter Kosten).

#### Wo kommt bei der Kalkulationsart 3 der Einkaufspreis her?
Für den oberen Bereich der Erfassung
![](agstkl_kalk3_ekpreis.jpg)
gilt ebenfalls, dass abhängig von der Einstellung der EK-Preisbasis in den Kopfdaten der Preis direkt aus dem bevorzugten Lieferanten kommt oder manuell erfasst werden kann. [Siehe](#Einkaufspreis).

Der Radiobutton hat bei der Kalkulationsart 1 keine Auswirkung. Bei 2 und auch bei 3 ist die Basis ja der Preis zu dem Sie das Material bekommen und bei KalkArt 3 wird dann mit Aufschlag weitergerechnet. So wird bei Radio auf Lief1Preis fix der Preis aus dem Artikellieferanten genommen (und ist nicht änderbar). Bei der Einstellung auf Nettopreis können / müssen Sie den Einkaufspreis der jeweiligen Materialposition eingeben.

#### Wie kann die Angebotsstückliste als detaillierte Kalkulation verwendet werden?
Wenn die Zusatzfunktion Mengenstaffeln in Ihrer **Kieselstein ERP** Installation freigeschalten ist, so erhalten Sie einen zusätzlichen Reiter Arbeitsplan ![](agstkl_arbeitsplan.JPG) in der Angebotsstückliste. 
Hier hinterlegen Sie die Positionen mit Rüst-und Stückzeit. Im weiteren Reiter Mengenstaffeln ![](agstkl_mengenstaffel.png) können Sie nun mit Klick auf neu eine weitere Mengenstaffel hinzufügen. Sobald Sie auf Speichern klicken, werden die Kosten und dadurch die möglichen Verkaufspreise errechnet. 
![](agstkl_mengenstaffeln.JPG)
Mit dem Ausdruck der Angebotsstückliste, erhalten Sie somit eine umfangreiche Dokumentation der Kalkulation der Angebotsstückliste mit den Auswirkungen auf den Preis je nach produzierter Menge.

{{% alert title="Hinweis" color="info" %}}
Ist in deiner Installation auch die Angebotsstücklistenschnellerfassung aktiviert, so können die Mengenstaffeln der Angebotsstücklisten nur aus dem Angebot heraus mit der Schnellerfassung bearbeitet werden.
{{% /alert %}}


#### Anhand welcher Zahlen wird der Verkaufspreis ermittelt?
Für die Arbeitszeit-Artikel werden die im Artikelstamm hinterlegten Verkaufspreise.
Für die Maschinenzeiten werden die definierten Kosten, genauer der VK Stundensatz der Maschine (Modul Zeiterfassung, unterer Reiter Maschinen, oberer Reiter Kosten) herangezogen.
Bitte beachten Sie, dass immer auch die Kalenderfähigkeit der Preise berücksichtigt wird. Also die Preise zum Belegdatum der Angebotsstückliste verwendet werden.

Für das Material wirkt zusätzlich auch der Parameter der Kalkulationsart.
D.h. in der Spalte VKPreis AgStkl wird der NettoVKPreis der Position berücksichtigt, welcher nur bei den Kalkulationsarten 1 und 3 erfasst werden kann.
In der Spalte VKPreis KD-Preisfindung wird der Verkaufspreis des Materials aus dem Artikelstamm berücksichtigt. Dieser Wert steht nur für Artikel zur Verfügung.
D.h. wenn Sie in der Regel Handartikel verwenden, sollten Sie die Kalkulationsart 3 verwenden.

#### kann der Verkaufspreis automatisch übernommen werden?
Mit dem Parameter VKPREIS_BEI_MENGENSTAFFEL_AUTOMATISCH kann definiert werden, dass der bei der Neuerfassung einer Mengenstaffel automatisch errechnete Verkaufspreis als gewählter Verkaufspreis vorgeschlagen wird. Steht dieser auf 1, so wird der Verkaufspreis aus den Angebotsstücklistenpositionen verwendet. Steht er auf 2, so wird der anhand der Verkaufspreisfindung, also den Kundenpreisen errechnete Verkaufspreis verwendet. Bei 0 ist er abgeschaltet.

#### Können Positionen der Angebotsstückliste im Angebot mitgedruckt werden?
Ja. Wenn Sie in der Angebotsstückliste das ![](AufBelegMitdrucken.gif) Auf Beleg mitdrucken, anhaken, so bedeutet dies, dass auf dem Angebotsdruck diese Positionszeile mit Artikelnummer, Bezeichnung, Menge mit ausgedruckt wird. Wenn nun auch noch der Preis an den Kunden kommuniziert werden sollte, so haken Sie bitte auch ![](PreiseMitdrucken.gif) an.
Bitte beachten Sie, dass bei der Überleitung der Angebotsstückliste in eine normale Stückliste, die Information des Preise mitdrucken nicht übernommen wird.

#### Wie wird eine Angebotsstückliste in eine Stückliste übertragen?
<a name="AngStkl in eine Stkl übertragen"></a>
Wenn es zum Auftrag kommt, müssen Sie in der Regel die angebotenen Baugruppen auch produzierten. Als Vorlage dient dafür oft die bereits erstellte Angebotsstückliste. D.h. bevor Sie aus dem Angebot einen Auftrag anlegen, sollte die Angebotsstückliste in eine Stückliste gewandelt werden. Nutzen Sie dafür den Knopf ![](Stueckliste_aus_Angebotsstueckliste.gif) eine Stückliste aus der Angebotsstückliste erzeugen. Bevor Sie dies machen, sollten Sie alle Handeingaben in echte Artikel umwandeln. Siehe dazu Bearbeiten, Handartikel in Artikel umwandeln.
Wird nun die Angebotsstückliste in eine echte Stückliste kopiert, so muss die Artikelnummer der Stückliste eingegeben werden. Das Projekt, das die Bezeichnung der Angebotsstückliste ist, wird in die Bezeichnung des Artikels übernommen. Es werden sowohl die Positionen als auch der Arbeitsplan übernommen.
Sind bei der Angebotsstückliste Mengenstaffeln hinterlegt, so werden die Positionen bei denen ein gewählter Verkaufspreis hinterlegt ist auch als Verkaufs-Mengenstaffel angelegt.
Daran anschließend kann nun aus dem Angebot der Auftrag erzeugt werden. Sollten Sie vergessen haben aus der Angebotsstückliste eine Stückliste zu machen, erscheint ein entsprechender Hinweis.
Bitte beachten Sie, dass dies eine einmalige Kopie ist. Eventuelle Änderungen in der Angebotsstückliste nach dem kopieren in die Stückliste haben keine weitere Wirkung mehr. D.h. ab Auftragserteilung gilt natürlich nur mehr das in den Stücklisten erfasste.

### Angebotsstücklisten Schnellerfassung
Mit der Angebotsstücklistenschnellanlage steht ein praktische Werkzeug für eine rasche Kalkulation von neuen Produkten, wie sie gerade in der Metallverarbeitung genutzt werden zur Verfügung. Der Gedankengang dahinter ist, dass über verschiedene Vorlagen unterschiedliche Bearbeitungsvarianten definiert werden können und man durch Angabe weniger Daten sehr schnell zu einem aussagekräftigen Verkaufspreis kommt.
[Siehe dazu bitte]( {{<relref "schnellerfassung"  >}} )


