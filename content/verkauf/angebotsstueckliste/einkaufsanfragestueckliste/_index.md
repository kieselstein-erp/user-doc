---
categories: ["Angebotsstückliste"]
tags: ["Einkaufsanfragestückliste"]
title: "Einkaufsanfragestückliste"
linkTitle: "Einkaufsanfragestückliste"
weight: 30
date: 2023-01-31
description: >
  Effizient Angebote von kooperativen Lieferanten einholen
---
Einkaufs-Anfrage-Stückliste
===========================

Die Einkaufs-Anfrage-Stückliste ist als praktische Erweiterung der Einkaufsangebotsstückliste, auch in Verbindung mit dem intelligenten Stücklistenimport insbesondere für die Elektronik Produktion gedacht. D.h. damit erfassen Sie einerseits die Bauteileliste (BOM) Ihrer Kundenanfragen, hinterlegen bei welchen Lieferanten Sie diese Artikel anfragen wollen, senden den Lieferanten automatisiert entsprechende Anfrage-EMails mit entsprechenden XLS Dateien. Ihre Lieferanten pflegen nun die Angebotspreise in diese XLS Dateien ein und senden Ihnen diese wiederum zu. Sie importieren diese und entscheiden mit wenigen Klicks bei welchen Lieferanten Sie die Bauteile denn wirklich kaufen wollen.<br>
Diese Daten sind nun die Basis für Ihr Angebot. Bei Bestellung des Projektes durch Ihren Kunden importieren Sie die Daten in den Bestellvorschlag und erzeugen so die entsprechenden Bestellungen für die Materialbeschaffung.
Sie finden dieses praktische Unter-Modul im Modul Angebotsstückliste ![](EkAngStkl.jpg) unterer Reiter ![](EkAnfStkl.gif) Einkaufsangebot.

Bitte beachten Sie, dass für die Bearbeitung der Daten ein ausreichend großer Bildschirm, im Sinne der Auflösung, erforderlich ist.

#### Wie ist der generelle Ablauf für die Bearbeitung der Einkaufs-Anfrage-Stückliste?
Dieser gliedert sich, über den gesamten Beschaffungsprozess hin wie folgt:

1.  Anlegen einer neuen Einkaufsanfragestückliste mit Angabe des Kunden, der Projektnummer, den gewünschten Mengenstaffeln, dem geplanten Fertigungstermin

2.  Erfassung der eigentlichen Stückliste zur Basis ein Satz im Reiter 3 Positionen. Nutzen Sie dafür auch den [intelligenten Stücklistenimport]( {{<relref "/warenwirtschaft/stueckliste/importe/intelligent">}} ) ![](intelligenter_stkl_import.gif)

3.  Definieren Sie nun im Reiter 5 Lieferant die Lieferanten an die Sie diese Anfrage Stückliste senden möchten. Nutzen Sie dafür auch die Möglichkeit ![](Einkaufsgruppe_Importieren.gif) Lieferanten aus vordefinierten Einkaufsgruppen übernehmen zu können. Sollte das Angebot Ihres Lieferanten in einer anderen Währung als Ihrer Unternehmenswährung erfolgen, so definieren Sie bitte diese abweichende Währung. [Siehe auch](#Wechselkurs).

4.  Mit ![](XLS_fuer_einen_Lieferanten_erzeugen.gif)erzeugen Sie aus der Einkaufsanfragestückliste eine entsprechende [XLS Datei](#wie-sieht-die-datei-aus-die-den-lieferanten-gesandt-wird) für den gewählten / selektierten Lieferanten.
    Dies kann zur Prüfung des Inhaltes usw. genutzt werden

5.  Mit ![](EMail_an_Lieferanten_senden.gif) erzeugen Sie EMails mit den [XLS Daten](#wie-sieht-die-datei-aus-die-den-lieferanten-gesandt-wird) für jeden der gewählten / aller hinterlegten Lieferanten
    ![](EK-Anfrage_Stkl_AnfrageabgabeTermin.jpg)

    Danach kommt die Abfrage
    ![](EK-Anfrage_Stkl_Anfrage_EMail-Text.jpg)
    welcher Text an jedes Anfrage-EMail mitgegeben werden sollte.
    Info: Diese Texte können passend zu den von Ihnen gewünschten Formulierungen entsprechend übersteuert werden.
    Siehe dazu as_egakmail.*

6.  Nun werden die übersandten XLS Dateien von Ihren Lieferanten mit den Angebotswerten befüllt und an Sie zurückgesandt.
    Wichtig ist, dass die Struktur der Daten erhalten bleibt.
    Speichern Sie die erhaltenen Angebots-XLS Dateien entsprechend in einem (Projekt-) Verzeichnis ab.

7.  Mit ![](EK-Anfrage_Stkl_Lieferantendaten_einlesen.gif) lesen Sie ebenfalls im Reiter 5 Lieferanten das von jeden einzelnen Lieferanten erhaltene Angebots-XLS ein.

8.  in der Kurzfassung kann nun im [Reiter 8 Preise optimieren](#wie-findet-man-den-passenden-lieferanten-kommt-zu-den-optimalen-preisen), der optimale Lieferant für jede angefragte Position ermittelt werden. [Details siehe bitte unten](#wie-findet-man-den-passenden-lieferanten-kommt-zu-den-optimalen-preisen).

9.  Haben Sie die optimale Konstellation gefunden, so können im Reiter 8 die gewählte Artikel-Lieferanten-Konstellation mit Klick auf ![](EK_Anf_Stkl_Preise_fixieren.gif) die Preise fixiert werden.

10. Um eine kompakte Zusammenfassung der gewählten Preiskonstellation zu finden, kann unter Vergleich
    ![](EK_Ang_Stkl_Vergleich.gif)
    In der Reportvariante
    ![](EK_Ang_Stkl_Bestelldaten.jpg)
    Bestelldaten eine kompakte Übersicht der als zu bestellen markierten Artikel-Lieferanten ausgedruckt werden.
    Hier werden auch die bei den einzelnen Positionen angegebenen Kommentare mit ausgedruckt.
    Wird diese Zusammenfassung freigegeben / von Ihrem Kunden bestellt, so

11. Wandeln Sie eventuelle Handeingaben in Artikel um. Es stehen dafür die Funktionen
    ![](EK_Ang_Stkl_Handeingabe_wandeln.gif)
    Handartikel in (einen neuen) Artikel umwandeln bzw. Handartikel durch (einen bereits bestehenden) Artikel ersetzen zur Verfügung.
    Danach

12. kann dieses Einkaufs-Anfrage-Stücklisten (Projekt) in den Bestellvorschlag übernommen und somit bestellt werden.
    Wechseln Sie dazu in das Modul Bestellung, unterer Reiter Bestellvorschlag und wählen dann
    einen neuen Bestellvorschlag anhand Einkaufsangebot(sstückliste) ![](EK_Ang_Stkl_BestVor_erzeugen.jpg) erstellen.
    Wählen Sie dazu die gewünschte EinkaufsAngebotsStückliste aus. Wählen Sie die Staffelmenge aus, für die der Bestellvorschlag errechnet werden sollte.
    Bestätigen Sie dann den geplanten Fertigungstermin und geben Sie die Vorlaufzeit an. Vorlaufzeit ist die Zeit um die die Ware vor dem geplanten Fertigungstermin bei Ihnen eintreffen sollte.
    ![](EK_Ang_Stkl_BestVor_uebernehmen.gif)
    Mit diesem Lauf werden die Positionen bei denen Bestellt angegeben ist mit dem gewählten Lieferanten in den Bestellvorschlag übernommen.
    Gegebenenfalls besteht noch die Möglichkeit diese Positionen des Bestellvorschlages zu bearbeiten, zu ergänzen. Beachten Sie bitte dazu den Punkt Nachkalkulation.

13. In der Regel wird der Bestellvorschlag durch Klick auf ![](EK_Ang_Stkl_BestVor_Bestellung_erzeugen.gif) in Bestellungen übergeleitet. Um in einem Lauf Bestellungen für alle Lieferanten des Bestellvorschlages zu erzeugen, haken Sie
    ![](EK_Ang_Stkl_BestVor_Bestellung_alle_Lieferanten.gif)
    je Lieferant eine Bestellung anlegen an und klicken auf den grünen Haken um diese Funktion auszuführen.

    Info: Bei diesem Lauf wird, wenn die Daten aus der EinkaufsAngebotsStückliste kommen, die Projektbezeichnung aus der Einkaufsangebotsstückliste in die Kopfdaten der Bestellungen übernommen (siehe Nachkalkulation)
    Gegebenenfalls wählen Sie vorher Abrufbestellung zu Rahmenbestellung(en) erzeugen.
    Hier bleiben diejenigen Positionen im Bestellvorschlag erhalten, für die keine Abrufbestellungen durchgeführt werden konnten.

14. Aus der Bestellungsauswahl heraus, versenden Sie nun mit Bestellung, alle angelegten drucken
    ![](EK_Ang_Stkl_Bestellungen_versenden.gif)
    alle Bestellungen die im Status angelegt sind, auf einmal an alle Lieferanten.
    Bitte beachten Sie, dass davon **ALLE** Bestellungen betroffen sind, die in diesem Status sind.

15. Nachkalkulation, genauer eine Prüfung ob denn nun wirklich alles zum damals gewünschten Preis bestellt wurde, finden Sie nun wiederum im Modul Einkaufs-Angebots-Stückliste unter
    ![](EK_Ang_Stkl_Vergleich.gif) Einkaufsangebot, Drucken und wählen Sie hier die Reportvariante ![](EK_Ang_Stkl_Druck_Nachkalkulation.jpg)

    Hier werden, anhand der Projektnummer, das ist genauer die manuell beim Anlegen der Einkaufs-Anfrage-Stückliste eingegebene Zahl, welche auch in der Bestellung in den Kopfdaten vorhanden sein muss, die Bestellpreise angeführt. Die Erweiterung auf die tatsächlichen Wareneinstandspreise ist durchaus denkbar.

#### Welche Bewandtnis hat es mit dem Projekt der Einkaufsanfragestückliste?
Das Projekt wird, bei der Bestellung, der Einkaufsanfragestückliste über den Bestellvorschlag bis in die einzelne Bestellung durchgereicht. Um nun auch ermitteln zu können, zu welchen Preisen den nun tatsächlich bestellt wurde, wird das Projekt herangezogen. D.h. es werden in der Überleitung des Bestellvorschlages in die einzelnen Bestellungen das Projekt automatisch mit übernommen. Umgekehrt wird bei der sogenannten Nachkalkulation die für jeden einzelnen Artikel relevante Bestellung anhand der Artikelnummer und des Projektes, genauer der nummerischen Stellen des Projektes herangezogen und so im Druck der Nachkalkulation mit aufgeführt.

#### Was passiert wenn ein Lieferant eine Zeile herauslöscht?
Der Import der XLS Dateien basiert darauf, dass einerseits die angegebenen Artikelnummern gesucht werden. Sind Handeingaben definiert, so wird nach den Bezeichnungen der Handeingabe gesucht. Das bedeutet auch, wenn in der Zwischenzeit Artikel in der Artikelnummer geändert werden, wird der XLS Importer keinen Zusammenhang mehr erkennen können.

#### Wie kann ich manuell weitere Preise hinzufügen?
Die Frage des Anwenders war eigentlich, wie kann man die eigenen Preise, z.B. aus den Beständen, mit in den Vergleich mit einfügen.
Hier ist die Vorgehensweise so, dass Sie sich selbst als Lieferant mit in die Liste der Lieferanten für diese EinkaufsAngebotsstückliste mit aufnehmen. Nun wechseln Sie in den Reiter 6 Positionen LF und fügen mit Neu die gewünschten Artikelpositionen hinzu und geben hier die gewünschten Preise ein.
Bitte beachten Sie, dass für eine gültige Erfassung unbedingt auch die Lieferzeit mit angegeben werden muss. Sollten Artikel lagernd sein, so ist eben die Lieferzeit 0 Wochen. Leer bedeutet nicht definiert und bedeutet auch, dass diese Positionen nicht weiter berücksichtigt werden, da ja nicht verfügbar.
Bitte beachten Sie, dass auch ein Preis mit dem Betrag 0,00 ein gültiger Preis ist. Wenn Preise nicht bekannt sind, so muss das jeweilige Preisfeld leer bleiben!

#### Wo sieht man ob noch Lieferantenangebote offen sind?
Wählen Sie die gewünschte Einkaufs-Anfrage-Stückliste und wechseln Sie in den Reiter 5 Lieferant.
![](EK_Anf_Stkl_Lieferanten.jpg)
Hier sehen Sie für jeden eingetragenen Lieferanten die eingetragenen Angebotsnummer und den Importzeitpunkt. Das bedeutet auch, dort wo nichts eingetragen ist, wurde auch noch keine Antwort Ihres Lieferanten in der jeweiligen Einkaufsangebotsstückliste hinterlegt.

<a name="Preise optimieren"></a>

#### Wie findet man den passenden Lieferanten, kommt zu den optimalen Preisen?
Wählen Sie dafür den Reiter 8 Preise optimieren.
Die Idee hinter dieser Funktion ist, dass man einerseits beim günstigsten Lieferanten einkaufen will / muss und andererseits aber auch die Artikel rechtzeitig zur Verfügung haben will. Natürlich kommt dann mit dazu, aber bei dieser Position kaufen wir dann doch bei einem anderen Lieferanten, als sich aus der beschriebenen Logik ergibt.

D.h. um für jeden Artikel den am Besten passenden Lieferanten zu finden, wechseln Sie in den Reiter 8 Preise optimieren.
Nun definieren Sie im Direktfilter die maximale Lieferzeit in KW (KalenderWochen). Definieren Sie zusätzlich für welche Mengenstaffel diese Optimierungsberechnung gemacht werden sollte. D.h. geben sie neben der maximalen Lieferzeit auch die Mengenstaffel an ![](EkAngStkl_Preise_optimieren_Lieferzeit.gif).
Bitte achten Sie darauf, dass nach der Definition der Filterbedingungen, die Liste aktualisiert ![](EK_Ang_Stkl_aktualisieren.gif) werden muss.

Filterung nach Artikelnummer, geben Sie hier einen Teil der Artikelnummer ein. Der Einfachheit halber wird hier nach Teilen der Artikelnummer gesucht.
Gleiches gilt für die Suche nach der Bezeichnung. D.h. es wird nur nach einem Begriff dieser Bezeichnung, nicht case sensitive (Groß-/Kleinschreibung egal) gesucht. Sollten Sie nach Kombinationen von Wörtern suchen, so können diese mit % nacheinander angeführt werden. Es muss jedoch die Reihenfolge (der Wörter) richtig sein.
Info: Aufgrund dieser sehr spezielle Liste gelten die in **Kieselstein ERP** üblichen Speicher- und Sortiermöglichkeiten leider nicht. Wir haben daher die Möglichkeit der Sortierung der Liste<br>
![](EkAngStkl_Preise_optimieren_Sortierung.gif)<br>
über eine Combobox geschaffen. Wählen Sie hier nach welcher Spalte die Darstellung sortiert sein sollte. Die Sortierung an sich ist immer aufsteigend. D.h. A kommt vor B bzw. der billigste Artikel vor dem teuersten.

Die Bedeutung der Spalten und ev. weitere zusätzliche Informationen:

![](EK_Ang_Stkl_Preise_optimieren.gif)

| Spalte | Bedeutung |
| --- |  --- |
| Menge  | die Satzmenge der Stücklistenposition |
| Eht  | die Mengeneinheit dazu |
| Artikel | Die Artikelnummer (Ident) |
| Bezeichnung | Die Bezeichnung des Artikels |
| Zusatzbezeichnung | die Zusatzbezeichnung des Artikels |
| Herstellernummer | Die Artikelnummer des Herstellers |
| K | Sind für diesen Artikel Kommentare aus den Lieferanten vorhanden. Dies wird durch ein K (Kommentar) signalisiert.Im Tooltip werden die Kommentare entsprechend angezeigt. |
| Verfügbar | Aktuelle Verfügbarkeit (Bewegungsvorschau) |
| Menge n | Die über den Mengenmultiplikator errechnete Menge dieser Mengenstaffel |
| Günst.Preis | Der günstigste Preis für diese Menge immer in Mandantenwährung inkl. Sicherheitsaufschlag. |
| LiefKbez | Die Lieferanten Kurzbezeichnung für den günstigsten Preis |
| LZ | Lieferzeit in Wochen |
| Übst.Preis | Der Preis aus dem übersteuerten Lieferanten für diese Menge in Mandantenwährung inkl. Sicherheitsaufschlag. |
| Übst.LZ | Die Lieferzeit aus dem übersteuerten Lieferanten |
| LF-Übst  | Durch Klick auf diesen Button kann aus der Liste der möglichen Lieferanten dieser Einkaufsanfragestückliste ein alternativer Lieferant ausgewählt werden |
|   | Die Spalten Menge 1 - 5 bis zum LF-Übst wiederholen sich 5x, für jede Mengenstaffel eine entsprechende Spaltengruppe. |

**Hinweis1:** Die angegebenen Mengen / Preise beziehen sich immer auch auf die Zusatzbedingung, innerhalb der Lieferzeit.<br>
**Hinweis2:** Wurde Mindestbestellmenge berücksichtigen bzw. Verpackungseinheit berücksichtigen angehakt, so ergibt sich der günstigste Preis aus dem umgerechneten Stückpreis.<br>
> Beispiel:<br>
> Es werden 1000Stk benötigt.<br>
> Die VPE sei 5000Stk bei einem Preis von 0,01€<br>
> So ist für eine Mengenstaffel von 1.000Stk der umgerechnete Preis (0,01*5000/1000) = 0,50€<br>
> Für eine Mengenstaffel von 2.000Stk nur mehr 0,25€<br>
>
**Hinweis3:** Wurde für einen Artikel kein Lieferant gefunden, so werden die Spalten Günst.Preis und LiefKbez in Rot angezeigt.
**Hinweis4:** Ist ein übersteuerter Lieferant definiert, so kann durch Rechtsklick auf LF-Übst ![](EK_Anf_Stkl_uest_Lieferant.gif)
![](EK_Ang_Stk_Bemerkungen_bearbeiten.jpg)
die Bemerkungen zu dieser Lieferanten Artikeldefinition, welche unabhängig von der Staffelmenge ist, bearbeitet werden.

#### Wie kann ich nun die Preise eines Lieferanten gezielt ändern?
Während des Preise optimierens werden Sie vermutlich auch bei Ihrem Hauptlieferanten Preise nachverhandeln unter gewissen Umständen werden einzelne Preise nachgebessert. Um dies so einfach wie möglich zu machen, haben wir die GoTo Funktion geschaffen. D.h. um den zu ändernden Preis zu definieren, klicken Sie dazu in das Feld Günst.Preis bzw. Übst.Preis der jeweiligen Mengenstaffel. Damit haben Sie sowohl den Lieferanten als auch die Position der zu ändernden Einkaufsangebotsstücklisten Position ausgewählt. Klicken Sie nun anschließend auf Goto ![](EkAngStkl_GoTo_Lieferant_und_Position.gif). Damit wechseln Sie in den Reiter 6 Positionen des Lieferanten, wobei der durch die Preisdefinition vorgegebene Lieferant ausgewählt wurde und anschließend wird der Cursor auf die gewünschte Zeile gestellt.

#### Wo können spezifische Lieferanten-Artikel/Positions-Kommentare eingegeben werden?
Wählen Sie dafür zuerst im Reiter 5 Lieferanten den gewünschten Lieferanten und erfassen dann im Reiter 6 die gewünschten Daten.

#### 7 Vergleich
Der Reiter Vergleich dient dazu um den gewählten Lieferanten mit dem billigsten und dem schnellsten Lieferanten zu vergleichen und zusätzlich den Lieferanten bei dem bestellt werden sollte aufzuzeigen. Auch hier kann beim gewählten Lieferanten für die jeweilige Mengenstaffel die entsprechende Bestellmarkierung gesetzt werden.

#### Wann wird das Preise fixiert gesetzt?
Kann nachträglich das Bestellen nochmal verändert werden?

A: Wenn alle Positionen der Einkaufsanfragestückliste einen Lieferanten hinterlegt haben, bei dem bestellt werden sollte, wird im Reiter 8 (Preise optimieren) das Preisfixiert angezeigt, womit aus dem Reiter 8 heraus keine Änderungen mehr gemacht werden können.<br>
Das bedeutet zugleich auch, dass gegebenenfalls spezielle Änderungen noch im Reiter 7 gemacht werden können / sollen. Z.B. wenn Die ganzen Einstellungen schon richtig sind, aber die Eine Position wird nicht beim Lieferanten A sondern beim Lieferanten B gekauft. In diesem Falle würden Sie einfach im Reiter 5 den Lieferanten B auswählen auf die jeweilige Position gehen und die gewünschte Mengenstaffel anhaken.
![](EK_Ang_Stk_bestellen_anpassen.gif)

#### Können aus dem Reiter 8, Preise optimieren Inhalte kopiert werden?
Ja. Markieren Sie die gewünschte Zelle und drücken Sie Strg+C (je nach Betriebssystem). Damit wird der Inhalt der Zelle in die Betriebssystem Zwischenablage übertragen und kann von dort aus weiterverwendet werden.

#### Wie gelangen die Preise in den Artikelstamm?
#### Wie gelangen die Preise auf die Bestellung?
Bei der Überleitung der Preise fixierten Einkaufsangebotsstückliste werden die Mengen und die Artikel übernommen. Für den Andruck der Lieferantenartikelnummern werden beim Druck der Bestellungen die im Artikelstamm hinterlegten Daten, also Artikellieferantennummern verwendet. Wenn nun neue Artikel definiert wurden, sind weder Lieferanten noch deren Artikelnummern beim jeweiligen Artikel hinterlegt.
Um nun diese Daten möglichst komfortabel übertragen zu können, steht im Reiter Preise optimieren die Funktion ![](Artikelnummer_LF_uebernehmen.gif) Artikelnummer_LF in Artikellieferant speichern zur Verfügung.
Die Übertragung erfolgt nach folgender Logik:
- wenn es für diesen Artikellieferanten noch keinen Eintrag gibt, so wird dieser angelegt, mit gültig ab Importdatum der EkAngstkl-Excelliste. Wurde das per Hand eingegeben (kein Importdatum) dann heute. Und es wird, da Neuanlage, auch der Preis übernommen. Sind mehrere Staffelpreise in der EK AngStkl definiert, so werden diese in die Staffelpreise übernommen. Es werden auch die Mindestbestellmenge und die Verpackungseinheiten aktualisiert.
- gibt es diesen Artikellieferanten für diesen Artikel schon, so wird, wenn die Artikelnummer des Lieferanten unterschiedlich ist, diese aktualisiert. Der Preis wird NICHT verändert.

#### Wo sieht man, welche Lieferanten noch keine Angebote (zurück-)gesandt haben?
Im Reiter 5 Lieferanten wird beim Versenden der EMail Anfragen und der EMail-Sendezeitpunkt mitprotokolliert. Zugleich wird beim importieren der Importzeitpunkt protokolliert. D.h. bei den Zeilen, bei denen zwei ein E-Mail Sendezeitpunkt aber noch kein Importzeitpunkt eingetragen ist, sollten Sie nachhaken, warum Ihr Lieferant noch nicht angeboten hat.

#### Können die Artikel zusammengefasst werden?
Ja. Durch Klick auf verdichten ![](EkAngStkl_verdichten.gif) werden:
-   alle Artikel mit der gleichen Artikelnummer zusammengefasst.
-   alle Handeingaben bei denen die Bezeichnung und die Herstellernummer gleich sind zusammengefasst. Bitte beachten Sie, dass die anderen Felder wie Zusatzbezeichnung, Hersteller, Herstellerbezeichnung nicht berücksichtigt werden.

Zusätzlich werden die Positionen ebenfalls zusammengefasst. D.h. wenn in der ersten Zeile C1, C2 steht und in der zweiten Zeile des gleichen Artikels C3, C4, C5 so wird daraus eben C1, C2, C3, C4, C5.<br>
Für die Referenzbemerkung und für die interne Bemerkung wird analog vorgegangen.
Bitte beachten Sie, dass das verdichten nur möglich ist, solange keine Lieferantendaten erfasst sind.

<a name="Wechselkurs"></a>

#### Wie ist mit abweichenden Währungen und dem Wechselkurs Risiko umzugehen?
#### Wir kaufen viel in fremder Währung, wollen da aber einen Sicherheitspolster haben?
Es ist durchaus üblich, dass auch im Inland das Währungsrisiko von Ihrem Distributor / Zwischenhändler an Sie weitergegeben wird. Dafür haben wir die Möglichkeit geschaffen, dass bei der Definition der Lieferanten für diese Einkaufsangebotsstückliste auch die Währung angegeben werden kann. Um das Währungsrisiko abzubilden, kann zusätzlich auf den Kurs ein Sicherheitsaufschlag in % angegeben werden. Das bedeutet, dass in den internen Preisberechnungen der Einkaufsangebotsstückliste die fremde Währung zum heutigen Tage auf Ihre Mandantenwährung umgerechnet wird und dann auf den Preis der Sicherheitsaufschlag dazugegeben wird. D.h. die Umrechnungsformel vom Fremdwährungsbetrag in Ihre Mandantenwährung lautet z.B. wie folgt:<br>
Annahme: 1 EUR = 1,10 USD<br>
Preis in USD = 5,-<br>
Preis in EUR = USD / 1,1 = 4,5454<br>
plus Sicherheitsaufschlag 5% = 4,5454*1,05 = 4,7728 EUR<br>

Dieser um den Sicherheitsaufschlag erhöhte Preis wird in allen Betrachtungen verwendet. Davon ausgenommen ist die Überleitung in den Bestellvorschlag. Hier wird der Preis in Mandantenwährung **ohne** Sicherheitsaufschlag verwendet.

Der Sicherheitsaufschlag muss für jedes Einkaufsangebot / Einkaufsanfragestückliste im Reiter 5 Lieferant (neu) definiert werden.

#### Wie ist der Ablauf bei der Bestellung und Fremdwährungen?
Bei der Überleitung der Einkaufsangebotsstückliste in den Bestellvorschlag werden automatisch die Preise anhand des hinterlegen Kurses zum Übertragungszeitpunkt (also heutiger Kurs) in die Mandantenwährung ohne den Sicherheitsaufschlag umgerechnet und somit normiert.<br>
Selbstverständlich können jetzt noch, wenn erforderlich, Änderungen am Bestellvorschlag durchgeführt werden.<br>
Wird nun der Bestellvorschlag in einzelne Bestellungen übertragen, so wird bei der Anlage der jeweiligen Bestellung die beim Lieferanten hinterlegte Währung herangezogen. Als Kurs wird wiederum der Tag der Umwandlung (also heutiger Kurs) herangezogen. Somit wird der Preis von der Mandantenwährung in die Lieferantenwährung umgewandelt.<br>
Hinweis: Sollten Sie mit einem Lieferanten eine andere Währung als der in der Sie üblicherweise beliefert werden für dieses Projekt vereinbart haben, muss für die automatische Erzeugung der Bestellung, für diesen Lieferanten die beim Lieferanten hinterlegte Währung auf die gewünschte Währung (der Einkaufsangebotsstückliste) geändert werden. Denken Sie daran diese danach wieder zurückzuändern.

#### Wo sieht man wieviel aktuell am Lager ist?
Neben den Lieferanten Informationen ist auch immer wieder zumindest interessant, welche eigenen Lagerstände man hat. Da es in genauerer Betrachtung um die frei verfügbaren Bestände geht, die anderen sind ja schon verkauft, wird im Reiter 6 Positionen LF auch die eigene freie (Lager-)Verfügbarkeit angezeigt.

#### Woher kommt die Herstellernummer?
Das kommt darauf an.
- Wenn die Position ein Artikel ist, so wird immer die Herstellerartikelnummer aus dem Artikelstamm angezeigt und verwendet.
- Ist die Position eine Handeingabe, so kommt die Definition von Hersteller, Herstellerartikelnummer und Herstellerbezeichnung aus der jeweiligen Position.

Bitte beachten Sie, wenn bei Artikeln Hersteller, Herstellernummer bzw. -bezeichnung geändert wird, so wird dies automatisch auch im Artikelstamm mitgeändert.

#### Wann werden die Herstellernummern usw. beim intelligenten Stücklistenimport in den Artikelstamm übertragen?
Beachten Sie dazu den Haken bei Artikel updaten. Ist dieser angehakt, so werden bei den gefundenen bzw. zugeordneten Artikeln alle Bezeichnungen und somit auch Hersteller und Herstellerartikelnummern bzw. -bezeichnungen aktualisiert.

#### Wie mit beigestellten Artikeln umgehen?
Vom Kunden beigestellte Ware wird als Beistellware kostenlos beigestellt. Daher sollten diese auch nicht in der Einkaufsanfragestücklisten enthalten sein. Immerhin sollten / dürfen Sie diese ja auch nicht anfragen.<br>
Sollten Sie diese trotzdem verwalten wollen / müssen, so empfiehlt sich, dass Sie einen Lieferanten Beistellmaterial (oder ähnlich) anlegen und die vom Kunden beigestellten Artikel entsprechend im Reiter Positionen LF definieren. Beim Preise optimieren werden dann durch die 0,00 Preise diese automatisch als beim Beistelllieferanten zu bestellen vorgeschlagen. Bitte beachten Sie, dass auch ein Preis von 0,00 einen gültigen Preis darstellt.

<a name="XLS Daten"></a>

#### Wie sieht die Datei aus, die den Lieferanten gesandt wird?
Die Datei welche automatisch an das EMail, welches an den Lieferanten gesandt wird, angehängt wird ist im allseits bekannten Format XLS, genauer Excel 97-2000 Format.
Bitte achten Sie darauf, dass für den Reimport wieder dieses Format verwendet werden muss.
Die Datei ist in Bereiche gegliedert.
1. Informationen über benötigte Artikel und Mengen
![](EkAngStkl_XLS1.gif)

2. Felder die der Lieferant befüllen sollte
![](EkAngStkl_XLS2.gif)

Felder die der Lieferant nicht befüllen sollte sind gesperrt, könnten jedoch auch vom Lieferanten entsperrt werden. Vereinbaren Sie mit Ihren Betreuern die Datei nur mit Werten zu befüllen und die Struktur gleich zu lassen.

D.h. in den blauen Bereichen sind Ihre Informationen an den Lieferanten enthalten, in den gelben Bereichen sind die Daten des Lieferanten enthalten.
Die gelb hinterlegten Felder sollten, soweit möglich und sinnvoll, von Ihrem Lieferanten mit Daten ergänzt werden.

**Hinweis:** Da es in der Elektronik-Branche durchaus üblich ist, und um unsere fremdsprachigen Mitglieder zu unterstützen, werden die Benennungen in Englisch ausgeführt, unabhängig in welcher Sprache der Lieferant bzw. der Mandant definiert ist. Wir können davon ausgehen, dass Ein- und Verkäufer Englisch können um diese Daten lesen zu können.

#### Wie wird mit Handartikeln umgegangen?
Beim Reimport der Daten der Lieferanten wird die Zuordnung entweder nach den Artikelnummern oder wenn es Handartikel sind nach Bezeichnung und nach Herstellernummer vorgenommen um wiederum eindeutig die Position in Ihrer Einkaufsanfragestückliste wiederzufinden.
Dies vor allem deswegen, da sich ja in der Zwischenzeit, also in der Zeit vom Versand der Anfrage an den Lieferanten bis zum Erhalt des XLS Angebotes die Inhalte der Anfragestückliste und oder der rückübermittelten xls Datei geändert haben könnten.

#### Wie wird mit gemischten Verfügbarkeiten umgegangen?
Die Aufgabe ist, ein Teil der Positionen sind beim Lieferanten lagernd und ein Teil der Positionen eben nicht. Die beim Lieferanten lagernden Positionen haben lange Lieferzeiten, aber die lagernde Menge reicht aus. D.h. man kann davon ausgehen, dass diese Artikel z.B. innerhalb von zwei Wochen verfügbar sind. Siehe dazu Parameter LF_LAGER_LIEFERZEIT_IN_KW.<br>
Die Berechnung geht nun so, dass bei lagernden Positionen nur die Lieferzeit laut Parameter verwendet wird und für alle nicht lagernden Positionen eben die angegebene Lieferzeit. Daraus ergibt sich unter Umständen eine bessere Gesamtlieferzeit für das Projekt.

#### Wie bringt man eine bestehende Stückliste in die Einkaufsanfragestückliste?
Einfach durch kopieren über die Zwischenablage. D.h. du gehst in die gewünschte Stückliste, markierst die anzufragenden Positionen in die Zwischenablage ![](Zwischenablage_kopieren.png)  und fügst diese in die Einkaufsanfragestückliste mit ![](Zwischenablage_Einfuegen.png)  einfügen ein.<br>
Da in den Kopfdaten die verschiedenen Staffelmengen angegeben werden müssen, wird in den Positionen immer nur ein Satz angegeben. D.h. damit wird dann der entsprechende Bedarf errechnet.