---
categories: ["Angebotsstückliste"]
tags: ["Schnellerfassung"]
title: "Schnellerfassung"
linkTitle: "Schnellerfassung"
weight: 10
date: 2023-01-31
description: >
  Effizient Angebote mit Vorlagen aus den Angebotsstücklisten erstellen
---
Angebotsstückliste - Schnellerfassung
=====================================

Mit der Angebotsstückliste Schnellerfassung haben Sie ein praktisches Tool um einfache mechanische Teile, aber nicht nur, nach einem Standardverfahren rasch zu kalkulieren. Ja nach Ihren Anforderungen können Sie die gewünschten Vorlagen entsprechend gestalten um so rasch zum gewünschten Ergebnis zu gelangen.
Unsere Empfehlung ist, dass je nach Aufgabenstellung bzw. Art des zu fertigenden Teiles entsprechende Vorlagen gemacht werden.

Das Arbeiten mit der Angebotsstücklisten Schnellerfassung gliedert sich in zwei Bereiche:

-   Definition der Vorlage(n)

-   Erstellen der Angebotsstückliste direkt aus dem Angebot heraus

Definition der Vorlage
----------------------

Um eine Angebotsstücklistenvorlage zu erstellen, starten Sie das Modul Angebotsstückliste ![](Angebotsstueckliste.jpg).
Legen Sie nun eine neue Angebotsstückliste an und definieren Sie diese als Vorlage für Kalkulation ![](Vorlage_fuer_Kalkulation.gif).
Neben den üblichen Kopfdaten definieren Sie auch das geplante Material für diese Vorlage.
Z.B.
![](Materialauswahl.gif)
Die zur Auswahl zur Verfügung stehenden Materialien werden im Modul Artikel, Grunddaten, Material definiert.
Geben Sie hier insbesondere das spezifische Gewicht und den aktuellen Einkaufspreis pro kg des Materials an.
Um entsprechende Infos über die Preisentwicklung zu bekommen, sollten die Materialpreise gepflegt werden. D.h. bei wesentlichen Preisänderungen des Rohstoffes entsprechend aktualisiert werden.
So kostete Aluminium 2015 ca. 1,60€/kg, Ende 2021 liegen wir bei ca. 2,75€/kg.
![](Material_Definition.gif)
Zusätzlich kann auch ein Standard Bild für diese Art von Artikel hinterlegt werden ![](Standard_Bild.gif).
Benennen Sie im Projekt Ihre Vorlage sprechend um diese dann in der Vorlagenauswahl entsprechend rasch wiederzufinden.
Wechseln Sie nach der Definition der Kopfdaten in den Reiter Positionen und legen Sie hier allgemeine Artikel / Materialien fest, wie z.B. Verpackung, ev. Transportkosten u.ä.
Anschließend definieren Sie welche Arbeitsgänge üblicherweise bei einem derartigen Artikel durchzuführen sind.

Erstellen der Angebotsstückliste
--------------------------------

Um aus einem Angebot heraus in die Angebotsstücklisten Schnellerfassung zu wechseln, bzw. eine neue Angebotsstückliste hinzuzufügen, klicken Sie in den Angebotspositionen auf die Schnellerfassung ![](Schnellerfassung.gif).
Nun erscheint die Liste der Angebotsstücklisten-Vorlagen.
![](Vorlagen_Auswahl.gif)
Wählen Sie die passende Vorlage aus.
Damit befinden Sie sich in der Schneller-Erfassung der Angebotsstückliste.

Diese Erfassung gliedert sich in folgende Bereiche:
![](Schnellerfassungsfenster.png)

### Zeichnung bzw. Bild des Artikels

Ziehe Sie hier die Zeichnung des anzubietenden / zu kalkulierenden Artikel in den angeführten Bereich. Damit wird dieser Artikel in der Angebotsstückliste hinterlegt.
Durch Klick auf Übernahme aus Zwischenablage ![](Zwischenablage.gif) kann das Bild direkt aus der Zwischenablage übernommen werden.
![](Schnellerfassung_Zeichnung.jpg)

### Bezeichnung und Material

Geben Sie hier die entsprechende Benennung des zu fertigenden Artikels ein.
Wählen Sie anschließend gegebenenfalls ein anderes Material aus, wählen Sie die Form
![](Auswahl_der_Form.gif)
und geben Sie die zur Form passenden Abmessungen an.
Somit wird das Gewicht des benötigten Materials errechnet und damit der ca. Einkaufspreis des Rohmaterials bestimmt.
Alternativ kann direkt das Gewicht angegeben werden. Als weitere Alternative kann einfach der geschätzte Preis der Materialposition eingegeben werden.
Durch Klick auf + Material kann weiteres Material aus dem diese Baugruppe besteht hinzugefügt werden. Überzählige Positionen können durch klick auf löschen wieder entfernt werden.

### Arbeitsplan mit Maschinen

Definieren Sie hier die Rüst- und die Stückzeiten für die jeweiligen Arbeitsgang.
Durch Klick auf das M ![](Schnellerfassung_Maschinenauswahl.gif) des gewünschten Arbeitsganges kann eine andere Maschine gewählt werden. Für Arbeitsgänge die nicht benötigt werden belassen Sie die Zeiten einfach auf 0.
Wird nur die Maschinenzeit gerechnet, so haken Sie bitte nur Maschine an.
Rechts unten sehen Sie die Arbeitszeit-Gesamtkosten

### Zusätzliche Verbrauchspositionen

Hier werden einerseits die bereits in der Vorlage enthaltenen zusätzlichen Materialpositionen angezeigt. Andererseits können mit + ![](Verbrauchsposition_hinzu.gif) weitere Positionen hinzugefügt werden, bzw. mit - ![](Verbrauchsposition_entfernen.gif) wieder entfernt werden. Auch hier wird rechts unten die Gesamtsumme der zusätzlichen Materialpositionen angezeigt.

### angebotene Mengenstaffel

Hier werden die üblichen, in der Vorlage definierten Mengenstaffeln, angezeigt, welche durch hinzufügen mittels + bzw. x ebenfalls angepasst werden können.
Wurden mehrere Mengenstaffeln angegeben, so wählen Sie die Menge, die in das Angebot übernommen werden sollte durch Klick auf den in der Mengenstaffel befindlichen Knopf übernehmen.
Zusätzlich können in dieser Darstellung der jeweilige Materialaufschlag definiert werden.
Weiters kann der errechnete Verkaufspreis ihren Wünschen gemäß angepasst werden.

Für den Druck im Angebot werden alle angeführten Mengenstaffeln mit im Angebot aufgeführt, wodurch Ihr Kunde eine gute Preisübersicht bekommt.
![](Angebot_mit_Mengenstaffeln.gif)

Bewährt hat sich, die wahrscheinlichste Mengenstaffel, sofern das möglich ist, in das Angebot aufzunehmen, da damit auch der Angebotswert realistisch ist.

#### Zu welchem Preis wird nun angeboten?
In der Liste der Mengenstaffeln werden Herstellkosten und Verkaufswert und daraus errechnet der Aufschlag in % angezeigt.
![](Mengenstaffeln_Preisdefinition.gif)
Der tatsächlich angebotene Preis kann im Feld Preis berechnet angepasst werden, wodurch sich auch der Aufschlag mit-ändert.

#### Wie kann über die Schnellerfassung eine Angebotsstückliste geändert werden?
Wählen Sie die Position, klicken Sie anschließend auf den in den Positionsdetails angezeigten Schnellerfassungsbutton ![](Schnellerfassung_aendern.gif).
Hinweis: Durch Ändern, ohne den Klick auf den Schnellerfassungsbutton, kann der entsprechende Verkaufspreis für die Angebotsposition ohne Rückwirkung auf die Angebotsstückliste angepasst werden.

#### Wie werden die Daten der spezifischen Angebotsstückliste abgelegt?
Über die Schnellerfassung wird eine weitere Angebotsstückliste, welche mit der erzeugten Angebotsposition verknüpft ist angelegt.<br>
D.h. es stehen sowohl die Daten für eine Überleitung in eine [Stückliste]( {{<relref "/verkauf/angebotsstueckliste/#wie-wird-eine-angebotsst%c3%bcckliste-in-eine-st%c3%bcckliste-%c3%bcbertragen" >}} )

als auch für die verschiedensten Vor- und Nachkalkulationen zur Verfügung.

#### Wie wird das Material bewertet ?
Beim Material wird immer das Gewicht egal ob errechnet (z.B. über die Materialtype/form) oder manuell erfasst für die Berechnung des Wertes herangezogen. Dies vor allem auch deswegen um die Materialkostenveränderungen über den Zeitraum darstellen zu können.

#### Wo sieht man die Kalkulation der Angebotsstückliste
Sie finden diese einerseits im Modul Angebotsstückliste, drucken, aber auch in der Vorkalkulation des Angebotes.
In beiden Darstellungen kann eine zum Belegdatum abweichende Preisgültigkeit angegeben werden, womit auch die Preisentwicklung sichtbar wird.
Voraussetzung für die Darstellung einer Preisentwicklung ist, dass bei den Artikeln, bzw. beim Material die Preise gepflegt werden.

#### Bildschirmeinstellungen
Die Angebotsschnellerfassung wird oft gemeinsam mit den Zeichnungen durchgeführt.
Zusätzlich sind, siehe oben, eine Menge an Daten auf einmal einzugeben / darzustellen. Wir gehen daher davon aus, dass der Anwender einen entsprechend großen / hochauflösenden Bildschirm besitzt.
Weiters wird die gewählte Darstellung inkl. der Positionen der horizontalen und vertikalen Trennlinien beim Klick auf Übernehmen auf dem Rechner lokal abgespeichert.
Das bedeutet: Bitte stellen Sie beim Aufruf des Angebotsstücklistenschnellerfassungsdialoges den Dialog wie für Sie gut lesbar ein und speichern Sie diese durch Übernehmen entsprechend ab. Die Darstellung wurde auf Schriftgröße 11 ausgelegt.

#### Einstellung der Höhen der Bereiche?
Um die Höhen der verschiedenen Erfassungsbereiche zu verändern, benutzen Sie bitte die blauen Pfeile ![](Schnellerfassung_Gewichtung.gif). Damit wird die Gewichtung des Erfassungsbereiches entsprechend erhöht bzw. reduziert. Diese Gewichtung wird ebenfalls beim Klick auf Übernehmen lokal abgespeichert.

neue Version

Rechenregel

Preis/Einheit wenn geändert, ändert nur Aufschlag auf Arbeitszeit. Material muss der Anwender von oben nach unten selber pflegen
Die Aufschläge kommen aus der Mengenstaffel der Vorlage, bzw. wenn es für die Mengenstaffel in der Vorlage keinen Eintrag gibt, so aus der davor definierten Mengenstaffel.
Anzeige in Rot bedeutet, dass der gewählte Preis kleiner als der aktuelle Lief1Preis des Artikels ist. Bei Arbeitszeiten gilt ebenfalls der Lief1 Preis der Tätigkeit. Bei Maschinenzeiten wird der Preis in rot dargestellt, wenn er unter dem kalkulatorischen Stundensatz ist.
Bitte beachten Sie, dass die Änderung von Summenwerten immer auf die Einzelpreise rückgerechnet werden. Nur diese werden für die weitere Berechnung verwendet. So kann es aufgrund der erforderlichen Rundungen auf die gewählte Nachkommastellenanzahl zu leichten Abweichungen kommen. Diese sind mathematisch bedingt.

Preis / Einheit in der Gesamtsumme und deren Rückrechnung geht immer vom Material plus Aufschlag aus und dann wird die Arbeitszeit plus Aufschlag dazugerechnet. Wird nun rechts der Preis / Einheit manuell eingegeben so wird damit NUR der Aufschlag der Arbeitszeit verändert. Was auch bedeutet, dass der Arbeitszeitaufschlag nicht unter -100% gehen sollte.
Wenn Sie tatsächlich besondere Preise erzielen wollen / müssen, so empfehlen wir, dies bereits in den oberen Bereichen bei der Erfassung der Zeiten und Materialpositionen zu berücksichtigen. Grundsätzlich gehen wir davon aus, dass Sie mit den schneller kalkulierten Artikeln bzw. Baugruppen, Gewinne erzielen möchten.

#### Überleitung in Stücklisten
#### Wie wird das Material übernommen?
Es werden Handeingaben angelegt.
Ähnliches gilt für die Angebotsvorkalkulation

![](Schnellerfassungsdialog.jpg)

#### Handeingabe für die Angebotsstücklisten Schnellerfassung?
Wenn man in der Position eine Handeingabe braucht, so in der Artikelauswahl auf Einmalartikel klicken und da dann eine Handeingabe auswählen.

#### Initialkosten
Der Parameter für Initialkostenartikel muss definiert sein, damit diese auch in das Angebot als eigene Position übernommen werden.<br>
Ist der Parameter nicht definiert, es sind aber Initialkosten gegeben, kommt derzeit leider keine Fehlermeldung.