---
title: "Lieferschein, Versanddienstleister"
linkTitle: "Versanddienstleister"
categories: ["Versanddienstleister"]
tags: ["Versanddienstleister"]
weight: 20
description: >
  Versanddienstleister, Österreichische Post AG
---
Externe Versanddienstleister
============================

Gerade wenn pro Tag mehrere Pakete versandt werden, ist es natürlich praktisch, wenn die Datenübergabe bis hin zum fertig ausgedruckten Etikett möglichst vollautomatisch erfolgt. Da die datentechnischen Anbindungen, also die Schnittstellen je Frachtführer sehr sehr unterschiedlich sind, ist leider eine spezifische Schnittstelle je Transportunternehmen erforderlich.
Diese zusätzlichen Module stellen wir gerne zur Verfügung.

Daher nachfolgend die Beschreibungen für die jeweiligen Frachtführer / Transportunternehmen:

1.  [Österreichische Post AG](#Österreichische Post)

<a name="Österreichische Post"></a>Österreichische Post

Die Österreichische Post AG stellt einen WebService über RestAPI zur Verfügung. Neben den üblichen Verträgen benötigen Sie auch den sogenannten API Key. Bitte beantragen Sie diesen bei Ihrem zuständigen Betreuer der österreichischen Post.
Der API Key besteht aus drei Teilen:
ClientID: 12345678
OrgUnitID: 12345678
OrgUnitGUID: abcdef01-012a-1b23-1c23-abcdef012345
URL:
Obige Key sind nur beispielhaft und werden nicht funktionieren, ebenso der in den Systemparametern beispielhaft angeführte Demokey.
Tragen Sie diesen Key in den Parameter POST_PLC_APIKEY ein. Das Format ist ClientID;OrgUnitID;OrgUnitGUID, also die drei Key jeweils durch ein Semikolon getrennt.
Als vierten Parameter muss die URL für den Aufruf des Postservers angegeben werden. Die derzeit bekannten Urls sind:
- Testsystem: <https://abn-plc.post.at/DataService/Post.Webservice/ShippingService.svc/secure>
- Echtsystem: <https://plc.post.at/Post.Webservice/ShippingService.svc/secure>

Um nun dem Versanddienstleister mitzuteilen, welchen Dienst Sie in Anspruch nehmen möchten, muss die passende Lieferart beim Lieferschein hinterlegt sein.
D.h. im Modul System, Mandant, Lieferart kann pro Lieferart der externe Produktcode definiert werden.
![](Lieferart_Versand.gif)

**Hinweis:** Die Übergabe an den externen Versanddienstleister erfolgt nur, wenn in der Lieferart des Lieferscheines auch ein externer Produktcode definiert ist. Welcher der Codes zu verwenden ist, stimmen Sie bitte mit Ihrem Versanddienstleister ab.

#### Drucken, Nachdrucken der PLC Versandetiketten
Sie finden dies unter<br>
![](PLC_Versand1.gif)

Beim Aufruf dieser Funktion wird, abhängig ob bereits eine entsprechende Versandnummer hinterlegt ist oder nicht, sofort die Kommunikation zum Postserver aufgebaut, die "Bestellung" des Transportes abgesetzt und das von der Post zur Verfügung gestellte PDF-Etikett angezeigt.
Zusätzlich werden diese Kommunikationsdaten in der Dokumentenablage des Lieferscheines hinterlegt.

Die von der Post übermittelte Versandnummer(n) wird in den beiden Versandnummern des Lieferscheines (Reiter Konditionen) hinterlegt.<br>
**Info:** Solange Versandnummern eingetragen sind, wird davon ausgegangen, dass bereits ein Auftrag an den Versanddienstleister (PLC) gegeben wurde.<br>
D.h. Sie können den Druck der Versandetikette(n) wiederholen, oder durch anhaken von ![](PLC_Versand2.gif) bestehende Versandnummer ersetzen, einen **NEUEN VERSANDAUFTRAG** plazieren. Zusätzlich wird zur Info die bestehende Versandnummer in Rot angezeigt.
![](PLC_Etikette.gif)

#### Wie lange dauert es bis ein Etikett vorhanden ist?
Das hängt stark von der Auslastung des dahinterliegenden Postservers ab. In der Regel kann mit ca. 10 - 30Sekunden gerechnet werden.

#### Es wird kein Postetikett gedruckt
Bitte prüfen Sie ob bei der Lieferart des Lieferscheines auch der externe Produktcode definiert ist.<br>
Ein Symptom ist auch, dass der Nachdruck des PLC Etikettes nicht aufgerufen werden kann.

#### Voraussetzungen
Da die Kommunikation über Ihren **Kieselstein ERP** Server läuft muss dieser auf den Postserver per Namensauflösung zugreifen können. Bitten Sie Ihren IT-Betreuer dies vor der Verwendung dieser Funktion sicherzustellen. Es muss vom **Kieselstein ERP** Server aus ein Ping gegen ping abn-plc.post.at möglich sein, d.h. eine Antwort liefern.

A: Sie benötigen einen Etikettendrucker, der idealerweise im Netzwerk verfügbar ist, um die Postetiketten zu drucken.
Einstellung des Druckers
![](PLC_Versand3.jpg)
Wir haben mit Treibern der Firma Seagull Scientific Inc. bisher sehr gute Erfahrungen gemacht.

#### Fehlermeldungen
Ungültige KundenID

A: Liefert ihr **Kieselstein ERP** nachstehende Fehlermeldung, so steht zwar hinter dem API Aufruf ein Server der österreichischen Post. Es scheint jedoch der Kunde, die Customer ID nicht bekannt zu sein.
![](PLC_Versand4.jpg)

#### Die API des Frachtführers hat einen Fehler festgestellt
Erscheint nachfolgende Fehlermeldung so bedeutet dies dass,
![](PLC_Versand5.jpg)
sehr wahrscheinlich der im Parameter angegebene Endpunkt, die Url nicht vollständig richtig ist. Darum meldet der Server der Post, dass er mit den übergebenen Daten nichts anfangen kann.

#### Wie den Versand mehrerer Pakete der Post mitteilen?
Tragen Sie bitte im Reiter Konditionen des Lieferscheins die Anzahl der Pakete vor dem Druck des Lieferscheines ein. Geben Sie hier gegebenenfalls auch das Gesamtgewicht der Sendung an.

#### EMail Adresse, Tracking Info?
Es werden, soweit vorhanden, an die Post / den Frachtführer auch die EMailadresse des Empfängers übergeben.
Die Versandauftragsnummer, also die Trackinginfo wird in den Versanddaten des Lieferscheines, nach der Abholung der Daten beim Postserver nachträglich eingetragen.

**WICHTIG:** Bitte klären Sie mit Ihrem Versanddienstleister in welcher Form er die von Ihnen übermittelten Daten Ihrer Kunden nutzt. Insbesondere im Sinne der DSGVO (Datenschutz Grundverordnung) kann es sich hier um Auftragsdatenverarbeitung handeln.