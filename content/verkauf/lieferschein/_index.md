---
title: "Lieferschein"
linkTitle: "Lieferschein"
weight: 60
description: >
  Lieferscheine mit Sammellieferschein und verketteten Lieferscheinen
---
Lieferschein
============

![](statusuebergang.png)

Anlegen eines Lieferscheins

Um einen neuen Lieferschein anzulegen gehen Sie bitte wie folgt vor:
Klicken Sie auf das Modul Lieferschein ![](Lieferschein.JPG) in der nun offenen Auswahl klicken Sie auf Daten aus einem bestehenden Angebot ![](angebot_neu.PNG), bestehenden Auftrag ![](auftrag_neu.PNG) oder aus einem bestehenden Lieferschein ![](Lieferschein_uebernehmen.gif) übernehmen.

Wenn Sie einen Lieferschein ohne Angebots-/Auftrags-Bezug erstellen wollen, so klicken Sie auf Neu ![](neu.gif).

Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den Kunden, Projekt, Bestellnummer, Termine - je nach Bedarf.

Mit einem Klick auf Speichern ![](speichern.JPG) bestätigen Sie die Eingaben.

Wechseln Sie in den Reiter Positionen und geben hier die Positionen des Lieferscheins ein.

Information zur Vorgehensweise bei [Auftragspositionen](#Auftragspositionen übernehmen)
Um eine neue Position hinzuzufügen klicken Sie auf ![](neu.gif) Neu wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#positionsarten">}} )), geben die Informationen (u.a. Menge) ein und klicken auf ![](speichern.JPG) Speichern. Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.

Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Konditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für diesen einen Lieferschein ab. 

Mit Klick auf ![](drucken.JPG) Drucken (oder oberes Menü Lieferschein - Drucken...) erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG), dem Versenden per E-Mail ![](e-mail.jpg)oder speichern ![](speichern.JPG), aktivieren Sie den Lieferschein, der Status (siehe oben) ändert sich von angelegt in offen. 

#### Statuswechsel im Lieferschein
1.  ![](angelegt.jpg) Angelegt (nach Anlegen des Lieferschein)
2.  der Lieferschein wird beim Drucken sofort von Angelegt auf Geliefert geändert.
3.  ![](geliefert.JPG) Geliefert (nachdem der Lieferschein manuell auf geliefert gesetzt wurde oder nachdem gedruckt wurde)
4.  ![](verrechnet.JPG) Verrechnet (nachdem der gesamte Lieferschein verrechnet wurde)
5.  ![](erledigt.jpg) Erledigt (der Lieferschein wurde manuell auf erledigt gesetzt)
6.  ![](storniert.JPG) Storniert (der Lieferschein wurde manuell storniert) -> alle Positionen wurden ans Lager zurückgebucht.

#### Wie kann ich Auftragspositionen übernehmen?
<a name="Auftragspositionen übernehmen"></a>
Legen Sie einen Auftragsbezogenen Lieferschein an. Verwenden Sie dazu am Besten den Knopf ![](Lsch_anhand_Auftrag.gif) Lieferschein aus Auftrag übernehmen in der Auswahlliste der Lieferscheine.
Nach der Auswahl des Auftrags werden Sie automatisch in die Sicht Auftrag geführt. D.h. in dieser Auswahlliste sehe Sie alle noch nicht vollständig ausgelieferten mengenbehafteten Positionen des Auftrags. Wählen Sie hier die auszuliefernde Position aus.
Unter Menge LS wird automatisch die noch offene bzw. auf diesem Lager verfügbare Menge vorgeschlagen. Ändern Sie diese gegebenenfalls entsprechend ab. **HINWEIS:** Bitte unterscheiden Sie klar zwischen der Lieferscheinbuchung im Modulreiter ![](LS_Positionen.gif)
und der Auftragsbezogenen Lieferscheinbuchung im Modulreiter ![](LS_Sicht_Auftrag.gif)
Unter Positionen werden für einen Auftragsbezogenen Lieferschein <u>**zusätzliche**</u> Positionen ausgeliefert. Diese reduzieren <u>**Nicht**</u> die offenen Mengen des Auftrages. Die offenen Mengen des Auftrages werden nur in der Sicht Auftrag entsprechend berücksichtigt. **Hinweis:** Mit dieser Funktion können nur Artikel übernommen werden, welche weder Chargen- noch Seriennummern geführt sind.
Für Seriennummern bzw. Chargennummern müssen diese Zuordnungen ja eingegeben werden. D.h. diese sind einzeln durch den Knopf Ändern ![](Liefermenge_aendern.gif) zu übernehmen.
**Hinweis2**: Beachte bitte auch die Auswirkungen der Nicht-Auftragspositionen bezogenen in der Auftragsnachkalkulation.

#### Welche Mengen werden in den Lieferschein übernommen?
Mit dem Parameter OFFENE_MENGE_IN_SICHT_AUFTRAG_VORSCHLAGEN können Sie das Verhalten der Mengenangabe bei der Übernahme vom Auftrag in den Lieferschein für nicht Chargen- bzw. Seriennummernbehaftete Artikel steuern.

-   0 = Aus. D.h. es muss die gewünschte Menge manuell eingegeben werden

-   1 = offene Menge des Auftrags bzw. wenn der Lagerstand des Abbuchungslagers geringer ist, dann die lagernde Menge

-   2 = keine Berücksichtigung der offenen Auftragsmenge, d.h. es wird Lagerstand des Abbuchungslagers verwendet, auch wenn dies eine Überlieferung bewirkt.

#### Was passiert, wenn ein Lieferschein aus einem vorhandenen Lieferschein erstellt wird, ein Lieferschein kopiert wird?
Wenn der Lieferschein ein "Freier Lieferschein" war, so werden die Kopfdaten und Positionen des Lieferscheins kopiert. Ein auftragsbezogener Lieferschein wird zu einem Freien Lieferschein umgewandelt und auftragsbezogene Positionen werden nicht kopiert. Wenn im Lieferschein ein Projekt (Projektklammer) hinterlegt ist, so bleibt der Projektbezug erhalten.

#### Ich kann meinen Auftrag bei der Auswahl aus dem Lieferschein nicht finden?
In der Auswahlliste der Aufträge werden nur Freie Aufträge und Abrufaufträge angezeigt, welche im Status offen oder im Status teilerledigt sind. Ist Ihr Auftrag noch im Status angelegt, so muss er zuerst gedruckt werden (an den Kunden versandt werden).

Rahmenaufträge werden durch Abrufe (Abrufaufträge) erfüllt und werden daher in der Auftragsauswahlliste aus dem Lieferschein nicht angezeigt.

#### Kann ich alle offenen Auftragspositionen auf einmal in den Lieferschein buchen?
Ja. Klicken Sie dazu in der Sicht Auftrag auf das ![](Lsch_anhand_Auftrag.gif) Auftrags-Icon. Damit werden alle offenen Positionen des Auftrages, soweit ein Lagerstand vorhanden ist, in den Lieferschein übernommen und somit der Auftrag soweit wie möglich erfüllt.

#### Können Texteingaben vom Auftrag in den Lieferschein übernommen werden?
Da die Texteingaben keine lieferbaren Positionen sind, werden die Texteingaben nicht automatisch mit in den Lieferschein übernommen. Um die einzelnen Texteingaben zu übernehmen, wählen Sie bitte die Texteingabe aus und klicken auf ändern und speichern. Damit wird diese Auftragsposition in den Lieferschein übernommen.

#### Wieso sind nach der Übernahme eines Auftrags im Lieferschein keine Positionen abgebucht?
Bei der Anlage eines Auftragsbezogenen Lieferscheines wird die Verknüpfung des Lieferscheines mit dem Auftrag hergestellt und die Kopfdaten des Auftrags in den Lieferschein übernommen. Inhalte des Auftrags werden nicht automatisch übernommen. D.h. Sie können dadurch die einzelnen Positionen des Auftrags, je nach der gewünschten auszuliefernden Menge in den Lieferschein übernehmen. Zugleich werden mit der durch die Übernahme hergestellten Verknüpfung die offenen Auftragspositionen mitverwaltet. D.h. es ist für Sie jederzeit ersichtlich, wieviel von einem Auftrag noch auszuliefern ist.

In der Sicht Auftrag kann jede einzelne noch offene Position des Auftrags in den Lieferschein übernommen werden. Zur Übernahme aller offenen Auftragspositionen siehe bitte oben.

#### Warum wird der Auftrag nicht automatisch vollständig abgebucht?
Die Praxis hat gezeigt, dass es immer wieder erforderlich ist, dass Artikel gleichzeitig an mehrere Kunden geliefert werden sollen, obwohl für die Erfüllung aller offenen Aufträge zuwenig Ware auf Lager ist. Daher muss der Anwender entscheiden können, wieviel für welchen Auftrag / welche Auftragsposition ausgeliefert wird.

#### Wo sehe ich den aktuellen Lagerstand eines Artikels?
Während der Übernahme der Auftragspositionen in den Lieferschein wird in der Statuszeile der aktuelle Lagerstand des Artikel, des für den Lieferschein gewählten Lagers angezeigt. (Anzeige neben dem Lieferscheinstatus)

![](LS_Statuszeile1.gif)

#### Ich will immer nur lagernde Artikel auswählen können, geht das?
Um nur lagernde Artikel vorgeschlagen zu bekommen, stellen Sie bitte den Parameter DEFAULT_ARTIKELAUSWAHL in der Kategorie Lieferschein auf 0.
Damit werden bei der Artikelauswahl in den Lieferscheinpositionen nur mehr die Artikel angezeigt, welche einen Lagerstand auf dem Abbuchungs-Lager des Lieferscheinkopfes bzw. aus dem übersteuerten Lager der LS-Positionen haben

#### Kann ich Auftragspositionen überliefern?
Übernehmen Sie vorerst die noch offene Auftragsmenge im Modulreiter Sicht Auftrag ![](LS_Sicht_Auftrag.gif). Ist die gewünschte Position nun laut Auftrag vollständig geliefert, so wechseln Sie in den Reiter Positionen ![](LS_Positionen.gif) und korrigieren Sie die Menge auf die gewünschte Menge. Dadurch haben Sie bewusst eine Überlieferung einer Auftragsposition herbeigeführt.

#### Kann ich bereits ausgelieferte Auftragspositionen überliefern?
Alternativ zu Obigem steht auch die Möglichkeit (früher) bereits vollständig ausgelieferte Auftragspositionen zu überliefern zur Verfügung. In der Sicht Auftrag des Lieferscheines entfernen Sie den Haken bei Nur Offene anzeigen.
![](Auftrag_zusaetzlich_ueberliefern.gif)
und klicken Sie auf aktualisieren. Nun sehen Sie alle Auftragspositionen und können durch Klick auf zusätzliche (Chargen-)Menge ![](Zusaetzliche_Chargennummer.gif) ausliefern diese eine Position bewusst überliefern.

#### Kann ich eine Auftragsposition gezielt unterliefern?
So wie es die oben beschriebene Überlieferung gibt, geht es bei der Unterlieferung darum, dies auch dem Kunden mitzuteilen. D.h. es sollte am Lieferschein bei Lieferrest nicht die unterlieferte Menge sondern eben Null angedruckt werden. Klicken Sie dazu auf den Haken ![](Kein_Lieferrest_mehr.gif) in der Sicht Auftrag des Lieferscheins der Lieferscheinposition.

#### Lieferung mehrerer Chargen auf eine Auftragsposition
Um eine Auftragsposition in einem Lieferschein mit mehreren Chargen und eventuell mit mehreren Chargendokumenten auszuliefern, steht die Funktion Zusätzliche Chargennummer angeben ![](Zusaetzliche_Chargennummer.gif) zur Verfügung.

Damit werden in den Lieferschein mehrere Positionen der gleichen Auftragsposition mit eventuell unterschiedlichen Chargen eingetragen.

#### Können die Los-Abliefermengen direkt in den Lieferschein übernommen werden?
Ja. Hat ein Lieferschein einen Bezug zum Auftrag und steht hinter der gewählten Position ein Los, welches dem Auftrag zugeordnet und die Identnummer der Lieferscheinposition hat, so kann durch Klick auf "Letzte Losablieferung übernehmen" diese übernommen werden. Genauer gesagt wird die Menge übernommen, die noch nicht geliefert wurde.

#### Wie wird ein Auftrag erledigt?
Automatisch wenn alle mengenbehafteten Positionen des Auftrags geliefert wurden.
Siehe dazu bitte auch

#### Wieso kann ich bei der Erstellung eines Auftragsbezogenen Lieferscheines den soeben angelegten Auftrag nicht auswählen
<a name="Wieso kann ich bei der Erstellung eines Auftragsbezogenen Lieferscheines den soeben angelegten Auftrag nicht auswählen"></a>
Damit ein Auftrag in der Liste der offenen Aufträge aufscheint muss er den Status offen (=gedruckt) oder teilgeliefert haben. Aufträge die angelegt, storniert oder bereits erledigt sind scheinen in der Liste nicht auf.

#### Was passiert beim Stornieren eines Lieferscheines?
Wenn ein Lieferschein storniert wird, werden alle Buchungen, die gemacht wurden, rückgängig gemacht. Das betrifft Lagerbuchungen, Auftragsreservierungen und die offenen Mengen in den zugehörigen Auftragspositionen. Die Lieferscheinpositionen behalten dabei ihre Informationen (Menge, Preis,...), der Status des Lieferscheins wechselt auf storniert. Der Storno eines Lieferscheins kann nur in der Form rückgängig gemacht werden, dass zwar die Positionen im Lieferschein erhalten bleibe, die Mengen werden jedoch auf Null gesetzt.

Im Auftrag Sicht Lieferstatus scheinen die Positionen von stornierten Lieferscheinen nicht auf.

#### Es wurde versehentlich der falsche Auftrag verknüpft. Kann ich das Ändern?
Um die Auftragszuordnung eines Lieferscheines zu ändern, oder auch die Verknüpfung zu einem Auftrag zu entfernen, müssen alle Positionen aus diesem Lieferschein entfernt werden. Dann gehen Sie wiederum in die Kopfdaten und wählen ändern. Es kann nun auch der Auftrag geändert oder die Zuordnung gelöscht werden. Zum Löschen der Verknüpfung mit dem Auftrag wählen Sie die Auftragsauswahl ![](Auftragsauswahl1.gif) und in der Auftragsauswahlliste dann löschen ![](Loeschen.gif).
Um aus einem Auftragsbezogenen Lieferschein einen Freien Lieferschein zu machen, muss zusätzlich noch die Lieferscheinart geändert werden.

#### Ich habe einen Lieferschein angelegt und kann ihn nicht mehr löschen. Warum?
Wenn Sie mit neu einen Lieferschein angelegt haben, dann alle Pflichtfelder befüllt haben und danach die Kopfdaten des Lieferscheines abgespeichert haben, so wird von **Kieselstein ERP** eine eindeutige Nummer für diesen Lieferschein vergeben. Laut Rechnungslegungsgesetz müssen Belege fortlaufend nummeriert sein. Aus diesem Grund kann der Lieferschein nicht mehr gelöscht werden. Er kann jedoch mit Klick auf ![](LS_Storno.gif) storniert werden.

Zum Storno ist folgendes zu beachten:
1. Beim Storno bleibt die Lieferscheinnummer erhalten, auch der Inhalt des Lieferscheines bleibt erhalten. Die auf dem stornierten Lieferschein angeführten Artikel mit Lagerbuchungen wurden jedoch durch das Storno wieder an das Lager zurückgegeben.
2. Es kann (braucht) nur ein Lieferschein im Status offen storniert werden.
Ein Lieferschein der bereits verrechnet oder erledigt ist darf nicht mehr storniert werden.
Ein Lieferschein der nur angelegt ist kann jederzeit verändert werden.
3. Klickt man bei einem stornierten Lieferschein in den Kopfdaten auf ändern, so kann damit das Storno wieder rückgängig gemacht werden. **Hinweis:** Dieses Verhalten ist für alle [Bewegungsmodule]( {{<relref "http://localhost:1313/verkauf/gemeinsamkeiten/#stornieren-von-bewegungsdaten"  >}} ) (Angebot - Bestellung) gleich.

#### Beim Versuch einen Lieferschein zu stornieren erscheint nachfolgende Meldung
![](Lieferschein_Storno.jpg)

Diese Meldung besagt, dass der Lieferschein im Status angelegt ist. Wie oben ausgeführt, kann dieser Lieferschein jederzeit verändert werden, daher ist er in diesem Status nicht stornierbar. Sollte er trotzdem storniert werden müssen, so drucken Sie diesen Lieferschein bitte aus, zumindest in die Vorschau und klicken dann in den Kopfdaten auf ![](LS_Storno.gif) stornieren.

#### Ich kann eine Artikelposition im Lieferschein nicht mehr ändern. Warum?
Aus Gründen der strengen Lagerlogik ist es derzeit nicht möglich, bei einer Lieferscheinposition einfach die Artikelnummer zu ändern bzw. die Positionsart von Ident auf z.B. Handeingabe zu ändern. Sie müssen derzeit die Position löschen und mit neu oder einfügen eine neue Position übernehmen.

#### Anzeige der Detaildaten in der Sicht Auftrag eines Lieferscheines
Die Anzeige ist von der Bearbeitung der einzelnen Position abhängig.

1.  Lesen: Es werden die tatsächlich erfassten Lieferscheinpositionen und Mengen angezeigt.
2.  Bearbeiten: Hier wird zwischen einer neu erfassten und der Bearbeitung einer bereits erfassten Lieferschein Position unterschieden.
    1.  Erfassen einer neuen Lieferscheinposition (eigentlich Übernahme einer Auftragsposition in den Lieferschein).<br>
    Als Menge wird die offene Menge der Auftragsposition oder die auf Lager verfügbare Menge, welche auch immer kleiner ist, vorgeschlagen. Die kleinste eingebbare Menge ist 0,001\. Ist ein Artikel Chargen oder Seriennummern pflichtig, so muss auch diese Information mit angegeben werden.
    2.  Wird eine bestehende Lieferscheinposition geändert, so wird hier die bereits erfasste / gebuchte Menge zur Bearbeitung vorgeschlagen.
3.  Beim Speichern der Lieferscheinposition wird:
    1.  nach der Erfassung einer neuen Lieferscheinposition diese angelegt.
    2.  nach der Änderung einer bestehenden Lieferscheinposition diese mit der entsprechenden Änderung abgespeichert.
    Danach ist die Lieferscheinposition auch in der Ansicht Positionen des Lieferscheines sichtbar und kann von dort gelöscht werden.
    3.  wurde ein Auftragsposition nur zum Teil ausgeliefert, so wechselt zugleich mit dem Speichern der Auftragsposition diese in den Status teilerledigt.
    4.  wurde eine Auftragsposition vollständig als geliefert gebucht, so scheint sie in der Sicht Auftrag nicht mehr auf. Für den Fall, dass trotzdem diese Lieferschein Position geändert werden sollte, muss dies vom Panel Positionen aus vorgenommen werden.

#### Was ist unter Bestellnummer einzugeben?
Bei einem Großteil der Geschäftsfälle ist es, schon aus Gründen der rechtlichen Sicherheit, erforderlich dass sich die Lieferung auf eine Bestellung Ihres Kunden bezieht. Diese Bestellnummer Ihres Kunden tragen Sie hier ein. Bezieht sich der Lieferschein auf einen Auftrag so wird dieses Feld automatisch durch den Auftrag vorbesetzt. Ist der Lieferschein das erste Papier in diesem Geschäftsfall, hat also der Lieferschein keinen Auftragsbezug, so kann die Bestellnummer hier direkt eingetragen werden. Die Bestellnummern werden in die Rechnung übernommen.<br>
Auch in der Rechnung steht dieser Bezug zur Kundenbestellnummer zur Verfügung.
Projekt und Bestellnummer werden in der Auswahlliste mit | getrennt angezeigt.

#### Nach dem Druck des Lieferscheines kann ich die Positionen nicht mehr verändern. Alle Knöpfe sind grau:
Durch den Druck ist der Lieferschein in den Status Geliefert gewechselt. In diesem Status dürfen die Lieferscheinpositionen nicht verändert werden. Zum Ändern des Lieferscheines wechseln Sie bitte in die Kopfdaten und Klicken Sie auf Ändern. Speichern Sie nun und beantworten Sie die Frage nach dem Zurücknehmen des Status mit Ja. Nun ist der Lieferschein wieder im Status angelegt und Sie können die Positionen wieder ändern. Der Status 'Geliefert' kann auch aus den Positionen heraus zurückgenommen werden, wenn eine neue hinzugefügt, eine bestehende bearbeitet bzw. eine Position gelöscht wird.

#### Kann auch ein Lieferschein an einen Lieferanten gesandt werden?
#### Lieferantenlieferschein
<a name="Lieferantenlieferschein"></a>
Ja. Wählen Sie einfach bei der Lieferscheinart Lieferant.
Dies wird z.B. auch dafür verwendet, wenn über den Wareneingang gebuchte Ware wieder an den Lieferanten zurückgesendet werden sollte.<br>
Bitte beachten Sie, dass ein Lieferschein an einen Lieferanten nicht verrechnet werden kann. In der Regel wird der Lieferschein solange offen gelassen, bis der Vorgang erledigt ist, Sie z.B. eine Gutschrift für die defekte Ware erhalten haben und erst wenn der Vorgang abgeschlossen ist, wird der Lieferschein manuell erledigt.

#### Wie sende ich Ware an Lieferanten zurück?
In der Regel taucht diese Frage auf, wenn Ware Ware über den Wareneingang der Bestellung eingebucht wurde. Nun stellt man fest dass man das nicht braucht, die Teile defekt sind o.ä.. D.h. Sie senden ganz einfach diese mittels Lieferschein wieder an den Lieferanten zurück.
Sie legen dazu einen neuen Lieferschein an. Nun stellen Sie die Lieferschein Art (ganz oben in den Kopfdaten) auf Lieferant.<br>
![](Lieferscheinart.jpg)<br>
**Hinweis:**
Die Lieferscheinart kann nur umgeschaltet werden, wenn im Lieferschein noch keine Positionen enthalten sind.

Nun klicken Sie auf Lieferant und wählen den gewünschten Lieferanten aus.
Damit werden die im Reiter Positionen eingefügten Artikel wiederum vom Lager abgebucht.

Denken Sie daran, dass in der Regel dieser Lieferschein nicht verrechnet wird. D.h. wenn die Lieferantengutschrift o.ä. gekommen ist, muss der Lieferschein manuell auf erledigt gesetzt werden.

#### Kann auch Waren mit einem Lieferschein wieder zurückgenommen werden?
Ja. Geben Sie dafür einfach bei der Menge ein negatives Vorzeichen an, damit wird die Ware wieder auf das Lager zugebucht. Beachten Sie bitte, dass für die Gestehungspreis-Berechnung wie unten beschrieben erfolgt. Dies kann unter Umständen zu einer entsprechenden Verfälschung des Gestehungspreises des Artikels führen. [Siehe dazu auch](#Kundenreklamation).

Gibt es für die Lieferscheinposition eine Auftragszuordnung, so wird anhand der gefundenen Auftragsposition der Gestehungspreis der damaligen Auslieferung ermittelt und dieser Gestehungspreis verwendet. Wird keine Zuordnung gefunden, so wird der aktuelle Gestehungspreis des Zubuchungslagers des Artikels verwendet.

Aus diesem Grunde kann es z.B. bei der Rücknahme von Lieferungen, ohne Auftragsbezug, welche z.B. direkt in ein Reparaturlager gebucht werden, vorkommen, dass nur der Gestehungspreis des Reparaturlagers verwendet wird. War nun der zurückgenommene Artikel noch nie am (Reparatur-)Lager, so ist der verwendete Gestehungspreis 0,00.
**Hinweis:** Gerade wenn der Lagerwert aus Versicherungs-, oder Zoll-Gründen auch dieses, meist nicht Ihnen gehörende Lager, werthaltig richtig sein muss, so definieren Sie bitte zusätzlich zur Zurücknahme der Artikel den aktuell gültigen Gestehungspreis dieses Artikels für das Lager.
Anmerkung: Vom Lageristen, der Wareneingangsbuchenden kann nicht verlangt werden, dass diese Betrachtung bereits bei der Wareneingangsbuchung bekannt ist. Daher muss eben die Bewertung nachträglich gemacht werden.

#### Verrechenbare / nicht verrechenbare Lieferscheine? Wozu ist das?
<a name="Verrechenbar"></a>
Grundsätzlich darf Ware Ihr Unternehmen nur dann verlassen, wenn es dafür zumindest einen Lieferschein gibt. Hier gibt es nun manchmal die Situation, dass Sie Ware versenden müssen, die im Endeffekt nicht verrechnet werden kann, wobei der dahinter liegende Vorgang aber noch nicht erledigt ist. Z.B. wenn Sie Ware an Ihren Lieferanten senden und auf eine Gutschrift warten. Trotzdem sollte klar zwischen noch nicht fakturierten Lieferscheinen und noch zu fakturierenden Lieferscheinen unterschieden werden.

Verwenden Sie dafür in den Konditionen des Lieferscheins die Checkbox ![](LS_Verrechenbar.gif).

D.h. ist die Checkbox nicht angehakt, so rechnen Sie auch nicht damit, dass dieser Lieferschein verrechnet werden wird. (Siehe dazu auch den Parameter LS_DEFAULT_VERRECHENBAR).

Um nun eine Auswertung über nur Verrechenbare bzw. nicht verrechenbare Lieferscheine zu erhalten, wählen Sie bitte Journal, offene Lieferschein und wählen in der Combobox unten ![](Lieferscheine_verrechenbar_nicht_verrechenbar.gif) die gewünschte Art aus.

Beachten Sie dazu bitte auch [Lagerwert zum Stichtag und offene Lieferscheine]( {{<relref "/docs/stammdaten/artikel/#lagerwert-zum-stichtag-und-offene-lieferscheine"  >}} )

#### In der Auswahlliste der Lieferscheine werden manche Lieferscheine in hellgrau dargestellt.
Damit werden bereits in der Übersicht die nicht verrechenbaren Lieferscheine dargestellt.

![](LS_nicht_verrechenbar.gif)

#### Können Lieferscheine manuell erledigt werden ?
<a name="Manuell Erledigen"></a>
Ja. Verwenden Sie dies, wenn z.B. die Garantieleistung abgeschlossen ist. Wählen Sie dazu im Lieferschein-Menü Bearbeiten, Manuell erledigen. ![](LS_Manuell_erledigen.gif)

#### Was bedeuten die einzelnen Spalten der Umsatzübersicht?
Sie finden in **Kieselstein ERP** in den Verkaufsmodulen jeweils eine Umsatzübersicht. [Siehe dazu auch]( {{<relref "/verkauf/angebot/#was-bedeuten-die-einzelnen-spalten-der-umsatz%c3%bcbersicht"  >}} )

Vor der eigentlichen Auswertung muss die Auswertungsart definiert werden. Wählen Sie aus:

![](LieferscheinUmsatzUebersicht1.gif)

![](LieferscheinUmsatzUebersicht2.gif)

Sie erhalten, passen zur obigen Definition, den Tage, Wochen oder Monatsweisen Umsatz anhand der Lieferscheine. Als grobe Orientierung der ausgelieferten Werte. Zusätzlich wird die Anzahl der Lieferscheine angegeben um Ihnen ein Gefühl für die Größe der Lieferungen zu vermitteln. Als Basis werden die Nettopreise der Lieferscheinpositionen, korrigiert um die Lieferscheinkonditionen verwendet.

Gerechnet wird immer vom aktuellen Datum aus 20 Einträge (Tage, Wochen, Monate) in die Vergangenheit.

#### Unsere Versandabteilung sollte nur die Versanddaten im Lieferschein eintragen, aber sonst nichts an den Lieferscheinen ändern. Geht das?
Ja. Geben Sie den Benutzern nur lesendes Recht auf die Lieferscheine. Damit können die Lieferscheine ausgedruckt werden. Zusätzlich können Warenausgangsetiketten, mit den Inhalten des Lieferscheines und das Lieferschein Adressetikett ausgedruckt werden. In der Vorschau des Adressetikettes können auch die Versanddaten, also Versandnummer, Gesamtgewicht der Sendung, Auslieferdatum und Anzahl der Pakete  eingegeben werden. Klicken Sie dafür auf ändern (bearbeiten) der Versanddaten und geben Sie die erfassten Daten entsprechend ein. Beachten Sie bitte, dass vor dem erneuten Ausdruck die Anzeige mit dem **oberen** aktualisieren Knopf erneut dargestellt werden muss.

#### Kann die Anzeige der Lieferschein Werte abgeschaltet werden?
Ja. Die Werteanzeige im Lieferschein steht nur für die Benutzer zur Verfügung, die auch das Recht LP_DARF_PREISE_SEHEN_VERKAUF besitzen.

#### Was definiert Liefer- bzw. Rechnungsadresse?
Üblicherweise werden Liefer- und Rechnungsadresse durch die Übernahme aus dem Auftrag definiert. Wird ein Lieferschein ohne Auftragsbezug hergestellt so wird die Rechnungsadresse anhand des Kunden, der durch die Lieferadresse ausgewählt wurde, definiert.

Hat ein Kunde mehrere Lieferadressen, so sollte diese so definiert sein, dass alle auf die gleiche Rechnungsadresse zeigen.

Ergibt sich aus einer eventuellen Änderung der Lieferadresse eine andere Rechnungsadresse so wird diese nach Rückfrage übernommen. [Siehe dazu auch]( {{<relref "/docs/stammdaten/kunden/#rechnungsadresse" >}} )

#### Kann die Rechnungsadresse geändert werden?
Sie finden Sie im Menü Bearbeiten den Punkt Rechnungsadresse ändern. Hiermit können Sie die Rechnungsadresse ändern solange der Status des Lieferscheins angelegt oder offen ist.

#### Was definiert welche Konditionen und Verkaufspreise verwendet werden?
Dies wird durch die Rechnungsadresse festgelegt. D.h.:

Lieferart und Spedition werden von der Lieferadresse des Lieferscheins definiert.

Zahlungsziel und allgemeiner Rabatt, aber auch die Verkaufspreisfindung werden durch die Rechnungsadresse definiert. Das bedeutet auch, dass die Kundenartikelnummern, welche über die Sonderkonditonen definiert werden, anhand der Rechnungsadresse ermittelt werden.

#### Kann ein Bezug zum Wareneingang angedruckt werden?
Ja. In der Reportvorlage des default Lieferscheines gibt es das Feld WE_REFERENZ. In diesem ist der Bezug ausgehend von der Lieferscheinposition zur dazugehörenden Lagerzugangsbuchung abgebildet.

D.h. es kann hier sehr klar dargestellt werden aus welcher Wareneingangsposition bzw. aus welcher Losablieferung der jeweilige Warenzugang stammt. Dies wird gerne verwendet um verschiedene Liefer-Zeugnisse, Herstellungs-Zertifikate auszudrucken.

Der Inhalt dieses Feldes sieht z.B. so aus: Bestellung|09/0123456|Lschnr|14.01.2010

Die Bedeutung ist wie folgt:

Anlieferart|Belegnummer|Wareneingangslieferscheinnummer wenn WE aus Bestellung|Wareneingangsdatum

#### Kann ein Lieferschein mit Preisen angedruckt werden?
Ja. Haken Sie dazu im Kunden der Lieferadresse des Lieferscheines, im Reiter Konditionen, das Feld ![](Preise_am_LS_andrucken.gif) Preise am LS andrucken bei der jeweiligen Lieferadresse des Lieferscheines an. Damit wird anstelle der Bestell-, Restmengen Info der Verkaufspreis angedruckt.

#### Nachträgliche Aktualisierung der Preise aus dem Auftrag in den offenen Lieferscheinen?
[Siehe dazu]( {{<relref "/verkauf/auftrag/#nachtr%c3%a4gliche-aktualisierung-der-preise-aus-dem-auftrag-in-den-offenen-lieferscheinen"  >}} )

#### Kann eine Begründung für einen eigenen Lieferverzug eingegeben werden?
<a name="Eigene_Termintreue"></a>
Ja.

Um die Verbesserung der eigenen Liefertermintreue steuern und beurteilen zu können, ist vorerst erforderlich, dass die Ursachen herausgefunden werden, warum es zu Verzögerungen der Lieferung kommt. Daher kann die Begründung für eine Lieferverzögerung für jeden Lieferschein angegeben werden.

Die möglichen Gründe müssen im Modul Lieferschein, unterer Modulreiter Grunddaten, oberer Modulreiter Begründung definiert werden.

Um nun den Grund für die Terminverzögerung am einzelnen Lieferschein anzugeben, wählen Sie in der Auswahlliste der Lieferscheine den gewünschten Lieferschein aus und wählen über den Menüpunkt Bearbeiten, Begründung die zutreffende Begründung aus.

Im [Warenausgangsjournal]( {{<relref "/verkauf/rechnung/#warenausgangsjournal"  >}} ) wird neben der Terminverzögerung im Vergleich zum im Auftrag eingetragenen Liefertermin, auch die Begründung mit angeführt.

#### Wo kann die eigene Termintreue ausgewertet werden ?
Siehe [Warenausgangsjournal]( {{<relref "/verkauf/rechnung/#warenausgangsjournal"  >}} ) 

#### Wie kann man aus den Reklamationen einen Lieferschein erstellen, ohne im Lager den Artikel manuell ein- und auszutragen?
Kunden und auch Lieferanten Reklamationen können mit den Lieferscheinen abgewickelt werden.

1. Lieferantenreklamation<br>
Wir haben dafür die Möglichkeit geschaffen, dass Lieferscheine mit der Lieferscheinart Lieferanten auch an Lieferanten geschrieben werden können. Üblicherweise wird zuerst die Ware zugebucht und erst danach wird festgestellt, dass die Ware zu reklamieren ist. Daher wird die Ware mit einem Lieferantenlieferschein an den Lieferanten zurückgesandt.<br><br>
Üblicherweise wird zugleich der Lieferschein auf nicht verrechenbar gesetzt (Konditionen). Haben Sie von Ihrem Lieferanten die Gutschrift zu dieser Lieferung erhalten, so wird der Lieferschein manuell erledigt.<br><br>
Wird die Ware wieder benötigt, so hängt die weitere Behandlung auch mit Ihrer Vereinbarung mit Ihrem Lieferanten ab.<br><br>
Haben Sie eine Gutschrift erhalten, so wird, z.B. über den Bestellvorschlag der erneute Bedarf automatisch wieder vorgeschlagen. Dadurch ist auch der Bestell-/Einkaufspreis in der Bestellung und in weiterer Folge beim Wareneingang anzuführen und somit wird auch der Gestehungspreis richtig weitergerechnet.<br><br>
Machen Sie jedoch eine Ersatzbestellung mit Null-Preisen, so ist zu beachten, dass durch die Zubuchung von Ware mit einem Null-Preis der Gestehungspreis auf den neuen Durchschnittswert gesetzt wird und damit der **erwartete Gestehungspreis <u>zerstört</u>** wird. D.h. hier ist bei den Einstandspreisen manuell einzugreifen.<br><br>
Es ist daher das Gutschriftsverfahren eindeutig vorzuziehen.

2. Kundenreklamationen<br>
Bei Kundenreklamationen erhalten Sie Ware vom Kunden zurück. Diese buchen Sie durch einen Lieferschein mit der Angabe von negativen Mengen wieder ans Lager. Das hat auch den Vorteil, dass dieser Rücknahme-Lieferschein ebenfalls in eine Sammel-Rechnung eingebunden werden kann und so dem Kunden nur der Differenzwert fakturiert wird.

Für weitere Themen der Reklamation siehe bitte [Reklamationsverwaltung]( {{<relref "/fertigung/reklamation" >}} ).

#### Können mehrere Aufträge in einen Sammel-Lieferschein übernommen werden ?
Ja. Dazu dient der obere Modulreiter Auftrag im Modul Lieferschein.

D.h. wenn ein weiterer Auftrag mit dem gleichen Lieferschein ausgeliefert werden sollte, so gehen Sie nachdem Sie die lieferbaren Positionen des ersten Auftrags in Sicht Auftrag gebucht haben in den oberen Modulreiter Auftrag und wählen mit Neu einen weiteren Auftrag des gleichen Kunden und der gleichen Lieferadresse aus. Nun wechseln Sie wiederum in den oberen Modulreiter Sicht Auftrag und buchen die zu liefernden Positionen dieses Auftrags. Und so fort, bis alle Lieferungen an den Kunden auf dem einen Sammellieferschein erfasst sind.

Sowohl im Ausdruck der Sammellieferscheines als auch im Ausdruck der Rechnung werden die Verknüpfungen zu den Aufträgen entsprechend dargestellt.

#### Von welchem Lager bucht der Lieferschein ab?
<a name="Abbuchungslager"></a>
Der Lieferschein bucht von dem in den Kopfdaten unter ![](Abbuchungslager_definieren.gif) definierten Lager ab. Die Vorbelegung des Abbuchungslagers kann im Kunden, bei Lieferantenlieferscheinen entsprechend beim Lieferanten in den Konditionen unter ![](Abbuchungslager_vorbesetzen.gif) ab Lager eingestellt werden.

**Hinweis:**

Wird ein Auftragsbezogener Lieferschein erzeugt, so ist dem Lieferschein das wie oben beschriebene Abbuchungslager zugeordnet. Um nun eventuell ein anderes Lager zu verwenden, muss vor der ersten Abbuchung, vor der Übernahme der ersten Position aus dem Auftrag in den Lieferschein, das in den Kopfdaten des Lieferscheines definiert werden.

Sind bereits Positionen im Lieferschein eingetragen so kann das Lager nicht mehr geändert werden. Gegebenenfalls sind die Positionen aus dem Lieferschein zu löschen, diese vorher in die Zwischenablage kopieren um den Aufwand zu minimieren.

#### Welches Abbuchungs-Lager wird bei einem Lieferanten-Lieferschein vorgeschlagen?
Wenn ein Lieferschein an einen Lieferanten gesandt wird, so wird im Hintergrund ein verdeckter Kunde angelegt. Für diesen Kunden gilt als Standard Regel, dass sein Abbuchungslager das Hauptlager ist. Also wird das Hauptlager als Abbuchungslager für einen Lieferantenlieferschein vorgeschlagen.

#### Kann der Lieferschein auch für Umbuchungen verwendet werden?
Ja.

Oft gibt es die Situation, dass Ware zwar an einen Kunden verkauft wird, aber dass für diesen Kunden trotzdem die Lagerbewirtschaftung gemacht wird. Oder auch die umgekehrte Situation dass Sie zwar Ware in ein Kundenkonsignationslager liefern, die Ware aber trotzdem noch Ihnen gehört.

Dafür stehen zwei Einstellungen / Funktionen zur Verfügung:

In den Kopfdaten des Lieferscheines kann neben dem (Abbuchungs-) Lager auch das Ziellager definiert werden. Wurde ein Ziellager definiert, so wird die Ware vom Abbuchungslager abgebucht und dem Ziellager zugebucht.

Je nach Vereinbarung mit Ihrem Kunden, welche sich auch immer im Typ des Lagers niederschlägt, kann der Lieferschein nun als erledigt gekennzeichnet werden oder er wird wie üblich verrechnet.

Für den Fall, dass dies ein auftragsbezogener Lieferschein ist, denken Sie bitte daran, dass die Lager Vorgaben in den Kopfdaten des Lieferscheines nur geändert werden können, wenn in diesem keine Positionen enthalten sind. D.h. wechseln Sie zuerst von Sicht Auftrag in die Kopfdaten und stellen Sie die Lager entsprechend ein. Erst danach erfüllen Sie den Auftrag unter Sicht Auftrag.

**Hinweis:**<br>
Diese Funktion steht auch bei Lieferscheinen an Lieferanten zur Verfügung. D.h. damit haben Sie die Möglichkeit Ware an Ihren Lieferanten z.B. als Beistellmaterial zu liefern und dieses Material dann im Fertigungslos zu verbrauchen. Siehe dazu auch [Abbuchungsläger im Los definieren]( {{<relref "/fertigung/losverwaltung/#l%c3%a4ger-eines-loses"  >}} ).

**Hinweis:**<br>
Wird im Lieferschein ein Ziellager definiert, so bewirkt dies immer dass:
1. der Artikel vom Lager abgebucht wird
2. der Artikel dem Ziellager zugebucht wird. Bei der Zubuchung muss ein Gestehungspreis angegeben werden. Für diesen Gestehungspreis wird der aktuelle Gestehungspreis des Abbuchungslagers zum Zeitpunkt der Buchung verwendet. Damit ist über die beiden Lager betrachtet, die Wertänderung gleich Null.

#### Kann bei einer Position das Lager übersteuert werden?
Ja, Sie können in der Eingabe der Position ein Lager auswählen, dass die Eingabe in den Kopfdaten übersteuert. Das bedeutet, dass in den Lieferscheinpositionen und unter Sicht-Auftrag kann unter dem Artikel das zu übersteuernde Lager ausgewählt werden (nur, wenn der Parameter LV_POSITION=0).

![](uebersteuertes_Lager.JPG)

#### Können die Lieferkonditonen für einen Lieferanten für den Lieferschein definiert werden?
Ja. [Siehe dazu]( {{<relref "/docs/stammdaten/lieferanten/#lieferkonditionen-f%c3%bcr-die-lieferschein-lieferanten-anpassen"  >}} ).

#### Können Versandetiketten erzeugt werden?
Ja.
Es gibt drei Arten von Versandetiketten die aus dem Lieferscheinmodul von **Kieselstein ERP** ausgedruckt werden können.
Es können diese drei Funktionen alle aus dem Menü des Lieferscheinmoduls, Menüpunkt Lieferschein aufgerufen werden.
1. Adressetikett<br>
Dient als zentraler Paketaufkleber. Geben Sie hier gegebenfalls die Versandnummer Ihres Versandunternehmens / Spediteurs ein, die Anzahl der Pakete, das Gewicht und das tatsächliche Auslieferdatum.
2. Warenausgangsetikett<br>
Wenn aus dem Menü aufgerufen, wird für jede mengenbehaftete Position ein entsprechendes Etikett mit den Adressdaten und den Positionsdaten gedruckt. Auch hier können Versandnummer, Anzahl der Pakete, das Gewicht und das tatsächliche Auslieferdatum angegeben werden. Zusätzlich kann die anzudruckende Menge übersteuert werden.<br>
Wird dieser Punkt aus einer einzelnen Position aufgerufen ![](WA_Etikette.gif), so wird ein Warenausgangsetikett nur für die gewählte Position gedruckt.<br>
- Ausdruck von nachträglichen Warenausgangsetiketten für bereits verrechnete Lieferscheine.<br>
In manchen Situationen ist es erforderlich, dass nachträglich Warenausgangsetiketten ausgedruckt werden. Dies ist ebenfalls möglich. Der wesentlichste Unterschied ist, dass in diesem Falle die Versanddaten NICHT in den Lieferscheindaten mitgespeichert werden (da bereits verrechnet). Dies ist durch die nicht vorhanden Ändern-/Speichern-Buttons ersichtlich.
3. Versandetiketten
Mit dieser Funktion können Verpackungs-Versandaufkleber für alle Positionen eines Lieferscheins auf einmal erzeugt werden.<br>
Die Idee dahinter ist, hinter jedem Artikel ist unter Verpackungsmenge (im Artikel unter Sonstiges) definiert, welche Menge maximal in einen Versandkarton passt. Daraus kann errechnet werden, wieviele Kartons je Position benötigt werden, selbstverständlich unter Berücksichtigung der Restmenge. Dies wird für jede Lieferscheinposition errechnet, wodurch sich die Gesamtanzahl der Pakete ergibt.<br>
Diese Daten werden als Gesamtes übergeben und so können mit einem Druck die Versandaufkleber für einen kompletten Lieferschein gedruckt werden.<br>
In den Versandetiketten sind auch die Basisdaten für SSCC mit enthalten.

#### Können Daten an externe Versanddienstleister übergeben werden?
Ja, [siehe bitte]( {{<relref "/verkauf/lieferschein/versanddienstleister">}} ).

#### Wie mit den Transportmitteln umgehen?
[Siehe dazu bitte]( {{<relref "/verkauf/lieferschein/transportmittel">}} ).

#### Wie ist für den Druck von Chargenreinen Etiketten vorzugehen?
Unter der Voraussetzung, dass dies alles auf einmal ausgedruckt werden sollte und dass der Druck Versandetiketten verwendet wird, muss lediglich je Charge eine eigene Lieferscheinposition angelegt werden.

#### Abbildung der Nachweispflicht für EU-Lieferungen
In Deutschland gelten Nachweispflichten für IG-Erwerb Lieferungen. Mit der Gelangensbestätigung muss der Empfänger der Lieferung bestätigen, dass dieser die Produkte erhalten hat. Hierzu wurde in **Kieselstein ERP** der Ausdruck des Lieferscheins erweitert, so dass je nach Länderart der Lieferadresse das Formular auch die Bestätigung des Erhalts beinhaltet. Wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer, wenn auch für Sie die Nachweispflicht gültig ist.

#### Warum wird der Lieferrest für eine Auftragsposition nicht angeführt?
Der Lieferrest wird für Positionen mit offenen Mengen aus dem Auftrag angeführt. Sobald eine Position des Auftrags in den Lieferschein über Sicht Auftrag übernommen wird, wird diese Information angeführt. Positionen, die nicht im Lieferschein geliefert werden, werden auch nicht als Position mit der Lieferrest (=Auftragsmenge) angezeigt.

#### Kann die Information über einen etwaigen Lieferrest am Lieferschein angedruckt werden?
Der Ausdruck des Lieferscheins kann um die Informationen zum Lieferrest ergänzt werden.
Im Lieferschein-Report finden Sie den Parameter P_TEILLIEFERUNG (0=keine Teillieferung, 1= Teillieferung Auftragsumfang nicht vollständig, 2= Teillieferung, dies ist die letzte Lieferung des Auftrags).
Gerne passt Ihr **Kieselstein ERP** Betreuer den Report und dessen Texte für Sie an.

#### Wie können Lieferscheinpositionen schnell erfasst werden?
<a name="Barcode Erfassung"></a>
Hierzu gibt es die Lieferschein-Schnellerfassung mittels Barcodescanner: Nach Klick auf das Scannersymbol ![](barcode_schnellerfassung_positionen.PNG) geben die Artikelnummer und Menge ein und speichern mittels Enter-Taste.<br>
Bitte beachten Sie dazu auch den Parameter SCHNELLANLAGE_SUCHE_ZUERST_NACH_EAN. Mit diesem wird nach dem gescannten Barcode in folgender Reihenfolge gesucht:
- Einkaufs-Ean mit entsprechender Mengenübersetzung
- Verpackungs-EAN-Nr mit entsprechender Mengenübersetzung aus Verpackungsmenge
- Verkaufs-EAB-Nr
- Artikelnummer

Mengenübersetzung bedeutet, dass die eingegebene Stückzahl mit der Verpackungsmenge multipliziert wird.<br>
Beispiel: eine Palette Getränk mit 12Stk einzelnen Flaschen.<br>
Sie scannen den Verpackungs-EAN und geben 2Stk ein, also werden 24 einzelne Flaschen gebucht.

### Ausliefervorschlag - Rückstandsauflösung
<a name="Ausliefervorschlag"></a>
Im Lieferschein steht im unteren Modulreiter auch die Funktionalität des Ausliefervorschlages zur Verfügung. So können Sie schnell die zu liefernden und derzeit lagernden Positionen aus Ihren Aufträgen einsehen. Lösen Sie die Rückstände mit dem **Kieselstein ERP** Ausliefervorschlag auf. Wenden Sie sich vertrauensvoll an Ihren **Kieselstein ERP** Betreuer damit dieser die Funktion für Sie zur Verfügung stellt.

Ausgehend davon, dass die Kunden-Aufträge und die Forecastaufträge ordentlich gepflegt sind, insbesondere was die Reihung der Termine betrifft, kann unter Berücksichtigung der Lieferdauer an den Kunden errechnet werden, wann welche Ware in den Versand gelangen muss.<br>
D.h. es wird beim Ausliefervorschlag, analog z.B. dem Bestellvorschlag, unter Angabe eines Stichtages errechnet, welche Ware zum gewählten Stichtag zum Versand gebracht werden muss. In diesem Ausliefervorschlag wird neben den offenen Auftragspositionen auch der Forcastauftrag Call-Off Tag und Call-Off Woche berücksichtigt.<br>
Da in der aller Regel diese Ware auch, ohne lange Manipulation der Mengen ausgeliefert werden muss, gibt es im Ausliefervorschlag, nach der Erzeugung desselben, nur die Möglichkeit einzelne Positionen herauszulöschen. Eine Änderung von Artikelnummern oder Mengen ist nicht vorgesehen.<br>
Zusätzlich ist zu beachten, dass nur mit dem Ausliefervorschlag der Call-Off Tag/Woche (also die Forecastposition) ausgeliefert werden kann. Sollte hier die Situation eintreten, dass nur Teilmengen geliefert werden können / sollten, so ist zuerst der Eintrag im Forecastauftrag anzupassen und danach der Ausliefervorschlag ev. neu zu erzeugen.

Der Ausliefervorschlag wird nur für den angemeldeten Mandanten errechnet. Bei der Berechnung wird als Ausgangsbasis das Hauptlager des jeweiligen Mandanten herangezogen. Aus technischen Gründen (Sortierung, Filterung) wird der zum Errechnungszeitpunkt vorgefundene Lagerstand mit in den Ausliefervorschlag übernommen. Das bedeutet, sollte sich nach der Erzeugung des Ausliefervorschlages Lagerstände verändern, so ist dieser Status durch Klick auf ![](Ausliefervorschlag_Lager_aktualisieren.gif) zu aktualisieren.

Da es in der Praxis passieren wird, dass trotz aller Maßnahmen zu wenig der auszuliefernden Artikel auf Lager sind, werden diese Zeilen in unterschiedlichen Farben dargestellt.
| Farbe | Bedeutung |
| --- | --- |
| Rot | wenn für die Position kein Lagerstand gegeben ist |
| Orange | wenn zwar Ware auf Lager ist, aber für die Lieferung ein zu geringer Lagerstand gegeben ist. Hier ist auch die Verbrauchsberechnung nach terminlicher Staffelung mit enthalten.
![](Ausliefervorschlag.gif) |

Bei der Überleitung der Positionen in Lieferscheine werden nur diejenigen Positionen übernommen, die so wie im Ausliefervorschlag definiert, vollständig in den Lieferschein eingetragen werden können. D.h. nach der Umwandelung des Ausliefervorschlages in die Lieferscheine, wobei diese nach Kunde und Lieferadresse zusammengefasst sind, sollte der Ausliefervorschlag leer sein. Sind Positionen im Ausliefervorschlag geblieben, so bedeutet dies, dass sie nicht geliefert werden konnten. Bitte handeln Sie entsprechend rasch und umsichtig.

Bitte beachte auch, dass nur Artikel in den Lieferschein übernommen werden, welche weder Serien- noch Chargennummern geführt sind.<br>
Für Serien- bzw. Chargengeführten Artikel empfehlen wir die Verwendung der [mobilen App]( {{<relref "/docs/installation/05_KES_App"  >}} ), um die auszuliefernden Positionen exakt zu erfassen.<br>
Manche Anwender nutzen dann den Vorschlag nur dafür um sicher zu stellen, dass alle Positionen geliefert sind (alternativ zum Journal offene Auftragspositionen)

Wichtig: Nur angelegte Aufträge werden NICHT berücksichtigt, womit auch bei der Auftragsfreigabeverwaltung NICHT freigegebene Aufträge NICHT ausgeliefert werden.
Bitte beachten Sie auch, dass wiederholende Aufträge NICHT berücksichtigt werden.
Da auf Rahmenaufträge keine Lieferungen erfolgen, durchgeführt werden können (sondern nur Abrufe), werden auch diese NICHT berücksichtigt.<br>
Hinweis: Für die Verrechnung von Zeiten im Sinne von Dienstleistungen siehe bitte [Abrechnungsvorschlag]( {{<relref "/verkauf/rechnung/abrechnungsvorschlag">}} ).

Durch den Klick auf ![](Ausliefervorschlag_ueberleiten.gif) werden Lieferscheine erzeugt.
Hier können alle Lieferscheine für einen Kunden bzw. nur eine bestimmte Lieferadresse erzeugt werden oder auch automatisch alle Lieferscheine für die Positionen der Auslieferliste mit entsprechenden Lagerständen erzeugt werden. Bitte beachten Sie, dass die Lieferscheine immer mit aktuellem Datum angelegt werden.

#### Welche Lager werden bei der Berechnung des verfügbaren Lagerstandes berücksichtigt?
Es wird für die Reservierungen aus den Aufträgen das Lager aus den Kopfdaten und für die Forecastaufträge das beim Kunden hinterlegte Abbuchungslager verwendet. Das bedeutet auch, dass eventuell auf anderen Lagern befindliche Artikel, z.B. QS-Lager o.ä., NICHT in die Verfügbarkeitsberechnung eingehen.

### Journal Linienabrufe
[Siehe]( {{<relref "/verkauf/forecast/#wo-sehe-ich-die-zu-liefernden-linienabrufe">}} ).

### Lieferscheine verketten, je Palette eigene SSCC erzeugen
Einige unserer Anwender dürfen große (Lebenmisttel-) Konzerne beliefern. Von diesen gibt es unter anderem die Forderung, dass je Palette eine weltweit eineindeutige [SSCC]( {{<relref "http://localhost:1313/docs/stammdaten/artikel/#ean-gtin-sscc-gs1">}} ) angegeben werden muss. Dies ist in **Kieselstein ERP** so gelöst, dass pro Palette ein Lieferschein erzeugt wird, auch wenn die Lieferung aus mehreren Paletten bestehen sollte. Um nun trotzdem nur einen Satz an Transportpapieren zu haben, sollten diese Lieferscheine verkettet werden.<br>
Lieferscheine verketten bedeutet es werden mehrere Lieferscheine an einen Kopf-Lieferschein gehängt / verkettet und so diese Lieferscheine beim Ausdruck des jeweiligen Lieferscheines immer ausgehend vom Kopflieferschein als ein Dokument ausgedruckt.

Durch die freigeschalteten Funktion Lieferscheine verketten wird in der Lieferscheinauswahl zusätzlich der Kopflieferschein in der Auswahlliste angezeigt.
![](LS_verketten1.jpg)

Zusätzlich steht der Reiter Verkettet zur Verfügung.
Um nun mehrere Lieferscheine miteinander zu verketten wählen Sie in der Auswahlliste den Kopflieferschein und klicken danach auf den Reiter Verkettet.
Nun fügen Sie mit neu ![](LS_verketten2.gif) einen weiteren Lieferschein der gleichen Lieferadresse hinzu. Mit ![](LS_verketten3.gif) mehrere Lieferscheine verketten, können im nachfolgenden Dialog mehrere Lieferscheine markiert und mit den grünen Haken ![](LS_verketten4.gif) übernommen werden.
Bitte beachten Sie, dass das verketten unabhängig vom Lieferschein-Status möglich ist, in aller Regel aber vor dem Ausdruck des eigentlichen Lieferscheines erfolgen wird.
Versehentlich verkettete Lieferscheine können durch Klick auf ![](LS_verketten5.gif) löschen wieder aus der Liste entfernt werden.

Bitte beachten Sie bei der Verrechnung von verketteten Lieferscheinen, dass diese normalerweise alle in die gleiche Rechnung eingebunden werden.

Dass Lieferscheine verkettet sind, wird in der Auswahlliste durch blaue Lieferscheinzeilen signalisiert.<br>
![](LS_verketten6.gif)<br>
Zugleich wird in den untergeordneten Lieferscheinen der jeweilige Kopflieferschein mit angezeigt, damit die Verkettung ersichtlich ist.

Beim Druck eines verketteten Lieferscheines wird jeder der verketteten Lieferschein angeführt, obwohl es nur ein Papier ist und dafür die Lieferscheinnummern der zugeordneten Lieferscheine mit angeführt.

#### Lieferung von Auftragsbezogenen Setartikeln
Bei der Lieferung von Setartikeln aus Aufträgen, insbesondere bei teilweisen Positionen mit Serien- bzw. Chargennummern behafteten Artikeln hat sich, auch in Kombination mit den mobilen Barcodescannern, folgende Vorgehensweise bewährt.
1. Liefern / buchen Sie aus dem Lieferschein, Sicht Auftrag die Seriennummern und Chargennummern tragenden Artikel. Achten Sie dabei darauf, dass die Setmengen stimmen. D.h. es sollte sich immer eine ganzzahlige Satzgröße des Sets ergeben.
2. nun liefern Sie den Setkopf in der richtigen Satzgröße und damit alle nicht Serien- bzw. Chargennummern tragenden Artikel
3. passen Sie die Sortierung im Lieferschein entsprechend an

Wenn Sie einen unserer mobilen Barcodescanner verwenden, gehen Sie bitte in gleicher Weise vor.

#### Erstellung von Lieferscheinen in Zusammenhang mit Forecast Auftragsverteilung
Ist diese Funktion aktiviert, so wird beim Schreiben von Lieferscheinen, egal ob manuell oder per mobilen Barcodescannern, für jede Buchung versucht eine offene Auftrags- bzw. Forecastposition zu finden. Damit kann einfach verfügbare Ware / Produkte geliefert werden und Ihr **Kieselstein ERP** ordnet dies chronologisch den jeweiligen offenen Aufträgen zu. Sind keine offenen Aufträge mehr vorhanden kommt eine entsprechende Meldung.
Zu diesem Verhalten gibt es zwei Ausnahmen:
1. Lieferung von z.B. Verpackungsmaterial, wie z.B. Paletten. Um diese Artikel mit in den Lieferschein aufnehmen zu können, muss der Artikel als ... gekennzeichnet sein
2. Lieferungen an Lieferanten. D.h. wenn die Lieferscheinart auf Lieferant steht, wird, obwohl obige Funktion aktiviert ist, keine Zuordnung in den Aufträgen gesucht.

#### Was bedeuten die Zeichen in der rechten Spalte der Lieferscheine
In der linken Spalte der Lieferscheine wird die Art des Lieferscheins dargestellt. Diese haben folgende Bedeutung

| Zeichen | Bedeutung |
| --- |  --- |
| leer | freier Lieferschein |
| A | Auftragsbezogener Lieferschein |
| L | Lieferanten Lieferschein |
| M | Auftragsbezogener und somit Lieferschein an den anderen Mandanten |
| m | freier Lieferschein an einen anderen Mandanten |
| ml | freier Lieferschein an die Lieferadresse des anderen Mandanten |

#### Kann am Lieferschein auch das Artikelgewicht angedruckt werden?
Um das Gewicht am Lieferschein anzudrucken, muss in den Kunden, Konditionen, ![](LS_Gewicht_angeben.gif) am LS Gewicht angeben angehakt sein. Siehe dazu bitte auch den Parameter DEFAULT_AM_LS_GEWICHT_ANDRUCKEN.

Hinweis: Bei einem Lieferschein an einen Lieferanten muss diese Definition am hinter dem Lieferanten verbundenen Kunden angehakt werden. D.h. Sie gehen dazu bitte wie folgt vor:
1. Sie wechseln auf den Lieferanten, Reiter Konditionen
2. hier finden Sie rechts oben ![](GoTo_Lieferkonditionen.gif) Goto Lieferkonditionen. Durch Klick auf den GoTo Button gelangen Sie
3. in die Lieferkonditionen der zum Lieferanten zugehörigen Kunden-Konditionen.
4. Haken Sie hier am LS Gewicht angeben an um für diesen Lieferanten das Gewicht am Lieferschein anzudrucken.

Hinweis: Wenn dies ein Lieferant ist, der zugleich Kunde ist, wirkt dies auch für Kundenlieferscheine.

#### Kann beim Lieferschein auch ein Kommentar angegeben werden?
Wählen Sie dazu Bearbeiten, interner Kommentar.<br>
Ist dieses Feld befüllt, so wird in der Auswahlliste der Lieferscheine in der Spalte Kommentar ein I für interner Kommentar angezeigt.


## Tipps und Infos
### Das EMail des Lieferscheines sollte immer an die Spedition gesandt werden.
Ist bei der Spedition (System, Mandant, Lieferart) eine EMail Adresse eingetragen, so wird das EMail an den Spediteur versandt, damit er in deinem Namen die Ware versenden kann und deinen Lieferschein dazugeben kann.<br>
Wenn das nicht gewünscht ist, die EMail Adresse beim Spediteur entfernen. Ev. anstatt dessen die formulargesteuerte Ansprechpartnerfunktion nutzen.

