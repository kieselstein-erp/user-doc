---
title: "Lieferschein, Transportmittel"
linkTitle: "Transportmittel"
categories: ["Transportmittel"]
tags: ["Transportmittel"]
weight: 10
description: >
  Transportmittel die dem Kunden gehören, möglichst einfach verwalten
---
Transportmittel
===============

Da das Thema wie denn nun die Transportmittel verwalten, wie denn Transporte bestellen, wie mit Leergut umgehen, usw. immer mehr unserer Anwender interessiert, hier einige Gedanken, Ideen um dieses Thema zu lösen.

## Vorkalkulation
Um z.B. den Aufwand für den Versand von Ware an Kunden bereits in der Stückliste mitzurechnen, empfiehlt sich einen Artikel anzulegen, der nicht lagerbewirtschaftet ist und der zwar einen Einkaufspreis hat, aber keinen Gestehungspreis.<br>
Ob der Artikel nun einen Pauschalbetrag beinhaltet oder als Mengeneinheit € (Euro) hat hängt von der Unterschiedlichkeit der Transporte / Transportkosten ab.<br>
Diesen Artikel hinterlegen Sie bei den entsprechenden Stücklisten.<br>
Damit haben Sie diese Kosten in der Gesamtkalkulation bereits mit enthalten. Es ist allen Beteiligten klar, dass dies nur Annahmen / Schätzungen sein können.<br>
Diese Vorgehensweise bewirkt, dass die Transportkostenartikel in die Lose als jeweilige Menge übernommen werden, auf den Losen aber **keine** Kosten verursachen.<br>

## Nachkalkulation
Wenn nun die Ware per Spedition, also einem externen Dienstleister an den Kunden gesandt (gerne global einfach versandt) wird, so erhalten Sie im Endeffekt auch eine Eingangsrechnung dafür. Diese Speditionsrechnungen sind meist Monatsrechnungen und beinhalten dementsprechend viele verschiedene Lieferungen. Das bedeutet nun dass Sie bei der Erfassung der Eingangsrechnungen diese den jeweiligen [Kundenaufträgen]( {{<relref "/einkauf/eingangsrechnung/#auftragszuordnung-wozu" >}} ) zuordnen. Damit landen die Kosten auf dem jeweiligen Kundenauftrag und Ihre Nachkalkulation stimmt.

### Alternative Betrachtung zur Nachkalkulation
Wenn es darum geht die Kosten für den Transport sehr genau auf die Kosten jedes einzelnen Produktes zu erfassen, hilft der Gedankengang, dass der Spediteur ein externer Dienstleister ist, ähnlich dem Veredelungsprozess in der Metallverarbeitung oder der Lohnbestückung.<br>
D.h. für die Vorkalkulation in der Stückliste gilt das oben beschriebene, mit der Ergänzung dass der Transportartikel als externer Arbeitsgang (Artikel, Reiter Bestelldaten) gekennzeichnet wird und dass der Transportartikel lagerbewirtschaftet ist, um den Wertefluss über das Lager in das Los abbilden zu können. [Details siehe bitte]( {{<relref "/fertigung/losverwaltung/fremdfertigung/#externer-arbeitsgang" >}} )

## Versand bestellen
Werden individuelle Produkte geliefert, die nicht von den üblichen Paketdiensten bedient werden, so muss in der Regel eine Spedition beauftragt werden um die Ware abzuholen und an die gewünschte Adresse zu bringen. Da die Spedition natürlich vorher wissen muss, wie groß denn die Lieferung ist, ist es üblich hier eine Bestellung an die Spedition zu senden.<br>
Da auch bei den Speditionen ein Sammelversand günstiger ist, als einzelne Lieferungen wird es durchaus üblich sein, dass mehrere Lieferscheine mit einer Speditionslieferung versandt werden.
Um den Transport zu bestellen benötigen Sie die Anzahl der Palettenstellplätze und wohin es den gehen sollte.<br>
Die Preise der Transporte sind in der Regel nach Zonen / Postleitzahlen gegliedert und nach Mengen gestaffelt.
D.h. Sie benötigen die Anzahl der Paletten und das Transportgewicht oft auch Bruttogewicht genannt.<br>

Wie kommen Sie nun zu diesen Daten?
1. Erstellen Sie den Lieferschein an den Kunden. Bei den Artikeln des Lieferscheines hinterlegen Sie das Stückgewicht = Nettogewicht des Artikels und die Verpackungseinheit. Damit erhalten Sie
    - wie schwer ist die Position also das Nettogewicht der Position
    - wieviele Verpackungen z.B. Paletten werden benötigt
    - natürlich kennen Sie auch die Versandzone,<br>
  in der Regel die Postleitzahl, der Ziel- = Lieferadresse (Anmerkung: eventuelle Tour-Informationen können ebenfalls beim Kunden hinterlegt werden)
  
    Somit haben Sie alle Informationen um den Versand bei Ihrer Spedition zu bestellen.<br>
    D.h. Sie erstellen:
2. eine Bestellung an die Spedition
    - Hinterlegen die Lieferadresse
    - In die Bestellpositionen tragen Sie die Zonenartikel mit den entsprechenden Mengen (Paletten) ein
    - Sie hinterlegen einen Gewichtsartikel mit dem das Nettogewicht der Sendung als Menge angegeben wird

    Somit haben Sie die Kosten der Sendung, die Termine und das Transportgewicht, oft auch Bruttogewicht genannt, für diese Sendung zusammengestellt.

**Hinweis1:** Zusatzinformationen wie Annahmezeiten und ähnliches können über die Lieferanten-/Kundenkommentare abgebildet werden. Bei Bedarf bitten Sie Ihren **Kieselstein ERP** Betreuer um Ergänzung der Formulare.<br>
**Hinweis2:** Der Zonenartikel ist je Versandzone (Postleitzahlenbereich) ein Artikel für diese Art der Verpackung (Palette). Die Anzahl der Verpackungen und die damit verbundenen Vergünstigungen hinterlegen Sie in den Einkaufsmengenstaffeln. Der Zonenartikel hat das Gewicht der Verpackung, was der Tara Ihrer Lieferung entspricht<br>
**Hinweis3:** Der Gewichtsartikel hat keine Kosten und ein Gewicht von 1kg. Somit geben Sie bei der Menge nur das auf dem Lieferschein angedruckte Nettogewicht an.<br>
**Hinweis4:** Bitte Sie Ihren **Kieselstein ERP** Betreuer das Gewicht auf der Bestellung mit anzudrucken.

#### Wie mit Übergrößen / Sondermaßen umgehen?
Üblich ist, dass Sondermaße auf Palettenstellplätze umgerechnet werden. D.h. wenn dies eine Verpackung eine Übergröße hat, so belegt sie z.B. zwei Palettenstellplätze. Daraus ergibt sich, dass eben zwei Palettenstellplätze bestellt werden müssen. Es empfiehlt sich dieses Sondermaß bereits auf der Bestellung mit anzuführen, damit der Übernehmer der Lieferung auch weiß, dass es eigentlich nur eine Verpackungseinheit ist.

## Leergutverwaltung
Nachdem natürlich auch die Hilfsmittel für den Versand entsprechende Werte darstellen, muss das Leergut mitverwaltet werden.<br>
Hier ist die Aufgabe, dass beim Versand oft bekannt ist, auf wievielen Paletten die Ware verpackt ist, aber wieviele Paletten Sie vom Fahrer des abholenden Kundenunternehmens bekommen, oder wieviele Sie bei der Anlieferung durch die Spedition bekommen, erst dann bekannt ist, wenn die Paletten auf Ihrem Firmengelände sind. D.h. diese Zusatzmengen werden immer handschriftlich notiert und auf die Lieferdokumente dazugeschrieben.<br>
Um nun rasch und einfach eine entsprechende Übersicht zu bekommen hat sich folgende Vorgehensweise bewährt:
- Legen Sie entsprechende Verpackungsmittelartikel an, z.B. Europaletten. Ev. getrennt in neuwertig und gebraucht
- Erfassen Sie die ausgehenden Verpackungsmittelartikel auf Ihren Lieferscheinen
- Wenn Sie Ware einer Bestellung bekommen, so fügen Sie beim Wareneingang die erhaltenen Verpackungsmittelartikel hinzu
- Bekommen Sie auf anderen Wegen Verpackungsmittel, so buchen Sie diese Änderungen über die Handlagerbewegung auf den Verpackungsmittelartikel

Somit finden Sie in der Artikelstatistik eine Zusammenfassung aller Bewegungen des jeweiligen Verpackungsmittels. Wenn die oben genannten Buchungen einem Kunden zugeordnet sind, so haben Sie zusätzlich auch noch die Gruppierung auf die jeweiligen Kunden. Nutzen Sie dafür auch die Kunden-Lieferstatistik gefiltert nach Artikel bzw. Artikelgruppen.<br>
**Info:** Natürlich können Sie diese manuellen Buchungen über die mobile Erfassung Online durchführen, was die Buchung an die Stelle verlagert an der auch die tatsächliche Warenbewegung geschieht.<br>
**Tipp:** Um auch bei Retourlieferungen vom Kunden eine eindeutige Zuordnung zum Kunden zu erreichen, schreiben Sie einen Lieferschein mit negativer Menge für die Verpackungsmittel. Auch das Erstellen dieses Retourlieferscheines kann auch über die mobile Erfassung durchgeführt werden.

## Eigenfahrten
Zu den Eigenfahrten gibt es folgende Ansätze:
- Diese werden, wie oben beschrieben zwar in der Vorkalkulation mitgerechnet, aber aufgrund der vielen Aufträge die von Ihrem Mitarbeiter mit seinem Firmenfahrzeug quasi gleichzeitig bedient werden, nicht einzeln erfasst. D.h. der Fahrer des Firmenfahrzeuges bucht seine Zeiten auf ein zentrales Transportzeiten-Los, in dem alle Transportzeiten für einen Zeitraum (ein Jahr) erfasst werden. Sie stellen dann die geplanten Stunden aus der Stücklistenvorkalkulation den Zeiten aus dem Transportzeiten-Los gegenüber. Damit haben Sie eine qualifizierte Aussage darüber ob Ihre Schätzungen insgesamt richtig sind.
- Es werden die Transportzeiten direkt auf die jeweiligen Aufträge oder auf die Lose gebucht, somit haben Sie die Istzeiten auf den Losen/Aufträgen.<br>

Bitte beachte, dass wenn z.B. Transportzeiten auf Lose gebucht werden, diese Zeiten natürlich die Herstellkosten des Produktes verfälschen. Daher empfiehlt sich, diese Auftragskosten "nur" auf den Auftrag zu buchen. Eine Möglichkeit ist, diese Zeitbuchungen über Lieferschein oder die Zeiterfassungs-App durchzuführen.<br>
Es muss ja der Fahrer wissen wo er was hinbringt. D.h. er hat sowieso die Lieferscheine mit. Wenn er nun zum Kunden fährt so kann er seine Auftragszeit durch abscannen des entsprechenden Tätigkeitscodes vom Lieferschein sehr einfach buchen.<br>
Das funktioniert wenn einzelne Kundenfahrten gemacht werden. Bei Sammelfahrten ist obige Vorgehensweise zu empfehlen, wobei natürlich auch entsprechende Mischungen durchaus sinnvoll sein können.

## Erfassung von Reise- und Fahrzeugkosten auf die jeweiligen Aufträge
Über die Reisediäten / Reisezeiten können je Auftrag auch noch die Reiseaufwandsentschädigungen an die Mitarbeiter und ev. Fahrzeugkosten mit erfasst werden. Dies setzt voraus, dass der Fahrer seine Reisediäten bucht. Hier ist sicherlich zu überlegen, den Kostenfaktor für die Tätigkeit des Transportes ev. inkl. dieser Kosten im ev. nur kalkulatorischen Verkaufspreis zu berücksichtigen.