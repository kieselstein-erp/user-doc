---
categories: ["Forecast"]
tags: ["Forecast"]
title: "Forecast Importe"
linkTitle: "Importe"
date: 2023-01-31
weight: 10
description: >
 Forecast importieren
---
EDI Importe
===========

Mit dem Modul Forecast stehen auch verschiedene Möglichkeiten der EDI Importe, welche meist sehr kundenspezifisch sind, zur Verfügung. Kundenspezifisch bedeutet, dass jeder der großen Abnehmer sein eigenes Verständnis von EDI (Electronic Data Interchange) hat und natrülich auch von den verschiedensten vorhandenen Formaten jeder seine eigene Unterversion verwendet.
Darum kann man zwar grundsätzlich davon sprechen, dass verschiedene EDI Import-Formate zur Verfügung stehen. Ob diese 1:1 für die jeweilige Anforderung passen / geeinget sind, ist in jedem Falle zu prüfen. Insbesondere das weit verbreitete EDI Fact Format hat hier so seine zig tausend Varianten und Subformen.

Wie Sie aus den nachfolgenden Beschreibungen und Definitionen unschwer erkennen werden, wird alles unter EDI verstanden. Von einer flachen ASCII Datei bis hin zu komplexen Auftrags Beschaffungs Strukturen.

Grundsätzlich unterscheiden wir zwischen zwei Arten von Import:

EDIFACT DELFOR Forecast Import

Um den EDIFACT DELFOR Import zu starten, legen Sie bitte die Kundenadresse an bzw. wählen Sie diese aus, wechseln Sie dann bitte in den Reiter Lieferadressen.

Nun wählen Sie im Menü
![](EDI_Import1.jpg) Import, Edifact Forecast.
Im nun erscheinenden Dialog geben Sie bitte alle zu importierenden Dateien an.
![](EDI_Import2.jpg)
Mit Klick auf öffnen werden die angegebenen Dateien in den Import übernommen.

![](EDI_Import3.jpg)

Der Import selbst ist in zwei Bereiche gegliedert. D.h. wählen Sie nun bitte Prüfen. Hier wird versucht festzustellen ob alle erforderlichen Daten gegeben sind.
Üblicher Fehler sind
Lieferadresse über die Lieferantennummer, aus Kunden, Konditionen, ist nicht definiert.
Artikel sind nicht definiert.
Bitte beachten Sie beim Import der Artikel, dass die Definition so lautet, dass aus den von Ihren Kunden angegebene Artikelnummer die Leerzeichen entfernt werden und dieser String dann in der Artikelkurzbezeichnung (die in diesen Anwendungen dann oft KundenTeileNummer genandt wird) gesucht wird.
Nach dem Prüfen erscheint nun entweder eine Liste der Fehler oder
![](EDI_Import4.gif)
es wurden keine Fehler gefunden, wodurch der Importknopf nun freigeschaltet wird.
Mit Klick auf diesen wird der tatsächliche Import durchgeführt.

Hinweis: Jeder Import erzeugt je Lieferadresse einen Forecastauftrag, der im Status angelegt erzeugt wird. Das bedeutet auch, dass Importe nur dann möglich sind, wenn kein Forecastauftrag im Status angelegt je Lieferadresse vorhanden ist.
Hier kommt beim EDIFACT Import dazu, dass in der Importdatei druchaus verschiedene Lieferadressen enthalten sein können. D.h. obige Bedingung muss für alle Lieferadressen erfüllt sein, sonst wird der Import bei der Prüfung bereits abgebrochen.

Hinweis: Importierte Dateien werden in das Unterverzeichnis Old verschoben.

#### Was bedeutet Lieferkunde wurde mit der Lieferantennummer nicht gefunden
Wird z.B. nachstehender Fehler angezeigt, so bedeutet dies, dass die unter Wert angezeigte Lieferantennummer in Ihrem Kundenstamm nicht gefunden wird.
![](EDI_Import6.jpg)

Damit kann keine Zuordnung zur erforderlichen Lieferadresse hergestellt werden.
Abhilfe: Bitte tragen Sie bei der Lieferadresse des Kunden im Reiter Konditionen die Lieferantennummer, also den unter Wert angezeigten Text ein und starten Sie danach den Import erneut.

Import der Dateien der Firma Elektrolux

Bei diesem Import, welcher durch den Klick auf ![](EDI_Import5.gif) im Reiter Lieferadressen gestartet wird, werden verschiedene XLS und TXT Dateien in einem Durchgang importiert.
Da in diesen Dateien KEINE Zuordnungen der Lieferadressen enthalten sind, ist es erforderlich, dass diese Zuordnung von Ihnen im Reiter Lieferadressen unter Importpfad vorgenommen wird.
Bitte geben Sie hier nur den Pfad auf die Dateien an (bitte keine Dateinamen angeben). Beachten Sie bitte auch, dass wir alle XLS und TXT Dateien aus diesem Verzeichnis in folgender Reihenfolge importieren.
1. XLS Dateien
2. TXT Dateien
In den XLS Dateien sind gedanklich nur je eine Art, also Forecast (montly) Wochen-CallOffs (CallOffWeekly) und Tages-CallOffs (CallOffDaily) enthalten. Da nicht immer alle Dateien vom Kunden übermittelt werden, werden nicht erhaltene Inhalte von einem zum anderen Forecastauftrag weiterkopiert.
Da der Datenaufbau der Importdateien nicht gleich ist, weder vom Spaltenaufbau noch von der Terminschreibung, entnehmen wir aus der Anordnung der Spalte die Information welche Datei dies ist. D.h. sollte von Ihrem Kunden der Datenaufbau verändert werden, ist dies in den **Kieselstein ERP** Importen zu berücksichtigen.
Wir gehen derzeit von folgenden Vorgaben aus:
Spaltennummern CallOff-Daily:
1 - Bestellnummer
2 - Artikelnummer
9 - Delivery Date (Gültige Werte für Datum sind: xls-Datumsfeld oder wenn es nur Text ist, so wird nur das Format "dd.MM.yyyy" bzw. "w/yyyy" akzeptiert
10 - Menge
Nutzdaten beginnen ab Zeile 12

Spaltennummern CallOff-Weekly:
1 - Bestellnummer
2 - Artikelnummer
8 - Delivery Date (Gültige Werte für Datum sind: xls-Datumsfeld oder wenn es nur Text ist, so wird nur das Format "dd.MM.yyyy" bzw. "w/yyyy" akzeptiert
9 - Menge
Nutzdaten beginnen ab Zeile 12

Spaltennummer Forecast
1 - Artikelnummer
4 - 13 Mengenspalten der Monate
Aus der Zeile 10 wird entnommen für welches Monat der jeweilige Import ist. Hier ist das Format mm/yyyy zwingend erforderlich.

Linienabruf-Import
Hier wird eine besondere Textdatei txt verarbeitet. Zusätzlich wird beim Import der Liefertermin abgefragt, welcher default mit heute plus zwei Tage vorbesetzt ist.
Vom Gedankengang muss es für jeden Eintrag des Linienabrufes auch eine Entsprechung in den DailyCallOffs geben. Wenn dies nicht der Fall sein sollte, so wird automatisch eine Forecastposition mit dem Artikel und dem Liefertermin angelegt. Die DailyCallOff-Menge wird auf 0 gesetzt und die Linienabrufzeilen verweisen auf diese Position.
Bitte beachten Sie auch, dass auch mehrfache gleiche Definitionen in den Linienabrufen zulässig sind.

Mögliche Fehler Linienabruf:
- Zuwenig Zeilen, also weniger als 2
- Zeilenlänge zu klein, wenn nicht alle Felder gelesen werden konnten
- Menge nicht numerisch
- Artikel nicht gefunden
- mehr als ein Artikel gefunden
- ungültiges Datum

Mögliche Fehler CallOff:
- Artikel wurde mit der Artikelnummer aus XLS in unseren cKBez nicht gefunden
- es wurden mehrere Artikel gefunden
- Bestellnummernfeld leer (Warnung)
- Datumsfeld ist leer
- Mengenfeld ist leer
- ungültige Zahl im Mengenfeld
- ungültiges Datum
- Datei nicht lesbar

ABLAUF vom Import zur Freigabe des Forecastauftrages

Der wesentliche Punkt eines Forecastauftrages ist unserer Meinung nach, ob Sie die erhaltenen Daten denn auch tatsächlich so akzeptieren wollen / können.
Das beginnt bei neuen Artikeln die eingespielt werden (über die noch nie gesprochen wurde) und endet bei völlig unrealistischen Lieferterminen.
Wir haben dafür zwei Prüf-Drucke geschaffen.
1. die Delta-Liste
Mit der Deltaliste vergleichen Sie den zuletzt freigegebenen Forecastauftrag mit dem noch nicht freigegebenen (also angelegten). Hier werden neu hinzugekommene aber auch noch nicht abgerufene lagernde Artikel angezeigt. Prüfen Sie die als unstimmig angezeigten Artikeln kritisch.
2. der Druck des Forecastauftrages
In der Auswertung nach Auftrag und Stkl-Struktur sehen Sie alle Artikel des Auftrags und die gesamte dazugehörende Stücklistenauflösung. Prüfen Sie auch diese akribisch.

#### Nach dem Einlesen der EDI Dateien sind doppelte Einträge
Bitte lesen Sie immer nur zusammengehörende Dateien ein. Beim Import gehen wir immer davon aus, dass die eingelesenen Daten für den durch den Import angelegten neuen Forecastauftrag sind. Alle erhaltenen Daten, Inhalte aller eingelesenen Dateien, werden in den einen Forecastauftrag eingetragen. Sind nun mehrere sich gegenseitig ergänzende EDI-Forecastaufträge in einen **Kieselstein ERP** Forecastauftrag zusammengefasst worden, so ist die Wahrscheinlichkeit sehr hoch, dass mehrere Einträge für den gleichen Artikel und gegebenenfalls den gleichen Termin enthalten sind.
Hinweis: Wenn einzelne Forecastaufträge (noch) nicht eingelesen wurden, so ist es auch nicht erforderlich diese nachträglich einzulesen. Es ist immer im aktuellen Forecastauftrag die Gesamtheit der von Ihrem Kunden bestellten bzw. angekündigten Artikel enthalten.
