---
categories: ["Forecast"]
tags: ["Forecast"]
title: "Forecast Exporte"
linkTitle: "Exporte"
date: 2023-01-31
weight: 20
description: >
 Forecast exportieren
---
EDI Exporte
===========

Für den Export der Lieferschein und Rechnungsdaten auch stehen EDI Export Formate, welche meist sehr kunden- bzw. Anwenderspezifisch sind, zur Verfügung. Kundenspezifisch bedeutet, dass jeder der großen Abnehmer sein eigenes Verständnis von EDI (Electronic Data Interchange) hat und natürlich auch von den verschiedensten vorhandenen Formaten jeder seine eigene Unterversion verwendet.
Darum kann man zwar grundsätzlich davon sprechen, dass verschiedene EDI Export-Formate zur Verfügung stehen. Ob diese 1:1 für die jeweilige Anforderung passen / geeignet sind, ist in jedem Falle zu prüfen. Insbesondere das weit verbreitete EDI Fact Format hat hier so seine zig tausend Varianten und Subformen.

Einrichten der Versandwege

Bitte Sie bitte Ihren **Kieselstein ERP** Betreuer, dass er für Sie für die jeweiligen Kunden die passenden Versandwege einrichtet. Die Möglichkeit die Versandwege selbst einrichten zu können ist für die Zukunft vorgesehen.

**ACHTUNG:** Der derzeitige EDI Export erfolgt in Kombination mit dem Zusatzprogramm EDI4ALL, welches ein reines Windowsprogramm ist.
Das bedeutet, dass nach dem Export, welcher vom **Kieselstein ERP** Server erzeugt wird, vom Anwender noch die eigentliche Konvertierung in das Kundenformat vorgenommen werden muss. Um dies möglichst einfach zu gestalten, wird auch der Aufrufbatch für EDI4ALL von **Kieselstein ERP** generiert. D.h. nach dem Export wechseln Sie bitte in das entsprechende Verzeichnis und rufen den Batch auf. Der Namensaufbau des Batch ist HV_MODUL_BELEGNUMMER.bat. Z.B.: hv_ls_16_100042.bat was bedeutet es wird der Lieferschein 16/100042 konvertiert.
Bitte achten Sie darauf, dass die **weitere Behandlung** von Ihnen **angestoßen werden muss**.

Lieferaviso

Der Ablauf bei der Erstellung eines Lieferavisos ist wie folgt:

-   Es wird der Lieferschein wie üblich erstellt und ausgedruckt

-   In den Kopfdaten des Lieferscheines finden Sie Button ![](EDI_Export1.gif) Lieferaviso übermitteln
    Recht für den Versandknopf = LS_LIEFERSCHEIN_VERSAND

-   Damit wird der in den Versand-Definitionen definierte Export für den jeweiligen Partner der Lieferadresse angestoßen und die Daten versandt / in den Dateistrukturen abgelegt.

-   Bitte beachten Sie, dass der Versand des Lieferscheines / des Lieferavisos NUR möglich ist, wenn sich
    1.) der Lieferschein im Status geliefert und
    2.) der Lieferschein tatsächlich ausgedruckt (oder gespeichert oder per EMail versandt) wurde

#### Ist ein erneuter Versand des Lieferavisions möglich ?
Ja. Wurde das Lieferaviso eines Lieferscheines bereits einmal erzeugt, so kann es, solange der Lieferschein im Status geliefert ist, erneut versandt werden. Es erscheint beim erneuten Versand ein Hinweis, welcher mit Ja zu beantworten ist.

#### Ist ersichtlich ob ein Lieferschein schon versandt wurde?
ja, es ändert sich die Hintergrundfarbe des Lieferavisobuttons auf Grün ![](EDI_Export2.gif)

Rechnungsversand

Auch für den Rechnungsversand per EDI sind einige wenige Parametrierungen passend zu den Versanddaten erforderlich.
Der Ablauf für den Rechnungsversand ist ähnlich der Erstellung des Lieferavisios. D.h.:

-   Es wird die Rechnung wie üblich erstellt und ausgedruckt

-   In den Kopfdaten der Rechnung finden Sie Button ![](EDI_Export1.gif) Rechnung übermitteln
    Recht für den Versandknopf = RECH_RECHNUNG_VERSAND

-   Damit wird der in den Versand-Definitionen definierte Export für den jeweiligen Partner der Rechnungsadresse angestoßen und die Daten versandt / in den Dateistrukturen abgelegt.

-   Bitte beachten Sie, dass der Versand der Rechnung NUR möglich ist, wenn sich
    1.) die Rechnung im Status offen und
    2.) die Rechnung tatsächlich ausgedruckt (oder gespeichert oder per EMail versandt) wurde

Info: Ein erneuter Versand der Rechnung bzw. die Anzeige ob der Versand schon durchgeführt wurde, ist analog zum Lieferschein.

#### Fehlermeldung: Das Verzeichnis existiert nicht bzw. kann nicht beschrieben werden.
In den Versandwegdefinitionen sind die jeweiligen Pfade hinterlegt unter denen die von **Kieselstein ERP** erzeugten Dateien erstellt werden sollten. Sind diese für Ihren **Kieselstein ERP** Server nicht vorhanden bzw. wenn Ihr **Kieselstein ERP** Server nicht auf diese Pfade schreiben darf, so erscheint diese Fehlermeldung. Bitte stellen Sie sicher, dass beide Voraussetzungen erfüllt sind. Gegebenenfalls bitten Sie Ihren **Kieselstein ERP** Betreuer um entsprechende Änderung der Parametrierung.