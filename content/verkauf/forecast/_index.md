---
categories: ["Forecast"]
tags: ["Forecast"]
title: "Forecast"
linkTitle: "Forecast"
date: 2023-01-31
weight: 50
description: >
 der große und der kleine Forecast
---

Erfassung der Forecastaufträge mit Historie
## großer Forecast mit EDI Import
## kleiner Forecast mit proprietären Importformaten

## Allgemeines

Forecast
========

Mit dem Modul Forecast-Planung steht ein praktisches Werkzeug zur Informationsweitergabe der meist unverbindlichen Forecastdaten deiner Kunden / deines Verkaufs zur Verfügung.
Der Forecast dient in aller Regel dazu, die Preise zu senken und gleichzeitig die Lieferzeiten zu reduzieren. Aufgrund seiner faktischen Unverbindlichkeit bedeutet dies umgekehrt, dass du meist ins Risiko gehen (musst) um die Wünsche des Kunden erfüllen zu können. Einige **Kieselstein ERP** Anwender helfen sich hier auch damit, dass ausgewählte teure Komponenten vom Kunden zusätzlich schriftlich, ohne Rückzugsmöglichkeit bestellt werden müssen.

Wir haben den Forecast in zwei Bereiche gegliedert:
### der klassische Forecast
Erfassen hier die Verkaufs-/Artikel insbesondere Stücklisten in einem Monatsraster für einen Zeitraum von 6-24Monaten bezogen auf Kunden und die jeweilige Lieferadresse.
Zur Übersichtlichkeit sollte je Kunde und Lieferadresse ein Forecastauftrag angelegt werden.
Von der Idee her gibt es je Forecastauftrag immer nur einen gültigen / aktiven Auftrag. Die Vorgänger-Forecast-Aufträge werden in den Status erledigt gesetzt und dienen der Beurteilung  der Planungssicherheit deines Kunden / Verkaufs.
Ein Forecast wird oft mit vielen umfassenden Verträgen unterlegt, in der Praxis stellt es sich jedoch so dar, dass du deinem Kunden, wenn er den Forecast nicht erfüllen konnte, trotzdem die Ware nicht auf den Hof stellen kannst, da du sonst deinen Kunden das letzte Mal beliefert hast. D.h. die anhand von Forecasts errechneten Daten werden in aller Regel an die Lieferanten weitergegeben, aber unter den gleichen völlig unverbindlichen Bedingungen.

### die sogenannten CallOffs
Unter CallOff werden die genaueren Einteilungen der Forecasts, in der Regel auf Wochen und Tages Basis vorgenommen. Da es hier schon sehr konkret um Liefertermine geht, sind diese Abrufe dann faktisch verbindlich.

### praktische Umsetzung
Da die Definition ob dies nun ein klassischer Forecast oder ein Daily-CallOff oder ein Weekly-CallOff ist, rein aus den Terminen gesteuert wird, werden alle Forecastdaten jeweils in einen Forecastauftrag importiert. D.h. es wird dies rein über die zeitliche Steuerung vorgenommen.
Um feststellen zu können was sich von einem zum anderen Forecastauftrag, den du von deinem  Kunden erhalten hast, geändert hat, nutzen wir die Deltaliste.<br>
Wir haben die CallOffs so umgesetzt, dass wenn ein Tages-CallOff einen Wochen-CallOff überlagert, so zählt NUR der Tages-CallOff. Überlagert der Wochen-CallOff den Forecast-Auftrag, so zählt der Wochen-CallOff.<br>
In den CallOffs sind auch Prüffunktionen enthalten die eine Abweichung zum übergeordneten Auftrag (Wochen-CallOff, bzw. Forecast-Auftrag) aufzeigen, damit du deine Kunden / Verkauf auf eventuelle Unstimmigkeiten hinweisen kannst.

#### Starten
Das Modul Forecast wird durch Klick auf ![](FC_starten.jpg) gestartet.
Wie üblich wird mit Neu ein neuer Forecast-Auftrag angelegt.
Vergeben Sie eine entsprechende eindeutige Kennung für den Forecastauftrag und definieren Sie Kunde und Lieferadresse. Der Forecastauftrag ist dafür gedacht, die laufenden Aktualisierungen, welche in der Regel monatlich erfolgen, Ihres Kunden zu erfassen, einzupflegen.

Der generelle Ablauf ist:
1. anlegen eines Kunden und Vergabe einer Forecastnummer / Kennung
Wichtig: CallOffTag, -Woche reserviert, Forecast dient nur der Information (und reserviert nicht)
Werden also die nächsten drei Wochen als verbindlich genommen, so tragen Sie hier bitte einfach drei Wochen ein.
Alleine durch das Verstreichen der Zeit wandelt sich ein Forecastauftrag in einen CallOff, der dann auch im Bestellvorschlag wirkt.
2. anlegen der Lieferadressen zum jeweiligen Kunden (Reiter 3)
3. Anlegen des eigentlichen Forecastauftrags (Reiter 4) zur jeweiligen Lieferadresse mit Angabe von Bemerkung (z.B. Kundenbestellnummer) und eines eventuellen Langtextes
4. Reiter 5, Positionen. D.h. eingeben der Forecastpositionen bzw. mit Klick auf Import die / eine Exceldatei einlesen.
Je nach der CallOff-Definition werden die Termine als CallOff oder Forecast (oder nicht definiert, was nach dem Forecast ist) interpretiert.
5. Bitte prüfen Sie die importierten Daten. Nutzen Sie dafür auch die Deltaliste aus dem Reiter 4
6. Ist der Forecast in Ordnung, so muss dieser (im Reiter 4) freigegeben werden. Damit greift die Reservierung nun endgültig.

#### Importieren
Die Logik beim Import ist:
Ist die Artikelnummer angegeben, so wird diese verwendet
ist die Artikelkurzbezeichnung angegeben, so wird diese verwendet, wenn die Artikelnummer NICHT angegeben ist.
dann gäbe es noch die echten Kundenartikelnummern aus den SoKos
D.h. wenn in den Artikel die Kurzbezeichnung angegeben ist und die Artikelnummer in der Excel Datei nicht angegeben ist, so wird diese als Suchbegriff / Übersetzung verwendet.
Somit müssen Sie in der Datei nur die beiden ersten Zeilen austauschen und diese als XLS !!! abspeichern. Und schon geht der Import.
Bitte auch beachten. Es kann nur ein angelegter Forecastauftrag bearbeitet werden. Ein freigegebener darf nicht mehr bearbeitet werden. Es kann die Freigabe aber solange zurückgenommen werden, solange es keinen weiteren Forecast im Status angelegt gibt.

### Welche Forecastarten gibt es
- Forecast ... ist immer eine monatliche Betrachtung. Hat ein Start-Monat und eine Dauer von 6 - 24 Monate. Er wird immer so gerechnet, dass der Bedarf am 1\. des Monates entsteht.
- CallOffWoche ... ist eine Wochenweise Betrachtung. Hat einen Starttermin welcher manuell immer der Montag sein kann und eine Dauer bis zu 10Wochen
- CallOffTag ... ist eine Tageweise Betrachtung. Hat einen Starttermin in der Regel am Montag und eine Dauer bis zu 4Wochen = 28Tage

Info: Sollte der Forecast generell nur als Info genutzt werden, so tragen Sie bitte bei CallOffTag und CallOffWoche -999 ein. Damit wird die automatische Reservierung deaktiviert. Bei Forecastauftrag tragen Sie 999 ein.
![](Forecast_nur_Planung.gif)
[Details dazu siehe](#Planungszwecke).

#### Gesamtbetrachtungen
Ein einzelner Forecastauftrag kann mit Forecast, CallOffWoche und CallOffTag gesamt betrachtet werden. Hier wird davon ausgegangen dass:
1. immer nur je einer Forcastauftragsart ein Forcastauftrag angelegt (aktiv) sein kann
2. Der höherwertige den niederwertigen überlagert
Wird die Betrachtung über mehrere Forecastaufträge angestoßen, so werden nur die jeweils aktiven Forecastaufträge der Art Forecastauftrag berücksichtigt. Hintergrund: Die CallOff-Aufträge sind bereits in den Bestellvorschlägen berücksichtigt. D.h. ausgehend von einer ordentlichen (täglichen) Verwendung haben Ihre Lieferanten bzw. Ihre Fertigung bereits die entsprechenden Informationen.
Es wird immer

#### Festlegungen
1. die Auflösung von Forecastaufträgen geschieht immer über alle Mandanten (bei zentralem Artikelstamm)
2. Werden Forecastaufträge nach Lieferanten und damit nach Artikel verdichtet, wird immer der über alle Mandanten erst gereihte Lieferant verwendet
3. Der angeführte Einkaufspreis ist
    1. der Einkaufspreis zum 1\. des ersten angedruckten Monates
    2. immer der Einkaufspreis des über alle Mandanten erst gereihten Lieferanten (so wie auch in der Stücklisten Gesamtkalkulation)
4. Der Forecast ist immer auch eine Information an die Lieferanten der Holding und gliedert sich nicht in die unterschiedlichen Bedarfe der verschiedenen Mandanten

#### Die Reiter im Modul Forecast
| Reiter | Funktion |
| --- |  --- |
| Auswahl | die Liste der angelegten Forecasts mit Forecast-Kennung, Kunde, Lieferadresse, Projekt-Text und Status |
| Kopfdaten | definieren Sie hier die Daten des Forecastauftrags. Bitte beachten Sie, dass diese Daten jederzeit verändert werden können. Es darf pro Kunde und Lieferadresse nur jeweils einen Forecast-Kopf-Auftrag geben |
| Lieferadressen |   |
| Aufträge | hier finden Sie alle Forecasts zum angegebenen Forecast-Kopf-Auftrag. Es kann pro Forecast-Art nur jeweils **ein** angelegter (= aktiver) Forecastauftrag definiert sein. Bitte beachten Sie bei den Forecastarten untereinander, dass diese auch eine terminliche Reihenfolge haben müssen. Aus Gründen der geforderten Flexibilität haben wir diese derzeit nicht gegeneinander verriegelt. Auch können die Termine eines jeden Forecasts können, solange dieser im Status angelegt ist jederzeit verschoben werden. Hier wird auch der Beginn, Dauer also der Zeitraum für die jeweilige Forecastart definiert und es kann für jeden Forecast auch eine Bemerkung angegeben werden |
| Positionen | Abhängig von der Forecastart und dem definierten Zeitraum werden hier die Artikel und deren jeweiligen Mengen zum entsprechenden Zeitpunkt eingetragen. In der Regel werden die Artikel eigengefertigte Stücklisten sein. Selbstverständlich können aber auch jegliche Art von Artikel hier definiert werden. Sind diese keine eigen gefertigten Stücklisten so werden sie eben als Handelswaren betrachtet |
| Linienabrufe |   |

Anmerkung:<br>
Wenn zu einem Forecast-Auftrag bereits Positionen definiert sind, können die Daten (Termine) dieses Forecast-Auftrags nicht mehr verändert bzw. storniert werden. Um den Forecast-Auftrag verändern zu können, müssen zuerst die enthaltenen Positionen gelöscht werden. Damit sind keine Positionen mehr definiert und somit kann der Kopf des Forecast-Auftrages im Reiter Aufträge entsprechend angepasst werden.

#### Druck- und Auswertefunktionen
Im Reiter Aufträge finden Sie das entsprechende Druckersymbol. Damit kann der gewählte Forecastauftrag ausgedruckt werden. Hier werden alle eigengefertigten Stücklisten über alle Mandanten hinweg aufgelöst um so eine Gesamtsicht über die Bedarfe der gesamten Unternehmensgruppe zu erhalten. Dafür werden auch die Mandantennummern der jeweiligen Mandanten der Artikelbedarfe und den dazu definierten Lieferanten angedruckt. Bitte beachten Sie, dass hier immer der erst-gereihte Lieferant, über alle Mandanten hinweg, des jeweiligen Artikels verwendet wird.
Es stehen zwei Sortierungen zur Verfügung ![](FC_Sortierung1.gif):
-   Nach Auftrag und Stücklistenstruktur
D.h. hier werden die Artikel in der Reihenfolge wie von Ihnen im Reiter Positionen erfasst angeführt. Unter jedem Stücklisten-Artikel finden Sie die Bedarfe der Stücklistenpositionen, deren Lieferanten, Lagermindeststände usw. Aufgrund der detaillierten Auflösung werden ein und der selbe Artikel mehrfach vorkommen.
-   Nach Lieferanten und darin nach Artikel
Hier werden alle Artikel zusammengefasst und es kommt jeder Artikel nur einmal vor.<br>
Diese Darstellung ist als Basis für die Information an Ihre Lieferanten gedacht. Daher wird jeder Lieferant auch auf eine eigene Seite gedruckt.

Die Darstellung ist auf 12 Monate / Zeiträume ausgelegt. Abhängig von den tatsächlich eingestellten Zeiträumen kann die Darstellung auch mehrzeilig werden. Dies reduziert logischer Weise die Lesbarkeit. Je nach gewählter Art des Forecastauftrages werden die Zeiträume in den Überschriften entsprechend angedruckt. Bitte beachten Sie, dass bei den CallOff-Forecast-Aufträgen die erfassten Mengen der in den Positionen angeführten Artikel bereits zu den jeweiligen Terminen reserviert sind. Damit wird die Kette, interne Bestellung, Bestellvorschlag gestartet.

Im Menü finden Sie Journal, Beschaffung

Hier wird eine Zusammenfassung aller Forecast-Aufträge der Art Forecast-Auftrag (keine CallOff) angezeigt / gedruckt. Damit erhalten Sie einen Überblick über alle angelegten Forecastaufträge. Die Zeitskala wird über alle betroffenen Forecastaufträge normiert und immer gleich dargestellt. Bitte beachten Sie auch, dass Termine vor heute nicht möglich sind und alle Mengen von vor heute auf heute verdichtet werden.
- Sortierung nach Auftrag (Artikel) und Stücklistenstruktur<br>
Bei dieser Sortierung werden alle Forecastaufträge mit allen Detailpositionen ausgewiesen, ähnlich dem oben beschriebenen.
- Sortierung nach Lieferanten und Artikel inkl. Verdichtung<br>
Damit erhalten Sie eine Information für Ihre Lieferanten welche Bedarfe, über alle Ihre eingeplanten Stücklisten und Artikel in Zukunft geplant sind.

**Hinweis:** Es ist nur die Forecastart Call-Off Tag dafür vorgesehen, dass diese mittels Ausliefervorschlag ausgeliefert wird. Liegt hinter einem Call-Off Tag Eintrag eine Lieferscheinposition, so kann diese nicht mehr verändert werden. Diese Sperre greift weiterführend auf den gesamten Call-Off Tag durch.

Im Menü finden Sie Journal, Geplanter Umsatz

Hier erhalten Sie eine Zusammenfassung aller Forecast-Aufträge, unabhängig welche Forecastart (Call-Off) definiert ist.
Es werden jedoch nur freigegebene Forecastaufträge berücksichtigt.
Es ist diese Darstellung auf 12Monate ausgerichtet / begrenzt. D.h. sollten Sie eine Betrachtung mehr wie 12Monate benötigen, so lassen Sie bitte den Forecast entsprechend mehrfach laufen.
Bei der Betrachtung werden alle gleichen Artikel eines Kunden auf jeweils das resultierende Monat zusammengezogen. Für die Verkaufswertberechnung wird die klassisches Verkaufspreisfindung herangezogen, mit Stichtag des Ersten des Monates. D.h. es werden die im jeweiligen Artikel hinterlegten Verkaufspreise inkl. der Staffelverkaufspreise herangezogen. Sollten für den Kunden Sonderkonditionen definiert sein, so greifen selbstverständlich auch diese.
![](Forecast_geplanter_Umsatz.gif)
Somit erhalten Sie eine Übersicht welche Stückzahlen und welche geplante Erlöse / Umsätze Sie je Artikel, je Monat bei Erfüllung des Forecasts erreicht werden könnten.
Bitte beachten Sie, dass hier keinerlei erteilte Aufträge usw. berücksichtigt sind.

In der Artikel-Zeile ist die obere Zeile die Menge und die darunter liegende der Wert der sich aus den Einträgen im Forecast ergibt.
Der Umsatz kann auch nach Artikelgruppen sortiert werden, wodurch entsprechende Summen nach Artikelgruppen dargestellt werden.
Rechts sehen Sie die Gesamtsummen der Mengen bzw. Umsätze.
In den Artikelgruppensummen und in den Gesamtsummen, werden nur mehr die entsprechenden geplanten Umsätze aufgeführt.

#### Statusübergänge
Die Forecastaufträge haben derzeit nur drei Stati.
1. angelegt
Dies bedeutet, dass der Forecastauftragt zwar in **Kieselstein ERP** vorhanden ist, seine Positionen wirken jedoch NICHT in den Reservierungen.
2. freigegeben.
Ein angelegter Forecastauftrag wird durch Klick auf ![](FC_Freigabe.gif) freigegeben. Damit werden die enthaltenen Positionen reserviert. Zugleich wird ein eventuell bereits freigegebener Forecastauftrag automatisch in den Status erledigt gesetzt.
3. erledigt
Zusätzlich zur automatischen Erledigung einzelner Forecastaufträge können diese auch manuell erledigt werden, Menü, Bearbeiten, Manuell erledigen.

Bitte beachte, dass es je Kunde+Lieferadresse+Forecastart jeweils nur einen Forecastauftrag im Status angelegt und einen im Status freigegeben geben darf. Die alten Forecastaufträge werden vor allem zur Verfizierung der Planungssicherheit (des Kunden, bzw. des Verkaufsteams) verwendet.

#### Übersicht über alle angelegten Forecastaufträge
Unter Journal, Übersicht finden Sie eine Darstellung aller Forecastaufträge im Status angelegt.
Diese sind nach Kunde+Lieferadresse+Beginn-Datum gereiht. Achten Sie bitte darauf dass IMMER der Call-Off-Tag vor der Call-Off-Woche vor dem Forecastauftrag eingelastet sein muss. Alles andere würde der Logik des Wesens der Forecastaufträge widersprechen.

#### Wie wirken die Forecastaufträge im Lager / in der Beschaffung?
Der Call-Off Tag bewirkt dass die je Tag und Artikel angegebene Menge zu diesem Tag reserviert wird.
Der Call-Off Woche bewirkt dass ähnlich dem Call-Off Tag ausgehend vom Beginndatum je Woche und Artikel die angegebene Menge zu diesem Tag (aus der Woche (Beginn + 7Tage)) reserviert wird.

#### Wie wird ein Call-Off Tag ausgeliefert?
Um die Artikel-Tages-Positionen eines Call-Off Tag auszuliefern muss der [Ausliefervorschlag]
( {{<relref "/verkauf/lieferschein/#ausliefervorschlag---r%c3%bcckstandsaufl%c3%b6sung"  >}} )
Positionen des Call-Off Tages die bereits in einen Lieferschein eingebunden sind können nicht mehr verändert werden.
Positionen des Call-Off Woche können nicht geliefert werden.

## Zusammenhang Forecast, Lose, Lager, Lieferung
Siehe: Vom Forecast zur Lieferung

#### Wie mit Änderungen in den Call-Offs umgehen?
<a name="Deltaliste"></a>
In der Regel erhalten Sie annähernd täglich die CallOff-Aufträge mit den neuen an den jeweiligen Tagen bzw. Wochen zu liefernden Mengen. D.h. der übliche Vorgang ist, dass der bisherige CallOff freigegeben ist. Nun werden die neuen CallOffs (Tag und Woche) eingelesen und werden noch im Status nicht freigegeben belassen.<br>
Nun drucken Sie durch Klick auf ![](FC_Deltaliste.gif) die Deltaliste aus. In dieser Liste werden die kumulierten Mengen der freigegebenen Forecasts mit den kumulierten Mengen der nicht freigegebenen Forecasts gegenübergestellt. Damit erhalten Sie sofort eine Übersicht der entsprechenden Abweichungen. In den bisherigen Mengen, denen des freigegebenen Forecasts, sind die bereits ausgelieferten Mengen berücksichtigt. Somit wird auch ein eventuelle Rückstand entsprechend mit berücksichtigt.<br>
Bitte beachten Sie dass die Termine immer beim Kunden eintreffend betrachtet sind.
![](FC_Deltaliste2.jpg)<br>
Grün unter bisher steht für Mengen die zum Datum nicht benötigt werden.
Schwarz hinterlegt bei aktuell bedeutet hier war bisher zu wenig als Bedarf definiert.

Zusätzlich werden daneben nach Lagerstands und Reichweiteninformationen angedruckt.
![](FC_Reichweite.gif)<br>
Hier sind auch Artikel enthalten, die eventuell in den bisherigen bzw. aktuellen CallOffs NICHT (mehr) enthalten sind und die noch Lagerstände bzw. offene Bestellungen, Fertigungsmengen oder Rahmenbestellungen haben. Damit haben Sie auch eine Information, darüber, wenn ein Kunde eine CallOff-Position einfach auslaufen lässt.

- Reichweite Lager: ... Darstellung wie weit der Lagerstand für die Lieferung der aktuellen Menge reicht (wie lange ohne Nachfertigung geliefert werden kann)
- Reichweite mit Zulauf: ... hier sind auch noch die offenen Fertigungsmengen, offenen Bestellungen und die offenen Rahmenbestellungen berücksichtigt.

Sie erhalten damit auch eine Aussage ob Ihr Kunde alle früher in CallOffs angeführten Artikel tatsächlich abruft. In der Reichweite Zulauf ist auch die Aussage, wie lange Ihr Kunde noch abrufen muss enthalten.

Wichtig: Bei der Deltaliste wird immer vom neuesten angelegten aber nicht freigegebenen Forecastauftrag ausgegangen und mit dem nächstliegenden freigegebenen (aber noch nicht erledigten) Forecastauftrag verglichen.

#### Können Forecastaufträge auch importiert werden
Ja. Nachdem ein Forecastauftrag angelegt ist und noch nicht im Status freigegeben, können in diesen die Positionen direkt aus einer .XLS Datei importiert werden.
Klicken Sie dazu im Reiter Positionen auf das importieren Icon.

Wir unterscheiden hier folgende Importarten:
![](Import_Formate.jpg)

Bitte beachten Sie auch den [VMI Import](#vmi-import)

Für den Standard-Import sind die Spaltenüberschriften in Ihrem Spreadsheet des ersten Blattes wichtig. Es werden dabei folgende Spaltenüberschriften / Felder unterstützt.

| Spaltenüberschrift | Bedeutung |
| --- |  --- |
| Artikelnummer | Die Artikelnummer des zu importierenden Artikels in Ihrem Artikelstamm. Diese Spalte muss immer vorhanden sein. |
| ArtikelKbez | Ist die Artikelnummer nicht definiert, so wird, nach diesem Text in der Artikel Kurzbezeichnung gesucht. In sehr vielen **Kieselstein ERP** Installationen wird dies als Kundenartikelnummer verwendet |
| KDArtikelnummer | Sind beide obige Spalten ohne Inhalt, so wird nach der Kundenartikelnummer in den Kunden-Sonderkonditionen gesucht. |
| Bestellnummer | Ein ev. Bestelltext der auf die eine Forecastposition zugeordnet werden sollte. |
| Menge | Menge dieser Position zum unten angeführten Termin. Bitte achten Sie darauf, dass dies ein Zahlenfeld sein muss |
| Termin | Termin zu dem dieses Position geliefert werden muss. Bitte achten Sie darauf, dass dies ein Datumsfeld sein muss. |

- Für den Import im Format Epsilon gilt eine spezielle Definition des XLS Formates (Forecast_Epsilon_Import.xls). Bitte wenden Sie sich dazu an Ihren **Kieselstein ERP** Betreuer.
- Für den Import der Rollierenden Planung gibt es eine weitere Definition (Forecast_Rollierende_Planung.xls).
- Zusätzlich gibt es den Import von VMI-CC Daten welcher im Gegensatz zum VMI Import dafür gedacht ist, die einzelnen Bedarfe manuell den Forecastaufträgen zuordnen zu können und somit die Übersicht deutlich zu erhöhen.
- Für den Import im VAT Format gilt ebenfalls eine spezielle Definition des XLS Formates(Forecast_VAT.xls). Es werden dabei nur die Zeilen importiert, welche in der Spalte measure mit GROSS befüllt sind. Die Monatsspalten werden in der Form importiert, dass diese ab dem aktuellen Importmonat eingelesen werden. So wird z.B. am 5.6.2022 der Import erst ab der Spalte Z, welche mit dem Datum 1.6.2022 beschriftet ist durchgeführt. Die Zuordnung zum Artikel wird über die Referenznummer durchgeführt. D.h. über den Wert der Spalte partCD muss eindeutig ein Artikel anhand seiner eindeutigen Referenznummer gefunden werden. Wird keiner oder werden mehrere Zuordnungen gefunden wird der Import mit einer entsprechenden Fehlermeldung abgebrochen.

- Zusätzlich stehen verschiedene [EDI Importformate]( {{<relref "/verkauf/forecast/EDI_Importe">}} ) und auch [EDI Exportformate]( {{<relref "/verkauf/forecast/EDI_Exporte">}} ) zur Verfügung.

#### Import rollierende Planung
Mit diesem Import werden die von einigen größeren Unternehmen monatlich gelieferten zwölf Monats Forecasts importiert.

![](Rollierende_Planung_Vorlage.gif)

Nach der Auswahl des Importformates rollierende Planung muss definiert werden, ob der Artikel anhand des Feldes ArtikelNr. in der Referenznummer oder anhand des Feldes Item No., was der Lieferantenartikelnummer entspricht in der **Kieselstein ERP** Artikelnummer = Ident gesucht werden sollte. Gibt es zu bei der Suche in der Referenznummer kein oder mehrere Ergebnisse, erscheint eine entsprechende Fehlermeldung und der Import wird abgebrochen.

Da oft auch nur reine Vergangenheitsdaten geliefert werden, also in dem Datenblock die Zeile mit Demand planned fehlt, wird dieser Block beim Import ignoriert. **Wichtig**: Verschiedene Tests haben leider ergeben, dass die Zeile Demand planned für den Import wichtig ist.

Solltest du neuere Formatvorlagen bekommen, es gibt in der Zwischenzeit Konverter um auch von XLSX auf dieses Rollierende Planungsformat zu kommen.<br>
Vermutlich auch wichtig: Die Anzahl der Monate ist nicht auf 12 begrenzt.

<a name="VMI Import"></a>

#### VMI Import?
Für unsere Anwender mit CleverCure Schnittstelle ist auch immer das Thema VMI (Vendor Managed Inventory) relevant. Es bedeutet dies, dass Sie verschiedene Artikel, eben die als VMI gekennzeichneten Artikel, faktisch sofort liefern können müssen. Da natürlich auch auf diesen Artikel entsprechende Fertigungszeiten gegeben sind, benötigen Sie eine Forecastplanung Ihres Kunden, mit dem Sie die eventuell in Zukunft benötigten Stückzahlen einplanen / vorfertigen können. Dafür ist dieser Import gedacht. Bitte beachten Sie unbedingt die rechtliche Verbindlichkeit bzw. eben **nicht** Verbindlichkeit solcher Angaben von Seiten Ihres Kunden.

Die Eigenschaft des VMI Importes ist, dass er verschiedene Auftraggeber und Lieferadressen in einer Datei enthalten hat.
Daher finden Sie diesen Import in der Auswahlliste der Forecastaufträge unter Forecast, Import, Positionen.<br>
![](VMI_Import.gif)

Für die Definition der Importdatei wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer (Forecast_VMI_aus_CC.xls)
In dieser Datei müssen die 3 Spalten Kunde, Lagerort, Artikelnummer Lieferant vorhanden sein.

Um den Kunden zu ermitteln, wird in der Partner Kurzbezeichnung nach dem letzten Wort aus der Spalte "Lagerort" gesucht. Hier muss genau ein Kunde gefunden werden, ansonsten wird eine Fehlermeldung ausgegeben. Steht z.B. in der Spalte Lagerort "Eugendorf 0201", dann suchen wir in der Partner Kurzbezeichnung nach einem Eintrag mit 0201 .
Der Artikel wird anhand der Spalte "Artikelnummer Lieferant" über die Artikelnummer gesucht. Wenn kein Artikel gefunden wird, wird eine Fehlermeldung ausgegeben.
Achtung: Es muss für den Kunden (als Lieferadresse) einen Forcast im Status angelegt geben.
Ab der Spalte K erwarten wir uns eine Überschrift im Format YYYY-MM.
Sobald ein Jahr/Monat >= dem aktuellen Jahr/Monat ist, wird die Forecastmenge mit dem 1\. des Monats in den Forecastauftrag übernommen.

WICHTIG: Gibt es bereits einen offenen Forecastauftrag für einen der in diesem File enthaltenen Kunden, können die Daten nicht importiert werden, vor allem weil es zu Mischungen kommen würde. Verwenden Sie dafür bitte den oben beschriebenen VMI-Einzel-Auftrags-Import = VMI-CC.

#### Wo sehe ich die Forecasts des einzelnen Artikels?
Im Modul Artikel, Info, Forecast werden die einzelnen Forecastpositionen dargestellt.<br>
![](Forecastpositionen.jpg)<br>
Zusätzlich wird vom laufenden Monat die gesamt ausgelieferte Menge und die für den jeweiligen Zeitraum vereinbarte und in den Auftragsbestätigungen enthaltenen Mengen dargestellt.
Als weitere Info wird die in den freigegebenen Forecastpositionen eingetragenen Mengen pro Monat aufgezeigt.
Über den gesamten Lagerstand wird dann aufgezeigt, wie lange dieser Lagerstand für die Forecastpositionen reichen würde, also die Reichweite Ihres Lagerstandes. Bitte beachten Sie, dass eine direkte Summierung der Mengen nur unter sehr genau bekannten Situationen erlaubt ist. In vielen Firmen wird der Forecast nur für die sehr grobe Planung genutzt. Die rechtsverbindlichen Bestellungen durch die Kunden erfolgen dann durch die Erfassung als Auftragsbestätigung.<br>
Wie in **Kieselstein ERP** üblich bedeutet:<br>
LS ... Lieferschein, also die in diesem Monat bereits gelieferten Mengen des Artikels,<br>
AB ... Auftragsbestätigung, also die für diesen Liefertermin (der Auftrags Kopftermine) geplanten Lieferungen<br>
FC ... Forecastaufträge

#### Es können die Positionen nicht mehr verändert werden?
Wenn ein Forecastauftrag bereits freigegeben ist, so können die zugehörigen Positionen nicht mehr verändert, bzw. gelöscht werden. Um dies zu ermöglichen nehmen Sie bitte die Freigabe des Forecastauftrages zurück.

#### Wo sehe ich die zu liefernden Linienabrufe?
<a name="Linienabrufe"></a>
Im Modul Lieferschein, steht im unteren Reiter Ausliefervorschlag auch das Journal der Linienabrufe zur Verfügung.<br>
Für die Berechnung des Ausliefertermines wird die Lieferdauer an den Kunden unter Berücksichtigt des Betriebskalenders gerechnet. D.h. der Forecast-Liefertermin abzüglich der Lieferdauer wird unter Berücksichtigung der Tagesarten Sonntag, Feiertag, Halbtag um diese Tage vorverlegt.

#### Kann ich Artikel als laufend zu liefern kennzeichnen?
<a name="Kommissionieren"></a>
Im Modul Artikel, Reiter Sonstiges kann Kommissionieren angehakt werden. Dies bewirkt, dass
1. diese Artikel im Ausliefervorschlag NICHT berücksichtigt werden
2. sie scheinen anstatt dessen im Journal (Lieferschein, unterer Reiter Ausliefervorschlag, Menüpunkt Journal) Linienabrufe auf
3. sie werden im Kommissionierterminal wie eine eigene Linie behandelt.

#### können ganze Lieferadressen als laufend zu liefern gekennzeichnet werden?
Im Reiter Lieferadressen kann angehakt werden, dass die hinter dieser Lieferadresse alle Artikel kommissioniert werden sollten und somit diese Lieferadresse auf dem Kommissionierterminal angezeigt werden.

Liefermengenfortschrittszahl Importe

Werden zum gleichen Artikel zur gleichen Lieferadresse zum gleichen (letzter Liefer-)Termin mehrfache Einträge erhalten, so wird:
1. beim XLS Import (siehe oben) die Mengen nur aktualisiert
2. bei allen anderen Importen werden zum Termin jeweils eine Sekunde hinzugezählt und so auch mehrfache Einträge dargestellt, damit Sie die unterschiedlichen Mengenangaben für den gleichen Artikel erkennen können.

#### Wo kann der Startwert der Liefermengenfortschrittszahl eingestellt werden ?
Siehe Kunde, Sonderkonditionen. Hier sind die Artikel mit eventuellen Liefermengenfortschrittszahlen Startwerte angeführt.<br>
Bitte beachten Sie dass die gültig von - bis auch für den Startwert der jeweiligen Liefermengenfortschrittszahl gilt. Dies wird so angewendet, dass der Startwert anhand des Von - Bis Datums in Bezug zum Berechnungszeitpunkt (liegt heute im von - bis) ermittelt wird.

#### Wie wird die Liefermengenfortschrittszahl berücksichtigt?
Der Sinn und Zweck der Liefermengenfortschrittszahl ist, die Menge zu ermitteln, die zwischen Ihrem Versandstandort und Ihrem Kunden noch unterwegs ist. D.h. einerseits meldet Ihr Kunde welche Stückzahlen er schon erhalten (zugebucht) hat, andererseits wissen Sie wieviel von welchem Artikel bereits an den jeweiligen Kunden (bitte jede Liefer-/Rechnungsadresse ist ein eigener Kunde) versandt wurde. Das bedeutet, der Liefermengen-Startwert plus die aktuell gelieferte Menge ergibt gemeinsam die Vergleichszahl. Diese wird nun mit der von Ihrem Kunden gemeldeten Liefermengenfortschrittszahl verglichen. Die Differenz(menge) ist die Menge die noch am Weg (auf der Strecke) zu Ihrem Kunden ist.
![](Liefermengen.gif)

Diese Differenzmenge wird automatisch wenn ein Forecastauftrag freigegeben wird (von angelegt nach freigegeben, mit dem grünen Haken im Reiter Aufträge) in den ältesten Forecastmengen des jeweiligen Artikels berücksichtigt. Die Berücksichtigung erfolgt in der Form, dass von der ältesten Position ausgehend die jeweilige Position auf erledigt gesetzt wird. Für den Fall, dass die Differenzmenge nur teilweise in einer Forecastpositionsmenge verbraucht wird, wird die Position geteilt, sodass die Menge unterwegs 1:1 in den erledigten Forecastpositionen abgebildet ist.
![](FC_Pos_Status.gif)
Beachten Sie hier bitte den Eintrag in der Spalte Status. Die Zeilen mit dem Status erledigt werden in den Reservierungen nicht mitgerechnet und bewirken daher auch keinen Bedarf. Die Idee dass diese Zeile trotzdem angezeigt wird ist, dass es sicherlich immer wieder Abstimmungsarbeiten mit Ihrem Kunden geben wird und daher diese Zeilen sehr nützlich sein können, insbesondere wenn der EDI Import verwendet wurde.

**WICHTIG:** Ist bei der Differenz / Menge unterwegs ein **negativer Wert** gegeben, so bedeutet dies, dass Ihr Kunde behauptet, dass er mehr bekommen hat, als Sie ihm gesandt haben.
Technisch gesprochen gibt es das nicht, aus der Praxis berichtet, kann es unter verschiedenen Umständen zu dieser Situation kommen:
1. Es gibt in Ihrem **Kieselstein ERP** mehrere Adressendatensätze (Partner) die gleichlautende Empfängeradressen haben. D.h. Sie haben z.B. einmal eine Lieferung an die Rechnungsadresse gesandt und einmal an die Lieferadresse. Ihr Kunde zählt diese selbstverständlich zusammen, für Ihr **Kieselstein ERP** sind das zwei unterschiedliche Kunden und werden daher getrennt betrachtet.
Lösung: Stellen Sie die Lieferungen / die Lieferadressen richtig. Sollte die nachträgliche Änderung zu kompliziert werden, bitten Sie Ihren **Kieselstein ERP** Betreuer um Hilfe.
2. Ihr Kunde hat die Lieferung eines anderen seiner Lieferanten auf einen Ihrer Wareneingänge gebucht, auch das soll schon vorgekommen sein.

Es bedeutet in jedem Falle, dass Sie diese Situation klären müssen. **Kieselstein ERP** unterstützt Sie dabei vor allem mit Statistiken, also Kunden-Lieferstatistik, eingeschränkt auf den Artikel und natürlich immer auf den Kunden eingeschränkt um den es geht und unterscheiden Sie klar zwischen Liefer- und Rechnungsadressen.

**Hinweis:** Da Sie, um die negative Menge, schon zuviel an Ihren Kunden geliefert haben, wird bei der Berechnung der offenen Mengen aus dem Forecast, diese offenen Mengen entsprechend reduziert. D.h. auch aus diesem Grunde muss dies geklärt werden.

#### Gibt es eine andere Möglichkeit die Menge unterwegs zu errechnen?
Ja es gibt eine weitere, allerdings nur manuelle Möglichkeit die Menge unterwegs zu errechnen.
Sie sehen im Liefermengenausdruck auch den Lieferscheintext. Wenn Ihr Kunde ordentlich arbeitet, wovon auszugehen ist, trägt er hier Ihre Lieferscheinnummer ein, manche tragen auch Ihre Rechnungsnummer ein. Das bedeutet, Sie wissen, bis zu welcher Lieferung der Kunde Ware eingebucht hat. Alle weiteren Lieferungen können als zum Kunden unterwegs betrachtet werden. D.h. Sie nehmen die Lieferscheinnummer der letzten Lieferung und ermitteln aus der Kundenlieferstatistik welche Lieferungen und Mengen noch nicht zugebucht wurden, die NACH der letzten Lieferung sind. Dies ist ebenfalls die Menge unterwegs (wenn wir davon ausgehen dass die Ware beim Kunden ankommt). Diese beiden Zahlen sollten übereinstimmen.
Warum macht das **Kieselstein ERP** nicht automatisch? Wir haben leider keinen Einfluss darauf wie Ihr Kunde Ihre Lieferscheinnummern erfasst (siehe oben, das ist keine echte **Kieselstein ERP** Nummer) manche erfassen / übermitteln eben auch nur die Rechnungsnummern usw.. Daher stellen wir derzeit programmtechnisch keine Zuordnung her.

#### Kann ich feststellen ob ein Kunde eine Lieferung nicht bekommen hat?
Analog zu obigem, muss natürlich jede deiner  Lieferungen auch in der Liefermengenstatistik aufscheinen. Fehlt hier eine Lieferung so wurde diese nicht eingebucht. Ob das deswegen ist, weil die Sendung verloren gegangen ist, oder der Kunde sie nicht eingebucht hat, muss bitte geklärt werden.

#### Mein Kunde beginnt jedes Jahr neu die Liefermengenfortschrittszahl zu errechnen. Wie kann das berücksichtigt werden?
Manche Firmen rechnen die Liefermengenfortschrittszahl nicht über die Lebensdauer des Produktes / der Lieferung sondern beginnen jedes Jahr, unter Umständen zu anderen Stichtagen als Ihr Geschäfts- / Wirtschaftsjahr, neu zu zählen. Das bedeutet, dass jedes Jahr der Startwert der Artikel für diese Kunden neu angepasst werden müssen. Da dies durchaus von Kunde zu Kunde unterschiedlich gehandhabt wird, müssen die übermittelten Liefermengenfortschrittszahlen überwacht werden. Nutzen Sie dazu den Report der Liefermengen aus Kunde, Info, Liefermengen.
Der Vorteil dieser Methode ist sicherlich darin zu sehen, dass Fehler aus der Vergangenheit nicht länger mitgeschleppt werden. Das interessante ist jedoch, wie mit Lieferungen umgegangen wird, welche während der Rücksetzung noch zu Ihrem Kunden unterwegs sind, gerade bei Lieferzeiten von 70Tagen oder ähnlichem.
Die Anpassung des Startwertes errechnet sich wie folgt:
![](LMFZ_Jahresreset.jpg)

Das bedeutet:
Da die Menge der gelieferten Artikel in **Kieselstein ERP** immer die Gesamtmenge des an diesen Kunden/Lieferadresse gelieferten Artikels darstellt, Ihr Kunde aber die Liefermengenfortschrittszahl jedes Jahr neu zu zählen beginnt, muss diese jedes Jahr neu gesetzt werden.<br>
Bitte tragen Sie, auch zur Rückverfolgbarkeit, den jeweils neuen Startwert in den Sonderkonditionen des Kunden ein. Das Gültig Ab Datum stellen Sie auf den Zeitpunkt ab dem der neue Startwert gilt.<br>
Theoretisch entspricht die Differenz zwischen dem Startwert aus dem vorigen Jahr und dem neuen Startwert aus dem aktuellen Jahr, der Liefermenge welche im vergangenen Jahr bei Ihrem Kunden eingetroffen ist. Praktisch hängt das natürlich auch noch von der Lieferdauer an Ihren Kunden ab. Würde in obigem Beispiel eine der Lieferungen welche normalerweise per Schiff erfolgen und 70Tage dauern, per Luftfracht gesandt, so wirft dies die Gegenüberstellung Ihrer Lieferreihenfolge in Vergleich zu den Wareneingangsbuchungen Ihres Kunden natürlich durcheinander.<br>
Eigentlich ist der Startwert jedes Jahr um die im vergangenen Jahr gelieferte Menge zu reduzieren (wird negativer).

#### Wie wirken Wareneingangsdatums in der Zukunft bei der Übermittlung der Liefermengenfortschrittszahlen?
Es kommt sehr selten vor, dass sich die Mitarbeiter Ihres Kunden bei der Erfassung der Wareneingänge im Datum vertippen. Wenn nun der Fall eintritt, dass das erfasste Datum wesentlich in der Zukunft liegt,
![](LMFZ_Zukunft.jpg)
so bewirkt dies, bei der Überleitung der EDI Aufträge in die Forecastpositionen, dass, insbesondere wenn dies mit jährlichen Liefermengenfortschrittszahlen kombiniert ist, dass dies die Berechnung der tatsächlichen Menge unterwegs komplett falsch darstellt.
Sollte dieser Fall eintreten, so lassen Sie bitte diese Datum durch Ihren Kunden korrigieren. Auch bei Ihm muss der Wareneingang und damit die Stichtagsbetrachtung seiner Warenbewegungen falsch sein. Alternativ kann dies auch durch Ihren **Kieselstein ERP** Betreuer in Ihrer **Kieselstein ERP** Datenbank korrigiert werden.

#### Wo erhalte ich eine Übersicht über die Liefersituation eines einzelnen Forecastauftrages?
Wechseln Sie dazu im Modul Forecastauftrag auf den Reiter Aufträge. Hier finden Sie neben dem Button für die Deltaliste den Druck-Button
![](Liefersituation.jpg)
 für ein übersichtliches Journal der Liefersituation jeder einzelnen Forecastposition dieses einen Forecastauftrages.

#### Wird ein eventueller Rückstand der EDI Daten berücksichtigt?
Der sogenannte Backlog. bzw. Back-Order oder auch Auftragsrückstand wird von **Kieselstein ERP** aus den EDI Daten QTY+83 herausgelesen. Da dies ein von Ihrem Kunden gemeldeter Rückstand ist, wird dies als Forecastposition mit Liefertermin (von der dann noch die Lieferdauer abgezogen wird) mit Importzeitpunkt als zusätzliche Forecastposition eingetragen.
Die Rückstandsmeldung (Backlog) Ihres Kunden begründet sich aus folgenden Daten:
1. seine Forecastmengen mit den gewünschten Eintreffterminen
2. Ihre Lieferungen welche hoffentlich rechtzeitig in der bestellten Menge beim Kunden sind
3. D.h. der Backlog ist eine Darstellung dessen, dass bereits bestellte Ware noch nicht beim Kunden ist.<br>
D.h. wir gehen davon aus, wenn Ihnen der Kunden einen Forecast sendet, so sind in den Forecastauftragspositionen ausschließliche Daten für die Zukunft enthalten. Ein eventueller Rückstand ist ausschließlich in einer Backlogposition enthalten. Leider ist in den uns bekannten EDI Daten im Backlog keine wie immer geartete Bestellnummer Ihres Kunden enthalten, wodurch entsprechende Zuordnungen verloren gehen und somit auch bei der EDI Übermittlung der Lieferscheindaten an Ihren Kunden NICHT mehr zur Verfügung stehen.

In anderen Worten ist der Backlog eine Information Ihres Kunden, dass Ware nicht wie gewünscht gekommen ist. Was nun aber die Ursache dafür ist, Lieferung verloren gegangen, falsche Ware geliefert, noch nicht geliefert, noch nicht eingebucht, Kunde findet die Ware nicht mehr obwohl die Übernahme bestätigt wurde usw. usf. muss Ihre Administration herausfinden.

#### Wie kann ich das prüfen?
Sie müssen das in den offenen auszuliefernden Positionen (Auftrag, Journal) finden. D.h. die Backlogmenge welche von Ihrem Kunden gemeldet wird, muss auch aus den noch zu liefernden Positionen unter Berücksichtigung der Menge zum Kunden unterwegs ersichtlich sein.

#### Wie wirkt die Liefermengenfortschrittszahl im Backlog?
Da in den Forecastmengen von der ältesten zur jüngsten hin die Liefermengenfortschrittszahl, genauer die Menge unterwegs zum Kunden, in der Form berücksichtigt wird, dass diese Positionen erledigt werden (und gegebenenfalls vorher noch gesplittet werden), ergibt dies dass der Backlog den Bedarf erhöht, da er ja zeitlich als erstes geliefert werden sollte und daher um die Menge unterwegs wieder reduziert wird.

#### Was geschieht bei der Freigabe eines Forecastauftrages?
Vor allem abhängig von der Eigenschaft Zusammenziehen aus dem Reiter  verhält sich ein Forecastauftrag wie folgt:

Bei der Freigabe werden auch die Verpackungsmengen bzw. die Verpackungsmittelmengen entsprechend berücksichtigt. D.h.:

- Sind Linienabrufe auf einer Forecastposition vorhanden, so werden diese Linienabrufmengen auf die Verpackungsmenge (Bündel) aufgerundet. Die Menge der Forecastpositio ist dann die Summe der aufgerundeten Linienabrufmengen. Die so errechnet Forecastmenge wird dann nicht mehr gerundet. Damit kann es vorkommen, dass von der Menge her keine vollen Verpackungsmittel gegeben sind. Dies ist so beabsichtigt.
- Sind auf einer Forecastposition keine Linienabrufe eingetragen, so wird die Forecastmenge auf die Verpackungsmittelmenge (Kistenmenge) aufgerundet.
Zusätzlich bleiben die original Mengen erhalten. Bei einer Rücknahme der Freigabe werden die Mengen wieder auf die original Mengen zurückgesetzt.

<a name="Planungszwecke"></a>

#### Kann ich den Forecast nur Planungszwecke verwenden?
Ja, Stellen Sie Call-Off Tag und Call-Off Woche auf -999\. Damit werden keine Reservierungen aktiviert.
![](Termineinstellung_ohne_Reservierung.gif)
Aber mit Forecastauftrag +999 können Planungszahlen dargestellt werden. So können die Auswertungen für die theoretischen Bedarfe gemacht werden.
Dies wird gerne dann verwendet, wenn die eigentlichen Abrufe / Bestellungen Ihrer Kunden über echte Bestellungen oder gerne auch direkt über EDI, z.B. Clever Cure, eingespielt werden.
Somit erhalten Sie z.B. folgende Bewegungsvorschau:
![](Bewegungsvorschau_mit_Forecast_ohne_Reservierung.jpg)

### Nützliche Auswertungen

#### Bedarfe
#### Infos an Ihre Vorlieferanten
#### Journal, Beschaffung

![](Info_an_Vorlieferanten.gif)
In der Sortierung nach Lieferant sehen Sie, welche Artikel Sie wann von Ihren Lieferanten benötigen.
Diese Auswertung ist nach Artikeln verdichtet. D.h. je Artikel ist nur ein Eintrag in dieser Liste mit den Gesamtbedarfen enthalten.
Durch die farbliche Gestaltung sehen Sie auch die momentane Reichweite Ihres Lagers bzw. der Bestellungen.
Hinweis: Die Bestellten Mengen werden nur in Ihrer Gesamtheit, aber nicht terminlich berücksichtigt.
![](Lieferanten_Forecast.jpg)

Auswertung sortiert nach Auftrag und Stücklistenstruktur.
Hier erhalten Sie für jeden Forecastauftrag die Auflösung jeder Forecastposition, ebenfalls nach Monaten dargestellt inkl. der enthaltenen Unterstücklistenpositionen.
![](Auftragsstruktur_Forecast.jpg)

#### Einzelforecast
Um eine Übersicht über einen einzelnen Forecastauftrag zu erhalten, wählen Sie im Reiter Aufträge den gewünschten Forecastauftrag aus und klicken auf den Drucker.
So sehen Sie die Auflösung nur dieses einen Auftrags. Das Journal liefert, wie üblich, die Übersicht über die Gesamtheit.

#### Liefersituation
Wie ist der aktuelle Auslieferstatus jeder einzelnen Forecastposition Ihres Forecastauftrages

![](Liefersituation.gif)

#### Deltaliste
Also der Vergleich des bisher freigegebenen Forecastauftrages mit dem nun geplanten neuen Forecastauftrag.
Hier sehen Sie die Abweichungen in Stückzahl und Termin.
![](Deltaliste.jpg)

-----------------
### Musterdateien
#### Forecast Import VAT Format
Als Muster [siehe](./Musterdateien/Forecast_Import_VAT.xls)<br>
In diesem XLS haben die Spalten folgende Bedeutung:
| Spalte | Name | Bedeutung |
| --- | --- | --- |
| C | partCD | Referenznummer aus dem Artikelstamm |
| J | measure | es werden nur die Zeilen übernommen, die mit GROSS gekennzeichnet sind |
| K und folgende | | beinhalten den zu importierenden Termin. Es werden NUR je Spalten mit Terminen ab dem aktuellen Importmonat importiert. Weiters werden nur jene Zellen eingelesen, die einen Wert größer 0 haben. |

#### Forecast Import Zeiss Format
Als Muster [siehe](./Musterdateien/Forecast_Import_Zeiss.csv).<br>
Dieser wird für den Import der Bestelldaten in das Zeiss Pufferlager verwendet.<br>
Bitte achten Sie auf den Spaltentrenner Semicolon ;<br>
In diesem CSV haben die Spalten folgende Bedeutung:
| Spalte | Name | Bedeutung |
| --- | --- | --- |
| J | Material | Referenznummer des Artikels |
| O | Kategorie Bezeichnung | Es werden nur die Zeilen übernommen, die mit Forecast gekennzeichnet sind. |
| R und folgende | | beinhalten den zu importierenden Termin. Diese Daten werden wie folgt interpretiert.<br>W ... Kalenderwoche .... Es wird der Montag dieser Woche als Termin eingetragen (W 43.2022 = 24.10.2022)<br>M ... Monat ... Es wird der 1\. des Monates als Termin eingetragen (M 11.2022 = 1.11.2022) |
| Spalten mit \>= oder \<= werden ignoriert | | |
