---
title: "Angebot"
linkTitle: "Angebot"
categories: ["Angebot"]
tags: ["Angebot"]
weight: 30
description: >
  Angebote erstellen, nachverfolgen, gewichteter Hoffnungsfall
---

Statusübergang
![](statusuebergang.png)
| Legende: | |
| --- | ---
| AB | Auftragsbestätigung an Kunde |
| AG | Angebot |


## Anlegen eines Angebots

Um ein neues Angebot anzulegen gehen Sie bitte wie folgt vor:
Klicken Sie auf das Modul Angebot ![](Angebot.JPG) in der nun offenen Auswahl klicken Sie auf Neu ![](Angebot_Neu.gif) .
Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den Kunden, Lieferzeit, Gültigkeit - je nach Bedarf.

Mit einem Klick auf Speichern ![](speichern.JPG) bestätigen Sie die Eingaben.

Wechseln Sie in den Reiter Positionen und geben hier die Positionen, die Sie anbieten möchten ein. 
Um eine neue Position hinzuzufügen klicken Sie auf Neu ![](Angebot_Neu.gif) wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#positionsarten">}} )), geben die Informationen (u.a. Menge) ein und klicken auf ![](speichern.JPG) Speichern. Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.

Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Konditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für dieses eine Angebot ab. 

Mit Klick auf ![](drucken.JPG) Drucken (oder oberes Menü Angebot - Drucken...) erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG), dem Versenden per E-Mail ![](e-mail.jpg) oder speichern ![](speichern.JPG), aktivieren Sie das Angebot, der Status (siehe oben) ändert sich von angelegt in offen. 

#### Können Positionen aus Stücklisten auch auf offiziellen Belegen angedruckt werden?
Ja. Es müssen dazu die gewünschten Positionen in der Stückliste markiert werden.
Danach werden beim Druck des Angebotes die gewählten Stücklisten-Positionen nach dem eigentlichen Artikel unter der Angabe von Menge, Mengeneinheit und Bezeichnung zeilenweise mit angedruckt. Siehe dazu auch [Stückliste]( {{<relref "/warenwirtschaft/stueckliste/#auf-beleg-mitdrucken" >}} ).

#### Wie ist die Positionsart "AGStückliste" zu verstehen?
Die Positionsart "AGStückliste" dient der Verwendung von Angebotsstücklisten im Angebot. Damit haben Sie die einfache Möglichkeit "Verkaufsstücklisten" zu erstellen und diese, ohne Artikel- und Stücklistenverwaltung aufzublähen, in einem eigenen Modul zu organisieren. Auch bei den Angebotsstücklisten können die anzudruckenden Angebotsstücklistenpositionen definiert werden.

Wurde die Positionsart AGStückliste ausgewählt, so kann über den Button "Agstkl..." eine bereits angelegte Angebotsstückliste ausgewählt werden, die damit diesem Angebot zugeordnet wird. Von **Kieselstein ERP** wird der kalkulatorische Wert der Angebotsstückliste ermittelt und in der Preiseingabe angezeigt. Sie können nun den Positionspreis manuell eingegeben, der Vorschlagswert für den kalkulatorischen Wert der Position kann übersteuert werden.

##### Wie werden die Preise von Stücklisten bzw. Angebotsstückliste in das Angebot übernommen
Dies hängt vom Parameter PREIS_AUS_GESAMTKALKULATION_WENN_STUECKLISTE ab.<br>
Zusätzlich wird dann das Verhalten noch von der Kalkulationsart der Angebotsstückliste gesteuert.
- Kalkulationsart = 1<br>
Wenn für den Stücklistenartikel kein Verkaufspreis hinterlegt ist, so wird als Verkaufspreis der Angebotsposition der Lief1Preis zzgl. MindestDB verwendet. (Bitte hier prüfen, ob auch die Gemeinkostenfaktoren berücksichtigt werden.)<br>
- Kalkulationsart = 2<br>
Hier wird für die Ermittlung des Verkaufspreises wie folgt vorgegangen:
1. Gibt es einen Verkaufspreis, so wird dieser verwendet, sonst,
2. Ist die Stückliste fremdgefertig, so gilt der Einzelpreis, sonst
3. wenn es einen Preis aus der Gesamtkalkulation gibt so gilt dieser, egal ob fremdgefertigt oder nicht,
4. treffen obige Punkte nicht zu, so der der Lief1preis als Einzelpreis


#### Wie können sehr rasch und einfach Angebotsstücklisten aus dem Angebot erzeugt werden?
Nutzen Sie dafür die Angebotsstücklisten-Schnellerfassung ![](Angebotsstuecklisten_Schnellanlage.gif). [Details dazu siehe]( {{<relref "/verkauf/angebotsstueckliste/schnellerfassung" >}} ).

#### Wie kann der Lieferantenpreis, der Einkaufspreis für einen Artikel schon im Angebot dokumentiert werden?
<a name="Lieferant_angeben"></a>
Wenn Parameter LIEFERANT_ANGEBEN in der Kategorie Angebot gesetzt ist (Wert des Parameters=1), so kann im Angebot und im Auftrag je Artikel-(Ident-)Position auch der Lieferant angegeben werden. Zusätzlich kann der geplante Einkaufspreis mit angegeben werden. Dies bewirkt im weiteren Verlauf, dass wenn die Position kopiert wird (ZWA), der Preis als Nettopreis im Bestellvorschlag und in der Bestellung aus dem Angebot (EK-Preis) verwendet wird. Wenn in dem Feld Lieferantenpreis keine Information eingegeben wird, so wird die übliche Logik (Preise und Bezeichnungen aus dem Artikelllieferanten) verwendet.![](lieferant_angeben.JPG)

#### Wie / wann kann die Währung eines Angebotes geändert werden?
In die Währung wird als erste Vorbesetzung die Währung des Kunden oder Lieferanten zum Anlegezeitpunkt übernommen. Ändert später ein Kunde oder Lieferant seine Währung, bleibt die übernommene Währung unberührt. Werden mengen-/preisbehaftete Positionen erfasst, so sperrt das Bewegungsmodule automatisch das Währungsfeld. Tipp: Ein Löschen aller mengen-/preisbehafteten Positionen öffnet das Währungsfeld wieder.

#### Wie kann ich den fälschlich zugeordneten Ansprechpartner wieder löschen?
Klicken Sie erneut auf den Knopf ![](Ansprechpartner_Auswahl.gif) und in der Auswahlliste dann auf ![](Loeschen.gif) löschen.

Damit wird der zugeordnete Ansprechpartner wieder entfernt.

#### Wie kann ich ein fälschlich erledigtes Angebot wieder auf offen setzen?
Gehen Sie in die Kopfdaten des entsprechenden Angebotes und klicken Sie auf ändern. Die nachfolgende Frage beantworten Sie mit Ja. Nun kann das Angebot wieder bearbeitet werden, bzw. kann daraus ein Auftrag erstellt werden.

#### Was versteht man unter alternativen Angebots-Positionen?
Eine Angebots-Position vom Typ Ident, Handeingabe, AGStückliste kann als alternativ gekennzeichnet werden, solange sich ein Angebot im Status Angelegt befindet. Eine entsprechend gekennzeichnete Position ist als alternativ zum direkten Vorgänger zu betrachten. Eine alternative Position wird am Angebots Druck mit dem Text "Option" gekennzeichnet. Sie erhält keine Positionsnummer und ihr Gesamtwert wird nicht angedruckt. Außerdem wird sie bei der Angebots-Gesamtwertberechnung sowie in sämtlichen Auswertungen und in der Angebots-Vorkalkulation nicht berücksichtigt.
![](Alternative_Angebotsposition.gif) alternativ kann in den Positionsdaten definiert werden. Sie finden diese unmittelbar unter der Eingabe der Mehrwertsteuer.

#### Was bewirkt eine AG Position vom Typ "Endsumme"?
Eine Position vom Typ "Endsumme" bezeichnet die Position im Angebotsdruck, an dem die Endsumme angedruckt wird. Damit ist es möglich, innerhalb der Angebots-Positionen Texte zu erfassen, die nach der Endsumme angedruckt werden. Pro Beleg kann nur eine Position vom Typ "Endsumme" erfasst werden. Es ist nicht möglich, eine preisbehaftete Position nach der "Endsumme" zu erfassen. Wird dies versucht, so wird automatisch die Position Endsumme nach der neu eingefügten preisbehafteten Position eingereiht.

#### Kann ich freie Texteingaben machen?
Ja. Bitte beachten Sie dazu [Freie Texte in Belegen]( {{<relref "/start/01_grundsätzliche_bedienung/freie_Texte">}} ).

#### Wie kann ich reine Kostenpositionen im Angebot anführen?
Verwenden Sie dazu [kalkulatorische Artikel]( {{<relref "http://localhost:1313/docs/stammdaten/artikel/#was-ist-ein-kalkulatorischer-artikel-" >}} ).

Für die theoretische Preisberechnung der Pauschalpositionen anhand der Verkaufspreise der kalkulatorischen Artikel siehe.

## Angebots Vorkalkulation
Mit der Angebots Vorkalkulation steht Ihnen die Kalkulation Ihres Angebotes unter Berücksichtigung aller Materialkosten über alle Stücklistenebenen zur Verfügung. Da aber, gerade bei Vorkalkulationen nicht jedes kleinste Detail von Bedeutung ist, steht mit dem Mandantenparameter ANGBOT_AGVORKALK_STKL_AUFLOESUNG_TIEFE die Möglichkeit der Begrenzung der Auflösungstiefe zur Verfügung. 
0 bedeutet, es werden keine Stücklisten Unterpositionen angedruckt. Es scheint lediglich die Stücklisten Position selbst auf.
1 bedeutet, es werden die Stücklistenpositionen der ersten Ebene ebenfalls in die Vorkalkulation mit einbezogen usw.

Bitte beachten Sie weiters, dass Fremdfertigungsstücklisten nicht weiter aufgelöst werden, also wie Zukaufteile behandelt werden.

#### Welche Preise werden in der Angebots Vorkalkulation angedruckt ?
In der Angebots Vorkalkulation werden der aktuelle Gestehungspreis, der Lieferantenstaffelpreis und der (Kunden-) Verkaufspreis angedruckt. Für die Findung des Lieferantenstaffelpreis werden auch die angebotenen Mengen, wie in der Stücklistengesamtkalkulation, mit berücksichtigt.
Beachten Sie bitte den Parameter LIEFERANT_ANGEBEN in der Kategorie Angebot. Steht hier der Wert des Parameters auf 1, so kann im Angebot und im Auftrag je Artikel-(Ident-)Position auch der Lieferant angegeben werden. Zusätzlich kann der geplante Einkaufspreis mit angegeben werden. Der Andruck der Lieferantenpreise ist so gelöst, dass:
a.) wenn kein spezifischer Lieferant auf der Angebotsposition eingegeben ist, so wird bei der Vorkalkulation der bevorzugte Lieferant aus dem Artikel verwendet
b.) ist die Position eine Handeingabe, so wird als Lieferantenpreis der eingegebene Gestehungspreis verwendet.
In der Deckungsbeitragsberechnung für die Lieferanten werden auch die Transportkosten des Lieferanten berücksichtigt. Diese können im jeweiligen Lieferanten im Reiter Konditionen eingegeben werden.

#### Wie kann ich Lagerstände in der Angebots Vorkalkulation andrucken?
Um die verfügbaren Lagerstände in der Angebots Vorkalkulation anzudrucken ist es erforderlich, dass der geplante Realisierungstermin für das Angebot unter Konditionen eingetragen wird.
Hintergrund ist, dass eine reine Lagerstandsanzeige als Aussage der Lieferfähigkeit viel zu gefährlich wäre, da es sein kann, dass zwar im Moment ausreichend Ware auf Lager ist, jedoch zum Zeitpunkt der Lieferung, also dem Realisierungszeitpunkt, die Ware bereits für andere Aufträge verbraucht würde.
Bitte beachten Sie, dass bei der Anzeige des fiktiven Lagerstandes alle Reservierungen, Fehlmengen, Bestellungen, Fertigungsablieferungen des jeweiligen Artikels terminlich berücksichtigt werden, jedoch nicht eventuell auf den gleichen Artikel lautende Angebote.
Wurde bei einem Angebot ein Realisierungstermin angegeben, so wird bei nicht ausreichenden fiktiven Lagerständen die Menge in Reverse gedruckt.

#### Was bedeutet Lief1preis zu alt?
[Siehe]( {{<relref "/warenwirtschaft/stueckliste/#was-bedeutet-das-lief1preis-zu-alt" >}} )

#### Wo bekommt man eine Übersicht über Lagerstände zu Angebotspositionen?
Nutzen Sie dazu bitte die Vorkalkulation in Verbindung mit dem geplanten Realisierungstermin.

#### Wie können einfach die Soll-Verkaufswerte von Kalkulatorischen- / Pauschalartikeln ermittelt werden ?
Nutzen Sie dafür im Angebot die Vorkalkulation.
In dieser werden die Verkaufswerte der kalkulatorischen Artikel angezeigt. Die Voraussetzungen dafür sind:
- es müssen die kalkulatorischen Artikeln entsprechende Verkaufspreise hinterlegt haben, wobei hier die übliche Verkaufspreisfindung wirkt
- in der Angebotsvorkalkulation wird dann zusätzlich die Summe der kalkulatorischen Werte angedruckt. Somit können Sie sehr einfach die pauschalen Werte und den tatsächlich geplanten Verkaufserlös vergleichen.

#### In der Vorkalkulation wird kein Gestehungspreis / Wert angedruckt, er ist aber im Artikel eingetragen.
Die Frage des Anwenders lautete, warum sehe ich beim oberen Artikel keinen Gestehungspreis,
![](Ang_Vorkalk_Gestpreis_fehlt.gif)
wenn er doch im Artikel definiert ist.
![](Ang_Vorkalk_Gestpreis_fehlt2.jpg)

Hintergrund: Der Gestehungspreis der einzelnen Position wird beim Anlegen der Position im Angebot mit abgespeichert. So sieht man in den Angebotspositionen unten im Detail, dass die obere Position einen Gestehungspreis von 0 hat. Die zweite Position hat den Gestehungspreis wie eingetragen.
Das kommt z.B. dann vor, wenn der Gestehungspreis erst danach angepasst wird.
**WICHTIG:** Wenn ein Gestehungspreis angepasst wird bewirkt dies automatisch, dass die Kette der Warenbuchungen durchbrochen wird. Passen Sie den Gestehungspreis nur in Notfällen über diesen Weg an. Wenn ein Gestehungspreis falsch ist, ist es viel richtiger die Ursache richtigzustellen.

Noch ein Tipp:
Nutzen Sie für Ihre Betrachtungen, gerade bei neuen Artikeln, den Vergleich mit dem Lief1Preis und sehen Sie den Vergleich mit dem Gestehungspreis als Sicherheit und als Vergleich mit der Vergangenheit.

Angebote mit Stücklistenpositionen

Bei verschiedenen Anwendungen ist es praktisch, wenn für den Kunden Details aus der Stückliste auf dem Angebot mit angedruckt werden. Auch hier kann durch den Mandantenparameter ANGBOT_AG_STKL_AUFLOESUNG_TIEFE die Auflösungstiefe der Stückliste entsprechend gesteuert werden. Bitte beachten Sie bei beiden Parametern die Erhöhung der Berechnungsdauer für den jeweiligen Report. Je tiefer die Auflösung erfolgen muss, desto länger dauert auch die Berechnung.

Woran erkenne ich ob eine Artikelnummer durch das Vorbesetzen oder durch Ändern / Artikelauswahl übernommen wurde?

In der Detailbearbeitung einer Angebotsposition wird bei der Positionsart Ident, die zuletzt gewählte Artikelnummer (= Ident) automatisch vorgeschlagen. Dies hat den Zweck, Ihnen entsprechende Tipparbeit bei ähnlichen / gleichen Artikeln (z.B. Mengenstaffeln) zu ersparen. Anhand der Anzeige der Bezeichnung des Artikels kann erkannt werden, ob die Artikelnummer aus dem Vorschlag kommt, also die zuletzt eingegebene Artikelnummer ist, oder ob diese aus einer Auswahl kommt. Wenn die Artikelnummer nur vorbesetzt wurde, so wird KEINE Bezeichnung angezeigt. Erst nachdem die Eingabe der Artikelnummer mit TAB(ulator) verlassen wird, bzw. die Artikelnummer über den Auswahl Knopf Artikel... ausgewählt wurde, werden die Bezeichnungen des Artikels angezeigt.

Angebot Druck

Es gibt drei spezielle Lieferzeiten (egal welcher Einheit), die auf dem Druck im Angebot folgendes bewirken:

| Lieferzeit | Sondertext |
| --- |  --- |
| 0 | Lieferung ab Lager |
| 98 | Nach Vereinbarung, freibleibend |
| 99 | Nach Vereinbarung |

### Fehlerhafter Angebotsdruck
<a name="Fehlerhafter Angebotsdruck"></a>

![](Fehlerhafter_Angebotsdruck.gif)

Erscheint ein Ausdruck wie oben dargestellt oder ähnlich, so wurde die Texteingabe nicht vollständig abgespeichert. Um die Texteingabe richtig auszudrucken gehen Sie bitte erneut in die Bearbeitung der Texteingabe und speichern Sie diese erneut ab. Damit ist der Fehler behoben.

[Positionsarten in den Verkaufsmodulen]( {{<relref "/verkauf/gemeinsamkeiten"  >}} )

#### Können die Angebots-Lieferzeiten geändert werden.
Ja. Wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer (ANG_ANGEBOTEINHEIT). Er wird das gerne für Sie einstellen.

#### Was bedeuten die einzelnen Spalten der Umsatzübersicht?
<a name="Umsatzübersicht"></a>
Sie finden in **Kieselstein ERP** in den Verkaufsmodulen jeweils eine Umsatzübersicht. Diese ist vor allem als schnelle Übersicht über den Status des jeweiligen Moduls gedacht. So erhalten Sie im Angebotswesen eine kompakte Übersicht über die Angebot nur für einen Vertreter (= Vertriebsmitarbeiter) oder über das gesamte Unternehmen.

Die Darstellungen bedeuten im Einzelnen:

Die linke Spalte stellt immer den Zeitraum dar:

![](Angebotsumsatzuebersicht1.gif)

D.h. die Anzeige erfolgt in Ihrer Unternehmenswährung in unserem Falle in Euro.

Unter Vorjahr sehen Sie die Werte des Jahres vor der gewählten Darstellung, darunter die einzelnen Monate des gewählten Darstellungsjahres, darunter die Jahressumme des gewählten Jahres und dann, soweit sinnvoll, eine Gesamtsumme.

In horizontaler Richtung stehen im Angebot folgende Spalten zur Verfügung:

![](Angebotsumsatzuebersicht2.gif)

| Spalte | Bedeutung |
| --- |  --- |
| Gesamt | Gesamtwert der gelegten Angebote im Zeitraum, z.B. für den Monat Januar/Jänner |
| Offene | Der Werte der im Zeitraum gelegten Angebote sind noch offen. D.h. also noch nicht bestellt bzw. als erledigt (= verloren) abgehakt. |
| Offene Termin bewertet | Hier sehen Sie Ihre gewichteten Hoffnungsfälle. D.h. hier sind die offenen Angebote gewichtet mit der Angebotswahrscheinlichkeit zum voraussichtlichen Realisierungstermin enthalten. Sind in Ihren Angeboten keine Wahrscheinlichkeit und keine Realisierungstermine eingetragen, so erscheinen diese auch nicht in dieser Spalte. |
| Bestellt | Welche Angebote konnten realisiert werden. D.h. welche Angebote wurde in Aufträge übergeleitet. |

Bei den Handeingaben werden keine Einzelpreise gedruckt.

Sieht der Druck einer Angebots, Auftrags, Rechnungsposition z.B. wie folgt aus,

![](Einzelpreise_fehlen1.gif)

so wurden die Verkaufspreise unvollständig eingegeben.

Vermutlich wurde eine Eingabe wie unten angeführt vorgenommen:

![](Einzelpreise_fehlen2.gif)

**Kieselstein ERP** druckt dies so wie von Ihnen gewünscht aus.

Richtig ist folgende Eingabe:

![](Einzelpreise_fehlen3.gif)

Beachten Sie, dass hier auch der Einzelpreis definiert ist und dieser somit auch im Ausdruck angeführt werden kann.

![](Einzelpreise_fehlen4.gif)

#### Ein nicht erhaltenes Angebot erledigen
Haben Sie für ein Angebot den Auftrag nicht erhalten, so sollte dieses trotzdem mit Begründung, warum Sie dieses Angebot nicht erhalten haben erledigt werden. Viele unserer Anwender geben bei den Erledigungsgründen deren Mitbewerber an. So sehen Sie im Laufe der Zeit, an welche Mitbewerber Sie die Angebote verlieren und können Analysieren, warum immer einige wenige Mitbewerber anstatt Ihnen die Aufträge erhalten. Die Auswertung dazu finden Sie unter Journal Abgelehnte Angebote.
Die Manuelle Erledigung des Angebotes finden Sie im Menü des Angebotsmoduls unter Bearbeiten, Manuell erledigen.
Die Definition der Angebots-Erledigungsgründe finden Sie unter Grunddaten, Erledigungsgrund im Modul Angebot.

<a name="Nachträglich einem Auftrag zuordnen"></a>

#### Kann ein Angebot nachträglich einem Auftrag zugeordnet werden ?
Wurde bei der Anlage des Auftrags vergessen, dass dadurch eigentlich ein Angebot erledigt wurde, so kann im Dialog des Erledigens nachträglich der Auftrag zugeordnet werden.
![](Angebot_Erledigen.jpg)
Wählen Sie dazu ebenfalls, den Menüpunkt Bearbeiten, Manuell Erledigen und wählen Sie nun durch Auftrag. Geben Sie im nachfolgenden Dialog den Auftrag an, durch den das Angebot erledigt wurde.

**Hinweis:** Ist auch das Modul Projektklammer in Verwendung, so gilt als Bedingung um ein Angebot durch einen Auftrag zu erledigen, dass sich sowohl Angebot als auch Auftrag auf dasselbe Projekt beziehen.

**Hinweis:** Es kann ein Angebot auch durch mehrere Aufträge erledigt werden.
Markieren Sie einfach die Aufträge die aus dem Angebot gewonnen werden konnten.
**WICHTIG:** Da es immer wieder vorkommt dass die Firma A anfrägt und im Endeffekt Firma B, z.B. ein Schwesterbetrieb aus dem Konzern, dann den Auftrag vergibt, wird in keinster Weise geprüft ob die Daten fachlich überhaupt zusammenpassen.

<a name="Mit Zusammenfassung"></a>

#### Mit Zusammenfassung
In verschiedenen Branchen kommt immer wieder die Notwendigkeit ein umfassendes Angebot in zwei Teilen zu drucken. Diese Möglichkeit haben wir mit dieser Funktion zur Verfügung gestellt.
Da der tatsächliche Druck immer mit einer individuellen Gestaltung zusammenhängt, muss der tatsächliche Ausdruck immer an Ihre Wünsche angepasst werden. Wenden Sie sich dazu bitte an Ihren **Kieselstein ERP** Betreuer.

Die Grundidee ist, dass ein Angebot mit Zusammenfassung zwei Teile hat. Zum Beispiel:

-   erster Teil wird als Deckblatt gedruckt in dem nur eine kurze Darstellung der angebotenen Positionen dafür aber mit Preisen dargestellt wird.

-   der zweite Teil beinhaltet genaue technische Definitionen, mit den Inhalten aus den Kommentaren, den Bildern der Artikel usw.. Hier ist auch oft gewünscht, dass die Bilder der Artikel entsprechend groß gedruckt werden. D.h. diese werden dann üblicherweise im Formular auf echte Höhe gestellt.

-   Eventuell haben diese Ausdrucke noch einen dritten Teil, der allgemeine Texteingaben nach der Endsumme andruckt.

Ob ein Angebot mit Zusammenfassung gedruckt werden sollte, wird in den Konditionen des Angebotes definiert.
Es gelten dafür folgende Voraussetzungen:
Für eine Zusammenfassung, muss es auch eine Endsumme geben.
Es werden in der Zusammenfassung nur die Zeilen der intelligenten Zwischensumme(n) gedruckt. D.h. es muss jede preisbehaftete Position in einer Zwischensumme enthalten sein.
Mehrstufige int. Zwischensummen mit der Zusammenfassung sind nicht vorgesehen.

Beachten Sie dazu auch den Parameter DEFAULT_MIT_ZUSAMMENFASSUNG, mit dem definiert werden kann, ob Ihre Angebote, Auftragsbestätigungen generell mit Zusammenfassung gedruckt werden sollten.

#### Wie füge ich Einzelpositionen eines Set-Artikels (über die Zwischenablage) in ein Angebot ein?
Um im Angebot mit Set-Artikel zu arbeiten muss mind. ein Set-Artikel eingefügt werden. Fügen Sie einen Set-Artikel in das Angebot ein, es werden auch die Einzelpositionen übernommen.
Mit dem Button Löschen können Sie einzelne Positionen löschen. Um nun neue Positionen im Set-Artikel hinzuzufügen haben Sie 2 Möglichkeiten:

-   Markieren Sie eine Position des Sets, danach klicken Sie auf "eine Position vor der aktuellen Position einfügen" (Icon) nun befindet sich der ausgewählte Artikel im Set-Artikel.

-   Kopieren Sie die gewünschten Positionen in die Zwischenablage, danach wechseln Sie zum Angebot, markieren eine Position des Sets und klicken auf "einfügen aus der Zwischenablage" (Icon). Bei dem nun folgenden Dialog klicken Sie auf "vor der aktuellen Cursorposition" somit werden die Positionen in den Set-Artikel im Angebot eingefügt.

Weitere Positionen außerhalb des Set-Artikels können Sie wie gewohnt durch Klick auf neu hinzufügen.

Wichtig: Bitte überprüfen Sie den Preis des Set-Artikels vor dem Versand des Angebots!

#### Angebotspositionen zusammenfassen
Mit der Positionsart intelligente Zwischensumme steht die Möglichkeit der Zusammenfassung von mehreren Angebotspositionen zur Verfügung. [Siehe dazu bitte auch]( {{<relref "/verkauf/gemeinsamkeiten/#wie-ist-die-positionsart-intelligente-zwischensumme-zu-verstehen" >}} )

Hier steht nun auch die Möglichkeit ![](ZWS_Positionspreise.gif) die Anzeige der Positionspreise abzuschalten. Damit wird erreicht, dass für die Positionen die innerhalb der Zwischensumme sind, keine Einzelpreise angedruckt werden. In anderen Worten, es wird damit ein frei konfigurierbarer Setartikel zur Verfügung gestellt.

#### Verwendung des Angebotes für reinen Handel
Im (IT-) Handel hat man oft die Situation, dass von der Idee her zwar standard Artikel geliefert werden, diese aber immer in einer (Kunden-)spezifischen Konfiguration, die beim Lieferanten auch bestellt werden muss, angeboten und geliefert werden. Die Vorgehensweise dazu ist wie folgt:
- a.) Der Kunde sendet / meldet einen Bedarf. Erfassen Sie dies bitte im Modul Projekte
- b.) Sie (der **Kieselstein ERP** Anwender) senden Anfrage(n) an Ihre Lieferanten
- c.) Sie erhalten entsprechende Angebote und entwickeln / erstellen daraus Angebote in **Kieselstein ERP**
- d.) Kunde erteilt Auftrag
- e.) Sie bestellen nun die genau richtigen Artikel bei Ihren Lieferanten

Wichtig zu wissen:

- Das Angebot enthält natürlich mehrere Positionen die einerseits Standard sind, die aber immer individuell für den Kunden konfiguriert werden. D.h. es wird ein standard Artikel verwendet. Die Zusatz-Beschreibung wird als Texteingabe individuell im Angebot hinterlegt.
- Es wird nicht für jeden Artikel (PC, Server) in seiner Spezialkonfiguration ein neuer Artikel erstellt. Trotzdem muss das konkrete Angebot des Lieferanten bei der einzelnen Angebotsposition hinterlegt werden können und auch die Einkaufspreise dazu. Dazu werden die Lieferanten Angebotsdaten in der Ident-Texteingabe eingetragen.
- Bei welchem Lieferanten bestellt wird, wird in der Angebotsposition und damit auch in der Auftragsposition hinterlegt.
- Der geplante Einkaufspreis wird in der Angebots- und in der Auftragsposition hinterlegt.
- Damit ist auch die Vorkalkulation des Angebotes in der Deckungsbeitragsrechnung richtig. Es werden die definierten Einkaufspreise herangezogen.
- Nun wird aus dem Angebot ein Auftrag erstellt (Auftrag aus Angebot). Dabei werden die Daten aus dem Auftrag übernommen, also der Lieferant, der Einkaufspreis und der ergänzende Ident-Text. Somit hat man, Lieferantenpreise,  bei welchem Lieferanten, die genaue Spezifikation und die erklärenden, beschreibenden Texteingaben für den Kunden
- Sodann wird entweder
   - ein Bestellvorschlag (für alle) gemacht
    - manuell eine Bestellung gemacht
    - der Bestellvorschlag über die Zwischenablage aus dem Auftrag befüllt
- die Bestellung(en)
    - gehen an den richtigen Lieferanten
    - es sind darin die im Angebot definierten Einkaufspreise enthalten
    - Es steht darin die genaue Beschreibung, so dass die individuelle Konfiguration auch so beim Lieferanten bestellt wird. Dafür ist das Bestellformular entsprechend für den Andruck der Texteingaben anzupassen.

#### Kann man eine Handeingabe in einen Artikel umwandeln?
Ja, mit Dem Menüpunkts Bearbeiten - Handartikel in Artikel umwandeln![](handartikel_in_artikel_umwandeln.png). Um zum Beispiel nach Erteilung des Auftrags Handeingaben in Artikel umzuwandeln, verwenden Sie diesen Menüpunkt. Nach Klick auf den Menüpunkt, erfolgt eine Abfrage, welche Artikelnummer Sie für den Artikel verwenden wollen, hier können Sie auch einfach eine neue Artikelnummer generieren.
![](gewuenschte_artikelnummer.JPG)
Mit Klick auf OK bestätigen Sie die Artikelnummer, ein neuer Artikel wird angelegt und im Angebot hinterlegt. Bitte achten Sie darauf, dass die Umwandlung nur im Belegstatus "Offen" möglich ist.

#### Können die Preise entsprechend der **Kieselstein ERP**-Preisfindung aktualisiert werden?
Ja, mit einem Klick auf das Taschenrechner-Icon ![](rechnung_klein.JPG) werden die Preise im Angebot entsprechend der Preisfindung aktualisiert. Das bedeutet das VK-Preislisten, Kundensonderkonditionen, Mengenstaffeln etc. berücksichtigt werden. Bitte beachten Sie, dass manuelle Eingaben z.B. von Fixpreisen überschrieben werden.

#### Kann der Angebotswert als gesamtes übersteuert werden?
Es kommt immer wieder vor, dass für ein Angebot ein pauschaler Betrag vereinbart wird. [Details siehe bitte]( {{<relref "/verkauf/auftrag/#kann-der-auftragswert-als-gesamtes-%c3%bcbersteuert-werden" >}} )

#### Ein Angebot muss immer von zwei Personen unterschrieben werden
Um dieses Verhalten zu erreichen, stellen Sie bitte den Parameter ZWEITER_VERTRETER = 1\. Zusätzlich muss das Formular des Angebotes entsprechend erweitert werden, damit auch die Daten des zweiten Vertreters so wie in Ihrer Firmenpolicy definiert angedruckt werden.

#### Kann man einem Angebot einen zusätzlichen Status geben?
In manchen Unternehmen sollten zusätzlich zu den üblichen Angebotsstatus auch der aktuelle gefühlte Status des Angebotes, in der Regel in Kombination mit den Akquisedaten, erfasst werden. Gehen Sie für die Definition der Angebots-Zusatzstatus wie folgt vor:
- a.) Hinterlegen Sie die gewünschten Zusatzstatus unter Grunddaten, Akquisestatus. Verwenden Sie dafür kurze Begriffe (maximal 15Zeichen)
- b.) wechseln Sie nun in das Modul System, unterer Modulreiter Sprache, oberer Modulreiter Status und ergänzen Sie die neuen (Zusatz-)Status um eine Langbeschreibung und um ein für Sie aussagekräftiges Icon mit 16x16Pixel
- c.) Starten Sie nun Ihrem **Kieselstein ERP** Client neu.

#### Journal offene Angebote
Der Stichtag im Journal offene Angebote wirkt in der Form, dass der erfasste Nachfasstermin der Angebote herangezogen wird. D.h. es werden alle Angebot mit dem Status Offen und einem Nachfasstermin vor bzw. zum Stichtag angezeigt. Hintergrund: Immerhin will man sich ja um die aktuell offenen Angebote kümmern und nicht die bei denen man mit dem Kunden vereinbart hatte, dass man sich erst in drei Wochen meldet.
Von vielen Anwendern wird für die laufende Arbeit mit **Kieselstein ERP** die Auswahlliste verwendet. D.h. es wird über Ansicht der Filter auf nur Meine offenen Angebote gestellt<br>
![](Ansicht_Angebote.gif)

und dann zusätzlich die Sortierung nach Nachfasstermin eingerichtet.<br>
![](Sortierung_nach_Nachfasstermin.gif)

Zusätzlich können natürlich die weiteren Direktfilter wie Kunde etc. verwendet werden.

Der besondere Vorteil dieser Arbeitsweise ist, dass Sie über die Kopfdaten einerseits direkt in den Kunden wechseln können um so z.B. die Telefonnummer direkt zu wählen und andererseits die Akquisedaten direkt erfassen können. Somit hat man direkt, ohne den Umweg über das Journal die Übersicht über seine offenen Angebote und kann gleich mit dem Telefonat und der Erfassung der Akquisedaten arbeiten.