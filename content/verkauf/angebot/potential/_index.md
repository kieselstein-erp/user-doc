---
title: "Angebotspotential"
linkTitle: "Potential"
categories: ["Angebot"]
tags: ["Potential"]
weight: 100
description: >
  Angebotspotential darstellen
---

## Darstellen des Angebotspotential
Mit dem Journal Angebotspotential kann über alle offenen Angebote (angelegte Angebote werden nicht berücksichtigt) eine Auswertung dargestellt werden, um deinen Lieferanten den möglichen Umsatz aufgrund der von dir an deine Kunden gelegten Angebote darstellen zu können.

Für diese Liste wir der bevorzugte Lieferant des jeweiligen Artikels herangezogen.
Voraussetzung ist, dass ein entsprechender Einkaufspreis hinterlegt ist.

Die Auswertung findest du unter Angebot, Journal, Angebotsjournal
