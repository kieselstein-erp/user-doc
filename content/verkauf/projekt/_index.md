---
title: "Projekt"
linkTitle: "Projekt"
weight: 20
description: >
  Projekte als Akquise Instrument, als Ticketsystem, als Klammer über komplexe Sales-Task, Verkaufstrichter, ev. eigene Forecastplanung
---
Die Projektverwaltung einerseits als initiales Instrument und andererseits als Klammer über den gesamten Verkaufsvorgang.

Mit dem Modul Projekte oder auch ToDo Liste genannt, steht Ihnen eine Projektliste bzw. ToDo Liste zur Verfügung.<br>
In den Grunddaten können die Auswahlmöglichkeiten an Ihre Bedürfnisse angepasst werden.

Die Projekt-Verwaltung kann sowohl für eine einfache Projektplanung als auch als einfaches aber doch sehr praktisches CIC-Tool (Customer Interaction Center) verwendet werden.<br>
Für eine komfortable Projektübersicht steht auch die zusätzliche Funktion [Projektklammer]( {{<relref "/verkauf/projekt/projektklammer">}} ) zur Verfügung.

Grundeinstellungen der Projekte
| Modulreiter | Beschreibung |
| --- |  --- |
| Bereich | Grundsätzliche Aufteilung der Projektverwaltung in mehrere Bereiche, wie z.B. Sales, Support, Development. Bitte achte darauf, dass dies **wenige** Bereiche sein sollten |
| Kategorie | Die Gruppierung / Unterteilung Ihrer Projekte z.B. für die Vertriebsbereiche |
| Typ | Welche Form von Projekt ist es, wie ist das Projekt zu behandeln. Z.B. Infogespräch, Kaltakquise, Produktpräsentation |
| Status | Welche Status können Ihre Projekte haben. Beachten Sie hier bitte besonders den Status erledigt. Dieser wird eigens protokolliert und ausgewertet. |

Von der Idee her ist es so, dass jedes Projekt einen Status und einen Termin ev. mit Uhrzeit hat. Der beginnende Status ist Angelegt, der endende Status ist erledigt, was auch extra protokolliert wird. (Siehe Grunddaten) <br>
Der Status offen, signalisiert den Zustand dazwischen. Die anderen Status sind für den Vertrieb nicht direkt relevant. Der Status getestet ist von der Logik her nach erledigt angesiedelt.
In den Grunddaten können Sie definieren, welche Status als erledigt gelten. Setzen Sie dazu den Haken bei ![](erledigt_status_pj.JPG) Erledigt.<br>
In der Auswahl der Projekte finden Sie im Menüpunkt Ansicht die Möglichkeit nur Ihre offenen Projekte anzusehen. Durch Klick auf Termin sind diese entsprechend nach dem geplanten Termin sortiert.<br>
In den Zusatzfiltern finden Sie, neben den Auswahlmöglichkeiten FÜR (= welcher Mitarbeiter sollte sich darum kümmern) auch die Möglichkeit dies nach LKZ (=Länderkennzeichen) und PLZ (=Postleitzahl) einzuschränken.<br>
Wir verwenden dies gerne um gezielt gewissen Gebiete, z.B. für Rundreisen, herauszufiltern.

Bitte beachten Sie, es können keine Projekte bebucht werden, die storniert sind, bzw. können keine Projekte storniert werden, die Zeiten beinhalten.<br>
In der Quickzeiterfassung werden stornierte Projekte nicht angezeigt.

#### Wieso sind Erzeuger, Mitarbeiter und Partner Pflichtfelder?
Erzeuger, also wer hat das Projekt aufgenommen, ist üblicherweise der Benutzer und dient Ihrer Information wer ursächlich mit dem Partner gesprochen hat.<br>
Mitarbeiter und Partner sind ganz bewusst auf Pflichtfeld gesetzt, da eine Aufgabe immer einem Mitarbeiter zugeordnet werden muss, sonst ist Keiner dafür zuständig und es wird das Projekt nicht bearbeitet. Wir haben auch bei unserer eigenen Anwendung festgestellt, dass es sehr positiv ist, wenn man den Partner, üblicherweise den Kunden, angibt für den das Projekt ist. Gibt es wirklich mal keinen Kunden, so setzen wir es auf unser eigenes Unternehmen.<br>
Um aus Projekten wirklich auch etwas zu machen, muss es auch einen Zuständigen dafür geben, sonst ist keiner verantwortlich. Sollte ausnahmsweise im Moment keiner Ihrer Mitarbeiter dafür zuständig sein, so müssen Sie trotzdem jemanden definieren, der sich darum kümmert. Sonst wird dieser Job nicht erledigt.

#### Zieltermin
Der Zieltermin wird automatisch mit einem Termin in der Zukunft vorbesetzt. Wie weit dies in der Zukunft sein sollte kann unter System, Parameter, PROJEKT_OFFSET_ZIELTERMIN eingestellt werden.
Zusätzlich kann dieser Termin abhängig von der Historyart mit dem im Projektdetail angegebenen Termin automatisch übersteuert werden.

#### Wozu Priorität
In aller Regel gibt es mehr oder weniger dringende / wichtige Projekte. Wir haben daher fünf Prioritätsstufen vorgesehen, wobei automatisch die mittlere Priorität (3) bei neuen Projekten vorgeschlagen wird.

#### Umsatz geplant
Erfassen Sie hier den durch dieses Projekt geplanten Umsatz in Ihrer Mandantenwährung.

#### Wahrscheinlichkeit
Erfasse hier das Potential oder auch die Umsatzwahrscheinlichkeit dieses Projektes, also die Bauchzahl deines Verkaufs.

#### Verkaufsfortschritt
Um die Entwicklung des Verkaufsfortschritts zu sehen, stellen Sie bitte den Parameter PROJEKT_MIT_UMSATZ auf den Wert 1 und definieren Sie bitte in den Grunddaten Ihre Verkaufsfortschrittsstufen. Bewährt hat sich, dass bis zu 10 Stufen für die Darstellung des Vertriebstrichters verwendet werden. Siehe dazu bitte auch Abbildung der [vertrieblichen Steuerung](#steuerung-des-vertriebs-mit-dem-projektmodul-von-kieselstein-erp).

#### Schätzung in Stunden
Erfassen Sie den geschätzten Zeitaufwand um das Projekt zu realisieren.

#### Verrechenbar
Kann der Zeitaufwand auch an den Partner verrechnet werden?<br>
D.h. das Verrechenbar hat keine wirkliche Programmfunktion sondern die Info, ob man diese Entwicklung / Konstruktion an den Kunden verrechnen kann oder nicht.<br>
Um dies eindeutig definieren zu können, hat es drei Status. Verrechenbar, nicht verrechenbar und nicht definiert.

#### Freigegeben
Ist das Projekt für die Realisierung bereits freigegeben?<br>
Freigegeben hat in diesem Sinne keine Programmfunktion, sondern dient Ihrer internen Information. Z.B. darf auf dieses Kundenprojekt schon gearbeitet werden oder eben nicht.

**Info:**<br>
Um Projekte nur an ausgewählte Personen freizugeben, siehe bitte Benutzerrecht PROJ_DARF_ALLE_SEHEN. D.h. hat ein Benutzer dieses Recht nicht, sieht er nur die Projekte bei denen er der zugewiesene Mitarbeiter oder einer der Techniker ist.

#### Können neue Projekte per Drag & Drop angelegt werden?
Wenn du aus deinem EMail Posteingang EMails direkt als Projekt neu angelegen möchtest, so ziehe bitte das EMail auf die Projekt-Auswahlliste und lasse es dort fallen. Damit wird ein neues Projekt angelegt.<br>
Es wird aus dem EMail-Absender erkannt, welcher Partner und welcher Ansprechpartner dies ist und damit das Projekt entsprechend vorbesetzt.<br>
Von der Erfassung her stehen Sie damit im Projekt neu, mit den vorbesetzten Daten. Der textliche Inhalt des EMails ist im Projekt-Kopf-Text eingetragen. Zusätzlich wird das EMail in der Dokumentenablage abgelegt.

Um ein weiteres Detail per EMail Drag & Drop zum gleichen Projekt hinzuzufügen wählen Sie zuerst das Projekt aus und gehen in den Reiter 3 bzw. 4 Details.<br>
Hier gehen Sie in gleicher Weise vor. Der textliche Inhalt des EMails wird in den Textinhalt des Details eingetragen. Das EMail selbst wird in der Dokumentenablage abgelegt.

Es ist damit das Projekt / das Projektdetail vorbesetzt. Durch klick auf Speichern wird der Inhalt abgespeichert. Wird das Speichern abgebrochen, so werden auch keine Einträge in der Dokumentenablage gemacht.

#### In der Projektauswahlliste sollte Umsatz und Wahrscheinlichkeit angezeigt werden.
Stellen Sie unter System, Parameter, PROJEKT_MIT_UMSATZ den Wert auf 1 und starten Sie das Projektmodul neu.

##### Projekt-Forecast
[siehe](#projekt-forecast)

#### Kann ich nur meine offenen Projekte sehen
Wählen Sie unter Ansicht Ihre gewünschte Darstellung.

#### Welche Daten werden für meine offenen Projekte berücksichtigt
Für die Entscheidung ob ein offenes Projekt Ihnen unter meine offenen Projekte angezeigt wird, werden die Zuordnungen unter
- Zugewiesener ... Mitarbeiter
- wird durchgeführt von ... im Detail
- Techniker

herangezogen. Sobald der angemeldete Benutzer in einem der drei Personenzuordnungen hinterlegt ist, wird das Projekt unter seine offenen aufgeführt. Der im Reiter Detail mögliche Erledigungsstatus wird derzeit nicht berücksichtigt.

**HINWEIS:**<br>
Ist der Arbeitsplatzparameter PROJEKTAUSWAHL_SCHNELL aktiviert, so greift diese Zuordnung nicht. D.h. wenn für dich obige Listung wichtig ist, so muss dieser Arbeitsplatzparameter deaktiviert sein.

#### Es sollten nur ausgewählte Mitarbeiter das jeweilige Projekt sehen
Um die Sichtbarkeit auf die Projekte einzuschränken dürfen das Recht
PROJ_DARF_ALLE_SEHEN für diese Rolle nicht vergeben werden. Damit wird die Sichtbarkeit auf die oben (offene Projekte) beschriebenen Anzeigen eingeschränkt.<br>
Beachten Sie bitte, dass wenn Sie dieser Rolle das Recht, PROJ_PROJ_CUD, also schreiben im Projekt (Kopf) geben, so können die Mitglieder dieser Systemrolle neue Projekte anlegen und sehen damit alle angelegten Partner.<br>
In der Regel wird dies so verwendet werden, dass die Projektleiter auch neue Projekte anlegen dürfen, die reinen Projektmitarbeiter dürfen nur ihre Projekte sehen und an den Kopfdaten der Projekte nichts verändern.

#### Was bedeuten die Farben in der Projektauswahlliste?
In der Projektauswahlliste wird mit Farben die Zeit und der Queue Status signalisiert.
So bedeutet grün, dass auf dem jeweiligen Projekt bereits Zeiten gebucht sind.
Violett bedeutet, dass dieses Projekt in der Queue (Arbeitsvorratsliste) des zugewiesenen Mitarbeiters eingetragen ist.<br>
Orange bedeutet, dass auf dieses Projekt bereits Zeiten gebucht sind und dieses Projekt auch in der Queue eingetragen ist.

#### Kann ein Projekt auch mit Artikel(n) verbunden werden?
Ja. Definieren Sie dafür in den Grunddaten den gewünschten Bereich entsprechend.<br>
![](Projekt_Bereich.gif)<br>
D.h. haken Sie dafür bitte Projekt mit Artikel an.<br>
Definieren Sie weiters ob es je Artikel nur ein Projekt geben darf und ob in diesem Bereich für jedes Projekt ein Artikel angegeben werden muss (Pflichtfeld).<br>
**Hinweis:**<br>
Aus Sicht der Artikel werden die Projekte eines Artikels unter Info, Projekt aufgelistet.
Um in der Artikelauswahlliste auch anzuzeigen, ob es für diesen Artikel auch Projekte gibt, haken Sie bitte bei beim gewünschten Projektstatus (als) Sperre im Artikel anzeigen an.

#### Ich möchte nur eine Kategorie sehen
Verwenden Sie den Zusatzfilter ![](Projekt1.gif), wählen Sie hier die Definition einer Kategorie und geben Sie nach dem Klick auf das Plus die gewünschte Kategorie an. Hier steht auch die Möglichkeit der Filterung nach Länder (LKZ) bzw. Postleitzahlen (PLZ) zur Verfügung um z.B. Vertriebsaktionen nur für ein bestimmtes Gebiet durchzuführen.

Hinweis: Es sind alle Filtermöglichkeiten durch ein logisches Und kombinierbar, also Direktfilter, Ansicht(sfilter) und Zusatzfilter. Damit können Sie die Darstellung rasch auf Ihre gewünschten Daten einschränken.

#### Sortierung der Anzeige
Zusätzlich können in der Projektverwaltung die Spalten kombiniert sortiert werden. D.h. durch den Klick auf den ersten Spaltenknopf kann z.B. nach dem Typ sortiert werden. Halten Sie nun die Strg Taste gedrückt und Klicken Sie auf z.B. den Termin, danach auf den Ort, so wird die Darstellung genau in dieser kombinierten Sortierung angezeigt. Sollte nun mit der Sortierung von vorne begonnen werden, so genügt ein Klick ohne die Strg Taste gedrückt zu halten.

### Aus dem Projekt Angebot / Auftrag neu anlegen
Wenn die Zusatzfunktion [Projektklammer]( {{<relref "/verkauf/projekt/projektklammer">}} ) genutzt wird, gibt es in den Projekt-Kopfdaten 2 Symbole, mit denen Aufträge ![](auftrag_neu.PNG) / Angebote ![](angebot_neu.PNG) angelegt werden können.

#### Details / Meilensteine
Hier kann die Projekt History hinterlegt werden. Sie sehen hier auch, wann hat wer welche zusätzlichen Anmerkungen hinzugefügt. Die Definition der Historyarten erfolgt in den Grunddaten des Projektes. Wählen Sie hier auch entsprechende Farben, um sofort eine farbliche Hervorhebung der wichtigen Details zu einem Projekt zu sehen.<br>
Um, in Verbindung mit der Projektklammer eine detailliertere Projektplanung zu ermöglichen betrachten Sie die Details bitte als Meilensteine. Siehe dazu auch [zeitlicher Verlauf](#projekt-zeitliche-übersicht) und [Gantt-Diagramm](#projektplanung--gantt-diagramm)

#### können Termine automatisch verschoben werden?
Ja, abhängig von der Definition der History Art kann auch automatisch der Zieltermin mitverschoben werden.<br>
![](Zieltermin_aktualisieren.gif)<br>
D.h. wenn hier Zieltermin aktualisieren angehakt ist, so wird der Termin des Details / der Historyart beim Speichern des Details aktualisiert.<br>
![](Zieltermin.gif)<br>
Beachten Sie dazu bitte auch den Parameter NACHFASSTERMIN_AUS_PROJEKT_AKTUALISIEREN.<br>
Ist dieser auf 1 gestellt und ist der aktuell bearbeitete Detaileintrag der jüngste, also der oberste, so werden für die Angebote die mit dem Projekt direkt verbunden sind und im Status offen sind, automatisch der jeweilige Nachfasstermin auf den Zieltermin des Projektes gesetzt.

#### Können Details wieder gelöscht werden?
Ja, durch Klick auf ![](Details_loeschen.gif) löschen, können die selbst erstellten Projektdetails wieder entfernt werden.<br>
Die Details anderer Benutzer können nur von diesen selbst entfernt werden.

#### Grunddaten
In den Grunddaten können Kategorie, Typ, Status, usw. definiert werden.

Da sich für die Anzeige der Status ICONs bewährt haben, können auch für selbst erstellte Projekt-Status ICONs hinterlegt werden. Gehen Sie dazu wie folgt vor:<br>
Definieren Sie im Modul Projekt unter Grunddaten, Status den gewünschten kurzen Begriff, z.B. Potential. Durch das Speichern wird dieser neue Status automatisch in die allgemeine Status Definition mit übernommen. Diese finden Sie unter System, Sprache, Status. Hier kann nun durch ändern, Datei ein Status-Icon für diesen Status definiert werden.<br>
Bitte beachten Sie, dass
- a.) die Icons nur 16x16 Pixel haben dürfen und nur das Dateiformat png
- b.) die Icons in den Auswahllisten erst nach Neustart des Clients angezeigt werden.

#### In den Kopfdaten werden keine Kategorien und keine Typen angezeigt?
Es sind für die gewählte Sprache keine Übersetzungen eingetragen.

Tragen Sie diese unter Projekte, unterer Modulreiter Grunddaten, für Kategorie, Typ, Status entsprechend ein.

#### Wozu Bereiche?
In vielen Anwendungsfällen ist es praktisch die Projekte in verschiedene Bereiche einzuteilen. Z.B. um Tickets, Verkaufsprojekte, Support oder Ähnliches getrennt und doch in einem zu behandeln.

Dafür wurden die Bereiche geschaffen.

Definieren Sie die möglichen Bereiche im Modul Projekt, unterer Modulreiter Grunddaten, Bereich.
Beachten Sie unbedingt die nachfolgende Definition der Zuordnung der Dokumente.

#### Wie ist die Zuordnung zwischen Bereichen und der Dokumentenablage?
![](Projekt_Dokumentenablage.gif)

Für die Projekte steht sowohl in der Auswahl als auch in den Details die Dokumentenablage zur Verfügung.<br>
Um die Trennung zwischen den Bereichen transparent zu machen, wurden die textlichen Bezeichnungen der Bereiche in den Pfad der Dokumentenablage übernommen. Das bedeutet auch, wenn die Texte verändert werden, so können die den Projekten zugeordneten Dokumente nur mehr über die gesamte Dokumentenablage (System) wieder gefunden werden.

#### Journal offene Projekte Stichtag
Der Stichtag bezieht sich auf das Anlage Datum aller Projekte die noch nicht erledigt sind. D.h. alle Projekte die vor dem Stichtag angelegt wurden und noch nicht erledigt sind werden aufgeführt.

#### Journal Aktivitätsübersicht
Das Journal Aktivitätsübersicht verschafft einen schnellen Überblick über die Aktionen des gewählten Zeitraum. Wählen Sie den Zeitraum und die gewünschte Sortierung aus und ob die Gesamtinformation (alle Projekt-Detail-Einträge) angeführt werden soll.

![](Aktivitaetsuebersicht.PNG)

Sie erhalten eine Auswertung  Telefonate / Arbeiten / Aktionen in der letzten Woche gemacht wurden. So können Sie den Ausdruck zum Reporting von Vertriebsprojekten und deren Statuss, Notizen, Termine zum Beispiel verwenden. Je nach Art der Verwendung der Zeiterfassung in Ihrem Unternehmen erhalten Sie eine Art "Chef-Auswertung" aller gebuchten Zeiten und Notizen zu einzelnen Belegen.


## Steuerung des Vertriebs mit dem Projektmodul von **Kieselstein ERP**
<a name="Verkaufssteuerung"></a>
Auch im Vertrieb / im Verkauf ist eine gute Kennzahlen getriebene Steuerung unbedingt erforderlich. Auch wenn manche Verkäufer der Auffassung sein sollten, dass sie das alles im Kopf / im Bauch haben. Lassen Sie sich davon nicht beeindrucken. Ein **guter Verkäufer** ist froh, wenn Sie ihm ein **Werkzeug zur Verfügung** stellen, mit dem er seine umfassenden und manchmal auch komplexen vertrieblichen Aufgaben lösen kann.

### Wie werden die vertrieblichen Projekte in **Kieselstein ERP** abgebildet?
Für uns hat jedes Vertriebsprojekt, welches z.B. ein eigener Bereich sein kann, folgende Definitionen:
- a.) Kategorie -> um welche Module geht es, ev. auch aus welcher Quelle kommt dieser Lead
- b.) Titel -> wählen Sie hier einen für Sie intern wirklich sprechenden Titel, um so in der Auswahlliste sofort eine Aussage machen zu können worum es geht. Vermeiden Sie Wiederholungen die Sie durch Kategorie oder Typ bereits gemacht haben
- c.) Typ -> wie genau ist der Lead zu Ihnen gekommen, z.B. Empfehlung
- d.) Umsatz geplant -> für welchen Umsatz steht dieses Projekt, in vielen Anwendungen auch der potentielle Kunde
- e.) Wahrscheinlichkeit -> Ihr Bauchgefühl, oder auch: Der Auftrag kommt ganz sicher
- f.) Status -> oft auch als NextStep bezeichnet. Was ist denn aktuell gerade zu tun um beim Kunden weiter zu kommen
- g.) Prio -> wie schnell wird sich der Kunde entscheiden? Das bedingt auch dass Sie sich intensiver / häufiger um den Kunden kümmern müssen oder eben nicht
- h.) Verkaufsfortschritt -> Wie entwickelt sich der Kunde im Verkaufstrichter vorwärts. Gerade diese Entwicklung zeigt Ihnen, ob Ihr Verkauf etwas weiter bringt oder nicht. Prüfen Sie die offenen Sales-Projekte jede Woche mindestens jedes Monat und beurteilen Sie die Entwicklung der wichtigen Projekte im Verkaufstrichter. Entspricht dies Ihren Erwartungen oder bleiben diese Entwicklungen hinten. Wenn Sie hinten bleiben, klären Sie die Ursachen ab und schaffen Sie verlässliche Zahlen.

Info: Für die (grafische) Darstellung dieser Entwicklungen kommen unterschiedliche Auswertungen zum Einsatz, welche alle auf Ihr Unternehmen etwas angepasst werden müssen. Wenden Sie sich dazu vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

Mit einer guten Abbildung in den vertrieblichen Projekten haben Sie ein gutes CRM-Tool um Ihre Vertriebsmitarbeiter ganz gezielt zum Erfolg zu steuern.
![](Verkaufstrichter.jpg)
So sehen Sie hier, für verschiedene Verkäufer, deren offene Vertriebsprojekte gereiht nach Verkaufsfortschritt.

![](Verkaufs_Prios.gif)

Eine andere Darstellung zeigt die Einordnung der Projekte in Prioritäten, was den Realisierungszeiträumen entspricht

#### Definition des Verkaufsfortschrittes
Die Phasen des Verkaufsfortschrittes müssen in den Grunddaten hinterlegt werden. Als mögliche Einstellungen, bitte an deine Sales-Denke anpassen, nachfolgender Vorschlag:<br>
![](Verkaufsfortschritt_Vorschlag.png)

#### Wie geht man mit Bereichen, Kategorie, Typ um?
Bereich, Kategorie, Typ in den Projekten, wie verwenden, bzw. was man dazu wissen muss:
-   Bereich:
    Der Bereich ist für eine sehr grobe Unterteilung gedacht. Z.B. Reparatur und Sales. Die haben miteinander nichts zu tun. Und: Jeder Bereich hat seinen eigenen Nummernkreis. D.h. aus der Reparatur gibt es das Projekt 1 und auch aus dem Sales. Ein Verschieben ist zwar technisch möglich, bewirkt aber ein Storno des Projektes im alten Bereich und eine Neuanlage mit Kopie im neuen Bereich. Der Grund sind die eigenen Nummernkreise je Bereich.

-   Kategorie:
    Kategorien sind global je Mandant verfügbar. Gedacht sind sie z.B. für Produktgruppen. D.h. können für die Erfassung der Obergruppen aller Bereiche genutzt werden. Im Sales Bereich wird hier auch gerne die Quelle des Leads eingetragen, also wo der Kontakt her kommt.

-   Typ:
    Hier wird es feiner, also worum geht es denn so eigentlich. Bei der Reparatur könnten hier verschiedene, grobe Fehlerarten stehen, beim Sales könnten ev. weitere Unterscheidungen gemacht werden.

Zusätzlicher Nutzen:<br>
Manchmal arbeiten an einem Projekt mehrere Personen. Diese können im Reiter Techniker als weitere Teilnehmer eingetragen werden. Nutzt man nun nach den Menüpunkt Ansicht **meine offenen** so hat jedeR die für Ihn wichtigen Projekte im jeweiligen Bereich.

#### Projekt Verknüpfung
Es steht auch die Verknüpfung von über und untergeordneten Projekten zur Verfügung.<br>
Damit können Projekte sowohl mit Vater- als auch mit Kind-Projekten verbunden werden (es sind durchaus mehrere Vaterprojekte möglich).<br>
Die jeweilige Verknüpfung eines Projektes wird im Reiter Verknüpfung angezeigt.<br>
D.h. Sie finden in den Projekten nun einen oberen Reiter Verknüpfung.<br>
Der Projektbaum in dem sich das jeweilige Projekt befindet, kann im Reiter Verknüpfung durch Klick auf das Druckersymbol dargestellt werden. Der Andruck der jeweiligen über bzw. untergeordneten Projekte ist in den jeweiligen Formularen vorbereitet und kann sehr rasch an Ihre Erfordernisse angepasst werden.<br>
![](Projektbaum.jpg)<br>
Das Ausgangsprojekt des Projektbaumes ist in der Baumstruktur farblich hervorgehoben.

#### Nachtragen von Projekten in Aufträgen, Angeboten
Projekte können in angelegten Aufträgen und Angeboten auch nachgetragen werden. Bitte beachten Sie jedoch, dass das Projekt für einen Auftrag, wenn dieser sich auf ein Angebot bezieht durch das Angebot definiert wird. D.h. wenn ein Auftrag sich auf eine Angebot bezieht, so muss ein Projekt im Angebot hinterlegt werden, damit auch der Auftrag sich auf dieses Angebot bezieht. Aus diesem Grunde wird in der Konstellation auch der Projekt Knopf in Grau (= nicht änderbar) dargestellt.

## Projektstatistik
Wenn bei Ihnen die Funktionalität der Projektklammer zur Verfügung steht, so finden Sie unter Info, Statistik auch eine Projektstatistik.<br>
Diese liefert eine Übersicht über alle Warenbewegungen die sich auf dieses Projekt beziehen.
Die Sortierung ist grundsätzlich nach Artikel und Seriennummer und Belegdatum damit vor allem die Bewegung der Seriennummern auf den Artikel klar ersichtlich ist.<br>
Wird aktueller Stand angehakt, so werden nur Seriennummernartikel gedruckt, deren Gesamtmenge ungleich 0,00 ist. Damit erhalten Sie:
- eine Übersicht über alle Bewegungen der Artikel auf einem Projekt
- die Anzeige über den aktuell beim Kunden für dieses Projekt installierten Seriennummernstand
Zusätzlich können noch Filter auf die verschiedenen Bereiche (Einkauf, Verkauf, Fertigung) eingerichtet werden.

### Projektforecast
Selbstverständlich interessiert das Vertriebscontrolling, also die Steuerung des Vertriebs, welche Umsätze wahrscheinlich in Zukunft gemacht werden. Dafür steht das Journal Projektforecast zur Verfügung. Da dies eine Auswertung für den Vertrieb ist, muss der Parameter PROJEKT_MIT_UMSATZ aktiviert (= auf 1) gestellt sein.<br>
Hiermit erhalten Sie eine Auswertung über alle offenen Projekte mit den Umsätzen und Wahrscheinlichkeiten und einer Prognose welche Umsätze Sie aus heutiger Sicht in den nächsten drei bzw. sechs Monate machen werden.<br>
Stellen Sie dazu die Auswertung auf sortiert nach Personal und geben Sie bitte einen passenden Stichtag für den Realisierungstermin in der Zukunft mit an.<br>
![](Forecast.jpg)<br>
Damit sehen Sie welche Umsätze Sie aus heutiger Sicht in Zukunft machen werden.<br>
Selbstverständlich leben Sie hier von der realistischen Einschätzung des jeweiligen Vertriebsmitarbeiters von Realisierungstermin und Wahrscheinlichkeit.<br>
Bewährt hat sich hier, die Mitarbeiter zu realistischen Schätzungen anzuhalten, aber die persönliche Note jedes Vertriebsmitarbeiters zu belassen. Diese berücksichtigen Sie dann in Ihren persönlichen und optimistischen oder vorsichtigen Planungen.

### Projekt zeitliche Übersicht
<a name="Zeitlicher Verlauf"></a>
Das Modul Projekt wird in Verbindung mit der Projektklammer gerne auch für Planungen der Entwicklung neuer Produkte aber auch als Übersicht für den zeitlichen Verlauf der verschiedensten Projekte verwendet. So ist ein Anwendungsfall, dass es als Maschinenbuch verwendet wird. D.h. für jede Maschine, für jedes Gerät, wird ein Projekt angelegt (siehe dazu auch Betreiber) und z.B. die Seriennummer der Maschine beim Projekttitel eingetragen. Nun werden alle Angebote, Aufträge, Reparaturen (welche in den Losen abgebildet sind) usw. diesem Projekt zugeordnet. Damit erhalten Sie sehr rasch einen Überblick über die Geschichte, die Vergangenheit, die Historie dieses Projektes, also des Gerätes.<br>
Dafür steht für das einzelne Projekt der Reiter Cockpit bzw. der Reiter Projektverlauf mit dem jeweiligen Druck zur Verfügung.<br>
Um einen Überblick über die Geschichte mehrerer Projekte eines Zeitraumes zu bekommen, steht das Journal Alle detailliert zur Verfügung.<br>
Damit erhalten Sie, soweit im definierten Zeitraum darstellbar,<br>
![](ProjektZeitdiagramm.jpg)<br>
- einerseits eine Übersicht über alle Belege die direkt oder indirekt mit dem Projekt verbunden sind und
- andererseits eine Übersicht über den zeitlichen Ablauf jedes einzelnen Projektes.

### Projektplanung / Gantt-Diagramm
<a name="Gantt"></a>
Oft wünscht man sich, dass man, insbesondere für Entwicklungsprojekte ein Diagramm mit Stunden und Terminen bekommt, um eine ungefähre Zeitplanung für den Fortschritt des Entwicklungsprojektes aber auch für die Auslastung seiner Mitarbeiter zu bekommen. Auch dafür kann das Modul Projekt verwendet werden.<br>
Hier werden die Details als Definition der Meilensteine verwendet. Hinterlegen Sie beim jeweiligen Detail = Meilenstein, wer (wird durchgeführt von), wann womit (Titel) beginnen sollte und wie lange (Dauer in Std.) dies dauern wird. Mit dem Journal Journal offene Projekte mit der Reportvariante Gantt erhalten Sie eine entsprechende zeitliche Darstellung wie sich dieser Projektverlauf darstellt.

## Projekt Cockpit
Mit der Zusatzfunktion Projektklammer werden im Projekt Cockpit alle mit dem gewählten Projekt direkt oder indirekt verknüpften Belege angezeigt.<br>
Beim Aufruf des Projektcockpits ist der gesamte Projektbaum eingeklappt. Somit wird in den Themen nur die Wurzel, das gewählte Projekt angezeigt.<br>
![](ProjektCockpitStart.jpg)<br>
Durch Klick auf das Plus bei Projekt wird die nächste Ebene aufgeklappt. Diese wird bereits durch die vorhandenen Daten definiert und so wird z.B.<br>
![](ProjektCockpit1.gif)<br>
angezeigt.

Durch die weitere Öffnung des Baumes werden die Module angezeigt, bei denen entsprechende Daten vorhanden sind.<br>
![](ProjektCockpit2.gif)<br>

So öffnen Sie den Baum der Reihe nach bis die gewünschte Information angezeigt wird.
![](ProjektCockpit3.gif)<br>

Umgekehrt kann durch Klick auf das grüne Plus ![](ProjektCockpit_ausklappen.gif) / respektive das Minus ![](ProjektCockpit_einklappen.gif), der gesamte Baum ausgeklappt oder eingeklappt werden. Das Default-Verhalten (ob aus- oder eingeklappt) kann durch den Arbeitsplatzparameter PROJEKT_COCKPIT_DEFAULT_EINGEKLAPPT definiert werden.

Wenn Sie nun die Dokumente des z.B. Angebotes sehen möchten, so klicken Sie auf die entsprechende Version und es wird im rechten Fenster das abgelegte Dokument des jeweiligen Beleges (soweit es Belege gibt) angezeigt.

Wenn Sie den eigentlichen Beleg ausgewählt haben,
![](ProjektCockpit4.gif)<br>
kann mit dem Klick auf den GoTo Button direkt in das jeweilige Modul gesprungen werden.

Ähnlich ist die Vorgehensweise für die Darstellung der erfassten Telefonzeiten.
![](ProjektCockpit5.jpg)<br>
Auch hier wechseln Sie mit dem GoTo direkt in die Telefonzeiterfassung

## Löschen von leeren Bereichen im Projekt
Will man im Projekt, Grunddaten, Bereich einen Bereich löschen, so kommt, auch bei einem definitiv nicht mehr verwendeten Bereich die Fehlermeldung:<br>
![](Bereich_Loeschen_01.png)<br>
Leider ist hier auch im Fehlermeldungsdetail keine wirkliche Ursache erkennbar.

Die einfachste Vorgehensweise ist:
- In die Auswahlliste des Projektes gehen
- den Bereich auswählen
- Ansicht alle Projekte ![](Bereich_Loeschen_02.png)
- Nun sieht man alle Projekte die auf den Bereich zeigen. Auch diese nicht nur stornieren,sondern anderen Bereichen zuordnen.
