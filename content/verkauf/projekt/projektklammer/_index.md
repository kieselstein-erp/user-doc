---
title: "Projektklammer"
linkTitle: "Projektklammer"
weight: 10
description: >
  Zusammenfassen mehrerer Angebote, Aufträge und deren untergeordnete Module zu einer gemeinsamen Projekt-Betrachtung
---
Projektklammer
==============

Die Projektklammer in **Kieselstein ERP** verbindet von Angebot bis zur Rechnung ein Projekt. So erhalten Sie auf einen Blick schnell Informationen über Vorgänge und Belege des Projekts.
Die Zusatzfunktion Projektklammer ist in das Modul Projekt integriert. In den anderen Modulen (u.a. Angebot, Auftrag, Lieferschein, Rechnung,...) können Sie in den Kopfdaten der Belege ein Projekt auswählen. Dadurch erzeugen Sie die Verbindung des einzelnen Beleges zum Gesamtprojekt. 

#### Wie erstelle ich die Verknüpfung von Belegen mit einem Projekt?
Dazu gibt es mehrere Vorgehensweisen: 

1.  Sie legen im Modul Projekte ein neues Projekt an und erzeugen in den anderen Belegen (z.B. Auftrag) einen neuen Auftrag aus Projekt ![](neu_aus_projekt.PNG).

2.  Sie erstellen die Verknüpfung direkt in vorhandenen Belegen: Verwenden Sie hierzu den Button Projekt in den Kopfdaten des Beleges und wählen aus der Liste das gewünschte Projekt aus. Die Möglichkeit per Hand einen Projektnamen einzugeben, der auf den Belegen wie bisher angedruckt wird, besteht weiterhin.<br>
Zur Anpassung der Ausdrucke bei der Verwendung der Projektklammer wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

3.  Erzeugen Sie Angebot bzw. Auftrag direkt aus dem Projekt bzw. aus dem Projektverlauf.<br>    Wird im Projekt Kopf auf Angebot ![](angebot_neu.PNG) geklickt, so wird sofort ein Angebot mit den Projektdaten und der direkten Zuordnung zum Projekt angelegt.<br>
Klicken Sie auf Auftrag ![](auftrag_neu.PNG) so wird ein Auftrag mit den Projektdaten und der direkten Zuordnung zum Projekt angelegt. Beachten Sie dabei bitte, dass ein über diesen Weg angelegter Auftrag im Projektverlauf direkt unter dem Projekt angeführt wird, da er keinen Vorgänger (aus der Projekt selbst hat).

4.  Aus dem Reiter Projektverlauf wählen Sie ein Angebot aus und klicken auf Auftrag ![](auftrag_neu.PNG). Damit wird ein Auftrag anhand des Angebotes angelegt, wobei der Vorgänger des Auftrags das Angebot ist. In diesem Fall wird das komplette Angebot kopiert inkl. Angebotspositionen. Bitte beachten Sie, dass auch die Alternativ Positionen übernommen werden.

5.  Auch in der Telefonzeiterfassung kann neben dem Partner das Projekt ausgewählt werden.

Bitte beachten Sie: Wenn zum Beispiel ein Auftrag aus einem Angebot erzeugt wurde, welches ein Projekt hinterlegt hat, wird das Projekt des Angebots verwendet. In den Kopfdaten der einzelnen Belege kann die Projektnummer nur geändert werden, wenn kein Vorgänger vorhanden ist. Da in diesem Fall das Projekt des Vorgängers verwendet wird.

#### Wo kann der Ablauf bzw. die Entwicklung des Projekts überprüft werden?
Im Modul Projekte finden Sie den Reiter Projektverlauf ![](reiter_projektverlauf.PNG).
Hier werden alle verknüpften Belege mit deren Status dargestellt. Klicken Sie auf den Beleg und anschließend auf den Button ![](goto_projekt.PNG) um direkt zu diesem Beleg zu wechseln.
Durch Klick auf das Druckersymbol ![](drucken.gif) gelangen Sie in die detaillierte Projekt-Nachkalkulation über alle mit diesem Projekt verbundenen Belege.
Im Reiter Istzeitdaten finden Sie ergänzend einen Ausdruck ![](drucken.gif) aller Zeiten des Projekts, der je nach Bedarf nach Mitarbeiter und Beleg sortiert werden kann.

#### Gibt es eine Möglichkeit zur Nachkalkulation des gesamten Projekts?
Wie oben beschrieben finden Sie im Projekt die Funktion Projektverlauf. Durch Klick auf das Druckersymbol, wird die Gesamt-Nachkalkulation des Projektes aufgerufen. In dieser werden alle an einem Projekt beteiligten Daten abgebildet. Besonders bei Projekten mit mehreren Aufträgen und langer Laufzeit erkennen Sie somit auf einem Blick welche Projekte mit welchen Partnern erfolgreich abgewickelt werden konnten. Auch während der Laufzeit haben Sie dadurch die Möglichkeit Abweichungen zu erkennen und vor dem Abschluss optimierend einzugreifen.

#### Gibt es Unterschiede zwischen der optischen Anzeige des Projektverlaufes und der Nachkalkulation?
Wie schon die Bezeichnung darauf hinweist, ist die optische Anzeige des Projektverlaufes für eine schnelle Übersicht über die Belegzusammenhänge gedacht.
So werden in dieser Übersicht auch die Werte des jeweiligen Beleges und deren Status angezeigt.
![](Projektverlauf.jpg)
Für eine detaillierte Auswertung mit Zeiten und z.B. Teillieferungen oder abweichenden Werten z.B. beim Wareneingang aber auch in der Verrechnung verwenden Sie bitte die Nachkalkulation.

#### Was ist das Projektcockpit?
Im Modul Projekte finden Sie den Reiter Cockpit ![](cockpit_PJ.JPG). Das Cockpit wird dazu verwendet, einen schnellen Überblick über die Dokumente und Einträge des Projekts zu erhalten.

Das Projektcockpit ist zweigeteilt in Themen und Chronologie. Im Bereich Themen finden Sie ähnlich der Dokumentenablage unterteilt in Einkauf, Verkauf, Auftrag, Lieferschein, Rechnung, Fertigung. Hier finden Sie die einzelnen Belege und die jeweiligen Versionen dazu und können diese im rechten Bereich des Fensters anzeigen und mittels dem GoToButton ![](goto_projekt.PNG) zu dem Beleg wechseln.

![](Chronologie_Projektcockpit.JPG)

Im Bereich Chronologie werden die jeweiligen Belege und Detaileinträge nach zeitlichem Verlauf angeführt. Auch hier können Sie die einzelnen Belege bzw. Einträge anzeigen.

Bitte achten Sie hier darauf, dass je nach Benutzerberechtigung Sicherheitsstufen der Dokumente berücksichtigt werden, das bedeutet, wenn ein Benutzer eine gewisse Sicherheitsstufe nicht aufrufen darf, so werden diese im Cockpit nicht angeführt.

Auftrag aus vorhandenem Angebot anlegen

Im Projektverlauf gibt es das Symbol ![](auftrag_neu.PNG), mit diesem ein Auftrag aus einem dem Projekt zugehörigem Angebot erstellt werden kann. 

Wählen Sie durch Anklicken der Zeile im Projektverlauf das Angebot aus und klicken auf das Icon. Nun wird ein neuer Auftrag aus dem Angebot mit Projektbezug angelegt.

**Anpassung an individuelle Anforderungen Ihres Unternehmens:**

Die Definition von Eigenschaften (vergleichbar mit Artikeleigenschaften) ermöglicht eine individuelle Anpassung der Felder an die Bedürfnisse in Ihrem Unternehmen.

Projektnummer als Pflichtfeld in den VK-Belegen definieren

Wenn der Parameter PROJEKT_IST_PFLICHTFELD gesetzt ist, so ist die Projektnummer ein Pflichtfeld in den Belegen (Angebot, Auftrag, Lieferschein, Rechnung, Gutschrift).

#### Wie findet man die Lose eines Projekts? 
Wenn die Zusatzfunktion Projektklammer vorhanden ist, kann in der Los-Auswahlliste anstatt der Auftragsnummer nach der Projektnummer gesucht werden.

Wenn Sie einem Los ein Projekt zugeordnet haben, so wird das Los im Projektverlauf angeführt.

#### Kann der Ansprechpartner aus dem Projekt auch auf der Rechnung angedruckt werden? 
Ja, für etwaige Rückfragen kann auf der Rechnung der Ansprechpartner aus dem Projekt angedruckt werden. Zur Anpassung der jeweiligen Reports wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.
