---
title: "Rechnung"
linkTitle: "Rechnung"
weight: 70
description: >
 Rechnung, Sammelrechnung, Anzahlungs- und Schlussrechnung, Gutschriften, Mahnwesen
---
Rechnung
========

Statusübergänge
![](statusuebergang.png)

Mit dem Modul Rechnung, mit dem exakter Weise die Ausgangsrechnungen gemeint sind, erstellen und verwalten Sie ihre ausgehenden Rechnungen.

Seien es nun Sammelrechnungen, [Anzahlungsrechnungen]( {{<relref "/verkauf/rechnung/anzahlung_und_schlussrechnung/">}} ), [Teilrechnungen]( {{<relref "/verkauf/rechnung/anzahlung_und_schlussrechnung/#teilrechnung">}} ), [Schlussrechnungen]( {{<relref "/verkauf/rechnung/anzahlung_und_schlussrechnung/#anlegen-und-abrechnen-einer-schlussrechnung">}} ), Reparaturrechnungen, Barverkaufsrechnungen usw.

Siehe auch Ermittlung der [Verkaufspreise]( {{<relref "/docs/stammdaten/artikel/verkaufspreis/">}} )

Verwendung der [Rabatte und Preise]( {{<relref "./preise_in_der_rechnung">}} ).

Anlegen einer Rechnung

Um eine neue Rechnung anzulegen gehen Sie bitte wie folgt vor:<br>
Klicken Sie auf das Modul Rechnung ![](Rechnung.JPG) in der nun offenen Auswahl klicken Sie auf Daten aus einem bestehenden Angebot ![](angebot_neu.PNG), Auftrag ![](auftrag_neu.PNG), Lieferschein ![](aus_lieferschein.JPG) oder aus einer bestehenden ![](rechnung_klein.JPG) Rechnung übernehmen.
Wenn Sie eine Bestellung ohne Angebots-/Lieferschein-Bezug erstellen wollen, so klicken Sie auf Neu ![](neu.gif).
Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den Kunden, Projekt, Bestellnummer, Termine - je nach Bedarf.
Mit einem Klick auf Speichern ![](speichern.JPG) bestätigen Sie die Eingaben.
Wechseln Sie in den Reiter Positionen und geben hier die Positionen der Rechnung ein.
Information zur Vorgehensweise beim Einfügen von [Lieferscheinen](#aus Lieferschein)
Um eine neue Position hinzuzufügen klicken Sie auf ![](neu.gif) Neu wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten]( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#positionsarten">}} )), geben die Informationen (u.a. Menge) ein und klicken auf ![](speichern.JPG) Speichern. Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.

Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Konditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für diese eine Rechnung ab.
Mit Klick auf ![](drucken.JPG) Drucken (oder oberes Menü Rechnung - Drucken...) erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG), dem Versenden per E-Mail ![](e-mail.jpg)oder speichern ![](speichern.JPG), aktivieren Sie die Rechnung, der Status (siehe oben) ändert sich von angelegt in offen. Bitte beachten Sie gegebenenfalls auch den Parameter GROSSE_DRUCKVORSCHAU_AKTIVIERT. Ist dieser abgeschaltet (= 0) so wirkt der Klick auf die große Druckvorschau nicht als Aktivierung. Vielmehr muss in dieser Einstellung der Beleg mit den anderen Aktivierungs-Ausdrucken aktiviert werden.

Zum Thema [elektronische Rechnung siehe bitte]( {{<relref "./elektronische_rechnung">}} ).

Rechnung

Im unteren Modulreiter ![](Rechnung.gif) erfassen Sie Ihre Rechnungen inkl. Zahlungserfassung und Umsatzübersicht.

[Gutschrift]( {{<relref "./gutschrift">}} )

Im unteren Modulreiter ![](Gutschrift.gif) erfassen Sie die Gutschriften. [Zu Gutschrift siehe bitte]( {{<relref "./gutschrift">}} ).

Kunden-Eingangsgutschrift

Für die Behandlung von Gutschriften aufgrund von Warenlieferungen an Ihre Kunden [siehe]( {{<relref "/verkauf/rechnung/kunden_eingangsgutschrift">}} ).

Proforma-Rechnung

[Siehe]( {{<relref "./proformarechnung">}} ).

Mahnwesen

Im unteren Modulreiter ![](AR_Mahnwesen.gif) steuern Sie die Mahnungen / Mahnläufe der Rechnungen. [Siehe dazu]( {{<relref "./mahnen">}} ).

Abrechnungsvorschlag

Für den [Abrechnungsvorschlag]( {{<relref "/verkauf/rechnung/abrechnungsvorschlag">}} ) von Zeiten, auftragsbezogenen Eingangsrechnungen und Reisekosten [siehe]( {{<relref "/verkauf/rechnung/abrechnungsvorschlag">}} ).

Zeitnachweis

Für den [Zeitnachweis siehe]( {{<relref "./zeitnachweis">}} ).

<a name="Grunddaten"></a>Grunddaten

Im unteren Modulreiter ![](AR_Grunddaten.gif) definieren Sie das Verhalten und die Texte der Rechnungen.

Hier finden Sie:

![](Rechnungstext.gif)

Unter Rechnungstext definieren Sie die Kopf und Fußtexte der Rechnungen, jeweils für den beim Einstieg ausgewählten Mandanten.

![](Mahntext.gif)

Hier definieren Sie die Mahntexte passend zu den jeweiligen Mahnstufen.

![](Rechnungsart.gif)

Hier finden Sie die Liste der möglichen Rechnungsarten, vor allem gedacht um die Übersetzungen für fremdsprachige Clients zu pflegen.

![](Zahlungsart.gif)

Hier sehen Sie die der möglichen Zahlungsarten. Auch dies gedacht um die Übersetzungen für fremdsprachige Clients zu definieren.

![](Mahnstufe.gif)

Definieren Sie die möglichen Mahnstufen und die Abstände zwischen den Mahnungen.
Die Mahntage für die Mahnstufe eins bedeuten, dass ab Fälligkeit der Rechnung noch z.B. 7 Tage (die Anzahl der angegebenen Tage) dazugerechnet werden. Erst wenn dieses Datum beim Mahnlauf erreicht ist, wird die Rechnung gemahnt.<br>
Die Tage der Mahnstufe 2 bedeuten, dass nach dem Versenden der ersten Mahnung weitere 14 Tage (die Anzahl der definierten Tage) verstreichen müssen, bis die Rechnung ein zweites Mal gemahnt wird.<br>
Sie können eine beliebige Anzahl an Mahnstufen definieren. Beim Überschreiten der letzten Mahnstufe, ... Tage erhalten Sie eine Meldung, dass diese Rechnung nun von **Kieselstein ERP** nicht mehr weiter gemahnt wird. Üblicherweise übergeben Sie in diesem Falle die Rechnung Ihrem Rechtsanwalt, Ihrem Inkassobüro.<br>
Bitte denken Sie aber daran, dass die Rechnung weiterhin als offen betrachtet wird. Im Falle einer Insolvenz Ihres Kunden, muss die Rechnung trotzdem ausgebucht werden. Auch wird der durch die Rechnung ausgelöste Umsatz weiterhin mitgerechnet.<br>
Können Sie, z.B. aufgrund von Eigentumsvorbehalt oder ähnlichem Ware vom insolventen Kunden zurückholen, so muss diese wieder über eine Gutschrift eingebucht werden, was sich dann entsprechend auf die Umsatzstatistiken auswirkt.

[Zahlungen:]( {{<relref "./zahlung">}} )

Rechnungsübersicht / offene Rechnungen:

Da in der Rechnungsverwaltung der Begriff "offene Rechnung" unter Umständen Verwirrung stiften könnte und da er anders verwendet wird als der Status in erster Betrachtung annehmen / vermuten lässt hier die Definition dazu:

Der Begriff offene Rechnung ist allgemein üblich und bezeichnet die Rechnungen die noch nicht vollständig bezahlt, also noch offen, sind.

Bitte beachten Sie hier den Unterschied zwischen dem Status der Rechnung, welche Offen ist, was soviel bedeutet wie Angelegt und Gedruckt, aber noch nicht (teil-)bezahlt  und dem Begriff der offenen Rechnung, welcher bedeutet, dass diese Rechnung noch nicht vollständig bezahlt wurde.

D.h. ein Rechnung hat praktisch zwei Status Informationen
- a.) Der eigentliche Status, angelegt, offen, verbucht
- b.) offen (=noch nicht bezahlt), teilbezahlt, bezahlt, also noch in Schwebe.

Siehe dazu auch das Statusdiagramm.

"Offene Rechnung sind Rechnungen, die noch nicht bezahlt sind. Also auch verbuchte Rechnungen, die an die FiBu übergeben wurden." -> Ausgenommen integrierte Fibu und Einnahmen/Ausgabenrechner.

Wir sprechen hier auch von noch nicht vollständig bezahlten Rechnungen.

Rabatt aus Rechnung und Lieferschein

Es besteht die Möglichkeit in den Lieferscheinen einen allgemeinen Rabatt und einen versteckten Aufschlag zu vergeben und die gleichen Möglichkeiten gibt es auch in der Rechnung.

Wichtig: Rabatt und versteckter Aufschlag aus der Rechnung gelten **NUR** für die Rechnungspositionen. Für Lieferscheinpositionen greift der Rabatt aus den Konditionen des Lieferscheines.

Sie haben dadurch die Möglichkeit unterschiedlich rabattierte Lieferscheine in einer Rechnung zu verrechnen.

#### Verrechnen von Lieferscheinen
<a name="aus Lieferschein"></a>
Um einen oder mehrere Lieferscheine zu verrechnen, klicken Sie bitte auf ![](Lieferschein_uebernehmen.gif) Rechnung aus Lieferschein erstellen in der Auswahlliste der Rechnungen. Um mehrere Lieferschein auf einmal zu übernehmen markieren Sie die gewünschten Lieferscheine und drücken Sie danach Enter/Eingabetaste bzw. den grünen Haken.
Wurden mehrere Lieferscheine ausgewählt, welche von der Rechnungsadresse her nicht zusammenpassen, so wird nur der erste Lieferschein übernommen. Zum Einfügen weiterer Lieferscheine siehe [Sammelrechnung](#Sammelrechnung).

Wurde ein Lieferschein in eine Rechnung eingebunden, so kann er nur mehr von der Rechnung aus verändert werden. Gehen Sie dazu in die Rechnung, Positionen und Stellen Sie den Cursor auf den zu ändernden Lieferschein. Nun wechseln Sie auf die ![](Sicht_Lieferschein.gif). Hier können Sie nun Preise sowie Rabattsätze und Steuersatz der einzelnen Lieferscheinposition verändern. Die Änderung der gelieferten Mengen ist nicht zulässig. Sollte dies erforderlich sein, so muss der Lieferschein vor der Einbindung in die Rechnung abgeändert werden.

#### Sortierung der Lieferscheine in der Rechnung<br>
Üblicherweise werden die Lieferscheine in der Reihenfolge der Übernahme in die Rechnung eingetragen.<br>
Mit dem Parameter LIEFERSCHEIN_UEBERNAHME_NACH_ANSPRECHPARTNER kann eingestellt werden, dass diese automatisch nach den Ansprechpartnern der Lieferscheine sortiert werden.

Es können in einer Rechnung auch Lieferscheine unterschiedlicher Rechnungsadressen verrechnet werden. Dazu muss das Land des Lieferscheins gleich sein. In den Rechnungspositionen können Lieferscheine unterschiedlicher Rechnungsadressen ausgewählt werden, setzen Sie bei der Auswahl dazu den Haken bei alle Rechnungsadressen ![](alle_RE_adressen.JPG).

#### Woher kommt der Verkaufspreis für den Lieferschein, bzw. die Rechnung?
[Siehe]( {{<relref "./preise_in_der_rechnung" >}} )

#### Bei der Verrechnung kommt die Meldung "nicht verrechenbar"
Lieferscheine können, z.B. für kostenlose Garantielieferungen als nicht verrechenbar gekennzeichnet werden.
Weitere Details [siehe bitte]( {{<relref "/verkauf/lieferschein/#verrechenbare--nicht-verrechenbare-lieferscheine-wozu-ist-das" >}} )

#### Wo kann ich die Standard Kopf und Fußtexte der Rechnung verändern?
Sie können in jedem Bewegungsdatenmodul die allgemein/standard verwendeten Kopf- und Fußtexte einstellen. Wird auf den Rechnungsausdrucken "Das ist eine Rechnung" nach dem Betreffblock angedruckt, so muss dies in den Grunddaten ![](Grunddaten.gif) unter ![](Rechnungstext.gif) eingestellt werden.

#### Wie werden die IG / Drittland Zusatztexte gesteuert?
Beim Rechnungsdruck wird automatisch der Status des Lieferlandes anhand des Länderkennzeichens erkannt.

Als zusätzliche Information wird das EU-Mitgliedsdatum ausgewertet.

Das EU-Mitgliedsdatum muss für die jeweilige Länderkennung im System unter System, Land definiert werden.

Grundsätzlich wird unterschieden ob das Lieferland und das Mandantenland  gleich sind. Ist es nicht gleich, so wird es als Auslands-Rechnung behandelt. Ist es eine Auslands-Rechnung, so wird anhand des Eintrags "EU Mitglied seit" und bis und des Rechnungsdatums entschieden ob eine Innergemeinschaftliche Lieferung oder eine Drittlandslieferung vorliegt. Je nachdem wird der Text der Innergemeinschaftlichen Lieferung oder der vereinfachten Warenlieferung angedruckt.

### Reverse Charge Verfahren
Das Reverse Charge (= Rückwärtsberechnung) Verfahren wird in Österreich für von Unternehmen erbrachte Bauleistungen an wieder Bauleistungen erbringende Unternehmen (für die Zulieferer der Baubranche) verwendet. Auf Grund dieser Regelung ist nur noch der Generalunternehmer Vorsteuerabzugsberechtigt. Dies bedeutet, es wird vom Zulieferer keine Mehrwertsteuer verrechnet. Es ist jedoch auf der Inländischen Rechnung ein entsprechender Hinweis (Gemäß §19 (1a) UstG 1994 (angeführt durch das 2.AÄG 2002) geht die Umsatzsteuerschuld auf den Leistungsempfänger über.) anzubringen.<br>
Da es in der Praxis immer wieder vor kommt, dass verschiedene Lieferungen an ein und denselben Kunden das eine Mal im Reverse Charge Verfahren, das andere Mal, z.B. für ein anderes Projekt, mit Umsatzsteuer erfolgt, muss dies je Rechnung eigens ausgewiesen werden.
D.h. Sie können in **Kieselstein ERP** wählen, ob eine Rechnung nach dem Reverse Charge Verfahren gestellt werden soll, oder ob die übliche Mehrwertsteuer zur Anwendung kommt.
Dies bewirkt, dass bei der Rechnung keine Mehrwertsteuer berechnet und ausgewiesen wird, da die Umsatzsteuerschuld ja auf den Empfänger übergeht.<br>
Bitte beachten Sie, dass die Angabe des Reverse Charge Verfahrens auch für die Gutschrift angewandt werden kann, d.h. auch die Gutschrift wird dann ohne Ausweis der Mehrwertsteuer erstellt.<br>
Das Reverse Charge Verfahren wird auch für Rechnungen in das EU-Ausland angewandt, wenn Leistungen verrechnet werden. Das Verhalten ist grundsätzlich gleich wie im Inland, nur muss ein anderer Text angedruckt werden.<br>
In beiden Fällen werden zu Ihrer und Ihres Kunden Information die Steuersätze je Position angedruckt und auch die daraus errechneten Mehrwertsteuer-Basisbeträge. Diese werden angedruckt um die richtige Buchung je noch Steuersatz in Ihrer Fibu zu ermöglichen. Sie werden für die Rechnungssumme NICHT mitgerechnet.

<a name="Rückdatieren"></a>Rückdatieren
### Rückdatieren
Gerade zum (Geschäfts-)Jahreswechsel ist es immer wieder erforderlich, dass Rechnungen bzw. Gutschriften auch für das vergangene Jahr angelegt werden. Zugleich gilt die Forderung nach der Durchgängigkeit der Belegnummern (also Rechnungsnummern bzw. Gutschriftsnummern). Um dies sicherzustellen wurde in **Kieselstein ERP** dies so realisiert, dass beim Anlegen des Beleges, also unmittelbar nach dem Neu, das Belegdatum definiert werden kann. Direkt während des Neu(e Rechnung) kann auch das Belegdatum entsprechend geändert werden. Wurde die Rechnung bzw. Gutschrift bereits erstellt und abgespeichert, so kann das Belegdatum nur mehr innerhalb des Geschäftsjahres verändert werden.<br>
Bitte beachten Sie, dass beim Rückdatieren auch die Rechnungsnummer entsprechend dem definierten Nummernkreis in das alte Jahr verschoben wird.<br>
Bitte beachten Sie, dass der Zeitbereich in dem Rückdatiert werden kann zusätzlich von zwei Parametern abhängt.
- Parameter BEWEGUNGSMODULE_ANLEGEN_BIS_ZUM hier bedeuten:<br>
    - Wert > 0: Belege können bis zum angegebenen Tag im alten Monat angelegt werden
    - Wert = -1: innerhalb des aktuellen und alten GJ (sofern nicht bereits durch FiBu gesperrt)
    - Wert < -1: legt die Tage fest, die vom aktuellen Datum zurückgerechnet werden. In diesem Zeitraum können Belege angelegt werden.
- Recht FB_CHEFBUCHALTER:<br>
Damit kann das Belegdatum bei Angebot, Eingangsrechnung, Lieferschein und Rechnung zu einem fast beliebigen Datum (beachten Sie dazu bitte auch das RLG) angelegt werden.

### Neu-Datum
<a name="Neu-Datum"></a>
Gerade kurz nach dem Jahreswechsel ist es oft erforderlich, dass mehrere Rechnungen noch für das alte Geschäftsjahr / Wirtschaftsjahr erstellt werden. Um nun nicht immer für jede neue Rechnung das Datum entsprechend ändern zu müssen, steht im Rechnungsmodul unter Extras, Neu Datum die möglich der Vorbelegung eines anderen Rechnungsdatums zur Verfügung.<br>
Das hier eingegebene Datum wird solange anstelle des aktuellen Datums für den Vorschlagswert des Belegdatums einer neuen Rechnung verwendet, bis entweder das Rechnungsmodul geschlossen wird, oder das Neu Datum wieder gelöscht wird. Dies gilt auch für die Funktionen Rechnung aus Auftrag oder Lieferschein. Bitte beachten Sie für eine eventuelle Rückdatierung die oben angeführten Punkte.<br>
**HINWEIS:**<br>
Gerade wenn Sie die Funktionen neue Rechnung aus Lieferschein bzw. neue Rechnung aus Auftrag verwenden, wird in der Regel die Rechnung mit dem aktuellen Datum angelegt.
Das bedeutet, wenn Lieferscheine / Aufträge noch im alten Geschäftjahres abgerechnet werden sollten, so **muss** die Funktion Neu-Datum verwendet werden.

#### Fortlaufende Rechnungsnummern / wiederholende Rechnungen
Grundsätzlich definiert das Rechnungslegungsgesetz (RLG), dass fortlaufende Rechnungsnummern verwendet werden müssen. Diese können jedoch nach Geschäftsfeldern oder ähnlichem gruppiert werden. Daraus ergibt sich auch, dass das Rechnungsdatum für die Ausgangsrechnungen aufsteigend sein sollte.<br>
Bei den wiederholenden Rechnungen wird das Rechnungsdatum auf jenes Datum gesetzt, das sich aus dem Wiederholungsintervall und dem Wiederholdatum ergibt. Daher kann es vorkommen, dass die aus dem Wiederholungslauf erzeugten Rechnungen ein älteres Rechnungsdatum haben. Mit dieser Begründung sollte es auch bei verschiedenen Prüfungen akzeptiert werden. **Hinweis:** Das verwendete Rechnungsdatum kann auch übersteuert werden. [Siehe dazu]( {{<relref "/verkauf/auftrag/#wie-wird-ein-wiederholender-auftrag-definiert-" >}} )

#### Wir wollen im neuen Geschäftsjahr die Rechnungsnummern nicht mit Eins beginnen lassen

[Siehe dazu]( {{<relref "/verkauf/gemeinsamkeiten/#k%c3%b6nnen-startwerte-f%c3%bcr-die-belegnummer-definiert-werden" >}} )

#### Umsatzübersicht, Was bedeutet was?
![](Rechnung_Umsatzuebersicht.gif)

In der Rechnungsverwaltung erhalten Sie durch Klick auf den Reiter Umsatzübersicht, rasch und komfortabel eine Übersicht über den Status Ihrer Ausgangsrechnungen.

Diese sind in drei Gruppen geteilt:
| Gruppe | Beschreibung |
| --- |  --- |
| Offene: | Von den Ausgangsrechnungen dieses Zeitraums ist der angezeigte Betrag noch nicht bezahlt.Beachten Sie bitte, dass bei Schlussrechnungen auch der in den Anzahlungsrechnungen enthaltene Betrag berücksichtigt wird. D.h. der offene Zahlbetrag einer Schlussrechnung ist immer um den in den Anzahlungen enthaltene Betrag reduziert. Der Zahlungsstatus der offenen Anzahlungsrechnungen ist bei diesen berücksichtigt. |
| Umsatzübersicht | Die Summe Ihrer gelegten Ausgangsrechnungen im Zeitraum, jedoch ohne Anzahlungsrechnungen. |
| noch nicht abgerechnete Anzahlungen | Die Summe der gelegten Anzahlungsrechnungen und noch nicht in einer Schlussrechnung eingebundenen Anzahlungsrechnungen im Zeitraum. |

Die Darstellung jeder Gruppe ist noch in Brutto, also inkl. Mehrwertsteuer und Netto, also exkl. Mehrwertsteuer unterteilt. Dies hat den Vorteil, dass Sie, je nach Betrachtungsvariante, den passenden Wert zur Verfügung haben.<br>
In der Auflistung finden Sie zusätzlich die Werte des Vorjahres, die Monatswerte und die Gesamtsumme des gewählten Jahres. Bitte beachten Sie, dass je nach Einstellung des Auswahldialoges in den Umsätzen die Gutschriften bereits berücksichtigt sind.

Alternativ steht dafür auch aus dem Menü, Journal, Umsatz zur Verfügung.
Hier finden Sie zusätzliche Möglichkeiten
![](Journal_Umsatz.gif)
- mit DB-Betrachtung. Liefert auch eine Spalte mit dem aktuellen Deckungsbeitrag im jeweiligen Monat<br>Dies ist natürlich von der Art der erfassten Daten abhängig
- ohne andere Mandanten: Das bedeutet die Erlöse die an einen anderen Mandanten verrechnet wurden, werden nicht berücksichtigt
- incl. offene zum Monatsletzte<br>Zeigt die Entwicklung der Summe der offenen Rechnungen zum jeweiligen Monatsletzten

Es ist dafür das Recht PART_KUNDE_UMSAETZE_R erforderlich

#### Wie werden Chargenmengen abgebucht ?
Werden mehrere Chargen für eine Rechnungs-/Lieferscheinbuchung benötigt, so werden diese anhand der Reihenfolge Ihrer Chargenangaben abgebucht. Das bedeutet: Wird zuerst die Charge A und dann die Charge B angegeben, so werden alle Mengen der Charge A verwendet und dann die (Rest-)Mengen der Charge B. Geben Sie die Chargen umgekehrt an, also zuerst B und dann A, so werden zuerst alle verfügbaren Mengen der Charge B verbraucht und dann der Rest von A genommen.

#### Rechnung direkt aus Auftrag erstellen.
<a name="Rechnung direkt aus Auftrag erstellen"></a>
In der Rechnungsauswahlliste finden Sie neben dem neue Rechnung aus Lieferschein auch den Knopf neue Rechnung aus Auftrag ![](Rechnung_aus_Auftrag.gif). Damit können Sie eine direkte Verbindung vom Auftrag in die Rechnung herstellen und ohne Erstellung eines Lieferscheines die Positionen eines Auftrages verrechnen. [Siehe dazu auch]( {{<relref "/verkauf/rechnung/abrechnungsvorschlag/#wie-kommt-die-auftragsnummer-in-die-rechnung">}} )

#### Wieso werden die Auftragspositionen nicht automatisch in die Rechnung übernommen ?
Durch die Verknüpfung einer Rechnung mit dem Auftrag wird in erster Linie eine Definition hergestellt, mit der besagt wird, dass dieser Auftrag, (teilweise) durch diese Rechnung verrechnet / ausgeliefert wird.
Sie werden daher automatisch bei Rechnung aus Auftrag in die Sicht Auftrag (oberer Modulreiter Sicht Auftrag im Rechnungsmodul) geführt. Hier finden Sie nun alle offenen Auftragspositionen, also die Positionen des Auftrags die noch nicht geliefert bzw. verrechnet sind. Übernehmen Sie nun die gewünschten Positionen aus dem Auftrag in die Rechnung, gegebenenfalls auch nur mit Teilmengen. Nur dadurch wird auch die offene Postenverwaltung des Auftrags entsprechend aktualisiert. **WICHTIG:** Werden zusätzliche Positionen in die Rechnung unter Sicht Positionen eingetragen, so sind dies zusätzliche Positionen, welche die offene Postenliste des Auftrags nicht verändert. D.h. sollte im Auftrag die gleiche Position enthalten sein, so wird die offene Auftragsmenge dadurch nicht geändert, was auch durch eine entsprechende Meldung angezeigt wird.
Siehe dazu bitte auch [Auftragspositionen in Lieferschein]( {{<relref "/verkauf/lieferschein/#wie-kann-ich-auftragspositionen-%c3%bcbernehmen">}} ) übernehmen.

#### Können alle Auftragspositionen auf einmal in die Rechnung übernommen werden ?
Durch Klick auf den Knopf ![](Rechnung_aus_Auftrag.gif) im oberen Modulreiter Sicht Auftrag können alle lieferbaren und noch offenen Positionen des Auftrags in die Rechnung übernommen werden.

#### Wie können Texteingaben vom Auftrag in die Rechnung übernommen werden?
Da die Texteingaben keine lieferbaren Positionen sind, werden die Texteingaben nicht automatisch mit in die Rechnung übernommen. Um die einzelnen Texteingaben zu übernehmen, wählen Sie bitte die Texteingabe aus und klicken auf ändern und speichern. Damit wird diese Auftragsposition in die Rechnung übernommen.

#### Verrechnung Wiederholender Aufträge
Mit der Funktion Extras, Wiederholende Aufträge verrechnen ![](Wiederholende_Auftraege.gif) werden für alle noch nicht verrechneten [Wiederholenden Aufträge]( {{<relref "/verkauf/auftrag/#wie-wird-ein-wiederholender-auftrag-definiert-">}} ) Rechnungen erstellt. Diese auftragsbezogenen Rechnungen können gegebenenfalls vor der Verrechnung wie üblich verändert werden. Daran anschließend können alle noch nicht gedruckten Rechnungen auf einmal ausgedruckt werden.<br>
Nach der Bestätigung, dass Sie abrechnen wollen erscheint ein Dialog in dem das Vorfälligkeitsdatum eingegeben werden kann.<br>
![](Vorfaelligkeitsdatum.jpg)<br>
Geben Sie hier den Stichtag ein zudem die Betrachtung für die Errechnung der anzulegenden Rechnungen erfolgen sollte.<br>
Bitte beachten Sie, dass auch alle Rechnungen zu diesem angegebenen Datum angelegt werden.
Gedacht ist diese Funktion um zum Beispiel am Abend des Monatsletzten bereits die Rechnungen für den Ersten des Folgemonates erstellen zu können.<br>
**Hinweis:** Beachten Sie unbedingt dass chronologische Durchgängigkeit gegeben sein muss. <br>
**Hinweis:** Beachten Sie bitte: Ob im jeweiligen Monat für einen Auftrag bereits eine automatische Wiederholende Rechnung erstellt wurde, wird anhand des in den Kopfzeilen (Konditionen) hinterlegten Textes entschieden. Ist hier kein oder ein falscher Text eingegeben, so wird dies NICHT als Monatsrechnung erkannt und durch den Automatismus eine weitere Wiederholende Rechnung angelegt.<br>
Manchmal wird, zu einem Wiederholenden Auftrag sofort manuell eine Rechnung erstellt um die erste Abrechnung so schnell wie möglich durchzuführen. Sollte dies auch bei Ihnen der Fall sein, so stellen Sie bitte den Wiederholungsbeginntermin auf den **nächsten** gewünschten Verrechnungstermin.

#### Automatische Erstellung von Monatsrechnungen
Unter Extras, Monatsrechnungen erstellen steht im Modul Rechnungen eine praktische Funktion für die automatische Verrechnung von Lieferscheinen zur Verfügung.

Es werden damit monatliche Sammelrechnungen für diejenigen Kunden erstellt, bei denen unter Konditionen im Modul Kunden Monatsrechnung angehakt ist.

Die Monatsrechnung wird für einen Kunden unter folgenden Voraussetzungen erstellt:
- Grundsätzlich gilt die Rechnungsadresse des Lieferscheines
- Ein / die Lieferschein(e) wurde(n) erstellt und dieser ist im Status geliefert
- Beim Kunden ist unter Konditionen Monatsrechnung angehakt
- Gibt es im aktuellen Monat noch keine Rechnung für diesen Kunden, welche im Status angelegt ist, 
    - so wird eine neue Rechnung erstellt und der Lieferschein in die Rechnung übernommen
    - Das Rechnungsdatum ist das Anlage Datum der Rechnung
- Gibt es in diesem Monat für diesen Kunden bereits eine angelegte Rechnung, so
    - wird die Rechnung um den neuen Lieferschein ergänzt
- Der jeweilige Lieferschein ist damit verrechnet.

Bitte beachten Sie, dass die unten angeführten Grenzen, maximaler Rechnungswert, maximale Rechnungspositionen bei diesem Lauf nicht berücksichtigt werden.

<a name="alle Lieferscheine verrechnen"></a>

#### Können alle Lieferscheine auf einmal verrechnet werden?
Unter Extras, offene Lieferscheine können alle Lieferscheine welche im Status geliefert sind verrechnet werden.
Zusätzlich wird vor der Verrechnung das Lieferscheindatum abgefragt bis zu dem diese verrechnet werden sollten. Dieses steht per default auf Vorgestern. Das hat den Vorteil, dass gegebenenfalls heute bzw. gestern erstellte Lieferscheine noch korrigiert werden können.
![](offene_LS_verrechnen.gif)<br>
Bitte beachten Sie, dass diese Funktion nur in Kombination mit den Sammellieferscheinen zur Verfügung steht.

Bei der Abfrage des Lieferscheindatums kann zusätzlich auch die Verrechnung nur einer Rechnungsadresse ausgewählt werden.
Bei der Verrechnung greifen folgende Parameter:
| Parameter | Wirkung |
| --- |  --- |
| MAXIMALER_NETTO_RECHNUNGSWERT | Mit diesem Mandantenparameter kann generell (für alle mit dem Verrechnungsautomaten erzeugten Lieferscheine) die Verrechnung auf einen Betrag begrenzt werden. Die Idee dahinter ist, dass sollte eine Rechnungsposition strittig sein, so wird von großen Kunden diese Rechnung oft sehr lange zurückgehalten. Wenn das nun sehr große Beträge wären, könnte sich das auf Ihre Liquidität auswirken, daher die Begrenzung. In der Standardeinstellung ist diese Grenze abgeschaltet. |
| Max. RE-Pos. | Da manche Kunden nur eine gewissen Anzahl an Positionen auf einer Rechnung akzeptieren, kann mit dieser Kundeneigenschaft (siehe Reiter Konditionen) definiert werden, wieviele Positionen über alle Lieferscheine gerechnet in einer Rechnung an diesen Kunden sein dürfen.Bei der Neuanlage eines Kunden ist dieses Feld leer, womit alle Lieferscheine in eine Rechnung übernommen werden. |
| je LF | Manche Kunden akzeptieren Rechnungen nur für jeweils eine Lieferadresse. Damit auch dies bei der automatischen Verrechnung der Lieferscheine berücksichtigt werden kann, haken Sie bitte je LF an. |
| Monatsrechnung | Ist für den Kunden im Reiter Konditionen Monatsrechnung angehakt, so wird er in diesem Lauf der Verrechnung der gelieferten Lieferscheine nicht berücksichtigt. |
| nicht verrechenbar | Ist ein Lieferschein als nicht verrechenbar gekennzeichnet, so wird er in dieser Übernahme auch nicht verrechnet. |

**Anmerkung:** Die Logik dahinter ist so, dass wenn obige Parameter ergeben würden, dass nicht einmal ein Lieferschein in die Rechnung übernommen werden würde, so wird dieser eine Lieferschein trotzdem verrechnet.<br>
Eventuelle Kundenhinweise werden entweder bei der Auswahl der Rechnungsadresse bzw. am Ende der Berechnung gesammelt angezeigt.<br>
**Hinweis:** Lieferscheine, deren Rechnungsadresse einen Kunden mit "Monatsrechnung" haben, werden nicht verrechnet. Weiters werden als nicht verrechenbar gekennzeichnete Lieferscheine auch nicht verrechnet.

Für den Versand der Rechnungen nutzen Sie bitte
![](angelegte_Rechnungen_versenden.gif)
alle angelegten Rechnungen einer oder aller Kostenstellen drucken oder auch
ausgewählten Rechnungen drucken bzw. als EMail versenden.
Nutzen Sie, gerade für den EMail Versand dazu auch die Kombination der Kopienanzahl der Rechnungen im Kunden, Reiter Konditionen und die Definition der Rechnungsempfänger EMail-Adresse in den Kopfdaten.

#### Können Lieferscheine auch mit Staffelpreisen verrechnet werden?
Manche Anwender erhalten von Ihren Kunden mehrere zu veredelnde Teile am gleichen Tag für verschiedenste Kommissionen u.ä.. Dazu gibt es dann noch die Vereinbarung, dass die Verkaufspreise für diese Arbeiten je nach der erhaltenen Tagesmenge berücksichtigt werden. Um dem Rechnung zu tragen, gibt es im Modul Rechnung, Menü, Extras den Punkt "offene LS mit AB-Staffelpreisen verrechnen". ![](offene_LS_mit_AB-Staffelpreisen.gif)

Die dahinter liegende Logik, der dahinter liegende Ablauf ist folgende:
1.  Es wird der Stichtag abgefragt
2.  Es erscheint eine Listen von Kunden-Rechnungsadressen, welche zum Stichtag offene Lieferscheine haben
3.  Nachdem ein Kunde ausgewählt wurde, wird nun: Wenn der Kunde
    1.  "bekommt Sammelrechnung" in den Konditionen angehakt hat, je eine Rechnung für alle Lieferscheine (wenn keine Zusatzfunktion SAMMELLIEFERSCHEIN, dann nach Auftrag getrennt je eine Rechnung)
    2.  "bekommt Sammelrechnung" in den Konditionen nicht angehakt hat, eine Rechnung pro Lieferschein.
4.  Die Preise der Lieferscheinpositionen werden automatisch angepasst. Je Lieferscheinposition wird über den zugehörigen Auftrag und dessen Belegdatum die Summe der Auftragspositionsmengen für den gleichen Artikel und Kunden zum Auftragsbelegdatum verwendet, um den VK(Staffel)preis zu ermitteln.

**Hinweis**: Wenn in einem Lieferschein einen Artikel ohne VK-Preisbasis eingetragen ist, wird dieser **Lieferschein nicht verrechnet**.

#### Ausdruck aller angelegten Rechnungen
Oft ist es praktisch die (automatisch) angelegten Rechnungen in einem Lauf auszudrucken. Verwenden Sie dazu den Menüpunkt Rechnung, Alle angelegten drucken. Hier können Sie noch wählen, ob alle Rechnungen oder nur die Rechnungen einer Kostenstelle in diesem Lauf gedruckt werden sollten. Ebenso erscheint eine Abfrage, ob Kunden mit Monatsrechnung berücksichtigt werden sollen. 

#### Aktivieren / versenden von ausgewählten Rechnungen
Ähnlich zum Druck aller angelegten Rechnungen wird oft auch gewünscht, dass man Rechnungen spezifisch nur aktiviert, druckt oder versendet. Wählen Sie dafür den Menüpunkt
![](ausgewaehlte_Rechnungen_drucken.gif)
ausgewählte Rechnungen mit den gewünschten Unterpunkten.

| Auswahl | Wirkung |
| --- |  --- |
| Aktivieren | Die Rechnung wird nur aktiviert, ohne dass irgendeine Form von Druck erfolgt. Somit auch keine Ablage der Rechnung in der Dokumentenablage |
| Drucken | Ausdruck der Rechnungen mit Berücksichtigung der Einstellungen der Kopien bzw. Kopiendrucker |
| als E-Mail versenden | Versendet das Original per Mail und druckt die Kopien am Drucker. Wenn Kriterien für Email Versand nicht erfüllt sind, dann wird die Rechnung nicht aktiviert. |

Bei eventuell auftretenden Fehlern wird die Rechnung übersprungen und bleibt somit im Status angelegt. In diesem Falle versuchen Sie die einzelne Rechnung zu drucken und ergänzen Sie bitte die fehlenden Daten.

#### Ich habe einen Auftrag mit Lieferschein verrechnet. Nun kann ich die Position nicht mehr in die Rechnung übernehmen.
Ja das ist so richtig, denn:

wenn aus einem Auftrag eine (Teil-)Lieferung gemacht wird, so wurde der Auftrag / diese Auftragsposition ja mit dem Lieferschein erfüllt. Wenn Sie nun die gleiche Lieferung auch per Rechnung durchführen möchten, so darf das nicht gehen, denn dadurch würde der Auftrag übererfüllt (außer es sollte dies absichtlich gemacht werden, was aber etwas anderes ist).
D.h. wenn die Lieferung nun verrechnet werden soll, so bitte den Lieferschein verrechnen, denn die Ware ist ja schon mit dem Lieferschein zum Kunden gegangen, nur der Lieferschein ist noch nicht verrechnet.

#### Wie geht man vor, wenn in einem bereits verrechneten Lieferschein ein Fehler entdeckt wurde und man diesen korrigieren möchte?
1. Aus dieser Rechnung die Position "Lieferschein x" löschen. -> Der Lieferschein wechselt vom Status "verrechnet" in "geliefert".
2. Der Lieferschein kann jetzt verändert werden: Im Lieferschein > Kopfdaten editieren und wieder speichern setzt den Status auf offen. Nochmals auf editieren und speichern klicken setzt den Status auf angelegt zurück. -> Alle Änderungen können im Lieferschein durchgeführt werden.
3\ Den Lieferschein drucken -> Lieferschein Status: "geliefert"
4. ACHTUNG: Wenn man in der Rechnungs-Auswahl auf das Symbol "Lieferschein aus Rechnung" klickt, wird eine NEUE Rechnung angelegt!<br>
Da die Rechnung bereits angelegt wurde, wechselt man bei dieser in die Positionen. 
5. Neue Position mit Art "Lieferschein" anlegen. Entsprechenden Lieferschein auswählen.
6. Gegebenenfalls kontrollieren über Sicht Auftrag

Unterschied zwischen Skonto und allgemeinem Rabatt.

Es kommt in manchen Unternehmen vor, dass z.B. für Zahlung mittels Bankeinzug ein besonderer Nachlass vereinbart wird. Hier stellt sich manchmal die Frage, ob dies mit einem allgemeinen Rabatt oder mit einem Skontosatz erfolgen sollte.<br>
Der wesentliche Unterschied wirkt vor allem in den Umsatzstatistiken und in den Betrachtungen für die Umsatzsteuerverprobung.<br>
Eine Rechnung mit allgemeinem Rabatt ist eine Rechnung deren Wert reduziert wurde. D.h. Umsatzsteuer, Statistiken der Umsätze, der Deckungsbeiträge usw. berücksichtigen diesen Rabatt.<br>
Es entsteht dadurch kein Skontoaufwand.<br>
Wird auf eine Rechnung Skonto gewährt/akzeptiert, so ist dieser zusätzliche Aufwand in den Statistiken nicht enthalten. Zusätzlich erfolgt die Berechnung der Umsatzsteuer auf Basis des Rechnungsbetrages. Erst nach Zahlung darf der Skontoaufwand berücksichtigt werden.
Im Verkaufsbereich hat sich in einigen Unternehmen die Unsitte eingeschlichen, dass zwar die preislichen Vorgaben erfüllt werden, aber über den Weg des zusätzlichen Skontos dann doch der Zielpreis unterlaufen wird.

Bitte beachten Sie daher den Unterschied zwischen Skonto und allgemeinen Rabatt.

Dazu ein Beispiel:
| Position | Betrag | Bemerkung |
| -- | --: | --- |
| Rechnungs-Netto-Betrag |  1.000,- | |
| abzgl. allgemeiner Rabatt 10% | 100,- | | 
| MwSt Basisbetrag | 900,- | |
| zzgl. MwSt 20% | 180,- | Diese sind zeitgerecht an das Finanzamt zu entrichten |
| Rechnungsbetrag | 1.080,- | |
| Rechnungs-Netto-Betrag | 1.000,- | |
| zzgl. MwSt 20% | 200,- | Diese sind zeitgerecht an das Finanzamt zu entrichten |
| Rechnungsbetrag  | 1.200,- |
| abzgl. 10%Skonto |  120,-  |

Ergibt zwar im Endeffekt den gleichen Zahlungseingang, aber alle Kalkulationen gehen vom höheren Betrag aus, auch Ihre Liquiditätsplanung. Zusätzlich ist die abzuführende Umsatzsteuer höher. Der Differenzbetrag wird erst mit dem Zeitpunkt der Kundenzahlung durch den Skontoaufwand wieder rückerstattet.

#### Beim Ausdrucken einer Auslandsrechnung erscheinen falsche Texte. Wie kann ich diese richtigstellen ?
In **Kieselstein ERP** werden am Ende einer Rechnung, abhängig vom Land des Rechnungsempfängers unterschiedliche Texte angedruckt. Der Inhalt dieser Texte ist im Report rech_rechnung_summary.jrxml definiert.

Es werden folgende Versandarten unterschieden:

| Landesart | Text | Bedingung |
| --- |  --- |  --- |
| Inland | keine zusätzlichen Texte | Inland. D.h. Das Länderkennzeichen des Mandanten und des Kunden sind gleich. |
| Reverse Charge Inland | Gemäß §19 .... geht die Umsatzsteuerschuld auf den Leistungsempfänger über. | Inland und beim Kunden ist unter Konditionen Reverse Charge angehackt. |
| EU-Ausland | Steuerbefreite Lieferung IG-Warenverkehr | EU Mitglied zum Datum.Siehe dazu System, Land, EU-Mitglied seit. |
| Drittland | Der Ausführer der Waren ...(ehemals EUR2 genannt) | Ausland und kein EU Mitglied |
| Reverse Charge Ausland | Gemäß §13.B ... geht die Umsatzsteuerschuld auf den Leistungsempfänger über | Ausland und beim Kunden ist unter Konditionen Reverse Charge angehackt. |

Diese Texte können in der Reportvorlage jederzeit angepasst werden. Beachten Sie hier bitte auch die manchmal sehr eigenwilligen Vorstellungen der Zollbeamten der Exportländer.

#### Stornieren einer Rechnung
Zum Stornieren einer Rechnung klicken Sie bei einer bereits ausgedruckten Rechnung in den Rechnungskopfdaten auf ![](Rechnung_Storno.gif) löschen / stornieren.
Bitte beachten Sie die unten beschriebene Thematik der Freigabe bereits verrechneter Lieferscheine.

Hinweis: Eine stornierte Rechnung kann durch Klick auf den ändern Knopf in den Kopfdaten wieder re-aktiviert werden. **<u>Wichtig:</u>** Stornieren Sie Rechnungen nur wenn dies tatsächlich für Ihren Kunden auch so erforderlich ist. Es empfiehlt sich zusätzlich, vor der Durchführung des Stornos die Begründung dafür in die Rechnung einzutragen. Denken Sie dabei immer daran, dass Sie nach sieben bzw. zehn Jahren einem Finanzbeamten sehr rasch erklären und begründen müssen, warum die Rechnung storniert wurde.
Rechnungen die nur verschrieben wurden, z.B. an die falsche Adresse des gleichen Kunden, können und sollten auch geändert werden. Haben Sie versehentlich eine Rechnung angelegt und wollte der Kunde die Ware z.B. auf Lieferschein kaufen, so leeren Sie die Rechnung wieder und verwenden Sie diese Rechnungsnummer für die nächste neu zu erstellende Rechnung. Achten Sie dabei unbedingt auf die Chronologie der Rechnungsnummern.

Behandlung von Lieferscheinen in stornierten Rechnungen:

Ist in einer zu stornierenden Rechnung ein Lieferschein (natürlich gilt dies auch für mehrere Lieferscheine) eingebunden, so müssen diese Lieferscheine, da ja die Rechnung storniert wurde wieder freigegeben werden. Um nun den Wert der Rechnung gleich zu belassen und auch die ehemalige Einbindung des Lieferscheines in die Rechnung zu dokumentieren, wird die Position Lieferschein in eine Handposition mit dem Lieferscheinwert umgewandelt. In dieser wird auf die Lieferscheinnummer hingewiesen.

Ansicht vor dem Storno:

![](LS_Storno_davor.jpg)

Ansicht nach dem Storno:

![](LS_Storno_danach.jpg)

Wird die Rechnung wieder entstorniert, so bleibt die Handbuchung trotz des Verweises in den Kopfdaten bestehen. D.h. ein eventuell zuvor eingebundener Lieferschein muss in den Rechnungspositionen neu übernommen werden.

<a name="Fremdwährungen"></a>

### Behandlung von Fremdwährungen
In **Kieselstein ERP** werden Rechnungsbeträge und Zahlbeträge sowohl in der Belegwährung als auch in der Mandantenwährung abgespeichert. Die Umrechnung erfolgt jeweils zum Kurs des Buchungsdatums. Das bedeutet, dass für die Umrechnung des Rechnungsbetrages von der Fremdwährung (z.B. CHF) in die Mandantenwährung (z.B. EUR) das Datum der Rechnung verwendet wird. Für die Umrechnung des Zahlungsbetrages wird das Datum der jeweiligen Zahlung verwendet. Daraus werden nun die Kursdifferenzen ersichtlich. Da diese Kursdifferenzen in aller Regel von Ihnen (dem **Kieselstein ERP** Anwender) zu tragen sind, wird die Errechnung des jeweiligen Zahlbetrages für die Auswertungen (Journale) zur Basis des Kurses der Rechnung mit übergeben. Um die Kursdifferenzen aufzuzeigen wird zusätzlich der anhand des Kurses des Zahlungsdatums errechnete Betrag in Mandantenwährung in den Journalen mit angedruckt.
Die Angabe der Kursdifferenzen steht nur für (Ausgangs-) Rechnungen und Eingangsrechnungen zur Verfügung.

Daher ist es auch wichtig, dass die sich laufend ändernden Kurse der Fremdwährungen vor dem Erstellen der jeweiligen Fremdwährungsrechnung eingetragen sind (Finanzbuchhaltung, Währung)

#### Können Rechnungssummen auch in anderen Währungen ausgewiesen werden ?
Ja. Verschiedene Länder verlangen, dass, wenn das Unternehmen nicht in der Landeswährung geführt wird, immer die Rechnungs- und Steuerbeträge in der Landeswährung ausgewiesen werden müssen.<br>
Definieren Sie dazu bitte im System, Mandant, Vorbelegungen2
![](Zusaetzliche_Rechnungswaehrung.gif)
die zusätzliche Rechnungswährung. Damit werden, zum jeweils aktuell hinterlegten Kurs, der Nettobetrag, die Steuersätze und die Bruttobeträge auch in die andere Währung umgerechnet. Achten Sie hier ganz besonders auf gültige Kurse.
Wir weisen ausdrücklich darauf hin, dass dies ein reines Rechenhilfsmittel ist und keinerlei Auswirkung hat, also nur für den Ausdruck verwendet wird.

#### Die Rechnung wird mit einem falschen Betrag / Steuerbetrag gedruckt. Wie kann ich das ändern ?
Kommt die Position direkt aus der Rechnung, so ändern Sie die Rechnung bitte entsprechend ab.
Kommt die Position aus dem Lieferschein, so muss die Lieferscheinposition geändert werden.
Wechseln Sie dazu in die (Rechnungs-)Position, wählen Sie die Zeile mit dem Lieferschein und klicken Sie nun auf Sicht Lieferschein.
Nun kann wiederum über ändern der Betrag / der Mehrwertsteuersatz geändert werden.
Wenn der Mehrwertsteuersatz falsch ist, so prüfen Sie bitte warum hier ein falscher Satz vorgeschlagen wurde. Kommt dies von einer falschen Einstellung des Kunden, wurde der Auftrag falsch angelegt usw..

#### Beim ausdrucken der Rechnung Sachkonten möglicherweise fehlerhaft. Was ist zu tun?
Wenn beim Ausdruck der Rechnung folgende Meldung erscheint, welche nur bei integrierter Finanzbuchhaltung aufscheint,
![](Sachekontendefinition_fehlerhaft.jpg)
so bedeutet dies, dass kein Umsatzsteuerbetrag anhand der Rechnung ermittelt wurde, aber die Buchung auf ein Konto erfolgen sollte, dessen Ust-Art z.B. auf Inland Normalsteuer steht.
Dies kommt manchmal dann vor, wenn zuerst ein ausländischer Kunde (Interessent) ohne UID Nummer angelegt wird und bei diesem dann im Auftragsfalle die UID Nummer nachgetragen wird. Hier ist immer die Steuerkategorie des Debitorenkontos zu prüfen.

D.h. bitte prüfen Sie einerseits ob die Kontendefinition des angegebenen Erlöskontos zur Rechnung passt. Gegebenenfalls ist auch die UVA-Art des Kontos zu korrigieren.

Für unsere Schweizer / Liechtensteiner Anwender:
Dies passiert gerne auch für den Rundungsartikel (auf die 5Rappen). D.h. bitte stellen Sie sicher, dass der Rundungsartikel über die Artikelgruppe auf ein Konto zeigt, welches als UVA-Art nicht zutreffend hinterlegt hat.

#### Beim ausdrucken der Rechnung erscheint Nettodifferenzbetrag zu hoch. Was ist zu tun?
Diese Meldung kommt, wenn eventuelle interne Rundungsdifferenzen zu hoch werden.
Hintergrund: Gerade bei der integrierten Finanzbuchhaltung kann es über alle Konten immer wieder zu kleinen Centdifferenzen kommen. Diese werden automatisch ausgeglichen und der Buchung mit dem höchsten Betrag zugewiesen. Ist diese Differenz aber zu groß, so liegt der Verdacht nahe, dass hier auch weitere Daten falsch sind. Daher wird die Buchung nicht durchgeführt.
Derzeit ist uns nur ein Fall bekannt, in dem dies auftreten kann. Dies ist bei unrichtigen / unvollständigen Definition der Artikel von Setartikeln der Fall.
D.h. erscheint die Meldung:
![](Nettodifferenz_zu_hoch.jpg)

So kann die Verteilungsrechnung der Setpositionen NICHT durchgeführt werden.
Bitte definieren Sie bei den beteiligten Artikeln der Setpositionen ebenfalls die Verkaufspreise.

Warum ist das wichtig?
Wenn Sie einen Setartikel verkaufen, so ist dieses Set ja nur eine verkäuferische Klammer um die darin enthaltenen Artikel. In Wirklichkeit lieferen Sie jeden einzelnen Artikel der Setposition. Somit hat auch jeder Setpositions-Artikel seinen Verkaufspreis / Erlös. Man will ja am Schluss auch wissen, welchen Erlös man mit den einzelnen Artikeln hatte, egal in welchem Set dieser enthalten war.
Dazu kommt, dass es Sets gibt, die pro Position unterschiedliche Mehrwertsteuersätze haben können. Auch daraus ergibt sich, dass jeder Setartikel seine Verkaufspreis haben muss.
Wie löst man dieses Problem?
Bitte hinterlegen Sie bei den Setpositionen einen Verkaufspreis, also den Artikeln die Sie nicht herschenken.

Ein Beispiel:
Obige Meldung wird deswegen ausgegeben, weil zwar der Kopf des Artikelsets einen Verkaufspreis hat, aber keine der Setpositionen einen Verkaufspreis hat.
![](Nettodifferenz_zu_hoch1.gif)
Dadurch ergibt sich die volle Differenz zwischen SetKopf und SetPositionen.

Richtig wäre z.B.
![](Nettodifferenz_zu_hoch3.gif)
Was bedeutet, dass der gesamte Erlös nur auf den ersten Artikel der Setpositionen geht. Richtiger wäre natürlich die Erlöse / Verkaufswerte auf alle Setpositionen zu verteilen.

**Wie kann das praktisch abgefangen werden?**
Idealerweise hat jeder Artikel seine Verkaufspreisbasis. Damit wird bei der Übernahme des Sets und bei allfälligen Preiskorrekturen, des SetKopfes die Preisverteilungsberechnung automatisch von Ihrem **Kieselstein ERP** gemacht. Sind aber bei den Artikeln keine Verkaufspreise definiert, so können wir auch keine Verteilungen berechnen, wodurch es zu dem angeführten Verhalten kommt.<br>
**UND:**<br>
Natürlich sollte dies bereits vom Angebot weg richtig definiert sein.

#### Journal offene Rechnungen bringt Fehlermeldung.
![](Faelligkeitsberechnungsfehler.jpg)
Die Fälligkeit einer Rechnung konnte nicht berechnet werden.
Dies bedeutet, dass die Mahnstufen und oder die Zahlungsziele für diesen Mandanten nicht definiert sind und daher für die Erstellung der offenen Postenliste zum Stichtag keine Berechnung durchgeführt werden kann.
Zur Definition der Mahnstufen der Rechnung [siehe bitte](#Grunddaten).
Zur Definition der Zahlungsziele [siehe bitte.]( {{<relref "/verkauf/gemeinsamkeiten/#zahlungsziel"  >}} )

#### offene Rechnungen, Nicht zugeordnete Fibu-Belege mitandrucken
Wenn das Journal der offenen Rechnungen nach Kunden, bzw. Lieferanten bei den Eingangsrechnungen sortiert ausgedruckt wird, so können, bei integriertem Finanzbuchhaltungsmodul auch die sogenannten nicht zugeordneten Finanzbuchhaltungsbelege mit angedruckt werden. D.h. es werden in dieser offenen Postenliste aus Sicht des ERP Teiles, die in den Debitoren- bzw. Kreditorenkonten verbuchten Beträge, welche keinen Bezug zu den automatischen Belegen (Rechnung, Gutschrift, Eingangsrechnung, Zusatzkosten) haben aufgelistet. Dies dient vor allem Ihrer Sicherheit, dass alle Beträge gleichlautend verbucht sind und nicht eventuelle Vorauszahlungen ohne Beleg in der Jahresbetrachtung vergessen würden.
In diese Betrachtung werden nur diejenigen Geschäftsjahre mit aufgenommen, die noch nicht abgeschlossen sind, also ausgehend vom letzten = jüngsten abgeschlossenen Geschäftsjahr die nachfolgenden Geschäftsjahre.

<a name="Sammelrechnung"></a>

#### Sammelrechnung
Von einer Sammelrechnung spricht man dann, wenn mehrere Lieferscheine mit einer Rechnung abgerechnet werden.
Um zu einer bereits bestehenden Rechnung weitere Lieferscheine hinzuzufügen, gehen Sie in die Rechnungspositionen klicken auf neu und wählen als Positionsart Lieferschein. Klicken Sie nun auf Lieferschein und wählen Sie einen oder mehrere Lieferscheine aus, welche ebenfalls in die Rechnung aufgenommen werden sollten. **Hinweis:** Es werden nur Lieferscheine angezeigt, die die gleiche Rechnungsadresse haben.
Beachten Sie, dass nur Lieferscheine des gleichen Lieferlandes in eine Rechnung übernommen werden können.
Alternativ können auch direkt bei der Übernahme der Lieferscheine in die Rechnung mehrere Lieferscheine markiert werden. Achten Sie auch hier darauf, dass nur Lieferscheine mit gleicher Rechnungsadresse und gleichem Lieferland markiert sind.

#### Export einzelner Rechnungen
Es steht auch der Export einzelner Rechnungen zur Verfügung. Diesen finden Sie unter Rechnung, Export, Einzelrechnung.

Dieser Exportlauf ist für Systeme gedacht, an die praktisch parallel zum Druck auch Daten in das Buchhaltungssystem übergeben werden müssen.

Damit dieser Menüpunkt sichtbar ist, muss der Parameter EINZELRECHNUNG_EXPORTPFAD gesetzt sein. Dieser Pfad definiert, vom **Kieselstein ERP** Server aus gesehen, in welchen Pfad die Rechnung exportiert werden soll. Der Pfad wird um die Rechnungsnummer ergänzt, wobei der Schrägstrich / durch einen Unterstrich _ ersetzt wird.

Achten Sie bitte darauf dass:
- die Pfadangabe inkl. abschließendem Schrägstrich / Backslash ist und
- dass Ihre **Kieselstein ERP** Server auf diesen Pfad schreibende Rechte hat.

Wird der Exportpfad umgeschaltet, muss der **Kieselstein ERP** Client neu gestartet werden.

Für diesen CSV Export wird die Reportvorlage rech_rechnung_expo verwendet. D.h. hier können die exportierten Daten Ihren Bedürfnissen entsprechend angepasst werden.

Neben den in der Rechnung üblichen Feldern wird hier zusätzlich der Fibu_MwSt_Code übergeben. Dieser Code kann im System, Mandant, MwSt für den jeweiligen MwSt-Satz unter FIBU-MWST-Code gesetzt werden.

Hinweis: Als Zeichensatz wird für den Export nicht das übliche UTF-8 sondern ANSI verwendet.

Beim Export wird zugleich die Rechnung als in die Fibu exportiert gekennzeichnet und dieser Export auch in den Fibu-Exportlauf (siehe Fibu, Export) eingetragen. Eine als in die Fibu exportiert gekennzeichnete Rechnung kann nicht erneut exportiert werden. Sollten Änderungen an einer bereits exportierten Rechnung durchgeführt werden müssen, muss diese zuerst aus dem Fibu-Exportlauf herausgelöscht werden.

Die Daten werden im CSV Format im Zeichensatz UTF8 übergeben. D.h. die Felder sind mit Komma getrennt. Die genaue Formatierung / Anordnung ist im oben genannten Report definiert und kann dort auch abgeändert werden. Achten Sie dabei auf die auch hier geltenden Regeln für die Anwender Reportvorlagen.

Bei der Bearbeitung dieses Formulars achten Sie bitte unbedingt darauf, dass die Felder im Report exakt in der zu exportierenden Reihenfolge und in ausreichender Breite angegeben sind. Weiters ist enorm wichtig, dass alle Felder exakt ausgerichtet sind. D.h. Überschriften müssen exakt, auf das Pixel genau, zu den zugehörigen Feldern passen. Verwenden Sie dazu die Formating Tools um die Felder exakt gleich breit und linksbündig zu positionieren. 

Zusätzlich: Vom Reportgenerator wird immer die maximale Anzahl an Spalten exportiert. D.h. die Zeile mit den meisten Spalten definiert auch deren Anzahl.

<a name="Einzelrechnungsexport"></a>

#### Verdichteter Export einzelner Rechnungen
Zusätzlich steht auch der Einzelrechnungsexport (verdichtet) zur Verfügung. Bei diesem Export werden die Daten nach Artikelnummern sortiert ausgegeben. Die tatsächlichen Inhalte werden durch das Formular rech_rechnung_expo2 definiert.

#### Mietrechnungen
In manchen Unternehmen stellt sich die Frage, wie regelmäßige Mietrechnungen an Kunden zu stellen sind.

Hier kommt dazu, dass manche Unternehmen = Kunden darauf bestehen, dass nur eine Mietvereinbarung gemacht wird und damit werden automatisch die Zahlungen überwiesen. Hier kommt es nun manchmal zu der Praxis, dass diese Zahlungen direkt in der Buchhaltung als Mietenerlöse verbucht werden.

Dazu ist folgendes anzumerken:

Diese Vorgehensweise ist zwar aus Buchhalterischer Sicht richtig, aus Sicht eines ERP- bzw. CRM- Systems ist dies falsch, da die gesamten Umsatzstatistiken eines Kunden verloren gehen. Dieser Fall tritt insbesondere dann ein, wenn Mietvereinbarungen nicht nur mit einem Kunden bestehen. Zusätzlich kommt dazu, dass die automatisierte Überwachung der Mieteingänge, z.B. durch das Mahnwesen, so unmöglich ist und in entsprechende Handarbeit ausartet und von der Sorgfalt der handelnden Personen abhängig ist. Aus unserer Sicht gibt es dazu zwei mögliche Vorgehensweisen:
1. Es wird eine Jahresrechnung erstellt, in der die monatlichen Zahlungen als Zahlungsvereinbarung festgeschrieben werden.
2. Es wird ein wiederholender Auftrag angelegt und damit automatisch die Monatsrechnung(en) erzeugt.

Damit erhalten Sie die Basis für eventuelle Mahnungen,

Ihre Kunden und Artikel-Statistiken sind richtig und

wenn die Aufwände (vor allem die Zeitaufwände) auch auf den jeweiligen wiederholenden Auftrag gebucht werden, gewinnen Sie zusätzlich die erforderliche Deckungsbeitrags- und Erfolgsrechnung auf der jeweiligen Mietvereinbarung.

#### Monatsrechnungen
Mit dem Menüpunkt *Monatsrechnungen erstellen* steht eine komfortable Möglichkeit zur Verfügung, alle offenen Lieferscheine der Monatsverrechnungskunden automatisch zu verrechnen.

D.h. für die Kunden, bei denen im Reiter Konditionen Monatsrechnung angehakt ist, werden alle Lieferscheine in eine Rechnung je Kunde zusammengefasst eingetragen. Diese Funktion ist so gestaltet, dass solange eine Rechnung im gleichen Monat und im Status angelegt ist, werden die noch nicht verrechneten Lieferscheine hinzugefügt.

Ist die Rechnung im Status offen, oder höher, so wird eine neue Rechnung für den jeweiligen Kunden angelegt.

#### Warenausgangsjournal
<a name="Warenausgangsjournal"></a>
Im Rechnungsmodul steht unter dem Menüpunkt Journal, Warenausgang das Warenausgangsjournal zur Verfügung. Diese sehr umfangreiche Auswertung kann neben dem klassischen Warenausgangsjournal auch für folgende Auswertungen verwendet werden:
-   Provisionsabrechnung bzw. auch Vertreter-Provision
-   Beurteilung der eigenen Termintreue mit Begründung, siehe [Lieferschein]( {{<relref "/verkauf/lieferschein/#wo-kann-die-eigene-termintreue-ausgewertet-werden-">}} ).<br>
    ![](Eigene_Tremintreue.jpg)<br>
    In der Verteilung sehen Sie wieviele Ihrer Lieferungen Termingerecht bzw. mit Terminüberschreitungen an Ihre Kunden ausgeliefert wurden.<br>
    In der Auswertung der Termintreue werden nur Lieferungen die auf einer Auftragsbestätigung basieren berücksichtigt.

Für eine entsprechende Adaptierung der jeweiligen Formulare wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

#### [Provisionsabrechnung]( {{<relref provisionsabrechnung >}} )

#### Wird nun der Brutto- oder der Nettowert in der Auswahlliste angezeigt?
<a name="Brutto- oder-Netto in Auswahlliste"></a>
Das hängt davon ab.<br>
Je nach Anwender wird in manchen Fällen der Brutto-Betrag und in anderen Fällen der Netto-Betrag in der Auswahlliste benötigt. Daher haben wir den Parameter BRUTTO_STATT_NETTO_IN_AUSWAHLLISTE geschaffen. Ist hier 1 eingetragen so wird für (Ausgangs-)Rechnungen und Eingangsrechnungen der Bruttowert der Rechnung, also inkl. MwSt angezeigt. Ist hier 0 eingetragen, so werden die Nettowerte angezeigt.<br>
Nachdem die Einstellung geändert wurde, muss das jeweilige Modul neu gestartet werden.

#### Wieso wird der Kurs in den Kopfdaten in Rot angezeigt?
Diese Anzeige signalisiert, dass der in der Rechnung hinterlegte Wechselkurs zum aktuell in der Währungstabelle eingetragenen Umrechnungskurs abweicht.

#### Von welchem Lager bucht die Rechnung ab?
Analog zum [Lieferschein]( {{<relref "/verkauf/lieferschein/#von-welchem-lager-bucht-der-lieferschein-ab">}} ) bucht die Rechnung von dem in den Kopfdaten definierten Lager ab.

#### Lieferung erfolgt in ein anderes Land als die Rechnungslegung. Was ist dabei zu beachten?
Das Thema für diesen Fall ist, dass die Umsatzsteuergesetze auf die Lieferadresse abzielen.

D.h. es muss in diesem Falle:
- immer ein Lieferschein erstellt werden in dem die richtige Lieferadresse und die richtige Rechnungsadresse angegeben sind.
- Es muss für die Rechnungsadresse ein eigenes Debitorenkonto angelegt werden, da damit auch die andere Steuerkategorie für das andere Land definiert wird.

#### Zusammenfassende Meldung
<a name="Zusammenfassende Meldung"></a>
Unter Journal, Zusammenfassende Meldung finden Sie im Rechnungsmodul die für diese Finanzamtsmeldung erforderliche Auswertung für den von Ihnen angegebenen Zeitraum.
In dieser Auswertung sind nur diejenigen Rechnungen angeführt, bei deren Kunden die Länderart EU-Ausland mit UID Nummer definiert ist.
Bei der Zusammenfassenden Meldung gehen wir davon aus, dass Sie die Meldung in Ihrer Mandantenwährung abgeben. Sollte diese nicht dem Euro entsprechen, z.B. Schweiz, Liechtenstein, so stellen wir Ihnen gerne auch eine Formularversion zur Verfügung, mit der beim Ausdrucken die Beträge entsprechend in Euro dargestellt werden.

Da wir von unseren Anwendern immer wieder zu verschiedenen Spezialfällen der umsatzsteuerlichen Behandlung gefragt werden, haben wir einen kleinen [Exkurs dazu zusammengestellt]( {{<relref "/management/finanzbuchhaltung/intrastat/#umsatzsteuerrecht-und-eu-ausland-ig-lieferung">}} )

[Für Österreich siehe dazu auch](
https://www.usp.gv.at/steuern-finanzen/umsatzsteuer/umsaetze-mit-auslandsbezug/zusammenfassende-meldung-zm.html)

#### UID Nummer, Unterschied Schweizer UID, europäische UID, UstIdNr
Bitte unterscheiden Sie bei der Erfassung der UID Nummer ganz klar zwischen den UID bzw. UstIdNr der Mitgliedsländer der Europäischen Union und der Unternehmensidentifikations-Nummer der Schweizer Finanzbehörden. Im gesamten **Kieselstein ERP** ist wenn wir von einer UID Nummer sprechen/schreiben, immer die Umsatzsteueridentifikationsnummer für die Unternehmen der Europäischen Union gemeint. Leider hat sich in der Schweiz auch die, in Österreich schon seit langem übliche, Abkürzung UID eingebürgert, obwohl damit völlig andere Zusammenhänge gegeben sind.
[Siehe dazu auch.](https://www.nordschwarzwald.ihk24.de/international/aktuell/Schweiz--UID-Nummer-vorgeschrieben/2995406)
Hier kommt mit dazu, dass seit 1.1.2016 die davor zwischenzeitlich eingeführte EORI Nummer von den Schweizer Behörden durch die sofort zu verwendende Schweizer UID Nummer ersetzt wird. Das bedeutet, dass auf allen Rechnungen an Schweizer Kunden immer die Schweizer Unternehmensidentifikationsnummer anzuführen ist.<br>
Aufgrund der Schweizer Gepflogenheiten beim Import tragen viele **Kieselstein ERP** Anwender die Schweizer UnternehmensIdentifikationsnummer in der zweiten Zeile der Rechnungsadresse beim Kunden ein.

#### Mahnsperren
<a name="Mahnsperre"></a>
Es kommt immer wieder vor, dass Rechnungen strittig sind, oder einfach nicht zum geplanten Zahlungstermin tatsächlich fällig gestellt werden können. Sie haben daher die Möglichkeit, unter Bearbeiten, Mahnsperre bis eine entsprechende Sperre einzutragen.

![](Mahnsperre.jpg) Diese Sperre wirkt im Mahnwesen und in der Liquiditätsvorschau.
Zusätzlich kann hier auch noch ein Kommentar mit angegeben werden.
Dieser wird in den Mahnungen mit angezeigt. D.h. wenn kein Datum der Mahnsperre angegeben wird, so kann hier ein Hinweis hinterlegt werden, welcher beim jeweiligen Mahnlauf angezeigt wird.

Beachten Sie bitte, dass üblicherweise bei der dritten Mahnstufe [automatisch]( {{<relref "/verkauf/rechnung/mahnen/#k%c3%b6nnen-kunden-ab-einer-gewissen-mahnstufe-automatisch-gesperrt-werden">}} ) die [Liefersperre]( {{<relref "/docs/stammdaten/kunden/#setzen--l%c3%b6schen-von-liefersperren">}} ) gesetzt wird.

#### Zahlungsplan
Zusätzlich zur Mahnsperre gibt es auch immer wieder den Fall, dass Rechnungen in monatlichen Raten zu einem bestimmten Stichtag bezahlt werden. Um dies zu definieren verwenden Sie bitte Extras, Zahlungsplan.<br>
![](Zahlungsplan1.gif)    ![](Zahlungsplan2.gif)<br>
Geben Sie hier den geplanten monatlichen Zahlbetrag ein und auch den Tag des Monates an dem Sie die Zahlung erwarten.<br>
Der Zahlungsplan ist rein für die Liquiditätsvorschau gedacht und hat sonst keine Auswirkungen.
Ist ein Zahlungsplan eingetragen, so wird diese Planung für die Liquiditätsvorschau verwendet.

#### Ausfuhrinformationen
Aus den Daten der Ausgangs-Rechnung können jederzeit auch kompakte und verdichtete Ausfuhrinformationen mit Angabe von Warenverkehrsnummer, Stückzahlen, Werte, Gewicht gedruckt werden. Für die Einrichtung des Formulars (rech_ausfuhrdaten) wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer. Für den Andruck des Gewichtes muss für den Kunden in den Konditionen "Am LS Gewicht angeben" eingeschaltet sein.

#### Werbeabgabe
<a name="Werbeabgabe"></a>
Werden in Österreich Inserate, Werbeanzeigen geschaltet, so ist dafür eine eigene Abgabe, also Steuer, zu entrichten. Die Besonderheit ist, dass auf diese Abgabe zusätzlich Mehrwertsteuer zu entrichten ist.

Die Werbeabgabe ist selbst zu berechnen und bis zum 15\. des zweitfolgenden Monats, nach Entstehen des Abgabenanspruchs, an das für die Umsatzsteuer zuständige Finanzamt zu entrichten. Oder vereinfacht gesagt, gemeinsam mit der österreichischen Umsatzsteuer.

Die Werbeabgabe wird bei Aktivierung der (Ausgangs-) Rechnung von **Kieselstein ERP** automatisch errechnet, wenn in der Rechnung Werbeabgabepflichtige Artikel enthalten sind.

Ob ein Artikel Werbeabgabepflichtig ist, wird im Artikelstamm, Reiter Sonstiges, Werbeabgabepflichtig definiert.

Um die Werbeabgabepflicht zu bestimmen müssen die Parameter definiert sein.
|Parameter | Beschreibung |
| --- |  --- |
| WERBEABGABE_ARTIKEL | Dieser Artikel wird als Werbeabgabe in die Rechnung mit übernommen.Für die Integrierte Finanzbuchhaltung ist darauf zu achten, dass die Artikelgruppe auf das Abgabenkonto der Werbeabgabe zeigt. Achten Sie bei der Definition auch darauf, dass die Werbeabgabe nur für österreichische Werbeeinschaltungen zu leisten ist. Wir empfehlen für Werbeeinschaltungen im Ausland andere Artikel als im Inland zu verwenden und zur Sicherheit die Länderartübersetzung für das Konto der Werbeabgabe **NICHT** zu definieren. |
| WERBEABGABE_PROZENT | Der Steuersatz mit dem die Bemessungsgrundlage zu versteuer ist in %. |

Bei der Aktivierung einer Rechnung, also beim Ausdrucken, wird die Werbeabgabe für alle abgabenpflichtigen Artikelpositionen errechnet.

Dieser Betrag wird mit dem Werbeabgabe Artikel als letzte Position in der Rechnung angeführt.

Wird eine Rechnung verändert, so wird bei der neuerlichen Aktivierung ein eventuell vorhandener Werbeabgabeartikel entfernt und wenn Werbeabgabepflichtige Artikel enthalten sind, erneut der Werbeabgabeartikel als letzte Position in die Rechnung eingefügt.

#### Kann ich auch bei einer verbuchten/bezahlten Rechnung den Vertreter noch ändern?
Wenn es zum Beispiel im Rahmen der Provisionsabrechnung auffällt, dass bei einer Rechnung der falsche Vertreter angegeben ist, so finden Sie in der Rechnung unter Bearbeiten den Menüpunkt Vertreter ändern. Wählen Sie die gewünschte Rechnung aus, klicken auf den Menüpunkt Vertreter ändern und wählen den gewünschten Vertreter aus.

#### Warum werden beim Kopieren von Rechnungen nicht alle Artikel übernommen?
Wenn eine Rechnung per *Daten aus bestehender Rechnung übernehmen* kopiert wird, werden alle Positionen, die auftragsbezogen sind, ausgelassen. Da diese Positionen schon im vorhergehenden Auftrag verrechnet wurden, werden diese nicht noch einmal in die neue Rechnung übernommen.

#### Kann ich erkennen, ob die Mehrwertsteuersätze von der Definition im Kunden abweichen?
Um Sicherheit, vor allem bei der Verbuchung in der Finanzbuchhaltung, zu schaffen, wird eine Fehlermeldung ausgegeben, wenn die Mehrwertsteuersätze aus dem Kunden nicht mit den Mehrwertsteuersätzen aus der Rechnung übereinstimmen.  Die Situation kann durch Kopieren von Rechnungen zb. eines Kunden aus dem Inland und dem anschließenden Abändern in einen Kunden aus dem Ausland vorkommen. Sollte eine, der folgenden Fehlermeldungen bei Ihnen auftreten, so überprüfen Sie bitte die Mehrwertsteuersätze der Positionen und nehmen gegegebenfalls die Änderungen gemäß der Definition im Kunden vor.

![](mwst_warnung2.JPG) ![](mwst_warnung.JPG)

#### Welche Beträge werden verwendet, wenn eine Rechnung aus einer Rechnung in Fremdwährung erstellt wird?
Wenn eine Fremdwährungsrechnung kopiert wird, bleiben die damaligen Fremdwährungspreise erhalten. 

In den Kopfdaten kann die Währung nur mehr dann geändert werden, wenn keine mengenbehafteten Positionen vorhanden sind.

#### Wie argumentiert man 0 Preise in der Rechnung für das Finanzamt?
Oftmals werden Positionen zu dem Zeitpunkt verrechnet, bei dem ein Lieferschein an das Kundenlager geschrieben wurde. Der Abruf aus dem Kundenlager jedoch wird nicht mehr verrechnet. So kann es vorkommen, dass auf einer Rechnung Positionen von unterschiedlichen Lagern abgebucht werden. Somit sind auf der Rechnung Positionen mit Preisen und ohne Preisen gemischt vorhanden. Zur klaren Dokumentation für das Finanzamt und Nachvollziehbarkeit für Ihren Kunden, kann das Abbuchungslager angedruckt werden.

#### Sicht Auftrag und Lieferschein
Die Reiter Aufträge, Sicht Auftrag und Sicht Lieferschein verhalten sich wie folgt:
- "Sicht Lieferschein" wird aktiv (klickbar) sobald eine Lieferschein-Position selektiert wird. Diese Aktivierung bleibt aufrecht, solange die Selektion einer Lieferschein-Position nicht aufgehoben wird.
- Reiter "Aufträge" und "Sicht Auftrag" sind aneinander gekoppelt. Der Reiter "Aufträge" wird grundsätzlich aktiviert, wenn eine Rechnung ausgewählt wurde, die mindestens einen Auftrag beinhaltet. "Sicht Auftrag" ist klickbar, wenn ein Auftrag selektiert wurde.

Man kann dabei drei Fälle unterscheiden:
1. Rechnung hat keinen Auftrag: "Aufträge" und "Sicht Auftrag" deaktiviert.
2. Rechnung hat nur einen Auftrag: "Aufträge" ist aktiviert, der einzig vorhandene Auftrag wird vorselektiert. Somit ist auch "Sicht Auftrag" automatisch aktiviert.
3. Rechnung hat mehr als einen Auftrag: "Aufträge" ist aktiviert, kein Auftrag vorselektiert. Somit ist auch "Sicht Auftrag" deaktiviert, bis ein Auftrag selektiert wird.

#### Beim Druck der Schlussrechnung werden die bereits bezahlten Beträge NICHT angedruckt, warum?
Wird die Schlussrechnung angedruckt, so steht in den Formularen auch die Möglichkeit zur Verfügung, dass neben den verrechneten Anzahlungsbeträgen auch die bezahlten Beträge auf der Anzahlungsrechnung gedruckt werden.<br>
Hierbei ist wichtig, dass bei der Berücksichtigung der Zahlbeträge das Datum der Schlussrechnung berücksichtigt wird. D.h. es werden nur diejenigen Zahlbeträge der Anzahlungsrechnungen mit in die Aufstellung der bereits geleisteten Zahlungen (auf den Anzahlungsrechnungen) mit aufgenommen, deren Datum vor dem Datum der Schlussrechnung ist.

<a name="Zufallsnummer"></a>

#### Die Kunden sollten nicht erkennen, dass ich fast nur für einen einzigen Kunden arbeite?
Manche unserer Anwender arbeiten oft längere Zeit an Projekten die fast ausschließlich für einen Kunden sind. Nun will man oft nicht, dass der Kunde erkennt dass dem so ist, man würde unter Umständen erpressbar werden. Um dieses Problem zu lösen haben wir den Parameter BELEGNUMMERNFORMAT_STELLEN_ZUFALL geschaffen.<br>
Dieser bedeutet, dass die letzten Stellen der Rechnungsnummer anhand eines Zufallszahlengenerators erzeugt werden. Somit haben Sie einerseits das Erfordernis der Finanzverwaltungen nach fortlaufenden Rechnungsnummern abgedeckt und andererseits erkennt Ihr Kunde nicht, dass eigentlich zwischen der Rechnungsnummer 17/012345 und der 17/012401 keine weitere Rechnung geschrieben wurde (In dem Beispiel steht der BELEGNUMMERNFORMAT_STELLEN_ZUFALL auf 2). Sie müssen lediglich der Finanzverwaltung bei einer eventuellen Prüfung mitteilen, dass die letzten zwei Stellen unrelevant sind. Bitte beachten Sie, wenn die Zufallszahlen aktiviert sind, wirken diese in den Modulen Angebot, Auftrag, Lieferschein, Rechnung, Gutschrift und Proformarechnung.

#### Rechnungen erledigen
Je nach Ausprägung der Nutzung der Zahlungsverwaltung bzw. des Mahnwesens ist es auch immer wieder erforderlich, dass Rechnungen manuell erledigt werden.<br>
Das bedeutet, bei **Kieselstein ERP** Installationen OHNE integrierter Finanzbuchhaltung steht auch die Möglichkeit zur Verfügung, dass Rechnungen manuell erledigt werden. Wählen Sie dazu im Menü den Punkt bearbeiten und dann Manuell erledigten.
Den nun erscheinenden Dialog
![](RE_manuell_erledigen.jpg)
Beantworten Sie bitte entsprechend.
Bitte beachten Sie, dass damit, ohne Zahlungsbuchung die Rechnung vollständig erledigt wird, egal in welchem Teilzahlungs-Status sie vorher war.

Sollten mehrere Rechnungen auf einmal erledigt werden, so klicken Sie auf mehrere und wählen aus der nun erscheinenden Rechnungsliste der offenen Rechnungen die zu erledigenden Rechnungen mittels Multiselekt aus. Klicken Sie nach der Auswahl auf den grünen Haken ![](RE_manuell_erledigen2.jpg).

<a name="Statistikadresse"></a>

#### Rechnungsadresse, Statistikadresse?
Es kommt immer wieder vor, dass die Verrechnung an zentrale Stellen erfolgen muss. Z.B. bei Einkaufsgemeinschaften, bei Vereinbarungen mit Gebietshändlern usw.. Da aber der Händler, die Einkaufsgemeinschaft in aller Regel nur dann Ware bei Ihnen bestellt, wenn ein Bedarf beim Endkunden gegeben ist, müssen Sie sich aus vertrieblicher Sicht um den Endkunden kümmern. Daher wird bei der Überleitung eines Lieferscheines in die Rechnung die Lieferadresse als Statistikadresse vorgeschlagen. Für die Auswertungen kann dann auf die alternative Auswertung anhand der Statistikadresse zurückgegriffen werden.

Um die Statistikadresse nachträglich anzupassen, nutzen Sie bitte im Menü, Bearbeiten, Statistikadresse ändern.

<a name="Setartikel"></a>

#### Behandlung von Setartikeln
In der Verrechnung werden Setartikel nach deren Positionswerten behandelt.
D.h. ausgehend vom Wert des Setartikel Kopfes, also des eigentlichen Setartikels, wird anhand der Verkaufspreise der einzelnen Positionen der Wert des Kopfes auf die Position verteilt.
Dies bewirkt auch, dass es unbedingt erforderlich ist, dass für jeden Artikel des Setartikels, also auch die Positionen, ein zumindest theoretischer Verkaufspreis gegeben sein muss.

Warum ist dies so?<br>
Es müssen einerseits die Setartikelpositionen einen Wert in der Warenausgangsstatistik haben, damit auch in der Artikelstatistik und in der Kundenstatistik und bei integrierter Fibu kann es durchaus so sein, dass jeder Setartikelposition ein anderes / abweichendes Erlöskonto hat. Dafür muss die Verteilungsberechnung gemacht werden und um diese Berechnung machen zu können, muss die jeweilige Setartikelposition auch einen Verkaufspreis haben.<br>
Da für die umsatzsteuerliche Betrachtung des Setartikels die so genannte Hauptleistung ausschlaggebend ist, wird für die Berechnung des Umsatzsteuerbetrages der dem (Kopf-) Setartikel zugeordnete Steuersatz herangezogen. Die Mehrwertsteuersätze der Unterartikel werden nicht berücksichtigt.<br>
Diese Vorgehensweise wird sowohl für die Überleitung in die integrierte Finanzbuchhaltung als auch für die Erzeugung der Fibu-Exportdaten verwendet.<br>
In kurzen Worten: Für die Mehrwertsteuer ist der Set-KopfArtikel ausschlaggebend. Die Erlöse kommen aus den Erlöskonten der Set-Positionen.

#### Swiss QR-Rechnung?
Für unsere Schweizer und Liechtensteiner Kunden, aber auch alle anderen, steht auch der Andruck des QR-Zahlscheins zur Verfügung.<br>
Wichtig sind dafür folgende Einstellungen:
- Anpassung des Rechnungsformulars, sodass der QR-Zahlschein angedruckt wird
- Hinterlegen der eigenen Bankverbindung unter Finanzbuchhaltung, unterer Modulreiter Bankverbindungen, Kopfdaten, IBAN
    Hinweis: Derzeit ist dafür nur eine Bankverbindung vorgesehen. Für eigene unterschiedliche Bankverbindungen, z.B. Währungsabhängig, ist der Rechnungsdruck entsprechend anzupassen.
- Währung des Zahlungsempfängers, also der Rechnung nur EUR oder CHF

Voraussetzungen dafür:
Ein Drucker der den QR-Code in ausreichender Auflösung ausdrucken kann.

#### Es ist für den Druck des QR-Codes alles eingerichtet, es wird aber kein QR Code gedruckt?
Vermutlich ist die Bankverbindung nicht richtig definiert, bzw. keine IBAN angegeben.

#### Mindermengenzuschlag?
Wird gerne auch für Versandkosten verwendet.<br>
[Siehe dazu bitte]( {{<relref "/docs/stammdaten/kunden/#mindermengenzuschlag">}} )

#### Indirekter Materialeinsatz
In verschiedenen Organisationen ist es wichtig zu wissen, welches Rohmaterial für welche Rechnung denn nun wirklich verwendet wurde.<br>
Nutzen Sie dafür im Modul Rechnung den Menüpunkt Info, Materialeinsatz ![](Materialeinsatz.gif).
Hier werden detailliert alle Positionen aufgeführt welche für die Produktion der Artikel dieser Rechnung erforderlich waren. Zusätzlich wird am Ende noch eine Zusammenfassung angezeigt.
Sollte hier kein Materialeinsatz auf Lose angezeigt werden, so wurde auch kein Material in die jeweiligen Lose gebucht. Für das Nachbuchen von Material [siehe]( {{<relref "/fertigung/losverwaltung/material_nachbuchen">}} ).

#### Nachkalkulation über den indirekten Materialeinsatz
In einigen Organisationen ist die Nachkalkulation über den Auftrag oft nicht möglich, da z.B. sehr viel auf Zuruf geliefert wird.
Wir nutzen dafür den Indirekten Materialeinsatz. Haken Sie hier die ![](Materialeinsatz_Nachkalkulation.gif) Nachkalkulation an um eine entsprechende Deckungsbeitragsauswertung sowohl auf der einzelnen Rechnungsposition als auch auf die gesamte Rechnung zu ermitteln.

#### Deckungsbeitrag einer Handelsrechnung
Gerade wenn Produkte völlig ohne Auftrag, man könnte sagen, auf Zuruf abgewickelt werden, will man trotzdem wissen, wieviel denn am Schluss übrig geblieben ist, also welchen Deckungsbeitrag man erzielt hat. Die Basis dafür ist die Materialeinsatzbetrachtung. D.h. diese Auswertung finden Sie im Rechnungsmodul Info, Materialeinsatz.

### Andruck Warenerklärung, IG-Lieferung
#### Andruck IG-Lieferungstext
Dieser Text wird angedruckt, wenn die Rechnung keine Reverse Charge hat und
- wenn ohne Lieferschein an das EU-Ausland geht und der Kunde eine UID Nummer eingetragen hat
- wenn mit Lieferschein so wird diese Entscheidung anhand der Lieferadresse getroffen. D.h. die Lieferadresse muss im EU Ausland sein und eine UID Nummer eingetragen haben.
**Hinweis:** Ist bei der Lieferadresse keine UID Nummer eingetragen, ist das auch keine IG-Lieferung, es wird also dieser Text <u>NICHT</u> angedruckt.

#### Tourismusverband
Nur im Bundeslandsalzburg gibt es für alle Unternehmen eine Tourismusabgabe jährlich zu entrichten.<br>
Die Idee scheint zu sein, dass es nur um die Umsätze innerhalb des Bundeslandes geht.<br>
Es baut diese Abgabe auf der Umsatzsteuererklärung auf, von der man dann die IG-Lieferungen, die Drittlandslieferungen und auch die Lieferungen die an Kunden außerhalb des Bundeslandessalzburg gehen, abziehen kann. Für diese Abzugsauswertung, gibt es im Modul Rechnungen, Journal, Alle, die Reportvariante Tourismusverband, mit der dieser Inlands-Umsatz außerhalb des Postleitzahlengebietes 5 ausgewiesen wird.<br>
![](Rechnungsjournal_alle_Rechnungen.png)<br>
Wählen nun<br>
![](Tourismusverband.png)
- die Reportvariante Tourismusverband
- den gewünschten Zeitraum (üblicherweise das vor-vorige Jahr)
- ev. sortierst du die Auswertung gleich nach Kunden

Danach mit der Diskette speichern als PDF Datei, da du dies als Beilage bei deiner Tourismusabgabenerklärung mit dazugeben musst.

---
## Wie ist bei einer Rechnungskorrektur vorzugehen?
Es kommt immer wieder vor, dass eine Rechnung zu korrigieren ist.<br>
Gerade kleine Unternehmen tendieren dazu, einfach die Rechnung neu auszustellen und diese unter der gleichen Rechnungsnummer erneut an den Kunden zu senden.<br>
Nun muss hier auch die Seite der Umsatzsteuer berücksichtigt werden. Es ist in der Praxis schon vorgekommen, dass bei einer allfälligen Steuerprüfung der Prüfer der Meinung war, dass, da die Rechnung zwar unter der gleichen Rechnungsnummer ausgestellt war, aber dies eben zwei (Papier) Ausgangsrechnungen waren, die Umsatzsteuer auch für die korrigierte Rechnung, also doppelt abzuführen ist.<br>
Um dies zu vermeiden, bringen Sie bitte einen deutlichen Hinweis an. [Siehe dazu auch](https://www.stb-karl.at/at/news/klientennews/oktober_2022/was_ist_bei_der_rechnungskorrektur_in_der_umsatzsteuer_zu_beachten_/index.html).

## Swiss QR-Code auf Ausgangsrechnungen
Hier wird default die Bankverbindung aus System, Vorbelegungen verwendet.<br>
Es kann dies gerne je nach Währung eingerichtet werden. Beachten Sie bei mehreren Bankkonten, dass hier nur die Bankverbindungen berücksichtigt werden, bei denen In Liquiditätsvorschau (berücksichtigen) mit angehakt ist. Für den Druck des QR-Codes ist die BIC erforderlich.

**Hinweis:**<br>
In der aktuellen Version ist das Unterdrücken der QR-Codes für die Kopien leider nicht möglich.