---
categories: ["Rechnung"]
tags: ["Provisionsabrechnung"]
title: "Rechnung"
linkTitle: "Provisionsabrechnung"
weight: 100
date: 2023-01-31
description: >
  Provisionsabrechnung
---
Provisionsabrechnung
====================

Die Provisionsabrechnung ist oft ein sehr komplexes Thema. Da dies in jedem Unternehmen anders gehandhabt wird, ist es erforderlich die Auswertung dafür an Ihre Bedürfnisse anzupassen. Da eine Provisionsabrechnung unserer Meinung nach nur auf gelieferten Waren beruhen kann, wurde diese in das Warenausgangsjournal integriert.
D.h. um eine Provisionsabrechnung zu erhalten, wählen Sie bitte im Modul Rechnungen, Journal, Warenausgangsjournal.
Nun schalten Sie die Reportvariante auf Provisionsabrechnung bzw. Report 2 um.

Hiermit erhalten Sie eine mögliche Auswertung für Ihre Provisionen.
In aller Regel ist eine Anpassung dieses Formulars an Ihre tatsächliche Handhabe zur Provisionierung erforderlich.<br>
Grundsätzlich stehen folgende Daten zur Verfügung.
-   Auswertung nach Vertreter ab Basis der Rechnung
-   Auswertung nach gelegten Rechnungen und Gutschriften im Zeitraum
-   Verkaufswert (Umsatz) je Position mit Wert(en)
-   Deckungsbeitrag je Position
-   Artikel-Provisionsfaktor je Position
-   Kunden-Provisionsfaktor
-   Vertreter-Provisionsfaktor

Ob nun Ihre Provisionsabrechnung auf Umsätzen oder auf Deckungsbeiträgen aufbaut, ob diese für bestimmte Kunden und oder Artikel begrenzt ist, muss im Formular entsprechend eingestellt werden.
Da in aller Regel, der tatsächliche Provisionsbetrag für den Vertreter nur der Geschäftsleitung und dem jeweiligen Verkaufsmitarbeiter bekannt ist, wird von **Kieselstein ERP** nur die Basis für die Ermittlung des eigentlichen Vertreter Provisionsbetrages berechnet. Die Multiplikation dieses Provisions-Basisbetrages wird dann von der Lohnverrechnung oder einer ähnlichen Stelle vorgenommen.

Provisionen an externe Partner
------------------------------

Müssen für die Vermittlung von Geschäften Provisionen an externe Partner bezahlt werden, so handelt es sich in diesem Falle um einen Vermittlungsaufwand. D.h. eigentlich müsste Ihnen Ihr Partner eine Provisionsrechnung senden und Sie erfassen dies als Eingangsrechnung und bitte im Aufwand (und nicht wie manche im Minder-Erlös). In manchen Branchen wird diese Provision auch Kommission genannt. (Hier der Hinweis, dass Kommission die Zuordnung zu einem Kundenprojekt bedeutet.)

Nun ist es oft so, dass Ihr Partner sich einfach das Geld auf seinem Konto erwartet. Dass er da nun auch noch eine Rechnung schreiben müsste ist ihm viel zu mühsam.
Daher haben wir die Möglichkeit der sogenannten [Ausgangsgutschrift]( {{<relref "/einkauf/eingangsrechnung/ausgangsgutschriften"  >}} ) geschaffen.