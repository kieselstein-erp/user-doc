---
categories: ["Rechnung"]
tags: ["Zeitnachweis"]
title: "Rechnung"
linkTitle: "Zeitnachweis"
weight: 60
date: 2023-01-31
description: >
  Zeitnachweis erstellen und versenden
---
Zeitnachweis
============

Im Modul Rechnung, unterer Modulreiter Zeitnachweis steht ein praktisches Werkzeug zur Verfügung, um Ihre Kunden über die für die verschiedensten Projekte aufgewendete Zeit zu informieren.
Diese Funktion ist dafür gedacht, dass diese Informationen täglich, per EMail, an Ihre Kunden gesandt werden.

![](Zeitnachweis1.jpg)

Beim ersten Aufruf steht der Zeitbereich auf dem gestrigen Tag und für alle Bereiche Ihrer Projekte.
Somit haben Sie eine Übersicht, was gestern gemacht wurde. Genauer über die Zeitbuchungen die im gewählten Zeitraum auf die verschiedenen Projektbereiche durchgeführt wurden.
Hier ist auch der Grundgedanken, dass aus den Zeitbuchungen die Felder Bemerkung und E: = Externer Kommentar an den Kunden kommuniziert werden.
Das Feld I: Interner Kommentar ist für Ihre Information bestimmt.
Weiters wird der Projekttitel mit angedruckt, sofern er länger als 5 Zeichen ist.

Um die angezeigten Buchungen zu ändern / zu korrigieren stehen verschiedene Möglichkeiten zur Verfügung.
![](Zeitnachweis5.gif) ... Goto Zeitbuchung. Damit springen Sie direkt auf die ausgewählte Zeitbuchung
![](Zeitnachweis2.gif) ... Goto Projekt. Damit springen Sie direkt auf das ausgewählte Projekt
![](Zeitnachweis3.gif) ... Durch Klick auf den linken Textbearbeitungsbutton kann direkt der externe Kommentar bearbeitet werden.
                  Mit Klick auf den rechten Textbearbeitungsbutton kann direkt der interne Kommentar bearbeitet werden.

Um nun die Zeitnachweise, welche gerne auch als Leistungsnachweis bezeichnet werden, auszudrucken, markieren Sie bitte die gewünschten Zeilen (Multiselekt) und klicken auf das Druckersymbol. Der Druck ist vor allem dafür gedacht, dass eine Vorschau der Informationen gegeben ist, welche dann direkt an Ihren Kunden gesandt wird.
Um auch eventuellen den EMail-Text einzusehen, markieren Sie bitte nur eine Zeile und klicken dann ebenfalls auf Drucken. Im Druckdialog wählen Sie dann, wie in **Kieselstein ERP** üblich, das EMail Symbol. Hier sehen Sie dann den Text der im EMail an Ihre Kunden gesandt wird.

Um alle markierten Zeilen zu versenden, nutzen Sie bitte direkt den EMail Knopf.
Welche EMail Adressen werden verwendet?
1. die Rechnungs-Empfänger EMail Adresse aus dem Kunden, Kopfdaten<br> ![](Zeitnachweis4.gif)<br>Ist hier nichts hinterlegt, wird
2. die allgemeine EMail-Adresse des Kunden verwendet. Ist diese nicht gegeben,
3. so fehlt auch das EMail Symbol in der Spalte. Ergänzen Sie dazu bitte die Daten entsprechend.

#### Wie werden fehlerhafte Zeitdaten angezeigt?
Sind Zeitdaten unvollständig, z.B. bei fehlenden Geht, ungültigen Unterbrechungen u.ä. werden diese Zeilen in Rot dargestellt und haben auch keine Dauer eingetragen

#### Wie mit abweichenden Zeiten umgehen?
Natürlich kommt es immer wieder vor, dass manche Zeiten in einer anderen Form, als tatsächlich aufgelaufen, an den Kunden kommuniziert werden sollten.
Dafür steht, direkt im Detailbereich die Möglichkeit diese Dauer mit einer abweichenden Zahl zu übersteuern zur Verfügung.
Diese Dauer wird sowohl dem Druck bzw. EMail-Versand übergeben als auch im Abrechnungsvorschlag entsprechend als abweichende Dauer behandelt.