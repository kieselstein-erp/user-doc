---
categories: ["Rechnung"]
tags: ["Elektronische Rechnung"]
title: "Rechnung"
linkTitle: "eRechnung"
weight: 80
date: 2023-01-31
description: >
  eRechnung
---
# Anforderungen im Rahmen der eRechnung

## Verpflichtend
Die Verwendung der eRechnung ist, zumindest in Deutschland für inländische Rechnungen ab 1.1.2025 auch im B2B eingehend verpflichtend. Ausgehend, ab 1.1.2026.

Für Kleinstunternehmer (< 800.000,- €) gibt es bis Ende 2027 entsprechende Übergangsregelungen.

Die eRechnung muss ausgestellt werden können und auch eingelesen und weiterverarbeitet werden können.

Es geht hier um den B2B Bereich, also zwischen Unternehmen.<br>
Der Bereich B2G also Rechnungen an den (deutschen) Bund ist schon länger geregelt.

Das betrifft damit folgende Bereiche:
- Ausgangsrechnung
- Gutschrift / Rechnungskorrektur
- [Eingangsrechnung]( {{<relref "/einkauf/eingangsrechnung/er_erechnung/">}} )

Anmerkung1:<br>
Das Ziel ist vor allem, so wie in anderen europäischen Ländern auch, den Mehrwertsteuerbetrug einzudämmen.

Anmerkung2:<br>
In Deutschland als auch in Österreich ist bei Geschäften mit dem Bund bereits seit einigen Jahren die eRechnung vorgeschrieben.

- Für Deutschland siehe [WACHSTUMSCHANCENGESETZ](https://www.bundesfinanzministerium.de/Monatsberichte/Ausgabe/2024/04/Inhalte/Kapitel-3-Analysen/3-4-wachstumschancengesetz.html)
  - oder auch [VeR](https://www.verband-e-rechnung.org/news/verbandsnachrichten/verpflichtende-e-rechnung-ab-2025-ist-beschlossene-sache/)
  - bzw. [E-Rechnung Bund](https://www.e-rechnung-bund.de/faq/xrechnung/) 
- Für Österreich siehe [WKO eRechnung](https://www.wko.at/digitalisierung/fragen-und-antworten-zur-e-rechnung)
- Generell [Wikipedia](https://de.wikipedia.org/wiki/Elektronische_Rechnung)

Anmerkung3:<br>
Auch für die eRechnung gelten die gesetzlichen Aufbewahrungspflichten (10Jahre, AT 7Jahre). Bitte berücksichtige das bei deinen Backupstrategien.

## Normen
Die eRechnung muss der internationalen Norm EN 16931 entsprechen.

## Sammlung von Links
- [Verband elektronische Rechnung](https://www.verband-e-rechnung.org)
- [Bund Deutschland](https://www.e-rechnung-bund.de/standard-xrechnung-3-0-1/)
- [Invoice Portal](https://invoice-portal.de/rechnungsformate/xrechnung-beispiel/)
- [30Tage Test](https://einfach-xrechnung.de/)
- [Spezifikationen für ZUGfERD](https://www.ferd-net.de/standards/zugferd-2.3/zugferd-2.3.html)

## Formate
Es gibt im wesentlichen folgende Formate:
- ZUGfERD 2.3
- eRechnung
- xRechnung

Für den Rechnungsversand wird von **Kieselstein ERP** das ZUGfERD 2.3 Format verwendet.

### Wie eine ZUGfERD Rechnung speichern?
Wenn du z.B. per EMail eine ZUGfERD Rechnung bekommst, so ist es ein wesentlicher Unterschied ob du die original Datei abspeicherst oder diese zuerst in deinem PDF Viewer (egal ob aus dem Adobe Acrobate Reader, oder den in den Browsern integrierten PDF-Betrachtern) anzeigst und von da aus speicherst.<br>
Da der der PDF-Viewer für die optische Anzeige die in der original Datei enthaltenen XML Informationen nicht benötigt, verwirft dieser anscheinend diese Daten bereits beim Import.<br>
D.h. wenn du nun das (reine) PDF mit Datei speichern unter abspeicherst, so fehlen in diesem die eingebetteten Daten, womit das Zugferdformat nicht mehr erkannt wird. D.h. in der Regel musst du die Originaldatei deines Lieferanten in das Drag & Drop Einlesefeld deiner Eingangsrechnung übernehmen.

Für das Abspeichern des im PDF eingebetteten Inhaltes [siehe]( {{<relref "/docs/stammdaten/system/drucken/#was-bedeutet-zugferd-versand">}} )

### unterstützte Mengeneinheiten
Beim Export der Ausgangsrechnungen im ZUGfERD Format EN16931 = ZUGfERD 2.3 müssen auch die Mengeneinheiten der einzelnen Positionen Norm-Konform verwendet werden.

[Die Details dazu sind unter](https://www.awv-net.de/upload/ferd/ZF23_DE.zip) [bzw. unter](https://www.ferd-net.de/publikationen-produkte/publikationen/detailseite/zugferd-232-deutsch) zu finden.<br>
Hier sind in der Datei 3. FACTUR-X 1.0.07 FR-EN.xlsx im Tabellenblatt Codelists alle gültigen Internationalen Mengeneinheiten aufgeführt.<br>

Das bedeutet, dass diese Mengeneinheiten in deinen Mengeneinheiten definiert werden müssen. Siehe dazu die ab 1.0.10 verfügbare Erweiterung der Definition der Einheiten unter System, Sprache, Einheit, Zugferd Einheit. [Siehe dazu auch](#die-einheit-stk-konnte-nicht-ins-zugferd-format-übertragen-werden)

[Weitere Einheiten siehe](./3.%20FACTUR-X%201.0.07%20FR-EN.xlsx)

### Umfang der ZUGfERD Implementierung
Vom Grundgedanken her, wird als Format das ZUGfERD 2.3 für die Inhalte der XML-Datei (factur-x) verwendet. Es wurden jedoch nur die unbedingt notwendigen Felder / Entitäten implementiert. Insofern ist von einer minimal Implementierung auszugehen.

Weitere Funktionalitäten können gerne integriert werden. Diese werden nach Definition Programmtechnisch umgesetzt.

#### Bankverbindung im ZUGfERD
Um deine Bankverbindung an deinen Kunden über die ZUGfERD-PDF Datei weiter zu reichen, muss diese Bank vollständig im System hinterlegt werden. D.h.
1. Definition deiner (Haus-)Bank unter Partner, Banken<br>Achte hier auf die Definition inkl. gültiger BIC
2. Anlegen eines Sachkontos für deine(Haus-)Bank im Reiter Sachkonten des Moduls Finanzbuchhaltung.
3. Definition der eigenen Bankverbindung (die du für die Zahlungsverwaltung sowieso brauchst) im unteren Modulreiter Bankverbindung im Modul Fibu, mit der Verbindung von Bank und Sachkonto und Angabe der richtigen IBAN.
4. Hinterlegen der bevorzugten Bankverbindung im Modul System, unterer Reiter Mandant, oberer Reiter Vorbelegung.

#### Zahlungsziel
Als Fälligkeit wird immer das netto Zahlungsdatum übergeben und Skonto usw. als Text mit in den XML Teil der ZUGfERD-Rechnung geschrieben.

#### Bestellnummer
Diese wird, wenn vorhanden, mit in den XML Teil der ZUGfERD-Rechnung geschrieben.

#### Datums
Liefer(end)datum und Rechnungsdatum entsprechen dem Rechnungsdatum.

#### Besondere Rechnungsarten
wie z.B. Anzahlungs- und Schlussrechnung werden derzeit nicht durchgereicht.

#### Fehlermeldungen und deren Behebung

##### die Einheit Stk konnte nicht ins ZUGfERD-Format übertragen werden.
![](Fehlende_ZUGfERD_Mengeneinheit.png)<br>
Trage die Einheit in System, Sprache, Einheit nach.

Übliche Werte sind:
| Kennung | ZUGfERD Einheit | Bemerkung |
| --- | --- | --- |
| Stk | H87 | Stück |
| m | MTR | Meter |
| d | DAY | Tag(e) |
| h | HUR | Stunde(n) |
| kg | KGM | Kilogramm |
| km | KMT | Kilometer |
| l | LTR | Liter |
| m² | MTK | Quadratmeter |
| m³ | MTQ | Kubikmeter |
| mm | MMT | Millimeter |
| min | MIN | Minute |
| mon | MON | Monat(e) |
| s | SEC | Sekunde(n) |
| w | WEE | Woche(n) |

#### Kurzfassung Einrichtung
Um die Zugferd-Funktionalität einzurichten empfehlen sich folgende Schritte:
- Aktivieren der Zusatzfunktionsberechtigung ZUGFERD
- Am Client neu anmelden
- Wenn noch nicht eingerichtet, einrichten des EMails Versandes
- Hinterlegen der EMail Adressen im Personal
- Einrichten der default Bankverbindung, dazu:
  - im Modul Partner unterer Reiter Bank deine Bank hinterlegen
  - im Modul Finanzbuchhaltung im Reiter Sachkonto ein Sachkonto für die Bank anlegen
  - im Modul Finanzbuchhaltung im Reiter Bankverbindung deine Bankverbindung eintragen.<br>
  **WICHTIG:** Auch deine IBAN und deine BIC eintragen. Genauer die deines Unternehmens, an die die Zahlungen durch deine Kunden erfolgen sollten
  - im Modul System, Mandant, für den jeweiligen Mandanten, 3 Vorbelegungen, die default Bankverbindung hinterlegen, also das oben definierte Konto
- Bei jedem Kunden, dem du eine Zugferdrechnung senden willst / musst, du deine dir / deinem Unternehmen zugeordnete Lieferantennummer hinterlegen.<br>
Diese findest du beim jeweiligen Kunden, Reiter Konditionen

Denke daran obige Mengeneinheiten zu definieren

### Prüfung auf Zugferd Konformität
Beim Rechnungsversand nach Zugferd gibt es folgende Möglichkeiten / Dinge zu beachten:
- Bei der Erzeugung einer Zugferd kompatiblen PDF/A-3 Datei wird bereits von den Report-Librarys (Jasperstudio) geprüft, ob die wesentlichen Dinge, wie z.B. dürfen nur nicht transparente Bilder verwendet werden, erfüllt sind.<br>
Ist dies nicht erfüllt kommt bereits eine entsprechende Fehlermeldung.

Zusätzlich muss geprüft werden, ob alle Formulare und Texteingaben auch die richtigen Schriften verwenden. Da dies, gerade bei umfangreichen Rechnungen, eine etwas länger dauernde Prüfung ist (es muss jedes Feld geprüft werden), gibt es eine eigene Zugferd Prüffunktion um sicherzustellen, dass deine Rechnung / dein Rechnungsformular entspricht. Gerade beim Erstversand von Rechnungen raten wir diese Prüffunktion zu verwenden.

Hier spielt leider auch die Verwendung von Logos u.ä. in deinen Kopf- und Fußzeilen mit rein, womit sich in der Umstellungsphase auch ergeben kann, dass eine Anpassung der Anwenderspezifischen Kopf- und Fußzeilen erforderlich ist.