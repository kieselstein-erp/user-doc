---
categories: ["Rechnung"]
tags: ["Elektronische Rechnung"]
title: "Rechnung"
linkTitle: "Elektronische Rechnung"
weight: 80
date: 2023-01-31
description: >
  Elektronische Rechnung
---
Elektronische Rechnung
======================

Nachfolgend eine Zusammenfassung der Themen und Begriffe rund um die sogenannte elektronische Rechnung.

Hinweis: Bitte trennen Sie dies wiederum vom Begriff der E-Rechnung, welche ab 1.1.2014 für alle Rechnungen an Gebietskörperschaften in Österreich erforderlich sind.

Wir befassen uns hier mit der mit 1.7.2012 in Deutschland und seit 1.1.2013 in Österreich erlaubten Form der elektronischen Rechnung. Diese wurde auch durch eine EU-Richtline erzwungen, die die Elektronische Rechnung der Papier Rechnung gleich stellt.

Für die ab 1.1.2025 mögliche Anwendung der sogenannten eRechnung, bzw. xRechnung, bzw. ZUGFeRD [siehe]({{<relref "/verkauf/rechnung/elektronische_rechnung/eRechnung">}}). Die ab 1.1.2025 geltenden Regeln übersteuern teilweise das unten gesagte. Bitte informiere dich welche Regelung nun für dich zutreffend ist.

#### Was ist beim Versand elektronischer Rechnungen zu beachten?
- Sie dürfen elektronische Rechnungen nur mit der Zustimmung des Empfängers an diesen senden
- Achten Sie darauf, dass die Rechnung mit einem entsprechenden Hinweis versehen ist, dass es sich um eine Mehrfachausstellung handelt, also dass es eine Kopie ist (siehe dazu auch Salzburger Wirtschaft vom 13.12.2013).
- Beachten Sie, dass trotz der Papier Behandlung immer die Datei das Original ist. D.h. für die versandten Dateien gelten die gesetzlichen Aufbewahrungsvorschriften von 7 bzw. 10 Jahren
- Versenden Sie Rechnungen nur im PDF Format.<br>
Hinweis für Insider: Natürlich kann mit den entsprechenden Programmen auch eine im PDF Format erhaltene Datei verändert werden.
- Um eine Rechnung so zu kennzeichnen, dass erkennbar ist, dass diese elektronisch versandt wurde, steht ein speziell eingerichtetes Formular zur Verfügung. Wenden Sie sich dazu bitte an Ihren **Kieselstein ERP** Betreuer.
- Gegebenfalls verwenden Sie bitte das [ZUGFeRD]({{<relref "/docs/stammdaten/system/drucken/#was-bedeutet-zugferd-versand">}}) Format.
- Um an die bestimmte Kunden immer die Rechnung per EMail zu senden [siehe]({{<relref "/docs/stammdaten/system/drucken/#re-email">}}).

#### Was ist beim Empfang elektronischer Rechnungen zu beachten?
<a name="Prüfung elektronischer Rechnungen"></a>
Von der Idee her wird eine elektronische Rechnung gleich behandelt wie eine Papier-Rechnung. D.h. wenn eine elektronische Rechnung Ihren dokumentierten Prüfablauf für eine Eingangs-Rechnungsprüfung durchlaufen hat und von Ihnen bezahlt wird, wird davon ausgegangen, dass die Rechnung rechtens war. Wichtig ist, dass der Prüfablauf definiert ist.

Hier wird vom österreichischen Finanzamt folgender Prüfablauf empfohlen:

![](Innerbetriebliches_Steuerungsverfahren.jpg)

Siehe dazu auch: <https://www.wko.at/Content.Node/Service/Steuern/Umsatzsteuer/Spezielles-zur-Umsatzsteuer/Rechtliche_Grundlagen_E-Rechnung.pdf>

und: <https://www.wko.at/Content.Node/Service/Steuern/Umsatzsteuer/Spezielles-zur-Umsatzsteuer/Elektronische_Rechnung_und_Vorsteuerabzug.html> 

Wie Sie aus obigem Bild erkennen können, ist dies der für jeden verantwortungsvoll handelnden Unternehmer übliche Mindestablauf der auf jeden Fall, zumindest gedanklich durchlaufen wird, bevor eine Rechnung bezahlt wird.

Mit diesem Steuerungsverfahren wird die Echtheit und Unversehrtheit des Inhaltes einer elektronischen Rechnung auch ohne qualifizierter Signatur gewährleistet.

D.h. Sie müssen anhand obiger Grafik noch die handelnden Personen / Abteilungen definieren und damit ist der Prüfablauf der Rechnung definiert.