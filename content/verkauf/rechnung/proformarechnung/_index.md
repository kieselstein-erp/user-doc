---
categories: ["Rechnung"]
tags: ["Proforma Rechnung"]
title: "Rechnung"
linkTitle: "Proforma Rechnung"
weight: 06
date: 2023-01-31
description: >
  Die Proforma Rechnung als Zolldeklaration
---
Proforma-Rechnung
=================

Im Modul Rechnung finden Sie auch das Untermodul ![](Proformarechnung1.gif).

Statusübergänge
![](statusuebergang.png)

Proformarechnungen benötigen Sie für die ev. nur vorübergehende Verbringung von Ware in ein Drittland, also für die Abwicklung Ihrer Export-Zollformalitäten.<br>
Das bedeutet, dass in aller Regel nur diejenigen Positionen auf einer Proformarechnung aufscheinen können, welche dann mittels Lieferschein direkt an den Kunden verrechnet werden.<br>
Daher können in einer Proformarechnung keine einzelnen Artikel oder Handeingaben eingegeben werden, sondern nur Lieferscheine, Texteingaben, Textblöcke, also die beschreibenden Positionsarten zum Lieferschein.<br>
Beachten Sie bitte, dass die Proformarechnung eine Art Steuererklärung ist. Stimmen Proformarechnung und tatsächliche Rechnung nicht überein, ist dies ein steuerrechtliches Vergehen, welches entsprechende Folgen haben kann.

Ein verrechneter Lieferschein kann nicht mehr in eine Proformarechnung eingebunden werden, ein bereits in eine (andere) Proformarechnung eingebundener Lieferschein ebenfalls nicht.

Beim Ausdruck der Profromarechnung wird neben den Lieferscheinnummern auch die Rechnungsnummer angeführt, mit der der in die Proformarechnung eingebundene Lieferschein bereits verrechnet wurde. Damit haben Sie jederzeit auch die Information welche Lieferscheine der Proformarechnungen mit welchen Rechnungen bereits verrechnet worden sind. **Hinweis:** Es steht auch im Modul Rechnungen ein Trick zur Verfügung, mit dem eine gewöhnliche Ausgangsrechnung als Proformarechnung ausgedruckt werden kann. Es wird dies in der Form umgesetzt, dass das im ersten Betrifft einer Rechnung der Text Proforma (-rechnung) enthalten sein muss.
![](Proformarechnung2.gif)<br>
Alle weiteren Behandlungen / Funktionen werden wie für jede andere Ausgangsrechnung auch vorgenommen.

Diese Funktion hat den Nachteil, dass diese Rechnungen auch in Ihrem Ausgangsrechnungsumsatz usw. enthalten sind, da dies ja ganz gewöhnliche Rechnungen sind. Verwenden Sie daher bitte das Modul Proformarechnung.

**Hinweis2:** Leider ist vielen Menschen der Unterschied zwischen Proforma Rechnung, Anzahlungsrechnung und Vorauszahlung nicht klar. Es kommt daher auch immer wieder vor, dass ein Kunde eine Profromarechnung verlangt und in Wirklichkeit ist dies eine Anzahlungsrechnung. Oft müssen Sie auch von Ihren Kunden Vorauszahlungen verlangen, die z.B. den gesamten Auftragswert darstellen. Auch diese werden gerne als Profromarechnung bezeichnet, obwohl diese genaugenommen Vorausrechnungen, also [Anzahlungsrechnungen]( {{<relref "/verkauf/rechnung/anzahlung_und_schlussrechnung"  >}} ) sind.

Bitte unterscheiden Sie ganz klar zwischen den verschiedenen Fällen.

In manchen Fällen ist es das Einfachste, wenn man die Auftragsbestätigung als Proforma-Rechnung ausdruckt. Wir haben daher den oben für die Rechnung beschriebenen Trick auch im Formular der Auftragsbestätigung zur Verfügung gestellt.

#### Welche Preise werden in der Proformarechnung angedruckt?
Da die Preise einer Profromarechnung immer Preise für den Zoll sind und nicht wirklich für den Kunden, kommen die in der Profromarechnung angedruckten Preise aus dem Artikel.
Ist nun für einen Artikel keine Verkaufspreisbasis angegeben, so werden auch keine Preise in der Profromarechnung angedruckt.

<a name="Veredelungskosten"></a>

#### Veredelungskosten in der Proformarechnung?
Auf der Proformarechnung sind die Werte für den Zoll darzustellen.
Nun wird es auch vorkommen, dass Sie von Ihrem Kunden Beistellware erhalten, diese veredeln und dann wieder an Ihren Kunden liefern.
D.h. auf der Proformarechnung ist ein um die Beistellware höherer Wert anzuführen als Sie tatsächlich verrechnen.
Das bedeutet, dass Sie für diese Ware
-   einen Wareneingang der Beistellware auf das Kundenlager buchen
-   diese Ware im Los für die Fertigung Ihres Produktes verbrauchen
-   Ihr Produkt mit einem Lieferschein an den Kunden senden
-   und diesen Lieferschein in eine Proformarechnung einbinden

Nun ist auf der Proformarechnung der Wert der Ware inkl. dem Beistellmaterial anzuführen, aber die Rechnung an Ihren Kunden darf natürlich nur die Kosten Ihrer Veredelung enthalten. Eine Besonderheit: Oft wird dies auch noch in Fremdwährung abgebildet, wobei sich eventuelle Fremdwährungsschwankungen nicht auswirken dürfen.

Wie dies nun abbilden:
1. Buchen Sie den Wareneingang in ein Lager der Lagerart Kundenlager.
2. Geben Sie dem Wareneingang auch die tatsächlichen Stückkosten
c3. In der Produktion über das Los stellen Sie sicher dass die Beistellware aus dem Kundenlager kommt.
Beachten Sie bitte, dass ein eventueller Mehrverbrauch sich zwar auf die Gestehungskosten auswirkt, aber nicht auf den Wert der Beistellware
4. Liefern Sie das Los wie üblich ab und liefern Sie das Produkt mit einem Lieferschein an Ihren Kunden.
5. nun binden Sie den Lieferschein in eine Proformarechnung ein.

Auf der Proformarechnung wird unter der eigentlichen Ware eine zusätzliche Zeile Zoll angezeigt, mit der diese indirekten Kosten aus der Beistellware dargestellt werden.

Hinweis: Hier kommt noch das Thema hinzu, dass im Los Mehrverbrauche, z.B. wegen Ausfällen während der Fertigung, gebucht wurden. Trotzdem muss der Einzelpreis der Bestellung angeführt werden. Bitte Sie Ihren **Kieselstein ERP** Betreuer dies für Sie entsprechend umzusetzen. [Siehe dazu auch]( {{<relref "/fertigung/losverwaltung/#kundenmaterial-verarbeiten" >}} ).

#### An welche Adresse ist eine Proformarechnung zu stellen?
Eine Profromarechnung wird immer an die Lieferadresse gelegt. Also an die (direkte) Adresse des Lieferscheines und NICHT an die Rechnungsadresse des Lieferscheines.<br>
Dies ist dann von Bedeutung, wenn die Lieferung in ein anderes Land geht, als dann die Rechnung. <br>
Beachten Sie dazu vor allem die umsatzsteuerlichen Bedingungen.