---
categories: ["Rechnung"]
tags: ["Gutschrift"]
title: "Rechnung"
linkTitle: "Gutschrift"
weight: 05
date: 2023-01-31
description: >
  Gutschrift zu Rechnung erstellen, inkl. Mengen- und Wertgutschrift
---
Gutschrift
==========

Statusübergänge
![](statusuebergang.png)

Im Modul Rechnung finden Sie auch das Untermodul ![](Gutschrift.gif).

Hier werden die Gutschriften verwaltet. Bitte beachten Sie, dass Warenrücknahmen nur im Modul Gutschrift gebucht werden können. Siehe dazu bitte gegebenenfalls Lieferscheinpositionen mit negativen Mengen.

Im Rechnungsmodul können zwar negative Preise eingetragen werden, dies muss aus Rabattgründen zulässig sein. Warenrücknahmen können jedoch nur im Gutschriftsmodul und im Modul Lieferschein gebucht werden. Im Modul Lieferschein kann durch Angabe einer negativen Menge ebenfalls eine Warenrücknahme gebucht werden.

Für einige Anwender ist es wünschenswert, dass Rechnungen und Gutschriften den gleichen Nummernkreis haben. Setzen Sie dazu den Mandantenparameter GUTSCHRIFT_VERWENDET_NUMMERNKREIS_DER_RECHNUNG auf **1** .

#### Welche Gutschriftsarten gibt es ?
Wir unterscheiden derzeit drei Gutschriftsarten
- die klassische Gutschrift, auch Rechnungskorrektur genannt, mit der Ware bzw. Leistung vom Kunden zurückgenommen wird und wieder aufs Lager gebucht wird
- die Wertgutschrift. Dies ist auch eine Gutschrift / Rechnungskorrektur mit dem Sie Ihrem Kunden eine entsprechende Wertminderung gutschreiben.<br>
Diese Gutschrift bewirkt, dass der Erlös / der Umsatz bei Ihrem Kunden sinkt und damit verbunden auch, dass der Erlös am Artikel sinkt.

- Die [Ausgangsgutschrift]( {{<relref "/einkauf/eingangsrechnung/ausgangsgutschriften" >}} ), die eine besondere Form der Eingangsrechnung ist.<br>
Die Ausgangsgutschrift wird für Provisionen und ähnliches verwendet. Also für Aufwandspositionen.
Bitte achten Sie auf diesen wesentlichen Unterschied, auch wenn im Endeffekt in der G+V das Endergebnis gleich ist.

## Anlegen einer Gutschrift

Um eine neue Gutschrift anzulegen gehen Sie bitte wie folgt vor:
Klicken Sie auf das Modul Rechnung ![](Rechnung.JPG) im unteren Reiter finden Sie die Gutschriften, in der nun offenen Auswahl klicken Sie auf Neu ![](neu.gif).

Nun geben Sie in den Kopfdaten die allgemeinen Informationen zum Beleg ein. Das bedeutet u.a. den die Rechnung, den Kunden, Projekt, Bestellnummer, Termine - je nach Bedarf.

Mit einem Klick auf Speichern ![](speichern.JPG) bestätigen Sie die Eingaben.

Wechseln Sie in den Reiter Positionen und geben hier die Positionen der Gutschrift ein.

Um eine neue Position hinzuzufügen klicken Sie auf ![](neu.gif) Neu wählen im unteren Bereich des Fensters die Art der Position ([Positionsarten]( {{<relref "/verkauf/gemeinsamkeiten/#positionsarten" >}} )), geben die Informationen (u.a. Menge) ein und klicken auf ![](speichern.JPG) Speichern. Gehen Sie für alle gewünschten Positionen wie oben beschrieben vor.

Wenn die Erfassung der Positionen abgeschlossen ist, dann wechseln Sie in den Reiter Konditionen.
Hier geben Sie die Informationen ein, wenn diese von den Grundeinstellungen abweichen und ändern ev. die Texte für diese eine Gutschrift ab. 

Mit Klick auf ![](drucken.JPG) Drucken (oder oberes Menü Rechnung - Drucken...) erhalten Sie eine Voransicht des Ausdrucks. Mit dem Ausdruck ![](drucken.JPG), der Druckvorschau ![](druckvorschau.JPG), dem Versenden per E-Mail ![](e-mail.jpg)oder speichern ![](speichern.JPG), aktivieren Sie die Gutschrift, der Status (siehe oben) ändert sich von angelegt in offen. 

**Ein wichtiger Hinweis:**

Um Ware zurückzunehmen muss das Gutschriftsmodul verwendet werden.
Wird eine Rechnung mit einem Artikel mit positiver Menge und einem negativen Betrag erstellt, so ergibt dies zwar einen negativen Wert, aber eine Lagerabbuchung, da die Menge in der Rechnung positiv (= Lagerabbuchung) ist.

**Hinweis:** Im Modul Lieferschein können Positionen mit negativen Mengen angegeben werden. Diese negativen Abbuchungen werden im Lieferschein als Lagerzubuchung behandelt.

#### Werden die Rechnungspositionen automatisch in die Gutschrift übernommen?
Nein. Da bei der Ausstellung einer Gutschrift nicht davon ausgegangen werden kann / darf, dass das Gleiche gutgeschrieben wird wie es verrechnet wurde, müssen die Positionen entsprechend neu eingegeben werden. 

Denken Sie hier z.B. an eine Kulanzgutschrift.

Wenn eine Gutschrift aus einer Rechnung erstellt wird, dann erscheinen die Kundenhinweise. Außerdem werden die Bestellnummer und die Projektbezeichnung übernommen. 

Als praktisches Hilfsmittel kann sicherlich das Kopieren über die [Zwischenablage]
( {{<relref "/start/01_grunds%C3%A4tzliche_bedienung/#a-namezwischenablageakopieren-in-internes_kopierengif-und-einf%c3%bcgenaus-internes_einfuegengif-zwischenablage" >}} ) verwendet werden. D.h. markieren Sie in der Rechnung die gutzuschreibenden Positionen und kopieren Sie diese in die Zwischenablage. Nun gehen Sie wieder in die Gutschrift und übernehmen die Daten aus der Zwischenablage. Damit haben Sie die gewünschte Position in der Gutschrift. Nun muss noch gegebenenfalls Menge und Preis korrigiert werden.

WICHTIG: Bei der Gutschrift wird sofort beim Speichern die Menge ins Lager gebucht und steht damit anderen Anwendern zur Verfügung. Sollten Sie nun feststellen, dass Sie z.B. den falschen Artikel zugebucht haben und ein Kollege hat teilweise genau diese Buchung (z.B. für einen Lieferschein) verbraucht, so kann diese Menge nicht mehr reduziert werden.

#### Kann eine Begründung für die Gutschrift angegeben werden?
In den Kopfdaten der Gutschrift kann ein Gutschriftsgrund angegeben werden. Da auch für diese Gründe immer wieder statistische Auswertungen erforderlich sind, müssen diese Gründe in den Grunddaten, Gutschriftsgrund vordefiniert werden.

<a name="Wert/Mengengutschrift"></a>Mengengutschrift / Wertgutschrift

Beim Anlegen einer Gutschrift kann in der Gutschriftsart zwischen Gutschrift und Wertgutschrift unterschieden werden.

<u>Gutschrift oder auch Mengengutschrift:</u>

Hier werden die angegebenen Artikel tatsächlich dem angegebenen Lager zugebucht. Es wird also der Lagerstand des jeweiligen Artikels um die entsprechende Menge erhöht.

<u>Wertgutschrift:</u>

Müssen Gutschriften ausgestellt werden, bei denen die Ware beim Kunden bleibt, aber es aus verschiedenen Gründen zu einer Preisreduktion kommt, so steht dafür die Wertgutschrift zur Verfügung. Dies bewirkt, dass die Lagerzubuchungen auf ein verstecktes Lager erfolgen, was für Sie bedeutet, dass sich der Lagerwert nicht verändert, aber die Preisreduktionen / Nachlässe in der Statistik des jeweiligen Artikels enthalten sind.

Bitte beachten Sie, dass das angegebene Lager bei der Wertgutschrift keine Wirkung hat.

#### Sind "Zahlungen" / Erledigungen einer Gutschrift ohne Rechnungsbezug trotzdem sichtbar ?
Aus diesem Grund gibt es auch in der Gutschrift einen oberen Modulreiter Zahlungen. Hier wird der Ausgleich dieser Gutschrift durch die Rechnung mit Angabe des Zahlungsdatums, der Rechnungsnummer und des Bruttobetrages angezeigt.

#### Wie wird eine Gutschrift als bezahlt / Gegenverrechnet verbucht?
Siehe dazu [Zahlungsart Gutschrift]( {{<relref "/verkauf/rechnung/zahlung/#zahlungsart-gutschrift" >}} ).

#### Wie kann die Lieferadresse einer Gutschrift angegeben werden?
Gerade im Lebensmittelhandel ergibt sich oft die Situation, dass die Gutschrift an die Rechnungsadresse zu senden ist, aber nicht nur aus Statistikgründen zusätzlich die Lieferadresse angegeben werden muss. Verwenden Sie für diese Angabe die Statistikadresse in den Kopfdaten der Gutschrift.

#### Wie behandle ich Reklamationskosten meines Kunden richtig?
<a name="Reklamationskosten"></a>
Diese Kosten sind bei genauerer Betrachtung Erlösminderungen. D.h. Sie erhalten für von Ihnen gelieferte Ware oder Leistung nicht den vorab verrechneten Betrag sondern einen geringeren Betrag. Daher muss dafür eine Gutschrift / Rechnungskorrektur ausgestellt werden. Damit sinkt der mit dem Kunden erzielte Umsatz und in aller Regel auch der erzielte Deckungsbeitrag.
Zusätzlich bewirkt dies, dass Sie diese Minder-Erlöse in der Artikelstatistik und auch in der Kundenstatistik sehen. Und nur dadurch  können Sie erkennen mit welchen Kunden Sie welche tatsächliche Deckungsbeiträge erwirtschaften.<br>
Bitte achten Sie dabei auf den Unterschied zwischen [Wert- und Mengengutschrift](#Wert/Mengengutschrift).

#### Bezeichnung als Gutschrift oder Rechnungskorrektur?
Mit dem Parameter GUTSCHRIFT_NENNT_SICH_RECHNUNGSKORREKTUR steuern Sie, ob die Gutschrift als Rechnungskorrektur bezeichnet wird. Derzeit ist es in Deutschland so, dass

die Bezeichnung "Gutschrift" nur noch in Fällen verwendet werden darf, in denen tatsächlich eine Leistung des Empfängers der Gutschrift erbracht wurde (siehe dazu auch [Kunden Eingangsgutschrift]( {{<relref "/verkauf/rechnung/kunden_eingangsgutschrift">}} )), während anderenfalls Bezeichnungen wie "Korrektur zur Rechnung", "Korrekturrechnung" o.Ä. verwendet werden soll. Keinesfalls gilt das Wort "Gutschrift", wenn es sich um eine korrigierte Rechnung oder Stornorechnung handelt. Hier ist auch zu berücksichtigen, dass der Wert der Gutschrift negativ ausgewiesen werden muss. Damit dies in sich stimmig ist, werden Menge und Gesamtpreis negativ ausgewiesen. Bitte beachten Sie, dass in Ihren **Kieselstein ERP** Gutschrifts-Ansichten die Werte, Mengen, Preis positiv dargestellt werden. Bitte beachten Sie, dass dies derzeit nur für die deutschsprachigen Belege verwendet wird, da im internationalen Gebrauch die Credit-Note üblich ist.

Bitte wenden Sie sich an Ihren **Kieselstein ERP** Betreuer, falls Ihre angepassten Belege nicht den gewünschten Richtlinien entsprechen.

<a name="Anzahlungsrechnung gutschreiben"></a>

#### Gutschrift einer Anzahlungsrechnung?
Aufgrund der Komplexität in der steuerlichen Betrachtung kann eine Anzahlungsrechnung bei integrierter Buchhaltung nicht gutgeschrieben werden.<br>
Nutzen Sie gegebenenfalls die Möglichkeit eine Anzahlungsrechnung zu korrigieren (und dem Kunden neu zu senden) oder stornieren Sie die Anzahlungsrechnung und senden dem Kunden den Stornoausdruck der Anzahlungsrechnung.

Wird **ohne** integrierter Buchhaltung eine Anzahlungsrechnung gutgeschrieben, so ist zu beachten, dass die Erlöskonten aus dem gutgeschriebenen Artikel definiert werden. Eine Berücksichtigung des Statuses der Anzahlungsrechnung erfolgt von **Kieselstein ERP** nicht.<br>
Buchen Sie daher gegebenenfalls dies in Ihrer Buchhaltung entsprechend um.