---
categories: ["Rechnung"]
tags: ["Preise"]
title: "Rechnung"
linkTitle: "Preise"
weight: 70
date: 2023-01-31
description: >
  Preise in den Verkaufsmodulen
---
Verwendung der Preise und Rabatte
=================================

Da doch einige Preise, Rabatte, Aufschläge usw. in **Kieselstein ERP** zur Anwendung kommen, hier eine Beschreibung der Abhängigkeiten dieser Preise untereinander.

## Vor den weiteren Ausführungen noch grundsätzliche Dinge in Bezug auf Rundung und Rabatte:

a.) **Kieselstein ERP** geht bei den Berechnungen immer von den Nettowerten aus und errechnet daraus die Bruttowerte. Also von den für den Unternehmer interessanten Werten hin zu den vom Kunden zu bezahlenden Werten inkl. der Umsatzsteuer. Aus diesem Grunde sind z.B. bei 20% MwSt Bruttobeträge wie 0,99 nicht darstellbar (Es gibt nur den Wert 0,98 oder 1,00).

b.) Rabattsätze in Rechnungspositionen sind immer nur Hilfsmittel für eine einfachere Ermittlung eines Nettopreises. Der wirklich relevante Preis ist einzig und allein der Nettopreis.

![](Preis_Panel.png)

Die Berechnungen zwischen dem Einzelpreis und dem Nettopreis werden mit voller Rechengenauigkeit durchgeführt. Bitte beachten Sie, dass es grundsätzlich zwei Berechnungsrichtungen gibt:

a.) von oben nach unten (vom Einzelpreis zum Nettopreis)

b.) von unten nach oben (Vom Nettopreis zum Einzelpreis)

Wenn Sie nun den Einzelpreis eingeben und dann den Rabattsatz (%) dazu und noch einen Zusatzrabatt (%) so ergibt sich daraus ein mit möglichst hoher Genauigkeit errechneter Nettopreis. Erst dieser Nettopreis wird gerundet. Zusätzlich wird für die Anzeige der Zwischenwerte (Rabattbeträge) diese ebenfalls gerundet. Es wird jedoch von oben nach unten mit voller Rechengenauigkeit gerechnet. Daraus kann es zu Darstellungen kommen, die bei reiner Betrachtung der angezeigten Zahlen unstimmig sind.

Nehmen wir nun obiges Beispiel und verändern wir nun den Nettopreis von 86,44 auf 86,00\. Nun wird von unten nach oben gerechnet. D.h. der Einzelpreis wird wie eingegeben belassen, nur der Rabattsatz und der Rabattbetrag werden entsprechend angepasst. Hier kann es nun vorkommen, dass der angezeigte Rabattsatz (mit der Anzeigerundung) und der Rabattbetrag nicht auf den Cent übereinstimmen. Es wird wie bereits betont im Hintergrund immer mit voller Rechengenauigkeit des Systems gerechnet (ca. 15Stellen). Zu Ihrer Information wird die Rechenbasis durch die beiden Radiobuttons neben dem Rabattbetrag bzw. dem Nettobetrag angezeigt.

**WICHTIG:** Wird nun abermals in das Eingabefeld des Einzelpreises gewechselt und dann dieses Feld mit oder ohne Änderung verlassen, so wird erneut die Berechnung von oben nach unten angestoßen und dadurch unter Umständen ein neuer Nettopreis auf Basis des angezeigten Rabattsatzes (%) berechnet. Dies kann einen anderen Nettopreis zur Folge haben. Beachten Sie, dass die Anzeige der Rechenbasis umspringt.

Dies passiert gerne dann, wenn Sie mit der Tabulatortaste durch die Felder durchspringen. Programmtechnisch wird als letztes der Nettopreis verlassen und daher werden zur Basis des Nettopreises die Rabatte neu berechnet. Wenn die Rabatte ihre Rechenbasis sind, verwenden Sie nach der Eingabe des Rabattes z.B. Strg+S um den Datensatz abzuspeichern, ohne in das Feld des Nettopreises zu gehen.

Wir haben diese Rechenmethode nach ausführlichen Überlegungen gewählt, da Sie für beide Seiten, für Sie als Anwender und für Ihre Kunden die fairste Methode ist.

Ein Beispiel dazu:

![](Preiseingabe_Preisbasis_Rabatt.gif)

Basis ist hier der Rabatt, es wird von oben nach unten gerechnet.

![](Preiseingabe_Preisbasis_Nettopreis.gif)

Hier ist die Basis der Nettopreis und es wird von unten nach oben, also der Rabatt errechnet. Daraus ergeben sich, wie oben ausgeführt unterschiedliche Rabattwerte.

Noch ein Hinweis: Der angezeigte Mehrwertsteuerbetrag und der sich daraus ergebende Bruttopreis dienen lediglich Ihrer Information. Die tatsächlichen Steuerbeträge können in Einzelfällen über den gesamten Beleg davon (geringfügig) abweichen.

Beim Rückrechnen von unten nach oben, wird ein eingegebener Zusatzrabattsatz (%) nicht verändert. Die sich daraus ergebenden Werte natürlich schon.

Und ein weiterer Hinweis: Relevant für die Versteuerung und für die Findung des Rechnungswertes ist einzig und allein der Nettopreis. Dieser wird auch auf den Belegen entsprechend angedruckt. Einzig die Funktionalität des versteckten Aufschlages kann diesen Wert noch zwischen Anzeige und Ausdruck verändern. Dies bedeutet, dass auf Basis des gerundeten Nettopreises der versteckte Aufschlag addiert wird und das Ergebnis wiederum gerundet wird. Dieses Ergebnis ist nun die Basis für den Druck und die weiteren Berechnungen. Der Nettopreis inkl. Aufschlag wird in der Benutzeroberfläche nicht angezeigt, sondern lediglich für die Findung des Belegwertes und für den Ausdruck ermittelt.

Ein Zusätzliches:

Änderungen im (unteren) Nettopreis werden nur in den Rabattbetrag und den Rabattsatz rückgerechnet und nicht in den Zusatzrabatt.

Beispiel zur Auswirkung der in den Konditionen definierten Aufschläge und Rabatte:

In den Konditionen wurde folgendes definiert:

![](Versteckter_Aufschlag.gif)

Einzelpreis 98,00, Positionsrabatt 10%, Zusatzrabatt 2%

Einzelpreis inkl. versteckten Aufschlag = Einzelpreis * (1+verst.Aufschlag%) = 98*1,05 = 102,90

Gesamtpreis der Position inkl. Menge ausmultipliziert =

            Menge * Einzelpreis inkl. versteckten Aufschlag * (1-allg.Rabatt%) * (1-Projektrabatt%) =

            1 * 102,90 * 0,98 * 0,97 = 90,76

Hinweis: Ab der Zeilensumme wir auf zwei Nachkommastellen kaufmännisch gerundet.

Die verschiedenen Preise am Ausdruck:

![](Preise_am_Ausdruck.jpg)

Bitte beachten Sie, dass wenn bei einer senkrechten Rabattspalte keine Rabattsätze oder nur negative Rabatte angedruckt würden, so wird diese Spalte grundsätzlich leer gelassen. Damit Ihr Kunde nicht auf die Idee kommt, da gäbe es noch etwas zu holen.

Bitte beachten Sie auch den Projektrabatt im Auftragsmodul, welches Ihnen wie oben ausgeführt einen zusätzlichen allgemeinen Rabatt, nach dem Gesamtrabatt ermöglicht.

Noch einige Anmerkungen zu Preisen und Berechnungen:

Eingabe der Einzelpreise: Diese sind in **Kieselstein ERP** so gestaltet, dass Sie zwischen 2 oder 4 Nachkommastellen in den Mandantenparametern wählen können. Diese Nachkommastellenanzahl wird auch für die Definition der Nachkommastellen für die Speicherung der Preise in die Datenbank verwendet.

Berechnung der Zeilensumme: Diese wird ausschließlich mit zwei Nachkommastellen dargestellt. Hintergrund ist die positions- bzw. zeilenweise Überleitung, vor allem der Rechnungen, auf die unterschiedlichen Erlöskonten. Würden hier mehr als zwei Nachkommastellen definiert, so würde es bei der Berechnung der Mehrwertsteuer und bei der Überleitung in die Finanzbuchhaltung wiederum zu Rundungsdifferenzen kommen. Die mathematisch richtige Auflösung dieser Differenzen würde nur Verwirrung stiften. Daher wird die Zeilensumme bereits auf zwei Nachkommastellen gerundet und von da an auch nur mehr mit den gerundeten Werten weitergerechnet. D.h. die Netto-Zwischensumme vor allgemeinen Rabatten wird als Summe der gerundeten Zeilensummen ermittelt.

Hier ein Beispiel, welches, wenn mit voller Genauigkeit gerechnet werden würde, falsche Ergebnisse lieferte.

![](Rabatte_Rundung.gif)

5,66-10% Rabatt = 5,094

5,094 * 2 = 10,188 gerundet also 10,19 was schlichtweg falsch wäre, da ja am Papier 5,09 + 5,09 gedruckt wird und dies auch an die Buchhaltung usw. übergeben wird. Bitte beachten Sie auch, dass für die Berechnung der Mehrwertsteueranteile ebenfalls die gerundeten Nettobeträge der Zeilensummen herangezogen werden.
**Wichtig:**

Für die Berechnung der Mehrwertsteuerbeträge wird wie folgt vorgegangen:

Es werden die Nettobeträge der auf zwei Nachkommastellen gerundeten Zeilensumme des jeweiligen Steuersatzes addiert. Für diese Summe wird der Mehrwertsteuerbetrag errechnet und diese ebenfalls auf zwei Nachkommastellen gerundet. Nun werden die Mehrwertsteuerbeträge zur Nettosumme hinzugerechnet und somit ergibt sich der entsprechende Bruttobetrag.

#### Wieso wird nicht mit voller Rechengenauigkeit gerechnet?
Da dies zu buchhalterisch falschen Ergebnissen führen würde. Ein Beispiel, welches dies auch für nicht Buchhalter veranschaulicht:

Aufgrund einer Währungsumrechnung ergäbe sich für ein Stück eines Artikels ein Preis von 1,234\. D.h. der Nettobetrag für ein Stück wäre:

1 Stk x 1,234 = 1,23 (gerundet)

10 Stk x 1,234 = 12,34

Die Frage die sicherlich jeder stellen würde, der eine Rechnung dieser Art erhält: Warum kosten 10Stk mehr als 10x1Stk (Zehn mal ein Stück). Daher wird auch der ermittelte Einzelpreis eines Artikels auf die festgelegte Stellenanzahl gerundet und dann bis zur Zeilensumme mit maximaler Genauigkeit gerechnet und dieses Ergebnis für die Zeilensumme auf zwei Nachkommastellen gerundet.

Hinweis zu angezeigter bzw. angedruckter Mehrwertsteuer.

Der in den Belegpositionen dargestellte Mehrwertsteuerbetrag und damit der Bruttoeinzelpreis weichen vom angedruckten Mehrwertsteuerbetrag und dem Bruttoeinzelpreis ab, da in der Darstellung der Belegpositionen der Allg. Rabatt/Projektierungsrabatt und der Versteckte Aufschlag NICHT eingerechnet werden.

Im Druck des Belegs werden die Werte angedruckt, so wie sie oben beispielhaft berechnet wurden, also unter Berücksichtigung von Allg. Rabatt/Projektierungsrabatt und dem verstecktem Aufschlag.

**Hinweis:** Es wird nach dem Prinzip half_even gerundet. Dies ist die von den Banken empfohlene Methode mit der statistisch höheren Genauigkeit, als die gewöhnliche Rundung. Diese Rundung steht laut Microsoft für Excel nicht zur Verfügung. Siehe dazu auch nachfolgenden kurzen Auszug aus einem Microsoft Artikel.

### Bankübliche Rundung (Bankers Runden)
<a name="Rundung"></a>
Wenn man gerundete Werte addiert und Werte wie 1,5 oder 2,5 immer in dieselbe Richtung gerundet werden, entsteht eine Abweichung, die mit der Anzahl der addierten Werte immer stärker wird.<br>
Eine Möglichkeit, diese Abweichung auf ein Minimum zu begrenzen, ist die bankübliche Rundung.
Bei diesem Rundungsverfahren werden Werte wie 1,5 oder 2,5 mal auf- und mal abgerundet. Die Konvention bei dieser Art der Rundung ist, immer auf die nächste gerade Zahl zu runden, so dass 1,5 und 2,5 zu 2 gerundet werden und 3,5 sowie 4,5 zu 4 gerundet wird. Es handelt sich hierbei um eine symmetrische Rundung.<br>
Es gibt keine Excel-Tabellenkalkulationsfunktionen für die bankübliche Rundung.
Entnommen aus <http://support.microsoft.com/kb/196652> Bankübliche Rundung (Bankers Runden)

Das entscheidende ist immer, das bei der Zahl fünf hinter dem Komma die Zahl vor dem Komma betrachtet werden muss, ob diese gerade oder ungerade ist. Bei einer geraden Zahl vor dem Komma wird abgerundet, bei einer ungeraden wird aufgerundet.
3,45 € - 10% = 3,105 was auf 3,10 abgerundet wird, da die Ziffer davor ungerade ist.

Manche sprechen hier auch von Geodätischem Runden oder vom Gausschem Rundungsverfahren.
Im wesentlichen geht es darum, dass das Ungleichgewicht, die Bevorzugung bei 0,5 ausgeglichen wird. Teilweise entnommen aus herber.de.

#### Wie wirken sich Währungsänderungen auf Preise aus?
In **Kieselstein ERP** werden die Preise grundsätzlich mit der eingestellten Anzahl der Nachkommastellen abgespeichert. Wird nun die Währung z.B. eines Angebotes geändert, so werden alle Preise anhand des Wechselkurses umgerechnet, sodass wertmäßig annähernd wieder der gleiche Angebotswert gegeben ist. Aufgrund der kaufmännisch unbedingt erforderlichen Begrenzung der Rechengenauigkeit, siehe dazu obiges Beispiel, kann bei einem zurückwechseln des Angebotes wieder in die Ursprungswährung ein etwas anderer Positionspreis ermittelt werden. Bitte beachten Sie diesen Umstand, bevor Sie den entsprechenden Beleg auf eine andere Währung umstellen. Die Neuberechnung der Preise erfolgt in dem Moment, in dem Sie auf Speichern klicken.

Dazu ein Beispiel:

Voraussetzung: Die Anzeige und damit die Speicherung der Einzelpreise ist auf vier Nachkommastellen eingestellt.

Der Kurs ist: 1,20 USD für einen Euro.

Es wird ein Angebot in USD erstellt.

Die Angebotsposition hat Einzelpreis von 0,0100 USD.

Der Benutzer ändert im Zuge eines Währungswechsels in den Kopfdaten die Belegwährung von USD nach Euro.

Die Neuberechnung des Positionspreises ergibt: 0,01 * 0,833333 = 0,00833333 => 0,0083 Euro gerundet.

Der Benutzer ändert im Zuge eines erneuten Währungswechsels in den Kopfdaten die Belegwährung von Euro nach USD.

Die Neuberechnung des Positionspreises ergibt: 0,0083 * 1,2 = 0,0099 USD

Anmerkung: Es wird in alle Regel erforderlich sein, nach der Währungsänderung eines Beleges die Positionspreise zu überarbeiten. Insofern ist die Neuberechnung der Belegspreise nur als Unterstützung zu verstehen.

#### Wozu dient der versteckte Aufschlag?
Wir kennen ja alle die sogenannten Rabattkäufer. Das sind diejenigen Kunden, welche einen möglichst hohen Rabattsatz kaufen, aber nicht wirklich auf den Preis achten.

Nun kommt noch die zweite Forderung hinzu, dass die Preisgestaltung aus Gründen des Controllings immer in gleicher Weise erfolgen sollte. Für diese Fälle gibt es den versteckten Aufschlag.

D.h. es wird die Rechnung in der gewohnten Kalkulations- / Preisfindungsart und Weise erstellt. Dann hinterlegt man einen versteckten Aufschlag. Der versteckte Aufschlag bewirkt, dass beim Rechnungsdruck die Preise um diesen Prozentsatz erhöht werden. Ihr Kunde sieht also einen höheren Einzelpreis. Auf diesen höheren Einzelpreis wird nun ein entsprechender allgemeiner Rabatt gegeben, den der Kunde natürlich sieht und sich entsprechend über die bevorzugte Behandlung freut.

#### Wie gebe ich runde Preise, die ich mit meinem Kunden ausgemacht habe in **Kieselstein ERP** ein?
Zum Beispiel in der "Handwerker" Branche gibt es immer wieder den Fall, dass ein Angebots/Auftragswert 1.234,45  ausmacht und man sich dann mit dem Kunden auf 1.200,- einigt. In **Kieselstein ERP** wird das über den versteckten Aufschlag (inkl. Vorzeichen) gelöst. In den Angebots / Auftrags Konditionen können Sie beim Belegwert einen gewünschten Wert eingegeben. Daraufhin wird der Belegwert über den versteckten Aufschlag angepasst. Wenn ein Angebot/Auftrag neue Positionen eingefügt, vorhandene geändert oder Positionen gelöscht werden, dann wird der Pauschalkorrektur-Betrag gelöscht und ein Hinweis angezeigt.

#### Wie wird eigentlich gerundet?
Die Rundung in **Kieselstein ERP** erfolgt nach der sogenannten kaufmännischen Rundung. Im Gegensatz zur technischen Rundung, wird hier alleinig die der Darstellung nachfolgende Zahl für die Bestimmung zur Auf. oder Abrundung herangezogen.

Dazu ein Beispiel:

Wir gehen hier von einer Darstellung mit zwei Nachkommastellen aus:

Ist die Ziffer an der dritten Nachkommastelle nicht größer als eine 4, wird abgerundet. Anderenfalls wird aufgerundet. Die folgenden Nachkommastellen werden nicht berücksichtigt.

-   aus 2,1349 € wird 2,13 €

-   aus 2,1350 € wird 2,14 €

Negative Zahlen werden nach ihrem Betrag gerundet, bei einer 5 also weg von Null:

-   aus -2,1349 € wird -2,13 €

-   aus -2,1350 € wird -2,14 €

Der Unterschied von der kaufmännischen zur technischen Rundung stellt sich vor allem in den Grenzbereichen dar.

Die einfachste Erklärung dafür liefert nachfolgendes Beispiel. Die Begründung dafür ist in der Statistik der großen Zahl zu suchen, die besagt dass über viele Zahlen gerundet, so wie es z.B. im Bankbetrieb vorkommt, ist die verwendete Kaufmännische Rundung im Endergebnis genauer.

Beispiel für den Grenzfall:

-   aus 2,225    € wird 2,22 €

-   aus 2,22501 € wird 2,23 €

D.h. ist die entscheidende 0,5 exakt gegeben wird noch abgerundet. Ist der Wert etwas größer wird aufgerundet.

**Hinweis:** Die Anzahl der Nachkommastellen der Einzelpreise kann als Mandantenparameter definiert werden.

#### Warum gibt es keine Preiseinheiten?
In **Kieselstein ERP** gibt es keine Preiseinheiten. Wir verfolgen hier den Gedanken, dass insbesondere Preise sofort lesbar sein müssen. Würden nun für Preise unterschiedliche Preisbasen, z.B. Preiseinheit zur Basis 100Stk verwendet, so müssen Sie bei jedem Preis den Sie sehen zuerst überlegen welche Preisbasis hat dieser Preis um dann erst erkennen zu können ob ein Preis stimmen kann oder nicht. Um trotzdem entsprechend kleine Preise abbilden zu können, können die Preise bis zu vier Nachkommastellen, im Einkauf bis zu sechs Nachkommastellen angegeben werden.

#### Geringere Verkaufspreise
Wenn ein geringerer Verkaufspreis als der vorgeschlagene möglich wäre (aufgrund der dreistufigen Verkaufspreisfindung), kommt ein entsprechender Hinweis

#### Nachkommastellen und Formulare
Die Preise auf den offiziellen Belegen, Angebote - Rechnung, Anfragen - Bestellung, werden in der von uns gelieferten Ausführung mit zwei Nachkommastellen angedruckt. Die Mengen mit drei Nachkommastellen.

Wird nun für Ihre Installation die Anzahl der Nachkommastellen verändert (System, Parameter, Menge_UI_Nachkommastellen und PreiseRabatte_UI_Nachkommastellen), so müssen auch die Formulare entsprechend angepasst werden. Wird diese Anpassung nicht durchgeführt, so werden die Werte nach der definierten Stellenanzahl abgeschnitten (**Keine Rundung!**). Bitte achten Sie darauf, dass die Formulare und die definierten Nachkommastellen zusammenpassen.

Sicherheit beim Ausdruck

Aufgrund der vielen Berechnungen der Preise und auch Aufgrund von Verdichtungen, sowie auch aus Formularfehlern kann es vorkommen, dass der von **Kieselstein ERP** errechnet Wert mit dem auf Ihrer Rechnung angedruckten Wert nicht übereinstimmt. Zu Ihrer Sicherheit haben wir dafür eine Sicherheitsprüfung in den Rechnungsformularen eingebaut. Hier wird vor dem Druck der Rechnung überprüft, ob der errechnete Rechnungswert mit dem angedruckten Rechnungswert übereinstimmt.
![](Ueberpruefung_Summenausdruck.jpg)
Die Toleranz für diese Wert kann mit dem Parameter (System, Parameter) MAXIMALE_ABWEICHUNG_SUMMENAUSDRUCK eingestellt werden. Üblicherweise ist dieser Wert auf 0,01 eingestellt.

#### Es wurde eine neue Preisliste angelegt, diese ist im Preisauswahldialog nicht sichtbar
Wurde eine neue Preisliste angelegt, ohne diese mit Werten zu befüllen, so wird diese Preislistenposition in der Auswahl der Verkaufspreise ![](Preiseingabe_Auswahl_VK-Preise.gif) nicht angezeigt, da ja noch keine Preisberechnung definiert ist.

Zugleich wird dies auch im Artikel dadurch symbolisiert, dass im oberen Modulreiter VK-Preise in der Spalte berechneter Preis ein leeres Feld angezeigt wird.

Um nun den Preis zu definieren, wählen Sie ändern, gegeben eventuell den Rabatt bzw. den Fixpreis ein und speichern den nun definierten Verkaufspreis ab. Nun wird dieser Preislisteneintrag auch in der Auswahl der Verkaufspreise angezeigt.

#### Was bedeutet das rote L! in der Preiserfassung?
Mit ![](Preis_Gueltigkeits_Warnung.gif) werden Sie darauf hingewiesen, dass die Preisgültigkeit Ihres bevorzugten Artikel-Lieferanten bereits abgelaufen ist. [Siehe dazu auch.]( {{<relref "/docs/stammdaten/artikel/#g%c3%bcltig-bis" >}} )

#### Einkaufspreis, Lief1Preis?
<a name="Einkaufspreis Lief1Preis"></a>
Der Einkaufspreis ist der Preis zu dem Sie bei Ihrem Lieferanten bestellen.<br>
Üblicherweise wird dieser im Artikellieferanten, soweit bekannt und erforderlich, vordefiniert.
Selbstverständlich kann dieser aber auch direkt in der Bestellung erfasst werden, z.B. für eine neue Stückzahl oder gleich für einen neuen Lieferanten.<br>
Der [Lief1Preis]( {{<relref "/docs/stammdaten/artikel/#was-ist-der-lief1preis" >}} ) ist der Preis Ihres bevorzugten Lieferanten. D.h. dies ist derjenige der in seiner Reihenfolge an erster stelle steht und der in seiner Preisgültigkeit zutreffend ist. D.h. das Belegdatum der Bestellung liegt zwischen Preis Gültig ab und Preis gültig bis.

#### Muss für den richtigen Bestellpreis der Lieferant auf bevorzugter Lieferant gesetzt werden?
Nein selbstverständlich nicht. Es reicht, wenn der Lieferant Ihrer Bestellung auch im Artikellieferanten eingetragen ist.
Beachten Sie dafür auch die [Rückpflege]( {{<relref "/einkauf/bestellung/#kann-der-bestellpreis-automatisch-in-den-artikel-zur%c3%bcckgepflegt-werden" >}} ) der Bestelldaten.