---
categories: ["Rechnung"]
tags: ["Abrechnungsvorschlag"]
title: "Rechnung"
linkTitle: "Abrechnungsvorschlag"
weight: 10
date: 2023-01-31
description: >
  Zeiten und Kosten anhand von Abrechnungsmodellen (automatisch) verrechnen
---
Abrechnungsvorschlag
====================

Mit dem Zusatzmodul Abrechnungsvorschlag steht ein sehr praktisches und effizientes Werkzeug zur Abrechnung vor allem der Mitarbeiterzeiten inkl. Reisen zur Verfügung.<br>
Für die Abrechnung von Lieferungen siehe bitte [Ausliefervorschlag]( {{<relref "/verkauf/lieferschein/#ausliefervorschlag---r%c3%bcckstandsaufl%c3%b6sung"  >}} ).

Die Idee hinter dem Zeit-Abrechnungsvorschlag ist, dass man als Dienstleistungsunternehmen regelmäßig, z.B. wöchentlich oder monatlich die erbrachten Dienstleistungen an seine Kunden verrechnet. Diese Verrechnung muss natürlich auch Teilverrechnungen mit unterstützen.
Bitte beachten Sie, dass für ein vernünftiges Arbeiten ein entsprechend großer Bildschirm erforderlich ist.

Es stehen folgende Datenarten für die Abrechnung zur Verfügung:

| Datenquelle | Beschreibung |
| --- |  --- |
| Zeit | Die von Ihren Mitarbeiten gebuchte Zeit auf Projekte, Aufträge, Lose |
| Maschinenzeit | Die durch Ihre Mitarbeiter gebuchten Maschinenzeiten auf die Lose |
| Telefon | Die erfassten Telefonzeiten |
| Reise | Die erfassten Reisekosten |
| ER | Die erfassten und den Aufträgen zugewiesenen Nettobeträge (ev. Teil-Beträge) der Eingangsrechnungen. Hinweis: Eingangsrechnungen die nur über die Reise-Spesen zugeordnet wurden werden nicht berücksichtigt. |

Um nun auch den Mitarbeitern entsprechende Steuerungsmöglichkeiten bzw. Informationsmöglichkeiten zur Verfügung zu stellen, kann:
- ein Auftrag als Pauschal oder nach Aufwand
- eine Zeitbuchung mit ihrem Verrechnungsfaktor, welcher bei Pauschal mit 0% und bei nach Aufwand mit 100% vorbesetzt wird
- die Eingangsrechnungs-Auftragszuordnung mit dem Verrechnungsfaktor gleicher Logik
definiert werden.

- Mitarbeiterzeiten werden zum ermittelten Verkaufspreis des ermittelten Tätigkeitsartikels verrechnet. Siehe dazu bitte Verrechnungszeitmodell.
- Maschinenzeiten werden zu den Verkaufssätzen des der jeweiligen Maschine hinterlegten Verrechnungsartikels verrechnet.
- Telefonzeiten werden über den Telefonzeitartikel abgerechnet.
- Reisekosten werden über den Reisekostenartikel abgerechnet, wobei Reisekilometer über den Reisekilometerartikel abgerechnet werden.
- Eingangsrechnungen werden über die Auftragszuordnung mit dem Verrechnungsfaktor über den Spesenartikel abgerechnet.

Die Definition der zusätzlichen Verrechnungsparameter finden Sie in den Detail Kopfdaten des jeweiligen Verrechnungszeitmodelles.
Bitte beachten Sie, dass für die Verrechnung z.B. der Abrechnungspositionen eines Kunden derzeit jeweils nur ein Verrechnungsmodell unterstützt wird. D.h. wenn Sie unterschiedliche Kundenaufträge abrechnen, bei denen z.B. unterschiedliche Kilometerartikel und damit unterschiedliche Verrechnungssätze verrechnet würden, so ist dies derzeit nicht möglich und wird mit einem entsprechenden Hinweis abgelehnt.

**Verrechnungszeitmodell** Die Idee hinter dem Verrechnungszeitmodell ist, dass es je nach Art Ihres Unternehmens und auch nach den oft sehr unterschiedlichen Vereinbarungen mit Ihren Kunden und auch nach der Art der durchzuführenden Dienstleistung unterschiedlichste Verrechnungsmodelle gibt.
Beginnend von den direkten Stundenverrechnungen bis hin zu Tageszeitmodellen welche unterschiedliche Stundensätze je nach Zeitpunkt der Erbringung (Nacht- und Wochenendzuschläge), Mehrzeitzuschläge reichen können. Hier kommen dann noch die individuellen Preisvereinbarungen mit Ihren Kunden hinzu.

D.h. die Kette der Verkaufsartikelermittlung für den Arbeitszeitartikel und damit dann die Verkaufspreisermittlung stellt sich wie folgt dar:
- Erfasste Tätigkeit des Mitarbeiters zum Zeitpunkt
    - Anhand Zeitpunkt (Uhrzeit, Tagesart) und Dauer ergibt sich ein Verrechnungsartikel
    - Über die Verkaufspreisfindung für den Kunden ergibt sich der eigentliche Verkaufspreis für die Rechnungsposition.
    Bitte beachten Sie, dass je zu verrechnender Stundenmenge auch die Verkaufsmengenstaffeln wirken.

Alternativ dazu gibt es auch die Variante, dass im Auftrag Preise für die Normalzeiten vereinbart werden, über die Abrechnung nun ausgehend von den im Auftrag vereinbarten Preis, Aufschlagsfaktoren angewendet werden.

#### Wird der Abrechnungsvorschlag neu errechnet oder sind das live Daten?
Da der Abrechnungsvorschlag eine umfassende Zusammenschau der oben angeführten Daten ist, die wiederum eine Mischung aus Stunden und Kosten sind, wird der Abrechnungsvorschlag errechnet. Als Abrechnungsstichtag wird immer der letzte Tage der vergangenen Woche vorgeschlagen. Das bedeutet natürlich auch, dass bis dahin von Ihrem Team alle Daten erfasst sein sollten, da Sie das als verantwortungsbewusster Unternehmer sowieso einfordern (müssen).
Bitte beachten Sie, dass bei der Neuberechnung Positionen die als erledigt markiert sind, nicht mehr im neu errechneten Vorschlag auftauchen.
Wenn verrechnete Positionen aus der Rechnung wieder entfernt werden, so werden diese auch enterledigt, erscheinen also auch bei einer eventuellen Neuberechnung. Positionen die manuell erledigt wurden, können nur solange ent-erledigt werden, als kein neuer Abrechnungsvorschlag gemacht wird. Entfernen Sie dafür den Haken bei nur offene.

#### Können auch Positionen ohne Kunden verrechnet werden?
Nein, aber Sie können Positionen, auch mehreren, jederzeit einem anderen Kunden zuweisen.

Definition der Verrechnungsmodelle

Sie finden im Modul Rechnungen einen unteren Modulreiter Verrechnungsmodell, wenn Sie auch das Recht zum Bearbeiten der Grunddaten besitzen.
Hier werden die mit Ihren Kunden vereinbarten Verrechnungsmodelle definiert.
Diese Modelle beinhalten die zeitlichen Definitionen und auch die Verrechnungsartikel für die verschiedenen Bereiche.
![](Abrechnung1.gif)

Beachten Sie bitte, dass bei der Eingangsrechnung nur der Artikel definiert wird und dann der Verrechnungspreis ausgehend vom Wert der Eingangsrechnung(en) um den Aufschlag erhöht wird.
Bei allen anderen Artikeln werden die Verkaufspreise anhand der durch den Kunden definierten Verkaufspreisfindung bestimmt.
Bitte bedenken Sie auch, dass die Eingangsrechnung in der Regel für die Verrechnung von zusätzlichen Reisespesen, wie z.B. Mietwagen-, Tank- oder Hotelrechnungen gedacht ist.

Mit der Checkbox Preise aus Auftrag bzw. Faktor wird die Preisfindung für den Abrechnungsvorschlag gesteuert.
D.h. ist diese Checkbox nicht gesetzt, so greift die Verkaufspreisfindung für die bei den Verrechnungstagen hinterlegten Artikel für ihren jeweiligen Kunden. D.h. es wirken auch die Sonderkonditionen. Da aber immer wieder auch auftragsspezifische Preise vereinbart werden können, kann durch das anhaken dieser Checkbox definiert werden, dass grundsätzlich die Preise aus dem Auftrag kommen. Zugleich wirkt hier, dass für eventuelle Überstunden / Zuschläge der (Zuschlags-) Faktor wirkt. D.h. wenn z.B. im Auftrag eine Normalstunde mit einem Preis von 90,- € vereinbart ist, die Normalstunde im Artikel mit 100,- hinterlegt ist und für die Zeit von 00:00 - 06:00 ein Überstundenartikel mit einem Preis von 200,- definiert ist, so wird der Überstundenartikel in der Überleitung des Abrechnungsvorschlages in die Rechnung auf Basis des Ausgangsartikels des Auftrages mit dem Faktor, in unserem Falle 2,00 multipliziert und somit 180,- verrechnet.

Bitte beachten Sie, dass wir für diese Logik davon ausgehen müssen, dass in einem Auftrag ein Artikel immer gleich definiert ist, also passend zu obigem Beispiel ist der Artikel Normalstunde in dem Auftrag immer mit 90,- vereinbart.
Zusätzlich: Sollten mehrere Aufträge auf einmal abgerechnet werden, werden diejenigen Artikel herangezogen die zuerst definiert wurden. D.h. der Auftrag mit der niedrigsten Auftragsnummer und die niedrigste Auftragsposition.

**Definition der Verrechnungstage:** Im Reiter 3 Verrechnungsmodelltag werden die Übersetzungen der von Ihren Mitarbeitern erfassten Tätigkeiten anhand verschiedener Zeitbereiche auf den jeweils zu verrechnenden Artikel definiert. D.h. es werden für jede Tagesart (Montag bis Sonntag und Feiertage) und Dauer bzw. Stundenbereiche für entsprechende Überstundenzuschläge usw.
![](VerrechnungsmodellTag.jpg)

Wichtig:
Bitte beachten Sie, dass für die Berechnung der Dauer zuerst die Zeiten anhand des Zeitraumes abgezogen werden. D.h. wenn z.B. ein Mitarbeiter an einem Tag folgende Buchungen macht:

| Tätigkeit | Uhrzeit | Dauer |
| --- |  --- |  --- |
| Kommt | 04:00 |   |
| aktive Reisezeit | 04:00 |   |
| Arbeitsbeginn | 06:00 |   |
| Pause Beginn | 12:00 |   |
| Pause Ende | 12:30 |   |
| aktive Reisezeit | 18:00 |   |
| Geht | 23:30 |   |

Und wenn das Tages-Verrechnungsmodell für diesen Tag wie folgt aussieht:

| Tätigkeit | Von | Bis | Dauer | Verrechnete Tätigkeit |
| --- |  --- |  --- |  --- |  --- |
| aktive Reisezeit oder auch Arbeit | 00:00 | 06:00 |   | Reise oder Arbeit 100% Zuschlag |
| aktive Reisezeit oder auch Arbeit | 22:00 | Ende des Tages |   | Reise oder Arbeit 100% Zuschlag |
| aktive Reisezeit oder auch Arbeit |   |   | 10 | Zuschlag 50% |
| aktive Reisezeit oder auch Arbeit |   |   | 12 | Zuschlag 75% |

So ergibt sich aus der Übersetzung:

| Verrechnungs-Artikel | Von | Bis | Dauer | Bemerkung |
| --- |  --- |  --- |  --- |  --- |
| 100% Zuschlag | 04:00 | 06:00 | 2,0 | Zeitbereich |
| 100% Zuschlag | 22:00 | 23:30 | 1,5 | Zeitbereich |
|   |   |   |   |   |
| kein Zuschlag | 6:00 | 12:00 | 6,0 | Dauer noch nicht ab 10Std |
| kein Zuschlag | 12:30 | 16:30 | 4,0 | Dauer noch nicht ab 10Std |
| 50% Zuschlag | 16:30 | 22:00 | 2,0 | Dauer ab 10Std |
| 75% Zuschlag | 18:30 | 22:00 | 3,5 | Dauer ab 12Std |

**Hinweis:** Für nicht definierte Zeiträume werden die erfassten Zeitartikel auch für die Verrechnung verwendet.
Das bedeutet zugleich:
Ist zwar ein Verrechnungsmodell definiert, in diesem sind aber keine Übersetzungen für die erfassten Zeitartikel definiert, so bewirkt dies, dass die gebuchten Tätigkeitsartikel 1:1 in die Verrechnung übernommen werden.

#### können Verrechnungstage kopiert werden?
Um die Erfassung möglichst einfach zu gestalten, können durch Klick auf ![](Tag_weiter_kopieren.gif) Verrechnungstage kopieren die Einträge des aktuellen Tages auf den darauffolgenden Tag kopiert werden.
Bitte beachten Sie, dass es zu keinen Überschneidungen kommen darf, anderenfalls die Buchung abgebrochen wird.

A: Zusätzlich gibt es die Möglichkeit Einträge über die Zwischenablage zwischen den Verrechnungsmodellen zu kopieren.
D.h. durch Klick auf ![](In_Zwischenablage_kopieren.gif) kopieren in Zwischenablage kann mit ![](Aus_Zwischenablage_uebernehmen.gif) übernehmen aus Zwischenablage der Eintrag von einem zum anderen Zeitmodell kopiert werden. Sollten sich beim Kopieren Überschneidungen, Unstimmigkeiten ergeben, so werden diese Datensätze ohne weitere Fehlermeldung übersprungen.

Wie ist denn nun eine Reise und deren Kostensituation zu betrachten?

Beispiel:
Ein Mitarbeiter fährt am Montag mit seinem Privatauto zum Flughafen Salzburg, fliegt dann nach Hamburg, fährt noch eine Stunde nach Lübeck, arbeitet hier zwei Tage und fliegt / fährt wieder zurück. So könnte die zeitliche Darstellung dazu wie folgt aussehen:

| Beschreibung | Von | Dauer |   | Tätigkeit |   | Kosten | km |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |
| Anreise Flughafen Sbg | 7:00 | 1Std |   | aktive Reisezeit |   |   | 50 |
| Flug Sbg Ham | 8:00 | 4Std |   | passive Reisezeit inkl. der Wartezeiten |   | 350 |   |
| Fahrt Ham Lübeck mit Leihwagen | 12:00 | 2Std |   | aktive Reisezeit |   | 150 |   |
| Arbeit vor Ort beim Kunden | 14:00 | 5Std |   | Eigentliche Tätigkeit vor Ort beim Kunden |   |   |   |
| Nächtigung |   |   |   |   |   | 95 |   |
| Arbeit vor Ort beim Kunden 2.Tag | 8:00 | 8Std |   | Tätigkeit |   |   |   |
| Anreise Flughafen Ham | 16:00 | 3Std |   | aktive Reisezeit |   | 60 |   |
| Flug Ham Sbg | 19:00 | 3Std |   | passive Reisezeit inkl. Wartezeitenwenn denn der Flieger fliegt |   |   |   |
| Rückfahrt Sbg nach Hause | 22:00 | 1Std |   | aktive Reisezeit |   |   | 50 |

Es bedeutet, für diese Reise, dass es vier Kostenblöcke gibt:
1. die Reisekilometer
2. die Kosten der aktiven Reisezeit
    - die Kosten der passiven Reisezeit
3. Nebenkosten wie Flugticket, Nächtigung, Leihwagen usw.
4. die Diäten, in DE Verpflegungsspesenersatz genannt.
5. die eigentliche aktive Zeit beim Kunden / vor Ort (auf der Baustelle o.ä.) welche innerhalb der Reise liegt.<br>
Bitte beachten Sie für die Verbuchung der Reise auch das unter [Reisezeiten]( {{<relref "/fertigung/zeiterfassung/reisezeiten">}} ) geschriebene.

#### Wie wird mit den Änderungen während der Verrechnung umgegangen?
Üblicherweise werden während der Verrechnung der Dienstleistungen Fehlbuchungen und ähnliches erkannt und somit sind entsprechende Korrekturen erforderlich.
Hier sind zwei Arten von Änderungen zu unterscheiden:
1. Die Änderung von Zeitpunkten, z.B. des Geht oder auch des Arbeitsbeginns sollte grundsätzlich **vor** Beginn der Abrechnung bereits geprüft und für gültig befunden werden. Da dies auch sehr stark mit der Monatsabrechnung, also der Mitarbeiter Zeitabrechnung zusammen spielt. Bitte nutzen Sie dafür das Zeitdatenjournal, gegebenenfalls die Produktivitätsstatistik(en) und die Monatsabrechnung.
2. Die Änderung von Projekt/Auftrag/Los Zuordnungen gegebenenfalls mit Änderung der Tätigkeiten.
Für die Änderung dieser Zuordnungen, also Zeitbuchungen springen Sie mit dem GoTo der Zeitdaten ![](Abrechnung3.gif) genau auf die Buchung und passen die P/A/L bzw. Tätigkeitszuordnung entsprechend an. Diese Änderungen werden automatisch in den Abrechnungsvorschlag rückübertragen und dieser somit aktualisiert.

#### Können verrechnete Zeitbuchungen noch einmal verändert werden?
Jein. Das bedeutet, wenn Zeitbuchungen verrechnet wurden, so sind diese aus dem Modul Zeiterfassung heraus nicht mehr änderbar. Wenn diese noch einmal geändert werden müssen, so müssen dazu die Positionen aus der Rechnung herausgelöscht werden, damit sind diese wieder im Status nicht verrechnet und können somit geändert werden.
Beim Versuch eine verrechnete Zeitbuchungsposition zu löschen, erscheint eine entsprechende Fehlermeldung.
![](Abrechnung2.jpg)
Das bedeutet, dass durch löschen der Verrechnungszeilen, es kann eine Zeit durchaus auch mit mehreren Rechnungspositionen verrechnet werden, diese wieder änderbar wird.
Daher auch die Stichtagsbetrachtung bei der Neuerzeugung des Abrechnungsvorschlages.

Bedenken Sie in diesem Zusammenhang bitte auch, dass die Verrechnung immer vorwärts gedacht ist. Vorwärts bedeutet in diesem Sinne, dass zuerst die Zeitbuchungen stimmen sollten / müssen und danach erfolgt erst die Verrechnung.

#### Wie sollte nun für die Verrechnung vorgegangen werden?
Unser derzeitiger Vorschlag ist, dass zuerst die Monatsabrechnung, ev. bis gestern, aller betroffenen Mitarbeiter geprüft wird.
Danach prüfen Sie bitte die Produktivitätsstatistik der betroffenen Mitarbeiter und
erst danach, also wenn soweit die Zeiten richtig sind, erstellen Sie bitte den neuen Abrechnungsvorschlag.

![](Abrechnungsvorschlag.jpg)

Bitte beachten Sie, dass der Abrechnungsvorschlag für die Abrechnung Auftrag für Auftrag gedacht ist. D.h. wählen Sie einen Auftrag aus und rechnen Sie diesen ab.
Der Ablauf dazu ist, dass Sie zuerst die Zeiten markieren, die abgerechnet werden sollten. Somit wird auch die Gesamtsumme der markierten Stunden angezeigt. Diese Gesamtsumme kann in Ihrem Wert kleine Rundungsdifferenzen zur dann effektiv abgerechneten Zeit haben. Dann klicken Sie auf Zeilen abrechnen ![](Abrechnungsvorschlag2.gif).
Im nun folgenden Fenster werden alle sich aus den markierten Zeilen ergebenden Abrechnungsartikel angezeigt.
Hier können noch erklärende Texte mit dazugegeben werden.
Ja nach Rechnungsformular können hier auch noch die tatsächlich verrechneten Werte verändert werden. D.h. wenn in Ihrem Rechnungsformular der Andruck der Abrechnungsdetails aktiviert ist, dürfen diese Zeiten nicht verändert werden. Ist dieser (default) nicht aktiviert, können die errechneten Stunden verändert werden.
![](Abrechnungsvorschlag3.jpg)

Hier sehen Sie rechts oben die insgesamt zu verrechnen vorgeschlagenen Stunden.
Zu den Artikeln haben Sie im darunter eingeblendeten Textfeld die Möglichkeit erklärende Texte mit dazuzugeben. Diese Texte werden als eigene Textposition in die Rechnung mit übernommen.
Stimmen die angezeigten / errechneten Daten, so klicken Sie bitte auf den grünen Haken. Damit werden diese Positionen in die Rechnung übertragen. Übertragen bedeutet, wenn eine Rechnung zu diesem Kunden und diesem Auftrag im Status angelegt ist, wird diese Rechnung ergänzt, anderenfalls wird eine neue Rechnung angelegt.
Bitte beachten Sie auch, dass immer nur gleichen Verrechnungsarten (Zeit, Reise, ...) abgerechnet werden können. D.h. Sie markieren zuerst z.B. die Positionen Zeit, ev. nur für einen Mitarbeiter und dann die Positionen der Reise usw..
Mit ESC oder durch Klick auf die Tür, wird die Verrechnung abgebrochen.

**Wichtig:**
Wird unter zu verrechnen 0,00 angezeigt oder eingetragen, so werden diese Positionen nicht in die Rechnung mit übernommen.

#### Können Zeitbuchungen auch manuell erledigt werden?
Ja. Markieren Sie die nicht zu verrechnenden Zeilen = Zeitbuchungen und klicken Sie auf den grünen Haken. Damit sind die Zeitbuchungen als erledigt markiert und werden nicht weiter abgerechnet.

#### Werden Positionen zusammengefasst?
Ja. Es werden Tätigkeiten / Zeitbuchungen mehrerer Mitarbeiter auf gleiche Auftragspositionen zusammen gefasst.
Tätigkeiten auf unterschiedliche Auftragspositionen werden getrennt aufgeführt, auch wenn diese die gleiche Tätigkeit haben sollten.

#### Wie kommt die Auftragsnummer in die Rechnung?
<a name="Auftragsnummer_der_Rechnung"></a>
Für die Positionen die einen direkten Bezug zu den Auftragspositionen haben, werden diese in die Rechnungsposition übernommen. D.h. das sind in der Regel die Zeiten die auf die Auftragstätigkeiten, also die Auftragspositionen gebucht wurden. Für Verrechnungspositionen welche aus den Eingangsrechnungen bzw. aus den Reiseerfassungen kommen, ist keine direkte Zuordnung zu den Auftragspositionen (sondern eben nur zum Auftrag gegeben). Daher wird in den Rechnungspositionen für die Verrechnungspositionen der Eingangsrechnungen (das sind vor allem die Spesenbelege der Reisen) und die Kosten / Diäten / Verpflegungsmehraufwand aus der Reiseerfassung keine Auftragsinformationen in der Statuszeile angezeigt.
Wird nun aber eine Rechnung zumindest in die Vorschau gedruckt und ist im Rechnungskopf kein Auftrag eingetragen, aber es sind Positionen mit Auftrags(positions)bezug in der Rechnung enthalten, so wird automatisch der Auftrag auch in die Kopfdaten der Rechnung übernommen.

#### Können Details der Abrechnung ausgedruckt werden?
Ja, dies ist in den Basis-Formularen vorgesehen und muss von Ihrem **Kieselstein ERP** Betreuer aktiviert werden.
Bitte beachten Sie, dass in diesen Formularen die tatsächlich gebuchten Werte verwendet werden. D.h. wenn aus dem Abrechnungsvorschlag ein Auftrag verrechnet wurde und dann die Rechnung gedruckt wird, so stimmen die Werte der Rechnungspositionen mit den Detail-Aufstellungen überein. Werden nun aber nachträglich Änderungen in der Verrechnung oder in den Rechnungspositionen gemacht, wirken diese Änderungen nicht mehr in die Werteverteilung der Berechnungen zurück.
Das bedeutet, sollten Sie feststellen, dass Sie z.B. die Werte entsprechend anpassen möchten, so löschen Sie bitte die eine Position aus der Rechnung heraus, korrigieren Sie die (Zeit)-buchungen und lassen dann den Abrechnungsvorschlag neu laufen. Anschließend übernehmen Sie die gewünschte Position erneut in die Rechnung, welche im Status angelegt sein muss.

#### Wo sehe ich, mit welchen Daten der Abrechnungsvorschlag zuletzt erzeugt wurde?
Im Menüpunkt Info, Zuletzt erzeugt finden Sie die Zusammenfassung mit welchen Daten der Abrechnungsvorschlag zuletzt erzeugt wurde. Hier finden Sie auch, falls der Abrechnungsvorschlag nicht durchgelaufen ist und warum nicht.
![](Abrechnungsvorschlag_Fehler1.jpg)
So besagt diese Meldung, dass Sie am 11.4\. versucht haben den Abrechnungsvorschlag durchzuführen, aber bei der Ermittlung der Zeitdaten ein Fehler aufgetreten ist. Genauer bei der Person mit dem Kurzzeichen alo am 10.2.2020.
Bitte korrigieren Sie den Fehler und starten Sie die Errechnung des Abrechnungsvorschlages erneut.

#### Wie genau rechnet der Abrechnungsvorschlag ?
Die Berechnungen des Abrechnungsvorschlages werden intern auf drei Nachkommastellen durchgeführt. Das bedeutet, dass eine Minute als 0,016(66) Stunden dargestellt wird. Damit könnten sich unter widrigen Umständen in der Darstellung der Stunden-Details als Rechnungsanhang geringfügige Differenzen ergeben.

#### Wo finde ich welche Zeiten schon abgerechnet wurden?
Es kommt manchmal vor, dass man Zeiten versehentlich manuell erledigt hat und stellt dies erst nach dem Erzeugen eines neuen Abrechnungsvorschlages fest.<br>
Wie welche Zeiten abgerechnet wurden und eben auch manuell erledigt wurden, finden Sie im Modul Zeiterfassung, Journal, Arbeitszeitstatistik. Hier hat sich die Sortierung nach Kunde + Beleg + Personal bewährt. Gegebenenfalls grenzen Sie noch auf eine Person ein.

<a name="kalkulatorischer_Artikel_keine_Ueberleitung"></a>

#### Ent-Erledigen, bzw. zurücknehmen der manuellen Erledigungen
Wie oben beschrieben, müssen auch versehentlich manuell erledigte Zeiten wieder ent-erledigt werden können.
Dies finden Sie ebenfalls im Abrechnungsvorschlag, Menü Bearbeiten, manuell erledigte ent-erledigen
![](Abrechnungsvorschlag_Zeiten_enterledigen.jpg)

Nun definieren Sie die Filterbedingungen um die fälschlicher weise erledigten Einträge zu finden und klicken auf aktualisieren.
![](Abrechnungsvorschlag_Zeiten_manuell_erledigt_aufheben.jpg)
Damit erhalten Sie eine Liste der manuell erledigten Zeiten und sonstigen über den Abrechnungsvorschlag verrechneten Positionen.
Wenn nun alle diese Einträge auf nicht mehr erledigt gesetzt werden sollten, klicken Sie bitte auf "manuell erledigt" - Status aller Einträge aufheben.

#### Bei der Überleitung erscheint die Fehlermeldung, dass diese abgebrochen wird.
Info: Kalkulatorische Artikel dürfen in einer Rechnung nicht aufscheinen. Daher können diese auch nicht in die Rechnung übergeleitet werden.
D.h. sollte z.B. eine Fehlermeldung ähnlich der nachfolgenden aufscheinen,
![](kalkulatorischer_Artikel_in_der_Zeitbuchung.jpg)
so muss:
- entweder die Tätigkeit der zugehörigen Zeitbuchung auf eine Tätigkeit geändert werden, die NICHT kalkulatorisch ist, oder
- die Eigenschaft dass dieser Artikel gleichzeitig ein Arbeitszeitartikel und ein Kalkulatorischer Artikel ist aufgelöst werden.
D.h. in diesem Falle sollte das Kalkulatorisch bei dem Artikel entfernt werden.
D.h. eigentlich sollte, wenn auch der Abrechnungsvorschlag verwendet wird, ein Arbeitszeitartikel nicht gleichzeitig ein kalkulatorischer Artikel sein.

#### Welche Rechnung, welches Rechnungsdatum wird verwendet?
Gerade bei Abrechnungen von Leistungen zum Jahresende rechnet man üblicherweise am Jahresbeginn des neuen Jahres die Zeiten des Dezember / letzten Quartals ab.<br>
Nutzen Sie dafür die Funktion [Neudatum]( {{<relref "/verkauf/rechnung/#neu-datum"  >}} ) (Unterer Modulreiter Rechnung, Menü-Eintrag Extras, [Neudatum]( {{<relref "/verkauf/rechnung/#neu-datum"  >}} )).

Gerade wenn Verrechnungen in mehreren Schritten / Teilen gemacht werden, ist es von Vorteil, wenn diese alle in die gleiche Rechnung münden. D.h. solange für den Kunden eine Rechnung im Status angelegt vorhanden ist, werden weitere abzurechnende Positionen zu dieser Rechnung hinzugefügt.

#### Es wurden Eingangsrechnungen den Aufträgen und den Spesen zugeordnet. Was wird nun weiterverrechnet?
Es werden nur die Eingangsrechnungen die direkt Aufträgen zugeordnet sind in die Verrechnung mit aufgenommen.
Eingangsrechnungen die bei Reise-Spesen hinterlegt wurden, werden als Reiseaufwand des Reisenden betrachtet und daher nicht in die Abrechnung mit aufgenommen.
Das bedeutet, dass z.B. Hotelrechnungen die als Eingangsrechnung erfasst werden und diese, um im Abrechnungsvorschlag wirksam zu sein, auch dem jeweiligen Auftrag zugeordnet werden müssen.