---
categories: ["Rechnung"]
tags: ["Eingangsgutschrift"]
title: "Rechnung"
linkTitle: "Eingangsgutschrift"
weight: 30
date: 2023-01-31
description: >
  Wie mit eingehenden Gutschriften von Kunden / Lieferanten umgehen
---
Kunden-Eingangsgutschrift
=========================

Wie sind Eingangsgutschriften, welche meist von großen Kunden ausgestellt werden, zu behandeln?

1. Wichtig: Aus steuerlicher Sicht sind diese Eingangsgutschriften als Erlös zu behandeln. Lassen Sie sich daher bitte nicht durch den üblicherweise verwendeten Begriff verwirren und in die Versuchung bringen dies als negativen Aufwand z.B. in der Eingangsrechnungsverwaltung zu erfassen. Es sind dies Erlöse und daher als Rechnung zu erfassen.
2. Gerade wenn an große Kunden geliefert wird, wird es, von diesen oft sehr großen Unternehmen, erzwungen, dass die Vergütung nach deren Wareneingangsbuchung erfolgt. Damit verbunden ist eine automatische Erstellung von Gutschriften, welche in aller Regel auch eine umgehende Überweisung an Sie als Lieferanten nach sich ziehen. Insofern wird das von den großen Kunden auch gerne als Serviceleistung an deren Lieferanten (die **Kieselstein ERP** Anwender sind) verkauft.

Leider ist es so wie überall im Leben, dazwischen sitzen Menschen, welche auch mal Fehler machen. So kommt es immer wieder vor, dass Wareneingangsbuchungen nicht / nicht richtig erfasst werden. Wie kann man das abfangen?
- Erstellen Sie für jede Ware die Ihr Haus verlässt, über Ihr Unternehmen abgewickelt wird, einen Ausgangslieferschein (das sollte sowieso eine generelle Regel sein).
- Erstellen Sie für jede Eingangsgutschrift die Sie von Ihrem Kunden erhalten eine Ausgangsrechnung mit Bezug zur Eingangsgutschrift, in der die Lieferscheine abgerechnet werden.

So behalten Sie auf einfache Weise den Überblick über die noch offenen Lieferschein an den jeweiligen Kunden. Prüfen Sie regelmäßig, z.B. einmal im halben Jahr, ob den alles richtig verrechnet ist, oder ob längst fällige Lieferscheine noch nicht verrechnet wurden.

**Warum ist das wichtig?**

Es ist in der Praxis bei den verschiedensten Unternehmen, in der jeweiligen Branche durchaus namhaften Kunden, immer wieder vorgekommen, dass ganze LKW-Züge an Ware nicht verrechnet wurden. Mit dem hier nicht fakturierten Geld, amortisiert sich Ihr ERP-System innerhalb von Tagen!!

Was natürlich oft zur gänzlichen Verwirrung beiträgt ist, dass viele von Eingangsgutschriften sprechen, aber keine Unterscheidung zwischen der Gutschrift einer Eingangsrechnung eines Lieferanten machen (die z.B. wegen Fehllieferung ausgestellt wurde) und einer Gutschrift von einem Kunden Aufgrund der Lieferung einer Ware.