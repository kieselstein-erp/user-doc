---
categories: ["Rechnung"]
tags: ["EDI"]
title: "Rechnung"
linkTitle: "EDI"
weight: 90
date: 2023-01-31
description: >
  Electronic Data Interchange
---
EDI
===

In **Kieselstein ERP** stehen verschiedene Möglichkeiten im Sinne von EDI (electronic data interchange) zur Verfügung

Der Datenaustausch in den beliebten Formaten CSV bzw. XLS zählt technisch gesprochen auch zu einer Form des elektronischen Datenaustausches. Diese sind sehr oft von manuellen Eingriffen begleitet. Daher verweisen wir für diese Dinge auf die jeweiligen Module.<br>
In dieser Beschreibung sprechen wir von Automatismen, welche zwischen den beteiligten Partner als Machine to Machine Kommunikation oder gerne auch mit dem modernen Schlagwort Industrie 4.0 bezeichnet werden.

Derzeit werden die nachfolgend angeführten Arten von EDI unterstützt. Bitte beachten Sie, dass die Implementierungen aufgrund der vielen möglichen Varianten immer eine Abstimmung zwischen den Partnern erfordern, also Ihnen und Ihrem Kunden / Lieferanten. So hat alleine das allseits bekannte EdiFact Format ca. 6000 Feldbedeutungen und selbst sehr umfassende Implementierungen liefern, auch abhängig von der Interpretation durch die Anwender auf der anderen Seite, unterschiedliche Daten.

CleverCure
----------

Diese Schnittstelle der Firma CureComp wird für den Import von Auftragsdaten, den Versand von Terminbestätigungen und dem Versand von Lieferaviso verwendet.<br>
Die Kommunikation ist per https: mit öffentlichen Zertifikaten und freigegebenen öffentlichen IP-Adressen.

EdiFact
-------

DesADV
