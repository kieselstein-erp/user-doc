---
categories: ["Rechnung"]
tags: ["Zahlung"]
title: "Rechnung"
linkTitle: "Zahlung"
weight: 50
date: 2023-01-31
description: >
  Verbuchen der Zahlungen
---
Zahlung
=======

Sowohl in der Eingangs- als auch in der Rechnungsverwaltung steht der Modulreiter Zahlung zur Verfügung.

Buchen Sie hier die ein- oder ausgehenden Zahlungen auf Ihre Rechnungen.

Es stehen vier Zahlungsarten zur Verfügung
1.  Zahlung auf eine Bankverbindung<br>
    Es wird hier die im Mandanten hinterlegte Bankverbindung vorgeschlagen
2.  Bar<br>
    Also Zahlung auf eines der Kassenbücher
3.  Gegenverrechnung mit einer Eingangsrechnung
4.  Rechnungsausgleich durch eine Gutschrift
    Geben Sie hier die entsprechende Gutschrift an.

Um Zahlungsbuchungen der verschiedenen Zahlungsarten durchführen zu können, müssen zuerst die Verbindungen der Zahlungsarten Bank und Bar definiert werden.

<a name="Bankverbindung"></a>
### Zahlungsart: Bank

Grundsätzlich muss im Modul Partner, unterer Modulreiter Bank die gewünschte Bank definiert sein.

Nun können Sie im Modul [Finanzbuchhaltung]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen" >}} ) Ihre (offizielle) Bankverbindungen (unterer Modulreiter) definieren. Bitte beachten Sie, dass die Bank auch einem entsprechenden Sachkonto zugeordnet werden muss. Definieren Sie also vorher die Sachkonten für jede Ihre Bankverbindungen im richtigen Buchungskreis.

Nachdem Sie nun Ihre Bankverbindungen definiert haben, können Sie im Modul Systemdaten, unterer Modulreiter Mandant, oberer Modulreiter Lieferkonditionen die üblicherweise für die Zahlungsbuchungen verwendete Bank hinterlegen. [Siehe dazu auch](#Vorgeschlagene Bank definieren).

### Zahlungsart: Bar
Für die Zahlungsart Bar muss zumindest ein Kassabuch definiert werden. Auch dieses wird im Modul Finanzbuchhaltung, unterer Modulreiter Kassenbuch definiert. Bitte beachten Sie auch hier, dass Sie für jedes Kassenbuch ein korrespondierendes Sachkonto benötigen.

### Zahlungsart: Gegenverrechnung
Da es auch Gegenverrechnungen zwischen Kunden und Lieferanten gibt, also Partnern mit denen Sie sowohl in einer Kunden als auch einer Lieferanten Beziehung stehen, haben wir auch die Möglichkeit geschaffen, dass (Ausgangs-)Rechnungen mit Eingangsrechnungen ausgeglichen werden. Verwenden Sie dazu die Zahlungsart Gegenverrechnung und wählen Sie dann die Eingangsrechnung ![](Zahlung_Gegenverrechnung.gif) aus, mit der Sie Rechnung ausgleichen wollen (diese kann auch teilbezahlt sein). Zusatzkosten werden in der Auswahl nicht angeführt.

Beim Speichern wird automatisch eine gegengleiche Buchung in den Zahlungen der entsprechenden Eingangsrechnung gemacht. Daher ist die Buchung der Gegenverrechnung aus Sicht der Eingangsrechnung derzeit nicht möglich.

<a name="Gutschriftszahlung"></a>
### Zahlungsart: Gutschrift

Es kommt immer wieder vor, dass Ausgangsrechnungen, zumindest teilweise, durch Gutschriftsbeträge ausgeglichen werden. Um den Ausgleich einer Ausgangsrechnung durch eine Gutschrift zu buchen wird diese "Zahlung" bei der Ausgangsrechnung hinterlegt.

Wählen Sie die durch die Gutschrift auszugleichende Rechnung, wechseln Sie in den Reiter Zahlungen und nach Klick auf Neu wählen Sie die Zahlungsart Gutschrift aus. Nun klicken Sie auf den Knopf ![](Zahlung_Gutschrift.gif) Gutschrift und wählen die Gutschrift mit der diese Rechnung ausgeglichen wird aus. Wählen Sie nur offene Gutschriften aus. Durch diesen Vorgang wird die Gutschrift automatisch auf bezahlt gesetzt. Gegebenenfalls muss die Rechnung noch auf vollständig erledigt gesetzt werden.

**Tipp:**

Wurde für eine Rechnung teilweise eine Gutschrift ausgestellt, so buchen Sie zuerst die Gutschrift in die Zahlung und danach erst den vom Kunden erhaltenen Betrag. So sehen Sie sofort, ob der Kunde tatsächlich den vereinbarten Betrag bezahlt hat.

### Zahlungsart: Wechsel
Da diese Zahlungsart derzeit sehr ungewöhnlich ist und auch die richtige Verbuchung der verschiedenen Stati der verschiedenen Wechselarten komplex wird, haben wir auf die Umsetzung bis auf weiteres verzichtet. Sollten Sie wirklich Wechsel akzeptieren / ausstellen, so legen Sie bitte ein passendes Bankkonto an und verbuchen Sie die Zahlung der Rechnung auf dieses Konto. Die tatsächlich offenen Wechsel sehen Sie dann beim entsprechenden Konto.

<a name="vorauszahlung"></a>
### Zahlungsart: Vorauszahlung

Um die Zahlungsart Vorauszahlung zu verwenden gehen Sie wie folgt vor: (hierzu benötigen Sie das Modul Integrierte Finanzbuchhaltung von **Kieselstein ERP**)

Erstellen Sie eine neue manuelle Buchung im Modul Finanzbuchhaltung als Umbuchung Bank an Debitor. Beachten Sie bitte, dass eine Zahlung immer als steuerfrei zu definieren ist.

![](vorauszahlung_re_buchung.PNG)

Nun erstellen Sie in der Eingangsrechnung eine neue Zahlung mit der Art Vorauszahlung.

Mit Klick auf den Button Vorauszahlung erhalten Sie eine Liste aus den "Offenen Posten" mit Buchungsart Umbuchung des jeweiligen Kreditors.

Wählen Sie die gewünschte Buchung aus und bestätigen mit Enter / Doppelklick.

![](vorauszahlung_re.PNG)

Nun wird eine neue Zahlung der Rechnung erfasst. Ist die Vorauszahlung höher als der Rechnungsbetrag wird der Rest als neue Vorauszahlungs-Umbuchung gebucht und der Saldo bleibt am Debitorenkonto erhalten. 

{{% alert title="Beachte" color="primary" %}}
Um diese Funktion im jeweiligen Modul nutzen zu können, ist die richtige Verwendung von Soll und Haben wichtig.<br>
D.h. bei Vorauszahlungen an einen Kreditor(Lieferanten) muss der erhaltene Betrag am Kreditorenkonto im Soll gebucht sein.<br>
Eine Vorauszahlung eines Debitors(Kunden) muss der erhaltene Betrag am Debitorenkonto im Haben gebucht sein.<br>
Ist dem nicht so, werden keine Vorauszahlungen zur Verbuchung angeboten.
{{% /alert %}}

#### Ich habe mich bei der Erfassung der manuellen Buchung für die Vorauszahlung vertippt - wie korrigiere ich den Fehler?
Um die manuelle Buchung zu korrigieren, die schon als Vorauszahlung bei einem Beleg hinterlegt ist, gehen Sie bitte wie folgt vor:

Wechseln Sie in den Reiter Zahlungen beim entsprechenden Beleg. Nun löschen Sie die Zahlung der zu ändernden Vorauszahlung. Im Anschluss kann die Umbuchung z.B. im Kreditorenkonto bearbeitet werden. Korrigieren Sie die Buchung und erfassen die Zahlung beim Beleg nochmals neu.

<a name="Vorgeschlagene Bank definieren"></a>

#### Bei der Zahlung wird keine Bankverbindung vorgeschlagen?
Welche Bank für eine neue Zahlung vorgeschlagen wird, kann unter System, unterer Modulreiter Mandant, oberer Modulreiter Vorbelegungen, Bereich Allgemein, Bankverbindung definiert werden. Für die Definition Ihrer eigenen Bankverbindungen für die Zahlungen siehe bitte Definition der [eigenen Bankverbindungen]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen" >}} ).

#### Welcher Wert wird im Feld "Auszug" im oberen Modulreiter "Zahlung" eingetragen?
Bei einer Zahlung über die Bank, werden in aller Regel diese Zahlungen in den Kontoauszügen der Bank dokumentiert. Damit Sie eine eindeutige Zuordnung der Zahlung zur Buchung auf Ihrem Bankkonto haben, geben Sie hier die Kontoauszugsnummer mit dem die Zahlung durchgeführt wurde an.

Gerade wenn auch das Modul integrierte Finanzbuchhaltung verwendet wird, so achten Sie darauf, dass die Kontoauszugsnummern chronologisch fortschreitend sind und exakt die Kontoauszugsnummern Ihrer Bank-Kontoauszüge widerspiegeln. Eine Vergabe von Kontoauszugsnummern z.B. in Form eines Datums, noch dazu in deutscher Schreibweise ist dafür völlig ungeeignet.

Sollten Sie Kontoauszüge ohne laufenden Kontoauszugsnummern erhalten, z.B. bei Internetbanking, so drucken Sie z.B. die jeweils verbuchten Bankbelege aus und vergeben diesen manuell eine aufsteigende Auszugsnummer.

Nur damit erhalten Sie eine entsprechende Übersicht über das jeweilige Bankkonto.

#### Wie werden Kreditkartenabrechnungen erfasst?
Genau betrachtet ist eine Kreditkarten Zahlung nichts anderes als ein Bankkonto bei einem weiteren Bankinstitut. D.h. legen Sie in den Bankverbindungen (Modul Finanzbuchhaltung) eine weitere Bank an und buchen Sie die Zahlungen auf diese Bank. Als Kontoauszugsnummer geben Sie die laufende und aufsteigende Abrechnungsnummer des Kreditkarteninstitutes an.

Haben Sie auch das Modul integrierte Finanzbuchhaltung, so wird der Kontoausgleich Kreditkartenkonto gegen Bank mit der Abbuchung von Ihrem Bankkonto gebucht. Hier sind entsprechend zwei Kontoauszugsnummern anzugeben.

#### Der Skontobetrag bleibt immer offen?
Ja das ist richtig so. Auch wenn ein Kunde eine Rechnung mit Skonto bezahlt, so sehen Sie in der Darstellung unter Offen den nicht bezahlten Betrag. Zugleich wird darunter der Erledigt Status der Rechnung mit dem Haken bei Erledigt angezeigt. D.h. obwohl der Kunde den Betrag unter offen weniger Bezahlt hat, haben Sie die Rechnung als erledigt akzepiert.
Der Vorteil ist, dass Sie immer sehen um wieviel weniger der Kunde tatsächlich bei dieser Rechnung bezahlt hat.

#### Wird der skontierte Betrag bei der Zahlung vorgeschlagen?
Ja. Wenn in der Definition der Zahlungsziele (System, Mandant, Zahlungsziel) sowohl Skontotage als auch Skontoprozente definiert sind, so werden bei der Bezahlung einer Rechnung auch die entsprechenden skontierbaren Beträge angezeigt.
![](Zahlung_Skonto_Auswahl.gif)
Durch Klick auf Übernehmen in der entsprechenden Skontozeile wählen Sie den skontierten Betrag als Zahlbetrag aus. Da Sie damit die Rechnung als Vollständig bezahlt akzeptieren wird automatisch der Haken bei Erledigt vorbesetzt, sodass die Zahlung nur mehr gespeichert werden muss.ybr
**Hinweis:** Ist im Zahlungstext zwar ein Skonto angeführt, aber werden keine skontierbaren Beträge vorgeschlagen, so sind in der Definition des Zahlungszieles entweder der Skontoprozentsatz oder die Skontotage nicht angegeben. Ergänzen Sie bitte diese Daten.
![](Zahlung_Skonto_Definition.gif)

<a name="Forderungsausfall"></a>

#### Wie können nicht bezahlte Rechnungen gebucht werden?
Es kommt leider immer wieder vor, dass Rechnungen teilweise oder vollständig nicht bezahlt werden.

Es gibt nun zwei Vorgehensweisen wie diese Rechnungen erledigt werden können.

1. Die Einfache, welche den Minderbetrag als Skontoaufwand/-erlös bucht
2. die aus Buchhaltungssicht bessere, welche dies auf Forderungsausfall bucht.

### 1. Ausbuchen als Skontoaufwand / -erlös
Wählen Sie bei der Rechnung den oberen Modulreiter Zahlungen.

Legen Sie eine neue Zahlung an, wählen Sie die Zahlungsart Bank, wählen Sie Ihre übliche Bank aus, geben Sie als Auszugsnummer entweder die aktuelle Auszugsnummer oder Auszugsnummer 0 an. Geben Sie nun den (Zahl-)Betrag mit 0 ein und haken Sie das Erledigt an.

Nun klicken Sie auf speichern und bestätigen die Meldung, "Der eingegebene Betrag ist kleiner als der offene. Wirklich speichern" mit Ja. Damit ist die Rechnung, ohne Zahlung als Erledigt verbucht.

Bei einer Anbindung der **Kieselstein ERP** Buchhaltung wird der Differenzbetrag als Skontoaufwand/-erlös verbucht.

### 2. Verbuchen als Forderungsausfall

Legen Sie eine [Bankverbindung](#Bankverbindung) an, mit dem Kontonamen Forderungsausfall. Die Bankverbindung zeigt auf das Sachkonto Forderungsausfall.

![](bank_forderungsausfall_defini.JPG)

Nun buchen Sie auf die Bank Forderungsausfall die "Zahlung", also den offenen Betrag, der nicht bezahlt wurde.<br>
Somit ist die Rechnung ebenfalls als vollständig bezahlt eingetragen und damit erledigt und der Forderungsausfall (die uneinbringliche Forderung) wurde auf das entsprechende Konto verbucht.

Hinweis: Diese Vorgehensweise ist für **Kieselstein ERP** Anwender OHNE integrierter Finanzbuchhaltung die einfachste Vorgehensweise. Anwender mit intergrierter Finanzbuchhaltung gehen, wegen der richtigen Buchung der Umsatzsteuer, bitte wie folgt vor:
- legen Sie das Sachkonto Forderungsausfall an. Dieses Sachkonto ist im Kontokreis der Erlöse anzuordnen.
- Legen Sie eine Artikelgruppe an, die als Konto das Sachkonto Forderungsausfall hinterlegt hat
- Legen Sie einen Artikel z.B. Forderungsausfall an, der als Artikelgruppe oben definierte Artikelgruppe hinterlegt hat.<br>
Stellen Sie diesen Artikel auf nicht lagerbewirtschaftet. Gegebenenfalls verstecken Sie diesen Artikel oder hinterlegen eine entsprechende Sperre, sodass er im täglichen Geschäft nicht versehentlich verwendet wird.
- Erstellen Sie nun eine Gutschrift, Gutschriftsart Wertgutschrift, in der der Wert des Forderungsausfalls mit dem Artikel Forderungsausfall dargestellt wird.
Schreiben Sie in die Gutschrift warum Sie diese rein interne Gutschrift erstellen. Z.B. Gerichtsbeschluss vom ...
-   Aktivieren Sie die Gutschrift, z.B. dadurch dass diese ausgedruckt wird.
-   Bezahlen Sie nun die Rechnung(en) durch diese Gutschrift

Damit wurde die Umsatzsteuer-Reduktion anhand der Gutschrift gebucht und beide Belege, Rechnung als auch Gutschrift sind erledigt.<br>
Durch die Einordnung des Sachkontos in den Kontenkreis Erlöse reduziert sich ihr Erlös. Einzig die Darstellung in der Artikelstatistik auf dem Artikel, den Sie zwar zu einem Betrag verkauft haben, für den Sie aber keinen tatsächlichen Erlös erzielt haben, fehlt.
Dies gehört, jedoch genauso wie der Skontoerlös zum Unternehmerischen Gewinn gehört, so gehört auch dies nur mehr in der Finanzbuchhaltung abgebildet.

<a name="Gutschrift bezahlen"></a>

#### Wie kann eine Gutschrift bezahlt werden?
Auch im Modul Gutschrift gibt es den Reiter Zahlungen, sowohl in der Eingangsrechnung als auch in der Ausgangsrechnung.<br>
Da Gutschriften in aller Regel durch eine Rechnung ausgeglichen werden, muss dieser Ausgleich in der Rechnung verbucht werden.<br>
D.h. für die Erledigung / Zahlung einer Gutschrift gehen Sie bitte in die Rechnung, die mit der Gutschrift ausgeglichen wird und wählen hier in der Zahlung die [Zahlungsart Gutschrift](#Gutschriftszahlung). Damit wird automatisch auch in der Gutschrift eine entsprechende gleichlautende Zahlungsbuchung hinterlegt.

#### Können Kommentare zu den Zahlungen erfasst werden?
Für Dokumentationszwecke, um die Buchungen auch zu einem späteren Zeitpunkt noch nachvollziehen zu können, gibt es die Möglichkeit einen Kommentar zur Zahlung und der Splittbuchung zu erfassen.
Dieser Kommentar wird in den Modulen der Eingangsrechnung und Ausgangsrechnung und auch in der Integrierten Finanzbuchhaltung von **Kieselstein ERP** angezeigt.
Sie können den Kommentar durch einen Klick auf den Textblock erfassen, wenn Sie im Bearbeiten der Buchungszeile sind. Sobald hier ein Eintrag besteht wird das Icon grün gefärbt ![](Kommentar_gruen.JPG).
In der Liste der Zahlungseinträge und der Finanzbuchhaltung wird in der Spalte Kommentar ein Haken gesetzt, wenn ein Kommentar zu der Zeile besteht ![](kommentar_hakerl.JPG).
Wenn Sie nun die Maus über die Buchung bewegen, wird der Eintrag angezeigt.

#### Können Zahlungen auch automatisiert erfasst werden?
Ja. Siehe dazu bitte [SEPA-Kontoauszugsimport]( {{<relref "/management/finanzbuchhaltung/kontoauszug_import/" >}} )

#### Können SEPA Lastschriften erstellt werden?
Ja. [Siehe dazu bitte]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag/#sepa-lastschriftverfahren" >}} )

#### Wie kann ich eine Rechnung mit Gutschrift und Skonto ausgleichen?
Wenn als letzte Zahlung einer Rechnung eine Gutschrift eingetragen wird und der verbliebene Rest als Skonto ausgebucht werden sollte, so kommt die Fehlermeldung
![](Erledigung_Gutschrift_mit_Skonto.jpg)
Die Erledigung einer Rechnung mittels Skonto während einer Gutschriftszahlung bitte mittels Durchläufer handhaben.

Das bedeutet, dass der offene Betrag entweder direkt als "Bank" auf das Skontokonto gebucht wird, oder eben dass als "Bank" das Durchläuferkonto angegeben wird und dann das Durchläuferkonto gegen den Skontoaufwand wieder ausgeglichen wird.

<a name="Eingangsgutschrift mit Skonto zahlen"></a>

#### Wie ist bei der Skontozahlung von Eingangsgutschriften vorzugehen?
Wenn Sie von Ihren Lieferanten Rechnungen mit einem Zahlungsziel mit Skonto erhalten und nun auch einmal eine Gutschrift von diesem Lieferanten erhalten, so ist es eine faire Vorgehensweise, wenn das Skonto bei der Zahlung der Eingangsrechnung aber auch der Eingangsgutschrift berücksichtigt wird.<br>
Um nun, gerade für die **Kieselstein ERP** Anwender mit integrierter Buchhaltung, dies mit automatischer Verbuchung des negativen Skontoertrages durchführen zu können, muss der Zahlungsausgleich zwischen den Eingangsrechnungen (und der Eingangsgutschrift) über ein Durchläuferkonto gebucht werden.<br>
Gehen Sie dazu bitte wie folgt vor:
1. Legen Sie eine Bankverbindung Durchläufer (o.ä.) an, [siehe]( {{<relref "/management/finanzbuchhaltung/#definition-der-eigenen-bankverbindungen" >}} ).
2. Buchen Sie den tatsächlich gegenverrechneten Betrag gegen das Durchläufer Konto
3. Erledigen Sie die Rechnung und verbuchen Sie damit den (negativen) Skontoertrag.

Beispiel:
1. eine Eingangsgutschrift mit negativem Skontoertrag kann nicht vollständig abgeschlossen werden. Dies stellt sich z.B. in der Eingangsgutschrift wie folgt dar:
![](Zahlung_ER_Gutschrift1.jpg)

Die Gegenseite auf der ursächlichen Eingangsrechnung sieht z.B. wie folgt aus:
![](Zahlung_ER_Gutschrift2.jpg)

2. Die Abwicklung über das Durchläuferkonto könnte dann ungefähr wie folgt aussehen:
Tipp:
Wenn Sie bei der Kontoauszugsnummer der Buchung in das Durchläuferkonto die ER Nummer der ursächlichen Eingangsrechnung mit angeben, haben Sie im Durchläuferkonto ebenfalls einen entsprechenden Zusammenhang.
![](Zahlung_ER_Gutschrift3.jpg)

Wichtig:
Ändern Sie nun die Eingangsgutschrift auf die Eingangsrechnungsart Eingangsrechnung, belassen Sie aber, da ja Gutschrift, den negativen Rechnungsbetrag.
![](Zahlung_ER_Gutschrift4.jpg)

Gleichen Sie nun die Eingangsrechnungsgutschrift entsprechend gegen das Durchläuferkonto aus.
![](Zahlung_ER_Gutschrift5.jpg)

Die korrespondierenden Buchungen auf dem Durchläuferkonto sehen dann z.B. wie folgt aus:
![](Zahlung_ER_Gutschrift6.jpg)

Damit ist auch der negative Skontoerlös gebucht und die Vorsteuer entsprechend berücksichtigt.

#### Verbuchen der Zahlung auf eine nicht kontierte Eingangsrechnung?
Die Buchung einer Zahlung auf eine nicht kontierte Eingangsrechnung, ist nicht nur bei integrierter Finanzbuchhaltung, nicht zulässig, da alle mit der Kontierung verbundenen Buchungen damit ebenfalls nicht richtig erfolgen können. Daher sind auch die Knöpfe für neu usw. deaktiviert.
D.h. um eine Eingangsrechnung bezahlen zu können, muss diese, nach bestem Wissen, richtig kontiert sein. Siehe dazu auch Rechnungslegungsgesetz, Prüfung der Vollständigkeit von Eingangsrechnungen. Ist eine Eingangsrechnung noch nicht zur Zahlung freigegeben, so nutzen Sie bitte die Funktion der Freigabe / Prüfung einer Eingangsrechnung. D.h. erfassen Sie diese vollständig, geben diese aber nicht frei.
Sollten Sie nach der Zahlung einer Eingangsrechnung feststellen, dass diese anders zu kontieren ist, so kontieren Sie diese, Voraussetzung entsprechende Rechte, um. [Siehe dazu auch]( {{<relref "/einkauf/eingangsrechnung/#kann-die-kontierung--kostenstelle-einer-eingangsrechnung-auch-ge%c3%a4ndert-werden-wenn-diese-schon-bezahlt-ist" >}} ).