---
categories: ["Rechnung"]
tags: ["Gebühren"]
title: "Stempelgebühren"
linkTitle: "Gebühren"
weight: 60
date: 2023-01-31
description: >
  Automatische Verrechnung von Stempelgebühren
---
Stempelgebühren
============

In manchen Ländern, z.B. Italien, muss bei einer Verrechnung in andere Länder für jede Rechnung eine entsprechende Stempelgebühr an das Finanzam / den Staat abgeführt werden.

Um dies abzubilden, ist die Vorgehensweise wie folgt:
- Legen einen Gebührenartikel mit der rechtlich richtigen Bezeichnung an
- definiere diesen Artikel als nicht Lagerbewirtschaftet
- für jedes (Export-) Land, für das diese Gebühr ausgewiesen werden muss, hinterlege im System, System, Land unter dem Reiter Gebührenartikel den oben definierten Artikel.
  - Kommt die Warnung kein gültiger Verkaufspreis vorhanden<br>
![](Warnung_Gebuehrenartikel.png)<br>
so muss zuerst mit dem passenden Verkaufspreis gültig ab ein Verkaufspreis für den Artikel hinterlegt werden.<br>
![](Verkaufspreis_Gebuehrenartikel.png)<br>


