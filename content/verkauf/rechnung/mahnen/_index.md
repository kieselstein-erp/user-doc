---
categories: ["Rechnung"]
tags: ["Mahnen"]
title: "Rechnung"
linkTitle: "Mahnen"
weight: 40
date: 2023-01-31
description: >
  Offene / überfällige Ausgangsrechnungen mahnen, automatisch oder manuell
---
Mahnen der Rechnungen
=====================

Mit dem in **Kieselstein ERP** implementierten Mahnwesen wird sowohl der Bereich der Einzelmahnung als auch der Sammelmahnung abgedeckt.
Für das Verhalten des Mahnwesens sind die Einstellungen der Mahnstufen und der Mahntexte, welche Sie im Modul Rechnung, Grunddaten vornehmen, relevant.
Basis des Mahnwesens ist das Rechnungsdatum und das Feld Tage Netto des Zahlungszieles (siehe System, Mandant, Zahlungsziel).
Bei dem Mahnlauf bestimmen das Rechnungsdatum plus Tage Netto die Fälligkeit der Rechnung.
Zu diesem Datum werden noch die Mahntage der ersten Mahnstufe hinzugerechnet. Ab dem sich nun ergebenden Datum wird die Rechnung als zu mahnen vorgeschlagen.
Wurde eine Rechnung gemahnt, so bestimmen wiederum die Mahntage der nächst höheren Mahnstufe, wann die Rechnung erneut zur Mahnung vorgeschlagen wird (wenn sie bis dahin nicht bezahlt wurde). Hier wird als Bezug jedoch das Datum der letzten Mahnung verwendet.

Ein Beispiel:
> Rechnungsdatum:    1\. 4.2005<br>
> Zahlungsziel: 8Tage 2%, 30 Tage netto<br>
Fälligkeit:               1.5.2005<br>
In der Mahnstufe zur ersten Mahnung sind 5Tage eingetragen.<br>
Mahnlauf erfolgt am 2.5.2005<br>
    Die Rechnung wird nicht gemahnt.<br>
Mahnlauf erfolgt am 9.5.2005<br>
    Die Rechnung kommt in Mahnstufe 1 als letztes Mahndatum wird der 9.5.2005 eingetragen<br>
In der Mahnstufe zur zweiten Mahnung sind 14Tage eingetragen.<br>
Mahnlauf erfolgt am 20.5.2005<br>
    Die Rechnung wird noch nicht erneut gemahnt.<br>
Mahnlauf erfolgt am 24.5.2005<br>
    Die Rechnung wird mit Mahnstufe zwei gemahnt.<br>
    Sind nun entsprechende Kostensätze für Mahnspesen und Mahnzinsen hinterlegt, so wird dies ebenfalls auf die Mahnung aufgerechnet.<br>
>

<a name="Mahnstufen definieren"></a>Es können bis zu 98 Mahnstufen definiert werden.
Die Mahnstufe 99 wird als sogenanntes Rechtsanwaltsschreiben geführt. Dies bedeutet, dass durch Erreichen dieser Mahnstufe die weitere Mahnung nicht mehr von **Kieselstein ERP** unterstützt wird. Vielmehr wird, durch Sie, diese Rechnung den Gerichten, Ihrem Rechtsanwalt übergeben. Mit diesem Schreiben beauftragen Sie Ihren Rechtsanwalt mit der Eintreibung Ihrer Forderungen.
Die Rechnung in Mahnstufe 99 wird von **Kieselstein ERP** nach wie vor als offen geführt, aber nicht mehr gemahnt.
**ACHTUNG:** In der Liste der zu mahnenden Rechnungen erscheint diese Rechnung nicht mehr.

Beispiel für übliche Mahnstufen:
![](Mahnstufen.gif) **<u>Hinweis:</u>** Ein Mahnlauf basiert auf den zum Zeitpunkt des Mahnlaufes gebuchten Beträgen. Wenn Sie nach Durchführung des Mahnlaufes Zahlungen einbuchen, so wird dadurch der Status, der offene Wert der jeweiligen Rechnung im Mahnlauf **NICHT** verändert.

<a name="Mahnlauf durchführen"></a>Durchführen eines Mahnlaufes, Sammelmahnung

Unter ![](Mahnlauf.gif) klicken Sie auf neu ![](Neuer_Mahnlauf.gif).

Nun werden alle offenen Rechnung des Mandanten nach den obigen Regeln durchsucht und die zu mahnenden Rechnungen in die Mahnliste aufgenommen. Bitte beachten Sie dazu auch die Einstellung des Parameters MINDEST_MAHNBETRAG, dieser steuert ab welchem Betrag eine Mahnung erstellt wird. In der Auswahlliste der Mahnläufe erhalten Sie einen neuen Eintrag. In diesem ist 

![](Mahnliste.gif)  

das Datum des Mahnlaufes, die Anzahl der zu mahnenden Rechnungen und die Anzahl der davon noch nicht gemahnten Rechnungen angeführt.

Im Reiter ![](Mahnungen.gif) sehen Sie nun eine Liste aller zu mahnenden Rechnungen.

Sie können hier die Mahnung einer einzelnen Rechnung ausdrucken ![](Mahnung_Einzeln_Drucken.gif), 

eine einzelne Rechnung aus der Mahnliste herauslöschen ![](Mahnlauf_loeschen.gif),

und mit ![](Mahnung_Einzeln_aendern.gif) die Mahnstufe der Rechnung verändern.

Eine bereits gemahnte Rechnung, also eine gedruckte Mahnung kann mit ![](Mahnlauf_zuruecknehmen.gif) wieder zurückgenommen werden.

![](Mahn_Zeile.gif)

Der Status des Mahnungsdruckes ist anhand des Häkchens am rechten Rand ersichtlich.

![](Hackerl.gif) = Mahnung bereits gedruckt. 

Gedruckte Mahnungen reduzieren auch die Anzahl der Offenen in der Sicht Mahnungslauf.

Wurden alle Mahnungen eines Mahnlaufes versandt, so kann ein neuer Mahnlauf gestartet werden.

Wichtig: Wird der Mahnungsdruck aus der Sicht ![](Mahnlauf.gif) gestartet, so werden sogenannte Sammelmahnungen erstellt. D.h. pro Kunde wird nur eine Mahnung versandt.

Wird der Mahnungsdruck aus der Sicht ![](Mahnungen.gif) gestartet, so wird nur diese eine Rechnung gemahnt. Ein nachfolgender Mahnungsdruck aller Mahnungen könnte eine weitere Mahnung an den gleichen Kunden senden.

Mahnlauf zurücknehmen![](Mahnlauf_zuruecknehmen.gif)

Im Modulreiter Mahnläufe. Damit werden alle bereits als ausgedruckt gekennzeichneten Mahnungen wieder als nicht ausgedruckt gekennzeichnet. Damit können Sie bereits versandte Mahnungen erneut versenden.

Mahnlauf löschen ![](Mahnlauf_loeschen.gif)

Im Modulreiter Mahnläufe. Hiermit löschen Sie den gesamten Mahnlauf. Z.B. wenn Sie vor dem Erstellen des Mahnlaufes vergessen haben Zahlungen einzubuchen.

Einzelmahnung

Aus der Auswahlliste der Rechnungen heraus gibt es eine weitere Möglichkeit gezielt einzelne Rechnungen zu mahnen.

Markieren Sie dazu die Rechnung und wählen Sie Rechnung mahnen.

![](Einzelmahnung.gif)

Nun können Sie gezielt die Mahnung dieser einen Rechnung definieren.

Bitte beachten Sie dass diese Einzelmahnung ebenfalls in die Verwaltung der Mahnungen / der Mahnlisten aufgenommen wird.

Bei einer neuen Mahnstufe wir ein neuer Mahnlauf mit nur dieser einen Rechnung angelegt. Bei einer bestehenden Mahnstufe wird der dazugehörende Mahnlauf verwendet.

Zum Zurücknehmen der Mahnstufe wählen Sie Rücknehmen der letzten Mahnung und klicken auf aktualisieren ![](Mahnenaktualisieren.gif).

Bitte beachten Sie, dass beim Zurücknehmen der Mahnstufe der dazugehörende Mahnlauf nicht gelöscht wird.

Bitte beachten Sie, dass abhängig von wo aus die Mahnungen gedruckt werden unterschiedliche Formulare zur Anwendung kommen.

Wird die Mahnung aus der Rechnung (Rechnung, Mahnen) oder aus der Liste der Mahnläufe heraus gedruckt, so wird das Formular für die Einzelmahnung verwendet (finanz_mahnung). Wird aus der Auswahlliste der bisherigen Mahnläufe der Druck der Mahnungen aufgerufen, so werden alle Mahnungen dieses Mahnlaufes als Sammelmahnung gedruckt und es wird das Formular der Sammelmahnung (finanz_sammelmahnung) verwendet.

![](Sammelmahnung_Drucken.gif)

Die Sammelmahnung bewirkt, dass pro Kunde nur ein Mahnformular mit allen zu mahnenden Rechnungen gedruckt wird.

Hinweis: Bitte beachten Sie, dass das Formular der Sammelmahnung immer nur beim Start des Sammelmahnungslaufes geladen wird.

Sonderform der Mahnung

Manche Kunden mancher **Kieselstein ERP** Anwender zahlen die Rechnungen schneller, wenn Sie die Rechnung explizit ausgedruckt erhalten. Daher gibt ist den Systemparameter AUSFUERHLICHER_MAHNUNGSDRUCK_AR. Steht dieser auf 1, so wird die Mahnung mit den Inhalten der Rechnung ausgedruckt und zusätzlich noch die Mahnstufe übergeben.

Hinweis: Diese Sonderform des Ausdruckes, steht nur unter Mahnwesen, Mahnungen (der Mahnläufe) zur Verfügung.

Versenden der Mahnungen nicht möglich

Erscheint beim Email- bzw. Faxversand der Sammelmahnungen folgende Fehlermeldung, so ist bei den angeführten Kunden keine EMailadresse bzw. Faxnummer hinterlegt. Tragen Sie diese bitte nach und starten Sie dann den Versand erneut.

![](Mahnen_fehlende_Faxnummer.jpg)

#### Es gibt bereits Mahnungen zu dieser Rechnung
![](es_gibt_bereits_Mahnungen.jpg)

Wenn bereits eine Mahnung zu einer Rechnung versandt wurde, so kann diese nicht mehr verändert werden.

Sollte die Rechnung trotzdem verändert werden müssen, so müssen die Mahnungen dieser Rechnung aus den Mahnungsläufen entfernt werden.

Das Datum des jeweils letzten Mahnlaufes sehen Sie in den Kopfdaten. Gehen Sie mit diesem Datum auf den unteren Modulreiter Mahnwesen, wählen Sie den Mahnlauf des letzten Mahndatums aus den Kopfdaten, wechseln Sie auf den Modulreiter Mahnungen und löschen Sie daraus die entsprechende Mahnung. Wurde die Mahnung bereits gedruckt, so muss zuerst noch der Druck zurückgenommen werden.

#### Üblicher Ablauf der Mahnungen
In der Praxis hat sich folgende Vorgehensweise im Mahnwesen bewährt.
1.  Es werden die Zahlungen (die Bank) in die Ausgangsrechnungen eingetragen.
2.  Es wird ein neuer Mahnlauf erstellt
3.  Es werden alle Mahnungen aus dem Mahnlauf als Sammelmahnung oder auch als Einzelmahnungen aus dem Modul Mahnwesen (!!) versandt.

In vielen großen und kleinen Unternehmen wird dieser Lauf einmal pro Woche durchgeführt. Dies ist sicherlich die effizienteste Form, um einen effektiven Zahlungseingang sicherzustellen.

#### Wieso sind in der Liste der Mahnungen auch Gutschriften angeführt?
In aller Regel ist der Mahnlauf ja auch immer eine Betrachtung der offenen und überfälligen Rechnungen.<br>
Da bei überfälligen Rechnungen immer auch Gutschriften zu berücksichtigen sind, werden diese im jeweiligen Mahnlauf mit angeführt. Drucken Sie nun eine Sammelmahnung für den Kunden aus, so werden hier auch die Gutschriften angeführt und Sie mahnen nur den offenen Betrag.<br>
Daher werden auch Rechnungen die bereits in der höchsten Mahnstufe sind mit angeführt. Es geht immer darum, dass Sie einen Überblick über die Zahlungssituation ihres jeweiligen Kunden haben.

#### Gibt es einen Mindestmahnbetrag?
Ja. Dieser kann in den Mandantenparametern unter MINDEST_MAHNBETRAG eingestellt werden. Er ist standardmäßig auf 5,- (€) eingestellt. Dies bedeutet für Sie auch, dass Sammelmahnungen deren Saldo unter diesem Mindestmahnbetrag ist, nicht versandt werden. Damit wird auch erreicht, dass alleinstehende Gutschriften, die zu Ihrer Information angezeigt werden, nicht gemahnt werden.

#### Wo sieht man Rechnungen mit Mahnsperren ?
Im unteren Modulreiter Mahnwesen (im Modul Rechnungen) werden im oberen Modulreiter 3 Mahnsperren die aktuell eingetragenen Mahnsperren auf den Ausgangsrechnungen angezeigt.

#### Wie testet man Sammelmahnungen?
Gerade zu Beginn ist es erforderlich auch die Mahnungsformulare und Texte an Ihre Bedürfnisse anzupassen. Da man dabei manchmal die Mahntage der Mahnstufen verstellt um auch entsprechende Mahnungen zu erhalten, sollten diese auf keinen Fall zum Kunden gelangen. Das Einfachste ist, die Sammelmahnungen direkt zu drucken. Nach erfolgtem Druck sind alle Mahnungen auf bereits gemahnt gestellt und können durch Klick auf Mahnlauf löschen ![](Mahnlauf_zuruecknehmen.gif) wieder als Mahnung noch nicht gedruckt gesetzt werden.<br>
Beachten Sie, dass damit die auch die Rechnungen mit Mahnsperren und die Gutschriften usw. als nicht mehr gemahnt gekennzeichnet sind. Umgekehrt kann auch der ganze Mahnlauf gelöscht und neu erstellt werden.

#### Können Kunden ab einer gewissen Mahnstufe automatisch gesperrt werden?
<a name="automatische Liefersperre"></a>
Mit dem Parameter LIEFERSPERRE_AB_MAHNSTUFE kann eingestellt werden, ab welcher Mahnstufe bei einem Kunden automatisch die Liefersperre gesetzt wird. Nutzen Sie dies um entsprechende Infos an Ihre Mitarbeiter über das Zahlungsrisiko zum Kunden weiterzugeben. **Hinweis:** Bei der Rücknahme der Mahnstufe erscheint eine Warnung und es muss von Ihnen selbst entschieden werden, ob Sie bei dem Kunden die Liefersperre belassen oder nicht. **Hinweis:** Wird die auslösende Rechnung für die [Liefersperre]( {{<relref "/docs/stammdaten/kunden/#setzen--l%c3%b6schen-von-liefersperren">}} ) bezahlt, so bleibt ebenfalls die Mahnsperre bestehen, bis diese von Ihnen zurückgenommen wird.<br>
Siehe dazu auch [Mahnsperre]( {{<relref "/verkauf/rechnung/#mahnsperren">}} ).<br>
Nutzen Sie dafür gegebenenfalls auch die Hinweise bei Kundenzahlung im Kundenkommentar.

#### Wo können Mahnspesen definiert werden?
<a name="Definition der Mahnspesen"></a>
Die Basismahnspesen definieren Sie in den Grunddaten der Rechnung, im Reiter Mahnstufe.
Geben Sie hier neben den Mahntagen auch den Zinssatz, dieser wird vom Jahreszinssatz auf die Tage umgerechnet, und die Spesen für die jeweilige Mahnstufe an.<br>
Für Fremdwährungs-Mahnspesen definieren Sie diese im Modul Finanzbuchhaltung, unterer Reiter Währungen.<br>
Hier wählen Sie die Fremdwährung aus und definieren im Reiter Mahnspesen die Mahnspesen in der gewählten Währung. Sind in der Fremdwährung keine Mahnspesen definiert, so werden die Standardmahnspesen in die Fremdwährung umgerechnet.

<a name="Mahnungsdruck"></a>

#### Kann ich bei der Mahnung auch die Rechnungsdaten mitschicken?
Ja, bitte stellen Sie den Parameter AUSFUEHRLICHER_MAHNUNGSDRUCK_AR auf 1. 
Dieser gibt an ob in Mahnungen die Mahnungsdaten zu sehen sind oder die Rechnungsdaten + Mahnstufe.
Das bedeutet, dass, wenn dieser Parameter eingeschalten ist, Sie an Ihre Kunden die Rechnung nochmals verschicken mit den zusätzlichen Informationen der Mahnung.

#### Wie ist zur Definition der Mahntexte vorzugehen?
Um die Mahntexte der verschiedenen Mahnstufen zu definieren wechseln Sie in den Grunddaten der Rechnung in den oberen Reiter Mahntext. Definieren Sie hier nun den für die jeweilige Mahnstufe gewünschten Text. Bitte beachten Sie, dass die Definitionen der Mahnstufen und der Mahntexte übereinstimmen.

#### Beim Mahnen wird Mahntext1, 2, .. angedruckt?
Bitte definieren Sie wie oben beschrieben die Mahntexte und auch die Mahnstufen.

#### Können auch Lastschriften definiert werden ?
[Ja, siehe bitte]( {{<relref "/management/finanzbuchhaltung/zahlungsvorschlag/#welche-bank-wird-f%c3%bcr-die-sepa-lastschrift-verwendet" >}} )

#### Wie gehe ich für die Mahnungen vor?
Unser Rat ist, dass die Zahlungsziele und die Mahnfristen (Reiter Mahnstufen der Grunddaten der Rechnung) richtig eingestellt sind.<br>
Damit haben Sie die sehr praktische Möglichkeit die offenen und überfälligen Ausgangsrechnungen fast automatisch zu mahnen.<br>
Dazu legen Sie wie oben beschrieben einen neuen Mahnlauf an.<br>
Idealerweise sortieren Sie die Liste der Mahnungen nach Kunden und innerhalb der Kunden nach Rechnungsnummer (Klick auf Kunden, Klick mit Strg gedrückt auf Rech_Nr. Speichern Sie dies ab)<br>
Nun sehen Sie welche Rechnung zu mahnen ist. Sollte nun doch eine Rechnung nicht zu mahnen sein, so löschen Sie diese bitte aus dem Mahnlauf heraus.<br>
Das ist vor allem deswegen wichtig, da durch den Eintrag in diese Mahnliste die Mahnstufe der Rechnung erhöht wird. D.h. wenn Sie nun zwar die Mahnungen z.B. ausdrucken aber nicht versenden, so würde der Kunde schlimmstenfalls in die höchste Mahnstufe rutschen, er hat aber die Information noch gar nicht erhalten. Das heißt es könnte so auch passieren, dass der Kunde immer weiter in der Mahnstufe nach oben rutscht. Dann kommt ein z.B. Supportanruf dieses Kunden, die Buchhaltung ist gerade nicht erreichbar, es ist ja eh alles im **Kieselstein ERP** sichtbar. Der Mitarbeiter sieht, der Kunde ist in Mahnstufe 3, keine Unterstützung, ganz schlechter Zahler.<br>Nun hat aber der Kunde ev. die Rechnung gar nicht bekommen und die Mahnungen haben Sie zwar im System erfasst aber nicht versandt. Die Wirkung davon wird normalerweise von kundenorientierten Serviceunternehmen nicht gewünscht. Daher eben bitte die Mahnungen **vorher** richtig aufbereiten.<br>
D.h. nachdem Sie die Mahnungsliste bearbeitet haben und hier die wirklich zu mahnenden Rechnungen enthalten sind, versenden Sie idealerweise die Mahnungen direkt per EMail.

#### An welche EMail-Adressen werden die Mahnungen versandt?
Die EMail Adressen für die Mahnungen werden **ohne** Berücksichtigung der Rechnungs-EMail-Adresse des jeweiligen Kunden versandt. Das hat für Sie den Vorteil, dass der Auftraggeber davon erfährt, dass Ihre Rechnung noch nicht bezahlt ist.<br>
In manchen Unternehmen hat es sich auch bewährt, dass die Mahnungen an z.B. allgemeine Adressen wie Office@... oder an bestimmte Personen gesandt wird. Um dies steuern zu können können spezielle Ansprechpartnerfunktionen definiert werden, mit denen dies spezifisch gesteuert werden kann. [Siehe dazu]( {{<relref "/docs/stammdaten/partner/#%C3%BCbersteuerung-von-email-adressen--faxnummer"  >}} )