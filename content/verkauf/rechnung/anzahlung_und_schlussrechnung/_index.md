---
categories: ["Rechnung"]
tags: ["Anzahlungs- & Schlussrechnung"]
title: "Rechnung"
linkTitle: "Anzahlungs- & Schlussrechnung"
weight: 20
date: 2023-01-31
description: >
  Anzahlungs- und Schlussrechnung
---
Anzahlungs- und Schlussrechnung
===============================

Unterscheidung zwischen Anzahlungsrechnungen, Teilrechnungen, Schlussrechnungen.
Im Gegensatz zur Anzahlungs- und Schlussrechnung ist die Teilrechnung eine fortschreitende Verrechnungsweise.

Anzahlungs- und Schlussrechnung wird von **Kieselstein ERP** unterstützt.<br>
Der wesentliche Unterschied ist:<br>
Eine Anzahlungsrechung wird **<u>vor</u>** Fertigstellung einer vereinbarten Leistung ausgestellt. Die Leistung wurde vom Empfänger noch nicht abgenommen!<br>
Bei der Teilrechnung wurde eine genau abgegrenzte Teilleistung vom Empfänger bereits abgenommen. Daher werden hier auch alle in dem Projekt bisher erbrachten Teilleistungen mit angeführt, da diese alle bereits abgenommen sind.<br>

### Proforma Rechnung, Handelsrechnung
In diesem Zusammenhang treffen wir auch immer wieder auf die Begriffe Profromarechnung und Handelsrechnung.<br>
Diese scheinen sich aus dem englischen Sprachraum mit einer ungenauen Übersetzung bei uns verbreitet zu haben.<br>
Bitte beachten Sie, dass eine richtige Profroma Rechnung ein Zolldeklaration ist. Siehe dazu [Proformarechnung](#proforma-rechnung-handelsrechnung)<br>
Eine Handelsrechnung ist jegliche Form einer gültigen Rechnung. In der Kombination der falschen Begriffe macht dann die Handelsrechnung wiederum Sinn. In der für die im deutschen Sprachraum üblichen Buchhaltungsregeln kann dies wie folgt übertragen werden:<br>
Profroma Rechnung (Im Sinne einer Vorauskasse) ist als Anzahlungsrechnung abzubilden.<br>
Handelsrechnung, welche dann, nach der Anzahlungs/Vorauskasse/Proforma-Rechnung, bei der tatsächlichen Lieferung mit dazu gegeben wird und die Leistungserbringung darstellt, ist als Schlussrechnung abzubilden.

## Anzahlungs- und Schlussrechnung
Die Anzahlungs- und Schlussrechnungen dienen in der Regel einer (teilweisen) Vorfinanzierung eines (Entwicklungs-)Projektes um bei den beteiligten Partnern, Auftraggeber und Auftragnehmer, eine gewisse Sicherheit zu erreichen. Wenn der Auftraggeber schon einen Teil anbezahlt, dann meint er es wirklich ernst. Die Anzahlungs- und Schlussrechnung ist im Projektgeschäft üblich und stellt eine gänzlich andere Vorgehensweise als die Teilrechnung, siehe unten, dar. Üblich sind z.B. 1/3, 1/3, 1/3\. D.h. 1/3 Zahlung bei Auftragserteilung, 1/3 Zahlung bei Lieferung und der Rest nach Abnahme des Projektes, jedoch spätestens 6Wochen nach Lieferung. Diese Punkte sind natürlich immer Verhandlungssache.

Gesetzlich ist es so, dass Anzahlungsrechnungen getrennt zu behandeln sind, da ja das Projekt noch nicht abgerechnet ist. Das bedeutet:
- Anzahlungsrechnungen müssen zum Zeitpunkt der Zahlung in die UVA aufgenommen werden.
- Wird die Schlussrechnung erstellt, so verrechnet diese das gesamte Projekt. Die darin / darauf geleisteten Anzahlungen sind inkl. MwSt. Ausweis vom Endbetrag der Schlussrechnung abzuziehen. <br>
Die Anzahlungsrechnungen sind daraufhin aus der FiBu zu stornieren und nur die Schlussrechnung in die FiBu zu übernehmen. Da die Zahlungen auf die Anzahlungsrechnungen ja beim Debitor(Kunden) bleiben, ergibt sich somit die offene Forderung aus der Schlussrechnung.

**Wichtige Info:** Rechtlich gesehen ist eine Anzahlungszahlung des Kunden nur ein "leihen von Geld". D.h. dieses Geld gehört nicht Ihnen, sondern wird nur vom Kunden zur Verfügung gestellt. Erst bei der Schlussrechnung wird die Leistung erbracht und damit haben Sie erst wirklichen Anspruch auf die Bezahlung Ihrer Leistung. Dies ist der große Unterschied zur Teilrechnung, wo bei jeder Teilrechnung(-slegung) dieser Teil als erbracht / geleistet angesehen wird.<br>
Insofern ist eine Teilrechnung in der Regel nichts anderes als eine teilweise Verrechnung eines Auftrages, durch mehrere Lieferscheine bzw. Rechnungen.

Um eine eindeutige Zuordnung zu einem Projekt zu erreichen, ist es erforderlich, dass sowohl Anzahlungs- als auch Schlussrechnungen einem Auftrag zugeordnet werden.

Ein Thema ist auch immer wieder die Zahlung auf eine Anzahlungsrechnung mit / ohne Skonto. Hier ist es logisch so, dass grundsätzlich das Zahlungsziel der Schlussrechnung gilt. Viele Kunden bestehen aber darauf, dass auch auf Anzahlungsrechnungen Skonto gewährt wird. D.h. bei der Berücksichtigung der Anzahlungsrechnung in der Schlussrechnung muss dann der bezahlte Betrag anstelle des Rechnungswertes berücksichtigt werden, da ja sonst das Skonto doppelt berücksichtigt würde.

So sollte die Endsumme einer Schlussrechnung wie folgt aussehen:
| Position | bezahlt | enth. MwSt | Netto |
| --- |  --: |  --: |  --: |
| Nettosumme |  |  | 5.000,- |
| zzgl. Mwst 20% |  |  | 1.000,- |
| Rechnungsendbetrag |  |  | 6.000,- |
| Anzahlung aus RE 06/0123456 3.000,-<br>enthaltene MwSt 500,- bezahlt | 2.450,- | 490, | 2.940,- |
| Anzahlung aus RE 06/0123457 1.500,-<br>enthaltene MwSt 250,- bezahlt | 1.225 | 245,- | 1.310,- |
| Offener Zahlbetrag |  |  | 1.310,- |

Will nun der Rechnungsempfänger die Schlussrechnung mit 2% Skonto bezahlen, so muss er von den 6.000,- 2% abziehen. Das ist dann der Basis-Zahlbetrag. Davon sind dann die bereits bezahlten 4.690,- abzuziehen. Was einen tatsächlichen Zahlbetrag von 5.880 - 4.690 = 1.190,- ergibt. Ob der Rechnungsleger (der Auftragnehmer) dies akzeptiert und nicht anstelle dessen nur den Skontobetrag vom Zahlbetrag akzeptiert ist Verhandlungssache.

**Hinweis:** In der Umsatzübersicht werden die Anzahlungen extra ausgewiesen. Es sind nur die Schlussrechnungen und die freien Rechnungen in der Umsatzgesamtsumme enthalten.

Bei offenen Ausgangsrechnungen wird wie folgt vorgegangen:

Die freien und die Anzahlungsrechnungen werden wie gewöhnliche Rechnungen behandelt. Bei den Schlussrechnungen wird der Wert der enthaltenen Anzahlungsrechnungen abgezogen. D.h. es bleibt als offener Betrag nur mehr der Rest-Wert der Schlussrechnung über. Vom Zeitraum her sind die Beträge in den Monaten der Anzahlungsrechnungen eingetragen.

## Anlegen einer Anzahlungsrechnung
Um eine Anzahlungsrechnung zu erstellen, muss bei der Erstellung des Rechnungskopfes die Rechnungsart Anzahlungsrechnung ausgewählt werden. Dies bedingt, dass ein Kundenauftrag, auf den sich diese Anzahlungsrechnung bezieht, angegeben werden muss.
Der Inhalt der Anzahlungsrechnung ist üblicherweise eine Handeingabe und eventuell ein erklärender Text dazu.
Auf keinen Fall sind in einer Anzahlungsrechnung Positionen des Projektes / des Auftrags enthalten. Diese werden alle in der Schlussrechnung aufgeführt.
Da, siehe oben, die Anzahlungsrechnung sehr oft auch als Vorauszahlungsrechnung zu verwendet wird, steht auch die Übernahme der Anzahlungspositionen in den Druck der Anzahlungsrechnungen zur Verfügung.<br>
D.h. nachdem die Rechnung als Anzahlungsrechnung definiert ist, wechseln Sie auf den Reiter ![](Anzahlungspositionen.gif) Anzahlungspositionen. Hier definieren Sie welcher Prozentsatz oder auch Betrag der jeweiligen Position in die Anzahlungsrechnung übernommen werden sollte.
Damit ergibt sich in Summe der entsprechende Wert der Anzahlungsrechnung, welcher durch eine Handeingabe mit der Bezeichnung Anzahlung und der netto Summe der angeführten Anzahlungspositionen automatisch befüllt wird. Diese Position wird automatisch angelegt. Sollten Sie auf der Rechnung einen anderen Text wünschen, so ändern Sie im Reiter Positionen einfach den Text der Handeingabe, belassen Sie diese aber als erste Position.<br>
Die Angabe der Anzahlungspositionen bewirkt, dass diese Liste der teil-verrechneten Auftragspositionen nur zur Information auf der Rechnung, in der Regel vor der eigentlichen Rechnungsposition angeführt wird und dient lediglich der Information an Ihren Kunden.

#### Wie wird eine Anzahlungsrechnung erstellt?
Anzahlungsrechnungen beinhalten immer nur einen kurzen Text mit einem Projektbezug und üblicherweise **eine** Handeingabe mit dem Anzahlungsbetrag. Erst in der Schlussrechnung werden alle Positionen des Projektes angeführt und dann, automatisch beim Ausdruck der Rechnung, werden die Anzahlungsrechnungen des Auftrags mit angeführt, um so zum entsprechenden offenen Betrag zu kommen.

Beispiel:<br>
1\. Anzahlung zu unserem Projekt,     Menge 1 Stk oder x Betrag 10.000,- 

**Hinweis:**

Oft wird an uns die Frage herangetragen, wie kann ich denn einen Prozentsatz, z.B. 30%, eines Projektes in der Anzahlungsrechnung angeben.

Das Einfachste ist hier, wenn Sie die Handeingabe für den Anzahlungsbetrag so eingeben, dass in der Menge der Prozentsatz (1,00 = 100%) und im Einzelpreis der volle Wert des Projektes angegeben wird.

![](Anzahlungs_Erfassung.gif)

So ist der verrechnete Anteil für Sie und für Ihren Kunden nachvollziehbar.

## Teilrechnung
<a name="Teilrechnung"></a>
Die Teilrechnung ist im Gegensatz zur Anzahlungsrechnung manchmal eine fortschreitende Verrechnungsweise. Diese fortschreitende Verrechnungsweise wird gerne im Baugewerbe verwendet. D.h. wird ein Gewerk teilweise fertig gestellt, z.B. beim Bau eines Hauses der Keller, so ist dieser Teil zu verrechnen. Wird nun das Erdgeschoss fertig gestellt so erfolgt eine weitere Teilrechnung. Diese beinhaltet von den Positionen her sowohl die Positionen der ersten Teilrechnung als auch die neu hinzugekommenen Positionen. Die Teilrechnungen sollten immer die selbe Basisrechnungsnummer haben und nur durch eine Subnummer unterschieden werden. Es sollten alle vergangenen Teilrechnungen eingesehen werden können, es kann aber immer nur die aktuelle Teilrechnung verändert werden. Auf der Teilrechnung ist immer der bereits bezahlte Betrag anzuführen, sodass der noch offene Zahlbetrag ersichtlich ist. Die letzte Teilrechnung ist zugleich auch die Schlussrechnung.<br>
Teilrechnungen werden derzeit von **Kieselstein ERP** nicht unterstützt. In anderen Worte, der Gewerksfortschritt wird durch die Teilrechnung abgebildet.

### Anlegen und Abrechnen einer Schlussrechnung
Um eine Schlussrechnung zu erstellen, muss bei der Erstellung des Rechnungskopfes die Rechnungsart Schlussrechnung ausgewählt werden. Geben Sie hier den Kundenauftrag an, welcher Endabgerechnet werden soll. Sie sehen in der Auswahl nur Aufträge, welche eine Anzahlungsrechnung aber keine Schlussrechnung beinhalten.

In der Schlussrechnung werden üblicherweise alle Positionen des Projektes, alle Lieferscheine, Auftragspositionen usw. angeführt, welche mit dem Projekt verrechnet werden. Durch die Kennzeichnung als Schlussrechnung erkennt **Kieselstein ERP**, dass auch die über den Auftrag zugeordneten Anzahlungsrechnungen mit berücksichtigt werden müssen und führt diese nach dem Rechnungsendbetrag mit am Ausdruck an.

<a name="Schlussrechnung bei offener Anzahlungsrechnung"></a>

#### Kann eine Schlussrechnung auch für offene Anzahlungsrechnungen erstellt werden ?

§ 11 Abs. 1 UStG besagt zur Schlussrechnung Folgendes:
"Wird eine Endrechnung erteilt, so sind in ihr die vor Ausführung der Lieferung oder sonstigen Leistung vereinnahmten Teilentgelte und die auf sie entfallenden Steuerbeträge abzusetzen, wenn über die Teilentgelte Rechnungen ... ausgestellt wurden."<br>
Leider verstehen viele Anwender und Kunden nicht, dass alleine aus diesem Titel auf eine Anzahlungsrechnung kein Skonto abgezogen werden dürfte (und ziehen sich dieses trotzdem ab). Richtig wäre, da ja die Anzahlung nur eine andere Form der Geldbeschaffung ist, dass erst bei der Schlussrechnung ein entsprechendes Skonto vom Rechnungsbetrag abgezogen wird.

Schlussrechnungen können in **Kieselstein ERP** angelegt werden, auch wenn es Anzahlungen gibt, die noch nicht bezahlt wurden. Diese Anzahlungen können dann aber nicht mehr per Hand erledigt werden, sprich: Der Haken in den Zahlungen ist deaktiviert und wird automatisch aktiviert, wenn der volle Rechnungsbetrag bezahlt ist. Ist die Schlussrechnung noch nicht in der Finanzbuchhaltung (Fibu) übernommen, können Sie wie üblich den Skonto buchen, also den Haken auf erledigt setzen.

Des weiteren ist das Verhalten in den Ausgangsrechnungen nun wie folgt:
Ist eine Schlussrechnung vorhanden und bereits in der Fibu (also gedruckt), dürfen
- keine weiteren Anzahlungsrechnungen angelegt werden
- stornierte Anzahlungsrechnungen dürfen nicht wieder auf angelegt geändert werden
- Zahlungen auf Anzahlungen müssen vollkommen bezahlt werden, damit diese als bezahlt gekennzeichnet werden

Ist eine Schlussrechnung vorhanden, aber noch nicht in der Fibu (also 'angelegt' in der RE) darf man
- weitere Anzahlungen erstellen
- Anzahlungen mit Skonto bezahlen
- Anzahlungen beliebig ändern (stornieren, storno aufheben, Positionen hinzufügen, solange es keine Schlussrechnung gibt)

Die Schlussrechnung kann erst gedruckt werden, wenn ALLE Anzahlungen entweder storniert oder in der Fibu (=gedruckt bei RE) sind. Sonst würden diese angelegten Anzahlungen nicht in die Schlussrechnung miteinfließen. **Wichtig:** Wenn die Integrierte Finanzbuchhaltung genutzt wird, so sind auf Anzahlungsrechnungen keine Gutschriften möglich. [Siehe]( {{<relref "/verkauf/rechnung/gutschrift/#gutschrift-einer-anzahlungsrechnung" >}} )

#### Wie werden Anzahlungsrechnungen einer Schlussrechnung zugeordnet
Wird eine Schlussrechnung erstellt, so werden dieser automatisch alle Anzahlungsrechnungen die sich auf den Auftrag der Schlussrechnung beziehen zugeordnet. D.h. es reicht die Angabe des Auftrags um damit auch die Anzahlungsrechnungen auf der Schlussrechnung anzuführen.

#### Welche Alternativen gibt es, die Anzahlungs-, Teilrechnungs-, Schlussrechnungsthematik selbst zu steuern?
Die einfachste Lösung ist, wenn Sie einen sogenannten Anzahlungsartikel definieren, der nicht Lagerbewirtschaftet ist.<br>
Hinterlegen Sie für den Artikel eine eigene Artikelgruppe, z.B. Anzahlung, welche wiederum auf das Konto der erhaltenen Anzahlungen bucht.<br>
Bei den Anzahlungsrechnungen schreiben Sie den Wert des Anzahlungsbetrages in den Verkaufswert der Artikelposition.<br>
Bei der dann auszustellenden Schlussrechnung ziehen Sie manuell die Artikelpositionen durch Angabe eines negativen Verkaufspreises wieder ab.<br>
Der Ausweis des Steuersatzes ist durch den Ausweis der kleinen 20% am rechten Rand der Rechnung gegeben.<br>
Um auch den Andruck des Rechnungstextes mit Anzahlungs- und Schlussrechnung zu steuern, kann z.B. der Eintrag Betrifft durch eine spezielle Formularanpassung verwendet werden. Wenden Sie sich dazu vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

#### Warum kann eine Anzahlungsrechnung nicht storniert werden?
Wenn eine Schlussrechnung vorhanden ist, kann die Anzahlungsrechnung nicht mehr storniert werden.

#### Kann eine Gutschrift zu einer Anzahlungsrechnung erstellt werden?
Ja. Auch in der Auftragsnachkalkulation wird die Gutschrift auf eine Anzahlungsrechnung dargestellt. Der Saldo der Schlussrechnung berücksichtigt Gutschriften auf Anzahlungsrechnungen
**Achtung:** Bitte beachten Sie jedoch, dass eine Gutschrift auf eine Anzahlungsrechnung bei integrierter Finanzbuchhaltung nicht möglich ist! Gegebenenfalls stornieren Sie bitte die Anzahlungsrechnung und senden dem Kunden den Beleg der Stornorechnung.

#### Warum kann eine Anzahlungsrechnung nicht erledigt werden? Warum kann ein etwaiger Skonto nicht verbucht werden?
Ein Ändern / Nachbuchen in einer Anzahlungsrechnung ist nicht mehr möglich, sobald eine Schlussrechnung zur Anzahlungsrechnung erstellt wurde.<br>
Um die Anzahlungsrechnung noch bearbeiten zu können gehen Sie bitte wie folgt vor:<br>
Löschen Sie die Zahlung aus der Schlussrechnung, danach ändern Sie die Art der Rechnung von Schlussrechnung auf Rechnung.<br>
Nun können Sie die Anzahlungsrechnung bearbeiten. Danach stellen Sie die Rechnungsart bei der ursprünglichen Schlussrechnung wieder auf Schlussrechnung, aktivieren die Schlussrechnung und denken daran auch den eventuellen Zahlungsbetrag wieder einzutragen.

{{% alert title="Vorsteuer Behandlung der Anzahlungs-Eingangsrechnung" color="primary" %}}
Bitte beachte, dass die in der (Inländischen) Anzahlungs-Eingangsrechnung enthaltene Vorsteuer erst mit der Zahlung in der UVA berücksichtigt werden darf.<br>
Aufgrund der aktuellen Buchungslogik kann bei der Berechnung der Vorsteuersumme nicht zwischen Anzahlungs-Eingangsrechnungen und normalen Eingangsrechnungen unterschieden werden.<br>
**Daher muss das Eingangsrechnungsdatum im gleichen Monat sein wie die vollständige Bezahlung der Anzahlungseingangsrechnung**.<br>
Anderenfalls würdest du dir unerlaubter Weise zu früh die Vorsteuer vom Finanzamt zurückholen, was richtig teuer werden kann!
{{% /alert %}}

**Info:** Dieses (obiges) Thema betrifft nur die Anzahlungs-Eingangsrechnung.<br>
Bei der Ausgangsrechnung wird dies aufgrund der unterschiedlichen (Erlös-) Konten (Siehe Finanzamt, Buchungsparameter) richtig in die UVA übernommen und somit die Umsatzsteuer für die Anzahlungsrechnung erst bei Bezahlung in der UVA angeführt.

#### Wie wird die Zahlung einer Schlussrechnung verbucht?
Wird für einen Auftrag (oder auch eine Bestellung) eine Schlussrechnung erstellt, so ist damit implizit verbunden, dass nur der sich aus dem Schlussrechnungswert abzüglich bereits gelegter Anzahlungsrechnungen ergebende Betrag offen ist. Das bedeutet:
1. es sind natürlich die Anzahlungsrechnungen zu bezahlen (diese werden auch als entsprechend offen mit angeführt)
2. es ist bei der Schlussrechnung immer nur der Schlussrechnungswert abzüglich der Summe der Anzahlungsrechnungen offen und wird auch nur entsprechend als offen vorgeschlagen.
3. ergibt es sich, dass der offene Betrag der Schlussrechnung durch die Anzahlungsrechnungen bereits vollständig ausgeglichen ist, so muss
    - bei **Kieselstein ERP** Installationen ohne integrierter Finanzbuchhaltung die Schlussrechnung manuell auf erledigt gesetzt werden (Menü, Bearbeiten, manuell erledigen)
    - bei **Kieselstein ERP** Installationen mit integrierter Finanzbuchhaltung trotzdem eine Zahlungsbuchung mit 0,00 erfolgen. Wir raten dafür das Durchläuferbankkonto zu verwenden.

#### Der Rechnungsempfänger ist in einem anderen Land als dort wo die Ware hingeht?
Es kommt auch immer wieder vor, dass Rechnungsempfänger und Lieferung in unterschiedliche Länder, wenn Drittland und z.B. EU-Ausland beteiligt sind also auch noch unterschiedliche Länderarten, gelegt werden müssen. Wenn es größere Projekte sind, wird dies mit Anzahlungs- und Schlussrechnung abgewickelt.<br>
Da, wenn der Rechnungsempfänger im Ausland ist, bei Lieferung das Empfängerland die Art der umsatzsteuerlichen Behandlung regelt, muss die Anzahlungsrechnung so erzeugt werden, dass, obwohl die Anzahlungsrechnung in ein anderes Land geht, trotzdem die Versteuerung für das Lieferland erfolgt. Um dies zu erreichen stellen Sie bitte:
- die Lieferadresse so ein, dass auch für den Lieferempfänger klar ersichtlich ist, dass dies eine Lieferung für Ihren eigentlichen Kunden ist.
- Beide Adressen, sowohl Lieferadresse als auch Rechnungsadresse müssen auf das gleiche Debitorenkonto zeigen. WICHTIG: Damit wird unter Umständen die Steuerkategorie entsprechend verändert. Bitte beachten Sie in jedem Falle die gesetzlichen Vorschriften. So ist das österreichische Finanzamt (Q1/2016) der Ansicht, dass bei einem Schweizer Rechnungsempfänger der seine Lieferung nach Italien dirigiert, dieser, neben der UID Nummer des italienischen Lieferempfängers, auch eine EU-UID Nummer benötigt.

Ein Beispiel:<br>
Ein Kunde aus der Schweiz = Drittland bestellt Ware. Diese sollte von Österreich nach Italien versandt werden. Am italienischen Standort ist eine UID Nummer gegeben. Die Rechnungslegung muss jedoch an die Schweiz erfolgen. Der Schweizer Kunde hat keine UID Nummer. Es sollten vor der Lieferung Anzahlungsrechnungen gelegt werden.
Damit dies als innergemeinschaftliche Lieferung betrachtet werden darf, sollte der Schweizer Kunde ein EU-UID Nummer haben (Achtung: Seit 2015 bezeichnet das Schweizer Finanzamt die Schweizer Unternehmens Identifikationsnummern auch als UID Nummer. Dies hat mit der UID-Nummer der europäischen Union nichts zu tun).
Wichtig ist bei der Anlage der Kunden, dass beide auf die gleiche Debitorennummer zeigen. So hat sich bewährt, dass die Lieferadresse wie folgt ausgeführt ist:
Rechnungsempfänger c/o
Lieferempfänger
im Lieferland
Es sollte auch zuerst die Debitorennummer für den Lieferempfänger eingegeben werden. Damit wird automatisch das Debitorenkonto mit den Einstellungen der Lieferadresse definiert und erst danach wird die Rechnungsadresse angelegt. Hier wird nun, bevor die Rechnung geschrieben wird, die Debitorennummer der Lieferadresse hinterlegt.
Somit wird von **Kieselstein ERP** dies auf den Debitor der Lieferadresse und nach dessen Regeln verbucht. Somit sind bei Anzahlungs- und Schlussrechnung die gleichen Debitoren beteiligt und zum Schluss die Salden ausgeglichen. Zusätzlich werden für die Konten der Anzahlungsrechnungsverbuchung (Erhaltene Anzahlungen und erhaltene Anzahlungen bezahlt) die Länderartübersetzungen anhand der Steuerkategorie des Debitorenkontos verwendet.
Umsatzsteuerlich wird der Rechnungsempfänger dann gleich wie die Lieferadresse behandelt.

#### Vorausrechnung?
Ein manchmal verwendeter Begriff anstelle des richtigen Begriffes Anzahlungsrechnung. Siehe bitte Anzahlungsrechnung.<br>
Auslöser ist immer, dass ein Kunde bevor er die Ware erhält, den vollen oder einen Teil des Auftragswertes vorauszahlen muss.<br>
Die richtige Abwicklung ist immer, zuerst den Auftrag anzulegen, dann auf diesen Auftrag eine Anzahlungsrechnung mit dem vereinbarten Vorausbetrag als Handeingabe einzubuchen. Hat der Kunde die Anzahlungsrechnung bezahlt kann produziert und dann geliefert werden. Die Lieferung erfolgt mittels Lieferschein und die Verrechnung des / der Lieferscheine(s) erfolgt mit einer Schlussrechnung. Damit sind die Anzahlungsrechnungen abgeschlossen und der verbleibende Saldo, welcher 0 oder auch negativ sein kann, aber hoffentlich positiv ist, ist mit der Schlussrechnung noch zu bezahlen.<br>

#### Mehrere Schlussrechnungen pro Auftrag?
Es sollte dem Kunden eine Auftragsbestätigung gesandt werden, damit das Gesamtvolumen des Auftrags klar ist, aber es werden dann in mehreren (Teil-)Lieferungen jeweils Anzahlungsrechnungen gelegt, wobei die Lieferungen dann jeweils eigenständig Schluss-abgerechnet werden. Wie kann das dargestellt werden?<br>
Nutzen Sie dafür die Funktionalität Rahmenauftrag mit Abrufaufträgen. D.h.
- dem Kunden wird als Bestätigung des Gesamtvolumens eine [Rahmenauftragsbestätigung]( {{<relref "/verkauf/auftrag/#rahmenauftrag" >}} ) gesandt.
- für den ersten Lieferauftrag wird eine Abruf-Auftragsbestätigung aus dem Rahmenauftrag erstellt
- auf diese Abruf-Auftragsbestätigung wird eine Anzahlungsrechnung in der vereinbarten Höhe erstellt
- Zahlungen usw. wie mit Kunde vereinbart
- dann wird die Gesamt-Lieferung durchgeführt
- nun kann für diese Gesamtlieferung die Schlussrechnung zu dem Abruf-Auftrag erstellt werden
- für die nächste Teillieferung wird ein weiterer Abrufauftrag angelegt und in gleicher Weise vorgegangen.

#### Welches Zahlungsziel wird für Anzahlungsrechnungen verwendet?
Anzahlungsrechnungen werden als Rechnung mit Auftragsbezug erstellt. Somit wird in der Regel das Zahlungsziel aus dem Auftrag verwendet.<br>
Um nun für die Anzahlungsrechnungen abweichende Zahlungsziele, z.B. sofort, automatisch eintragen zu können, kann im System, Mandant, Vorbelegungen für die Anzahlungen ein eigenes Zahlungsziel definiert werden.

#### Wie ist bei einer Rückerstattung von bezahlten Anzahlungsrechnungen vorzugehen?
Sollte der, hoffentlich für Sie nie zutreffende, Fall eintreten, dass ein Auftrag, für den Sie Anzahlungsrechnungen gelegt haben, nicht geleistet werden kann, so tritt hier der Fall ein, dass Sie dem Kunden das auf die Anzahlungsrechnung(en) bezahlte Geld wieder zurückbezahlen müssen.
In diesem Falle gehen Sie bitte wie folgt vor:
1. Stornieren Sie eventuell noch offene Anzahlungsrechnungen und senden Sie die Stornobelege dem Kunden
2. Summieren Sie den Nettobetrag der bereits bezahlten Anzahlungsrechnungen. Ev. nur teilbezahlte Anzahlungsrechnungen, ändern Sie so ab, dass der Wert der Anzahlungsrechnung dem Zahlbetrag entspricht.
3. erstellen Sie nun eine Schlussrechnung mit dem Wert der bereits bezahlten Anzahlungsrechnungen und aktivieren Sie diese. Damit wandert der Anzahlungsbetrag in den Erlös.
Der offene Zahlbetrag der Schlussrechnung muss 0,00 sein.
4. erstellen Sie nun eine Gutschrift für die Schlussrechnung mit dem Wert (nicht dem offenen Zahlbetrag) der Schlussrechnung und dem gleichen Artikel wie in der Schlussrechnung (Gegebenenfalls kopieren Sie den Inhalt der Schlussrechnung über die **Kieselstein ERP** Zwischenablage in die Gutschrift).
5.) nun bezahlen Sie die Schlussrechnung mit dem Zahlbetrag von 0,0
6.) Überweisen Sie Ihrem Kunden den zurückzuzahlenden Betrag und buchen diese Zahlung in die Gutschrift.<br>
Bitte achten Sie darauf, dass das Datum der Belege / Zahlungsbuchungen der Punkte 3.) bis 5.) zum gleichen Belegdatum bzw. Zahlungsdatum erfolgen.

#### Wie werden allgemeine Rabatte auf Anzahlungsrechnungen dargestellt?
Wenn auf der Anzahlungsrechnung in den Konditionen ein allgemeiner Rabatt eingetragen ist, so wird dies in einer eigenen Zusammenfassung am Ende der Positionen der Anzahlungsrechnungsinformation dargestellt.
![](Anzahlungsrechnung_mit_Rabatt.gif)<br>
Somit ist auch der aus dem Anzahlungssatz ermittelte Wert und der sich daraus ergebende Nettobetrag ersichtlich.<br>
Die tatsächliche Abrechnung inkl. Mehrwertsteuer wird dann in der eigentlichen Rechnung dargestellt.

## Anzahlungsrechnungen für das folgende Jahr
Hier geht es insbesondere um die Erbringung von Montageleistungen für das Jahr 2024 und die Forderung an den Kunden noch in 2023 eine Anzahlung zu leisten.
Für die Schweiz ergibt sich hier als Besonderheit, dass am 1.1.2024 ein MwSt Satz Änderung von 7,7 auf 8,2 % Beschlossen wurde. Da nun das Leistungserbringungsdatum ja erst in 2024 ist, muss auch die Umsatzsteuer von 2024 angewandt werden.
D.h. man muss dafür einen eigenen MwSt Satz Bezeichnung z.B. Anzahlungsrechnung für Leistungserbringung nächstes Jahr einrichten und hier bereits ab 2023 die geänderte MwSt definieren können.
Üblicherweise ergibt sich das Leistungserbringungsdatum aus dem Lieferscheindatum. Einzig bei der Anzahlungsrechnung müsste man dies z.B. mit dem Finaltermin des Auftrags kombinieren und in Abhängigkeit davon den MwSt Satz entsprechend anwenden. Dies wäre ein Wunsch an die zukünftige Entwicklung des KES.

Beispiel für Lieferantenanzahlungsrechnung siehe auch: https://mein-lernen.at/buchungssaetze-bezahlung/verbuchung-anzahlungen-an-lieferanten/
