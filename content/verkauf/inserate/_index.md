---
title: "Inserate"
linkTitle: "Inserate"
weight: 45
description: >
  Inseratenverwaltung als Klammer vom Angebot über die Bestellung bis zur Verrechnung
---
Inseratenverwaltung
===================

Die **Kieselstein ERP** Inseratenverwaltung dient der Gesamtverwaltung der für die Vermittlung von Werbeeinschaltungen usw. erforderlichen Belege. Es ist dies eine Klammer in Form eines Inseratenauftrags über die Module Kunde, Lieferant, Bestellung, Eingangsrechnung, Rechnung.

Zusätzlich wird der in diesen Projekten übliche Workflow integriert unterstützt.

![](Inserat1.jpg)

In der Auswahlliste erhalten Sie bereits einen großen Überblick über den Status Ihrer Inseratenaufträge.

Für die Anzeige der Namen der Kunden bzw. Lieferanten wird das Stichwort verwendet, womit einerseits Platz gespart wird und andererseits die bei Ihnen übliche Zuordnung zum Namen Ihres Partners gewählt werden kann. Sobald Sie den Mauszeiger auf ein Inserat bewegen erhalten Sie weitere Informationen zu diesem Inserat.

Die angezeigten Felder bedeuten:

| Name | Beschreibung |
| --- |  --- |
| Inserat | Inseratennummer, wird laufend im jeweiligen Geschäftsjahr vergeben |
| Kunde | Die Kurzbezeichnung des Kunden |
| Datum | Das Datum des Inserates |
| Stichwort | Das in den Kopfdaten erfasst Stichwort zu diesem Inserat |
| Lieferant | Die Kurzbezeichnung des Lieferanten |
| Termin | Der Erscheinungstermin des Inserates, gegebenenfalls der Start-Termin wenn z.B. ein Radiospot eine gewisse Zeit laufen sollte |
| Art | Die Art des Inserates.Bitte beachten Sie, dass die Inseratenart als Artikel ausgeführt ist um für Sie die größtmögliche Flexibilität zu erreichen. |
| Bestellnummer | Die Nummer der Bestellung mit der dieses Inserat an den Lieferanten gesandt wurde. |
| Erschienen | Dieses Inserat ist bereits im gewünschten Medium erschienen |
| Menge |  |
| Preis | Der Inseraten-Basispreis ohne Rabatte und ohne Zusatzartikel |
| Status |  |
| Status der Rechnung |  |
| Re Nr | Die Nummer der Rechnung des ersten Kunden.Für Details siehe bitte Reiter Kunden |
| ER-Nr | Die Eingangsrechnungsnummer welche mit dem Inserat verknüpft ist. |

Der Ablauf eines Inseratenauftrags ist in der Regel wie folgt:

**1.) Anlegen des Inserates:**

Anlegen des Inserates mit Kunde, Lieferant, Angabe des Preises mit eventuellen Rabatten und Nachlässen.

Dann die Angabe des Mediums usw.

![](Inserat2.jpg)

Der Vertreter wird automatisch aus dem Kunden (Provisionsempfänger) übernommen.

Mit dem Artikel wählen Sie den Basisartikel, also die grundsätzliche Dienstleistung für den Inseratenauftrag aus.

Definieren Sie nun noch den Erscheinungstermin und gegebenenfalls auch den Ende-Termin (Bis) dafür.

Unter Anhang definieren Sie die entsprechenden Texte, welche an den Lieferanten übermittelt werden sollen.

Die Anhänge sind zweigeteilt und steuerbar, damit zu Ihren Partnern auch die richtigen Texte gelangen. D.h. durch anhaken bzw. nicht anhaken der Felder Bestellung bzw. Rechnung werden diese Texte in den jeweiligen Formularen mitgedruckt.

**Der Reiter Kunden**

Es kommt immer wieder vor, dass eine Gemeinschaft von Firmen sich zu einem Inserat entschließt. D.h. es gibt zwar nur einen Inseratenauftrag, aber bezahlt wird dieser von verschiedenen Unternehmen.

Im Reiter Kunden können Sie neben dem Hauptkunden weitere Rechnungsempfänger für den Inseratenauftrag definieren. Die Aufteilung der Inseratenkosten erfolgt anteilig, also wenn drei Kunden beteiligt sind, erhält jeder eine Rechnung über 1/3 des Wertes. In der Inseratenauswahlliste wird nur der Hauptkunde angezeigt.

**Der Reiter Eingangsrechnungen**

Hier sehen Sie die dem Inseratenauftrag zugewiesenen Eingangsrechnungen.

**Der Reiter Artikel**

Hier können weitere Artikel die für dieses Inserat zutreffend sind mit Menge und Preis definiert werden.

Beachten Sie bitte, dass in den Feldern Wert ![](Inserat4.gif) die Preisgebarung der Artikel mit berücksichtigt ist.

Das Inserat ist damit im Status angelegt ![](Inserat_angelegt.gif).

Sobald Sie einen zusätzlichen Artikel im Inserat erfassen, erscheint im Reiter Kopfdaten bei der Gesamtansicht ein rotes A. Das erleichtert die Erfassung und Bearbeitung der Inserate, da dadurch sofort erkannt wird, ob auch zusätzliche Artikel vorhanden sind, oder noch erfasst werden müssen.

**2.) Bestellung**

Ist nun das Inserat / der Inseratenauftrag vollständig erfasst, so gilt es diesen an den Lieferanten zu senden.

Wählen Sie aus dem Menü, Bearbeiten Bestellungen auslösen

![](Inserat5.gif)

Im nun erscheinenden Dialog wählen Sie den Kunden dessen Inserate an die Lieferanten versendet werden sollten.

Nun wählen bzw. bestätigen Sie den Lieferanten für den die Bestellung(en) für den gewählten Kunden erzeugt werden sollten.

Das Inserat ist nun im Status bestellt ![](Inserat_bestellt.gif).

Denken Sie daran, dass diese Bestellung noch an Ihren Lieferanten versandt werden muss.

Nutzen Sie dafür im Bestellmodul ![](Inserat_bestellen.jpg) die Funktion Menü, Bestellung, alle angelegten Drucken.

![](Inserat_Bestellungen_versenden.gif)

Wählen Sie hier noch das gewünschte Verhalten.

Alternativ kann auch direkt in der Auswahlliste der Inserate durch Klick auf ![](Bestellung_ausloesen.gif) *Bestellung für selektierten Kunden auslösen* der Bestelllauf für den Kunden des Inserates, auf dem der Cursor steht, erzeugt werden. Durch den Klick auf diesen Button wird eine Bestellung erzeugt und Sie werden in die Druckvorschau der Bestellung im Bestellmodul geführt. Führen Sie hier eine optische Überprüfung Ihrer Bestellung durch und senden Sie diese dann z.B. per EMail an Ihren Lieferanten.

Sollten nachträglich Preise, Mengen oder auch Artikel im Inseratenauftrag verändert werden, so werden diese Änderungen automatisch in die Bestellpositionen übernommen. Die Bestellung wird dadurch wieder in den Status angelegt zurückgesetzt, da diese geänderte Bestellung ja noch nicht erneut an den Lieferanten versandt wurde.

Durch das oben beschriebene Verhalten ergibt sich implizit, dass pro Bestellung an einen Lieferanten auch immer nur ein Kunde enthalten sein kann.

**3.) Erschienen**

Nachdem Sie das Inserat bestellt haben, sollte es auch zum gewünschten Termin erscheinen.

Das bedeutet, dass von der Grundidee her ab diesem Zeitpunkt das Inserat verrechnet werden könnte.

Darum muss das Erscheinen des Inserates erfasst werden. Gehen Sie dazu wie folgt vor.

Geben Sie im Direktfilter der Auswahlliste den Lieferanten ein für den Sie die erschienenen Inserate erfassen wollen.

Klicken Sie nun auf die Spalte Termin, sodass diese absteigend sortiert ist. Somit haben Sie die Inserate in der terminlichen Reihenfolge vorliegen.

Nun wählen Sie das erschienene Inserat aus und klicken auf ![](Inserat_erschienen_erfassen.gif) erschienen bzw. drücken Sie Strg+E.

Das Inserat ist damit im Status erschienen ![](Inserat_erschienen.gif).

Haben Sie versehentlich das falsche Inserat auf erschienen gesetzt, so klicken Sie erneut auf den Button erschienen.

**4.) Verrechenbar**

Nachdem Sie das Inserat erschienen ist gibt es zwei Möglichkeiten.

Sie warten bis Sie die Eingangsrechnung erhalten oder, da ja der komplette Auftragsumfang bereits bekannt ist, wird das Inserat sofort verrechnet.

In diesem Falle Markieren Sie das Inserat in den Kopfdaten mit Klick auf ![](Inserat_verrechenbar_setzen.gif) als verrechenbar.

Das Inserat ist damit im Status verrechenbar![](Inserat_verrechenbar.gif) .

Haben Sie versehentlich das falsche Inserat auf verrechenbar gesetzt, so klicken Sie erneut auf den Button verrechenbar, womit das Inserat wieder im Status erschienen ![](Inserat_erschienen.gif) ist.

Wer wann dieses Inserat auf Verrechenbar gesetzt hat ist in den Kopfdaten des Inserates (unten) ersichtlich.

**5.) Eingangsrechnung**

Sie werden in der Regel für Ihren Inseratenauftrag auch eine oder mehrere Eingangsrechnungen erhalten.

Diese Eingangsrechnungen beinhalten üblicherweise mehrere Ihrer Inseratenaufträge / Bestellungen.

Daher wählen Sie aus dem Menü, Bearbeiten, Eingangsrechnung erfassen, den Lieferanten aus und klicken der Reihe nach die Inserate an, die mit dieser einen Eingangsrechnung verrechnet werden.

Damit wird automatisch eine Eingangsrechnung erzeugt. D.h. hier tragen Sie die fehlenden Daten ein. Bei entsprechend vorbesetzten Daten aus den Lieferanten sind dies nur mehr die Lieferantenrechnungsnummer und das Eingangsrechnungsdatum. Beachten Sie, dass der Betrag bereits vorbesetzt ist. Hier ist wichtig, dass der auf der Lieferantenrechnung angedruckte Betrag mit dem von **Kieselstein ERP** errechneten Betrag übereinstimmt. Gibt es hier Abweichungen, so wurden in der Erfassung Fehler gemacht, welche unbedingt korrigiert werden müssen, da sonst die Nachkalkulation der Inserate, die Abrechnung an den Kunden u.ä. nicht mehr stimmen würden.

Weiters gibt es die Spalte 'Werbeabgabe'. In der Summe der ER wird die Werbeabgabe hinzugefügt und gemäß Parameter PREISERABATTE_UI_NACHKOMMASTELLEN_EK gerundet.

Die Voraussetzung für diese Vorgehensweise ist, dass die in den Inseratenaufträgen erfassten Beträge / Preise / Mengen stimmen. Da in der Praxis z.B. die Zeilenanzahl, die ja in der Menge erfasst wird, erst bekannt ist, wenn das Inserat / die Zeitschrift gedruckt ist, würde dieser Ablauf eine doppelte Bearbeitung der Inserate bedingen. Wir haben daher eine alternative Erfassung im Modul Eingangsrechnungen geschaffen.

Gehen Sie dazu wie folgt vor:

![](ER_Inserat_Zuordnung.gif)
Erfassen Sie die Eingangsrechnung mit Kontierung, Lieferantenrechnungsnummer und Bruttobetrag. Nun wechseln Sie in den Reiter Inseratenzuordnung und wählen mit Neu der Reihe nach die Inserate aus, welche mit dieser Eingangsrechnung verrechnet wurden. Wenn ein Inserat ausgewählt ist, so können Menge und Preise sowie die Rabatte geändert werden. Diese Daten werden direkt dem Inserat zugeordnet und nur dort gespeichert. Auch hier wird die Werbeabgabe hinzugerechnet, wenn im Inserat ein Artikel der werbeabgabenpflichtig ist inkludiert ist.

**Hinweis:** Der Betrag, der hier eingegeben wird (sei es bei Lieferant oder bei Kunde) wird in das Inserat zurückgepflegt und überschreibt etwaige vorherigen Eingaben! Wenn Sie mehrere Eingangsrechnungen zu einem Inserat erfassen, zählen Sie den neuen Betrag zum Vorhandenen und schreiben das Ergebnis in das Feld.

![](zuordnung_inserate.PNG)

Im Feld offen sehen Sie, welcher Betrag noch nicht Inseraten zugeordnet ist. D.h. wenn alle Positionen der ER richtig erfasst sind, muss der offene Nettobetrag 0,00 sein.

Zugleich wird mit der Zuordnung der Eingangsrechnung auch die auslösende Bestellposition erledigt. Sind alle Bestellpositionen einer Bestellung erledigt, so wird die gesamte Bestellung als erledigt verbucht.

Wurde versehentlich ein falsches Inserat einer Eingangsrechnung zugeordnet, so muss diese "Zeile" wieder aus der Inseratenzuordnung entfernt werden.

**6.) Verrechnungsstop**

Üblicher Weise werden nach der Erfassung der Eingangsrechnungen umgehend die Rechnungen an die Kunden erzeugt. Die Praxis zeigt uns jedoch, dass es immer wieder Fälle gibt, in denen diese Verrechnung nicht sofort gemacht werden kann.

Der hier vorgesehene Ablauf ist:

a.) Drucken Sie die Liste der zu verrechnenden Inseratenaufträge aus, Menü, Journal, zu verrechnen.

Hier erhalten Sie die Liste der Inseratenaufträge, die laut Status verrechnet würden.

b.) Bei der Kontrolle der Liste werden Sie eventuell einige Inseratenaufträge entdecken die doch noch nicht verrechnet werden sollten.

c.) Wählen Sie nun den Inseratenauftrag (Eingabe der Inseratennummer im Direktfilter) und klicken Sie auf Verrechnungsstop ![](Inserat_verrechnung_stoppen.gif). Geben Sie eine entsprechende Begründung an und klicken Sie auf ok.

Das Inserat ist nun im Status Verrechnung gestoppt ![](Inserat_verrechnungsstop.gif).

Damit Sie daran denken, dass diese gestoppten Inseratenaufträge trotzdem verrechnet werden müssen, haben wir auch dies Inseratenaufträge mit der Begründung in die Liste der zu verrechnenden Aufträge aufgenommen.

Kann nun das Inserat verrechnet werden, so entfernen Sie den Stop durch erneuten Klick auf ![](Inserat_verrechnung_stoppen.gif) wieder. Die Begründung für den Verrechnungsstop bleibt in den Kopfdaten erhalten.

Daher haben wir auch die gestoppten Inseratenaufträge mit in diese Liste aufgenommen.

Eine alternative Vorgehensweise wäre, den Inseraten-Status-Filter in der Auswahlliste auf ![](Inseratenstatus_verrechenbar.gif) zu stellen, dann z.B. die Inserate nach Kunden zu sortieren und so direkt, ohne Papier, die verrechenbaren Inserate zu prüfen und hier gegebenenfalls die Verrechnung zu stoppen.

**7.) Verrechnen**

Nachdem nun alle verrechenbaren Inserate geklärt sind, wählen Sie im Menü, Bearbeiten, Rechnungen auslösen.

![](Inserat_Rechnungen_ausloesen.gif)

Wählen Sie hier noch aus, wie die Verrechnung erfolgen sollte:

![](Inserate_Rechnungserstellung.jpg)
- alle verrechenbaren Inserate
- nur Inserate eines bestimmten Kunden
- nur das gewählte / selektierte Inserat.

Nach Klick auf ok werden, ohne weitere Rückfrage alle als verrechenbar definierten Inseratenaufträge, die der obigen Auswahl entsprechen und die keinen Verrechnungsstop haben, an die Kunden verrechnet.

Nach der Verrechnung erscheint ![](Inserate_Rechnungen_angelegt.gif).

Erscheint hier die Meldung dass keine (0) Rechnungen angelegt wurden, so wurde kein verrechenbares Inserat gefunden.

**Hinweis:**

Um auch nachträgliche Rechnungsänderungen zu ermöglichen, werden Rechnungen so lange ergänzt, so lange diese im Status angelegt ![](Rechnung_angelegt.gif) sind. D.h. wird für die Verrechnung eine Rechnung gefunden, die auf den gewünschten Kunden/Rechnungsempfänger lautet und ist diese im Status angelegt, so werden weitere Positionen zu dieser Rechnung hinzugefügt. Dies wird solange gemacht, bis die Rechnung gedruckt wurde und damit im Status offen ist.

Sollten Sie einmal eine Rechnung wirklich stornieren müssen, so achten Sie bitte darauf, dass diese tatsächlich storniert sind.

Nach erfolgter Verrechnung wechseln Sie in das Modul Rechnungen ![](Inserat_verrechnen.jpg) und wählen Menü, Rechnung, Alle angelegten drucken, Alle oder nur eine spezifische Kostenstelle.

![](Inserat_alle_rechnungen_versenden.jpg)

Damit erhalten Sie in effizienter Form alle Inseratenaufträge verrechnet.

**Hinweis:**

Wenn Rechnungen zu einem anderen als dem aktuellen Datum angelegt werden sollen, so definieren Sie bitte vor dem Erzeugen der Rechnungen das gewünschte Neudatum. Siehe dazu in der Inseratenverwaltung, Extras Neudatum. Dieses Datum wird für das Anlegen neuer Rechnungen so lange verwendet, bis es entweder gelöscht wird, oder es wird das Inseratenmodul geschlossen.

**8.) Erledigt / abgeschlossen**

In der Inseratenauswahlliste sehen Sie neben dem Status des eigentlichen Inserates auch noch den Status der dazugehörenden Rechnung. Genauer ist das der Status aller Rechnungen die mit dem Inserat verbunden sind. D.h. sind alle Rechnungen des Inserates bezahlt, so wird der Bezahlt Status angezeigt. Ist nur eine einzige Rechnung nicht oder nur teilweise bezahlt, so wird nur der Status der Teilzahlung angezeigt.

#### Wie mit geänderten Bestellpreisen umgehen?
In der Abwicklungspraxis, gerade bei kleinen Inseratenaufträgen, ist es in der Regel so, dass der tatsächliche Preis, die Anzahl der Zeilen (= Menge) nicht vorher verhandelt wird, sondern diese Inserate einfach bestellt werden. Erst durch die Eingangsrechnung wird der tatsächliche Preis vom Lieferanten vorgegeben.

Durch die Erfassung der Eingangsrechnung und damit der Pflege der Eingangsrechnungspreise wird zugleich die Bestellposition aktualisiert. Da Sie ja die Eingangsrechnung bereits erhalten haben, muss diese geänderte Bestellung nicht nachträglich an den Lieferanten gesandt werden.

**Hinweis:**

Die ursprünglich versandte Bestellung ist in der Dokumentenablage des Bestellmodules ersichtlich.

#### Wie mit zu ändernden Rechnungspositionen umgehen?
Es kommt trotz der vorgelagerten Prüfung vor, dass Rechnungen nachträglich verändert werden müssen. Damit die Durchgängigkeit der Kalkulation sichergestellt ist muss dazu wie folgt vorgegangen werden.

1.  Löschen Sie die Inseratenposition aus der Rechnung (Die Rechnungsnummer wird in der Inseratenauswahlliste angezeigt)
    Beantworten Sie dazu die Abfrage 
    ![](Inserat_Verrechnung_loeschen.gif)
    entsprechend mit ja. Wird die Frage mit Nein beantwortet, so bleibt auch die Position in der Rechnung erhalten.
    **Info:** Damit wird auch die gesamte Kette der Erfordernisse von in Fibu verbucht und verprobt bis bereits bezahlt entsprechend richtig abgehandelt. D.h. es kann durchaus vorkommen, dass eine Rechnung nicht mehr geändert werden darf / kann. In diesem Falle ist eine entsprechende Gutschrift auszustellen und der Vorgang neu aufzurollen.

2.  Durch das Löschen ist das Inserat wieder im Status verrechenbar, kann dieses also wieder verändert werden.
    Ändern Sie das Inserat wie gewünscht ab.

3.  Nun muss das Inserat wieder verrechnet werden. Wählen Sie daher Bearbeiten, Rechnungen auslösen und in der Regel nur Inserat xy (das gewählte). Damit wird die Inseratenposition wieder in die unter 1.) gewählte Rechnung eingetragen / hinzugefügt, wenn nicht in der Zwischenzeit diese Rechnung z.B. versandt wurde.

#### Wie erhalte ich eine rasche Übersicht über verrechnungsgestoppte Inserate?
Stellen Sie den Statusfilter auf Gestoppt. So werden nur die Inserate angezeigt, welche im Status gestoppt sind.

In den Kopfdaten ersehen Sie unten die Begründung, warum die Verrechnung gestoppt wurde.

#### Wie wird bei Inseratenserien vorgegangen?
Die Praxis hat gezeigt, dass Inseratenserien / Kampagnen zwar von der Grundidee her immer gleich gedacht sind. In der Realisierungsphase ist es jedoch meistens so, dass dann für jeden Detailschritt eine eigene Behandlung wie z.B. unterschiedliche Sujets, der Kunde muss noch neue Bilder liefern, es erscheint das Inserat bewusst an anderen Stellen des Mediums usw. durchzuführen sind.

Daher verwenden wir anstelle der komplexen Definition von Serien, die Funktionalität des Kopierens von Inseraten, durch Klick auf ![](Inserat3.gif) Inserat kopieren.

#### Welche Auswertungen gibt es noch?
![](Inserat_Journale.gif)

Die DB-Auswertung (Deckungsbeitrag) steht nur für User zur Verfügung, welche auch das Recht LP_FINANCIAL_INFO_TYP_1 besitzen. Damit wird sichergestellt, dass nur berechtigte Benutzer die entsprechenden Auswertungen sehen. Sie dient vor allem auch der Steuerung der jeweiligen Vertreter und eventuellen Provisionsregelungen u.ä..

![](journal_offene_inserate.PNG)

Mit dem Journal Offene Inserate erhalten Sie einen Überblick über Inserate, die noch nicht abgeschlossen sind. Bei dieser Auswertung gibt es die Sortiermöglichkeiten nach Belegnummer / Kunde / Lieferant / Vertreter. Weiters ist es möglich einen Stichtag zu definieren um zum Beispiel alle nicht verrechneten Inserate zum Jahreswechsel zu erhalten.

**Stichtagsauswertung:**

Da die Stichtagsauswertung, insbesondere der nicht verrechneten Inserate immer auch für Abgrenzungen verwendet wird, finden Sie hier auch den Abgrenzungswert mit ausgewiesen. D.h. es werden dass alle Inserate die zum Stichtag offen waren angeführt, egal ob eine Eingangsrechnung zugeordnet ist oder nicht. Zusätzlich werden in den Wert der Abgrenzung nur jene Inserate mit aufgenommen, die zum Stichtag bereits eine Eingangsrechnung zugeordnet hatten. Zusätzlich wird beim Abgrenzungswert noch berücksichtigt, wenn der Einkaufswert höher als der angeführte Verkaufswert ist, so wird der Einkaufswert für den Abgrenzungswert verwendet, da davon auszugehen ist, dass bei der Rechnungslegung der Verkaufswert noch korrigiert wird. Inserate die in der Abgrenzung nicht berücksichtigt werden, werden durch einen grauen Hintergrund angezeigt.

#### Kann nachträglich der Vertreter eines Inserates geändert werden?
Ja. Unter dem Menüpunkt Bearbeiten finden Sie auch den Punkt Vertreter ändern. Voraussetzung ist, dass der Benutzer auch das Recht Chefbuchhalter hat (FB_CHEFBUCHHALTER). Damit ist es möglich, auch bei bereits komplett abgeschlossenen Inseraten den Vertreter, also den Provisionsempfänger zu ändern. Eine Aufteilung des Inserates auf mehrere Vertreter ist nicht vorgesehen.

#### Wie erhält man eine rasche Übersicht über den Inseraten Inhalt?
Dafür stehen zwei Arten zur Verfügung:

a.) in der Auswahlliste der Inserate wird nach einer kurzen Verweildauer mit der Maus auf einer Inseratenposition die Rubrik, die Bezeichnung (Fett als Überschrift) und der Kunden-Anhang als Tooltip angezeigt

b.) Ausgehend davon, dass die Inserate z.B. auf die offenen eines Kunden durch die Direktfilter eingeschränkt wurden, kann in den Kopfdaten des Inserates mit den blauen Pfeilen ![](Inserate_blaettern.gif) im durch den Filter definierten Bereich durch die Inserate geblättert werden.

#### Wie werden Inseratenaufträge bei Belegen sortiert?
Das Anlegen von mehreren Inseraten für einen Kunden übers Jahr verteilt bewirkt, dass mehrere Bestellungen angelegt werden - diese werden nach Erscheinungstermin sortiert
Beim Anlegen von mehreren Inseraten in einer Bestellung wird innerhalb der Bestellung nach Auftragsnummer der Inserate sortiert.
Innerhalb der Rechnung für einen Kunden erfolgt die Sortierung nach Inseraten-Auftragsnummer.

#### Kann ich für einen Kunden auch Monatsrechnungen erstellen?
Ja, für die monatliche Verrechnung der Inseratenaufträge gehen Sie bitte wie folgt vor:

1.) Definieren Sie beim Kunden (Kunde - Reiter Konditionen, Hakerl bei Monatsrechnung), dass dieser eine Monatsrechnung erhalten soll

2.) Wenn Sie in den Inseraten nun die Rechnung für diesen Kunden auslösen, so wird monatlich eine Rechnung erzeugt.

     Sollte für diesen Kunden in dem Zeitraum zwischen dem 1\. und Letzten des Vormonats schon eine Rechnung bestehen, so wird keine Rechnung erstellt.

     Solange diese Rechnung noch im Status "angelegt" ist, werden die Inserate, die Sie verrechnen dieser Rechnung zugeordnet.

     Sobald die Monatsrechnung gedruckt ist (also nicht mehr im Status angelegt ist), wird eine neue Rechnung für den Kunden erstellt.

Grundsätzlich wird die Monatsrechnung jeweils in den ersten Tagen des Monats für das vorhergehende Monat erstellt. Somit kann ein Inserat des aktuellen Monats nicht über eine neue Monatsrechnung verrechnet werden (zb.: Inserat aus Juni soll Ende Juni abgerechnet werden).

#### Was passiert, wenn der Kunde nach dem Kopieren geändert wird?
Wenn nach dem Kopieren ein neuer Kunde ausgewählt wird und der Rabatt anders als beim vorherigen Kunden ist, dann erscheint eine entsprechende Meldung, welcher Rabatt verwendet werden soll.
