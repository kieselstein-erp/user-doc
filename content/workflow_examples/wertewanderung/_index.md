---
title: "Wertewanderung"
linkTitle: "Wertewanderung"
categories: ["Ablauf"]
weight: 20
description: >
  Wie wandern die Werte während des Produktionsprozesses
---
Eine einfache aber hoffentlich klarstellende Beschreibung wie die Werte, also Kosten für Material und Zeit, durch deine Produktion wandern.

In anderen Worten, wo findet man welche Werte

| Angelegt | In Produktion | Abgeliefert |
| --- | --- | --- |
| Material ist auf Lager | Material ist ins Los gebucht | |
| Material ist zum Losbeginntermin als Bedarf reserviert | Fehlendes Material ist zum Losbeginntermin als fehlend / benötigt verwaltet | |
| Arbeitszeiten (Mensch und Maschine) sind als Bedarf bekannt, es darf noch nicht gebucht werden | Arbeitszeiten können gebucht werden |
| | | Summe der Materialkosten und Arbeitszeiten werden als Ablieferpreis im Los gebucht und mit diesem Einstandspreis ins Lager gebucht. Dieser (Lagerzugangs-)Wert definiert die Korrektur für die Berechnung des neuen Gestehungspreises |

Je nach Parameter wird der Ablieferpreis aus jeder einzelnen Ablieferung oder aus dem Durchschnittspreis der Ablieferungen definiert.

[Siehe dazu auch](Wertefluss_Lager_Produktion.pdf)