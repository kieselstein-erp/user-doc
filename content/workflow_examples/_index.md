---
title: "Muster Abläufe"
linkTitle: "Muster Abläufe"
categories: ["Ablauf"]
tags: ["Ablauf"]
weight: 60
description: >
  Beispielhafte Abläufe vor allem für den Einsteiger / die Einsteigerin
---
Eine Zusammenstellung beispielhafter Gedanken für die Abbildung von Unternehmensabläufen.

Die Idee ist, einem Einsteiger / einer Einsteigerin, einem/r neuen MitarbeiterIn sehr rasch die Idee hinter deinem **Kieselstein ERP** zu vermitteln.

Selbstverständlich ist diese sehr grobe Richtung um dein Wissen über dein Unternehmen zu ergänzen.
