---
title: "Warenfluss"
linkTitle: "Warenfluss"
categories: ["Ablauf"]
weight: 30
description: >
  Wie stellt sich der Warenfluss aus Sicht eines Handelshauses dar
---
Die Betrachtung des Zusammenspiels von Stammdaten und Bewegungsdaten. 

## Stammdaten
### Artikel
- Bezeichnung
- Kommentar / Beschreibung / Bilder
- Statistik (Vergangenheit)
- Offene Bestellungen / Reservierungen (Zukunft)
- Lagerstand

### Kunden
- Name, Adresse, UID Nummer
- Konditionen
  - Zahlungsziel
  - Lieferart
- Umsatz
- Sonderkonditionen

### Lieferanten
- Name, Adresse, UID Nummer
- Konditionen
  - Zahlungsziel
  - Lieferart
- Umsatz

## Bewegungsdaten
### Angebot(e)
#### Kopfdaten
- Kunde
- Lieferzeit
#### Positionen
- Artikel
 - Mengen
 - Bilder
 - Konditionen
   - Zahlungsziel
   - Rabatt
- Realisierung

### Bestellung(en)
#### Kopfdaten
- Lieferant
- Termin
#### Positionen
- Artikel
- Mengen
- Bilder
- Konditionen
  - Zahlungsziel
  - Rabatt
#### Wareneingang
- Lieferschein
- Transport
- WE-Positionen
  - Mengen
  - (Wareneinstandspreise)
  
Bewegungsdaten sind
- laufend,
- haben fortlaufende Nummern
- beziehen sich auf Stammdaten

## Einfacher Auftragsablauf

| Prozessschritt | Artikel-Lager | Reserviert | Bestellt |
| --- | --: | --: | --: |
| 1. Kundenbestellung 1.000 Stk | 0,00 | 0,00 | 0,00 | 
| 2. AuftragsbestätigungInfo an Lager -> Packliste | 0,00 | 1.000,00 | 0,00 |
| 3. Bestellvorschlag (autom. Beschaffung) | 0,00 | 1.000,00 | 0,00 |
| 3a. Bestellung | 0,00 | 1.000,00 | 1.000,00 |
| 4. Lieferterminbestätigung | 0,00 | 1.000,00 | 1.000,00 |
| 5. Terminbestätigung an Kunde | 0,00 | 1.000,00 | 1.000,00 |
| 6. Wareneingang (750Stk, Teillieferung) | 750,00 | 1.000,00 | 250,00 |
| 7. LS anhand AB (350Stk, Teillieferung) | 400,00 | 650,00 | 250,00 |
| 8. RE anhand LSArtikel | 400,00 | 650,00 | 250,00 |

