---
title: "Prozessfluss"
linkTitle: "Prozessfluss"
categories: ["Ablauf"]
weight: 10
description: >
  Vom Auftrag über die Rechnung zur qualifizieren Nachkalkulation
---

## Auftragserfassung
Warum muss jeder Auftrag erfasst werden?
- Übersicht über alle erhaltenen Aufträge
- Es wird für die Verrechnung nichts vergessen
- Jederzeit offener Auftragsstand z.B. für die Bank
- Ist die Grundlage der Auftrags-Erfolgs-Berechnung

## Anzahlungs- & Schlussrechnung
Der Auftrag definiert den gesamten Umfang.
- Mit den Anzahlungsrechnungen werden „nur“ Vorauszahlungen der Kunden angefordert.
- Diese müssen auch in der Bilanz ausgewiesen werden.
- Erst die Schlussrechnung definiert den Umfang der erbrachten Leistung.

## Auftrags-Erfolgs-Rechnung
Wozu überhaupt?

Nur wenn ich weiß welche Aufträge gut gelaufen sind und welche weniger gut, kann ich reagieren und die passenden Maßnahmen ergreifen.

Das sagt mir doch der Steuerberater?
> **Nein!** Der Steuerberater hat in der Finanzbuchhaltung nur die Gesamtsicht. Er sieht nicht welcher Auftrag gut und welcher schlecht gelaufen ist.<br>
Das wird jedoch für das **Lernen** benötigt!
>

## Was & wie muss erfasst werden?
Es geht immer darum nur die notwendigen Daten zu erfassen.

Lieber ungefähr richtig, wie genau falsch!

Also
- Das wesentliche Material
- Die Zeiten der eigenen Mitarbeiter, auch der Chef’s
- Die Kosten eventueller externer Mitarbeiter

### Was erfassen?
#### Material
- Idealerweise über Bestellung → Wareneingang → Lieferschein
- Alternativ über Eingangsrechnung und Auftragszuordnung
- Definition was gebucht wird z.B. alles was mehr als 10,- € kostet
#### Zeiten
Zumindest täglich erfassen, wer wieviele Stunden auf welchen Auftrag gearbeitet hat
#### Externe Dienstleister
- Idealerweise auch deren Zeiten erfassen und damit Überprüfung der abgerechneten Zeiten
- Erfassung der Eingangsrechnung und Zuordnung zu den Aufträgen

## Bauchgefühl?
- Jeder Unternehmer muss jederzeit den Deckungsbeitrag seiner Aufträge kennen
- Das Bauchgefühl mit Zahlen unterlegen

## Wozu dann die Projektklammer?
- Ein Projekt besteht aus mehreren Aufträgen
- Mit der Projektklammer werden, unter anderem, die Aufträge gemeinsam betrachtet und so der Gesamterfolg über alle dazugehörenden Aufträge dargestellt.
