---
categories: ["Extras"]
tags: 
title: "Rollen und Rechte"
linkTitle: "Rollen und Rechte"
weight: 50
description: >
 Benutzer anlegen, Systemrollen definieren, Rollen mit Benutzer und Personen und Mandanten verbinden
---

Beschreiben wie am Besten neu angelegte Rollen Testen
- zweiten Client mit dem neuen User mit der neuen Rolle öffnen, schauen ob alles so ist wie gewünscht.
- gegebenefalls im ersen Client, an dem man mit admin Rechten angemeldet ist Recht ändern
- im zweiten Client abmelden anmelden oder wenn zwei Mandanten, dann nur Mandantenwechsel auf den gleichen Mandanten machen
