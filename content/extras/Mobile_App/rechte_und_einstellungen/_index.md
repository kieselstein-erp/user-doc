---
categories: ["Extras"]
tags: ["Mobile Apps"]
title: "Rechte und Einstellungen"
linkTitle: "Rechte und Einstellungen"
weight: 30
date: 2023-01-31
description: >
 Rechte und Einstellungen für die mobile Applikation
---
Beachten bei den Anmeldungen, dass Benutzername und Passwort Case-Sensitiv sind (Groß/kleinschreibung)

Der Zugriff erfolgt über die RestAPI -> siehe Benutzer Mandant

Um grundsätzlich das jeweilige Modul öffnen zu können, sind zumindest Leserechte dafür erforderlich.

Hinweis: Aktuell ist die Anzeige der Module noch nicht mit den Rechten kombiniert. D.h. es kann sein, dass z.B. der Artikel zwar angezeigt wird, aber dann keine weiteren Informationen dazu ausgegeben werden dürfen, da der Benutzer dieses Recht nicht hat.<br>
Es ist diese Erweiterung geplant.

