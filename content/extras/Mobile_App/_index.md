---
categories: ["Extras"]
tags: ["Mobile Apps"]
title: "Mobile Apps"
linkTitle: "Mobile Apps"
weight: 30
date: 2023-01-31
description: >
 Mobile Applikationen die für dein Kieselstein ERP zur Verfügung stehen
---
mit der mobilen App stehen unter den verschiedenen Android Geräten folgende Funktionen in einer <u>Online Verbindung</u> zu deinem **Kieselstein ERP** Server zur Verfügung.

Für die Installation [siehe]( {{<relref "/docs/installation/05_KES_App">}} )

Bitte beachte, dass die **Kieselstein ERP** App ein Single User, Single Instance, Single Mandant Client zu deinem **Kieselstein ERP** ist.<br>
D.h. sie ist dafür gedacht, dass sich ein Benutzer an einer **Kieselstein ERP** Installation und an einem Mandanten anmeldet. Das gilt auch, wenn es einerseits ein Testsystem und andererseits ein Echtsystem geben sollte.

Für die [erforderlichen Rechte und Berechtigungen]( {{<relref "/extras/Mobile_App/rechte_und_einstellungen">}} )

Mit der **Kieselstein ERP** App können annähernd alle Buchungen der mobilen Barcodescanner und wesentliche Buchungen der Zeiterfassung(sterminals) online gemacht werden. Erforderlich ist dafür eine direkte Verbindung zu deinem **Kieselstein ERP** Server, welche gerne über einen vorgelagerten Proxyserver realisiert werden kann.<br>
Verbindungen intern werden in der Regel per WLan umgesetzt. Verbindungen von extern meist über die Telefon-Provider und unter Verwendung des https Protokolles aufgebaut.<br>
Beachte dass dafür öffentliche Zertifikate auf deinen Servern eingerichtet sein müssen. Wir beraten dich gerne.
**Wichtig:** Da die https Verbindung nur ein Verschlüsselung der Daten darstellt, raten wir, bei Verbindungen von außen, gut konfigurierte VPN Verbindungen zu verwenden. Nur damit hast du halbwegs Sicherheit, dass keine unbefugten Nutzer Zugang zu deinem Netzwerk / **Kieselstein ERP** Server bekommen.
Wird trotzdem nur https verwendet, so beachte, dass die Sicherheit nur am Passwort hängt.

Die nachfolgende Auflistung umfasst die Funktionen die mit ca. Ende Mai 2024 zur Verfügung stehen werden. 

## Einrichtung des Zugriffes
Nach dem Start der Applikation erscheint der Anmeldebildschirm<br>
![](LogOn.png)<br>

Durch Klick auf das Zahnrad kommt man in die Konfiguration.<br>
![](Konfiguration.png)<br>
Hier ist vor allem der Zugriff auf den Server, Mandant, Sprache, und ev. die RFID Leser einzurichten. In der Verwendung mit Android Barcodelesern wie z.B. Zebra TC21 oder Memor1, ist die Verwendung der Scannengine durch betätigen der Scanntaste bereits integriert.

## Anmeldung mit Barcode
Durch betätigen des Scann-Buttons bzw. durch Tipp auf ![](Anmeldescan.png) [siehe] kannst du dich komfortabel anmelden.

Nach erfolgter Anmeldung, bitte darauf achten, dass der Benutzer die RestAPI Systemrolle eingetragen haben muss, erscheint in Abhängigkeit von den Rechten folgende Modulauswahl.<br>
![](Menu.png)<br>

## Artikel
### Lagerplatzzuordnung
Ändern der Lagerplatz Zuordnung
- Textsuche. Werden hier drei oder mehr Zeichen eingegeben, so werden die ersten gefundenen Artikel deren Artikelnummer bzw. Text der Suche entspricht angezeigt. Ein Tipp auf den in der Liste angezeigten Artikel wählt diesen aus.
### Artikelauswahlliste
Mit dem Parameter ANZEIGEN_ZUSATZBEZ2_IN_AUSWAHLLISTE kann die Anzeige der Zusatzbezeichnung2 aus dem Artikelstamm in der Liste der Artikel aktiviert werden. Es wirkt dieser Parameter auf gleichzeitig in der Artikelauswahlliste deines Artikelstamms im Client.

Ist der Parameter LIEF1INFO_IN_ARTIKELAUSWAHLLISTE aktiviert, so kann in der Artikelauswahlliste auch nach Lieferanten- bzw. Herstellernummer gesucht werden.

### Artikelanzeige
Hier wird auch die aktuelle Verkaufspreisbasis des Artikels mit angezeigt, wenn bei dem Artikel auch eine Verkaufspreisbasis gegeben ist.
![](Verkaufspreisbasis_anzeigen.png)  

## Inventur
Es wird die Liste der noch nicht durchgeführten Inventuren angezeigt. Wähle die entsprechende Inventur aus und erfassen die Inventurartikel durch scannen und Eingabe der gezählten Menge bzw. auch der Chargennummern bzw. der Seriennummern. Wichtig: Damit die Inventur(en) aufgelistet wird muss diese EINEM Lager zugewiesen sein und du musst Rechte auf dieses Lager haben.

### Abweichung zu groß
Weicht die Inventurmenge um mehr als 10% vom Lagerstand erscheint dieser Hinweis (gesetzlich ist die Anzeige des Lagerstandes nicht zulässig).


## Fertigung
![](Fertigung.png)<br>
- Losgröße ändern
- Los Ablieferung

## Zeiterfassung
![](Zeiterfassung.png)<br>
Weiters können die Barcodes der Fertigungsscheine genutzt werden.

Bitte beachte, dass die Zeiterfassung für die Fertigung trotzdem im Modul Zeiterfassung der App gemacht werden muss, insbesondere wenn du die Barcodes des Fertigungsscheines nutzt.

## Bestellungen
![](Bestellung.png)<br>
Angabe der Wareneingangslieferscheinnummer deines Lieferanten.<br>
Durch Tipp auf Plus eine Warenzugangsbuchung für diese eine Position<br>

Hier werden auch die VDA Lables und die kombinierten Barcodes unterstützt. Siehe dazu [Barcodes]( {{<relref "/docs/installation/11_barcode">}} )

## Lieferung
![](Lieferung.png)<br>
Damit kann ein freier Lieferschein ergänzt werden.<br>
Der Ablauf ist, dass im **Kieselstein ERP** Client ein neuer Lieferschein angelegt wird, welcher keinem Auftrag zugeordnet werden darf.<br>
Nun kann durch Tipp auf + eine weitere Lieferschein Position hinzugefügt werden.

### freier Lieferschein an einen Kunden
Ein freier Lieferschein kann auch sehr einfach durch Scan der Kundennummer $Kxx erzeugt werden (max. 10 offene je Kunde). Für die Einstellung der Kundennummer [siehe]( {{<relref "/docs/stammdaten/kunden/#k%C3%B6nnen-auch-kundennummern-verwaltet-werden">}} )
Hier hat sich in der Praxis auch die Variante herausgebildet, dass für die wenigen Kunden, meist indirekte Lieferanten kurz die Verwaltung nach Kundennummern aktiviert wird und die Kunden-Adressetiketten ausgedruckt werden und danach diese Verwaltung wieder abgeschaltet wird.

### Lieferschein an Lieferanten
Legen einen Lieferantenlieferschein an. Wechsle aus den Lieferschein-Kopfdaten mit dem Goto in den Lieferanten. Gehe von da in den Reiter Konditionen und klicke nun rechts ob auf Lieferkonditionen ![](Lieferkonditionen_indirekter_Kunde.png)<br>
Damit bist du in diesem indirekten Kunden. Hast du nun noch die Kundennummern aktiviert, kannst du hier die Kundennummer eintragen und das entsprechende Etikett (Info, Adressetikett) ausdrucken und kannst so sehr effizient deine Lieferantenlieferscheine zusammenstellen.

## Lieferung Auftrag
![](Lieferung_anhand_Auftrag.png)<br>
Nach der Eingabe der Auftragsnummer muss der Auftrag durch antippen ausgewählt werden.<br>
Sind mehrere Lieferscheine im Status angelegt für diesen Auftrag verfügbar, so erscheint die Meldung<br>
![](Lieferscheine_buchbar.png)<br>
Das bedeutet, dass durch antippen von \<neu><br>
die Liste der bereits vorhandenen Lieferscheine angezeigt wird. Nun kannst du einen bestehenden Lieferschein auswählen / antippen, oder durch erneutes antippen von \<NEU> aus der Liste einen neuen Lieferschein anlegen.<br>
![](Lieferschein_auswaehlen.png)<br>
Anzeige der Liste der für diesen Auftrag angelegten Lieferscheine. Damit kann die weitere Bearbeitung eines Lieferscheins gewählt werden.

Hinweis:<br>
Es werden nur Lieferscheine angezeigt die im Status angelegt sind. Wurde ein Lieferschein bereits gedruckt, ist er automatisch im Status offen und scheint somit in der Liste nicht mehr auf. Solltest ein bereits gedruckter Lieferschein ergänzt werden, so muss dieser über den Client wieder in den Status angelegt zurück gebracht werden.

### Wie wird bei Unstimmigkeiten vorgegangen
Es kommt in der Praxis vor, dass man zwar VDA Etiketten hat, aber die im Etikett angegebene Menge nicht mit dem Lagerstand übereinstimmt.
- Ist nun die Menge aus dem Etikett größer als der Lagerstand erscheint eine Warnung und es wird nur die vom Lager verfügbare Menge gebucht
- Ist die Menge aus dem Etikett, größer als die offene Auftragsmenge, so wird nur die offene Auftragsmenge in den Lieferschein übertragen.
{{% alert title="Hinweis" color="warning" %}}
Bitte beachte, dass manuelle Korrekturen bei Verwendung von Barcodes nach dem VDA 4992 Standard keinerlei Wirkung auf den Inhalt des Barcodes haben.<br>
Das bedeutet, manuell ausgebesserte Mengen bedingen immer ein neu Drucken des Etikettes.
{{% /alert %}}


## Abmelden
Durch Tipp auf Abmelden gelangt man wieder in die Anmeldemaske und kommt damit auch in die [Einstellungen / Konfiguration](#einrichtung-des-zugriffes) der mobilen App.


## Anzeige des Log-Files
In deiner mobilen App werden viele Aktionen in einer Log-Datei mitprotokolliert. Damit nun jeder Benutzer diese Logdatei ansehen kann, ohne dass er/sie aus der Applikation aussteigen muss, wird beim Schwingen (Shake) des Gerätes diese Logdatei angezeigt.<br>
![](Log-File.png)<br>
Hier siehst du die genaue Version aber eben auch die Log-Datei-Einträge.<br>
Durch Tippen auf Close, wird diese Anzeige wieder verlassen.

--------------------------------
## geplante Erweiterungen
### Artikel
#### Dokumente anzeigen
Scannen Sie den Artikel ein und Sie erhalten eine Liste der für diesen Artikel abgelegten Kommentare und Dokumente (unter Berücksichtigung Ihrer Dokumentenrechte) im Format png und pdf. Durch Tipp auf eines der angezeigten Dokumente werden diese angezeigt. Bitte beachten Sie die unten angeführten Hinweise zur Anzeige von Dokumenten.

### Bestellung
#### Bestellung vormerken
Ergänzen des Bestellvorschlages durch die Erfassung der zu beschaffenden Artikel. Die benötigten Artikel werden einfach durch scannen und Angabe der gewünschten Menge in den Bestellvorschlag als Vormerkartikel mit dem Standardlieferanten übernommen und können einfach bei Ihren Lieferanten bestellt werden. Wurde ein Artikel bereits in den Bestellvorschlag übernommen erscheint ein entsprechender Hinweis mit ergänzen oder korrigieren. Während der Erfassung darf der Bestellvorschlag von keinem anderen Benutzer belegt werden. Eine quasi parallele Erfassung durch mehrere **Kieselstein ERP** App Benutzer ist möglich.

### FERTIGUNG
- Los ausgeben
- Los auf in Produktion stellen
- Materialbuchung im Los
- Los Belege drucken
- Los vollständig erledigen
- Losdokumente anzeigen

### WARENEINGANG
Wareneingang

### LIEFERUNG
#### Lieferschein-Dokumente


### Anzeigen von Dokumenten:
??????????????????????????<br>
Für die Anzeigen von Dokumenten stehen die üblichen Grafikformate (PNG, jpg, gif) und das verbreitete PDF Format zur Verfügung, sowie die Anzeige von Textdateien. Um die PDF Inhalte anzeigen zu können, muss einmalig aus dem Google PlayStore der cleverdox Viewer je mobile Device installiert werden. Sie werden automatisch dazu aufgefordert.<br>
Die Bedienung des cleverdox Viewers ist so gestaltet, dass mit einem Doppeltipp die Umrandung weggeschaltet werden kann. Diese verschwindet automatisch wenn Sie auf eine zweite Seite umblättern (Wischen). Durch einen erneuten Doppeltipp, wird die Umrandung wieder angezeigt, womit durch einen Tip auf das X links oben der PDF Viewer wieder verlassen werden kann.

### Suche nach Artikeln
Bei der Textsuche muss das Wort innerhalb der Bezeichnung des Artikels vorkommen.

### Suche nach Kunden
Im Gegensatz zur Suche nach Artikeln wird bei der Suche nach Kunden von links herein gesucht. Genau genommen ist dies von der Einstellung deines **Kieselstein ERP** abhängig. Siehe dazu bitte Parameter PARTNERSUCHE_WILDCARD_BEIDSEITIG

### Können die ausgedruckten Belege in der App angezeigt werden?
Mit der Online App werden wie oben beschrieben derzeit PDFs angezeigt.

## Die Bedienung der **Kieselstein ERP** App
Abhängig von der Berechtigung des Benutzers in seiner RestAPI Systemrolle stehen verschiedene Bereiche zur Verfügung.

## Fehlermeldungen
Nachfolgend eine Liste von Warnungen bzw. Fehlermeldungen und derem Behebung

| Bereich | Meldung | Bedeutung / Behebung |
| --- | --- | --- | ---|
| Inventur | Sie verfügen nicht über die erforderlichen Benutzerrechte | Für die Inventur benötigst du das Schreibrecht am Artikel (WW_ARTIKEL_W) und das Recht auf das zu inventierende Lager |
| Inventur | Keine offenen Inventuren vorhanden | Es gibt keine offenen Inventuren für deine Lagerberechtigungen |
| Drucken | Für den Arbeitsplatz 192.168.xxx.yyy ist im Arbeitsplatzparameter DRUCKER..... kein Drucker angegeben.| [Siehe]( {{<relref "/docs/stammdaten/system/parameter/#arbeitsplatz-parameter" >}} ) und [Serverdrucker]( {{<relref "/docs/stammdaten/system/parameter/#ansprechen-der-serverdrucker" >}} ) |

## Sammlung von Bildern
Nachfolgend nur eine Sammlun von Bildern für die weitere Verwendung.
Aufgenommen mit Rustdesk
- ![](Anmeldung01.png) 
