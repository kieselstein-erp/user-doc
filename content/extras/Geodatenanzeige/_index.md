---
categories: ["Extras"]
tags: ["Geodatenanzeige"]
title: "Extras"
linkTitle: "Geodatenanzeige"
weight: 60
date: 2023-01-31
description: >
 Die Adressen deiner Partner in GoogleMaps anhand Serienbriefen anzeigen
---
Geodatenanzeige
===============

Mit der Funktion Geodatenanzeige steht ein praktische Instrument zur Verfügung mit dem die für Sie richtigen und wichtigen Kunden in Google Maps über die Google API angezeigt werden können.
Das bedeutet, dass hinter jedem relevanten Partner die Geodaten hinterlegt werden (ICON)
Als Filterbasis steht derzeit der Serienbrief zur Verfügung. D.h. Sie definieren im Modul Serienbrief (unterer Modulreiter im Modul Partner) welche (Filter-)Kriterien für die Anzeige angewandt werden sollten.
Im Modul Geodaten wählen Sie den Ausgangsstandort und den gewünschten Serienbrief aus.
Nach der Ermittlung der Adressen aufgrund der Serienbrief-Kriterien wird eine kurze Information zum Datenstatus angezeigt.
![](Geodaten1.jpg)

Dies bedeutet, dass von den gewählten Adressen noch keine / nur teilweise Geodaten vorhanden sind.
Um die Geodaten, welche beim einzelnen Partner hinterlegt werden, zu aktualisieren klicken Sie bitte unten auf den Linken Button, Lade Geodaten.
![](Geodaten2.gif)

Nun wird die Liste der zu aktualisierenden Geodaten angezeigt.
![](Geodaten3.jpg)
Mitlaufend mit dem Fortschritt wird auch der Status angezeigt. Leider ist hier von Google eine maximale Anzahl an Anfragen pro Minute vorgegeben (wir fragen derzeit alle 500ms ab), weshalb diese Abfrage unter Umständen etwas Zeit in Anspruch nimmt. Selbstverständlich werden die Geodaten in Ihren **Kieselstein ERP** Partnerdaten mitgespeichert, weshalb diese Abfrage je Partner nur einmal erfolgt, was bewirkt dass bei erneuten Abfragen nur mehr die neu hinzugekommenen Adressen aktualisiert werden müssen. Wenn die Abfrage beendet ist, erscheint rechts unten ok.
Es werden folgende Stati angezeigt:
| Status | Bedeutung |
| --- | --- |
| Eintrag | noch nicht abgefragt |
| OK | erfolgreich abgefragt und Geodaten aktualisiert |
| OVER_QUERY_LIMIT | abgefragt, aber von Google abgelehnt (falls von diesen nur solche kommen, dann hat man wohl das Limit überschritten) |
| ZERO_RESULTS | erfolgreich abgefragt, aber kein Resultat erhalten |

Abfragen für Datensätze mit Status QUERY_QUERY_LIMIT werden wiederholt. Liefert die erste Abfrage (gesamte Adresse) ZERO_RESULTS so wird die Abfrage beim nächsten Durchlauf mit nur dem Ort wiederholt, liefert diese wieder ZERO_RESULTS, dann wird nicht weiter abgefragt.
Das abspeichern der Geodaten beim Partner bedeutet aber auch, wenn ein Partner übersiedelt / umzieht, so müssen seine Geodaten aktualisiert werden. Nutzen Sie dafür im Partner den Knopf ![](Geodaten4.gif), aktualisiere Geodaten.

Nachdem der Ladevorgang abgeschlossen ist (100%, es erscheint rechts unten ok) kann die Karte geladen werden.

Klicken Sie daher bitte auf Lade Karte.
Es werden die Daten anhand des gewählten Serienbriefes in Google Map dargestellt.

Die Pins

Die Pins sind farblich unterschiedlich gestaltet.
| Farbe | Bedeutung |
| --- | --- |
| weiß | die angegebene Ausgangsadresse |
| orange | Kunde |
| grün | Lieferant |
| blau | (nur) Partner |

Navigieren Sie mit der Maus auf den Pin, so wird der (Kunden-)Name und die Adresse angezeigt.
Klicken Sie mit der linken Maus auf den Pin, so bedeutet dies einen Goto auf das jeweilige Modul, also auf den Kunden bzw. Lieferanten bzw. Partner. D.h. es wird in das (Kunden-) Modul gewechselt und Sie stehen, wie beim GoTo üblich am gewählten Kunden.

Die Karte

So sieht z.B. ein kleiner Ausschnitt in Bezug auf die Stadt Salzburg wie folgt aus:
![](Geodaten5.jpg)
Die Navigation mit der Karte erfolgt wie üblich:
| Aktion | wie erreichen |
| --- | --- |
| Zoom | durch Rollen des Mausrades |
| Verschieben | die Karte mit der linken Maus anklicken und dies als Mittelpunkt an die gewünschte Stelle bringen |

#### können weitere Filter / Anzeigen zur Verfügung gestellt werden?
Aktuell steht nur der Serienbrief als erste Datenbasis für die Anzeige der Adressen / Pins zur Verfügung. Es ist jedoch das Softwarekonzept dahinter so aufgebaut, dass weitere Filter / Datenquellen hinzugefügt werden können. Falls Bedarf an weiteren Filtern besteht, so wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

#### Kann der Serienbrief auch für alle Sprachen genutzt werden?
Da üblicherweise ein Serienbrief auf die Sprache des Partners abgestimmt sein muss, ist dieser mit einem Filter auf die gewünschte Sprache ausgestattet. Bei der Geodatenanzeige, will man aber oft, dann doch die Partner unabhängig von ihren Kommunikationssprachen sehen. Daher kann nach der Auswahl des gewünschten Serienbriefes die Kommunikationssprache auf alle Sprachen umgestellt werden.<br>
![](Geodaten_andere_alle_Sprachen.jpg)<br>
Das bewirkt, dass die Adressen mit dem nun gewählten Filter neu geladen werden.
Damit haben Sie die Möglichkeit ohne Änderung des Serienbriefes diesen für alle Sprachen zu verwenden, bzw. sich auch die Adressen aller Sprachen entsprechend anzeigen zu lassen.

#### Was ist zu tun, wenn ein Kunde übersiedelt / umzieht?
Da, um die Anzahl der Abfragen zu reduzieren, die Geodaten beim Partner hinterlegt werden, müssen bei einer Adressänderung des Partners diese aktualisiert werden.
Klicken Sie dafür im Partner auf den Knopf ![](Geodaten4.gif), aktualisiere Geodaten.

#### Wo sieht man welche Adressen in den Geodaten angezeigt werden?
Die Basis für die Adressen sind die Adressen des Serienbriefes. Wenn Sie nun exakt prüfen möchten, welche Kunden / Partner tatsächlich an die Anzeige übergeben werden, so prüfen Sie bitte die [Empfängerliste](../Partner/Serienbrief.htm#Empfängerliste) direkt im Serienbrief.

#### Wo sieht man die aktuellen Geodaten des Partners?
Wechseln Sie im Partner bzw. Kunden, bzw. Lieferanten in den Reiter Kopfdaten.
Nun bewegen Sie die Maus über den Knopf Geodaten.
Im Tooltip werden die aktuell hinterlegten Geodaten angezeigt.
![](aktualisiere_Geodaten.jpg)

Welche Voraussetzungen sind erforderlich?

A:
1. Es muss auf dem Rechner auf dem der **Kieselstein ERP** Client läuft Java 8 bzw. Java 11 in der aktuellen Version installiert sein.
2. Für die Abfrage der Geodaten muss der Rechner auf dem der **Kieselstein ERP** Client läuft eine direkte Verbindung mit der Google-API aufbauen können. Das bedeutet dass er ins Internet zumindest eine Verbindung zur Google API aufbauen können muss.
Sie müssen eine gültige Google API besitzen, welche unter System, Parameter, GOOGLE_API_KEY eingetragen werden muss.
Und: Sie müssen die **Kieselstein ERP** Modulberechtigung und das Benutzerrecht (PART_GEODATENANZEIGE_R) für dieses Modul besitzen.

Um den Google API Key für Geodaten und ähnliches zu bekommen rufen Sie bitte die Web-Seite
<https://developers.google.com/maps/documentation/geocoding/get-api-key?hl=de>
![](Geodaten6.gif)

Klicken Sie auf Google API Console.
Sie benötigen dafür ein Google-Konto über das eventuelle Kosten abgerechnet werden.

Folgen Sie den Anweisungen, Projekt anlegen usw.

https://console.developers.google.com/apis

Zu aktivierende Apis:
- Google Maps JavaScript API
- Google Maps Geocoding API

Vorgensweise Freischalten zum Freischalten der Benötigten APIs
1. Verwaltung der Google Apis https://console.developers.google.com/apis
2. Im Dashboard auf "+ APIS UND DIENSTE AKTIVIEREN" klicken
 ![](apis_und_dienste_aktivieren.PNG)
3. Auswählen der beiden Apis: Google Maps JavaScript API und Google Maps Geocoding API

4. Nach Aktivierung müssen im Dashboard die beiden Apis zu sehen sein
![](dashboard_aktivierte_apis.PNG)

5. Der in Parameter "GOOGLE_API_KEY" einzutragende Key ist unter "Zugangsdaten" zu finden
![](zugangsdaten_api_schluessel.PNG)

6.) Nach Klick auf den Api-Key dürfen keine Einschränkungen ausgewählt sein
![](zugangsdaten_api_einschraenkungen.PNG)

#### Fallen Kosten an?
Das hängt davon ab. D.h. je nach Menge der Aufrufe und Umfang und Intensität der Nutzung können Kosten anfallen. Bitte beachten Sie die angeführten Nutzungsbestimmungen. Wir lehnen jegliche Form von Verantwortung für entstehende Kosten in diesem Zusammenhang ab.

#### Beim Verbindungsaufbau kommen Fehlermeldungen?
Da vom **Kieselstein ERP** Client eine direkte Verbindung zur GeodatenAPI hergestellt werden muss, gibt es natürlich eine Vielzahl von Möglichkeiten warum etwas nicht geht. Nachfolgend die derzeit bekannten Meldungen und die Abhilfe dazu:

Hoppla da ist ein Fehler aufgetreten

Diese Meldung kommt direkt von Google. Die Ursache dafür ist meist:
- Der hinterlegte API Key stimmt nicht
- Es müssen die APIs "Geocoding API" und "Javascript API" für diesen Key freigeschaltet/aktiviert sein. Es wird gerne vergessen auch die Javascript API zu aktivieren.

connection refused

Diese Meldung kommt, wenn wenn keine Verbindung nach außen aufgebaut werden kann. Dies ist oft der Fall, wenn sehr strikte Regeln in der Firewall eingestellt sind. Bitte dies mit Ihrer IT-Technik klären.

Unmittelbar nach dem Eintrag des API Keys kommt connection refused

Anscheinend kann es bis zu 24Std dauern, bis der API Key tatsächlich gültig ist. D.h. bitte versuchen Sie, wenn gerade ein neuer API Key eingetragen wurde, dies in den folgenden Tagen erneut.

Es erscheinen auch nach einer angemessenen Wartezeit keine Pins, keine Karte

In dem Fall klicken Sie bitte manuell auf den Reiter 2 MAP
Wird hier der Developermode angezeigt, so ist der Zugriff nicht mehr erlaubt. Ev. ist keine Kreditkarte hinterlegt, die Anzahl der freien Abrufe pro Zeitraum (Monat) bereits ausgeschöpft oder ähnliches.

Hier wird dann ein Webdialog angezeigt, der den Inhalt "Google Google Maps kann auf dieser Seite nicht richtig geladen werden".
Hier gibt es einen Link "Bist du Inhaber dieser Website".<br>
![](Geodaten_Inhaber_der_Website.jpg)<br>
Bitte auf diesen Link klicken. Es gibt dann auch eine Erklärung warum man möglicherweise "a darkened map, or 'negative' Street View image" erhält. Unter anderem falscher API key, kein Verrechnungs-Account hinterlegt, ungültige Zahlungsmethode (Kreditkarte abgelaufen?!) usw..
Von daher bitte im betreffenden Google Account nachschauen ob eine gültige Zahlungsmethode hinterlegt ist, und/oder ob der Zugriff auf den API key eingeschränkt wurde.

Request Denied bei sehr vielen Adressen

Diese Meldung kann leider vieles bedeuten. Da von Google auch immer wieder die URLs in den zurückgegebenen Meldungen / Nachrichten geändert werden, kann hier nur eine kleine Auswahl der Möglichkeiten angeführt werden.<br>
Der wesentlichste Punkt ist, dass bei dem Google-Konto des API-Key eine gültige Kreditkarte hinterlegt sein muss. Laut unserem aktuellen (Ende 2021) Wissensstand, wird von Google jedes Monat ein Budget von 200,- USD zur Verfügung gestellt. Ist dieses verbraucht wird von der Kreditkarte abgebucht.<br>
Wichtig scheint zu sein, dass ohne gültiger Kreditkarte der Dienst nicht zur Verfügung gestellt wird.

Siehe dazu auch:<br>
Error-Message: You must enable Billing on the Google Cloud Project at <https://console.cloud.google.com/project/_/billing/enable> Learn more at <https://developers.google.com/maps/gmp-get-started><br>
Siehe dazu auch <https://mapsplatform.google.com/pricing/>

Anmerkung:<br>
Für genauere Analysen kann von Ihrem **Kieselstein ERP** Betreuer ein sehr umfassendes Client-Logging aktiviert werden, in dem obige Meldungen ersichtlich sind. Wir sind uns durchaus dessen bewusst, dass dies für Otto Normalverbraucher nicht lesbar ist. Daher wird es auch nicht nach außen durchgereicht. Wir bekommen leider keine besseren Infos, wenn der Zugriff auf den Dienst gesperrt ist, wenn eben keine gültige Kreditkarte hinterlegt ist.

Prüfen Sie gegebenenfalls bitte folgende bei Google hinterlegte Daten, welche sich eventuell geändert haben könnten:
-   EMail-Adresse
-   Passwort bzw. Authentifizierungsart
-   Kreditkarte mit Kreditkartennummer(n), Sicherheitsabfrage Ihres Kreditkartenanbieters
-   Kreditlimit

#### Wieso wird die direkte Kundenadresse angezeigt, diese aber in den Geodaten nicht dargestellt?
Die Anzeige über den Button ![](Adresse_im_Browser_anzeigen.gif) [Adresse im Browser](../Partner/index.htm#MAP_Kartendienst) anzeigen, übergibt die einzelne Adresse an Google Maps bzw. Openstreetmap.<br>
Hingegen der Button ![](aktualisiere_Geodaten.gif) aktualisiere Geodaten, versucht über die Google-API die Geodaten der jeweiligen Adresse zu ermitteln. Dafür muss auch der Google-Account für die API (siehe oben) richtig hinterlegt sein.