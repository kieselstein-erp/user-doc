---
categories: ["Zutritt"]
tags: 
title: "Zutritt"
linkTitle: "Zutritt"
weight: 80
description: >
 Zutrittsanbindung
---
Zutrittskontrollsystem
======================

Im **Kieselstein ERP** steht auch ein Zutrittsmodul zur Verfügung. Dieses ist eng mit den Zutrittsmodulen der Firma MECS GmbH in Verbindung.

Daher kann diese Beschreibung nur das damals umgesetzte und gut funktionierende Konzept darstellen. Es kann gerne als Grundlage für eine Verbindung zu einer anderen Zutritts-Hardware genutzt werden. **Die MECS Zutritts-Hardware steht nicht mehr zur Verfügung**.

-------------------------------------------------------------
In **Kieselstein ERP** steht Ihnen mit dem Modul Zutritt ![](Zutritt.gif) ein umfassendes Werkzeug zur Steuerung der Zutritte Ihrer Mitarbeiter zur Verfügung. 

Da die Zutritte für Mitarbeiter freigeschaltet werden, ist das Personalmodul Voraussetzung um auch die Zutritte verwenden zu können.

Da die Ansteuerung der Zutritte zur entsprechenden Hardware passen muss, baut diese derzeit auf den Modulen LanPortACC und Zutrittskontroller auf.

Logisch gesehen arbeiten die Komponenten wie folgt zusammen:

![](Zutrittskonzept_mit_ZE.png)

| Aktion | Beschreibung |
| --- |  --- |
| 1 | Schlüssel wird an Türleser (RFID) angelegtRFID Leser sendet Daten zu LanPortACC |
| 2 | LanPortACC sendet Schlüsseldaten an Zutrittskontroller,frägt bei Zutrittskontroller nach, ob geöffnet werden darf. |
| 3 | Zutrittskontroller frägt zyklisch beim **Kieselstein ERP** Server die Konfiguration ab. |
| 4 | **Kieselstein ERP** Server liefert auf Anfrage die Zutrittsdefinitionen. |
| 5 | Zutrittskontroller entscheidet selbständig anhand der Zutrittsklasse, ob für diesen Schlüssel für die aktuelle Zeit geöffnet werden darf. |
| 6 | LanPortACC erhält das Relaiskommando vom Zutrittskontroller und schließt für die eingestellte Öffnungszeit das Realis. |
| 7 | Person kann die Türe öffnen. Es wird dies auch durch das grüne LED am RFID Leser angezeigt. |

Vorgehensweise bei neuer Hardware, Installation:
1.  Erstellen Sie ein klares Konzept wie, wo, welche Zutritte geplant sind.
2.  IP-Adressen die Sie für jeden Zutrittskontroller brauchen:
    Adresse des Servers
    Adresse des NTP Servers
3.  Definieren und programmieren Sie die IPs der LanPortACC
4.  Definieren und programmieren Sie die IPs der Zutrittskontroller

Konzept:

| Geschoss | Bereich / Mieter | Kontroller | Kontroller IP | Bezeichnung / Örtlichkeit / Lokation | Benennung | IP-Adresse | MAC-Adresse |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |
| - |  |  |  |  |  |  |  |

WICHTIG: Bei der Montage der RFID Leser muss unbedingt der in der Beschreibung geforderte Abstand der Leser zu metallischen Teilen beachtet werden. Werden die RFID Leser z.B. in Liften auf VA (Edelstahl) Verkleidungen montiert, so kommt es zu deutlichen Leseeinbußen. Das kann dazu führen, dass manche Karten gut und andere schlecht lesbar sind.

Konfiguration des Zutrittskontrollers

Dieser kann über eine Telnet Sitzung parametriert werden.

open 192.168.8.220

User: root, PW: tini

ipconfig /? für Hilfe für die Definition der IP-Adressen
Info: Bedenken Sie bitte dass bis dass sich der Controller auf die Telnetsitzung meldet, einige wenige Minuten vergehen können.

Um die Konfiguration des Zutrittskontrollers zu ändern ist ein FTP Zugriff auf den Kontroller erforderlich. Dieser Zugriff ist üblicherweise nur lesend möglich. Wenn nun neue Parameter übertragen werden müssen, so muss vorher der schreibende Zugriff erlaubt werden.

rw ... Schaltet den FTP Zugang in den Schreib-Modus

ro ... Schaltet wieder in den Read-Only Modus zurück.

Der rw Modus bleibt bis zum nächsten Neustart des Kontrollers erhalten.

Bitte denken Sie daran, unbedingt auch den NTP Server zu setzen. Nur damit kann Ihr Zutrittskontroller zum richtigen Zeitpunkt den gewünschten Zutritt gewähren. Es empfiehlt sich, den **Kieselstein ERP** Server als NTP Server zu konfigurieren und die Zutrittskontroller vom **Kieselstein ERP** Server aus mit der richtigen Zeit zu versorgen. Denken Sie auch daran, den **Kieselstein ERP** Server mit dem Internet zu synchronisieren. So wird immer von der gleichen, richtigen Zeit ausgegangen.

Für die Konfiguration des Zutrittskontrollers stellen Sie eine FTP Verbindung zum Kontroller her. User und PW wie oben.

Nun finden Sie unter /usr/zutrittOnline/accessSrv.properties die Einstellungen des Kontrollers.

Hier sind zwei Dinge wichtig:
| Wert | Vorschlag | Bedeutung |
| --- | --- | --- |
| myid | ZC0001 | ID des Zutrittskontrollers maximal 6Stellen |
| httpSvrName | 192.168.8.10 | Adresse des **Kieselstein ERP** Servers |

Alle anderen Werte bitte wie geliefert belassen.

Zum Abspeichern der Datei muss in der Telnet-Sitzung der rw Modus eingeschaltet werden.

**Hinweis:** Per default ist die Kennung der Zutrittskontroller auf die Seriennummer der Geräte gesetzt. Wir empfehlen diese in Ihre Kontrollerdefinitionen zu übernehmen. 

Passwortänderung am Zutrittskontroller

Um das default Passwort am Zutrittskontroller zu ändern, stellen Sie bitte eine Telnetverbindung zum Zutrittskontroller her.

Nun geben Sie "rw" ein.

Nun geben Sie "passwd" ein.

Sie müssen nun das neue Passwort eingeben und dieses durch eine erneute Eingabe des Passwortes bestätigen.

Das Passwort muss mindestens fünfstellig sein.

Programmierung der LanPortACC

Hinweis: Haben die LanPortACC bereits eine Verbindung mit den Zutrittskontrollern, so kann zwar die IP-Adresse ausgelesen werden, ein Zugriff auf die Daten des LanPortACC ist nicht möglich (diesen hat der Zutrittskontroller).

Auf der **Kieselstein ERP** Installations-CD finden Sie unter Tools/Zutritt Discovery_40002256_F.exe. Mit diesem Programm können die IP-Adressen der LanPortsACC definiert werden.

Ändern der IP-Adresse des LanPortACC. Doppelklicken Sie den gewünschten LanPortACC. Nun erscheint ein Login Dialog:

User: root, PW: dbps

Wählen Sie nun Network. Stellen Sie nun auf Statische IP um und definieren Sie die IP-Adressen laut Ihrem Konzept.

Es empfiehlt sich die Inbetriebnahme in einem Netzwerk mit DHCP durchzuführen. Schalten Sie aber immer nur einen LanPortACC ein, damit erkennen Sie sehr einfach, welches Gerät welche IP-Adresse hat.

Hinweis: Der Rebootvorgang des LanPortACC dauert ca. 1Minute.

Testen der Ausgänge eines LanPortACC:

Verwenden Sie dazu das auf der **Kieselstein ERP** Installations-CD mitgelieferte Programm. Sie finden es unter Tools\Zutritt\LanPortVisualizer\LanportVisualizer.jar. Um dieses Programm starten zu können, ist eine gültige JavaRunTime (1.6) Voraussetzung.

![](Lanport1.gif)

Geben Sie die IP-Adresse des LanPortACC ein und klicken Sie auf Connect.

Nun muss die Verbindung hergestellt werden können, so wie im untenstehenden Beispiel.

![](Lanport2.gif)

Mit GPIO4 kann das Relais 4 kurz eingeschaltet werden. Siehe dazu auch Definition des Zutrittsobjektes in **Kieselstein ERP**, Feld Relais: ![](Lanport3.gif)

Damit können Sie sicherstellen, dass alle Relais die gewünschte Funktion haben.

Wird beim Verbinden (Connect)

![](Lanport6.gif)

ausgegeben, so ist dieser LanPortACC bereits mit einem anderen Gerät verbunden.

Dies wird üblicherweise ein Zutrittskontroller sein.

Auslesen der Mifare Schlüssel / Karten.

Auch dies kann mit dem LanPort Visualizer durchgeführt werden. Stellen Sie die Verbindung her und halten Sie den Schlüssel / die Karte vor den Mifare-Leser. Die Schlüsselnummer wird in der Dialogbox angezeigt.

![](Lanport4.gif)

Die Schlüsselnummer ist der Datenstrom bis zum Sonderzeichen ![](Lanport5.gif).

Diese Schlüsselnummer ist im **Kieselstein ERP** Personalstamm unter Ausweis einzugeben, jedoch **OHNE** führende Nullen.

Zum Auslesen der Mifare Schlüssel kann auch ein Zeiterfassungsterminal verwendet werden, [siehe](../Zeiterfassung/Zeiterfassungsterminal.htm#Auslesen der Schlüssel):

Alternativ kann auch ein Tischleser zum Auslesen der Mifare Schlüssel verwendet werden.

Der Tischleser ist mit einer USB-Tastatur Schnittstelle ausgestattet. D.h. Sie schließen den Tischleser einfach an eine USB Schnittstelle Ihres PCs an. Zur Übernahme der Ausweisnummer gehen Sie nun bitte in die Personalverwaltung, wählen den Mitarbeiter aus, dem Sie den Schlüssel zuordnen möchten. Nun gehen Sie in die Details, wählen ändern und stellen den Cursor in das Feld Ausweis, welches leer sein sollte. Nun halten Sie den Schlüssel auf den Tischleser. Es wird nun direkt die Ausweisnummer in das Feld des Ausweises eingetragen.

Bedeutung der Zustände der Leds auf den Kartenlesern.

| Anzeige | Bedeutung |
| --- |  --- |
| Gelb dauernd | Power |
| Gelb blinkt einmal | Lesen des Schlüssels |
| Rot | Kein Zutritt für bekannten Schlüssel |
| Grün | Zutritt |

**Info:** Die Kommunikation wird immer von den Zutrittskontrollern zum Server hin angestoßen.

Die Lanports kommunizieren mit den jeweiligen Zutrittskontrollern bidirektional.

Definition des Zutrittes in **Kieselstein ERP**
------------------------------------

Grundsätzliches:

-   Zutrittsobjekte. Dies sind die Module, die die Türen, Aufzugszugänge oder ähnliches frei schalten. Sie werden von Zutrittskontrollern verwaltet.

-   Zutrittsmodelle. Diese bestimmen wann (von-bis) und wie Zutrittsobjekte geöffnet werden dürfen.

-   Zutrittsklassen. Diese definieren welche Zutrittsobjekte (Türen) zu welchen Zutrittsmodellen (Zeiten) zugeordnet werden.
    Einem Mitarbeiter kann eine Zutrittsklasse ab einem gewissen Tag (Datum) zugeordnet werden.

-   Hauptmandant, Hausverwaltung, Mieter.
    In **Kieselstein ERP** kann mit dem Zusatzmodul Hausverwaltung eine elegante Steuerung des Zutritts in einem mehr Parteiengebäude realisiert werden. Kommt das Modul Hausverwaltung zum Einsatz, so ist zwingend erforderlich, dass für die Hausverwaltung der Hauptmandant verwendet wird.

<a name="Zutrittdefinition"></a>Um die verschiedenen Definitionen richtig erstellen zu können, schlagen wir folgende Vorgehensweise vor:

-   **Zutrittsklassen**
    Definieren Sie welche verschiedenen Arten von Personengruppen Zutritt zu Ihren Gebäuden haben sollten.
    Beispiel:
        - Geschäftsleitung
        - Mitarbeiter der Fertigung

-   **Zutrittsmodelle**
    Definieren Sie die verschiedenen Zeiten, wann ein Zugang zu Ihren Gebäuden erlaubt sein sollte.
    Diese Definition baut auf dem Betriebskalender auf. Im Betriebskalender sind wiederum verschiedene Tagesarten definiert, für die Zugänge erlaubt werden können. Als Sonderzutrittsmodell kann die Funktion des Daueroffen und der Online Prüfung angesehen werden.

-   **Zutrittsobjekte**
    Die Zutrittsobjekte stellen die eigentlichen Türen dar.
    Bestimmen Sie dabei auch die Öffnungsart des Zutrittsobjektes
    Zutrittsobjekte werden von den Zutrittskontrollern verwaltet. Daher muss für jedes Zutrittsobjekt angegeben werden, von welchem Kontroller es verwaltet wird.

Zutrittsmodelle und Zutrittsobjekte werden in der Zutrittsklasseobjekt Zuordnung miteinander verbunden. Die Summe der Zutrittsklasseobjekte ergibt dann die eigentliche Zutrittsklasse, welche dann Ihren Mitarbeitern zugeordnet wird.

**Hinweis:** Zutrittskontroller und Zutrittsobjekte können nur vom Hauptmandanten aus definiert werden.

Programmteile des **Kieselstein ERP** Zutrittsmoduls:

Auch im Zutrittsmodul ist die Trennung der Funktionalitäten in den unteren Modulreitern realisiert.

Sie finden hier:

![](Zutritt_untere_Modulreiter.gif)

-   **Zutritt**
    Definieren Sie hier die Zutrittsklassen und die Zutrittsklassenobjekte

-   **Zutrittsmodell**
    Hier werden die Zutrittsmodelle mit ihren Öffnungszeiten definiert

-   **Daueroffen**
    Welche Zutrittsobjekte sollen wann dauernd geöffnet sein

-   **Onlinecheck**
    Erstellung eines Pin-Codes mit dem z.B. ein Besucher einen kurzen Zutritt zu einem Zutrittsobjekt erhält.

-   **Protokoll**
    Protokoll der versuchten und erlaubten Zutritte

-   **Grunddaten**
    Hier werden die Zutrittscontroller und die Zutrittsobjekte und deren eventuelle Verwendungsbeschränkung definiert.
    Die **Grunddaten** stehen nur für den **Hauptmandanten** zur Verfügung. D.h. bei Gebäuden mit mehreren Mietern ist der Hauptmandant für die Hausverwaltung vorgesehen. Denn die Hausverwaltung bestimmt welchem Mieter welche Zutrittsobjekte zugeordnet sind.

Definition des Zutritts:

Nachdem Sie die drei grundsätzlich erforderlichen Definitionen (Zutritts- klassen, modelle, objekte) getroffen haben, empfiehlt sich folgende Reihenfolge bei der Definition des Zutrittes:

1.  **Zutrittskontroller**
    ![](Zutrittskontroller.gif)
    Definieren Sie hier die Kennung (ID) des Controllers (das ist üblicherweise seine Seriennummer), die Bezeichnung (wo steht er, für welche Bereiche ist er gedacht) und seine statische IP-Adresse.

2.  **Zutrittsobjekte**
    Die Zutrittsobjekte werden Hardwareseitig mit den LanPotACC realisiert. Hier ist die Verknüpfung zwischen der **Kieselstein ERP** Zutrittssoftware (Logik) und den eigentlichen Zutritt gewährenden Elementen. D.h. hier wird definiert wie und wo geöffnet werden soll.
    ![](Zutrittsobjekt.jpg)
    Mit Kennung vergeben Sie einen kurzen Begriff für das Zutrittsobjekt (die Türe).
    Unter Bezeichnung geben Sie die exakte Bezeichnung des Standortes des Objektes an. Die Kennung muss eindeutig sein, die Bezeichnung sollte es sein.
    Unter Adresse geben Sie die IP-Adresse des beschrieben Lanports an.
    Unter Zutrittskontroller wählen Sie den steuernden Zutrittskontroller aus. Beachten Sie hier bitte die Begrenzungen der kleinen und großen Zutrittskontroller.
    Bei Relais wählen Sie das Relais 4, welches üblicherweise in Ihrem LanportACC für den Türöffner verdrahtet ist.
    Mit der Öffnungszeit wird die Dauer des Anzugs der Relais bestimmt.
    Bei Leser-Typ geben Sie den verwendeten Lesertyp für den RFID Leser an.
    Mit der Angabe des Mandanten können Sie definieren ob dieses Zutrittsobjekt für alle Mandanten bestimmt ist (von diesen verwendet werden kann) oder ob es nur für einen bestimmten Mandanten ist. D.h. wird hier ein Mandant angegeben, so kann dieses Objekt nur von diesem Mandanten verwendet werden. Objekte ohne Mandantenzuordnung können von allen in der Zutrittsklassenobjekte Definition verwendet werden.
    Bitte beachten Sie, dass Zutrittskontroller und Zutrittsobjekte **NUR** vom Hauptmandanten aus bearbeitet / definiert werden können.

    Relaisverdrahtung auf den Lanports

    | Relais | Stecker | Pin | Signal |
    | --- |  --- |  --- |  --- |
    | 4 | X6 | 5 | V-Out (+12V) |
    | 4 | X6 | 6 | Out2 (Kontakt) |
    | 4 | X6 | 7 | Out2 (Kontakt) |
    | 4 | X6 | 8 | GND |
    | 5 | X6 | 1 | V-Out (+12V) |
    | 5 | X6 | 2 | Out1 (Kontakt) |
    | 5 | X6 | 3 | Out1 (Kontakt) |
    | 5 | X6 | 4 | GND |

    **Wichtig:** Vom Zutrittskontroller wird nur jeweils ein Lanport verwaltet. Daher muss die IP-Adresse der angegebenen Lanports eindeutig sein. Eine getrennte Ansteuerung der Relais 4 und 5 durch zweimalige Definition der Lanports wird derzeit nicht unterstützt.

3.  **Verwendungsbeschränkung**
    ![](Verwendungsbeschraenkung.jpg)
    Mit der Verwendungsbeschränkung kann für das unter Zutrittsobjekt ausgewählte Objekt für bestimmte Mandanten ein Verwendungszähler hinterlegt werden. Dies bewirkt, dass der angegebene Mandant nur bis zur angegebene Anzahl Zuordnungen zu diesem Objekt treffen darf. Würde diese Menge überschritten, so erhält der Benutzer einen entsprechenden Fehlerhinweis.
    **WICHTIG:** Ist für ein Zutrittsobjekt auch nur eine Verwendungsbeschränkung definiert, so gilt diese Beschränkung auch für die nicht definierten Mandanten. Diese haben dann eine Beschränkung der Anzahl auf null, also keine Verwendung. Wird ein derartiges Objekt bei einem Mandanten verwendet, welcher keine Verwendung eingetragen hat, so erhalten Sie 
    ![](Verwendungsbeschraenkung.gif)
    Dies bedeutet, dass für das gewählte Zutrittsobjekt eine Verwendungsbeschränkung hinterlegt ist. Es ist jedoch für Ihren Mandanten, z.B. dem Mieter, keine Verwendungsbeschränkung eingetragen. Das bewirkt, dass **keine** Verwendung erlaubt ist.

    Hinweis: Da gerade für die Garagen Zutritt sowohl für den Verwendungszähler als auch mit den Pin-Codes (Onlineprüfung) gewünscht ist, muss um die für die Onlineprüfung erforderliche Zutrittsklasse definieren zu können, für jeden Mandanten der Zutritt gewähren darf, eine Verwendungsbeschränkung hinterlegt werden. Sollte der Mandant zwar einen Zugang mit Onlineprüfung aber ohne Verwendung benötigen, so muss die Verwendungsbeschränkung zwar angelegt, aber mit 0 definiert werden.

    Beim Speichern der Zutrittsklasse eines Mitarbeiters wird, wenn in dieser Zutrittsklasse auch Objekte mit Verwendungsbeschränkung enthalten sind, die Anzahl geprüft. Würde die Verwendungsbeschränkung überschritten, erscheint ein entsprechender Fehlerhinweis. Bei der einer Reduktion des Verwendungszählers wird dies nicht gegen die tatsächlich eingetragenen Verwendungen (Personen mit entsprechenden Klassen) geprüft.

4.  **Zutrittsmodelle**
    In den Zutrittsmodellen definieren Sie wann der Zugang erlaubt ist.
    ![](Zutrittmodell1.gif)
    Mit neu legen Sie in den Details ein neues Zutrittsmodell an. Die Bezeichnung des Zutrittsmodells und dessen Beschreibung kann jederzeit geändert werden.
    **Hinweis:** mit ![](Zutrittsmodelle_Alle_Tagesarten.gif) können alle definierten Tagesarten in den Zutrittsmodelltag übernommen werden. Definition der Tagesarten [siehe](../personal/index.htm#Tagesarten):
    **Hinweis:** Bei der Tagesart Feiertag werden nur jene Definitionen berücksichtigt, die **OHNE** Religionseinschränkung angelegt wurden. D.h. auch Mitarbeiter, die Aufgrund Ihres Bekenntnisses einen Feiertag haben, haben Zutritt, als wenn diese Definition nicht gegeben wäre.

    Unter Zutrittsmodelltag definieren Sie zu welchen Tagen grundsätzlich Zutritt sein darf. Hier stehen alle Tagesarten aus dem Betriebskalender (siehe Personal) zur Verfügung.
    ![](Zutrittmodell2.gif)

    Wählen Sie nun den gewünschten Tag aus und definieren Sie unter Öffnungszeiten die Zeiten an denen Zutritt gewährt wird.
    ![](Zutrittmodell3.gif)
    **Beginn:** Zeitpunkt ab dem geöffnet werden kann.

    **Ende:** Zeitpunkt ab dem nicht mehr geöffnet werden darf.

    **Rest des Tages:** Es kann bis 24:00 geöffnet werden. Das bedeutet, dass Sie für Personen, die immer Zutritt haben sollten für jeden Tage einen Zutritt mit Beginn 00:00 und bis Rest des Tages definieren können.
    **Hinweis:** Die Zutrittskontroller sind grundsätzlich so aufgebaut, dass immer ab 00:00 kein Zutritt gewährt wird, außer es ist eine entsprechende Zutrittsklasse angegeben. Dies bedeutet auch, dass wenn die Zutrittskontroller keine Daten einer Zutrittsdefinition mehr haben, da sie nicht mehr aktualisiert wurden, öffnen diese auch nicht mehr.

    Mit der Öffnungsart können Sonderformen der Öffnung festgelegt werden.

    Bedeutung der Öffnungsarten:
    | Öffnungsart | Bedeutung |
    | --- |  --- |
    | A Nur mit Key | Es kann nur mit der Karte / Schlüssel mit RFID Nummer geöffnet werden |
    | O Onlinecheck | Es wird Online geprüft ob geöffnet werden darf. Diese Abfrage kann je nach Serverbelastung etwas länger dauern. Sie setzt die Verfügbarkeit des **Kieselstein ERP** Servers voraus. |
    | P Key + PIN-Code | Es muss neben der Karte / Schlüssel auch noch ein persönlicher Pincode eingetippt werden. |

    **Hinweis:** Religionsspezifische Zutritte siehe [Betriebskalender](../personal/index.htm#Betriebskalender)

    **Gültigkeit der Feiertagsdefinition:**

    Wurde ein Zutrittsobjekt nur für einen Mandanten definiert, so wirkt hier der Betriebskalender dieses Mandanten. Wurde ein allgemeines Zutrittsobjekt definiert, so wird hier der Betriebskalender des Hauptmandanten herangezogen.

5.  **Zutrittsklassen**
    Die Zutrittsklassen werden durch die Verknüpfung der Zutrittsmodelle mit den Zutrittsobjekten gebildet. Jeder Person / jedem Mitarbeiter kann eine Zutrittsklasse zugeordnet werden. Anhand der Zutrittsklasse wird entschieden, ob eine Person zu einem bestimmten Zeitpunkt bei einem bestimmten Objekt Zutritt bekommt.
    ![](Zutrittsklasse1.jpg)

    Definieren Sie zuerst die gewünschten Zutrittsklassen, wie oben unter Grundsätzliches beschrieben.

    Danach wählen Sie eine Zutrittsklasse aus und definieren mit der Zutrittsklasseobjekt Zuordnung

    ![](Zutrittsmodellobjekt1.gif)
    - **Wann:** also das Modell
    - **Wo:** also das Objekt<br>
    geöffnet werden soll.

    Die Summe der Zutrittsmodellobjekt-Zuordnungen ergibt dann die eigentliche Zutrittsklasse.
    ![](Zutrittsmodellobjekt2.gif)

    **Hinweis:** Da jeder Mieter die Zutritte seiner Mitarbeiter auf den Zutrittsobjekten abbilden muss, können die Zutrittsklassen nur für jeden Mandanten erstellt werden. Ansonsten würde es die Möglichkeit geben, dass z.B. die Hausverwaltung die Zutrittsklassen eines Mieters verändert, was sicherlich nicht gewünscht ist.

    **Hinweis:** Der Hauptmandant = Hausverwaltung kann bei der Zutrittsklasseobjekt-Zuordnung ALLE Zutrittsobjekte auswählen. Dies bewirkt, dass die Hausverwaltung sich Zugang zu allen Zutrittsobjekten vergeben kann, z.B. für den Hausmeister.

Zuordnung zu den Personen:

Nachdem Sie nun die Zutrittsklassen definiert haben, können diese den Personen zugeordnet werden.
Jeder Person kann eine Zutrittsklasse mit einem Gültig ab Datum zugewiesen werden. Dieses ist solange gültig, bis der Mitarbeiter austritt, oder bis eine andere Zutrittsklasse ab einem gewissen Datum einen neuen Zutritt definiert. Sollte nun für einen Mitarbeiter nur der Zutritt vollständig abgeschaltet werden müssen, so können Sie entweder alle Zutritte für diesen Mitarbeiter herauslöschen oder Sie definieren ein Zutrittsmodell welches keinen Zutritt gestattet.

**<a name="Aktualisierungsintervall"></a>Hinweis:** Aktualisierungsintervall

Die Kontroller sind üblicherweise so konfiguriert, dass im Ein-Stunden Takt die Daten in den Kontrollern aktualisiert werden. D.h. Änderungen in den Zutrittsklassen / Zutrittsmodellen wirken erst nach der nächsten Aktualisierung. Bei dieser Aktualisierung werden die Daten für die nächsten 15 Kalendertage in den Kontroller übertragen. Bitte beachten Sie, dass ein Aktualisierungsintervall unter 10 Minuten die Netzwerk und Serverbelastung unnötig erhöht und zusätzlich eine große Anzahl an Log-Daten erzeugt.

**Sonderfall:** Gültigkeitszeitraum der Zutrittsklasse.

In der Personalverwaltung können für jede Person Zutrittsklassen hinterlegt werden. Jede Zutrittsklasse hat ein gültig ab Datum. Das bedeutet ab diesem Datum ist für diese Person diese Zutrittsklasse gültig. Da für die Zutrittsobjekte (in den Zutrittsobjekten selbst) nur jeweils eine Zuordnung zur Zutrittsklasse hinterlegt werden kann, wird immer die zum Aktualisierungszeitpunkt gültige Zutrittsklasse übertragen. Sollte nun ein Mitarbeiter ab einem gewissen Zeitpunkt eine geänderte Zutrittsklasse erhalten, so ist für die Realisierung dieser Änderung die Verfügbarkeit der Kommunikation zwischen dem **Kieselstein ERP** Server, genauer der HELIUM  V Software und dem Zutrittskontroller unabdingbare Voraussetzung. Ist die Kommunikation nicht gegeben, so bleiben die Daten seit der letzten Aktualisierung im Kontroller für maximal 15Tage im Kontroller erhalten. Wird innerhalb dieser 15Tage keine Aktualisierung vorgenommen, so werden ab dem 16.Tage keine Zutritte mehr zugelassen. Die 15Tage Vorprogrammierung können in den Mandantenparametern von **Kieselstein ERP** definiert werden.

Dieses Verhalten gilt auch für Mitarbeiter, welche ab einem gewissen Datum aus dem Unternehmen ausgetreten sind. Auch hier ist die stündliche/rechtzeitige Aktualisierung der Zutrittskontroller für das Abschalten des Zutrittes Voraussetzung.

**<u>Zu beachten:</u>**
-   Dem Mitarbeiter werden Zutrittsklassen ab einem gewissen Datum zugeordnet, d.h. es ist immer nur eine Zutrittsklasse zu einem bestimmten Datum gültig. Sollte diese Zuordnung verändert werden, ist sie erst ab der nächsten "Abholung" des Zutrittskontroller gültig.
-   Anhand der Ausweisnummer des Mitarbeiters wird über die Person die Zutrittsklasse bestimmt.
-   Die Zutrittsklasse ist mit den Öffnungszeiten in den Zutrittskontrollern hinterlegt und somit kann der Kontroller selbsttätig bestimmen ob für diese Person das Zutrittsobjekt (Türe) geöffnet werden darf.
-   Damit eine Person Zutritt bekommt muss diese eingetreten sein. D.h. das entsprechende Eintrittsdatum muss gesetzt sein. Siehe dazu auch Thematik Aktualisierungsintervall.

Zutrittsprotokoll

Nach Auswahl des unteren Modulreiters Protokoll finden Sie ein Protokoll der Zutritte für den jeweiligen Mandanten.

WICHTIG: Die angezeigten Bezeichnungen für Personen, Zutrittsobjekte und den Zutrittscontrollers werden über die Namen welche zum Zeitpunkt des Zutrittes eingestellt waren dargestellt. Dies hat den Vorteil, dass wenn danach Zutrittsobjekte umbenannt werden, trotzdem die alten Bezeichnungen  erhalten bleiben.

Hinweis: Ein erlaubter Zutritt ist in der rechten Spalte E... mit einem Haken gekennzeichnet.

![](Zutrittsprotokoll.gif)

Bitte beachten Sie auch die Direktfilter. Damit können Sie sehr komfortabel feststellen wann, wer, wo Zugang wollte. D.h. durch Eingabe eines Teiles des Namens können Sie die Anzeige auf die Person einschränken. Durch Eingabe des Zutrittsobjektes schränken Sie dies auf das entsprechende Objekt ein.

Übergabe der Daten an die Zutrittskontroller / Parametrierung

Bitte beachten Sie, dass die Zutrittskontroller, genauso wie die Zeiterfassungsterminals, über den Benutzer <lpwebappzemecs> auf **Kieselstein ERP** zugreifen. Der für diesen Benutzer eingestellte Default-Mandant bestimmt den Mandanten, dessen Parametrierung für die Kommunikation mit den Zutrittscontrollern herangezogen wird. Es sollte dies der Hauptmandant sein.

Beim Anlegen einer neuen Zutrittsklasse erhalte ich eine Fehlermeldung:

Die Kennung dieser Zutrittsklasse wird bereits von einem anderen Mandanten verwendet. Bitte verwenden Sie eine andere Kennung.
Diese Meldung besagt, dass diese Zutrittsklassenkennung bereits von einem anderen Mandanten verwendet wird. Da diese über das gesamte **Kieselstein ERP** Zutrittskontrollsystem eindeutig sein muss, kann einen Zutrittsklassenkennung im gesamten System nur einmal verwendet werden.

Warum ist die Zutrittsklassenkennung nur dreistellig?

Die Zutrittsklassenkennung wird in den Zutrittskontrollern abgespeichert. Da jeder Ausweis mit einer Zutrittsklassenkennung verknüpft ist, wird diese entsprechend oft gespeichert. Um nun hier Platz zu sparen wurde diese auf drei Stellen begrenzt. Da die Kennung Alphanumerisch vergeben werden kann, können ca. 46.000 verschiedene Zutrittsklassen in **Kieselstein ERP** vergeben werden.

Journale um die Übersicht über die Zutritte zu behalten.

Damit Sie den Überblick über die eingestellten Zutritte behalten, haben wir im Zutritt Journale geschaffen, mit denen die aktuellen Einstellungen übersichtlich ausgedruckt werden können.

-   Journal der konfigurierten Geräte
    Dieses finden Sie unter Grunddaten, Journal

-   Liste der Zutrittsklassen
    Dieses finden Sie unter Zutritt, Journal
    ![](Journal_Zutrittsdefinition.jpg)

-   Ausweise und Zutrittsklassen
    Hier sehen Sie welchem Mitarbeiter welche Zutrittsklasse zugeordnet ist.
    Siehe dazu [Personalliste](../personal/index.htm#Personalliste).

Zutritts-Onlinecheck

Mit dem Zutritts-Onlinecheck steht Ihnen eine komfortable Möglichkeit für den Besuchereinlass zur Verfügung. Wählen Sie dazu den unteren Modulreiter Onlinecheck und klicken Sie nun auf neu. Es wird automatisch ein Online Zugang mit aktuellem Datum und einer Gültigkeitsdauer ab jetzt für 10Minuten und einem zufällig generierten vierstelligen Pin-Code vorgeschlagen. Zusätzlich muss die Zutrittsklasse definiert werden. Hier ist zu beachten, dass diese nur Objekte mit Pin-Code Eingabe beinhalten darf.

![](Onlinecheck.gif)

Die Zutrittsklasse wird bei der Online-Prüfung nur für die Bestimmung der Zutrittsobjekte verwendet. Das Zutrittsmodell wird anhand des eingegebenen Zeitbereiches definiert.

Eingabe des Pin-Codes

Am RFID-Leser mit Pin-Code muss zuerst die # Taste gedrückt werden. Der Leser bestätigt dies durch eine Piebs und durch abwechselndes Blinken der roten und grünen Leds. Tippen Sie nun innerhalb einer Sekunde den erhaltenen Pin-Code ein. Nach der Eingabe der vierten Ziffer wird der Pin-Code in verschlüsselte Form zum **Kieselstein ERP** Server gesandt und wenn zutreffend der Zutritt gestattet.

Der Online Zutritt wird beliebig oft gewährt, solange der Zutritt im angegebenen Zeitraum erfolgt.

Besucherausweise

Anstelle der Eingabe des PIN-Codes kann auch eine Ausweisnummer angegeben werden. Damit haben Sie die komfortable Möglichkeit Ihren Besuchern für einen gewissen Zeitraum den Zugang anhand einer Zutrittsklasse gewähren zu können. Bitte beachten Sie, dass dies einen Online-Prüfung ist. D.h. es ist die Verfügbarkeit des **Kieselstein ERP** Servers Voraussetzung dafür, dass dem Besucher der Zugang gestattet wird. Da die Besucherausweise in die Zutrittskontroller geladen werden müssen, gilt für die Änderung bzw. Neudefinition der Besucherausweise das unter [Aktualisierung](#Aktualisierungsintervall) der Daten gesagte. ***???***

Daueroffen

Mit dem unteren Modulreiter Daueroffen kann unabhängig von den definierten Zutrittsklassen definiert werden, wann welche Zutrittsobjekte immer geöffnet sind.

![](Daueroffen.gif)

Definieren Sie auch hier zu welchen Tagen welches Zutrittsobjekt immer geöffnet sein sollte. Daueroffen bedeutet, dass das im Zutrittsobjekt definierte Relais, dauernd angezogen ist, der Kontakt geschlossen ist.

Die Liste der Zutrittsobjekte ist wieder abhängig, ob diese Definition durch den Hauptmandanten oder durch einen anderen Mandanten erfolgt. Der Hauptmandant kann das Daueroffen für alle Zutrittsobjekte definieren. Die anderen Mandanten können nur für die eigenen Zutrittsobjekte das Daueroffen festlegen.

**Hinweis:** Beachten Sie dazu bitte, dass die Türöffner für dieses Daueroffen, d.h. für 100% Einschaltdauer ausgelegt sein müssen.

## Welche Bedingungen müssen erfüllt sein, damit ein Mitarbeiter Zutritt erhält.
-   Gültige und hinterlegte Ausweisnummer
-   Eingetreten
-   Gültige Zutrittsklasse
-   Zuordnung der Zutrittsklasse zum Zutrittsmodell
-   Zutrittsmodell, welches zum gewünschten Zeitpunkt Zutritt gewährt
-   Zutrittsobjekt, welches der Zutrittsklasse zugeordnet ist.
-   Zutrittsobjekt (LanportACC) ist über den Zutrittskontroller ansprechbar und der Türöffner kann das Signal des LanportACCs auswerten.

## Welche Türschließer können eingesetzt werden ?
Beim Einsatz von Türschließern sind vor allem versicherungstechnische Fragen und sicherheitstechnische Fragen ausschlaggebend. Bitte holen Sie sich entsprechend fachmännischen Rat von Spezialisten. Alle hier angeführten Informationen sind nur als Gedankenanstoß gedacht. Sie sind weder vollständig noch ausreichend. Jegliche Haftung wird, in welcher Form auch immer abgelehnt.

### **Versicherungssicht:**
Von Versicherungen wird zwischen Einbruch und Diebstahl unterschieden.

Bei Einbruch werden deutlich höhere Versicherungssummen gewährt. Hier können Sie auch Ihr Inventar usw. versichern. Bei Diebstahl werden nur sehr geringe Versicherungssummen gewährt.

Das bedeutet für den Schutz Ihres Inventars müssen Sie die Kriterien für einen Einbruch erfüllen.

Daher muss aus Versicherungstechnischer Sicht geklärt werden, ob die einzelne Tür, das einzelne Zutrittsobjekt den Zugang so verhindern muss, dass es bei Überwindung der Barriere ein Einbruch ist.

Eine ausreichende Verriegelung liegt dann vor, wenn das Türschloss verriegelt ist. Das geht soweit, dass von den Versicherungen verlangt wird, dass bei einer Tür der Schlüssel zweimal herumgedreht ist, also der Sperrriegel ausreichend weit in den Türstock hineinreicht.

Wird eine Tür nur mit einem einfachen Türöffner, welcher noch dazu am Türstock angebracht ist, ausgestattet, so ist dieses Hindernis von einem Einbrecher sehr leicht zu überwinden. Von der Versicherung wird in diesem Fall von einem Diebstahl ausgegangen.

Um diesen Schutz zu erreichen werden sogenannte Motorschlösser eingesetzt.

Diese Motorschlösser benötigen eine Stromversorgung / Elektrische Ansteuerung durch das Türblatt hindurch.

Für die Definition der Abmessungen so eines Motorschlosses können Sie in erster Näherung folgende Skizze verwenden.

![](Zutritt_Tuerschlossdefinition.png)

Beim Einsatz der Motorschlösser oder gegebenenfalls auch der Türöffner achten Sie bitte darauf, dass diese, falls erforderlich, für den Dauerbetrieb, also Daueroffen ausgelegt sind.

Dass ausschließlich die Leser im Außenbereich angebracht sind, alle Auswerteelektronik usw. jedoch im geschützten Innenbereich ist, ist  selbstverständlich.

Hinweis: Die Elektroschlösser werden nur mit DIN-Maßen angeboten. Andere Dornmaße (z.B. ÖNORM) werden nicht angeboten. Berücksichtigen Sie dies bei der Planung Ihrer Türen.

**Neustarten des Zutrittscontrollers:**

Ein Neustart des Zutrittskontrollers kann bei bestehender Telnet Sitzung mit folgenden Commandos ausgelöst werden:
- a.) restart ... für einen vollständigen Reboot des Betriebssystems und der Zutrittssoftware durch.
- b.) zostop mit anschließendem zostart. Startet nur die Zutrittssoftware neu
- c.) Abstecken des Controllers, ein-zwei Minuten warten und wieder anstecken.

Durch das Neustarten werden sämtliche Zutrittsberechtigungen und Definitionen der Zutrittsobjekte (Lan-Ports) neu geladen.
