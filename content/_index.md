---
title: "Kieselstein ERP"
linkTitle: "Kieselstein ERP"
type: "docs"
weight: 1

cascade:
- _target:
    path: "/**"
    kind: "page"
  type: "docs"
- _target:
    path: "/**"
    kind: "section"
  type: "docs"
- _target:
    path: "/**"
    kind: "section"
  type: "home"
---

**Kieselstein ERP**, das umfassende [Open Source ERP-System](https://gitlab.com/kieselstein-erp) für produzierende kleine und mittelständische Unternehmen im deutschsprachigen Raum.<br>
Betreut durch die Kieselstein-ERP eG

![](Kiesel.png)<br>
https://www.kieselstein-erp.org



Für weitere Infos [siehe](https://kieselstein-erp.org/impressum/)

## weitere Mitglieder sind herzlich willkommen
Insbesondere Umsteiger von HELIUM V / Helium 5 haben normalerweise einen sehr geringen Umstellungsaufwand.

Berechnungsformel für [Mitgliedsbeitrag](https://kieselstein-erp.org/#beitrag) und gleich Mitgliedsantrag ausfüllen.
