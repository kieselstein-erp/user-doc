---
title: "Instandhaltung"
linkTitle: "Instandhaltung"
categories: ["Instandhaltung"]
tags: ["Instandhaltung"]
weight: 400
description: >
  Wartung, Instandhaltung nach Standorten, Hallen, Geräten. Mit benötigten Zeiten und Material und Protokollierung der durchgeführten Arbeiten
---
Instandhaltung
==============

Mit dem in **Kieselstein ERP** integrierten Modul Instandhaltung steht nun auch die Abbildung der Prozesse für ein technisches Facilitymanagement zur Verfügung.

Es erstreckt sich von der Definition der Kunden, Standorte, Hallen, Anlagen, Maschinen bis hin zu den Wartungsplänen für jedes einzelne Gerät, wobei die Wartungspläne Material und Wartungstätigkeiten beinhalten können.

Mit der Zuordnung zu Personengruppen ist auch eine entsprechende Zuweisung / Verteilung auf Ihre Techniker / auf Ihr Wartungspersonal gegeben.

Das Modul Instandhaltung ![](Instandhaltung1.gif) gliedert sich in folgende Teile:

![](Instandhaltung2.gif)

Im Reiter Auswahl legen Sie die zu verwaltenden Instandhaltungskunden an.

![](Instandhaltung3.gif)

Hier steht auch eine komfortable Suche nach direkt dem Kunden oder dem Standort zur Verfügung.

Im Reiter Standort definieren Sie die unterschiedlichen Instandhaltungs-Standorte, die unterschiedlichen postalischen Adressen, der Gebäude für ein und den selben Kunden.

Definieren Sie hier den Hauptverantwortlichen / Ihren Ansprechpartner für das Gebäude.

Hinterlegen Sie in den Bemerkungen wichtige Informationen zu diesem Standort.

Für jeden Standort muss auch ein Kundenauftrag angegeben werden. Auf diesen Auftrag laufen alle Erlöse und Aufwände, wie z.B. Wartungsrechnungen, die Zeiterfassung, die Zuordnung zum Material-Los usw. zusammen.

Somit erhalten Sie eine entsprechende Nachkalkulation des Erfolges jedes einzelnen Standortes.

Im Reiter Gerät definieren Sie die Geräte für den jeweiligen Standort.

Geben Sie hier den Gerätetyp an, welcher in den Grunddaten definiert werden muss.

Definieren Sie hier auch gegebenenfalls

![](Instandhaltung_Halle_Anlage_Maschine.gif)

in welcher Halle, in welcher Anlage bzw. in welcher Maschine sich dieses Gerät befindet.

Bei der Definition von Halle, Anlage, Maschine ist zu beachten, dass die Definition der Anlage sich auf die zuvor ausgewählte bzw. Neu angelegte Halle bezieht. Die Definition der Maschine bezieht sich wiederum auf die zuvor angelegte Anlage.

Sie erhalten damit die Möglich bis zu sechs Ebenen zu verwalten:

> Kunde
>> Standort
>>> Halle
>>>> Anlage
>>>>> Maschine
>>>>>> Gerät

Wobei es möglich ist, nach dem Standort nur direkt ein Gerät anzugeben und so mit nur drei Ebenen, z.B. für diesen Kunden, eine ausreichend exakte Definition durchzuführen.

Nachdem das Gerät definiert ist, legen Sie unter Wartungsschritte die für dieses Gerät durchzuführenden [Wartungsarbeiten](#Wartungsschritte definieren) fest.

Unter Wartungsliste definieren Sie, welches Material Sie eventuell für die [Wartungsarbeiten](#Wartungsmaterial definieren) benötigen.

#### Wie bekomme ich einen neuen Kunden in die Instandhaltung?
Legen Sie zuerst den Kunden im **Kieselstein ERP** Kundenmodul ![](Instandhaltung_Kunde.gif) an.

Wechseln Sie nun in die Instandhaltung in die Auswahl und übernehmen Sie mit Neu ![](Instandhaltung_Neu.gif) den Kunden in Ihre Instandhaltungsverwaltung.

#### Wie kann ich einen neuen Standort definieren?
Der Standort verweist auf die Partner. D.h. es muss der Standort zumindest bereits als Partner ![](Instandhaltung_Partner.jpg) angelegt sein um ihn als Standort in die Instandhaltung übernehmen zu können.

Definieren Sie im Partner unter Ansprechpartner die Personen, mit denen Sie an diesem Standort zu tun haben.

Den Hauptansprechpartner, z.B. den Hausverwalter geben Sie unter Ansprechpartner an. Ist der Ansprechpartner ausgewählt, so werden daneben die Telefonnummern des Ansprechpartners angezeigt. Oben die Festnetznummer und darunter die Handynummer.

#### Wo kann die Gerätetype definiert werden?
Im Modul Instandhaltung wählen Sie den unteren Modulreiter Grunddaten und dann den oberen Modulreiter Gerätetyp.

Definieren Sie hier die von Ihnen gewünschten Typen.

#### Wo kann ein Hersteller definiert werden?
Im Modul Instandhaltung wählen Sie den unteren Modulreiter Grunddaten und dann den oberen Modulreiter Hersteller.

**Hinweis:**

Mit diesen Eingaben werden die gleichen Daten wie im Modul Artikel, Hersteller bearbeitet.

<a name="Wartungsschritte definieren"></a>

#### Wie können die für eine Wartung erforderlichen Arbeiten definiert werden ?
Definieren Sie zuerst die für die Wartungsarbeiten erforderlichen unterschiedlichen Tätigkeiten. D.h. definieren Sie die entsprechenden Artikel als [Arbeitszeitartikel]( {{<relref "/docs/stammdaten/artikel/#wie-kann-ein-arbeitszeitartikel-definiert-werden" >}} ). Beachten Sie bitte, dass die Definition der Wartungsschritte immer für das eine gewählte Gerät erfolgt.

Übernehmen Sie die gewünschten (Wartungs-) Tätigkeiten im Reiter Wartungsschritte und geben Sie für jeden Wartungsschritt die geplante Dauer (Sollzeit), das Wiederholungsintervall, ab wann die Wartung durchzuführen ist, die [Personalgruppe]( {{<relref "/docs/stammdaten/personal/#personalgruppe" >}} ) die diese Wartung durchführen kann/darf an. Gegebenenfalls fügen Sie eine kurze Bemerkung hinzu und geben den Wochentag an, an dem die Wartungsarbeiten durchzuführen ist.

Bei der Erfassung der Wartungsschritte / Wartungstätigkeiten werden immer die bisher eingegebenen Werte vorgeschlagen, um die Eingabe zu erleichtern und trotzdem flexibel zu halten.

Über die Zwischenablage können auch ganze Wartungsschritte zwischen verschiedenen Geräten kopiert werden.

Verwenden Sie ![](In_Zwischenablage.gif) um die markierten Wartungsschritte in die **Kieselstein ERP** Zwischenablage zu kopieren und ![](Von_Zwischenablage.gif) um die Daten der Zwischenablage in die Wartungsschritte des aktuell ausgewählten Gerätes einzukopieren.

<a name="Wartungsmaterial definieren"></a>

#### Wie kann das für die Wartung eventuell benötigte Material definiert werden ?
Unter Wartungsliste definieren Sie die für die Wartung eventuell benötigten Materialien.

Wie in **Kieselstein ERP** üblich müssen die gewünschten Artikel zuerst im Artikelstamm definiert sein.

Mit Neu übernehmen Sie einen weiteren Artikel in die Wartungsliste.

Definieren Sie die Menge die Sie für eine Wartung benötigen.

Mit verrechenbar kann die Information hinterlegt werden, dass wenn dieser Artikel benötigt wird, dieser auch zu verrechnen ist.

Mit Wartungsmaterial legen Sie fest, dass Sie für eine Wartung dieses Material benötigen, also Ihr Techniker diesen Artikel mitnehmen sollte.

Wenn die Positionsart von Ident auf Handeingabe umgestellt wird, so kann ein Handartikel angelegt werden. Beachten Sie, dass für die weitere Verwaltung dieser Artikel nicht berücksichtigt wird.

Mit Veraltet kann eine kurze Begründung und das Datum ab dem der Artikel veraltet ist angegeben werden.

Für eine länger Beschreibung zu diesem Artikel, z.B. Einbauanleitungen o.ä. verwenden Sie die Texteingabe ![](Texteingabe.gif), welche während der Änderung der Position bearbeitet werden kann.

#### Welche Techniker dürfen die Arbeiten eines Standortes durchführen?
Definieren Sie im Reiter Techniker die auf einen Standort geschulten Techniker. Einer der Techniker sollte auch als Verantwortlicher für den Standort definiert werden.

#### Drucken des Wartungsplanes
Unter Journal, Wartungsplan steht Ihnen eine Auswertung der zukünftigen Wartungsarbeiten zur Verfügung.

Definieren Sie hier den Stichtag bis zu dem die errechneten Wartungsarbeiten aufgeführt werden sollen.

Grundsätzlich ist diese Auswertung nach Personalgruppen sortiert und danach nach Kunde und Standort, sodass Sie pro Standort eine Liste der zu wartenden Geräte erhalten.

Zusätzlich kann auch der Wartungsplan nur für einen Standort ausgedruckt werden.

#### Wie können die durchgeführten Wartungsarbeiten erfasst werden.
Die Wartungsarbeiten gliedern sich üblicherweise in zwei Bereiche:

a.) die Protokollierung auf welchen Geräten Wartungs- / Inspektionsarbeiten durchgeführt wurden.

b.) Wieviel Zeit für die Wartungsarbeiten an einem Standort aufgewendet wurden.

Die Protokollierung der Wartungsarbeiten für die Geräte führen Sie im Menüpunkt Bearbeiten, Wartungen erfassen durch.

![](Wartungserfassung.jpg)

Diese Erfassung ist so gedacht, dass normalerweise die Techniker die Erfassungen schriftlich protokollieren müssen. D.h. diese Listen werden in regelmäßigen Zeiträumen, täglich, wöchentlich nacherfasst.

D.h. bei der Erfassung wird unter Personal der durchführende Techniker ausgewählt, unter am das Datum der Durchführung eingegeben. Nun tippen Sie die am Wartungsplan angeführte Gerätenummer ein und drücken Enter oder Sie schließen an Ihren Rechner einen Barcodescanner mit Wedgeinterface an und scannen rasch und komfortabel alle durchgeführten Wartungsschritte der Geräte ein.

Konnte eine Wartung nicht durchgeführt werden, so haken Sie bitte nicht möglich an, um Ihrem Kunden auch dies protokolliert nachweisen zu können.

Unter dem Reiter Historie sind die durchgeführten Wartungsarbeiten je Gerät ersichtlich. Hier können falls erforderlich auch Korrekturen der bereits erfassten Daten vorgenommen werden.