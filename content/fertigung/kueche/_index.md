---
title: "Küche"
linkTitle: "Küche"
categories: ["Küche"]
tags: ["Küche"]
weight: 200
description: >
  Speiseplan, Kassenimporte, Sofortverbrauch
---
Küche
=====

Mit dem Küchenmodul wurde eine Möglichkeit der Verwaltung der Anforderungen einer Großküche geschaffen.

Mit dem Küchenmodul steht in **Kieselstein ERP** die erforderlichen Funktionalitäten zur Steuerung und vor allem zur komfortablen Abrechnung von Großküchen zur Verfügung. In einer ersten Übersicht werden folgende Funktionalitäten damit abgedeckt.

-   Erfassung des Verbrauches mittels OffLine Barcodeerfassung für bestimmte Lager, inkl. der Umbuchungsmöglichkeit und Unterstützung der EAN-Mengenübersetzung. Dieser Verbrauch wird in Tagesaufträge eingetragen / gesammelt.
-   Erstellung von Rezepten
-   Erstellung / Definition von Setartikeln zur komfortablen Verwaltung / Auflösung von kombinierten Artikeln wie z.B. Mischgetränken
-   Anbindung an verschiedene Kassentypen, mit automatischer täglicher Übernahme. Mit Übersetzungen der Kassennummern in die eigenen Artikel
-   Web-Clients für die Anbindung von z.B. Außenstellen (Shops) mit den Funktionen Terminbestellung, Wareneingang, Direktbestellung, Rücklieferung
-   Web-Clients für die Anbindung von Speisesälen, Kantinen
-   Abrechnung der Außenstellen über Kassenimport mit ev. Kundenzuordnung und damit Weiterverrechnung mittels unbarer Bezahlung.
-   Waageninterface Modul mit Artikelgruppen zu vereinfachten Warenzubuchung auf Tagesbestellung je Lieferant z.B. für Fleischerei, Warenanlieferung per EAN-Scanner
-   Detaillierte Buchungssteuerung je Lager und Kostenstelle
-   Mehrlagerverwaltung zur Steuerung der Lagerbewirtschaftung jeder Außenstelle
-   Definition Sofortverbrauch mit Zeitfaktor in Tagen -> Buchung des Sofortverbrauches in eigene Tagesaufträge. Dadurch Aufzeigen der als verdorben ausgebuchten Werte.
-   Gegenüberstellung des theoretischen Wareneinsatzes zum tatsächlichen Wareneinsatz bis in die Artikelebene. Dadurch klares Erkennen der Abweichungen zum Soll
-   Gegenüberstellung des Verkaufserlöses zum theoretischen und zum tatsächlichen Wareneinsatz
    Bei diesen Gegenüberstellungen werden die Tagesaufträge der Lagererfassung und die Tagesaufträge aus dem Sofortverbrauch entsprechend berücksichtigt.

Zugleich besteht die Möglichkeit, dass Erfassungsdaten von verschiedenen Kassensystemen in das Küchemodul eingespielt werden um entsprechende Vergleichszahlen von Verkauf und Wareneinsatz zu errechnen.

Weiters haben wir mit diesem Modul versucht, die Abläufe und Forderungen einer Großküche mit möglichst einfacher Bedienung praxisgerecht abzubilden.

Dieses Modul gliedert sich in folgende Bereiche:

[Küche / Speiseplan und Definitionen](#Detailfunktionen des Küchenmoduls)

<!-- [WebClient's](WebClient_Kueche.htm) -->
[Waagenterminal]( {{<relref "waagenanbindung"  >}} )

[Importdefinitionen Kassen]( {{<relref "kassenimport_definitionen"  >}} )

Folgende zusätzliche Funktionalitäten kommen in Kombination mit dem Küchenmodul zum Einsatz
-   Auftragsreservierung mit Termin auf Stunden und Minuten
-   Erfassung der erwarteten Rückgabe per Knopfdruck für ausgewählte Artikelgruppen
-   Druck der Kommisionier- / Packlisten je Artikelgruppe an unterschiedliche Drucker (= Ausgabestellen) damit Verkürzung der organisatorischen Wege.
-   Erfassung der weiteren Bedarfe über Bestellvorschlag, damit automatische Zuordnung zum bevorzugten Lieferanten und Auspreisung der Bestellung

 Module die für eine runde Anwendung des Küchenmoduls verwendet werden, und daher zum Einsatz kommen
-   Artikel, Mehrlager, Rezepte (Stücklisten)
-   Lieferanten, Bestellungen, Eingangsrechnungen
-   Produktion, Küchenmodul
-   Kunden, Auftrag, Lieferschein, Rechnung
-   Versanddienst, Kostenstellen

<a name="Detailfunktionen des Küchenmoduls"></a>Detailfunktionen des Küchenmoduls
-   Definition des Speiseplans je Kostenstelle pro Tag für beliebige Tage im Voraus
-   Definition der Kassenartikelübersetzungen
-   Definition Mengenfaktor von Kassenartikel zu Artikel
-   Import der Kassenformate ADS3000 und Oscar
-   Gegenüberstellung der theoretischen geplanten Wareneinsätze zu den tatsächlichen Wareneinsätzen bzw. zu den Verkaufserlösen aus den Kassen und Lieferscheinen inkl. Darstellung der auf den Tagesaufträgen gebuchten Artikel / Verbrauche
    Damit Erkennen des echten Wareneinsatzes eines Tages, einer Woche.
    Erkennen des Deckungsbeitrages jedes Tages, einzelner / der Gerichte.

**Erweiterungen in anderen Modulen**
-   Übersetzungen der EAN Barcodes auf Artikel inkl. Mengenfaktor
-   Parametrierung der Web-Clients für die Außenstellen und die Erfassung in den Speisesälen / Kantinen
-   Definition Sofortverbrauch in Tagen. D.h. hier kann der Verderb einer Ware gestaffelt nach deren tatsächlicher Haltbarkeit angewendet werden.

**Weitere indirekte Funktionalitäten**
-   Ermöglicht über die Auftragsreservierung / Terminverwaltung die Abbildung des Cateringgeschäftes
-   Verrechnungssteuerung anhand Kunden aus den Kassen(-schnittstellen) bis hin zur Zahlungsverwaltung und Mahnwesen
-   Übersicht über noch nicht verrechnete Lieferungen, Projekte z.B. Catering Aufträge
-   Nachbearbeitung der Kassen-Lieferscheine z.B. bei Fehlbuchungen, möglich
-   Nachbearbeitung der Bestellungen der Außenstellen
-   Eingangsrechnungskontrolle, Übersicht über offene Bestellungen
-   Klare Lieferantenstatistik mit Artikelumsätzen und Gesamtumsätzen z.B. für Lieferantenverhandlungen
-   Klare Artikelstatistik z.B. zum Erkennen strukturell zu viel / falsch eingekaufter Ware
-   Zuordnung der Lieferscheine zu Kunden / Kostenstellen für externe und interne Verrechnung
-   Gruppierung der Lieferscheine in der Sammelrechnung nach Ansprechpartnern, Kunden mit Detailsummen
-   Warenbewirtschaftung am Ausgangsartikel, z.B. für Mischgetränke, Speisen nach Rezepten
-   Definierter Lagerstand in den jeweiligen Lagern. Z.B. Weinlager, Lager der Fleischerei, Tiefkühllager, usw.
-   Bessere Übersicht über den tatsächlichen Lagerstand / Schwund in jeder einzelnen Außenstelle
-   Erfassen des (täglichen) Bestellbedarfes mit komfortabler Überleitung in einzelne Bestellungen

**Grundeinstellungen**

Bevor mit der Definition der [Speisepläne](#Definition des Speiseplans) begonnen werden kann, müssen die Grunddaten entsprechend definiert werden:

Definition der Kassenartikel

Mit den Kassenartikel (unterer Modulreiter Grunddaten, oberer Modulreiter Kassenartikel) werden die Übersetzungen für die Kassenimportdateien definiert. D.h. von der Kasse werden Artikelnummern übergeben, welche dann auf die Artikelnummern Ihres Artikelstamms übersetzt werden. Für den Zusammenhang beim Import der Kassendateien [siehe]( {{<relref "/fertigung/kueche/kassenimport_definitionen">}} ). Diese Übersetzung ist vor allem für den Speiseplan gedacht.

Definition der Tageslose

Mit den sogenannten Tageslosen werden die Buchungen aus den Erfassungsstiften (KDC100) vom Entnahme-Lager in das jeweilige Tageslos gebucht. Ein Tageslos ist eine freie Materialliste, welche für jedes Lager und jeden Tag wenn erforderlich automatisch angelegt und sofort in den Status ausgegeben gesetzt wird. Tageslose werden nicht erledigt. D.h. es können jederzeit, durch einen Berechtigten, weitere Korrekturbuchungen auf diese Tageslose durchgeführt werden. Im Tageslos sind die Entnahmewerte enthalten.

Die Definition der Tageslose finden Sie im unteren Modulreiter Grunddaten, oberer Reiter Tageslos.

Es kann je Lager nur ein Tageslos mit Sofortverbrauch und ein Tageslos ohne Sofortverbrauch definiert werden.

Definition der Umrechnung

Für den Import der Kassendaten steht neben der Übersetzung für den Speiseplan auch die Umrechnungsdefinition zur Verfügung. Damit wird ein auf der Kasse gebuchter Artikel einem Artikel Ihres Artikelstammes zugeordnet und die von der Kasse erhaltene Menge wird mit dem Umrechnungsfaktor multipliziert und vom Lager abgebucht.

Sofortverbrauch

In das Tageslos Sofortverbrauch werden diejenigen Artikel gebucht, welche beim entsprechenden (Nacht-) Lauf aufgrund des Wareneingangsdatums als verbraucht erkannt und daher auszuscheiden sind, gebucht.

Dieser Lauf kann im Küchenmodul auch unter Bearbeiten, Sofortverbrauch durchführen manuell gestartet werden.

**Wichtig:**

Um einen Sofortverbrauch buchen zu können, muss für das jeweilige Lager auch die Tageslosdefinition gegeben sein.

Wechseln Sie dazu auf Küche, Grunddaten, Tageslos und legen Sie je Abbuchungslager die Definition eines Tagesloses an.

![](Sofortverbrauch_Tageslos_definieren.jpg)

<a name="Definition des Speiseplans"></a>Definition des Speiseplans

Der bzw. die Speisepläne werden üblicherweise tageweise erfasst und dargestellt.

![](Speiseplan.gif)

Wählen Sie zuerst den Tag aus, für den Sie den Speiseplan erstellen möchten.

Erfassen Sie dann Menge, die Speise = Rezept, den zugehörigen Kassenartikel und die Küchengruppe.

Um auch Speisepläne für verschiedene Nutzergruppen zu ermöglichen, können über die Küchengruppe (= Fertigungsgruppe) diese Nutzergruppen abgebildet werden.

Sollte nur eine Küchengruppe dargestellt werden, so kann über Ansicht und Auswahl der Küchengruppe dies darauf eingeschränkt werden.

Wird die Speiseplan-Position abgespeichert, so wird die aktuelle Definition des Rezeptes (= Stückliste) intern abgelegt. D.h. bei Änderungen der Rezeptur wirkt sich diese Änderung erst nach erneuter Übernahme auf die jeweilige Speiseplanposition aus. Die Ablage der Speiseplanpositionen dient vor allem für die Vergleichsrechnungen.

Eine hilfreiche Funktion um schnell Speisepläne zu erstellen erfolgt über die Zwischenablage. Es werden die gewünschten Positionen im Speiseplan für einen Tag markiert und in die Zwischenablage kopiert und können dann an einem anderen Tag eingefügt werden.

Journal Küchenauswertung 1

Mit dem Journal Küchenauswertung 1 steht eine Gegenüberstellung der über die Kassenimportdateien erhaltenen Verkäufe, z.B. der Mittagsmenüs, zu den theoretischen Wareneinsätzen anhand des Speiseplans zur Verfügung. Zusätzlich werden die auf die Sofortverbrauche und die Tageslose gebuchten Wareneinsätze angezeigt. So haben Sie einen Überblick über den tatsächlichen Wareneinsatz im Vergleich zu den erzielten Erlösen des eingegebenen Zeitraums.

Bei den Wareneinsätzen auf den Tageslosen wird die Summe der auf den Losen gebuchten Materialien verwendet.

Journal Küchenauswertung 2

Mit dem Journal Küchenauswertung 2 wird der detaillierte Wareneinsatz der Tageslose und Sofortverbrauche den theoretischen Verbrauchen aufgrund des Speiseplans gegenübergestellt. Hier sehen Sie ob Ihre theoretische Planung aufgrund der Rezepte mit den tatsächlich verbrauchten Artikeln zusammenstimmt.

Bei den Wareneinsätzen auf den Tageslosen wird die Summe der auf den Losen gebuchten Materialien verwendet.

Journal Deckungsbeitragsanalyse

Mit dem Journal Deckungsbeitragsanalyse erhalten Sie eine Auswertung über die Erlöse und den Wareneinsatz eines Zeitbereiches gegliedert nach Kostenstellen. Damit steht Ihnen eine rasche Auswertung über den Deckungsbeitrag jeder einzelnen Kostenstelle zur Verfügung.

| Spalte | Beschreibung |
| --- |  --- |
| Kostenstelle |  |
| Rechnung | Umsatzerlöse im Zeitraum nach Rechnungsdatum netto |
| Wareneingang | Wareneingang im Zeitraum nach Wareneingangsdatum zur Basis des Einstandspreises jeder Wareneingangsposition |
| Sofortverbrauch | Die in die Sofortverbrauchslose gebuchten Waren je Kostenstelle zur Basis Gestehungspreis |
| Übrige Lose | Die Warenbewegungen auf alle übrigen Lose (nicht Sofortverbrauch) |
| Warenausgang | Warenausgang im Zeitraum nach Warenausgangsdatum zur Basis des Gestehungspreise.Hier werden keine Handeingaben berücksichtigt. |

Allergene im Artikel

Auf Grund der Kennzeichnungspflicht, müssen Allergene im Lebensmittelbereich angeführt werden. In den Grunddaten können die einzelnen Allergene definiert werden.

Einen Überblick über die enthaltenen Allergene pro Artikel (nur Stücklisten) erhalten Sie im Journal Allergene.

Da die Information von den Lieferanten der Produkte weitergegeben werden muss, gibt es einen Import der Lieferantendaten im Menüpunkt Pflege (Recht WW_DARF_LAGERPRUEFFUNKTIONEN_SEHEN muss vorhanden sein). Hier ist der Aufbau: Spalte 1 = Artikelnummer des Lieferanten und ab Spalte 3 sind die Allergene angegeben. Mit einem X wird gekennzeichnet, dass dieses Allergen in dem Artikel enthalten ist. Importiert wird ab Zeile 2, da Zeile 1 die Überschriften enthält. Allergene, die nicht in den Grunddaten definiert sind, werden ignoriert, genauso wie nicht angelegte Lieferantenartikelnummern.

Lagerbuchung mit Barcodestift KDC100 (ACHTUNG: der KDC100 steht nicht mehr zur Verfügung. Melde dich bitte bei entsprechenden Bedarf. Wir haben mit ähnlichen Stiften, ähnliche Lösungen umgesetzt)

Mit dieser Funktion, Sie finden das Einlesen unter Bearbeiten, Stiftdaten einlesen, werden komfortabel alle Lagerentnahmen eines Tages erfasst. Die Erfassung basiert auf den auf den Artikeln aufgebrachten Barcodes. Hier sollten bevorzugt die EAN-Codes der Hersteller verwendet werden. Für die Übersetzung der EAN-Codes auf den tatsächlichen Artikel mit eventueller Mengenumrechnung, siehe Artikel, EAN-Nummern Zuordnung.

Werden die Daten eines Stiftes ausgelesen, so werden die darauf gebuchten Lagerentnahmen auf ein zum Entnahmelager passendes Tageslos gebucht. D.h. die EAN-Nummern werden übersetzt und die sich daraus ergebenden Mengen vom Entnahmelager in das Tageslos eingetragen. Dadurch erhalten Sie eine klare Übersicht, welche Waren / Artikel von welchem Lager an welchem Tag entnommen wurden.

Stehen auf einzelnen Artikeln keine EAN-Codes / Barcodes zur Verfügung, so erstellen Sie passende Barcodes z.B. durch Verwendung der Artikeletikette (Artikel, Info, Etikette). 

Wichtig: Beim Einlesen werden nur Artikel akzeptiert, die in der EAN-Übersetzung enthalten sind. Sollten Sie eigene Barcodes, z.B. mit der eigenen Artikelnummer verwenden, so müssen diese trotzdem in der EAN-Übersetzung eingetragen werden.

Vor der Verwendung dieser Funktion muss für jedes Lager ein Tageslos definiert sein.

Zu Installation der Scanner siehe bitte: [Installation Barcodescanner KDC100]( {{<relref "installation_KDC100" >}} )


Parametrieren und Einlesen der Stiftdaten:

Tritt beim Einlesen ein Fehler mit folgendem Inhalt auf, so ist auf dem entsprechenden PC die serielle Kommunikation nicht ordnungsgemäß konfiguriert
Wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer.

Die Stifte müssen vor der Verwendung von Ihrem **Kieselstein ERP** Händler freigeschaltet werden.

Danach muss jedem Stift, bezogen auf den Arbeitsplatz, sein Default-Lager zugeordnet werden.
Wählen Sie dafür, System, Arbeitsplatzparameter. Wählen Sie Ihren PC aus oder fügen Sie Ihren PC durch Klick auf Neu hinzu.
Nun wechseln Sie auf den oberen Modulreiter Parameter, wählen erneut Neu und wählen den Parameter KDC100_DEFAULTLAGER. Geben Sie nun die interne ID des Lagers an. Dieses teilt Ihnen Ihr **Kieselstein ERP** Betreuer gerne mit.

Sind alle Stifte richtig parametriert, so sollten zur Zeitsynchronisierung alle Stifte einmal ausgelesen werden.

**Wichtig:** Für die serielle Kommunikation darf nur eine Java Version installiert sein. Daher unbedingt das Java Update abschalten.

Einlesen der Stiftdaten.

Stecken Sie alle Stifte an die USB-Übertragungsstation(en) an. Es müssen für alle Stifte entsprechende USB-Portkonfigurationen hinterlegt sein. Kommt es beim Anstecken eines Stiftes zum Aufruf des Hardwareassistenten, so wurde an dieser Schnittstelle noch nie ein Stift angeschlossen. Gehen Sie dazu wie [oben beschrieben](#Installation KDC100) vor.

Warten Sie beim Einstecken jedes Stiftes solange, bis durch einen Ton angedeutet wird, dass der Stift von Ihrem PC erkannt wurde. Jeder Stift muss nach dem Einstecken kurz "USB Port connected" anzeigen.

Belassen Sie die Stifte in der Übertragungsstation, bis der Einlesevorgang vollständig abgeschlossen ist. Für Datenfehler, die durch Entfernen der Stifte während des Auslesevorganges auftreten, lehnen wir jede Verantwortung ab.

In der Küchenauswahlliste, klicken Sie im Menü auf Bearbeiten und wählen, Stifte einlesen.

Nun werden alle derzeit angeschlossenen Stifte mit der Anzahl der Datensätze und dem entsprechenden Default Lager angezeigt. Bitte beachten Sie, dass abhängig von der Anzahl der aktiv angeschlossenen Stifte, die Startzeit variiert.

Wird in der Übersichtsliste der angeschlossenen Stifte beim Stiftnamen unbekannt angezeigt ![](Kueche_KDC100_Einlesen1.gif)

so ist der Stift für die Übertragung nicht freigeschaltet. Bitte wenden Sie sich an Ihren **Kieselstein ERP** Händler.

![](Kueche_KDC100_Einlesen2.gif)

**Hinweis:** Sind Stifte längere Zeit nicht in Betrieb gewesen, so sollten diese vor der Verwendung vollständig geladen werden und zusätzlich einmal ohne Daten eingelesen werden, damit die Uhrzeit des Stiftes mit der Systemzeit Ihres PC's, welche unbedingt gleich der Systemzeit des **Kieselstein ERP** Servers sein muss, synchronisiert wird.

Nun stehen folgende Funktionen zur Verfügung:
| Funktion | Beschreibung |
| --- |  --- |
| Lager | Dieser Wert ist mit dem oben beschriebenen Default Lager vorbesetzt. Durch Klick auf Lager kann ein anderes Lager ausgewählt werden. |
| Stift einlesen | Übernahme der gescannten Artikel in das durch das Lager definierte Tageslos, mit gleichzeitiger Buchung der Artikel über die EAN-Übersetzung vom angegebenen Lager in das Tageslos. Nach erfolgter Buchung der Daten in das Tageslos werden die Daten im Stift automatisch gelöscht. Kommt es während der Übertragung zu Fehlern, so wird die Transaktion abgebrochen und die Daten im Stift bleiben erhalten. |
| Leeren | Wird die anschließende Frage mit Ja beantwortet, so werden alle Daten am Stift unwiederbringlich gelöscht. |

Sollten Sie, warum auch immer, die Stifte umstecken müssen, so verlassen Sie bitte die Einlesefunktion und starten diese erneut. Dadurch werden die angeschlossenen Stifte neu initialisiert.

**ACHTUNG:** Werden beim Anstecken der Stifte zwar die Seriennummer angezeigt, aber z.B. keine Daten, obwohl Daten im Stift sind, so ist davon auszugehen, dass die USB-Stromversorgung des Ports zu schwach ist. Verwenden Sie in diesem Fall einen entsprechenden USB-Hub um ausreichen Power für die Stifte zu liefern.

Nachdem alle Stifte ausgelesen wurden, verlassen Sie das Menü mit ![](Kueche_Stifteinlesen_verlassen.gif) bzw. ESC.

Bitte prüfen Sie unbedingt das Protokoll des Einlesens, welches Sie im unteren Modulreiter, KDC100 Protokoll finden. Hier werden die übersetzen Artikeln und Mengen angezeigt, aber auch die Barcodes welche nicht übersetzt werden konnten.

**Hinweis:**

Werden Schnittstellen mit angeschlossenen Stiften nicht angezeigt, so werden diese Schnittstellen von anderen Programmen belegt und können daher von **Kieselstein ERP** nicht verwendet werden. Geben Sie diese gegebenenfalls frei um auch Daten von diesen Stiften (Com-Ports) einlesen zu können. Wird ein Com-Port der Ihrer Meinung nach zur Verfügung stehen sollte nicht angezeigt, so verwenden Sie bitte unter der Eingabeaufforderung mode com?, wobei Sie für das ? die Nummer des gewünschten Com-Ports eingeben. Hier wird gegebenenfalls auch angezeigt, ob ein entsprechender  Com-Port belegt ist

<a name="Bedienung des KDC100"></a>Bedienung des Barcodestiftes:

Die Stifte werden in der sogenannten Fabrikseinstellung geliefert.
Es sind in dieser Einstellung alle gängigen Eindimensionalen Barcodes freigeschaltet. Für eine Verbesserung der Erfassungssicherheit können einzelne Barcodetypen abgeschaltet werden.

**<u>ACHTUNG:</u>** Der Stift sendet Laserlicht aus. Sehen Sie nicht direkt in den Lichtstrahl des Lasers.
![](KDC100_Laserlicht.jpg)

<br>

![](KDC100.png)<br>

| Funktion | Beschreibung |
| --- |  --- |
| Gut/Schlecht | War die letzte Barcodelesung gut = grün oder Fehlerhaft = rot<br>Zugleich Ladezustandsanzeige, während die Stifte am USB Port angeschlossen sind. Rot = es wird noch geladen, grün = Stift ist ausreichend geladen. |
| Ladezustand | Beachten Sie den Ladezustand des Stiftes. |
| Anzeige | hier wird der zuletzt gescannte Barcode mit der Scannzeit angezeigt. |
| Scannknopf | Richten Sie den Stift auf den zu scannenden Code und drücken Sie kurz die große Taste direkt unter der Anzeige. Nun wird der Laserstrahl ausgesandt. Richten Sie den Laserstrahl nun auf die Mitte des Barcodes aus. Eventuell muss der Abstand zum Barcode reduziert oder erhöht werden. Siehe dazu auch unten: so erzielen Sie gute Scannergebnisse. Der Stift schaltet das Display nach ca. 5Sekunden ab. Konnte der Barcode gelesen werden, werden Uhrzeit und gelesene Daten angezeigt. Konnte der Barcode nicht gelesen werden, so wird *Failed reading* angezeigt.Haben Sie irrtümlich einen falschen Barcode eingelesen, so scannen Sie bitte den mitgelieferten Storno Barcode, welcher unter ... ausgedruckt werden kann. |
| USB Anschluss | Um den Stift in die Übertragungs- und Ladestation zu stecken, klappen Sie den USB Anschluss mit der Entriegelungsnoppe heraus. Nach der Entnahme aus der Übertragungsstation klappen Sie den USB Anschluss wieder ein, um ihn vor Verschmutzung und Beschädigung zu schützen. |
| Lautsprecheröffnung | In sehr schmutzigen Umgebungen kann es von Vorteil sein, die Öffnung für den Lautsprecher abzukleben. |

**Hinweis:** Bedenken Sie bitte die üblichen Vorkehrungsmaßnahmen zum Erhalt der Lebensdauer der in den Stiften eingebauten Lithium Polymer Batterien. Das bedeutet, dass die ersten fünf Ladezyklen vollständig erfolgen sollten. Dass also immer die Stifte komplett leer sein sollten bevor sie wieder erneut geladen werden.

Übertragen Sie die Daten der Stifte täglich. Dadurch erreichen Sie:
- aktuelle Daten in Ihrem **Kieselstein ERP** System
- Im Falle eines Verlustes des Stiftes oder anderer unvorhersehbarer Ereignisse, ist der Datenverlust entsprechend geringer.

Sollten Sie trotzdem so viele Daten in den Stift einscannen, dass der interner Speicher voll wird, so wird die Meldung "Buffer Full" ausgegeben.

Wir empfehlen die Stifte mit den vergebenen Namen zu kennzeichnen, dadurch wird das Handling deutlich einfacher.

<a name="Erlaubte Barcodes einschränken"></a>

#### Kann die Art der erlaubten Barcodes eingeschränkt werden?
Ja. Starten Sie das Menü des Scanners durch gleichzeitiges Drücken beider Cursortasten auf der linken Seite des Scanners.

Drücken Sie die nach unten Taste bis zu Set Barcodes und wählen Sie den Menüpunkt durch Drücken auf den Scannknopf aus.

Nun finden Sie die Liste der vom Scanner unterstützen Barcodes. Die Codes, welche vom Scanner akzeptiert werden, sind vorne mit einem * gekennzeichnet. Durch Drücken auf den Scannknopf können diese Ein- und Ausgeschaltet werden.

Um das Menü zu verlassen wählen Sie an dessen Ende Save & Exit.

Gehen Sie in den nachfolgenden höheren Menüs jeweils an das Ende bis Exit Menü um wieder in den normalen Scannmodus zu gelangen.

Die weiteren Einstellungen des Scanners bitte belassen. Falsch eingestellte Scanner können entsprechende Fehlfunktionen liefern.

**Hinweis:**

Von **Kieselstein ERP** werden üblicherweise nur die Barcodes Code39 und Code128 verwendet.

Weitere Barcodetypen können vom Reportgenerator jederzeit erzeugt werden. Achten Sie auf die Übereinstimmung der Einstellungen.

**<u>Wichtig:</u>**

**Die Scanndaten des Codes EAN-128 können derzeit nicht übertragen werden. Schalten Sie diesen Barcode auf jeden Fall aus.**

#### Kann die Scann-Breite des Stiftes eingestellt werden?
Ja im Menü des Scanners kann zwischen breitem und schmalen Scannbereich umgeschaltet werden.

Starten Sie das Menü durch gleichzeitiges Drücken beider Cursortasten auf der linken Seite des Scanners.

Drücken Sie die nach unten Taste bis zu Scan Options und wählen Sie den Menüpunkt durch Drücken auf den Scannknopf aus.

Wählen Sie Scan Angle und aktivieren sie den gewünschten Winkel Narrow (schmal) oder Wide (breit) durch betätigen des Scannknopfes. Danach wählen Sie Save & Exit.

Gehen Sie in den nachfolgenden höheren Menüs jeweils an das Ende bis Exit Menü um wieder in den normalen Scannmodus zu gelangen.

Die weiteren Einstellungen des Scanners bitte belassen. Falsch eingestellte Scanner können entsprechende Fehlfunktionen liefern.

#### Kann der Stift auch ohne Scann aktiviert werden?
Ja. Betätigen Sie einfach eine der Tasten auf der linken Seite des Scanners.

#### Wann wird der Ladezustand angezeigt?
Der Ladezustand wird immer nach einem erfolgreichen Scann angezeigt.

Denken Sie daran, dass der Stift nur bei geringem Batterie Ladezustand aufgeladen werden sollte um die Lebensdauer der Lithium Polymer Batterien entsprechend zu erhalten.

#### Woher sehe ich, welche Seriennummer ein Stift hat?
Betätigen Sie gleichzeitig beide Tasten auf der linken Seite des Scanners um in das Menü zu gelangen.

nun blättern Sie mit der unteren Pfeiltaste (links) bis zum Punkt System Config und drücken Sie die Scanntaste. Nun gehen Sie wiederum nach unten bis zum Punkt Version. Hier werden Firmwareversion (FW:) und Seriennummer (SN:) angezeigt.

Steppen Sie nun durch die Menüs zurück, indem Sie im jeweiligen Menü ganz nach unten gehen und bei Exit usw. jeweils die Scanntaste betätigen.

Weiters ist der endende Teil der Seriennummer auf der Rückseite des Stiftes aufgedruckt.

#### Die Stiftdaten können nicht übertragen werden, wie sollte ich vorgehen?
Betätigen Sie gleichzeitig beide Tasten auf der linken Seite des Scanners um in das Menü zu gelangen. Wählen Sie nun

View Data mit den seitlichen Cursor Tasten und betätigen Sie die große Scanntaste (vorne/oben).

Nun werden die gescannten Barcodes (oben) und der Scannzeitpunkt (unten in kleiner Schrift) angezeigt.

Das Format des Scannzeitpunktes ist JJJJMMDD:HHMMSS.

Blättern Sie mit den Cursortasten durch die erfassten Daten und protokollieren Sie die Buchungen. Lässt sich ein Datensatz eindeutig als defekt / falsch identifizieren, so notieren Sie bitte trotzdem die Buchung. Drücken Sie dann die Scanntaste und wählen Sie Delete & Exit. Damit wird der ausgewählte Datensatz gelöscht.

Übertragen Sie die gelöschten Buchungen per Hand in Ihr **Kieselstein ERP** System. Wenn dies für Sie nicht lesbar ist, so senden Sie uns bitte diese Daten, wir teilen Ihnen gerne mit, wie diese manuell und nachträglich eingebucht werden können.

Zum Schluss verlassen Sie das Datenmenü durch Anwahl von Top Menu und zweimal Cursortaste nach oben, Exit Menu.

KDC 100 Protokoll

Beim Import der Stiftdaten können eventuelle Fehler auftreten. Ist dies der Fall, so erscheint ein Hinweis. Die tatsächlichen Fehler können unter Grunddaten KDC100 Protokoll eingesehen werden.

#### Mein KDC reagiert überhaupt nicht mehr, was tun?
Bevor Sie den Stift zur Reparatur zu uns einsenden, prüfen Sie bitte ob der Stift durch einen Software-Reset wieder aktiviert werden kann. Gehen Sie dazu wie folgt vor:

1.) Schließen Sie den Stift mit dem mitgelieferten Spezial-Micro-USB Kabel an einen PC an.

2.) Drücken Sie die Nach-Unten Taste am linken Rand und die Scann-Taste gemeinsam, für ca. 5Sekunden.

3.) Wenn die LEDs aufleuchten, je nach Ladezustand rot, gelb, oder grün, lassen Sie die Tasten los.

4.) Nun wird das Logon angezeigt

![](KDC100_Logon_Screen.jpg)

Nun sollte Ihr KDC 100 wieder funktionieren.

**Hinweis:** Bitte beachten Sie, dass nur von uns gelieferte KDC100 Stifte verwendet werden können.
