---
title: "Küche, Details"
linkTitle: "Details"
categories: ["Küche"]
tags: ["Küche"]
weight: 10
description: >
  Details zum Küchenmodul
---
Küche-Details
=============

Definieren Sie hier

Speiseplan

die Übersetzung der Kassenartikel

die Tageslose

Umrechnungsfaktoren für die Shops

Zusätzlich finden Sie hier die beiden Imports der Kassendateien und die Extraliste für die Importprotokolle.

Definition Übersetzungen Kassen Artikelnummern, Speiseplan-Tagesartikel

Da die täglichen Menüs immer gleich erfasst werden und da die Daten in aller Regel von externen Kassen angeliefert werden, müssen die von der Kasse (Oscar) gelieferten Daten auf die HELIUM V Artikel übersetzt werden.

Das wird im Küchenmodul, unterer Modulreiter Grunddaten, Kassenartikel vorgenommen.

Beachten Sie bitte, dass die von der Kasse gelieferten Daten, also die Artikelnummer exakt mit den eingegebenen Artikelnummern übereinstimmen müssen. D.h. es müssen auch die führenden Nullen mit übergeben werden.

Sind nun die Kassenartikel definiert, so kann je Tag ein Speiseplan definiert werden.

![](Kueche2.gif)

Definieren Sie hier die möglichen Speisen für den jeweiligen Tage unter Angabe der Menge, des Rezeptes aus der Rezeptverwaltung, des zugehörigen Kassenartikels und der Küchengruppe, welche die Speisen produziert.

**Wichtig:** Der Speiseplan ist wiederum die Vorlage für den Lieferschein welche über den Webclient der Essensausgabe erstellt wird.

Import der Kassendaten

[Siehe:]( {{<relref "/fertigung/kueche/kassenimport_definitionen">}} )

**Protokoll des Imports:**

Dies kann über die Extraliste eingesehen werden.

Definieren Sie unter System, Extraliste eine neue Extraliste, mit dem Namen Importprotokoll für die Belegart Kueche.

Dialogbreite z.B. 1000 Pixel, unter Query kopieren Sie bitte folgenden String ein.

[SELECT p.c_typ as TYP, p.c_text AS Meldung , p.t_quelle AS Buchungsdatum , p.t_anlegen AS Anlagedatum FROM FLRProtokoll p WHERE p.c_typ='KASSA_OSCAR' or p.c_typ='KASSA_ADS3000' ORDER BY p.t_anlegen DESC](SELECT%20p.c_typ%20as%20TYP,%20p.c_text%20AS%20Meldung%20,%20p.t_quelle%20AS%20Buchungsdatum%20,%20p.t_anlegen%20AS%20Anlagedatum%20FROM%20FLRProtokoll%20p%20WHERE%20p.c_typ='KASSA_OSCAR'%20or%20p.c_typ='KASSA_ASD3000'%20ORDER%20BY%20p.t_anlegen%20DESC)

Nach der Definition der Extraliste öffnen Sie danach das Küchenmodul erneut.