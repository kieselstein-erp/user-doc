---
title: "Küche"
linkTitle: "Waagenanbindung"
categories: ["Küche"]
tags: ["Waagenanbindung"]
weight: 30
description: >
  Anbindung von Wiegedaten
---
Waagenanbindung
===============

{{% alert title="Info" color="info" %}}
Nachfolgender Artikel beschreibt ein mögliches Konzept.<br>
Die Grundlagen dafür sind im **Kieselstein ERP** vorhanden.<br>
Wenn du eine ähnliche Lösung suchst, wende dich vertrauensvoll an deinen **Kieselstein ERP** Betreuer
{{% /alert %}}


Mit dem Küchenmodul steht auch die Anbindung von Wiegedaten in komfortabler Form zur Verfügung.

Dies ist in der Weise aufgebaut, dass nach einmaliger Anmeldung der gewünschte Lieferant definiert wird und dann die Zubuchung über Artikelgruppen und Artikelgruppe erfolgt.

Zusätzlich kann das Waagenterminal mit einem seriellen Barcodescanner ausgerüstet werden. Damit kann über die EAN-Übersetzung im Artikelstamm eine sehr einfach zu bedienende Zubuchung von Artikeln erfolgen.

Das Waagenterminal ist derzeit nur für Lagerzubuchungen ausgelegt.

Das Gehäuse des Terminals ist in Edelstahl ausgeführt und entspricht IP65.

Bitte achten Sie unbedingt darauf, dass das Gehäuse satt geerdet ist.

Die Zubuchung erfolgt in der Form, dass je Lieferant und Tag eine Bestellung mit gleichzeitigem Wareneingang angelegt und zugebucht wird. Als Menge wird die eingegebene Menge bzw. eben das Gewicht gebucht. Als Preis wird der Preis des Artikellieferanten verwendet.

Das Waagenterminal ist mit einem Touchschirm ausgestattet, sodass die Bedienung in einfacher Weise mittels Finger erfolgen kann.

Dem Terminal sollte aus Wartungsgründen eine fixe IP-Adresse zugeteilt werden.

Achten Sie bei der Montage des Terminals darauf, dass eine möglichst normale, senkrechte Sicht auf den Bildschirm des Terminals gegeben ist. Zur Befestigung an der Wand werden vier Montagelaschen mitgeliefert. Mit diesen Laschen muss auch die Erdung des Terminals durchgeführt werden. Wir empfehlen diese gemeinsam mit Ihrer Waage zu erden. Waagenterminal, Wiegeelektronik und Waage sollten ein gemeinsames Chassis Potiential haben.

## Einstellung des Waagenterminals

Um in die Einstellungen zu gelangen tippen Sie im Anmeldedialog auf das **Kieselstein ERP** Icon.

Dadurch wird die Versionsinformation des Waagenterminals angezeigt.

Durch Tipp auf OK gelangen Sie nach Eingabe des Zugangscodes in die Einstellungen des Terminals.

Der default Zugangscode lautet 123 (und dann der OK Button). Bitte ändern Sie diesen umgehend.

Im ersten Reiter (COM) sind die seriellen Schnittstellen für die Waage definiert. In der Standardausführung ist die Com2 für die Waage herausgeführt.

Ist auch der Port für den Barcodescanner ausgeführt, so ist dieser als Com1 zu definieren. Sollten keine EAN Codes gescannt werden, so bitte auf KEIN stellen.

Im zweiten Reiter wird die IP-Adresse Ihres **Kieselstein ERP** Servers, der Mandant und die Sprache definiert.

Im dritten Reiter (Allgemein) wird der Server für die Zeitsynchronisierung und die Einstellung des Kennwortes definiert.

Mit OK gelangen Sie wieder zurück in das Wiegeprogramm.

Mit Beenden gelangen Sie in das Betriebssystem.

Einstellung der IP-Adresse

Beachten Sie bitte, dass das Waagenterminal nicht offline fähig ist.

Erforderliche Parametrierungen im **Kieselstein ERP**

Die Definition welche Artikel auf dem Waagenterminal erfasst werden können, erfolgt in der Form, dass eine Haupt/Vaterartikelklasse z.B. Fleischerei definiert wird.

Vom Waagenterminal werden alle Artikelgruppe deren Vaterklasse der Definition des Waagenterminals entspricht (z.B. Fleischerei) angezeigt. Diese Anzeige ist auf maximal 20 Artikelgruppen begrenzt. Bitte bedenken Sie, dass die Buttons maximal 10 Zeichen in zwei Zeilen darstellen können. D.h. sowohl die Benennung der Artikelgruppen als auch der einzelnen Artikel sollte in zwei Wörtern, die obige Kriterien erfüllen, erfolgen, damit das Auslangen finden.

Fehlermeldung: Dem Gerät ist kein Kunde zugeordnet.

Das bedeutet für die IP-Adresse des Waagenterminals ist in den Arbeitsplatzparametern keine Kundenzuordnung vorgenommen. Die Kundenzuordnung wird für die Definition der Lieferadresse der Bestellungen benötigt.

Gehen Sie daher in System, Arbeitsplatzparameter und definieren Sie einen neuen Parametersatz für das Waagenterminial. 

Wichtig: Es muss bei PC-Name die IP-Adresse bzw. die Namensauflösung des Terminals angegeben werden. Wir empfehlen die IP-Adresse anzugeben.

Definieren Sie nun den Parameter: KUE_ESSENSAUSGABE_KUNDEID.

Dies ist die Kunden_ID des Partners aus der Kundentabelle. Diese teilt Ihnen Ihr **Kieselstein ERP** Betreuer gerne mit.
´´´bash
   select I_ID from PART_KUNDE where PARTNER_I_ID = (
      select I_ID from PART_PARTNER where C_NAME1NACHNAMEFIRMAZEILE1 like('%Anlief%')
   );
´´´
Die Lagerbuchung auf die die Zubuchung des Wareneingangs erfolgt, kommt aus dem Kunden.

Die ID der übergeordneten Vatergruppe muss je Terminal in den Arbeitsplatzparametern durch den Parameter KUE_VATERARTIKELGRUPPEID definiert werden. 

**Wichtig:** Am Tagesende sollte die Erfassung bis zur Lieferantenauswahl verlassen werden.

Besser noch, das Terminal täglich abschalten oder zumindest abmelden.

Für den Waagenuser sind keine Rechte, aber eine Rolle erforderlich.

## Bedienungsablauf

**Anmeldung am Terminal:**

Es steht hier der numerische Ziffernblock zur Verfügung. D.h. sowohl Benutzername als auch Passwort sollten numerisch eingegeben werden. Dieser Benutzer muss in der **Kieselstein ERP** Benutzerverwaltung angelegt sein. Damit ergibt sich auch die Funktionalität, dass hinter jeder Buchung die entsprechende Person hinterlegt ist, so wie in **Kieselstein ERP** üblich.

D.h. tippen Sie den Benutzernamen ein und tippen Sie dann auf den grünen Haken.

Nun geben Sie das Passwort ein und tippen auf anmelden. Sie sind nun angemeldet.

**Wählen Sie nun den gewünschten Lieferanten aus**

Die Liste der Lieferanten wird durch die Partnerklasse bestimmt. Die ID der Partnerklasse muss in den Arbeitsplatzparametern für das jeweilige Terminal unter KUE_PARTNERKLASSEID definiert werden.

Wählen Sie den Lieferanten durch antippen und durch nachfolgenden Fingertipp auf den grünen Haken.

Nun wählen Sie aus den angezeigten Artikelgruppen die passende Artikelgruppe aus.

Der gewählte Lieferant wird in der Titelzeile angezeigt. Stellen Sie nun fest dass ein anderer Lieferant gewählt werden sollte, so tippen Sie auf den blauen Pfeil nach links.

Die Lieferanten müssen auch Artikel haben.

Artikelgruppen und Partnerklassen werden nur aktualisiert beim Login in das Terminalprogramms

Nach der Auswahl der Artikelgruppe werden die Artikel der Artikelgruppen angezeigt.

Sind hier mehr als 18 Artikel definiert, so kann mit den Pfeil-Knöpfen geblättert werden.

Parallel wird das von der Waage ermittelte Gewicht angezeigt.

Durch tippen auf den entsprechenden Artikel werden die gewogenen Daten übernommen.

Nun springt das Terminal zurück in die Auswahl der Artikelgruppen.

Um wieder zur Auswahl des Lieferanten zurückzugelangen, tippen Sie auf den Rückwärtspfeil.

Um einen EAN Code einzulesen, tippen Sie bitte auf den Knopf EAN (wenn freigeschaltet) und Scannen Sie nun den EAN Code des jeweiligen Artikels ein.

Sollte anstelle des Gewichtes eine Menge manuell erfasst werden, so wählen Sie unten Menge, tippen die gewünschte Menge ein und schließen das numerische Feld mit dem Haken. Nun wählen Sie noch den Artikel aus, der zugebucht werden sollte.

## Anbindung der Waage:

Über serielle Sub-Min-D-Stift, 9polig, 9600, n,8,1 (Type: Avery Weigh.Tronix)

Abfrage des Gewichtes erfolgt mit Strg+E

Antwort der Waage: G xxxxx.xx kg

Die Daten der Waage werden vom Waagenterminal in der Anzeige der Artikelauswahl im 0,5 Sekundentakt (ca.) gepollt.