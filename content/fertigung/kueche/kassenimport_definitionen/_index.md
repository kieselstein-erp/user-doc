---
title: "Küche, Kassen"
linkTitle: "Kassen"
categories: ["Küche"]
tags: ["Kassen"]
weight: 20
description: >
  Importdefinitionen zu den Kassen für das Küchenmodul
---
Kassen Importdefinitionen
=========================

Es werden Daten der Kassentypen Oscar und ADS 3000 importiert

Importarten:

In der Partnerklasse wird die Art des Imports definiert, siehe Grunddaten.

Importart Shop ... erzeugt pro Tag und Kunde einen Lieferschein

Importart Essensausgabe ... legt nur Importdaten für Vergleich ab. Hier werden jeder Speiseplanartikel mit seinen Unterpositionen in die internen Importdatei abgelegt und steht somit für weitere Auswertungen zur Verfügung.

Das bedeutet beim Kunden muss auch die richtige Partnerklasse z.B. Shop hinterlegt werden.

Ist keine Importart, keine Partnerklasse definiert, so wird ein entsprechender Fehlereintrag angelegt.

**Wichtig:** Es muss für jeden Kunden die Partnerklasse mit der entsprechenden Importart definiert sein.

Shops und Essensausgaben sind in einer Datei und für das Format der Oscarkasse erstellt.

Die Daten des Gästehauses sind im Format ADS3000 erstellt.

Die Dateien werden je nach Kassentyp in den entsprechenden Pfaden (System, Parameter, PFAD_KASSENIMPORTDATEN_%) erwartet. Bei der Eingabe darauf achten, dass nur Forward-Slash verwendet werden. Derzeit ist dieser Pfad noch auf C: und nicht alle Sonderzeichen beschränkt (8.3).

Bei allen Feldern werden vor dem Import die führenden Nullen entfernt.

Die Übersetzung der in den Importdateien erhaltenen Artikelnummern wird in folgender Reihenfolge abgearbeitet:

a.) wird die Artikelnummer als Kassenartikel im Speiseplan gefunden, so wird dies als Rezept des Speiseplans interpretiert

b.) Nun wird auf die Umrechnung geprüft (Küche, Grunddaten, Umrechnung) und wenn gefunden der Übersetzte Artikel mit dem Umrechnungsfaktor verwendet. Wurde auch kein Umrechnungsartikel gefunden, so wird angenommen, dass dies ein Artikel ist.

c.) Artikelnummer. Hier wird automatisch erkannt ob dies ein einfacher Artikel oder ein Setartikel ist. Setartikel werden als Set in die Dateien eingetragen.

Der Import der beiden Dateien wird von der Artikelseite her völlig identisch durchgeführt.

Für die Durchführung des Imports stehen zwei unterschiedliche Funktionen zur Verfügung:

a.) Einzelimport

Hier kann, je nach Importtyp eine einzelne Datei für den Import ausgewählt werden.

Stellen Sie sicher dass der entsprechende Importpfad unter System, Parameter definiert ist.

OSCAR ... PFAD_KASSENIMPORTDATEN_OSCAR

ADS 3000 ... PFAD_KASSENIMPORTDATEN_ADS

**Hinweis:** Es werden alle Dateien im jeweiligen Verzeichnis eingelesen.

**Hinweis:** Die Pfadangaben sind aus Sicht des HELIUM V Servers zu erstellen.

b.) Alle Daten einlesen

Hier werden alle Daten beider Importpfade eingelesen.

Importformat OSCAR

Es wird für den Import die Tagesabschlussdatei verwendet. Diese ist wie folgt definiert:

| Feld | Länge | Bemerkung |
| --- |  --- |  --- |
| Zeitintervall | 1 | Wird nicht verwendet |
| Datum | 8 | JJJJMMTT |
| Mandant | 4 | Wird nicht verwendet |
| Haus | 5 | Wird nicht verwendet |
| Betriebsstelle | 4 | Anhand der Betriebsstelle wird über die Filialnummer der Kunde ermittelt |
| Artikel | 14 | Artikelnummer der Kasse -> Übersetzung siehe unten |
| Gesamtumsatz | 13 | inkl. 2 Nachkommastellen |
| Gesamtmenge | 12 | inkl. 3 Nachkommastellen |
| Umsatz Sonderpreis1 | 13 | inkl. 2 Nachkommastellen, wird nicht verwendet |
| Umsatz Sonderpreis2 | 13 | inkl. 2 Nachkommastellen, wird nicht verwendet |
| Umsatz Sonderpreis3 | 13 | inkl. 2 Nachkommastellen, wird nicht verwendet |
| Umsatz Sonderpreis4 | 13 | inkl. 2 Nachkommastellen, wird nicht verwendet |
| Umsatz Sonderpreis5 | 13 | inkl. 2 Nachkommastellen, wird nicht verwendet |
| Menge zu Sonderpr.1 | 12 | inkl. 3 Nachkommastellen, wird nicht verwendet |
| Menge zu Sonderpr.2 | 12 | inkl. 3 Nachkommastellen, wird nicht verwendet |
| Menge zu Sonderpr.3 | 12 | inkl. 3 Nachkommastellen, wird nicht verwendet |
| Menge zu Sonderpr.4 | 12 | inkl. 3 Nachkommastellen, wird nicht verwendet |
| Menge zu Sonderpr.5 | 12 | inkl. 3 Nachkommastellen, wird nicht verwendet |

Importformat ADS 3000

Auch hier wird für den Import die Tagesabschlussdatei verwendet. Diese ist wie folgt definiert:

| Feld | Länge | Bemerkung |
| --- |  --- |  --- |
| Kundennummer | 8 | Anhand der Kundennummer wird über die Filialnummer der Kunde ermittelt |
| Rechnungsnummer | 12 | Wird im Lieferschein unter Kommission eingetragen |
| Artikelnummer | 5 | Artikelnummer der Kasse -> Übersetzung siehe unten |
| Platzhalter | 3 | wird nicht verwendet |
| Artikelbezeichnung | 20 | wird nicht verwendet |
| Datum | 6 | JJMMTT |
| Vorzeichen | 1 | + / - |
| Menge | 7 | inkl. 2 Nachkommastellen |
| Preis | 7 | inkl. 2 Nachkommastellen |
| MwSt-Kennzeichen | 1 | wird nicht verwendet |
| Nummer | 5 | wird nicht verwendet |
| Bedienernummer | 2 | wird auf die zu verwendende Preislistennummer übersetzt. Dazu muss im Artikel, Grunddaten, Preislisten, je Preisliste die entsprechende Fremdsystemnummer definiert sein.Es wird dafür ausschließlich der unter Fixpreis hinterlegte Preis verwendet.Siehe dazu auch [Besonderheit Bedienerlager](#Besonderheit Bedienerlager). |
| Platzhalter | 4 | wird nicht verwendet |

#### Auflösung der aus den Kassendateien erhaltenen Artikelnummern
Die in den Kassendateien enthaltenen Artikelnummern werden in folgender Reihenfolge aufgelöst. Bitte beachten Sie, dass bei bei allen numerischen Feldern die führenden Nullen entfernt werden.
-   Suche Artikelnummer im Speiseplan,
-   Suche Artikelnummer in der Umrechnung (Küche, Grunddaten, Umrechnung
-   Suche Artikelnummer in Artikel
    hier wird ein eventuell definierter Setartikel automatisch erkannt und entsprechend behandelt.
-   Wurde die Artikelnummer nicht gefunden, so wird ein Fehlereintrag erstellt.

Wurde die Artikelnummer gefunden, so wird versucht diese vom entsprechenden Lager abzubuchen. Konnte die Buchung durchgeführt werden, so wird dies ebenfalls protokolliert ansonsten wird ebenfalls ein Fehlereintrag erstellt.

Da beim Import in beiden Arten (Shop und Essensausgabe) das Datum der jeweiligen Buchungen übergeben werden, werden diese als Datum für das jeweilige Tageslos bzw. den Lieferschein verwendet.

<a name="Besonderheit Bedienerlager"></a>

#### Besonderheit Bedienerlager
In der Praxis gibt es die Situation, dass verschiedene Bediener beim Import der Daten im ADS3000 Format, von anderen Lagern als im Kunden definiert abbuchen sollten.

Dies kann im Küchenmodul unterer Modulreiter Grunddaten, oberer Reiter Bedienerlager definiert werden.

Geben Sie hier die Bedienernummer an und das Lager von dem, entgegen der Regel, dass der Kunde das Lager definiert, abgebucht werden soll.