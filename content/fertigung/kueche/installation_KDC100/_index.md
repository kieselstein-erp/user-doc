---
title: "Küche"
linkTitle: "KDC100"
categories: ["KDC100"]
tags: ["KDC100"]
weight: 90
description: >
  Installatiion KDC100
---
Installation KDC 100
====================

Die Barcode Data-Sampler KDC 100 werden zur Erfassung von Barcodes verwendet. Die Stifte haben den Vorteil, eine Laser-Barcode-Leseeinheit zu besitzen und andererseits völlig ohne Benutzerführung auszukommen. Zusätzlich sind sie mit einer Echtzeituhr und einer eindeutigen Seriennummer ausgestattet. Damit kann der Stift und der Zeitpunkt der Lesung identifiziert werden.

Für die Stifte steht derzeit eine USB Schnittstelle für Windows XP zur Verfügung.

Damit die Stifte von **Kieselstein ERP** richtig erkannt werden, müssen diese von Ihrem **Kieselstein ERP** Lieferanten freigeschaltet werden.

Um die Uhrzeit am Stift mit der **Kieselstein ERP** Systemzeit möglichst synchron zu halten, werden die Stifte bei jeder Lesung mit dem PC synchronisiert.

Wird ein Stift zum ersten Mal in den jeweiligen Rechner eingesteckt, so erscheint der Hardwareinstallationsassistent.

**Hinweis:** Sind die Stifte ganz neu, bzw. sehr tief entleert, so kann es 1-2Minuten dauern, bis der Hardware-Installationsassisten erscheint.

**WICHTIG:** Entfernen Sie bitte unbedingt die Schutzfolie vor dem Fenster des Laserstrahles.

![](Installation_KDC100_1.gif)

![](Installation_KDC100_2.jpg)

![](Installation_KDC100_3.gif)

Die erforderliche Datei finden Sie auf Ihrer **Kieselstein ERP** Installations-CD im Verzeichnis Tools\KDC100

Den Windows Logo Test mit Fortsetzen beantworten

![](Installation_KDC100_4.jpg)

Nun beenden Sie die Hardwareinstallation mit Fertigstellen.

Wir empfehlen, alle verfügbaren USB Ports zu parametrieren, damit ein einfacher Betrieb möglich ist. Wird ein USB Hub verwendet, so sollten auch hier alle Ports definiert werden.