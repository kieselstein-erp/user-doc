---
title: "Reklamation"
linkTitle: "Reklamation"
categories: ["Reklamation"]
tags: ["Reklamation", "8D-Report"]
weight: 300
description: >
  Kunden, Lieferanten, Fertigungsreklamation, 8D-Report, Fehler und Maßnahmen und Auswertungen
---
Reklamation
===========

In **Kieselstein ERP** steht Ihnen mit dem Modul Reklamation ![](Reklamation.jpg) ein praktisches Werkzeug zur Verwaltung Ihrer Reklamationen zur Verfügung.

Mit der Reklamationsverwaltung sollten die Reklamationskosten, Maßnahmen usw. erfasst werden.
Eine Reklamation kann sich auf eine Bestellung (=Wareneingang), auf eine eigene Lieferung (LS oder RE) oder auf einen Fertigungsauftrag beziehen.
Die Reklamation beinhaltet Ursachen, Maßnahmen, Aktionen, Kosten.
Aus der statistischen Auswertung ergeben sich Wirksamkeiten der Maßnahmen und damit qualitätsrelevante Erkenntnisse. Diese sind wiederum im QS-Audit (ISO9000) auschlaggebend. Was wurde seit dem letzten Audit unternommen um die Qualität im Unternehmen zu verbessern.

Das Reklamationsmodul wurde an den sogenannten 8D Report angelehnt, welcher der strukturierten Qualitätsverbesserung der (eigenen) Fertigung dient.

Es können folgende Daten erfasst werden:
-   Reklamation Nr
-   Datum
-   Auslöser (Kunde, Lieferant, Fertigung)
-   Partner, Ansprechpartner, Telefonnummer (C 25) unter der der Reklamierende erreichbar ist.
-   Kostenstelle
-   Aufgenommen durch Mitarbeiter
-   Aufgenommen durch Telefon, Brief, Fax, ....
-   Bezug zu Bewegungsmodul, LS, Re, BE (WE-Pos), Los, Reparatur, ...
-   Reklamationsgegenstand Artikel oder Handeingabe. Bei Handeingabe kann auch die Bezeichnung angegeben werden
-   Menge die Reklamiert wird<br>
Das könnte auch ein Fremdprodukt sein (wird zwar bei uns reklamiert, kommt aber von einem anderen Lieferanten, wir sollten das aber trotzdem behandeln)
-   Schwere der Reklamation die Auswahlmöglichkeiten sind in den Grunddaten definierbar (1 ... leichter Mangel, 5 ... Grob Fahrlässig)
-   Grund der Reklamation. Die Gründe sind in den Grunddaten definierbar
-   Fehler Angabe (des Partners)
-   Analyse, d.h. Eingabe einer Beschreibung der Fehleranalyse in einem Kommentarfeld.
-   Weiterleiten an: D.h. eine Liste (definierbar) an Checkboxen an die die Reklamation weitergeleitet wird (als Info) hinter diesen Checkboxen liegen wieder EMailadressen, das könnten auch Personal_IDs sein, an die die Info dass es eine Reklamation gibt weitergeleitet/gesendet wird. Z.B. muss die Buchhaltung informiert werden, dass eine Rechnung nicht bezahlt werden darf, da es da vorher noch etwas zu klären gibt.
-   Funktion EMail Versand der Reklamation an die ausgewählten Weiterleitungen
-   Fehlerursache Die Fehlerursachen können ebenfalls in den Grunddaten definiert werden.
-   Ist die Reklamation berechtigt Ja/Nein
-   Rücksprache dient für ...<br>
Rücksprache durch (Mitarbeiter=Personal)<br>
Rücksprache erfolgt(e) am Datum mit Textfeld für die Eingabe des Namens der Person mit der gesprochen wurde
-   (welche) (Sofort) Maßnahme(n) wurde(n) gesetzt, Auswahlliste anhand der Grunddaten.<br>
Maßnahme ist bis zum (Datum) zu erledigen<br>
Maßnahme wurde erledigt am / von
-   Aufwandserfassung für die Reklamation<br>
Kosten für Arbeitszeit
Eingabe als Wert oder in Stunden
Kosten für den Materialaufwand
- Korrektur<br>
(welche) Korrekturmaßnahme(n) wurde getroffen um den Fehler in Zukunft zu vermeiden.
Textfeld mit Beschreibung zusätzlich angebbar welche Korrekturmaßnahmenart dies ist, also
    - Kurzfristig
    - Mittelfristig
    - Weiterführende Maßnahme
    Korrekturmaßnahme(n) werden von
    bis zum umgesetzt
    Nachweis der Wirksamkeit der Korrekturmaßnahme(n) erbracht (Text) und damit am von abgeschlossen.
-   Langtextkommentar zur Reklamation
-   Ausgleich der Reklamationskosten durch<br>
Nachbesserung bzw. Gutschrift. Auch diese Einstellungen können in den Grunddaten ergänzt werden.
-   Erfassung der Reklamationsbehandlung durch den Lieferanten.<br>
Auswahlmöglichkeit aus vorbildlich, zufriedenstellend, schleppend, ungenügend.
-   Dokumentenzuordnung zur Reklamation, z.B. Bilder des defekten Artikels usw.


Die Reklamationen gliedern sich in drei Bereiche:
1.  Kundenreklamationen
2.  Lieferantenreklamationen
3.  Fertigungsreklamationen

### Kundenreklamationen
Erfassen Sie hier die Reklamationen Ihrer Kunden mit Bezug auf den Kunden und damit auch auf Rechnung, Lieferschein bis hin zum fertigenden Los und dem Mitarbeiter der es auf einer bestimmten Maschine produziert hat.
Da Kundenreklamationen üblicherweise ihre Ursache in Lieferanten oder Fertigungsfehlern haben, kann bei der Kundenreklamation zusätzlich der Auslöser mit seinen Daten eingegeben werden.
Wählen Sie dazu zwischen ![](Fehler_aus_Fertigung_oder_Lieferant.gif) Fertigung und Lieferant aus.
Je nach Ihrer Auswahl werden die nachfolgenden Felder entsprechend gesteuert, sodass die Fehlerdaten in einer Reklamation erfasst werden können.

### Lieferantenreklamationen
Hier werden Ihre Reklamationen an Ihre Lieferanten erfasst.

### Fertigungsreklamationen
Erfassen Sie die internen Reklamationen zur stetigen Verbesserung Ihrer Qualität.

### Können bei einer Reklamation Dokumente hinterlegt werden?
Ja. Entweder Sie verwenden die interne Dokumentenablage. Verwenden Sie dazu das Symbol  [Dokumentenablage]( {{<relref "/docs/stammdaten/system/dokumentenablage" >}} ) in der Auswahlliste der Dokumente oder hinterlegen Sie Dateien über den [Dokumentenlink](../System/Dokumentenablage.htm#Dokumentenlink). Zusätzlich können in der Reklamation im Reiter 5 Bilder, die Möglichkeit Bilder zu dieser Reklamation zu hinterlegen. Diese werden beim Druck des 8D Reportes auch mit ausgedruckt.

### Anzeige der reklamierten Artikel im Artikelstamm
<a name="Reklamationen am Artikel"></a>
Ist ein Artikel in einer offenen Reklamation enthalten, so wird dies durch das Symbol Reklamation ![](Artikel_Reklamationen.gif) in der Artikelauswahlliste angezeigt. Zugleich wird bei Verwendung des Artikels ein Hinweis ausgegeben, dass auf dem Artikel eine offene Reklamation eingetragen ist.
Eine Übersicht der offenen Reklamationen eines Artikels erhalten Sie im Artikel unter Info, Reklamationen

### Seriennummern
In der Reklamation können nun auch Serien- bzw. Chargennummern ausgewählt werden.
Die einfachste Vorgehensweise für die Definition von Kunden, Lieferschein usw. ist, direkt die Seriennummer auszuwählen. Daraus ergibt sich der Rest. Bitte beachten Sie, dass die in der Auswahl angezeigten Seriennummern passen zur Art der Reklamation eingeschränkt wird. D.h. bei Kundenreklamationen werden nur Seriennummer bzw. Chargennummern angezeigt, die auf Lieferscheine oder Rechnungen gingen, bei Lieferantenreklamationen nur Wareneingänge und bei Fertigungsreklamationen nur Bewegungen die auf Lose gingen.
Die Anzeige der Historie der Serien-/Chargennummern wird im Seriennummernjournal ebenfalls unterstützt.
Hier werden nur für diejenigen Serien-/Chargennummern die zugehörigen Reklamationen angezeigt, welche auch in Ihrer Warenbewegungsliste enthalten sind. Da die Erfassung der Seriennummer auch für nicht von Ihnen bewirtschaftete Seriennummern erfolgen kann, durch die Handeingabe, könnte es vorkommen, dass Sie für Seriennummern Reklamationen erfasst haben, die im Artikel-Seriennummernjournal dadurch nicht angezeigt werden.

## Kurze Beschreibung der in den Grunddaten einzustellenden Tabellen für die Katalogisierung
![](Rekla_Grunddaten.gif)

### Fehler
Definieren Sie hier, welche Fehler Sie als Ursache für eine Reklamation angeben können wollen.
Hier geht es darum, dass Fehler und Maßnahmen katalogisiert werden. Nur durch die Katalogisierung können Aussagen darüber gemacht werden, wie sich Ihre Qualität entwickelt. In welchen Bereichen Handlungsbedarf besteht.
Aus der Praxis hat sich gezeigt, dass es unmöglich ist, auf Null Fehler zu kommen. Aber die Fehler um die Hälfte, also 50% zu reduzieren gelingt immer wieder.
![](Rekla_Grunddaten1.gif)
Diese Fehler werden auch am Zeiterfassungsterminal bei der Angabe von Schlecht-Stück als Angabe zur Verfügunggestellt.

### Fehlerangabe
Welche Fehlerangaben in Form von standardisierten Angaben, welche für eine Auswertung sinnvoll sind, kommen von Ihren Kunden.

![](Rekla_Grunddaten2.gif)

### Maßnahme
Welche standardisierte Maßnahme setzen Sie um den Fehler in Zukunft abzustellen

![](Rekla_Grunddaten3.gif)

### Aufnahmeart
Wie wurde der Fehler entgegengenommen / von Ihnen / Ihren Mitarbeitern erfasst

![](Rekla_Grunddaten4.gif)

### Schwere

Wie schwer ist / wiegt der Fehler.

Kann der Artikel noch verwendet werden oder ist er einfach Schrott

![](Rekla_Grunddaten5.jpg)

Die angegebenen Punkte gehen in die Formel der Lieferantenbeurteilung ein. Wichtig ist:

Für die geringste Schwere, also die beste Lieferung gibt es die höchsten Punkte.

Die höchsten / maximalen Punkte sind 30\. Siehe dazu auch [Formel](#Formel).

### Behandlung
Wie ist die Reklamationsbehandlung des Lieferanten, oder auch Ihre eigene. D.h. kümmert sich Ihr Lieferant darum seinen Fehler so rasch wie irgend möglich auszubessern oder ist ihm dies schlichtweg egal.

![](Rekla_Grunddaten6.jpg)

Auch hier gehen die vergebenen Punkte in die Lieferantenbeurteilung ein.

D.h. für die beste Behandlung Ihrer Reklamation, ("Setzt Himmel und Hölle in Bewegung um den Fehler, woher er auch immer kommt, auszubessern"), gibt es die höchsten Punkte. Siehe auch [Formel](#Formel).

In der Reklamation erfolgt beim manuellen Erledigen die Abfrage, wie die Behandlung erfolgt ist, Sie können hier die in den Grunddaten definierte Art auswählen.

### Termintreue Auswirkung
Wie wirken sich Lieferungen zu falschen Terminen auf die Beurteilung Ihrer Lieferanten aus.

[Siehe unten](#Termintreue) und siehe bitte auch [Formel](#Formel). Auch hier: Für die beste Termintreue gibt es die höchsten Punkte.

### Wirksamkeit
Welche Wirksamkeit können Sie bei Ihren Fehlervermeidungs Maßnahmen feststellen.

![](Rekla_Grunddaten7.jpg)

## Lieferantenliefertermintreue
Zur Beurteilung der Lieferantentermintreue steht Ihnen unter Journal, Lieferantentermintreue eine Auswertung der Liefertermine Ihrer Lieferanten zur Verfügung.

Sie erhalten im wesentlichen eine Gegenüberstellung der bestätigten Liefertermine zu den tatsächlichen Wareneingangsterminen Ihrer Lieferantenlieferungen.

![](Reklamation1.gif)

In dieser Auswertung ersehen Sie welche Lieferungen Ihres Lieferanten mit wie vielen Tagen Verspätung geliefert wurden. Bei den Verspätungstagen werden nur Arbeitstage, also keine Sams-, Sonn- und Feiertag, berücksichtigt.

## Lieferbeurteilung
In Erweiterung der Reklamationsverwaltung kann auch eine annähernd automatische Beurteilung der Lieferanten gemacht werden. Es müssen dazu die Punkteschemen für Liefertermintreue, Schwere der Reklamation und Art der Reklamationsbehandlung hinterlegt werden.

Die Definition der Schwere wurde für die standard Reklamation bereits beschrieben. Hier sind nun zusätzlich noch Reklamationsbehandlung und Termintreue zu definieren.
### Reklamationsbehandlung
Definieren Sie in den Grunddaten, unter Behandlung, wieviele Punkte Ihr Lieferant je Lieferung für die Art und Weise seiner Reklamationsbehandlung erhält.
### Termintreue
Definieren Sie in den Grunddaten, unter Termintreue, welche Punkteanzahl der Lieferant für die Terminerfüllung bzw. Über/Unterschreitung bekommt.<br>
![](Termintreue.gif)<br>
Obige Tabelle bedeutet, dass bei einer zu frühen Lieferung ab dem 15.Tag der Lieferant für die Termintreue keine Punkte mehr bekommt. Aufgrund der Formel ergibt dies keine Punkte für diesen Wareneingang.<br>
Für eine termingerechte Lieferung erhält der Wareneingang die höchste mögliche Punkteanzahl von 30Punkten, wobei diese für den Zeitraum einer Woche (in Kalendertagen) rund um den gewünschten Liefertermin definiert ist.<br>
Die Basis für den Lieferverzug ist der Solltermin. Dieser wird je Wareneingangsposition wie folgt errechnet:
- Ist ein Ursprungs-AB-Termin definiert, so wird dieser verwendet, ansonsten:
- Ist ein AB-Termin definiert, so wird dieser verwendet, ansonsten:
- Ist in der Bestellposition ein Positionstermin definiert, so wird dieser verwendet, ansonsten:
- wird der Termin aus den Kopfdaten verwendet
und die abweichenden Tage der Lieferung aus dem gefundene Solltermin mit dem Wareneingangsdatum des jeweiligen Wareneingangs errechnet. [Siehe dazu auch](../Bestellung/index.htm#bestätigter Liefertermin).

### Formel
Die Formel ist derzeit fest programmiert und lautet:
Termintreue x (Schwere der Reklamation + Reklamationsbehandlung)

Ist für einen Wareneingang keine Reklamation angegeben, so wird für Schwere und Behandlung die jeweils höchste definierte Punkteanzahl verwendet.<br>
Beachten Sie, dass um die unten angeführten Grenzwerte überhaupt erreichen zu können, für jeden Wert der Formel der Maximalwert der Punkte von 30 definiert werden muss.

### Klassen
Aus der Formel ergeben sich nun auch die maximal erzielbaren 1.800 Punkte.

Diese können in System, Parameter unter LIEFERANTENBEURTEILUNG_PUNKTE_KLASSE_A, B, C definiert werden. Sie sind üblicherweise auf A ... 1600, B ... 1200, C ... 800 eingestellt.

### Durchführen der Lieferantenbeurteilung
Im Journal finden Sie den zusätzlichen Punkt ![](Lieferantenbeurteilung.gif) Lieferantenbeurteilung. Dieser ist als eigenständiges Modul ausgeführt. Sollte dieser in Ihrer **Kieselstein ERP** Installation nicht freigeschaltet sein, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.

Wählen Sie hier den Betrachtungszeitraum, üblicherweise ein Quartal.

Das Ergebnis dieser Beurteilung wird zum Ende-Zeitpunkt in die Klassifizierung des jeweiligen Lieferanten eingetragen.

### Änderung der Lieferantenklassifizierungen
Es ergibt sich unter Umständen, dass Klassifizierungen von Lieferanten abgeändert werden müssen.

Dies kann in den Beurteilungen des einzelnen Lieferanten durchgeführt werden.

Die Beurteilungen eines Lieferanten finden Sie im Modul Lieferanten im oberen Modulreiter Beurteilung.

![](Lieferantenklassifizierung.gif)

Mit ändern können die Punkte einer vorhandene Klassifizierungen abgeändert werden, wodurch sich auch eine andere Klasse ergibt. Damit bei einer eventuellen Neuberechnung die von Ihnen eingetragene Beurteilung nicht überschrieben wird, muss diese auf Gesperrt (Eingefroren) gesetzt werden.

Wenn Sie die Klassifizierung abändern, sollte auch die Begründung dafür im Feld Kommentar angegeben werden.

### Ausgewählte Lieferanten nicht beurteilen.
Manche Lieferanten sollten / brauchen nicht beurteilt werden, da sie z.B. keine qualitätsrelevanten Artikel liefern. Diese Lieferanten werden bei der Durchführung der Lieferantenbeurteilung übersprungen.

Die Einstellung dafür finden Sie in den Konditionen des jeweiligen Lieferanten. Hier wird auch die aktuelle Beurteilung angezeigt und die daraus resultierende Klasse.

### Reklamationsbehandlung eingeben, Reklamation abschließen
Ist eine Reklamation abgeschlossen, so muss diese mit Bearbeiten, Manuell erledigen abgeschlossen / erledigt werden. Geben Sie hier noch zusätzlich die Art der Reklamationsbehandlung mit an.

![](Reklamation2.jpg)

Ihre Beurteilung der Reklamationsbehandlung sehen Sie unter Detail. Die Art der Behandlung hat wie oben beschrieben Punkte hinterlegt, was sich wiederum in der Lieferantenklassifizierung auswirkt.

### Richtige Behandlung von Kunden Reklamationskosten
Werden von Kunden Reklamationskosten verrechnet, so sind diese als Gutschrift zu erfassen. [Siehe.](../Rechnung/Gutschrift.htm#Reklamationskosten)

### Auswertung der Fehlerarten in der Reklamationsverwaltung
Mit dieser Auswertung haben Sie eine gute Übersicht über den aktuellen Stand der Fehler Ihrer Produktion.

Vor allem auch durch die Auswertung nach Fehlerarten, Maschinen und oder Verursache können sehr einfach steuernde Maßnahmen ergriffen werden um die Qualität entsprechend zu steigern.

![](Fehlerarten.jpg)

Nutzen Sie auch die Auswertungen nach Maschinengruppen bzw. Verursacher bzw. der Kombination daraus:

![](Fehlerarten_Maschinen.jpg)

#### Es lassen sich nur wenige Maßnahme erfassen
Es gibt im 8D-Report aus fachlichen Gründen nur drei Maßnahmen.
Kurz, mittel, langfristig. Also was machen Sie um sofort den Fehler abzustellen, und dass das "nie mehr" passiert (langfristig). Dazu gibt es je Maßnahme zwei Felder: die datentechnische Maßnahme, welche in den Grunddaten zu definieren ist und der Klartext mit dazu.

#### Wieso hat der 8D Report nur drei Maßnahmen
**8D** steht für die **acht** obligatorischen **Disziplinen** (Prozessschritte), die bei der Abarbeitung einer Reklamation erforderlich sind, um das zu Grunde liegende Problem zu überwinden. ([Siehe](http://de.wikipedia.org/wiki/8D-Report))

Diese sind:
| Disziplin | Beschreibung des Prozessschrittes |
| --- | --- |
| D1 | Zusammenstellen eines Teams für die Problemlösung |
| D2 | Problembeschreibung |
| D3 | Sofortmaßnahmen festlegen |
| D4 | Fehlerursache(n) feststellen |
| D5 | Planen von Abstellmaßnahmen |
| D6 | Einführen der Abstellmaßnahmen |
| D7 | Fehlerwiederholung verhindern |
| D8 | Würdigen der Teamleistung |

Diese acht Punkte finden Sie direkt auf dem Druck der Reklamation. Damit ergibt sich zwangsläufig, dass nur drei Maßnahmen für die weitere Fehlervermeidung erforderlich sind.

#### Ein Lieferant erscheint nicht in der Lieferbeurteilung
Damit ein Lieferant auch tatsächlich beurteilt wird, muss dies bei ihm auch aktiviert sein.

Bitte prüfen Sie, ob im Lieferanten, Konditionen Beurteilen ![](Lieferanten_Beurteilen.gif) angehakt ist.

#### Werden von den Punkten aus der Liefergenauigkeit bei Vorliegen von Reklamationen diese "Reklamationspunkte" in Abzug gebracht?
Bitte beachten Sie die Formel.
Wichtig ist, dass das beste Produkt die höchste Punktezahl erhalten muss.
Zusätzliche Info: Damit die Formel funktionieren kann, muss es für Termintreue, Schwere der Reklamation und Reklamationsbehandlung immer auch einen Eintrag für maximale Punkte geben.

#### Kann eine Reklamation auch für mehrere Artikel angelegt werden?
Das Thema bei der Reklamation ist, dass man durchaus von einem Kunden mehrere gleiche Artikel zurück bekommt, aber: Die Analyse ergibt oft unterschiedliches.
Daher kann eine Reklamation nur für einen Artikel und damit oft auch nur für eine bestimmte Seriennummer sein.
Daher haben wir das so gelöst, dass eine Reklamation durch Klick auf Reklamation kopieren ![](Reklamation_kopieren.gif) in der Auswahl der Reklamationen einfach kopiert werden kann. Damit können sehr rasch neue Reklamationen für weitere Artikel für den gleichen Fall angelegt werden und trotzdem haben Sie die Möglichkeit jeden Artikel sehr individuell zu behandeln.

#### Kann man aus einer Wareneingangsposition gleich eine Reklamation anlegen ?
Ja. Für Lieferantenreklamationen kann direkt aus der einzelnen Wareneingangsposition heraus eine Reklamation, ebenfalls durch Klick auf den Reklamationsbutton ![](Reklamation_kopieren.gif) erzeugt werden.

#### Was bewirken die Parameter?
| Parameter | Beschreibung |
| --- |  --- |
| REKLAMATION_BELEGNUMMERSTARTWERT | definiert den Wert mit dem beim Geschäftsjahreswechsel bei einer neuen Reklamation begonnen wird |
| KUNDENREKLAMATION_DEFAULT | definiert die Unterart der Reklamation, wenn als Reklamationsart KUNDE ausgewählt wurde |
| LIEFERANTENREKLAMATION_BESTELLUNG_MANUELL | es können neben der Auswahl aus Bestellung bzw. Wareneingang auch manuelle Infos zu Bestellung Wareneingang eingegeben werden. Wird gerne für Speditionsbestellungen verwendet |
| BESTELLUNG_UND_WARENEINGANG_SIND_PFLICHTFELDER | ist dies abgeschaltet, so kann eine Lieferantenreklamation auch ohne Angabe dieser Felder angelegt werden |

#### Kann die Lieferantenbeurteilung auch dokumentiert werden?
Es kann die Beurteilung jedes Lieferanten auch in der Dokumentenablage des jeweiligen Lieferanten unter dem Knoten Beurteilung abgelegt werden.
D.h. durch den Klick auf ![](Rekla_Aktivieren.gif) Aktivieren und in Dokumentenablage ablegen, wird die Beurteilung erneut durchgerechnet, die nun errechneten Punkte in der Beurteilung des Lieferanten abgelegt und das Dokument in der Dokumentenablage des Lieferanten im Knoten Beurteilung hinterlegt.<br>
Um die Beurteilung auch an Ihre Lieferanten zu senden, verwenden Sie zusätzlich den Button ![](Rekla_Email_senden.gif) als EMail versenden.

#### Wie erfasst man die Reklamationsnummern der Kunden / Lieferanten?
Sie finden in den Kopfdaten der Reklamation die Felder LfReklaNr bzw. LfLsNr respektive KdReklaNr bzw. KdLsNr.
Dies steht gleichbedeutend für Lieferanten-Reklamations-Nummer und Lieferanten-Lieferschein-Nummer bei einer Lieferantenreklamation respektive Kunden-Reklamations-Nummer bzw. Kunden-Reklamations-Lieferschein-Nummer. Beide Felder dienen der Organisation und Dokumentation.
Damit Sie die dazugehörende Reklamation auch wieder finden, stehen in der Auswahlliste der Reklamationen die Direktfilter zur Verfügung.

## Lieferantenbeurteilung um Softskills ergänzen
**Kieselstein ERP** Anwender die die Lieferantenbeurteilung sehr ernst nehmen, müssen diese um sogenannte Softskills, wie z.B. Qualität der Kommunikation, wie ist der Lieferant technisch aufgestellt, hat er selbst ein QS-System u.v.a.m. ergänzen. Da diese Daten
- Anwender individuell
- Lieferanten individuell
sind, haben wir diese in die Lieferanteneigenschaften ausgelagert.
D.h. in den Lieferanteneigenschaften werden die Werte / Parameter als auch die Formeln und deren Interpretation definiert.
Dies hat den Vorteil, dass sowohl Parameter als auch Formel vom Inhalt her vom Anwender jederzeit ergänzt und angepasst werden können.
Hier hat sich auch eine Denke in 100Punkten bzw. 100% für die Gesamtzahl an möglichen Werten als leichter anwenderbar gezeigt.
D.h. es sind zusätzlich zu den Softskills auch die Punkte für Termintreue, Reklamationsschwere und Reklamationsbehandlung auf ein Maximum von 100Punkten auszurichten.

| Parameter | Wert / Max Punkte | Gewichtung Typ EMS | Gewichtung Typ Distributor |
| --- |  --- |  --- |  --- |
| Kommunikation | 100 | 5 | 20 |
| Technologie | 100 | 60 | 10 |
| QS System | 100 | 35 | 70 |
| Gesamt |   | 100 | 100 |

Das bedeutet, dass es z.B. für die Kommunikation maximal 100Punkte ([%]) gibt. Diese fließen in das Gesamtergebnis für Ihren Lieferanten des Typs EMS (Elektronic Manufactoring Services) mit 5% ein. Damit ergibt sich auch, dass die Gewichtung in Summe immer 100% sein muss.

Wir haben dies wie folgt in den Lieferantenbeurteilungsformularen für Sie implementiert. In der Regel wird eine kurze Bedienungseinweisung durch einen **Kieselstein ERP** Betreuer erforderlich sein.
- a.) es werden in den Lieferanteneigenschaften der Typ und die Parameter definiert
- b.) der Typ definiert mit seinem Wert (aus der Combobox) auch wie die Verteilung der Softskill-Parameter erfolgt
- c.) Je Typ muss ein Lable und ein Wert (als Checkbox) angelegt werden
- d.) bei der Checkbox muss als Druckname der gewählte Typ eingetragen werden
- e.) Die Verbindung von der Verteilung zum Parameter sind die ersten beiden Buchstaben des Drucknames des Parameters. D.h. wenn diese übereinstimmen wird dieser Faktor als Verteilungsfaktor gerechnet. Die Trennung zwischen den Faktoren ist der | Vertical Bar. Bitte beachten Sie die Groß-Klein-Schreibung.
Beispiel für die Gewichtung Typ EMS: Ko05|Te60|QS35

Nachfolgend ein möglicher Ausdruck der Lieferantenbeurteilung.

![](Beurteilung_anhand_Lieferanteneigenschaften.gif)

Dazu die Einstellungen in den Lieferanteneigenschaften
![](Lieferanteneigenschaften1.jpg)

Mit der Verbindung zu den Gewichtungen:
![](Lieferanteneigenschaften2.jpg)

Bitte beachten Sie, dass für eine kurze Übersichtsinformation nur der Haken bei verdichtet gesetzt werden muss.