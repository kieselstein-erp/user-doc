---
title: "Was ist zu tun"
linkTitle: "Was ist zu tun"
weight: 1100
description: >
  Was ist zu tun, was sollte als nächstes gefertigt werden?
---
Was sollte als nächstes gefertigt werden?
=========================================

Nachdem immer wieder die Frage an uns herangetragen wird, wie man erkennen kann, was als nächstes zu fertigen ist, hier eine Zusammenfassung der dafür erforderlichen Punkte / Maßnahmen. Bzw. was wir in der Praxis bisher umsetzen durften.

## Die Frage / der Wunsch
Wie zeige ich meinem Mitarbeiter was er als nächstes fräsen, drehen, hobeln usw. sollte

## Wie zeigt man das in der Fertigung an?
Das einfachste ist, wenn ein Rechner in die Produktion gestellt wird, an dem z.B. ein bis zwei Bildschirme angeschlossen sind.
Diese Bildschirme hängen z.B. Rücken an Rücken mitten in der Fertigungshalle, möglichst so, dass sie von allen gut einsehbar sind.
Denken Sie daran dass ev. zwei Bildschirme mit guten HDMI Kabeln mit dem Rechner verbunden billiger sind, als ein großer Bildschirm.

### Was sollte man dann in der Fertigung anzeigen
Bewährt hat sich aus dem Modul Lose
![](was_ist_zu_tun1.jpg)
den unteren Modulreiter offene AGs mit dem oberen Modulreiter offene AGs zu nutzen.
Hier finden Sie alle Arbeitsgänge die noch nicht fertig sind, bzw. deren Lose nicht vollständig erledigt (bzw. storniert) sind.
Sortieren Sie diese Liste nach Arbeitsgang Beginn.
In der Combobox oben wählen Sie die einzelne Maschine oder auch die Maschinengruppe aus, welche betrachtet werden sollte.
Für die Anmeldung an Ihrem **Kieselstein ERP** verwenden Sie z.B. einen Benutzer Fertigung, der nur ein lesendes Recht auf die Fertigung hat.

### Wie die Größe des Bildschirms ermitteln?
Stellen Sie die Liste der offenen AGs entsprechend Ihren Wünschen ein, üblicherweise wird das Bildschirmfüllend sein.
Drücken Sie F12, das bedeutet aktuelle **Kieselstein ERP** Ansicht ausdrucken.
Ermitteln Sie nun durch Vergrößern dieses Blattes, welche Größe der Bildschirm haben sollte. Denken Sie dabei daran, dass es nicht um die Auflösung des Schirms geht sondern um die physikalische Größe des Bildschirms. Hängen Sie dieses Blatt / die Blätter an die Stelle an der Sie den Bildschirm platzieren möchten. Gehen Sie an die weitest entfernte Stelle von der Sie das noch sehen können möchten.<br>
Nutzen Sie ev. auch eine größere Schriftgröße (Hauptmenü, Ansicht, Schriftart)
Vergrößern Sie das Blatt so weit, dass es gut lesbar ist.
Nun haben Sie ein Maß, eine Idee dafür, wo Sie Ihre(n) Bildschirm(e) platzieren müssen.
Gegebenenfalls sind mehrere Rechner z.B. je Maschinengruppe / Abteilung, mit jeweils mehreren Bildschirmen eine bessere Wahl.
Für die Betrachtung der Kosten denken Sie bitte an die Wegeoptimierung Ihrer Mitarbeiter.

Tipp: Nutzen Sie für die einfache Bedienung noch die Funktion Layout Speichern und Spalteneinstellungen speichern.

#### Voraussetzungen:
Es werden die Arbeitsgänge ordentlich geplant
Es werden die Arbeitsgänge und die Stückzahlen entsprechend rückgemeldet und es wird auch der FERTIG Status der Arbeitsgänge genutzt

Für die Maschinen gibt es einzelne Maschinen welche wiederum Maschinengruppen zugeordnet sind.
Ergänzend / Alternativ nutzen Sie die Fertigungsgruppen. Ob Maschinengruppen oder Fertigungsgruppen besser geeignet sind, hängt von der Art Ihres Produktionsbetriebes ab.

Tipp: Wenn Sie Rechner / PCs mitten im Produktionsbetrieb haben, hat sich bewährt, dass diese auf einen Fertigungsswitch laufen, welcher KEINE galvanische Verbindung zum restlichen Netzwerk hat. D.h. dass diese mit Lichtwellenleitern, zumindest einem kurzen Stück für die galvanische Trennung, angebunden sind.

Hintergrund: Je älter Ihre Maschinen sind, desto, mehr Störungen für Ihre IT produzieren diese. Haben Sie nun eine galvanische Verbindung, z.B. mit Netzwerkkabeln, streuen diese Störungen in Ihr Netzwerk und stören damit Ihre Server usw..