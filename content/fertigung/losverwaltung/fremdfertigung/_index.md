---
title: "Fremdfertigung"
linkTitle: "Fremdfertigung"
categories: ["Fremdfertigung"]
tags: ["Fremdfertigung"]
weight: 200
description: >
  Fremdfertigung, Verlängerte Werkbank, Lohnfertigung
---
Fremdfertigung
==============

In vielen Fertigungsbetrieben, produzierenden Unternehmen werden Artikel zur Veredelung, Lohnfertigung auch im Sinne der verlängerten Werkbank außer Haus gegeben.
Aus unserer Erfahrung hat sich gezeigt, dass der Ablauf in Metall verarbeitenden Betrieben und in Elektronik Fertigungsbetrieben zwar theoretisch gleich ist, in der Praxis aber, vor allem aufgrund der doch deutlich unterschiedlichen Fachlichkeiten sich deutliche Unterschiede in der Handhabung herauskristallisieren.

Ablauf in der Elektronik Fertigung

In der Elektronik Fertigung wird oft die reine Bestückung als Lohnbestückung außer Haus gegeben. Das Material wird vollständig oder teilweise beigestellt. Meist werden immer hochwertige Spezial Komponenten, vor allem spezielle Halbleiter beigestellt. Es wird dann der Auftrag an den Lohnbestücker gegeben, die Standardkomponenten, wie Widerstände, Kondensatoren, aus seinen Beständen zu nehmen und als Materialsatz zu verrechnen. Hier kommt mit dazu, dass aus technischen Gründen (Antistatic, MSL) die Artikel möglichst NICHT umverpackt werden sollten und es kommt weiters dazu, dass die modernen Bestückungsautomaten im Rüstvorgang bzw. bei der Messung der Bauteile eine vorher nicht exakt bekannte Anzahl an Bauteilen verwerfen. Das bedeutet, es macht keinen Sinn, exakt auf die gewünschte Losgröße abgezählte Bauteilemengen an den Lohnfertiger zu liefern, sondern man übergibt diesem ganze Verpackungseinheiten, wie z.B. SMD-Rollen (5000Stk) oder IC-Stangen (13Stk in der Stange). Meist rechnet man noch eine Übermenge von einigen Prozenten (auch gestaffelt nach Wertigkeiten der Bauteile) mit dazu. Daraus ergibt sich, dass Sie wissen müssen:
1. wieviel haben Sie Ihrem Lohnfertiger geliefert
2. wieviel darf er davon verbraucht haben, was wiederum bedeutet
3. Sie müssen wissen wieviel er von welchem Bauteil jetzt auf Lager hat

Dies bedingt eine Lagerbewirtschaftung Ihrer Bauteile (= Artikel) beim Lieferanten. D.h. Sie benötigen dafür je Lohnfertiger ein eigenes Lieferanten-Lager (Artikel, Grunddaten, Lager). Zusätzlich bewirtschaften Sie selbst den theoretischen Verbrauch der Bauteile aus dem Lieferanten-Lager.

Kurz zusammengefasst ist der Ablauf wie folgt:
1. Die Lose für die zu fertigenden Baugruppen sind angelegt. Als Abbuchungslager ist NUR das Lieferanten-Lager eingetragen.
2. Sie beschaffen die Bauteile und buchen diese über den üblichen Wareneingang in Ihr Hauptlager ein.
3. mit der theoretischen Fehlmengen Liste erstellen Sie eine Liste der Bauteil-Bedarfe für Ihren Lieferanten, genauer für das Lieferanten-Lager
4. Korrigieren Sie diese Mengen auf Verpackungseinheiten / lagernde Mengen
5. Erstellen Sie nun einen Lieferantenlieferschein mit Ziellager. Das Ziellager ist das Lieferanten-Lager. Damit werden die Bauteile vom Hauptlager ins Lieferanten-Lager umgebucht.
6. Senden Sie dem Lieferanten die Bauteile mit diesem Lieferschein und den weiteren benötigten Fertigungsunterlagen (Bestückungspläne, Montageanweisungen usw.)
7. Der Lieferant fertigt für Sie die Baugruppen und liefert diese in der bestellten Losmenge oder mit Teilmengen
8. Buchen Sie den Wareneingang für die Bestückungstätigkeit und ev. für den vom Lieferanten beigestellten Materialsatz
9. Buchen Sie die Ablieferung des Loses in der erhaltenen Stückzahl. Damit ist die Baugruppe auf Lager, Bestückungskosten und Materialsatzkosten sind im Los und damit in den Ablieferwerten enthalten. Das Lieferantenlager ist entlastet. Ein sehr aufgeschlossener Lohnfertiger teilt Ihnen auch noch mit welche / wieviele Bauteile bei der Bestückung vom Automaten verworfen wurden, defekt wurden und ähnliches. Diese sollten Sie ebenfalls vor der Los-Ablieferbuchung als nachträgliche Materialentnahme buchen, womit Kosten und Lagerwerte ebenfalls stimmen. Erhalten Sie dies nicht, so stimmen gegebenenfalls die Lagerwerte um einige sehr geringe Mengen nicht. Stimmen diese mit den Branchenüblichen Kennzahlen zusammen J/N -> ev. Handlungsbedarf / Abstimmung mit dem Lohnfertiger gegeben.

Welche Dinge sollte noch beachtet werden?

-   Wie betrachten Sie / Ihr Lohnfertiger die Lagerstände? Zum Zeitpunkt der Materialausgabe oder zum Zeitpunkt der Ablieferung. Setzen Sie dazu in den Stücklistenkopfdaten "Materialbuchung bei Ablieferung" entsprechend.

-   Fremdfertigungsstückliste
    Wenn das Material von Ihnen beschafft wird und dem Lohnfertiger beigestellt wird, so bewirtschaften Sie diese Stückliste, also darf Fremdfertigung NICHT angehakt werden.
    Wird die komplette Baugruppe von Ihrem Lohnfertiger erstellt, so macht er auch die Materialbeschaffung usw. D.h. er verkauft Ihnen (die von Ihnen entwickelte Baugruppe) zu einem vereinbarten Preis. In diesem Falle dient die Stückliste (nur) der technischen Definition. Daher ist Fremdfertigung anzuhaken

-   Buchung des Lieferantenlieferscheines mit Ziellager
    Hier hat sich die Verwendung des mobilen Barcodescanners sehr bewährt. D.h. der Mitarbeiter hat die oben unter 3.) beschriebene Liste, der Lieferschein wurde bereits im **Kieselstein ERP** Client angelegt und richtig definiert und der Mitarbeiter bucht mit der Lieferschein-App Rolle für Rolle in den Lieferschein und damit in das Lieferanten-Lager.

-   Sonderfall
    Teilweise beigestelltes Material und teilweise vom Lohnfertiger mit bestücktes Material:
    Diese Situation bilden Sie mit zwei Stücklisten ab:
    1. die Kopf- oder Baugruppenstückliste: In dieser ist sämtliches von Ihnen beschafftes Material enthalten
    2. eine Unterstückliste, welche
        - in der Kopfstückliste (1.) enthalten ist und
        - als Fremdfertigungsstückliste definiert ist

Lieferanten Lieferschein mit Ziellager

Eine praktische Variante davon ist, für die Buchungen den mobilen Barcodescanner und den Lieferschein mit Ziellager zu verwenden.
Erstellen Sie einen Lieferschein mit Ziellager Lieferantenlager, also das Lager Ihres Lohnfertigers
Buchen Sie die Artikel die an Ihren Lohnfertiger gesandt werden in den Lieferschein. Dadurch wandern die Artikel von Ihrem z.B. Hauptlager in das Lieferanten Lager.
Versenden Sie die Ware an Ihren Lieferanten = Lohnfertiger.
Sollten Sie parallel dazu auch Ware an Ihren Lohnfertiger verkaufen, so achten Sie bitte darauf, dass Sie diese dann auch verrechnen müssen, wodurch der Lieferant auch zum Kunden wird.

Wo ist die externe Tätigkeit zu hinterlegen?

Trennen Sie klar zwischen der Tätigkeit z.B. für die Zeitdauer der externen Tätigkeit (Fremdfertigung) und dem Doing welches Sie als Material bei Ihrem Lieferanten bestellen.
Ein Vorschlag wäre z.B.:
- die internen / eigenen Tätigkeiten beginnen mit AZ_ und haben Tätigkeitskosten hinterlegt
- die Zeitreservierung für die Fremdarbeitstätigkeiten beginnen mit AF_ und haben KEINE Kosten hinterlegt. Die Kosten kommen über die Lohnfertigungstätigkeit ins Los, welche wiederum über den Wareneingang zugebucht wird.<br>
Wenn Sie auch die Auslastungsanzeige verwenden, sollten Sie für die Fremdarbeitsgänge auch die geplanten externen Zeiten als Rüstzeit und damit als fixe Dauer eintragen.
- Die eigentliche Bestellung der Lohnfertigung, welche gedanklich eine Materialposition ist und auch Lagerbewirtschaftet ist, beginnt mit EXTERN_

Damit haben Sie eine sehr klare Trennung zwischen den verschiedenen Bereichen.

<a name="Externer Arbeitsgang"></a>Externer Arbeitsgang

## Externer Arbeitsgang
Für die Abbildung vor allem der abschließenden Fremdfertigungsarbeitsgänge, steht im Artikel im oberen Modulreiter Bestellung die Eigenschaft "externer Arbeitsgang" zur Verfügung.
Ist dieser externe Arbeitsgang angehakt, so bewirkt dies, unter der Voraussetzung dass die Los-Stückliste nicht Seriennummer bzw. Chargennummern geführt ist, dass bei der Fehlmengenauflösungsbuchung zugleich die Ablieferbuchung auf das Los mit der gleichen Menge gemacht wird (es ist dies ja die Anlieferung der fertigen Baugruppen, welche von Ihrem Subfertiger produziert wurden). Sollten trotzdem Artikel im Los für die abgelieferte Menge fehlen, erscheint die Sollsatzgrößen unterschritten Meldung.
Konnte die Ablieferung durchgeführt werden, so wird automatisch und ohne Rückfrage die Los-Ablieferetikette ausgedruckt.

Um die Bedienung entsprechend klar zu ermöglichen wird hier die mehrfach Ablieferung (markieren mehrere Zeilen) NICHT unterstützt.

Bitte beachten Sie die unterschiedliche Verwendung der Eigenschaft externer Arbeitsgang für die [Signalisierung des benötigten Materials]( {{<relref "/docs/stammdaten/artikel/#externer-arbeitsgang"  >}} ).

### Wie sieht man, welche Artikel nun für ein einzelnes Los an den Dienstleister zu liefern sind?

Die Herausforderung ist, dass das Los noch im Status angelegt ist. D.h. das Material ist noch in den verschiedenen Lagern oder noch nicht verfügbar.
Um dies festzustellen, verwenden Sie im Modul Los aus dem Reiter Material den Druck, mit der Reportvariante Kommissionierliste.
Gegebenenfalls muss diese noch eingerichtet werden.
![](Reportvariante_Kommissionierliste.gif)
Nun wählen Sie beim Druck diese Reportvariante ![](Kommissionierliste_auswaehlen.gif) Kommissionierliste.
![](Kommissionierliste.gif)
Sie erhalten hier eine Übersicht über:
- das benötigte Material sortiert nach Lagerplätzen
- die bereits entnommene Menge
- die offene Menge
- die offene Bestellmenge
- den Lagerstand Ihres Hauptlagers
- den Lagerstand des Abbuchungslagers des Loses, was dem Lagerstand bei Ihrem Lieferanten entspricht
- der zusätzlichen Info, wieviel davon noch in angelegten Lieferscheinen gebucht ist
- dem Lagerort des Hauptlagers
![](Kommissionierliste2.jpg)

Somit kann mit dieser Liste das Material für das jeweilige Los, idealerweise mit dem Memor, Menüpunkt freier Lieferschein, erstellt werden.
Idealerweise liefern Sie hier passende Verpackungseinheiten.
Zusätzlich sehen Sie hier auch die Beschaffungssituation. D.h. die rot hinterlegten Zeilen bedeuten dass zuwenig lagernd und auch zuwenig bestellt ist.
Die grün hinterlegten Zeilen bedeuten, dass bereits ausreichend Material am Lieferantenlager gebucht ist. Beachten Sie dazu bitte auch die Spalte davon in angelegten Lieferscheinen. Diese Menge ist zwar Lagertechnisch auf das Lieferantenlager gebucht, aber vermutlich ist die Ware noch nicht an Ihren Lieferanten versandt worden.

Fremdfertigung in der Metallverarbeitung

Die Fremdfertigung in der Metallverarbeitung ist im Gegensatz zur Elektronik meist davon geprägt, dass Teile im Hause vorgefertigt werden, dann zur Veredelung (Galvanisieren, Härten, ...) ausser Haus gegeben werden, dann diese Teile wieder zurück kommen, gegebenenfalls weiterverarbeitet (Poliert, geprüft, verpackt, ...) werden. Ev. gibt es eine zweite Fremdfertigung und erst danach erfolgt der Versand an den Kunden. Siehe dazu auch [Veredelung]( {{<relref "/warenwirtschaft/stueckliste/#fremdfertigung" >}} ).<br>
Da in der Metallverarbeitung verschiedenste individuelle Fremdfertigungsschritte erfolgen, welche aber technologisch alle gleich sind (das Härten von kleinen oder großen Teilen ist einfach nur härten, obwohl es natürlich sehr unterschiedliche Kosten verursacht), werden in der Regel nur die Fremdfertigungsartikel angelegt, welche durch die Technologie beschrieben sind. Z.B. härten.<br>
Die Details für den Fremdfertigungsvorgang werden im Arbeitsplan beschrieben.<br>
Bitte beachten Sie hier die Kette, Fremdfertigungstätigkeit (im Arbeitsplan) mit detaillierter Beschreibung der Anforderungen, zeigt auf die Materialposition der Stückliste (die eigentliche Lohnfertigung die bestellt wird). Über das Los wird diese Los-Materialposition bei Ihrem Lieferanten bestellt und zugleich der Inhalt des Arbeitsgangkommentars (aus der Stückliste/Los) angedruckt.
Zugleich bedeutet dies aber, dass das Material dieser Bestellung sofort, egal wie die Einstellungen für die Materialbewirtschaftung sind, beim Wareneingang in das Los zu buchen sind. Zusätzlich können solche Veredelungsartikel nie auf Lager liegen. Mit diesem Verhalten ist sichergestellt, dass immer die richtigen Kosten / Preise auf dem jeweiligen Los landen (und dass keine Durchschnittspreise gebildet werden).<br>
D.h. von **Kieselstein ERP** wird vor der Buchung des Wareneingangs überprüft ob ein derartiger Fremdfertigungsartikel, welcher sich NUR aus dem Bezug der Bestellposition zur Los-Materialposition zum Arbeitsgangartikel ergibt, auf Lager liegt. Da dies nicht sein sollte, erscheint eine entsprechende Fehlermeldung. In diesem Falle brechen Sie bitte die Zubuchung ab und stellen zuerst den Lagerstand dieses Artikels richtig.<br>
**Hinweis:** Wurden echte Fehlbestellungen gemacht, dann ist der falsch bestellte Artikel ein echter Lagerartikel mit Längenangabe. Dieser wird dann
1. aus dem Los ans Lager zurückgebucht und
2. wenn ein anderer Auftrag = Los diesen Artikel verwenden kann, wird dieser direkt vom Lager genommen. Dieser Artikel muss dann auch in seiner Länge (und nicht nur Qualität und Durchmesser) definiert sein muss. Es darf da gedanklich keine Mischbeschaffungen, welche zu Mischpreisen führen würden, geben.

Zusätzlich erscheint eine Abfrage, Fremdarbeitsgang erledigt J/N. Mit Ja wird der Fremdarbeitsgang, auf den das Material verweist, als Fertig abgehakt. Ist das der letzte Arbeitsgang im Los, wird zugleich die Los-Ablieferbuchung gemacht.