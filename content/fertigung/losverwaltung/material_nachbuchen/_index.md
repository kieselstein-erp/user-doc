---
title: "Material nachbuchen"
linkTitle: "nachbuchen"
categories: ["nachbuchen"]
tags: ["nachbuchen"]
weight: 600
description: >
  Material zusätzlich ins Los buchen
---
Material nachbuchen
===================

Da wir immer wieder nach den einzelnen Schritten für die manuelle Nachbuchung von Material in die Lose gefragt werden, insbesondere auch wenn andere Artikel als geplant verwendet werden, hier eine Beschreibung der detaillierten Schritte dafür.

## 1. Feststellen welches Los denn überhaupt betroffen ist.<br>
Ermitteln Sie den Artikel.<br>
So gehen Sie z.B. in der Rechnung über den Reiter Positionen auf die Position Lieferschein und von hier weiter in den Reiter Lieferschein.
Hier finden Sie die mit diesem Lieferschein gelieferten Artikel.
Mit dem GoTo Artikel springen Sie direkt in den Artikel.<br>
![](Goto_Artikel_aus_LS.jpg)

Im Artikel wählen Sie im Menü Info Statistik<br>
![](Statistik.jpg)<br>
In der Regel finden Sie hier aufgrund der chronologischen Zuordnung das dazugehörende Los, mit dem der Artikel produziert wurde.<br>
![](GoTo_Los_aus_Statistik.gif)<br>
Klicken Sie nun auf die Belegnummer der Losablieferung um direkt auf das Los zu springen.

Sollten die Zuordnung nicht eindeutig ersichtlich sein, so wählen Sie anstatt der Statistik das Warenbewegungsjournal. Dieses Journal zeigt die eineindeutige Zuordnung zwischen Wareneingang (der Losablieferung) und dem Verbraucht (dem Lieferschein)<br>
![](Warenbewegungsjournal.jpg)

## 2. ein erledigtes Los auf teilerledigt zurückstellen
Um nachträgliche Buchungen auf ein vollständig erledigtes Los durchführen zu können, muss dieses wieder in den Status Teilerledigt zurückgenommen werden.<br>
Wechseln Sie daher in die Kopfdaten des Loses und klicken auf ändern.
![](Los_Aendern.jpg)

Die Frage beantworten Sie mit Ja
![](Los_Status_aendern.jpg)

## 3. Material nachbuchen
Bei dem teilerledigten oder auch nur ausgegebenen Los wechseln Sie nun in den Reiter Material.
Hier sehen Sie
![](Los_Materialliste.jpg)
dass das Material insgesamt nicht ausgegeben, also auch nicht vom Lager in die Produktion gebucht wurde.<br>
Hinweis: Bitte beachten Sie in jedem Fall die [Sollsatzgröße](index.htm#Sollsatzgröße) unterschritten Meldung, welche in derartigen Konstellationen ausgegeben wird und handeln Sie entsprechend.

Nun gibt es zwei Möglichkeiten:
1. Ist das gewünschte Material auf Lager, so klicken Sie auf ![](nachtraegliche_20Materialentnahme.gif) Material nachträglich entnehmen.
Im nachfolgenden Dialog
![](nachtraegliche_Materialentnahme_durchfuehren.gif)
wird die benötigte bzw. lagernde Menge entsprechend vorgeschlagen. Klicken Sie anschließend auf den Knopf Entnahme um die Buchung vom Lager ins Los durchzuführen.
2.  das gewünschte Material nicht im Lager und Sie entscheiden sich dafür z.B. ein etwas dickeres Material zu verwenden (weil dieses lagernd ist) so ändern Sie den Artikel dieser Position.<br>
Wichtig: Diese Änderung gilt nur für diese Position für dieses Los, die Stückliste wird damit nicht verändert (siehe dazu auch Menüpunkt Info, Vergleich mit Stückliste)<br>
In unserem Beispiel ist das 180x20mm Material nicht lagernd, also wird das 180x25mm Material verwendet.<br>
Sie klicken also auf ändern ![](Los_Sollposition_aendern.jpg)
und anschließend auf Artikel um den gewünschten Artikel auszuwählen.
Nach der Änderung des Soll-Materials geben Sie dieses wie oben beschrieben aus.
3. Hinweis: Da es auch immer wieder zu ungeplantem Mehrverbrauch kommt, können auch nachträgliche (ungeplante) Materialentnahmen vorgenommen werden. Nutzen Sie dafür ![](Materialentnahme_ohne_Sollposition.jpg) Die nachträgliche Materialentnahme OHNE Bezug zur Sollposition.
4.  Nach der Materialausgabe mit eventueller Korrektur der Sollposition sieht die Losposition z.B. wie folgt aus:<br>
![](Los_Materialliste_ausgegeben.jpg)<br>
Das bedeutet, dass für dieses Position das gesamte Material entnommen wurde.<br>
In weiterer Folge muss dies für jede Position mit Fehlmengen, siehe dazu auch Reiter Fehlmengen, durchgeführt werden.<br>
Für die verschiedensten Automatismen nutzen Sie bitte die entsprechenden Parametrierungen
5. Nachkalkulation aktualisieren
Mit dieser nachträglichen Änderung muss auch die Berechnung der Ablieferwerte aktualisiert werden.
Wechseln Sie dazu in den Reiter Ablieferung und klicken zur Aktualisierung der Berechnung aller Ablieferungen auf ![](Los_Ablieferung_neu_rechnen.gif) den Taschenrechner.
Sie erhalten nun aktualisierte Herstellkosten dieses Produktes / dieser Baugruppe.
6.  Los wieder vollständig erledigen.
Wenn das Los eigentlich aber fälschlich erledigt war, sollte es zum Abschluss auch wiederum erledigt werden.
Nutzen Sie dafür den Menüpunkt Bearbeiten, Manuell erledigen<br>
![](Los_Manuell_erledigen.gif)<br>
Die nachfolgende Frage des Erledigungsdatums beantworten Sie in der Regel mit Datum aus letzter Ablieferung übernehmen.
Beachten Sie dabei, dass damit eventuelle vergangene Statistiken verändert, also korrigiert wurden.

## Material vollständig
Wurde in den Kopfdaten des Loses das Material als vollständig gekennzeichnet, so erscheint die Fehlermeldung ![](Material_vollstaendig.png)<br>
Um nun trotzdem Material in das Los buchen zu können, muss diese Kennzeichnung in den Kopfdaten zurückgenommen werden [siehe]( {{<relref "fertigung/losverwaltung/#kann-man-ein-los-als-nicht-mehr-zu-ver%C3%A4ndern-kennzeichnen">}} ).
