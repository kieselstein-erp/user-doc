---
title: "Trumpf Anbindung"
linkTitle: "Trumpf Anbindung"
categories: ["Trumpf"]
tags: ["Trumpf"]
weight: 1000
description: >
  Geplant: Übergabe von 2D Zeichnungen an Trumpf FAB
---
Trumpf FAB Anbindung
====================

Dies ist eine Beschreibung der geplanten Anbindung des **Kieselstein ERP** an  Laserschneidemaschinen der Firma Trumpf.
Mit dieser bidirektionalen Schnittstelle werden die Los als Fertigungsaufträge an Trumpf FAB per XML Datei Kommunikation übergeben.
Ergänzend dazu werden nur diejenigen Artikel als zu fertigen übergeben, deren Artikelklasse als Tops gekennzeichnet sind.
Weiters werden die Geodaten der Artikel in den Formaten PSM und PAR übergeben, wobei die Artikelnummer und ... und die Stückliste in der diese Datei liegen muss, die Datei welche als GEO-Daten übergeben werden.
Für die Ermittlung der Gestehungspreise der gefertigten Platinen, wird die unter [Tops]( {{<relref "/fertigung/losverwaltung/trumpf_tops">}} ) beschriebene Logik verwendet.

Organisatorisch ist es so, dass Trumpf FAB immer Trumpf Boost voraussetzt. D.h. auch wenn Trumpf FAB von einem anderen Entwicklerteam als Trumpf Boost entwickelt wurde, setzt es trotzdem auf Boost auf. Auch verwenden beide Softwaresysteme eine gemeinsame Datenbank.

Voraussetzungen, Vorbereitende Arbeiten um **Kieselstein ERP** mit Trumpf FAB zu verbinden.
--------------------------------------------------------------------------------

### Organisatorisches
Ein wesentlicher Punkt für die Verbindung von zwei IT-Systemen ist die qualifizierte Pflege der Stammdaten. Nur damit können die Systeme untereinander Daten automatisiert austauschen.
Daher sind in den **Kieselstein ERP** Stammdaten, insbesondere Artikel einige Definitionen vorzunehmen um einen guten Datenaustausch vornehmen zu können.
Zur Laserartikel, Platinen, Laseroberfläche, Material,
Dateipfade
Geodaten

Sind Artikel an das FAB/Boost übergeben, sollten diese je nach Status der Produktion nicht mehr geändert werden.
Jedenfalls müssen Artikeländerungen IMMER über das **Kieselstein ERP** veranlasst werden, bzw. durch die Konstruktion. Eine Schnell-Schnell-Änderung z.B. im Boost wird von Seiten **Kieselstein ERP** nicht unterstützt. Es ist dies, aus unserer Sicht für die durchgängige Pflege der Teile grundsätzlich zu vermeiden.

Rückmeldungen die **Kieselstein ERP** vom Trumpf-FAB erwartet.
a.) Artikel -> MasterData
Eine Bestätigung dass der Artikel gültig in den Artikelstamm von FAB übernommen werden konnte.
Wir gehen davon aus, dass in der Regel nur neue Artikel übernommen werden. Sind neue Varianten von Artikel erforderlich muss ein Artikel mit einer entsprechenden Versionsnummer neu erzeugt werden. Nur in Ausnahmefällen sollten bestehende Artikel aktualisiert werden. Es ist dafür in jedem Falle eine Abstimmung mit der Produktion / dem Schachtler / dem Programmierer der Tafel(n) erforderlich. Von Seiten Trumpf wird dringend angeraten, ab dem Zeitpunkt ab dem ein Teil in einem Job, also einer Tafel, eingeplant ist, dieses nicht mehr zu ändern. Für die Details in dieser Vorgehensweise wenden Sie sich bitte an Ihren Trumpf Betreuer.
b.) Fertigungsaufträge -> ProdData
Von **Kieselstein ERP** werden für jeden Laserartikel, es wird gerne auch von Platinen gesprochen, ein eigener Produktionsauftrag an das Trumpf FAB gesandt.
In diesem steht im wesentlichen die Artikelnummer, die benötigte Stückzahl, das Rohmaterial und der gewünschte Endetermin.
An Rückmeldungen erwarten wir:
b1.) Der Produktionsauftrag wurde gültig ins FAB übernommen
b2.) der Produktionsauftrag wurde in einen Job übernommen. D.h. es gibt bereits ein Schneideprogramm für eine Tafel in dem dieser Produktionsauftrag enthalten ist.
b3.) Von diesem Produktionsauftrag wurde xx Stück als gut gefertigt. D.h. beim Abräumen wurde die entsprechende Gut-Stückzahl ermittelt.
Wichtig: Der Unterschied zwischen einem FAB Produktionsauftrag und einem **Kieselstein ERP** Fertigungslos. In einem Los sind eine gewissen Anzahl von Artikeln die für die Produktion dieser gesamten Baugruppe benötigt werden. Darin enthalten sind auch Artikel bei denen als Artikelklasse eine Artikelklasse mit der Eigenschaft Tops angehakt haben. Nur diese Teile werden an das Trumpf FAB übermittelt. Im Sprachgebrauch werden diese Artikel auch Laserteile bzw. Platinen genannt.
Es wird für jedes Laserteil ein eigener Produktionsauftrag an das Trumpf FAB übermittelt. Der Endetermin ergibt sich aus dem Beginntermin des Arbeitsganges der nach dem Arbeitsgang Lasern eingeplant ist.
Für jedes Laserteil, also für jeden Produktionsauftrag erwarten wir die entsprechenden Rückmeldungen.
In der Regel sind diese Rückmeldungen innerhalb weniger Sekunden von Seiten Trumpf verfügbar und werden dann nach **Kieselstein ERP** übernommen.

### IT-Technisches
#### Betriebssysteme
Da das Trumpf-System ausschließlich unter dem Betriebssystem Windows läuft, ist, trotz aller Nachteile, anzuraten, auch Ihren **Kieselstein ERP** Server unter diesem Betriebssystem laufen zu lassen.
Theoretisch würde es reichen, nur den **Kieselstein ERP** Trumpf Microservice z.B. direkt am Trumpf Server laufen zu lassen. Womit aber immer noch die Thematik der direkten Filezugriffe gegeben ist.<br>
Achten Sie beim Einsatz des Trumpf-Systems darauf, dass auch alle Anforderungen an die CPU im Detail unterstützt werden. Gerade wenn es um automatische Abrollungen aus 3D-Daten geht.

#### Dienste
Der von **Kieselstein ERP** geplante Microservice muss lesend auf die Geo-Daten = Zeichnungen zugreifen und muss schreibend, löschend, lesend in die gemeinsamen Dateipfade zur Kommunikation mit Trumpf schreiben können. Daher muss der Dienst unter einem Benutzerkonto laufen, welcher die entsprechenden Berechtigungen besitzt.<br>
Zugleich empfiehlt sich, dass auch der **Kieselstein ERP** Server unter einem Benutzerkonto läuft, vor allem wenn auch die Dateiverweise im Artikelstamm z.B. für die Anzeige von Zeichnungen und ähnlichem verwendet werden.

### Zusätzliches
Automatische Debitorennummer muss aktiviert sein. Trumpf verlangt, wenn Kunden übergeben werden, dass auch Kundennummern übergeben werden. Damit Sie dann im **Kieselstein ERP** Kundenstamm diese auch gut finden können, haben wir uns für die Debitorennummer entschieden. D.h. wenn beim Los ein Kunde, direkt oder über den Auftrag (nicht über eine ev. Verknüpfung der Projektklammer) hinterlegt ist, so muss dieser Kunde auch eine Debitorennummer bekommen, welche von Ihrem **Kieselstein ERP** automatisch generiert wird.

#### Verzeichnis:
Wird im Artikel ein TrumpfPfad hinterlegt, so bedeutet dies, dass entgegen der anderen Logik dieser Pfad für die Suche der Zeichnungsdateien (Geo-Dateien) durchsucht wird.
Dies kann auch für Stücklisten oder Hilfsstücklisten verwendet werden.

#### Anlegen weiterer Materialpositionen im Los:
Dies kann für die Laserartikel nicht vorgenommen werden, bzw. muss in diesem Falle ein TrumpfPfad für den Artikel definiert sein, damit beim Export der Artikeldaten (MasterData) an Trumpf auch die richtige Zeichnung übergeben wird.
Die Losausgabe ist nur möglich, wenn für alle Laserartikel auch Zeichnungen gegeben sind.
Ob dies ein Pfad (nur ein Ordner) ist oder ein expliziter Dateiname wird anhand der im Microservice hinterlegten zu übertragenden Dateiendungen definiert.
Bitte vermeiden Sie in jedem Falle die Verwendung von Punkt in den Dateipfaden.
Wichtig: Die Funktion Ersatztypen darf für die Laserartikel NICHT verwendet werden.

#### Was ist ein LaserArtikel?
Das ist ein Artikel der eine Artikelklasse hat, in der wiederum der Haken für Tops gesetzt ist.

#### Welcher Zeitpunkt wird an Trumpf übergeben?
Es wird der Erste Arbeitsgang der ebenfalls Mitglied der Artikelklasse Tops ist, verwendet um, mit dem nächsten Arbeitsgang seinen eigenen Endetermin festzustellen verwendet.
D.h. wenn z.B. die Tätigkeit Lasern heißt und der Arbeitsgang 10 ist, so wird der Arbeitsgangbeginn der nächsten Tätigkeit z.B. Kanten als der spätesten Endetermin für das Lasern der Laserteile verwendet. Damit können Sie im Arbeitsgang-Beginn-Termin Offset in der Stückliste bereits definieren, welche operative Zeit, inkl. Leer- bzw. Pufferzeiten, Sie für das Lasern planen.<br>
Sollten mehrere Tätigkeiten Lasern gegeben sein, wird die früheste Tätigkeit verwendet.

### Mircoservice Parametrierungen
Angabe der zu verwendenden Datei-Extensions in der zu verwendenden Reihenfolge. Z.B. .psm, .par
was bedeutet, dass wenn eine Datei mit der Erweiterung .psm gefunden wird diese als CAD File Information mit vollständigem UNC Pfad an Trumpf übergeben wird.<br>
Pfadangaben für die Zeichnungsdateien, werden ebenfalls in den Parametern / Properties für den Microservice eingestellt. z.B. *\\TrumpLaserServer\\Solidedge*<br>
Die Parametrierungsdatei für den Service finden Sie unter ?:\kieselstein\microservice\trutops\etc\service.json<br>
Name des Dienstes / Services: ??<br>

### Parametrierung des **Kieselstein ERP** für den Trumpfservice
TRUTOPS_BOOST_SERVER    <http://localhost:8282><br>
An diesen http-Server sendet **Kieselstein ERP** bei der Losausgabe die Losnummer. Damit weiß der **Kieselstein ERP** TruTops Microservice dass er die Daten für dieses Los für Trumpf aufbereiten muss.<br>
Durch Klick auf ![](Systemparameter_leeren.gif) kann der Eintrag gelöscht werden, womit die Prüfungen bei der Losausgabe abgeschaltet werden und auch die Übergabe an den Microservice nicht mehr erfolgt.<br>
Dies ist für die Inbetriebnahme durchaus praktisch, hat im laufenden Betrieb natürlich entsprechende dramatische Auswirkungen.

**Hinweis:** Erscheint bei der Losausgabebuchung: Am Server ist ein Fehler aufgetreten, so ist der Syntax für den Parameter TRUTOPS_BOOST_SERVER falsch. Das passiert gerne, wenn man die Vorgaben per Copy & Paste übernimmt. Bitte prüfen Sie, dass hinten und vorne keine Leerzeichen oder ähnliches im Boost_Server link angegeben sind.

### Voraussetzungen, Fehlermeldungen für die Losausgabe bei aktiviertem TruTops_Boost_Server
Für die Berechnung des gewünschten Fertigstellungsdatum muss es einen Arbeitsgang geben der die Artikelklasse mit gesetztem TOPS ![](TruTops_Artikelklassen_Flag.gif)hat.
Ist dies nicht der Fall kommt die Fehlermeldung:
![](TruTops_Export_Fehler1.jpg)

Für die Übergabe an Trumpf müssen die Artikel entsprechend definiert sein.
Hier werden vor allem das Material und die Kennung, sowie die sogenannte Laseroberfläche und deren Kennung herangezogen.
![](TruTops_Export_Fehler2.jpg)

![](TruTops_Export_Fehler3.jpg)

Anmerkung:
Die Übergabe erfolgt immer bei der Ausgabe des Loses.
Dabei erscheinen zuerst die Warnungen / Informationen welche vor bei der Losausgabe erfolgen sollten. Danach wird der TrumpfExport durchgeführt.

![](Los_Trumpf_Kommunikations_Info.jpg)

Ermittlung des Dateipfades für die Geodaten:
- a.) Es können in der Parametrierungsdatei des MicroserviceTops unter path.cadfilename die Liste der zu durchsuchenden Dateipfade angegeben werden. Bitte achten Sie auf die XML spezifische Notation der doppelten \ Backslash (um ein Backslash zu erhalten müssen zwei angegeben werden).
Grundsätzlich werden diese Verzeichnisse durchsucht
- b.) ist im Laserartikel im Reiter Trumpf unter Boost-Pfad ein vollwertiger Dateiname angegeben, so wird diese Geodatei verwendet. Ist nur ein Pfad angegeben, so wird versucht, ob hier die entsprechende Geodatei (Dateiname siehe unten) vorhanden ist.
![](Trumpf_Boost_Pfad.jpg)
- c.) Es wird anhand der Artikelnummer der Stückliste des Loses ein Verzeichnis in obigen Pfaden gesucht
- d.) Darin muss es für jeden Laserartikel eine Datei mit der Erweiterung psm bzw. par geben. Siehe dazu extensions.cadfilename. Diese werden in der Reihenfolge der Angabe bearbeitet. D.h. wenn zuerst die psm Extension angegeben ist, und es gibt eine entsprechende Datei, so wird diese verwendet, egal ob auch eine par Datei vorhanden ist.
- e.) Da in der CAD üblicherweise die Versionen mit -A hochgezählt werden, wird für das finden des richtigen Filenamens der - (Bindestrich) nicht beachtet. D.h. es wird erwartet, dass z.B. obiger Artikel 12982F eine Geodatei mit dem Fiilenamen 12982-F.psm (oder .par) hat.
- f.) Ist einer der Laserartikel in einer Hilfsstücklisten enthalten, so wird, ausgehend von der Artikelnummer der Hilfsstückliste in der gleichen Logik vorgegangen.
- g.) wird keine Geodatei gefunden, so wird in der Dateiliste nachgesehen, wird auch hier nichts gefunden, so wird obige Fehlermeldung ausgegeben.
Die Dateiliste wird täglich aufgebaut und beinhaltet alle Dateien aus den path.cadfilename die der Erweiterung extensions.cadfilename entsprechen.
Da üblicherweise zwischen Konstruktion und Fertigungsbeauftragung mehr wie ein Tag vergehen, sollte diese Indexierung immer die richtige Datei finden.

Hinweis: Wird bei der Ausgabe eines Lose auch über die Dateiliste keine Geodatei gefunden erscheint ein entsprechender Hinweis.
Ein ähnlicher Hinweis erscheint, wenn für einen Artikel mehrere Geodateien gefunden werden.
Die Vorgehensweise ist nun, dass Sie wie unter b.) beschrieben, den passenden Dateinamen für den jeweiligen Laserartikel angeben. Somit ist eindeutig definiert, welche Geodatei Sie für die Beschreibung des Artikels verwenden möchten.

LogDatei

LOG-File
Das Microservice LOG-File ist unter ?:\heliumv\microservice\trutops\logs zu finden und kann für eine Fehlerursachensuche gerne verwendet werden.

![](Ruecksetzen_Exportstatus.jpg)
Klick oben