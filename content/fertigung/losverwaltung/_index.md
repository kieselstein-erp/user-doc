---
title: "Losverwaltung"
linkTitle: "Losverwaltung"
categories: ["Losverwaltung"]
tags: ["Losverwaltung"]
weight: 100
description: >
  Fertigungsauftäge, Produktionsaufträge, Lose planen und verwalten. Das Herzstück der Produktionssteuerung. Das PPS für dein Unternehmen.
---
Im **Kieselstein ERP** steht dir mit dem Modul Fertigung ![](Fertigung.gif) ein umfassendes Werkzeug zur Verwaltung deiner Fertigungsaufträge zur Verfügung. Da die Fertigungsaufträge zugleich Fertigungslose sind, wird in **Kieselstein ERP** der Begriff Los für die Fertigungsaufträge verwendet.

## Statusübergänge
![](statusuebergaenge.png)

Weitere [Planungs- und Auswertungsjournale]( {{<relref "/fertigung/losverwaltung/planung_auswertung">}} ), Liste der zu spät kommenden Artikel

[Anzeige was zu tun ist, was ist als nächstes zu fertigen]( {{<relref "/fertigung/losverwaltung/was_ist_zu_tun">}} ).

Erklärung der [Zusammenhänge Stückliste und Fertigung]( {{<relref "/fertigung/losverwaltung/stueckliste_fertigung">}} ).

[Die ideale Firma]( {{<relref "/docs/einleitung/die_ideale_firma">}} )

[Zeitbewirtschaftung in der Fertigung]( {{<relref "/fertigung/zeitwirtschaft_in_der_fertigung">}} )

### Kann die Anzeige der offenen Lose beeinflusst werden?
Neben den Direktfiltern, mit denen man direkt auf ausgewählte Lose zugreift, steht auch der Filter auf den Losstatus zur Verfügung.
![](ZuProduzieren.gif)
Damit wird nach den folgenden Status gefiltert:
| Status | Bedeutung |
| --- | --- |
| Alle | es werden die Lose aller Status angezeigt |
| Offene | es werden nur die offenen Lose angezeigt. Das sind alle Lose im Status angelegt, Ausgegeben, in Produktion, Teilerledigt|
| Zu produzieren | es werden nur die Lose der Status Ausgegeben, in Produktion, Teilerledigt angezeigt |

Default steht diese Auswahl auf Zu produzieren. Die Default Einstellung kann als Eigenschaft des Arbeitsplatzes in den Arbeitsplatzparametern mit dem Parameter FERTIGUNG_ANSICHT_OFFENE_LOSE entsprechend gesteuert werden.

### Fremdarbeitsgänge, wie können Fremdarbeitsgänge in **Kieselstein ERP** abgebildet werden?
Diese Thematik ist vor allem bei Maschinenbau Unternehmen gegeben.<br>
Es bedeutet, dass während des Fertigungsprozesses auch mehrmals das teilgefertigte Produkt außer Haus gegeben werden muss, damit ein Produktionsprozess, der im eigenen Unternehmen nicht durchgeführt werden kann, von einem sogenannten Fremdfertiger (Lohnfertiger, verlängerte Werkbank) durchgeführt wird.<br>
Hier ein Beispiel aus einem Maschinenbau Unternehmen, welches kundenspezifische Gehäuse herstellt. Diese werden in Kundenfarben ausgeliefert. Das beschichten dieser Gehäuse wird außer Haus durchgeführt.<br>
Die Artikelnummer des Endproduktes wird fortlaufend vergeben.<br>
Im Artikel des Endproduktes wird die Oberfläche und die Farbe definiert.<br>
Diese Information muss über den Bestellvorschlag an den Lieferanten gelangen.
Es wird daher ein oder mehrere Fremdarbeitsgangartikel angelegt. Dessen Artikelnummer ist (vorne) gleich wie das Endprodukt, wird aber hinten durch _x für den Fremdarbeitsgang ergänzt.
Das Anlegen des Fremdarbeitsgang Artikels geschieht durch kopieren des Endprodukt-Artikels und ausbessern der Bezeichnung und der Artikelklasse.<br>
Damit können auch verschiedene Fremdarbeitsgänge je Endprodukt definiert werden.
Zusätzlich wird im Arbeitsplan der Arbeitsgang Fremdarbeit aufgenommen, um den Mitarbeitern zu signalisieren, dass hier auch eine Außer-Haus Bearbeitung erfolgt / erforderlich ist.
Zum Thema [Fremdfertigung siehe bitte auch dort]( {{<relref "/fertigung/losverwaltung/fremdfertigung">}} ).

### Beistellware, siehe [Bestellungen]( {{<relref "/einkauf/bestellung/#beistellware-wie-wird-das-richtig-gemacht" >}} )

### Wozu Losklassen
Mit den Losklassen haben Sie die Möglichkeit Fertigungslose für weitere Auswertungen zu markieren. Die Losklassen werden in der Fertigungsverwaltung, Grunddaten hinterlegt und sind NICHT Mandantenabhängig. Das hat den Vorteil, dass dadurch auch übergeordnete Auswertungen durchgeführt werden können (z.B. Managementstatus)

### Los anlegen
Klicken Sie auf den Neu Button und definieren Sie die Daten des Loses.
Beim Speichern des Loses werden die aktuellen Stücklisten Daten in das Los übernommen und alle Artikel, mit Ausnahme der Arbeitszeitartikel reserviert.
Der Reservierungstermin ist der Beginntermin des Loses.

| Lable | Bedeutung |
| --- |  --- |
| Beginntermin | Zu diesem Zeitpunkt wird mit der Produktion des Loses begonnen. |
| Endetermin | Zu diesem Zeitpunkt ist (voraussichtlich) die Produktion des Loses abgeschlossen.  |
| Durchlaufdauer | Zeit in Tagen die es benötigt ein Produkt durch die Fertigung zu bringen.Die (Durchlauf-) Dauer ist ein ein Erfahrungsfaktor und hat keinen bzw. nur einen sehr bedingten, Zusammenhang mit der geplanten Sollzeit. |

Bitte beachte dazu auch die Parameter:
- AUTOMATISCHE_ERMITTLUNG_BEGINNTERMINOFFSET, 
- NUR_TERMINEINGABE, 
- AUTOMATISCHE_ERMITTLUNG_LOS_ENDE

Für das automatische Anlegen neuer Lose siehe bitte [Interne Bestellung]( {{<relref "/fertigung/losverwaltung/interne_bestellung">}} ) ![](Interne_Bestellung.gif).

#### Was sind nun die aktuellen Stücklisten Daten?
Genau genommen wird für die Materialpositionen die errechnete Zielmenge der Stückliste mit der Losgröße multipliziert und dieser Wert als Sollmenge in das Los eingetragen. Für die Berücksichtigung von Ersatztypen [siehe bitte dort]( {{<relref "/warenwirtschaft/stueckliste/ersatztypen/#wie-werden-ersatztypen-in-das-los-%c3%bcbernommen" >}} ).<br>
Sind Stücklisten Positionen als Rüstmenge gekennzeichnet, so wird die Menge 1:1 übernommen.
Bitte beachten Sie dazu auch die verschiedenen Rundungsoptionen anhand von [Verschnittbasis, Verschnittfaktor, Verschnittmenge, Fertigungs-VPE]( {{<relref "/docs/stammdaten/artikel/#verschnittbasis--verschnittfaktor-" >}} ).<br>
Für die Verwaltung von Werkzeugen siehe unter anderem [Kunststofffertigung](#besonderheiten-in-der-kunststofffertigung).
Für die Behandlung von Hilfsstücklisten [siehe bitte](#Hilfsstückliste).

#### Können Stücklisten eines anderen Mandanten übernommen werden?
Beim zentralen Artikelstamm können auch die Stücklisten eines anderen Mandanten übernommen werden. Klicken dazu bei der Stücklistenauswahl erneut auf das Stücklistensymbol. Je nachdem ob ein oder mehrere andere Mandanten gegeben sind, muss zuerst der andere Mandant und dann die Stückliste bei dem jeweiligen Mandanten ausgewählt werden.
Bei der Überleitung wird die Montageart und die Fertigungsgruppe anhand der gleichen Bezeichnung zugeordnet. Ist diese nicht gegeben, wird die erste Fertigungsgruppe / die erste Montageart verwendet.
Die Überleitung der Maschinen erfolgt anhand der Maschinen ID. D.h. es wird angenommen, dass gleichwertige Maschinen auch die gleiche Maschinen ID haben.
Das Ziel und Abbuchungslager werden mit dem Hauptlager des Mandanten besetzt.

Wie werden Beginn und Ende bedient ?

Bei der Anlage von Losen müssen auch der Beginn- und der Ende-Termin definiert werden.
![](Los_Beginn_Ende.gif)
Um die Bedienung möglichst einfach zu steuern gibt es Dauer und Rückwärts / Vorwärts. Das bedeutet:
Ist Dauer angehakt, so wird die Dauer stabil gehalten. Das bedeutet z.B. wenn Sie den Beginntermin ändern so wird der Endetermin um die Dauer versetzt mitgezogen.
Ist Dauer nicht angehakt, so wird die Dauer zwischen Beginn und Ende errechnet und je nachdem welche Datum Sie hier einstellen ergibt sich die entsprechende Dauer.
Ein Sonderfall stellt die Änderung der Durchlaufzeit dar. Wird diese verändert, so gilt es zu definieren, ob der Beginn- oder der Endetermin die Ausgangsbasis ist. Ist Vorwärts gewählt, so wird der Beginntermin stabil gelassen und bei Änderung der Dauer der Ende verschoben. Ist Rückwärts gewählt geschieht dies genau umgekehrt.

Der Beginntermin wird bei der Neuanlage eines Loses entweder aus dem Auftragspositionstermin abzgl. der Kunden-Lieferdauer vorbesetzt oder wenn in das Los manuel eine Stückliste eingetragen wird, dass wird als Beginn heute angenommen.

#### Kann der Lostermin nachträglich geändert werden?
Wählen Sie dafür Menüpunkt bearbeiten, Termin verschieben.
![](Los_Termin_verschieben.jpg)
Somit erscheint der Verschiebe-Dialog.
![](Los_Termin_verschieben2.jpg)
Nun geben Sie die gewünschten Termine ein. Durch Klick auf das Schloss
![](Los_Termin_verschieben3.gif) erreichen Sie, dass die Termine gemeinsam verschoben / geändert werden.
Mit Klick auf Speichern werden die Lostermine entsprechend verschoben.
Es können damit auch alle offenen Lose (angelegt, ausgegeben, in Produktion, teilerledigt) verschoben werden.

#### geht das auch für mehrere Lose?
Wählen dafür ebenfalls Bearbeiten, Termin verschieben und klicken nun auf den Button Lose.
Im nachfolgenden Dialog geben Sie die weiteren Lose an, die mit-verschoben werden sollten und klicken anschließen auf den grünen Haken um die gewählten Lose in die Liste der zu aktualisierenden Lose zu übernehmen. Mit speichern werden alle Lose entsprechend auf die beiden gewünschten Termine gesetzt.

#### Da immer wieder die Frage kommt, zu welchem Termin wirkt das denn im Bestellvorschlag?
Im Bestellvorschlag greift für die positiven Mengen immer der Beginntermin und für ev. negative Menge der Endetermin, da wird es wieder zurückgegeben.
Bitte beachten Sie dazu auch den Beginnterminoffset bzw. den Parameter FRUEHZEITIGE_BESCHAFFUNG. Damit kann der Termin für den Bedarf für die einzelne Position neben dem Beginntermin eigens gesteuert werden. Beachten Sie dazu auch den Parameter [AUTOMATISCHE_ERMITTLUNG_BEGINNTERMINOFFSET](#Automatischer Beginnterminoffset).

#### Können Lose anhand Kundenaufträgen angelegt werden ?
Siehe dazu bitte [Lose anhand Kundenaufträgen anlegen]( {{<relref "/fertigung/losverwaltung/los_anhand_kundenauftraegen">}} ), sowohl für Lose eines Auftrags als auch für Lose eines Auftrags mit Stücklistenauflösung.

#### Können Lose in einer bestimmten Reihenfolge eingeplant werden ?
Siehe dazu bitte [Lose anhand Kundenaufträgen nach Reihenfolge anlegen]( {{<relref "/fertigung/losverwaltung/los_anhand_kundenauftraegen/#k%C3%B6nnen-lose-eines-auftrags-st%C3%BCcklisten-%C3%BCbergreifend-in-einer-bestimmten-reihenfolge-angelegt-werden">}} )

#### Materialliste oder Artikel
Beim neu Anlegen eines Loses kann zwischen zwei Losarten unterschieden werden.
Artikel: Dies bedeutet, dass die Basis (Solldaten) des Loses aus einer Stückliste kommen.
Materialliste: Dies bedeutet, dass es keine üblicherweise verfügbaren Solldaten für dieses Los gibt. Z.B. für einen Reparaturauftrag nach Aufwand, allgemeine Arbeiten usw.. Sie können einer Materialliste manuell jederzeit Solldaten hinterlegen. Diese Solldaten gelten jedoch nur für dieses eine Los. Beim Ändern der Losdaten könnten diese Daten wieder gelöscht werden.
So kann die Materialliste, auf der natürlich auch Zeiten gebucht werden können, auch dazu nutzen, Kosten für allgemeine Arbeiten wie Zusammenräumen, oder Verbrauch von Büromaterial zu erfassen. Gut bewährt hat sich dies auch für die Erfassung der Aufwendungen von Entwicklungen und ähnlichem. Die kann soweit gehen, dass durch Eingabe der Solldaten die geschätzten Entwicklungszeiten bzw. Entwicklungskosten hinterlegt werden und so in der Nachkalkulation immer der Überblick über Soll Ist gewahrt bleibt.

#### kann ich einen Gewichtshinweis ausgeben?
In manchen Installation ist es eine wichtige Info, wenn ein Los, z.B. für einen Montageauftrag, das Gesamtgewicht der Einzelkomponenten einen gewissen Wert überschreitet, da damit gesteuert wird, ob die Ware selbst ausgeliefert wird oder ob dazu eine Spedition beauftragt werden muss.
Diese Info wird nur bei der Neuanlage eines Loses ausgegeben. Als Gewichtsgrenze dient das im Mandantenparameter GEWICHTSHINWEIS angegebene Gewicht in kg.

Zeitbuchungen auf ein Los

Um auf ein Los Zeiten buchen zu können, muss dieses
1. Ausgegeben und noch nicht vollständig erledigt sein
2. Zumindest eine Soll-Materialposition haben. Soll-Materialpositionen können unter ![](Material.gif) angelegt werden. Es besteht hier auch die Möglichkeit für einen Artikel eine Sollmenge von Null einzugeben.

Wenn die Buchung mittels BDE-Erfassung erfolgen sollte, so muss im Los zumindest ein Arbeitszeitartikel enthalten sein, damit ein Fertigungsbegleitschein ausgedruckt werden kann.

## Los ausgeben

Durch Klick auf ![](Los_Ausgabe.gif) aus den ![](Kopfdaten.gif) heraus werden alle Material-Positionen des Loses ausgegeben. Bitte beachten Sie den Unterschied der Bedeutung des Ausgabeknopfes im Reiter ![](Material.gif). Die Ausgabe eines Loses bedeutet, dass alle Materialpositionen des Loses in der entsprechenden Menge vom Lager/den Lägern abgebucht werden. Ist nicht ausreichend Material am Lager vorhanden, so erfolgt jeweils ein Eintrag in die Fehlmengenliste. Durch die Statusänderung des Loses ist es ab diesem Zeitpunkt auch für die Zeitwirtschaft bebuchbar. Bei der Losausgabe gibt es im SNR-Dialog einen Haken 'Automatik'. Wenn dieser angehakt ist, werden, wenn die ausgewählte Menge gleich der benötigten Menge ist, die Seriennummern automatisch übernommen.
Zum Abschluss der Losausgabe wird der Fertigungsbegleitschein und daran anschließend die Ausgabeliste ausgedruckt.
Hier wirken folgende Parameter:

#### Kann die Ausgabe des Materials auch in Verbindung mit der Buchung der Tätigkeiten gesteuert werden?
Es stehen dafür zwei Parameter zur Verfügung
1. MATERIAL_BEI_ERSTEM_AG_NACHBUCHEN
Ist dieser Parameter aktiviert, so wird bei der Buchung der ersten Tätigkeit auf dieses Los, das gesamte benötigte Material, soweit verfügbar, auf das Los gebucht.
2. MATERIAL_ANHAND_GLEICHER_ARTIKELGRUPPE_BUCHEN
Ist dieser Parameter aktiviert, so wird nur das Material der Artikelgruppe der Tätigkeit, bei der ersten Zeitbuchung in das Los, soweit verfügbar, gebucht.
Beispiel: Es werden für die Produktion Ihrer Schränke auch Bolzen benötigt. Nun gibt es eine Tätigkeit Bolzen setzen. Um nun zu erreichen, dass die Bolzen erst aus dem Lager entnommen werden, wenn mit dem Bolzen setzen begonnen wird, müssen sowohl die Tätigkeit Bolzensetzen als auch die Bolzen selbst, die gleiche Artikelgruppe haben.
Damit erreichen Sie, dass das Material nach tatsächlichem Fertigungsfortschritt gebucht wird.
Bitte beachten Sie, dass wenn die Tätigkeit wieder gelöscht werden sollte, z.B. wegen Fehlbuchung, wird deswegen das Material nicht mehr an das Lager zurück gegeben.
Dass die automatische Ausgabebuchung des Materials bei Ändern des Losstatuses auf ausgegeben bzw. in Produktion abgeschaltet ist.
Bitte beachten Sie in diesem Zusammenhang auch den Parameter [AUTOMATISCHE_ERMITTLUNG_BEGINNTERMINOFFSET](#Automatischer Beginnterminoffset).
Sollte nun, trotzdem doch kein Material gebucht werden, so kann in den Artikelgruppen diese Funktion durch entfernen des Hakens bei ![](BeiErsterZeitbuchungAbbuchen.gif) bei erster Zeitbuchung abbuchen, dies für die gewählte Artikelgruppe abgeschaltet werden.
c.) Sind beide Parameter deaktiviert (= 0) so wird die übliche Materialbuchung anhand der anderen Parameter vorgenommen. Hier greifen vor allem auch die Stücklisteneigenschaft Materialbuchung bei Ablieferung bzw. die Parameter KEINE_AUTOMATISCHE_MATERIALBUCHUNG, BEI_LOS_ERLEDIGEN_MATERIAL_NACHBUCHEN.

#### kann der Zeitpunkt des Materialbedarfes anhand der Arbeitspläne gesteuert werden?
<a name="Automatischer Beginnterminoffset"></a>
Mit dem Parameter AUTOMATISCHE_ERMITTLUNG_BEGINNTERMINOFFSET kann bei der Terminberechnung der Arbeitsgänge, [siehe dazu](#kann-der-losbeginn-automatisch-errechnet-werden), auch aktiviert werden, dass der sogenannte Beginnterminoffset der Materialpositionen passend zum Arbeitsgangbeginn gesetzt wird. D.h. abweichend vom Beginntermin des Loses, werden die Artikel um den Arbeitsgangbeginn später benötigt. Somit scheinen diese Artikel auch entsprechend später im Bestellvorschlag bzw. in der internen Bestellung auf. Das reduziert Ihr Lager entsprechend. Die Verbindung zwischen Tätigkeit und Material wird über die Artikelgruppen hergestellt. D.h. die früheste Tätigkeit einer Artikelgruppe definiert mit ihrem Arbeitsgangbeginn auch den Beginnterminoffset der Artikel der gleichen Artikelgruppe.
Damit ist auch gegeben, dass wenn es für Artikel keine Artikelgruppen gibt, bzw. keine Tätigkeiten für diese Artikelgruppe definiert ist, so werden diese zum Beginntermin des Loses benötigt.

#### Von welchen Lagern wird abgebucht?
[siehe](#läger-eines-loses)

#### Mehrere Lose zugleich ausgeben
![](Mehrere_Lose_ausgeben.gif)

Wird im Menü, Bearbeiten, Mehrere Lose ausgeben gewählt, so können damit:

- alle angelegten Lose anhand einer bestimmten Fertigungsgruppe in einem Durchgang ausgegeben werden. Bitte beachten Sie, dass dabei derzeit keine Fertigungspapiere ausgedruckt werden.
- Mit enthaltene Stücklisten (ausgeben) werden alle Lose, zu denen entsprechenden Stücklisten des Loses (inkl. Kopflos) vorhanden sind (unabhängig vom Auftrag) einzeln ausgegeben. Zum Schluss können die einzelnen Fertigungsbegleitscheine der ausgegebenen Lose gedruckt werden. Tritt ein Fehler auf, kann die Los-Augabe abgebrochen werden (alle bis dahin ausgegebenen Lose bleiben jedoch ausgegeben).

Bevor die eigentlichen Lager-Ausgabe-Buchungen durchgeführt werden, wird die Gesamtfehlmenge aller davon betroffenen Lose(Stücklisten) geprüft. Werden Fehlmengen festgestellt, so erscheint ein Dialog, in dem die dadurch entstehenden Fehlmengen angezeigt werden. In dieser Liste werden alle Artikel die Fehlmengen erzeugen würden angeführt. Mit dem GoTo kann in den jeweiligen Artikel für weitere Informationen gesprungen werden. Die Berechnungsbasis der Fehlmenge ist, Sollmenge der Materialposition größer als die Summe der Lagerstände der Los-Abbuchungsläger.
![](Fehlmengen_bei_Stuecklistenausgabe.jpg)
- alle bis Beginntermin. Hier werden alle Lose bis inkl. dem angegebenen Beginntermin ausgegeben. Zum Schluss werden die Fertigungsbegleitscheine ausgedruckt. Tritt ein Fehler auf, so wird die Los-Ausgabe abgebrochen, alle bis dahin ausgegebenen Lose bleiben jedoch ausgegeben.

**Info:**
Werden mehrere Lose gleichzeitig ausgegeben, so werden Chargen- bzw. Seriennummern tragende Artikel NICHT mit ausgegeben. Hintergrund ist die Komplexität in der Benutzerführung. Das bedeutet, dass Chargen- bzw. Seriennummern geführte Artikel als Fehlmenge in die jeweiligen Lose eingetragen werden und anschließend manuell oder mit der Ablieferbuchung ausgegeben werden müssen.

Zweite Art der Losausgabebuchung

In manchen Betrieben gibt es kein wirkliches (physikalisches) Lager. Vielmehr wird dort das eingehende Material direkt in der Fertigung gelagert. Wenn nun der klassische Produktionsprozess: anlegen, ausgeben, rückmelden zum Einsatz kommt, ist der optische (offensichtliche) Lagerstand falsch, da ja keine Trennung zwischen Material in Fertigung und Material im Lager möglich ist.

Hier gibt es nun ein anhand der Stückliste definiertes anderes Verhalten ([Materialbuchung bei Ablieferung]( {{<relref "/warenwirtschaft/stueckliste/#materialbuchung-bei-ablieferung" >}} ), in dem von **Kieselstein ERP** das Material erst bei der Erledigungsmeldung vom Lager in das Los abgebucht wird.

Dies hat den Vorteil dass das offensichtlich verfügbare Material auch dem datentechnischen Lagerstand entspricht. Es hat den Nachteil, dass keine Halbfertigfabrikatsinventur möglich ist, was aber in Betrieben dieser Art auch nicht gewünscht wird.

#### Dürfen alle Benutzer ausgegebene Lose wieder zurücknehmen?
Es dürfen nur diejenigen Benutzer ein Los wieder in den Status angelegt zurücknehmen, die das Recht FERT_DARF_AUSGEGEBEN_ZURUECKNEHMEN besitzen.
Dies ist vor allem dann von Bedeutung, wenn, z.B. Ersatztypen, Änderungen bei den Sollpositionen und oder den Ist-Mengen gemacht wurden.

#### Wo sehe ich ob am Los schon gearbeitet wird
Dies wird dadurch angezeigt, dass in der Auswahlliste der Lose das Los in grün dargestellt wird. Ist zusätzlich auf den Auftrag des Loses Pönale / Vertragsstrafe hinterlegt, so wird, da ja die Pönale in Rot angezeigt werden würde, das Los in violett dargestellt.

Drucken einer weiteren Ausgabeliste

In verschiedenen Unternehmen ist es praktisch, wenn zwei unterschiedliche Losausgabelisten automatisch ausgedruckt werden. Um dies zu erreichen gehen Sie bitte wie folgt vor:

1. Definieren Sie einen weiteren Report für die Losausgabeliste. Das Formular muss den Namen fert_ausgabeliste2.* haben.
2. Richten Sie die Möglichkeit des manuellen Druckes über Reportvarianten (Mutation) ein und definieren Sie das Formular wie gewünscht.
3. Stellen Sie den Parameter LOSAUSGABELISTE2_AUTOMATISCH_DRUCKEN auf 1\. Nun wird automatisch bei der Losausgabe eine weitere Materialentnahmeliste mit den in dem Formular definierten Daten gedruckt.

#### Druck mehrerer Losausgaben in einer Liste
<a name="Druck mehrerer Losausgaben in einer Liste"></a>
Sollten für eine effizientere Materialentnahme das Material für mehrere (ähnliche) Lose auf einmal vom Lager in die Produktion entnommen werden, so steht unter dem Punkt Los, Drucken, Ausgabeliste, mit dem Knopf Lose die Möglichkeit zur Verfügung, die Ausgabeliste mehrerer Lose zusammenzufassen und so die Artikel gemeinsam vom Lager zu entnehmen, oder auch den Materialbedarf der Lose gemeinsam zu betrachten.

#### kann die Frage nach dem Druck der Papiere abgeschaltet werden?
Um die Frage ob die Fertigungspapiere gedruckt werden sollten abzuschalten, stellen Sie bitte den Parameter MEHRERE_LOSE_AUSGEBEN_FERTIGUNGSPAPIERE_DRUCKEN auf 0\. Damit wird diese Frage unterdrückt.

#### Läger eines Loses
<a name="Läger eines Loses"></a>
In der **Kieselstein ERP** Losverwaltung können mehrere Läger für ein Los definiert werden. Dies bewirkt, dass bei der Losausgabe zuerst das Material vom ersten Lager entnommen wird. Ist auf diesem Lager kein Material mehr verfügbar, so wird versucht Material vom nächsten Lager zu entnehmen. Bis hin zum letzten dem Los zugeordneten Lager.
Dadurch können Sie z.B. Material von Kundenlägern, welches aber von Ihnen verwaltet wird, bevorzugt abbuchen. Erst wenn das gesamte Material (des jeweiligen Artikels) vom Kundenlager verbraucht wurde, wird auf Ihr eigenes Lager, wenn dies definiert ist, zurückgegriffen. **Hinweis:** Auch das Zubuchungslager, also jenes Lager, auf das das Los seine Ablieferungen bucht, kann in den Kopfdaten unter Ziellager eingestellt werden.

#### Welche Läger werden automatisch vorbesetzt?
<a name="Lager vorbesetzen"></a>
Im Artikel unter Grunddaten, Lager können die Lager definiert werden, welche automatisch als Abbuchungslager für ein neues Los vorgeschlagen werden. Geben Sie hier unter ![](Abbuchungslager_Vorbesetzen.gif) Sortierung Losausgabe die gewünschte Reihenfolge der Läger an die als Abbuchungslager übernommen werden sollten. Wurde kein Lager definiert, so wird automatisch das Hauptlager übernommen.
Zusätzlich kann für jede Stückliste definiert werden, welche Läger ergänzend berücksichtigt werden sollten. Siehe dazu [Abbuchungslager]( {{<relref "/warenwirtschaft/stueckliste/#abbuchungsl%c3%a4ger" >}} ).

Die Reihenfolge für das automatische Vorbesetzen der Abbuchungs-Läger für das Los ist wie folgt:
1. Es werden alle Läger, welche in der Stückliste definiert sind, übernommen
2. Es werden alle Läger die "Sortierung Losausgabe" definiert haben, in dieser Reihenfolge übernommen
3. Ist das Hauptlager noch nicht vorhanden, wird dieses ebenfalls übernommen

Um nun Fremdfertigungs-Lose komfortabel zubuchen zu können, reicht es im Wareneingang einer Bestellung das entsprechende Lieferantenlager anzugeben. Dieses kann wiederum in den Konditionen des Lieferanten definiert werden.

Hinweis: Das Ziellager des Loses wird aus dem Ziellager der Stückliste übernommen.

#### Was wird bei Materialentnahme auf das jeweilige Los zuerst abgebucht.
Das hängt von der Einstellung des Loses ab.

Üblicherweise wird beim Anlegen des Loses nur das Hauptlager als Abbuchungslager automatisch definiert. Sie können jedoch im Reiter Läger ein weiteres Abbuchungslager hinzufügen oder wenn gewünscht auch die Reihenfolge durch Klick auf die Pfeile verändern.

#### Wie wird bei der Abbuchung von Chargen vorgegangen?
Sind im Material des Loses Chargengeführte Artikel vorhanden, so erscheint bei der Materialausgabe eine Abfrage welche Charge verwendet werden sollte. Ist nur eine Charge lagernd, so kann nur diese eine Charge entnommen werden, weshalb keine Abfrage erscheint. **Hinweis:** Darf ein Fertigungslos von mehreren Lagern abbuchen, so wird immer zuerst das erste Lager geleert.
Dadurch ergibt sich die Situation, dass bei chargengeführten Artikeln, mit unterschiedlichen Chargen auf unterschiedlichen Lägern, aber immer nur einer Charge auf einem Lager, dass ohne Rückfrage das Material zuerst vom ersten Lager, dann vom Zweiten usw. entnommen wird.

#### Es sollte bei der Losausgabe immer die älteste Charge zuerst vorgeschlagen werden
Setzen Sie dafür bitte den Parameter LOSAUSGABE_AUTOMATISCH auf 1.
Oft wird dieser Parameter auch in Kombination mit dem Parameter AELTESTE_CHARGENNUMMER_VORSCHLAGEN = 1 verwendet.
Dies bewirkt, dass in aller Regel alle Losausgaben ohne Benutzerinteraktion ablaufen. Einzig, wenn die benötigte Losmenge nicht in einer Charge zur Verfügung steht, also der Benutzer entscheiden muss, was denn tatsächlich entnommen werden sollte, erscheint eben die Frage welche Charge nun wirklich verwendet wird.

## Fehlmengen
In **Kieselstein ERP** steht Ihnen auch die Möglichkeit der Fehlmengenbuchung zur Verfügung. Das bedeutet, Sie können ein Los bereits ausgeben, wenn noch nicht das gesamte Material für diesen Fertigungsauftrag verfügbar ist.
Das hat den entscheidenden Vorteil, dass Sie frühzeitig mit der Produktion beginnen können. Ist ein Los im Status ausgegeben, so können Zeiten und Material darauf gebucht werden. Wird nun auf einem fehlmengenbehafteten Artikel Ware zugebucht, so erhalten Sie eine entsprechenden Hinweis, dass diese Ware in der Fertigung benötigt wird und Sie können diese wiederum sofort auf den Fertigungsauftrag ausbuchen.
Eine Information über die Auslöser der Fehlmenge erhalten Sie über den Druck der Fehlteile. Wechseln Sie dazu im Los in den Reiter Fehlmengen und klicken auf das Druckersymbol. Hier erfolgt eine Auflösung in die jeweiligen Unterlose, so können Sie erkennen, welche Materialposition auslöst, dass die einzelnen Lose nicht gefertigt können.<br>
**Hinweis:** Der Termin der Fehlmenge entspricht immer dem Produktionsstart des Loses, korrigiert um den [Beginnterminoffset](#Automatischer Beginnterminoffset).<br>
**Hinweis:** Wurde beim Warenzugang die Fehlmenge auf Lager belassen, so muss dies durch die [nachträgliche Materialbuchung](#Nachträgliche Materialentnahme) dem Los zugeordnet werden.
Bitte beachten Sie, dass bei der Auflösungsbuchung der Fehlmenge immer nur die gerade zugebuchte Menge weiter in das/die Los(e) gebucht werden. Die Buchung von bereits lagerndem Material muss mit der [nachträglichen Material-Entnahme](#Nachträgliche Materialentnahme) durchgeführt werden.

#### Kann man im Los schnell die Fehlmengen sehen?
Im Reiter Fehlmengen werden diejenigen Artikel angezeigt die für dieses Los noch Fehlmengen sind.<br>
Bitte beachten Sie, dass je nach Status des Loses dies unterschiedliche Darstellungen sind.
Ist das Los nur angelegt so sind dies theoretische Fehlmengen. D.h. hier werden hier nur Originalartikel angezeigt, deren noch auszugebende Menge (Soll - Ist) kleiner dem Lagerstand ist.
Ist der Parameter LAGERINFO_IN_POSITIONEN aktiviert (=1), so werden auch die Lagerstände angezeigt.
Ist in Ihrer **Kieselstein ERP** Installation auch die Ersatztypenverwaltung verfügbar, so sehen Sie auch die Lagerstände aller Ersatztypen.
Dafür ist definiert, dass die direkten und die indirekten Ersatztypen herangezogen werden.
Direkt sind diejenigen die in der Stückliste hinterlegt sind und somit direkt in das Los übernommen wurden (die grünen Zeilen).
Indirekte Ersatztypen sind diejenigen, die beim Originalartikel unter Ersatztypen zugewiesen sind.
Dies vor allem, damit Sie einen Hinweis bekommen, dass ev. andere Artikel für diese Position eingesetzt werden sollten, da verfügbar.

#### Wann werden die nachträglichen Entnahmen ausgedruckt?
Wenn die fehlenden Materialien nach der Wareneingangsbuchung sofort wieder in das Los / die Lose ausgebucht wurde, wird beim Schließen eines beliebigen Moduls auch die Liste der nachträglichen Materialentnahmen ausgedruckt. Dies dient vor allem dazu, dass jedes Material eindeutig gekennzeichnet sein muss, vor allem wenn es in die Fertigung gegeben wird. Von manchen Anwendern wird dieses Verhalten als störend empfunden. Daher kann es mit dem Parameter AUFGELOESTE_FEHLMENGEN_DRUCKEN abgeschaltet werden.

#### Fehlmengen nachträglich Auflösen

Eine weitere Möglichkeit ist, alle Fehlmengen über den Menüpunkt ![](alle_fehlmengen_aufloesen.png) im Modul Fertigung aufzulösen.
![](frage_fehlmengen_aufloesen.JPG)
Nach Klick auf den Menüpunkt erfolgt noch die Abfrage, ob man wirklich alle Fehlmengen auflösen will, danach wird die Fehlmengenauflösung durchgeführt und ein Ausdruck dazu angezeigt. Ausgenommen hiervon sind Artikel, die serien- oder chargennummerntragend sind.
Die Verbrauchsbuchung erfolgt in der Form, dass anhand des Beginntermins das früher zu fertigende Los zuerst bedient wird, also die Ware auf dieses Los gebucht wird. Auch hier werden die (Fehl-)Mengen anhand der Reihenfolge der Los-Lager abgebucht. Im Anschluss wird ein entsprechender Report mit allen entnommenen Artikeln angezeigt.
Für diesen Menüpunkt ist das Recht FERT_DARF_FEHLMENGEN_PER_DIALOG_AUFLOESEN erforderlich.

#### Braucht man für die Bestellungen Fehlmengen?
Bitte beachten Sie, dass für die Reservierung der Artikel der Status angelegt des Loses bereits ausreicht. Da damit die Artikel zum Beginn-Termin des Loses reserviert sind, werden diese beim nächsten Bestellvorschlagslauf bzw. bei der internen Bestellung mit berücksichtigt.

#### Wir benötigen für die Fertigungs-Fluss-Steuerung eine Lagerplatzzuordnung am Los
Mit der Zusatzfunktion LAGERPLATZ_IM_LOS kann im Los, durch Klick auf den Button ![](Lagerplatz_im_los.gif) jedem Los ein Lagerplatz, unabhängig vom Ziellager des Loses, zugeordnet werden. Ist die Zusatzfunktion aktiviert, so wird der Lagerplatz auch in der Losauswahlliste mit angezeigt.

#### Anzeige der Artikelklasse im Los-Material
Mit dem Parameter LOS_MATERIAL_ARTIKELKLASSE_ANZEIGEN kann die Anzeige der Artikelklasse im Reiter Material aktiviert werden.

#### Erfassung des geplanten Materialeintrefftermins
Mit dem Parameter LOS_MATERIAL_ZIELTERMIN_ANZEIGEN kann in den Kopfdaten die zusätzliche Information eingefügt werden, die angibt, bis wann das Material vollständig sein wird.
Dies Info muss manuell eingegeben werden. (Siehe Import...)<br>
![](Material_vollstaendig.png)<br>
Zusätzlich kann diese Info auch über den Menüpunkt Bearbeiten, Terminverschieben solange bis das Los vollständig erledigt ist, verändert werden.

Zusätzlich wird damit in der Los-Auswahlliste dieser Termin, vor dem Beginntermin angezeigt.

#### Import von Los-Terminen
Ab der Version 0.1.1 steht in den Lose auch die Möglichkeit zur Verfügung, dass Los-Beginntermin, der Positionstermin des anhängend Auftrags und, wenn aktiviert, das <u>Material vollständig bis zum</u> über einen XLS Import aktualisiert werden können.<br>
Du findest diese Funktion im Modul Los, Menüpunkt Los, dann Import und nun<br>
![](Los_Termin_Import.png)<br>
Der Aufbau der XLS Datei muss wie folgt sein:<br>
![](Los_Termin_Import_XLS.png)<br>
Der Import erfolgt wie üblich in zwei Durchläufen.
- zuerst wird die Gültigkeit der Daten überprüft
- ist diese gegeben, sos werden nach dem Klick auf Importieren die Termine wie gewünscht aktualisiert.

Die Spalte Auftragspositionstermin bedeutet, dass, wenn dieses Feld befüllt ist, der Termin der Auftragsposition, auf die dieses Los zeigt, auf diesen Termin gesetzt wird.

Bitte beachte, dass die drei Datumsfelder auch im XLS als Datum definiert sein müssen.

#### was bedeutet das kleine f?
<a name="Das kleine f"></a>
Um nun trotzdem eine rasche Übersicht über die Verfügbarkeit bzw. Produzierbarkeit von Stücklisten zu erhalten, wurde neben der Anzeige des großen F (für Fehlmenge bei ausgegebenen Losen) auch das kleine f, für aktuell jetzt nur aufgrund des Lagerstandes verfügbar, eingeführt.
Analog zur Anzeige der Fehlmengen und der wahrscheinlichen Eintrefftermine der Artikel, steht hier diese Aussage ebenfalls zur Verfügung.
Bitte beachten Sie, dass diese Darstellung für jedes Los / jeden Artikel auf die derzeitige Möglichkeit = ausreichend am Lager verfügbar abzielt. Eine Verfügbarkeitsberechnung ist darin nicht enthalten. D.h. es kann durchaus sein, dass Sie nun das erste Los ausgeben, damit die Artikel vom Lager entnommen werden und sofort sind für die anderen Lose zuwenig Artikel verfügbar.
Um nun für Fremdfertigungsartikel, welche ja erst später geliefert werden können, die Anzeige des kleinen f unterdrücken zu können, Sie möchten ja nur den Material Status wissen, ob bereits mit der Produktion begonnen werden kann, kann über die Artikelgruppe, z.B. der Fremdfertigungsartikel, definiert werden, dass diese Artikel in die temporäre Fehlmengenbetrachtung nicht einbezogen werden. D.h. wenn Sie z.B. für das externe Verzinken oder Härten, eine Artikelgruppe externe Bearbeitung definiert haben, so setzen Sie bei Fremdfertigung ![](Fremdgefertigte_Artikel.gif)einen entsprechenden Haken.

#### was bedeutet das kleine b?
Ergänzend zum kleinen f bedeutet das kleine b, dass zwar immer noch zuwenig Material am Lager verfügbar ist, aber für alle Artikel sind alle Positionen ausreichend bestellt und diese Bestellpositionen haben bereits einen bestätigten Liefertermin (Reiter Sicht Lieferantentermine im Bestellmodul). Das bedeutet, es ist zumindest alles im Laufen.
Auch hier wirkt die Ausnahmeregelung für die Fremdfertigungsartikel.

In anderen Worten:
Artikel der Artikelgruppe mit angehaktem Fremdfertigungsartikel, werden in der Betrachtung der vorab Fehlmengen (kleines f, kleines b) nicht berücksichtigt, da sie vom Produktionsprozess her erst später benötigt werden. Für die tatsächliche Fehlmenge (großes F) hat dies keine Auswirkung.

Zusammengefasst hat die rechte Spalte im Los folgende Aussage:

| Anzeige | Losstatus | Bedeutung |
| --- |  --- |  --- |
| f | angelegt | es ist für zumindest eine Materialposition zu wenig Material auf Lager. Von dieser Betrachtung ausgenommen sind Artikel die Mitglied einer Artikelgruppe mit Fremdfertigung ist. |
| b | angelegt | wie f, aber es sind alle Artikel ausreichend bestellt und für die Bestellpositionen sind die Liefertermine bereits durch den Lieferanten bestätigt. |
| <leer> | angelegt | keine Fehlmengen (für die Betrachtung nur des einen Loses, keine Verfügbarkeitsberechnungen) |
| F | ausgegeben,in Produktion,teilerledigt | Es sind bei zumindest einer Materialposition weniger Teile in das Los gebucht worden, als laut Sollmenge erforderlich ist.Dies gilt unabhängig ob der jeweilige Artikel Mitglied einer Fremdfertigungsartikelgruppe ist oder nicht. |
| B | ausgegeben,in Produktion,teilerledigt | Wie F, aber es sind alle Artikel ausreichend bestellt und für die Bestellpositionen sind die Liefertermine bereits durch den Lieferanten bestätigt. |
| L | ausgegeben,in Produktion,teilerledigt | Es sind noch Fehlmengen (F) auf das Los gebucht, aber für diese Materialpositionen sind inzwischen ausreichend am Lager. |
| <leer> | ausgegeben,in Produktion,teilerledigt | keine Fehlmengen. D.h. für alle Teile wurden soviel ins Los gebucht (oder mehr) wie laut Sollmenge benötigt wird. |

**Hinweis 1**:<br>
Im Vergleich zur theoretischen Fehlmengenliste wird in der Anzeige der theoretischen Fehlmengenliste die (ist bereits) bestellt Information, also dem gelben Hintergrund, **nur** die Information ist bereits bestellt berücksichtigt. Dagegen muss in der Auswahlliste jede bestellte Position auch vom Lieferanten bestätigt sein.
**Hinweis 2**:
Artikel die nicht Lagerbewirtschaftet sind, werden in dieser Betrachtung nicht berücksichtigt.

### Solldaten nachträglich ändern
<a name="Solldaten nachträglich ändern"></a>
Im Reiter Material kann mit neu ![](Neue_Sollposition.gif) ein neuer Solleintrag im Los hinterlegt werden. Damit definieren Sie neue Solldaten und ev. auch einen entsprechenden Sollpreis für die neue Losposition. Bitte beachten Sie, den Unterschied zur nachträglichen Materialentnahme. Ev. zusätzlich erfasste Sollmengen werden am linken Rand mit einem "N" angezeigt. Dass es Solldaten sind, ist am Eintrag der Menge ersichtlich.
Mit Klick auf ![](Solldaten_aendern.gif) ändern können die erhaltenen Solldaten geändert werden. Sowohl Artikelnummern als auch Sollausgabemengen.
Diese Funktion kann z.B. zur Verwendung von alternativen Artikeln, nur einmalig freigegebenen Artikeln verwendet werden. Eine Änderung der Sollmenge ist im gesamten Bereich möglich. Die Fehlmenge wird entsprechend mitgezogen. Beachten Sie bitte, dass die angezeigte Sollsatzgröße / Sollsatzmenge aus der Sollmenge über die Losgröße errechnet wird.

### Nachträgliches Ändern bereits ausgegebener Mengen
<a name="Nachträgliches Ändern bereits ausgegebener Mengen"></a>
Es kommt manchmal vor, dass bereits in ein Los ausgegeben Artikel wieder zurückgenommen werden müssen. Dies könnte nun durch eine allgemeine Rücknahme des Loses erfolgen. Da dies gerade bei bereits teilerledigten / teilabgelieferten Losen nicht mehr möglich ist und da dies sehr oft nur einzelne Artikel betrifft, steht dafür die Funktion ![](Nachtraegliche_Entnahme.jpg) Nachträgliche Änderung bereits ausgegebener Mengen zur Verfügung.
Durch Klick auf diese Knopf erscheint eine Liste der Detailbuchungen der Buchungen dieser Position von Lager ins Los. Wählen Sie eine entsprechende Ausgabeposition aus 
![](Nachtraegliche_Entnahmeliste.jpg)<br>
und ändern Sie im unteren Dialog die Entnahmemenge auf die gewünschte Stückzahl. Nach der Bearbeitung aller Positionen schließen Sie dieses Fenster durch Klick auf das rechte obere X.
Beachten Sie bitte, dass bei diesen Mengenänderungen auch die Fehlmengen entsprechend angepasst werden.
Mit dieser Darstellung sehen Sie auch aus wievielen Einzelbuchungen die Entnahmebuchung besteht.

### Nachträgliche Material-Entnahme
<a name="Nachträgliche Materialentnahme"></a>
Mit ![](Nachtraegliche_Mat_Entnahme.gif) im Reiter Material ![](Material_im_Los.gif) kann nachträglich Material auf das Los gebucht werden. Sie gelangen dadurch in einen eigenen Eingabe Dialog in dem Sie Material auf das Los entnehmen können. Der Dialog ist in der Sollposition mit der ausgewählten Materialposition des Loses vorbesetzt. Unter Menge wird diejenige Menge vorgeschlagen, welche Sie noch auf das Los buchen sollten und aufgrund des Lagerstandes auch können.
Sollte die Buchung die Fehlmenge reduzieren, so belassen Sie den Haken bei Fehlmenge reduzieren. Ist es erforderlich, dass die Fehlmenge erhalten bleibt, so darf hier kein Haken gesetzt sein.
Sollte es erforderlich sein, dass zusätzlich Material auf das Losgebucht werden soll, also Material dass mit der Losposition keinen Zusammenhang hat, so klicken Sie bitte auf Sollposition und löschen Sie den Bezug durch Klick auf ![](SollPositionLoeschen.gif).
Ist keine Sollposition gewählt, so kann ein beliebiger Artikel ausgewählt werden.
Ev. zusätzlich erfasste Ist-Mengen werden am linken Rand mit einem "N" angezeigt. Dass es Ist-Daten sind, ist am Eintrag unter Ausgegeben ersichtlich.
Bitte beachten Sie, dass für Rückgaben und **Erhöhung der Fehlmengen** die ![](Nachtraegliche_Entnahme.jpg) [Änderung bereits ausgegebener Mengen](#Nachträgliches ändern bereits ausgegebener Mengen) verwendet werden muss.
Mit dem Knopf ![](NachtraeglicheMat_Entnahme_ohne_Sollposition.gif) Nachträgliche Materialentnahme ohne Sollposition, haben wir oben beschriebene Bedienung vereinfacht. D.h. wenn Sie auf diesen Knopf klicken, so wir ebenfalls der Dialog der nachträglichen Materialentnahme aufgerufen. Hier ist jedoch unabhängig auf welcher Zeile der Materialentnahme der Cursor steht, die Sollposition nicht vorbesetzt und somit die zusätzliche Entnahme von Material deutlich einfacher.

#### Hinzufügen eines weiteren Ersatzartikels
Bei aktiver Ersatztypenverwaltung ergibt sich immer wieder auch die Forderung, dass ausnahmsweise für dieses eine Los, ein weiterer Ersatzartikel zu einem Originalartikel hinzugefügt werden muss. Insbesondere in der Verbindung mit dem Traceimport, aber auch generell für die Betrachtung der Gegenüberstellung der Sollmengen mit den ausgegebenen Mengen, muss eine neue Los-Materialposition als Ersatztype für die Originalposition gekennzeichnet werden.
Um eine zusätzliche Position als Ersatztype zu definieren, gehen Sie bitte wie folgt vor:
1. Fügen Sie den zusätzlichen Artikel hinzu. Siehe oben Nachträgliche Materialentnahme, bzw. eine neue Sollposition mit neu.
2. markieren Sie den Artikel und klicken Sie auf ![](Ersatztype_nachdefinieren.gif) Als Ersatztype definieren.
3. nun wählen Sie aus den aufgelisteten Original Sollpositionen diejenige Position aus, für die diese Position ein Ersatztype sein sollte.
Somit ändert sich die Zeile auf grün und Sie erhalten ![](Ersatztype_nachdefinieren2.gif), also N für Nachträglich hinzugefügt und grün für Ersatztype.

#### Wir haben eine lebende Stücklistenentwicklung, wie vorgehen?
In einigen Branchen / Unternehmen werden die Stücklisten praktisch durch die Produktion z.B. eines Schaltschrankes bestimmt. D.h. in der Regel werden die wichtigsten Komponenten in die Stückliste geschrieben und eine grobe Schätzung was an üblichem Material wie Kabel, Stecker, Klemmen usw. verbraucht wird. Die tatsächliche Menge ergibt sich aus der Verdrahtung, der Montage des Schrankes. D.h. der Ablauf ist, gerade bei Kabeln, Leitungen so, dass das Material Bundweise z.B. 100m vom Lager entnommen und auf das Los gebucht wird. Der verbleibende Rest wird dann am Ende der Produktion wieder gemessen und die Restmenge vom Los wieder ans Lager zurückgebucht.

Da es meistens erwünscht ist, dass diese nachträglichen Änderungen ersichtlich sind, oft auch kein Bezug zu Sollpositionen herstellbar ist, verwenden Sie dafür bitte die die Funktion:

Nachträgliche Materialentnahme ohne Sollposition ![](NachtraeglicheMat_Entnahme_ohne_Sollposition.gif)
Diese Zeilen werden durch die (Buchungs-)Art N(achträglich) in den Materialpositionen angezeigt

Werden zusätzliche Positionen benötigt, welche nicht auf Lager sind, so tragen Sie diese bitte mit ![](Neue_Sollposition.gif) Neu ein. Bei einem Los In Produktion werden die Fehlmengen dadurch entsprechend aktualisiert, was wiederum zur Berücksichtigung im Bestellvorschlag führt. Da auch dies eine Änderung der Solldaten gegenüber der Stückliste ist, wird die Buchungsart ebenfalls auf Nachträglich gesetzt.

Sollte sich eine Materialentnahme auf eine Sollposition beziehen, so verwenden Sie dafür ![](Nachtraegliche_Mat_Entnahme.gif) Nachträgliche Materialentnahme. Diese Buchung, wird bei einem Ausgegebenen Los ebenfalls zusätzlich in die Materialpositionen eingefügt, da sie aber einen Bezug zur Sollposition hat, wird als Buchungsart S(ollposition) angezeigt. 

**ACHTUNG:** Das Verhalten dieser Funktion ist von der Auswahl ![](Fehlmenge_reduzieren.gif) abhängig.

- Wird Fehlmenge reduzieren angehakt, so wird diese Buchung als die verspätete Entnahme auf die Sollposition betrachtet, die Fehlmenge reduziert und in den Losmaterialpositionen keine weitere Zeile mehr eingefügt.<br>
Hinweis: Auch wenn eine Entnahme durchgeführt wird, die größer als die tatsächliche Fehlmenge ist, wird dies als Entnahme mit direktem Bezug auf die Sollposition betrachtet.
- Wird Fehlmenge reduzieren nicht angehakt bzw. abgehakt, so wird dies als zusätzliche Materialentnahme betrachtet und die Buchung ohne Sollmenge, aber mit Bezug auf die Sollposition eingetragen.

**Info:**

Denke auch an die Möglichkeit der Buchung mittels Mobilem Scanner direkt in der Produktion, was die Handhabung deutlich vereinfacht und immer richtige Lagerstände bewirkt.

Buchung zusätzlicher Kosten ins Los

Es kommt immer wieder vor, dass ungeplante Kosten in ein Fertigungslos gebucht werden müssen. Da dies dann meistens keine Ware im eigentlichen Sinne ist, z.B. Flugtickets, müssen diese über einen kleinen Umweg eingebucht werden. Die Vorgehensweise ist wie folgt:

- Legen Sie einen Artikel an, der nicht Lagerbewirtschaftet ist und als Mengeneinheit Ihre Mandantenwährung z.B. Euro hat.
- Definieren Sie bei diesem Artikel den Gestehungspreis mit 1,- €
- Gehen Sie in das Los, Material und buchen Sie mit ![](NachtraeglicheMat_Entnahme_ohne_Sollposition.gif) Nachträgliche Materialentnahme ohne Sollposition diesen Artikel in das Ist-Material des Loses. Als Menge geben Sie den Betrag an mit dem das Los belastet werden sollte und klicken Sie auf Entnahme.

- Gegebenenfalls ergänzen Sie in der Materialposition des Loses den Kommentar noch um eine weiterführende Bezeichnung, damit Sie später noch wissen welche Kosten das tatsächlich waren.

Material ans Lager zurückgeben

Es kommt immer wieder vor, dass zuviel Material für ein Los ausgegeben wurde.
Um dieses Material an das Lager zurückzugeben verwenden Sie im Los im Reiter Material ![](Material_im_Los.gif) den Knopf nachträgliche Materialentnahme ![](NachtraeglicheMaterialentnahme.gif). Hier ist üblicherweise Lagerabgang ![](Lagerabgang.gif) voreingestellt. Nehmen Sie hier nun den Haken heraus, so wird eine Lagerzugangsbuchung des gewählten Artikels mit der angegebenen Menge durchgeführt.<br>
**Wichtig:** Beachten Sie bitte den Unterschied zur oben beschriebenen Funktion [nachträgliches ändern der ausgegebenen Mengen](#Nachträgliches ändern bereits ausgegebener Mengen). Bei Rückgabe des Materials über die nachträgliche Materialentnahme werden die Fehlmengen **NICHT** erneut in das Los eingetragen.<br>
**Wichtiger Hinweis:** Ein großer Unterschied zwischen den beiden Rückgaben ist auch in Bezug auf die Pflege der Gestehungspreise gegeben.<br>
Wurde zuviel Material in ein Los entnommen, so wird dieses mit aktuellem Gestehungspreis vom Lager ins Los gebucht. Wird diese Menge über die [nachträgliche Materialentnahme](#Nachträgliche Materialentnahme) zurückgegeben, so wird eine Lagerzubuchung durchgeführt und dafür der zum Zeitpunkt der Zubuchung gültige Gestehungspreis verwendet. Es wird also eine weitere Zubuchung generiert. Wird nun die ursprüngliche Zubuchung, z.B. der Wareneingang verändert, so wirkt diese Veränderung bis ins Los, aber nicht mehr bei der Lagerrückgabe von Los ans Lager. Hier bleibt der alte Gestehungspreis erhalten. Dies kann soweit gehen, dass damit negative Gestehungspreise auf einem Los verursacht werden.
Das bedeutet: Müssen Mengen ans Lager zurückgegeben werden, so sollte dies in aller Regel über die [Korrektur der bereits ausgegebenen](#Nachträgliches Ändern bereits ausgegebener Mengen) Mengen durchgeführt werden. Dies hat die Eigenschaft, dass dadurch die Entnahmebuchung rückgängig gemacht wird (so als wenn es sie nie gegeben hätte) und auf Grund dessen, wirken Änderungen der ursprünglichen Zubuchung wieder wie gedacht.

<u>Nachteil:</u> Werden Lagerbestände/-bewegungen zum Stichtag betrachtet und gehen diese Entnahmen / Rückgaben über diese Stichtagsbetrachtung, so ändert sich dadurch der Lagerstand / die Warenbewegung vor/zum Stichtag, wodurch die Betrachtung des Lagerstandes / der Warenbewegung zum nachfolgenden Stichtag verfälscht wird.

### Fehlmengen bei der Warenzubuchung auflösen
<a name="Fehlmengen bei der Warenzubuchung auflösen"></a>
Fehlmengen können durch zwei verschiedene Buchungen aufgelöst werden.
1. Automatische Auflösung bei der Warenzugangsbuchung
Bei jedem Warenzugang wird geprüft, ob auf dem zugebuchten Artikel Fehlmengen eingetragen sind. Ist dies der Fall, so erscheint ein entsprechender Dialog, mit dem Sie die Zugangsmenge auf die verschiedenen Lose verteilen können. Dadurch werden automatisch die Lose aufgelöst.
![](Nachtraeglich_Fehlmengen_aufloesen.jpg)
In dieser Funktion steht Ihnen auch das Markieren mehrerer Los-Zeilen zur Verfügung. Durch Klick auf ![](Markierte_Zeilen_auf_einmal_entnehmen.gif) markierte Zeilen auf einmal entnehmen werden die markierten Positionen gebucht.
Hinweis: Wird eines der **Kieselstein ERP** Module geschlossen, so wird automatisch geprüft, ob nachträgliche Fehlmengenauflösungen gebucht wurden, wenn ja, so erscheint die Meldung 
![](Nachtraegliche_Fehlmengen_aufloesung_drucken.jpg)
Beantworten Sie diese mit Ja, so kann je Los eine Liste der aufgelösten Fehlmengen gedruckt werden. Hier wird pro Los ein eigenes Blatt ausgedruckt.

2. Manuelle Fehlmengenauflösung.
Mit der oben beschriebenen Funktion der nachträglichen Materialentnahme können, bei dem angehakten Feld **Fehlmenge reduzieren** die entsprechenden Fehlmengen reduziert / aufgelöst werden.

#### Welche Fehlmengen können aufgelöst werden

Aus verschiedenen Umständen kann es vorkommen, dass fehlende Mengen nicht (halb-) automatisch den Losen zugebucht werden. Um auch diese Daten zu sehen gibt es das Journal Auflösbare Fehlmengen. Hier sehen Sie alle Artikel und alle Lose, die Fehlmengen behaftet sind, bei deren Artikel aber Lagerstände gegeben sind.

#### Wie mit ultra dringenden Fehlmengen umgehen?
Sie kennen das. Jeder bemüht sich, dass das Material rechtzeitig da ist, aber bei der Fülle an verschiedenen Positionen, geht mal was unter. Nun muss man, gerade in größeren Firmen, dem Einkauf signalisieren, dass das aber jetzt wirklich Ultra dringend ist.
Dafür haben wir im Los die Möglichkeit geschaffen, Materialpositionen als (wirklich) dringend markieren zu können. Sie finden diese Möglichkeit im Reiter Material. Markieren Sie die wirklich dringenden Positionen und klicken Sie dann auf dringend ![](Dringend.gif). Damit werden die entsprechenden Materialpositionen als dringend markiert und in Blau dargestellt.

Im Journal Fehlmengen aller Lose, kann nur Dringende ![](Dringend2.gif)angehakt werden, womit man eine Übersicht über die "sofort" zu beschaffenden Artikel bekommt.

#### Lagerstand im Material anzeigen?
Durch aktivieren des Parameters LAGERINFO_IN_POSITIONEN, werden in den Reitern Material und Fehlmengen zusätzlich
- der aktuelle Lagerstand
- die Verfügbarkeit insgesamt
- und die bestelle Menge mit angezeigt.

Dies vor allem um Ihnen eine rasche Abschätzung über die Materialsituation des entsprechenden Loses zu ermöglichen.
Bitte nutzen Sie für eine weitere detaillierte Ansicht die theoretische Fehlmengenliste<br>
![](Theoretische_Fehlmengen_Liste.jpg)

Ist in Ihrer **Kieselstein ERP** Installation auch die Funktion der Ersatztypenverwaltung aktiviert, so wird neben dem Lagerstand der Original-Artikel auch die gesamt lagernde Menge aller direkt oder indirekt diesem Artikel zugewiesenen Ersatzartikel angezeigt. D.h. damit können Sie sehr rasch feststellen, ob Sie den Bedarf ev. durch die Ersatzartikel erfüllen können.<br>
![](Fehlmengen_mit_Lagerstand_Ersatztypen.gif)<br>
So bedeutet in obiger Darstellung dass auf dem Artikel der zweiten Zeile zwar kein Lagerstand auf den original Artikel gegeben ist. Auf den hinterlegten Ersatztypen sind jedoch ausreichend Artikel lagernd. Interessant, dass trotzdem den 260Stk der fehlenden Originalartikel bestellt sind.

#### Fehlmengen aller Lose bringt kein Ergebnis?
Bitte beachten Sie, dass Fehlmengen nur für ausgegebene Lose vorhanden sein können. Wenn Sie nun in diesem Journal einen Filter auf Lose eingeben, welche alle noch nicht ausgegeben sind, gibt es, da ja noch nicht ausgegeben und damit keine Fehlmengen, auch kein Ergebnis.<br>
Wenn Sie für eine Gruppe an Losen, egal ob schon ausgegeben oder nicht eine Übersicht über die Materialverfügbarkeit erhalten möchten, so kann dafür der [Bestellvorschlag]( {{<relref "/einkauf/bestellung/vorschlag/#wie-startet-man-einen-bestellvorschlag" >}} ) mit einer Einschränkung auf die gewünschten Lose verwendet werden.<br>
Ev. ist aber auch das Thema [Minderverfügbarkeit]( {{<relref "/warenwirtschaft/stueckliste/#minderverf%c3%bcgbarkeit" >}} ) in den Stücklisten für Sie interessant.

[Stückrückmeldung]( {{<relref "/fertigung/losverwaltung/stueckrueckmeldung">}} )

#### Rücknahme der gesamten Losausgabebuchung
<a name="Rücknahme der Losausgabe"></a>
Ist ein Los noch nicht teilweise erledigt, so kann durch Klick auf Ändern in den Kopfdaten des Loses, der Status des Loses von ausgegeben / in Produktion auf angelegt zurückgenommen werden. Das bedeutet, dass der gesamte Inhalt der Materialbuchungen vom Los wieder an das Lager zurückgegeben wird.

#### Los erledigen, ans Lager abliefern
<a name="Los erledigen, abliefern"></a>
Mit dem oberen Modulreiter ![](Ablieferung.gif) gelangen Sie zur Funktionalität der Ablieferung der Fertigung an das Lager. D.h. die Fertigmeldung, die Erledigung der tatsächlich gefertigten Mengen.
Durch Klick auf Neu wird eine weitere Teilerledigung des Loses durchgeführt. Es werden die jeweils offenen Mengen vorgeschlagen.
Bei der Erledigung der Los beachten Sie bitte die Checkbox Erledigt. Diese wird automatisch vorbesetzt, wenn die nun insgesamt zugebuchte Menge der Soll-Losgröße entspricht. Ist ein Los vollständig erledigt, so können darauf keine Zeiten mehr gebucht werden. Wenn der Haken bei ![](Vollstaendig_Erledigen.gif) herausgenommen wird, so ist das Los im Status teilerledigt und kann daher noch bebucht werden. [Siehe dazu bitte auch enthaltene Stücklisten ausgeben]( {{<relref "/warenwirtschaft/stueckliste/#enthaltene-st%c3%bccklisten-ausgeben" >}} ).

[Ablieferung mit Seriennummern siehe bitte.](#Seriennummern Generator)

Bedienung der [Serien]( {{<relref "/docs/stammdaten/artikel/serienchargennummern/#wozu-dienen-seriennummern" >}} ) - / [Chargennummern]( {{<relref "/docs/stammdaten/artikel/serienchargennummern/#wozu-dienen-chargennummern" >}} ) siehe bitte dort.

Für die Ablieferung mit [Geräteseriennummern siehe bitte]( {{<relref "/fertigung/losverwaltung/geraeteseriennummern">}} ).

Für die Berechnung der Material-Ablieferpreise werden IMMER die Gestehungspreise des auf das Los gebuchten Materials entsprechend seiner Zubuchung verwendet. Bitte beachten Sie, dass der kalkulatorische Preis (Kalk.Preis) aus der Stückliste KEINE Auswirkung auf diese Werte hat.
Für die Ermittlung der Personalkosten werden die auf das Los gebuchten Zeiten verwendet, wobei bei Teilablieferungen noch die Satzgrößen berücksichtigt werden (müssen). D.h. während der Teilablieferungen werden die Zeiten der Arbeitsgänge anhand der Teil-Stunden welche aus den Stunden pro Stück (Sollsatzgröße) errechnet werden, anteilig auf die Ablieferung angerechnet. Erst bei vollständiger Erledigung werden alle Stunden als auf dem Los verbraucht angenommen Für die Maschinenzeitkosten wird im Sinne der Stundenanzahl in gleicher Weise vorgegangen.
Als Basis für den Stundensatz der Mitarbeiter-Zeiten kommt der Parameter [PERSONALKOSTEN_QUELLE]( {{<relref "/docs/stammdaten/personal/#personalkosten-in-den-nachkalkulationen" >}} ) zur Anwendung.<br>
Für die Maschinenstunden kommt der unter der jeweiligen Maschine zum jeweiligen Zeitpunkt gültige Stundensatz zur Anwendung. [Siehe bitte]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung/#welche-kosten-werden-f%c3%bcr-die-maschinenstunde-angesetzt" >}} )

Um nun bei groben Buchungsfehlern, insbesondere im Sinne der richtigen zeitlichen Reihenfolge, trotzdem und nachträglich die richtigen Kosten den Losen und den damit verbundenen nachträglichen Belegen, z.B. Lieferschein, die richtigen Werte zur Verfügung stellen zu können, kann der Zeitpunkt der Losablieferungsbuchung gegebenenfalls manuell korrigiert werden.
Bitte beachten Sie, dass Sie damit in die zeitliche Abfolge der Zubuchung eingreifen. Dies kann, insbesondere in der Gestehungspreisberechnung und im Sinne der Inventur entsprechende Auswirkungen haben. Ändern Sie diesen Zeitpunkt nur wenn es unbedingt erforderlich ist.

#### Kann ein Los auch überliefert werden?
<a name="Überlieferung"></a>
Ja. Wenn Aufgrund der eingegebenen Gesamtmenge eine Überlieferung des Loses erfolgt, also mehr produziert wurde als in der Losgröße geplant war, so erscheint folgender Dialog.

![](Los_Ueberliefern.gif)
- Mit nein wird die Ablieferung abgebrochen.
- Bei Ja wird die Überlieferung ohne Änderung der Los-Solldaten zugebucht. D.h. wenn Sie z.B. aus der Produktion eines Zuschnittsvorganges (z.B. werden von einer Stange Scheiben herunter geschnitten) mehr erhalten als errechnet, so ändert sich ja der Materialbedarf nicht, "nur" die erhaltene Stückzahl ist höher.
- Bei Abliefern + Losgröße ändern, wird die Losgröße auf die neue Stückzahl erhöht **<u>und</u>** die Solldaten und die Materialentnahmen auf das Los werden auf die neue Losgröße angepasst.

Beachten Sie dazu bitte auch die Einstellung Überlieferbar in den Kopfdaten der dazugehörenden Stückliste. Diese Einstellung der Stückliste dient nur der Steuerung der Überlieferung an den Zeiterfassungsterminals.

#### Rücknahme eines erledigt(en) Loses?
Es kommt immer wieder vor, dass ein Los versehentlich vollständig erledigt wurde.
Dies kann, bei entsprechenden Rechten, über den Reiter Kopfdaten und Klick auf ändern, mit Bestätigung der nachfolgenden Frage, wieder in den Status teilerledigt zurückgenommen werden.
Der wesentliche Unterschied ist, dass ein vollständiges Los keine Fehlmengen hat, egal ob die richtige Menge im Los verbaut wurde. Ein teilerledigtes Los, auch wenn die Abliefermenge >= der Losgröße ist, hat seine entsprechenden Fehlmengen.

#### Welche Rechte sind für die Ablieferung erforderlich?
Um direkt im Reiter Ablieferung das Los auf erledigt zu setzen, ist das Recht FERT_DARF_LOS_ERLEDIGEN erforderlich.
Hat der Anwender dieses Recht nicht, so kann auch die manuelle Ablieferung aus dem Menüpunkt Bearbeiten, Manuell Erledigen nicht mehr aufgerufen werden.

#### Können aus der Ablieferung Etiketten gedruckt werden?
Ja. Klicken bei der gewünschten / entsprechenden Ablieferung auf ![](Ablieferetikette.gif) Etikette drucken.

Im nun erscheinenden Druckdialog wird die Anzahl der Exemplare ![](Ablieferetiketten_Exemplare.gif) auf Basis der Abliefermenge durch die im Artikel eingepflegte Verpackungsmenge (Artikel, Sonstiges) errechnet. Ist hier keine Menge angegeben, so werden die Exemplare mit 1 vorbesetzt. Ein unrundes Ergebnis wird aufgerundet.

#### Wieso ergeben sich bei der Losablieferung unterschiedliche Gestehungspreise?
Bei jeder Losablieferung wird versucht den Gestehungspreis, getrennt in Arbeit und Material, der abgelieferten Mengen möglichst exakt zu bestimmen.<br>
Bei Teilablieferungen ergibt sich hier, dass, eventuell nur teilweise, bereits mehr Material bzw. Arbeitszeit auf das Los gebucht ist, als tatsächlich für diese Teilablieferung benötigt wird. Daraus folgt, dass bei Teilablieferungen die Mengen immer nur bis zur sogenannten Sollsatzgröße berücksichtigt werden. Handelt es sich um Material, so kann, bei richtigen Stücklisten, die Sollsatzgröße nicht unterschritten werden, da ja damit eines der Geräte nicht vollständig wäre, handelt es sich um Arbeitszeit, so haben Ihre Mitarbeiter schneller als geplant gearbeitet.
Werden nun Teilabgelieferte Lose vollständig erledigt, so werden auf die letzte Ablieferung alle verbleibenden Kosten aufgerechnet.
Ein Beispiel:
Losgröße 100 Stück
98 Stück können sofort fertiggemeldet werden, da sie bei Inbetriebnahme sofort funktionieren. Zwei Geräte müssen repariert werden. Nun wird die gesamte verbrauchte Mehrzeit und auch das zusätzlich verbrauchte Material auf die letzten zwei Geräte (genauer die letzten zwei Stück der Ablieferung) verteilt.
Stellt sich heraus, dass eine Unterlieferung stattfindet, weil z.B. eines der Geräte nicht verwendet werden kann, so werden die gesamten Kosten auf das letzte Gerät verteilt.
Diese Thematik ist nicht nur bei Geräten gegeben sondern selbstverständlich auch bei Teilen aus dem Maschinenbau, weil Maße nicht gehalten werden konnten, Lunker auftreten usw..
Werden um beim obigen Beispiel zu bleiben nur die 98 Stück zurückgemeldet / abgeliefert, so werden auf die letzte Ablieferung die gesamten Restkosten des Mehrverbrauches, Mehraufwandes aufgerechnet.
Daraus können sich stark unterschiedliche Gestehungskosten je Ablieferung ergeben. Umgekehrt hat diese strenge Betrachtung den Vorteil, dass z.B. Anlaufkosten nur in den ersten Ablieferungen enthalten sind oder dass Sie sehen dass die eine Fertigungsschicht zu anderen Kosten arbeitet als die andere usw..
Da dies ohne die Stückrückmeldung die genauest mögliche Betrachtung ist, hängt die Richtigkeit der Teil-Ablieferungen von der Genauigkeit Ihrer Sollvorgaben ab. Werden, so wie in manchen Betrieb üblich, Sollzeiten von einer Minuten angegeben und am Schluss werden für fünf Stück (=Losgröße) 10Stunden benötigt und es wird jedes Stück einzeln fertig gemeldet, so ergibt sich, da ja Aufgrund der Sollsatzgröße nur je eine Minute auf die ersten vier Lose gebucht werden darf, für die letzte Ablieferung ein Zeitaufwand von 295Minuten.
Achten Sie daher auf möglichst richtige Sollzeiten nur dann machen die Soll-/Ist-Vergleiche wirklich Sinn.

Wenn der Parameter LOSABLIEFERUNG_GESAMTE_ISTZEITEN_ZAEHLEN gesetzt ist, so zählen für den Ablieferwert nur die Zeiten bis zum Abliefzeitpunkt. Das bedeutet, dass die erste Loasblieferung alle Zeiten bis zum ersten Ablieferzeitpunkt enthält und alle weiteren Ablieferungen jeweils die Zeiten ab dem vorherigen Ablieferzeitpunkt bis zum aktuellen Ablieferzeitpunkt beinhalten. Die letzte Ablieferung (Wenn das Los erledigt ist) enthält die Zeiten ab der vorletzten Ablieferung + Zukunft (=Nachträglich gebuchte Zeiten).

#### Ändern der Gestehungspreise eine bereits abgelieferten Loses
Manchmal ist es erforderlich, die errechneten Ablieferpreise eines Loses nachträglich zu verändern.

Hier empfiehlt sich grundsätzlich, die Ursachen richtig zustellen. D.h. wenn Wareneinstandspreise falsch waren, so muss der Wareneingang korrigiert werden. Siehe dazu bitte [Wareneingang]( {{<relref "/einkauf/bestellung/#wie-h%c3%a4ngen-wareneingang-wareneingangsposition-und-bestellposition-zusammen" >}} ) bzw. [Korrektur der Handlagerbewegungen]( {{<relref "/docs/stammdaten/artikel/#wie-kann-der-preis-einer-handbuchung-ge%c3%a4ndert-werden" >}} ).<br>
Oder es wurden falsche Mitarbeiter- bzw. Maschinenzeiten gebucht. Auch hier sollte bevorzugt die Basis der Berechnungsdaten richtig gestellt werden.

Nach dieser Korrektur muss unter Umständen die Neukalkulation der Ablieferpreise angestoßen werden. Verwenden Sie dazu Button ![](Losablieferung_neu_kalkulieren.gif) neu kalkulieren.

In einigen Fällen würde die Aufrollung der fehlerhaften Ursprungsdaten zu kompliziert werden. In diesen Fällen verwenden Sie bitte die manuelle Korrektur des Gestehungspreises im Modul Artikel, oberer Modulreiter Lager. Bitte beachten Sie, dass bei dieser Korrektur die Verkettung der Warenbewegungen durchbrochen werden muss und damit verloren geht.

#### Lose manuell erledigen / enterledigen
Da die manuelle Erledigung von Losen als Möglichkeit der Unterlieferung von Losen gedacht ist, werden bei der manuellen Erledigungsbuchung keine Abbuchungen vom Lager ins Los durchgeführt. Lediglich bei negativen Sollmengen, also geplanten Rückgaben aus dem Los ans Lager, werden Lagerzubuchungen dieser Mengen durchgeführt.
Wird der Erledigungsstatus eines Loses aufgehoben, so werden keinerlei Lagerbuchungen durchgeführt. Wenn ein Los manuell erledigt wird, werden alle Ablieferungen nachkalkuliert.
Siehe dazu bitte auch Änderung der [Losgröße](#Ändern der Losgröße).

#### Können Lose automatisch erledigt werden?
Insbesondere in Umgebungen mit Zeiterfassungsterminals ist der Wunsch, dass die Lose sich automatisch erledigen.<br>
Hierbei ist zu beachten, dass es in diesen Konstellationen auch immer wieder dazu kommt, dass offline Geräte, z.B. die Zeiterfassungsstifte, verwendet werden, um effizient Zeiten erfassen zu können. Das bedeutet, dass zwar die Ablieferbuchung am Terminal gemacht wird und somit die gefertigte Stückzahl im Lager gebucht wird, aber, aufgrund der Offline Erfassung sind unter Umständen noch nicht alle Zeitbewegungsdaten eines Loses auf das Los gebucht. Würde nun das Los sofort abgeschlossen werden, würden a.) die Zeiten und somit die Kosten fehlen und b.) könnten die Zeiten nicht mehr nachträglich auf das vollständig erledigte Los gebucht werden.
Daher haben wir einen Automatik-Job geschaffen, der einmal täglich (in der Nacht) prüft ob Lose erledigt werden sollen / können.<br>
D.h. wenn ein Los eine Abliefermenge hat, die gleich oder größer der Losgröße ist und wenn die letzte Ablieferbuchung länger als drei Tage (siehe Automatik im Modul System) vergangen ist, so wird versucht das Los automatisch zu erledigen. Kommt es bei der Erledigungsbuchung zu Fehlern, so wird in diesem Lauf das Los übersprungen. Am nächsten Tag / beim nächsten Lauf wird erneut versucht das Los zu erledigen. Wenn in der Zwischenzeit der Fehler verschwunden ist, wird das Los dann erledigt.<br>
D.h. wenn dieser Automatikjob aktiviert ist, sollte es keine alten nicht erledigten Lose geben. Bleiben Lose stecken, so prüfen Sie deren Erledigung manuell (Menü, Bearbeiten, Manuell Erledigen)<br>

#### Mit welchem Datum wird ein Los erledigt?
Bei der automatischen Erledigung wird das Los mit dem Datum und der Uhrzeit der Erledigungsbuchung erledigt.

Wird ein Los manuell erledigt, so erscheint die Frage:

![](Los_Erledigen.jpg)

Damit haben Sie die Möglichkeit das Erledigungsdatum von Losen auf die zuletzt durchgeführte Ablieferung zu setzen. Z.B. da vergessen wurde im letzten Monat die Lose entsprechend zu erledigen.

Hintergrund: In den Auswertungen wird immer wieder auch auf das Erledigungsdatum Bezug genommen, weshalb wichtig ist, dass dieses entsprechend richtig ist.

#### mehrere Lose erledigen / stornieren
Mit dem unteren Modulreiter ![](mehrere_Lose_erledigen.gif) Erledigen/Stornieren können mehrere Lose in einem Zug erledigt werden.
Bitte beachten Sie, dass bei der Erledigung die ev. noch offenen Abliefermengen nachgebucht werden. In diesem Zuge werden auch Materialentnahmebuchungen aus dem Lager ins Los gemacht.
Um alle markierten Lose zu erledigen klicken Sie auf den grünen Haken.

Weiters können Los die im Status angelegt sind gemeinsam markiert und damit storniert werden. Sollten auch Lose markiert sein, die einen anderen Status als angelegt haben, werden diese nicht verändert.

#### Dokumente zur Ablieferung hinterlegen?
Wenn der Artikel eines Loses als Dokumentenpflichtig gekennzeichnet ist, so können/müssen nach einer erfolgten Ablieferung auch Dokumente zur jeweiligen Ablieferung hinterlegt werden. Das müssen bedeutet, dass ein Los, bei dem der Artikel als Dokumentenpflichtig gekennzeichnet ist, erst dann vollständig erledigt werden kann, wenn hinter jeder Ablieferungsbuchung auch ein entsprechendes Dokument hinterlegt ist.

**Hinweis:** Das Dokumentenpflichtig nur für Artikel hinterlegt werden kann, kann für die Ablieferung einer Materialliste kein Dokument hinterlegt werden. Hier kommt dazu, dass diese Dokumente meistens nur für Chargengeführte Artikel benötigt werden, welche dann als Artikel zu bewirtschaften sind.

**Info:** Das Icon zur Dokumentenablage wird erst angezeigt, wenn es eine Ablieferungsbuchung gibt.

### Sollsatzgröße
<a name="Sollsatzgröße"></a>
Dies ist jene Menge, die sich aus der Sollmenge des Loses dividiert durch die Losgröße, multipliziert mit der Abliefermenge ergibt. In anderen Worten, diese Menge an Material brauche ich um eine bestimmte Abliefermenge überhaupt produzieren zu können, wenn die Stücklisten richtig sind.

#### Was bedeutet Sollsatzgröße unterschritten?
Wenn eine Ablieferbuchung gemacht wird, so benötigen Sie, für jedes Stück welches produziert wurde (analog natürlich auch für jeden Liter der gemischt wurde) eine durch die Los-Sollmengen vorgegebene Anzahl / Menge an Material. Üblicherweise wird diese Menge durch die Stückliste vorgegeben.
Wird nun eine Ablieferbuchung gemacht, so muss für die Anzahl der produzierten Baugruppen/Geräte/Rezepte je Satz das Material im Los gebucht sein. Ist dieses Material nicht vorhanden, so kann das Produkt auch nicht erzeugt werden. Soweit zumindest die Theorie.
Wenn Sie nun Lose abliefern und bekommen die Meldung Sollsatzgröße unterschritten, so kann diese Meldung bei entsprechenden Rechten übersprungen werden.
Wenn Sie dies machen, so müssen Sie sich bewusst sein, dass nun diese Mengen in der Mengen UND Kostenbetrachtung fehlen.
In anderen Worte, es fehlen sowohl die Kosten im Los, es wurde also zu billig produziert, und es stimmt Ihr Lagerstand nicht, da ja die Mengen nicht vom Lager auf das Los gebucht wurden.
Dies ist auch der Grund, warum z.B. Ablieferbuchungen am Terminal nur mit ausreichenden Mengen für die entsprechend abgelieferte Produktmenge gemacht werden dürfen.
Oder auch in anderen Worten, wenn Sie mit Sollsatzgröße unterschritten weiterarbeiten, machen Sie wundersame Lagervermehrung und im Endeffekt stimmt auch der Lagerstand nicht mehr.

## Nachkalkulieren
Gegenüberstellung der Soll und Ist Arbeitszeiten in Stunden und Kosten, sowie der Material Ist und Soll Kosten des gewählten Loses.<br>
Beachten Sie bitte die [Art der Nachkalkulation]( {{<relref "/docs/stammdaten/personal/#personalkosten-in-den-nachkalkulationen" >}} ).

Im Ausdruck der Nachkalkulation wird auch der Arbeitszeitfortschritt der Personal- und der Maschinenzeiten angezeigt, mit einem entsprechend farbigen Verlauf.
Von der Idee her ist der jeweilige Balken 200% (Ist/Soll) breit. Solange er unter 90% ist, ist er grün, dann wird er orange ab 100% hellrot und ab 110% rot.
![](Zeitfortschritt.gif) Zusätzlich wird mit dem Haken neben der jeweiligen Tätigkeit angezeigt, dass alle Arbeitsgänge dieser Tätigkeit bereits als fertig eingetragen sind.
Die Anzeige der Nachkalkulation kann auch von den Terminals aus aufgerufen werden um den Mitarbeitern eine entsprechende Fortschrittsinformation zu vermitteln. Denken Sie daran, nur die Informationen z.B. Stundenvergleich an die Mitarbeiter zu übergeben die auch für Sie aus Sicht der Unternehmensführung erwünscht sind.

### Drucken
Unter Fertigung, Drucken stehen ihnen die wesentlichsten Formulare für Ihre Fertigung zur Verfügung.

#### Fertigungsbegleitschein
Drucken Sie hier den Fertigungsbegleitschein, manchmal auch Arbeitsschein oder ähnlich genannt aus. Auf diesem Papier sind alle Arbeitsgänge des Loses, die Vorgabezeiten und die Barcodes für die Erfassung auf der BDE Station oder mittels Zeitstiften enthalten.<br>
Die Gestaltung dieses Papiers wird üblicherweise an Ihre Bedürfnisse angepasst.
#### Ausgabeliste
Dies ist die Liste mit den laut **Kieselstein ERP** auf das Los entnommenen Artikeln.
#### Theoretische Fehlmengen Liste<br>
Die theoretische Fehlmengenliste wird vor allem vor der tatsächlichen Materialbuchung verwendet. Hier erhalten Sie eine Übersicht über alle noch fehlenden Materialien dieses Loses, um entscheiden zu können, ob der Produktionsstart tatsächlich erfolgen kann.

### Zeiten Buchen
Zum Buchen der Personal und Maschinenzeiten siehe bitte [Zeiterfassung]( {{<relref "/fertigung/zeiterfassung" >}} ).

#### Begriffe und Schlagwörter im Zusammenhang mit der Fertigungsverwaltung
- Fertigungsauftrag
- Los / Losverwaltung
- Fehlmenge
- Ausgeben / Ausgegeben
- In Produktion
- Abgeliefert
- Erledigt
- Sollmenge
- Satzgröße / Sollsatzgröße
- Nachträgliche Entnahme
- Produktionsstart, Beginn
- Durchlaufzeit
- Ende, Abliefertermin, Produktions Ende

### Berechnung des Beginns, der Durchlaufzeit und des Endes / Abliefertermins.
<a name="Berechnung Termine"></a>
Bei der Internen Bestellung wird nach dem Konzept der sogenannten Rückwärts-Terminierung vorgegangen.
Auftrags-Liefertermin<br>
minus Kunden-Lieferdauer (siehe Kunde, Konditionen) der Lieferadresse des Auftrags bzw. des Forecastauftrags. Siehe dazu bitte auch [Reservierungen, Termine]( {{<relref "/docs/stammdaten/artikel/#reservierungen-lieferdauer-freigegebene--nicht-freigegebene-auftr%c3%a4ge-bzw-forecast" >}} )<br>

Ergibt Produktionsendtermin = Abliefertermin<br>
minus Durchlaufzeit in Kalendertagen<br>
Ergibt Produktionsstarttermin<br>
    minus Fertigungsvorlaufzeit (Mandantenparameter)<br>
    Ergibt Produktionsendtermin des Unterloses<br>
        Dies wird solange fortgesetzt, bis die Stücklistenhierarchie vollständig aufgelöst ist.<br>
minus Bestellungsvorlaufzeit Eigenfertigung (Mandantenparameter)<br>
Ergibt gewünschter Liefertermin der Einkaufsteile<br>

**Beachte** dazu auch den Parameter AUTOMATISCHE_ERMITTLUNG_AG_BEGINN für die automatische Einplanung der Arbeitszeiten (seriell).

### Wann wird die Stückliste aktualisiert?
Eine Stückliste kann manuell über Bearbeiten, Stückliste aktualisieren aktualisiert werden. Dies ist nur für Lose im Status angelegt möglich. Die Aktualisierungsbuchungen werden nur durchgeführt, wenn die Stückliste tatsächlich geändert wurde. Dies wird anhand des Anlage und Änderungsdatums des Loses bzw. der Stückliste bestimmt.

Zusätzlich wird vor der Losausgabe der Änderungsstatus der Stückliste geprüft und es erscheint ein entsprechender Hinweis.

Es werden hier sowohl das Änderungsdatum des Arbeitsplans als auch der Stückliste geprüft.

#### Kann der Arbeitsplan nachträglich aktualisiert werden?
Ja. Der Arbeitsplan kann solange aktualisiert werden, bis das Los vollständig erledigt ist. Es wird bei der Aktualisierung so vorgegangen, dass alle Solldaten die aus den Stücklisten gekommen sind aktualisiert werden. Sind in der Stückliste Arbeitsgangpositionen hinzugekommen, so werden diese eingefügt, sind Arbeitsplanpositionen entfallen, so werden diese auf Null gesetzt. Nachträglich (N) im Los hinzugefügte Arbeitszeitpositionen werden nicht verändert.

Sollten sich wesentliche / große Teile der Solldaten des Arbeitsplans geändert haben, so löschen Sie am besten zuerst die Positionen des Arbeitsplans im Fertigungsauftrag (Reiter Zeitdaten) und wählen dann Bearbeiten, Arbeitsplan aktualisieren.

#### Können alle Lose auf einmal aktualisiert werden?
Ja, [siehe dazu]( {{<relref "/warenwirtschaft/stueckliste/#aktualisierung-aller-lose" >}} )

#### Wie wird mit dem fehlenden Material bei der Los-Aktualisierung umgegangen?
Ist ein Los bei der Aktualisierung bereits im Status ausgegeben, so muss auch die Warenbewirtschaftung des Loses nachgezogen werden. D.h. wird/werden die Materiallisten der Lose aktualisiert, so wird üblicherweise auch versucht das Material zu aktualisieren. D.h. Ware die im Los überflüssig ist, da sich die Stückliste geändert hat, wird an das Lager zurückgegeben. Material welches fehlt wird versucht vom Lager zu nehmen bzw. werden Fehlmengen erzeugt. In einigen Anwendungen, insbesondere in Kombination mit dem [Lagercockpit]( {{<relref "/docs/stammdaten/artikel/lagercockpit/" >}} ) ist es nicht erwünscht, dass diese Funktion automatisch ausgeführt wird. Stellen Sie daher hier den Parameter BEI_LOS_AKTUALISIERUNG_MATERIAL_NACHBUCHEN auf 0\. Es werden damit keine Materialbewegungen durchgeführt. Die Umbuchungen sollten dann im Lagercockpit durchgeführt werden.

##### Zurücknehmen der Losausgabe?
Durch Klick auf Ändern, kann ein ausgegebenes Los wieder zurückgenommen werden. Das bedeutet, dass
1. Das Material wieder dem Lager zugebucht wird
2. Die Positionen der Stückliste neu in das Los übernommen werden.

Bitte beachten Sie, dass hierdurch die Solldaten des Loses automatisch aktualisiert werden.

Hinweis zu nachträglich manuell hinzugefügten Positionen. Diese sind mit einem eigenen Status (N) gekennzeichnet und werden daher auch bei der Aktualisierung der Stückliste nicht aus dem Los gelöscht.

#### Wie können die Preise von Nicht lagerbewirtschafteten Artikeln im Los aktualisiert werden?
Üblicherweise werden z.B. für Stromkosten und ähnliche fixen Kosten, nicht lagerbewirtschaftete Artikel angelegt. Diese Artikel werden oft in Hilfsstücklisten zusammengefasst, in die jeweilige Stückliste eingebunden und so auf das Los gebucht. Wenn nun festgestellt wird, dass bei einem bereits ausgegebenen oder erledigten Los diese Werte / Preise falsch sind, so ist folgende Vorgehensweise für die Aktualisierung des Ist-Preises im Los zielführend.
1. Wenn das Los noch komplett zurückgenommen werden kann (siehe oben), so nehmen Sie dieses vollständig zurück, ändern Sie **danach** den Gestehungspreis des nicht lagerbewirtschafteten Artikels und geben Sie danach das Los wieder aus. Beachten Sie, dass diese Preisänderung auch für alle zukünftigen Ausgaben gilt.
2. Kann das Los nicht zurückgenommen werden, so gehen Sie bitte wie folgt vor:
    - Das Los ist im Status ausgegeben oder teilerledigt (Ablieferungen müssen dafür nicht zurückgenommen werden)
    - im Reiter Material wählen Sie den gewünschten Artikel und geben diesen über ![](Ausgabemenge_aendern.gif) Ausgabemenge ändern an das Lager zurück (die Ausgabemenge auf 0,00 setzen).
    - korrigieren Sie nun den Gestehungspreis des nicht lagerbewirtschafteten Artikels
    - im Reiter Material des Loses geben Sie nun über ![](nachtraegliche_Materialentnahme.gif) nachträgliche Materialentnahme den Artikel wieder aus.
  
    Damit haben Sie den richtigen Preis / Wert auf dem Los.

#### Wie wirken Änderungen der Losgrößen und der Produktionstermine?
Auch bei diesen Änderungen werden die Daten neu aus der Stückliste übernommen, um die Reservierungsdaten entsprechend zu aktualisieren.

#### Meine in der Stückliste eingetragenen Zeiten sind im Fertigungsauftrag unter Material
Bei der Übernahme der Stücklisten in den Fertigungsauftrag werden nur die Positionen des Arbeitsplanes in die Zeitdaten (= Zeitvorgaben) des Loses übernommen. Alle anderen Artikel werden als Material übernommen, auch wenn dies Arbeitszeitartikel sind.

#### Was bedeutet Einheit kann nicht konvertiert werden ?
Diese Fehlermeldung bedeutet, dass in Ihrer Stückliste eine Positionseinheit (Std) nicht in die Zieleinheit (h) des Artikels konvertiert werden kann.

![](Fehlermeldung_Einheit.gif)

Daher kann das Los nicht gültig abgespeichert werden.

Um das Los abzuspeichern, korrigiere die Positionseinheit der Stückliste.

Hinweis: Wenn Sie die Stücklistenposition nicht aufgrund der angezeigten Einheit erkennen können, so gehen Sie die Stücklistenpositionen durch. Auch hier erhalten Sie bei unstimmigen Einheiten die oben dargestellte Fehlermeldung.

Sind Positionsmengeneinheit und Zielmengeneinheit konvertierbar, aber im System noch nicht hinterlegt, so definieren Sie bitte die Einheiten [Konvertierung]( {{<relref "/docs/stammdaten/artikel/mengeneinheit">}} ).

#### Pflegefunktionen in der Fertigung
Im Fertigungsmodul gibt es, bei entsprechender Berechtigung, den Menüpunkt Pflege.

Die hier angeführten Programmfunktionen sollten nur auf Anweisung der **Kieselstein ERP** Techniker aufgerufen werden.

**Wichtig: Diese Pflegefunktionen wirken immer für alle Mandanten.**

#### Kommentar zu einem Fertigungsauftrag
Der Kommentar eines Fertigungsauftrages kann jederzeit bearbeitet werden. Verwenden Sie dazu den Menüpunkt Bearbeiten, Kommentar. Der bisher eingegebene Kommentar wird direkt in den Kopfdaten angezeigt.

Hinweis: Beim Anlegen des Loses kann der Kommentar auch direkt eingegeben werden.

Es ist jedoch oft der Fall, dass Informationen zur Produktion dieses Loses erst während der Fertigung eingepflegt werden können. Da zu diesem Zeitpunkt der Status des Loses in der Regel nicht verändert werden sollte, steht diese Funktion auch über das Menü zur Verfügung.

#### Produktionsinformation zu einem Fertigungsauftrag
Neben dem Kommentar steht für einen Fertigungsauftrag auch die Produktionsinformation zur Verfügung. Auch Sie kann jederzeit bearbeitet werden. Verwenden Sie dazu den Menüpunkt Bearbeiten, Produktionsinformation.

Um die Produktionsinformation zu drucken, verwenden Sie bitte den Menüpunkt Los, Drucken, Produktionsinformation.

Beim Ausdruck der Produktionsinformation werden auch die in der Dokumentenablage hinterlegten Bilder die im Format gif/jpg/bmp/png/tiff/pdf sind mit ausgedruckt. Der Name (des Dokuments), der Dateiname und die Schlagworte werden mit an den Report übergeben. Sollten nur bestimmte Dokumente gedruckt werden, so wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer. Er wird das Druckformular entsprechend für Sie anpassen.

#### Soll-Zeiten / Ist-Zeiten
Grundsätzlich sollte, schon aus Gründen der exakten Nachkalkulation die exakte Ist-Zeiten Erfassung verwendet werden. Es stehen dafür viele Möglichkeiten zur Verfügung. Diese Ist-Zeitenerfassungsmöglichkeiten können auch gerne jederzeit erweitert werden.
Da es in sehr sehr seltenen Fällen effizienter sein kann, trotzdem die Soll-Zeiten als Ist-Zeiten zu verwenden, kann dieses Verhalten in **Kieselstein ERP** über den Parameter ISTZEITEN_GLEICH_SOLLZEITEN gesteuert werden.
Wird diese Funktion verwendet, so wird davon ausgegangen, dass die Sollzeiten des Arbeitsplanes der Stückliste, genauer des jeweiligen Loses auch als Ist-Zeiten verwendet werden. Für die im Los herangezogenen Ist-Zeiten werden die Sollzeiten des jeweiligen Arbeitsganges unter Berücksichtigung der abgelieferten Menge verwendet.

Ein Beispiel:<br>
Geplante Rüstzeit 1Std, geplante Stückzeit 10min, Losgröße 10Stk -> ergibt Sollzeit 2,67Std.
Werden nun 8Stk zurückgemeldet (Ablieferung) so wird eine Ist-Zeit von 2,67/10*8Std = 2,14Std angenommen.
Wird ein Los dann mit dieser Menge auf vollständig erledigt gesetzt, so bleibt Wertmäßig das verbrauchte Material am Los und es wird nur die Ist-Zeit zur Basis der Ablieferung (also die 2,14Std) verwendet.
**Hinweis:** Um trotzdem, zumindest eine gewissen Sicherheit bei den Personal-Zeitdaten zu erreichen, sollten Sie immer die Gegenüberstellung der im Zeitraum gebuchten Loszeiten zu den Anwesenheitszeiten Ihrer produktiven Mitarbeiter im Auge haben (Journal, Ablieferungsstatistik).

#### Materialentnahme bei Ausgabe oder bei Erledigung?
<a name="Materialentnahme bei Ausgabe oder bei Erledigung"></a>
In manchen Fertigungsbetrieben sind die Bereiche Lager und Fertigung sehr stark durchmischt. Daraus ergibt sich, dass der physikalische Lagerstand nicht klar zum Material in Fertigung abgegrenzt werden kann. Manchmal ist dies auch aufgrund des gegebenen Fertigungsprozesses günstiger. Daher kann in der Stückliste definiert werden, wann diese Materialbuchung im Los durchgeführt werden sollte.
Bitte beachten Sie, dass bei Materialbuchung erst bei Ablieferung für die in der Fertigung im Los noch nicht entnommenen Mengen, also der fehlenden Mengen, entsprechende Fehlmengen gebucht werden.
Ist die Materialentnahme auf Buchung bei Erledigung gesetzt, so sollten in der Regel für die abgelieferten Mengen keine Fehlmengen mehr gegeben sein. D.h. wenn die Meldung Sollsatzgröße unterschritten erscheint, sollten die Materialbuchungen geprüft werden.
Wichtig: Wird die Materialentnahme erst bei Ablieferung gebucht, so wird vor der eigentlichen Ablieferungsbuchung die Materialentnahmebuchung durchgeführt. Wird diese Ablieferung abgebrochen, so bleiben die bereits durchgeführte Materialentnahmen auf dem Los bestehen, da dies exakt Ihre Erfassung der Rückmeldungen ist. D.h. es sollten zumindest täglich bzw. bei jedem Schichtwechsel die entsprechenden Stückzahlen zurückgemeldet / abgeliefert werden.

#### Material automatisch bei Ablieferung nachbuchen
<a name="Materialnachbuchen"></a>
Ist der Parameter BEI_LOS_ERLEDIGEN_MATERIAL_NACHBUCHEN eingeschaltet (=1) so bewirkt dies, dass vor jeder Ablieferungsbuchung versucht wird, dass ev. noch fehlende Material, bezogen auf die Abliefermenge, aus dem Lager nachzubuchen. 
Gerade in Verbindung mit dem oben angeführten Punkt, Materialentnahme erst bei Erledigung, bewirkt dies, dass das benötigte Material erst zum Zeitpunkt der Fertigmeldung des Loses, einer Teilmenge, vom Lager gebucht wird. Ist Ihre Fertigung so organisiert, dass Materiallager und Fertigungsbereich nicht wirklich getrennt werden kann, so ist dies eine sehr praktische Funktion, da damit der physikalische und der Lagerstand laut **Kieselstein ERP** übereinstimmen. Somit können Sie jederzeit eine entsprechende Zwischeninventur durchführen. Zugleich unterstützt diese Funktion die Flexibilität dieser Unternehmen, da das Material nicht für zwar eingeplante und ausgegebene Lose verbraucht wird, sonder eben erst, die z.B. auf Zuruf fertiggestellten Lose bebucht.

#### keine automatische Materialbuchung
<a name="keine automatische Materialbuchung"></a>
Ist diese Eigenschaft in der Stückliste gesetzt, bedeutet dies dass wenn ein Los ausgegeben wird, keine Materialbuchung gemacht wird.
Wenn man nun erreichen möchte, dass für bestimmte Stücklisten / Produkte IMMER das Material manuell vom Lager in die Fertigung gebucht werden sollte, so:
1. haken Sie bitte keine automatische Materialbuchung an
2. Materialbuchung bei Ablieferung bitte nicht anhaken

Das bewirkt, dass das gesamte tatsächlich verwendete Material manuell in das jeweilige Los gebucht werden muss.

Gerne werden dafür unsere verschiedenen Apps verwendet, damit der Mitarbeiter das sehr flexibel in das jeweilige Los buchen kann.

#### Material nachbuchen
<a name="keine automatische Materialbuchung0"></a>
[Siehe]( {{<relref "/fertigung/losverwaltung/material_nachbuchen">}} ).

#### Ausdrucken von Zeichnungen mit dem Fertigungsbegleitschein
Ist beim einem Artikel eine Zeichnung hinterlegt ([siehe]( {{<relref "/docs/stammdaten/artikel/#zeichnungen-hinterlegen" >}} )), so wird wenn diese PDF Datei als eigene Seite(n) mit ausgedruckt.

#### Losstatistik
Mit der Losstatistik steht Ihnen ein praktisches Instrument für Auswertungen Ihrer Fertigungsaufträge zur Verfügung.

![](Losstatistik.gif)

Dabei bedeuten:
- Der Datumsbereich: Schränkt die auszuwertenden Lose auf die in diesem Zeitraum vollständig erledigten Lose ein. Vollständig erledigt bedeutet: Entweder wurde durch die vollständige Ablieferung oder durch eine Überlieferung die Sollmenge in diesem Zeitraum erfüllt oder es wurde in diesem Zeitraum das Los manuell, z.B. wegen Unterlieferung erledigt.
- Stückliste: Schränkt die Auswertung unter Berücksichtigung des Erledigungsdatums auf die angegebene Stückliste ein.
- Auftrag: Liefert eine Auswertung aller diesem Auftrag zugeordneten Lose, **ohne** Berücksichtigung des Erledigungsdatums.
- Los: Liefert die Auswertung dieses einen Loses, **ohne** Berücksichtigung des Erledigungsdatums.

Hinweis: In der Losstatistik steht auch der Verkaufspreis des Artikels (der Stückliste des Loses, wenn es keine freie Materialliste ist) zur Verfügung. Dieser Verkaufspreis wird, wenn ein direkter Bezug zur Auftragsposition gegeben ist, aus dem Auftrag genommen, ansonsten wird die Verkaufspreisbasis des Artikels verwendet. Für den direkte Bezug zur Auftragsposition ist neben dem Bezug zur Auftragsnummer auch die Angabe der Auftragsposition erforderlich. Konnte der Verkaufspreis nicht aus dem Auftrag berücksichtigt werden, so wird anstatt dessen die aktuelle Verkaufspreisbasis des Artikels ohne Berücksichtigung von Preislisten oder Mengenstaffeln herangezogen.

#### Kopplung Losnummer an die Auftragsnummer
In manchen Unternehmen ist es von Vorteil, wenn die Losnummer sehr eng mit der Auftragsnummer korreliert. Es kann daher mit dem Parameter LOSNUMMER_AUFTRAGSBEZOGEN = 1 diese Verbindung hergestellt werden. Dies bewirkt folgende Losnummern:

JJ/AAAAA-PPP.

JJ .......... das Jahr

AAAAA ... die letzten fünf Stellen der Auftragsnummer

PPP ....... die laufende Nummer der Auftragsposition

Bedeutet auch, dass diese Darstellung nur für Belegnummernkreise ohne Jahrtausendinformation zur Verfügung steht.

Da trotzdem manche freie Lose, also Lose ohne Auftragsbezug angelegt werden müssen, steht für diese Definition der Parameter LOS_BELEGNUMMERNSTARTWERT_FREIE_LOSE zur Verfügung. Dieser Startwert wird bei jedem Jahreswechsel herangezogen.

Dieser Startwert ist üblicherweise auf 900000000 gesetzt um zu obiger Definition zu passen.

Die Suche nach der Auftragsnummer in den Losen ist in diesem Falle so gestaltet, dass sie einfach die Auftragsnummer eingeben. Es werden alle Lose für diesen Auftrag angezeigt.

Bitte achten Sie darauf, dass bei der Eingabe der Losnummer kein - (Bindestrich) zusätzlich nach der Auftragsnummer eingegeben wird.

Sollte gezielt auf ein sogenanntes freies Los, also ein Los ohne Auftragsbezug gesprungen werden, so geben Sie die Losnummer vollständig ein. Wird nach Teilen einer freien Losnummer gesucht, so verwenden Sie z.B. 07/9%35\. Damit werden alle freien Lose des Jahres 07 welche 35 in den nachfolgend Stellen der Losnummer beinhalten angezeigt. Also z.B. auch 07/000000350.

Automatische Kopplung der Fertigung mit
- dem Kundenauftrag
- der Kundenauftragsposition
- dem Lieferschein
- der Bestellung

Zur besseren Unterscheidung wird teilweise ein eigener Nummernkreis verwendet.

#### Kopplung Losnummer an die Auftragsnummer mit Los-Bereichen
Zusätzlich zur oben beschriebenen Kopplung kann auch eine Auftragskopplung nach Bereichen realisiert werden. Stellen Sie dazu den Parameter LOSNUMMER_AUFTRAGSBEZOGEN auf 2.

Die Idee dahinter ist, dass die Losnummern einerseits den Auftragsbezug haben und andererseits auch einen Bezug zu verschiedenen Bereich liefern. Daher definieren Sie bitte in der Losverwaltung unter Grunddaten die Bereiche.

Bei Bereich geben Sie eine Ziffer zwischen 0-9 ein und unter Bezeichnung die von Ihnen gewünschte Bereichsbezeichnung, z.B. Mechanik, Antriebe, Software usw.

Bei der Anlage eines neuen Loses muss nun, wenn auch eine Auftragsnummer eingegeben ist (dies also kein freies Los ist), auch der Bereich eingegeben werden. Von **Kieselstein ERP** wird dann für die Losnummer der nächste freie Zahlenbereich für die Losnummer gesucht und das Los mit dieser Nummer angelegt.

Wurde ein Los versehentlich in einem falschen Bereich angelegt, so muss es im richtigen Bereich neu angelegt werden und das im flaschen Bereich angelegte Los storniert oder anderweitig verwendet werden.

Der Bezug zur Auftragsposition ist in dieser Struktur nicht gegeben.

Erfassung zusätzlicher Fertigungsstati im Los

In vielen Produktionsbetrieben ist die Erfassung eines des Fortschrittsstatus am einzelnen Fertigungsauftrag erwünscht. Üblicherweise wird dies Arbeitsplatzabhängig erfasst.

Daher steht in **Kieselstein ERP** folgende Möglichkeit der Erfassung zusätzlicher Stati im Fertigungsauftrag zur Verfügung. Bitte beachten Sie, dass falls sie das Los bearbeiten wollen, jeweils die Zusatzstati zurückgesetzt werden müssen. Klicken Sie dazu auf Bearbeiten und beantworten Sie die Frage entsprechend und führen dieses Vorgehen bei Bedarf erneut durch.

**Voraussetzungen für die Erfassung**

Es müssen die Los-Zusatzstati definiert sein. Siehe dazu bitte Fertigung, unterer Modulreiter Grunddaten, Zusatzstatus.
Info: Jedem Status kann auch ein ICON zugeordnet werden. Das bedeutet: Nach dem Sie die Zusatzstati im Modul Fertigung definiert haben, sind diese auch in den allgemeinen Stati eingetragen. Damit kann im System unterer Modulreiter Sprache, oberer Modulreiter Status dem neu angelegten Status auch ein entsprechendes ICON hinterlegt werden.

Arbeitsplatz zu Stati zugeordnet -> Siehe System, Arbeitsplatzparameter

![](Zusatzstatus_definieren.jpg)

Den einzutragenden Wert erfragen Sie bitte bei Ihrem **Kieselstein ERP** Händler.
Barcodescanner mit Wedge Interface damit kann durch eine spezielle Programmierung des Barcodeheaders und durch Scann der Losnummer des Fertigungsbegleitscheines der Status mittels Barcodescanner gesetzt werden
Die Funktion des scannens des Zusatzstatuses steht erst nach der Definition des Arbeitsplatzparameters und nach anschließendem Neustart des Fertigungsmoduls zur Verfügung.
Dadurch wird erreicht, dass, sobald sich der Anwender in der Fertigungsauswahlliste befindet, kann direkt durch das Scannen der Losnummer vom Fertigungsbegleitschein der Status eingetragen werden.
Wir haben dafür die Funktionstaste F12 verwendet.
Nach drücken der F12, bzw. bei richtiger Einstellung des Wedge-CCD-Scanners erscheint folgender Dialog:
![](Zusatzstatus_scannen.gif)
Ist ein Status für ein Los schon eingetragen, erscheint ein entsprechender Hinweis.
Ein eventuell falsch eingetragener Status muss unter dem oberen Modulreiter Zusatzstatus gelöscht werden.
Die Bedienung dieses Dialoges ist ausdrücklich für den Barcodescanner gedacht. Es können laufend die Lose gescannt werden. Zum Verlassen des Dialoges drücken Sie ESC oder klicken Sie auf das x rechts oben.
Erscheint anstatt des obigen Dialoges 
![](Fehler_Fert_Zusatzstatus.gif)<br>
so wurde eine falsche ID für den Arbeitsplatzparameter definiert. Stellen Sie diesen bitte richtig.

**Programmierung der Wedge-CCD-Scanner**

Üblicherweise finden Sie in der erweiterten Konfiguration der Scanner auch eine Header Definition.

Für Datalogic Scanner wählen Sie
- Enter Configuration
- one character header
- dann scannen Sie die Hexnummer der gewünschten Taste, für diese Funktion wird die F12 benötigt, also 90 (Hex)
- Exit and Save Configuration

Anzeige der jeweiligen Stati eines Loses.

Wählen Sie dazu den oberen Modulreiter Zusatzstatus. Hier können die zusätzlichen Stati eines Loses, welche lediglich Ihrer Information, z.B. des Fortschrittes eines Loses, angegeben und gegebenenfalls auch wieder gelöscht werden. Die möglichen Zusatzstati müssen unter Grundeinstellungen definiert werden. Siehe dazu auch oben.

#### Kann eine Ausgabeliste auch für mehrere Lose auf einmal gedruckt werden.
Ja, gehen Sie dazu wie üblich bei einem ausgegebenen Los / in Produktion befindlichen Los auf Los, Drucken, Ausgabeliste. Hier wird üblicherweise sofort die Ausgabeliste des gewählten Loses angezeigt. Wenn nun mehrere / ein anderes Los ausgedruckt werden soll, so klicken Sie bitte auf ![](Mehrere_Lose_Auswaehlen.gif).

In der nun erscheinenden Liste der Lose wählen Sie durch markieren die in dem Ausdruck anzuführenden Lose.

![](Mehrere_Lose_markieren.jpg)

Markieren einzelner Lose mit Strg+Mausklick, eines ganzen Bereiches mit Umschalt+Mausklick.

Nun erscheint in der Druckvorschau der erste Teil der gewählten Lose ![](Mehrere_Lose_anzeigen.gif).

Klicken Sie nun bitte auf aktualisieren ![](Mehrere_Lose_aktualisieren.gif) und es wird eine gesamte Ausgabeliste für die Lose ausgegeben.

In der Überschrift der Ausgabeliste werden die Losnummern aller berücksichtigten Lose angedruckt.

#### Wiederholende Lose / Monatslose
In einigen Fertigungsbetrieben ist es aus strukturellen Gründen nicht sinnvoll jeden kleinen Fertigungsauftrag exakt mit seinen Istzeiten zu bebuchen. Hier hat sich bewährt, dass sogenannte Monatslose angelegt werden. Diese sind nach Baugruppen gruppiert. Dies ist für die Erfassung der Istzeiten hinreichend genau und reduziert die Administrativenzeiten auf ein erträgliches Maß.
Wiederholende Lose werden in der Fertigung, unterer Modulreiter Wiederholende definiert.

Die einzelnen Felder haben dabei folgende Bedeutung:<br>
![](Wiederholende_Lose.gif)

| Art | Artikel oder freie Materialliste |
| --- |  --- |
| Kostenstelle | Kostenstelle |
| Stückliste | Artikelnummer des anzulegenden Loses |
| Projekt | Projektbezeichnung. Hinweis dies wird um das Monat / die Zeitrauminformation ergänzt |
| Ziellager | Ziellager der Zubuchung |
| Fertigungsgruppe | Fertigungsgruppe |
| Fertigungsort | Fertigungsort |
| Wiederholungsintervall | In welchen Abständen sollte dieses Los angelegt werden |
| ab | Ab welchem Stichtag wird das Los angelegt**WICHTIG:** Dieses Datum definiert zugleich den Zeitraum für den das Los angelegt wird. |
| Losgröße | Mit welcher Losgröße sollte das Los angelegt werden |
| Vorauseilend | Um wieviele Tage vor dem Stichtag sollte das Los vorher angelegt werden. |
| Aktiv | Verwenden Sie diese Einstellung zum vorübergehenden deaktivieren der automatischen Anlage |
| Sortierung | Reihenfolgedefinition |

<u>Beschreibung Vorauseilend:</u>

Sie möchten Ihre Monatslose immer um 5Tage vor dem Monatswechsel anlegen, die Lose sollten entsprechend dem Stichtag einmal im Monat angelegt werden. D.h. Sie definieren dass das Los zum 1.. Los angelegt werden sollte. Durch das Wiederholungsintervall monatlich ist definiert, dass immer um ein Monat weiter gesprungen wird. Durch Vorauseilend z.B. 5Tage definieren Sie, dass diese Prüfung bereits 5Tage vorher erfolgt und somit zum 26.7\. bereits das Los für den August angelegt wird.

#### Wie werden die Lose aus den Wiederholenden Losen erzeugt?
Im unteren Modulreiter Wiederholende Lose, wählen Sie oben in der Menüleiste bearbeiten, Wiederholende Lose anlegen
![](Wiederholende_Lose_anlegen.jpg)

#### Halbfertigfabrikatsinventur
<a name="Halbfertigfabrikatsinventur"></a>
Mit der Halbfertigfabrikatsinventur wird das noch in der Fertigung befindliche Restmaterial inkl. Arbeitszeiten ermittelt. Damit haben Sie einen Wert der angearbeiteten Waren. Bei der Bestimmung der noch in der Fertigung befindlichen Mengen werden die (teil-)abgelieferten Mengen berücksichtigt. Für die Bestimmung der Arbeitszeitanteile bei teilabgelieferten Losen ist die Richtigkeit der Sollstunden der Arbeitszeit wichtig.
Hinweis: Diese Auswertung wird oft auch Work in Progress genannt.

Welche Materialmengen werden in die Halbfertigfabrikatsinventur aufgenommen?
Es werden die noch nicht Rückgemeldeten anhand der Sollsatzgrößen verwendet.
Ein Beispiel:
Losgröße: 100 Stk
Sollsatzgröße: 3 Stk
Ausgegebene Menge: 100 Stk
Abgelieferte Losmenge: 30 Stk

Anhand der abgelieferten Losmenge ergibt sich über die Sollsatzgröße eine Menge von 90 Stk (3 x 30). Diese Menge wurde bereits in der Rückmeldung / Ablieferung vom Los ans Lager berücksichtigt. Die Differenz 100 - 3 x 30 = 10 ist noch frei im Lager verfügbar und geht daher in die Halbfertigfabrikatsinventur ein. Für den Materialwert wird der Gestehungspreis zum Zeitpunkt der Entnahme aus dem Lager herangezogen.

Welche Stunden werden berücksichtigt?
Auch hier wird in gleicher Weise wie beim Material vorgegangen. Es wird jedoch zusätzlich der Buchungszeitpunkt zum Zeitpunkt der Ablieferung berücksichtigt. D.h. es werden nur jene Stunden als bereits in den abgelieferten Losmengen enthalten berücksichtigt, welche für diesen Zeitpunkt am Los gebucht sind.
Da in aller Regel die Lose weiter bearbeitet sind, als dies die Ablieferung erfordert, muss auch hier die Sollsatzgröße berücksichtigt werden. So sind in einem Elektronikbetrieb z.B. alle SMD Bauteile bereits bestückt, aber die THT Artikel werden nur nach Bedarf per Hand nachgelötet. D.h. die Bestückungszeit für die SMD ist bereits voll am Los gebucht, die Handarbeitszeiten nur soweit wie für die Ablieferung erforderlich.
Daraus ergibt sich, dass die Richtigkeit der Sollzeiten einen wesentlichen Einfluss auf die Zwischen-Kalkulation für die Halbfertigfabrikatsinventur hat. Dies gilt auch für die Berechnung der Gestehungswerte für die Teilablieferungen.

Bitte beachten Sie dazu auch die [Lagerabstimmung]( {{<relref "/fertigung/losverwaltung/lagerabstimmung">}} ) und [Gestehungspreisberechnung zum Stichtag]( {{<relref "/docs/stammdaten/artikel/#wie-wird-der-gestehungspreis-f%c3%bcr-die-lagerstandsliste-zum-stichtag-errechnet" >}} ).

Sie finden im Journal der Halbfertigfabrikatsinventur verschiedene Sortierungsmöglichkeiten:
![](HFI_Sortierungen.JPG)
Zusätzlich erhalten Sie eine Auswertung nach Fertigungsgruppen, wenn Sie den Haken bei Sortiert nach Fertigungsgruppe ![](sortiert_Fertigungsgruppe.JPG) setzen.

#### Wie wirkt sich eine Rücknahme der Losausgabe auf die Halbfertigfabrikatsinventur zum Stichtag aus ?
Unter Umständen dramatisch. Warum ?

Die Basis für die Berechnung der Halbfertigfabrikatsinventur (HF) ist das Ausgabedatum des Loses und der Buchungszeitpunkt der jeweiligen Losablieferungen.

Wird nun eine Losausgabe im ersten Monat durchgeführt, dann eine Halbfertigfabrikatsinventur gemacht und dann die Losausgabe zurückgenommen und dann das Los im Folgemonat wieder ausgegeben, so wurde
1. das HF Material im ersten Monat als in Fertigung mitgerechnet.
2. durch die Rücknahme der Ausgabe wird das Material wieder ans Lager zurückgegeben
3. durch die erneute Entnahme wird das Ausgabedatum eben auf das neue Datum gesetzt und somit im Folgemonat wird das Material als ebenfalls in Fertigung befindlich betrachtet.

Wird nun erneut die HF-Inventur zum ersten Monat durchgeführt, so ist, durch die erneute Ausgabe das Material nicht mehr zum ersten Monat sondern erst zum Folgemonat ausgegeben. Dadurch ist der Wert im ersten Monat nicht mehr enthalten, sondern vielmehr noch im Lager. Daher stimmt der "alte" HF-Inventurausdruck mit dem "aktuellen" HF-Inventurausdruck nicht mehr überein, obwohl beide den gleichen Stichtag haben.

Bitte bedenken Sie dies bei den Änderungen an Ihren Losen.

Wenn eine nachträgliche Losänderung erforderlich ist, ist es aus obigem Gesichtspunkt besser, wenn die Änderungen im Los direkt durchgeführt werden.

#### Ändern der Losgröße
<a name="Ändern der Losgröße"></a>
Unter dem Menüpunkt Bearbeiten, Losgröße ändern, steht Ihnen bei ausgegebenen, in Produktion befindlichen oder teilerledigten Losen die Möglichkeit der Änderung der Losgröße zur Verfügung. Die Losgröße kann sowohl erhöht als auch reduziert werden.
Bei der Änderung der Losgröße werden die Sollmengen anhand der Sollsatzgrößen neu berechnet. Danach wird geprüft ob Material zurückgegeben werden sollte und danach werden die Fehlmengen aktualisiert.<br>
![](Losgroesse_aendern.jpg)<br>
Wird das Material reduziert, so muss entschieden werden, was mit dem überzähligen Material geschehen sollte. Wird hier Ja gewählt, so wird die überzählige Menge ans Lager zurückgegeben, wird Nein gewählt, so bleibt das Material am Los und der Mehrverbrauch geht entsprechend in die Nachkalkulation ein.
Ein Los kann nur auf die bereits abgelieferte Menge reduziert werden.

Wenn die Losgröße erhöht wird, der Parameter KEINE_AUTOMATISCHE_MATERIALBUCHUNG=0 und in der Stückliste l "Materialbuchung bei Ablieferung" =0 ist, dann wird Material gemäß der Sollsatzgröße nachgebucht. Bei SNRS/CHNRS erscheint der SNR/CHNR-Dialog (gleich, wie bei der Los-Ausgabe).

#### Rückgabe von Lospositionen ans Lager
In **Kieselstein ERP** werden auch negative Sollmengen unterstützt. Das bedeutet, dass z.B. eine Baugruppe zerlegt werden kann und die daraus gewonnen Einzelteile werden wieder ans Lager zurückgebucht. So könnte der Inhalt, der dieses Zerlegen beschreibt z.B. so aussehen, dass mit einer positiven Menge die Baugruppe komplett vom Lager entnommen wird und die daraus gewonnenen einzelnen Komponenten mit negativen Mengen eingetragen werden.

Die Termine wirken so, dass die positiven, vom Lager entnommenen Mengen zum Beginnzeitpunkt des Loses entnommen werden, die negativen Mengen, die also zurückgegeben werden, werden zum Ende-Zeitpunkt des Loses als zurückgebbar verwaltet. Bei der (Teil-)Ablieferung werden diese Lose ans Lager gebucht.

Bei mehreren Lägern für dieses Los achten Sie bitte darauf, dass die Rückgabe der negativen Mengen immer ins erste Abbuchungslager erfolgt.

#### Erstlos?
In manchen Anwendungen ist es erforderlich, dass für die Freigabe von z.B. Serien-Artikeln verschiedene zusätzliche Tätigkeiten durchgeführt werden. Wir haben dies in **Kieselstein ERP** so implementiert, dass im Fertigungsbegleitschein der Parameter ERSTLOS zur Verfügung steht. Erstlos ist ein Los dann, wenn von der Chronologie der Losnummern her, dies für diesen Artikel das erste Los ist.

#### Parameter die die Ansicht der Losauswahlliste steuern
Es stehen folgende Parameter für die Einstellung der Ansicht der Losauswahlliste zur Verfügung:

| Parameter | Wert | Bedeutung |
| --- |  --- |  --- |
| AUFTRAG_STATT_ARTIKEL_IN_AUSWAHLLISTE | 0 ... aus<br>1 ... ein | Anstelle der Artikelnummer und der Artikelbezeichnung werden Auftragsnummer und Projekt des Auftrags angezeigtHinweis: Die Anzeige des Kunden ist in dieser Einstellung nicht möglich |
| KUNDE_STATT_BEZEICHNUNG_IN_AUSWAHLLISTE | 0 ... aus<br>1 ... ein | Anstelle der Artikelbezeichnung wird der Kundenname aufgrund des Auftrags angezeigt. Mit obiger Einstellung nicht kombinierbar. |
| ENDE_TERMIN_STATT_BEGINN_ANZEIGEN | 0 ... aus<br>1 ... ein1 | Anstelle des Beginntermines wird der Endetermin angezeigt |

Parameter für das Verhalten der Termineingabe:

| Parameter | Wert | Bedeutung |
| --- |  --- |  --- |
| NUR_TERMINEINGABE | 0 | Eingabe mit Berücksichtigung der Durchlaufzeit und als Vorwärts oder Rückwärtsterminierung |
|  | 1 | Eingabe von Beginn und Endetermin, daraus wird die Durchlaufzeit ermittelt. Keine Vorwärts bzw. Rückwärtskalkulation. |

#### Zuordnung der verantwortlichen Techniker und weiterer Techniker
In den Kopfdaten des Loses kann der für das Fertigungslos verantwortliche Techniker definiert werden. Gegebenenfalls kann bei der Zeiterfassung die Auswahlliste auf die Techniker eingeschränkt werden. [Siehe dazu]( {{<relref "/fertigung/zeiterfassung/#bei-der-erfassung-der-zeiten-sollen-nur-die-lose-angezeigt-werden-bei-denen-der-mitarbeiter-als-techniker-eingetragen-ist" >}} ). Um weitere Techniker zu definieren, welche ebenfalls auf dieses Los buchen, wählen Sie den oberen Modulreiter Techniker. Definieren Sie hier die weiteren Personen, welche normalerweise ebenfalls auf dieses Los buchen dürfen.

#### Steuerung der Fertigung in den Losen / Zeitdaten 
In den Zeitdaten eines Loses kann sehr genau gesteuert werden wie dieses Los in den verschiedenen Auslastungs-/Zeitplanungen berücksichtigt werden sollte.

![](Los_Zeitdaten.gif)

| Feld/Label | Bedeutung |
| --- |  --- |
| Tätigkeit | Tätigkeit die durchgeführt werden sollteFür die Vorkalkulation der Personalzeiten werden die unter Artikellieferant hinterlegten Sätze verwendet. Für die Istkalkulation beachten Sie bitte den Parameter [PERSONALKOSTEN_QUELLE]( {{<relref "/docs/stammdaten/personal/#personalkosten-in-den-nachkalkulationen" >}} ) |
| Arbeitsgang | Die laufende Nummer des Arbeitsgangs als übergeordnete Gruppe der nachfolgenden Unterarbeitsgänge |
| Unterarbeitsgang | Untergliederung eines einzelnen Arbeitsganges in weitere Schritte.Insbesondere für die Steuerung des Verhaltens der Zeiterfassungsterminals erforderlich. |
| Personalkosten/Maschinenkosten | Anzeige der Soll-Kosten zu Ihrer Information |
| AG-Art | Arbeitsgangart, siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Rüstzeit | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Stückzeit | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Gesamtzeit | Anzeige der errechneten Gesamtzeit anhand Stück- und Rüstzeit in Stunden |
| AG-Art | Arbeitsgangart siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| AG-Beginn | Arbeitsgangbeginn, für die Übernahme der Berechung der Arbeitsgang Beginn Termine siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ).Im Unterschied zur Stückliste wird hier der Beginntermin des Arbeitsganges für jeden Arbeitsgang angegeben. Wird bei einem Arbeitsgang der Termin geändert, so werden alle nachfolgenden Arbeitsgänge ebenfalls um die Differenztage verschoben. Dies wird in beiden Richtungen durchgeführt. |
| Aufspannung | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Maschine | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Aut.Stop bei Geht | Automatisches Stop bei Gehtbuchung des Mitarbeiters.Dieser Wert wird beim Anlegen des Arbeitsganges, des Loses von der Einstellung der Maschine übernommen und kann hier, z.B. für eine Geisterschicht, abgeändert werden.Er wirkt sich nur bei den Stop-Buchungen für die Maschine aus. D.h. ist der Haken gesetzt, so wird bei der Gehtbuchung des Mitarbeiters auch eine Stop-Buchung für die Maschine eingetragen. ist der Haken nicht gesetzt, so läuft die Maschine weiter und es muss die Stop-Buchung manuell eingetragen werden. Siehe dazu auch [Maschinenzeiterfassung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ). |
| Nur Maschinenzeit | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Material | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Kommentar | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |
| Text | siehe [Arbeitsplan der Stückliste]( {{<relref "/warenwirtschaft/stueckliste/arbeitsplan">}} ) |

#### Arbeitsgangversatz
Mit dem Arbeitsgang-Beginn-Datum können Sie steuern wann der Arbeitsgang (AG) begonnen wird.
Somit kann ein Versatz - zum Beispiel weil ein Arbeitsgang ausgelagert zu einem anderen Unternehmen ist - abgebildet werden. 
Weiters steuern Sie hiermit die parallele oder serielle Durchführung von Arbeitsgängen, in dem Sie bei parallel durchführbaren AG keinen Versatz angeben.

Der intern relativ gerechnete Lostermin ist zugleich der AG-Beginn. Falls Sie nun den Arbeitsgang 4 verschieben, werden darauffolgende Arbeitsgänge ebenso weiter verschoben.
Somit sehen Sie zum Beispiel die direkte Auswirkung, wenn eine Fremdfertigung mehr Tage in Anspruch nimmt als ursprünglich geplant.<br>
Die Eingaben des Arbeitsgangversatzes werden in der [Auslastungsvorschau]( {{<relref "/fertigung/losverwaltung/planung_auswertung/#planungs--und-auswertungsjournale" >}} ) berücksichtigt. Dadurch erkennen Sie im Detail die aktuelle Situation in der Fertigung und erhalten ein essentielles Steuerungswerkzeug um optimal die Auslastung zu planen.

#### Geplante Materialkosten
Für verschiedene Anwendungen ist es erforderlich, dass reine Material-Sollkosten für ein Los definiert werden.

Definieren Sie daher in den Los-Kopfdaten die ![](Los_geplante_Materialkosten.gif) in Ihrer Mandantenwährung.

Sind bei einem Los geplante Materialkosten definiert, so werden damit die Soll-Materialkosten in den Materialpositionen für die Nachkalkulationen deaktiviert und anstatt dessen nur die Soll-Materialkosten verwendet.

Weiters haben Sie die Möglichkeit im Reiter Material für jede einzelne Position einen Sollpreis zu definieren. Wenn ein Kalk-Preis aus der Stückliste vorhanden ist, wird dieser verwendet, auch wenn dieser 0 ist.  Wenn kein Kalkpreis vorhanden ist, dann wird der Lief1Preis unter Berücksichtigung der Mengenstaffel verwendet. 

Zur Aktualisierung der Preise gibt es den Button Sollpreise neu kalkulieren ![](sollpreise_neukalkulieren.JPG). Nach Klick auf das Icon werden die Preise gemäß der Informationen aus derm Artikellieferant aktualisieren.

#### Seriennummern Generator
<a name="Seriennummern Generator"></a>
Für die Ablieferung von Losen sind verschiedene Seriennummerngeneratoren vorgesehen.
Den Type des jeweiligen Seriennummern Generators definieren Sie im System, Parameter, SERIENNUMMERNGENERATOR.

| Typ | Funktion |
| --- |  --- |
| 1 | Erzeugt eine laufende Seriennummer über alle Artikel hinweg. |

Weiters hat man hier immer wieder die Herausforderung, dass eine Reihe von fortlaufenden Seriennummern erfasst werden sollte.
Nutzen Sie auch dafür die Seriennummernautomatik.
![](Seriennummernerfassung.jpg)
Beim Start des Seriennummern Erfassungsdialoges steht der Cursor im Seriennummern Feld. Drücken Sie nun Strg+O um die nächste Seriennummer anhand der oben beschriebenen Seriennummernautomatik zu bekommen. Somit steht die nächste frei verfügbare Seriennummer im Seriennummern Erfassungsfeld.
Drücken Sie nun Enter und die Seriennummer wird in die darüber liegende Liste der erfassten Seriennummern übernommen.
Nun drücken Sie erneut Strg+O und bekommen die nächste freie Seriennummer und drücken erneut Enter.
Links oben wird die abzuliefernde Menge angezeigt, rechts oben die bereits erfasste Anzahl der Seriennummern. So haben Sie jederzeit den Überblick, wieviele Seriennummern Sie bereits erfasst haben.

Ergänzend steht auch die Seriennummernerfassung mit Angabe der Stückzahl zur Verfügung.
Das bedeutet, es wird die Startseriennummer, durch Klick auf ![](Seriennummern_generieren.gif) oder Strg+O oder manueller Eingabe angegeben.
Sie schalten durch entfernen des Hakens bei Komma auf Seriennummern von bis Eingabe um
![](Seriennummern_automatik_mit_Anzahl.gif)
und stellen den Radiobutton auf Anzahl. Nun geben Sie unter Anzahl die gewünschte Stückzahl ein und klicken entweder auf ![](Seriennummern_in_Auswahlliste_uebernehmen.gif) Plus um die errechneten Seriennummern in der Auswahlliste zu sehen und eventuell noch zu bearbeiten, oder Sie klicken direkt auf Übernehmen ![](Seriennummer_uebernehmen.gif)womit die erzeugten Seriennummern in die davor liegende (Ablieferungs-) Erfassung übernommen werden.
Hinweis: Die Einstellung von Komma kann mit dem Parameter DEFAULT_SNR_DIALOG_KOMMA voreingestellt werden.
Der Vorschlagswert für das beginnt mit der Seriennummernreihe kann mit dem Parameter SERIENNUMMER_BEGINNT_MIT voreingestellt werden.

#### Ablieferung mit Seriennummernerfassung
Es kommt immer wieder vor, dass auf Produkten bereits von anderen Systemen Seriennummern vorgegeben sind.
Damit die Erfassung der Seriennummer und der Ablieferung nun auf schnelle Weise erfolgen kann, steht in der Ablieferung auch die Barcodeablieferung ![](Ablieferung_mit_Seriennummer.gif) zur Verfügung.
Bei dieser Art der Seriennummernerfassung wird auf die GS1 Codedefinition Bezug genommen. 

#### Können automatisch Lose für Unterstücklisten angelegt werden ?
Ja. Dies ist eine der extrem nützlichen Funktionen von **Kieselstein ERP**. Siehe dazu bitte die Beschreibung zur [Internen Bestellung]( {{<relref "/fertigung/losverwaltung/interne_bestellung">}} ).

#### Gibt es auch einen Ausdruck zu den offenen Arbeitsgängen?
Ja, das Journal offene Arbeitsgänge. Damit steht Ihnen eine übersichtliche Darstellung alle offenen Arbeitsgänge zur Verfügung. [Siehe dazu auch unterer Modulreiter offene AGs](#offene AGs).
![](Journal_offene_Arbeitsgaenge.gif)
Diese Auswertung kann nach
- Kunde
- Kostenstelle
- Fertigungsgruppe
- Artikelgruppe

eingeschränkt werden. Gerade die Auswertung nach Artikelgruppen bietet die Möglichkeit den Arbeitsaufwand je Produktionsbereich (Drehen, Kabelfertigen) aufzugliedern und so eine gute Aussage über die Produktionsauslastung zu erhalten.<br>
Mit Losnummer von bis kann gegebenenfalls noch der Bereich der herangezogenen Lose eingeschränkt werden.<br>
Es werden alle offenen Lose bis zum Stichtag berücksichtigt.
Stichtag ![](Journal_offene_arbeitsgaenge_Stichtag.gif)
Hier wählen Sie sowohl den Bezug des Stichtages als auch das Datum des Stichtages.<br>
Liefertermin ... Liefertermin anhand des dem los zugeordneten Kundenauftrages<br>
Beginn ... Produktionsbeginn des Loses<br>
Ende ... Produktionsende des Loses<br>
Der gewählte Bezug des Stichtages definiert zugleich die zweite Sortierung der Liste. Die Hauptsortierung dieser Auswertung ist die Artikelgruppe.

In dieser Auswertung wird auch der Arbeitsgangversatz innerhalb des Arbeitsplanes des Loses = Zeitdaten berücksichtigt.

#### Verschieben der Lostermine
<a name="Lostermine verschieben"></a>
Da es sich aus den verschiedensten Gründen immer wieder ergibt, dass die Lieferung von Losen verschiebt und andererseits bereits ausgegebene Mengen bzw. die Verwendung anderer Artikel im Los bereits gebucht sind, haben wir die Funktion Termin verschieben geschaffen. Sie finden diese im Menü der Fertigung unter Bearbeiten, Termin verschieben.<br>
![](LosTerminVerschieben1.gif)<br>
Damit werden die Termine des gewählten Loses entsprechend verschoben und die anhängenden Reservierungen, Fehlmengen, geplanten Ablieferungen ebenfalls geändert, ohne dass dafür die bereits auf das Los gebuchten Materialien zurückgenommen und wieder ausgegeben werden müssen. Durch die relative Speicherung der Arbeitsgang-Beginn-Termine wird mit diesem Vorgang auch der geplante Beginn der Arbeitsgänge mit verschoben.<br>
Bitte beachten Sie das unterschiedliche Verhalten, bei geöffnetem oder geschlossenem Schloss. D.h. ist das Schloss geöffnet ![](LosTerminVerschieben2.gif) so können Beginn und Ende getrennt geändert werden. Ist das Schloss geschlossen, so werden immer beide Termine gemeinsam verändert. Definieren Sie bitte zuerst das Verhalten der Termine, durch Auswahl / Klick auf das Schloss und geben Sie erst danach den / die neuen Termine ein.

Ist in Ihrer **Kieselstein ERP** Installation der Parameter AUTOMATISCHE_ERMITTLUNG_AG_BEGINN definiert, so kann mit der Funktion Termine verschieben **nur** der gewünschte Endetermin definiert werden.<br>
![](LosTerminVerschieben3.gif)<br>
Achten Sie darauf, dass das Ende an einem zu Ihren Produktionsprozessen passenden Tag liegt.
Durch die neue Definition des Endes wird damit der Arbeitsgangbeginn jedes Arbeitsganges des Loses neu errechnet. Damit werden auch so Dinge wieder Feiertag bzw. Betriebsurlaub berücksichtigt.

Bitte beachten Sie zu diesem Thema auch unbedingt die Funktion [Auftragstermin]( {{<relref "/verkauf/auftrag/#auftragstermin-verschieben" >}} ) verschieben. Mit der Funktion in den Losen werden **nur** die Termine im Los verschoben. D.h. der Ausliefertermin des Auftrags an den Kunden ist davon nicht betroffen.

#### Automatische Ermittlung Los Ende Termin
Eine weitere Möglichkeit die Durchlaufzeit eines Loses zu ermitteln, ist die Vorgehensweise, dass in der Stückliste für jeden Arbeitsgang der sogenannte Arbeitsgangversatz hinterlegt wird.
Das bedeutet dass der Zeitversatz in Tagen vom ersten zum zweiten Arbeitsganges die organisatorische Dauer für den ersten Arbeitsgang darstellt.<br>
Damit ergibt sich die geplante Gesamtdauer für das Los. Daraus ergibt sich auch, dass hier Arbeitstage zur Anwendung kommen.<br>
Info: Üblicherweise rechnet **Kieselstein ERP** in Kalendertagen.<br>
Durch die Verwendung der Arbeitstage ergibt sich, dass bei der Anlage des Loses aber auch bei der Verschiebung des Los-Beginntermins, der Beginntermin des jeweiligen Arbeitsganges anhand des Firmenzeitmodells errechnet werden.<br>
Es muss dafür der Parameter AUTOMATISCHER_ERMITTLUNG_LOS_ENDE aktiviert werden. Dieser Parameter hat Vorrang vor dem Parameter AUTOMATISCHE_ERMITTLUNG_AG_BEGINN (siehe unten).<br>
Dies bewirkt, dass für die Berechnung des tatsächlichen Starttermins des jeweiligen Arbeitsganges die Arbeitstage anhand des Firmenzeitmodells ermittelt werden. Damit können Feiertage, Betriebsurlaube u.ä. abgebildet werden. Bitte beachten Sie, dass im Firmenzeitmodell jeder Tag mit einer Sollzeit > 0 als vollwertiger Arbeitstag gerechnet wird. So wird der in Österreich übliche Freitag mit meist nur 4Std auch als voller Tag gerechnet.<br>
Daraus ergibt sich auch, dass in dieser Einstellung bei den Termineingaben nur der jeweilige Beginntermin eingegeben werden kann. Das Ende errechnet sich immer automatisch.

#### Kann der Losbeginn automatisch errechnet werden?
<a name="Beginntermin automatisch errechnen"></a>
Setzen Sie den Parameter AUTOMATISCHE_ERMITTLUNG_AG_BEGINN auf jene Zeit in die die Dauer eines Arbeitsganges getaktet werden sollte.<br>
Zugleich muss das Firmenzeitmodell definiert werden.<br>
Dies bewirkt nun, dass davon ausgegangen wird, dass ein Arbeitsgang eine Loses immer nur von einem Mitarbeiter bearbeitet wird. Die Dauer des Arbeitsgangs wird auf die angegebene Stundenzahl aufgerundet und die Anzahl der Stunden und Tage passend zum Firmenzeitmodell ermittelt.<br>
Eine übliche Einstellung sind z.B. 4Std. Das ergibt bei einem üblichen Zeitmodell von 4x8Std und 1x4,5Std (für Freitag) 9Blöcke für die Abarbeitung der Arbeitsgänge eines Loses. Ausgehen vom ermittelten Los-Ende-Termin wird nun der Losbeginntermin errechnet und automatisch auf diesen Termin gesetzt.
In dieser Berechnung wird auch der Betriebsurlaub berücksichtigt, sodass Sie zuerst den Betriebsurlaub im Betriebskalender (Modul Personal) definieren sollten und erst danach die Planung für die Lose machen sollten.<br>
Bitte beachten Sie, dass nur für diese Berechnung auch der MGZ Faktor (**M**itarbeiter**G**leich**z**eitig) berücksichtigt wird, welcher wiederum nur dann zur Verfügung steht, wenn auch die [Reihenfolgenplanung]( {{<relref "/fertigung/losverwaltung/los_anhand_kundenauftraegen/#k%c3%b6nnen-lose-eines-auftrags-st%c3%bccklisten-%c3%bcbergreifend-in-einer-bestimmten-reihenfolge-angelegt-werden" >}} ) aktiviert ist. Er bedeutet, dass diesen Arbeitsgang in seiner Stückzeit, mehrere Mitarbeiter gleichzeitig daran arbeiten, reduziert also die Stückzeit.

#### Los kann nicht geändert werden
Wenn man versucht ein Los über die Kopfdaten zu ändern kommt die Fehlermeldung "Der zu löschende Eintrag hat noch abhängige andere Einträge, löschen Sie diese bitte zu erst".

Wie gehen Sie nun vor ?

Hintergrund: Mit dem Los wurden Bestellpositionen verknüpft. Daher darf die Los-Soll-Position nicht verändert werden. D.h. um das Los zu ändern, muss zuerst die Bestellung zurückgenommen werden. Beachten Sie dazu bitte, dass diese wahrscheinlich bereits beim Lieferanten in Produktion ist.

#### Wie können die Bestellung eines Loses herausgefunden werden ?
[Siehe dazu.]( {{<relref "/einkauf/bestellung/#wie-findet-man-bestellungen-eines-loses" >}} )

#### Wieso haben manche Los-Materialpositionen eine Sollmenge von 0?
Hintergrund ist, dass es für das Los die Funktion "Los aktualisieren anhand Stückliste" gibt. Bei dieser Funktion werden alle Sollmengen aktualisiert. Danach wird versucht die Positionen mit Sollmenge 0,00 zu entfernen. Sind mit dieser Position jedoch weitere Datensätze verbunden, z.B. Bestellpositionen, so kann aus datentechnischer Sicht diese Position, auf die sie sich ja bezieht, nicht mehr gelöscht werden. Daher bleiben manchmal Positionen mit 0,00 in der Sollmenge in den Losmaterialpositionen enthalten. Gerade bei Stücklistenänderungen von sehr dynamischen Stücklisten kommt dies gerne vor.

#### Worauf ist beim Storno eines Loses zu achten?
<a name="Los Stornieren"></a>
Gerade bei Anwendern, die eine enge Kopplung der Losnummer mit der Auftragsnummer verwenden, taucht immer wieder die Frage auf, wieso kann ich eine Auftragsposition nicht stornieren, ich habe doch das Los storniert.

Aufgrund der relationalen Datenbank ist hier zu beachten, dass der direkte Positionsbezug im Los vor dem Storno entfernt werden muss. Gehen Sie dazu wie folgt vor:

Bevor das tatsächliche Storno durch Klick auf ![](Los_Storno.gif) ausgeführt wird entfernen Sie den Positionsbezug.

Klicken Sie auf Position ![](Los_Positionsauswahl.gif), klicken Sie auf Löschen ![](Los_Positions_zuordnung_loeschen.gif).

Zur Bestätigung haken Sie nun noch ![](Los_Kein_Auftrags_Positionsbezug.gif) an.

Damit ist der Positionsbezug dieses (zu stornierenden Loses) zum Auftrag aufgetrennt.

Speichern Sie die Änderung ab und ![](Los_Storno.gif) stornieren anschließend das Los.

Sollten Sie bereits ein Los storniert haben, ohne den Auftragsbezug zu löschen, so machen Sie zuerst das Losstorno durch klick auf ändern ![](Los_Aendern.gif) rückgängig, entfernen die Auftrags-Positions-Verknüpfung und stornieren dann anschließend das Los wieder.

#### Koppelprodukte
Die Frage des Anwenders war, wie können sogenannte Koppelprodukte in **Kieselstein ERP** abgebildet werden.

Zur Erklärung:

Unter Koppelprodukt wird verstanden, dass aus einem Ausgangsmaterial verschiedene unterschiedliche Fertigmaterialien erzeugt werden.

Die grundsätzliche Frage dazu ist:
Werden die unterschiedliche Produkte in einem Arbeitsgang / einem Fertigungsauftrag erzeugt oder sind das verschiedene Produktionsaufträge.

Wenn es verschiedene Produktionsaufträge sind, so lautet die Lösung:

Legen Sie für jeden Veredelungsauftrag eine entsprechende Stückliste an.

Beispiel:<br>
Rohware kann veredelt werden nach:
- Produkt A Mahlgrad 10
- Produkt B Mahlgrad 20
- Produkt C Mahlgrad 30

So legen Sie eine Stückliste an für Produkt A Mahlgrad 10, in dem die erforderliche Menge Rohware enthalten ist.

In gleicher Weise verfahren Sie mit Produkt B usw.

Diese Lösung hat den Vorteil, dass Sie sehr flexibel in der "Verwendung" der Rohware sind.

Ist es aber ein Produktionsauftrag oder genauer, wird automatisch eine bestimmte Verteilung von daraus gewonnen Produkten vorgegeben, so ist dazu anzumerken, dass die grundsätzliche Ausrichtung von 

**Kieselstein ERP** auf eine zusammenfügende Produktion ausgerichtet ist, also in der Regel man nach einem Fertigungsprozess immer <u>ein</u> Endprodukt erhält.

Es gibt jedoch den Trick über die Materialrückgabe. D.h. durch die Angabe von negativen Sollmengen wird bei der Ablieferung des Loses das Material mit den negativen Sollmengen an das Lager zurückgegeben.

Beispiel ähnlich dem obigen, aber in einem Fertigungsprozess:<br>
Gegeben sei:
- Aus 1000kg Rohware erhalten wir:
- 500 kg Produkt A
- 300 kg Produkt B
- 200 kg Produkt C

Es wird eine Stückliste für Produkt A Mahlgrad 10 angelegt. In Worten ausgedrückt steht in dieser:

Für 1kg Produkt A (Die Mengeneinheit sei kg) benötigen wir 2kg Rohware und erhalten zugleich 0,6kg Produkt B und 0,4kg Produkt C.

In der Stückliste steht also:
- plus 2 kg Rohware
- minus -0,6kg Produkt B
- minus -0,4kg Produkt C

Dies sieht im ersten Blick etwas verwirrend aus. Weiss man jedoch, dass die negativen Mengen eine Buchung an das Lager bei der Ablieferungsbuchung bewirkt, so wird es klarer.

Von der Grundidee her raten wir zu einer Buchung über getrennte Fertigungsaufträge, da damit der Kostenfluss deutlich transparenter ist.

#### Los-Schnellanlage
In manchen Unternehmen kommt es immer wieder vor, dass faktisch innerhalb weniger "Sekunden" ein neues Los angelegt werden sollte, da sofort mit der Produktion begonnen werden muss. Hier kommt oft auch dazu, dass dies von der Fertigungssteuerung direkt verlangt wird und die Administration mit Auftragsanlage usw. dann gemacht wird / werden kann, wenn die Produktion schon läuft. Dafür haben wir die Funktion Los-Schnellanlage ![](Los_Schnellanlage.gif) geschaffen.
Es ist dafür das Rollenrecht FERT_LOS_SCHNELLANLAGE erforderlich.
Mit der Los-Schnellanlage erreichen Sie, dass bei Klick auf dieses Icon und nach Bestätigung der Abfrage sofort ein Los ohne Auftragsbezug mit Materialliste gegebenenfalls in Ihrer Fertigungsgruppe angelegt, ausgegeben und der Fertigungsbegleitschein ausgedruckt wird. Damit steht sofort ein Papier zur Verfügung auf das die Fertigungszeiten gestempelt werden können. Die eventuell nachträglich erforderliche Zuordnung zu Aufträgen verändert die Losnummer nicht mehr, auch wenn für Sie die Verbindung zwischen Losnummer und Auftragsnummer aktiviert ist (Parameter: Losnummer_Auftragsbezogen).

#### Filter auf Fertigungsgruppen
Es ist oft praktisch, wenn man gezielt nur die Lose einer bestimmten [Fertigungsgruppe]( {{<relref "/warenwirtschaft/stueckliste/#fertigungsgruppe-wozu-ist-das-und-wo-definiert-man-das" >}} )
 / Abteilung bearbeitet. Dazu dient der Filter in der Losauswahlliste ![](Auswahl_Fertigungsgruppe.gif), welcher rechts neben nur offene zu finden ist. Wenn die [Rollenrechte]( {{<relref "/docs/stammdaten/benutzer/#einschr%c3%a4nkung-auf-fertigungsgruppen" >}} ) nicht eingeschränkt sind, so ist dieser mit Alle vorbesetzt, ansonsten mit der ersten Fertigungsgruppe, welche der Rolle zugewiesen ist.

#### Zeiten direkt aus dem Fertigungslos buchen?
Um direkt aus der Auswahlliste Beginn und Ende auf dieses Los für den angemeldeten Mitarbeiter zu buchen, kann diese Funktion durch den Parameter BEGINN_ENDE_IN_AUSWAHLLISTE der Kategorie Fertigung aktiviert werden. Damit wird durch einen Klick auf ![](LosZeiterfassung_Beginn.gif) Zeiterfassung starten für den angemeldeten Benutzer ein Eintrag für dieses Los in der Zeiterfassung zum aktuellen Zeitpunkt angelegt. Mit einem Klick auf ![](LosZeiterfassung_Ende.gif) Zeiterfassung beenden wird ein Endeeintrag zum aktuellen Zeitpunkt eingetragen.

Bitte beachten Sie, dass in **Kieselstein ERP** der Beginn einer neuen Tätigkeit immer die bisherige Tätigkeit beendet. D.h. in aller Regel wird nur Beginn "gestempelt".

**Hinweis:**

Damit die Zeiterfassungs-Icons angezeigt werden, muss der Benutzer das Recht PERS_ZEITEREFASSUNG_CUD schreiben in der Zeiterfassung besitzen.

#### Los-Etiketten Druck
Im Fertigungs-Los stehen derzeit drei grundsätzliche Ausdrucke für Etiketten zur Verfügung.

| Aufruf | Funktion |
| --- |  --- |
| Los, Drucken, Etikette | Ausdruck von einzelnen Etiketten auf einem dezidierten Etikettendrucker mit und ohne Losinhalte |
| Los, Drucken, Etikette A4 | Wie oben, jedoch so gestaltet, dass A4 Etiketten eingelegt werden können. |
| Los, Ablieferung, Etikett | Ausdruck von Etiketten passend zur Ablieferung. Gegebenenfalls inkl. Geräteseriennummerninformation bzw. Chargen-/Seriennummern-Information |

#### Fehlerangaben
Als Ergänzung der Stückrückmeldung (Gut-/Schlecht-Stück) kann je rückgemeldeter Stückzahl gegebenenfalls auch noch ein entsprechender Fehler mit angegeben werden.
![](Fehlerangabe.gif)
Die Definition der Fehler erfolgt im Modul Reklamation, unterer Modulreiter Grunddaten, oberer Modulreiter Fehler.
Zusätzlich können bei der Erfassung der Gut-/Schlechtstück auf dem Zeiterfassungsterminal auch die Fehlernummern angegeben werden.
Sind auf einem Los Fehler erfasst, so wird in der Nachkalkulation ![](Fehler_vorhanden.gif) angezeigt.
Zusätzlich werden im Ausdruck der Nachkalkulation die Fehler detailliert aufgeführt.

## Besonderheiten in der Kunststofffertigung
<a name="Besonderheiten in der Kunststofffertigung"></a>
In der Kunststofffertigung hat sich folgende Herangehensweise herausgebildet. Zu den Einstellungen in den Stücklisten [siehe bitte dort]( {{<relref "/warenwirtschaft/stueckliste/#wie-sind-die-st%c3%bccklisten-in-der-kunststofffertigung-anzulegen" >}} ).

- Ausschussmengen / Überproduktion<br>
Üblicherweise werden ausgehend von der vom Kunden gewünschten Stückzahl eine Übermenge (größere Stückzahl) produziert um mit großer Wahrscheinlich die gewünschte Stückzahl zu erreichen. Dies wird üblicherweise durch die Erhöhung der Losgröße gegenüber der Auftragsmenge um den Ausschussfaktor vorgenommen. Sind in den Stücklisten auch Verpackungen enthalten, so sind diese von der Erhöhung ebenfalls betroffen.
- Rückmelden der tatsächlich gefertigten/gespritzen Stückzahlen<br>
Oft wird nicht nur die benötigte Stückzahl gefertigt, sondern das Granulat vollständig verbraucht, da ja der Kunde in der Zukunft sicher wieder Teile benötigt. Es ist daher nach dem Spritzen der Teile die Losgröße auf die tatsächlich gefertigte Menge zu korrigieren (Siehe Los, Bearbeiten, Losgröße ändern). Damit wird das benötigte / verbrauchte Material entsprechend korrigiert.
- Erfassung der tatsächlich verwendbaren Stückzahlen nach der Qualitätsprüfung.<br>
Bei der Prüfung der Teile werden Teile aussortiert werden müssen, welche nicht den Vorgaben entsprechen. Dadurch werden zum  Schluss weniger Teile abgeliefert als produziert wurden. Setzen Sie hier bei der letzten Buchung der Losablieferung den Haken bei Vollständig erledigt, so werden die Material- und Arbeitszeit-Kosten auf die reduzierte Ablieferungsgesamtmenge umgerechnet, da der Mehraufwand sich ja auf die Gutteile verteilen muss.
- Ermittlung der Schusszahlen am Werkzeug<br>
Diese kann aus der Statistik des Werkzeugs ermittelt werden. Dafür werden neben den verwendeten Losen auch die Erfassungsfaktoren (Schusszahl) benötigt. Bitten Sie Ihren **Kieselstein ERP** Betreuer dies entsprechend einzurichten.

#### Rüstmengen / Werkzeuge
Wie können z.B. in der Kunststofftechnik Werkzeuge mitverwaltet werden?
Hintergrund: Üblicher Weise steht ein Werkzeug nur einmal zur Verfügung. D.h. der Produktionsprozess kann erst gestartet werden, wenn das Werkzeug zur Verfügung steht.
Dies kann durch die Rüstmenge abgebildet werden. [Siehe dazu bitte]( {{<relref "/warenwirtschaft/stueckliste/#wie-sind-die-st%c3%bccklisten-in-der-kunststofffertigung-anzulegen" >}} ).<br>
Das bedeutet, dass für den Beginn der Produktion, also dem Ausgeben des Loses, das Werkzeug auf Lager sein muss. Ist dem nicht der Fall, so kommt<br>
![](Ruestmenge_nicht_lagernd.jpg)<br>
Rüstmenge nicht auf Lager und die Ausgabe wird abgebrochen. Oder in anderen Worten, ohne Werkzeug kann nicht produziert werden.<br>
Aus diesem Grund können Lose, die das **selbe Werkzeug verwenden, nicht gemeinsam** ausgegeben werden, da ja das erste Los das Werkzeug belegt und somit dieses Werkzeug für das zweite Los nicht zur Verfügung steht.<br>
Dazu die Bemerkung eines Anwenders: Das bringt Sauberkeit in den Prozess.

Beachte dazu den Parameter: ... Trotz fehlender Rüstmenge ausgeben

#### Kann man ein Los als nicht mehr zu verändern kennzeichnen?
Besonders bei einer laufenden Aktualisierung und Änderung von Stücklisten, gibt es im Unternehmensprozess den Zeitpunkt, bei dem die Stückliste freigegeben ist. Das bedeutet, dass ab diesem Zeitpunkt keine Änderungen der Materialpositionen und Mengen der Lose mehr gemacht werden dürfen. Um diesen Status in **Kieselstein ERP** abzubilden gibt es im Reiter Kopfdaten das Icon ![](Material_vollstaendig.JPG) (Material vollständig).

Der Klick auf das Icon wird protokolliert ![](Material_vollstaendig_meldung.JPG).

Nach Klick auf dieses Icon, kann keine Änderung der Soll-Materialien am Los mehr durchgeführt werden.

![](fehler_nicht_aendern_material.JPG)

Sollte das Material trotzdem geändert werden müssen, so kann, von berechtigten Personen durch erneuten Klick auf das Icon ![](Material_vollstaendig.JPG) dieser Status wieder zurückgenommen werden.

#### Kann in der Losauswahlliste auch nach der Bezeichnung gesucht werden?
Wenn der Parameter LOSNUMMER_AUFTRAGSBEZOGEN >=1, dann wird in den Los-Auswahllisten anstatt der Auftragsnummer die Erweiterte Artikelsuche angezeigt (hier ist zusätzlich noch der Parameter TEXTSUCHE_INKLUSIVE_ARTIKELNUMMER möglich.

#### Wie erhält man eine schnelle Übersicht über alle offenen Arbeitsgänge?
<a name="offene AGs"></a>
Für die Planung und Steuerung der Fertigung ist das Wissen über die offenen Arbeitsgänge essentiell. Einen Überblick über alle offenen Arbeitsgänge aller Lose erhalten Sie im Modul Fertigung, im unteren Reiter Offene AGs (Arbeitsgänge). Um Informationen über spezielle Arbeitsgänge zu erhalten, nutzen Sie die Filtereingabemöglichkeiten und die Sortiermöglichkeiten der einzelnen Spalten. Mit dem GoTo-Button können Sie direkt zu dem ausgewählten Arbeitsgang wechseln.

![](offeneag.jpg)

Bitte beachten Sie dazu auch die beiden Journale und die Möglichkeit das Journal der offenen Arbeitsgänge direkt in einem [Browser anzuzeigen]( {{<relref "/docs/stammdaten/system/web_links/#web-schnittstellen" >}} ) und die Beschreibung der Funktion  *Liste der offenen Los-Arbeitsgänge für eine bestimmte Tätigkeit*

**Hinweis:**<br>
In der Combobox werden nur die tatsächlich in dieser Liste vorhandenen Maschinen angezeigt. Maschinen die als versteckt gekennzeichnet sind, aber trotzdem verwendet werden, werden mit einem führenden * gekennzeichnet, da diese Maschinen üblicherweise bereits ausgeschieden wurden.

#### Wie können die einzelnen Arbeitsschritte gesteuert werden?
Um die Reihung einzelner Arbeitschritte anzupassen, finden Sie im unteren Reiter Offene AG eine CheckBox Reihen ![](offeneAG_reihen.JPG), wenn Sie hier einen Haken setzen, so ändert sich die Sortierung.<br>
Ab diesem Zeitpunkt erfolgt die Anordnung nach Maschine, AG-Beginn, Versatz-MS, Losnummer sortiert und es erscheinen die AUF/AB Pfeile ![](offeneAG_Pfeile.JPG). 
Wenn Sie den aufwärtsgerichteten Pfeil anklicken, so wird zum Versatz 1ms hinzugefügt. 
Wenn Sie den aufwärtsgerichteten Pfeil anklicken, so wird zum Versatz 1ms abgezogen, solange bis Versatz-MS >=0 ist.
Bitte beachten Sie, dass die Grenze für die Verschiebung der Kalendertag ist.
Einen schnellen Überblick über die nächsten Schritte in der Fertigung erhalten Sie über das Journal Maschine+Material. So können Sie zum Beispiel steuern, dass in der nächsten Zeit, alle Arbeitsgänge mit dem bestimmten Material und der bestimmten Maschine gefertigt werden.
![](offeneAG_Journal.png)
Bitte beachten Sie dazu auch die Reihung welche durch die [grafische Auslastungsplanung] erfolgt.

Zusätzlich gibt es in Verbindung mit der Zusatzfunktion Maschinenzeiterfassung den Reiter Reihen.<br>
Hier haben Sie die Möglichkeit, manuell die detaillierte Reihung / Reihenfolge der Arbeitsgänge / Lose vorzunehmen. Das bedeutet:
1. wählen Sie die gewünschte Maschine aus
2. Klicken Sie nun auf "AGs anhand AG-Beginn sortieren" ![](Los_neu_reihen.gif). Damit wird anhand der aktuellen Reihenfolge der Arbeitsgang-Beginn-Datum in die interne Reihenfolgendefinition übernommen.
3. nun kann mit den Pfeilen geklickt werden -> Es werden nun das I_REIHEN (je Maschine) berechnet (anhand der aktuellen Reihenfolge des AG-Beginns)
Nun kann mit Pfeilen ![](Los_umreihen.gif) auf/ab umsortiert werden.
Diese Sortierung wirkt vor allem in Verbindung mit dem [Erfassungsterminal]( {{<relref "/fertigung/zeiterfassung/erfassungsterminal">}} ).<br>
Hinweis: Denke an die Kombination mit der automatischen [AG-Beginn Berechnung](#kann-der-losbeginn-automatisch-errechnet-werden)

#### Können einzelne Arbeitsgänge gezielt erledigt werden?
Durch Klick auf ![](Arbeitsgang_fertig.gif) Arbeitsgang Fertig(melden) kann dieser Arbeitsgang gezielt erledigt, genauer auf den Status Fertig gesetzt werden. Mit der damit verbundenen Aktualisierung der Liste verschwindet der Arbeitsgang damit zugleich aus der Auswahlliste und natürlich auch aus den offenen Arbeitsgängen in allen anderen Auswertungen.
Da es in der Praxis oft vorkommt, dass manche Arbeitsgänge von verschiedenen Menschen einfach nicht erfasst / übersprungen werden, kann durch Aktivierung des Parameters VORHERIGE_ARBEITSGAENGE_MITERLEDIGEN die Funktion zur Verfügung, dass damit beim Erledigen eines einzelnen Arbeitsganges alle davor noch nicht erledigten Arbeitsgänge mit erledigt werden.
Da das enterledigen durch Positionierung des Cursors auf einen erledigten Arbeitsgang und erneuten den Klick auf den grünen Haken erfolgt, wird bei der Enterledigung, unabhängig vom Parameter VORHERIGE_ARBEITSGAENGE_MITERLEDIGEN nur der gewählte Arbeitsgang enterledigt.
Der Parameter hat den Vorteil, dass damit implizit die Pflege der offenen Arbeitsgänge und damit die Auslastungsplanung besser wird.

#### Welche Lose können denn nun wie produziert werden?
Dafür haben wir im unteren Modulreiter offene AGs, den oberen Reiter Produzieren geschaffen.
![](offeneag1.jpg)

Das Ziel dieser Darstellung ist, damit in einfacher Weise, bereits angelegte Lose so zu optimieren, dass mit der idealen Reihenfolge möglichst geringe Rüstzeiten erreicht werden und zugleich auch Termingerecht geliefert wird.
Der erste Schritt ist hier sicherlich, dass man sich die zusammenpassenden Teile entsprechend sortiert, also z.B. Teile die auf der gleichen Maschine gemacht werden oder auch Teile die das gleiche Ausgangsmaterial haben usw..
Dann sortiert man diese z.B. nach AG-Beginn(-termin).
In der rechten Spalte = heute produzierbar, wird angezeigt, ob für dieses Los, ausgehend vom im Moment verfügbaren Lagerstand, ausreichend Material vorhanden ist. Bitte beachten Sie, dass hier auch für Unterstücklisten nur die derzeit im Lager befindlichen Mengen berücksichtigt werden. Die Verbrauchsberechnung erfolgt in der Reihenfolge in der Sie im Moment die Liste sortiert haben. D.h. es wird zuerst das Material für das in der ersten Zeile befindliche Lose verbraucht. Das was davon übrig bleibt im Los der zweiten Zeile usw.. Ist für das jeweilige Lose theoretisch ausreichend Material vorhanden, so wird der Haken bei heute produzierbar angezeigt.

Angelegte Lose verdichten

In der Liste Produzieren können durch Klick auf ![](offeneAG_verdichten.gif) verdichten auch bereits angelegte Lose verdichtet werden. Dazu können mehrere Lose, üblicherweise einer Stücklistennummer (= gleiche Baugruppen) aber auch mehrerer Baugruppen per Mehrfachauswahl markiert werden.
Für die Verdichtung wird wie folgt vorgegangen:
1. es werden nur Lose gleicher Stücklisten in sich zusammengefasst
2. es wird immer auf das vom Beginntermin älteste Los hin verdichtet
3. für dieses älteste Los wird die Losgröße so erhöht, dass damit die gesamte Losgröße der verdichteten Lose produziert wird.
4. die nun überzählen Lose werden storniert.
5. Lose bei denen nachträgliche Materialentnahmen gebucht sind, werden beim Verdichten ausgelassen.
6. da das Verdichten NUR für angelegte Lose möglich ist, werden in dieser Sicht nur Lose im Status angelegt angezeigt.

### Markierte Lose ausgeben

Durch Klick auf ![](Lose_kombiniert_ausgeben.gif) markierte Lose ausgeben, werden alle Lose auf einmal ausgegeben.
Bitte beachten Sie, dass diese in der Reihenfolge von oben nach unten ausgegeben werden. Wird die Ausgabe bei einem Los abgebrochen, werden auch die nachfolgenden Lose nicht mehr ausgegeben.
Da bereits ausgegebene Lose im Reiter Produzieren NICHT angezeigt werden, sehen Sie die durch den Abbruch noch nicht ausgegebenen Lose weiterhin in der Liste und können so komfortable diese weiter behandeln.
Bei der Ausgabe erscheint die Abfrage wie bei ![](Lose_kombiniert_ausgeben2.jpg) den Ausgaben vorgegangen werden sollte.
Nur produzierbare bezieht sich auf das von Los zu Los sich ändernde verfügbare Material (das in der Reihenfolge erste Los verbraucht das Material und dann kann das nachfolgende nur ausgegeben werden, wenn ausreichend verfügbar ist). Direkt drucken bedeutet, dass der Fertigungsschein jedes Loses ohne dem Umweg über die Druckvorschau erfolgt.

### Export nach PROfirst
[Siehe bitte]( {{<relref "/docs/installation/30_schnittstellen/profirst/#export-der-zu-schneidenden-lose--arbeitsg%c3%a4nge  " >}} ).

#### Was ist der Eintrefftermin?
Im Reiter Fehlmengen finden Sie die Spalte Eintrefftermin. Hier wird das Datum eingetragen, zu dem Zeitpunkt man den Lagerzugang erwartet. Dieser früheste Eintrefftermin berücksichtigt zusätzlich zu den Bestellpositionen auch die zu erwartenden Losablieferung (= frühester Ende-Termin eines offenen Loses). So erhalten Sie schnell Überblick über die Engstellen oder zu kritischen Terminen, Lagerständen  zum Beispiel in Unterlosen. Um etwas Sicherheit zu erreichen, wird bei den Bestellungen nur der vom Lieferanten bestätigte Termin angezeigt. D.h. Bestellungen die noch ohne eingetragener Auftragsbestätigung sind, werden hier nicht berücksichtigt.

#### Zusätzliche Informationen aus der Stückliste bei der Ausgabeliste des Loses andrucken?
Für den Ausdruck der Ausgabeliste stehen unter anderem auch die Felder Stückliste Kommentar und Stückliste Position zur Verfügung.
Wenn Artikel/Menge und Montageart gleich sind, wird der Kommentar/Position aus Stkl-Position angezeigt, beim Verdichten werden die Positionen/Kommentare mit , getrennt angeführt.
Bitte wenden Sie sich zur Anpassung Ihres Reports an Ihren **Kieselstein ERP** Betreuer.

#### Kann der Fortschritt eines Loses angegeben werden?
<a name="Losbewertung"></a>
Unter dem Menüpunkt Bearbeiten finden Sie mit Bewertung/Kommentar die Möglichkeit zum Beispiel den Fortschritt des Loses zu erfassen.

![](Losfortschritt_Bewertung.JPG)

Geben Sie hier in Prozent den Fortschritt an und hinterlegen Sie einen Kommentar um die Einschätzung zu begründen. Wenn der Parameter BEWERTUNG_IN_AUSWAHLLISTE auf 1 gesetzt ist, so wird nun in der Auswahlliste der Lose die Bewertung angeführt. So erhalten Sie einen schnellen Überblick über den Status und Fortschritt der einzelnen Lose. Zusätzlich können die Informationen in der [Auftragsnachkalkulation]( {{<relref "/verkauf/auftrag/nachkalkulation/#gibt-es-eine-m%c3%b6glichkeit-den-laufenden-losfortschritt-in-der-auftragsnachkalkulation-anzuf%c3%bchren" >}} ) berücksichtigt und zu verschiedenen Auswertungen hinzugefügt werden - kontaktieren Sie hierzu Ihren **Kieselstein ERP** Betreuer.<br>
Diese Erfassung ist auch über den KDC (Barcodescanner) möglich - kontaktieren Sie hierzu Ihren **Kieselstein ERP** Betreuer.

#### Wie sind Änderungen in den Kopfdaten des Loses möglich?
Wenn man auf das Bearbeiten Icon der Los-Kopfdaten im Status > Angelegt und <=Teilerledigt die Frage "Ausgaben zurücknehmen?" mit Nein beantwortet, dann können nun die Kopfdaten mit Ausnahme von Beginn/Ende/Losgröße/Stückliste (Ziellager, wenn Status = Teilerledigt) editiert und gespeichert werden. Es erfolgt keine Materialbuchung.

![](ausgaben_zuruecknehmen.PNG)

#### Können Lose importiert werden?
Es steht auch ein XLS Import für Lose zur Verfügung.
Sie finden diesen im Modul Los, Menüpunkt Los, XLS-Import ![](Los_Import.gif).
Wichtig, wie in den anderen XLS Importen auch, sind hier die richtigen Spaltenüberschriften.
Diese sind:
| Spaltenüberschrift | Bedeutung |
| --- | --- |
| Stueckliste | Die Artikelnummer der gewünschten Stückliste |
| Projekt | Die Projektbezeichnung.<br>**Hinweis:** Die früher gegebene Einmaligkeit der Projektbezeichnung (FAUF) ist nicht mehr gegeben.|
| Losgroesse | die Menge die produziert werden sollte.<br>**Hinweis:** Ist die Menge 0, so wird diese Zeile nicht importiert. |
| Endetermin | das Datum an dem das Los fertig sein sollte.<br>Hinweis: Der Beginntermin wird anhand der Defaultdurchlaufzeit der Stückliste errechnet. |
| Kommentar | Der Eintrag in das Feld Kommentar |

Für die Losgröße gehen wir davon aus, dass dies eine Zahl ist.
Für die Termine gehen wir davon aus, dass diese als Datum formatiert sind.

**Info:** Damit der Pfad nicht jedesmal neu eingestellt werden muss, haben wir dies so programmiert, dass nach der Auswahl der Importdatei der Pfad abgespeichert wird. Somit kann beim nächsten Import dieser Pfad wieder vorgeschlagen werden.

#### Können Solldaten importiert werden?
Es in freie Materiallisten auch Solldaten importiert werden.
Musterdatei siehe lossollmaterial.csv mit Artikel;Menge
Der Import wird gleich wie der Import des Ist-Materials über den CSV Import ![](Losimport1.gif) aufgerufen.
Je nach Status des Loses wird dies entsprechend importiert. D.h. ist das Los im Status angelegt, wird angenommen, dass dies Solldaten sind.
Ist das Los im Status ausgegeben, wird dies als Materialverbrauch, siehe unten, importiert.

#### Können Materialverbrauche importiert werden?
Ja. Beim Import kann zwischen zwei Arten gewählt werden.
1. Echte Verbrauchsbuchung inkl. Chargen- bzw. Seriennummern
2. Harmonisierung von Lagerständen
Für den Import klicken Sie im Reiter Material des Loses auf ![](Losimport1.gif) CSV-Import Istmaterial.
Wählen Sie dann bitte die zu importierende Datei aus und beantworten die nachfolgende Frage
![](Losimport2.jpg)
entsprechend. D.h. damit wird entschieden welche Art von Daten Sie von z.B. Ihrem Bestückungsautomaten erhalten haben, womit sich zwei verschiedene Vorgehensweisen umsetzen lassen:
1. die gesamten Bauteile werden faktisch im Bestückungsautomaten gelagert und Ihr Automat weiß immer nur "seinen" Lagerstand. Damit ist es auch erforderlich, dass Sie nach jedem Los die Daten aus dem Bestückungsautomaten exportieren und exakt in dieser Reihenfolge wieder in Ihr **Kieselstein ERP** einlesen.
2. Es werden vom Automaten die Verbrauche JE Bestückungsauftrag in die CSV-Datei gespeichert. Damit müssen diese Verbrauche (nur) passend zum jeweiligen Los eingelesen werden. Trotzdem ist bei eventuellen Chargen die richtige Reihenfolge des Einlesens für den passenden Verbrauch der Chargennummern (siehe unten) wichtig.

Das Dateiformat ist CSV mit Strichpunkt getrennt.
Die Reihenfolge der Felder ist:
Artikelnummer;Menge;SerienChargennummer
Als Dezimaltrenner wird der Punkt verwendet. Bitte keine Tausendertrenner verwenden.
Wir der resultierende Lagerstand eingelesen so können keine Serien/Chargennummern übergeben werden. In diesem Falle wird immer die älteste Serien-/Chargennummer zuerst verbraucht.
Genauer: Wenn ein Artikel Seriennummer oder Chargennummern geführt ist und es wird eine Serien- bzw. Chargennummer übergeben, so wird automatisch der älteste Wareneingang mit dessen Serien-/Chargennummer verwendet.

Diese Funktion wird gerne für eine Harmonisierung mit einem externen Bestückungsautomaten mit oder ohne eigener Lagerwirtschaft verwendet.
D.h. wenn je gefertigtem Los der Lagerstand harmonisiert wird, ist die Differenz die Menge die vom Automaten verwendet wurde. Damit sind auch die, gerade bei SMD Automaten oft verworfenen Bauteile, welche einen Mehrverbrauch darstellen, darin enthalten.
Bitte beachten Sie, dass beim Import der verbrauchten Menge diese immer dazugebucht wird. Beim Import des resultierenden Lagerstandes wird immer die Differenz zwischen dem Lagerstand in **Kieselstein ERP** und der übergebenen Liste in das jeweilige Los gebucht. Beachten Sie dies bei der Ausgabe des Loses. Wird der reine Verbrauch eingelesen, so darf vorher bei der Losausgabe KEIN Material gebucht werden. Wird der Lagerstand eingelesen und damit nur die Differenz gebucht, so kann die Materialentnahme in **Kieselstein ERP** durchaus auch mit der Ausgabe mit gebucht werden (Standardeinstellung) und es wird nur die Differenz gebucht.

Der Zweck dieser Funktion ist, dass Ihr ERP System immer das führende System sein muss und dadurch jeder Benutzer z.B. der Einkauf, jederzeit über den richtigen Lagerstand informiert ist.

#### Import Tracereport
Eine weitere Möglichkeit ist einen sogenannten Tracereport, also eine Aufstellung welche Chargen / Batch-ID für welche Stücklisten verbraucht wurden einzuspielen und so die oft umfangreiche Chargenerfassung deutlich zu vereinfachen.
Sie finden diesen Import im Modul Los, Menüpunkt Los, Import, TraceImport

Da, gerade bei SMD, die Fertigung / Bestückung oft in mehreren Durchläufen erfolgen muss, denken Sie an SMD Oberseite / Unterseite, ist der Import immer additiv, also hinzufügend.

Eine Musterdatei finden Sie im Musterdateiverzeichnis unter TraceReport_Muster.txt.
Der Aufbau ist Text, Tabs getrennt.
Die wesentlichen Informationen werden aus der ersten Spalte (Layout) und aus der 10.Spalte (Batch) verwendet.
Zusätzlich können zwei Arten von Traceimport-Formaten unterschieden werden.
Die Tracedatei der My9 hat 13Spalten. In der Spalte 10 ist der Batch, welcher von **Kieselstein ERP** als Chargennummer eingelesen wird, enthalten.
Die Tracedatei der My200 hat 14Spalten. Hier findet sich der Batch, also die Chargennummer in der Spalte 11.
In der Spalte 1 = Layout steht die Artikelkurzbezeichnung der Stückliste. Diese wird noch um die Definition ergänzt, dass nur die Zeichen bis zum ersten / Schrägstrich (ohne demselben) verwendet werden.
Die Spalte Batch entspricht der verwendeten Chargennummer.
Die Spalte 5 = PCB-Nummer liefert die laufende Leiterplatten-Nummer auf die die Bauteile bestückt wurden.
Jede Zeile des Tracefiles repräsentiert 1Stk Verbrauch der jeweiligen Chargennummer im gefundenen Los.
Beispiel für die Spalte Layout:
Im Tracefile steht: 358417-297113/SE171208Co. Es wird eine Stückliste mit der Kurzbezeichnung 358417-297113 gesucht.

Für die Materialbuchung muss das Los im Status In Produktion bzw. teilerledigt sein.
Trennung Ober / Unterseite
Erfolgt dadurch, dass "nur" das erhaltene Material verbucht wird.
Damit werden die verschiedenen Materialverbrauche eingebucht.
D.h. wenn später noch was nachbestückt wird, kann man das auch einspielen

Ablauf des Imports:
Starten Sie den Importvorgang und wählen Sie die entsprechende Datei aus.
![](Traceimport1.jpg)
Nun klicken Sie auf aktualisieren. Hier erhalten Sie eine Anzeige über die fehlerhaften Einträge in dieser Tracedatei in Bezug auf die in Ihren, im Status ausgegebenen, in Produktion oder teilerledigt befindlichen Lose.

![](Traceimport2.jpg)
Zeilen ohne Fehler werden in dieser Vorabprüfung nicht angezeigt.
Geprüft wird, ob der anhand der Charge ermittelte Artikel in einem der Lose benötigt wird.
Entspricht der Import Ihren Vorstellungen, so klicken Sie auf ![](Traceimport3.jpg).
Es werden nun die ermittelten Bauteile den Los-Soll-Materialpositionen zugewiesen und dies in einer Buchungsliste angezeigt.
![](Traceimport4.jpg)
| Spalte | Bedeutung |
| --- | --- |
| Stückliste | die Artikelnummer der Stückliste des Loses |
| Material | die Artikelnummer des zu buchenden Materials |
| Bezeichnung | die Bezeichnung des Artikels des Materials |
| Charge | die verwendete Chargennummer |
| PCB | die Anzahl der Leiterplatten die mit dieser Menge bestückt werden konnten |
| Lagerstand | der Charge |
| Offen | wieviel sollte noch in das Los gebucht werden |
| zu Buchen | die Aufgrund des Traceimportfiles zu buchende Menge der Charge auf die jeweilige Losposition ausgehend von der Originalposition |

Insbesondere wenn mehrere Baugruppen in diesem Tracefile enthalten sind, z.B. die Produktion eines ganzen Tages, können die Zuordnungen entsprechend unterschiedlich ausfallen. D.h. gegebenenfalls korrigieren Sie die zu buchende Menge. Hier ist es auch erlaubt, eine größere Stückzahl als tatsächlich laut Berechnung offen ist einzugeben. Beachten Sie jedoch, dass keine wie immer gearteten Übertragsberechnungen bzw. Korrekturen durchgeführt werden. D.h. wenn von Ihnen Mengen korrigiert / umgebucht werden, so müssen diese in sich stimmig sein.

In Verbindung mit der Ersatztypen-Verwaltung, werden für das Feld "zu Buchen" die hinterlegten Ersatztypen mit berücksichtigt. Das bewirkt, dass die Sollmengen des Originalartikels als auch die Sollmengen der dem Originalartikel zugeordneten Ersatztypen zusammengezählt werden. Der Verbrauch, durch die Chargen (Artikel) wird dann von oben nach unten den jeweiligen Losen / Lospositionen zugewiesen. Damit wird erreicht, dass bei der Bestückung sehr flexibel die vom Automaten verwendeten Chargen, egal ob diese auf die Originalartikel oder die Ersatzartikel zeigen, vorgegangen werden kann.
Zusätzlich werden die Verbrauche nach Losnummern aufsteigend sortiert. D.h. die Lose mit den niedrigeren Nummern werden zuerst aufgefüllt.

Wurden die Mengen geprüft und für gut befunden, klicken Sie auf durchführen ![](Traceimport5.jpg), womit die Materialbuchungen vom Lager ins Los tatsächlich durchgeführt werden.
Hinweis: Eine Verriegelung der importierten Datei erfolgt nicht, da diese meist auch noch von anderen Systemen verwendet wird. Am Ende des Importes wird die importierte Datei um _importiert ergänzt. Damit ist ersichtlich, dass diese Datei schon einmal importiert wurde. Bitte achten Sie darauf, die gleiche Datei nicht mehrfach zu importieren.

Mögliche Fehlermeldungen des Traceimports:
- Es wurde keine Stückliste der Kurzbezeichnung 358417-297113 gefunden<br>
Bitte stellen Sie die Definition der Kurzbezeichnung Ihrer Stücklisten richtig
- Es konnte kein Artikel der Chargennummer R123456 gefunden werden.<br>
Es gibt keinen lagernden Artikel dieser Chargennummer
- Es konnte kein Los im Status, ausgegeben/In Produktion/Teilerledigt gefunden werden<br>   Bedeutet: Es wurde ein passender Artikel für die erhaltene Chargennummer gefunden. Es konnte aber kein Los **der Stückliste** gefunden werden, in dem dieser Artikel noch Fehlmenge ist

**WICHTIG:** Für den Traceimport ist nur die Chargennummer relevant. Die für die Chargennummer auch zur Verfügung stehende Version, welche in manchen Installationen anders verwendet wird, wird bei der Ermittlung des verfügbaren Lagerstandes dieser Chargennummer nicht berücksichtigt.

#### Ablieferstatistik, Leistungskennzahl der Fertigung
Unter Journal, Ablieferstatistik finden Sie eine Auswertung über die Ablieferungen, also die Erfolgsmeldungen Ihrer Produktion.
Diese können für Alle Artikel, einen bestimmten Artikel, nur die Kopfstücklisten oder nur die Kopflose, ev. für einen bestimmten Zeitraum erstellt werden.
![](Ablieferstatistik1.gif) ![](Ablieferstatistik2.gif)
Für die Auswertung der Ablieferungen eines einzelnen Artikels wird derjenige Artikel also diejenige Stückliste vorgeschlagen, welche in der Auswahlliste der Lose ausgewählt ist. Ist hier ein Artikel ausgewählt, so wie im linken Bild, so wird dieser vorgeschlagen, ist hier eine Materialliste ausgewählt (so wie im rechten Bild), so wird diese Möglichkeit nicht angezeigt.

Je nach Sortierung wird am Ende der Auswertung bzw. bei jedem Artikel die Entwicklung dargestellt.
Ist die Sortierung nach Artikel ausgewählt und sind im Auswertezeitraum mehr wie drei Ablieferungen eingetragen, so werden Gestehungspreise mit Material- und Arbeitsanteil auch grafisch dargestellt.
Ist die Sortierung nach Ablieferdatum ausgewählt, so wird am Ende des Journals die Gesamtrückmeldung in einer Monatsbetrachtung grafisch angezeigt. Also die Werte die Ihre Fertigung rückgemeldet hat.

#### Werden in der Ablieferstatistik Lose doppelt betrachtet?
Jein bzw. das kommt darauf an.
Bei der Berechnung des Ablieferpreises eines Loses gehen immer auch die Kosten einer Unterbaugruppe, eines Unterloses mit ein. Daraus ergibt sich implizit, dass der Wert eines Unterloses im Wert des übergeordneten Loses enthalten ist.
Um diese Unterschiede betrachten zu können, haben wir die Auswahlmöglichkeiten nur Kopfstücklisten bzw. nur Kopflose geschaffen.
Hier ist der Unterschied auch in der Art, in dem Ziel Ihrer Auswertung zu suchen.
**Beispiel:** Es sollte wöchentlich die Leistungskennzahl Ihrer Fertigung im Sinne der verkaufsfähige Produkte ermittelt werden.
Sind nun, aus Sicht der Stücklistenstruktur, Ersatzteile (die Sie wiederum extra gefertigt haben) auch verkaufsfähige Produkte oder sind diese Unterbaugruppen eben nur Halbfertigfabrikate?
Wenn diese nur Halbfertigfabrikate sind, so wählen Sie die Stücklistenstruktur als Basis und haken nur Kopfstücklisten an. Damit kann es auch vorkommen, dass wenn in einer Woche nur Unterbraugruppen gefertigt werden, da diese nicht verkaufsfähig sind, die Fertigung KEINE Leistung (im Sinne des Verkaufes) erbracht hat. Dafür ist dann hoffentlich die darauffolgende Woche wieder überdurchschnittlich.
Wenn hier auch der Verbrauch bzw. der nicht Verbrauch der Ablieferungen der Unterlose in übergeordnete Lose mit betrachtet werden sollte, so wählen Sie nur Kopflose. Hier wird die Struktur aus den Materialbewegungen ermittelt.

#### Erforderliche Rechte
Zusätzlich zum mindestens erforderlichen Leserecht für das Modul Los / Fertigung sind für die unteren Modulreiter folgende Rechte erforderlich:
- offene Arbeitsgänge ... FERT_LOS_CUD und das Modul Zeiterfassung
- Interne Bestellung ... FERT_LOS_CUD
- Wiederholende ... FERT_LOS_CUD
- Grunddaten ... LP_DARF_GRUNDDATEN_SEHEN

Das Recht FERT_LOS_CUD hat auch Auswirkungen darauf, welche Journale vom Benutzer aufgerufen werden können.
Hat er dieses Recht, so stehen alle Menüeinträge für den Menüpunkt Journal zur Verfügung.
Hat er dieses Recht nicht, also z.B. nur FERT_LOS_R so werden im Menüpunkt Journal nur die Journale für Alle Lose, Offene Lose, Offene Arbeitsgänge angezeigt. Ab der Ablieferstatistik ist das schreibende Recht für die Lose erforderlich.

#### Einmalartikel
Es kommt, gerade in innovativen Betrieben immer wieder vor, dass ein einmaliger Artikel z.B. für einen Prototypen benötigt wird. Dies kann theoretisch mit einem Handartikel im Los und dann in der Bestellung usw. abgebildet werden. Meist ist aber die zusätzliche Forderung, dass der Artikel auch im Bestellvorschlag erscheinen sollte und somit automatisch die Beschaffung ausgelöst wird.
Dafür steht die Funktionalität des Einmalartikels in der Buchung von zusätzlichem Los-Soll-Material zur Verfügung.
D.h. wenn Sie im Los mit neu weitere Sollpositionen hinzufügen, danach auf die Artikelauswahl klicken, so steht hier neben dem Neu auch die Anlage eines Einmalartikels ![](Einmalartikel_anlegen.gif) zur Verfügung.
Hier werden nur sehr wenige Erfassungsdaten für den Artikel abgefragt.
![](Einmalartikel_erfassen.gif)
Als zusätzliche Eigenschaft ist dieser Artikel grundsätzlich nach der Anlage versteckt und hat eine automatisch generierte fortlaufende Artikelnummer, welche mit ~ beginnt.
Sollten Sie nun nach der Fertigung Ihres Prototypen feststellen, dass Sie diesen Artikel doch öfter benötigen, so muss lediglich das Versteckt entfernt werden und dem Artikel eine Artikelnummer Ihren Regeln entsprechend gegeben werden.

#### Rankingliste
Die Rankingliste dient der vollautomatischen Steuerung der Dringlichkeit der zu fertigenden Lose, gegliedert nach Fertigungsgruppe.
Ziel:
Liste mit dem Ranking der Fertigungsaufträge sortiert nach Fertigungsgruppe und darin nach Termin

Ausgangspunkt ist die Reservierungsliste, also alle reservierten Stücklisten.

Als erstes werden die Lagerstände in der Reservierungsliste verbraucht. D.h. was von den Reservierungen vom Lager bedient werden kann, braucht nicht gefertigt werden.
Wenn es kein ausgegebenes Los in der Fertigung der Artikel gibt, so wird der Artikel nicht weiter berücksichtigt.
Hintergrund: Der Mensch entscheidet ob der Fertigungsbeginn sinnvoll ist oder nicht.
Es dürfen hier NUR bereits ausgegebene Lose berücksichtigt werden.
Ein Los darf nur EINMAL aufscheinen.
Von welcher Fertigungsgruppe wird dieser Artikel gefertigt ?
Liste je Fertigungsgruppe (Seitenumbruch dazwischen) erstellen, sortiert nach Termin und Soll-Fertigungszeit (Dauer) absteigend.
Spezialfall Kabelschneider:
Die Kabelschneider müssen jedes Los teilfertigen, da ja zuerst die Kabel vorbereitet = geschnitten werden müssen.
D.h. grundsätzlich müssen die Kabelschneider alle Lose fertigen, mit Ausnahme derer, die sie bereits gefertigt haben. Dieser Produktionsschritt wird bereits durch den Zusatzstatus, Kabel Geschnitten erfasst.
Unterstücklisten
Hier gehen wir davon aus, dass diese terminlich vor den zu fertigenden Stücklisten eingeplant sind und damit automatisch rechtzeitig erscheinen. Sie werden also nicht extra berücksichtigt, sondern eben nur über die Reservierungen. D.h. das auch die Reservierungen der Lose mit eingehen.

Um die Listen kurz zu halten, werden nur die Lose, welche die nächsten 14Tage zu fertigen sind angedruckt.

#### Materialbedarfsübernahme
<a name="Materialbedarfsübernahme"></a>
Aus der Offline Erfassung aus der HVMA2 steht die Funktionalität der Materialbedarfsübernahme zur Verfügung. In kurzen Worten gibt der Mitarbeiter unterwegs, z.B. ein Monteur, an, welches Material er z.B. für die Fertigstellung seines Projektes noch benötigt.
Bei der Synchronisierung der Daten der HVMA mit Ihrem **Kieselstein ERP** werden diese Bedarfsmeldungen in die **Kieselstein ERP** Datenbank übernommen und müssen nun vom Lagerverantwortlichen / -zuständigen entsprechend gebucht / übernommen werden.
Das Statusdiagramm jeder einzelnen Zeile der Materialbedarfe ist wie folgt:
1. Angelegt ... die Daten sind frisch durch die Synchronisierung eingespielt (RestAPI)
2. offen ... die Daten wurden (automatisch durch den Import) ausgedruckt. D.h. der Lagerverantwortliche weiß, das Material vorzubereiten ist
3. gelesen ... bedeutet zur Kenntnis genommen, aber (noch) nichts gemacht
4. verbucht ... bedeutet ist übernommen und ins Los verbucht. Das kann sein, dass bereits eine Materialausgabe gemacht wurde oder dass Fehlmengen erzeugt wurden (und damit der Bedarf im Bestellvorschlag erscheint) oder auch dass die Rückgabe von Material ans Lager gebucht wurde.
Für die Verbuchung wird auch protokolliert, wann dies von welcher Person durchgeführt wurde.

Info: Materialentnahmen werden immer als nachträgliche Materialentnahme im Los eingetragen.

#### Können Lose automatisch erledigt werden?
Ja. Es gibt einen Automatikjob mit dem versucht wird alle Lose, deren Ablieferung >= der Losgröße ist und deren letzte Ablieferung mehr als drei Tage (je nach Parametrierung) vergangen ist, automatisch zu erledigen. Kann diese Erledigung nicht durchgeführt werden wird das Los übersprungen und dies beim nächsten Lauf / Tag wieder versucht.
D.h. wenn "alte" Lose als unerledigt stehen bleiben, so versuchen Sie die Erledigung manuell durchzuführen, um die Ursache für das Nicht-Erledigen durch den Automatiklauf herauszufinden.<br>
Hinweis: Der Automatiklauf muss aktiviert werden (System, Automatik, Lose erledigen)

#### Was bedeutet das Ausrufezeichen neben dem Losstatus?
Es wird bei einer Ablieferbuchung durch das Terminal bzw. einer mobilen Buchung, versucht den aktuellen Ablieferpreis zu errechnen.
Konnte dies nicht durchgeführt werden, was eine Vielzahl von Gründen haben kann, wird neben dem Losstatus der Zusatzstatus ![](Ablieferpreis_ungueltig.gif) Ablieferpreis angezeigt. Diese bedeutet, dass von der Ablieferautomatik der Ablieferpreis nicht errechnet werden konnte und daher die Preisaktualisierung der Ablieferung manuell durchgeführt werden sollte.
Die Details wann dies versucht wurde finden Sie im Reiter Zusatzstatus
![](Zusatzstatus_Ablieferpreis.jpg)
Hier wird das Datum angezeigt zu dem dieses Verhalten festgestellt wurde.
Zum Beheben wechseln Sie bitte in den Reiter Ablieferung und klicken auf ![](Ablieferpreis_neu_kalkulieren.gif)Ablieferpreis neu errechnen. Ev. sollten Sie, nach gültiger Berechnung des neuen Ablieferpreises den Zusatzstatus entfernen.

#### Abbildung von MSL Anforderungen?
In speziellen Elektronik-Fertigungen kommt manchmal auch die Frage nach der Abbildung des [Moisture Sensitivity Level](https://de.wikipedia.org/wiki/Moisture_Sensitivity_Level).
Hier geht es darum, dass aus Gründen der Fertigungsqualität die Elektronikbauteile nur eine kurze Zeit in freier Luft (Floor Life) sein dürfen.
Hier werden 9 Stufen unterschieden, die je nach geforderter Qualität bei den Bauteilen zu beachten sind.
In der Regel werden die Stufen 2 bis 5a verwendet

| Stufe | Zeit | Bedingung |
| --- |  --- |  --- |
| 1 | unbegrenzt | 30 °C / 85 % RH |
| 2 | 1 Jahr | 30 °C / 60 % RH |
| 2a | 4 Wochen | 30 °C / 60 % RH |
| 3 | 168 Stunden | 30 °C / 60 % RH |
| 4 | 72 Stunden | 30 °C / 60 % RH |
| 5 | 48 Stunden | 30 °C / 60 % RH |
| 5a | 24 Stunden | 30 °C / 60 % RH |

Da in aller Regel dies selten und nur für sehr wenige Bauteile zur Anwendung kommt, schlagen wir derzeit folgende Vorgehensweise vor:
1. Bitten Sie Ihren **Kieselstein ERP** Betreuer, die Definition (in der Technik des Artikels) für Farbcode auf MSL zu ändern.
2. Tragen Sie die Stufen und deren Dauer in den Grunddaten des Artikels im Reiter MSL ein.
3. Definieren Sie nun die MSL sensitiven Artikel im Reiter Technik des jeweiligen Artikels ![](MSL_eintragen.gif).
4. Sie bekommen nun bereits in der Losausgabeliste die entsprechenden MSL Codes angedruckt, womit die Behandlung der Artikel definiert ist
5. Bei der Entnahme der Artikel aus dem Feuchteschutzbeutel / -bereich, beginnt die Floor Life Time zu laufen. D.h. drucken Sie nun ein Artikeletikett, gerne auch ein Los Entnahme Etikett aus, auf dem das aktuelle Datum und Uhrzeit und der MSL aufgedruckt sind. Kleben Sie dieses Etikett auf die Bauteil-Verpackung.
6. Wenn das Bauteil dann wieder in den Feuchteschutzbereich eingelagert wird, drucken Sie erneut ein Artikeletikett mit aktuellem Datum und Uhrzeit, womit die sich nun ergebende Floor Life Time definiert ist.
Beachten Sie, dass dafür die Etiketten angepasst werden müssen. Dies könnte auch so gestaltet werden, dass eine Spalte für den händischen Eintrag der gesamten Floor Life Time vorgesehen ist und Sie somit sehr rasch erkennen können, wann das Bauteil das nächste Mal ausgebacken (getrocknet) werden muss.

**Hinweis:** Ev. sollte für diese Bauteile auch die Reel-ID verwendet werden, womit eine Vereinzelung der jeweiligen Rollen gegeben ist.

## Material rechtzeitig bestellen
Es kommt von vielen Anwendern immer wieder das Thema, wie vorgehen um Material rechtzeitig bestellen zu können.<br>
Hintergrund ist immer, dass die Konstruktion / Entwicklung / das Design eines neuen Gerätes / Produktes einfach seine Zeit dauert. Diese Zeit fehlt oft zum Schluss hin, also zum Auslieferzeitpunkt. D.h. wenn man nun die Beschaffung früher starten könnte / würde, so gewinnt man, rein durch organisatorische Maßnahmen den notwendigen Zeitpuffer.

**Wie also vorgehen?**<br>
Die Lösung dafür lautet **mitwachsende Stücklisten**.<br>
D.h. die Stücklisten ändern sich während des Konstruktionsprozesses solange bis sie den Wünschen des Kunden, den Vorgaben entsprechen. Es werden aber die Teile / Baugruppen die bereits bekannt sind, zum frühest möglichen Zeitpunkt in die Stücklisten eingetragen. Somit können daraus Lose angelegt werden und der Bestellprozess gestartet werden.

Ein Beispiel:
- Kunde erteilt Auftrag (oder die Geschäfts-/Verkaufsleitung gibt den Auftrag auch ohne schriftlicher Bestellung frei)
- Die Konstruktion, die Entwicklungsabteilung beginnt zu entwickeln, Dauer ca. 3Wochen
- Die entwickelten Stücklisten werden aus der CAD der Konstruktion nach **Kieselstein ERP** übernommen, die Lose angelegt, der Bestellvorschlag durchgeführt und die Bestellung an die Lieferanten übermittelt
- Die Beschaffung der teilweise auftragsspezifischen Teile dauert zwei Wochen (wenn's schnell geht, manchmal auch vier Wochen usw.)
- nun kann mit der eigentlichen Fertigung begonnen werden, diese dauert sechs Wochen
- Ergibt also insgesamt elf oder 13 Wochen oder mehr

Gelingt es nun, die Verteilung so zu machen, dass die Zukaufteile bereits bei Auftragserteilung bekannt sind, so kann, innerhalb weniger Stunden / Tage eine vorab Stückliste (aus der CAD, oder auch manuell in **Kieselstein ERP**) angelegt werden und so der Beschaffungsvorgang um drei Wochen früher gestartet werden.

**Kann das funktionieren?**<br>
Unserer Meinung nach ja, denn bereits in der Angebotsphase haben Sie ein Konzept welche Teile benötigt werden, um den Auftrag erfüllen zu können. D.h. die wesentlichsten Einkaufskomponenten sind bekannt. Und ob Sie nun 5m Kabel mehr oder weniger brauchen, oder Schrauben oder .... ist natürlich für die Funktion Ihres Produktes wesentlich, die Arbeit liegt aber oft im fertigen / im Verbauen von vorhandenen Komponenten und je früher Sie diese im Hause haben, desto früher können Sie mit der eigentlichen Fertigung beginnen und termingerecht fertig werden.

**Wie also vorgehen?**
- Kunde erteilt Auftrag
- Konstruktion erstellt eine vorab Stückliste in der die wichtigen Komponenten enthalten sind. Dauer: 1/2Tag
- Die vorab Stückliste wird nach **Kieselstein ERP** übernommen, die Lose angelegt, der Bestellvorschlag durchgeführt und die Bestellung an die Lieferanten übermittelt
- Die Beschaffung der Teile dauert 2-4Wochen, während dessen konstruiert die Konstruktion weiter
- die nun fertigen Stücklisten werden aus der CAD nach **Kieselstein ERP** übernommen und die Lose aktualisiert.<br>
Nun wird erneut ein Bestellvorschlag durchgeführt und eventuell noch fehlende Artikel werden nachbestellt.
- Damit kann die Fertigung um 2,5 bis 3 Wochen früher beginnen und ist innerhalb der Fertigungszeit von 6-7 Wochen am Ziel

<u>Nachteil:</u>
Es könnte sein, dass Sie in der Phase der Vorabstückliste falsche Artikel spezifizieren. Je nach Art Ihres Unternehmens ist es egal ob die Teile etwas länger auf Lager liegen. Sie werden sowieso verbraucht und aufgrund der Art der Teile fällt die Kapitalbindung nicht wirklich ins Gewicht.<br>
Selbstverständlich wird man sich bei teuren Teilen eine Vorabbeschaffung sehr gut überlegen, die Konstruktion hinterfragen, ob sie sich sicher ist.

**Hinweis:**<br>
Für eine elegante Lösung [siehe]( {{<relref "/warenwirtschaft/stueckliste/importe/siemens_nx">}} ), Einkaufsstückliste.

## Hilfsstückliste
Beim Anlegen eines Loses werden die in einer Stückliste enthaltenen Hilfsstücklisten, mit bis zu 10 Ebenen in das Los eingebunden. Man könnte auch sagen, die Struktur wird flach gedrückt. D.h. alle Positionen der Hilfsstücklisten sind nun anstelle des Artikels der Hilfsstückliste im Los enthalten.<br>
Bitte beachten Sie auch das im Kapitel [Stücklisten zu den Hilfsstücklisten]( {{<relref "/warenwirtschaft/stueckliste/#hilfsst%c3%bcckliste" >}} ) gesagte.<br>
Hiervon ist vor allem die Einbindung der Arbeitspläne in das Los betroffen. Dieses Verhalten kann mit dem Parameter ARBEITSGAENGE_BEI_HILFSSTUECKLISTEN_VERDICHTEN gesteuert werden.<br>
1. 0 = Aus. Das bedeutet, es werden die Arbeitsgänge der Hilfsstücklisten an die Arbeitsgänge der Stückliste hinten angefügt. Genauer: Die durch die Überleitung entstehenden Arbeitsplanpositionen um die durch die Hilfsstückliste neu hinzukommenden Positionen ergänzt
2. 1= Verdichten
3. 2= Verdichten anhand Artikelgruppe
Es wird davon ausgegangen, dass hinter den Tätigkeiten, also den Arbeitszeitartikeln Artikelgruppen hinterlegt sind. Damit werden alle Tätigkeiten aus den Hilfsstücklisten in der Reihenfolge ihrer Definition,  ausgehend von der Stückliste bis zur letzten Arbeitsgangposition der letzten Hilfsstückliste, in die Sollzeiten des Loses verdichtet.<br>
Rüstzeiten und Stückzeiten werden aufsummiert. Davon ausgenommen sind Tätigkeiten die als externe Arbeitsgänge (Artikel, Bestelldaten, externer Arbeitsgang) definiert sind. Bei diesen wird die maximale Rüstzeit der jeweiligen Tätigkeit bestimmt und dieser Wert als Rüstzeit für die "verdichtete" Tätigkeit übernommen. Damit können Fremdfertigungstätigkeiten wie z.B. verzinken, lackieren, als faktisch eine externe Tätigkeit betrachtet werden. Zusätzlich wird bei dieser Einstellung der Hinweis wenn für Fremdfertigungs-Tätigkeiten kein Material (in den Positionen) zugeordnet ist, unterdrückt.
Wichtig: Die Verdichtung erfolgt nur, wenn dem Arbeitsgang **KEIN** Material zugeordnet ist.

## Kundenmaterial verarbeiten
<a name="Kundenmaterial verarbeiten"></a>
Viele unserer Anwender erhalten sogenannte Beistellware von Ihren Kunden zur Verfügung gestellt, welche dann über kundenspezifische Stücklisten bzw. Lose verarbeitet werden. Natürlich geht es auch hier darum jederzeit zu wissen wieviel vom Kundenmaterial noch da ist.<br>
Sollten Sie die Beschaffung des Materials auch für Ihren Kunden übernommen haben, aber das Material "nur" zwischenlagern, so beachten Sie bitte zusätzlich das unter [Lagerstand bereits verrechneter Ware]
( {{<relref "/docs/stammdaten/artikel/#lagerstand-bereits-verrechneter-ware" >}} ).<br>
D.h. Sie lagern die vom Kunden erhaltene Ware in ein Kunden-Lager ein. Dieses Lager ist üblicherweise als Konsignationslager gekennzeichnet.<br>
Im Los stellen Sie im Reiter Läger ein, von welchem Lager sich dieses Los bedienen darf. D.h. hier stellen Sie ein, dass das Los nur vom Kunden-Lager abbuchen darf.<br>
Somit ist der entsprechende Verbrauch auf die Kunden-Stücklisten-Artikel dokumentiert. Zugleich ist der Lagerstand Ihrer Artikel auch für Ihren Kunden transparent nachvollziehbar.<br>
Idealerweise definieren Sie in den Artikel, die von Ihrem Kunden beigestellt werden, den Kunden auch als Lieferanten, aber zum 0,00 Null Preis. D.h. über den Bestellvorschlag werden die Artikel vom Kunden abgerufen, mit allen Möglichkeiten des Bestellungsmodules. Hier finden wir immer wieder, dass zwar die Lieferung der fertigen Produkte von Ihrem Kunden angemahnt wird, aber der Kunde bis heute seine ganz speziellen Beistellteile nicht geliefert hat. Somit können Sie nachvollziehbar begründen, warum denn die negative Lieferantenbewertung nicht zutreffend ist, bzw. Ihren Kunden selber trifft.<br>
Es werden automatisch die Artikel die von Kundenkonsignationslagern abgebucht werden, mit einem Gestehungspreis von 0,00 in die Lose gebucht. Dies hat vor allem dann eine Bedeutung, wenn Sie für Zoll- oder Versicherungszwecke trotz allem den auf den Kundenkonsignationslager eingelagerten Artikeln einen entsprechenden Wert geben müssen. [Siehe dazu auch]( {{<relref "/verkauf/rechnung/proformarechnung/#veredelungskosten-in-der-proformarechnung" >}} ).

## Ladeliste
Es steht im Los auch das Journal der sogenannten Ladeliste zur Verfügung.<br>
Der wesentliche Zweck ist, dass für Unternehmen mit mehreren Standorten, der Transport zwischen den Standorten als eigener Arbeitsgang definiert wird.<br>
D.h. wenn Sie nun wissen möchten, welche Teile vom Werk A zum Werk B transportiert werden sollten, so tragen Sie im Arbeitsplan für die Lieferung von A nach B einen entsprechenden Arbeitsgang ein. Nun nutzen Sie die Ladeliste und filtern auf die Tätigkeit liefern an Werk B. Hier finden Sie alle Lose und deren Tätigkeit mit dem jeweiligen Vorgänger-Arbeitsgang und dem nachfolgenden Arbeitsgang. Wir gehen davon aus, dass die Arbeitsgänge und deren Fertigmeldung entsprechend akribisch erfasst sind. D.h. alle erledigten Arbeitsgänge sind mit Fertig abgeschlossen und erscheinen damit nicht mehr auf der Ladeliste.
Gerne kann dies aber auch im Formular so erweitert werden, dass nur dann die Arbeitsgänge auf der Ladeliste erscheinen, deren Vorgänger entweder fertig oder zumindest Gut-Stück rückgemeldet haben.<br>
D.h. der wesentliche Punkt für die Ladeliste ist, dass davon ausgegangen wird, dass auf dem jeweiligen Vorgänger Arbeitsgang bereits entsprechende Stückrückmeldungen gebucht sind. Wird der Transport durchgeführt so wird die gelieferte Stückzahl direkt auf dem Arbeitsgang Werk A liefern eingetragen.<br>
Somit ergibt sich aus der Differenz der Stückzahlen ob noch etwas zu liefern ist, oder eben nicht. Wurde die Lieferung mit der vollen Menge durchgeführt, also alle Teile geliefert, so sollte der Arbeitsgang (Werk A) liefern auf erledigt = fertig (Los, Reiter Zeitdaten, ![](Arbeitsgang_Fertig.gif) ) gesetzt werden.<br>
Alternativ kann dafür auch die Funktion direkte Stückrückmeldung genutzt werden.

Info: Für diese Funktion ist das Schreibrecht am Los FERT_LOS_CUD erforderlich.

## Manuelle Stückrückmeldung
Es steht auch die sogenannte Manuelle Stückrückmeldung im Modul Los zur Verfügung.
Die Idee dahinter ist, dass nur durch scannen der Losnummer möglichst einfach die offenen Arbeitsgänge der Reihe nach gebucht bzw. erledigt werden können.
Voraussetzung für die einfache Bedienung ist, dass auf dem erfassenden Rechner ein Barcodescanner zum Lesen der Los-Barcodes und der Personal-Barcodes mit einem sogenannten Wedgeinterface installiert ist (Bitte beachten Sie die Tastatureinstellung).
D.h. Sie finden im Modul Los, Menü, Bearbeiten, manuelle Stückrückmeldung.
Mit Aufruf dies Menüpunktes erscheint der Erfassungsdialog wie z.B.:
![](manuelle_Stueckrueckmeldung.jpg)
Der Ablauf ist nun wie folgt gedacht:
- Es wird die Losnummer gescannt<br>
Dadurch werden alle Arbeitsgänge des Loses angezeigt.
Die Erfassungszeile steht auf dem ersten noch nicht erledigten Arbeitsgang.
Erledigt bedeutet, dass bei diesem Arbeitsgang Fertig angehakt sein muss.
- Für den ersten noch nicht erledigten Arbeitsgang steht der Cursor / die Erfassung im Feld Menge gut. D.h. vom Anwender wird die Gut-Menge eingegeben.
- Wird nun Enter gedrückt, so steht der Cursor im Feld Kommentar.<br>
Hier stehen nun zwei Möglichkeiten der Erfassung zur Verfügung.
  - Es wird der Personalausweis des Mitarbeiters der diesen Arbeitsgang gemacht hat gescannt.Dadurch erscheint dieser unter Ausweis bzw. rechts daneben der Name des Mitarbeiters
  - Es wird ein Kommentar zu diesem Arbeitsgang eingegeben und anschließend TAB gedrückt, womit der Cursor im Feld Ausweis steht und so auch hier der Personalausweis des Mitarbeiters erfasst werden kann.
- Wird von der Gut-Menge ausgehen TAB gedrückt, steht der Cursor im Feld schlecht in dem die Schlecht-Menge erfasst werden kann.
- Mit einem weiteren TAB steht der Cursor auf Fertig, wo durch Drücken der Leertaste dieses Feld aktiviert werden kann. Alternativ kann Alt+F gedrückt werden, wodurch sofort das Feld aktiviert ist.<br>
Mit einem erneuten TAB sind Sie wiederum im Feld zur Erfassung der Ausweisnummer / der Person die diesen Arbeitsschritt ausgeführt hat.
- Nach dem Scannen der Ausweisnummer ist der Knopf Speichern aktiviert. Durch Enter, Alt+S, oder Klick auf den Button, wird die Erfassung abgespeichert.
- Nun wird entweder
  - Die Erfassung mit dem nächsten Arbeitsgang fortgesetzt, indem mit der Maus der Cursor auf den nächsten Arbeitsgang gestellt wird oder
  - durch Klick auf Zurück die Erfassung beendet oder
  - mit Alt+L ein anderes Los ausgewählt oder
  - da der Cursor automatisch im Los Erfassungsfeld ist, kann mit einem Scann eine neue Losnummer eingescannt werden, womit die Erfassung für das nun gescannte Los beginnt.

Um Details zu den Stückrückmeldungen des Arbeitsgangs einzusehen, klicken Sie neben dem Arbeitsgang auf den GoTo Button ![](manuelle_Stueckrueckmeldung_goto_Arbeitsgang.gif), wodurch im Hintergrund die Anzeige der Stückrückmeldungen auf diesem Arbeitsgang angezeigt werden. Legen Sie einfach die Fenster entsprechend nebeneinander.

## Rüstlager
Nachdem von unseren Elektronik Bestückern z.B. EMS Dienstleister immer wieder die Frage nach dem Lager direkt an den SMD Automaten kommt, hier die aktuellen Möglichkeiten dies in **Kieselstein ERP** abzubilden. Bitte denken Sie bei diesem Thema auch daran, ob Sie wirklich den Buchungsaufwand für die Buchung einer SMD-Rolle, die ca. 5,- bis 10,- € kostet, betreiben wollen.
Das Rüstlager ist meist dadurch physikalisch abgebildet, dass direkt am SMD Automaten entsprechendes Material aufgerüstet ist.
In manchen Betrieben wünscht man sich nun, dass man sieht wo sich denn nun welche Rolle des gleichen Bauteiles befindet. Daher werden die entnommenen Rollen auf das sogenannte Rüstlager umgebucht.
Um dies entsprechend abzubilden gibt es folgende Varianten.
- Es wird die Stückliste so eingestellt, dass keine Materialbuchung bei Losausgabe gemacht wird.<br>
Es wird ev. auch der zusätzliche Losstatus, Ausgegeben, der ja vor dem In Produktion kommt, aktiviert.<br>
Werden nun Bauteile (in der Elektronik spricht man gerne von Bauteilen, also Artikeln wie Widerständen, Kondensatoren usw.) für das Rüsten des SMD Automaten benötigt, wird das Material, die ganze Rolle (also 5.000Stk, auch wenn nur 100 benötigt werden) in das Los gebucht. Das geht auch mit dem Memor.<br>
Ist das Los fertig, muss es in der Regel wieder abgerüstet werden, vor allem auch um die Anzahl der verworfenen Bauteile tatsächlich zu erkennen. D.h. es werden, wiederum mit dem Memor, die Mengen ans Lager zurückgegeben, die übrig geblieben sind.<br>
Eine Erweiterung / Vereinfachung, die vor allem aus der Rückverfolgung kommt, ist, dass man jede (Bauteil-) Rolle kennzeichnet. Man spricht dabei von Reel-ID, was nichts anderes bedeutet, als dass das Bauteil Chargengeführt ist und jede Rolle eine automatisch vergebene Chargennummer bekommt.<br>
Damit spart man sich das zurückzählen, denn wenn die Rolle aus ist, klickt man im **Kieselstein ERP** auf Charge vollständig verbrauchen und das **Kieselstein ERP** bucht die noch lagernden Teile anteilig auf die betroffenen Lose. Diese Funktion steht auch mit dem Memor zur Verfügung.
  - Untervariante: Man bucht das Material automatisch bei der Ausgabe und akzeptiert, dass die Menge, wenn nicht im Hauptlager, dann am Rüstautomaten ist. Spart enorm Zeit und über die Artikel-Statistik bzw. die Funktion Artikel, Info, Frei in Fertigung, sieht man wo es noch sein könnte. Machen die meisten unserer Anwender, weil alles andere viel zu viel Zeit kostet.
- Wenn man wirklich das Rüstwaagerl (es gibt auch SMD Automaten mit zwei Rüst-Vorrichtungen. D.h. die erste steht im Produktionsprozess und die zweite wird für das nächste Los vorbereitet) als eigenständiges Lager nutzen will, kann man das über ein Rüstlager abbilden (ACHTUNG: Wenn zwei Rüstwaagerl, dann auch zwei Lager)<br>
Man stellt die Lose so ein, dass sie nur von einem der Rüstlager abbuchen dürfen.<br>
Nun legt man sich einen Ziellagerlieferschein an, also Hauptlager an Rüstlager und bucht z.B. mit dem Memor im Ziellagerlieferschein die ganzen ausgegebenen Mengen.
Wenn man nun das Los ausgibt, entnimmt es automatisch die Stückzahlen und reduziert das Rüstlager.
Dazu benötigt man in der Regel noch eine Darstellung welches Material man am Rüstlager noch benötigt. Das ist ein Sonderreport für den Druck des Losmaterials. Gibt's auch.
Üblicherweise macht man diese Version für die zur Verfügungstellung des Materials für seine externen Fertigungsdienstleister.

Wenn das wirklich so gewünscht wird, könnte man einen laufenden Rüst-Ziel-Lager Lieferschein machen, der z.B. nur monatlich oder Quartalsweise abgeschlossen wird. D.h. der solange angelegt bleibt, bis man das Monat abgeschlossen hat. Druckt man sich die Lieferscheinnummer im Barcode aus, kann das wieder sehr komfortabel mit dem Memor bedient werden.

Alternative die recht praktisch ist:
- man bucht die ganzen Rollen in das jeweilige Los
- in den Losausgabelisten werden die Übermengen (nicht frei in Fertigung, sonder das was zuviel auf das Los gebucht ist) mit angedruckt.
- bei der Rückgabebuchung werden die echt zurückgegebenen Stückzahlen der Bauteile wieder eingebucht.

Dies in Verbindung mit der VDA-Scann-APP, die für **Kieselstein ERP** Mitglieder zur Verfügung steht, ist eine sehr effiziente und praktische Funktion.

## Übermengen Verwaltung
In der Elektronik Produktion werden üblicherweise ganze Rollen von z.B. 10.000 Stk in die Produktion gegeben. Jede dieser Rollen hat eine eigene Reel-ID um die geforderte detaillierte Rückverfolgbarkeit gewährleisten zu können. -> Siehe dazu auch VDA-Scann-App

Da der Lagerstand im Lager immer stimmen muss (auch wenn manche der Meinung sind, dass dies nur ein Soll ist), werden auch für z.B. 100Stk ganze Rollen (oder eben Reste von Rollen) in das Los gebucht. Somit sind am Los entsprechende Übermengen gebucht.<br>
Um nun weitere Lose bedienen zu können muss man wissen ob Übermengen in der Fertigung unterwegs sind. Dafür gibt es den Report Losausgabeliste mit Übermengen (fert_ausgabeliste_mit_uebermenge). Hier sind die auf andere Lose gebucht Übermengen ersichtlich. Die Detailsicht ist in den Artikelreservierungen ersichtlich, auch negative Übermengen um einen Gesamtblick zu bekommen.

**ACHTUNG:** Der Bestellvorschlag berücksichtigt derzeit die Übermengen nicht, wobei dies üblicherweise kein Thema ist, denn eine 5.000 Stk Rolle für einen 10k Widerstand kostet aktuell 5,- €. Lieber eine Rolle zu viel auf Lager.

Ab der Version 1.0.15 steht die Übermengenverwaltung zur Verfügung.<br>
Um diese zu aktivieren, muss der Parameter LOS_UEBERMENGE_ERLAUBEN aktiviert (auf 1) gestellt werden.<br>
Zusätzlich kann mit dem Parameter ANZEIGEN_UEBERMENGE_ARTIKEL_AUSWAHLLISTE die reine Übermenge in der Artikelauswahlliste angezeigt werden.<br>
Weiters kann mit dem Parameter ANZEIGEN_UEBERMENGE_ARTIKEL_LAGER diese Information auch im Reiter Lager des Artikels angezeigt werden.

Die Übermengen sind faktisch negative Fehlmengen. Daher wurde diese Verwaltung in die Fehlmengenverwaltung mit aufgenommen. Das bedeutet, wenn es negative Fehlmengen gibt, so sind das Übermengen. Der wesentliche Unterschied ist, dass bei Übermengen der Termin der geplante Los-Endetermin ist. Bei Fehlmengen ist es, so der Produktionsbeginntermin.


## Fertigungsbegleitschein mit oder ohne Sollzeiten
Mit dem Parameter FERTIGUNG_FERTIGUNGSBEGLEITSCHEIN_MIT_SOLLDATEN kann aktiviert bzw. deaktiviert werden, dass an den Fertigungsbegleitschein überhaupt Solldaten übergeben werden.
D.h. wenn du auf deinen Fertigungsscheinen keine Sollzeiten findest, prüfe mal den Parameter.


### Wie wird der Gestehungspreis bei der Losablieferung gebildet?
Die Frage des Anwenders war:<br>
Wie berechnet sich der Gestehungspreis bei Teilablieferung eines Loses?<br>
Die ersten Baugruppen sind immer relativ teuer. Nun besteht der Verdacht, dass sich die Maschinen- und Arbeitszeit auf die abgelieferten Baugruppen aufteilt?<br>
Wenn wir die Baugruppen weiterverarbeiten obwohl sie noch den doppelten Preis haben, zieht sich ja ein entscheidender Fehler durch die Kette. Oder werden durch die Chargenführung alle darauf aufbauenden Preis neu berechnet sobald das erste Los komplett abgeliefert ist?

Die Antwort:<br>
Das hängt unter anderem auch vom Parameter ABLIEFERUNGSPREIS_IST_DURCHSCHNITTSPREIS ab.<br>
Ist dieser auf 0 = default, dann ist das beschriebene Verhalten Absicht, da man wissen will, wieviel den die Muster / die ersten Teile gekostet haben.<br>
Hier kommt die Qualität der Solldaten mit dazu. D.h. für die Berechnung des Gestehungspreises wird die Sollsatzgröße (Menge pro Satz) und die Sollarbeitszeit pro Satz herangezogen. Damit sieht man, dass bei den ersten Komponenten bei denen man teuer eingekaufte Muster einsetzt usw., dass dies einen völlig anderen Gestehungspreis ergibt, als dann bei den Folge-Komponenten.<br>
Schaltet man den Parameter auf 1, so wird immer der Durchschnittspreis errechnet und dieser auch aktualisiert. D.h. es kann dann durchaus sein, dass bei der Nachkalkulation der ersten Ablieferungen auch der hohe Preis heraus kommt, danach bewegt sich dann der Preis hoffentlich nach unten.

Ja es ist ein Thema der mitlaufenden Nachkalkulation, dass diese eben nur soweit stimmt, wie die Daten eingepflegt sind.

**Wichtig:** Es wird pro Artikel und Lager der Gestehungspreis errechnet, **nicht pro Charge**.

Aber immer wenn ein Lager-Nulldurchgang ist, wird der Gestehungspreis faktisch neu gebildet, denn<br>
                                          alter Lagerwert + hinzugekommener Lagerwert (WE, LosAblieferung)<br>
neuer Gestehungspreis = ----------------------------------------------------------------------------<br>
                                                                      neue gesamt Lagermenge

Dies wird von **Kieselstein ERP** auch bei jeder Korrektur des Preises der Buchung (und damit quasi dem neuen Lagerwert) aufgerollt.<br>
D.h. wenn Sie sich z.B. beim Wareneingang im Preis vertippt haben, auch wenn das Material schon längst verbraucht ist, so kann damit trotzdem der Wareneinstandspreis richtiggestellt werden und die sich daraus ergebenden nachfolgenden Gestehungspreise dieses Artikels werden komplett aufgerollt.<br>
Fachlich geht die Kette aber weiter. D.h. es ändern sich damit ja die Kosten des Unter-Unter-Loses usw.. Die ganze Kette rauf bis zum fertigen Produkt. Dies wird **NICHT automatisch** gemacht. D.h. da bitte auf Preise aktualisieren klicken.