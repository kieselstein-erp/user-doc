---
title: "Planung / Auswertung"
linkTitle: "Planung / Auswertung"
categories: ["Planung","Auswertung"]
tags: ["Planung","Auswertung"]
weight: 700
description: >
  Planungs- und Auswertungsjournale
---
Planungs- und Auswertungsjournale
=================================

In **Kieselstein ERP** stehen Ihnen im Modul Fertigung ![](Fertigung.gif) einige praktische Auswertungsjournale zur Verfügung.

Diese finden Sie im Menüpunkt Journal.
Als zusätzliches Modul ist die [grafische Auslastungsplanung](grafische%20Auslastungsplanung.htm) geplant.

## Zeitentwicklung

Mit der Zeitentwicklung wird im wesentlichen die Entwicklung der Durchlaufzeiten Ihrer Lose dargestellt.

Hier werden ebenfalls die Soll- und Istzeiten der Lose dargestellt.

Die Sollzeit wird zum Zeitpunkt der ersten Arbeitszeitbuchung eines Loses in das Diagramm übernommen, die Istzeit wird zum Zeitpunkt der letzten Arbeitszeitbuchung eines Loses in das Diagramm übernommen.

Es werden diejenigen Lose ausgewertet, bei denen der Erledigungszeitpunkt innerhalb des gewählten Zeitraums liegt. Unter Erledigungszeitpunkt verstehen wir entweder die Manuelle Erledigung eines Loses, oder dann, wenn die Abliefermenge die Soll-Losgröße erreicht hat.

### Auslastungsvorschau für die Auslastungsplanung
## <a name="Auslastungsvorschau"></a>
Bei der Auslastungsvorschau erhalten Sie eine Gegenüberstellung der Sollarbeitszeiten aller offenen Lose zum Beginn- und Endezeitpunkt zur verfügbaren Kapazität.
Die Auswertung geht immer immer von jetzt aus und liefert alle Lose deren Produktionsbeginn vor oder zum angegebenen Stichtag ist.
Zusätzlich kann zwischen einer Gesamtdarstellung und einer Gliederung nach Artikelgruppen gewählt werden.
Bei der Gliederung nach Artikelgruppen werden die den Tätigkeiten / Arbeitszeitartikeln anhand der zugewiesenen Artikelgruppen zusammengefasst. So können Sie Ihre Arbeitspläne detailliert erstellen und für die Auslastungsvorschau diese in überschaubare Gruppen gliedern.
Eine Auswertung mit zusätzlichen Informationen zu u.a. Losnummer und (Unter-)Arbeitsgängen finden Sie dazu ergänzend unter Journal/Auslastungsvorschau detailliert.

Die Auslastungsliste gliedert sich in zwei Bereich:
Oben: Die Aufstellung der Zeitbedarfe und der verfügbaren Stunden
Unten: Die grafisch aufbereiteten kumulierten Zeiten und verfügbaren Stunden.

![](Auslastung_Zahlen.gif)

![](Auslastung_Grafik.jpg)

Die Rote oder Beginn Linie zeigt die Sollzeiten der Lose pro Tag zu den jeweiligen Beginnterminen der Lose.

Die Blaue oder Ende Linie zeigt die Sollzeiten der Lose pro Tag zu den jeweiligen Endeterminen der Lose.

Die grüne Linie zeigt die Personalverfügbarkeit.

Sie können aus diesen drei Kurven sehr einfach ablesen ob mit den vorhandenen Mitarbeitern die eingebuchten Tätigkeiten erfüllt werden können oder nicht. Auch können Sie daraus ablesen wie weit Ihre (Fertigungs-)Aufträge reichen oder ob Sie z.B. Überstunden machen müssen oder weitere Mitarbeiter brauchen etc.. Von der Theorie her sollte die grüne Linie immer zwischen der roten und der blauen Linie liegen. Die Praxis zeigt uns in aller Regel, wie auch in obigem Beispiel, anderes.

Personalverfügbarkeit [siehe bitte](../personal/index.htm#Verfügbarkeit).

**Hinweis:**

Wenn Sie Lose mit sehr langen Durchlaufzeiten haben, so wird für die Darstellung ein entsprechend großer Zeitraum angezeigt. Um eine realistische Planung zu erhalten, empfiehlt sich auch bei den Durchlaufzeiten der Lose realistische Werte zu verwenden.
Da es in der Praxis immer wieder vor kommt, dass Fertigungsaufträge zwar eingeplant sind, aber aus Gründen der Materialsituation oder wenn sich ein Kunde einfach nicht zum bereits besprochenen Abruf entscheiden kann, so setzen Sie dieses Los bitte einfach auf Produktion gestoppt. Damit bleibt es in Ihrer Planung erhalten und wirkt trotzdem nicht in Planung der Auslastung / Tätigkeiten etc.

**Hinweis**: In der Auslastungsplanung werden die Sollstunden aus den offenen Los-Mengen (Losgröße - Ablieferung) errechnet. Bis zur Version 0.0.x deines **Kieselstein ERP** ist diese Betrachtung leider falsch an den Report übergeben worden, wodurch nur Lose ohne Ablieferungen und vollständig abgelieferte Lose richtig betrachtet wurden. Siehe unten.

## Detaillierte Darstellung der Auslastung

Es gibt immer wieder den Wunsch nach der detaillierten Darstellung der Auslastung.
Unter detaillierte wird verstanden, dass jedes Los mit der Dauer der jeweiligen Arbeitsgängen aufgeführt wird und dass für die verschiedenen Tätigkeiten auch die entsprechenden Verfügbarkeiten aus dem Personal angegeben werden.
Nutzen Sie dafür bitte die Auslastungsvorschau detailliert.
Hier erhalten Sie eine Darstellung ähnlich der oben beschriebenen. Es wird jedoch jeder Arbeitsgang jedes Loses, auch hier gruppiert nach den Artikelgruppen, angeführt.
Zusätzlich sehen Sie Ihre Verfügbarkeiten und Stundenbedarfe sowohl in einer wochenweisen Darstellung als auch grafisch aufbereitet.
![](Auslastung_detailliert_Losliste.gif)

![](Auslastung_detailliert_Grafik.jpg)

Zusätzlich ist die Liste so aufgebaut, dass in der Regel die nächste Woche als Anzeigebereich vorgeschlagen wird.
So erhalten Sie einen raschen Überblick der nächsten Woche.

**Hinweis:**<br>
Ab der Version 0.2.1 wurde die Berechnung beider Auslastungsvorschau um folgendes Feature korrigiert / ergänzt:
- Statt der, bisher jetzt falsch berechneten Sollzeit werden nun 
  - Sollzeit (gesamt geplant), 
  - Sollzeitoffen (noch offene Sollzeit, über die noch offene Abliefermenge)
  - und Sollzeitist (berechnet über Soll-Ist, minimal 0, also die theoretisch fehlenden Stunden, wenn alles zu 100% stimmt) an den Report geliefert.
  Die standard Reports wurden dementsprechend mit Feldern erweitert




## Zeitentwicklung

In der Zeitentwicklung sehen Sie die tatsächliche Erfüllung Ihrer Fertigungsaufträge.

Für die Betrachtung werden die Lose herangezogen, die im Betrachtungszeitraum (von-bis) manuell vollständig erledigt bzw. vollständig abgeliefert wurden. 

Für die Linie Sollzeit, wird die gesamte Sollzeit eines Loses zum Zeitpunkt der ersten Arbeitszeitbuchung herangezogen.

Für die Linie Istzeit, wird die gesamte tatsächliche Istzeit zum Zeitpunkt der letzten Arbeitszeitbuchung verwendet.

Aus dem Abstand der beiden Linien können Sie erkennen, wie realistisch Ihre tatsächlichen Durchlaufzeiten sind.

![](Zeitentwicklung_Grafik.gif)

## Auslieferliste

In dieser Auswertung erhalten Sie eine Übersicht über alle offenen Fertigungsaufträge und Kunden-Aufträge, die bis zum Stichtag auszuliefern sind, in einer sehr detaillierten Form.
Basis sind die offenen Lose und Kundenaufträge die (noch) keine Verknüpfung zu Losen haben.
Die Liste ist nach Auslieferdatum auf Basis des Kundenauftrags-Liefertermins ev. je Position sortiert.
Je Los wird neben den üblichen Kopfdaten auch der Status des Fertigungsfortschrittes jedes einzelnen Arbeitsgangs dargestellt. Eine wichtige Info ist hier, wenn ein einzelner Arbeitsgang tatsächlich fertig ist. Die kann einerseits direkt im Los unter Zeitdaten, durch anhaken von Fertig gesetzt werden, oder es wird ein Barcode $FERTIG auf der BDE Station oder dem Zeiterfassungsstift gebucht.
Damit erhalten Sie eine komplette Übersicht über den aktuellen Fertigungsstatus aller offenen Lose.
Zusätzlich wurden deshalb in diese Liste auch noch Informationen wie Fehlmengen, Rahmenaufträge aufgenommen.

Um allgemeine Lose wie Werkstattaufträge aus der Auslieferungsliste herausfiltern zu können, werden auch die Losklassen mit an die Liste übergeben. D.h. Sie definieren, welche Losklassen nicht mit aufscheinen sollten. Ihre **Kieselstein ERP** Betreuer wird die Liste entsprechend anpassen, sodass nur mehr die für Sie relevante Information enthalten ist.![](Auslieferliste.gif)

**Wie wirkt der Stichtag in der Auslieferliste?**
1. Es werden alle Angelegten/InProduktion/TeilerledigtenLose gesucht, bei denen der Produktionsbeginn <= Stichtag ist.
2. Danach wird das Ablieferdatum berechnet:
Ablieferdatum = Auftragsspositionsliefertermin (wenn vorhanden) ansonsten Auftragsliefertermin - Kundenlieferdauer.<br>
Wenn kein Auftrags/Positionsbezug, dann ist der Abliefertermin das Produktionsende
3. Zusätzlich werden nun noch alle Offenen/Teilerledigten Auftragspositionen hinzugefügt, zu denen es noch keine Lose gibt und deren Ablieferdatum (=Auftragsspositionsliefertermin-Kundenlieferdauer) <= Stichtag ist

**Nur Lose berücksichtigen**

Mit dem setzen des Häkchens "nur Lose nach Ende-Termin sortiert" gibt es die Möglichkeit, nur mehr die Lose (und nicht mehr zusätzlich die offenen Auftragspositionen) zu berücksichtigen. Diese werden nach ihrem Ende-Termin sortiert. Damit wird die Auslieferliste nur mehr für die Fertigungssteuerung verwendet. Für die echte Auslieferung kann dann das Auftragsjournal "Auszuliefernde Positionen" genutzt werden.

## Auslieferliste als Liste der zu spät kommenden Artikel

Unsere Anwender wünschen sich immer wieder, jetzt schon zu erkennen, welche Artikel / Bauteile in der Beschaffung in einigen Wochen bzw. Monaten Schwierigkeiten machen werden. D.h. das sind Artikel bei denen heute schon bekannt ist, dass diese nicht rechtzeitig kommen werden.
Gerade in Zusammenhang mit den ansteigenden Lieferzeiten, zeigt diese Frage die Wichtigkeit einer ordentlichen Materialwirtschaft oder in anderen Worten, was hilft Ihnen wenn das gesamte Material im Hause ist, nur die letzte Beilagscheibe wurde vergessen und auf diese sind 6Wochen Lieferzeit. Das kostet Platz, Kundenvertrauen und belastet auch die Liquidität.

Um eine fundierte Aussage zu bekommen müssen folgende Voraussetzungen erfüllt sein:
1. es sind alle Aufträge angelegt und die Liefertermine stimmen
2. es sind alle Lose angelegt und die Beginn- und Endetermine stimmen
3. es sind alle Lieferterminbestätigungen Ihrer Lieferanten zum aktuell bekannten Termin eingepflegt. Hier liegt sicherlich auch die Kunst darinnen, zu wissen wie gut der Lieferant seine Liefertermine einhalten wird. Siehe dazu auch Wiederbeschaffungsmoral.

**Bitte bedenken Sie:** Auch wenn die Terminpflege der Aufträge und Lose einen entsprechenden Zeitaufwand erfordert. Sie gewinnen damit Sicherheit, Planbarkeit und nicht zuletzt auch Liquidität und vor allem Kundenvertrauen.

Nun wechseln Sie in das Modul Lose, Journal, Auslieferliste.
Als Stichtag wählen Sie einen Termin in der Zukunft, der Ihrem tatsächlichen Betrachtungshorizont aus Sicht der Liefertermine an die Kunden entspricht.
Hier bekommen Sie eine Liste aller offenen Kundenaufträge und aller offenen Lose und bei den Losen auch den jeweiligen Fertigungsfortschritt und die Materialsituation mit Verfügbarkeiten der Artikel.

Diese Auswertung steht auch als Automatik Job mit der Erzeugung entsprechender PDF Dateien zur Verfügung um diese täglich, in der Nacht, für Ihre tägliche Lage / Projektbesprechung zeitgerecht zur Verfügung zu haben.

### Auslieferliste umfangreich
![](Auslieferliste_umfangreich.jpg)
In dieser Auswertung sehen Sie alle offenen Auftragspositionen und die dazu gehörenden Lose. Bei den Losen wiederum den Fortschritt der Tätigkeit und den aktuellen Status der jeweiligen Materialverfügbarkeit. Bitte beachten Sie beim Material insbesondere die roten Zeilen. Rot bedeutet, Material zum Los-Beginntermin ist zu wenig. Das kann bedeuten, dass Sie das zwar bestellt haben, aber der Lieferant einen Termin zu spät bestätigt hat.<br>
Bitte beachten Sie weiters auch die Termindarstellungen:
- Liefertermin = Liefertermin der Auftragsposition, d.h. zu diesem Datum muss die Waren / das fertige Produkt beim Kunden sein.
- Abliefertermin = Liefertermin abzgl. Lieferdauer an den Kunden (Kunde, Konditionen)
- Beginn = Beginntermin des Loses
- Ende = Endetermin des Loses und natürlich darf das Ablieferdatum nicht nach dem Endetermin des Loses sein.

#### Die Termine zwischen Auslieferliste und Aufträgen stimmen nicht überein
Bitte beachten Sie die Unterschiede der verschiedenen Terminfelder.
Für die Auslieferliste wird nur der Positionstermin der jeweiligen Auftragsposition herangezogen.
Beachten Sie, dass in den Auftragskopfdaten auch der Finaltermin, der Wunschtermin und der Bestelltermin vorhanden sind. Diese dienen Ihrer Information bzw. für die Liquiditätsplanung. Für die Auslieferung der Ware gilt ausschließlich der Liefertermin bzw. der Rahmenendetermin (bei Rahmenaufträgen).

## Offene Arbeitsgänge

Damit steht eine praktische Auswertung der offenen Arbeitsgänge aller offenen Lose zur Verfügung.

Sie sehen hier, eventuell gegliedert nach Artikelgruppen, bei welchen Losen noch welche Arbeitsgänge offen sind. Wenn Sie für die Tätigkeitsgruppen entsprechend gut gruppierte Artikelgruppen angelegt haben, so können Sie damit die offenen Arbeitsgänge Ihrer Tätigkeitsgruppen (diese entsprechen oft auch den Mitarbeitergruppen) entsprechend steuern.

Bitte beachten Sie, dass im jeweiligen Beginn-Termin auch der AG-Beginn (Arbeitsgang Beginn) des jeweiligen Arbeitsgangs berücksichtigt ist.

![](offene_Arbeitsgaenge.gif)

Geben Sie den gewünschten Stichtag und die zugehörige Terminart auf den sich der Stichtag beziehen sollte an.

Wählen Sie gegebenenfalls noch entsprechende Filter, z.B. Kunde, Artikelgruppe der Tätigkeiten usw.

Sie erhalten nun eine Aufstellung aller noch offenen Arbeitsgänge, sortiert nach der gewählten Terminart (Ende, Beginn, Liefertermin)

In dieser Auswertung ist am Ende auch eine Zusammenfassung der Maschinenbelegung nach Kalenderwochen mit einer groben Auslastungsanzeige enthalten. Diese könnte z.B. wie folgt aussehen.

![](offene_Arbeitsgaenge.jpg)