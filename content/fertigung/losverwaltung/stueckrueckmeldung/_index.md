---
title: "Stückrückmeldung"
linkTitle: "Stückrückmeldung"
categories: ["Stückrückmeldung"]
tags: ["Stückrückmeldung"]
weight: 100
description: >
  Den Fortschritt einzelner Arbeitsgänge Rückmelden
---
Stückrückmeldung
================

In **Kieselstein ERP** steht Ihnen auch die zusätzlich die Funktionalität der Stückrückmeldung zur Verfügung. Damit ist es möglich den Fortschritt jedes Arbeitsgangs zu verfolgen.

Mit der Stückrückmeldung werden für jeden Arbeitsgang die produzierten Gut-/und Schlecht-Stück erfasst.
Üblicherweise wird die Stückrückmeldung direkt am Zeiterfassungsterminal durch den Mitarbeiter, bei der Beendigung bzw. dem Wechsel seiner Tätigkeit durchgeführt. Damit ist in aller Regel auch die Zuordnung vom Arbeitsgang zur Person und zur zurückgemeldeten Stückzahl gegeben. Siehe dazu bitte auch den Parameter STUECKRUECKMELDUNG_BENOETIGT_ZEITERFASSUNG, bzw. unten.

Wozu
wer braucht das
Soll ich das für alle Arbeitsgänge machen ?

Um manuell die erfassten Stückzahlen einzugeben bzw. diese zu korrigieren, wählen Sie bitte das Fertigungsmodul (die Losverwaltung) und wählen das gewünschte Los aus. Nun wählen Sie bitte den oberen Modulreiter Zeitdaten und wählen den gewünschten Arbeitsgang aus.
Wechseln Sie nun in den oberen Modulreiter Gut/Schlecht und geben die gefertigten Gut-/Schlecht Stück an.
Dafür sind in der Regel
Person, Datum, Uhrzeit, ev. Maschine und die Gut-/Schlecht-/InArbeit zu erfassen

Da in einigen Anwendungen die Forderung nach der automatischen Rückrechnung der Istzeit von parallel ablaufenden / bedienten Rüstzeiten bzw. Einzelaufträgen besteht, muss auch erfasst werden, mit welcher Zeitbuchung und von wem diese Gutschlechtbuchung erfolgt ist.
Dazu geben Sie die Zeitbuchung an mit der diese Meldung erfolgte.

Die InArbeit Menge wird für die Berechnung des Soll-Ist Vergleiches der Umspannzeiten benötigt, da gerade im Schichtbetrieb es immer wieder vorkommt, dass der erste Mitarbeiter ein Teil einlegt und dann, aufgrund des Schichtwechsels, der zweite Mitarbeiter das Teil aus der Maschine entnimmt.

In der Übersicht der Los-Soll-Zeiten sehen Sie die Gesamtsummen der erfassten Gut und Schlecht Mengen.
![](Stueck1.gif)

Die übliche Buchung der Stückrückmeldung läuft wie folgt ab:

1.  Erfassung des Datums
    ![](Stueckrueck1.gif)

2.  Erfassung der Person und deren Buchungszeitpunkt
    ![](Stueckrueck2.jpg)

3.  Erfassung der Gut-, Schlecht-, in-Arbeit
    sowie eventuell der Fehler und eines Kommentares
    ![](Stueckrueck3.gif)

Ist der Parameter STUECKRUECKMELDUNG_BENOETIGT_ZEITERFASSUNG deaktivier (auf 0) so wird beim Klick auf Neu das aktuelle Datum und die aktuelle Uhrzeit vorgeschlagen und es kann direkt mit der Gut-/Schlechterfassung fortgesetzt werden.
![](Stueckrueck4.gif)