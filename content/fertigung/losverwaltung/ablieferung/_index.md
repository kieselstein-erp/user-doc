---
title: "Ablieferung"
linkTitle: "Ablieferung"
categories: ["Ablieferung"]
tags: ["Ablieferung"]
weight: 100
description: >
  Baugruppen / Lose abliefern, als fertig produziert rückmelden
---
Im **Kieselstein ERP** steht im Reiter Ablieferung die Fertigmeldung deiner gefertigten Baugruppe zur Verfügung.

Je nachdem ob die Stückliste Chargen- oder Seriennummern geführt ist, müssen diese Daten entsprechend angegeben werden.

Wenn in deiner Installation auch die Geräteseriennummern Verwaltung aktiviert ist, muss hier auch die Zuordnung zwischen den verbauten Unter-Baugruppen und dem zurückgemeldeten Gerät gemacht werden. Selbstverständlich können auch Seriennummern geführte Artikel dem Gerät zugeordnet werden. Weitere Details siehe Geräteseriennummernverwaltung.