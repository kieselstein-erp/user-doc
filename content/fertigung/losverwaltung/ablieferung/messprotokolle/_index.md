---
title: "Messprotokolle"
linkTitle: "Messprotokolle"
categories: ["Messprotokolle"]
tags: ["Prüfprotokolle", "Messwerte erfassen"]
weight: 100
date: 2023-01-31
description: >
  Wie bringt man Mess- bzw. Prüfprotokolle die in der Fertigung anfallen auf den Lieferschein?
---
Zwei Möglichkeiten
- mit dem Fertigungsschein zugleich ein Formular(PDF) ausdrucken, welches die Prüfwerte enthält
  Bei der Losablieferbuchung die Messwerte erfassen und manuell auf dem Papier notieren
  Den Beleg einscannen und bei der Losablieferung dazuhängen
  Dafür den Stücklistenartikel im Sonstiges auf Dokumentenpflichtig setzen
  Beim Druck des Lieferscheines, können die Dokumente als eigenes Dokument per EMail-Anhang mitgesandt werden
  zusätzlich gibt es auch eine Möglichkeit dieses "Bild" mit auszudrucken
- Chargeneigenschaften definieren
  diese Chargeneigenschaften bei jeder Losablieferung erfassen
  und dann entsprechend wieder auf den gewünschten Papieren erfassen
  Gerne auch erweitert um die Sollvorgaben für den Artikel aus den Artikeleigenschaften anhand Artikelgruppe