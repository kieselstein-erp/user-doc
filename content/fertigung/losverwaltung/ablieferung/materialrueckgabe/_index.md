---
categories: ["Fertigung"]
tags: ["Materialrückgabe"]
title: "Materialrückgabe"
linkTitle: "Materialrückgabe"
weight: 200
date: 2023-01-31
description: >
 Materialrückgabe abgeschlossen
---
Gerade in der Elektronik Fertigung gibt es immer wieder die Herausforderung, dass auch definiert werden muss, dass man noch Material aus dem Fertigungsprozess zurück erwartet.<br>
Hintergrund dafür ist, dass, in Verbindung mit der Übermengenverwaltung, immer ganze Rollen z.B. á 10.000Stk auf den Automaten aufgerüstet werden, obwohl nur 100Stk benötigt werden und damit am Ende der Produktion die restlichen 9.900Stk wieder zurückerwartet werden.<br>
Gerade bei guten Rüstplänen, wird diese Rolle nicht nur für ein Los, sondern für viele Lose benötigt.

Aus diese Grunde dürfen Loses nicht mit der vollständigen Ablieferung automatisch erledigt werden, sondern benötigen einen eigenen Status, mit dem die Materialwirtschaftsveranwtwortlichen definieren können, dass dieses Los auch vollständig abgerüstet ist, also das Material wieder vom Lager vereinnahmt wurde.

Ergänzend kommt dazu, dass in Abhängigkeit welches Produkt, welche Baugruppe nun mit der Stückliste gefertigt wird, man diese Verwaltung benötigt oder eben nicht.

Um diese Funktion zu aktivieren muss der Parameter MATERIAL_RUECKGABE_ERWARTET auf 1 gestellt werden. Damit erscheint in der Stückliste eine zusätzliche Checkbox ![](Materialrueckgabe_erwartet.png) Materialrückgabe erwartet.<br>
Mit dem Parameter MATERIAL_RUECKGABE_ERWARTET_STKL_DEFAULT_WERT kann gesteuert werden, ob bei einer Neuanlage einer Stückliste diese Einstellung automatisch gesetzt ist.

Ist diese Funktion aktiviert und ist in der Stückliste Materialrückgabe erwartet angehakt, so findet man im Los, im Reiter Ablieferung ![](Materialrueckgabe.png) Materialrückgabe. 

Damit kann signalisiert werden, dass für dieses Los bereits die gesamte Materialrückgabe durchgeführt wurde.

Ist die Materialrückgabe noch nicht durchgeführt worden und erledigt man trotzdem das Los mit seiner vollen Menge, so erscheint die Frage,<br>
![](Erledigen_mit_ohne_Materialrueckgabe.png)<br>
Trotzdem erledigen.<br>
Das bedeutet,
- bei Nein wird die Abliefermenge an das Lager gebucht, das Los aber nicht erledigt
- bei Ja wird ebenfalls die Abliefermenge ans Lager gebucht und das Los erledigt und zugleich die Materialrückgabe gesetzt.

### Materialrückgabe setzen
Durch Klick auf ... wird die Materialrückgabe gesetzt bzw. wenn sie bereits gesetzt war, wird dieser Status wieder gelöscht.

Zusätzlich wird eine gesetzte und damit vollständig abgeschlossene Materialrückgabe im Reiter Ablieferung angezeigt ![](Materialrueckgabe_gesetzt.png). Neben dem Status wird auch angezeigt, wer wann die Materialrückgabe eingetragen hat.

### Verhalten bei freier Materialliste
Bei einer freien Materialliste, wird die Materialrückgabe nicht unterstützt. Das bedeutet gegebenenfalls muss eine leere Stückliste verwendet werden um diese Funktion nutzen zu können.

### Automatisches Erledigen von Losen
Da es beim Automatischen Erledigen der Lose darum geht, z.B. nachträglich geöffnete Lose wieder zu erledigen / schließen wird vom Automatikjob der Status der Materialrückgabe nicht berücksichtigt.

Gegebenenfalls stelle den Zeitraum *vergangene Arbeitstage* ausreichend lang ein, sodass deine MitarbeiterInnen ausreichend Zeit haben die Lose wirklich zu erledigen.