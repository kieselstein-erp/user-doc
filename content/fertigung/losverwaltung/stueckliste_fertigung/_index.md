---
title: "Stückliste > Fertigung"
linkTitle: "Stückliste > Fertigung"
categories: ["Stückliste","Fertigung"]
tags: ["Stückliste","Fertigung"]
weight: 800
description: >
  Zusammenhänge zwischen Stückliste und Fertigung
---
Um den Grundgedanken hinter den zusammengehörenden Modulen Stückliste und Fertigung zu vermitteln, haben wir diese kurze Beschreibung erstellt. Für die Erklärung der Details sei auf die jeweiligen Beschreibungen in den Modulen verwiesen.

Es geht hier um die Themen:
-   Stücklisten
-   Arbeitsplan <-> Fertigung
-   Arbeitsplan in der Fertigung
-   Fertigungsbegleitschein
-   Istzeiterfassung
-   Nachkalkulation

Eine Stückliste besteht grundsätzlich aus den beiden Teilen Materialliste und Arbeitsplan (=Zeitvorgaben).

Sehr oft kommt der Inhalt einer Stückliste aus einem CAD-Programm, da sich dort die benötigten Teile aus der Konstruktion, aus der Erfindung des Teiles ergibt.

Der Arbeitsplan beschreibt welche "Handgriffe" zu machen sind, um die Teile so zu bearbeiten (sein es eine zerspanende Bearbeitung oder ein Zusammenfügen) dass daraus ein neues Produkt, eine neue Baugruppe wird.

Die Stückliste enthält die Vorgabedaten / Solldaten für den Produktionsauftrag.

Durch das Anlegen des Loses / des Fertigungsauftrages werden die Daten der Stückliste verwendet und mit der Losgröße multipliziert. Also die Mengen mit der Losgröße multipliziert, die Stückzeiten mit der Losgröße multipliziert und so eine Kopie der Stückliste inkl. aller Vorgabedaten genau passend zum Fertigungsauftrag als Solldaten dieses Auftrags übernommen. Dabei werden zugleich die benötigten Materialien reserviert und die Zeiten eingeplant.

Wird nun mit der Fertigung / Produktion des Fertigungsauftrags begonnen, so wird das Los in den Status in Produktion gesetzt und damit das Material vom Lager in den Fertigungsauftrag gebucht und es sind Zeitbuchungen auf den Fertigungsauftrag erlaubt. Nun wird vom Mitarbeiter gestempelt, dass er mit dem Arbeitsschritt 1 beginnt und er führt diesen Arbeitsschritt für die geforderte Stückzahl des Loses durch. Ist der erste Arbeitsgang fertig, so wird vom Mitarbeiter der zweite Arbeitsgang angestempelt. Und so fort, bis alle Arbeitsschritte auf allen Teilen durchgeführt wurden.

Sind einzelne Teile fertig produziert werden diese durch das Los an das Lager abgeliefert.

Aufgrund des Material- und des Arbeitszeit Einsatzes ergeben sich nun die entsprechenden Kosten.

Somit ist der Kreis des Werteflusses geschlossen, die Materialien sind veredelt als Ganzes wieder im Lager gelandet.

Zusätzliche Variante Stückrückmeldung und Maschinen mit [Mehrmaschinenbedienung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ):

Diese zusätzlichen Möglichkeiten kommen meistens kombiniert zum Einsatz.

In vielen Firmen ist es so, dass die Tätigkeit die zu planende Größe darstellt, d.h. dass die Engpass Kalkulation auf einige wenige Tätigkeiten abzielt.

In vielen Metallverarbeitenden Betrieben gehören selbstverständlich die Maschinen mit dazu, wobei es hier die drei Varianten gibt:
-   Reine Mitarbeiterzeit
-   Reine Maschinenzeit
-   Mitarbeiterzeit und Maschinenzeit kombiniert.

Bei der reinen Maschinenzeit spricht man in aller Regel von der Maschinenlaufzeit. Ergänzend zu den obigen Buchungen wird hier anstelle der Tätigkeit exakt auf den einzelnen Arbeitsgang gebucht und so eindeutig definiert, welcher Arbeitsgang gerade durchgeführt wird.

Ist bei der Buchung auf den Arbeitsgang eine Maschine mit angegeben, so wird automatisch der Start auf die Maschine mit gebucht. Wechselt der Mitarbeiter die Maschine durch Buchung eines weiteren Arbeitsbeginns auf einen anderen Arbeitsgang, so läuft trotzdem die Maschine weiter. Erst durch die Maschine-Stop Buchung wird die Maschinenlaufzeit beendet.

In **Kieselstein ERP** sind alle möglichen Kombinationen dieser Buchungen erlaubt. Um die Erklärung möglichst einfach zu halten, wurde auf die Ausführung der Details verzichtet. Bitte informieren Sie sich in den entsprechenden Kapiteln darüber.