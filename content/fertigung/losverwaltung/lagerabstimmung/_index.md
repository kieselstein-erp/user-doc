---
title: "Lagerabstimmung"
linkTitle: "Lagerabstimmung"
categories: ["Lagerabstimmung"]
tags: ["Lagerabstimmung"]
weight: 800
description: >
  Lagerabstimmung vor allem im Rahmen der Inventur
---
Lagerabstimmung
===============

Gerade zu Zeiten einer vollständigen Inventur tritt immer wieder die Frage nach einer vollständigen und durchgängigen Lagerabstimmung auf. 

Hier ist enorm wichtig, dass man sich über den Status des Materials im Klaren ist.

Also ist das Material von der Buchung her am Lager oder ist es als Teil der Halbfertigfabrikatsinventur im Los.

Bewährt hat sich die Vorgehensweise, dass alle Lose abgeschlossen werden und dann erst die Zählung und die Lager/Inventurkorrekturbuchungen vorgenommen werden.

Die Vorgehensweise ist also folgendermaßen:
1. ALLE Lose die das jeweilige Lager betreffen ermitteln und gegebenenfalls die ursprünglichen Losgrößen festhalten.
2. die richtige Abliefermenge sicherstellen.
3. Erledigen
    - a.) die Lose (manuell) vollständig erledigen oder
    - b.) die Losgröße auf die Abliefermenge setzen. Hier kann das Los auf Teilerledigt gelassen werden.
4. Nun kann mit der Halbfertigfabrikatsinventur überprüft werden, ob tatsächliche alle Lose des Lagers behandelt wurden. D.h. die Halbfertigfabrikatsinventur muss 0 ergeben. D.h. es dürfen keine Mengen auf den Losen gebucht sein.
5. die Lagerkorrekturen einbuchen
    - nun die Lose entweder neu anlegen (wenn Version 3a). **WICHTIG:** Es dürfen die manuell auf vollständig erledigt gesetzten Lose nicht enterledigt werden. Siehe dazu unten. Oder
    - nun die Losgröße wieder auf den ursprünglichen Wert setzen (wenn Version 3b)

**Warum Lose nicht wieder enterledigen?**

Werden Lose manuell erledigt, so geschieht keine Lagerbuchung, weder im Bereich der Ablieferung noch im Bereich der Materialentnahme (Ausgabe) in die Lose.

Das gleiche Verhalten ist auch bei der Rücknahme der Erledigung geben. D.h. auch hier werden keine Warenbewegungen gebucht. Das bedeutet, wenn ein Los enterledigt wird, so wird einfach das am Los gebuchte Material wieder weiter verwendet. Bei dieser Abstimmung wollen wir aber erreichen, dass das gezählte Material wieder in die Fertigung wandert.

**Zählen des Materials beim Lohnfertiger**

Natürlich muss zum Inventurzeitpunkt auch das Material deines Lohnfertigers berücksichtigt werden. D.h. es muss ein Vergleich mit dem beim Lohnfertiger verfügbaren Material mit dem auf seinem **Kieselstein ERP** Lager gebuchten Mengen erfolgen.<br>
Hier ist unbedingt zu beachte, dass die externe Fertigung auch ein ERP-System, idealerweise natürlich **Kieselstein ERP** einsetzt. Hat dein externer Fertiger nun Lose aufgerüstet, so wird sein Lagerstand nicht mit deinem Lagerstand übereinstimmen. Es muss hier unbedingt das die gehörende in der Fertigung befindliche Material mitgerechnet werden. Ev. hilft ihm hier, wenn er **Kieselstein ERP** einsetzt, eine Anpassung seiner Inventurliste.