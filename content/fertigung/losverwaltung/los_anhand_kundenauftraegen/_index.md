---
title: "Lose anhand Kundenaufträgen"
linkTitle: "anhand Kundenaufträge"
categories: ["Fertigung"]
tags: ["Anlegen anhand Kundenaufträge"]
weight: 100
description: >
  Lose anhand eines Kundenauftrages anlegen
---
Lose anhand Kundenaufträgen anlegen
===================================

Sehr oft sollen Fertigungsaufträge nicht in der Gesamtbetrachtung der Internen Bestellung sondern anhand von Kundenaufträgen angelegt werden.

In **Kieselstein ERP** stehen Ihnen dazu zwei Funktionen zur Verfügung.
1. aus der Auswahlliste der Lose
2. aus dem Anlegen neuer Lose

## 1. in der Auswahlliste der Lose finden Sie ![](Lose_anhand_Auftrag1.gif) Neu anhand Auftrag

Wählen Sie hier den gewünschten Auftrag aus.
Als nächstes erhalten Sie eine Liste der Lose die aufgrund des Inhaltes des Auftrags angelegt werden sollten.
![](Lose_anhand_Auftrag3.jpg)

es wird nun die Liste der anzulegenden Lose wie folgt dargestellt:
![](lose_anh_ab_liste.JPG)
Die angeführten Stücklisten ergeben sich aus den im Auftrag enthaltenen Positionen und den dazugehörenden Unterstücklisten.
Es werden die Positionsnummer des Auftrags (PosNr) die Stücklisten-Ident, die Bezeichnung angezeigt.
Ebenso wird 
- der aktuelle Lagerstand aller Läger des Artikels die bei der interne Bestellung berücksichtigt werden
- die offene Fertigungsmenge
- die derzeit reservierten Mengen 
- die Fehlmenge auf dem jeweiligen Artikel 
- und die verfügbaren Mengen angezeigt.

Die Losgröße der Kopf- (= übergeordneten) Stücklisten entspricht der Auftragsmenge.
Die Losgröße der untergeordneten Stücklisten wird durch Multiplikation aus der Menge der Kopfstückliste errechnet.
Die Unterpositionen werden in dieser Ansicht mit U in der PosNr gekennzeichnet.
Die Termine (Beginn, Ende) werden wie folgt berechnet:
- Der Beginntermin ergibt sich aus dem Liefertermin der Auftragsposition abzüglich der Kunden-Lieferdauer (Siehe Kunde, Konditionen) abzüglich Durchlaufzeit.
- Der Ende Termin des Kopfloses ergibt sich aus aus Endetermin plus der in der Stückliste hinterlegten Durchlaufzeit.
- Der Endetermin des Unterloses ergibt sich aus dem übergeordneten Los abzüglich der Fertigungsvorlaufzeit (DEFAULT_VORLAUFZEIT_INTERNEBESTELLUNG) - und so weiter. 
- Wird aufgrund der Rückwärts Terminierung ein Beginntermin vor dem heutigen Tage errechnet, so werden alle Termine um die höchste Differenz (zu heute) nach hinten verschoben, also auf Vorwärtsterminierung umgeschaltet. Dass auf Vorwärtsterminierung umgeschaltet wurde, wird in Rot angezeigt.
- In der Spalte in AB-Los erkennen Sie, dass für diesen Auftrag schon Lose angelegt wurden.

**Wichtig:** In den reservierten Mengen der Kopfstücklisten werden die aufgrund des gewählten Auftrags reservierten Mengen nicht mitgerechnet, da diese Darstellung sonst ja doppelt mitgerechnet werden würde.

Bei der Berechnung des Vorschlagswertes Losgröße werden Lagerstände usw. NICHT berücksichtigt. Die neue Losgröße ist die berechnete Losgröße abzüglich der bereits gefertigten Menge.
Im Tool-Tip der Artikelnummer (bewegen Sie dazu den Mauszeiger auf die Artikelnummer und warten einen Moment) werden die bereits vorhandenen Lose angezeigt (ausser das Los befindet sich im Status storniert).

Ändern Sie die Produktionswerte wie gewünscht und benötigt ab.

Durch Klick auf ![](Lose_anhand_Auftrag4.jpg) speichern werden die Lose entsprechend angelegt.
- Lose die einen direkten Bezug zur Auftragsposition haben, werden mit einem Bezug dazu angelegt. 
- Lose die sich aufgrund der Unterstücklisten ergeben werden von hinten her ohne Auftragsbezug angelegt. Bei direkter Verknüpfung zwischen Auftrags- und Los-Nummer werden diese Unterlose mit der Nummer 999 beginnend abwärts gezählt. [Siehe dazu auch](Interne_Bestellung.htm#Losnummer Auftragsbezogen).
- Positionen bei denen bei der Losgröße 0,00 eingetragen wurde werden nicht angelegt.

**Hinweis:**<br>
Für Handeingaben aus den Auftragspositionen wird für die Berechnung des Beginntermines der Wert des Parameters INTERNEBESTELLUNG_DEFAULTDURCHLAUFZEIT verwendet.
Es können auch mehrere Lose zu einer Auftragsposition angelegt werden, auch wenn auftragsbezogene Losnummern verwendet werden (ab dem zweiten Los wird eine Nummer <999 verwendet).

**Hinweis:**<br>
Änderungen an den anzulegenden Mengen bewirken **<u>keine</u>** Änderungen der Mengen der darunter liegenden Stücklisten. Gegebenenfalls müssen diese Änderungen selbst errechnet und eingetragen werden.<br>
Mein Tipp:<br>
Verwende die Interne Bestellung und wählen die Berechnung nur für einen Kundenauftrag.

#### Wie werden Handeingaben des Kundenauftrags behandelt?
Handeingaben des Kundenauftrags werden als Materialliste angelegt. Die Bezeichnung der Handeingabe wird in die Referenz des Loskopfes mit übernommen.

#### Welche Lager werden für die Berechnung der Lagerstände berücksichtig?
Es werden die Lagerstände der Läger berücksichtigt, die auch in der internen Bestellung berücksichtigt werden. [Siehe]
( {{<relref "/docs/stammdaten/artikel/#lagerdefinition">}} )

#### Woher kann erkannt werden, in welcher Reihenfolge der Werker die Lose nun bearbeiten sollte
Oft tritt die Frage auf, in welcher Reihenfolge nun der Werker die Lose abarbeiten sollte?
Aufgrund der [Terminberechnung](index.htm#Berechnung Termine) reicht es, wenn Sie die Lose in der Auswahlliste nach deren Beginntermin sortieren und schon sehen Sie welche Lose wann gefertigt werden sollten. D.h. es werden bei der Terminberechnung der Lose die entsprechenden Hierarchieebenen berücksichtig und somit sind die zuerst zu produzierenden Lose diejenigen, welche den frühesten Beginntermin haben.
Von der Idee her, sollten auch die Fertigungspapiere in dieser Reihenfolge in die Produktion gegeben werden.

**Tipp:**
Es hat sich bewährt die Anzahl der (offenen) Los-Fertigungsbegleitscheine im Umlauf so gering wie möglich zu halten. Die Werker buchen gerne auf "irgendwelche" Zettel.

## 2. aus dem Anlegen neuer Lose

Im Anlegen neuer Lose finden Sie neben der Anzeige / Auswahl des Auftrags den Knopf ![](Lose_anhand_Auftrag2.gif) Lose für Auftragspositionen anlegen. Damit werden für alle Auftragspositionen eines Auftrags entsprechende Lose angelegt. Die Beginn- und Endetermine werden, entgegen Lose anhand Kundenauftrag aus den Vorgaben genommen. Eine Auflösung der Auftragspositionen in die Unterstücklisten findet nicht statt

<a name="Anlage nach Reihenfolge"></a>

#### Können Lose eines Auftrags Stücklisten übergreifend in einer bestimmten Reihenfolge angelegt werden?
In der Funktion ![](Lose_anhand_Auftrag1.gif) neue Lose anhand Auftrag kann in Kombination mit dem Parameter AUTOMATISCHE_ERMITTLUNG_AG_BEGINN und der Aktivierung des Parameters REIHENFOLGENPLANUNG eine Stücklisten übergreifende Reihenfolgenplanung innerhalb des Auftrags automatisch vorgenommen werden. Bitte beachten Sie, dass dafür auch der direkte Los-Auftragsbezug LOSNUMMER_AUFTRAGSBEZOGEN = 1 aktiviert sein muss. Ist die Reihenfolgenplanung aktiviert, so muss in den Stücklisten Kopfdaten eine Reihenfolgennummer angegeben werden. Diese kann von 01 bis 89 sein. Sie bewirkt, dass diese Reihenfolgennummerierung in die Los-Subnummer übernommen wird. Die Stückliste und damit das Los mit der höchsten Nummer wird als letztes gefertigt und so an den Kunden ausgeliefert. Dies bedeutet wiederum, dass der Endetermin des Loses mit der höchsten Subnummer zugleich der geplante Abliefertermin ist. In Verbindung mit der automatischen Ermittlung des Arbeitsgang-Beginns ergibt sich eine entsprechende Rückwärtsterminierung. Bitte beachten Sie, dass hier bewusst keine Begrenzung für die Vergangenheit vorgenommen wird um eine eventuell erforderliche Umplanung leichter darstellen zu können.

Wozu nun das Ganze?
Die Idee dahinter ist, dass der Kunde verschiedene Komponenten / Auftragspositionen bestellt und auch geliefert bekommt, welche zwar im Detail auf den Papieren aufgeführt sein müssen, diese aber in Abhängigkeiten von einander zusammen gebaut werden. Denken Sie z.B. an Verrohrungen in verschiedenen Qualitäten die in baulichen Maßnahmen berücksichtigt werden. Hier kauft der Kunde die teurere Verrohrung und will diese auch entsprechend ausgewiesen haben, aber die Fertigung muss das, auch terminlich entsprechen vor dem schließen des Bauwerks berücksichtigen.

Oder wie es in der Anforderung stand:
Auftrag 501 mit den Positionen Y1 und Y2 welche Stücklisten sind.
Die Stückliste Y1 beinhaltet die Unterstücklisten X2, X3 und X5
Die Stückliste Y2 beinhaltet die Unterstücklisten X1, X3 und X4

Gefertigt werden muss dies in der Reihenfolge
X1, X2, ... X5

D.h. Sie geben bei den Stücklisten X1 - X5 die Reihenfolgennummer 1 - 5 an und bei den Fertigproduktstücklisten dann die Reihenfolgennummern 89.
Hinweis: Sollten sich aus der Struktur gleiche Reihenfolgennummern ergeben, so werden die Los-SubNummern an der Einerstelle raufgezählt. Hier werden auch Buchstaben unterstützt, womit sich theoretisch 36 Möglichkeiten der gleichen Reihenfolge ergeben.

Um die Reihenfolgennummern anzuzeigen, wählen Sie in der Stückliste, z.B. der Y1 Info, Gesamtkalkulation und dann die Reportvariante Struktur.

Wählen Sie nun neue Lose anhand Auftrag und Sie erhalten eine entsprechende Reihung Ihrer Lose, wie z.B.:<br>
![](Lose_mit_Reihenfolge.gif)<br>
Bitte beachten Sie, dass in der Reihenfolgenauflösung kein direkter Auftragspositionsbezug im Los gegeben ist.
Bitte beachten Sie auch das unter [Beginntermin berechnen ausgeführte](index.htm#Beginntermin automatisch errechnen).

Bitte beachten Sie zusätzlich, dass bei aktiver Reihenfolgenplanung, die Überleitung der vorgeschlagenen Lose aus der internen Bestellung derzeit deaktiviert ist, da vor allem aus Gründen der Verdichtung der Auftragsbezug und damit die Reihenfolgenplanung verloren geht.

#### Wie wird mit gleichlautenden Stücklisten aus verteilten Stücklistenstrukturen umgegangen?
Es werden die benötigten Mengen auf die zuerst, also am Frühesten benötigte Position verdichtet. Diese Verdichtung greift jedoch nur für die Unterpositionen. Damit bleiben eventuelle unterschiedliche Liefertermine welche direkt im Auftrag definiert wurden erhalten.

#### Bestimmte Artikel / Stücklisten werden bei uns laufend gefertigt. Wie damit umgehen?
Es gibt in den verschiedensten Produkten immer wieder die unternehmerische Entscheidung, diese einfach nach Bedarf auf Zuruf oder auch nach einem ausgelagerten Kanban-System zu fertigen. Dafür wird gerne ein Los pro Kalenderjahr angelegt und die Werker buchen die gefertigten Stückzahlen auf dieses Los und erhöhen immer wieder automatisch die Losgröße durch die rückgemeldete Abliefermenge. Diese Stücklisten werden NUR bei der Funktion neue Lose anhand Auftrag nicht berücksichtigt, genauer die vorgeschlagene Sollmenge auf 0 gesetzt. [Siehe dazu auch](../Stueckliste/index.htm#Jahreslos).

Hinweis: [Zum Thema Kanban siehe]( {{<relref "/docs/schulung/begriffe#kanban_was_ist_das?" >}} ). In der Unternehmensgröße / Fertigungsauftragsgröße erachten wir Kanban als der Beginn eines perfekten Chaos. Kanban ist super, wenn lange gleiche Serien gefertigt werden. **Kieselstein ERP** Anwender sind kleine schnelle flexible KMU. Die Reaktionszeiten eines Kanban System's sind, aufgrund der menschlichen Abläufe viel zu langsam.
Oder in anderen Worten: Wieso Zettel von A nach B tragen, wenn du ein tief integriertes ERP hast.