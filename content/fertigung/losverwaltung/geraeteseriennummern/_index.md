---
title: "Geräteseriennummern"
linkTitle: "Geräteseriennummern"
categories: ["Geräteseriennummern"]
tags: ["Geräteseriennummern"]
weight: 300
description: >
  Seriennummern von Geräten mit den Seriennummern der verbauten Baugruppen verbinden
---
Geräteseriennummern
===================

In **Kieselstein ERP** steht Ihnen als eigenes Modul auch die Geräteseriennummernverwaltung zur Verfügung.

Die grundsätzliche Idee dahinter ist, dass, insbesondere Hersteller von Geräte welche aus mehreren Artikeln / Baugruppen bestehen, wissen müssen, in welchem Gerät welche Seriennummer des Artikels enthalten ist.

![](Geraeteseriennummer.png)

Wie oben skizziert geht es darum, zu wissen, dass der Artikel / die Baugruppe mit der Snr. 102 im Gerät mit der Snr. 900 enthalten ist. Bzw. Der Artikel mit der Seriennummer 113 im Gerät mit der Seriennummer 901.
In anderen Worten, die Geräteseriennummernverwaltung ist dann interessant, wenn Sie mehrere gleiche Geräte in einem Los fertigen und trotzdem von jeder Unterbraugruppe wissen müssen, in welchem Gerät der Artikel mit der Seriennummer verbaut wurde. Z.B. wegen Garantie, auf den einzelnen Unterbaugruppen.
In anderen Worten, ohne der Geräteseriennummernverwaltung müssten Sie für jedes Gerät ein eigenes Los anlegen, um die eindeutige Zuordnung zu erreichen.

Für diese Zuordnung müssen folgende Voraussetzungen gegeben sein:
Die Stückliste (Gerät) muss Seriennummern tragend sein.
Die gewünschten Artikel / Baugruppen müssen ebenfalls Seriennummern tragend sein.

Bei der Los-Materialausgabebuchung werden die Seriennummern der Artikel vom Lager ins Los gebucht.

Bei der Ablieferung des Gerätes von der Fertigung (Los) wiederum ans Lager wird je Gerät die Seriennummernzuordnung eingetragen.

D.h. es wird immer ein Stück Gerät zurückgemeldet(abgeliefert) und dann die in diesem Gerät verbauten Seriennummern der Artikel dem Gerät zugeordnet.

Die Erfassung ist so aufgebaut, dass diese mit Wedge CCD Scannern gemacht werden kann.

Der Ablauf ist also folgender:
-   Sie wechseln im Los auf Ablieferung
-   Mit neu ![](GeraeteSnr1.gif) beginnen Sie die nächste Ablieferung
-   Nun klicken Sie auf Seriennummer ![](GeraeteSnr2.gif)
-   Im erscheinenden Seriennummerndialog ![](GeraeteSnr3.gif) geben Sie die Serienummer des Gerätes (Snr. 900) an und klicken auf Übernehmen. Hier kann nur ein Gerät angegeben werden, da sonst keine eindeutige Zuordnung möglich wäre.
-   Nun klicken Sie auf speichern ![](GeraeteSnr4.gif).
-   sie sehen nun eine Liste alle Unterstücklisten dieses Loses, welche selbst Seriennummerntragend sind. Geben Sie hier bitte exakt die im Gerät verbauten Seriennummern an. Diese müssen für die jeweilige Baugruppe bereits vom Lager ins Los entnommen worden sein. ![](GeraeteSnr5.gif)
-   Wurden alle Seriennummern eingegeben, so klicken Sie auf Ok.
    Damit wird das Gerät dem Lager zugebucht und die Seriennummern der enthaltenen Artikel (Unterbaugruppen) mit der Seriennummer des Gerätes verknüpft.

Die Zuordnung der Seriennummer der Artikel zum Gerät sehen Sie:
-   In der Ablieferung des Loses im Detail der Ansicht einer einzelnen Ablieferung
    ![](GeraeteSnr6.gif)
-   Im Seriennummernjournal im Artikel
    Geben Sie hier die gesuchte Seriennummer ein, z.B. der Unterbaugruppe Snr. 102.
    So sehen Sie im Seriennummernjournal, dass diese Baugruppe im Los jj/nnnnnnn verbaut wurde und dass es mit dem Gerät 900 verbunden ist.

#### In meinem Gerät ist nur ein Seriennummern tragender Artikel. Das Gerät sollte die gleiche Seriennummer bekommen.
Mit dem Button Direkte Übernahme der Seriennummern ![](Direkte_Uebernahme_Seriennummer.gif) können die auf **<u>einem</u>** Artikel gebuchten Seriennummern auch als Geräte Seriennummer übernommen werden. Wählen Sie dazu einfach aus den in das Los gebuchten Seriennummern diejenigen Seriennummern aus die abgeliefert / fertiggemeldet werden sollen.

Von **Kieselstein ERP** wird für jede Seriennummer die Verkettung als Geräteseriennummer und damit je Seriennummer ein Ablieferungseintrag angelegt.

#### In meinem Gerät sind mehrere gleiche Artikel die Seriennummerntragend sind.
Bitte beachten Sie, dass für die Zuordnung von Seriennummern zu Geräteseriennummern die Menge der Position immer 1 sein muss.
Anderenfalls erhalten Sie die Hinweis, dass die Seriennummernartikel eine Positionsmenge von 1 haben muss.
In anderen Worten, wenn z.B. 2Stk eines gleichen Artikels in einem Gerät verbaut sind, z.B. zwei redundante Netzteile, so müssen diese als zwei Positionen erfasst werden mit je **einem Stück**.

#### Kann es vorkommen, dass eine Unterbaugruppe in mehreren Geräte verbraucht wurde?
Nicht gleichzeitig, aber hintereinander.
D.h. wenn Sie ein Gerät mit Baugruppen darinnen ausgeliefert haben, so wird es vorkommen, dass Sie im Service Fall einzelne Baugruppen wieder zurückbekommen. Um das Gerät so schnell wie möglich zum Funktionieren zu bringen, wird der Servicetechniker hier unter Umständen eine andere gleichwertige Baugruppe einbauen. Von Ihnen wird die Baugruppe mittels negativem Lieferschein zurückgenommen, repariert und eventuell in ein anderes Gerät wieder eingebaut. Daher kann es sein, dass Sie in der Anzeige der Geräteseriennummern in der die Unterbaugruppe enthalten ist, mehrere Geräte angezeigt bekommen.

#### Ich kann keine nachträgliche Materialentnahme buchen, warum?
Sie erhalten folgende Fehlermeldung beim Erfassen einer Materialentnahme mit seriennummerntragenden Artikel:

![](geraetesnr_meldung_1.PNG)

Bitte beachten Sie, dass es gibt zur Materialbuchung zwei Icons gibt. Einerseits die ![](NachtraeglicheMaterialentnahme.gif) Nachträgliche Materialentnahme und andererseits die ![](NachtraeglicheMat_Entnahme_ohne_Sollposition.gif) Nachträgliche Materialentnahme ohne Bezug zur Sollposition.
Bei den Geräteseriennummern verwenden Sie bitte das Icon ![](NachtraeglicheMaterialentnahme.gif) mit dem Pfeil verwendet werden. Dann kann die Position mit SNR gebucht und das Material entnommen werden.

#### Wie können Listen von Geräteseriennummern abgeliefert werden?
Gerade in Unternehmen, welche die Baugruppenfertigung an Ihre Sublieferanten / Lohnbestücker ausgelagert haben, ist oft die Situation gegeben, dass man Listen von Geräte mit Unterseriennummern bekommt. Damit diese nun nicht mühsam einzeln erfasst werden müssen, gibt es in der Losablieferung den Button "Geräteseriennummernablieferung per CSV".<br>
Es muss dafür der Parameter SERIENNUMMER_EINEINDEUTIG auf eins stehen und es muss die Zusatzfunktion Gerätesseriennummern vorhanden sein.<br>
Der Import ist so aufgebaut, dass dieser pro Los durchgeführt wird. Damit sind die beteiligten Artikel eindeutig definiert. Nun wird pro Gerät eine Zeile importiert. Hier gilt die Definition, dass die **erste** Seriennummer die Seriennummer des **abzuliefernden Gerätes** ist und danach dann die Seriennummern der (Unter-)Baugruppen kommen, welche in diesem Gerät verbaut wurden.<br>
Mit der nächsten Zeile wird das nächste Gerät importiert.

Der Import wird in einem Lauf durchgeführt. Sollten eventuelle Buchungen nicht möglich sein, weil z.B. die Seriennummer nicht im Los gebucht ist, so wird diese Zeile = dieses Gerät beim Import übersprungen. Am Ende des Imports erscheint eine Liste mit den Buchungen, welche NICHT durchgeführt werden konnten.<br>
Info: Das Import-File hat KEINE Überschrift.<br>
B0501;6200102415920;A19848;292777;313438

Das Los wird auch bei vollständiger Ablieferung nicht erledigt.

#### Wie finde ich Geräteseriennummern?
Siehe [Seriennummernjournal]( {{<relref "/docs/stammdaten/artikel/serienchargennummern/#wo-findet-man-die-seriennummern-bzw-chargennummern-historie" >}} )
