---
title: "Papierlose Fertigung"
linkTitle: "Papierlos"
categories: ["Papierlose Fertigung"]
tags: ["Papierlos"]
weight: 100
description: >
  Papierlose Fertigung
---
Im **Kieselstein ERP** steht ab der Terminal Version ... welche auch die Kieselstein Version 0.2.xx voraussetzt, die sogenannte Papierlose Fertigung zur Verfügung.

Die Grundidee ist, dass damit die bisher verwendeten Fertigungsbegleitscheine durch eine Touch Bedienung ersetzt werden und dass auf faktisch jeder Maschine ein Terminal zur Verfügung steht. Mit den Android Terminals sollten hier auch die Hardwarekosten in einem erträglichen Rahmen sein.

Voraussetzungen:
- um die Dokumente aus der Dokumentenablage anzeigen zu können müssen für den Benutzer Terminal auch die Berechtigungen auf die Dokumente gesetzt werden.

In der Terminalkonfiguration muss auch definiert werden, wie weit von jeweils heute zurück die offenen Arbeitsgänge / Lose-Beginntermin (ist noch herauszufinden) eingelesen werden sollten.<br>
Default 14Tage wenn du ältere Planungsdaten haben solltes entweder erhöhen, was in deutlich längeren Ladezeiten resultieren kann. Wir raten: Bitte die Daten pflegen. Offene Arbeitsgänge die mehr als 14Tage zurück liegen kann es meiner Meinung nach nicht geben.

In der Terminal Konfiguration können
1. die Fertigungsgruppe
2. die Maschinengruppe
3. die Maschine

definiert werden. Die Wertung geht von Maschine zur Fertigungsgruppe. D.h. wenn das Terminal für eine Maschine definiert ist, greift diese Definition, egal was darüber eingestellt ist.
In anderen Worten, die Maschine greift vor der Maschinengruppe und diese vor der Fertigungsgruppe.

Bitte beachte, dass diese Combobox jeweils eigenständig zu betrachten sind. D.h. bei der Konfiguration ergibt sich keine Filterung aufgrund des Vorgängers. D.h. es werden immer alle Maschinengruppen / Maschinen angezeigt, egal was davor eingestellt wurde.

Es kann definiert werden, wieviele Tage in die Zukunft die offenen Arbeitsgänge betrachtet werden und wieviele offenen Arbeitsgänge, beginnend vom ältesten eingelesen werden.
Bitte beachte, dass die Anzahl der einzulesenden offenen Arbeitsgänge in die Reaktionsgeschwindigkeit deines Terminals eingeht.
Die tatsächlich einzulesende Anzahl hängt auch mit der Kombination der  Maschinen(gruppen) / Fertigungsgruppen Filter zusammen.
Beachte auch, dass eine hohe Refreshrate und eine große Anzahl an offenen Arbeitsgängen, deinen **Kieselstein ERP** Server entsprechend belastet. 

Wichtig: Die Filterung auf die Fertigungsgruppe greift auf die Maschinengruppe und nicht auf die Fertigungsgruppe die in den Kopfdaten der Lose definiert ist.

Refreshintervall wirkt auf beide Fenster der Papierlosen Fertigung. Das sind die Auswahlliste der Person der Maschinen und die Anzeige der aktiven Arbeitsgänge

### Einstellung von Zusatzbuttons
#### Allgemein, also immer für das Los der gewählten Tätigkeit
In der LP_Arbeitsplatzkonfig können derzeit global für alle Terminals Zusatzbuttons definiert werden. Die Buchung dieser Buttons greift immer auf das ausgewählte Los. D.h. es kann damit z.B. generell die Nacharbeit auf das gerade in Arbeit befindliche Los definiert und gebucht werden.

```bash
> INSERT INTO public.lp_arbeitsplatz(
>    i_id, c_standort, c_bemerkung, c_pcname, c_typ, c_geraetecode, b_default)
>    VALUES (12, null, null, 'Terminal', 'WIN-Terminal', 'PAPIERLOSE_FERTIGUNG', false);
>    
> INSERT INTO public.lp_arbeitsplatzkonfiguration(
>    arbeitsplatz_i_id, c_system)
>    VALUES (12, '{"additionalBookingButtons":[{"cnr":"Z0001","name":"Zuschneiden"},
> {"cnr"":"Z0002","name":"Schweißen"}]}')
```
Anmerkung:<br>
Der c_pcname ist egal, das Kriterium ist der c_geraetecode mit 'PAPIERLOSE_FERTIGUNG'<br>
Im c_system ist die Konfiguration als JSON String, derzeit nur für die zusätzlichen Buttons<br>(im Beispiel 2 Buttons) und wird später erweitert.

cnr ... Artikelnummer der Tätigkeit
name ... Bezeichnung des Zusatzbuttons

Hinweis: Die Definition der Fertigungsgruppe, Maschinengruppe und der Maschinen muss sich in der eigenen Bezeichnung untereinander unterscheiden, sodass man alleine schon anhand der unterschiedlichen Bezeichnungen erkenn, welche Definition für das Terminal nun gilt.

#### Zusatzbuttons mit Buchung auf bestimmte Lose
z.B. Will man die Reinigung der Fertigungshalle, oder das Späne ausleeren immer auf einem Los haben, um so einen Überblick über diese Kosten zu bekommen.<br>
Dafür können seit der Version 0.0.36 weitere Losspezifische Zusatzbuttons definiert werden.

Beispiel (2 Buttons ohne Losangabe, 1 Button mit Los):
```bash
{ "additionalBookingButtons": [ { "cnr": "Z0001", "name": "Zuschneiden" }, { "cnr": "Z0002", "name": "Schweißen" }, { "cnr": "Z0000", "name": "Aufräumen", "productionordercnr": "23/0000001" } ] }
```

productionordercnr ... Vollständige Losnummer auf die diese Buchung erfolgen sollte

D.h. für die zusätzlichen Buttons ist zusätzlich die entsprechende Losnummer anzugeben. Diese Buttons buchen immer auf das angegebene Los und nicht auf das gewählte Los in der Auswahlliste. Die Bezeichnung der neuen Buttons bildet sich aus Losnummer + Name.

Refreshzeit sollte hoch sein wegen Gleichzeitigkeit und Serverlast
-> ev. Signalisierung der Terminals untereinander (UDP)
Wenn der Mitarbeiter auf einer anderen Maschine als der nun am Terminal ausgewählten arbeitet (und der Refresh das mitbekommen hat, ev. Refreshbutton drücken) dann ist der Personal+ Button wieder schwarz, womit der Mitarbeiter sagen kann ich mache da wieder weiter.

Es wird zusätzlich die aktuell vom Mitarbeiter gebuchte Tätigkeit also Info angezeigt.
Damit wird der Button der Person+ aktiviert (schwarz statt grau)

Wenn Rechte auf DokuAblage, dann auch anzeigen


-->>
Wenn man die erwarten Arbeitsgänge, der gewählten Maschinen nicht sieht, prüfen ob die Anzeige der Maschinengruppe (MG darstellen) angehakt ist.
Es werden nur die Maschinen angezeigt, bei denen die Maschinengruppe angezeigt werden sollte.

Wie alt können die offenen Lose / Arbeitsgänge sein und wie weit geht das in die Zukunft
Es kann dies je Terminal konfiguriert werden.
Umgekehrt werde ich meinen Mitarbeitern keine Arbeitsgänge ausführen lassen, die erst in 5 oder mehr Wochen zu produzieren sind. Daher können beide Werte in der Terminalkonfiguration eingerichtet werden.

Wie nun mit dringenden Arbeiten, die sofort gemacht werden sollten umgehen, wie diese Information sofort ans Terminal rausbringen?

### Maschinen Arbeitsgang Auswahl
![](Maschinen_Arbeitsgang_Auswahl.png)

In dieser Liste siehst du alle offenen Arbeitsgänge, welche der Definition, nicht mehr als *Planungstage* von jetzt / heute in die Zukunft und von Anbeginn der offenen Arbeitsgänge an nicht mehr als *Limit für Response* offene Arbeitsgänge nach den gewählten Filterbedingungen (Fertigungsgruppe, Maschinengruppe, Maschine)

| Überschrift | Bedeutung |
| --- | --- |
| Fertigungsgruppe | die der Maschinengruppe zugewiesene Fertigungsgruppe.<br>**ACHTUNG:** Das hat nichts mit der Fertigungsgruppe der Lose zu tun |
| Maschinengruppe | Die Bezeichnung der Maschinengruppe |
| Maschine | Die Bezeichnung der Maschine |
| MA | Das Kurzzeichen des Mitarbeiters / der Mitarbeiterin die den Arbeitsgang aktuell gestartet hat (beachte auch das *Aktualisierungsintervall*)
| Losnummer | die Losnummer des Arbeitsgangs auf dem die Maschine läuft |
| AG | Die Arbeitsgangnummer inkl. Unterarbeitsgang? |
| seit | Seit diesem Zeitpunkt wurde von MA der Arbeitsgang auf der Maschine gestartet |

#### Auswahl einer Maschine
Tippe die Zeile der Maschine entsprechend an und du kommst damit in die nächste Maske.

Erscheint dir diese Liste nicht aktuell, so wähle ![](Aktualisieren.png) aktualisieren.<br>
Um zurück zur Auswahlmaske zu kommen, wähle ![](zurueck.png) zurück.


### Bedienen des auf der Maschine laufenden Arbeitsgangs
![](Bedienen_des_Arbeitsgangs.png)
In der Überschrift siehst du den Namen des angemeldeten Mitarbeiters / der Mitarbeiterin. Solltest du die Arbeit an der Maschine übernehmen, aber nicht diese Person sein, so bitte mit zurück ![](zurueck.png) zur Auswahl der Mitarbeiter zurückgehen und mit deinem Ausweis entsprechend anmelden.

Unter dem Namen siehst du den gerade laufenden Arbeitsgang auf dieser Maschine. Welche Maschine das ist siehst du im Fenstertitel (Fertigungsgruppe, Maschinengruppe, Maschine)

| Überschrift | Bedeutung |
| --- | --- |
| Losnummer | Die Losnummer des laufenden Arbeitsgangs |
| Stücklistennummer | Die Stücklistennummer und die Bezeichnung der Stückliste |
| AG | Die Arbeitsgangnummer inkl. Unterarbeitsgang |
| Kunde | Der Name1 des Kunden |
| Beschreibung | Die Beschreibung der Tätigkeit |
| seit | Der Startzeitpunkt der Arbeit auf diesem Arbeitsgang |
| von | Von welcher Person wurde dieser Arbeitsgang gestartet<br>Dieser ist rot hinterlegt, wenn das nicht die oben angezeigte Person ist. In diesem Falle kannst du durch Tippen auf ![](AG_uebernehmen.png) Arbeitsgang übernehmen diesen Arbeitsgang auf der Maschine weiterbearbeiten. Das bewirkt, dass die Maschine für die vorherige Person gestoppt wird und nun auf dich steht **und** die Zeitbuchung für die bisherige Person läuft auf dem Arbeitsgang weiter (wenn sie gestartet war und nicht auf einer anderen Maschine gearbeitet hat). Für dich wird nun deine Arbeitszeit auf diesn Arbeitsgang gebucht.
| Arbeitsgang wechseln | Durch tippen auf ![](Arbeitsgangwechsel.png) kommst du in die Auswahlliste (siehe unten) der weiteren offenen Arbeitsgänge dieser Maschine. |

Von der Idee her bleibt diese Maske immer stehen, solange dieses Terminal eindeutig der Maschine zugewiesen ist. Einzig für den MitarbeiterInnen Wechsel, gehst du zurück in die Auswahlmaske / Erfassung des Mitarbeiters.

#### Die Bedeutung der Knöpfe beim laufenden Arbeitsgang
Wenn du den Arbeitsgang antippst, werden die darunter dargestellten Knöpfe aktiviert und du kannst diese verwenden.

Wirkung für den einzelnen Arbeitsgang
| Überschrift | Bedeutung |
| --- | --- |
| Autonom | Bedeutet du meldest dich von diesem Arbeitsgang (mit deiner Zeitbuchung) ab und die Maschine läuft autonom weiter. In der Zeiterfassung wird das als Endebuchung dargestellt.<br>Autonom ist nur verwendbar, wenn der Arbeitsgang von mir gestartet wurde. |
| Maschine stop | Die Zeit auf der Maschine wird gestoppt. Deine Zeit auf dem Arbeitsgang läuft aber weiter |
| Fertig | dieser Arbeitsgang wurde fertiggestellt. Damit erscheint er nicht mehr auf dem Terminal. Sollte dies fälschlich erfolgt sein, muss dies im Client zurückgenommen werden|
| Stückrückmeldung | |

Wirkung auf das gesamte Los
| Überschrift | Bedeutung |
| --- | --- |
| Losmenge | Korrektur der Losgröße |
| Abliefern | Die Ablieferbuchung für das Los |
| Material | Material anhand des Sollmeteriales in das Los nachbuchen |
| Dokumente | Anzeige der Dokumente des Stücklistenartikels |

Zusätzliche nicht vorgesehene Tätigkeiten

### Arbeitsgangwechsel
![](Arbeitsgangwechsel02.png)
Wähle hier den nächsten Arbeitsgang aus, den du starten möchtest.

## Buchen auf abweichende Maschine
In der Liste deiner auswählbaren Maschinen 
![](Auswaehlbare_Maschinen.png)  

hast du rechts oben auch einen Listen-Button<br>
![](Arbeitsgaenge.png) <br>
Tipps du hier darauf, kommt einer Liste aller offenen Arbeitsgänge.

Sind den Arbeitsgängen geplante Maschinen zugeordnet, so wird unter dem 
AG-Beginn Datum die aktuell zugewiesene Maschine angezeigt.

![](MaschineAendern_Ueberschrift.png)  

![](MaschineAendern.png)  

Durch einen Tipp auf diese Maschine kann eine andere Maschine ausgewählt werden und durch Tipp auf den grünen Pfeil wird der Arbeitsgang mit der abweichenden Maschine gestartet.<br>
Die hier angezeigten Maschinen sind auf die Maschinengruppe der definierten Maschine eingeschränkt. D.h. wenn du z.B. alle Bohrmaschinen in einer Maschinengruppe hast, so werden nur die Bohrmaschinen angezeigt, aber nicht die Maschinen der Maschinengruppe Fräsen.

Der Arbeitsgang wird durch Tipp auf den grünen Pfeil ![](AG_Starten.png)  gestartet.
