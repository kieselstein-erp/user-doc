---
title: "Interne Bestellung"
linkTitle: "interne Bestellung"
categories: ["Interne Bestellung"]
tags: ["Interne Bestellung"]
weight: 400
description: >
  Welche Lose müssen angelegt und damit produziert werden, um rechtzeitig liefern zu können?
---
Im **Kieselstein ERP** steht 
Ist der Bestellvorschlag für die eigene Produktion und den Fremdfertigern, mit Berücksichtigung der dafür benötigten Unterstücklisten / Unterbaugruppen.

Interne Bestellung
==================

Genauso wie Sie Artikel / Material bei Ihren Lieferanten bestellen müssen, muss auch Ihre Fertigung wissen, wann welcher Fertigungsauftrag zu produzieren ist. Dazu dient die Interne Bestellung.

Der grundsätzlich Gedanke ist, dass der Vertrieb nur die Aufträge anzulegen braucht. Wenn die Fertigungsleitung / Arbeitsvorbereitung dann regelmäßig die Aufträge überprüft werden alle Produkte rechtzeitig hergestellt. Wichtig: Aus der internen Bestellung müssen dann die Lose angelegt werden. Sind die Lose angelegt, so kann (erst) der (externe) Bestellvorschlag erzeugt werden. Anderenfalls würden die Reservierungen für die Artikel die sich erst indirekt durch Lose ergeben fehlen.

Grundsätzlich ist die Berechnungslogik der Internen Bestellung gleich wie der Bestellvorschlag, hat jedoch die Besonderheit, dass zusätzlich die Stücklistenhierarchien aufgelöst werden. D.h. die angelegten Reservierungen / Fehlmengen werden solange durch iteriert / die Stücklisten-Hierarchien aufgelöst, bis alle erforderlichen Unterstücklisten angelegt sind. Da dieser Lauf auch während des Fertigungsprozesses durchgeführt werden muss, werden die offenen Lose ebenfalls berücksichtigt.

Bitte beachten Sie hier auch die Wirkung der Fertigungs-Satzgröße, welche nur in der internen Bestellung berücksichtigt wird. Ist bei dem (Stücklisten-) Artikel eine Fertigungssatzgröße eingestellt, so bewirkt dies, dass immer ganzzahlige Vielfache der Fertigungssatzgröße angelegt werden. Die Vorgehensweise ist, so, dass bei Unterschreitung des Lager-Mindeststandes auf den Lagersollbestand aufgefüllt wird und dieser errechnete Wert passend zur Fertigungssatzgröße aufgerundet wird. Sollte immer nur wenn unbedingt erforderlich eine bestimmte Menge produziert werden, so belassen Sie das Lagersoll auf 0 und definieren lediglich die Fertigungssatzgröße.

Ein Beispiel zur Berechnungslogik, ohne Lager-Mindeststand, Lagersollstand bzw. Fertigungssatzgröße:

**1\. Stücklisten**

1.1 Stückliste A-Kopf            Dies ist eine sogenannte Kopfstückliste in der mehrere Unterstücklisten enthalten sind

    1 Stk Unterstückliste U1

    2 Stk Unterstückliste U2    In dieser Unterstückliste ist noch eine weitere Unter-Unterstückliste enthalten

        3 Stk Unter-Unterstückliste U2A

1.2 Stückliste B-Kopf

    1 Stk Unterstückliste U2    Daraus ergibt sich automatisch, dass auch hier die Unter-Unterstückliste U2A enthalten ist.

        3 Stk Unter-Unterstückliste U2A

**2\. Aufträge**

2.1 Auftrag Müller 100 Stk Stückliste A-Kopf, Liefertermin 20.12.2006

2.2 Auftrag Maier 75 Stk Stückliste B-Kopf, Liefertermin 18.11.2006

2.3 Auftrag Ersatzteil 2 Stk Unterstückliste U2, Liefertermin 4.11.2006

**3\. Lagerstände**

3.1 Stückliste A-Kopf      0 Stk

3.2 Unterstückliste U1     2 Stk

3.3 Unterstückliste U2     3 Stk

3.4 Unter-Unterstkl U2A   1 Stk

**4\. offene Fertigungsaufträge**

4.1 Stückliste A-Kopf 50Stk Endtermin 18.12.2006

4.2 Unterstückliste U1 48(50)Stk Endtermin 15.12.2006, davon 2Stk erledigt -> liegen auf Lager

4.3 Unterstückliste U2 97(100)Stk Endtermin 15.12.2006, davon 3Stk erledigt -> liegen auf Lager

4.4 Unter-Unterstückliste U2A 290(300)Stk Endtermin 10.12.2006, davon 10Stk erledigt -> 1 liegt auf Lager, 9 (3x3) sind in der U2 verbaut worden.

**5\. Zeitlicher Zusammenhang der Unterstücklisten**

Wie ist der zeitliche Zusammenhang vom Auftragstermin (Liefertermin bzw. Positionstermin) zum Fertigungstermin, also Beginn und Ende über die Stücklistenhierarchie.

![](Interne_Bestellung.png)

Die Durchlaufzeit wird in den Stücklisten (Kopfdaten) definiert und wird für die Berechnung der Rückwärtsterminierung ausgehend vom Auftragsliefertermin, welcher auch in den Reservierungen eingetragen ist, ermittelt.

**Wie ist nun die Vorgehensweise zu Ermittlung der anzulegenden Lose?**

anhand der Lagerstände, der Reservierungen, der Fehlmengen, der offenen Fertigungsmengen werden die anzulegenden Stücklisten ermittelt. (Anmerkung: Die offenen Fertigungsmengen können wie Bestellungen unterwegs betrachtet werden). Offene Fertigungsmengen sind die angelegte Losgröße einer Stückliste abzüglich der bereits zurückgemeldeten Menge (Abliefermenge). Die abgelieferte Menge ist bereits im Lagerstand berücksichtigt, die noch benötigte Menge ist in der Reservierung bzw. in den Fehlmengen berücksichtigt.

Wichtig: Wenn bei der Rückmeldung der Ablieferungsmengen die Fehlermeldung Sollsatzgröße Unterschritten angezeigt wird, so kann dies bei der Internen Bestellung bewirken, dass eventuell eine zu große Menge für die Produktion vorgeschlagen wird.

| **Stückliste** | **Auslöser** | **Bedarf** | **Menge** | **Termin / Ende** | **DLZ** |
| --- |  --- |  --: |  --: |  --- |  --- |
| A-Kopf | 1...Auftrag Müller |  | 100 | 20.12.2006 | 7 AB-Vorlauf |
|   --"-- | 2...Lagerstand |  | 0 |  |  |
|   --"-- | 3...offene Fert.Mng |  | 50 | 18.12.2006 |  |
|   --"-- | 4...zus.Bedarf 1 | 50 |  | 13.12.2006 | 3 |
| U1 | 5...offene Fert.Mng |  | 48 | 15.12.2006 |  |
| -"- | 6...Lagerstand |  | 2 |  |  |
| -"- | 7...zus.Bedarf 2 aus 4 | 50 |  | 10.12.2006 | 4 |
| U2 | 8...offene Fert.Mng |  | 97 | 15.12.2006 |  |
| -"- | 9...Lagerstand |  | 3 |  |  |
| -"- | 10...zus.Bedarf 3 aus 4 | 100 |  | 10.12.2006 | 9 |
| U2A | 11...offene Fert.Mng |  | 290 | 10.12.2006 |  |
| -"- | 12...Lagerstand |  | 1 |  |  |
| -"- | 13...zus.Bedarf 4 aus 10 | 300 |  | 1.12.2006 | 3 |

**Vorlaufzeitzeiten Aufträge:** Hier wird der Zeitraum angegeben um den der Endtermin des Fertigungsloses früher fertig sein muss, als der tatsächliche Liefertermin ist. Ist der Auslöser des Bedarfes ein Auftrag oder ein Forecast-CallOff (Tag oder Woche)-Auftrag, so wird diese Vorlauzeit durch die Lieferdauer der Lieferadresse ersetzt und somit das Los gerade rechtzeitig zur Auslieferung fertig. Dieser Termin wird aus Auftragssicht oft auch Abliefertermin genannt. Die standard Vorlaufzeit der Aufträge kann in den Mandantenparametern eingestellt und für jede interne Bestellung neu definiert werden.

**Liefertermin für Artikel ohne Reservierungen:** Für Artikel die nur aufgrund von Lagermindestbeständen in die interne Bestellung kommen, wird dieser Termin als Endtermin herangezogen.

**Durchlaufzeiten:** In diesem Beispiel wurden bei den Durchlaufzeiten die Wochenenden und Feiertage aus Gründen der leichteren Verständlichkeit NICHT berücksichtigt. In **Kieselstein ERP** werden nur Arbeitstage für die Ermittlung der Durchlaufzeiten herangezogen. Arbeitstage sind jene, die kein Samstag und kein Sonntag und kein Feiertag bzw. Betriebsurlaubstag sind.

In diesem Beispiel wird davon ausgegangen, dass alle erforderlichen Artikel in **Kieselstein ERP** angelegt sind.

In jeder Stücklistenebene können auch Zukaufsartikel enthalten sein. Diese wurden aus Gründen der Übersichtlichkeit nicht mit angeführt.

Versuch einer anderen [Darstellung](Interne_Bestellung.xls).

![](interne_Bestellung.JPG)

Häufig gestellte Fragen zur Internen Bestellung:

#### Mein Artikel xy aus Auftrag 4711 erscheint nicht in der internen Bestellung.
Damit ein Artikel in der internen Bestellung ermittelt wird, muss er eine eigengefertigte Stückliste sein.

D.h. bitte prüfen Sie, ob der gewünschte Artikel auch in der Stücklistenverwaltung angelegt ist. Ist dies der Fall, so stellen Sie bitte sicher, dass in den Kopfdaten der Stückliste bei Fremdfertigung KEIN Haken eingetragen ist.
Sollte dies alles richtig sein, so prüfen Sie bitte auch die Bewegungsvorschau. Wenn von diesem Artikel ausreichend auf Lager bzw. bereits in Fertigung ist, so muss ebenfalls kein Fertigungslos angelegt werden.

#### Warum wird mein Los nicht für den neu angelegten Auftrag, sondern für einen älteren reserviert?
Wie oben ausgeführt erfolgt die Auflösung, die Berechnung der Bedarfe rein über die terminliche Situation. (Ausnahme: [exakter Auftragsbezug](#Eingabefelder interne Bestellung)) D.h. wird z.B. ein neuer Auftrag zusätzlich zu einem früheren Termin eingeplant so verschieben sich dadurch die Bedarfe und deren Erfüllungen. D.h. es wird unter Umständen der neu angelegte Auftrag mit dem früheren Termin noch durch geplante Losablieferungen erfüllt, jedoch kann daher ein später an den Kunden zu liefernder Auftrag nicht mehr erfüllt werden. Daher wird für diesen Auftrag ein neues Los vorgeschlagen.

<a name="Eingabefelder interne Bestellung"></a>

#### Bedeutung der Eingabefelder für die interne Bestellung
Wenn Sie auf Interne Bestellung durchführen klicken![](Interne_Bestellung_Start.gif), so erscheint nachfolgender Dialog.

![](Interne_Bestellung_Dialog.PNG)

Die Felder bedeuten im Einzelnen:
| Feld | Bedeutung |
| --- | --- |
| <a name="Vorlaufzeit"></a>Vorlaufzeit | Der Endetermin eines Loses sollte die angegebenen Tage vor dem Liefertermin eingeplant sein. Ist der Auslöser ein Auftrag oder ein Forecast-CallOff-Auftrag, so wird die Lieferdauer der Lieferadresse für die Berechnung verwendet.<br>**Hinweis:** Der Beginntermin des Loses ergibt sich aus der Durchlaufzeit der Stückliste |
| <a name="Toleranz"></a>Toleranz | Sind bereits Lose für einen Produktionstermin angelegt, wieviele Tage vom oben errechneten Termin dürfen diese (verspätet) fertig werden. Siehe dazu auch Parameter:  INTERNEBESTELLUNG_VERDICHTUNGSZEITRAUM |
| Liefertermin für Artikel ohne Reservierung | Werden Artikel ohne Reservierungen z.B. nur wegen Lagermindeststandsunterschreitungen aufgrund von händischen Verbrauchsbuchungen in die interne Bestellung aufgenommen, so wird dieser Termin als Endetermin für die Lose verwendet. |
| Interne Bestellung löschen | Sind bereits Daten in der internen Bestellung vorhanden, so werden diese, wenn angehakt, vor der Errechnung der neuen Daten gelöscht.Siehe dazu auch Parameter: DEFAULT_INTERNEBESTELLUNG_LOESCHEN |
| verdichten | Soll anschließend an die Errechnung der Daten für die interne Bestellung diese sofort verdichtet werden. |
| exakter Auftragsbezug | Berücksichtigt Lose mit Auftragsbezug exakt, d.h. dass bei der Erfüllungsberechnung zuerst der 1:1 Bezug zwischen Auftragsposition und Los berücksichtigt und somit als erledigt betrachtet wird.Das wirkt vor der eigentlichen rein terminlichen Betrachtung der Bedarfe. |
| Tage | Wieviele Tage sollten verdichtet / zusammengefasst werden. Siehe dazu auch Interne Bestellung verdichten. |
| Los | Ev. sollten nur die Bedarfe, an weiteren zu fertigenden Losen, für ein bestimmtes Los errechnet werden. Wird nur ein Los ausgewählt, so werden die Materialartikel des Loses als Definition der zu prüfenden Artikel verwendet. Für die enthaltenen Artikel wird die Bewegungsvorschau für die Berechnung der Bedarfe herangezogen. |
| Aufträge | Ev. sollten nur die Bedarfe, für entsprechende Kundenaufträge errechnet werden. Analog zu den Losen werden hier die Artikel der Auftragspositionen für die zu prüfenden Artikel verwendet. Auch für diese wird die Bewegungsvorschau für die Berechnung der Bedarfe herangezogen.Bitte bedenken Sie, dass eventuelle Bedarfe auf Stücklistenbasis errechnet werden. D.h. wenn bereits ausreichend Unterstücklisten als Los angelegt sind, werden keine weiteren Fertigungsaufträge vorgeschlagen.Sollten Sie dies erzwingen wollen, so nutzen Sie bitte die Funktion [neue Lose aus Auftrag]( {{<relref "/fertigung/losverwaltung/los_anhand_kundenauftraegen">}} ) (aus der Sicht Los-Auswahl) |

#### Wodurch entsteht die Losgröße in der Internen Bestellung:
Neben dem Bedarf der aus der Erstellung von Auftragsbestätigungen entsteht, werden die Einträge im Artikel Reiter Bestelldaten berücksichtigt. Ist hier im Lagermindeststand eine Menge eingetragen, so wird auf die Menge des Lagersollstand aufgefüllt.
Bitte beachten Sie hier auch die Wirkung der Fertigungs-Satzgröße, welche nur in der internen Bestellung berücksichtigt wird. Ist bei dem (Stücklisten-) Artikel eine Fertigungssatzgröße eingestellt, so bewirkt dies, dass immer ganzzahlige Vielfache der Fertigungssatzgröße angelegt werden (auch bei Losen, die direkt aus dem Auftrag kommen). Die Vorgehensweise ist so, dass bei Unterschreitung des Lager-Mindeststandes auf den Lagersollbestand aufgefüllt wird und dieser errechnete Wert passend zur Fertigungssatzgröße aufgerundet wird.

Bitte achten Sie vor allem bei Unterstücklisten auf die korrekte Lagerbuchung: Wenn bei der Rückmeldung der Ablieferungsmengen Fehlermeldungen Sollsatzgröße Unterschritten angezeigt wird, so kann dies bei der Internen Bestellung bewirken, dass eventuell eine zu große Menge für die Produktion vorgeschlagen wird.

<a name="verdichten"></a>Verdichten der internen Bestellung

Durch Klick auf den Knopf ![](Verdichten.gif) interne Bestellung verdichten werden Lose für gleiche Artikel die im angegebenen Zeitraum gefertigt werden sollen auf ein Los zum frühesten benötigten Termin zusammengefasst. Dies wird vor allem gemacht um die Rüstkosten zu reduzieren. Zusätzlich werden die Lagermindeststände geprüft.
Wenn Sie mehrere Zeilen markieren und auf das Icon Verdichten ![](Verdichten.gif) anklicken, so erfolgt die Abfrage, ob alle oder nur die markierten Zeilen verdichtet werden sollen.
![](verdichten_interneBestellung.JPG)
Wenn der Parameter INT_BEST_VERDICHTEN_RAHMENPRUEFUNG gesetzt ist, so werden beim Verdichten die Rahmenmengen geprüft.

#### Werden Rahmenaufträge auch in der internen Bestellung berücksichtig?
Nein. Da die Rahmenaufträge keine Reservierungen (sondern nur Rahmenreservierungen) auslösen, werden diese in der Bewegungsvorschau-Berechnung nicht berücksichtigt und somit auch nicht in der internen Bestellung.
Hintergrund: Ein Rahmenauftrag läuft idealerweise einige Monate, in der Regel 12Monate. Somit muss der entsprechende Abrufplan manuell definiert werden, da nur Sie wissen in welchen Losgrößen idealerweise dieser Artikel zu produzieren ist.

**Prüfen der Lagermindeststände**

Es wird für Artikel, bei denen ein Lagermindeststand oder eine Fertigungssatzgröße eingetragen ist, überprüft, ob die errechnete anzulegende Losgröße noch durch offene Rahmenaufträge gedeckt ist. Also ob offene Rahmenreservierungen für diesen Artikel in einer Menge die gleich oder größer der vorgeschlagenen Fertigungsmenge ist eingetragen sind.
Ist dem nicht so, erscheint eine Meldung in der die Liste der betroffenen vorgeschlagenen Lose aufgeführt wird.
Zweck dieser Überprüfung ist, dass in aller Regel Lagermindeststände bzw. Fertigungssatzgrößen nur für Artikel eingepflegt werden, die laufend produziert werden.

Als Verdichtungszeitraum für die Verdichtung der interne Bestellung wird der Wert des Mandantenparameters INTERNEBESTELLUNG_VERDICHTUNGSZEITRAUM verwendet.

#### Können die Termine für die anzulegenden Lose vorgegeben werden?
Verwenden Sie dazu entweder die Änderung der Detaildaten der einzelnen Zeilen und definieren zum Beispiel Termine und Dauer.
![](beginn_ende_dauer_IntBest.JPG)
Wenn einzelne Lose nicht angelegt werden sollten, so entfernen Sie die Zeilen durch Klick auf stornieren/löschen ![](loeschen.jpg).

Um für alle Einträge in der internen Bestellung die Termine gemeinsam zu setzen, nutzen Sie bitte den Menüpunkt Bearbeiten, Beginn/Ende aller Einträge ändern.
![](beginn_ende_dauer_IntBest_alle.JPG)
Damit werden eben für alle Einträge der Internen Bestellung die Beginn und Ende Termine gesetzt.

Dies ist vor allem in Verbindung mit der Internen Bestellung für einzelne Lose entsprechend praktisch.

#### Wie werden aus der Vorschlagsliste Lose erstellt?
Der Klick auf den Knopf  ![](Lose_anlegen.JPG) Lose anlegen bewirkt, dass die gesamten Lose bzw. nur die markierten Zeilen der Auswahlliste erstellt werden.
Bitte definieren Sie vor der Überleitung die gewünschten Termine.
Bei der Überleitung kann definiert werden, in wie weit der Auftragsbezug mit übernommen wird.
![](Lose_mit_Auftragsbezug_anlegen.jpg)

Bitte beachten:
Ist in Ihrem **Kieselstein ERP** die [Reihenfolgenplanung](Lose_anhand_Kundenauftraegen.htm#Anlage nach Reihenfolge) aktiviert, so ist derzeit die Überleitung der vorgeschlagenen Lose deaktiviert, da vor allem aus Gründen der Verdichtung der Auftragsbezug und damit die Reihenfolgenplanung verloren geht.

#### Bei der Überleitung der internen Bestellung bleiben einzelne Zeilen stehen?
Damit wird signalisiert, dass für diesen Auftrag, genauer für diese Auftragsposition bereits Lose angelegt sind. Diese werden nicht überschrieben und daher bleiben diese Bedarfszeilen stehen.
Sie haben nun zwei Möglichkeiten, um dieses Thema zu lösen:
a.) Wenn der Artikel der zusätzlich gefertigt werden sollte bereits in einem Los für diesen Auftrag eingetragen ist, so erhöhen Sie die Losmenge (Losgröße) entsprechend, um so den jeweiligen Bedarf wieder zu erreichen
b.) ist "nur" die Auftragsposition bereits besetzt, so lösen Sie bei dem bereits angelegten Los den Auftrags-Positions-Bezug (auf die Positionsauswahl klicken und mittels Radiergummi den Bezug entfernen). Nun kann dieser Bezug in der Überleitung der internen Bestellung für diese Auftragsposition eingetragen werden und damit das Los angelegt werden.

#### Woher kann man erkennen, dass Lose auf Grund von Terminverschiebungen zusätzlich erstellt werden?
Auch wenn ein Los schon in der Fertigung ist kann es sein, dass auf Grund der Terminsituation und der Einstellung [Toleranz](#Toleranz) das Los nochmals in der Internen Bestellung aufscheint. Diese Zeilen werden farblich (grün) gekennzeichnet und somit hervorgehoben. Prüfen Sie bitte in diesen Fällen mit welchem Maßnahmen Sie die eventuelle Terminthematik oder auch Stückzahlthematik auflösen können.
Die Logik für die Anzeige ist, es gibt für diesen Artikel offene Lose, deren Endetermin NACH dem gewünschten / benötigten Liefertermin der internen Bestellung sind.
Ev. nutzen Sie dafür auch die Anzeige des Los-Endetermines in der Auswahlliste der Lose. Siehe dazu Parameter ENDE_TERMIN_ANSTATT_BEGINN_TERMIN_ANZEIGEN.
Es ist dies eine rein terminliche Steuerung und NICHT Stückzahlabhängig.

#### Kann eine interne Bestellung nur für ein Los erstellt werden?
Ja, wählen Sie hierzu beim Erstellen der internen Bestellung das jeweilige Los durch Klick auf den Button Los ![](Los_interneBestellung.JPG) aus.
Nun werden beim Erstellen der internen Bestellung nur mehr die Stücklisten, berücksichtigt, die im Los-Sollmaterial enthalten sind.

#### Es sollten alle anzulegenden Lose den gleichen Termin haben
Wenn man wirklich alle anzulegenden Lose auf den faktisch gleichen Beginn- und Ende-Termin setzen will, so steht dafür im Menü im Punkt Bearbeiten, der Menüpunkt Beginn/Ende aller Einträge ändern zur Verfügung.

#### Wie werden Hilfsstücklisten bei der Internen Bestellung behandelt?
Die interne Bestellung berücksichtigt auch Hilfsstücklisten und deren Unterstruktur. Diese werden am Ende des Laufes jedoch wieder gelöscht (nur die Hilfsstücklisten). Als Durchlaufzeit einer Hilfsstückliste wird 0 angenommen.

#### Wie werden Terminverschiebungen in der internen Bestellung berücksichtigt ?
Werden (z.B. Auftrags-) Termine verschoben, so ergibt sich daraus unter Umständen, dass Lose früher gefertigt werden sollten. Je nach Einstellung der Toleranzfrist kommt es dadurch dazu, dass Lose vorgeschlagen werden, die vom Grundgedanken her bereits angelegt sind, aber eben aus der terminlichen Situation früher gefertigt werden müssen.
Es wird dies in der internen Bestellung dadurch angezeigt, dass die Zeile des Loses grün hinterlegt dargestellt wird.
![](interne_Bestellung_Termin_ueberschritten.jpg)

Wie aus dem Bild unten ersichtlich, ist zwar für diesen Artikel / diese Stückliste bereits ein Los angelegt. Aufgrund des Endetermines und einer Toleranz von z.B. einem Tag wird festgestellt, dass dieses Los aber deutlich zu spät fertig wird. Daher werden in der internen Bestellung die Zeilen dieses Artikels in Grün hinterlegt.
![](interne_Bestellung_Termin.gif)

#### Wo finde ich zusätzliche Informationen zur Fertigungssteuerung?
Im Journal Bedarfszusammenschau erhalten Sie einen Überblick über die Entwicklung der internen Bestellung um die Entscheidung über die einzuplanende Fertigungssatzgröße zu erleichtern.
![](bedarfszusammenschau.PNG)
Basis hierfür sind die Stücklisten der internen Bestellung und der offenen Lose. Der Kunde kommt aus der Stückliste.
![](bedarfszusammenschau_report.PNG)

Zukunftsdaten: Die Mengen kommen aus der Summe der Fehlmengen und Reservierungen zum Liefertermin
Vergangenheit: Summe der Abbuchungen zum Belegdatum

Vor der Erstellung des Reports können Sie entscheiden, ob Handlagerbewegungen berücksichtigt werden ![](handlagerbewegungen_beruecksichtigen.PNG)
und ob die Zeichnung aus dem Fertigungsbegleitschein (Artikelkommentar) mitgedruckt werden sollen![](mit_Zeichnungen.PNG).

#### Werden Standorte auch in der internen Bestellung berücksichtigt?
Ja. Für Detail [siehe bitte]( {{<relref "/einkauf/bestellung/standorte" >}} ).

#### Wie ist nun der Zusammenhang von Los und Material in Abhängigkeit vom Losstatus?
Da es in seiner Wirkung doch komplex ist, hier eine Zusammenstellung wie sich die Materialsituation in Bezug auf die Beschaffung und den Losstatus auswirkt.

| Losstatus | Aktion | Materialsituation | Fehlmenge | Ablieferung |
| --- |  --- |  --- |  --- |  --- |
| angelegt | Los wurde angelegt | reserviert zum Losbeginntermin, ev. versetzt um den Beginnterminoffset der jeweiligen Position |   |   |
| ausgegeben | Ausgeben des Loses | je nach Stückliste, Materialbuchung bei Ablieferung wird alles vorhandene Material vom Lager ins Los gebucht oder es werden grundsätzlich Fehlmengen erzeugtMaterial das nicht lagernd ist, erzeugt grundsätzlich Fehlmengen. Ev. gibt es vorher die Frage ob, obwohl Fehlmengen entstehen würden das Los ausgegeben werden sollte.Zeitbuchungen sind nicht erlaubt. Wird oft (Parameter AUSGABE_EIGENER_STATUS) direkt in Produktion gesetzt | Fehlmenge |   |
| in Produktion |   | Wie ausgegeben, es dürfen Zeiten darauf gebucht werden | Fehlmenge |   |
| teilabgeliefert | Ablieferungsbuchung ohne Erledigung | Wie in Produktion, es wurden jedoch bereits teilweise Materialsätze produziert und an das Ziel-Lager rückgemeldet | Fehlmenge | abgelieferte Mengedem Lager zugebucht Sollsatzgrößenerfüllung beachten |
| erledigt | Ablieferungsbuchung mit Erledigung oder Los manuell erledigt | vom Lager abgebucht | Sollten noch Fehlmengen im Los gebucht gewesen sein, so sind diese hinfällig, da der Mensch entschieden hat, das brauchen wir nicht mehr. | abgelieferte Menge dem Lager zugebucht |

Es bedeutet dies:
1. ist ein Los angelegt, so ist das Material zum Losbeginntermin reserviert. Siehe dazu bitte auch den Parameter AUTOMATISCHE_ERMITTLUNG_BEGINNTERMINOFFSET.
2. ist ein Los ausgegeben, bis hin zu teilerledigt, so wurde das Material entweder vom Lager entnommen oder es gibt Fehlmengen, die den Bedarf zum Losbeginntermin aufzeigen. Fehlmengen werden beim Wareneingang bevorzugt behandelt. D.h. auch, die Reservierung wandert schlimmstenfalls in die Fehlmenge
3. Die Fehlmenge bleibt solange bis die Ware ins Los gebucht wurde ODER das Los wurde erledigt.
Daher: kann der Bedarf bereits auf Basis der angelegten Lose ermittelt werden.

#### wie kann die Zahl der angelegten Lose reduziert werden?
Insbesondere in Unternehmen, welche per EDI täglich andere Bedarfe ihrer Kunden mitgeteilt bekommen, bewirkt die oben beschriebene Logik, dass es immer wieder zu kleinen "Nachfüll-Losen" kommt. Das führt dann soweit, dass eine fast unübersichtliche Anzahl von angelegten Losen vorhanden ist und man mit dem manuellen Verdichten nicht nach kommt.
Um dieses Verhalten zu reduzieren, gibt es den Parameter INTERNE_BESTELLUNG_ANGELEGTE_ENTFERNEN. Stellen Sie diesen auf 1, so bewirkt dies, dass vor dem eigentlichen Start der Berechnung des Bestellvorschlages, alle Lose die im Status angelegt oder storniert sind, vollständig aus Ihrem **Kieselstein ERP** entfernt werden.
Der Gedankengang dahinter ist, dass Lose die echt produziert werden im Status Ausgegeben, in Produktion bzw. Teilerledigt sind. Diese werden von dieser Logik nicht verändert.
Bitte beachten Sie, dass für den Fall dass auf einem angelegten Los Maschinenzeiten gebucht sein sollten, was nur durch manuelle Änderung des Losstatuses von Ausgegeben wieder in Angelegt möglich ist, diese Buchungen entfernt werden müssen. Ähnliches gilt für die Buchungen Ihrer Mitarbeiter auf Loszeiten. Hier würde gegebenenfalls eine Meldung, Los nicht mehr vorhanden ausgegeben werden.

Lose die im Status Angelegt bzw. Storniert sind, werden nicht automatisch beim Lauf der internen Bestellung entfernt.<br>
Anstatt dessen steht nun im unteren Reiter interne Bestellung im Menü die Funktion Bearbeiten, Angelegte/Stornierte Lose entfernen zur Verfügung, welcher die angelegten bzw. stornierten Lose endgültig entfernt.

#### Kann die Berechnung der Termine verändert werden?
Grundsätzlich ist die Berechnung des Endetermines eines Loses in der Form realisiert, dass der Auftragsliefertermin abzüglich der Kundenlieferdauer den gewünschten Los-Ende-Termin ergibt. Davon wird noch die default Durchlaufzeit der Stückliste abgezogen und so der Beginntermin errechnet. Ist dieser Beginntermin nun vor heute, so wird der Beginntermin auf heute gesetzt und der Endetermin durch hinzuzählen der default Durchlaufzeit ermittelt, welche dann NACH dem gewünschten Ausliefertermin an den Kunden sein wirde.
Ist nun die Unternehmensphilosophie die, dass der Kundentermin auf jeden Falle zu halten ist, so kann durch Aktivierung des Parameters INTERNE_BESTELLUNG_TERMINE_BELASSEN =1 so verändert werden, dass damit die Termine zusammengedrückt werden. D.h. es wird der Endetermin wie beschrieben errechnet. Ist dieser vor heute, dem Berechnungsdatum der internen Bestellung, so wird er auf heute gesetzt. Vom ermittelten Endetermin wird die default Durchlaufzeit abgezogen und so der Beginntermin ermittelt. Wäre dieser vor heute, so wird er ebenfalls auf heute gesetzt. Damit ergibt sich eventuell, dass ein Los sofort zu produzieren ist bzw. kann sich damit auch ergeben, dass das Los innerhalb des Tages zu produzieren und zu liefern ist.

#### Was bedeutet Freigabe in der internen Bestellung?
Wenn in Ihrer **Kieselstein ERP** Installation auch das Modul Stücklistenfreigabe aktiviert ist, so erscheint in der Internen Bestellung auch die Spalte Freigabe ![](Stkl_Freigabe.gif). Damit wird mit dem Haken signalisiert, dass die Stückliste bereits freigegeben ist. Das bedeutet umgekehrt auch, dass Sie sich um die Freigabe der noch nicht freigegebenen Stücklisten kümmern sollten um diese entsprechend produzieren zu dürfen. Zum Thema Stücklisten-Freigabe [siehe](../Stueckliste/index.htm#Freigabe).

#### Wann kommt eine Stückliste in die internen Bestellung?
Solange ein Bedarf für die Stückliste gegeben ist, der nicht durch Lagerstand, offene Fertigungsaufträge (oder auch externe Bestellungen) gedeckt wird.
Zusätzlich ist dieses Verhalten noch terminlich anhand der Toleranzfrist gesteuert.
Ein Bedarf entsteht, wenn eine Auftragsbestätigung, ein Lagermindeststand oder ein Los oder auch ein Forecastauftrag gegeben ist. Diese Position haben auch alle Termine zu denen das fertige Produkt zur Verfügung stehen sollte.

#### Der angezeigte Bedarf ist nicht erklärbar?
Gerade in Unternehmen die komplexe mehrstufige Stücklistenstrukturen haben, ergibt sich in der internen Bestellung immer wieder die Situation, dass ein Los zur Fertigung vorgeschlagen wird, für das es auf den ersten und auch den zweiten Blick keine Erklärung gibt, warum man den diese Baugruppe fertigen sollte.
Um dies für Sie transparenter dazustellen, gibt es ab Version 13411 auf der Spalte des Artikels einen Tooltip, mit dem die hierarchische Begründung angezeigt wird.
Der eigentliche Hintergrund ist, dass es einen Bedarf aus einer übergeordneten Stückliste gibt, der nicht durch die bereits angelegten Lose erfüllt werden kann.
Es kann dies aber auch ein Hinweis auf Stücklistenfehler oder ähnliches sein.
Bitte beachten Sie, dass für diese Analysen KEIN Verdichten der internen Bestellung gemacht werden darf und es muss der bestehende interne Bestellvorschlag gelöscht werden.
![](IntBS1.gif)

| Meldung | Bedeutung |
| --- | --- |
| ![](IntBS2.gif) | Für die ausgewählte Stücklistenzeile wurde der Lagermindeststand unterschritten | 
| ![](IntBS3.gif) | Für die ausgewählte Stücklistenzeile wird aufgrund der Hierarchieauflösung des übergeordneten Loses eine weitere Baugruppe benötigt.
| ![](IntBS4.gif) | Für die ausgewählte Stücklistenzeile wird aufgrund der Hierarchieauflösung des Kunden-Auftrags ein kompletter Baugruppensatz benötigt. Der Artikel ist erst in der vierten Unterebene zu finden.

<a name="Losnummer Auftragsbezogen"></a>

#### Wie verhält sich die Losnummer, wenn die Losnummer auftragsbezogen ist?
Ist der Parameter LOSNUMMER_AUFTRAGSBEZOGEN auf 1 = aktiviert gestellt, so wird bei der Erzeugung der Losnummer als Basis die Auftragsnummer verwendet.
Danach werden die jeweiligen Auftragspositionen, die eigengefertigte Stücklisten sind, mit deren Positionsnummer angeführt. Somit bedeutet z.B. 21/001234-002, dass diese ein Los für den Auftrag 21/001234 ist und aus diesem Auftrag die Position 002 gefertigt werden sollte.
Wenn sich nun aus dieser Stückliste Unterlose ergeben, so werden diese mit der gleichen beginnenden Losnummer aber von 999 nach unten / vorne zählend angelegt.

Wie wird nun vorgegangen, wenn Lose storniert werden?
Wird ein Los storniert, so erscheint die Frage nach Auftragspositionsbezug entfernen. Bestätigen Sie diese mit Ja, wenn daran anschließend auch die Auftragsposition gelöscht werden sollte. Bitte beachten Sie dabei zusätzlich, dass sich dann zwar die in den Los Kopfdaten angezeigte Positionsnummer ändert, die Losnummer aber gleich belassen wird.
Wird nun z.B. bei einem weiteren Durchlauf der internen Bestellung, festgestellt, dass für den Auftrag weitere Kopflose angelegt werden sollten, die sich ergebende Losnummer durch das stornierte Los bereits belegt ist, so wird ausnahmsweise auch dieses Kopflos in seiner Nummerierung der 999 Logik unterworfen, also in die nächste freie Positionsnummer von 999 absteigend eingereiht.