---
categories: ["Nachkalkulation"]
tags: ["Nachkalkulation", "Vorkalkulation"]
title: "Kalkulation"
linkTitle: "Vor- Nachkalkulation"
weight: 20
date: 2023-01-31
description: >
 Punkte zur Nachkalkulation
---
Beschreibung wie man zu einer durchgängigen Darstellung der Vor und Nachkalkulationen kommt,
unter Berücksichtigung der Besonderheiten der verschiedenen Module.

Es beginnt bei der Angebotsschnellerfassung, welche nur zwei Aufschlagsfaktoren hat
bis hin zur Auftragsnachkalkulation.

Welche Module sind nun bei der Vor- und Nachkalkulation beteiligt
- Angebotsschnellerfassung
- Angebotsstückliste(n-Druck)
- Angebotsvorkalkulation
- Stücklisten Gesamtkalkulation
- Los Nachkalkulation
- Los Gesamtkalkulation
- Auftrags Nachkalkulation
- Auftrags Statistik

Da der Arbeitszeitgemeinkostenfaktor in der Stkl-Gesamtkalkulation nur auf die
Menschzeit wirkt, bei den anderen Auswertungen Mensch und Maschinen von Kosten
her NICHT getrennt sind, ist der dringende Rat, den Arbeitszeitgemeinkostenfaktor
auf 0 zu belassen, den allgemeinen Gemeinkostenfaktor sowieso,
und dafür den Fertigungsgemeinkostenfaktor zu verwenden.
Versprochen ich zeichne da ein schönes Bild.
Wichtig die Maschinen detailliert auszufüllen um so die echten Maschinenkosten
zu haben und natürlich auch den Gehaltskostenfaktor Schuhe, Schreibtisch, Heizung,
anteilige Miete mit einzurechnen.
Sollten nur Menschzeiten verwendet werden, ist es additiv. Arbeitszeitgemeinkostenfaktor
und oben drauf der Fertigungsgemeinkostenfaktor.

Siehe auch Mengendruck der Angebotsstückliste, mit dem die Wirkung der Aufschlagsfaktoren
für die Ermittlung der Verkaufspreise dargestellt wird.

Siehe auch Calc-Sheet mit den Ebenen der DB Berechnungen
