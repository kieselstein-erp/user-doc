---
title: "Trumpf Anbindung"
linkTitle: "Tops"
categories: ["Trumpf"]
tags: ["Tops"]
weight: 1000
description: >
  Geplant: Rücklesen der Trumpf-Daten
---
Trumpf-Tops Anbindung
=====================

Von den **Kieselstein ERP** Anwendern werden auch Laserschneidemaschinen der Firma Trumpf eingesetzt. Gerade bei vielen zu fertigenden verschiedenen Aufträgen ist die Ermittlung der Gestehungskosten eines Blechteiles sehr mühsam. Andererseits wird vom Schachtler diese Arbeit sowieso erledigt, da er ja die einzelnen Komponenten auf der Blechplatte optimal plazieren muss. Dafür wird vom Schachtler das von Firma Trumpf gelieferte Programm Tops (TruTops Laser) verwendet. Die Daten dieses Programms werden üblicherweise in einem Verzeichnis je Schachtelauftrag zentral abgelegt. Bitte achten Sie darauf, dass das in **Kieselstein ERP** als Einlesepfad definierte Verzeichnis mit dem Ablageverzeichnis übereinstimmt.
In jedem Schachtelauftrag sind die Daten der gelaserten Blechteile enthalten.
Aus diesen Dateien können wiederum die Daten für die Gestehungspreisberechnung ermittelt werden.
Hinweis: Bitte beachten Sie auch die [Trumpf FAB]( {{<relref "/fertigung/losverwaltung/trumpf_fab">}} ) Anbindung.

**<u>Für die Kalkulationsmethode 1 werden folgende Daten ermittelt:</u>**
- Laserzeit
- Materialart
- Materialgewicht

In der Tops Schnittstelle können die Kosten für die Laserzeit pro Stunde hinterlegt werden.
Für das Material wird über die Kurzbezeichnung der aktuelle Gestehungspreis verwendet. Um auf die Gewichtsangabe zu kommen wird dazu zusätzlich der Gewichtsfaktor (aus dem Artikeldetail des Bleches) berücksichtigt.

Eine zusätzliche Forderung an diese Schnittstelle ist, dass auch eine spätere Nachkalkulation im Los, bei unter Umständen geänderten Preisen, auch aus damaliger Sicht, richtig sein muss.

Wir haben dies durch die Lagerbewegung der gelaserten Teile gelöst.
Dafür ist eine eigene Artikelklasse erforderlich. Diese muss als Tops definiert sein.
Wir nun am jeweiligen Arbeitsplatz die Losnummer des Fertigungsscheines gescannt, so werden automatisch die Artikel der Tops-Artikelklasse in der Sollmenge dem Lager zugebucht.
Beim nächsten Preisaktualisierungslauf wird der Gestehungspreis der lagernden Tops Teile auf den aktuell errechneten Wert gesetzt. Werden nun durch die Losablieferung die Tops-Teile entnommen, so greift der letztgültige Gestehungspreis, welcher in der Abbuchung protokolliert wird.
Somit liefern auch spätere Nachkalkulationen die gleichen Ergebnisse.

Der Import der Tops Daten muss in einem eigenen Lauf von einem eigenen Client angestoßen werden. Bitte achten Sie darauf dass die Daten ausreichend oft, aber nicht zu oft aktualisiert werden ( z.B. täglich / stündlich).

**Hinweis:**

Die Preisänderungen werden intern protokolliert und können für Optimierungszwecke o.ä. jederzeit ausgewertet werden.

**Hinweis:**

Da die eigen gefertigten TopsTeile keine Stücklisten sind, würden diese im Bestellvorschlag aufscheinen. Wir haben daher den Bestellvorschlag um die Funktionalität erweitert, dass Artikel bei deren Artikelklasse TOPS angehakt ist, nicht in den Bestellvorschlag mit aufgenommen werden.

**<u>Variante 2 der Tops-Kalkulation:</u>**

Da von Tops das Gewicht für den resultierenden Teil errechnet wird, kann dieser Wert nur bedingt für die Ermittlung der Gestehungskosten verwendet werden. In manchen Anwendungen sollte vielmehr das umschließende Rechteck mit einem Laserabstand verwendet werden und automatisch der entsprechende Verschnitt errechnet werden.
Die Verschnittberechnung erfolgt in der Form, dass das umschließende Rechteck in die Abmessungen des definierten Materialartikels eingepasst wird. Es erfolgt eine einfache Einpassung, so dass ermittelt wird, in welcher Lage (X oder Y orientiert) die größere Stückzahl aus dem Material gewonnen werden kann. Diese Stückzahl wird für die Berechnung des Gestehungspreises des Materials verwendet. D.h. es werden die Kosten der Tafel (des Materials) durch die Stückzahl dividiert und daraus die Materialkosten für ein Stück ermittelt.
Die Abmessungen der Tafel (des Materials) werden aus den in **Kieselstein ERP** hinterlegten Breite x Tiefe (Technik) genommen.
Zusätzlich werden zu Ihrer Information die Werte des umschließenden Rechtecks inkl. Höhe (= Dicke des Ausgangsmaterials), das Gewicht und die Laserzeit in das resultierende Teil eingetragen.
Die weiteren Werte werden wie unter Kalkulation 1 beschrieben verwendet.

Ablauf der richtigen Erfassung / Buchung / Berechnung der Einstands/Gestehungspreise

A1.) Der Schachtler plaziert die Teile (Datentechnisch). Dadurch werden die entsprechenden HTML / XML Daten erzeugt.

A2.) Der Nachtlauf (Automatik Lauf) übernimmt die errechneten Preise der Laserteile in den QuickLagerstand und somit in den QuickGestehungspreis des jeweiligen Artikels.

B1.) In der Losverwaltung, Scan der Losnummer.

B2.) Es werden alle Soll-Material-Artikel des Loses bearbeitet, bei deren Artikelklasse Tops angehakt ist.

B3.) Für diese Artikel werden die offenen Mengen (Losgröße - bereits abgeliefert) errechnet und daraus die offene Satzgröße bestimmt.

B4.) Dem Anwender wird die offene Satzgröße vorgeschlagen, er kann diese entsprechend der tatsächlich gelaserten Satz-Menge korrigieren.

B5.) Diese Artikel werden mit den Mengen, unter Berücksichtigung der Satzgrößen, auf das Hauptlager zugebucht. Als Einstandspreis wird der aktuelle QuickGestehungspreis (A2) verwendet.

B6.) Durch die Losablieferungsbuchung werden diese Artikel wieder vom Lager entnommen. Mit diesem Ablauf wird erreicht, dass die Gestehungspreise anhand der jeweils aktuell hinterlegten Materialkosten eingefroren werden und somit auch bei späteren Nachkalkulationen erhalten bleiben.

Berechnung der Größe und des Gestehungspreises des verbrauchten Materials

Die Größe des verbrauchten Materials wird aus den TrumpfTops Daten genommen. Dazu wird der eingestellte Laser-Rand addiert und die sich ergebende Fläche wird als Fläche des hinterlegten Materials für die Gestehungspreisberechnung herangezogen. Ergibt sich nun, dass die inkl. Rändern errechnete Fläche kleiner ist als eine ganze Tafel, so wird die gesamte Tafel als Verbrauch angenommen.
Die Basis für die Berechnung der Gestehungspreise ist das eingesetzte Material - bitte achten Sie hier darauf, dass zum Beispiel bei Bestellungen die Zubuchungen und deren Preise korrekt sind. Falls Sie Abweichungen der Gestehungspreise der Trumpf-Tops Ablieferung feststellen, überprüfen Sie die Zubuchungen des Basismaterials.

## Verwendung von Trumpf Job
Es ist die Anbindung mittels JobExport geplant. D.h. es werden anstatt der HTML Dateien die JobExp??.XML Dateien für die Ermittlung der Kosten der gelaserten Teile verwendet.
Für die Ermittlung des verwendeten Materials wird der XML Tag SheetIdentNo verwendet. D.h. die unter diesem Tag angegebene Bezeichnung muss in der Kurzbezeichnung des Artikels definiert sein, dessen Gestehungspreis für die Ermittlung der Kosten des geschnittenen Artikels verwendet wird.
Um auch die von Trumpf generierten Rest-Tafeln verwenden zu können, wurde die Suche nach den Tafeln so erweitert, dass bei den Resttafeln folgende Logik greift:
Wird keine exakte Zuordnung des SheetIdentNo in der Artikelkurzbezeichnung gefunden, so wird davon ausgegangen, dass der erste Teil der zu findenden Kurzbezeichnung das Material der Tafel beschreibt und der durch ein Minus getrennte zweite Teil die verwendeten Abmessungen.
Wird also die exakte Kurzbezeichnung, der verwendeten Tafel, nicht gefunden, so werden die Abmessungen abgetrennt und nur nach dem Material gesucht. Werden hier mehrere Artikel gefunden, so wird derjenige mit der größten Fläche, das was also dann vermutlich ein Großformat sein wird, verwendet.
Weiters wird die importierte Datei in das Unterverzeichnis OLD verschoben.