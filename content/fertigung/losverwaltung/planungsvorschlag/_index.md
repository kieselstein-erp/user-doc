---
title: "Planungsvorschlag"
linkTitle: "Planungsvorschlag"
categories: ["Produktionsvorschlag"]
tags: ["Produktionsvorschlag"]
weight: 750
description: >
  Arbeitsgänge mit Maschinen seriell planen
---
Im **Kieselstein ERP** steht mit dem Planungsvorschlag, der auch Produktionsvorschlag genannt wird, ein praktisches Werkzeug für die Planung der Abarbeitung der Arbeitsgänge je Maschine, basierend auf den jeweiligen Maschinenzeitmodellen zur Verfügung.

Der Grundgedanke ist, dass eine bestimmte Maschine den gesamten Fertigungsprozess dieses Loses definiert. D.h. z.B. für die Elektronik Fertigung, dass die eine SMD-Linie immer optimal ausgelastet sein muss. Alle anderen vor- und nachgelagerten Arbeitsgänge richten sich nach diesem Arbeitsgang. Man könnte auch sagen, bei den vor- und nachgelagerten Arbeitsgängen sind immer ausreichend Resourcen zur Verfügung um dies dann zeitgerecht abarbeiten zu können. Siehe dazu auch die Gedanken nach der TOC (theory of constraints).<br>
Von der Idee her, ist dieses Tool für die kurzfristige Planung gedacht, also für die nächsten 1-2 Wochen.

Du findest dieses Tool, welches aktiviert werden muss, im Modul Los, unterer Reiter Planungsvorschlag.

## Erstellen eines neuen Planungsvorschlages
Durch Klick auf ![](planungsvorschlag_erzeugen.png) Planungsvorschlag erzeugen, wird der Dialog für die Berechnung der Planungsdaten aufgerufen.<br>
Hier ![](planungsvorschlag_erzeugen_2.png) müssen folgende Felder befüllt werden.

| Feld | Bedeutung |
| --- | --- |
| Planungsbeginn | Die Berechnung startet mit diesem Datum |
| Minimaler Losstatus | Damit kannst du steuern, welche Lose berücksichtigt werden. So macht es auf einer SMD Linie z.B. keinen Sinn, wenn Lose die noch nicht vollständig sind, auf die Linie gegeben werden. Da bei den elektronischen Baugruppen immer auch bedrahtete (THT) Bauteile berücksichtigt sind, wird für diese Betrachtung die Artikelklasse herangezogen. Siehe Material komplett bzw. unten |
| Maximales Los Beginn Datum | Damit wird die Berechnung auf die Lose eingeschränkt, deren Beginndatum vor bzw. diesem Datum entsprechen |
| Material komplett | für alle Artikel eines Loses sind für die Artikel der Artikelklasse mit Tops gesetzt ausreichend Material ins Los gebucht.|
| Maschinengruppe | es werden nur Arbeitsgänge berücksichtigt, die eine Zuordnung auf Maschinen dieser Maschinengruppe haben |

Fehlermeldung:
- ![](fehlendes_maschinenzeitmodell.png)<br>
Erscheint die Fehlermeldung verfügbare Stunden konnten nicht ermittelt werden, so ist für eine der Maschinen, die in der Maschinengruppe definiert sind und die in den ermittelten Arbeitsgängen enthalten sind, kein gütliges Maschinenzeitmodell enthalten. Bitte dies im Modul Zeiterfassung, unterer Reiter Maschinen, oberer Reiter Zeitmodell nachtragen. Maschinenzeitmodelle haben einen eigenen unterern Reiter im Modul Zeiterfassung.

### Ablauf der Berechnung
Die Berechnung erfolgt immer je Maschine.<br>
Es werden die bestehenden Arbeitsgänge der Maschinen der gewählten Maschinengruppe nach AG-Beginn und MS-Versatz je Maschine eingereiht. AG-Beginn mit gleichem Zeitpunkt werden nach ihrer Losnummer gereiht.<br>
Nun wird das gewünschte Beginndatum (Planungsbeginn) angewandt und die nachfolgenden Arbeitsgang-Beginn anhand der verfügbaren Stunden des jeweiligen Tages der jeweiligen Maschine anhand des Maschinenzeitmodells errechnet. Davon ausgehend wird der Arbeitsgangbeginn des nächsten Arbeitsgang ermittelt und so fort bis alle Arbeitsgänge eingeplant sind.<br>
Dies wird für jede Maschine der Maschinengruppe durchgeführt.

Bitte beachte auch, dass Arbeitsgänge die als Fertig gekennzeichnet sind, nicht mehr in die Berechnung aufgenommen werden.

![](planungsvorschlag.png)

Damit erhält man einen Vorschlag in welcher Reihenfolge die Arbeitsgänge abgearbeitet werden sollten und wann denn der jeweils nächste Arbeitsgang auf dieser Maschinen starten sollte.<br>
Die default Sortierung ist nach Datum, Maschine, Reihung.<br>
Mit den blauen Pfeilen können die Arbeitsgänge den Bedürfnissen angepasst werden, wodurch sofort die Berechnung der Arbeitsgang-Beginn-Datum aktualilsiert wird.

Im unteren Bereich befindet sich das Detail-Fenster in dem auch die Rüst- und Stückzeiten sowie die Maschine geändert werden können. Weiters kann auch hier die Reihung manuell vorgegeben werden.

Bitte beachte, dass dies **<u>nur</u>** eine Simulation ist, solange bis die Zeiten durch Klick auf ![](planungsvorschlag_zurueckschreiben.png) Planungsvorschlag Termine auf Lose übernehmen in die Lose zurückgeschrieben werden.

### Zurückschreiben
Bitte beachte, dass beim Zurückschreiben in der Reihenfolge der Maschinen und dann der Reihenfolgen zurückgeschrieben wird. Bei jedem Zurückschreiben werden die vor und nach dem Arbeitsgang liegenden Arbeitsgangbeginntermine neu berechnet. Das bedeutet auch, sollten zwei hintereinander liegende Arbeitsgänge des gleichen Loses im Produktionsvorschlag enthalten sein, gewinnt der später berechnete, also der mit der höheren Maschinennummer und der späteren Reihung, wodurch sich der zuvorberechnete Beginntermin des Arbeitsganges wieder verändern wird.

## Dinge die da noch reinspielen
- der Betriebskalender
- die Maschinenzeitmodelle
- die Zuordnung der Maschinen zu den Maschinenzeitmodellen
- die Zuordnung der Maschinen zu den Maschinengruppen
- das Firmenzeitmodell und damit AUTOMATISCHE_ERMITTLUNG_AG_BEGINN
Hier liegt der Trick darin, dass man für die Abbildung des ev. Dreischicht-Betriebes ein eigenes Firmenzeitmodell macht, das mit z.B. 100Std pro Woche definiert ist.
- AUSGABE_EIGENER_STATUS

Indirekt spielen auch rein
- default Durchlaufzeit der Stückliste um die interne Bestellung, insbesondere bei Unterbaugruppen entsprechend richtig zu haben. Siehe dazu auch automtaische Berechnung der default Durchlaufzeit (derzeit ein Script)
- für die Ermittlung des Beginntermines der Lose über den internen Bestellvorschlag natürlich die Liefertermine der Auftragspositionen und die Lieferdauer ab die Kunden.

## Ändern von Arbeitsplänen während des Planungsvorschlages
Solange Arbeitsgänge im Planungsvorschlag referenziert (angezeigt) werden, können diese nur über den Planungsvorschlag geändert werden.<br>
Will man einen Arbeitsgang vom Los aus entfernen, so erscheint die Fehlermeldung ![](Arbeitsgang__im_produnktionsvorschlag_aus_los_loeschen.png).
Wenn nun tatsächlich dieser Arbeitsgang entfernt werden sollte, muss dieser aus dem Produktionsvorschlag herausgelöscht werden. D.h. bitte in den Produktionsvorschlag auf exakt diesen Arbeitsgang wechseln und mit dem ![](Einzelnen_AG_loeschen.png) Löschenbuttung aus dem unteren Detail diesen Arbeitsgang entfernen.

Sollte der gesamte Planungsvorschlag entfernt werden müssen, er kann ja jederzeit wieder neu errechnet werden, allerdings mit den in den Losen gespeicherten Daten, so kann aus der Auswahlliste das Löschen verwendet werden. Dafür muss die Sicherheitsabfrage ![](Planungsvorschlaege_loeschen.png) entsprechend mit Ja beantwortet werden.

## Direkt auf den Arbeitsgang im Los wechseln
In der Übersicht steht auch ![](Goto_AG.png) der Goto Arbeitsgang zur Verfügung. Damit kann direkt in die Zeitdaten auf den einen Arbeitsgang dieses Loses gesprungen werden.

Bitte beachte, dass Änderungen die während des Planungsvorschlages im Los gemacht werden, den Planungsvorschlag nicht verändern.

Werden Daten zurückgeschrieben, so werden die des Planungsvorschlages verwendet und überschreiben damit gegebenenfalls die anderen Werte.