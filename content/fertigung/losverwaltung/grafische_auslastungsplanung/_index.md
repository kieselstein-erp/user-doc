---
title: "Auslastungsplanung"
linkTitle: "Auslastungsplanung"
categories: ["Auslastungsplanung"]
tags: ["Auslastungsplanung"]
weight: 900
description: >
  Auslastungsplanung, Anforderungsbeschreibung
---
grafische Auslastungsplanung
============================

Diese Beschreibung dient einer Vorabinformation wie eine grafische Auslastungsplanung erfolgen könnte. Es ist nicht geplant dies direkt im **Kieselstein ERP** zur Verfügung zu stellen. Da jedoch doch einige Mitglieder der **Kieselstein ERP eG** ein derartiges Werkzeug benötigen, freuen wir uns auf dein Beteiligung um
- entweder so ein Werkzeug zu programmieren oder
- gemeinsam ein optimales Tool zu finden, 
mit dem die Forderungen abgedeckt werden können.

Es ist auch zu prüfen, ob dies vollständig im Swing-Client integriert sein sollte, oder ob dafür ein eigener grafischer Client, mit allen Vor- und Nachteilen eingesetzt werden sollte.

Zusätzlich neben den vielen zur Planung der Fertigung direkt in **Kieselstein ERP** integrierten Werkzeugen steht die grafische Auslastungsplanung (Kapazitätsplanung) zur Steuerung der Fertigung / Produktion zur Verfügung. Damit können die Arbeitsgänge der Fertigungsaufträge / Lose direkt im Browser mit der Maus geplant / verteilt / eingeteilt werden.

Die Geschwindigkeit von Datenabruf, Aufbau der Grafiken und dem zurückspeichern der geänderten Arbeitsgänge hängt von der Leistungsfähigkeit der beteiligten Rechner ab. Für eine vernünftige Darstellung raten wir Bildschirme mit mindestens 1920 x 1080 zu verwenden. Eine Darstellung auf kleineren Bildschirmen ist durchaus möglich, ob damit die Planungsarbeit machbar ist, muss die Praxis zeigen.

Bitte beachten Sie, dass in der Regel das Verschieben der Arbeitsgänge, also der eigentliche Planungsvorgang nur an einem Arbeitsplatz, von einer Person, gemacht werden sollte.
Ein gemeinsamer Planungsvorgang, in dem z.B. jeder Mitarbeiter nur seine Maschinengruppe behandelt ist vorstellbar.

Wie funktioniert die Auslastungsplanung?

Beim Start werden nach der Anmeldung die Daten per WebService Schnittstelle vom **Kieselstein ERP** Server geholt und mittels Browser-App dargestellt. Nun können Sie die Arbeitsgänge mit der Maus sowohl zeitlich als auch zwischen den Maschinen verschieben.
Wichtig: Ab dem Abruf der Daten findet aus Geschwindigkeitsgründen keine Interaktion mehr mit dem **Kieselstein ERP** Server statt. D.h. wenn in der Zwischenzeit z.B. Lose storniert oder erledigt werden oder neue Lose / Arbeitsgänge hinzukommen, so wird dies erst nach dem nächsten Refresh entsprechend dargestellt. Beim Speichern der verschobenen Arbeitsgangtermine werden eventuell nicht mehr änderbare Daten erkannt und es wird eine entsprechende Hinweisliste ausgegeben. In diesem Falle gehen wir davon aus, dass die von Ihnen durchgeführte Planung, z.B. durch einen Refresh überprüft wird.

Start der grafischen Auslastungsplanung

Wählen Sie Ihren bevorzugten HTML5 Browser und geben Sie in die Adressleiste <http://Kieselstein_ERP_SERVER:8280/hvcapacity/> ein. Ersetzen Sie bitte Kieselstein_ERP_SERVER durch den Namen des bei Ihnen installierten **Kieselstein ERP** Servers.
Nun erscheint der Anmeldedialog. Geben Sie bitte Ihren Benutzernamen und Kennwort ein und klicken Sie anschließend auf anmelden.
![](Grafische_Auslastung1.gif)

Nun werden die Daten geladen, was durch den Fortschrittsbalken unten angezeigt wird.

Je nach Menge der zu verarbeitenden Daten erscheint nach einigen Sekunden / Minuten die grafische Auslastungsplanung.

Die Bedienelemente der grafischen Auslastungsplanung

![](Grafische_Auslastung3.gif)

Im oberen Bereich können das Datum und der zu bearbeitende Zeitraum gewählt werden.
Mit Speichern können die geänderten Arbeitsgänge in Ihre **Kieselstein ERP** Datenbank zurückgeschrieben werden, bzw. mit Rückgängig können einzeln die verschobenen Arbeitsgänge wieder an ihren ursprünglichen Platz zurückgesetzt werden.
Durch einen Klick auf Account (rechts oben) kommen Sie in den Abmelde-Dialog. Bitte beachten Sie, dass die Anmeldung mit der grafischen Auslastung so wie jeder **Kieselstein ERP** Client als ein Benutzer in Ihrem Benutzerzähler zählt. Melden Sie sich daher immer entsprechend ab, wenn die grafische Auslastungsplanung nicht benötigt wird.

Beim Start der grafischen Auslastung wird das aktuelle Datum vorgeschlagen. Um das Startdatum zu ändern, klicken Sie bitte mit der Maus in das Datumsfeld und wählen Sie das gewünschte Datum. Wenn man eine Übersicht über z.B. die in der letzten Woche noch nicht vollständig abgeschlossenen Arbeitsgänge bekommen möchte, so kann ein Datum in der Vergangenheit durchaus von Vorteil sein. Siehe dazu bitte auch unten Farben.

Verschieben der Arbeitsgänge

Um einen Arbeitsgang zu verschieben, klicken Sie diesen bitte mit der linken Maustaste an und schieben Sie diesen auf die neue Position.
Um einen Arbeitsgang auf eine andere Maschine zu verschieben, ziehen Sie den Arbeitsgang in die jeweilige Zeile der Maschine. 
Ob die Produktion mit der gewählten Maschine fachlich möglich ist, wird von Ihnen entschieden. Hier gibt es keine programmierten Einschränkungen.
Sollte ein Arbeitsgang am Beginn des jeweiligen Tages eingereiht werden, so drücken Sie bitte vor dem Fallen-lassen (dem Loslassen der Maustaste) die Großschreib-Taste (Shift).

Nach dem Verschieben erscheint meistens folgendes Fenster:
![](Auslastung_Nachfolger_mitverschieben.jpg)
Damit kann definiert werden, wie vor allem die Nachfolger mit-verschoben werden sollten.
D.h. um die nach dem soeben verschobenen Arbeitsgang gereihten Arbeitsgänge dieses Loses seriell hinter dem soeben verschobenen Arbeitsgang einzureihen, haken Sie bitte Nachfolger ebenfalls verschieben an.
Ist der Arbeitsgang der letzte Arbeitsgang des Loses, kann das Losendedatum auf das geplante Ende des Arbeitsganges gesetzt werden. Dafür haken Sie Das Losendedatum setzen an.
Ist der Arbeitsgang der erste Arbeitsgang des Loses, kann das Losbeginndatum auf den geplanten Beginn des Arbeitsganges gesetzt werden. Dafür haken Sie Das Losbeginndatum setzen an.
Wichtig: Die Verschiebung des Losbeginndatums hat wiederum Auswirkungen auf die Beschaffung des mit dem Los reservierten Materials

Werden die Maschinenzeitmodelle berücksichtigt?

Ja. D.h. es wird ein Tag immer mit der gleichen Breite dargestellt. Die tatsächlich verfügbaren Produktions-/Maschinenstunden für den jeweiligen Kalendertag hängen für jede Maschine vom hinterlegten Maschinenzeitmodell ab. [Siehe dazu auch](../Zeiterfassung/Maschinenzeiterfassung.htm#Maschinenzeitmodell). Das bedeutet, dass die Balkenbreite des Arbeitsganges massiv variieren kann, da sich diese Breite ja aus den verfügbaren Stunden ergibt.
Hinweis: Für den Bereich der noch nicht Maschinen zugeordneten Arbeitsgänge, wird ein Zeitmodell von 8Std je Kalendertag, inkl. Sa, So, Feiertag angenommen, da diese Arbeitsgänge eingeplant werden müssen.

Werden von **Kieselstein ERP** fachlich logische Zusammenhänge zwischen den Arbeitsgängen geprüft?

Bei der grafischen Auslastungsplanung gehen wir davon aus, dass ein entsprechend geschulter und fachlich kompetenter Mitarbeiter diese Arbeiten durchführt. Da es sehr viele Kombinationsmöglichkeiten in der Reihenfolge von Arbeitsgängen gibt, haben wir keine wie immer gearteten Verriegelungen vorgenommen. Es werden nur sehr grobe Überschreitungen von Terminen angezeigt (siehe Farben).

Markieren aller Arbeitsgänge eines Loses

Klicken Sie dazu einen der Arbeitsgänge an und klicken Sie dann auf die angezeigte Losnummer. Nun werden die entsprechenden Arbeitsgänge in einer etwas dunkleren Farbe dargestellt.
![](Auslastungsplanung_Los_markiert.jpg)
Zusätzlich werden die Verbindungslinien zwischen den Arbeitsgängen dargestellt und der Beginn des ersten Arbeitsgangs mit einem grünen Rechteck und das Ende des letzten Arbeitsgangs mit einem roten Rechteck dargestellt.
Tipp: Manchmal ist es für die klarere Darstellung sinnvoll, dass obwohl die Arbeiten eigentlich ohne wirklicher Maschine durchgeführt werden, trotzdem eine (fiktive) Maschine bzw. den Arbeitsplatz zu hinterlegen. Damit ist die Planung wesentlich klarer / leichter.

| Farben | Bedeutung für die Arbeitsgänge |
| --- | --- |
| grün | normale Darstellung |
| blau | dieser Arbeitsgang hätte in der Vergangenheit, vor dem Beginn der Darstellung bereits begonnen werden sollen|
| rot | dieser Arbeitsgang überschreitet den Finaltermin des zugehörigen Kundenauftrages |
| schraffiert | dies bedeutet, dass die Sollarbeitszeit bereits überschritten ist. Die Länge des schraffierten Balkens zeigt an um wieviel die Sollzeit bereits überschritten ist|
| gepunktete Umrandung | dies bedeutet, dass der Arbeitsgang gegenüber dem original Zeitpunkt bereits verschoben wurde. D.h. wird die Planung zurück-gespeichert, so wird der Starttermin des Arbeitsganges verändert |

Anzeige der Maschinengruppen

am linken Rand wird die interne Nummer der Maschinengruppe angezeigt. Um die Bezeichnung zu sehen, bewegen Sie bitte die Maus direkt auf die interne Nummer und warten kurz. Es wird der Name als Tooltip angezeigt.
Bitte beachten Sie, dass die Anzeige einzelner Maschinengruppen in der Definition der Maschinengruppe deaktiviert werden kann.

Was bedeutet die unterschiedliche Höhe der Maschinen?

Damit wird die Auslastung / Überlastung der Maschinen signalisiert.
![](Grafische_Auslastung4.gif)

D.h. ist eine Maschine so verplant, dass keine Überlast gegeben ist, so ist diese nur eine Zeile hoch (so wie Stanze bzw. Trockenanlage).
Sind jedoch terminliche Kollisionen, so werden die Überlappenden Arbeitsgänge übereinander dargestellt und die Höhe der Maschine als auch der gesamten Maschinengruppe wird entsprechend größer. D.h. wenn alle Maschinen richtig eingeplant sind, so hat jede Maschine nur die Höhe von einer Zeile.
Hinweis: Diese Betrachtung gilt nur für den ausgewählten Zeitraum.

Wo werden Daten zu den Arbeitsgängen angezeigt?

Bewegen Sie die Maus über einen Arbeitsgang. Nach ca. einer Sekunde erscheinen weitere Details zu diesem Arbeitsgang. Diese bedeuten:
- Los
- Stückliste
- Artikel(nr der Tätigkeit)
- Tätigkeit ... Beschreibung der Tätigkeit
- Dauer [min]: ... Offset (innerhalb des Tages)
- Verfügbar [min]: Verfügbare Arbeitszeit (der Maschine) für den ersten Tag des Arbeitsganges anhand des zu diesem Tag gültigen Maschinenzeitmodells
- geplanter Beginn: (Datum)
- Aktueller Beginn: (Datum)
- Tag (Spalte): Offset zur Darstellung
- PlanItemId: Interne Verwaltungsnummer

Änderung des Betrachtungszeitraumes

Wichtig: Wird während der Planung der Betrachtungszeitraum geändert, so werden die bisher geplanten Daten verworfen. 

Wie kann man Arbeitsgänge nicht in der Auslastungsplanung anzeigen?

Dies erreichen Sie am Einfachsten dadurch, dass Sie dafür eine Maschinengruppe definieren, welche nicht in der Auslastungsplanung ![](Auslastungsanzeige_anzeigen.gif) angezeigt wird.
Nun definieren Sie eine Maschine welche Mitglied dieser Maschinengruppe ist und hinterlegen diese Maschine bei den Arbeitsgängen, die nicht angezeigt werden sollten.

Wie kann man Arbeitsgänge außerhalb des angezeigten Bereiches hinausschieben?

Arbeitsgänge können im gewählten Zeitbereich, z.B. ein Monat, frei hin und her bewegt werden. Der im Browser angezeigte Bereich ist oft kleiner. Um nun einen Arbeitsgang aus diesem Zeitraum hinauszuschieben, ziehen Sie den Arbeitsgang an den rechten Rand. Wenn Sie damit sehr nahe an den Rand kommen, bewegt sich das Fenster selbsttätig nach links bzw. rechts, sodass Sie den Arbeitsgang, im gewählten Zeitbereich einlasten können.

**<a name="Reihung der Arbeitsgänge"></a>Hinweis:** Die Reihung der Arbeitsgänge innerhalb des Tages wird unter Versatz MS, im unteren Modulreiter offene AGs abgespeichert. Ein Umreihen, eine Veränderung dieses Wertes wirkt entsprechend für die Anzeige der Tagesauslastung. Das bedeutet, in der Regel wird die Reihenfolge durch die grafische Auslastungsanzeige gegeben. 

Wie mit dem Einplanen beginnen?

Gerade zum Beginn des Einsatzes der Auslastungsplanung stellen sich immer zwei Fragen:
1. wie mit der Einplanung beginnen, vor allem wie die Altlasten aufarbeiten
2. wie werden von **Kieselstein ERP** neue Aufträge, also Lose, eingelastet

**wie beginnen?**<br>
1. wählen Sie einige wenige Maschinen, die bei Ihnen im Unternehmen die Auslastung bestimmen. Es sind dies die Maschinen an denen es sich immer staut, die nie ausfallen dürfen. Verplanen Sie diese Maschinen optimal. Sind die wenigen Maschinen, wir denken hier an 2-3 Maschinen maximal eine Maschinengruppe (z.B. das Drehen), verplant, so ergibt sich in der Regel der Ablauf an den anderen Maschinen von selbst. Für die Steuerung der Mitarbeiter nutzen Sie bitte auch im Modul Los, unterer Reiter offene AGs, das Journal Maschinen + Material für die jeweilige Maschine / Maschinengruppe. Damit erhalten Ihre Mitarbeiter eine gute Übersicht über die von Ihnen geplanten Arbeitsgänge auf den jeweiligen Maschinen.
2. erledigen Sie alle alten Arbeitsgänge die in der Vergangenheit übersehen bzw. vergessen wurden, dass sie schon lange erledigt sind
3. planen Sie reserve Arbeitsgänge / Lose in der Zukunft z.B. Ende des Jahres, Ende des Quartals

**wie werden von **Kieselstein ERP** neue Lose eingelastet?**<br>
In der Regel steuert der Kundentermin, wann welches Los fertig sein sollte. Daraus ergibt sich anhand der, in der Stückliste, hinterlegten Durchlaufzeit auch der Beginntermin des Loses und somit der Zeitpunkt zu dem mit dem ersten Arbeitsgang begonnen werden sollte. Für die Staffelung der weiteren Arbeitsgänge hat sich in vielen Unternehmen bewährt, dass der Arbeitsgangversatz pro Arbeitsgang auf mindestens einen Tag gestellt wird. So haben Sie einen möglichen Ablauf der Fertigungsschritte. Werden nun neue Lose angelegt, so sieht man bei der Aktualisierung der Anzeige, an welchen Maschinen es wiederum zu Kollisionen kommt, einfach dadurch, dass die Zeilenhöhe der Maschine(n) größer als eine Arbeitsganghöhe ist. Damit haben Sie die benötigte Information.<br>
Denken Sie bitte daran, dass das Herumschieben der Arbeitsgänge nur die Simulation der Auslastung darstellt. Erst durch das Zurückspeichern werden die Arbeitsgänge aktualisiert.
Wir empfehlen auch, dass die Planung nur von einer Person gemacht wird. Arbeiten mehrere gleichzeitig daran, so ist die Abstimmung zwischen den Personen extrem wichtig.

Für das einlasten neuer Lose, genauer der Einplanung des zeitlichen Versatzes der Arbeitsgänge, hat sich die Aktivierung des Firmenzeitmodells mit dem Parameter [AUTOMATISCHE_ERMITTLUNG_AG_BEGINN](index.htm#Beginntermin automatisch errechnen) bewährt. Damit werden die Arbeitsgänge innerhalb eines Loses seriell hintereinander eingeplant.

Wie sieht man wie Störungen / Verspätungen wirken?

Werden Arbeitsgänge nicht wie geplant erledigt, sondern z.B. bei Maschinenausfällen, falschen Solldaten, usw., zu spät erledigt, so sieht man, dass Arbeitsgänge "hängen bleiben". Das bewirkt wiederum, dass sich die Höhe der Maschinenzeile verändert / erhöht und macht somit sichtbar, dass es hier zu zeitlichen Überschneidungen kommt.
Zusätzlich werden Arbeitsgänge die vor dem aktuellen Tag bereits erledigt werden hätten sollen, in hellblau dargestellt.
![](Auslastungsplanung_zu_spaet.gif)

#### Kann eine Liste der nicht zugeordneten Arbeitsgänge ausgedruckt werden?
Alle angelegten Arbeitsgänge sind im Modul Los, unterer Modulreiter offene AG (Arbeitsgänge) sichtbar.
Hier können einzelne Maschinen/-gruppen oder die Übersicht über alle offenen Arbeitsgänge eines bestimmten Zeitbereiches oder auch alle (dazu einfach auf die beiden Radiergummi klicken) ausgedruckt werden. Die Arbeitsgänge die keiner Maschine zugeordnet sind, werden als letztes ausgedruckt.

#### Wie werden die Sollzeiten in die Auslastungsanzeige übernommen ?
Abhängig vom Parameter Auslastungszeitenberechnung werden diese mit der vollen Sollzeit (=0) oder mit der laut Fortschrittsfaktor errechneten Sollzeit (=1) in die Auslastungsanzeige übernommen. Bitte beachten Sie, dass die Erfassung des Fortschrittes in % erfolgt und üblicherweise bei der Buchung am Zeiterfassungstift wenn 100% Fortschritt gescannt werden, wird dieser Arbeitsgang als Fertig angenommen. Bitte beachten Sie, dass trotzdem auch 0-Zeiten übernommen werden.

#### Reihenfolge, Sortierung Maschinengruppen
Die Maschinengruppen sind aufsteigend nach ihrer internen Nummer (IId) sortiert. Die angezeigte 2-stellige Zahl ist allerdings die Identifikationsnummer der 1\. Maschine darin.

#### Sichtbarkeit Maschinen
Die Sichtbarkeit wird über die Maschinengruppen gesteuert (Hakerl "in Auslastungsanzeige anzeigen").

#### Anzeige Soll-/Istzeit
Prinzipiell wird die Dauer (Länge) des AGs in der Auslastungsanzeige mit Soll-Zeit abzgl. Ist-Zeit berechnet. Ist die Differenz negativ, so wird der AG schraffiert.
Ist der Parameter AUSLASTUNGSZEITENBERECHNUNG auf 1, so wird der Fortschritt des AGs mit einberechnet in dem dieser Prozentsatz von der zuvor berechneten Differenz abgezogen wird.

#### Verschieben von Arbeitsgängen und was passiert mit den davor eingetragenen AGs?
Wird ein AG verschoben und hat dieser Nachfolger, so erscheint dann im aufpoppenden Fenster, ob die Nachfolger mitverschoben werden soll. Die Vorgänger werden nie mitverschoben.

#### Arbeitsgang Farben
| Farbe | Bedeutung |
| --- | --- |
| hellblau | Startzeit < Heute |
| hellgrün | Startzeit >= Heute |
| rot | Startzeit > Finaltermin des Auftrags |
| grün | AG markiert |

Wenn sich das Los auf einen Auftrag bezieht, so kommt der Finaltermin aus der AB. Ist diese Verbindung nicht gegeben, so wird anstelle dessen der Los-Ende-Termin verwendet

Priorität ist aufsteigend zu sehen, dh. wenn AG/Los ist markiert zutrifft, dann ist die AG-Farbe grün, egal ob eine Bedingung darunter auch zutrifft, usw..