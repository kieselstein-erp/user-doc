---
title: "Reisezeiten"
linkTitle: "Reisezeiten"
categories: ["Reisezeiten"]
tags: ["Reisezeiten"]
weight: 550
description: >
  Reisezeiten erfassen und abrechnen
---
Reisezeiten
===========

In **Kieselstein ERP** steht Ihnen mit dem Modul Reisezeiten eine komfortable Erfassung der Reisezeiten und -kosten zur Verfügung. Sie finden diese im Modul Zeiterfassung ![](Reisezeiten1.jpg) nach Auswahl des jeweiligen Mitarbeiters unter Reisezeiten ![](Reisezeiten3.gif).
Die Definition der verschiedenen Tagesätze / Reiseaufwandsentschädigungen erfolgt im unteren Modulreiter Diäten ![](Reisezeiten2.gif) auch Verpflegungs-Mehraufwand genannt.
Da die Spesensätze nicht streng mit den Ländern gekoppelt sind, so werden z.B. für Grenzregionen andere Spesensätze verrechnet, als für weiter entfernte Landesteile und bestimmte Städte (Mailand, London) haben eigene Sätze, können diese eigenständig im Modulreiter Diäten definiert werden.
Auf eine Definitionsmöglichkeit für Nächtigungsgelder haben wir verzichtet, da diese in aller Regel nach Aufwand abgerechnet werden.

Die Buchung der Reisezeit gliedert sich in zwei Teile.

**Beginn**

Die ist der jeweilige Beginn einer Reise eines Reiseabschnitts. D.h. wenn Sie z.B. mehrere Kundenbesuche an einem Tag machen, so beginnen Sie mit der Abreise von Ihrem Unternehmen bis zum ersten Kunden, danach fahren Sie zum zweiten Kunden, geben also den Zeitpunkt der Abreise vom ersten Kunden zum zweiten Kunden als erneuten Beginn der Reise zum zweiten Kunden an und so fort.
![](Reisezeiten5.gif)
Definieren Sie jeweils welchen Kunden Sie besucht haben, mit wem Sie gesprochen haben und in welches Reiseland, siehe dazu oben, Diäten, dies erfolgte. Geben Sie unter Kommentar eine entsprechende Begründung an.
Wenn Sie mit dem eigenen Fahrzeug, einem Firmenfahrzeug und ähnlichem unterwegs sind, so geben Sie zusätzlich den Beginn-Kilometerstand an.
![](Reisezeiten6.gif)
Geben Sie zusätzlich das Kennzeichen Ihres Privatfahrzeuges an bzw. wählen Sie aus den hinterlegten Firmenfahrzeugen aus, mit dem Sie unterwegs sind. Zusätzlich können eventuelle weitere Spesen angegeben werden.

Weiterreise

Die Weiterreise wird wie ein Beginn gebucht. Ihr **Kieselstein ERP** erkennt die Weiterreise daran, dass es davor ebenfalls einen Reisebeginn (und kein Ende) gibt.
Das bedeutet, dass neben dem neuen km-Beginn
![](Reisezeiten4.gif)
der km-Stand des Vorgänger-Abschnittes angezeigt wird und somit alternativ auch die Entfernung eingegeben werden kann, was dann zum km-Beginn des nunmehrigen Reiseabschnittes führt.

**Ende**

Am Ende der Reise geben Sie den Endezeitpunkt, wenn Sie also wieder im Unternehmen sind, an und geben dazu den Kilometerstand bzw. die gefahrene Entfernung an, wenn Sie mit einem Fahrzeug unterwegs waren. Bitte beachten Sie, dass für die Berechnung der Entfernungen die Kilometerstände Beginn und Ende herangezogen werden. Die Eingabe der Entfernung dient lediglich als Rechenhilfsmittel.
 **Wichtig:** Wenn, wie z.B. bei Außendienstmitarbeitern durchaus üblich, mehrere Kunden an einem Tag besucht werden, so buchen Sie bitte, so wie oben beschrieben immer nur Reisebeginn, so lange, bis die Dienstreise tatsächlich beendet ist. Wird immer Beginn, Ende gebucht, so werden von **Kieselstein ERP** die Diäten für jeden Reiseabschnitt berechnet, was ziemlich wahrscheinlich keine Reisediäten zur Folge hat.

**Bitte beachten:**
Eine Reise, z.B. zu einem Kunden, ist nicht die Anreise morgens, mit Beginn und Ende und dann die Abreise am Abend mit Beginn und Ende, sondern Sie beginnt am Morgen und endet am Abend. Oder allgemeiner sie beginnt bei der Abfahrt aus dem Büro und endet mit der Rückkehr ins Büro (Büro steht hier für Ihr Unternehmen oder auch die Privatadresse des Mitarbeiters).
**Falsch:** ![](Reisebuchung_falsch.gif)
Das würde bedeuten, dass das zwei Reise zu je einer Stunde sind.

**Richtig:** ![](Reisebuchung_richtig.gif)
Der Mitarbeiter war den ganzen Tag unterwegs.

Wäre aber der Mitarbeiter mehrere Tage unterwegs, so könnte das durchaus auch wie folgt aussehen:
![](Reisebuchung_mehrere_Tage.gif)

#### Wie wird die Entfernung bei mehreren Beginns berechnet?
Je nach Unternehmens-Gepflogenheiten und auch um den Erfassungsaufwand so gering wie möglich zu halten, wird immer vom Reisebeginn aus die Entfernung bis zum nächsten eingetragenen Reisebeginn bzw. maximal bis zum Reiseende berechnet. Dies bewirkt dass bei mehreren Beginns trotzdem nur der Kilometerstand / die Entfernung beim ersten Beginn und beim Ende eingetragen werden muss, bewirkt andererseits auch, dass die Kosten für die Reise immer dem Auftrag zugeordnet werden, der dem Reisebeginn zugeordnet ist. Gibt es mehrere Beginns, ohne erfasstem Kilometerstand, so trägt der erste Beginn, genauer der Beleg dazu, die gesamten Kosten.

#### Meine Reise besteht aus vier Blöcken, wie erfasse ich das richtig?
Wenn eine Reise aus vier Abschnitten besteht, so müssen diese mit fünf Buchungen erfasst werden.
D.h. wenn der Reisetag z.B. so aussieht, dass:
- 07:00-07:45 Anreise      40km
- 12:30-12:45 Weiterreise  5km
- 15:15-15:30 Weiterreise  5km
- 16:00-16:15 Heimreise   40km

![](Reise_ueber_mehrere_Abschnitte.jpg)

**Auswertungen finden Sie unter Info, Reisezeiten bzw. Fahrtenbuch.**

**Hinweis:** Für die Berechnung der Kilometersätze werden die im Modul Personal beim jeweiligen Mitarbeiter unter Personalgehalt definierten Kilometersätze herangezogen. **Hinweis:** Wurden im Auswertungszeitraum Änderungen am Kilometergeld vorgenommen, so greift die jeweils zum Beginn des Auswertungszeitraums gefundene Definition. D.h. für eine korrekte Berechnung muss die Fahrtenabrechnung jeweils mit den entsprechenden Änderungen übereinstimmen. **Hinweis:** Drucken Sie zuerst das Fahrtenbuch aus und stellen Sie sicher, dass die eingetragenen Daten richtig sind. Die Fahrtkosten, werden dann in die Reisekostenabrechnung übernommen. Dadurch erhalten Sie einen Beleg in dem alle angefallenen Reisekosten ersichtlich sind.

**Definition der Diätensätze**

Die Definition der Diäten finden Sie im Modul Zeiterfassung, unterer Modulreiter Diäten.
Die Diätensätze und deren Berechnungen sind je nach Branchen und Ländern enorm unterschiedlich. Wir haben mit **Kieselstein ERP** eine Möglichkeit geschaffen alle Regeln abzubilden. Damit dies auch passend zu Ihren Anforderungen erfüllt werden kann, sind anwenderspezifische Formulare erforderlich. Für die Anpassung Ihres Formulars wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer. **Hinweis:** Es kann pro Reiseland und Datum nur jeweils eine Diäten Definition eingeben werden.
Da es für den gelernten Österreicher klar ist, dass grenznahe Gebiete, bestimmte Städte, andere Diätensätze haben als das eigentliche Land, so hat z.B. Frankfurt einen höheren Diätensatz als Deutschland, aber der direkte Grenzort zu Salzburg, Freilassing, einen niedrigeren als Deutschland, so können / müssen eigene Reiseländer definiert werden.

**Info:**

Wenn wir hier von Diäten sprechen, so ist dies der in Österreich übliche Begriff für den Ersatz des Mehraufwandes in der Haushaltsführung. Also diejenige Entschädigung, welche Sie erhalten, da Sie
nicht "zu Hause" ihr Mittagsmahl und Ihr Abendessen einnehmen können.
Der Begriff Spesen, meint zusätzliche Auslagen im Sinne der Reise, wie Eintrittskarten, Trinkgelder, usw.

#### Wie werden die Diäten definiert ?
Hier sein einerseits auf die [amtliche Seite](https://www.bmf.gv.at/steuern/selbststaendige-unternehmer/betriebsausgaben/ba-reisekosten.html) verwiesen. Hier steht für Inlandsreisen, also für Reisen von in Österreich steuerpflichtigen Personen in Österreich, ein Tagesgeld in Höhe von maximal 26,40 Euro zu. Dieser Betrag versteht sich für die Zeitspanne von 24 Stunden. Dauert eine Reise länger als drei Stunden, so kann für jede angefangene Stunde 1/12 (somit 2,20 Euro) gerechnet werden. Dauert eine Reise mehr als 11 Stunden an, erhält man bereits die volle Diät von 26,40 Euro.
Das bedeutet dass
![](Diaeten_Inland_AT.gif)
als Tagessatz die volle Diät von 26,40 €, als Mindestsatz 3 x 2,20 also 6,60€ und Stundenweise (-verrechnung) ab 3Stunden und dann mit einem Stundensatz von 2,20€ einzutragen ist.

Somit bedeutet, dass wenn Stundenweise angehakt ist, dass für die Anrechnung des Diätensatzes eine Reisemindestdauer von Ab Stunden (in der Regel werden es drei Stunden sein) erforderlich ist.

Bitte beachten Sie, dass diese Zeiten immer für einen Reiseabschnitt und einen Reisetag gelten. Also z.B. für die Zeit zwischen dem ersten Beginn und dem zweiten Beginn und dann der Zeit zwischen dem zweiten Beginn und dem Ende der Reise.

#### Können auch andere Regeln hinterlegt werden?
Durch die Angabe eines Reisediäten Scripts (siehe oben, Dateiname Script) können auch andere Regeln hinterlegt werden.

#### An einigen Tagen werden keine Diäten angedruckt?
Die Berechnung der Diäten richtet sich auch nach dem Arbeitszeitgesetz. Hier steht unter vielem Anderem, dass auch den Mitarbeitern die Diäten erst gebühren wenn sie **<u>mehr</u>** als drei Stunden auf Dienstreise sind. Das bedeutet: Wenn z.B. ein Mitarbeiter eine Reisezeit von 9:00-12:00 bucht, so sind das exakt drei Stunden. Dafür werden keine Diäten ausgewiesen. Bucht er aber 8:59-12:00 so sind das mehr wie drei Stunden und es werden entsprechende Diäten errechnet.

Der Faktor der Mindeststunden kann für jedes Reiseland unter Diäten im Feld Ab Stunden eingestellt werden. Es muss dafür die Stundenweise Berechnung aktiviert werden, also Stundenweise muss angehakt sein.

#### Wie kann ich die Reisekosten nachrechen?
Bitte beachten Sie bei der Prüfung der Reisekosten, dass diese eine Tagesweise Betrachtung sind. D.h. die Reisekosten richten sich nach dem Kalendertag. Ist nun ein Mitarbeiter von z.B. 10:00 des ersten Tages bis 10:00 des zweiten Tages auf Dienstreise, so sind das zwar 24Std (also gedanklich ein Tag) aber laut Arbeitszeitgesetz sind das zwei Reisetage.
Am ersten Tag sind es 14Std, es gebührt ihm (normalerweise, hier kann es Abweichungen je nach Kollektiv/Tarifvertrag geben) der volle Diätensatz.
am zweiten Tag sind es 10Std. Je nach Reiseland (Inland/Ausland) bekommt er dafür 2/3 oder 10/12 des Diätensatzes des Reiselandes.

Reisekosten einem Auftrag / Projekt zuordnen

Jeder Reisebeginn kann einem Auftrag oder einem Projekt zugeordnet werden. Damit ist der Kunde/Partner definiert. Hier definieren Sie zusätzlich den Faktor (%) wieviel der gesamten Reisekosten auf diesen Kunden gerechnet werden dürfen. Beim Reise Ende geben Sie an mit welchem Firmenfahrzeug diese Reise gemacht wurde, oder ob dies mit dem Privatfahrzeug erfolgte. Wurde die Reise mit dem Firmenfahrzeug gemacht, dann greifen die beim Fahrzeug definierten Kosten, wurde sie mit dem Privatfahrzeug gemacht, so werden die km-Kosten aus dem Personalgehalt verwendet.
Die Erweiterung der Erfassung der km-Stände bei jeder Reisebuchung hat den Vorteil, dass die Zuordnung der tatsächlichen Kosten für den jeweiligen Auftrag, Kunden, Projekt wesentlich exakter erfolgen kann.

<a name="Definition von Fahrzeugen"></a>Definition von Fahrzeugen

Im Modul Personal unterer Reiter Fahrzeug ![](reiter_fahrzeug.JPG), können Sie Firmenfahrzeuge definieren. Klicken Sie dazu auf Neu und geben eine für Sie sprechende Bezeichnung das Fahrzeugs und dessen KFZ-Kennzeichen ein.

 ![](fahrzeug.jpg)
Die Kosten für das Fahrzeug hinterlegen Sie im oberen Reiter Kosten ![](reiter_kosten.JPG), geben Sie hier für das entsprechende Datum die KM-Kosten ein. ![](kosten_fahrzeug.JPG)
Die Auswertung der Fahrten über alle bzw. einzelne Fahrzeuge über einen frei definierbaren Zeitraum finden Sie im Menü Info - Fahrzeuge.

Die Verwendungsart des Fahrzeuges steuert, welche Personen das Fahrzeug verwenden und somit in der Reisezeiterfassung auswählen können.
Es stehen drei Definitionen zur Verfügung:
a.) Privatfahrzeug ... dieses Fahrzeug gehört Ihrem Mitarbeiter
b.) Firmenfahrzeug ... dieses Fahrzeug gehört Ihrem Unternehmen, wird aber nur einem Mitarbeiter zur Verfügung gestellt
c.) Poolfahrzeug ... dieses Fahrzeug gehört Ihrem Unternehmen und wird allen Mitarbeitern zur Verfügung gestellt.

Die Zuordnung der Fahrzeuge, der Fahrzeugverwendungsarten, erfolgt für das Privatfahrzeug und das Firmenfahrzeug im Modul Personal, auf der entsprechenden Person, im Reiter Fahrzeuge. Hier können nur Privatfahrzeug und Firmenfahrzeug ausgewählt werden. Das Poolfahrzeug steht immer allen Mitarbeitern zur Verfügung.
Wird ein Privatfahrzeug oder ein Firmenfahrzeug einer weiteren Person zugeordnet, so dass faktisch zwei Personen das gleiche Fahrzeug nutzen würden, so erscheint ein entsprechender Hinweis. Es ist dies aber zulässig.

Bei der Auswahl des Fahrzeuges in der Reisezeiterfassung = Reisebeginn, stehen die der Person zugewiesenen Fahrzeuge und die Poolfahrzeuge zur Verfügung.

#### Wie werden passive Reisezeiten erfasst?
Um die Erfassung sogenannter passiver Reisezeiten so einfach wie möglich zu machen, und da diese neben der normalen Arbeitszeit zusätzlich anfallen, haben wir uns entschieden dies als eine Eigenschaft einer Tätigkeit zu definieren. Das bedeutet, dass eine passive Reisezeit im Modul Artikel, im Reiter Bestelldaten als ![](passive_Reisezeit.gif) solche definiert werden muss.
Es bewirkt dies, dass in der Monatsabrechnung und damit auch im Zeitsaldo, diese Zeiten als nicht anwesend, im Sinne der Iststunden betrachtet wird, dass aber diese Zeiten in einer eigenen Spalte ausgewiesen werden.
Das bedeutet, dass die passiven Reisezeiten als Tätigkeit während des Tages erfasst und den Aufträgen / den Auftragspositionen zugewiesen werden. Rein durch die Eigenschaft des Tätigkeitsartikels, wird definiert, dass dies eine passive Reisezeit ist. Selbstverständlich sollten Sie die Bezeichnungen der Artikel so wählen, dass die Erfassung eindeutig möglich ist.
Bitte beachten Sie, dass diese Zeiten für Ihre Mitarbeiter, abhängig von den jeweiligen vertraglichen Vereinbarungen, trotzdem zumindest teilweise bezahlt werden müssen.
Der Vorteil dieser Lösung ist, dass die passiven Reisezeiten auch als Kosten in den Aufträgen aufscheinen und auch im Abrechnungsvorschlag entsprechend berücksichtigt werden und die Erfassung in allen Modulen wie gewohnt vorgenommen werden kann.
Eine passive Reisezeit wird in der Monatsabrechnung wie eine Pause / Unterbrechung ausgewiesen und geht demzufolge auch nicht in die Überstundenberechnung ein.
Eine passive Reisezeit wird durch die nachfolgende Tätigkeit, bzw. Ende bzw. Geht beendet. Wird in der passiven Reisezeit eine Unterbrechung gebucht, so reduziert diese (Mittags-)Pause die Dauer der passiven Reisezeit.

**Hinweis:** Selbstverständlich müssen, für die Diäten (Verpflegungs-Mehraufwand) Abrechnungen, neben den passiven Reisezeiten auch die eigentlichen Reisen, mit Reise Beginn, ev. Weiterreise und Reise Ende zusätzlich erfasst werden. Sollten Sie in der Monatsabrechnung nur passive Reisezeiten vorfinden, so wird das vom **Kieselstein ERP** zwar akzeptiert, ist aber im Sinne der Entschädigung des Mehraufwandes noch um die Reisezeiten zu ergänzen.

**Hinweis2:** Da die passiven Reisen auch noch von den normalen Arbeitszeiten abhängig sind (was eine normale Arbeitszeit ist, z.B. Kernzeit, ist im Gesetz nicht ausreichend klar definiert) müssen die Zeiträume an denen die passiven Reisezeiten als solche gerechnet werden definiert werden. Siehe dazu Personal, Kollektiv, Passive Reise. Sind keine Zeiträume definiert, wird auch keine passive Reisezeit gerechnet.

**Hinweis3:** Um z.B. den Sonntag auch als außerhalb der Kernzeit zu definieren,
![](Passive_Reise_am_Sonntag.jpg)
ist sowohl das Bis als auch das ab mit 00:00 und dann der Haken bei Rest des Tages zu setzen.

<a name="Passive Reisezeit"></a>

#### Wie werden passive Reisezeiten in der Monatsabrechnung dargestellt?
Passive Reisezeiten, welche auch tatsächlich (vom Zeitraum her) als solche anzuwenden sind, werden in der Monatsabrechnung von den Iststunden abgezogen und insofern als Unterbrechung ausgewiesen, da damit ja der bezahlte Gleitzeitsaldo sinkt. Darum werden in der letzten Zeile der Monatsabrechnung die Stunden der passiven Reisezeit korrigiert um den Auszahlungsfaktor hinzugerechnet.  ![](passive_Reisezeit.gif)

#### Mögliche Differenzen bei der Reisezeitbetrachtung
Bitte beachten Sie, dass die Reisezeitabrechnung vom Gesetzgeber so vorgegeben ist, dass diese ab der begonnenen Stunde zu rechnen sind. D.h. wenn z.B. die Reisezeit mit einer Beginnzeit von 6:59 gebucht wird, so wird gegenüber einem Reisebeginn von 7:00 eine zusätzliche Stunde hinzugerechnet. Dies passiert manchmal, wenn einerseits Mitarbeiter exakt zum Zeitpunkt Ihre Zeit nachbuchen und andererseits das Auto-Kommt aktiviert ist, bei dem automatisch eine Sekunde vor dem Tätigkeitsbeginn ein Kommt eingebucht wird.

#### Wie können weitere Spesen zugeordnet werden?
<a name="Weitere Spesen"></a>
Insbesondere mit der Kopplung der HVMA Reisezeiterfassung, können auf Reiseabschnitte mit Auftragsbezug weitere Reisekosten / Reisespesen mit Beleg erfasst werden.
Die manuelle Erfassung der Zuordnung der Aufwandsbelege wurde wie folgt umgesetzt:

-   Erfassen Sie den Beleg wie üblich in der Eingangsrechnungsverwaltung

    -   Wurde der Beleg bereits über HVMA mit einem Foto des Beleges erfasst, so ist der Beleg in der Dokumentenablage zu finden

    -   Zusätzlich ist dieser noch nicht kontiert, sondern nur der Betrag erfasst. Im Zahlungsziel ist auch die geplante / durchgeführte Zahlung des Beleges mit einem Verweis auf den Mitarbeiter enthalten, sodass Sie erkennen können, wie dies mit Ihren MitarbeiterInnen abzurechnen ist.
        Die Kontierung (für die Buchhaltung) muss von entsprechend ausgebildeten und berechtigten Personen durchgeführt werden.

-   In der Reisezeiterfassung wählen Sie nun die Reise-Beginnbuchung auf die dieser Beleg zugewiesen werden soll.

-   Wechseln Sie nun in den Reiter Spesen und ordnen mit Neu den oben erfassten Beleg zu.
    Bitte beachten Sie, dass diese Spesen, vor allem wegen der Auftragszuordnung, nur auf Reisebeginnbuchungen zugewiesen werden können.
    Sollte die ausgewählte Eingangsrechnung bereits einem anderen Reiseeintrag zugewiesen sein, so erscheint ein entsprechender Hinweis.
    ![](Reisespesen_Zuordnung.jpg)

Belege die keine Verbindung mit den Reisen haben können über die Auftragszuordnung dem Auftrag zugewiesen werden.

**WICHTIG:** Für den Abrechnungsvorschlag werden Eingangsrechnungen die in den Spesen zugeordnet wurden NICHT berücksichtigt.
Hintergrund: In Bezug zum Abrechnungsvorschlag, werden die Reisespesen in der Regel durch Pauschalen gedeckt, bzw. dem Kunden nicht in dieser transparenten Form dargestellt.
Es ist daher eine automatische Zuordnung zum Auftrag nicht vorgesehen.
**Hinweis:** In der Nachkalkulation des Auftrags sind die Spesen in der Zeile Reisekosten aber enthalten.