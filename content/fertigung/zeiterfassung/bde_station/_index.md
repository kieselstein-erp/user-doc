---
title: "BDE Station"
linkTitle: "BDE Station"
categories: ["Zeiterfassung"]
tags: ["Zeiterfassung","Terminal", "BDE Station"]
weight: 210
description: >
  BDE Station als Zeiterfassungsterminal
---
BDE Station
===========

Die BDE Station (**B**etriebs**d**aten**e**rfassung) ist ein integrierter Bestandteil des **Kieselstein ERP** Systems. Sie ist mittels sogenannter Java-Server-Pages direkt im Applikationsserver integriert.

Der Grundgedanke ist, dass als einfache Variante die Zeiterfassung in einem Browser und mittels Barcodescanner im Wedge-Modus die wesentlichsten Funktionen für die Anwesenheitszeiterfassung und die Buchungen auf Lose, Aufträge erfüllt werden können.

Die Bedienung erfolgt mittels Barcode. Diese findest du 
1. im Modul Personal, Journal, Personalliste, und dann Barcode-Liste anhaken
2. im Modul Zeiterfassung, unterer Reiter Sondertätigkeiten. Hier gibt es die Möglichkeit die gewünschten Sondertätigkeiten durch auf der BDE-Station buchbar zu definieren. Im Menüpunkt Info, Sondertätigkeitenliste können die Barcodes für die Sondertätigkeiten ausgedruckt werden.
3. auf dem Fertigungsbegleitschein bzw. der Auftragspackliste stehen die Barcodes für die Buchung der jeweiligen Tätigkeit zur Verfügung.

Der Aufruf für den Browser lautet: http://KES-SERVER:8080/ze/ze?cmd=bdestation, bzw. generell 
[WEB_Links]( {{< relref "web_links" >}} )

Das Browserfenster startet üblicherweise im Vollbildmodus. Zum Schließen Klicke bitte auf den X SCHLIESSEN Link, wodurch das Fenster wieder in die Normaldarstellung zurückgeschaltet wird. Nun kannst du mit Alt+F4, bzw. den Klick auf das x rechts oben den Browser beenden.

Um von der Normaldarstellung wieder in den Vollbildmodus zu wechseln drücke bitte die F11-Taste.

Solltest du FireFox als Browser verwenden, so bitte unter Extras, Einstellungen, Web-Features die Funktion Pop-up-Fenster blockieren deaktivieren.

Für den Test bei einer Neuinstallation, rufe bitte zuerst die Anwesenheitsliste auf (siehe [WEB_Links]( {{< relref "web_links" >}} ) ).

Danach die BDE-Station. Es kann bei Neuinstallationen vorkommen, dass die Anwesenheitsliste funktioniert, aber die BDE-Station nicht. In diesem Falle wenden dich bitte an deinen  **Kieselstein ERP** [Betreuer](../../html/faq/faq_kundeninstallation.htm#JBoss_Settings).

Die Bedienung der BDE Station erfolgt mittels Barcodescanner mit Wedge (Tastatur) Interface.
Siehe dazu auch https://www.bedienungsanleitu.ng/datalogic/quickscan-qd2131/anleitung<br>
- Zuerst Rest to default factory Setting
- Danach USB Keyboard Germany

Bitte achte darauf, dass das Wedge Interface passend zur eigentlichen Tastatur, zur Tastaturemulation deines Rechners programmiert ist.

Solltest du mit einer deutschen Tastaturemulation insbesondere falsche Zeichen für die Losnummer erhalten so muss die Tastaturemulation des Scanners richtiggestellt werden. Beispiel: Bei der Losnummer der BDE-HTML Seite wird anstelle der ![](BDE_St1.gif)
die falsche Nummer ![](BDE_St2.gif)  angezeigt.

Hier ist vermutlich der BDE Scanner noch auf die default Einstellung US eingestellt. Bitte den Barcodescanner entsprechend dem Tastaturlayout deines Rechners [umstellen](#Programmierung der Barcodescanner).

Bedienung der **Kieselstein ERP** BDE Station

Beim Start des Browser wird folgendes Fenster, im Vollbildmodus angezeigt.

![](BDE_Station_Ausweisnummer.gif)

Da auf den BDE-Stationen mehrere Mitarbeiter Zeitbuchungen vornehmen, muss zuerst die Ausweisnummer des Mitarbeiters eingescannt werden.

Nachdem einscannen einer gültigen Ausweisnummer erscheint folgende Anzeige:

![](BDE_Station2.gif)

Sollte hier

![](BDE_Station3.gif)

angezeigt werden, so ist der Ausweis / die Ausweisnummer des Mitarbeiters im Personalstamm nicht eingetragen.

**Hinweis:**

Achte bei mehreren Mandanten darauf, dass die Ausweisnummern über alle Mandanten hinweg eindeutig sind.

Bei Beleg/ Sondertätigkeit/ Kombi-Code scanne nun bitte den entsprechenden Code ein. Es stehen hier folgende Möglichkeiten zur Verfügung:

-   Sondertätigkeiten wie Kommt, Geht usw.
    Voraussetzung ist, das die jeweilige Sondertätigkeit auch für die BDE-Station freigegeben ist. (Siehe Zeiterfassung, Sondertätigkeiten, Auf BDE Station buchbar)
-   Losnummer
-   Auftragsnummer
-   kombinierter Los, Maschine, Tätigkeiten Code ($V)
-   kombinierter Auftrags Code ($U)
-   Tätigkeits-Ende Code ($FERTIG)
-   Los Kombinations Code ($PLUS)
-   Los-Sperr-Code ($SPERREN)

Abhängig vom eingescannten Code wird die nachfolgende Abfrage gesteuert.

-   Sondertätigkeiten<br>
    Fortsetzung wieder mit der Ausweisnummer
-   Losnummer<br>
    Nun folgt die Erfassung der Maschine oder der Tätigkeit
    -  Wird hier eine Maschine(n-ID) eingescannt, so muss danach die Tätigkeit angegeben werden
    -  Wird hier der Fertig Code eingescannt, so muss danach die Tätigkeit angegeben werden
    -  Wird hier nur die Tätigkeit eingescannt, so ist damit die Erfassung dieser Zeiteingabe abgeschlossen und es wird wieder mit der Frage nach der Ausweisnummer fortgesetzt.
    -  Wird hier der **Los-Sperr-Code** eingescannt, so wird der Stücklistenartikel des Loses gesperrt. Es wird dazu die Sperre eingetragen, bei der ![](BDE_Sperre_durch_Fertigung.gif) Sperre durch Fertigung angehakt ist.<br>
    Im Sperrgrund der Sperre wird die Los Nr. und der Vorname und Name des sperrenden Mitarbeiters eingetragen. Diese Funktion dient der Signalisierung von der Produktion an die Konstruktion, dass es mit einem Artikel Produktionsprobleme gibt.
-   Auftragsnummer<br>
    Es wird die Standard Arbeitszeit (Parameter DEFAULT_ARBEITSZEIT) für die Tätigkeitsbeginnbuchung verwendet. Damit ist die Zeitbuchung beendet und es wird wieder mit der Frage nach der Ausweisnummer fortgesetzt.
-   kombinierter Code<br>
    Fortsetzung wieder mit der Ausweisnummer
-   Tätigkeits-Ende Code<br>
    Erfassung der Tätigkeit
-   Los Kombinations Code $PLUS<br>
    Erfassung der nächsten Losnummer [siehe](#$PLUS)

![](BDE_Station4.gif)

#### Sperrcode
Siehe oben Los-Sperr-Code

**Hinweis:**

Wenn du bei der Schrittweisen Erfassung feststellst, dass versehentlich ein falsches Los o.ä. eingescannt wurde, so scanne einfach den Storno Code ($STORNO). Damit wird die Eingabe abgebrochen.

Es wird Los nicht gefunden angezeigt

![](BDE_Fehlermeldung.gif)

Wir auf der BDE Station diese Meldung angezeigt, so muss der Barcodescanner entsprechend Ihrem Tastaturlayout [programmiert](#Programmierung der Barcodescanner) werden.

## Verwendung des Los Kombinationscodes $PLUS, Arbeitsgänge gemeinsam buchen
Manchmal werden mehrere Lose parallel / zugleich bearbeitet. Z.B. werden gleichzeitig mehrere verschiedene Gehäuse gebogen oder unterschiedliche Nutzen trotzdem gleichzeitig gefertigt. Dafür ist dieser Code gedacht. Bei einer Verteilungsbuchung ist der Ablauf so, dass zuerst die automatischen Pausen gebucht werden und danach die Verteilungsbuchungen gemacht werden.

Zur Information, dass Zeitverteilungen noch nicht abgeschlossen sind, erhalten Sie die Meldung "Offene Zeitverteilung vorhanden!". 
![](zeitverteilung.PNG)
Das bedeutet, dass der ausgewählte Mitarbeiter eine Zeitverteilung auf mehrere Lose gestartet hat, diese aber noch nicht abgeschlossen wurde. Die Zeiten werden erst gebucht, wenn die Zeitverteilung abgeschlossen ist. Sobald die Buchung abgeschlossen ist, wird auch die Meldung nicht mehr angezeigt. Um die Meldung zu vermeiden, ist es, bei einer gleichzeitigen Verwendung von BDE-Station und Zeiterfassungsstiften wichtig, dass bevor das GEHT auf der Station gebucht wird, die Stifte übertragen werden. 

Ein Beispiel:<br>
Es sollten vier unterschiedliche Baugruppen auf einem Bestückungsautomaten in einem Zug quasi gemeinsam bestückt werden. Diese Baugruppen haben eine gesamte Bestückungszeit von einer Minute. D.h. diese eine Minute fällt ja gemeinsam für alle vier Lose an, also auf das 1.Los PLUS dem 2.Los PLUS dem 3.Los PLUS dem 4.Los.<br>
Um nun die gemeinsame Dauer für die Bestückung zu erfassen gehen Sie wie folgt vor:<br>
Scannen Sie die 1. Losnummer (nach der Ausweisnummer).<br>
Nun scannen Sie $PLUS. Damit weiß **Kieselstein ERP**, dass ein weiteres Los gemeinsam erfasst werden soll.<br>
Nun scannen Sie die nächste Losnummer und so weiter bis zur letzten Losnummer. Also LosNr $PLUS LosNr $PLUS LosNr<br>
Nach der letzten Losnummer scannen Sie die Tätigkeit. Damit sind die gemeinsam zu erfassenden Lose definiert.<br>
Beim Ende dieser gemeinsamen Tätigkeit, z.B. durch Beginn einer neuen Tätigkeit oder durch Geht, wird die angefallene Gesamt-Ist-Zeit auf die Lose anteilsmäßig aufgeteilt. Dabei erhält jedes Los den gleichen Teil. In unserem obigen Beispiel mit einer Minute auf vier Lose erhält jedes Los 15Sekunden.<br>
Für die Definition der Zeitverteilung [siehe bitte Beschreibung Zeitverteilungstypen]( {{<relref "/fertigung/zeitwirtschaft_in_der_fertigung/ag_gemeinsam_buchen/#zeitverteilungstyp" >}} )

## Programmierung der Barcodescanner
Bitte unterscheide ganz eindeutig die Einsatzgebiete der Barcodescanner.

Für die verschiedenen Terminalversionen, welche zu **Kieselstein ERP** mit angeboten werden, werden serielle Barcodescanner verwendet. Für die HTML BDE-Station werden Barcodescanner verwendet, die eine Tastatur emulieren.

Hier nachfolgend die Parametrierung für dieses sogenannte Wedge Interface. Für die Verwendung als serieller Barcodescanner siehe bitte die Beschreibung bei den jeweiligen Scannern.

Üblicherweise sind bei den von uns eingesetzten Datalogic Barcodescannern kleine Heftchen mit dabei, in denen die Tastaturlayouts programmiert werden können. Sehen Sie unter Keyboard Nationality nach und stellen Sie den Scanner auf z.B. Deutsch.

Wir haben zusätzlich hier den entsprechenden Code noch mit angeführt.

![](BDE_Wedge_Deutsch.gif)

## Wie stelle ich meinen BDE-Stations Computer am Besten ein?

Hinterlege den gewünschten [Link]( {{< relref "web_links" >}} ) direkt in die Startseite des gewählten Browsers.

Stelle den Autostart nun so ein, dass der Browser gestartet wird.

Nun sorge noch dafür, dass sich der Computer beim Hochfahren automatisch anmeldet (Netzwerkanmeldung ist nicht erforderlich).

Nun läuft auf diesem BDE-Stations Computer permanent die BDE-Stations Browser Erfassung. Auch wenn es zu einem Stromausfall kommt, fährt der Computer danach wieder hoch und startet automatisch die Browsererfassung.

Achte bei den BDE Stationen darauf, dass die Zeit regelmäßig synchronisiert wird.

Allgemeine Kommandos / Abfragen an der HTML BDE-Station

Folgende Kommandos stehen zusätzlich zu den unter Zeit, Sondertätigkeiten eingetragenen und als Auf der BDE-Station buchbar definierten zur Verfügung:

$TAGESSALDO ... Anzeige des Tagessaldos des Mitarbeiters für die Anwesenheitszeit des heutigen Tages

$SALDO ............ Anzeige des aktuellen Gleitzeitsaldos bis gestern Abend und der Urlaubsabrechnung bis gestern Abend. Exakt: Aliquoter Urlaubsanspruch abzüglich verbrauchten Urlaub.

## Wieso kann auf der BDE Station kein Arzt gebucht werden?
Es ist NICHT vorgesehen, dass an der BDE Station Arzt, Krank, Urlaub oder ähnliches gebucht wird.

Der Hintergrund:

Der Mitarbeiter geht angeblich zum Arzt und kommt nach einigen Stunden wieder. Nun vergisst er den Arzt Beleg zu bringen oder er war nur eine Stunde beim Arzt etc..

D.h. es wird dies in allen Anwendungen so gehandhabt, dass der Mitarbeiter wenn er zum Arzt geht, auf Unterbrechung oder auf Geht stempelt. Wenn er wieder kommt, oder er am Morgen später kommt, so bucht er entweder Unterbrechungs Ende oder eben ein spätes Kommt.

Wenn er nun die Arzt Bestätigung bringt, wird die Unterbrechung auf Arzt geändert und damit die Abwesenheit in eine BEZAHLTE Abwesenheit geändert.<br>
Alternativ kannst du auch, einfach die akzeptierten Arztstunden in den Sondertätigkeiten eintragen, womit dem Mitarbeiter/der Mitarbeiterin die entsprechende Stundenanzahl ohne genaue Zeitangabe gutgeschrieben wird.
