Userstorys zum **Kieselstein ERP** Zeiterfassungs-Terminal auch ZE-Terminal genannt.

Ist ebenfalls unter AGPL lizenziert.

Versuch der Beschreibung aller User Interaktionen mit einer Gewichtung wie wichtig / dringend
| Nr | Wichtig | Dringend | Beschreibung |
| --- | --- | --- | --- |
| 1 | I | | Anmeldung der Person <br> 1. Barcode mit $P<br> 2. verschiedene RFID Leser<br> 3. Durch Eingabe der Ausweisnummer |
| 1a | | | Anzeige meiner letzten Buchungen, offene Zeitverteilung, angestempelte Maschinenparallel Bedienung, bitte immer mit der Startuhrzeit |
| 2 | I | | Kommt Buchung -> Bucht das Kommt in der Zeiterfassung<br>entweder durch antippen eines Buttons oder durch $KOMMT als Barcode oder durch drücken von K (nach der Eingabe der Ausweisnummer) |
| 3 | I | | Geht Buchung -> Bucht das Kommt in der Zeiterfassung<br>entweder durch antippen eines Buttons oder durch $GEHT als Barcode oder durch drücken von G |
| 4 | I | | Unter(brechung) Buchung -> Bucht die Unterbrechung = Pause in der Zeiterfassung<br>entweder durch antippen eines Buttons oder durch $UNTER als Barcode oder dur drücken von U |
| 5 | I | | Arzt, Behörde, .... Buchung -> Bucht die Sondertätigkeit in der Zeiterfassung<br>entweder durch auswählen aus einer Combobox oder durch $Arzt usw. als Barcode |
| 6 | | | Status -> Button oder Barcode. Zeigt den Zeitabrechnungsstatus an. D.h. Gleitzeitsaldo bis gestern Abend und Urlaubsanspruch bis gestern Abend. Bei Urlaubsanspruch gesteuert nach Teilzeitmodell ?? D.h. einmal Stunden, einmal Tage. Sollte man generell einstellen können -> Drücken des Buttons, oder Barcode oder Drücken von S |
| 6a | | | Anzeige der Monatsabrechnung bis gestern Abend, letztes Monat und ev. auch aktuelles Monat gesamt. Jedenfalls sollte der Report wissen, von wo er aufgerufen wird und den Stichtag um das im Report anpassen zu können. |
| 7 | | | Verbindung auch von Extern, z.B. VPN bzw. https oder ... jedenfalls auch für einen DAU Anwender einfach installierbar (man sendet dem Anwender das Gerät und er muss es nur anstecken, ala Teamviewer) |
| 8 | | | Grundsätzlich muss papierlose Fertigung möglich sein |
| 9 | | | Mehrsprachigkeit d.h. das Terminal wird auf eine Sprache/locale eingestellt. |
| 10 | | | Bilder der Buttons definierbar |
| 11 | | | Anwendung als Kommt/Geht/Pause oder als vollwertiges ZE-Terminal |
| 11a | | | Anzeige der Liste aller anzuzeigenden Personen (je nach Personaleigenschaft) |
| 12 | I | | Anzeige der von mir gestarteten Maschinen |
| 13 | | | Änderung der Maschine für den (nächsten) zu buchenden Arbeitsgang, Auswahl wenn möglich, mit Maschinengruppe / Maschine aus gewünschten Arbeitsgang vorbesetzt  |
| 14 | | | Grundsätzliche Unterscheidung zwischen nur Tätigkeit oder mit Maschine |
| 15 | I | | Mehrmaschinen Bedienung, ACHTUNG: in der erweiterten Form, sodass jederzeit weitere Arbeitsgänge gestartet und gestoppt werden können |
| 16 | | | Mehrmaschinen Bedienung im Sinne von ein Arbeitsgang wird gleichzeitig auf mehreren Maschinen gestartet |
| 17 | | | Erfassung von Prüfwerten / Messwerten |
| 18 | | | Anbindung von Messmitteln (USB, Bluethooth, ..) aber auch z.B. Kamera um ein Schnittbild zu Fotografieren  |
| 19 | | | Erkennung von Maschine bleibt stehen (Geisterschicht) |
| 20 | | | Buchung von Los(nr), Auftrag(nr), Projekt(nr) |
| 21 | | | danach Maschine |
| 22 | | | danach Tätigkeit bzw. Arbeitsgang mit Unterarbeitsgang |
| 23 | | | oder kombinierte Barcodes, d.h. Los, Maschine, Tätigkeit bzw. AG&UAG in einem Barcode<br>ergibt dann (20-23) jeweils einen zu buchenden Arbeitsgang |
| 24 | | | gezieltes Maschine-Stop,<br>wobei es hier immer Missverständnisse gibt. Üblicherweise verbinden die Menschen mit dem Stop auch, dass die Tätigkeit darauf beendet wird (siehe erweiterte Form) D.h. ev. brauchen wir ein Beende den Arbeitsgang mit Mensch und Maschine. Was dann bedeutet es läuft um die eine Maschine weniger gleichzeitig, bzw. Mensch und Maschine arbeiten beide nicht mehr. |
| 25 | I | | gezieltes Fertigbuchen, des gesamten AG's aber auch xx% |
| 25a |  | | Wieder aufheben eines Fertiggebuchten AG's, weil verbucht usw. Ohe Änderung der %te. |
| 26 | I | | Ablieferbuchung mit Gut-Schlecht Stück, Berücksichtigung Sollsatzgröße und externer Arbeitsgang ohne SollSatzPrüfung  |
| 27 | | | Ausdrucken von Ablieferetiketten, und Hinterlegung entsprechender steuernder Texte, wo denn der Mitarbeiter den "Zettel" holen sollte |
| 28 | | | Ändern der Losgröße |
| 29 | | | Erfassung von Materialentnahmen, sowohl als Soll erfasst als auch zusätzliches = anderes Material. Dabei auch: dafür brauch ich das andere Material nicht mehr. |
| 30 | | | Anzeige von Dokumenten, aus:<br>- Dokumentenlink<br>- Kommentar,<br>- Dokumentenablage<br>jeweils mit Rechten und der Möglichkeit dies am nächst passenden Drucker auszudrucken |
| 30a | | | Sowohl in Abhängigkeit des Loses = Artikel, als auch des Arbeitsganges (das gibts im KES noch gar nicht) und auch frei, also Filesystem bzw. Artikeleingabe / Stkl-Eingabe |
| 31 | | | Zoom (Zweifinger) in den Dokumenten |
| 31a | | | Redlining unterstützen, Kommentar an Konstruktion eingebbar (kann doch eh jedes Handy ;-) ) |
| 32 | | | Displaygröße von 12" bis 23" wird unterstützt.<br>23" ev. mit externem Bildschirm |
| 33 | | | Das Terminal ist Offline-fähig. D.h. zumindest die Ist-Zeit-Erfassung wird zwischengepuffert. |
| 34 | | | Anzeige des Los-Fortschritts-Statuses aus einem Report |
| 35 | | | Anzeige Artikelstammblatt des Stücklistenartikels |
| 35a | | | Anzeige des Statuses eines Auftrages |
| 35b | | | Anzeige des Statuses eines Projektes |
| 36 | | |  |
| 37 | | | Wartungsanweisung. D.h. welche Wartungsschritte muss der Mitarbeiter zu jedem Schichtende machen, oder heute auf seiner/seinen Maschinen |
| 38 | | | Erfassung der Durchführung der Wartungsanweisung mit Zeit und Material  |
| 39 | | |  |
| 40 | | | Urlaubsantrag, Zeitausgleichsantrag und ev. auch Krankenstandsantrag (wenn das auf einem weit entfernten Terminal gebucht wird und der Mitarbeiter in der 10km entfernten Halle krank wird, oder auf der Baustelle ist) |
| 41 | | |  |
| 42 | | | Losauswahlliste mit den offenen Arbeitsgängen, sortiert nach Arbeitsgangbeginn inkl. ms=Reihung innerhalb des Tages |
| 42a | | | Darstellung der Los-Arbeitsgang-Auswahlliste dass man auch die offene Losmenge und bei Stückrück auch die offene Menge jedes einzelnen AGs sieht. |
| 43 | | | Filterung der Losauswahlliste anhand Tätigkeit, Maschinengruppe, Maschine. |
| 44 | | | Filter der Losauswahlliste jederzeit umschaltbar. Die Aktualisierung muss sehr rasch erfolgen. Ev. auch als neue default Filterung definierbar |
| 45 | | | Default Filter hinterlegbar. |
| 46 | | | Suchen eines Loses mittels Barcodescanner oder Tastatureingabe. D.h. der Scann geht auf die Losnummer, bei der Tastatureingabe auch Artikelnummer der Stkl vorstellbar. Zusätzlich sollte der gewählte Filter greifen. |
| 47 | | | Terminalparameter global sicherbar und ladbar, aber mit .... das nicht usw.. Z.B. Filterung der Losauswahlliste nicht umschalten, alles andere schon. |
| 48 | | | Pausenhupe von Terminal ansteuerbar |
| 49 | | | Waagenanbindung für Pulverbeschichtung usw. |
| 50 | | | Stückrückmeldung mit gut und schlecht Stück  |
| 51 | | | Fehlerbegründungserfassung als Zwang bei den Schlechtstück |
| 52 | | | Foto bei Schlechtstück hinterlegbar |
| 53 | | | Abfrage von Stückrückmeldung ist ein Parameter  |
| 54 | | | Die Stückrückmeldung kann jederzeit abgebrochen werden (muss man nicht extra aktivieren) |
| 55 | | |  |
| 56 | | |  |
| 57 | | |  |
| 58 | | |  |
| 59 | | |  |
| 60 | | | $PLUS Buchung: (siehe oben erweiterte Mehrmaschinenbedienung)<br>Es werden mehrere unterschiedliche oder gleiche!! Arbeitsgänge auf unterschiedlichen Maschinen gemeinsam gestartet. |
| 61 | | | $PLUS Buchung: Es können jederzeit einzelne Arbeitsgänge mit unterschiedlichen Maschinen hinzugefügt werden oder auch gestoppt werden.  |
| 62 | | | $PLUS Buchung: Beim Stop eines einzelnen AGs müssen die Gut/Schlecht-Stück eingegeben werden. |
| 63 | | |  |
| 64 | | |  |
| 65 | | |  |
| 66 | | |  |
| 67 | | |  |
| 68 | | |  |
| 69 | | |  |
| 70 | | | Terminal läuft Bildschirm füllend, sodass man unter keinen Umständen (nur über Password) ins Betriebssystem kommt, oder man kann die Anzeigegröße definieren, sodass auch andere Programme auf der Hardware laufen und bedient werden können.  |
| 71 | | | Einbindung Anwesenheitsliste als Bildschirmschoner, aber auch Anzeige von Firmeninformationen, also anderer Weblink oder ....<br>man baut in die Anwesenheitsliste die Zusatzinfos ein. Es ist aber in der Regel kein Platz dafür (bei den kleinen Terminals) |
| 72 | | | Anzeige der tatsächlich laufenden bzw. NICHT laufenden Maschinen |
| 73 | | | Rückmeldung wenn der Mitarbeiter GEHT bucht und seine Soll-Stück-Zeit noch nicht erreicht hat. Ist irgendwie böse, muss aber trotzdem motivierend rüber kommen. Muss auf jeden Fall, auch vom Inhalt her einstellbar sein. |
| 74 | I | | Anzeige des Rechner-Namens, der IP und ... im .... damit man das Terminal vernünftig einrichten kann. Ev. kann man vom Terminal aus auch die Teamviewer ID auslesen und diese auch anzeigen. |
| 75 | | | Anzeige der anwesenden Personen als eigene Liste nach Rechten, oder mit direkter Aktivierung des Bildschirmschoners |
| 76 | | | Zeitsynchronisierung vollautomatisch und gegen die Kieselstein-ERP Server |
| 77 | | |  |
| 78 | | |  |
| 79 | | |  |
| 80 | | | Mehrere Mandanten können bei eindeutiger Ausweisnummer auf einem Terminal buchen  |
| 81 | | |  |
| 82 | | |  |
| 83 | | |  |
| 84 | | |  |
| 97 | | | Timeouts der verschiedenen Anzeigefunktionen sind einstellbar und übertragbar |
| 98 | | | Alle Barcodes sind in der Losauswahlliste auch über Buttons bedienbar |
| 99 | | | Auch die $PLUS Buchung kann über die Losauswahlliste erfolgen. Bzw. wird die einfache Mehr-Arbeitsgangerfassung mit Maschinen umgesetzt.  |




Bei wichtig bedeutet:
| Buchstabe | Bedeutung |
| --- | --- |
| I | brauchen wir initial |



Zusätzlich:
- Auf welcher Hardware läuft das ?
- auf welchen Betriebssystemen läuft das
- Anbindung der Opticon Zeiterfassungsstifte https://www.wa-consulting.at/support


- Es sollten auch ZE-Stifte angebunden werden können
- Weitere Sondertätigkeiten definierbar. D.h. der Mitarbeiter kann auch Arzt, Krank, Behörde, Kindkrank usw. am Terminal buchen
- Fehlererfassung
- Redlining
- [Papierlose-Fertigung](#papierlose-fertigung)

Hardware
- Touchbedienung, also komplett ohne weitere Zettel usw. bedienbar
  - dazu idealerweise auch integrierte RFID Leser
  - gedanklich auch Fingerprint, aber DSGVO / Gewerkschaft usw.
- Barcodescanner Bedienung
- RFID Leser, alle bisherigen und ELATEC4
- alternativ zu Touch auch Tastatur und oder Mausbedienung
  - es kommt auch immer wieder der Wunsch nach Eingabe der Personalnummer
  - aber auch Los / Auftragsnummern
  - D.h. ich stehe vor dem Terminal, gebe meine Personal oder Ausweisnummer an und danach K für Kommt, G für Geht usw.
- vorbereitet für Maschinenanbindung -> Also Maschine läuft / steht usw.
- Waagenanbindung, z.B. für das Pulver beschichten. D.h. es wird Material entnommen und auf die Waage gestellt, dann Artikelnummer und Charge gescannt und das entnommene Gewicht als Material mit der Charge ins Los gebucht.
- Anbindung von Messmitteln für die Erfassung von Prüfungen
  - z.B. Schiebelehren mit USB oder Bluethooth Schnittstelle.<br>Damit auch führen durch entsprechende Prüferfassungsdialoge
- Einlesen von Offline Zeiterfassungsstiften. Aktuell der Opticon-Reader.<br>Aus diesem Titel heraus muss es die Möglichkeit einer eindeutigen Zuordnung der verschiedenen USB Device / BlueTooth-Device zu verschiedenen Aufgaben geben. So wirkt der Opticon-Reader ganz anders als z.B. die Schiebelehre, obwohl beide über einen USB-Hub am gleichen Gerät angesteckt sind.
- Größe des Terminals je nach Einsatzfall
  - ca. 12"
  - Std 15"
  - 23" oder sogar zweiter Bildschirm für entsprechend detailreiche Zeichnungen
  - Zoom in den Zeichnungen mit zwei Finger muss möglich sein
- Das Terminal ist offline fähig. D.h. reine Datenerfassung ist auch möglich wenn keine Serververbindung. D.h. man kann Kommt/Geht buchen, Tätigkeiten starten usw., aber die Ablieferbuchung bzw. die Stückrückmeldung, Fehlererfassung oder auch Messwerterfassung ist nur Online möglich

WLan / Lan Anbindung
- Lan bevorzugt aber
- immer wieder auch WLan erforderlich

Verbindung auch per https möglich, in der aktuellen Verschlüsselungstechnik

Status-Abfragen und Infos
- Monatsabrechnung aktuell, gegebenenfalls nur bis gestern Abend oder letztes Monat
- Anzeige Urlaubs und Gleitzeit Saldo bis gestern Abend
- Urlaubs-Antrag, ZA Antrag (Krankenstands-Antrag)
- Aktueller Status eines Loses
  - Fortschrittsanzeige
- Aktueller Status eines Auftrages
- Zeichnungen und Infos des Loses, Auftrags, Artikels. Diese kommen aus:
  - Kommentar einer = bestimmter, alle, bestimmte, inkl. Dateiverweis
  - Dokumentenablage, je nach Belegart bzw. Gruppierung, Sicherheitsstufe definierbar
  Ebenfalls mehrere Belegarten, Gruppierungen
  - Dokumentenlink, auch hier angebbar welcher, z.B. nur Zeichnung und QS-Anweisung
- Wartungsanweisungen zur Maschine lesen / ansehen
  - Wartungsschritte erfassen können, mit Zeit und Material
- Terminals haben eigene Hilfe(-Seite)

Sprachen
- Deutsch
- Englisch
- vorbereitet für
  - Slowenisch
  - Italienisch
  - Polnisch

Infos zurücksenden können.<br>
D.h. der Werker stellt etwas fest und will dies an die Abteilung (welche) zurückmelden. Das kann ein Text sein, ein Foto weil ein Teil der Maschine gebrochen ist oder auch eine Zeichnung. Es hat dies einen Bezug zum Los, oder auch zur Maschine. Wie bekommt der Verantwortliche Mitarbeiter z.B. der Konstrukteur oder der Kundendisponent mit, dass was zu tun ist?

Zeit und Material erfassen können. Insbesondere Material mit Chargen und Seriennummern im kombinierten Barcode

Parameter, Buttons<br>
Alle Buttons können (am Terminal?) aktiviert bzw. deaktiviert werden. Damit verbunden sind auch ev. Funktionsaufrufe.
Zusätzlich sollte man die Konfiguration eines Terminals sehr einfach irgendwo ablegen können und dann bei einem anderen Terminal wieder einlesen können. Ziel ist, dass man nicht zig Terminals, einzeln manuell parametrieren muss. Idealerweise wird die Konfiguration beim Start des Terminalprogramms übernommen. Wobei dies wiederum durchläuft, also sollte es ein Kommando an alle Terminals geben, holt euch die Parameter neu.<br>
Aber ACHTUNG: Es gibt auch Parameter wie z.B. Maschinen-Zuordnung / Maschinengruppen-Filter, Spezielle Tätigkeiten, welche von dieser allgemeinen Parametrierung NICHT betroffen sind.
Ev. kann man am Terminal selbst dann auch definieren, den Parameter NICHT von der Zentrale aus übersteuern. Das sollte man dann z.B. farblich in der Parameteranzeige sehen.

Es sollten alle Funktionen die per Barcode verwendet werden können auch für die Los-Auswahlliste zur Verfügung stehen, vermutlich besser, als Buttons ohne Barcode zur Verfügung stehen, wobei eben die Gestaltung des Fertigungsbegleitscheines durch das Abschalten verschiedener Buttons ersetzt wird. Ev. ergibt sich daraus auch, dass man die Reihenfolge der Buttons anpassen können sollte. Für die verwendeten Barcodes [siehe]({{< relref "/docs/installation/11_barcode/#unterstützte-barcodefunktionen" >}}), Spalte ZE-Terminal

Hier kommt dazu, dass die einfache [Metallverarbeiter-Erfassung](#einfache-metallverarbeiter-erfassung) umgesetzt werden sollte.

weitere Feature-Wünsche
- Losauswahlliste inkl. Filterung nach Maschine bzw. Maschinengruppe
  - Maschinen(gruppen)filter jederzeit umschaltbar<br>
  Bedienung zweistufig mit Maschinengruppe und eingerückt die Maschinen dazu.
  - In der Losauswahlliste sollte man (fast) alle Knöpfe als Terminaleigenschaft ein- und ausschalten können, da je nach Standort das Terminal eben verschiedene Funktionalitäten hat.<br>So ist der Trick, dass auch die Materialvorbereitung faktisch eine Maschine ist. Auf dem Terminal (das in der Materialvorbereitung steht) ist die Materialentnahme auf das Los aktiviert und ev. die Zeichnungen, auf allen anderen Terminals nicht.
  - Man sollte in der Los-Auswahlliste durch scann der Losnummer ($L..) einen Filter auf die Arbeitsgänge dieses Loses haben, damit man sieht was noch offen ist.
  Zusätzlich kann man dann damit die Zeichnungen des Loses (eigentlich des Stücklistenartikels) anzeigen lassen.
  - Idealerweise kann man auch durch Tipp auf einen speziellen Button, die Losnummer manuell in der üblichen bzw. vollen Schreibweise eingeben.
- Wenn man im Ordner der Dokumentenablage arbeitet und der Ordner ist leer, so muss man trotzdem im Ordner bleiben (und nicht ganz rausfliegen). Ev. den Ordner (Zeichnung) nur in grau angezeigen und man kann nicht reinspringen, und oder es steht dahinter (leer). So dass man gar nicht weiter springt.
- immer wieder die Vorstellung, dass man billige Android Tabletts verwenden kann. Wenn man die Kamera verwendet, so kann man damit den Mitarbeiter Barcode scannen und der Rest geht über das Touch. Ev. doch so, dass man, wie am Handy, mit einer Taste das Tablett aktivieren muss und dieses automatisch nach einigen Minuten wieder in Standby geht.
- Wunsch nur 12" Terminal
  - Zeichnungen des Teils ansehen.
  - Eigentlich interessiert nur die Laufzeit der Maschine und die damit gefertigte Stückzahl.
  - D.h. es sollte
    - eine Zuordnung zur Maschine geben
    - eine Losauswahl was genau gefertigt wird / zu fertigen ist
    - einen Zeichnungs-Knopf -> wichtig ist in der Darstellung, dass man da mit den Fingern zoomen kann
    - einen Start-Stop Button nur für die Maschinenzeit (das Terminal weiß an welcher Maschine es ist)
    - ob die mit der Menschzeit dann gekoppelt ist ?
- die Geisterschicht. D.h. 
  - Maschine wird von Mensch gestartet.
  - Mensch geht heim
  - Maschine schaltet sich nach xx Std ab. Wie / mit welchen Tools bekommen wir das mit?<br> Eine Idee war einmal, einen kleinen Lichtsensor auf die gn/gb/rt Laufanzeige zu hängen. Der Sensor, eigentlich das Terminal, weiß welche Maschine das ist und wenn dann der Stop kommt und länger als xx Sekunden ansteht, wird die Maschine gestoppt.<br>
  Einen Start muss dann ja eh wieder ein Mensch machen.<br>
  Wichtig wäre, dass der Stop auch am Terminal sichtbar ist.
  D.h. wir wissen ja, das Terminal ist nur für Maschine x und könnten da, bei den Kommt/Geht Buttons auch anzeigen auf welches Los/AG die Maschine gerade läuft, oder sie steht eben.
  wäre z.B. eine zusätzliche Zeile (zwischen Uhr und Buttons)<br>
  und wenn sie steht, dann Rot, steht seit .... Uhrzeit und Dauer
- Man braucht am Terminal die Möglichkeit, dass der Mitarbeiter am Arbeitsgang buchen kann, das sind xx Schlecht Stück und damit wird ab diesem Arbeitsgang die Losgröße reduziert, ohne dass Material zurückgegeben wird.<br>
Zusätzlich muss er dann den Fehlergrund mit angeben.<br>
Trotzdem braucht man eine Statistik was alles als Schlecht gebucht wurde, also  Material mit Zeit vernichtet wurde. Das muss dann als täglicher Managementsummary an die QS-Leitung und an die GL etc.. Diese vernichteten Werte gehören natürlich auch dargestellt.
Beim Anwender wurden in 2022 1Mio € auf Schrott gebucht worden.
- Am Terminal sollte es auch eine Hilfe geben, die man z.B. über den Browser darstellen kann. Es darf aber sonst keine weitere Webseite aufgerufen werden können. Vermutlich kommt dann sofort der Wunsch, dass hier auch Firmeninformationen angezeigt werden. Ob dies ein Feature für den Bildschirmschoner ist, muss definiert werden.

### Einfache Metallverarbeiter Erfassung
Gerade für die Metallverarbeiter, also reine Lohnfertigung ist die Herausforderung, dass man zu Beginn des Tages nicht weiß, welche Aufträge noch dazu kommen. So beginnt man mit einem Arbeitsgang auf einer Maschine, bekommt dann 2Std später einen weiteren Fertigungsauftrag, also einen weiteren Arbeitsgang hinzu, dann einen dritten. Dann ist der erste AG fertig und man starten einen weiteren usw. usf..
Am Ende des Tages sollte das System errechnen wieviel Maschinen- und Menschzeit auf welchen Arbeitsgang zu buchen ist. Dafür ist sicherlich auch eine Erweiterung des Kernels erforderlich. [Siehe]({{< relref "/fertigung/zeiterfassung/ze_terminal/einfache_mehrfacherfassung" >}})

### Papierlose-Fertigung 
Ein Wort zu diesem sehr modernen Gedanken.<br>
Es besteht der berechtigte Wunsch, dass die Mitarbeiter am Terminal sehen, was denn der nächste zu erledigende Arbeitsschritt / Auftrag ist.<br>
Parallel dazu besteht die Forderung aus der Qualitätssicherung, dass jedes Teil, ob Halbfertig-Produkt oder Rohteil, welches sich in der Fertigung befindet, so eindeutig gekennzeichnet ist, dass man jederzeit weiß was das ist und idealerweise auch in welchem Zustand.
Aus meinen bisherigen Erfahrungen werden wir daher noch länger zumindest <u>einen </u> Fertigungsbegleitschein haben, der sich beim Teil befindet. Auf diesem Papier, welches idealerweise nur eine Din A4 Seite hat, wird zumindest der Barcode des Loses gerne in QR-Code, aufgedruckt sein. Womit man natürlich auch den Wunsch produziert, durch scannen dieses QR-Codes, sehe ich am xxx-Device, was das ist und in welchem Status.<br>

Ein weiterer Punkt der hier immer kommt ist, wie plant man die Aufträge, was denn wirklich nun zu tun ist. Meine persönliche Meinung, die inzwischen doch von etlichen Anwendern bestätigt wird ist, es zählt einzig der Termin, des Arbeitsganges. Eventuell macht man innerhalb der Arbeitsgänge eines Tages noch eine weitere Reihung, sodass man quasi die Zeit / Stunde definiert wann der Mitarbeiter damit beginnen sollte. Alles andere, wie Priorisierung usw. läuft im Endeffekt wieder auf eine gute Terminsteuerung hinaus. Ein Thema das hier dazu gehört ist die Kapazitätsplanung bzw. die automatische Einplanung von Arbeitsgängen, siehe dort.

Zeiterfassungsterminal<br> ![](Terminal_Asus.png)
==============================================

Mit den Zeiterfassungsterminal steht eine komfortable Zeiterfassung zur Verfügung. Sie wurden vor allem für die einfache praktische Zeiterfassung in Produktionsbetrieben geschaffen.
Die Terminals sind in der Regel mit einem Touchbildschirm ausgerüstet und werden üblicherweise in Kombination mit Barcodescannern und oder RFID-Lesern für die Auftragserfassung bedient.
Die Geräte bieten eine Montagemöglichkeit mittels Standfuß oder Wandhalterung. **Hinweis:** Die Terminals werden mit Wandhalterungen geliefert.
Bitte behandle die Terminals entsprechend sorgsam. Siehe dazu auch [Betriebsumgebung](#Betriebsumgebung). **Info:** Die Kommunikation wird immer von den Terminals zum Server hin angestoßen.

Bedienung des Zeiterfassungsterminals:
====================================

Neben den nachfolgend beschriebenen Funktionalitäten sind diese Geräte auch [Offline](#offline-modus) fähig.

Bevor eine Buchung gemacht werden kann, müssen Sie sich zuerst identifizieren.
Dies erfolgt mittels Barcodescanner, mit dem die Ausweisnummer des Mitarbeiters gelesen wird. Die Ausweise können im Modul Personal [ausgedruckt](../personal/index.htm#Personalliste) werden.

Wartet das Terminal auf einen Ausweis, so wird <Bitte Ausweis lesen> angezeigt.
![](Nettop2.jpg)

Konnte Ihr Ausweis gültig gelesen werden, so wird Ihr Name angezeigt.

Wählen Sie nun eine der Möglichkeiten durch antippen des entsprechenden Symbols z.B. mit dem Finger:

**KOMMT**

Mit Kommt beginnen Sie (Diese Möglichkeit kann abgeschaltet werden. Für Personen spezifische Abschaltung siehe [Personal](../personal/index.htm#Personen Parameter))

**GEHT**

Mit Geht beenden Sie Ihre Tagesarbeit. Bevor das eigentliche Geht gebucht wird, wird die Stückrückmeldung (siehe unten STÜCK) durchgeführt.

**Pause**

Mit Pause gehen Sie in die Pause und kommen auch mit Pause aus dieser wieder zurück. (Kann abgeschaltet werden)
Die obigen Zeiten können immer nur zum aktuellen Zeitpunkt gebucht werden.

**SALDO**

Mit Saldo wird Ihr aktueller Gleitzeitsaldo und der aktuelle Stand Ihres Urlaubsanspruchs in Tagen dargestellt. Bitte beachten Sie dass dies die Berechnung zum Stichtag des Vortages 24:00 ist. Verbindliche Unterlagen erhalten Sie immer nur von Ihrer zuständigen Stelle.

Diese Anzeige verschwindet nach fünf Sekunden wieder, da dies nur Ihrer persönlichen Information dient.

![](Nettop9.gif)

Mit dieser Anzeige können Sie auch die Abrechnung des aktuellen Monates bzw. des Vormonates einsehen, in dem Sie auf die entsprechenden Knöpfe tippen.

**MASCHINEN**

Durch Tipp auf Maschinen sehen Sie, welche Maschinen aktuell von Ihnen gestartet sind

**STÜCK**

Durch Tipp auf Stück können Sie auch während des Tages die Stückrückmeldung für Ihre offenen (Umspann-) Arbeitsgänge veranlassen. Damit könnte Sie z.B. immer wenn ein Transportbehälter voll ist, diese Stückzahl bereits an das System melden. Damit müssen Sie bei Schichtwechsel nur mehr die abschließende Teil-Menge des zuletzt begonnenen Transportbehälters angeben.
Bitte beachten Sie, dass es, um die Stückrückmeldungen zum Zeitpunkt zu erfassen, erforderlich ist, dass wir dafür eine sehr kurze Unterbrechungsbuchung bei diesem Mitarbeiter einfügen müssen. D.h. Sie werden bei diesen Buchungen zwei Unterbrechungs-/Pausenbuchungen mit einem Zeitversatz von einer Sekunde in den Zeitbewegungen der Mitarbeiter vorfinden. **Hinweis:** Werden alle drei Stückzahlen mit 0 (Null) befüllt, so wird keine Stückbuchung gemacht, da ja keine brauchbare Information eingegeben wurde.
Die Stückmeldung kann durch Tippen auf das rote X abgebrochen werden. Siehe dazu auch Parameter Stückrückmeldung abbrechbar.

**ANTRAG**

Durch Tipp auf Antrag kann ein Urlaubs- oder Zeitausgleichsantrag direkt vom Terminal aus gestellt werden.
Bitte beachten Sie für die Versendung des Antrags die Regeln wie unter [Urlaubsantrag](index.htm#Urlaubsantrag) beschrieben.

**Lose**

Durch Tipp auf Lose werden die offenen Arbeitsgänge angezeigt. Hier kann die Buchung, ohne Fertigungsschein durchgeführt werden (Papierlose Fertigung). [Siehe](#Direkt auf Lose buchen).

**Maschine**

Auswahl einer für die Arbeitsgangbuchung abweichenden Maschine. [Siehe](#Maschinenauswahl).

Erfassung der Arbeitsgänge

Die wesentlichste Bedienung des Terminals erfolgt durch Scannen von Arbeitsgängen Ihres Fertigungsscheins.

Beachten Sie dazu, dass das Terminal auf Arbeitsgänge und Unterarbeitsgänge ausgelegt ist.

Es muss dafür ein entsprechend konfigurierter Fertigungsschein verwendet werden.

![](Nettop11.gif)

Der Ablauf der Buchung wird von der Type des Arbeitsgangs, welcher im Fertigungsauftrag definiert wird, bestimmt.

Es können zwei Arbeitsgang Arten bebucht werden:

a.) Keine Arbeitsgang Art definiert.

Dies bewirkt, dass mit dem Scannen des Barcodes die Arbeit auf diesem Arbeitsgang (des Loses auf der Maschine) begonnen wird. Es wird dieser Arbeitsgang durch Scann eines gleichwertigen Arbeitsgangs beendet.

b.) Arbeitsgang Art Umspannzeit

Die Arbeitsgangart Umspannen wird so verwendet, dass diese immer parallel zu anderen Arbeitsgängen verwendet werden kann. Bei den Zeitauswertungen werden diese Zeiten nur insofern berücksichtigt, dass nur die theoretische Zeit (Menge x Sollzeit) berücksichtigt wird.

Das bedeutet, dass auch mehrere Arbeitsgänge der Art Umspannzeit zugleich angestempelt werden können.

Wird nun ein Arbeitsgang beendet (a.) ) oder bucht der Mitarbeiter Geht, so muss für jede angemeldete Arbeitsgang Art Umspannzeit die gefertigte Menge angegeben werden.

Da es in der Praxis immer wieder vorkommt, so auch im obigen Beispiel, dass mehrere Arbeitsgänge der Arbeitsgangart Umspannen auf einem Arbeitsgang durchzuführen sind, aber doch die unterschiedlichen Stückzahlen erfasst werden müssen, haben wir die Logik eingeführt, dass wenn auf einen der Umspannarbeitsgänge ein Barcode eingelesen wird, so werden automatisch alle Arbeitsgänge der Type Umspannzeit (UZ) eingebucht, was bewirkt, dass bei der Stückmeldung für alle Arbeitsgänge die gefertigten Mengen eingegeben werden müssen.

![](Nettop12.jpg)

Geben Sie hier die gefertigte Gut-Menge, die gefertigte Schlecht/Ausschuss-Menge und die noch in der Maschine befindliche InArbeit Menge an. Ist der Arbeitsgang abgeschlossen, so haken Sie noch Fertig an.

Die Bedienung ist so, dass Sie die Ziffern mittels Nummernblick eintippen und danach auf Enter tippen. Damit springt die Erfassung in das nächste Feld. Geben Sie auch bei Schlecht eine oder hoffentlich eine Stückzahl von 0 ein, dann mit Enter weiter zu InArbeit. Auch hier muss die Menge eingegeben werden. Nun haken Sie noch gegebenenfalls den Fertig Status des Arbeitsgangs durch Tipp auf den Haken ![](Nettop13.gif) an. Damit ist die eine Erfassungszeile abgeschlossen und die Eingabemaske wird auf die nächste Zeile gestellt. Sind alle Felder erfasst, so kann durch Tipp auf den nun grünen Haken ![](Nettop14.jpg) dies bestätigt werden.

Bitte beachten Sie, dass Ihre tatsächliche Buchung erst nach der Erfassung gebucht wird, also zu dem Zeitpunkt wenn Sie auf den grünen Haken tippen.

Bitte beachten Sie dazu auch die [Arbeitsgang Arten]({{< relref "/warenwirtschaft/stueckliste/arbeitsplan" >}})

Wenn die Funktion offene Menge anzeigen aktiviert ist, so wird bei der Arbeitsgangbeginnbuchung die offene Menge (Noch zu fertigen) angezeigt.

![](terminal_offene_Menge.JPG)

### Automatische Laufzeitbuchungen

Gemeinsam mit den Stückrückmeldungen für die Umspannzeiten werden bei aktivierter theoretischer Istzeit (THEORETISCHE_IST_ZEIT_RECHNUNG) automatisch auch die Stückrückmeldungen (Gut/Schlecht-Stück) auf die Maschinenlaufzeiten gebucht. Es wird dies nur beim ersten Unterarbeitsgang des (Haupt-)Arbeitsganges der als Arbeitsgangart Umspannzeit definiert ist gebucht und geht nur auf den ersten Unterarbeitsgang des Hauptarbeitsgangs der als Laufzeit definiert ist. Es wird immer nur der jeweils erste Unterarbeitsgang als Auslöser / Ziel verwendet.

### PDFs am Zeitbuchungsterminal anzeigen

Wenn ein Pfad hinterlegt ist, gibt es einen Knopf (PDF-) Anzeige. Hier können Sie zum Beispiel Zeichnungen zu Produkten hinterlegen und anzeigen.
Wird auf das Icon getippt, so wird ein Dateiauswahldialog von einem voreingestellten Verzeichnis, welches auf Ihrem Server liegt angezeigt (Unterordner und PDF-Dateien).
Nun wählen Sie das gewünschte Unterverzeichnis (Touch-Bedienung) je nach Ebene, und danach das darin liegende PDF zur Anzeige. 

Nach 10Minuten Untätigkeit wird die Datei automatisch geschlossen.

### Losgrößenänderung am Terminal buchen

In der Kunststofffertigung ist es oft so, dass das Material fertig gespritzt wird. D.h. der Mitarbeiter geht am Ende des Spritzvorganges zur Maschine und liest die gespritzte Menge ab. 
Da damit nur bekannt ist wieviel Teile die Maschine gemacht und damit Material, Zeit, Energie verbraucht hat, ist das die Ausgangsbasis für die weiteren Betrachtungen.
Mit dem Barcode $GROESSEAENDERN (eigentlich AusweisNr, LosNr, $GROESSEAENDERN) erscheint ein nummerisches Eingabefenster in dem die neue Losgröße angeben wird. 
Wird die Losgröße reduziert, so wird das überschüssige Material zurück gegeben. Wird die Losgröße erhöht wird das Mehr-Material, soweit möglich entnommen, der Rest wird zur Fehlmenge. 

### Maschinenänderung am Terminal buchen

Wenn eine andere Maschine, als definiert verwendet werden soll, so buchen Sie bitte wie folgt: 
Scannen Sie wie gewohnt Ihre Ausweisnummer, danach den Maschinencode (\$M+Maschinennummer). <br>Eine Liste der Maschinen finden Sie im Modul Zeiterfassung, unterer Reiter Maschinen, oberer Menüpunkt Journal. Falls hier keine Barcodes angedruckt werden, wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer um dies für Sie einzurichten.<br>
Nun können Sie wie gewohnt den kombinierten Code ($V+Daten) von Ihrem Fertigungsbegleitschein scannen und die Zeit wird auf die gewünschte Kombination aus Losnummer, Arbeitsgang und Maschine gebucht.

### Istzeiten am Terminal buchen

Wenn das Modul Stückrückmeldung nicht vorhanden ist, kann vom Terminal eine Losbuchung mit Tätigkeit (Code $W) ohne Positionsbezug gebucht werden.
So können Sie die Istzeiten auf gewissen Tätigkeiten je Los buchen und erhalten damit eine Zeitmessung mit der Aufteilung auf einzelne Tätigkeiten.
Bitte wenden Sie sich an Ihren **Kieselstein ERP** Betreuer um die jeweiligen Tätigkeiten im Fertigungsbegleitschein im kombinierten Code für das Los zu hinterlegen.

Q: Können auch Barcodes für Kommt und Geht verwendet werden?

A: Ja, es werden die Barcodes der Sondertätigkeiten wie Kommt, Geht usw. unterstützt. Voraussetzung ist, das die jeweilige Sondertätigkeit auch für die BDE-Station freigegeben ist. (Siehe Zeiterfassung, Sondertätigkeiten, Auf BDE Station buchbar)

Q: Wie vorgehen, wenn ich mitten unter der Buchung feststelle, das ist das falsche Los?

A: Beginnen Sie einfach wieder von vorne mit dem einscannen der Ausweisnummer. Dadurch werden alle bisherigen Eingaben verworfen.

Q: Kann auch die Fertig-Buchung eingescannt werden?

A: Ja. Bitte beachten Sie hier die Reihenfolge:

1.) Ausweisnummer

2.) Den Fertig Code

3.) die Losnummer

4.) Die Tätigkeit, wenn auf dem Terminal nicht eine Default-Tätigkeit eingestellt ist.

Bitte beachten Sie dazu auch die Einstellung "Lose mit Auftragsbezug".

Q: Kann ich eine Los-Ablieferungsbuchung machen?

A: Ja. Scannen Sie dazu:
- 1.) Ausweisnummer
- 2.) die Losnummer
- 3.) den Ablieferungs Code

Wichtig: Bei dieser Buchung ist eine Überlieferung der offenen Losmenge möglich. Wird das Los überliefert, so werden die Solldaten nachgezogen, siehe dazu auch [Losgröße ändern](../Fertigung/index.htm#Ändern der Losgröße). Auch wenn die Losmenge erfüllt bzw. überliefert wird, wird der Status des Loses nie auf (vollständig) erledigt gesetzt. Dies dient dazu, dass die Steuerung ob auf ein Los noch gebucht werden kann oder nicht, manuell erfolgen muss (zumindest sind unsere Kunden derzeit dieser Meinung).
Ob überliefert werden darf, kann im Terminal und in den Kopfdaten der Stückliste definiert werden.

Q: Anzeigen des Artikelstammblattes

A: Mit dem Barcode $X0artikelnummer wird das Stammblatt des Artikels am Terminal angezeigt. Dies kann durch Anpassung des Reports für spezielle Infos wie z.B. Zeichnungen, Lagerstände und ähnliches verwendet werden

Q: Anzeigen des Losfortschrittes, der aktuellen Nachkalkulation

A: Mit dem Barcode $X1losnummer wird der Druck der aktuellen Nachkalkulation des Loses aufgerufen. Je nach Firmenphilosopie sollten darauf die Werte / Kosten angezeigt werden oder nicht. Gegebenenfalls ist der Druck der Nachkalkulation entsprechend anzupassen.

Q: Bei der Ablieferbuchung werden nicht lagerbewirtschaftete Artikel nicht gebucht?

A: Die Terminals haben üblicherweise eine spezielle Rolle. Bitte stellen Sie sicher, dass diese Rolle auch auf das Lager KEIN LAGER buchen darf.

Q: Kann ich am Terminal sehen wer anwesend ist?

A: Ja. Durch Scannen / Lesen Ihres Ausweises und nachfolgendem Tippen auf ![](Anwesenheitsliste_anzeigen.jpg) werden abhängig von Ihren Berechtigungen die Anwesenheitszeiten angezeigt. Für die Einstellung was angezeigt werden sollte siehe bitte im Personal, Daten, Anwesenheitsliste für Abteilung bzw. Anwesenheitsliste für alle am Terminal anzeigen.

Q: Ich wollte gar nicht buchen, wie kann ich das verwerfen?

A: In einigen Anwendungen sind die Timeouts für das Verlassen der Anzeigen lange eingestellt.
Nun kann es sein, dass jemand seinen Barcode einscannt, dann aber nichts weiter macht, quasi davon läuft.
Damit nun nicht eine andere Person sich als der vorhin am Terminal buchende ausgibt, sich seinen Zeitsaldo ansieht, gibt es die Zurück-Funktion.
D.h. wenn Sie z.B. nach dem Scannen Ihres Ausweises nichts weiter machen möchten, so tippen Sie idealerweise auf
![](Nettop_zurueck.jpg) zurück. Damit wird auch die Anzeige Ihres Namens wieder gelöscht.

Q: Kann die Liste der geladenen Personen aktualisiert werden?

A: Mit dem Button Personal-Refresh ![](Personal_Refresh.jpg), kann die Liste der geladenen Personen aktualisiert werden.
Dies ist einerseits interessant wenn die Anmeldeseite verwendet wird und andererseits, wenn in Ihrem Unternehmen aus verschiedensten Gründen, der **Kieselstein ERP** Server, z.B. über Nacht, abgeschaltet wird.

Q: Wie wird die Uhrzeit des Terminals eingestellt?

A: Die Uhrzeit des Terminals wird automatisch beim Starten und danach jede Stunde mit der Uhrzeit des **Kieselstein ERP** Servers synchronisiert. Achten Sie daher auf eine gültige Uhrzeit Ihres **Kieselstein ERP** Servers. [Siehe dazu auch]( {{<relref "/docs/installation/13_zeitserver">}} ).

Q: Welche Rechte benötigt das Terminal um Buchungen durchführen zu können?

A: Damit das Terminal seine Aufgabe erfüllen kann müssen mindestens folgende Rechte gegeben sein:
- FERT_LOS_DARF_ABLIEFERN
- FERT_LOS_R
- PERS_ZEITEREFASSUNG_CUD
- PERS_ZEITERFASSUNG_R
- PERS_ZEITERFASSUNG_REPORTS_SEHEN

Bitte bedenken Sie auch, dass dieser Rolle ein Recht auf das benötigte Lager gegeben werden muss. In der Regel wird das das Hauptlager sein. Es müssen jedenfalls alle vom jeweiligen Standort aus bewirtschafteten Ziellager gebucht werden dürfen.

Alternativ / ergänzend werden folgende Rechte definiert:
- ANGB_ANGEBOT_R
- AUFT_AUFTRAG_R
- FERT_LOS_DARF_ABLIEFERN
- FERT_LOS_R
- LP_DARF_EMAIL_SENDEN
- PERS_ANWESENHEITSLISTE_R
- PERS_SICHTBARKEIT_ALLE
- PERS_ZEITEREFASSUNG_CUD
- PERS_ZEITERFASSUNG_R
- PERS_ZEITERFASSUNG_REPORTS_SEHEN
- PROJ_PROJEKT_R
- WW_ARTIKEL_R

Q: Können in der Terminalablieferbuchung Geräte mit Chargen- oder Seriennummern gebucht werden?

A: Am Terminal ist die Erfassung von Seriennummern bei der Ablieferung möglich. Bitte beachten Sie, dass Chargennummern derzeit bei der Ablieferung nicht erfasst werden können.
Je nach Größe bzw. Gewicht der produzierten Stücklisten / Produkte kann es deutlich praktischer sein, dafür [mobilen Geräte]( {{<relref "/extras/Mobile_App">}} ) zu nutzen. D.h. man geht mit dem mobilen Barcodescanner zum Artikel.

Die Los-Ablieferbuchung erkennt automatisch anhand der Stückliste, dass dafür Seriennummern erfasst werden müssen. D.h. es erscheint bei der Erfassung der abgelieferten Menge die nachfolgende Seriennummern-Liste.
![](Seriennummernliste_leer.jpg)

Wenn nun die Seriennummern mit dem Barcodescanner erfasst werden, so scannen Sie einfach der Reihe nach die Seriennummern der abgelieferten Artikel.
Für die manuelle Eingabe von Seriennummern tippen Sie auf das grüne Plus ![](Seriennummern_Plus.jpg).
Im nachfolgenden Dialog
![](Seriennummernerfassungsliste.jpg)
tippen Sie zuerst auf einen weiteren Eintrag hinzufügen ![](Weitere_Seriennummer_erfassen.jpg)
und geben danach die Seriennummer ein.
Um die nächste Seriennummer zu erfassen, tippen Sie erneut auf einen weiteren Eintrag hinzufügen.
Bei der Erfassung können sowohl Zeichen als auch Ziffern eingegeben werden.
Bitte achten Sie auf die gültigen Zeichen für Seriennummern.
Um eine eventuell falsche Seriennummer aus der Liste zu löschen, wählen Sie ![](Erfasste_Seriennummer_loeschen.jpg).

Beenden Sie die Erfassung durch einen Tipp auf den grünen Haken.
Nun erscheint die komplette Liste der erfassten Seriennummern.
![](Seriennummern_kontrolle.gif)
Ein erneuter Tipp auf den grünen Haken vervollständig die Seriennummernablieferbuchung.
Sollten Sie bei der Prüfung feststellen, dass eine Seriennummer fehlt, so kann mit dem grünen Plus wieder in den Seriennummernerfassungsdialog gewechselt werden und die entsprechenden Korrekturen durchgeführt werden.
Sollte bei der Erfassung versehentlich eine bereits früher zugebuchte Seriennummer erfasst werden, wird die Buchung verweigert und eine entsprechende Fehlermeldung LAGER_SERIENNUMMER_SCHON_VORHANDEN ![](Seriennummer_schon_vorhanden.jpg)
angezeigt, womit die Ablieferbuchung verworfen wird.

### Wie wird ein Urlaubsantrag am Terminal gestellt?
A: Scannen Sie Ihre Ausweisnummer und tippen Sie auf Antrag.
![](Urlaubsantrag1.jpg)
Nun wischen / streichen Sie mit dem Finger über die Tage für die Sie einen Urlaubs-/Zeitausgleichsantrag stellen möchten.
Wählen Sie dann aus ob Urlaub- oder Zeitausgleich und bei eintägigen Einträgen auch noch ob das eventuell nur eine Antrag für einen halben (Zeitausgleichs-)Tag sein sollte.
Nun Tippen Sie noch auf den nun grünen Haken ![](Urlaubsantrag2.jpg) um den Eintrag abzusenden.

## Terminalkonfiguration

Hinter dem **Kieselstein ERP** ![](kieselstein_icon_klein.png) Icon finden Sie die Konfiguration des Terminals. Diese ist üblicherweise mit einem Kennwort geschützt.

Als Standard Passwort geben Sie bitte 3145 ein und tippen auf den Ok Knopf.

![](Nettop3.jpg)

**Wichtig:** Ändern Sie umgehend dieses Defaultpasswort zu Ihrer eigenen Sicherheit.

Nun steht Ihnen folgende Optionen zur Verfügung.

![](Nettop4.jpg)

![](Nettop15.jpg)

![](Nettop19.jpg)

![](Nettop16.jpg)

![](Nettop17.jpg)

![](Nettop18.jpg)

| Reiter | Eintrag | Beschreibung |
| --- |  --- |  --- |
| **Barcode** | Port Leser: | An welchem Port ist der serielle USB Barcode Scanner bzw. ein RFID Leser angeschlossen.Hinweis: Zur Installation des Seriellen USB Barcode Leser muss gegebenenfalls ein entsprechender Treiber installiert werden. Installieren Sie den Treiber zuerst. Prüfen Sie seine Funktion mit einem Terminalprogramm, z.B. Hyperterm.Erst danach sollte des Terminalprogramm gestartet werden. |
|   | Lesertyp: | Geben Sie den Lesertyp / die Hardware an die am Terminal angeschlossen ist. Derzeit sind verfügbar:BarodePROMAG MF7V&B 55MPROMAG PCR300ELATEC TWN3 (kann auch für den ELATEC TWN4 verwendet werden)Diese Leser werden immer über einen USB-Com-Port angesprochen. Daher muss oben der dazupassende Com-Port ausgewählt sein. Siehe dafür auch ev. Gerätesteuerung. |
|   | RFID Order: | Wurde ein RFID Leser angegeben so ist die Lesereihenfolge zu definierenLSB ... Least Signifikant Bit (zuerst)MSB ... Most Signifikant Bit (zuerst) |
|   | Internes Leser-Modul | Wenn vorhanden, bitte den entsprechendenUSB-Com-Port angeben. |
|   | Modultyp intern: | Geben Sie die Hardware an, welche für den internen Leser definiert ist. Derzeit verfügbar:RWD-MifareSIMONS_VOSSPROMAG_PCR300ELATEC_TWN3 (auch für TWN4)WICHTIG: Wird nur der RFID Leser verwendet, so muss dies unter Port Leser konfiguriert werden und das interne Leser-Modul auf nicht vorhanden gestellt werden. |
|   | Anmeldeseite: | Ist dies angehakt, so wird eine Button-Liste aller Personen angezeigt die buchen dürfen. Damit wird der Barcodeleser hinfällig. Setzen Sie dies nur bei absolutem Vertrauen in alle buchenden Personen ein. |
|   | Kommt/Geht Schnellerfassung | Anstelle des Ablaufes Ausweis, Tätigkeit, wird dieses Terminal nur als Buchungsterminal für die Anwesenheit verwendet. D.h. es wird zuerst Tätigkeit, Person gebucht, wobei die Tätigkeit (Kommt/Geht/Pause) wie eingestellt / vom Vorgänger verlassen verwendet wird. |
|   | Default Gehtfür Schnellerfassung | Üblicherweise steht das Terminal bei der Kommt/Geht Erfassung auf Kommt. Ist dies aber ein "Geht" Terminal, so haken Sie bitte default Geht an, so springt es immer, nach dem Timeout, auf Geht zurück. |
|   | Timeout (Geht)für Schnellerfassung | Wenn, bei Einstellung auf Kommt, die Person vor Ihnen Geht ausgewählt hat, so springt nach der Zeit der Knopf wieder auf Kommt zurück. Gilt analog auch für Pause bzw. wenn Default Geht.Ist hier 0 eingetragen, so ist die Rücksetzfunktion abgeschaltet. Der Knopf bleibt wie vom Vorgänger gewählt. Siehe um Mitternacht. |
|   | Sperrzeit Doppelbuchung:für Schnellerfassung | Da es immer wieder vorkommt, dass Buchungen nicht bewusst gemacht werden, wird damit verhindert, dass eine Person die gleiche Buchung innerhalb der angegebenen Zeit ein zweites Mal an den Server sendetSiehe dazu bitte auch Parameter [VORHERIGE_ZEITBUCHUNG_LOESCHEN](#Doppelbuchung vermeiden)mit dem ein ähnliches Verhalten erreicht werden kann. |
|   | um Mitternacht für Schnellerfassung | Ist diese Funktion aktiviert = angehakt, so wird kurz nach Mitternacht der aktivierte Knopf in jedem Falle auf Kommt zurückgestellt. |
|**Kieselstein ERP** | Server: | Definieren Sie hier an welcher IP-Adresse bzw. unter welchem Namen das Terminal Ihren **Kieselstein ERP** Server findet. |
|  | Mandant: | Für welchen Mandanten die Buchungen durchgeführt werden sollten. |
|   | Personal filtern: | Nur die Personen des angegebenen Mandanten ins Terminal laden |
|  | Locale: | Welche Sprache im Sinne Ihrer **Kieselstein ERP** Installation verwendet werden sollte. Siehe dazu [Locale](#Locale). |
|   | Stellen Belegnummer: | Anzahl der Belegnummer passend zur Einstellung Ihres **Kieselstein ERP** |
|   | Jahr: | Die Anzahl der Jahresstellen Ihrer **Kieselstein ERP** Belegnummern |
|  | Maschinen-identifikation | Maschinenidentifikationsnummer für die Liste der offenen Arbeitsgänge |
|  | Kommt buchen | Kommt Buchung am Terminal erlaubt ?<br>Hinweis: Dies ist auch ein Parameter in den Personaleigenschaften. D.h. es kann auch pro Mitarbeiter:inn deaktiviert werden. |
|  | Pause buchen | Pause (Unterbrechung) Buchung am Terminal erlaubt ? |
|  | Nachkomma-stellen | Anzahl der eingebbaren Nachkommastellen bei der Gut/Schlecht Meldung. 0 ... auch kein Komma möglich |
|  | Default Tätigkeit | Diese Tätigkeit wird sofort nach der Losnummer als Tätigkeit auf diesem Arbeitsplatz gebucht. |
|  | Lose mit Auftragsbezug | Ist dies angehakt, so wird bei der Erfassung von 12stelligen Losnummern ausgegangen. Weiters wird der kombinierte Barcode ($V) mit Arbeitsgangnummern erwartet. Ist dies nicht angehakt, so wird von 10Stelligen Losnummern ausgegangen. Es werden einzelne Buchungen von Losnummer und Tätigkeit erwartet. Die Tätigkeit wird nur erwartet, wenn keine default Tätigkeit definiert ist. |
|  | Stückrückmeldung | Auf diesem Terminal werden je Arbeitsgang auch die Gut-/Schlechtmengen gebucht |
|   | Anzeige offene Mg.: | Zeigt bei der Stückrückmeldung die errechnete Offene Menge anhand der Vorgänger Arbeitsgänge an. |
|  | Überliefern zulassen | Wenn angehakt:Bei der Ablieferungsbuchung erfolgt nur eine Warnung wenn die offene Menge durch die Rückmeldung überschritten wird, es können jedoch mehr als geplant zurückgemeldet werden. Es wird dadurch die Losgröße und die Sollmengenbuchungen jeder Losposition entsprechend umgerechnet/erhöht.Wenn nicht angehakt: kann maximal bis zur Losgröße abgeliefert werden.Hinweis: Siehe dazu auch die Einstellungen in den Stücklistenkopfdaten. |
|   | Fehlercode ein: | Wenn angehakt muss bei Schlechtstück ein entsprechender Fehlercode eingegeben werden.Definition der Fehlercode siehe Reklamation, Grunddaten, Fehler |
|   | Mehrfachfehlererf.: | Es können für eine Stückrückmeldung mehrere Schlechtstück mit eigenen Fehlercodes angegeben werden. |
|   | Stückzähler Ablieferung: | Wenn angehakt, wird immer die von einem Stückzähler, z.B. an einer Kunststoffspritzmaschine angezeigte Menge für die Losablieferung eingegeben. D.h. es wird immer die Menge zwischen dem Stückzähler und der bereits abgelieferten Menge als Abliefermenge zugebucht. Die Differenzmenge darf nur positiv sein. |
|   | In Arbeit ausblenden | Bei der Stückrückmeldung kann der Mitarbeiter mit dem In Arbeit signalisieren, dass die Auf und Umspannzeit für den Artikel der gerade, z.B. bei seinem Geht, auf der Maschine ist, noch zu seiner Soll/Ist-Zeit mitzurechnen ist. Ist dies angehakt, so ist die Erfassung dieser Menge nicht möglich. |
|   | Fertig bei Sollmenge: | Wenn angehakt, wird bei Erreichen der Sollmenge beim jeweiligen Arbeitsgang, automatisch auch das Fertig dieses Arbeitsgangs gesetzt. Das bewirkt auch, dass damit diese Arbeitsgänge nicht mehr in der "Tagesliste" der noch StückRück zu meldenden Arbeitsgänge aufscheint. |
|   | 0 Stk-Rückmeldung ein: | wenn angehakt, wird für einen Arbeitsgang für den die Stückrückmeldung mit 0/0/0 Menge gebucht wurde, bei der nächsten Rückmeldung dieser Arbeitsgang mit 0/0/0 vorbesetzt und der Eingabefocus auf den ersten Arbeitsgang ohne Menge gesetzt. |
|   | Stk-Rm. nur Haupt-AG: | Die Stückrückmeldung wird nur für den Hauptarbeitsgang abgefragt. |
|   | Abliefern mit Gut/Schlecht | Bei der Ablieferbuchung kann auch eine Schlechtmenge angegeben werden. Wird eine Schlechtmenge angegeben muss auch der Fehlergrund definiert werden. Die möglichen Fehler definieren Sie im Reklamationsmodul, Grunddaten, im Reiter [Fehler](../Reklamation/index.htm#Fehler). Bitte definieren Sie sowohl Kennung, wie auch Sprachabhängige Bezeichnung. |
| **Kieselstein ERP**-2** | Filter offene Arbeitsgänge: Maschinengruppe | Es werden in der Los-Auswahlliste nur die Arbeitsgänge dieser Maschinengruppe angezeigt |
|   | Filter offene Arbeitsgänge: Maschinenidentifikation  | Es werden in der Los-Auswahlliste nur die Arbeitsgänge dieser Maschine angezeigt. |
|   | Filter offene Arbeitsgänge: Tätigkeit | In der Los-Auswahlliste werden nur die Arbeitsgänge der definierten Tätigkeit angezeigt. |
|   | Hinweis bei Ablieferung  | Kennung für Vorzugsteil. Dient dazu den Bediener darauf hinzuweisen, dass durch die Ablieferbuchung ein Beleg am nächstgelegenen Drucker ausgedruckt wurde und dass er dieses vom Drucker holen und zum gefertigten Produkt dazugeben muss. |
|   | Stückrückm. abbrechbar | Bewirkt, dass die z.B. bei einer Ende oder Gehtbuchung erforderliche Stückrückmeldung abgebrochen werden kann. Wird die Stückrückmeldung abgebrochen, wird auch die Ende-/Geht-Buchung nicht gespeichert, also der gesamte Buchungsvorgang abgebrochen. |
| **Verbindung** | Protokoll | Wählen Sie hier das für die Verbindung zu Ihrem **Kieselstein ERP** Server verwendete Protokoll. In der Regel wird für Inhouse Kommunikation http verwendet.Für Verbindungen in Rechenzentren (Cloud) wird ein https Protokoll mit Zertifikaten verwendet. Da hierfür auch die Zertifikate auf der Serverseite installiert werden müssen, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer. **Hier muss auch das aktuelle https Protokoll TLS 1.3?? zur Verfügung stehen** |
| **Allgemein** | Sound | Wenn angehakt erfolgt eine akustische Rückmeldung der Buchung. Beachten Sie bitte, dass dafür auch der Sound auf dem Gerät aktiviert sein muss. |
|  | Zeitsynch. | Von wo sollte die Zeitsynchronisierung erfolgen. Hier ist in aller Regel der **Kieselstein ERP** Server einzustellen. Ab Windows 7 muss der Zeitserverdienst installiert sein und der Zeitsync ist auf Dienst zu stellen. Dies ist die derzeitige Art der Auslieferung. **Muss integriert sein** |
|  | Offline-DB | Wo befindet sich der Speicherort für die Datenbank für den Offline-Betrieb. Hier bitte ausschließlich einen . (Punkt) eingeben. |
|   | Timeout (Person) | Das Timeout wie lange nach dem Scann der Ausweisnummer die Person als angegeben betrachtet wird. Danach erscheint wiederum Ausweisnummer angeben. Wird diese Zeit überschritten, so muss der Personenbarcode erneut gescannt werden. |
|  | Meldungen (Timeout) | Definiert wie lange Meldungen angezeigt werden.Davon ausgenommen sind schwere Fehlermeldungen. Diese müssen durch Tipp auf den grünen Haken bestätigt werden. |
|  | Saldo | Wie lange wird der Zeitsaldo / die Monatsabrechnung der Person angezeigt |
|  | Kennwort | Definieren Sie hier das gewünschte Kennwort. |
|  | Touch-Screen | Wird das Terminalprogramm an einem Rechner mit Touch-Screen betrieben. Ist dies nicht angehakt, so wird der Mauszeiger angezeigt. Ist Touch-Screen angehakt, so wird der Mauszeiger unterdrückt. |
|  | Debug | Ist dies angehakt, so werden in einem zusätzlichen unteren Dialog Feld die vom seriellen CCD Scanner erfassten Codes angezeigt.<br>Für Steuerung des Terminals von extern muss der Debug-Modus ebenfalls aktiviert sein. Achten Sie darauf, dass dieser im Normalbetrieb abgeschaltet ist. |
|  | Expertenmodus | In diesem Mode wird für jede Buchung die Buchungszeit abgefragt. Damit können Sie komfortabel Zeiten Ihrer Mitarbeiter auch in der komplexen Form der Zeitbuchungen nachbuchen |
|  | Fertig buchen | Wird die Personalnummer eingescannt, so werden die offenen Tätigkeiten dieser Person angezeigt und können auf Fertig gesetzt werden. |
|  | Sollzeitprüfung | Nach jeder Buchung erfolgt eine Prüfung ob die Sollzeit bereits durch die Summe der Istzeiten überschritten ist. Gegebenenfalls erfolgt eine entsprechende Warnung. Bitte beachten Sie dazu auch den Parameter MAXIMALE_EINTRAEGE_SOLLZEITPRUEFUNG. Es werden hier maximal die angegeben Anzahl Datensätze für die Prüfung herangezogen. |
|   | Letzte Buchungen: | Ist dies angehakt, so werden die letzten 10 Buchungen für heute und gestern sortiert jüngste oben angezeigt.<br>Idealerweise nicht in Kombination mit Fertig buchen |
|  | Logging | Wenn aktiviert werden alle Scanns in diese Datei protokolliert. |
|  | Personal neu laden um  | Gibt die Uhrzeit an, zu der der im Terminal für den Offline-Betrieb gespeicherte Personalstamm automatisch neu geladen werden sollte. Im Terminal werden nur Ausweisnummer und Namen gespeichert. Keine DSGVO relevanten Daten. |
| **Anzeige** | Automatisch | Automatische Anpassung der Bildschirmauflösung an Ihren Schirm. |
|  | Breite | Breite des angezeigten Fenster in Pixel |
|  | Höhe | Höhe des angezeigten Fensters in Pixel. Die Verwendung einer festen (kleineren) Angabe empfiehlt sich bei parallelem Betrieb der Terminalprogramms gemeinsam mit anderen Programm z.B. auf einem Prüf-Rechner. |
|   | Sprache Terminal | In welcher Sprache wird das Terminal selbst betrieben. Derzeit stehen Deutsch, Englisch, Slowenisch zur Verfügung. Bitte beachten Sie, dass nach der Auswahl einer **anderen Sprache** das Terminalprogramm **neu gestartet** werden muss. Siehe dazu [Locale](#Locale). |
|   | Seitenminiaturen | Aktiviert für die Dokumentenanzeige, dass im damit geöffneten Acrobate Reader auch Seitenminiaturen angezeigt werden. |
|   | Schaltflächen Groß  |   |
|  | Maschinen | Es wird der Knopf Liste meiner angestempelten Maschinen angezeigt.  |
|  | Stückrückmeldung | Es wird der Knopf Stückrückmeldung angezeigt. Damit kann ohne Zeitbuchung die Stückrückmeldung erfolgen. |
|   | Urlaubsantrag  | Es kann damit direkt am Terminal auch ein Urlaubs bzw. Zeitausgleichsantrag erfasst werden |
|   | Lose  | Durch tippen auf den Button Lose wird die Liste der offenen Lose angezeigt. [Somit kann ohne Papier auf die jeweiligen Arbeitsgänge gebucht werden](#Direkt auf Lose buchen).  |
|   | Abweichende Maschine  | Es kann anstelle der durch den Arbeitsplan, Fertigungsschein definierten Maschine ein andere Maschine direkt am Terminal gewählt werden. |
|   | Dokumente Verzeichnis | Ist in der Regel ein Netzwerkpfad auf den das Terminal zugreifen darf, in dem PDF und oder DXF Zeichnungen hinterlegt sind, welche der Mitarbeiter durch einen Auswahldialog anzeigen kann. |

| Taste | Bedeutung |
| --- |  --- |
| ![](Nettop5.gif) | Beendet die Eingabe und kehrt in den Terminalmodus zurückIst das Terminal im Offline Modus, so wird hier versucht ob eine Verbindung zum Server aufgebaut werden kann. Gelingt dies, wird in den Online Modus gewechselt. |
| ![](Nettop6.gif) | Verwirft die gemachten Änderungen und kehrt in den Terminalmodus zurück |
| ![](Nettop7.gif) | Ausstieg ins Betriebssystem |

### Weitere mögliche Einstellungen:

**Feste IP Adresse**

Für die Aktualisierung des Terminals hat es sich bewährt, dass diesem eine feste IP Adresse zugeteilt wird. Diese ist im Betriebssystem einzustellen.
Start, Einstellungen, Netzwerk- und DFÜ-Verbindungen
Etwas längeres Antippen der Lanverbindung (CS89501). Im erscheinenden Pop-Menü wählen Sie Eigenschaften. Stellen Sie hier, IP-Adresse, Subnetzmaske, Standardgateway entsprechend ein und schließen Sie gültig mit ok (rechts oben).
Nach der Änderung der IP Adresse bitte die Einstellungen [speichern](#Speichern der Systemeinstellungen) und dann das Terminal mittels Aus/Ein neu starten.

### Das Terminal ist im Offline Modus, wie kommt man am schnellsten in den Onlinemodus?
Machen Sie einen doppel-Tipp auf die rote Statuszeile. Damit versucht das Terminal sofort eine Onlineverbindung aufzubauen.

Q: Wie komme ich von der Anmeldeseite wieder in die Konfiguration?

A: Um den Knopf für die Konfiguration auswählen zu können, tippen Sie bitte einfach auf einen der angezeigten Mitarbeiter. Dann erscheint die übliche Erfassungsmaske mit den üblichen Timeouts und Sie können bequem das Konfigurationsmenü starten.
Der Tipp auf den Personal-Refresh ![](Personal_Refresh.jpg) Button bewirkt nur ein Neu laden der Personen, was in der Anmeldeseite oft sehr praktisch ist. Wenn jedoch nur 1-2Personen angelegt sind, gerade zu Beginn, dann hätten Sie 0,5Sekunden Zeit um in die Konfiguration zu wechseln, was nicht funktionieren wird.

Q: Wo sehe ich welches Terminal was ist?

A: gerade bei mehreren Terminals stellt sich oft die Frage welches Terminal hat welche Parameter, Eigenschaften, Einstellungen usw.
Am Terminal selbst werden im Konfigurationsmenü in der Titelleiste folgende Daten angezeigt:
![](Nettop_Namen.gif)<br>
Nach dem Wort Optionen: Der Hostname der dem Terminal gegeben ist (oft auch als der Computername gekennzeichnet)<br>
Danach der PC - Name aus den Arbeitsplatzparametern
Danach der Standort aus den Arbeitsplatzparametern
![](Nettop_Arbeitsplatzzuordnungen.gif)
Info: Wenn der PC - Name und oder der Standort aus den Arbeitsplatzparametern geändert wird, so muss das Terminalprogramm komplett neu gestartet werden, um die richtig Zuordnung anzuzeigen.
Idealerweise sind der Hostname und der PC - Name in den Arbeitsplatzparametern gleich.
Zusätzlich sehen Sie bei getätigten Zeitbuchungen im Modul Zeiterfassung bei der buchenden Person unter Quelle ebenfalls den Hostnamen des Terminals ergänzt um ZT ![](Nettop_Quelle.gif).

Um nun z.B. den automatischen Druck der Ablieferetikette mit der Ablieferbuchung am Terminal einzurichten, muss in den Arbeitsplatzparametern
1. als PC-Name die Quelle angegeben werden.
![](Nettop_Arbeitsplatzparameter1.jpg)
2. in den Parametern
![](Nettop_Arbeitsplatzparameter2.jpg)<br>
als Parameter DRUCKERNAME_MOBILES_LOSABLIEFER_ETIKETT und der Proxy-Druckserver (bekommen Sie von Ihrem **Kieselstein ERP** Betreuer) und der Druckername angegeben werden.
Damit wird z.B. bei jeder Ablieferbuchung am Terminal ein Ablieferetikett gedruckt.

**Anwesenheitsliste**

Die Anwesenheitsliste der Mitarbeiter ist als Bildschirmschoner ausgeführt. D.h. nach wenigen Minuten schaltet das Terminal automatisch auf die Anzeige der Anwesenheitsliste um. Somit sieht man jederzeit, wann wer an welchen Aufträgen arbeitet. Wenn mehrere Terminals zum Einsatz kommen, empfiehlt sich zusätzlich die Verwendung des Proxyserers, der die Last am **Kieselstein ERP** Server dramatisch reduziert.

Für die Einstellung des Bildschirmschoners haben sich folgende Werte bewährt:

| Beschreibung | Wert |
| --- |  --- |
| Updateintervall  | 30 Sekunden |
| Bewegungsintervall  | 10 Sekunden |
| Fensterbreite  | 860 |
| Fensterhöhe | 768 |

Üblicherweise wird dafür ein speziell angepasster Report der Anwesenheitsliste verwendet, der auf diese Höhe ausgelegt ist. Wenden Sie sich dafür an Ihren **Kieselstein ERP** Betreuer.

## Locale

Wird das Terminal in einer fremdsprachigen Umgebung betrieben, z.B. Englisch, Polnisch, Slowenisch, so sollte in der Regel auch die Kommunikation mit dem Anwender in der gewünschten Sprache erfolgen. D.h. neben der Umschaltung der Anzeige-Sprache des Terminals im Reiter Anzeige, müssen auch die Daten in der Fremdsprache zur Verfügung stehen.
D.h. es muss Ihr **Kieselstein ERP** die Funktion Mehrsprachigkeit besitzen und es müssen, insbesondere die Sondertätigkeiten (Kommt, Geht) und die Tätigkeitsartikel auch in der Fremdsprache übersetzt verfügbar sein um diese in der gewünschten Sprache für den Mitarbeiter lesbar anzeigen zu können.

## Offline Modus
Wenn das Terminal nach erfolgreichem Start, die Verbindung zum **Kieselstein ERP** Server verliert, geht es in den Offline Modus.

Ist das Terminal im Offline Modus, so werden die Daten auf der lokalen Festplatte zwischengespeichert.

Es wird dies durch ![](Nettop8.gif) in der Statuszeile angezeigt.
Es wird im 30 Minuten Intervall geprüft ob der **Kieselstein ERP** Server wieder verfügbar ist, wenn ja werden die erfassten und zwischengespeicherten Daten an den Server übertragen.
Die direkten Online-Funktionalitäten werden soweit wie möglich unterstützt, können jedoch nicht in vollem Umfang zur Verfügung gestellt werden. So erfolgt normalerweise bei jeder Buchung eine Prüfung, ob noch auf das Fertigungs-Los gebucht werden darf. Im Offline Modus kann diese Prüfung nicht erfolgen, d.h. es werden alle gebuchten Daten akzeptiert.
Wird bei der Übertragung festgestellt, dass z.B. auf dieses Fertigungs-Los nicht gebucht werden kann, so wird bei der entsprechenden Person ein ENDE Eintrag mit einem entsprechenden Kommentar hinterlegt.
Ist auch die Person nicht zuordenbar, so werden die Buchungen mit Kommentar beim LPAdmin eingetragen.

Geht das Terminal beim Starten sofort in den Offline-Modus oder wechselt es während des Betriebes in den Offline-Modus, so werden alle Personalbuchungen und auch alle Losbuchungen akzeptiert. Wurde bereits einmal eine Online Verbindung zum **Kieselstein ERP** Server hergestellt, so wurden die Personendaten (Ausweisnummer, Name) bereits Terminal abgelegt. Somit kann für die zuletzt bekannten Personen auch deren Name angezeigt werden. Wird eine vom Aufbau her gültige Ausweisnummer eingegeben, ist die Person aber nicht bekannt, so wird im Offline Modus dieser Ausweis trotzdem akzeptiert, es wird jedoch nur die Ausweisnummer angezeigt. Im Online Modus wird ein ungültiger Ausweis nicht akzeptiert.

**Hinweis:** Ist das Terminal im Offlinemodus, so wird, wenn seit der letzten Buchung bereits 30Minuten vergangen sind, bei einer erneuten Buchung versucht eine Verbindung zum **Kieselstein ERP** Server aufzubauen. Ist der Verbindungsversuch erfolgreich, so werden sofort die inzwischen am Terminal gespeicherten Daten an den Server übertragen.
Um eine Onlineverbindung sofort zu versuchen kann auf die rote Offline Statuszeile getippt werden.

Q: Wieviele Daten können im Offline Modus gespeichert werden ?

A: Die lokale Datenbank des Terminals kann bis zu 4GB an Daten aufnehmen, unter der Voraussetzung, dass noch ausreichend Festplatten-Speicherplatz zur Verfügung steht.

## Änderungen der Ausweisnummer einer Person
Achten Sie bitte unbedingt darauf, dass alle Terminals im Online Modus sind. Dadurch sind alle Buchungen in der **Kieselstein ERP** Datenbank enthalten. Falls Sie nicht sicher sind, buchen Sie mit dem alten Ausweis / Schlüssel und prüfen Sie an einem **Kieselstein ERP** Client, dass diese Buchung im System ist.<br>
Nun ändern Sie die Ausweisnummer der Person, geben ihr den neuen Schlüssel.
Das war die Änderung. Zur Sicherheit können Sie gerne den Mitarbeiter mit dem neuen Schlüssel zur Buchung schicken. Wenn das Terminal Online ist, wird ab sofort mit der neuen Ausweisnummer, mit dem neuen RFID Schlüssel gebucht.
Damit sicher gestellt ist, dass im Offline Modus auch die lokalen Terminaldatenbank die aktuellen Namen und Ausweisnummern hat, womit auch die Buchungsmöglichkeit gesteuert wird, kann entweder das Aktualisieren manuell durch Tippen auf ![](ZET_4.jpg) angestoßen werden, oder es wird in der Konfiguration des Terminals unter Allgemein, Personal neu laden um angehakt und ein geeigneter Zeitpunkt eingetragen.

## Starten des Terminals
Für den Start des Terminalprogramms starten sie dieses durch einen Doppeltipp auf das ![](ZET_KES1.jpg) TimeTerminal Icon.

Geht das Terminal nach dem Starten sofort wieder in den Offline Modus, so stellen Sie bitte sicher, dass mit allen Ports richtig auf den **Kieselstein ERP** Server zugegriffen werden kann. Sollten Sie einen Proxyserver verwenden, so stellen Sie diesen so ein, dass lokale Adressen nicht über den Proxyserver laufen.

## Betriebsumgebung des Terminals
Diese Werte orientieren sich an üblichen All-In-One-PC Terminals. Selbstverständlich können auch sehr robuste Industrie-PCs zum Einsatz kommen, welche einen entsprechend weiten Einsatzbereich haben können.
- 0 - 40 °C, nicht kondensierend
- **Wichtig:** Montiere, bedienen das Terminal so, dass du möglichst senkrecht auf das Terminal siehst. Nur dadurch treffen dine Finger parallaxenfrei auf dem Touchbildschirm auf und werden so richtig in Ihrer Position erkannt. Eine Bedienung von sehr schräg oben oder von sehr schräg seitlich führt zu Fehlbedienungen.
- **Hinweis:** Bitte achte darauf, dass die Oberfläche des Terminals nicht beschädigt wird. Sollte die Bedienung des Terminals mit dem Finger nicht möglich sein, oder immer mit sehr schmutzigen Händen durchgeführt werden müssen, so sollte die Oberfläche des Schirms mit einer entsprechenden Folie geschützt werden.
- Sorgen Sie bitte dafür dass der Temperaturbereich eingehalten wird.

### Schutzfolie für das Terminal
Gerade bei Bedienung mit schmutzigen Fingern / Händen raten wir dringend zur Verwendung einer Schutzfolie. Diese schütz den eigentlichen Touchbildschirm vor Verschmutzung und kann relativ einfach ausgetauscht werden.

Für das Aufbringen der Schutzfolien gehen Sie bitte wie folgt vor:

1.  Die selbst haftende Rückseite der Display-Schutzfolie wird von einer dünnen Trägerfolie geschützt. Die Trägerfolie ist mit silberfarbigen Aufklebern versehen, welche zum Abziehen der Träger von der eigentlichen Display-Schutzfolie dient.

2.  Der Bildschirm des Terminals muss vor dem Aufbringen der Schutzfolie staub- und fettfrei sein. Das mitgelieferte Mikrofaser Putztuch eignet sich hervorragend für die Reinigung.

3.  Fassen Sie an einer Ecke den silberfarbigen Aufkleber ganz außen mit zwei Fingern an und ziehen Sie diesen langsam nach oben ab. Danach kann auch die zweite Seite am anderen silbernen Aufkleber abgezogen werden.
    ACHTUNG: Ziehen Sie die Trägerfolie nur wenige Zentimeter von der Schutzfolie ab.

4.  Platzieren Sie nun die freigelegte selbst haftende Unterseite der Folie exakt am Displayrand.
    ACHTUNG: Die Folie ist exakt auf das Terminal abgestimmt. Sie darf nicht unter die Bildschirmabdeckung ragen.

5.  Ziehen Sie nun langsam, Stück für Stück die Trägerfolie ab und streichen Sie mit den Fingern die Schutzfolie auf das Display.

6.  Lufteinschlüsse zwischen Display und Folie streichen Sie mit Hilfe des mitgelieferten Mikrofaser Putztuches aus.
    Optimale Ergebnisse erzielen Sie, wenn Sie vorsichtig von Innen nach außen streichen.
    ACHTUNG: Niemals scharfe oder spitze Gegenstände verwenden.

7.  Sollten sich noch Schmutzpartikel unter der Folie befinden, können Sie die Schutzfolie mit einem Stückchen Klebefilm wieder anheben.

8.  Mit einem weiteren Stückchen Klebefilm lassen sich Verschmutzungen vorsichtig entfernen.

![](nettop_Schutzfolie.png)

## Programmierung des Barcodescanners
Der Barcodescanner wird per USB Kabel angeschlossen und muss eine serielle Schnittstelle emulieren. Für die von uns üblicherweise verwendeten Datalogic Touch90 bzw. Heron 130 scannen Sie bitte den untenstehenden Code.
![](ZET_USB_COM.gif)

Bitte beachten Sie, dass für die Verwendung auch entsprechende USB-Treiber installiert sein müssen. Diese finden Sie auf Ihrer **Kieselstein ERP** Installation's CD unter Tools\Treiber\Barcodescanner\Datalogic\Touch90\USB-COM-Install.EXE.

## Installation des ZE-Terminal Programms
[Siehe bitte.]( {{<relref "/docs/installation/04_ze_terminal" >}} ) 

## Fragen rund um das Terminal
Q: Was ist zu tun, wenn der Barcodescanner zwar piebst aber nichts passiert, keine Buchung erfolgt?

A: Stellen Sie sicher, dass das Terminalprogramm Fehlerfrei startet. Es passiert immer wieder, dass bei Übersiedelung des Terminals an einen anderen Standort der Barcodescanner an einen anderen, besser passenden USB Port angeschlossen wird und dadurch die Kommunikation zwischen Terminal und Barcodescanner nicht mehr funktioniert. Dies wird am Terminal durch
![](ZET_COM_X_nicht_vorhanden.jpg)
Der Anschluss Com ?? ist nicht vorhanden angezeigt.
In diesem Falle gehen Sie in die Konfiguration des Terminals und stellen Sie bitte den Com-Port für den Seriellen USB-Barcodescanner richtig ein. Üblicherweise ist an den Terminals nur ein Com-Port verwendet, daher wird auch vom Terminal dann nur dieser eine Port vorgeschlagen.

Sollte es trotzdem nicht funktionieren, sollte mit folgenden Schritten die Ursache herausgefunden werden können:
-   Kann der Barcode überhaupt gelesen werden?
    -   Prüfen Sie dies bitte mit einem anderen Barcodescanner oder
    -   parametrieren Sie den Scanner um, sodass er im Wedge Modus läuft damit kann auf einem Rechner über die Kommandozeile geprüft werden ob der Scanner den Barcode überhaupt liest oder
    -   schalten Sie das Terminal in den Debugmodus, somit sehen Sie ebenfalls ob der Scanner Daten an das Terminal liefert.
-   Ist der Barcode nur bei einem Ausdruck nicht lesbar oder ist dies bei allen
    -   Fertigungsbegleitscheinen
    -   Auftrags-Packlisten
    so.
    Ist es bei allen Papieren so, dann ist vermutlich die Konfiguration des Terminals oder des Scanners falsch.
    Stellen Sie dies über den Terminal Debugmodus fest.

-   Um das Terminal in den Debugmodus zu bringen gehen Sie bitte wie folgt vor:
    -   Am Terminal links oben auf das **Kieselstein ERP** Icon Klicken.
    -   Bitte geben Sie das eingestellte Passwort ein (wir gehen davon aus dass eines hinterlegt ist)
    -   Nun wählen Sie den oberen Reiter allgemein und haken rechts unten Debug an.
    -   Mit ok verlassen Sie die Konfiguration und gehen wieder zurück in den Terminalmodus
    -   Scannen Sie nun den Barcode einer Person. Es muss im Debugfenster unten $Pxxx (die Ausweisnummer der Person) angezeigt werden. Unmittelbar danach wird der Name der Person angezeigt, wenn die Person erkannt wurde.
    -   Scannen Sie nun die Auftrags- oder Losnummer.
        Wichtig: Wird die Auftrags / Losnummer richtig angezeigt?
        Ein Auftrag hat das LeadIn $A und danach kommt im Klartext die Auftragsnummer.
        Ein Los hat das LeadIn $L und danach kommt im Klartext die Losnummer.
        Gerade bei Wedge Scannern kann es vorkommen dass diese auf ein falsches Tastaturlayout eingestellt sind. Siehe dazu bitte auch [Barcodescanner umstellen](BDE_Station.htm#Programmierung der Barcodescanner).
-   Wenn bis hierher alles wie gewünscht funktioniert, so ist eventuell die Ursache, dass keine default Tätigkeit im Terminal hinterlegt ist. Prüfen Sie auch dies.
-   Sollte trotzdem keine Funktion des Scanners in Verbindung mit dem Terminal erkennbar sein, stehen wir Ihnen gerne mit weiterem Rat und Tat zur Seite.

## Können auch andere Barcodescanner verwendet werden?
Grundsätzlich haben wir mit den von uns verwendeten Datalogic Scannern sehr gute Erfahrungen bezüglich Qualität und Lebensdauer gemacht.
Sollten Sie trotzdem einen anderen Scanner einsetzen wollen, so muss die serielle Schnittstelle auf 9600, n, 8, 1 gestellt werden, was die übliche Defaulteinstellung von Scannern ist. Beachten Sie bitte auch, dass zum Ende des Datenstromes ein CR (Carriage Return) gesandt werden muss, mit dem der Datenstrom abgeschlossen wird.

Q: Terminal meldet beim Start falscher Benutzer
A: Stellen Sie sicher, dass der in der Terminalkonfiguration hinterlegte Mandant richtig ist, sodass sich das Terminal am **Kieselstein ERP** anmelden kann.

Q: Was ist zu tun, wenn bei der Zeitbuchung folgende Fehlermeldung erscheint? "Das Los 13-00101 AZ kann nicht gefunden werden"

A: Die Fehlermeldung bedeutet, dass eine Buchung auf das Los nicht erfolgreich durchgeführt werden konnte, da es nicht gefunden wird. Bitte überprüfen Sie in diesem Fall ob bei der Belegnummer und/oder beim Artikel der Tätigkeit (AZ) ein Bindestrich (-) verwendet wird. Wenn dies der Fall ist, müssen die Artikelnummern der Tätigkeiten entsprechend abgeändert werden. Gehen Sie dazu in den Arbeitszeitartikel und ändern die Artikelnummer.
**Achtung:** Für die etwaige Änderung der Belegnummer wenden Sie sich bitte an Ihren **Kieselstein ERP** Betreuer!

## Zeitbuchungen mit Zeiterfassungsstift 
Der Zeiterfassungsstift KDC100 wird leider nicht mehr geliefert. Es werden daher Opticon-Stifte eingesetzt. Siehe dort.

Q: Fehlermeldung bei der Übertragung von Zeitbuchungen mit Zeiterfassungsstift

A: Kommt es bei der Übertragung zur Fehlermeldung: *No entity found for query*, so ist vermutlich der Parameter KDC_BUCHT_ARBEITSGANG nicht aktiviert. Stellen Sie diesen bitte im System, Parameter auf 1.

Q: Wieso wird anstatt einer Zeitbuchung auf einen Arbeitsgang eine Endebuchung eingefügt?

A: Kann die Buchung des Zeiterfassungsstiftes nicht durchgeführt werden, so wird trotzdem versucht die Buchung und vor allem den Zeitpunkt so gut wie möglich in der **Kieselstein ERP** Zeiterfassung abzulegen. Hintergrund ist, dass die Zeiterfassungsstifte Offline sind. D.h. vom Erfassungszeitpunkt der Buchung mit dem Stift bis zum Eintrag in die **Kieselstein ERP** Datenbank, also den Zeitpunkt der gültigen Übertragung der Daten, kann sich der Status des dahinter liegenden Loses, des Arbeitsganges, der Maschine und ev. auch des Mitarbeiters ändern.
Daher werden diese Buchungen anstelle der direkten Losbuchung als Endebuchung angelegt. Die Idee dahinter ist, dass von Ihnen dann manuell die Ursache behoben wird und dann die Ende-Buchung auf das jeweilige Los, den jeweiligen Arbeitsgang gebucht / nachgetragen wird.
Hinweis: Kann auch nicht mehr auf den Mitarbeiter gebucht werden, wird die Buchung unter dem Admin abgelegt.
![](Zeitbuchung_nicht_durchfuehrbar.jpg)

Q: Was passiert wenn eine GEHT-Buchung vergessen wurde?
A: Wenn es bei einer Kommtbuchung davor Buchungen gibt und die letzte Buchung davor kein Geht ist, so wird am Terminal eine Meldung angezeigt. Somit kann der buchende Mitarbeiter die Personalabteilung informieren und um Abänderung bitten.

## $PLUS, Können mehrere Los gleichzeitig gebucht werden?
A: Mit dem $PLUS Barcode können auf den Terminals auch mehrere Lose gleichzeitig angestempelt werden.
Hier ist der Ablauf so, dass sich der Mitarbeiter identifiziert, und dann den $PLUS Barcode scannt.
Nun erscheint ein Erfassungsfenster, in dem die Arbeitsgänge die gemeinsam durchgeführt werden angestempelt werden.
Bitte beachten Sie, dass diese Erfassung relativ rasch hintereinander gemacht werden sollte.
Als Bestätigung, dass alle Arbeitsgänge, die gemeinsam gemacht werden, erfasst sind, scannen Sie bitte den $PLUS Barcode erneut.
Damit wird eine gemeinsame Buchung gestartet, welche mit einem neuen Buchungsbeginn bzw. Ende bzw. Geht beendet werden muss.
Bitte beachten Sie, dass die Bedienung am Terminal einen anderen Ablauf hat als die der BDE-Station.
![](ZET_5.jpg)

Um in dieser Buchung auch andere Maschinen verwenden zu können, kann die Maschinenbarcodeliste verwendet werden.

Q: Gleichzeitige Buchung auf verschiedene Lose?

A: Nachfolgend eine etwas ausführlichere Beschreibung der $PLUS Buchung für das ZE-Terminal.
Der Zweck ist, dass mehrere Los-Arbeitsgänge von einem Mitarbeiter gleichzeitig gestartet werden. Dies wird vor allem dann verwendet, wenn ein Mitarbeiter mehrere Maschinen quasi gleichzeitig bedient.
Siehe dazu bitte auch [Mehrmaschinenbedienung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ) auch [Maschinenzeiterfassung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ) genannt.
Für dieser Buchung, welche per Barcode oder mit der Losauswahlliste bedient wird, benötigen Sie folgende Unterlagen:
- Ausweis des Mitarbeiters
- Fertigungsschein mit kombinierten Arbeitsgang und Maschinen Barcodes
- ev. die Maschinenliste um auf abweichende Maschinen zu buchen.

Der Ablauf ist nun folgender, wobei davon ausgegangen wird, dass diese Buchungen am ZE-Terminal erfolgen.

1.  Scannen des Mitarbeiterausweises.
    ![](Gemeinsame_Losbuchung1.gif)
    Siehe Personal, Journal, Personalliste, Barcodeliste anhaken und alle oder nur den gewählten Mitarbeiter drucken

2.  Vom Fertigungsbegleitschein scannen Sie
    Lose gemeinsam buchen
    ![](Gemeinsame_Losbuchung2.gif)

3.  Nun erscheint die Erfassungsliste der gleichzeitig zu startenden Arbeitsgänge
    ![](Gemeinsame_Losbuchung3.jpg)

4.  Scannen Sie nun Tätigkeiten mit den Maschinen die in den kombinierten Barcodes enthalten sind
    ![](Gemeinsame_Losbuchung4.gif)

5.  Somit erscheint dieser Arbeitsgang in der Verkettungsbuchung
    ![](Gemeinsame_Losbuchung5.gif)

6.  Scannen Sie nun relativ rasch alle Arbeitsgänge die von Ihnen gleichzeitig gestartet werden
    Für das Timeout siehe bitte Konfiguration des Terminals, Allgemein, Meldungen Timeout

7.  Schließen Sie die Buchung durch einen erneuten Scann von
    Lose gemeinsam buchen
    ![](Gemeinsame_Losbuchung6.gif)
    ab.

8.  Wo sehen Sie welche Arbeitsgänge angestempelt sind?
    Scannen Sie einfach Ihren Ausweis. Es erscheint die Liste der offenen Zeitverteilungen, also der Arbeitsgänge die gemeinsam gestartet sind.
    ![](Gemeinsame_Losbuchung7.gif)
    Es muss dafür Letzte Buchungen (Konfiguration, Allgemein, Letzte Buchungen) aktiviert sein.
    Hinweis: Maschinen die indirekt durch die offene Zeitverteilung den jeweiligen Mitarbeitern zugewiesen sind, werden in der Maschinenliste nicht angezeigt.

9.  Beenden der Arbeit auf den gemeinsam gestarteten Tätigkeiten
    Scannen Sie den Ende-Barcode
    ![](Gemeinsame_Losbuchung8.gif)

    Ja nach Parametrierung sind nun noch gegebenenfalls die Stückrückmeldungen der abgeschlossenen Tätigkeiten zu erfassen.

10. Stückrückmeldung erfassen
    ![](Gemeinsame_Losbuchung9.jpg)
    Geben Sie bei Gut die Gut-Stück an. Also die Stückzahl die in den nächsten Arbeitsgang als in Ordnung weitergegeben werden kann.
    Bei Schl. geben Sie die Schlecht-Stück, also die defekten / kaputten Teile an.
    Fertig wird dann angehakt, wenn der Arbeitsgang komplett fertig ist. Damit erscheint dieser Arbeitsgang in Zukunft auch nicht mehr auf einer der Terminal Auswahllisten.
    Um durch die Felder zu springen tippen Sie einfach auf Enter. Sind alle Felder befüllt worden, so wird der Haken grün
    ![](Gemeinsame_Losbuchung10.jpg)

11. Womit Sie die gemeinsamen Loszeitbuchung beendet und gültig abgespeichert haben.
Sollte nun auf eine abweichende Maschine gebucht werden, so scannen Sie vor dem Punkt 4, kombinierter Arbeitsgangbarcode, den Barcode der gewünschten Maschine.
Diese Liste finden Sie unter Zeiterfassung, Maschinen, Journal, Maschinenliste, Barcode anhaken
![](Gemeinsame_Losbuchung11.gif)

Q: bei der Buchung auf eine abweichende Maschine erscheint eine Fehlermeldung dass dies abweichend zum geplanten Arbeitsgang ist und daher nicht zugelassen ist.

A: Sie können diese Prüfung
![](keine_Maschinenpruefung.jpg)
durch den Parameter PARAMETER_KDC_KEINE_MASCHINENPRUEFUNG deaktivieren

Q: Offene Zeitverteilung vorhanden

A: Wird am Terminal offen Zeitverteilung vorhanden angezeigt,
![](ZET_6.gif)
so bedeutet dies, dass die nachfolgenden Arbeitsgänge, auch in rot angezeigt, gemeinsam gestartet wurden. Diese können gemeinsam beendet werden oder es kann ein weiterer Arbeitsgang hinzugefügt werden. Um zu den Arbeitsgängen einen weiteren hinzuzufügen, scannen Sie bitte am Terminal erneut den $PLUS = Lose gemeinsam buchen.
Sie kommen wieder in die Erfassungsliste der Verkettungsbuchung. Hier können Sich durch scannen der Barcodes, so wie oben beschrieben weitere Arbeitsgänge, die Sie zum gleichen Beginnzeitpunkt gestartet haben hinzufügen. Die Erfassung beenden Sie wiederum durch Scann von $PLUS bzw. Lose gemeinsam buchen.

Wird bei offener Zeitverteilung ein einzelner Arbeitsgang gescannt, so erscheint:
![](ZET_7.jpg)
Damit wird signalisiert, dass noch offene Zeitverteilungen gegeben sind und dass diese aufgelöst = beendet werden.
Unmittelbar daran anschließend erscheint nun die Erfassung der Gut-Schlecht-Stück.

Weiters werden offene Zeitverteilungen dann aufgelöst, wenn ein GEHT oder eine UNTERbrechung gebucht wird.

## $PLUS Wie werden die Zeiten der $PLUS Buchung verteilt?

A: Für die Behandlung der zu verteilenden Zeiten stehen aktuell folgende verschiedene Vorgehensweisen zur Verfügung. Diese werden durch den Parameter ZEITVERTEILUNG_TYP gesteuert.

-   Wert 0:<br>Hier erfolgt die Zeitverteilung aus dem Gedanken heraus, dass immer ähnliche Tätigkeiten gemeinsam durchgeführt werden. Damit wird, egal wie die Sollzeiten definiert sind, die Verteilung anhand der Losgrößen vorgenommen wird.
    Hintergrund, wie ist mit Rüstzeiten umzugehen, werden diese als eigene Arbeitsgänge und immer eigenständig gebucht, oder sind diese in den Sollzeiten des einzelnen Arbeitsganges enthalten.
    Darum werden eben die Zeiten auf die einzelnen Lose anhand der Losgrößen verteilt.
    Der Gedankengang dazu ist: Es werden nur sehr ähnliche Arbeitsgänge auf einmal gemacht. Das heißt es ist davon auszugehen, dass die Fertigungsschritte sehr ähnlich sind. Daher ist anzunehmen, dass das Rüsten nur einmal anfällt und dass die Stückzeiten annährend gleich sind. Das bedeutet, dass die Gesamtzeit des einzelnen Arbeitsganges anhand der Losgrößen verteilt werden kann. Je Los bedeutet dies:
    Dauer (der einzelnen Tätigkeit des MAs) * Losgröße (jedes betroffenen Loses) / Summe aller Losgrößen aller an der Buchung beteiligten Lose.
    Bitte beachten Sie, dass mit dieser Einstellung immer die gleiche Tätigkeit für den durchgeführten Arbeitsgang gebucht wird. Also den als erstes angestempelten / ausgewählten Arbeitsgang.
    Bitte beachten Sie auch den wesentlichen Unterschied der Verbuchung der Maschinenzeiten.
    Wurden mit der $PLUS Buchung Maschinen gestartet die als Manuell zu bedienende Maschinen definiert sind, so werden die Zeiten 1:1 mit den MitarbeiterInnen Zeiten gebucht. Sind das aber Maschinen die unter die Mehrmaschinenbedienung fallen, also alle Maschinen die **nicht** als Manuell gekennzeichnet sind, so wird, die gesamte Zeit zwischen Beginn und Ende der $PLUS Buchung auf alle beteiligten Maschinen gebucht.

-   Wert 1:<br>Hier erfolgt die Zeitverteilung anhand der zurückgemeldeten Gut-/Schlecht-Stück.
    D.h. es werden für jede Tätigkeit die zurückgemeldete Stückzahl mit der Sollzeit multipliziert und dies als Basiswert für die Ermittlung des Verteilungsfaktors verwendet.
    Ist für einen Arbeitsgang keine Sollzeit gegeben, so wird der Wert des Parameters ZEITVERTEILUNG_SOLLZEIT_WENN_0 verwendet, welcher default auf 60Minuten gestellt ist.
    Wird keine Stückzahl für die Gut/Schlecht-Menge angegeben, so wird die Buchung ignoriert. Damit haben Sie die Möglichkeit fälschlich gestartete Tätigkeiten aus der Verteilung auszunehmen.
    Im Gegensatz zur Einstellung Wert 0 wird hier diejenige Tätigkeit verwendet, welche für den Arbeitsgang definiert ist. D.h. es kann ein Mitarbeiter durchaus verschiedenste Tätigkeiten durchführen.
    Beachten Sie bitte auch, dass für Mehr-Maschinen die gesamte Zeit welche zwischen Beginn- und Endzeitpunkt der $PLUS-Buchung liegt, die Maschine läuft angenommen und gebucht wird. Also alle Maschinen gleichzeitig laufen.

-   Wert 2:<br>Auch hier erfolgt die Zeitverteilung anhand der zurückgemeldeten Gut-Schlecht-Stück.
    Zusätzlich werden jedoch mögliche Rüst-Arbeitsgänge berücksichtigt.
    D.h. das Ziel ist, dass man damit die Möglichkeit hat, dass gleichzeitig gerüstet und auf anderen umgespannt / gefertigt wird.
    Das bedeutet, anhand der gebuchten Arbeitsgänge wird festgestellt ob überhaupt ein Rüstarbeitsgang gebucht wurde. Ist dem nicht so, wird wie unter Wert 1 beschrieben vorgegangen.
    Gibt es einen Rüstarbeitsgang und auch Umspann-Arbeitsgänge, so werden die Umspann-Arbeitsgänge mit ihren Sollzeiten eingebucht und die nun verbleibende Zeit wird als Rüstzeit gebucht.
    Ist die Summe der Umspann-Soll-Zeit größer als die verfügbare Zeit, so wird von der verfügbaren Zeit je Rüsten eine Minute abgezogen und die nun verbleibende Zeit wird auf die Umspann-Arbeitsgänge wie unter Typ 1 beschrieben verteilt.

Q: wie wird die Zeitverteilung von manuellen Maschinen durchgeführt?

A: Wenn eine $PLUS Buchung, also Los-Zeiten gemeinsam starten für manuelle Maschinen durchgeführt wird, so bedeutet ja das manuelle Buchen, dass die Maschine immer fix einer Mitarbeiter-Zeitbuchung zugeordnet ist. Das bedeutet weiterhin, dass wenn nun z.B. zwei Arbeitsgänge auf verschiedenen Maschinen gestartet wurden, die Laufzeit der Maschinen genauso aufgeteilt wird, wie die Arbeitszeit für den Mitarbeiter (siehe oben, Zeitverteilung der $PLUS Buchung). In anderen Worten, wenn Sie damit erreichen möchten, dass die Laufzeit der Maschinen sich über den gesamten Zeitraum erstreckt, aber nur die Zeit des Mitarbeiters verteilt wird, so ist dies bei manuell zu bedienenden Maschinen nicht vorgesehen. In diesem Falle ist das gemeinsam buchen dafür gedacht, dass der Mitarbeiter seine Arbeitsgänge z.B. am Morgen startet und dann zuerst den ersten Arbeitsgang macht und dann den zweiten usw.. Es wird auch davon ausgegangen, dass wenn ein Arbeitsgang beendet wird auch keine Maschinenlaufzeiten anfallen.
Sehen Sie dies bitte als ganz deutlichen Unterscheid zur [Mehrmaschinenbedienung](Maschinenzeiterfassung.htm#manuelle Bedienung).

## Zeitverteilung $PLUS Es werden bei den Zeitverteilungen keine Arbeitsgangnamen angezeigt?
A: Am Terminal werden nur die Bezeichnungen der Tätigkeiten angezeigt. Wenn nun z.B. nur die Kurzbezeichnung bei der Tätigkeit definiert ist, so wird nach der AG10: null angezeigt.
In diesem Falle stellen Sie bitte Ihre Artikeldefinition richtig.
![](Kein_Arbeitsgangname.jpg)

bzw.
![](fehlernder_Taetigkeitsname.gif)

Q: Es müssen immer soviele bzw. zuviele Arbeitsgänge bestätigt werden?

A: Gedanklich will bei der Stückrückmeldung der Mitarbeiter immer nur den Arbeitsgang zurückmelden an dem er gerade gearbeitet hat.
Andererseits ist der Ablauf so, dass ein Mitarbeiter durchaus mehrere Maschinen gleichzeitig startet, womit sich implizit ergibt, dass mehrere Arbeitsgänge parallel laufen.
Hier kommt mit dazu, dass den einzigen Zeitpunkt den ein Mitarbeiter ziemlich sicher weiß, der Beginn seiner Arbeit ist. Weshalb die Logik der **Kieselstein ERP** Zeiterfassung auch so aufgebaut ist, dass immer (nur) Beginn gebucht wird und, für den Menschen, der nächste Beginn automatisch die zuletzt gebuchte Tätigkeit beendet. Womit sich implizit ergibt, dass damit auch alle, noch nicht vollständig beendeten Arbeitsgänge, wenn die Stückrückmeldung aktiv ist, eingegeben werden müssen.

Q: Welche Arbeitsgänge werden für die Stückrückmeldung vorgeschlagen?

A: Früher wurden immer alle seit dem letzten Kommt angestempelten und nicht als Fertig gekennzeichneten Arbeitsgänge für die Buchung der Gut/Schlecht Stück vorgeschlagen.
Da gedanklich der Fertigungsmitarbeiter aber mit der Stückrückmeldung, bzw. der als Auslöser erforderlichen Ende (bzw. Geht) Buchung davon ausgeht, dass er damit diese Arbeitsgänge abgeschlossen hat, wurde dies nun insoweit eingeschränkt, dass die seit dem letzten Ende bzw. seit dem letzten Kommt angebuchten Arbeitsgänge, die noch nicht als fertig gekennzeichnet wurden, angezeigt werden und somit die Stückrückmeldung abgefragt wird.
Siehe dazu bitte auch Parameter [EINZELARBEITSGANG_BUCHEN](Maschinenzeiterfassung.htm#Einzelarbeitsgang).

Q: Es erscheint Fehler In Zeitdaten

A: Erhalten Sie bei der Auflösungsbuchung mehrere gestarteter Lose eine Fehlermeldung ähnlich der nachstehenden,
![](Fehler_in_Zeitdaten1.gif)
so bedeutet dies, dass in den Zeitbuchungen der Person (Es wird dazu das Kurzzeichen in Klammern angegeben, in diesem Falle JH) zum angegebenen Zeitpunkt eine ungültige Zeitbuchung gegeben ist. In diesem Falle muss zuerst diese Fehlbuchung korrigiert werden und danach die Ende-Buchung erneut durchgeführt werden.
Hinweis: Es kann durchaus sein, dass die Fehlbuchung zwar auf das gleiche Los, den gleichen Arbeitsgang zeigt, aber durch eine andere Person ausgelöst wurde.

So könnte in der Zeiterfassung z.B. angezeigt werden:<br>
![](Fehler_in_Zeitdaten2.jpg)<br>
Das bedeutet, dass durch die fehlende Endebuchung der Pause / Unterbrechung keine Zeitverteilungsberechnung gemacht werden kann, was oben angezeigten Fehler verursacht.
Bitte stellen Sie zuerst die Buchungen richtig.

Q: Wie kann man Lose / Arbeitsgänge gemeinsam starten und abweichende Maschinen auswählen?

A: Verwenden Sie dafür bitte die Buchung direkt auf [Lose, Arbeitsgänge](#Direkt auf Lose buchen).
Hier hat sich bewährt, dass für jede Maschinengruppe ein Terminal zum Einsatz kommt. D.h. jedes Terminal ist eindeutig einer Maschinengruppe zugewiesen.
![](Maschinengruppe_definieren.gif)
Nun wählen Sie, nach dem Scan des Personenbarcodes, Lose. Es werden nur die Arbeitsgänge angezeigt, die der Maschinengruppe zugeordnet sind.
Nun planen Sie, welche Arbeitsgänge Sie auf welchen Maschinen tatsächlich machen.
D.h. zuerst den Arbeitsgang antippen und dann Tipp auf Maschine ![](Maschinen_auswaehlen.jpg)
Nun wählen Sie verketten ![](Verketten.jpg) und tippen nun die Arbeitsgänge an, die sie gemeinsam starten möchten und bestätigen erneut mit verketten.

Q: Kann das Terminal auch per WLan angebunden werden?

A: Technisch betrachtet kommen alle neuen Terminals auch mit einem WLan Anschluss. Unsere Meinung zum Thema WLan ist, WLan ist super für mobile Geräte, wie z.B. den mobilen Barcodescanner. Unser Rat: Belasten Sie Ihr WLan bitte nicht mit stationären Geräten. Die Bandbreite des WLans ist stark begrenzt. Halten Sie dies daher für wirklich mobile Geräte frei.
Hier kommt oft mit dazu, dass die Terminals mitten in den Produktionshallen montiert sind. Da wird Roboter geschweisst und Laser geschnitten und gefräst und Kabel gekrimpt usw. usf.. Das bedeutet, dass die Funkausbreitung oft von den Abstrahlungen der teilweise schon sehr alten Maschinen massiv gestört wird. Und natürlich ist diese Störung jede Minute anders. Daher: Investieren Sie den Aufwand und verkabeln Sie ihre stationären IT-Geräte ordentlich. Denken Sie dabei auch an die mögliche Potentialverschiebung zwischen Switch(en) und Gerät(en). Für Fragen dazu wenden Sie sich bitte vertrauensvoll an Ihr **Kieselstein ERP** Team. Aufgrund unserer lange Geschichte haben wir auch in diesen Bereichen entsprechende Erfahrungen, die frisch ausgelernte IT-Techniker nicht einmal vom Hörensagen kennen.

Auch zeigt die Praxis immer wieder, dass WLan meist nicht die erforderliche Stabilität für einen echten Dauerbetrieb hat.

Q: Mit welcher Zeit bucht das Terminal?

A: Früher wurden alle Zeitbuchungen mit der Uhrzeit (dem Zeitstempel) des erfassenden Gerätes, wie z.B. Terminal, gebucht. Seit einiger Zeit wird eine eventuelle geringe Abweichung zwischen den Uhrzeiten der Terminals gegenüber dem Server in der Form gehandhabt, dass wenn diese Differenz unter 10Sekunden ist (siehe bitte dazu Parameter TOLERANZ_ZEITABWEICHUNG_TERMINAL), so wird für die Buchung exakt die Zeit des Servers verwendet. Ist die Zeitdifferenz größer, weil z.B. Daten vom mobilen Barcodestift nachübertragen werden, wird die Zeit aus der original Zeitbuchung verwendet.

<a name="Doppelbuchung vermeiden"></a>Q: Wie können Doppelbuchungen vermieden werden?

A: Es kommt immer wieder vor, dass Mitarbeiter nicht so genau wissen ob sie jetzt nach Hause gehen oder kommen. D.h. es werden einfach die falschen Knöpfe betätigt und in der Regel fällt dem Mitarbeiter dies bei der Prüfung der Buchung durch die Rückmeldung des Terminals auf.

Nun korrigiert der Mitarbeiter seine Buchung innerhalb z.B. 30Sekunden.
Damit hat er für fast den gleichen Zeitpunkt eine gültige und eine ungültige Buchung erfasst. Um dies nun zu vermeiden gibt es mit dem Parameter VORHERIGE_ZEITBUCHUNG_LOESCHEN die Möglichkeit, Ihr **Kieselstein ERP** so einzustellen, wenn eine Zeiterfassungsbuchung vom Terminal an den **Kieselstein ERP** Server übertragen wird, und es sind mehr als die eingestellten Sekunden (welche >0 sein müssen) vergangen, so werden wenn dies eine KOMMT/GEHT/UNTERbrechungsbuchung von der Quelle ist, welche mit ZT beginnt (also Terminal), so die letzte Buchung des Tages gesucht. Wenn diese nicht länger als die Anzahl der Sekunden vergangen ist und es ebenfalls eine KOMMT/GEHT/UNTER - Buchung ist, dann wird diese gelöscht (auch wenn diese Buchung von einer anderen Quelle kam).

Besonderheit die sich durch die automatische Schichterkennung ergibt.
Wird, z.B. beim Geht, fälschlicherweise eine Kommt-Buchung durchgeführt so wird aufgrund der Schichterkennung damit das Zeitmodell des Tages geändert. Nun stellt der Mitarbeiter seinen Fehler fest und korrigiert innerhalb des Zeitraums (30") die Buchung auf ein Geht, so wird damit das an dem Tag vorhandene Zeitmodell (wieder) gelöscht.

### Können vom Terminal aus auch Ablieferetiketten gedruckt werden?
Es kann dies über die Arbeitsplatzparameter (System) eingerichtet werden.
Nutzen Sie dafür als PC-Name
![](Arbeitsplatzparameter.gif)  die in der Zeiterfassung angegebene ![](Quelle_der_Zeitbuchung.jpg) der Zeitbuchung des jeweiligen Terminals.

Tragen Sie hier beim Arbeitsplatzparameter DRUCKERNAME_MOBILES_LOSABLIEFER_ETIKETT den entsprechenden Druckernamen ein.

Für die Verwendung von Windows-Druckern von einem Linux-Server aus, ist ein spezieller Printservice einzurichten. Bitte wende dich an deinen **Kieselstein ERP** Betreuer.

Hinweis: Sollten ev. Terminal-PC-Name falsch geschrieben sein, so passe bitte den Namen des Terminals an. Vermeiden diese Einträge zu löschen.

Um Varianten der Ablieferetiketten zu drucken nutze das Feld Vorzugsteil aus dem Artikel der Stückliste. Nutze dafür, je nach Standort des Terminals auch den Eintrag im Reiter **Kieselstein ERP** - 2 in der Konfiguration des Terminals unter Kennung.

<a name="Materialbuchung"></a>Q: kann auch Material direkt am Terminal gebucht werden?

A: Es kann mit einem speziellen Barcode die Materialbuchung am Terminal aufgerufen werden.
![](Terminal_Materialbuchung.jpg)
Mit dieser Funktion erhalten Sie eine Liste der Sollmaterialpositionen des Loses. Zugleich werden je Sollmaterialposition die Sollmenge, die bisher entnommene Menge angezeigt. Zusätzlich wird als Entnahme die offene Menge vorgeschlagen. Hier können die Mengen korrigiert werden. Sind alle Positionen bestätigt kann durch Tipp auf den grünen Haken die Materialbuchung zum Server gesandt werden. Konnten die Buchungen durchgeführt werden, so wird die Darstellung verlassen und wieder auf einen neuen Mitarbeiter gewartet. Konnte das Material nur teilweise verbucht werden, wird bei den Sollmaterialpositionen bei denen die Buchungen nicht durchgeführt werden konnten entsprechende Fehlermeldungen angezeigt.
![](Terminal_Materialbuchung_Fehler.gif)
Bitte beachten Sie, dass die Materialbuchungen, nach dem Tipp auf den grünen Haken auf einmal durchgeführt werden. Es wird dann das Ergebnis für alle Artikel / Positionen aufgelistet.
Eine Rückgabe von Material ist derzeit in dieser Funktion nicht vorgesehen. D.h. es muss immer eine positive oder keine Entnahme durchgeführt werden.

Info:
Um zu erreichen, dass das Material nicht automatisch mit der Losausgabe, also der Änderung des Losstatuses, ausgegeben wird, muss entweder in der Stückliste Materialbuchung bei Ablieferung aktiviert sein, bzw. der Parameter KEINE_AUTOMATISCHE_MATERIALBUCHUNG aktiviert sein.
Zusätzlich kann auch die Handausgabe ![](Handausgabe.gif) zur Änderung des Losstatuses verwendet werden.

<a name="Direkt auf Lose buchen"></a>Q: Kann direkt auf Lose gebucht werden?

A: Um die digitale Fertigung zu unterstützen, muss in der Konfiguration des Terminals auch der Button Lose aktiviert sein. Wenn dem so ist, erscheint in der Button-Leiste auch der Knopf Lose
![](Nettop_Lose.jpg)

D.h. nach dem Scan der Ausweisnummer tippen Sie auf Lose. Somit erscheint die Liste der offenen Arbeitsgänge.
Diese ist, ja nach Konfiguration, gegebenenfalls auf eine Maschine bzw. eine Maschinengruppe eingeschränkt (Siehe Konfiguration).

![](Nettop_Losauswahlliste.jpg)

Die Arbeitsgänge sind vom ältesten zum aktuellsten Arbeitsgang sortiert, was bedeutet, dass Sie eigentlich den ersten Arbeitsgang zuerst beginnen sollten.
Es werden dabei nur die Arbeitsgänge der Lose im Status in Produktion bzw. teilerledigt angezeigt.
D.h. achten Sie bei der Planung darauf, nur diejenigen Lose auszugeben, die tatsächlich gefertigt werden sollten. Gegebenenfalls trennen Sie zwischen Los ausgegeben und Los in Produktion. Siehe dazu den Parameter AUSGABE_EIGENER_STATUS.

-   Mit Start beginnen Sie die Tätigkeit auf dem Arbeitsgang mit der entsprechenden Maschine.

-   Mit Fertig wird der Arbeitsgang als vollständig abgearbeitet gekennzeichnet.

-   Mit Verketten wird die gemeinsame Arbeit auf verschiedenen Arbeitsgängen von unterschiedlichen oder gleichen Losen gestartet.

-   Mit Ende beenden Sie alle aktuell laufenden Tätigkeiten. Dies wird insbesondere für das Beenden der Verkettungsbuchungen genutzt um die entsprechende Zeitverteilung zu erreichen.

-   Mit Material starten Sie die [Materialentnahmebuchung](#Materialbuchung) auf das Los durch tipp auf
    ![](Nettop_Losauswahlliste_Materialbuchung.jpg)

-   Mit Abliefern werden entsprechende Mengen dieses Loses abgeliefert.

-   Mit Losgröße kann die jeweilige Losgröße des Loses geändert werden.

-   Mit Maschine kann der Arbeitsgang auf einer anderen Maschine als beim Arbeitsgang hinterlegt durchgeführt werden.

-   Mit Info, erhalten Sie das Artikelstammblatt der Stückliste in einer PDF Sicht dargestellt

-   Mit Kalkulation sehen Sie die aktuelle Nachkalkulation des Loses mit allen Daten und den entsprechenden Zeitentwicklungen der Arbeitsgänge, gegliedert nach Personen bzw. Maschinen.
    Bei Kalkulation werden die Daten ebenfalls in einer PDF Sicht dargestellt.

-   Mit Dokumente werden die Dokumente des [Dokumentenlinks](#Dokumentenlink) des Stücklistenartikels des Loses angezeigt.

-   Mit Kommentar sehen Sie das Artikelstammblatt der Stückliste des Loses. D.h. hier können z.B. die Zeichnungen Ihrer Stückliste hinterlegt und angezeigt werden.

-   Mit Losinfo wird aus dem Los die Produktionsinformation (Modul Los, Menüpunkt Los, Drucken, Produktionsinformation) angezeigt.

Um die Losauswahlliste gut bedienen zu können, ist eventuell eine Anpassung des Timeouts für diese Liste erforderlich. Wählen Sie in der Konfiguration, im Reiter Allgemein das Timeout für Lose. Bewährt hat sich hier ein Wert von ca. 60 Sekunden. ![](Nettop_Los_Timeout.jpg)

Q: Welche / wieviele Arbeitsgänge werden angezeigt?

A: Aus Geschwindigkeitsgründen werden immer nur die 200 ältesten Arbeitsgänge angezeigt. Da dies auch abhängig von der Einschränkung nach Maschine bzw. Maschinengruppe ist, wird unter Umständen eine andere Anzahl von Arbeitsgängen angezeigt. Zusätzlich greift hier noch der Parameter ZEITVERTEILUNG_TYP. D.h. bei Typ = 2 werden gegebenenfalls nur der Rüst- und der erste Umspann-Arbeitsgang je Arbeitsgang angezeigt. Bitte achten Sie auf eine realistische Zeitplanung Ihrer Lose und Arbeitsgänge, bzw. aktivieren Sie dafür im Reiter HeliumV -2 die Maschinengruppe bzw. die Maschinenidentifikation.

Q: Wie verkettet man Lose / Arbeitsgänge?

A: Tippen Sie auf Verketten. ![](Los_Auswahlliste_verketten.jpg)Nun sind alle anderen Symbole grau und auch die Liste der Arbeitsgänge ist nicht markiert.
Tippen Sie nun diejenigen Arbeitsgänge an, die Sie gemeinsam starten möchten. Zum Abschluss Tippen Sie wieder auf verketten.
Damit werden die Buchungen für die zukünftige Verteilung hinterlegt.
Sollten Sie nun erneut das verketten starten wollen, so muss zuerst diese Verteilung beendet werden. Anderenfalls erscheint die Meldung
![](Los_Auswahlliste_offene_Verteilungsbuchung.jpg) Beenden Sie bitte vorher die vorhandenen Verteilungsbuchungen.
Das Beenden kann z.B. durch das Einscannen von Ende erfolgen.

Für die Verteilungsberechnung beachten Sie bitte unbedingt das unter [$PLUS](#Zeitverteilung $PLUS) geschriebene.

<a name="Dokumentenlink"></a>Q: Kann man auch Zeichnungen eines Loses ansehen?

A: Ja. Wählen Sie einen Arbeitsgang eines Loses und tippen Sie auf
![](Los_Auswahlliste_Dokumente_anzeigen.jpg) Dokumente.
Nun erscheint die Liste der für den Stücklistenartikel definierten Dokumentenlinks welche z.B. wie folgt aussehen könnte.
![](Los_Auswahlliste_Dokumentenlinks.jpg)
Zur Definition der Dokumentenlinks siehe bitte, System, Mandant, Dokumentenlink.
Wählen Sie nun das entsprechende (Unter-)Verzeichnis, tippen dann die gewünschte Zeichnung an und tippen dann auf die Lupe um die Zeichnung im jeweiligen Viewer anzuzeigen.

**Wichtig:** Das Terminal muss auf den hinter den Dokumentenlinks liegenden Netzwerkpfad zumindest lesende Zugriffsrechte haben, anderenfalls eine entsprechende Fehlermeldung erscheint
![](Los_Auswahlliste_Dokumente_nicht_gefunden.jpg)

Bei der Anzeige werden derzeit folgende Dateitypen unterstützt:
pdf, tif, png, jpg und dxf/dwg. Für dxf/dwg muss der FreeDwgViewer installiert sein, für PDF der AcrobatReader.

Zum Verlassen der Auswahlliste tippen Sie bitte auf die Tür.

Q: Was bedeutet die Fehlermeldung

A: Wird die Fehlermeldung: 404 - Datei oder Verzeichnis nicht gefunden
![](Los_Auswahlliste_Dokumente_nicht_gefunden2.gif)
angezeigt bedeutet dies, dass Ihr **Kieselstein ERP** Server auf den Zeichnungspfad kein lesendes Recht hat.

Q: Es kommt immer die Fehlermeldung Maschine nicht gefunden

A: Wenn eine Fehlermeldung ähnlich dieser kommt
![](Maschine_nicht_gefunden.jpg)
so prüfen Sie, dass die Definitionen am Terminal mit der Definition für Ihr **Kieselstein ERP** übereinstimmen.
So ist gerne die Länge der Belegnummer bzw. die Anzahl der Jahresstellen falsch.
So waren in diesem Falle die Stellen vertauscht.
![](Stellen_vertauscht.gif)
Was bedeuten würde, dass es nur zweistellige Belegnummern gäbe und das Jahr 7 Stellen hat.
Achten Sie bitte auch darauf, dass das Trennzeichen in den Belegen keinen - Bindestrich enthalten darf.
Hinweis: Diese Meldung kann auch bei der Auswahl aus der Los-Auswahlliste vorkommen. Die Ursache ist die gleiche.

Q: Buchen auf eine abweichende Maschine

<a name="Maschinenauswahl"></a>A: Durch Tipp auf ![](Maschinenauswahl_aufrufen.jpg) Maschine kann abweichend zur durch den Arbeitsplan, Fertigungsbegleitschein geplanten Maschine eine andere Maschine ausgewählt werden.
![](Maschine_auswaehlen.jpg)
Im oberen Bereich kann durch Tipp auf die Maschinengruppe die Auswahl auf eine Maschinengruppe eingeschränkt werden.
Um dies für den Mitarbeiter so einfach wie möglich zu gestalten, kann die hier vorgeschlagene Maschinengruppe im Personal, Daten, Maschinengruppe vorbesetzt werden.

![](Maschinengruppe_vorbesetzen.gif)

Q: Der RFID Leser geht nicht mehr?

A: Es kann, mit Stromschwankungen, verschiedensten anderen Störungen leider vorkommen, dass eines der USB Geräte nicht mehr funktioniert. Hier hat sich bewährt, diese Geräte wirklich stromlos zu machen um sie wieder ordentlich neu zu initialisieren. D.h. bitte das Terminal herunterfahren und danach komplett ausschalten und komplett vom Strom nehmen. In der Regel werden aktuell Netzwerkkarten und USB Geräte auch dann noch versorgt, wenn zwar der Rechner angeblich abgeschaltet ist, aber noch am Stromnetz angeschlossen ist. Warten Sie dann bitte nach dem abstecken ca. 5-10Minuten und schließen Sie danach das Terminal wieder an. Die Wahrscheinlichkeit dass dann der RFID Leser oder auch ein Barcodescanner wieder funktioniert ist sehr hoch.

Q: Mein Barcodeleser liest nicht mehr, reagiert nicht

A: Heutzutage kann man alle Barcodescanner programmieren. Wenn nun versehentlich z.B. eine falsche Schnittstelle ausgewählt wurde, kann es Situationen geben, dass der Barcodescanner nicht mehr reagiert. Hier ist es bei den von uns gelieferten Scannern so, dass Sie in diesem Falle den Scanner abstecken, die Scantaste drücken und halten und dann den Scanner anstecken. Der Scanner geht nun in einen Resetmodus und sollte wieder funktionieren. Beachten Sie bitte, dass damit auch der Scanner neu zu parametrieren ist.
![](Set_to_Std_USB_COM.png)
Für die Verwendung als Barcodescanner für unsere Terminals nutzen Sie bitte obigen Setupcode.
ACHTUNG: Je nach Anschluss am Terminal, ist die Terminalsoftware neu auf die nun verfügbare COM-Schnittstelle einzustellen.

In sehr sehr wenigen Fällen, funktioniert auch das nicht. Hier muss der Scanner Softwaretechnisch zurückgesetzt werden. Auch dies ist, mit Ihrer Unterstützung, per Fernwartung möglich. Bitte melden Sie sich in diesem Falle bei uns.

Sollte der Scanner nach dem Anstecken und bei laufendem Terminalprogramm beim Scannen ein komisches GrGr ausgeben, so bedeutet dies, dass der Com-Port des Scanners nicht registriert ist. D.h. er muss im Konfigurationsmodus des Terminals, richtig eingestellt werden.

**Wenn Sie den Scanner neu aufsetzen / per Barcode parametrieren, so achten Sie unbedingt darauf NUR die Definition für obigen COM Port zu verwenden.**

Q: Wie wird das Terminal an der Wand montiert?

A:

![](Montage_Terminal.jpg)

Q: Wie kann ein Terminal neu gestartet werden?

A: Sollte ein Terminal neu gestartet werden müssen, so betätigen Sie bitte kurz und mit Gefühl den Ein-Ausschalter.
Dieser befindet sich, je nach Version, entweder an der rechten unteren Ecke, oder rechts oben an der Rückseite.

        ![](Terminal_Ein_Aus.jpg)                      ![](Terminal_Ein_Aus1.jpg)                   

Nun sollte das Terminal innerhalb von fünf Minuten heruntergefahren und ausgeschaltet sein. Geht das nicht, so drücken Sie bitte ca. 10Sekunden auf den Ein/Ausschalter und halten diesen. Bitte mit Gefühl (und nicht mit roher Gewalt) solange bis das Terminal sich ohne herunterfahren ausschaltet.
Danach schalten Sie das Terminal durch erneutes Drücken auf den Ein-Ausschalter wieder ein.
Achten Sie bitte auf etwaige Fehlermeldungen, z.B. Com xx nicht vorhanden

Achten Sie beim Umstecken der angeschlossenen Geräte, z.B. Barcodescanner, darauf, dass diese wieder exakt an den gleichen Steckplatz angeschlossen werden.
Werden unter mehreren Terminals die Scanner getauscht, wird jedem Scanner auf jedem Terminal ein neuer Com-Port zugeordnet, das muss in jedem Falle konfiguriert werden.
Bitte achten Sie auch darauf, dass ab Verwendung von Windows 10 am Terminal, die Terminalversion 1.1.2.2 mit einer speziell eingerichteten Zeitsynchronisierung verwendet werden sollte.

Achten Sie auch darauf, dass Ihr **Kieselstein ERP** Server Zeitsynchron ist. Die Terminals synchronisieren gegen den **Kieselstein ERP** Server und solange dieser mit der tatsächlichen Uhrzeit synchron ist, können die Terminals die richtige Zeit anzeigen.

Q: Was alles prüfen, wenn das Terminal keine Verbindung zum **Kieselstein ERP** Server bekommt

A: Nachfolgend eine Liste von Punkten die geprüft werden sollten, wenn die Verbindung vom Terminal zu Ihrem **Kieselstein ERP** Server nicht möglich ist.
Unter Verbindung verstehen wir, dass sich das Terminal lizenzieren darf. Da das Terminal eine Vielzahl von Parametern hat, mit denen das Verhalten gesteuert werden kann, haben wir hier nur die datentechnische Verbindung beschrieben.

-   Stimmt der Servername
    Tragen Sie gegebenenfalls anstelle des Servernamens die IP Adresse ein
    Probieren Sie, ob Sie vom Terminal aus den Server Pingen können

-   Sitzt das Terminal hinter einem VPN Tunnel

-   Geht die Anwesenheitsliste?
    Nutzen Sie dafür den [Weblink]({{< relref "/docs/stammdaten/system/web_links" >}})
       
    Bitte um Beachtung, dass dafür auch ein Benutzerrecht in der Rolle des Terminalbenutzers gegeben sein muss

-   Stimmt die Art der Verbindung (http/https)

-   Darf das Terminal über seinen Port, in der Regel 8080, auf den Server zugreifen
    Gegebenenfalls muss dieser Port in der Firewall Ihres **Kieselstein ERP** Servers freigeschaltet werden. Achten Sie dabei auf die Sicherheit!

-   Ist die eingestellte Sprache, die Locale im Reiter **Kieselstein ERP**, richtig. D.h. können Sie sich am **Kieselstein ERP** Client mit der im Terminal eingerichteten Sprache auch anmelden

-   Gibt es vom Terminal Protokolleinträge in der Server Log Datei?
    Sind diese nicht gegeben, so verhindert die Firewall, die Sprache, die IP-Adresse dass überhaupt ein Zugriff stattfindet

Funktioniert die Verbindung, so ist als nächster Schritt die richtige Einstellung der Belegnummern wesentlich.
Sollten Sie unlogische Fehlermeldungen erhalten, so stimmen diese Einträge nicht.

Q: Am Terminal kommt der Losstatus von < > erlaubt keine Buchung

Wenn das Terminal beim Start z.B. folgende Fehlermeldung anzeigt, so sind in der Terminal-internen-Datenbank ungültige Buchungen gespeichert.
![](Losstatus_erlaubt_keine_Buchung.jpg)
Um diese zu löschen, gehen Sie in die Konfiguration,
Reiter Allgemeines und klicken dann bei Offline-DB ganz rechts auf den Button mit den ... drei Punkten.

![](Fehler_in_interner_Datenbank.jpg)

Nun kann die interne Datenbank bearbeitet werden.
Achten Sie darauf, dass wirklich nur die defekten Datensätze entfernt werden.

![](Fehler_in_interner_Datenbank2.jpg)