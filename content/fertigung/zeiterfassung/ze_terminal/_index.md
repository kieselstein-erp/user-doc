---
title: "Zeiterfassung Terminal"
linkTitle: "Terminal"
categories: ["Zeiterfassung"]
tags: ["Zeiterfassung","Terminal"]
weight: 220
description: >
  Zeiterfassungsterminal
---
Beschreibung der Funktionen des **Kieselstein ERP** Zeiterfassungsterminals

basierend auf der Version 0.0.9.0

[Für die Installation siehe]( {{<relref "/docs/installation/04_ze_terminal"  >}} )

Der Zugriff erfolgt über Standard Schnittstellen (RestAPI) deines **Kieselstein ERP** Systems, welche selbstverständlich auch von anderen Geräten / Funktionen genutzt werden können. In welcher Form die Schnittstellen ausgeführt werden, ist noch von der Technik festzulegen.

Aktuell steht die Software des Zeiterfassungsterminals unter Windows und Android zur Verfügung.

Hinweis: Die weitere Betreuung von Terminals der Firma MECS ist nicht vorgesehen. Diese haben zwar den Vorteil sehr robust zu sein, sie laufen seit ca. 20Jahren in rauer Betriebsumgebung, werden aber nicht mehr produziert und auch nicht supported. Es sollten daher anstatt dessen entsprechend geeignete Industrie PC's eingesetzt werden.

## Anwesenheitszeiterfassung
Für die Anwesenheitszeiterfassung identifiziert sich der/die MitarbeiterInn mittels Barcode Ausweisnummer bzw. RFID Chip bzw. aus der Anwesenheitsliste.<br>
Somit wird der MitarbeiterInnen Name angezeigt.
![](Anwesenheitszeitbuchung_01.png)  
Unter dem Namen werden die direkt verfügbaren Knöpfe / Funktionen angezeigt, wobei Saldo deaktiviert werden kann.<br>
Durch Tipp auf Kommt, Pause, Geht, wird die entsprechende Buchung durchgeführt und direkt zum Server gesandt. Ist keine Serververbindung verfügbar, werden die Buchungen zwischengespeichert.

Darunter werden die letzten **Heute** gebuchten Zeiten angezeigt.

Diese Anzeige bleibt solange bestehen, bis entweder einer der drei Buttons getippt wird oder bis man Abmelden antippt, was ein Verlassen der Erfassung ohne Buchung bewirkt.

Der Tipp auf Saldo zeigt den aktuelle Gleitzeit und Urlaubssaldo<br>
![](Saldo_01.png)  <br>
Tippt man hier auf Detail, so wird die Monatsabrechnung des aktuellen Monates angezeigt.
![](Monatsabrechnung_01.png)  <br>
Tippt man hier auf den linken Pfeil, kann man um ein Monat zurückblättern, bzw. mit dem Pfeil nach rechts ![](Monatsabrechnung_02.png)  wieder vorwärts blättern.<br>
Mit dem X wird die Anzeige verlassen.

**Hinweis:** Der Zeitsaldo sind die noch zu erbringenden Stunden, bis Ende des Monates, gerechnet vom heutigen Tag weg. D.h. hier sieht der Mitarbeiter / die Mitarbeiterin wieviel er/sie bis zum Monatsende noch zu leisten hat, um auf einen ausgeglichenen Monatssaldo zu kommen.<br>
Bitte beachte, dass der verfügbare Urlaub üblicherweise in Tagen dargestellt wird. Ist das Zeitmodell auf Teilzeitmodell gestellt, so wird der verfügbare Urlaub in Stunden angezeigt.

**Hinweis2:** Sind im Zeitmodell nur die Monatsstunden definiert, so können die Ansprüche nur bis jeweils zum Ende des aktuellen Monates errechnet werden.

### Es wird keine Monatsabrechnung angezeigt
Kann eine Monatsabrechnung vom Server nicht gültig erstellt werden, wird diese nicht an das Terminal übergeben. Um herauszufinden warum, lass dir die Monatsabrechnung der Person am Client anzeigen. Dann kommt eine entsprechende Fehlermeldung. Wird diese behoben, wird die Monatsabrechnung auch am Terminal angezeigt.

## Bedienung des Terminals mittels Barcode
Da neben der Anwesenheitszeiterfassung das Terminal vor allem für die Erfassung der verschiedensten Produktionszeiten (Mensch und Maschine) gedacht ist und in aller Regel noch immer Fertigungsbegleitpapiere mitgegeben werden müssen, ist die Bedienung des Terminals auf Barcode ausgerichtet. Ob diese Barcodes mittels einfacher eindimensionaler Strichcodes oder mittels QR-Code erfolgt ist nur vom eingesetzten Barcodescanner abhängig. Das Terminal selbst bekommt das nicht mit.

Welche Barcodes werden vom Terminal unterstützt? [Siehe dazu]( {{<relref "/docs/installation/11_barcode" >}} )

Damit die Beschreibung etwas leichter lesbar ist, verwenden wir hier die sich in der Praxis eingebürgerte Form von ich, kombinierter Code .... usw., wobei ich bedeutet, es wird:
- der Barcode der Ausweisnummer des/der MitarbeiterInn gescannt oder
- die RFID Karte der/des MitarbeiterInn gelesen
- über die Anmeldeseite der/die MitarbeiterInn ausgewählt

### Ablauf abweichende Maschine
Ich, abweichende Maschine, Kombicode.
- Liste der möglichen Maschinen siehe, Zeiterfassung, Maschinen, Journal, Maschinenliste, Barcodeliste
- Kombicode: Dieser muss so eingestellt sein, dass anstelle der Maschine ?? übergeben wird. Nur damit kann erreicht werden, dass die Maschine übersteuert werden kann. Wird keine Maschinen-Übersteuerung angegeben, wird automatisch die Zeit auf die im Los-Arbeitsgang hinterlegte Maschine gebucht. -> ist aktuell zu prüfen. D.h. es muss üblicherweise im Kombicode die Maschine enthalten sein. Obige Beschreibung stimmt aktuell (2023-10-31) nicht.

### Ablieferbuchung am Terminal
Durch Scann von Ich, Losnummer, Ablieferung gelangt man in die Ablieferbuchung.<br>
![](Ablieferbuchung.png)  <br>
Gib hier die Menge der guten Teile bei Gut ein. Sind Teile schlecht, da z.B. die Maßgenauigkeit nicht geben ist, so gib diese bei Schlecht ein. Mit dem Tippen auf Ok wird die Ablieferbuchung durchgeführt.
Stellst du vorher dem Ok noch den Schalter bei Etikett drucken auf Ein, so wird zusätzlich zum Ablieferetikett auch das Mobile Etikett gedruckt.
Bei der Schlecht Buchung wird kein Etikett gedruckt.
Die Schlecht Menge wird auf das erste definierte Lager der Lagerart Schrott gebucht. Ist dieses nicht definiert, erfolgt die Buchung ebenfalls auf das Hauptlager, allerdings in einer eigenen Ablieferbuchung.

#### Ablieferung mit Info Text
![](Ablieferbuchung_mit_Infotext.png)  
In gewissen Anwendungsfällen, will man den buchenden MitarbeiterIn darauf hinweisen, dass er unbedingt die bei der Ablieferbuchung erzeugten Ablieferetiketten auch in die Lieferung, die Verpackung der Lieferung, mit dazugeben muss.

Das kann dadurch erreicht werden, dass:
- der Stücklistenartikel des Loses wird mit einem definierten Vorzugsteil gekennzeichnet
- Am Terminal wird in der Konfiguration unter
![](Ablieferbuchung_ausloesen.png)  
die Kennung des [Vorzugsteiles]( {{<relref "/docs/stammdaten/artikel/#wie-kennzeichnet-man-vorzugsteile" >}} ) eingetragen. Damit erscheint der Text: Bitte Begleitpapiere beilegen. Nun muss noch deine Ablieferetikettte wie oben beschrieben entsprechend für den Druck eingerichtet sein. Natürlich sollte der Druck der Ablieferetikette dieses Terminals auf den nächstliegenden Drucker gedruckt werden. Daher empfehlen wir, den Terminals fixe IP-Adressen zu geben um eine stabile Zuordnung zu erreichen.

### Materialbuchung am Terminal
Durch scann von ich, Materialentnahme für Los, kann am Terminal geplantes Material in das Los gebucht werden.
![](Materialbuchung_01.png)  
Es werden hier die tatsächlich entnommenen Mengen in der Spalte Entnahme eingegeben.
Durch Tipp auf Ok wird dann versucht die angegebenen Mengen vom Lager in das Los zu buchen.<br>
Können einzelne Positionen nicht gebucht werden, so erscheint zuerst eine allgemeine Meldung<br>
![](Materialbuchung_02.png)  <br>
welche angibt wieviele Buchungen nicht durchgeführt werden konnten. Nach Bestätigung derselben sieht man in der nun dargestellten Liste was die genaue Fehlerursache für die Nicht-Durchführung der jeweiligen Lagerentnahme Buchung ist. Es wird dies in der Regel ein fehlender Lagerstand gegebenenfalls auch für die jeweilige Serien- bzw. Chargennummer sein.<br>
Bitte beachte, dass die durchführbaren Buchungen auch gemacht wurden.

Du kannst nun den fehlenden Lagerstand z.B. über einen **Kieselstein ERP** Client nachbuchen und die Materialentnahmebuchung mit Ok noch einmal versuchen, oder mit Abbrechen die Materialbuchung beenden.

[Für die Materialbuchungen stehen folgende Barcode Definitionen zur Verfügung]( {{<relref "/docs/installation/11_barcode/#barcode-definitionen-f%C3%BCr-materialbuchungen" >}} )

#### zusätzliches Material buchen
Am Terminal kann mit den Artikel-Barcode-LeadIns $I, ohne LeadIn und den QR-Codes nach VDA4992 zusätzliches Material in das Los gebucht werden.

Hinweis: In der mobilen App wird dies derzeit (noch) nicht unterstützt.

### Losablieferetiketten
Für den Druck der Losablieferung Etiketten ausgelöst durch das Terminal, aber auch jedes andere mobile Device, wurde definiert, dass derzeit immer nur ein Exemplar gedruckt wird (auch wenn theoretisch die Schnittstelle mehr könnte).

Eigentlich so, dass wenn keine SNR dann eine Etikette und mit Seriennummer je Seriennummer je eine -> Also: machen wir mal immer eine Etikette.<br>
D.h. von der Idee her, wird in der Ablieferung jede Seriennummer einzeln erfasst und damit auch einzeln gedruckt.

### Einrichten der Etikettendrucker für das Terminal
Dafür fixe IP Adresse des Terminals erforderlich
Siehe Arbeitsplatzparameter DRUCKERNAME_MOBILES_LOSABLIEFER_ETIKETT aber auch
DEFAULT_MOBIL_DRUCKERNAME_ETIKETT

### Anzeige der laufenden Maschinen
Immer wenn sich der/die MitarbeiterInn am Terminal identifiziert, werden unmittelbar danach anschließend die heutigen Zeitbuchungen und daran anschließend, die vom jeweiligen Mitarbeiter gestarteten Maschinen angezeigt.
![](Anwesenheitszeitbuchung_02.png)  

Ab der Version 0.0.10.0 wird bei den Maschinen auch das Buchungsdatum angezeigt.<br>
![](Anwesenheitszeitbuchung_03.png)  <br>
Damit kann man erkennen, ob gegebenenfalls vergessen wurde eine Maschine zu stoppen. Sollte dies der Fall sein, bitte eineN ZuständigeN bitten dies zu korrigieren. Beachtet dazu auch das Auto-Stop bei Geht.

### ENDE, STOP, FERTIG
#### Ende
Mit der Ende Buchung wird die laufende Tätigkeit des Mitarbeiters beendet, egal was er gerade angestempelt hat. Aus Produktions-Sicht macht er/sie nun **"nichts"**.<br>
Ein Ende bewirkt **kein** Maschinen-Stop.
#### Stop
Bei der Stop-Buchung muss immer auch eine Maschine (und ein Arbeitsgang) angegeben werden. Damit wird die Maschine gestoppt. Aus Produktions-Sicht, steht nun die Maschine. Das hat NICHTS mit der Ende Buchung zu tun.<br>
Das bedeutet auch, wenn ein Arbeitsgang komplett beendet wird, so muss STOP und danach ENDE gebucht werden, damit sowohl die eine Maschine gestoppt wird (nicht mehr läuft) und auch, dass der Mitarbeiter nun nichts mehr macht. Wenn der Mitarbeiter auf einem anderen Arbeitsgang weiter macht, muss er nur (das kann er auch ohne der Ende Buchung machen) die neue Tätigkeit anstempeln. Ist in der Tätigkeit die Maschine mit enthalten (und es ist die gleiche zu vor gestartete Maschine), so wird diese Maschine nun diesem Arbeitsgang zugeordnet.

Mit dieser Fähigkeit ist die Mehrmaschinenbedienung möglich.

#### Fertig
Fertig signalisiert, dass dieser eine Arbeitsgang beendet wurde. Es ist dies **nur** ein Status des Arbeitsganges. Er bewirkt kein Stop und kein Ende.

## Ausweisnummer, RFID Seriennummer, Barcode
Für die Zuordnung der verschiedenen Ausweismöglichkeiten zur jeweiligen Person wird die Ausweisnummer im Personal verwendet. 

## Aktualisierung der lokalen Personalliste
Da das Terminal auch Offline fähig ist, muss es lokal die möglichen Personen speichern. Um diese Information vom **Kieselstein ERP** Server zu bekommen, wird
- alle zwei Stunden die Liste aktualisiert
- immer wenn eine Buchung auf eine Person gemacht wird, wird im Online Modus diese gegen den Server geprüft. Wird nun festgestellt, dass diese Person soeben neu angelegt wurde, werden die Daten auch in der Offline Datenbank im Terminal aktualisiert.<br><br>Das bedeutet, dass in der Regel nach der Vergabe der Ausweisnummer im Client, die Person an ein Terminal geht und die Buchung versucht, auch wegen Tippfehlern bei der Ausweisnummer und ähnlichem. Damit wird dieses Terminal automatisch aktualisiert. Alle anderen Terminals werden innerhalb der nächsten ca. 120Minuten aktualisiert.

## Fehlermeldungen
Bei der Übergabe des kombinierten Barcodes vom Terminal an die RestAPI werden folgende Kriterien geprüft. Trifft eines der Kriterien nicht zu, wird ein BadRequest mit Errorcode 9 zurückgeliefert, egal welches der Kriterien nicht zutrifft.
- das Los mit der Nummer muss vorhanden sein
- der Mandant muss übereinstimmen
- es darf auf ein Los im Status Angelegt gebucht werden wenn der Parameter "ZeitdatenAufAngelegteLoseBuchbar" = 1 ist
- es darf auf ein Los im Status Erledigt gebucht werden der Parameter "ZeitdatenAufErledigtBuchbar" = 1 ist
- Der Arbeitsgang wird nur auf vorhanden geprüft. D.h. egal ob der Arbeitsgang als fertig markiert ist oder nicht, kann vom Terminal aus trotzdem darauf gebucht werden

![](BadRequest_01.png)  


## Geisterschicht

## Messmaschine, reine Maschinenzeit
Es gibt immer wieder die Herausforderung, dass parallel zu den Arbeiten am Werkstück auch gemessen werden muss. Dies wird üblicherweise so gemacht, dass während der Menschlosen-Laufzeit der Maschine, die im vorherigen Arbeitsgang gefertigten Teile gemessen werden. Üblicherweise ist die Sollzeit dafür in der Umspannzeit enthalten. Will man nun die Laufzeit der Messmaschine mit erfassen, so steht die Barcodeerfassung der Messzeit zur Verfügung.

Diese ist so gestaltet, das nur die Zeit der Messmaschine auf den Arbeitsgang, z.B. Messen gebucht wird. Als Dauer wird immer die Stückzeit für ein Stück eingebucht. Die Aufspannung wird dabei NICHT berücksichtigt, da es ja immer ein Messvorgang ist.

D.h. auch wenn z.B. fünf Teile automatisch hintereinander gemessen werden, so muss in der Sollzeitdefinition die gesamte Messzeit für die fünf Stück mit einer Aufspannung von fünf angegeben werden. Für die (nun theoretische) Istzeit wird davon ausgegangen, dass immer die fünf Stück unmittelbar hintereinander (gerne auch parallel) gemessen werden, womit die Aufspannung für die theoretische Istzeit NICHT berücksichtigt werden darf.

Das bedeutet: Wenn die Messzeit für die Messmaschine (bitte den besonderen Barcode beachten ($V mit $ML)) erfasst wird, dann wird diese
- wenn die Maschine frei ist, mit einer Zeit von Jetzt + Sollzeit (als Dauer) eingebucht
- ist die Maschine nicht frei, so wird diese Zeit unmittelbar nach dem Ende des laufenden Arbeitsgangs eingebucht.
  - ist das sich daraus ergebende Danach um mehr als 10 Sekunden später, erscheint am Terminal eine Information, dass der Vorgang um ... Sekunden später gestartet wird.


------------
# Vorabbeschreibung

Nachfolgend die Vorabbeschreibung der weiteren **gewünschten** Funktionen für das **Kieselstein ERP** Zeiterfassungs-Terminal.

- Grundsätzlich ist bei den Terminals auf die Umgebung zu achten.
  - Außen Einsatz, Temperatur von bis, Feuchtigkeit usw.
  - Außen Einsatz, bedeutet auch andere Standorte, welche mit langsamen Leitungen angebunden sind und keinen VPN Tunnel haben
  - Einsatz in der mechanischen Fertigung, Späne, Lärm (also man hört nichts), Öl tropft auf das Terminal
  - Einsatz in Tischlerei/Schreinerei, also sehr feiner Staub der die Lüftungsschlitze verklebt
- Es sollten auch ZE-Stifte angebunden werden können
- Weitere Sondertätigkeiten definierbar. D.h. der Mitarbeiter kann auch Arzt, Krank, Behörde, Kindkrank usw. am Terminal buchen
- Fehlererfassung
- Redlining
- [Papierlose-Fertigung](#papierlose-fertigung)

Hardware
- Touchbedienung, also komplett ohne weitere Zettel usw. bedienbar
  - dazu idealerweise auch integrierte RFID Leser
  - gedanklich auch Fingerprint, aber DSGVO / Gewerkschaft usw.
- Barcodescanner Bedienung
- RFID Leser, alle bisherigen und ELATEC4
- alternativ zu Touch auch Tastatur und oder Mausbedienung
  - es kommt auch immer wieder der Wunsch nach Eingabe der Personalnummer
  - aber auch Los / Auftragsnummern
  - D.h. ich stehe vor dem Terminal, gebe meine Personal oder Ausweisnummer an und danach K für Kommt, G für Geht usw.
- vorbereitet für Maschinenanbindung -> Also Maschine läuft / steht usw.
- Waagenanbindung, z.B. für das Pulver beschichten. D.h. es wird Material entnommen und auf die Waage gestellt, dann Artikelnummer und Charge gescannt und das entnommene Gewicht als Material mit der Charge ins Los gebucht.
- Anbindung von Messmitteln für die Erfassung von Prüfungen
  - z.B. Schiebelehren mit USB oder Bluethooth Schnittstelle.<br>Damit auch führen durch entsprechende Prüferfassungsdialoge
- Einlesen von Offline Zeiterfassungsstiften. Aktuell der Opticon-Reader.<br>Aus diesem Titel heraus muss es die Möglichkeit einer eindeutigen Zuordnung der verschiedenen USB Device / BlueTooth-Device zu verschiedenen Aufgaben geben. So wirkt der Opticon-Reader ganz anders als z.B. die Schiebelehre, obwohl beide über einen USB-Hub am gleichen Gerät angesteckt sind.
- Größe des Terminals je nach Einsatzfall
  - ca. 12"
  - Std 15"
  - 23" oder sogar zweiter Bildschirm für entsprechend detailreiche Zeichnungen
  - Zoom in den Zeichnungen mit zwei Finger muss möglich sein
- Das Terminal ist offline fähig. D.h. reine Datenerfassung ist auch möglich wenn keine Serververbindung. D.h. man kann Kommt/Geht buchen, Tätigkeiten starten usw., aber die Ablieferbuchung bzw. die Stückrückmeldung, Fehlererfassung oder auch Messwerterfassung ist nur Online möglich

WLan / Lan Anbindung
- Lan bevorzugt aber
- immer wieder auch WLan erforderlich

Verbindung auch per https möglich, in der aktuellen Verschlüsselungstechnik

Status-Abfragen und Infos
- Monatsabrechnung aktuell, gegebenenfalls nur bis gestern Abend oder letztes Monat
- Anzeige Urlaubs und Gleitzeit Saldo bis gestern Abend
- Urlaubs-Antrag, ZA Antrag (Krankenstands-Antrag)
- Aktueller Status eines Loses
  - Fortschrittsanzeige
- Aktueller Status eines Auftrages
- Zeichnungen und Infos des Loses, Auftrags, Artikels. Diese kommen aus:
  - Kommentar einer = bestimmter, alle, bestimmte, inkl. Dateiverweis
  - Dokumentenablage, je nach Belegart bzw. Gruppierung, Sicherheitsstufe definierbar
  Ebenfalls mehrere Belegarten, Gruppierungen
  - Dokumentenlink, auch hier angebbar welcher, z.B. nur Zeichnung und QS-Anweisung
- Wartungsanweisungen zur Maschine lesen / ansehen
  - Wartungsschritte erfassen können, mit Zeit und Material
- Terminals haben eigene Hilfe(-Seite)

Sprachen
- Deutsch
- Englisch
- Slowenisch
- Italienisch
- Polnisch

Infos zurücksenden können.<br>
D.h. der Werker stellt etwas fest und will dies an die Abteilung (welche) zurückmelden. Das kann ein Text sein, ein Foto weil ein Teil der Maschine gebrochen ist oder auch eine Zeichnung. Es hat dies einen Bezug zum Los, oder auch zur Maschine. Wie bekommt der Verantwortliche Mitarbeiter z.B. der Konstrukteur oder der Kundendisponent mit, dass was zu tun ist?

Zeit und Material erfassen können. Insbesondere Material mit Chargen und Seriennummern im kombinierten Barcode

Parameter, Buttons<br>
Alle Buttons können (am Terminal?) aktiviert bzw. deaktiviert werden. Damit verbunden sind auch ev. Funktionsaufrufe.
Zusätzlich sollte man die Konfiguration eines Terminals sehr einfach irgendwo ablegen können und dann bei einem anderen Terminal wieder einlesen können. Ziel ist, dass man nicht zig Terminals, einzeln manuell parametrieren muss. Idealerweise wird die Konfiguration beim Start des Terminalprogramms übernommen. Wobei dies wiederum durchläuft, also sollte es ein Kommando an alle Terminals geben, holt euch die Parameter neu.<br>
Aber ACHTUNG: Es gibt auch Parameter wie z.B. Maschinen-Zuordnung / Maschinengruppen-Filter, Spezielle Tätigkeiten, welche von dieser allgemeinen Parametrierung NICHT betroffen sind.
Ev. kann man am Terminal selbst dann auch definieren, den Parameter NICHT von der Zentrale aus übersteuern. Das sollte man dann z.B. farblich in der Parameteranzeige sehen.

Es sollten alle Funktionen die per Barcode verwendet werden können auch für die Los-Auswahlliste zur Verfügung stehen, vermutlich besser, als Buttons ohne Barcode zur Verfügung stehen, wobei eben die Gestaltung des Fertigungsbegleitscheines durch das Abschalten verschiedener Buttons ersetzt wird. Ev. ergibt sich daraus auch, dass man die Reihenfolge der Buttons anpassen können sollte. Für die verwendeten Barcodes [siehe]({{< relref "/docs/installation/11_barcode/#unterstützte-barcodefunktionen" >}}), Spalte ZE-Terminal

Hier kommt dazu, dass die einfache [Metallverarbeiter-Erfassung](#einfache-metallverarbeiter-erfassung) umgesetzt werden sollte.

weitere Feature-Wünsche
- Losauswahlliste inkl. Filterung nach Maschine bzw. Maschinengruppe
  - Maschinen(gruppen)filter jederzeit umschaltbar<br>
  Bedienung zweistufig mit Maschinengruppe und eingerückt die Maschinen dazu.
  - In der Losauswahlliste sollte man (fast) alle Knöpfe als Terminaleigenschaft ein- und ausschalten können, da je nach Standort das Terminal eben verschiedene Funktionalitäten hat.<br>So ist der Trick, dass auch die Materialvorbereitung faktisch eine Maschine ist. Auf dem Terminal (das in der Materialvorbereitung steht) ist die Materialentnahme auf das Los aktiviert und ev. die Zeichnungen, auf allen anderen Terminals nicht.
  - Man sollte in der Los-Auswahlliste durch scann der Losnummer ($L..) einen Filter auf die Arbeitsgänge dieses Loses haben, damit man sieht was noch offen ist.
  Zusätzlich kann man dann damit die Zeichnungen des Loses (eigentlich des Stücklistenartikels) anzeigen lassen.
  - Idealerweise kann man auch durch Tipp auf einen speziellen Button, die Losnummer manuell in der üblichen bzw. vollen Schreibweise eingeben.
- Wenn man im Ordner der Dokumentenablage arbeitet und der Ordner ist leer, so muss man trotzdem im Ordner bleiben (und nicht ganz rausfliegen). Ev. den Ordner (Zeichnung) nur in grau angezeigen und man kann nicht reinspringen, und oder es steht dahinter (leer). So dass man gar nicht weiter springt.
- immer wieder die Vorstellung, dass man billige Android Tabletts verwenden kann. Wenn man die Kamera verwendet, so kann man damit den Mitarbeiter Barcode scannen und der Rest geht über das Touch. Ev. doch so, dass man, wie am Handy, mit einer Taste das Tablett aktivieren muss und dieses automatisch nach einigen Minuten wieder in Standby geht.
- Wunsch nur 12" Terminal
  - Zeichnungen des Teils ansehen.
  - Eigentlich interessiert nur die Laufzeit der Maschine und die damit gefertigte Stückzahl.
  - D.h. es sollte
    - eine Zuordnung zur Maschine geben
    - eine Losauswahl was genau gefertigt wird / zu fertigen ist
    - einen Zeichnungs-Knopf -> wichtig ist in der Darstellung, dass man da mit den Fingern zoomen kann
    - einen Start-Stop Button nur für die Maschinenzeit (das Terminal weiß an welcher Maschine es ist)
    - ob die mit der Menschzeit dann gekoppelt ist ?
- die Geisterschicht. D.h. 
  - Maschine wird von Mensch gestartet.
  - Mensch geht heim
  - Maschine schaltet sich nach xx Std ab. Wie / mit welchen Tools bekommen wir das mit?<br> Eine Idee war einmal, einen kleinen Lichtsensor auf die gn/gb/rt Laufanzeige zu hängen. Der Sensor, eigentlich das Terminal, weiß welche Maschine das ist und wenn dann der Stop kommt und länger als xx Sekunden ansteht, wird die Maschine gestoppt.<br>
  Einen Start muss dann ja eh wieder ein Mensch machen.<br>
  Wichtig wäre, dass der Stop auch am Terminal sichtbar ist.
  D.h. wir wissen ja, das Terminal ist nur für Maschine x und könnten da, bei den Kommt/Geht Buttons auch anzeigen auf welches Los/AG die Maschine gerade läuft, oder sie steht eben.
  wäre z.B. eine zusätzliche Zeile (zwischen Uhr und Buttons)<br>
  und wenn sie steht, dann Rot, steht seit .... Uhrzeit und Dauer
- Man braucht am Terminal die Möglichkeit, dass der Mitarbeiter am Arbeitsgang buchen kann, das sind xx Schlecht Stück und damit wird ab diesem Arbeitsgang die Losgröße reduziert, ohne dass Material zurückgegeben wird.<br>
Zusätzlich muss er dann den Fehlergrund mit angeben.<br>
Trotzdem braucht man eine Statistik was alles als Schlecht gebucht wurde, also  Material mit Zeit vernichtet wurde. Das muss dann als täglicher Managementsummary an die QS-Leitung und an die GL etc.. Diese vernichteten Werte gehören natürlich auch dargestellt.
Beim Anwender wurden in 2022 1Mio € auf Schrott gebucht worden.
- Am Terminal sollte es auch eine Hilfe geben, die man z.B. über den Browser darstellen kann. Es darf aber sonst keine weitere Webseite aufgerufen werden können. Vermutlich kommt dann sofort der Wunsch, dass hier auch Firmeninformationen angezeigt werden. Ob dies ein Feature für den Bildschirmschoner ist, muss definiert werden.

### Einfache Metallverarbeiter Erfassung
Gerade für die Metallverarbeiter, also reine Lohnfertigung ist die Herausforderung, dass man zu Beginn des Tages nicht weiß, welche Aufträge noch dazu kommen. So beginnt man mit einem Arbeitsgang auf einer Maschine, bekommt dann 2Std später einen weiteren Fertigungsauftrag, also einen weiteren Arbeitsgang hinzu, dann einen dritten. Dann ist der erste AG fertig und man starten einen weiteren usw. usf..<br>
Am Ende des Tages sollte das System errechnen wieviel Maschinen- und Menschzeit auf welchen Arbeitsgang zu buchen ist. Dafür ist sicherlich auch eine Erweiterung des Kernels erforderlich. [Siehe]({{< relref "/fertigung/zeiterfassung/ze_terminal/einfache_mehrfacherfassung" >}})

### Papierlose-Fertigung 
Ein Wort zu diesem sehr modernen Gedanken.<br>
Es besteht der berechtigte Wunsch, dass die Mitarbeiter am Terminal sehen, was denn der nächste zu erledigende Arbeitsschritt / Auftrag ist.<br>
Parallel dazu besteht die Forderung aus der Qualitätssicherung, dass jedes Teil, ob Halbfertig-Produkt oder Rohteil, welches sich in der Fertigung befindet, so eindeutig gekennzeichnet ist, dass man jederzeit weiß was das ist und idealerweise auch in welchem Zustand.<br>
Aus meinen bisherigen Erfahrungen werden wir daher noch länger zumindest <u>einen </u> Fertigungsbegleitschein haben, der sich beim Teil befindet. Auf diesem Papier, welches idealerweise nur eine Din A4 Seite hat, wird zumindest der Barcode des Loses, gerne in QR-Code, aufgedruckt sein. Womit man natürlich auch den Wunsch produziert, durch scannen dieses QR-Codes, sehe ich am xxx-Device, was das ist und in welchem Status.<br>
Für die Umsetzung dafür [Siehe]({{< relref "/fertigung/losverwaltung/zeitwirtschaft_in_der_fertigung/papierlose_fertigung" >}})

Ein weiterer Punkt der hier immer kommt ist, wie plant man die Aufträge, was denn wirklich nun zu tun ist. Meine persönliche Meinung, die inzwischen doch von etlichen Anwendern bestätigt wird ist, es zählt einzig der Termin, des Arbeitsganges. Eventuell macht man innerhalb der Arbeitsgänge eines Tages noch eine weitere Reihung, sodass man quasi die Zeit / Stunde definiert wann der Mitarbeiter damit beginnen sollte. Alles andere, wie Priorisierung usw. läuft im Endeffekt wieder auf eine gute Terminsteuerung hinaus. Ein Thema das hier dazu gehört ist die Kapazitätsplanung bzw. die automatische Einplanung von Arbeitsgängen, siehe dort.

### Urlaubsantrag
Auf dem Terminal kann auch vom Mitarbeiter ein entsprechender Urlaubsantrag gestellt werden. Dafür muss in der Konfiguration<br>
![](Antraege_anzeigen.png) <br>
der Anträge Button anzeigen aktiviert sein. Wenn du keine Krankenstandsanträge am Terminal erfassen möchtest, so bitte den Krankmeldung zulassen auf Aus stellen.

Damit die EMail vom Terminal an den Abteilungsleiter, Geschäftsleitung und im CC an die Lohnverrechnung versandt werden kann, muss der Terminal-Benutzer im Personal im Reiter Daten unter Absenderdaten ebenfalls eine EMail-Adresse, welche in deinem System erlaubt ist, hinterlegt haben. Die EMail wird mit dieser Absenderadresse an die Personen gesandt, die Abteilungsleiter bzw. Geschäftsleitung sind und dazu wird bei diesen Personen ebenfalls diese interne EMail-Adresse (Personal, Daten, Absenderdaten, E-Mail) verwendet.

Weitere Details [siehe]({{< relref "/docs/stammdaten/personal/urlaubsantrag/" >}}) [bzw.]({{< relref "/fertigung/zeiterfassung/#wie-wird-ein-urlaubsantrag-gestellt" >}})

Wichtig: Damit diese Einstellung (der Absender EMail-Adresse) greift, muss das Terminalprogramm neu gestartet werden.

### Maschinen
Mit Tipp auf den Maschinen-Stopp-Button (muss konfiguriert werden) ![](Maschinen_Stopp_Button.png), erscheint die Liste der laufenden Maschinen.<br>
![](Maschinen_Stoppen.png)

Durch Tipp auf das rote Viereck, wird die gewählte Maschine gestoppt.

