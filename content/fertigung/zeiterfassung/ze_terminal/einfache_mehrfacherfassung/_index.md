---
title: "Zeiterfassung Terminal, Mehrfacherfassung"
linkTitle: "Mehrfacherfassung"
categories: ["Mehrfacherfassung"]
tags: ["Zeiterfassung","Terminal", "Mehrfacherfassung"]
weight: 300
description: >
  Mehrfacherfassung am Zeiterfassungsterminal, oder auch dynamische $PLUS Buchung
---
Diese Beschreibung ist eine Vorabbeschreibung einer weiteren **gewünschten** Funktion für das **Kieselstein ERP** Zeiterfassungs-Terminal.

Gerade die kleinen Lohnfertiger der Metallverarbeitungsbranche kämpfen mit der quasi dynamischen $PLUS Buchung, Lose gemeinsam buchen.
Hier ist zusätzlich zu bedenken, dass mit dem Terminal sehr sehr einfache Menschen arbeiten. D.h. das muss so einfach wie möglich sein.
D.h. das Terminal sollte relativ automatisch erkennen, ob man ein Los hinzufügen will oder das erste beendet und dann am zweiten weiterarbeiten möchte.
Die Grundidee ist, dass wir eigentlich an der Zeitbuchungslogik im Kernel nichts ändern, nur die Datenerfassung etwas erleichtern.

Beispiel eines möglichen Ablaufes:
- a.) MA beginnt am Morgen mit Los 1
- b.) um 10:00 stempelt er Los 2 an.
- c.) Nun gibt es eigentlich zwei Möglichkeiten
  - er beendet Los1 oder
  - er arbeitet ab 10:00 nun parallel auf beiden Losen.
- d.) um 11:00 beendet er Los1, d.h. das Los2 läuft nun weiter

Wenn das der Mitarbeiter mit heutigem SW-Stand am Terminal buchen möchte, muss er für
- a.) einfach das neue Los beginnen, für
- b.) muss er ein Ende Stempeln, dann ein $PLUS, dann beide Lose angeben (und ev. auch noch StückRück buchen, was man an der Stelle gar nicht will)
- c.) ein Ende Stempeln (und ev. Stückrück für alle beiden Lose buchen) und dann Los2 wieder neu Starten.

Mein Gedanke ist nun, dass man mit einem Parameter am Terminal anhaken kann, dass Arbeitsgänge weiterlaufen sollten. Das bewirkt, dass
- wenn ein weiteres Los angestempelt wird, kommt die Frage ob das Los1 (der Arbeitsgang) beendet werden sollte, oder ob der Arbeitsgang / das Los weiterlaufen sollte.
- Wird weiterlaufen gewählt, so machen wir / das Terminal die unter b.) beschriebenen Schritte automatisch
- Auch bei c.) sollte es so sein, dass erkannt wird, dass mehr wie ein Los aktiv läuft und daher die Frage erscheinen welches Los, genauer welchen AG der Mitarbeiter beenden will.

Noch ein Zusatzwunsch: Wenn Ende = a.) gewählt wird, so sollte eine Frage kommt: Ist der Arbeitsgang damit erledigt J/N. Wird Ja gewählt, so wird das Fertig auf den Arbeitsgang gebucht.

- ad Verteilungsberechnung bei der $PLUS Buchung. Wenn man das immer beim GEHT oder ENDE
macht, bis zurück zum vorherigen Kommt oder zum vorherigen Ende
Man sieht dann auch wenn ein Wechsel drinnen ist / ein Eintrag
Natürlich werden da auch die Maschinenzeiten mitgebucht, also verteilt

Ergänzend
- Los anstempeln = immer offene Zeitverteilung? damit immer hinzugestempelt werden kann
- bei Loserledigen prüfen auf offene Zeitverteilungen
- bei vergessenem Geht Aktion setzen bei Kommt, wenn offene Zeitverteilungen

- a.) es wird faktisch immer über $PLUS gearbeitet, merkt aber der Mitarbeiter nicht
D.h. er bucht Kommt und seinen ersten AG und der landet bereits automatisch in Zeitverteilung
- b.) Er stempelt ICH und es erscheint sofort eine Maske ähnlich Maschinen
Hier kann er mit Plus einen weiteren AG dazufügen, bzw. einfach mit Scan des kombinierten Codes
Oder er kann eine der angeführten AG's Stoppen
- c.) Mit Geht oder Ende wird die Zeitverteilung gestartet.
  - Die Gut-Schlecht kommen natürlich erst nach Ende bzw. Geht.
  - Will er zwischendurch auch GutStück erfassen, so ??
  - Maschinen sind "eh" fix die Zeit des jeweiligen AGs
    Personalzeiten müssen verteilt werden, ähnlich parallele Maschinenbedienung
    ZEITVERTEILUNG_TYP = anhand Sollzeit
- d.) Erweiterungen bei Losablieferung wenn offene Zeitverteilungen

Vorschlag:
- a.) es wird die Tabelle der offenen Zeitverteilung generell um das Ende erweitert
- b.) am Terminal gibt es einen Konfigurationsparameter Automatisches gemeinsam Buchen (Auto $PLUS)
- c.) Der Mitarbeiter wird nach dem ICH (Scann des Personal Barcodes) immer in die Maske des automatischen gemeinsam Buchens geführt.
- d.) Diese Maske (die ev. das jetzige $PLUS sein könnte) kann
  - d1.) durch scann des kombinierten Barcodes eine weitere Tätigkeit in die offene Zeitverteilung aufnehmen
  - d2.) hat rechts neben der Anzeige der angestempelten Arbeitsgänge je einen Button STOP, womit dieser Arbeitsgang gestoppt wird.
  - d3.) hat einen Abbrechen Button, womit man wieder in der normalen Erfassungsmaske landet und hat
  - d4.) zur leichteren Bedienung Kommt, Pause, Geht Buttons.
- e.) Wird der letzte Arbeitsgang gestoppt, ist also nach dem Stop kein AG mehr offen, so wird, vom Terminal, automatisch auch ein Ende an den Server gesandt. Damit startet der Server dann auch die Zeitverteilungsberechnung, womit man dann die Zeiten von der offenen Zeitverteilung in die Zeitbuchungen überträgt. Es sind dann auch alle angestempelten Maschinen gestoppt. (Das ist beim jetzigen $PLUS auch schon so)
- f.) Es sollte die Anzeige der letzten Buchungen abgeschaltet sein, da das sowieso sofort in das offene Zeitverteilungsfenster springt.
- g.) Der Report pers_anwesenheitsliste_offene_zeitverteilung.jrxml muss um die Info des gestoppt erweitert werden.

Dafür sind im Kernel folgende Erweiterungen erforderlich:
- Das Ende bei der offenen Zeitverteilung hinzu
- die Zeitverteilungsberechnungen müssen dieses Ende berücksichtigen.

Ev. Benennung der Funktion, des Parameters im Terminal: Gemeinsam-Buchungs-Automatik 

WICHTIG: Es muss das, ich mache das auf einer anderen Maschine durch $M... auch gehen.

Inhalt der Liste am Terminal: Losnummer, AG, Tätigkeit, Maschine, Startzeit, Stop-Button
gleicher AG kann nicht doppelt gestempelt werden -> Fehlermeldung				
gestoppter AG kann später wieder gestempelt werden				

ACHTUNG: Es geht auch um echten drei-Schicht Betrieb
