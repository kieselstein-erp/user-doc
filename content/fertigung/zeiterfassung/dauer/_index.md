---
title: "Zeiterfassung nur als Dauer"
linkTitle: "Dauererfassung"
categories: ["Zeiterfassung"]
tags: ["Dauererfassung"]
weight: 500
description: >
  Erfassung nur von Anwesenheitsdauer, ohne Beginn / Ende
---
Zeiterfassung über Zeiträume (Dauer)
====================================

Als Alternative der auf den "Zeitpunkt orientierten" [Zeiterfassung]( {{<relref "/fertigung/zeiterfassung" >}} ) steht für projektorientiertes Arbeiten die sogenannte "Dauererfassung" zur Verfügung.
Bitte beachten Sie, dass eine gemischte Betrachtung, sowie ein Wechsel zwischendurch nicht möglich ist.

Wenn der Parameter VON_BIS_ERFASSUNG auf 1 gestellt ist, so können die Zeiten in **Kieselstein ERP** als Dauer erfasst werden.
Mit dem Parameter VON_BIS_ERFASSUNG_KOMMT_GEHT_BUCHEN steuern Sie, ob Sie auch Anwesenheitszeiten oder nur Projektzeiten erfassen wollen. 
Wenn Sie den Parameter auf 0 stellen, so werden keine Anwesenheitszeiten erfasst, eine Buchung muss nicht zwischen KOMMT und GEHT eingebettet sein.

![](zeit_dauer.JPG)

Wie werden Zeiten bei der Dauererfassung eingetragen?

Wenn Sie die Dauererfassung nutzen, so klicken Sie auf das Icon Neu ![](neu.jpg).  Nun können Sie einen Zeitraum eingeben ![](zeiten_dauer.JPG).

Als Alternative dazu geben Sie im ersten Feld der Zeit einen Zeitpunkt ein und ergänzen im Feld Dauer ![](dauer_dauer.JPG) die Anzahl der Stunden.

Klicken Sie auf Auftrag bzw. Los um die Zeitbuchung einem Beleg zu zuordnen.

Geben Sie in dieser Form den gesamten Tag ein, die einzige Bedingung ist, dass das 2.Von nicht vor dem 1.Bis sein kann.

**Kieselstein ERP** schlägt somit immer das letzte Bis als Von Zeitpunkt vor.