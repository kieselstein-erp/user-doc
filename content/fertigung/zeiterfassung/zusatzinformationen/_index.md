---
title: "Zeiterfassung Zusatzinformationen"
linkTitle: "Zusatzinformationen"
categories: ["Zeiterfassung"]
tags: ["Zusatzinformationen"]
weight: 730
description: >
  Zusatzinformationen zur Zeiterfassung
---
Gerade im Bereich der Zeiterfassung, der Mitarbeiterzeitabrechnung tauchen immer wieder Fragen auf, wie diese Sonderfälle zu behandeln sind. Wir haben hier eine lose Sammlung von bisher aufgetretenen Fragen zusammengestellt, welche wir regelmäßig ergänzen. Bitte beachten Sie, dass dies keine verbindlichen Auskünfte sind. Für verbindliche Auskünfte wenden Sie sich bitte an Ihren Steuerberater, Lohnbüro usw.

#### Mitarbeiter geht exakt während der Mittagspause zum Arzt. Ist dies Arzt oder Unterbrechung.
Was der Mitarbeiter in seiner Freizeit macht ist ihm überlassen. Er kann gerne zum Arzt gehen, er macht dies aber in seiner Freizeit. Das gilt analog auch wenn der Mitarbeiter während seines Urlaubes zum Arzt geht.

#### Mitarbeiter wird während des Urlaubes krank.
Ab einer Krankheitsdauer von drei Tagen und nach überbrachter Krankbestätigung unterbricht dies den Urlaub.
