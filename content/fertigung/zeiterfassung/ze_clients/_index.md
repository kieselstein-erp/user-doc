---
title: "Zeiterfassung"
linkTitle: "Clients"
categories: ["Zeiterfassung"]
tags: ["Clients"]
weight: 220
description: >
  Zeiterfassungs Clients
---
Zeiterfassungs-Clients
======================

In **Kieselstein ERP** stehen derzeit folgende Erfassungsarten für die Zeiterfassung zur Verfügung. BDE-Station, Zeiterfassungsterminals und Zeiterfassungsstifte werden üblicherweise mittels Barcodescanner bedient. Die dafür erforderlichen Barcodes können direkt aus **Kieselstein ERP** erzeugt werden. Die Barcodes können gleichwertig auf allen Erfassungsgeräten verwendet werden. Zusätzlich stehen bei den Terminals auch RFID Leseeinheiten intern bzw. extern zur Verfügung.

Manuelle Zeiterfassung

[Buchung im Zeiterfassungs Modul in **Kieselstein ERP**]( {{<relref "/fertigung/zeiterfassung">}} )

Start in den Kopfdaten der Module [Angebot]( {{<relref "/verkauf/angebot">}} ), [Auftrag]( {{<relref "/verkauf/auftrag">}} ), [Fertigung}( {{<relref "/fertigung">}} ), [Projekt]( {{<relref "/verkauf/projekt">}} ) in **Kieselstein ERP**]

Zum schnellen Starten der Zeiterfassung finden Sie in den Modulen Angebot, Fertigung und Projekt Start ![](Zeitbuchung_Start.png)  und Stop ![](Zeitbuchung_Stop.png)  Icons. Mit Klick auf das Start-Icon wird die Buchung erfasst. Auch hier gilt, dass mit Klick auf das nächste Start-Icon zum Beispiel in einem anderen Projekt die erste Buchung beendet wird.

Zeiterfassung an Terminals

[Zeiterfassungsterminal]( {{<relref "/docs/installation/04_ze_terminal"  >}} )
![](Terminal.png)  

Diese Entwicklung wurde unter anderem für die Zeiterfassung in Metallverarbeitungs- und  Maschinenbauunternehmen geschaffen. Die Terminals sind mit einem Touchbildschirm ausgerüstet und werden üblicherweise in Kombination mit Barcodescannern für die Auftragserfassung bedient. Neben den beschriebenen Funktionalitäten sind diese Geräte auch [Offline](http://fs:8080/heliumhelp/heliumhelp/Zeiterfassung/Zeiterfassungsterminal_Nettop.htm#Offlinemodus) fähig.
Das Gerät hat eine Bildschirmdiagonale von ca. 40cm (15").
Das Nettop Terminal kommt üblicherweise mit Barcodescanner. Zusätzlich kann als externes Gerät auch ein RFID Kartenleser angeschlossen werden.
Zusätzlich wird dieses Terminal für die Übertragung der Daten der mobilen Zeiterfassungsstifte benötigt.

Das Zeiterfassungsterminal ist so programmiert, dass es auch auf Android Systemen eingesetzt werden kann. Weitere [Details siehe bitte auch]( {{<relref "/docs/installation/04_ze_terminal"  >}} )

[BDE-Station]( {{<relref "/fertigung/zeiterfassung/bde_station">}} )

Über eine Browserapplikation steht die Funktionalität einer Betriebsdatenerfassung zur Verfügung. Sie können damit sowohl die Anwesenheitszeiten als auch die Zeiten auf den Projekten erfassen. Bitte beachten Sie, dass für diese Erfassung der Netzwerkzugriff essentiell ist. Die Bedienung erfolgt mittels Barcodescanner mit Tastaturemulation. Die Funktionalität dieses Browser Terminals ist um Lieferumfang der Zeiterfassung mit enthalten.

Quick-Zeiterfassung

Einfache Browsererfassung direkt am Arbeitsplatz für jeden Benutzer der sich per Browser zum **Kieselstein ERP** Server verbinden kann. Für die Erfassung ist der Zugriff auf den **Kieselstein ERP** Server erforderlich.
Diese Funktionalität ist um Lieferumfang der Zeiterfassung mit enthalten.

Automatische HMTL Buchung bei Logon / Logoff

Per Script werden beim Logon des Benutzerrechners entsprechende Buchungen als Zeiterfassungsbuchung an den **Kieselstein ERP** Server übermittel. Analog dazu stehen entsprechende Buchungen bei der Abmeldung (z.B. Geht) zur Verfügung.

[Zeiterfassungsterminal](Zeiterfassungsterminal.htm) mit [Fingerprint](Fingerprint.htm) oder Barcodescanner oder RFID Leser

Mobile Zeiterfassung / Zeiterfassung über den Browser / über eine App

[Quickzeiterfassung]( {{<relref "/fertigung/zeiterfassung/quickzeiterfassung">}} )

Hierbei können Mitarbeiter die Zeiten zeitnah im Browser eingeben. Die Auswahl des Kunden und anschließend der offenen Belege ermöglicht eine schnelle Erfassung.
