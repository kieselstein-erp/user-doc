---
title: "Überstundenabrechnung"
linkTitle: "Überstundenabrechnung"
categories: ["Überstundenabrechnung"]
tags: ["Überstundenabrechnung"]
weight: 560
description: >
  Überstundenabrechnung
---
Das Thema der Überstunden ist, gerade in Österreich, ein sehr weites Thema. Es gibt verschiedene Ausprägungen in den unterschiedlichen Kollektivverträgen oft durch Betriebsvereinbarungen ergänzt. Kollektivverträge werden in Deutschland Tarifverträge genannt.

Die in **Kieselstein ERP** implementierte Zeiterfassung ist als Zeiterfassung konzipiert. Die Besonderheiten, welche sich durch die weiterführende Lohnabrechnung ergeben, sind soweit unbedingt erforderlich unterstützt.

Bitte beachten Sie, dass die Zeiterfassung die Lohnabrechnung auf keinen Fall ersetzt.

{{% alert title="Warning" color="warning" %}}
Bitte beachte: Der Kollektivvertrag ist **<u>NICHT</u>** Mandantenabhängig. D.h. ein definierter Kollektivvertrag kann für alle Mandanten verwendet werden. Solltest du je Mandant unterschiedliche Kollektivverträge benötigen so muss dies zentral als jeweils **eigener** Kollektivvertrag angelegt werden.
{{% /alert %}}



Dieses Kapitel gibt unter anderem Antworten und Informationen zu folgenden Fragen: 

#### Wie können die Überstunden definiert werden ?
#### Kann für den Überstunden-Saldo auch ein Zeitfaktor hinterlegt werden ?
#### Wie werden die ausbezahlten Überstunden ausgeglichen ?

Definition der Abrechnungsart

Abweichend zur üblichen Gleitzeitstundenabrechnung steht auch die Abrechnung nach Betriebsvereinbarung A zur Verfügung.
Definieren Sie in den Details des Kollektivvertrages die Abrechnungsart entsprechend.
![](Abrechnungsart.gif)
Für Details zur Abrechnungsart Betriebsvereinbarung A siehe bitte [unten](#Betriebsvereinbarung).

Definition der Überstunden

Die Überstundenregelungen werden üblicherweise zwischen den Tarifpartnern in sogenannten Kollektivverträgen vereinbart.

Daher gibt es in **Kieselstein ERP** die Möglichkeit je Mitarbeiter den zuständigen Kollektivvertrag zu definieren.

Deshalb wurde auch die Definition der Überstunden mit dem Kollektivvertrag gekoppelt.

Den Kollektivvertrag finden Sie im Personal, unterer Modulreiter ![](Kollektiv1.gif). 

![](Kollektiv2.jpg)

Unter Detail definieren Sie die Sollarbeitszeit pro Woche und die Normalstunden.

Sollarbeitszeit sind die Sollstunden, die der Mitarbeiter pro Woche arbeiten sollte.

Normalstunden sind diejenigen Stunden welche noch als Normalstunden gerechnet werden. Erst wenn die Wochenanwesenheit über den Normalstunden ist, so werden Überstunden gerechnet.

Die Zeit zwischen Soll-Arbeitszeit des Zeitmodells und Normalstunden werden Mehrstunden genannt. (Zum Beispiel: Bei einer Teilzeitkraft mit 30Std und einer grundsätzlichen Wochenarbeitszeit von 40 Std. ergeben sich bis zu 10Std. Mehrstunden.)

Überstunden 50%

50%ig Überstunden sind grundsätzlich Zeiten die pro Woche über die Normalstunden hinausgehen. Hat eine Woche weniger als sieben Tage, so werden die Sollstunden (Arbeitszeit) und die Normalstunden anhand des Zeitmodells entsprechend umgerechnet.

Zusätzlich können auch feste Zeitbereiche definiert werden, welche fix 50%ig Überstunden sind. Wir sprechen hier von 50%igen-Tagesüberstunden. Z.B. für Samstagsarbeiten oder auch am Tagesrand, z.B. von 6:00-7:00 und von 19:00-20:00

Überstunden 100%

Ab einer gewissen Tageszeit werden 100%ig Überstunden bezahlt. Z.B. zwischen 20:00 und 6:00.

Definieren Sie diese Zeiten unter 100%ige Überstunden.

**Blockzeit / steuerlich begünstigte / nicht begünstigte Überstunden**

Zusätzlich zu der zeitlichen Unterscheidung zwischen 50%igen und 100%igen Überstunden gibt es auch noch die Trennung nach steuerbegünstigten und nicht steuerbegünstigten Überstunden. Dies ist so definiert, dass wenn in der sogenannten Blockzeit ein zusammenhängender Zeitraum von mindestens 2 Überstunden vorliegt, werden diese Stunden als steuerbegünstigte Überstunden behandelt. Ist die zusammenhängende Zeit kürzer, so werden diese als nicht steuerbegünstigte Stunden behandelt.
Werden Stunden an Sonn- oder Feiertagen geleistet, so werden diese **immer** als steuerbegünstigte Stunden behandelt.
Üblicherweise dauert die Blockzeit von 19:00-7:00\. D.h. Blockzeit bis wird auf 07:00 und Blockzeit ab auf 19:00 eingestellt.

Wie werden die Arbeitszeit, die Mehrstunden, die 50%igen und die 100%igen Überstunden berechnet?

|  | Beschreibung | Ergebnis |
| --- |  --- |  --- |
| a. | Vom zu betrachtenden Tag (der aktuelle Tag) werden die 100%igen Überstunden ermittelt.Für die weitere Betrachtung wird der Tag ohne den 100%igen Überstunden verwendet. | 100%ige Überstundenfür den aktuellen Tag |
| b. | Von diesem Tag werden die 50%igen-Tagesüberstunden ermittelt. | 50%ige Tages-Überstundenfür den aktuellen Tag |
| c. | Es wird die Wochensumme der nun verbleibenden Tagesstunden gebildet.Überschreitet diese Summe die Normalstunden so sind dies 50%ige Überstunden der Woche. | 50%ige Wochen-Überstundenfür die aktuelle Woche |
| d. | Die Differenz der nun verbliebenen Stunden ergibt die Mehrstunden pro Woche. | Mehrstunden pro Woche. |

**Hinweis:**

Die Überstundenabrechnung kann nur mit definierten Normalstunden erfolgen.

**Hinweis zur Definition der Normalstunden:**

Da es bei Schichtarbeiten sehr oft wechselnde Zeitmodelle / Sollzeiten gibt, aber keine Mehrstunden anfallen / ausbezahlt werden, ist es auch erlaubt, die Normalstunden unter die Sollstunden der Zeitmodelle zu setzen.

Das bedeutet: Die Definition der Normalstunden bewirkt folgende Berechnungen:

| Stunden | Ergebnis |
| --- |  --- |
| keine oder 0 | keine Überstundenberechnung |
| größer 0 aber kleiner als die Sollarbeitszeit | keine Mehrstundenberechnung aber Ist-Stunden über der Sollarbeitszeit werden als 50%ige Wochenüberstunden behandelt |
| größer der Sollarbeitszeit | Mehrstundenberechnung für die Woche. Erst nachdem die Ist-Stunden die Normalstunden überschreiten werden 50%if Überstunden ausgewiesen |

**Definition der 100%igen (Tages-)Überstunden:**

![](Kollektiv3.jpg)

Legen Sie hier die Zeiträume fest an welchen 100%ige Überstunden gerechnet werden sollten.

Bis bedeutet: von 00:00 bis z.B. 6:00 und ab z.B. 20:00 bis 24:00.

Wird Rest des Tages angehakt, so wird für die 100%igen Überstunden bis zum Ende des Tages (24:00) gerechnet. Ein Zeitraum davor kann damit nicht eingegeben werden. Sie brauchen diese Definition z.B. für Sonntagsarbeit, Feiertagsarbeit oder ähnliches.

**Pausen ignorieren**

![](Ueberstunden_Pausen_ignorieren.gif)

Mit Pausen ignorieren definieren Sie, ob bei der Berechnung der Dauer der Überstunden die eventuell automatisch gebuchten Pausen berücksichtigt werden sollen, oder ob diese ignoriert werden sollen. Ist "Pausen ignorieren" angehakt, so wird die Dauer der UNTERbrechung für die Ermittlung der Dauer der Überstunde ignoriert. D.h. es werden für die Pause auch die Überstunden-Zuschläge bezahlt bzw. im Lohndatenexport mitgerechnet.

**Definition der 50%igen Tages-Überstunden:**

Die Definition erfolgt in gleicher Weise wie die Definition der 100%igen. Achten Sie darauf, dass keine Überschneidungen zwischen 50 und 100%igen Überstunden definiert sind.
Wobei in der Berechnung zuerst die 100% gelten und danach die 50% verwendet werden.

**Schachtelung / Reihung der Überstunden**

Für die Definition von Tageweisen Überstunden beachten Sie bitte, dass die 100%igen Überstunden eine höhere Priorität als die 50%igen haben. Um nun die oft üblichen gemischten Tage mit 100%igen und 50%igen Überstunden zu definieren genügt es für den jeweiligen Tag diese wie unten dargestellt einzupflegen. 

![](Ueberstunden.png)

**Darstellung der Mehr und Überstunden in der Monatsabrechnung**

![](Ueberstundendruck.png)

Wie wirken die hinterlegten Faktoren in den jeweiligen Überstunden-Feldern?

In den jeweiligen Feldern für den Faktor der Überstunden werden die entsprechenden Zahlen eingegeben.
Wenn Sie hier zum Beispiel 0,5 eingeben, so gilt: 1 Stunde wird als Gleitzeit gut geschrieben und 0,5h werden als Überstunde erfasst. Insgesamt wird eine Stunde gearbeitet und 1,5 Stunden gutgeschrieben.
Bei 1 gilt 1 Stunde Gleitzeit und eine Stunde Überstunde - dies wird auch als 1:2 bezeichnet. Es wird eine Stunde gearbeitet und 2 Stunden gutgeschrieben.

### Wann werden die Überstunden (Faktoren) wirksam?
Die Problematik der auszuzahlenden Stunden und damit auch der Überstunden liegt vor allem darin, dass der Arbeitgeber gezwungen ist, die gesetzlichen Regelungen verständlich an alle Mitarbeiter zu kommunizieren. Gerade bei gering bezahlten Mitarbeitern, haben diese oft keinerlei Wissen über die gesetzlich ausgehandelten Kompromisse.

Wann entstehen nun Überstunden ?<br>
Überstunden entstehen durch ein Erbringen von Arbeitsleistung, welche über das vereinbarte Maß hinaus gehen. Das vereinbarte Maß kann eine Wochenarbeitszeit sein (40Std), oder auch ein Zeitraum (Nachtstunden). Wichtig ist, der Zeitpunkt der Erbringung definiert den Anteil der Überstunden. Werden nun Überstunden ausbezahlt, so wird nur, die bereits erbrachte Leistung, in Geld umgerechnet.<br>
Da bei den MitarbeiterInnen oft die Meinung vorherrscht, bezahle mir bitte 10 Überstunden aus, also entstehen jetzt erst die 50%-igen Überstunden. Dies ist schlichtweg falsch. Es müssen, die verschiedenen Überstundentöpfe entsprechend bebucht werden.

**Erfassung der ausbezahlten Überstunden**

Werden Überstunden ausbezahlt, so müssen, damit der angezeigte Gleitzeitsaldo auch richtig ist, diese ausbezahlten Stunden bei der jeweiligen Person im Personalstamm zum Auszahlungszeitpunkt erfasst / eingegeben werden.
Verwenden Sie dazu das Modul Personalverwaltung, wählen Sie den Mitarbeiter und gehen Sie mit dem oberen Modulreiter auf ![](Kollektiv4.gif).
Geben Sie hier die ausbezahlten Stunden ein. **Hinweis:** Mehrstunden bis Überstunden Steuerpflichtig 200% reduzieren den Zeitsaldo des Mitarbeiters.
Gutstunden werden dem Zeitsaldo, abhängig vom Parameter GUTSTUNDEN_ZU_UESTD50_ADDIEREN als 50%ige Steuerpflichtige Überstunden hinzugerechnet. D.h. steht der Parameter auf 1, so werden diese den steuerpflichtigen 50%igen Überstunden hinzugerechnet. Steht der Parameter auf 0, so werden diese dem Gleitzeitsaldo hinzugezählt.
Denken Sie daran eine entsprechende Begründung unter Kommentar einzugeben, damit Sie auch später nachvollziehen können, warum diese Eingabe gemacht wurde. **Hinweis:** Die Qualifikationsprämie dient Ihrer Information / Dokumentation und wird derzeit nicht ausgedruckt.
 **Anmerkung:** Die Bezeichnung 50% bzw. 100%ige Überstunden hat sich in Österreich eingebürgert. Bitte betrachten Sie dies einfach als zwei verschiedene Berechnungsformeln / Errechnungsmethoden. Gerne übersetzen wir in Ihrer Installation die Begriffe auf besser zu Ihrem Anwendungsfall passende Begriffe. **Anmerkung:** Die je nach Kollektivvertrag üblichen Multiplikatoren für die geleisteten Überstunden sind je nach Kollektivvertrag sehr sehr unterschiedlich zu behandeln. Wir bitten Sie daher diese Abrechnung in Ihrer Lohnverrechnung durchzuführen. Von **Kieselstein ERP** erhalten Sie die geleisteten Stunden, sowie die verschiedensten Abwesenheitszeiten. Die Lohnsteuerrechtlich richtige Abrechnung auch der Überstunden ist Aufgabe Ihrer Lohnverrechnung.

#### Können verschiedene Kollektivverträge bzw. Tarifverträge hinterlegt werden?
Es können unterschiedliche Kollektivverträge, in Deutschland Tarifvertrag genannt, hinterlegt werden, für einen Mitarbeiter nur jeweils ein Kollektivvertrag. Damit können unterschiedliche Abrechnungsmodelle in Ihrem Unternehmen für verschiedene Mitarbeitergruppen realisiert werden. So kann z.B. in einem Golfclub für die Greenkeeper ein Kollektivvertrag mit Überstundenabrechnung hinterlegt werden, für die Administration wird, da kein Kollektivvertrag ausverhandelt ist, eine andere Vereinbarung, also ein anderer Kollektivvertrag hinterlegt. Damit rechnet **Kieselstein ERP** jeden Mitarbeiter nach seinem Kollektivvertrag ab.
Bitte beachten Sie, dass der beim Mitarbeiter hinterlegte Kollektivvertrag immer global, also für alle Zeitbuchungen gilt.

#### Sehe ich den Gleitzeit- und Überstundensaldo auch am Schirm?
Ja. Dieser wird unter Gleitzeitsaldo im Personal beim jeweiligen Mitarbeiter angezeigt.

Bitte beachten Sie die ab Version 5.10.048.3749 geänderte Berechnung.

![](Ueberstunden_Gleitzeitsaldo.gif)

Es werden Mehrstunden und Überstunden exakt errechnet. Der sich aus der Monatsabrechnung ergebende Gleitzeitsaldo wird unter Saldo extra ausgewiesen.

#### Überstunden verteilen?
Ist im Kollektivvertrag das Überstunden verteilen angehakt, so bewirkt, dass die zur Verfügung stehenden Überstunden eines Mitarbeiters, also das was an Stundenguthaben besteht, beim Abbau von Überstunden z.B. durch Zeitausgleich, in folgender Reihenfolge verbraucht werden. Wenn der Haken bei Überstunden verteilen gesetzt ist, so erfolgt die Betrachtung wochenweise (Differenz der Ist-Wochenstunden zu Soll-Wochenstunden). Wenn ein Mitarbeiter somit an einem Tag laut Definition Überstunden gemacht hat, im Wochengesamt die Sollzeit jedoch unterschreitet, so werden die gemachten Überstunden verteilt.

Der Verbrauch der Überstunden erfolgt laut österreichischem Arbeitszeitgesetz in folgender Reihenfolge:

1.) 50%ige Pflichtig
2.) 100%ige Pflichtig
3.) 50%ige Begünstigt
4.) 100%ige Begünstigt
5.) Mehrstunden

Gedacht und gerechnet wird nun in genau umgekehrter Richtung.

D.h. man geht von der jeweiligen Sollarbeitszeit aus und füllt dies von 5 nach 1 hin auf. Was dann noch übrig bleibt sind dann die Stunden des jeweiligen Typs. 

<a name="Überstunden erst ab erbrachten Sollstunden"></a>

#### Überstunden erst ab erbrachten Sollstunden?
Wenn der Haken gesetzt ist, so erfolgt eine tageweise Betrachtung. Hat ein Mitarbeiter an diesem Tag die Sollstunden noch nicht erfüllt, befindet sich aber zu einer Zeit im Unternehmen zu der Überstunden definiert sind, so werden keine Überstunden gebucht. Wenn der Haken nicht gesetzt ist, so werden je nach Definition die Überstunden gebucht, auch wenn die Sollstunden nicht erfüllt sind.
Gerade bei Arbeiten rund um Mitternacht hat diese Definition entsprechende Wirkung. D.h. ist der Haken gesetzt und ein Mitarbeiter arbeitet (ausnahmsweise einmal) über Mitternacht hinaus, so ist die Anrechnung der Nachtstunden davon abhängig, ob er am gedanklich darauffolgenden Tag mehr als seine Sollstunden erbringt oder ob er früher nach Hause gegangen ist.
Ist der Haken gesetzt und der Mitarbeiter hat an dem Tag an dem die Stunden nach Mitternacht angefallen sind, weniger als seine Sollstunden für diesen Tag erbracht, so wird auch die Mitternachtszeit derzeit NICHT als Überstunden gewertet.

#### Was ist bei Sonn- und Feiertagsüberstunden ab (Stunden) einzutragen?
In dem Feld Son-und Feiertagsüberstunden ab (Stunden) erfassen Sie Sonderzuschläge bei Arbeiten an Feiertagen (zb. im Winter Schneeräumung bei extremen Schneefall am Heiligen Abend)

Wird hier 8 eingetragen, so werden die ersten 8Std wie üblich behandelt und nur das was darüber ist, fällt in 200%ige Überstunden. Wenn es keine 200% Überstunden gibt, so tragen Sie in dem Feld bitte 24 ein.

#### Wochengesamtsicht
Wochengesamtsicht bedeutet, dass für die Berechnung ob bei Teilwochen die Normalstunden erreicht bzw. überschritten werden, nicht der Vergleich der Sollstunden der Teilwoche zu den Sollstunden einer ganzen Woche herangezogen werden, sondern dass immer nur volle Wochen berücksichtigt und errechnet werden. Zusätzlich werden für die Teilwochen, die sich aufgrund des Eintrittes ergeben, die Normalstunden nur anhand der Anzahl der Kalendertage der Woche herangezogen.
Immer nur volle Wochen berücksichtigt bedeutet, dass wenn der Monat z.B. am Samstag endet, so werden die Wochenweisen Überstundenberechnungen der 50%igen Überstunden erst im kommenden Monat berücksichtigt, da nur da die Woche vollständig ist.
Daraus ergibt sich auch eine etwas längere Rechenzeit für die Berechnung aller relevanten Daten.

<a name="Überstunden, Mehrstunden"></a>

#### Überstunden, Mehrstunden, steuerfrei und steuerpflichtig, wie wird das berechnet?
Die Verteilung der geleisteten Stunden auf die verschiedenen Stundenarten erfolgt nach folgender Regel (Da die Daten, je nach Land unterschiedlich sind, hier am Beispiel Österreich angeführt):
Weiters ist grundsätzlich zu beachten, dass es sich hier um eine Vermischung von Tages-, Wochen- und Monatsbetrachtungen handelt.

| Stundenart | Steuersatz | Betrachtung | Bedeutung |
| --- |  --- |  --- |  --- |
| Normalstunden |  |  | Stunden die während der Normalarbeitszeit geleistet werden bzw. innerhalb des Zeitraums / der Dauer der Sollarbeitszeit liegen. |
| Mehrstunden |  | Wochenbetrachtung | Stunden die über die Normalarbeitszeit hinausgehen aber noch innerhalb der 40Stunden Wochenarbeitszeitregel liegen und keine 100%igen Überstunden sind. |
| Überstunden 50% | steuerfrei | Tages- undMonatsbetrachtung | Stunden die über die Mehrstunden hinausgehen und im Zeitraum zwischen 6:00-20:00 geleistet werden.Im Monat sind 5Stunden steuerfrei |
|  | steuerpflichtig | Tages- undMonatsbetrachtung | Siehe oben, ab dem Beginn der 6.Stunde ist diese steuerpflichtig. |
| Überstunden 100% | steuerfrei | Tagesbetrachtung | Stunden im Zeitraum zwischen 20:00-6:00 oder an Sonn- und Feiertagen.Ab drei zusammenhängenden Überstunden sind diese steuerfrei |
|  | steuerpflichtig | Tagesbetrachtung | Siehe oben, jedoch für Überstunden(teile) bis zu drei Stunden. |
| Überstundenpauschale |  | Monatsbetrachtung | Eine Vereinbarung zwischen Arbeitnehmer und Arbeitgeber im Zeitraum zusätzliche Stunden zu erbringen. Diese erhöhen die monatliche Verpflichtung der Soll-Stundenleistung des Arbeitnehmers. |

Da die Monatsabrechnungen meistens mitten in der Woche sind und um, vor allem bei Teilzeitkräften, eine möglichst gerechte Abrechnungsart der Wochenbetrachtungen für die Teilwoche zu erreichen, wird bei Teilzeitmodellen ein Referenzmodell angegeben, auf dessen Basis die Ermittlung der Mehrstunden für die Teilwoche erfolgt. Denken Sie in diesem Zusammenhang an Monats-Erste/Letzte die auf einen Donnerstag fallen.

![](ueberstunden.jpg)

#### In der Überstundenabrechnung werden die Stunden nach Mitternacht nicht als Überstunden ausgewiesen ?
Hintergrund ist hier vermutlich, dass beim Kollektiv / Tarifvertrag für den Mitarbeiter Überstunden erst ab erbrachten Sollstunden angehakt ist. [Siehe dazu auch](Ueberstundenabrechnung.htm#Überstunden erst ab erbrachten Sollstunden).

<a name="Überstunden ausweisen"></a>

#### Im Kollektivvertrag / Tarifvertrag wurde Überstunden definiert, es werden jedoch keine ausgewiesen
Die Überstundenberechnung des Kollektivvertrages, in Deutschland Tarifvertrag genannt, greift erst, wenn für den Vertrag Normalstunden definiert sind.

Normalstunden sind diejenigen Stunden, welche der Mitarbeiter in der Woche arbeiten muss um bei Zeiten die darüber gehen überhaupt einen Überstundenanspruch zu haben (zum Beispiel 40h).

Für Teilzeitkräfte gilt, die Zeiten zwischen den vereinbarten Sollstunden pro Woche bis zu den Normalstunden sind sogenannte Mehrstunden, welche in Österreich in manchen Kollektivverträgen extra zu vergüten sind.

#### Wie können meine Mitarbeiter Überstunden machen, wenn es fixe früheste erlaubte Kommt-Zeiten und späteste erlaubte Geht-Zeiten gibt?
Legen Sie ein zweites Zeitmodell an, mit denselben Zeiten, aber keiner späteste erlaubte Geht-Zeit. Über das Benutzerrecht PERS_ZEITERFASSUNG_ZEITMODELL_TAGEWEISE_AENDERN kann ein Zeitmodell für die Tage, an denen Überstunden gemacht werden, geändert werden. Das Zeitmodell wird von einem Befugten geändert. Pro Tag kann nur ein Zeitmodell verwendet werden.

#### Überstundenabrechnung, Blockzeit, Steuerfrei/Steuerpflichtig usw.
Laut österreichisches Arbeitsrecht sind Zeiten die in der sogenannten Blockzeit anfallen steuerbegünstigt. Unter Nachtzeit im steuerlichen Sinn versteht man den Zeitraum zwischen 19:00 Uhr und 7:00 Uhr. Begünstigt sind nur Zuschläge für Arbeitsstunden, die während einer zusammenhängenden Nachtarbeitszeit von mindestens drei Stunden (Blockzeit) geleistet werden.
**WICHTIG:**
Die Blockzeit ist die Voraussetzung für die steuerfreien Stunden.
Parallel dazu können dies 50%ig oder 100%ig Überstunden sein.

Definition der Blockzeit:

Die Blockzeit wird ab 00:00 bis z.B. 07:00 und ab 19:00 bis 24:00 definiert.

Vom Anwender kann der Endezeitpunkt für die Blockzeit ab Mitternacht = **Blockzeit bis** und der Beginnzeitpunkt der Blockzeit bis Mitternacht = **Blockzeit ab** definiert werden.

Von 00:00    ![](Blockzeit_bis_ab.gif)   bis 24:00

Abschalten der Blockzeit:

Sollte die Blockzeit abgeschaltet werden, so muss für die eindeutige Zuordnung der Überstunden die Blockzeit abgeschaltet werden.
Tragen Sie dazu im Detail des Kollektivvertrages für die Blockzeit bis und für die Blockzeit von jeweils 00:00 ein.
Wichtig: Die Abschaltung der Blockzeit bedeutet nicht, dass die begünstigten und nicht begünstigten Überstunden nur in einem Feld betrachtet werden.
Vielmehr bedeutet:

-   Wenn die Blockzeit **nicht** aktiv (abgeschaltet) ist, so werden
    Alle 100%igen Überstunden an den Werktagen als steuerpflichtig betrachtet
    Alle 100%ige Überstunden an Sonn- und Feiertagen werden als steuerfrei betrachtet

-   Wenn die Blockzeit aktiv ist, so werden
    Alle 100%igen Überstunden an den Werktagen als steuerpflichtig betrachtet, außer diejenigen Stunden, die in die Blockzeit fallen und länger als drei Stunden andauern. Diese sind steuerbegünstigt.
    Alle 100%igen Überstunden an Sonn- und Feiertagen werden als steuerbegünstigt betrachtet

Sollten Sie in Ihren Arbeitsverträgen keine Trennung zwischen steuerbegünstigt und steuerpflichtig benötigen, so bitten Sie Ihren **Kieselstein ERP** Betreuer die Darstellung zusammenzuziehen.

Ausgleich der Überstunden

Es ergibt sich immer wieder, dass Mitarbeiter einerseits Überstunden machen und andererseits dafür an den nächsten Tagen Zeitausgleich nehmen.
Das bedeutet, dass die angesammelten Überstunden durch die Minusstunden wieder verbraucht werden müssen.
Hier gehen wir so vor, dass am Monatsende diese Stunden ausgeglichen werden.
Hat der Mitarbeiter am Monatsende ein negatives Gleitzeitsaldo, so werden solange Überstunden verbraucht, bis sein Gleitzeitsaldo ausgeglichen ist. Es werden immer die teuersten Stunden zuerst verbraucht. Der Verbrauch erfolgt also in folgender Reihenfolge:

-   Überstunden 100%ig steuerpflichtig
-   Überstunden 100%ig steuerfrei
-   Überstunden 50%ig steuerpflichtig
-   Überstunden 50%ig steuerfrei
-   Mehrstunden

Dieser **Stundenausgleich** wird, falls erforderlich, auf der Monatsabrechnung angedruckt.

![](Stundenausgleich.gif)

Eventuell verbleibende negative Gleitzeitsaldi werden ins nächste Monat übertragen.

Automatische Auszahlung von Überstunden

Bei einigen Firmen werden immer alle Überstunden ausbezahlt, die negativen Stunden bleiben aber bestehen.
Die Auszahlung der Überstunden wird üblicherweise erst im nächsten Monat abgerechnet. D.h. wenn beim Mitarbeiter diese Automatik aktiviert ist, wird die Auszahlung auf den 1\. des Folgemonates gebucht.
Für die Aktivierung der Auszahlungsautomatik gehen Sie bitte auf Personalgehalt des gewünschten Mitarbeiters. Hier haken Sie bitte ab dem gewünschten Monat bei "Überstunden monatl. auszahlen." an. Nun wird das Feld "Verbleibender Puffer" freigeschaltet. Hier geben Sie den verbleibenden Überstundenpuffer an. Dieser Wert kann auch 0 sein.
Ist nun "Überstunden monatl. auszahlen." angehakt, so werden alle Überstunden, inkl. den Stunden des Vormonates, zum 1\. des nächsten Monates ausbezahlt, solange "Verbleibender Puffer" nicht unterschritten wird. Es werden die teuersten Überstunden zuerst abgebaut.
Damit hängt auch die Überstundenabrechnung/Stundenausgleich zusammen. In einigen Firmen wird dies auch ZA Polster, Zeitausgleichs Polster genannt.

#### Werden ausbezahlte Stunden auch eingetragen wenn nichts auszuzahlen ist?
Wenn die Auszahlungsautomatik (Personal, Personalgehalt) aktiviert ist,
![](Ueberstunden_auszahlen.gif)
so werden nur dann automatische Einträge im Reiter Stundenabrechnung gemacht, wenn auch tatsächlich etwas auszubezahlen ist.
Ist der Saldo negativ, so ist nichts auszuzahlen und daher erfolgt hier auch kein Eintrag.

Hinweis:
Den aktuelle protokollierten Stundensaldo sehen Sie auch im Reiter Gleitzeitsaldo. Hier werden alle sich aus der jeweiligen Abrechnung ergebenden Saldi eingetragen.

Überstundenabrechnung/Stundenausgleich:

Im jeweiligen Kollektivvertrag können die Multiplikatoren für die sogenannten 50%igen und 100%igen Überstunden definiert werden.

Am Ende eines jeden Monats werden vorhandene Überstunden aufgeschlüsselt (inkl. die des Vormonates).

Dabei werden wochenweise 50%igen Überstunden zu den steuerpflichtigen 50%igen Überstunden hinzugerechnet.

Wenn der gesamte Gleitzeitsaldo (also der des aktuellen Monates inkl. der des Vormonates) negativ ist, werden die verfügbaren Überstunden dazu verwendet, den negativen Saldo zu decken.

**Wichtig:** Eine 100%ige Überstunde entspricht einer Überstunde, multipliziert mit dem im Kollektivvertrag hinterlegten Faktor.

**Hinweis:** Die Überstunden werden nur "verbraucht", wenn im Personal ein Kollektivvertrag hinterlegt ist.

Bei der Betrachtung der Stunden aus Sicht der Monatsabrechnung ist zu beachten, dass der linke Bereich die Abrechnung der geleisteten Stunden im Vergleich zu den Sollstunden darstellt. Der Bereich der Überstundendarstellung ist so zu sehen, dass dies die darin enthaltenen Überstunden sind. Daraus ergibt sich, dass z.B. eine 50%ige Überstunde bei einer Auszahlung oder ev. auch bei Zeitausgleich nur mit **zusätzlichen** 0,5Std auswirkt. Die tatsächlich geleistete Arbeitsstunde ist bereits in der Gegenüberstellung der Soll-Ist Stunden enthalten.

Einstellung für einen Durchrechnungszeitraum von 53Wochen

Seit 2008 kann auch ein Durchrechnungszeitraum von 53Wochen mit den Mitarbeitern vereinbart werden. Dies bedingt, dass dies praktisch ein reines Gleitzeitmodell ist. D.h. im Kollektivvertrag sind keinerlei Überstundenfaktoren oder Blockzeiten zu definieren.
Wichtig: Wurden keine Blockzeiten definiert, so ergibt dies bei allfälligen Überstunden immer steuerpflichtige Überstunden.

Überstunden bei negativem Gleitzeit-Saldo verbrauchen bedeutet, dass bei negativem Gleitzeit-Saldo die Überstunden verbraucht werden. Dies ist so geregelt, dass die teuersten Überstunden zuerst verbraucht werden. Also beginnend von den 100%igen Steuerpflichten, zu den 100%igen Steuerfreien, dann die 50%igen Steuerpflichten, dann die 50%igen Steuerfreien und dann die Mehrstunden

Eine Blockzeit von 00:00 bis 00:00 bedeutet keine Steuerbegünstigen Überstunden. D.h. es werden nur Steuerpflichtige Überstunden errechnet. Daher dürfen auch nur Steuerpflichtige Überstunden ausbezahlt werden.

Überstunden-Faktor aus dem Kollektiv und Rückrechnung in ausbezahlte Stunden
a.) Ausbezahlte Überstunden mit Faktor > 0 reduziert die Überstunden (und die Normalstunden)
b.) Ausbezahlte Überstunden mit Faktor = 0 reduziert (nur) die Normalstunden

Ausbezahlte Überstunden sind auf Normalstunden umgerechnete Überstunden
D.h. Überstunden und Mehrstunden werden immer auch in die Normalstunden mitgerechnet

Ist ein Überstundenfaktor von 0 definiert, so werden die Überstunden ohne Veränderung ins nächste Monat übertragen, da diese nur ausbezahlt werden können. Sie werden nicht mit dem negativem Gleitzeitsaldo abgerechnet, da es ja keine **zusätzlichen** Stundenguthaben sind.

## Betriebsvereinbarung

In der Betriebsvereinbarung, Version A haben wir folgende Definition der Überstundenermittlung und des dazugehörenden Verbrauches umgesetzt.

Für Details zur Abrechnungsart Betriebsvereinbarung A siehe bitte unten.

Definition der Überstunden

Durch die Definition der Abrechnungsart Betriebsvereinbarung A für den gewählten Kollektivvertrag wird der Reiter 5 Überstunden Betriebsvereinbarung A ![](Betriebsvereinbarung1.gif) freigeschaltet und die anderen Überstundendefinitionen deaktiviert. Zugleich wird die Detail-Erfassungsmaske für die Parameter der Überstunden auf die Bezeichnung und die Faktoren der 50 und 100%igen Überstunden eingeschränkt.
![](Betriebsvereinbarung2.gif)

Die Sollzeiten kommen nach wie vor aus den Zeitmodellen, werden jedoch nur mehr mit den innerhalb der Gleitzeit angerechnet.

Stundenabrechnung: Die hier positiv eingebuchten Stunden bedeuten, dass dieses ausbezahlt wurden, wirken daher negativ in den jeweiligen Gesamtsaldo.
D.h. wenn eine Umbuchung von z.B. 50% bezahlt auf den Gleitzeitsaldo erfolgen sollte, so werden die 50%igen Üst ausbezahlt, sind also positiv einzugeben, und der Gleitzeitsaldo erhöht sich, ist damit also negativ einzugeben.

Hinweis: Wird der Kollektivvertrag der Betriebsvereinbarung zum ersten Mal aktiviert, so muss danach das Personalmodul neu gestartet werden.
Wichtig: Es ändern sich damit bei den betroffenen Personen die Darstellung der Gleitzeitsaldi als auch der Ausbezahlten Stunden.

Die Logik hinter der Betriebsvereinbarung A:
![](Betriebsvereinbarung3.gif)

Vom Gedankengang gibt es vier Arten von Stunden, von denen drei auch Überstundenzuschläge haben. D.h.:
100%ige Überstunden, das sind die Stunden von 00:00 bis 100%Ende und von 100%Beginn bis 24:00
50%ig Überstunden, das sind die Stunden von 100%Ende bis 50%Ende und von 50%Beginn bis 100%Beginn
Gleitzeitstunden sind grundsätzlich die Stunden zwischen 50%Ende und 50%Beginn, mit der Unterteilung, sind diese Stunden unter der Gleitzeit-Stundenanzahl, so gehen diese in den normalen Gleitzeittopf, sind diese über der angegebenen Gleitzeitstundenanzahl, so gehen diese in den Topf der 50%igen Gleitzeitstunden.
Zusätzlich wird bei Überstunden die eigentliche Zeit (Stunde) und der daraus entstehende zusätzliche Anspruch getrennt verwaltet.
Damit wird erreicht, dass sowohl die echten Anspruchsstunden als auch die Zuschläge getrennt verwaltet / ausbezahlt / übertragen werden können.

![](Betriebsvereinbarung4.gif)

Wie aus obigem Bild ersichtlich, werden angefallene Überstunden immer dem jeweiligen Topf zugebucht. Zugleich mit der Zubuchung wird auch der Zuschlag anhand des Zuschlagsfaktors (siehe Detail des Kollektivvertrags) errechnet und in den Überstundenzuschlagstopf gegeben.
Die Zeiten können wie oben dargestellt entsprechend eingestellt werden.
Ein Sonderfall ist der Zeitraum von 50% Ende bis zu 50% Beginn, welcher grundsätzlich der Gleitzeit-Zeitraum ist. Also jene Zeit in der die tägliche Arbeitszeit erbracht werden sollte. Wird in diesem Zeitraum eine Zeit von Gleitzeit überschritten, wandert diese Zeit in den eigenen Gleitzeit-Topf, welcher ebenfalls als 50%iger Zuschlag betrachtet wird.

## All-In
Da immer wieder die Frage kommt, wie kann ich sogenannte All In Verträge abbilden?

All In bedeutet, dass egal wieviel der Mitarbeiter / die Mitarbeiterin arbeitet, es ist alles bezahlt. Üblicherweise wird das vom Arbeitgeber in der Erwartungshaltung gemacht, dass der Mitarbeiter deutlich mehr als die üblichen 40Std arbeitet. Es gibt aber auch immer wieder Situationen, wo weniger gearbeitet wird. Die Formulierung der Anwender lautet dann meistens, es darf keine Plus und keine Minus-Stunden geben.
{{% alert title="Primary" color="primary" %}}
ACHTUNG: Laut österreichischem Arbeitszeitgesetz müssen die sich aus den geleisteten IST-Stunden ergebenden Stundenlöhne den branchenübliche Stundenlöhne entsprechen. Die Strafen sind hier empfindlich. Bitte darauf achten.
{{% /alert %}}

Wie definiert man das nun?
Lege ein neues Zeitmodell an.
Im Reiter Detail kannst du 
![](All_In_Zeitmodell_01.png)  
die Sollstunden pro Monat nutzen und hier z.B. eine Stunde eintragen. Das bedeutet, dass der Mitarbeiter im Monat mindestens eine Stunde arbeiten sollte.<br>
Nun ordne dem Mitarbeiter dieses Zeitmodell zu.<br>
Zusätzlich gehst du in den Reiter Personalgehalt und trägst bei der Überstundenpauschale einen passenden Wert ein. Hier könnte z.B. 260 Std stehen (160Std hat ein übliches Mitarbeitermonat und 100Std on Top für die Mehrleistung). Das bedeutet, dass der Mitarbeiter zwischen einer Stunde und 261 Stunden keine Überstunden / Mehrzeiten angerechnet bekommt. **ACHTE auf die Arbeitszeitgesetze.**

ACHTUNG: Für Urlaub bzw. Krankenstand und ähnliches gilt der 13Wochen Durchrechnungszeitraum also Definition für die tatsächlich zu bezahlenden Löhne / Gehälter. Diese Auswertung findest du im Modul Zeiterfassung, Auswahl der Person, Info, Wochenabrechnung.<br>
Üblicherweise werden für den Urlaubsverbrauch "normale" Zeitmodelle verwendet und der Urlaub nur tageweise betrachtet.
