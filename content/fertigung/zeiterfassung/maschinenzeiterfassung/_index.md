---
title: "Maschinenzeiterfassung"
linkTitle: "Maschinenzeiterfassung"
categories: ["Maschinenzeiterfassung"]
tags: ["Maschinenzeiterfassung"]
weight: 600
description: >
  Erfassung von Maschinenzeiten, von der manuellen Maschinenbedienung bis zur Mehrmaschinen / Mehr Werkstück Bedienung
---
In **Kieselstein ERP** finden Sie neben der klassischen Mitarbeiterzeiterfassung auch eine sogenannte Maschinenzeiterfassung. Diese wurde vor allem zur Realisierung der sogenannten Mehrmaschinenbedienung eingeführt. Mehrmaschinenbedienung bedeutet, ein Mitarbeiter betreut abwechselnd mehrere Maschinen, also annährend gleichzeitig, da ja die Maschinen mit den jeweiligen CNC Programmen fast vollständig autonom arbeiten.

{{% alert title="Hinweis" color="info" %}}
Die dafür erforderlichen Funktionalitäten im Zeiterfassungsterminal, werden aktuell programmiert. Teile der Mehrmaschinen Funktionen können über die HTML BDE abgebildet werden. Bitte informiere dich vor einem Echteinsatz wie deine Forderungen aktuell und oder in Zukunft umgesetzt werden können.
{{% /alert %}}

Für die Maschinenzeiterfassung benötigen Sie das gleichnamige Modul aus **Kieselstein ERP**.

Wenn Ihre Maschinen immer auch eineN BedienerIn erfordern, so handelt es ich um eine direkte manuelle Bedienung der Maschine. [Details dazu siehe bitte unten](#manuelle Bedienung).

In der Praxis hat sich inzwischen auch eine Sonderform herauskristallisiert, welche wir Parallelbedienung von mehreren Maschinen genannt haben. [Siehe](#Parallele Bedienung).

![](Maschinenzeiterfassung.png)

Mit der Mehrmaschinenbedienung in Kombination mit den Zeiterfassungsstiften kann der Erfassungsaufwand deutlich zu reduziert werden, bei gleichzeitiger exakter Erfassung der tatsächlichen Zeiten auf Maschinen und Mitarbeiter.

Um diese Zeiten auch mittels des Zeiterfassungsstiftes erfassen zu können, müssen folgende Voraussetzungen gegeben sein:

Grundsätzlich ist es so, dass gerade bei der Mehrmaschinenbedienung möglichst keine administrativen Zeiten für die Zeiterfassung verwendet werden soll. D.h. der Buchungsvorgang der Zeit sollte raschest möglich durchgeführt werden können.

Raschest möglich bedeutet ein Scan-Vorgang.

Dies haben wir Mithilfe von Zeiterfassungsstiften realisiert.

Unter der Voraussetzung, dass der Stift personifiziert ist, werden in einem Scannvorgang:
-   Los Nr
-   Maschinen ID
-   Tätigkeit
-   Datum
-   Uhrzeit

erfasst.

Diese Daten werden zusammen mit der Personalnummer im Zeiterfassungsstift gespeichert und dann bei Bedarf in die **Kieselstein ERP** Datenbank eingetragen. Beachten Sie bitte, dass die Stifte **keine** Online-Verbindung zur **Kieselstein ERP** Datenbank haben und dass keine Überprüfung auf erlaubte LosNummern, Maschinen IDs und ähnliches erfolgen kann. Für eine online Verbindung, eine online Zeiterfassung siehe bitte PDA Erfassung.

Um die Daten in einem Scannvorgang unterzubringen, muss der Barcode wie folgt aufgebaut sein, wobei wir zwei Typen unterscheiden:

Buchung mit Maschinen ID, geteilt in Beginn und Ende (= Maschinen Stop)

Buchung ohne Maschinen ID

Los Nr         ID Tätigkeit

06/1234567 12 1234567

06/1234567 12 AZ12345

06/1234567 12 $STOP

Das bedeutet, dass die Losnummer vollständig angedruckt wird, die Maschinen ID ebenfalls. Die Länge der Artikelnummer der Tätigkeit ist auf insgesamt sieben Stellen begrenzt. Als LeadIn Sequenz wird $V verwendet.

Dieser Code wird sowohl vom ZE-Stift als auch von der HTML BDE Station verstanden.

Beide Geräte verstehen aber auch die Einzelcodes, also

$P ... Ausweisnummer

$L ... Losnummer oder $R ... Reparaturnummer oder $A ... Auftragsnummer

$M ... Maschinen ID (kann auch entfallen) oder

$FERTIG ... Fertigmeldung der einen Tätigkeit

$T ... Tätigkeit oder $STOP für die Maschinen Ende Buchung, Ende der Maschinenzeit.

$V ... kombinierter Buchungscode

Anstelle der Losnummer können auch die Sonderformen $KOMMT, $GEHT usw. angeführt werden.

Tipp:

Manchmal ist es praktisch, wenn sich Mitarbeiter aus verschiedenen Schichten jeweils einen Stift teilen. Dies können Sie in der Form realisieren, dass Sie den Stift als Einpersonenstift parametrieren. Nun scannt der Mitarbeiter z.B. nach dem Schichtwechsel, aber **immer**  nach der Entnahme aus der Übertragungsstation seine Ausweisnummer (in der Langform). Damit ist der Stift auf diesen Mitarbeiter eingestellt.

WICHTIG: Beachten Sie dabei bitte, dass bei der Datenübertragung der Stift wieder der hinterlegten Person zugeordnet wird. D.h. nach der Entnahme aus der Übertragungsstation muss der (andere) Mitarbeiter immer seine Ausweisnummer einscannen.

#### Wo werden die Maschinen / die Maschinengruppen definiert?
Die Maschinen werden in der Zeiterfassung, unterer Modulreiter Maschinen definiert.
Die Maschinengruppen werden ebenfalls in der Zeiterfassung, unterer Modulreiter Grunddaten, oberer Modulreiter Maschinengruppen definiert.
Für die Maschinendefinition stehen folgende Felder zur Verfügung:

![](Maschinendefinition.gif)

| Feld | Bedeutung |
| --- |  --- |
| Inventarnummer  | Vergeben Sie eine Inventarnummer für die Maschine |
| Bezeichnung  | Die bei Ihnen gebräuchliche Bezeichnung für die Maschine |
| Maschinengruppe  | Die Maschinengruppe, der Sie diese Maschine zuordnen. Nach den Maschinengruppen wird auch die Kapazitätsvorschau gruppiert. |
| Identifikationsnummer  | Ein zweistelliges Kennzeichen um die Maschine eindeutig zu identifizieren, bestehend aus Buchstaben und Ziffern. Diese Nummer wird im Barcode der Zeiterfassung hinterlegt. Mit den zwei Zeichen können 1296 Maschinen definiert werden. |
| Kaufdatum  | Als Erinnerung, wann die Maschine gekauft wurde. |
| manuelle Bedienung | Diese Maschine wird immer von einer Person bedient. D.h. die Maschinenzeit ist immer gleich der Mitarbeiter-Zeit |
| Automatisches Stop bei Geht | Default Vorschlagswert für neue Tätigkeiten für die Buchung des automatischen Stops der Maschine bei der Mitarbeiter-Gehtbuchung |
| Anschaffungskosten | Die Anschaffungskosten der Maschine als Berechnungsgrundlage für den geplanten (internen) Stundensatz |
| Abschreibungsdauer  | Auf wieviele Monate wird die Laufzeit der Maschine gerechnet |
| Verzinsung / Jahr | die kalkulatorischen Zinsen die für die Finanzierung der Maschine gerechnet werden |
| Energiekosten / Jahr  | welche Energiekosten zum Betrieb der Maschine pro Jahr werden veranschlagt |
| Raumkosten / Jahr | Der Raum / Platz den diese Maschine belegt, lässt sich in Raumkosten ausdrücken. |
| sonstige Kosten / Jahr  | Hinterlegen Sie hier weitere Kosten |
| Planstunden / Jahr | Wieviele Stunden im Jahr sollte diese Maschine geplanter Weise laufen. |


Zusätzlich kann der Stundensatz der Maschine im oberen Modulreiter Kosten definiert werden. Hinterlegen Sie hier den Stundensatz der Maschine ab einem gewissen Zeitpunkt.
Die Maschinenverfügbarkeit wird durch das Maschinenzeitmodell dargestellt. Siehe Grunddaten.

#### Können auch Maschinen-Leistungsfaktoren hinterlegt werden?
Es können auch sogenannte Maschinen-Leistungsfaktoren abhängig vom verwendeten Material definiert werden.
Das bedeutet, dass sich der Abnützungsfaktor und damit der Kostenfaktor der Maschine ändert, je nachdem welches Material verwendet wird.
So hat z.B. normales Aluminium den Faktor 100\. Verwendet man aber z.B. Alloy 625 (2.4856) so ergibt dies einen Leistungsfaktor von 170\. Das bedeutet bei Alloy wird die Maschine gegenüber Aluminium um 70% stärker beansprucht, womit auch der Kostenfaktor der Maschine der 1,7 fache ist.

Wie ist das nun zu definieren?
- Legen Sie im Artikel, unterer Modulreiter Material die gewünschten Materialien an
- Für Ihre Zukaufsartikel / Zuschnitte usw., definieren Sie im Reiter Technik aus welchem Material dieser Artikel ist.
![](Material_beim_Zukaufsartikel_definieren.gif)
- Definieren Sie nun für jede Maschine und für jedes relevante Material die entsprechenden Aufschlagsfaktoren
![](Material_definieren.gif)

Wenn nun in der Stückliste im Arbeitsplan auch die Maschine definiert ist, und diese angenommen, einen kalkulatorischen Stundensatz von 50,- hat, so wird für eine Kalkulation OHNE Materialaufschlag der Faktor von 1,00 herangezogen, womit in unserem Beispiel der Gestehungspreis bei 50,- bleibt.
![](Musterkalkulation_mit_Aluminium.jpg)

Ist in der Stückliste aber das **erste** Material z.B. mit Alloy definiert und ist bei der Maschine ein Leistungsfaktor von 70% hinterlegt, so wird der kalkulatorischen Stundensatz der Maschine auf 85,- € erhöht. Womit wir zu folgender Kalkulation kommen.
![](Musterkalkulation_mit_Alloy.jpg)

Wie wird nun der Artikel definiert, der das Material und damit den Leistungsfaktor definiert?
- Wenn einem Arbeitsgang direkt eine Materialposition zugeordnet ist
- Wenn einem Arbeitsgang keine Materialposition zugeordnet ist, dann die erste Materialposition aus dem Reiter Position, die weder Stückliste noch Hilfsstückliste sind. Also die echte erste Materialposition.
- In den Angebotsstücklisten wird zusätzlich noch unterstützt. Wurde bisher keine Materialzuordnung gefunden, so wird der erste Eintrag aus dem Rohmaterial (der Reiter 5 Material) herangezogen.

Wird kein Material gefunden oder keine Materialzuordnung im Artikel oder es ist kein Maschinen-Leistungsfaktor definiert, so wird der Faktor 1 angenommen.

Info: Dieser Aufschlag wird bei allen Maschinen-Stundensätzen, die einen Stücklisten-, Los- oder Angebotsstücklistenbezug haben, hinzugerechnet.

### Zuordnung der Abrechnungsartikel zu den Maschinen
Wenn Sie auch das Modul Abrechnungsvorschlag besitzen muss zusätzlich zu jeder Maschine auch ein Verrechnungsartikel definiert werden. Das bedeutet, dass der Verkaufspreis dieses Verrechnungsartikel als Basis für die stundenweise Verrechnung verwendet wird.
![](Maschinenabrechnungsartikel.gif)
Bitte beachten Sie gegebenenfalls die erforderliche Übereinstimmung für die verschiedenen Vorkalkulationen und daraus die Verrechnung.

### Wie werden Maschinenzeitmodelle definiert?
Im Modul Zeiterfassung finden Sie einen unteren Reiter Masch. Zm. ![](MaschZeitM.JPG). Hier Können Sie die Zeitmodelle der Maschinen hinterlegen, klicken Sie dazu auf den unteren Reiter, so erhalten Sie eine Übersicht über die derzeit angelegten Maschinenzeitmodelle. Sobald Sie Verfügbarkeiten eingetragen hatten, werden entsprechende Maschinenzeitmodelle angelegt. 
Um ein neues Zeitmodell anzulegen, klicken Sie auf Neu, geben einen sprechenden Namen ein und klicken auf Speichern. Nun können Sie im Reiter Zeitmodelltag die einzelnen Tage und die Verfügbarkeit der Maschine in Stunden eingeben.
![](maschZeitmodelltag.JPG)

#### Kann die Maschinenzeit auch manuell erfasst werden?
Ja. Wählen Sie den unteren Modulreiter Maschinen, so können hier die Zeitbuchungen der Maschinen erfasst und korrigiert werden.

Im Gegensatz zur Mitarbeiterzeiterfassung wird hier davon ausgegangen, dass Maschinen eine gewisse Zeit laufen. D.h. jede Maschine hat eine Von-Bis Zeit während dieser sie für ein Fertigungslos für eine bestimmte Tätigkeit gebucht ist.
Der Start einer Maschine muss immer von einem Mitarbeiter durchgeführt werden, daher ist für die Von Buchung auch eine Person anzugeben. Dieser Mitarbeiter ist für die Maschine ab diesem Zeitpunkt verantwortlich.
Damit Sie einen einfachen Überblick erhalten, welche Maschine gerade welchen Auftrag bearbeitet, wird in der Auswahlliste der Maschinen auch die jeweils letzte Buchung angezeigt.
![](Maschinen_Auswahlliste.jpg)

Die Von Bis Zeit Definition bedingt auch, dass immer wenn neue Zeiten dazu kommen, automatisch die Bis-Zeit der letzten Buchung eingetragen wird, wenn diese noch keine Bis Zeit eingetragen hat.
![](Maschinenzeiterfassung_manuell.jpg)
Zusätzlich kann, z.B. als Erinnerung, warum diese Zeit verändert wurde, eine Bemerkung angegeben werden.

#### Wieso ist der Bis Eintrag manchmal ein Pflichtfeld?
Wird ein Maschinen-Zeiteintrag nachträglich eingefügt, so muss, um das Ende diese Zeitbuchung zu definieren, auch der Bis-Eintrag eingegeben werden. Dies kommt gerne vor, wenn bestehende Maschinenzeitbuchungen geändert werden.

<a name="Maschinenkosten"></a>

#### Welche Kosten werden für die Maschinenstunde angesetzt?
Im oberen Modulreiter Kosten, können die Kosten ab einem gewissen Datum pro Stunde für diese Maschine definiert werden.
Zusätzlich stehen die Definition von Anschaffungskosten, Verzinsung, Abschreibungsdauer und sonstigen Kosten zur Verfügung, um eine theoretische Berechnung der Stundenkosten einmal auf Basis der geplanten Jahresstunden und andererseits auf Basis der tatsächlichen auf die jeweilige Maschine gebuchten Kosten hinterlegen und berechnen zu können.

![](Maschinenkosten.gif)

Es ist hoffentlich allen Kostenrechnern und Kalkulanten transparent, dass dies nur eine sehr theoretische Betrachtung von geplanten Maschinenkosten sein kann. Für die tatsächlichen Kosten ist die Erfassung der tatsächlichen Reparaturaufwände und der tatsächlichen Laufzeit wesentlich. D.h. je genauer / umfassender Sie die tatsächlichen Laufzeiten der Maschine erfassen umso besser können auch die tatsächlichen Stunden der jeweiligen Maschine errechnet werden und um so realistischer sind Ihre abgegebenen Verkaufspreise.

Die Auswertung dazu finden Sie in der Produktivitätsstatistik. Es ist dafür das Recht LP_FINANCIAL_INFO_TYP_1 erforderlich.
Die Berechnung der theoretischen Stundensätze finden Sie im Journal Maschinenliste.

#### Maschinenbelegung darstellen
Unter Journal Maschinenbelegung kann die geplante Belegung der Maschinen dargestellt werden. Wird
![](nur_selektierte_maschine.gif) nur selektierte Maschine angehakt, so wird nur die Belegung für diese Maschine errechnet, was einen deutlichen Geschwindigkeitsvorteil bringt.

In dieser Darstellung sehen Sie einerseits die kurzfristige Belegung der jeweiligen Maschine für die nächsten zwei Wochen
![](Maschinenbelegung.jpg)
Mit den schwarzen Balken wird signalisiert an welchen Tagen die Maschine geplanter Weise belegt ist.

Weiters wird die Entwicklung der Maschinenauslastung über den eingestellten Zeitraum dargestellt.
![](Maschinenauslastung.jpg)
Hier bedeutet der Schnitt zwischen Belegung und Verfügbarkeit, dass bei kontinuierlichem Lauf der Maschine diese bis zum Schnittpunkt der beiden Linien, die Maschine ausgelastet ist.
Das bedeutet weiters, dass idealerweise die Verfügbarkeit immer ein kleinwenig über der Belegung liegt. Was auch bedeutet, dass sie immer rechtzeitig Lieferfähig sind. Ist die Verfügbarkeit höher als die Belegung, so bedeutet dies gleichzeitig, dass ab diesem Zeitpunkt keine Aufträge / Arbeitsgänge mehr für die Maschine eingeplant sind.

#### Kann die nachfolge Maschine angezeigt werden ?
Wird, mit Maschine des nächsten Hauptarbeitsgangs ![](naechste_Maschine_anzeigen.gif)  angehakt, so wird ausgehend vom Arbeitsgang der Maschine die Maschine des ersten Unterarbeitsgangs des 

nächsten (Haupt-) Arbeitsgangs angezeigt. Dies wird gerne für Maschinen (= Arbeitsplatz/Tätigkeit) wie z.B. Werkzeugvorbereitung oder Materialanlieferung verwendet. Damit erhalten Sie die Information, zu welcher Maschine welches Werkzeug bzw. welches Material gebracht werden muss.

#### Wie kann man am effizientesten mit der Maschinenbelegung arbeiten ?
In der Praxis hat sich bewährt, dass für die Einplanung der Maschinen diese einzeln abgearbeitet werden. Zuerst werden die am dichtesten belegten Maschinen eingeplant, hin zu den weniger dicht belegten.

Um eine Maschine effizient und rasch durchzuplanen hat sich bewährt, dass in zwei **Kieselstein ERP** Clients auf einem Zwei-Bildschirmsystem mit entsprechend hoher Auflösung gearbeitet wird.

So haben Sie z.B. am linken Bildschirm die Losverwaltung geöffnet und am rechten Bildschirm wird die Belegung einer Maschine angezeigt. Nun sehen Sie rechts die Auslastung Ihrer Maschine, verschieben links die Arbeitsgänge der Lose laut Ihren Erkenntnissen / Bedarf und klicken rechts wiederum auf aktualisieren. In wenigen Sekunden ist die Belegung neu durchgerechnet.

Unsere Anwender erreichen damit eine sehr gute Vorplanung der Maschinen. Dass diese Darstellung nur bei entsprechend exakter Erfassung der Solldaten richtig ist, erklärt sich von selbst. Beachten Sie bitte auch, dass die Fertig-Rückmeldung des jeweiligen Arbeitsganges z.B. über die Zeiterfassungsterminals entsprechend wichtig ist.

#### Kann eine Maschinen Besten Liste erstellt werden ?
Ja. [Siehe bitte Maschinenerfolg](index.htm#Maschinenerfolg).

<a name="Einzelarbeitsgang"></a>

#### Bei der Erfassung der Stückrückmeldungen sollte immer nur der letzte Arbeitsgang des Mitarbeiters angezeigt werden ?
In manchen Unternehmen wird immer sequenziell gearbeitet. D.h. trotz der Möglichkeit der Mehrmaschinenbedienung/-erfassung, sollten den Fertigungsmitarbeitern, also den am Terminal buchenden Personen immer nur der zuletzt angestempelte Arbeitsgang angezeigt werden. Um diese Funktionalität zu aktivieren stellen Sie bitte den Parameter EINZELARBEITSGANG_BUCHEN auf 1.
Ergänzend [siehe dazu bitte auch](Zeiterfassungsterminal_Nettop.htm#$PLUS).

<a name="Maschinenzeitbuchungen finden"></a>

#### Wie sieht man, auf welche Maschinen sich eine Mitarbeiterzeitbuchung bezieht?
In der Zeiterfassung steht auch eine Anzeige der mit der Zeitbuchung verbundenen Maschinenzeitdaten zur Verfügung. Das bedeutet, wenn von Ihren Mitarbeitern, z.B. am Terminal, auch gleichzeitig Maschinen(zeiten) gestartet werden, so kann dies durch Klick auf zugehörige Maschinenzeitdaten ![](Maschinenzeitbuchungen.gif) eine Liste angezeigt werden, in der die Maschinen der mit dem ausgewählten Losarbeitsgang verbundenen Maschinenzeitbuchungen anzeigt. Da die Zuordnung nicht 1:1 gegeben sein muss (Mehrmaschinenbedienung) können hier auch mehrere Buchungen angezeigt werden. Ein Doppelklick auf eine der Maschinenzeitbuchungszeilen bringt Sie direkt zur Buchung.

### Wir haben eine Maschine verkauft und müssen nun alle Arbeitspläne ändern. Wie geht das effizient?
A: [Siehe bitte Maschine ersetzen](../Stueckliste/index.htm#Maschine ersetzen).

## manuelle Bedienung, Bedienung einzelner Maschinen
Abhängig von der Art des Unternehmens, aber auch der Art der Maschinen wird oft auch erwartet, dass Maschinen immer eine manuelle Bedienung haben.
Das bedeutet, die Maschine läuft nur, wenn auch ein Mitarbeiter diese bedient. Das bedeutet weiters, dass wenn der Mitarbeiter die Tätigkeit bzw. den Arbeitsgang bzw. das Los wechselt, wird die Maschine gleichzeitig mitgestoppt. Dies gilt auch für Unterbrechungen und die Geht und Ende Buchung.
Das hat in der Konstellation den Vorteil, dass der Mitarbeiter sich hier nicht um die Maschinenstop-Buchungen kümmern muss und somit die Bedienung wesentlich einfacher ist.
Diese Funktion ist als Eigenschaft der Maschine gestaltet. D.h. dafür muss bei der Maschine der Haken bei manuelle Bedienung gesetzt sein. ![](manuelle_Bedienung.gif)
Dies bewirkt gleichzeitig, dass bei Buchungen auf Los-Arbeitsgänge in der Zeiterfassung des Mitarbeiters, auch eine Maschine aus der Gruppe der manuellen Bedienung angegeben werden kann, bzw. dass hier direkt die Maschine mit angezeigt wird.

**Info:** Wechselt ein Mitarbeiter zwischen Maschinen die als Mehrmaschinenbedienung definiert sind und Maschinen mit manueller Bedienung hin und her, so wird die Laufzeit der Maschinen der Mehrmaschinenbedienung NICHT unterbrochen.

## Parallele Bedienung mehrerer Maschinen
In einigen Betrieben werden sehr flexibel nicht nur gleiche Teile, sondern der gleiche Arbeitsgang zum gleichen Zeitpunkt vom selben Mitarbeiter bedient. Dies ist in der Regel dann der Fall, wenn damit auch gleiche Maschinen (vom gleichen Typ) für diesen Arbeitsgang programmiert sind.
Um hier eine so rasche und doch ausreichend exakte Erfassung der tatsächlichen Zeiten zu erreichen gibt es die Buchung der Parallelen Bedienung. Diese steht aktuell nur als Barcode zur Verfügung. Auch das Lose-gemeinsam-buchen ist für diese Art der Buchung nicht vorgesehen.
Interessant ist diese Art von Buchung immer dann, wenn auf den z.B. drei von vier Maschinen der selbe Auftrag-Arbeitsgang läuft und auf der vierten Maschine werden Lückenbüßer eingefüllt. D.h. je nach aktuellem Fertigungsstand, wird mal schnell ein Mini-Auftrag mit 100Stk dazwischen geschoben, dann steht die vierte Maschine eine kurze Zeit und dann kommt der nächste Kleinauftrag. Parallel wird vom selben Mitarbeiter laufen der große Auftrag mit seinen 100.000Stk bedient.
![](Parallele_Maschinenbedienung.gif)
So bedeutet dieses Bild, dass ein Mitarbeiter vom Hauptauftrag (AB10) drei Maschinen gleichzeitig laufen lässt und sich dazwischen im Kreis bewegt und einfach nur die Teile einlegt. Zusätzlich macht er, weil noch ein paar Minuten übrig sind 2Std am Auftrag 20 für 2Std und dann ab 11:00 noch 3Std am Auftrag 30.

Die Bedienung ist nun wie folgt:
-   Der Mitarbeiter bucht sein Kommt, z.B. 8:00
-   Der Mitarbeiter startet alle vier Arbeitsgänge (3x AB 10, AG20 und 1x AB 20, AG10)
-   um 10:00 ist er mit dem Kleinauftrag 20 fertig und stoppt daher die Maschine
-   ev. rüstet in der Zwischenzeit ein anderer Mitarbeiter den Arbeitsgang für den Auftrag 30/AG50
-   um 11:00 startet er nun den Auftrag 30, AG50
-   und beendet diesen um 14:00
-   Hier, also beim Ende der jeweiligen Arbeitsgänge gibt er die gefertigte Stückzahl an. Idealerweise werden nur mehr die gesamt gefertigten Mengen angegeben und keine Stückrückmeldung des einzelnen Arbeitsganges (es würde dies technisch funktionieren, verkompliziert aber die Buchung)<br>
Es kann hier durchaus die Gesamtstückzahl für alle drei Maschinen in einer Ablieferbuchung angegeben werden.
-   Alle anderen Arbeitsgänge beendet er durch seine Geht-Buchung um 17:00
WICHTIG: Bei all diesen Maschinen muss automatisches Stop bei Geht aktiviert sein.

Hinweis: Um die Barcodes für alle Maschinen zu erzeugen müssen diese auch im Arbeitsplan mit angegeben sein, bzw. müssen die Maschinencodes entsprechend vorliegen.

In diesem Falle ist die Buchungsreihenfolge: Ich, Maschine (SNr1), Parallele Bedienung. Danach wiederum: Ich, Maschine (SNr2), Parallele Bedienung, usw.

### Wie errechnen sich die Kosten für die Teile?
Die Kosten für die Teile errechnen sich wie folgt:
-   Die Laufzeit der Maschine Snr1, auf den ersten Block, plus den 2\. & den 3.Block, sind also 3 x 11Std = 33Std á 20,- (Kalk.Stundensatz)
-   darauf aufgeschlagen wird die automatisch errechnete anteilige Mannzeit. D.h. der Mitarbeiter hat 3x11 + 1x2 + 1x3 Std an den Maschinen gearbeitet. Also hat er einen Maschinenbestenfaktor von 3,45 erreicht bzw. sind je Maschinenstunde noch 0,29Stunden aus den Personalkosten hinzuzurechnen.

Damit haben Sie eine möglichst genaue Abbildung der auf den Maschinen gebuchten Zeit erreicht.

Oder in anderen Worten: Mit dieser Art der Buchung hat die Maschinenzeit, welche sehr oft verkauft wird, Vorrang, vor der Mitarbeiterzeit.

Bearbeiten von gebuchten Zeiten:
Sind finden im Modul Zeiterfassung, unterer Reiter Maschinen, die jeweilige Maschine, Zeitdaten, einen Haken für ![](Maschinenparallelbedienung.gif) Parallelbedienung. Damit wird signalisiert, dass hier die Maschine Vorrang in der Betrachtung hat.

### Wie wird rüsten gebucht?
Rüsten bzw. sonstige Tätigkeiten werden wie üblich gebucht. D.h. von der Zeit die für die Verteilung der Mitarbeiterzeit auf die Maschinen zur Verfügung steht, wird die Zeit die für normale Tätigkeiten aufgewendet wurde abgezogen.

Bitte beachten Sie dass sich daraus ergibt:
Die anteilige Arbeitszeit eines Mitarbeiters mit paralleler Maschinenbedienung, wird immer ausgehend von den "nicht gebuchten" Tätigkeiten auf die parallele Maschinenbedienung verteilt.

Ein Beispiel:
- 08:00 Kommt des Mitarbeiters
- 08:00 Rüsten auf Maschine 1
- 10:00 Ende (Rüsten auf Maschine 1)
- 10:01 Start parallele Bedienung Maschine 1
- 10:02 Start parallele Bedienung Maschine 2
- 14:00 Stop Maschine 1
- 16:00 Geht mit Autostop der Maschine 2

Das bedeutet:
1. Der Mitarbeiter ist 8Std anwesend
2. Er hat 2 Std direkt eine Tätigkeit gebucht. Diese Zeit wird von der zu verteilenden Zeit auf die parallele Bedienung abgezogen
3. Seine, auch direkt nicht sichtbare, "freie" Zeit wird auf die Maschine 1 und auf die Maschine 2 verteilt.

Daher wichtig: Wenn innerhalb eines Mitarbeiters und eines Tages, zwischen paralleler Bedienung und direkt gebuchten Tätigkeiten gewechselt wird, muss die direkt gebuchte Tätigkeit beendet werden.
Es bedeutet dies auch, wenn mit einer weiteren Tätigkeit, die sich nicht auf die parallele Bedienung bezieht, begonnen wird, ist diese selbstverständlich zu buchen / stempeln.

### Kann bei der parallelen Maschinenbedienung auch die Stückrückmeldung am Arbeitsgang gemacht werden?
A: Das ist derzeit so nicht vorgesehen. Nachdem die parallele Maschinenbedienung nur für die Erfassung der Maschinenzeiten gedacht ist, kann aktuell keine Stückrückmeldung auf den einzelnen Arbeitsgang gebucht werden. Sollte dies in Ihrer Anwendung erforderlich sein, prüfen Sie bitte zuerst die Verwendung von [$PLUS](Zeiterfassungsterminal_Nettop.htm#$PLUS), also dem gleichzeitigen Start der mehrere Arbeitsgänge / Lose, also Losegemeinsam buchen.
Reicht dies nicht aus, wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.