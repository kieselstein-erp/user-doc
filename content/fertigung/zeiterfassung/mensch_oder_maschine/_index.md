---
title: "Mensch oder Maschine"
linkTitle: "Mensch oder Maschine"
categories: ["Maschinenzeiterfassung"]
tags: ["Mensch oder Maschine"]
weight: 8000
description: >
  Welche Resource hat Vorrang? Mensch oder Maschine
---
Mensch oder Maschine
====================

Diese Frage kommt, meist in sehr versteckter Form bei jeder Systemeinführung.
Die Frage an Ihr Unternehmen ist, wie werden die vorhandenen Resourcen genutzt und weiterverrechnet.

Wir empfehlen dringend, dass Sie sich für eine führende Resource entscheiden.

So gibt es viele Branchen, welche den Menschen, das Personal, die Mitarbeiter:innen als die führende Resource betrachten. Oder in anderen Worten, Hauptsache es sind die qualifizierten Mitarbeiter:innen im Hause / tätig. Die Maschinen dafür haben Sie alle Mal.

In anderen Branchen, insbesondere in der mechanischen Lohnfertigung, ist die Resource die tatsächlich zählt und somit auch die die verkauft wird, die Maschine. Es wird alles daran gesetzt, dass die Maschine faktisch 24Std / 7 Tage die Woche läuft. Hauptsache die Maschine läuft, die Mitarbeiter:innen dafür haben wir immer ausreichend.

Interessant werden natürlich Betrachtungen, bei denen zwar die Maschinenauslastung gegeben sein muss, aber z.B. für das Rüsten müssen dann doch Mitarbeiter:innen mit besonderen Kenntnissen verfügbar sein.

Hier nun eine Sammlung von Wünschen und deren dafür erforderlichen Einstellungen:

### Es zählt nur der Mensch
Für die Vorkalkulation müssen bei allen Tätigkeitsartikeln im Artikellieferant die entsprechenden kalkulatorischen Stundensätze (= interner Aufwand) hinterlegt sein
Bei den Maschinen dürfen keine Kosten hinterlegt sein.
Um die tatsächlichen Mitarbeiterkosten zu erhalten muss die Personalkostenquelle auf 1 oder 2 gestellt sein und bei den Mitarbeiter:innen im Reiter Gehalt der Kalkulatorische Stundensatz eingetragen sein

### Es zählt nur die Maschine
Üblicherweise gilt dies nur für Arbeiten, die tatsächlich an der Maschine durchgeführt werden.
Hier hat sich bewährt, dass anhand der Tätigkeiten, Drehen, Fräsen, bereits festgelegt wird, welche Tätigkeit eine Maschine benötigt und welche nicht.

Somit legen Sie für die Tätigkeiten ohne Maschine, wie z.B. verpacken, reinigen, im Artikellieferant den kalkulatorischen Stundensatz auf den gewünschten internen Aufwand / Stundensatz.

Für die Tätigkeiten die an der Maschine durchgeführt werden (Drehen, Fräsen) hinterlegen Sie im Artikellieferant einen kalkulatorischen Stundensatz von 0,01 (damit in der Stücklistengesamtkalkulation keine Fehler angezeigt werden).

Bei den Maschinen hinterlegen Sie den jeweiligen kalkulatorischen Stundensatz = interner Aufwand für die Maschine.

Für eine Vorberechnung für den Maschinenstundensatz nutzen Sie gerne auch die Werte in den Maschinendetaildaten und dann das Journal der Maschinenliste, in der anhand Ihrer Detaildaten theoretische Stundensätze errechnet werden.

Die Personalkostenquelle muss in diesem Falle auf 0 stehen. Damit werden die Mitarbeiter:innen-Kosten aus dem Artikellieferanten geholt.