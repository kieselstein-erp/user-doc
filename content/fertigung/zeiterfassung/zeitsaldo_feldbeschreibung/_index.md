---
title: "Zeiterfassung"
linkTitle: "Monatsabrechnung Feldbeschreibung"
categories: ["Zeiterfassung"]
tags: ["Monatsabrechnung Feldbeschreibung"]
weight: 225
description: >
  Monatsabrechnung Feldbeschreibung
---
Zeitsaldo-Feldbeschreibung
==========================

Da viel der **Kieselstein ERP** Anwender die Auswertung der Zeitsaldos auch für die Überleitung in verschiedene Lohnverrechnungssysteme nutzen, haben wir hier eine Beschreibung jedes Feldes zusammengestellt. Bitte beachten Sie, dass die Felder der Betriebsvereinbarung A nur dann zur Verfügung stehen, wenn auch die Kollektivvertragsdefinition entsprechend gegeben ist.<br>
Hinweis:<br>
Bitte beachten Sie dazu auch den [Lohndatenexport]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ), welcher mit dem Zeitsaldo genau genommen nichts zu tun hat.

Bitte beachten Sie auch, dass diese Darstellungen sich immer auf die Daten einer Person und eines Monates beziehen. Weitere Details für verschiedene Auswertungen siehe bitte auch Monatsabrechnung.

| Feldname | Beschreibung | BVA Topf |
| --- |  --- |  --- |
| Personalnummer | Die Personalnummer des Mitarbeiters/der Mitarbeiterin |   |
| Person | Vorname und Nachname der Person |   |
| MonatJahr | Monatsname und Jahr (März 2018) |   |
| Soll | Sollstunden des Monates lt. Zeitmodell OHNE Feiertagssoll |   |
| Ist | geleistete Iststunden |   |
| Feiertagsoll | Sollstunden die auf Feiertage entfallen |   |
| Feiertag | Feiertagsname |   |
| Arzt | Stunden die im Abrechnungszeitraum auf Arzt gebucht sind |   |
| Krank | Stunden die im Abrechnungszeitraum auf Krank gebucht sind |   |
| Behoerde | Stunden die im Abrechnungszeitraum auf Behörde gebucht sind |   |
| GleitzeitsaldoAktuellerMonat | Gleitzeitsaldo zum Ende des Auswertemonates |   |
| GleitzeitsaldoVormonat | Gleitzeitsaldo zum Ende des Vormonates des Auswertemonates |   |
| Sonstigebezahlt | Sonstige im Abrechnungszeitraum bezahlte Abwesenheitsstunden (Siehe Sondertätigkeiten, Bezahlt) |   |
| Sonstigenichtbezahlt | Nicht bezahlte im Abrechnungszeitraum Abwesenheitsstundenwie z.B. Zeitausgleich |   |
| Reise | Stunden der aktiven Reise im Abrechnungszeitraum |   |
| UestdsaldoMehrstunden | Überstunden Saldo der Mehrstunden zum Ende des Auswertemonates |   |
| Uestdsaldo50 | Überstunden Saldo der 50%igen Überstunden (nicht steuerbegünstigt) zum Ende des Auswertemonates |   |
| Uestdsaldo50Steuerfrei | Überstundensaldo der 50%ig steuerbegünstigen Überstunden zum Ende des Auswertemonates |   |
| Uestdsaldo100 | Überstunden Saldo der 100%igen Überstunden (nicht steuerbegünstigt) zum Ende des Auswertemonates |   |
| Uestdsaldo100Steuerfrei | Überstunden Saldo der 100%igen stuerbegünstigte Überstunden zum Ende des Auswertemonates |   |
| Urlaub | Konsumierte Urlaubsstunden im Auswertemonat |   |
| UrlaubStundenAliquot | Urlaubsanspruch in Stunden inkl. Auswertemonat |   |
| UrlaubStundenGeplant | Nach dem Auswertemonat bereits eingeplante Urlaubsstunden |   |
| UrlaubStundenAktuell | Urlaubsstunden des Abrechnungsjahres Anspruch |   |
| UrlaubStundenAktuellVerbraucht | Urlaubsstunden des Abrechnungsjahres verbraucht bis inkl. Auswertemonat |   |
| UrlaubStundenRest | Urlaubsstunden aus den Vorjahr(en) (Übertrag) |   |
| UrlaubStundenRestVerbraucht | Anzahl der aus den Vorjahren verbrauchte Urlaubsstunden |   |
| UrlaubTageAliquot | Urlaubsanspruch in Tagen inkl. Auswertemonat**Hinweis:** Die auf der Monatsabrechnung ausgewiesenen Stunden bzw. Tage verfügbarer Urlaub ergibt sich aus eine Subtraktion vonUrlaubTageAliquot minus UrlaubTageAktuellVerbraucht. Gegebenenfalls ist der Resturlaub dazuzuzählen. D.h. Plus UrlaubTageRest minus UrlaubTageRestVerbraucht. |   |
| UrlaubTageGeplant | Nach dem Auswertemonat bereits eingeplante Urlaubstage |   |
| UrlaubTageAktuell | Anspruch der Urlaubstage des Abrechnungsjahres |   |
| UrlaubTageAktuellVerbraucht | Urlaubstage des Abrechnungsjahres verbraucht bis inkl. Auswertemonat |   |
| UrlaubTageRest | Urlaubstage aus den Vorjahr(en) (Übertrag) |  |
| UrlaubTageRestVerbraucht | Anzahl der aus den Vorjahren verbrauchte Urlaubsstunden |   |
| UrlaubTageVorjahr |   |   |
| Uestdsaldo200 | Überstunden Saldo der 200%igen Überstunden zum Ende des Auswertemonates |   |
| Ausbezahlt200 | im Auswertemonat ausbezahlte 200%ige Überstunden |   |
| Ausbezahlt100 | im Auswertemonat ausbezahlte 100%ige nicht begünstigte Überstunden |   |
| Ausbezahlt100Steuerfrei | im Auswertemonat ausbezahlte 100%ige begünstigte Überstunden |   |
| Ausbezahlt50 | im Auswertemonat ausbezahlte 50%ige nicht begünstigte Überstunden |   |
| Ausbezahlt50Steuerfrei | im Auswertemonat ausbezahlte 50%ige begünstigte Überstunden |   |
| AusbezahltGutstunden | im Auswertemonat ausbezahlte Gutstunden |   |
| AusbezahltMehrstunden | im Auswertemonat ausbezahlte Mehrstunden |   |
| AusbezahltNormalstunden | Im Auswertemonat ausbezahlte Normalstunden.In den Normalstunden sind alle anderen Stunden (50/100/200) ebenfalls enthalten. |   |
| Qualipraemie | im Auswertemonat bezahlte Qualifikationsprämie in Mandantenwährung |   |
| KindKrank | im Auswertemonat enthaltene Stunden zur Pflege Ihrer Kinder / Angehörigen |   |
| Abteilung | Abteilung des Mitarbeiters |   |
| Kostenstelle | Kostenstelle des Mitarbeiters |   |
| Eintrittsdatum | Eintrittsdatum |   |
| KollektivAbrechnungsart | Art des Kollektivvertrages, vor allem Betriebsvereinbarung A (BVA) |   |
| BetriebsvereinbarungAUestd100 | angefallene Überstunden 100% lt. BVA im Auswertemonat | 100% |
| BetriebsvereinbarungAUestd50 | angefallene Überstunden 50% lt. BVA im Auswertemonat | 50% |
| BetriebsvereinbarungAUestd50UeberGleitzeit | angefallene Überstunden 50% lt. BVA im Auswertemonatfür den Zeitbereich innerhalb der Gleitzeit | G50% |
| BetriebsvereinbarungAGleitzeit | angefallene Stunden lt BVA im Auswertemonat | GZ |
| BetriebsvereinbarungAUestd50UeberGleitzeitZuschlag | angefallene Zuschlagsstunden 50% lt. BVA im Auswertemonatfür den Zeitbereich innerhalb der Gleitzeit | G50%-Zuschlag |
| BetriebsvereinbarungAUestd50Zuschlag | angefallene Zuschlagsstunden 50% lt. BVA im Auswertemonat | 50%-Zuschlag |
| BetriebsvereinbarungAUestd100Zuschlag | angefallene Zuschlagsstunden 100% lt. BVA im Auswertemonat | 100%-Zuschlag |
| BetriebsvereinbarungAUebertragGleitzeit | Saldo der Gleitzeitstunden inkl. Vormonaten abzgl. Auszahlungen bis zum Auswertemonat | GZ |
| BetriebsvereinbarungAUebertragUestd50UeberGleitzeit | Saldo der Überstunden 50% inkl. Vormonaten abzgl. Auszahlungen bis zum Auswertemonat für den Zeitbereich innerhalb der Gleitzeit | G50% |
| BetriebsvereinbarungAUebertragUestd50UeberGleitzeitZuschlag | Die Zuschlagsstunden zu obigem | G50%-Zuschlag |
| BetriebsvereinbarungAUebertragUestd50 | Saldo der Überstunden 50% inkl. Vormonaten abzgl. Auszahlungen bis zum Auswertemonat | 50% |
| BetriebsvereinbarungAUebertragUestd50Zuschlag | Die Zuschlagsstunden zu obigem | 50%-Zuschlag |
| BetriebsvereinbarungAUebertragUestd100 | Saldo der Überstunden 100% inkl. Vormonaten abzgl. Auszahlungen bis zum Auswertemonat | 100% |
| BetriebsvereinbarungAUebertragUestd100Zuschlag | Die Zuschlagsstunden zu obigem | 100%-Zuschlag |
| BetriebsvereinbarungAAuszahlungGleitzeit | Im Auswertemonat ausbezahlte Gleitzeitstunden | GZ |
| BetriebsvereinbarungAAuszahlungUestd100 | Im Auswertemonat ausbezahlte Überstunden 100% | 100% |
| BetriebsvereinbarungAAuszahlungUestd100Zuschlag | Die im Auswertemonat ausbezahlte Zuschläge zu den 100%igen Überstunden | 100%-Zuschlag |
| BetriebsvereinbarungAAuszahlungUestd50 | Im Auswertemonat ausbezahlte Überstunden 50% | 50% |
| BetriebsvereinbarungAAuszahlungUestd50Zuschlag | Die im Auswertemonat ausbezahlte Zuschläge zu den 50%igen Überstunden | 50%-Zuschlag |
| BetriebsvereinbarungAAuszahlungUestd50UeberGleitzeit | Im Auswertemonat ausbezahlte 50%igen Überstunden innerhalb des Gleitzeitrahmens | G50% |
| BetriebsvereinbarungAAuszahlungUestd50UeberGleitzeitZuschlag | Die im Auswertemonat ausbezahlte Zuschläge zu den 50%igen Überstunden die innerhalb des Gleitzeitrahmens liegen | G50%-Zuschlag |
| BetriebsvereinbarungAVormonatGleitzeit | Saldo der Gleitzeitstunden inkl. Vormonaten vor Auswertemonat | GZ |
| BetriebsvereinbarungAVormonatUestd100 | Saldo der Überstunden 100% inkl. Vormonaten vor Auswertemonat | 100% |
| BetriebsvereinbarungAVormonatUestd100Zuschlag | Die Zuschlagsstunden zu obigem | 100%-Zuschlag |
| BetriebsvereinbarungAVormonatUestd50 | Saldo der Überstunden 50% inkl. Vormonaten vor Auswertemonat | 50% |
| BetriebsvereinbarungAVormonatUestd50Zuschlag | Die Zuschlagsstunden zu obigem | 50%-Zuschlag |
| BetriebsvereinbarungAVormonatUestd50UeberGleitzeit | Saldo der Gleitzeit- Überstunden 50% inkl. Vormonaten vor Auswertemonat | G50% |
| BetriebsvereinbarungAVormonatUestd50UeberGleitzeitZuschlag | Die Zuschlagsstunden zu obigem | G50%-Zuschlag |
| BetriebsvereinbarungAGleitzeitDiff | Differenz zwischen Soll und BetriebsvereinbarungAGleitzeit im Auswertemonat |   |
| BetriebsvereinbarungAEntsprichtNormalstunden | Enthält den in Normalstunden umgerechneten Wert alle Stunden und Zuschläge |   |

BVA ... Betriebsvereinbarung A
Für die Definition welche Stunden in welchen Stundentopf fallen, siehe bitte die Einstellungen im jeweiligen Kollektivvertrag

Wie im [Kollektivvertrag, Art Betriebsvereinbarung A](Ueberstundenabrechnung.htm#Betriebsvereinbarung) ausgeführt gibt es gedanklich sieben Töpfe für die Abrechnung der Überstunden.
GZ ... Gleitzeit in diesem Sinne Normalstunden
G50% ... die über die erlaubten Gleitzeitstunden hinausgehenden aber innerhalb der Gleitzeit angefallenen Stunden
50% ... die Stunden im Zeitbereich der 50%igen Überstunden
100% ... die Stunden im Zeitbereich der 100%igen Überstunden
...-Zuschlag ... die Zuschlagstöpfe dazu