---
title: "Fingerprint"
linkTitle: "Fingerprint"
categories: ["Zeiterfassung"]
tags: ["Fingerprint"]
weight: 9100
description: >
  Der Einsatz von Fingerprintlesern in der Zeiterfassung, wird in der Regel nicht akzeptiert.
---
Fingerprint
===========

In **Kieselstein ERP** steht auch die Identifizierung per Biometrie, hier mittels Fingerprint zur Verfügung.

Dadurch hat jeder Mitarbeiter jederzeit seinen Schlüssel für die Identifikation für die Zeiterfassung oder auch für den Zutritt zur Verfügung. Das Vergessen der Schlüssel oder eine versehentliche Verwendung falscher Schlüssel gehören damit der Vergangenheit an.

Die Identifikation mittels Fingerprint ist so gestaltet, dass sie mit den anderen Erfassungsmöglichkeiten wie z.B. RFID kombiniert werden kann.

Für die Identifikation mittels Fingerprint müssen zuerst einige Finger der jeweiligen Person eingelesen werden.

**ACHTUNG:** Bitte holen sie das schriftliche Einverständnis Ihrer Mitarbeiter für die Abspeicherung der Biometriedaten ein. Beachten Sie die unterschiedlichen Vorschriften in den verschiedenen Ländern.

Für Österreich beachten Sie bitte auch die Ausführungen der [Arge-Daten](http://www2.argedaten.at/php/cms_monitor.php?q=PUB-TEXT-ARGEDATEN&s=62555vqo). [Beachten Sie dazu auch die unten angeführten Vorteile des Fingerprints.](#Vorteile Fingerprint)

## Vorteile der Fingerprintlösung:
Bei der Einführung von biometrischen Systemen muss immer der Betriebsrat (wenn vorhanden) in die Entscheidungsfindung einbezogen werden und zusätzlich eine Betriebsvereinbarung bzw. Einverständniserklärung von den Mitarbeitern unterzeichnet werden. Diese ist dann oft Bestandteil des Arbeitsvertrages. Die Schwierigkeit dabei ist, den Betriebsrat und/oder Datenschutzbeauftragten von der Notwendigkeit der Biometrie zu überzeugen.

Nun noch wichtige Punkte die in unserer Fingerprintlösung zur Sicherheit der personenbezogenen Daten eingesetzt werden:
1.  Es werden keine Bilder der Fingerabdrücke gespeichert, sondern nur Vektordaten, aus denen der Fingerabdruck nicht mehr reproduziert werden kann.
2.  Die Templates (Vektordaten) sind verschlüsselt im Fingerprint-System gespeichert.
3.  Es werden keine persönlichen Daten gespeichert, sondern nur eine Benutzer-Nummer. Diese Nummer wird erst in **Kieselstein ERP** mit dem Mitarbeiter verknüpft. D.h. erst in der **Kieselstein ERP** Software ist eine Zuordnung zur Person möglich, so wie es auch bei anderen Systemen mit Kartenlesern oder ähnlichem der Fall ist.  
4.  Der Zeilensensor des Fingerprintleser hat einen Selbstreinigungs-Effekt. Durch das Ziehen des Fingers werden latente Fingerabdrücke verwischt und können nicht zur Vervielfältigung verwendet werden.
5.  Durch den Einsatz von Fingerprint können Betrugsversuche im Vorfeld verhindert werden, die in der Regel zur sofortigen Kündigung führen. Z.B. Stempelung für einen Kollegen.
6.  Kein Diebstahl oder Missbrauch möglich, da ohne Fingerprint kein Zutritt oder Stempelung möglich ist.
7.  Kein Vergessen oder Verlieren des Ausweises, der Karte.

Aus unserer Sicht ist die Biometrie mit z.B. einem Kartensystem gleich zu setzen. Die Karte hat eine Nummer, die in **Kieselstein ERP** dem Benutzer eindeutig zugeordnet werden kann, wie es auch bei der Biometrie der Fall ist. Es werden nur die Templates (Vektordaten) in der **Kieselstein ERP** Datenbank gespeichert. Der direkte Zugang zur **Kieselstein ERP** Datenbank ist über entsprechende Kennwörter durch Sie, den Anwender, zu schützen / geschützt.

Das bedeutet:<br>
Fingerprint muss für die Zeiterfassung aktuell durchgesetzt werden.<br>
Wenn diese Dinge gegeben sind, wende dich gerne an deine **Kieselstein ERP** Betreuer. Das KnowHow wie dies umgesetzt werden kann ist vorhanden.