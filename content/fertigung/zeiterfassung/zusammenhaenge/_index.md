---
title: "Zeiterfassung Zusammenhänge"
linkTitle: "Zusammenhänge"
categories: ["Zeiterfassung"]
tags: ["Zusammenhänge"]
weight: 720
description: >
  Zusammenhänge Mitarbeiterzeit und Maschinenzeit
---
Zusammenhänge Mitarbeiterzeit und Maschinenzeit
===============================================

In **Kieselstein ERP** finden Sie neben der klassischen Mitarbeiterzeiterfassung auch eine sogenannte Maschinenzeiterfassung. Diese wurde vor allem zur Realisierung der sogenannten Mehrmaschinenbedienung eingeführt.

Mehrmaschinenbedienung bedeutet, ein Mitarbeiter betreut abwechselnd mehrere Maschinen, also annährend gleichzeitig, da ja die Maschinen mit den jeweiligen CNC Programmen fast vollständig autonom arbeiten.

Doch wie hängen nun die einzelnen Zeiten der Ausdrucke von **Kieselstein ERP** zusammen?

Um einen Überblick über die produktiven Stunden eines Mitarbeiters zu erhalten, stehen Ihnen unterschiedliche Journale zur Verfügung. 

Hier befassen wir uns mit der Produktivitätstagesstatistik, der Produktivitätsstatistik und dem Maschinenerfolg.

Ein kurzer Überblick über die Zahlen die in dn Journalen verwendet werden:

**1** Anwesenheitszeitzeit (Kommt -- Pause / Pause-Geht)

**2** Ist-Zeit = gearbeitete Stunden auf Losen

**3** theoretische Sollzeit aus Rüst- und Stückzeit laut Arbeitsplan. 
   Die Rüstzeit wird nur für den Mitarbeiter gerechnet, der als erstes den Arbeitsgang beginnt. 
   Die Stückzeit ergibt sich aus (Gut + Schlechtstück) * Stückzeit laut Arbeitsplan

**4** innerhalb der Anwesenheitszeit (1) wurden die Stunden an Maschinen bewirkt

**5** Verhältnis von Maschinenlaufzeit (4)/ Gesamtanwesenheitszeit (1)

**6** Diese Zeit war der Mitarbeiter im Unternehmen ohne auf Lose angemeldet zu sein.

**7** Diese Zeit wurde vom Mitarbeiter auf als unproduktiv gekennzeichnete Lose gebucht.

![](produktivitaetsstatistik_summe_zahlen.PNG)

Grafik 1: Produktivitätsstatistik

![](produktivitaetstages_summe_zahlen.PNG)

![](produktivitaetstagesstatistik_monat_grafik_soll_ist_zahlen.PNG)

Grafik 2: Produktivitätstagesstatistik

![](Maschinenfaktor_zahlen.PNG)

Grafik 3: Maschinenerfolg

Betrachtet man nun die Grafiken genauer, so lassen diese einige Rückschlüsse zu:

**Grafik 1: Produktivitätsstatistik**

In der Produktivitätsstatistik erkennt man (6), dass die Person 37% der Zeit, die sie im Unternehmen anwesend war, nicht gebucht hat. Das bedeutet, dass die Aussagekräftigkeit der anderen Statistiken in Frage gestellt werden kann, da ein Großteil der Anwesenheitszeit keine Aufträge/Lose/etc. hinterlegt hat. Ebenso erkennen Sie in dieser Grafik die Aufteilung zwischen externen und internen Aufgaben. Je nach Mitarbeiter, ist hier abzuschätzen, ob das Verhältnis dem Soll entspricht. Beispielsweise ist bei Produktionsmitarbeitern das Ziel, dass der Anteil an Extern möglichst hoch ist, da dadurch die eingesetzte Arbeitszeit auch in Aufträge für Kunden fließt und so die Finanzierung sichergestellt ist. Bei Mitarbeitern in der Organisation hingegen, wird der interne Anteil höher sein, da diese für die internen Abläufe des Unternehmens verantwortlich sind.

**Grafik 2: Produktivitätstagesstatistik**

Detailiertere Auskunft über die einzelnen (Fertigungs-)aufträge bietet die Produktivitätstagesstatistik. Hier werden alle Sollzeiten mit den Istzeiten eines Tages verglichen (Gegenüberstellung). Hakt man Monatsbetrachtung an, so erfolgt auch eine Gesamtbetrachtung mit einer Grafik. Am ersten Blick erkennt man, dass die Person schneller gearbeitet hat, als die Sollzeiten vorgeben. Bitte bedenken Sie hier bei der Auswertung, dass der Grund hierfür einerseits natürlich die Schnelligkeit der Person, aber andererseits auch die Qualität der Sollzeiten sein kann. Um qualitativ hochwertige Daten zu erhalten überprüfen Sie die Arbeitspläne auf korrekte Vorgabezeiten und die inhaltlich richtige Buchung des Mitarbeiters. Weiters werden Lose, die als unproduktiv gekennzeichnet wurden, zusätzlich ausgewiesen.

**Grafik 3: Maschinenerfolg**

Die Auswertung über den Maschinenerfolg gibt Auskunft über das Verhältnis zwischen Mitarbeiteranwesenheitszeit und Maschinenlaufzeit. Neben der Information an welchen Tagen der Mitarbeiter anwesend war, erhalten Sie die Information welche Maschinen er mit welcher Laufzeit gestartet hat. Am Ende der Woche wird die Gesamtsumme der Anwesenheitszeit, die umme der verursachen Maschinenstunden und der Maschinenfaktor ausgegeben. Es werden beide Zeiten gegenübergestellt und der Faktor berechnet. 

In diesem Fall bedeutet das, dass die Person 160,6h im Unternehmen anwesend war und in dieser Zeit 250,5 Maschinenstunden bewirkt hat. Ziel ist es hier einen möglichst hohen Faktor zu erreichen, um durch zb.  Mehrmaschinenbedienung eine optimale Auslastung der Maschinen zu erzielen. Bitte achten Sie hier darauf, dass gerade wenn Maschinen in die sogenannte Geisterschicht hineinlaufen, dass der tatsächliche Maschinenstop und somit die tatsächliche Laufzeit der Maschine ganz wesentlich in die Maschinenlaufzeit und damit in den Erfolgsfaktor eingeht. Beachten Sie hierzu auch die Einstellung des "Auto Geht" beim jeweiligen Arbeitsgang.Fin