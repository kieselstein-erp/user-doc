---
title: "Quickzeiterfassung"
linkTitle: "Quickzeiterfassung"
categories: ["Quickzeiterfassung"]
tags: ["Quickzeiterfassung"]
weight: 700
description: >
  Schnelle Browser Zeiterfassung auf Projekte, Aufträge Lose für die Mitarbeiter:innen
---
Quickzeiterfassung
==================

In **Kieselstein ERP** steht mit der Quickzeiterfassung eine komfortable und schnelle Erfassungsmöglichkeit mittels Web-Browser zur Verfügung. Für die Bedeutung der verschiedenen Quickzeiterfassungslinks siehe bitte [WEB_Links]( {{< relref "web_links" >}} ).

Die Login Daten der Quickzeiterfassung sind wie die Benutzeranmeldedaten, welche Sie unter Benutzer in **Kieselstein ERP** definiert haben. Die Login Informationen werden in einem Cookie auf dem lokalen Rechner abgelegt, damit Sie komfortabel bei der nächsten Verwendung direkt zu Ihren Daten geführt werden.

Sollten andere Login Daten für den aktuell auf dem Rechner angemeldeten Benutzer definiert werden müssen, so verwenden Sie bitte den unter Quick-ZE neue Logindaten [WEB_Links]( {{< relref "/docs/stammdaten/system/web_links/" >}} ) QuickZE neue Login Daten angegebenen Link.

Erscheint die Meldung:

HTTP Status 400 - Es wurde kein Benutzername oder Kennwort angegeben. Bitte verwende http://?????cmd=quickze

so wurde bisher noch kein Benutzer definiert. Verwenden Sie bitte den folgenden [WEB_Links]( {{< relref "/docs/stammdaten/system/web_links/" >}} ) .

Nach Aufruf des richtigen Links erscheint:

![](Quickz1.gif)

geben Sie hier bitte den in **Kieselstein ERP** für Sie definierten Benutzernamen und das dazugehörige Passwort an. Entspricht Ihre bevorzugte Client-Sprache der Systemsprache kann das Feld Locale leer bleiben, ansonsten muss hier das gewünschte Client-Locale (z.B. deAT oder deDE) angegeben werden. Klicken Sie daran anschließend auf Login.

Nach gültiger Anmeldung erscheint die Quick-ZE Erfassungsmaske mit Ihrem Namen.

![](Quickz2.gif)

In der Erfassungsmaske sehen Sie vier Auswahltabellen, Sondertätigkeit, Kunden, offener Auftrag, Tätigkeit, die Möglichkeit eine Bemerkung zur Buchung einzugeben und den Status der letzten Buchung.

Zur Erfassung der Sondertätigkeit, z.B. Kommt oder Geht, machen Sie einen Doppelklick auf die jeweilige Sondertätigkeit oder Sie wählen die gewünschte Sondertätigkeit aus und klicken auf den Knopf Sondertätigkeit. Damit wird diese Buchung mit der aktuellen Zeit Ihres Rechners in Ihre Zeitbuchungen eingefügt.

**WICHTIG:** Achten Sie darauf, dass die Systemzeit Ihres Rechners aktuell ist.

Nach erfolgter Buchung wird das Browserfenster in den Hintergrund geschaltet um Ihnen zu signalisieren, dass die Buchung erfolgt ist.

Die Liste der angezeigten Sondertätigkeiten kann im **Kieselstein ERP** Client, Zeiterfassung, unterer Modulreiter Sondertätigkeiten, mit der Checkbox ![](Quickz3.gif) eingeschaltet werden.

Unter Kunde werden alle Kunden aufgelistet, bei denen noch Aufträge offen sind.

Unter offener Auftrag werden die Aufträge des ausgewählten Kunden gelistet.

Unter Tätigkeit werden die Tätigkeiten des Auftrags, sowie die Ihnen zugeordneten Tätigkeiten und ev. die default BDE-Tätigkeit gelistet.

Die Bedienung der Auftragsbuchungen erfolgt in der Form, dass Sie durch einen Einfachklick den Kunden auswählen. Dadurch wird die Liste der offenen Aufträge des Kunden aktualisiert. Nun wählen Sie durch einen Einfachklick den gewünschten Auftrag aus. Nun kann mit einen Doppelklick auf die nun durchgeführte Tätigkeit der Beginn der Tätigkeit gebucht werden. Alternativ kann wiederum die gewünschte Tätigkeit angeklickt werden und Sie klicken anschließend auf den Button Tätigkeit.
Wenn die Eingabe eines Kommentares / einer Beschreibung zur Tätigkeit erwünscht ist, so geben Sie dies bitte vor dem Doppelklick auf die Tätigkeit unter Bemerkung ein.

Bei gültiger Buchung wird in der Statuszeile die bestätigte Buchung angezeigt und gleichzeitig das Browserfenster in den Hintergrund gesetzt.

In der Quickzeiterfassung werden stornierte Projekte nicht angezeigt.

**Hinweis:** Beim Start der **Kieselstein ERP** Quickzeiterfassung wird diese möglichst Bildschirm füllend dargestellt um Ihnen eine entsprechende übersichtliche Datenerfassung zu ermöglichen.

Unter Tätigkeit werden drei verschiedene Bereiche angezeigt:

1. ![](Quickz4.gif)

Dies ist die Liste der im gewählten Beleg eingetragenen Arbeitszeitartikel.

![](Quickz10.jpg)

Hier sehen Sie bei den Auftragszeiten auch die geplanten Sollstunden und die bisher angearbeiteten Stunden auf dieser Tätigkeit.

2. ![](Quickz5.gif)

Dies sind die Ihnen zugeordneten Tätigkeiten. Diese werden im Person unter Verfügbarkeit definiert.

3. ![](Quickz6.gif)

Dies ist der Standard Arbeitszeitartikel. Dieser wird im System, Parameter, DEFAULT_ARBEITSZEITARTIKEL definiert.

Damit steht ihnen eine breite Palette an Buchungsmöglichkeiten zur Verfügung.

**Hinweis:**

Die in der Quickzeiterfassung angezeigten Texte können zusätzlich übersteuert werden. Es sind dafür folgende Properties zu definieren:
| Property |
| --- |
| quickze.sondertaetigkeit |
| quickze.offenerauftrag |
| quickze.taetigkeit |
| quickze.kunde |
| quickze.bemerkung |
| quickze.auftrag |

Ist keine Anmeldung möglich, [siehe bitte]( {{< relref "/docs/stammdaten/system/web_links/#fehler-bei-der-anmeldung" >}} )  

**Hinweis:**<br>
Die Quickzeiterfassung legt die Daten der Anmeldung in einem Cookie ab. Daraus ergibt sich, dass immer nur ein Benutzer je Client-Rechner und Anmeldung definiert werden sollte, was exakt der Praxis entspricht. Die Anmeldung verschiedener **Kieselstein ERP**-Benutzer unter dem gleichen Rechner-Login ist in der Quickzeiterfassung nicht vorgesehen. Sollten mehrere **Kieselstein ERP** Benutzer auf einem Rechner erforderlich sein, so muss auch der am Rechner angemeldete Benutzer gewechselt werden.

Zum Löschen von Cookies [siehe]( {{< relref "/docs/stammdaten/system/web_links/#l%c3%b6schen-von-cookies" >}} )

#### Kann die Quickzeiterfassung auch auf meinem Handy, BlackBerry, mobilen PDA verwendet werden?
Grundsätzlich Ja, aber wir haben die mobile App erstellt. [Siehe dort]( {{< relref "/docs/installation/05_KES_App" >}} )

Voraussetzung dafür ist, dass Ihr **Kieselstein ERP** Server von außen erreichbar ist, also der Port 8080 direkt auf den **Kieselstein ERP** Server durchgeroutet wird. Bitte beachten Sie die Sicherheitserfordernisse um gegen Angriffe aus dem Netz entsprechend abgesichert zu sein.

Für die Bedienung ohne Doppelklick haben wir für die beiden Buchungsmöglichkeiten jeweils einen Button geschaffen.

D.h. wählen Sie in der Quickzeiterfassung in Ihrem Handy-Browser die gewünschte Buchung wie oben beschrieben aus und tippen Sie dann auf den Button Sondertätigkeit ![](Quickz8.gif) um Kommt / Geht usw. 

zu buchen bzw. auf den Button ![](Quickz7.gif) um die gewünschte Tätigkeit zu buchen.

Bitte beachte, dass das **Kieselstein ERP** Zeiterfassungs"Terminal" auch auf Android lauffähig ist.

**ACHTUNG:**

Um von außen eine Verbindung zu Ihrem **Kieselstein ERP** Server herstellen zu können muss der HTTP-Port (8080) durch Ihre Firewall geroutet werden. Stellen Sie unbedingt sicher, dass die JBoss Console abgeschaltet ist. D.h. diese darf in Ihrem **Kieselstein ERP** Deploy Verzeichnis nicht eingetragen sein.

#### Die Quickzeiterfassung liefert beim Start immer Fehler
Oft liegt das an einer falsch eingegebenen URL. Haben Sie z.B. direkt aus dem WebLink die Url kopiert und sofort die Abfrage gestartet, so wird logischerweise der angegebene Server nicht gefunden. Die aktuellen Browser versuchen daher die richtige URL zu erraten und ändern den Link entsprechend ab, z.B. auf <http://www.kieselstein_erp_server.com:8080/ze/ze?cmd=quickze>. Wird nun nur der Servername korrigiert, so gibt es diesen trotzdem nicht. D.h. es muss darauf geachtet werden, dass nur <http://DerNameIhresKieselsteinERPServers:8080/ze/ze?cmd=quickze> eingegeben wird und die automatischen Korrekturen die viele Browser vornehmen ignoriert werden.

#### Beim Start der Quickzeiterfassung kommt ein Fehler
Wenn mehrere Benutzer unter dem gleichen Account auf einem Rechner arbeiten, oder auch wenn z.B. Benutzer gelöscht bzw. geändert wurden, so kann es zu folgender Fehlermeldung kommen:

F: Fataler Fehler

com.lp.util.EJBExceptionLP: javax.persistence.NoResultException: No entity found for query at com.lp.server.benutzer.ejbfac.BenutzerFacBean.benutzerFindByCBenutzerkennung(BenutzerFacBean.java:602) at

Um diesen zu beheben melden Sie sich bitte mit http://KIESELSTEIN-ERP_SERVER:8080/ze/ze?cmd=quickze&reenter=true erneut an um die alten Logindaten zu löschen.

#### Welche Benutzerrechte werden für die Quickzeiterfassung benötigt?
In der Quickzeiterfassung greifen die Rechte der jeweils angemeldeten Benutzer. Bitte nicht mit der Rolle QuickZE verwechseln. D.h. um alle Funktionen nutzen zu können benötigen Sie folgende Rechte:
- ANGB_ANGEBOT_R
- AUFT_AUFTRAG_R
- FERT_LOS_R
- FERT_LOS_DARF_ABLIEFERN
- PERS_ZEITERFASSUNG_CUD
- PERS_ZEITERFASSUNG_R
- PROJ_PROJEKT_R
- PERS_DARF_KOMMT_GEHT_AENDERN
- LP_DARF_EMAIL_SENDEN
- PERS_ANWESENHEITSLISTE_R

#### Wie sieht nun die Quickzeiterfassung aus?
Je nach aktivierten Modulen und Benutzerrechten kann die Erfassungsoberfläche der Quickzeiterfassung z.B. wie folgt aussehen:
![](Quickz9.jpg)