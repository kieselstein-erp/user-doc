---
title: "Zeitserver"
linkTitle: "Zeitserver"
categories: ["Zeitserver"]
tags: ["Zeitserver"]
weight: 230
description: >
  Zeitserver
---
Einrichten von Zeitservern
==========

Gerade in der Zeiterfassung, aber auch für das Zutrittskontrollsystem ist eine exakte amtliche Zeit wesentlich. Stelle daher sicher, dass alle beteiligten Komponenten Zeitsynchron sind.

Beteiligte Komponenten sind:
- **Kieselstein ERP** Server
- **Kieselstein ERP** Clients, also alle Rechner auf denen dein **Kieselstein ERP** ausgeführt wird
- **Kieselstein ERP** Stationen
- die verschiedenen Zeiterfassungsterminals, egal ob mobil oder fest installiert
- die Zutrittskontroller

Üblicherweise wird der **Kieselstein ERP** Server auf die Zeit eines Atomzeit-Server im Internet synchronisiert.

Alle anderen Netzwerkkomponenten werden auf diesen Server synchronisiert.

Eine Auswahl der verfügbaren Zeitserver findet man unter <http://www.pool.ntp.org/zone/europe> oder unter <http://timeserver.verschdl.de/>

Wir verwenden üblicherweise:

| Server | IP (V4) |
| --- |  --- |
| ntp0.fau.de (ntp0-rz.rrze.uni-erlangen.de) | 131.188.3.220 |
| ntp1.fau.de (ntp1-rz.rrze.uni-erlangen.de) | 131.188.3.221 |
| ntp2.fau.de (ntp2-rz.rrze.uni-erlangen.de) | 131.188.3.222 |
| ntp3.fau.de (ntp3-rz.rrze.uni-erlangen.de) | 131.188.3.223 |
| ptbtime1.ptb.de | 192.53.103.108 |
| ptbtime2.ptb.de | 192.53.103.104 |

Synchronisierung der Server mit den Atomzeitservern

## Windows11
- Einstellungen
- Zeit und Sprache
- Zusätzliche Uhren
- Nun auf den Reiter Internetzeit klicken<br>
![](Win11_Internetzeit.png)
- Einstellungen ändern
- einen der obigen Server eintragen und auf Jetzt aktualisieren klicken
![](Win11_Internetzeitserver_eintragen.png)

## Windows10
- Einstellungen
- Zeit und Sprache
- Uhren für unterschiedliche Zeitzonen hinzufügen
- Nun auf den Reiter Internetzeit klicken<br>
![](Win11_Internetzeit.png)
- Einstellungen ändern
- einen der obigen Server eintragen und auf Jetzt aktualisieren klicken
![](Win11_Internetzeitserver_eintragen.png)

## Zeitserverdienst unter Windows zur Verfügung stellen
Idealerweise gibt es in deinem gesamten Unternehmen **<u>einen</u>** Zeitserver (z.B. den Domainencontroller), gegen den alle anderen Rechner synchronisieren. Nur damit erreichst du, dass alle wirklich die gleiche Uhrzeit haben.<br>
### Hinweis:
Gerade wenn in den Zeitmodellen Rundung zu Gunsten des Unternehmens verwendet wird, ist es enorm wichtig, dass die amtliche Zeit verwendet wird, denn wenn aufgrund einer falschen Uhrzeit den Mitarbeitern Zeit abgezogen wird, ist das mehr als peinlich und natürlich haben die Mitarbeiter dafür dann kein Verständnis mehr.

## Linux
Eventuell muss der NTP Dienst zuerst installiert werden. In unseren Linux-Installationen ist dieser bereits eingerichtet.
Installation mit 
> yum install ntp<br>
> chkconfig --levels 235 ntpd on

Bei bereits laufendem ntp Dienst:<br>
> service ntpd stop<br>
> ntpdate IP-Adresse ... verwende hier die Adresse eines erreichbaren NTP Servers<br>
> service ntpd start<br>

Ist der Zeitdienst (ntpd) wirklich synchron ?<br>
ntpq -p

## MAC OS X
Öffnen die Systemeinstellungen und klicke auf Datum und Uhrzeit.<br>
Wähle den Reiter Datum & Uhrzeit und setze einen Haken bei Datum & Uhrzeit automatisch einstellen. Wähle einen Server aus der Liste aus oder gib den Namen eines NTP Server, z.B. aus obiger Liste, an. Siehe dazu auch: <http://docs.info.apple.com/article.html?artnum=61273>

Siehe dazu auch: http://www.msxfaq.de/verschiedenes/timesync.htm