---
title: "Erster Einsatz"
linkTitle: "Erster Einsatz"
categories: ["Zeiterfassung"]
tags: ["Erster Einsatz"]
weight: 700
description: >
  Vorgehensweise zum Ersteinsatz der Zeiterfassung
---
Vorgehensweise zum Ersteinsatz der Zeiterfassung
================================================

Da von vielen Anwendern immer wieder ein kleines Kochbuch gewünscht wird, wie beim Ersteinsatz der **Kieselstein ERP** Zeiterfassung vorgegangen werden sollte, hier eine Aufstellung der erforderlichen Schritte in der unserer Meinung nach erforderlichen Reihenfolge.

Voraussetzung ist sicherlich, dass Sie sich zuvor mit der grundsätzlichen Bedienung von **Kieselstein ERP** vertraut gemacht haben.

-   Grunddaten / Basiseinstellungen

    -   Die Zeitmodelle müssen vollständig definiert sein
        D.h. alle Tage / Tagesarten und gegebenenfalls die fixen Pausen müssen für jedes Zeitmodell definiert sein

    -   Die Feiertage (Betriebskalender) müssen definiert sein.

    -   Der Kollektiv / Tarifvertrag muss definiert sein

    -   Personengruppen wenn detailliertere Kostenbetrachtungen gewünscht werden

-   Was muss für die Personen definiert sein

    -   Grunddaten, damit die Personen / Mitarbeiter buchen können

        -   Es muss die Person angelegt sein

        -   Die Person muss die richtige Ausweisnummer eingetragen haben

    -   Für die Abrechnung

        -   Kollektiv/Tarifvertrag, wenn zutreffend

        -   Religionszugehörigkeit, wenn es religionsspezifische Feiertage gibt

        -   Eintrittsdatum

        -   Urlaubsanspruch

        -   Zeitmodell (bitte immer mit 1\. des Monats beginnend)

        -   Für die Reisezeitabrechnung

            -   km-Geld 1&2

        -   Personengruppe (wenn zutreffend)


### Wie soll ich bei der Einführung der Zeiterfassung vorgehen, die alten Daten ins **Kieselstein ERP** bringen?

Bei der Einführung von **Kieselstein ERP** stellt sich die Frage wie geht man mit den alten Ansprüchen der Mitarbeiter um. Dies sind vor allem die Urlaubsansprüche und die Gleitzeit- / Stundenguthaben.

Gehen Sie dazu wie folgt vor:

Definieren Sie einen Stichtag ab dem die in **Kieselstein ERP** gebuchten Daten echt verwendet werden, also keine Testdaten mehr sind.

Dieser Stichtag könnte z.B. der 1.Oktober sein.

Ermitteln Sie die Ansprüche im alten System zu diesem Stichtag.

Hinweis: Dies Ansprüche können auch nachgetragen werden. Ein Zeitdruck besteht nur in Hinsicht auf die nächste Monatsabrechnung, in unserem Beispiel also zum 31.10..

**a.) Eingabe des Gleitzeit- / Überstundenanspruchs zum Stichtag:**

Tragen Sie nun die Anspruchsstunden unter Gleitzeitsaldo ein. Als Datum wählen Sie das Vormonat. Eventuell durch bisherige Tests eingetragene Stunden setzen Sie auf 0,00\. Haken Sie nach der Erfassung der richtigen Salden das Gesperrt (Eingefroren) an. Damit wird sichergestellt, dass von **Kieselstein ERP** diese Daten nicht mehr durch die Monatsabrechnung verändert werden.

**b.) Eingabe des Urlaubsanspruchs:**

Stellen Sie dazu sicher, dass vor dem Stichtag KEINE Urlaubsansprüche in **Kieselstein ERP** eingetragen sind.

Am Einfachsten überprüfen Sie dies im Reiter Sonderzeiten in der Zeiterfassung.

Stellen Sie sicher, dass wenn der Mitarbeiter während des laufenden Jahres eingetreten ist, sein Zeitmodell ab diesem Zeitpunkt richtig definiert ist. Ist ein Mitarbeiter vor dem laufenden Jahr eingetreten, so muss sein Zeitmodell zumindest ab dem 1.1\. des laufenden Jahres definiert sein.

Das Eintrittsdatum des Mitarbeiters sollte seinem tatsächlichen (letzten) Eintritt in Ihr Unternehmen entsprechen.

Tragen Sie die Anspruchswochen für das gesamte Kalenderjahr ein. In Österreich in der Regel 5Wochen für Deutschland / die Schweiz nutzen Sie bitte die Eingabe Jahresanspruch in Tagen.

Stellen Sie Tage zusätzlich und Stunden zusätzlich auf 0.

Drucken Sie nun eine Monatsabrechnung zum Stichtag (1.Oktober, also Ende September) für den entsprechenden Mitarbeiter aus.

Die Differenz der Anspruchstage (also die Differenz der Anspruchstage die **Kieselstein ERP** berechnet zu den laut Ihren Aufzeichnungen tatsächlichen Tagen) tragen Sie unter Tage zusätzlich ein.

Wenn Sie Ihre Urlaubsabrechnung auch in Stunden geführt haben, gehen Sie analog zu den Tagen vor.

Wenn die Urlaubsabrechnung nun auch auf Stunden erweitert werden sollte, so müssen die "Tage zusätzlich" auf Stunden zusätzlich umgerechnet werden. Ermitteln Sie dazu die entsprechenden Stunden. Oft kann hier der Faktor von Ermittelter Anspruch lt. ZM in Tagen zu Ermittelter Anspruch lt. ZM in Stunden verwendet werden.

Rechnen Sie nun diese Differenz der Tage auf die entsprechenden Urlaubsstunden um.