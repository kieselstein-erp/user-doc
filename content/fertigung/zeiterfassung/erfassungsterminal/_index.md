---
title: "Erfassungsterminal"
linkTitle: "Erfassungstermina"
categories: ["Zeiterfassung"]
tags: ["Erfassungsterminal"]
weight: 9000
description: >
  Konzept eines Erfassungsterminals mit eingebundenen Messwerten u.ä.
---
Erfassungsterminal
==================

{{% alert title="Konzeptbeschreibung" color="info" %}}
Dies ist nur eine Konzeptbeschreibung für eine weitere Terminalvariante. Das Terminal selbst steht in **Kieselstein ERP** derzeit nicht zur Verfügung.
{{% /alert %}}

In Zusammenhang mit den Prüfplänen der Stückliste haben wir auch ein Erfassungsterminal geschaffen.
Dies dient vor allem der Fertigungssteuerung auf Basis der Fertigungsgruppen und der Maschinengruppen.
Es werden die "In-Produktion" Lose angezeigt und deren Arbeitsgänge gebucht und die Ablieferungen mit und ohne Prüfergebnisse erfasst.
Die Bedienung ist, wie bei unseren Terminals üblich, auf Touchbildschirme ausgelegt.
Das Erfassungsterminal ist eine reine Java-Applikation und kommuniziert mit dem **Kieselstein ERP** Server über Rest-API. Das bedeutet einerseits, dass es auf vielen verschiedenen Betriebssystemen zum Einsatz kommen kann und dass andererseits auf Ihrem **Kieselstein ERP** Server die Webservices eingerichtet sein müssen.

Allgemeines

Nachfolgend eine Beschreibung der Bedienung des Erfassungsterminals.
Das Erfassungsterminal ist auf die Mitarbeiter-Identifizierung mittels RFID-Karten, in der Regel Mifare, ausgelegt. Damit wird eine optimale Steuerung und Identifizierung der Benutzer erreicht. Die Zuordnung geschieht über die Ausweisnummer. D.h. es muss die Seriennummer der dem Mitarbeiter übergebene RFID Karte (aufgrund der Bauform des verwendeten Leser sind hier wirklich Karten erforderlich) unter Ausweis beim Mitarbeiter eingetragen sein.
Für die Ermittlung der Seriennummer der Karte halten Sie diese für den RFID Leser. Sie erhalten eine Fehlermeldung dass Karte Nr. 1234567890AB nicht zugeordnet werden kann. Tragen Sie die angezeigte Nummer beim entsprechenden Mitarbeiter unter Ausweis ein.

Anmeldemaske

Mit dem Start des Terminals erscheint folgende Anmeldemaske:
![](anmeldemaske_fertigung.PNG)

Der Bildschirm des Erfassungsterminals ist in mehrere Bereiche geteilt. Im Infobereich (rechts oben) sind generell geltende Daten abgebildet, wie z.B. konfigurierter Mandant, Stationsname, aktuelle Uhrzeit. Der kontextbezogene Bereich wird abhängig von der jeweils gerade gezeigten Maske individuell befüllt. Im Navigationsbereich ist die Navigation zu den einzelnen Masken abgebildet. In der Statuszeile werden bei Bedarf Meldungen des Terminals angezeigt.

Für die [Konfiguration](#Einrichten) des Terminals tippen Sie bitte auf das **Kieselstein ERP** Icon links oben.

## Anmeldung
Die Anmeldung erfolgt über die Ausweisnummer. Ist die Person bekannt und berechtigt wird die nächste Maske eingeblendet und der Name der angemeldeten Person mit der Zeitbuchungsfunktionalität unterhalb des Terminalinfobereichs angezeigt.
Bei Abmeldung wird wieder auf die Anmeldemaske umgeschaltet.

## RFID-Karte
Die mit der Ausweisnummer programmierte RFID-Karte ist in das angeschlossene und konfigurierte RFID-Lesegerät zu legen. Damit wird automatisch angemeldet. Die Abmeldung erfolgt durch Herausziehen der Karte.

## Barcode
Mit dem angeschlossenen und konfigurierten Barcodescanner wird die Ausweisnummer gescannt und angemeldet.<br>
Diese Funktionalität ist aktuell nur in der Konfiguration Kommissionierterminal verfügbar. Eine Abmeldung erfolgt durch Zurückkehren zur Anmeldemaske (über die Zurück-Buttons)

## Personalfunktionen
![](personalfunktionen.PNG)
Unterhalb der Terminalinformation sind die Daten der gerade angemeldeten Person. Dazu können die gängigen Zeitbuchungen durchgeführt, sowie der aktuelle Zeitsaldo angezeigt, werden.

## Erfassungsterminal
Nach der Anmeldung werden Sie, abhängig von der konfigurierten Fertigungsgruppe in die Los-Auswahlliste geführt.

### Los-Auswahlliste
Die Basismaske des Erfassungsterminals ist die Los-Auswahlliste.
![](et_losauswahlliste.jpg)
In dieser Liste finden sich jene Lose, die sich in Produktion befinden (Losstatus "In Produktion" oder "Teilerledigt") und dessen aktueller Arbeitsgang auf einer Maschine in der konfigurierten Maschinengruppe bearbeitet wird. Als Losinformation werden in der Tabelle die Losnummer, die Artikelnummer der zugrundeliegenden Stückliste, die Artikelbezeichnung, die noch offene Menge, der Los-Endetermin, und die Inventarnummer der Maschine auf dem der aktuelle Arbeitsgang läuft, angegeben. Die Default-Sortierung ist aufsteigend nach Losnummer. Möglich ist außerdem nach der über den HELIUM Client festgelegten Priorität/Reihung der der offenen Arbeitsgänge, siehe Konfiguration.

Mit Tip auf die jeweilige Los-Zeile wird in die Detailansicht des gewählten Loses gewechselt.

### Los-Steuerung
![](et_lossteuerung.PNG)

In dieser Maske sind die Detailinformationen über das aktuell bearbeitete Los zu sehen. Der obere Bereich mit Losnummer, Losstatus, Artikelnummer, Artikelbezeichnung, Los-Beginntermin, Los-Endetermin bleibt als Übersicht auf allen weiterführenden Masken bestehen.

Weiters sind folgende Informationen angeführt:
-   Losgröße
-   abgelieferte Menge
-   offene Menge
-   Fertigungssatzgröße
-   definierte Bündelmenge (Verpackungsmenge des Artikels)
-   definierte Kistenmenge (Verpackungsmittelmenge des Artikels)
-   berechnete Bündelanzahl (Losgröße / Bündelmenge)
-   berechnete Kistenanzahl (Losgröße / Kistenmenge)
-   Arbeitsgangbezeichnung
-   Maschine auf der der Arbeitsgang läuft
-   eventuell werden noch Angabe zu Zwischenprüfungen und Schliffbilder angezeigt

Start-Button

Mit Tipp auf den Start-Button ![](et_start.gif) wird der Arbeitsgang gestartet. Das bedeutet, dass in der Zeiterfassung ein Beginneintrag für die Maschine des Arbeitsgang (Maschine-Start) und für die aktuell angemeldete Person (auf die Tätigkeit des Arbeitsgangs) erstellt werden. War die Maschine zuvor im gestarteten Status, wird sie beendet und anschließend wieder mit den aktuellen Daten (Los, Personal) gestartet.

Drucker-Icon-Button

Durch Tipp auf das Drucker Symbol ![](et_druck.gif) öffnet sich der Druckauswahl-Dialog.
Die Auswahlmöglichkeiten richtet sich nach den definierten Druckernamen im Konfigurationsmenü des Terminals. Ist ein Druckername nicht hinterlegt, wird auch der entsprechende Druck nicht angezeigt. Für die Auswahlmöglichkeiten in dieser Druckauswahl werden folgende Druckerdefinitionen vorausgesetzt:
-   Fertigungsschein → Drucker
-   Verpackungsetikett → Etikettendrucker
-   Bündeletikett → Bündeletikettendrucker<br>
(Zusätzlich zum Druckernamen muss auch die Reportvariante dazu in der Konfiguration hinterlegt sein)
-   E-... Verpackungsetikett → E-... Verpackungsetikett Drucker 1 und Drucker 2
    (Zusätzlich zum Drucker 1 und/oder Drucker 2 Namen auch die Reportvariante dazu in der Konfiguration hinterlegt sein)

![](et_Druckerauswahl.gif)

Nach Klick auf den gewünschten Druck erscheint eine Meldung, dass die Anfrage erfolgreich dem Server übermittelt wurde.
 ![](et_lossteuerung_druck_uebermittelt.PNG)

Da die Aufbereitung des Druckes etwas Zeit in Anspruch nehmen kann, wird der angeforderte Druck nicht sofort am Drucker bearbeitet. Wurde der angeforderte Druck vom Server am Terminal empfangen und an den Drucker gesendet, erscheint folgende Meldung:
![](et_lossteuerung_druck_gesendet.PNG)

Beim Klick auf den gewünschten Druck wird geprüft, ob der Drucker überhaupt erreichbar ist. Falls z.B. der Druckername nicht korrekt eingetragen wurde erscheint folgende Meldung.
![](et_lossteuerung_drucker_nicht_gefunden.PNG)
Bitte achten Sie darauf, dass der Druckername exakt dem Namen des Druckers im jeweiligen Betriebssystem entsprechen muss. Gegebenenfalls kopieren Sie diesen aus der Druckerkonfiguration des Betriebssystems.

E-... Verpackungsetikettdruck

Für den E-... Verpackungsdruck können 2 Drucker sowie die zu verwendende Reportvariante in der Konfiguration hinterlegt werden. Sind alle diese 3 Parameter definiert, so hat man für diesen Druck die Möglichkeit aus diesen beiden Druckern zu wählen. Genaueres siehe dazu weiter unten "Reiter Fertigung" des Konfigurationsdialogs.

![](et_lossteuerung_druck_everpackungsetitkett.PNG)

Nach Tipp auf einen Drucker kommt ein weiteres Fenster in dem, nach Tipp auf den entsprechenden Button, die Menge und die Anzahl der Exemplare eingegeben werden können. Sind zu beiden Parametern Werte definiert worden, erscheint darunter das Drucksymbol. Mit Tipp darauf wird der eigentliche Druck ausgelöst.

![](et_lossteuerung_druckauswahl_eingabe.PNG)

Maschinen-Icon-Buttons

![](et_lossteuerung_maschinenbuttons.PNG)
Über den Button "Maschinen-Stopp" erhält man eine Liste aller Maschinen, die die angemeldete Person selbst gestartet hat. Mit Klick auf die Zeile kann die Maschine gestoppt werden.

![](et_maschinenstopp.PNG)

Mit dem Button "Maschinen" kann der gewählte Arbeitsgang auf eine der hinterlegten alternativen Maschinen zugewiesen werden.
Info: Diese alternativen Maschinen können im Modul Stückliste, Arbeitsgang entsprechend definiert werden. Um diese zusätzliche Funktion freizuschalten wenden Sie sich bitte vertrauensvoll an Ihren **Kieselstein ERP** Betreuer.
Die beim jeweiligen Arbeitsgang hinterlegten Alternativ-Maschinen werden hier angezeigt. Mit Klick auf eine Maschine wird -- nach Nachfrage -- auf diese gewechselt. Die vorher aktive Maschine wird gestoppt.
![](et_maschine_agverschieben.PNG)

### Material und Arbeitsplan
Über die Navigation aus der Los-Steuerung kann man in die Material-Übersicht wechseln. Hier werden die Sollmaterialen des Loses angezeigt. Ebenso kann man in eine Übersicht des Arbeitsplans des Loses wechseln.

Scan der Materialien über Barcode

Bei aktiviertem Barcodescan in der Materialliste (Konfiguration -> Reiter Fertigung) können die Materialien über die Artikelnummer als Barcode abgescant werden. Dabei wird eine zusätzliche Spalte "Scan" angezeigt, die mit einem Haken signalisiert, ob der Artikel bereits gescant wurde. Das farbige Rechteck rechts neben dem textlichen Status des Loses, zeigt den Status der Scans dieses Loses an. Ist es rot, wurde noch kein Artikel gescant, ist es orange (so wie im folgenden Screenshot) wurden Artikel bereits gescant, aber noch nicht alle. Ist es grün, wurden alle Artikel gescant und alle Scan-Haken sind gesetzt. Scans können mit einem Klick auf die Zeile zurückgenommen werden, jedoch nur alleine durch den Barcodescan gesetzt werden.

Die Materialien können über einen Klick auf die Spaltenüberschrift beliebig sortiert werden.

![](et_materialscan_teilerledigt.PNG)

Ist in der Konfiguration zusätzlich ein Verzeichnis definiert, wird dieses dazu verwendet, um die Scans zu speichern. Das ermöglicht bei einem Wechsel des Loses, dass die bereits getätigten Scans wieder geladen werden können. Achten Sie dabei, dass das Terminal Lese- und Schreibrechte auf das definierte Verzeichnis hat.
Theoretisch könnte man ein Verzeichnis gewählt werden, auf das mehrere Erfassungsterminals Zugriff haben. Dabei könnten die Scans derselben Lose geladen und gespeichert werden. Achtung: Derzeit ist aber auf keine parallele Bearbeitung eines Loses Rücksicht genommen worden. D.h. scannen Sie auf 2 Terminals die Materialien desselben Loses ab, wird der Zustand immer jenes Terminals gespeichert, das zuletzt einen Scan hinzugefügt oder gelöscht hat.

Ist kein Speicherverzeichnis in der Konfiguration definiert worden, so gilt der Scanstatus solange man sich in dem Los befindet.

### Einrichtedaten eingeben
Befindet sich ein Los im Status "In Produktion", wurden noch keine Mengen abgeliefert. In diesem Fall wird in der Navigation der Los-Steuerung der Button "Einrichtedaten eingeben" angezeigt.
![](et_lossteuerung_navi_in_produktion.PNG)
Mit Klick darauf wird auf eine Maske gewechselt in der man die Gut- und Schlechtmengen eingegeben kann, die für das Einrichten der Maschine verbraucht wurden. Bei der später folgenden Ablieferung wird die Schlechtmenge ins definierte Schrottlager gebucht. Die Losgröße wird dabei um diese Menge erhöht.

![](et_gut_schlecht.PNG)
Bei der Eingabe der Gut- und Schlechtmenge muss auf den jeweiligen Button geklickt werden.
Danach erscheint ein Eingabefeld mit der man die Mengen eingeben kann. Durch die Umrandung des Buttons und dem Text oberhalb des Eingabefelds kann man erkennen, welchen Wert man gerade bearbeitet. Die "Enter"-Taste übernimmt den Wert in das entsprechende Feld, welches nun in Grün angezeigt wird. Mit dem "X"-Button des Eingabefelds wird der Wert des gewählten Feldes gelöscht. Ist Gut- und/oder Schlechtmenge definiert ist ein Weitergehen in die nächste Maske ("Weiter"-Button grün) möglich. Die gesamte Abliefermenge ist dabei die Summe der Gut- und Schlechtmenge.

### Abliefern (mit Prüfen)
Befindet sich das Los im Status "Teilerledigt", sind bereits Mengen abgeliefert worden. In diesem Fall wird in der Navigation der Los-Steuerung der Button "Abliefern (mit Prüfen)" angezeigt.
![](et_abliefermenge.PNG)

Mit Klick darauf kann die Abliefermenge mit dem bekannten Eingabefeld definiert werden. Bei Betätigung des "Enter"-Buttons des Eingabefelds wird der Wert übernommen und in die nächste Maske gewechselt.

### Prüfplan
Nachdem entweder die Gut-/Schlechtmenge oder die Abliefermenge definiert wurde, wird in die Maske des Prüfplans gewechselt. Hier werden die Prüfkombinationen angezeigt, die im aktuellen Los definiert sind.
![](et_pruefplan_abliefern.PNG)
In der Tabelle ist ersichtlich was geprüft werden muss und ob die Prüfung bereits durchgeführt (Erledigt) wurde und ob alle Messwerte innerhalb der definierten Toleranzen liegen (Ok).

### Prüfdatenerfassung
Mit Klick auf einen Eintrag des Prüfplans wechselt man zur Erfassung der Prüfwerte.
![](et_pruefplan_erfassung2.PNG)

In dieser Maske werden die Daten der Prüfkombination angezeigt.
Für die Eingabe der Werte tippen Sie auf den Button mit der Bezeichnung des einzugebenen Prüfwerts. Nach Bestätigung über den "Enter"-Button wird der Wert übernommen. Der Prüfbutton wird dabei rot eingefärbt, wenn der Messwert außerhalb des Toleranzbereichs liegt und grün, wenn innerhalb. Ist bei einer Prüfkombination ein Prüfwert unerheblich, so wird dieser bereits mit grün eingefärbt und mit Eingabewert 0,00 vorbelegt.

Sind alle Prüfwerte erfasst -- bei einer Sichtprüfung ist diese lediglich zu bestätigen -- erscheint der "Fertig"-Button in grün. Mit Klick darauf werden die Werte im Terminal zwischengespeichert und in die Maske des Prüfplans zurückgekehrt.

### Ablieferung
Sind alle Prüfkombinationen des Prüfplans erfasst und erledigt worden (und bei einer Freigabeprüfung auch alle innerhalb des Toleranzbereichs) kann die Ablieferung durchgeführt werden. Dazu erscheint in der Maske des Prüfplans im rechten Bereich der "Abliefern"-Button.

### Abliefern nach Freigabeprüfung
Wenn nur eine Schlechtmenge abgeliefert werden soll, so werden keine Daten im Prüfplan - auch wenn sie vorhanden wären - angezeigt. Man kann diese Menge sofort über den (rot hinterlegten) "Abliefern"-Button abliefern. Schlechtmengen werden immer ins definierte Schrottlager gebucht.
Eine eingegebene Gutmenge wird in das Einrichtelager abgeliefert, das mit Parameter EINRICHTELAGER_ERFASSUNGSTERMINAL definiert werden muss. Ist dies nicht definiert, wird die Gutmenge ins Los-Ziellager gebucht.

### Abliefern nach Ablieferprüfung
Sind erfasste Prüfwerte nicht im Toleranzbereich, so wird der "Abliefern"-Button zwar angezeigt, aber mit roten Hintergrund. Diese Ablieferung wird ins Schrottlager gebucht. Sind alle Werte ok (siehe Spalte im Prüfplan) ist die Button-Hintergrundfarbe grün und die Ablieferung landet im Los-Ziellager. Erfolgt eine Überlieferung des Loses, wird die Losgröße mit der Summe der Abliefermengen gleichgezogen.

Nach der Ablieferung wird in die Los-Steuerung zurückgewechselt.

## Kommissionierterminal

### Kommissionierziele
Nach Anmeldung erhält man eine Übersicht aller verfügbaren Kommissionierziele. Dabei gibt es zwei verschiedene Typen. Unter Typ "Linie" befinden sich alle Produkte mit Linienabrufen, unter Typ "NoKa" alle Produkte, die als NoKa-Artikel definiert sind. Dazu gibt es die Information der Lieferadresse und deren Kurzbezeichnung. Basis aller Daten am Kommissionierterminal sind die lieferbaren Produkte (Forecastpositionen) aus den Forecastaufträgen.
![](kt_kommissionierziele.PNG)

### Lieferbare Produkte
Als lieferbare Produkte, die am Terminal zum Kommissionieren angezeigt werden, gelten:
-   Forecastpositionen mit Status "ANGELEGT", die sich in einem Forecastauftrag mit Status "FREIGEGEBEN" und Forecast mit Status "ANGELEGT" befinden und entweder
    1.  Linienabrufe haben
    2.  als NoKa-Artikel (Reiter Sonstiges im Artikel) definiert sind
-   Forecastpositionen mit Status "ANGELEGT", "IN PRODUKTION" oder "TEILERLEDIGT", aus Forecastaufträgen mit Status "FREIGEGEBEN" oder "ERLEDIGT", die bereits mit einem Los verbunden sind und der Losstatus "In Produktion" oder "Teilerledigt" ist. In diesem Fall wurde das Produkt noch nicht (fertig) kommissioniert

Die Verbindung der Forcastposition zu einem Los wird zu dem Zeitpunkt durchgeführt, sobald ein Kommissionierer am Terminal ein Produkt für die Kommissionierung auswählt. Details siehe unten.

Es ist weiter dabei zu beachten, dass Linie immer vor NoKa geht. D.h. ist ein Produkt (Forecastposition) als NoKa definiert und hat Linienabrufe, so werden für die Kommissionierung immer nur die Daten aus den Linienabrufen herangezogen. Sind in einem Forecastauftrag, Forecastpositionen vorhanden, die NoKa-Artikel sind und welche, die Linienabrufe haben, so werden diese wie vorher beschrieben gesplittet unter dem entsprechenden Typ am Terminal angezeigt.

### **Kommissionierware <-> Lagerware**

Bei den Artikeln wird zwischen Kommissionier- und Lagerware unterschieden. Die Kommissionierung einer Kommissionierware wird über ein zugehöriges Los mit dessen Sollmaterialen (Module) abgebildet. Die Abbuchung eines Moduls erfolgt ins Los. Erst bei Fertigstellung des Loses mit Erfassung aller Module wird die Ware in den Lieferschein gebucht. Lagerwaren liegen bereits vollständig am Lager und werden bei Abbuchung direkt aus dem Lager in den Lieferschein gebucht.

Um eine Kommissionierware handelt es sich, wenn sich die Stückliste im aktuellen Mandanten befindet und der Fertigungsgruppe aus dem Mandantenparameter KOMMISSIONIERUNG_FERTIGUNGSGRUPPE zugehörig ist.

### **Produkt <-> Module**

Was ist der Unterschied zwischen Produkt und Module, wozu diese Bezeichnungen?
Das (fertige) Produkt ist diejenige Stückliste, welche beim Kommissionieren gefertigt wird. D.h. auch dass die Produkte Mitglieder der Fertigungsgruppe des Kommissionierterminals sein müssen. Siehe dazu bitte Parameter KOMMISSIONIERUNG_FERTIGUNGSGRUPPE. Ein Produkt besteht in der Regel aus mehreren Modulen, welche zu einem Produkt zusammenkommissioniert werden.
Da die Module sehr oft nur einzelne Litzen sind und die einzelne Handhabung daher mühsam wäre, werden diese zu Bündeln zusammengefasst. Die Bündelmengen werden im Artikel (des Moduls) im Reiter Sonstiges unter Verpackungsmenge definiert. Die Modul-Bündel werden beim Kommissionieren in Kisten gegeben. Die möglichen Mengen, welche in die Kisten gegeben werden können werden im Artikel (des Produktes) im Reiter Sonstiges unter Verpackungsmittelmenge definiert. Achten Sie bitte bei der Definition darauf, dass die Kistenmenge immer ein ganzzahliges Vielfaches der Bündelmenge ist.  Bitten Sie Ihren **Kieselstein ERP** Betreuer die Feldbezeichnungen im Artikel entsprechend auf Bündelmenge / Kistenmenge umzubenennen.

### Produkte

Mit Klick auf eines der gezeigten Ziele, werden alle lieferbaren Produkte dieser Lieferadresse und Typs geladen. Diese werden nach folgenden Kriterien sortiert:
1.  Ausliefertermin aufsteigend
2.  Bereich Herde vor Mulde
3.  Linie absteigend
4.  Artikelnummer absteigend
Der Ausliefertermin ist der Termin der Forecastposition abzüglich der hinterlegten Kundenlieferdauer. Die Menge ist bei Typ "NoKa" die Menge aus der Forecastposition und bei Typ "Linie" die Mengensumme der Linienabrufe. Da es für Produkte des Typs "NoKa" keine Linienabrufe gibt, in denen der Bereich definiert wäre, muss die Bezeichnung des Bereichs in der Zusatzbezeichnung 2 des jeweiligen NoKa-Artikels vermerkt werden.

Die Gesamtanzahl der Kisten ergibt sich aus der Division der Menge und definierten Kistenmenge (Reiter Sonstiges im Artikel) des Produkts. In der Spalte "Kommissionierer" wird der Name der Person angezeigt, die zuletzt eine Bündelbuchung dieses Produkts durchgeführt hat.
![](kt_produkte_linie.PNG)
Für die Liste der Produkte gibt es die Möglichkeit diese nach meinen angearbeiteten, allen offenen und von anderen angearbeiteten Produkte zu filtern. Hat sich Kommissionier entschieden ein Produkt zu kommissionieren, so wählt er dieses mit Tipp auf die Zeile.

Daraufhin wird versucht ein Los zu finden. Um die zu kommissionierenden Module anzeigen zu können, muss eine Verbindung zwischen Produkt und Los hergestellt werden. Dies erfolgt nach folgenden Bedingungen:
1.  Artikelnummer des Stücklistenartikels entspricht der Artikelnummer des Produkts
2.  Fertigungsgruppe ist gleich jener aus dem Parameter KOMMISSIONIERUNG_FERTIGUNGSGRUPPE
3.  Status des Loses ist "Angelegt"

Werden mehrere Lose gefunden wird das älteste verwendet. Stimmt die Losgröße nicht mit der tatsächlich zu lieferenden Menge des Produkts überein, so wird die Losgröße auf den Wert der Forecastmenge korrigiert. Die Differenz wird dabei auf die Losgröße des nächstältesten Loses übertragen, sodass die Summe der Losgrößen über alle Lose (nach den beschriebenen Kriterien) dieses Produkts gleichbleiben. Das neu verbundene Los wechselt in den Status "In Produktion". Gibt es kein weiteres Los und müsste eine Menge > 0 übertragen werden, so wird ein neues angelegt mit der zu übertragenden Menge als Losgröße. Die fixe Verbindung zum gefundenen Los sowie die Manipulation der Losgrößen und des Status, erfolgt aber erst mit ersten Buchung eines Bündels.

Wurde einmal die Verbindung zu einem Los hergestellt, bleibt diese bestehen. Wurde das Produkt nicht fertig kommissioniert und dieses wieder mit Klick auf die Zeile in der Maske "Produkte" ausgewählt, wird das bereits verbundene Los herangezogen.

### Zeitbuchung auf das Los
Mit Start der Kommissionierung einer Kommissionierware, erfolgt auch eine Zeit-Beginn-Buchung auf das Los.

### Kommissionierung
Mit der Produktauswahl startet nun die eigentliche Kommissionierung. Über das verbundene Los werden die zu kommissionierenden Module geladen.
![](kt_linienabrufe.PNG)

Die Module entsprechen den Lossollmaterialen aus dem verbundenen Los. Es gibt auch die Möglichkeit Materialen nicht für die Kommissionierung zu berücksichtigen, z.B. Etiketten. Alle Materialen, die einer der im Parameter "KOMMISSIONIERUNG_AUSGESCHLOSSENE_ARTIKELGRUPPEN " definierten Artikelgruppen zugehörig sind, werden nicht am Terminal angezeigt. Diese werden nach Abschluss der Kommissionierung automatisch ins Los nachgebucht.

### Versandetiketten
Die Versandetiketten können über den Druck-Button rechts der Tabelle gedruckt werden.
**Achtung:** Der Druck-Button wird nur dann angezeigt, wenn
1.  der Forecast-Lieferadresse (aktuelles Kommissionierziel) im **Kieselstein ERP** ein Kommissionierdrucker zugewiesen wurde
2.  im Konfigurationsmenü des Terminals diesem Kommissionierdrucker ein Druckername hinterlegt wurde. (siehe Abschnitt Konfigurationsdialog)

### Module
Die Module werden in der Tabelle unterhalb der Linieninformation aufgelistet. Gibt es mehrere Linien für dieses Produkt, gibt es damit pro Linie einen Zeilenblock aller Module. Darin sind sie nach Artikelnummer absteigend sortiert. Es gibt weiters die Info über die Kistennummer, Gesamtbündelanzahl, Bündelanzahl und Anzahl der gescannten Module.
Wenn das Produkt eine Lagerware ist, dann wird dieses durch ein Modul dargestellt, dass das Produkt selbst repräsentiert. Dies wurde zur Vereinfachung der Lagerwarenbuchung so gewählt.
![](kt_buendelmengeneingabe.PNG)
Über den konfigurierten Barcodescanner - oder alternativ über einen Tip auf die Zeile - können nun Artikelnummern gescant werden. Nach einem Scan erscheint die Maske für die Bündelmengeneingabe. Dabei kann eine Zahl bis zur maximal noch offenen Menge der aktuellen Kiste eingegeben werden. Mit der "Enter"-Taste des virtuellen Nummernfelds oder des "Ok"-Buttons wird die Eingabe bestätigt. Ist die eingegebene Zahl größer der maximal zulässigen, wird sie auf diese zurückgesetzt mit einer entsprechenden Meldung. Verwendet man den Barcodescanner wird über einen neuen Artikelnummernscan die angezeigte Bündelmenge ebenfalls bestätigt.

Die gültige Ist(Bündel-)Menge wird dann (über die RestAPI) in das Los (oder bei Lagerware direkt auf den Lieferschein) gebucht. D.h. wenn z.B. die Bündelmenge im Artikel mit 12 Stk. definiert ist, so werden 12 Stk. des Moduls - bei einer Bündelmengeneingabe von 1 - als Istmaterialentnahme ins Los gebucht werden (oder bei Lagerware auf die Lieferscheinposition des betreffenden Artikels).

War die Buchung erfolgreich wird der Zähler der Scans um die gebuchte Bündelanzahl erhöht. Wurde die geforderte Bündelanzahl für das Modul zur Gänze gescant, so verschwindet die Zeile dieses Moduls. D.h., dass immer nur jene Module in der Tabelle sichtbar sind, die noch zu kommissionieren sind. Unterhalb der Tabelle wird die Information des zuletzt getätigten Scans eingeblendet, also Bereichsnummer, Linie, Bündelanzahl, Artikelnummer, Auftragsnummer und Kiste.
Sind mehrere Linien in einem Produkt vorhanden, so sind die Linien absteigend sortiert. Wird eine Artikelnummer gescant, wird immer jenes Modul gebucht, das laut Sortierung der Tabelle zuoberst ist. Das gilt auch für den simulierten Bündelscan mit Tip auf die Zeile.

### Fertigstellung und Übertrag auf den Lieferschein bei Kommissionierwaren
Sind alle Bündel erfolgreich gescant worden, werden alle restlichen für die Kommissionierung ausgeschlossen Materialien automatisch nachgebucht, die Losablieferbuchung ins Ziellager des Loses durchgeführt und das Los vollständig erledigt.
Danach wird die abgelieferte Menge als Lieferscheinposition in den zugehörigen Lieferschein übernommen. Es erscheint dazu eine Meldung am Terminal.

Wenn notwendig, können (zwischendurch) Teilablieferungen durchgeführt werden. Tippen Sie dazu auf den LKW-Button (neben dem Druck-Button). Dadurch wird die maximal ablieferbare Menge errechnet und nach dem gleichen Schema in den Lieferschein übernommen. Das Los wird dabei nicht erledigt und erscheint, da noch etwas zu kommissionieren ist, nach wie vor in der zu kommissionierenden Produktliste. Sollte die weitere Kommissionierung dieses Loses nicht mehr erforderlich sein, muss dieses Los über den **Kieselstein ERP** Client manuell erledigt werden.

Der Positionsstatus der Forecastposition wird bei Fertigstellung, sowohl bei Kommissionier- als auch bei Lagerwaren, auf "ERLEDIGT" gesetzt.

### Anlegen des Lieferscheins für Kommissioner- und Lagerwaren
Für den Eintrag der abgelieferten Menge in einen Lieferschein (oder die Buchung aus dem Lager bei Lagerwaren), wird die Forecast-Lieferadresse verwendet. Zugleich wird der (intern verwaltete) Kommissioniertyp beim Lieferschein hinterlegt.
Der Lieferschein muss im Status "Angelegt" sein. Ist so ein Lieferschein bereits vorhanden, so wird er um die neue abgelieferte Menge ergänzt, ansonsten wird ein neuer Lieferschein angelegt.
**WICHTIG:** Für die Entscheidung ob ein neuer Lieferschein angelegt werden sollte, ist nur der Status ausschlaggebend. Das Lieferscheindatum wird bei der Anlage auf heute / jetzt gesetzt, aber sonst nicht weiter berücksichtigt.
Ist in dem Lieferschein bereits eine Position enthalten die der Forecastposition (des abliefernden Loses) entspricht, so wird die Menge der Position um die abgelieferte Stückzahl erhöht. Anderenfalls wird eine neue Lieferscheinposition eingetragen.

## Einrichten des Terminals, Konfigurationsdialog
Der Konfiurationsdialog kann in der Anmeldemaske (Startmaske) durch den Tipp auf das **Kieselstein ERP** Logo geöffnet werden. Nach Eingabe des Kennworts werden die Reiter der Konfiguration angezeigt: Barcode, Serververbindung, Fertigung, Kommissionierung, Info.
Mit dem Button "Beenden" wird das Terminal beendet. Änderungen in der Konfiguration werden dabei **nicht** übernommen, nur mit "Ok" werden die Daten als Json-String im Unterordner "etc" des Verzeichnisses der Terminal-Jar-Datei als "terminalconfig.json" gespeichert. Dabei werden die Änderungen sofort übernommen und das Terminal neu initialisiert.

**Hinweis:**
Werden Änderungen an der seriellen Kommunikation oder an den Basisdaten des Rechners vorgenommen, so muss das Terminal in jedem Falle neu gestartet werden.

### Reiter Barcode
- *Port Barcodescanner:* Auswahl der aktiven Com-Ports
- *Port RFID-Scanner:* Auswahl der aktiven Com-Ports
- *RFID-Order:* LSB (least significant bit) oder MSB (most significant bit)

### Reiter Serververbindung
Standardeinstellungen des Terminals

| Parameter | Beschreibung |
| --- |  --- |
| Mandant | in welchem Mandanten soll der Terminal laufen |
| Station | Name der Station, wird z.B. als Quellenangabe für Zeitbuchungen verwendet |
| Server | Name des **Kieselstein ERP** Servers |
| Locale | Auswahl der verfügbaren Sprache |
| Anzeige Fenstermodus | nicht angehakt dann Vollbild |
| Terminalfunktion | Auswahl der Terminalfunktionalität, Erfassungsterminal oder Kommissionierterminal |
| Kommt/Geht buchen | nicht angehakt wird die Zeile rechts in den Personalfunktionen, mit den Buttons "Kommt", "Geht", "Saldo" nicht angezeigt |
| Sortierung Loswauswahl | Auswahl der Default-Sortierung der Loswauswahl, nach Losnummer oder nach Priorität (über den HELIUM Client festgelegte Reihung, siehe Modul Fertigung, unterer Reiter offene AGs, oberer Reiter Reihung) |

### Reiter Fertigung
Konfiguration speziell für Terminalfunktion "Erfassungsterminal"

| Parameter | Beschreibung |
| --- |  --- |
| Maschinengruppe | Maschinengruppe für die das Terminal zugeteilt wird |
| Drucker | Name des Standarddruckers für den Fertigungsschein |
| Etikettendrucker | Name des Etikettendruckers |
| Bündeletikettendrucker | Name des Bündeletikettendruckers |
| Reportvariante Bündeletikett | Reportvariante, die für den Bündeletikettendrucker verwendet werden soll (wie im **Kieselstein ERP** Client unter System → Mandant → Reportvariante für Report "fert_losetikett.jasper" angegeben, z.B. "fert_losetikett_buendel.jasper") |
| E-... Verpackungsetikett Drucker 1 | Name von Drucker 1 für diese Variante des Losetikettendrucks |
| E-... Verpackungsetikett Drucker 2 | Name von Drucker 2 für diese Variante des Losetikettendrucks |
| Reportvariante E-... Verpackungsetikett | Reportvariante, die für die E-... Verpackungsetikettdrucker verwendet werden soll (wie im **Kieselstein ERP** Client unter System → Mandant → Reportvariante für Report "fert_losetikett.jasper" angegeben, z.B. "fert_losetikett_verpack.jasper") |
| Warnungsgrenze Überlieferung  | Bei wievielen Stk an Überlieferung soll eine Warnung erscheinen? Angabe entweder in Stk. (z.B. 10) oder in Prozent (z.B. "10%")Wenn leer dann ist jede Überlieferung zulässig. |
| Barcodescan in Materialliste  | Wenn angehakt, ist der Barcodescan über die Artikelnummer in der Materialliste aktiviert. |
| Barcodescan Verzeichnis  | Bei aktiviertem "Barcodescan in Materialliste" kann hier ein Verzeichnis definiert werden, worin die Scanergebnisse gespeichert werden. Dabei wird pro Los eine Datei angelegt. |

### Reiter Kommissionierung
Konfiguration speziell für Terminalfunktion "Kommissionierterminal"

| Parameter | Beschreibung |
| --- |  --- |
| Kommissionierdrucker | Zuweisung der lokal am Terminal verfügbaren Drucker zu den in **Kieselstein ERP** definierten Kommissionierdruckern (mandantenabhängig) |

Im Kommissionierterminal können bis zu neun verschiedene Drucker definiert werden. Der Grundgedanke dahinter ist, dass je Forecastauftrag unter Umständen auf einen anderen Drucker die Kommissionieretiketten gedruckt werden müssen.
Definieren Sie bitte im Modul Forecastauftrag, im unteren Modulreiter Grunddaten, im oberen Modulreiter Kommissionierdrucker, welche Druckernamen die Sie verwenden möchten.<br>
![](kt_konfig1.gif)<br>
Bitte beachten Sie, dass dies nur die Definition der Namen sind. Die Zuordnung erfolgt im Kommissionierterminal.

Obige Definition bewirkt dann, dass in der Druckerkonfiguration des Kommissionierterminals
![](kt_konfig2.gif)<br>
die beiden Drucker angezeigt werden. Hinterlegen Sie hier nun wie beim Erfassungsterminal beschrieben den für das jeweilige Kommissionierterminal zu verwendenden Drucker.

Hinweis: Welcher Drucker vom Kommissionierterminal verwendet werden sollte, wird dann in der Lieferadresse des Forecastauftrages definiert.
![](kt_konfig3.gif)<br>

### Reiter Info
Hier werden die Verbindungsdaten und Informationen zum **Kieselstein ERP** Server angezeigt. D.h. bei der Neuinstallation eines Terminals prüfen Sie in diesem Reiter ob Ihre Konfiguration richtig ist und ob die Verbindungszeiten wie erwartet sind.

![](Konfiguration_Info.gif)

## Drucker
Die vom Terminal angesteuerten Drucker müssen selbstverständlich im jeweiligen Betriebssystem des Terminals eingerichtet sein.
Beachten Sie dazu bitte auch das unter [Barcodedrucker](../System/barcodes.htm#Barcodedruckertreiber) beschriebene.

## Parametrierungen am **Kieselstein ERP** Server für das Kommissionierterminal
Es werden nur Lose eine bestimmten Fertigungsgruppe angezeigt. Diese muss mit dem Parameter KOMMISSIONIERUNG_FERTIGUNGSGRUPPE eingestellt werden.
Für die Kommissionierung können bestimmte Artikelgruppen von der Kommissioniererfassungsbuchung ausgenommen werden. Definieren Sie diese mit dem Parameter KOMMISSIONIERUNG_AUSGESCHLOSSENE_ARTIKELGRUPPEN. Sollten mehrere Artikelgruppen ausgeschlossen werden müssen, so trennen Sie diese durch | (Vertikal Bar).
Bitte beachten Sie, dass für das in aller Regel erforderliche automatische Nachbuchen der ausgeschlossenen Artikel, auch der Parameter BEI_LOS_ERLEDIGEN_MATERIAL_NACHBUCHEN gesetzt sein muss.
Lastauto -> bedeutet Erfassung abschließen und die ganzen Sollsatzgrößen in den Lieferschein buchen
Lieferschein wird immer zu heute angelegt (00:00-23:59:59). D.h. derzeit werden in der dritten Schicht gegebenfalls zwei Lieferschein erzeugt.
Fehlermeldung: Die Artikeldaten des Loses sind unbrauchbar. Bitte wenden Sie sich an Ihrern Vorgesetzten.
Bedeutet: Bitte prüfen Sie, ob das Los wirklich mit den gewünschten Daten aus der Stückliste übereinstimmt. Ev. wurden aus Vergangenheitsdaten unrunde Sollstzgrößen o.ä. erzeugt.

Ist ein Los schon vollständig ausgegeben, aber noch nicht abgeliefert so liegt für den Anfang der Trick darin, dass man das Los wieder zurücknimmt (Kopfdaten, Ändern, Ja) und dann eine Handausgabe macht. Das setzt das Los in den Status ausgegeben, gibt aber keine Mengen aus, sondern erzeugt nur Fehlmengen, genauso wie das Kommissionierterminal das auch macht.

## Erforderliche Rechte für Erfassungs- bzw. Kommissionierterminal
Damit das Erfassungsterminal seine Daten verbuchen kann, sind folgende Rechte erforderlich:
- FERT_LOS_CUD
- FERT_LOS_DARF_ABLIEFERN
- LS_LIEFERSCHEIN_CUD
- PERS_ZEITEREFASSUNG_CUD
- PERS_SICHTBARKEIT_ALLE