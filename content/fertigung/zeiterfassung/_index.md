---
title: "Zeiterfassung"
linkTitle: "Zeiterfassung"
categories: ["Zeiterfassung"]
tags: ["Zeiterfassung"]
weight: 500
description: >
  Anwesenheitszeiten, Zeiten auf Projekten, Aufträgen, Losen, Angeboten, Telefonzeiten, Maschinenzeiten, Zeitabrechnungen, Überleitung in fremde Lohnverrechnung
---

Zeiterfassung

Diese kann mit dem **Kieselstein ERP** Client erfolgen, aber auch mit weiteren Geräten

Zeiterfassung
=============

In **Kieselstein ERP** steht Ihnen mit dem Modul Zeiterfassung ![](ZE.gif) ein umfassendes Werkzeug zur Erfassung und Bearbeitung der Zeitbewegungsdaten zur Verfügung. Im Zeiterfassungsmodul werden sowohl die Mitarbeiter Anwesenheitszeiten als auch die Projektzeiten der Mitarbeiter erfasst bzw. bearbeitet. Für die effiziente Erfassung der Zeiten im Unternehmen stehen verschiedene [Erfassungsgeräte](#Erfassungsarten / Terminals) zur Verfügung.
Siehe bitte auch [Zusatzinformationen]( {{<relref "/fertigung/zeiterfassung/zusatzinformationen">}} ).
Zum Thema [Reisekosten]( {{<relref "/fertigung/zeiterfassung/reisezeiten">}} ) und deren Abrechnung siehe bitte dort.
Zum Thema [Export der Zeitbewegungsdaten]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ), z.B. für die Lohnabrechnung siehe bitte dort.
Als Alternative steht für projektorientiertes Arbeiten die sogenannte "[Dauererfassung]( {{<relref "/fertigung/zeiterfassung/dauer">}} )" zur Verfügung.
[Zeiterfassungsgeräte siehe bitte](#Erfassungsarten / Terminals) bzw. generell [Zeiterfassungsclients]( {{<relref "/fertigung/zeiterfassung/ze_clients">}} ).

## Warum ist die Projektzeiterfassung für Ihr Unternehmen und für Ihre Mitarbeiter wichtig?

Die Projektzeiterfassung dient grundsätzlich der Sicherstellung des unternehmerischen Erfolges und damit direkt der Sicherung der Arbeitsplätze Ihrer Mitarbeiter.
Sie werden sich nun fragen wieso das?

In verkürzter Form kann man Folgendes festhalten:<br>
Wenn Projekte verkauft werden, so werden in irgendeiner Form immer Angebote gelegt und sei es nur mündlich. Die Projektzeiterfassung stellt nun die verbrauchten Zeiten den geplanten Zeiten gegenüber und sei es nur in Form eines direkten Kosten / Erlös-Vergleiches.<br>
Wurde nun der Aufwand für dieses Projekt beim Angebot unterschätzt, so ergibt dies einen negativen Erfolg. Wenn dieses Wissen an den Verkauf weitergegeben wird, so kann beim nächsten Projekt besser und richtiger angeboten werden. Auch im umgekehrten Fall, also einem viel zu hohen Ertrag ist dieses Wissen enorm wichtig, da gerade KMUs immer zu marktfähigen Preisen anbieten müssen. Wenn nun ein Produkt viel zu teuer eingeschätzt wird, würde dies bedeuten, dass es für den Mitbewerb ein Leichtes ist, Ihre Preise zu unterlaufen und damit verlieren Sie den nächsten Auftrag und damit den Kunden. Also ist eine sehr umfassende, genaue und zeitnahe Erfassung der Projektkosten und damit auch der Zeiten für Ihr Unternehmen überlebensnotwendig und trägt dadurch zum Erhalt der Arbeitsplätze ganz entscheidend bei.

Oder in anderen Worten:
Die Zeiterfassung dient zwar der exakten Erfassung der Zeiten, dient jedoch nicht dazu die Mitarbeiter unter Druck zu setzen.
Es ist ein wichtiges **<u>Werkzeug</u>** um das **<u>Unternehmen</u>** zu **<u>steuern</u>**.  <br>
**Tipp:** Es hat sich bei den Einführungen der Zeiterfassung immer wieder gezeigt, dass es sehr wichtig ist, diese Message VOR dem Einsatz der Zeiterfassung an die Mitarbeiter zu kommunizieren.

**Hinweis:**
Manche Arbeitsrechtler sehen bereits eine Produktivitätsstatistik oder auch die Maschinenbestenliste als ein arbeitsrechtliches Thema. Holen Sie die Menschen ins Boot. Nur wenn Sie Gewinne erwirtschaften, können auch die Arbeitsplätze erhalten bleiben.
Oder gerne auch: Die Mitarbeiter:innen müssen prüfen ob der Chef richtig gerechnet hat.

## Grundzüge der Erfassung der Zeitdaten

Das Ziel der Zeiterfassung ist eine möglichst genaue Erfassung alle angefallenen Zeiten. Es wurde aus diesem Grunde die Zeiterfassung so aufgebaut, dass jede Zeitbuchung einen Beginn zu diesem Zeitpunkt darstellt.<br>
D.h. bei der Kommt Buchung läuft die Anwesenheitszeit ab diesem Zeitpunkt. Bei einer Tätigkeitsbuchung läuft die Zeit auf dieser Tätigkeit (auf dem Auftrag) ab dem eingebuchten Zeitpunkt. Da ein Mensch nur eine Tätigkeit zur gleichen Zeit ausführen kann, ist es auch so realisiert, dass bei Beginn einer neuen Tätigkeit automatisch die alte Tätigkeit beendet wird. Auch die [Mehrmaschinenbedienung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ) geht davon aus, dass einerseits ein Mensch nur eine Tätigkeit zu einer Zeit ausführen kann und das gleiche gilt für die Maschinen. D.h. auch eine Maschine kann nur eine Tätigkeit zu einer Zeit ausführen.

Tätigkeiten werden entweder durch andere Tätigkeiten beendet oder durch eine Ende oder Geht Buchung.

Eine Ende Buchung bedeutet, dass eine Tätigkeit jetzt beendet wird, und die Anwesenheitszeit des Mitarbeiters nun auf kein Projekt mehr gebucht wird. Die Geht Buchung beendet ebenfalls eine ev. aktive Tätigkeit und beendet zugleich auch die Anwesenheit des Mitarbeiters.

Bitte beachten Sie den Unterschied zwischen Kommt, Geht und Unterbrechung. Eine Geht Buchung besagt, dass der Mitarbeiter nicht mehr vor hat an diesem Arbeitstag noch ins Unternehmen zu kommen. Eine Unterbrechungsbuchung sagt aus, dass er, z.B. nach der Mittagspause, wieder ins Unternehmen kommen will. Diese Information ist für die Anwesenheitsliste wichtig. Es informiert damit jeder Mitarbeiter seine Kollegen über seine geplante Anwesenheit im Unternehmen.

Bitte definieren Sie, ob Ihre Zeitberechnungen / Kalkulationen Maschinen oder Mitarbeiter orientiert sind. [Details dazu siehe bitte]( {{<relref "/fertigung/zeiterfassung/mensch_oder_maschine">}} ).

## Zeitbuchung in **Kieselstein ERP**

![](Zeiterfassung_KES.JPG)
Um eine Zeitbuchung vorzunehmen, stehen Ihnen verschiedene Möglichkeiten zur Verfügung:

Es gibt zwei Zeilen für die Schnellbuchung
- Kommt, Unter, Ende, Geht (Alt+K, Alt+U, Alt+E, Alt+G)
- Projekt, Angebot, Auftrag, Los (Alt+P, Alt+T, Alt+A, Alt+L)

Diese Buttons haben wiederum zwei Verhalten
- a.) Mit dem Klick auf eines der Icons KOMMT, UNTER, GEHT und ENDE, wird die Buchung mit dem aktuellen Zeitpunkt vorgenommen (Alt+K, Alt+U, Alt+E, Alt+G) (gilt am aktuellen Tag).
- b.) Mit dem Klick auf Projekt, Angebot, Auftrag und Los, öffnet sich ein Auswahldialog des jeweiligen Belegs für den die Buchung vorgenommen wird.

Klickt man auf Neu (und stellt dann eine Uhrzeit ein) und danach auf den Button (z.B. Kommt) so bewirkt dies, dass die Sondertätigkeit (z.B. Kommt) an dem gewählten Tag mit der eingestellten Uhrzeit gespeichert wird. Wenn hier ein Beleg ausgewählt wird, so öffnet sich die Auswahlliste des jeweiligen Beleges. Danach geben Sie z.B. einen Kommentar in der Bemerkung ein und klicken auf speichern.

#### Können Angebote und Aufträge gemeinsam genutzt werden?
Mit dem Parameter ZEITBUCHUNG_AG_AB_GEMEINSAM=1 und wenn der Benutzer auf beide Module zumindest ein lesendes Recht hat, also RECHT_AUFT_AUFTRAG_R und RECHT_ANGB_ANGEBOT_R gegeben ist, so wird, wenn der Parameter aktiviert ist, anstatt der Buttons Angebot, Auftrag der Button "Angebot/ Auftrag" angezeigt. Damit werden in der Auswahlliste Angebote und Aufträge kombiniert dargestellt, wobei die Basis ist die Auftragsauswahlliste ist, welche um die Spalten der Angebotsauswahlliste ergänzt wird.

### Verwendung der Favoritenliste
Um der Praxis gerecht zu werden, gibt es in **Kieselstein ERP** eine Favoritenliste![](Icon_aufruf_Favoritenliste.JPG). Hier werden die letzten 15 Belege und deren Zeitbuchung aufgelistet.
Mit Klick auf das Icon öffnet sich die Listenansicht.
![](Favoritenliste_Zeit.JPG)
Nun markieren Sie die gewünschte Zeile.
Mit dem Icon Zeile als Vorschlagswert übernehmen ![](Vorschlagswert_uebernehmen.JPG), wird die markierte Zeile in die Felder übernommen. Sie können noch Änderungen vornehmen (z.B. Kommentare eintragen) und klicken auf speichern. Hierzu können Sie auch die gewählte Zeile markieren und mit Enter / Doppelklick die Inhalte übernehmen.
Mit dem Icon Zeitbuchung mit aktueller Zeit speichern ![](Favoritenliste_mitaktuellerZeit.JPG), wird die markierte Zeile übernommen und gespeichert. Der Shortcut Strg+S speichert die markierte Zeile zum aktuellen Zeitpunkt. (Diese Funktion gilt nur für Zeitbuchungen am aktuellen Tag.)

#### Mit welcher Zeit werden die Daten verbucht?
<a name="Zeitverbuchung"></a>
Die Buchung der Zeiten erfolgt immer von dem Gerät aus, auf dem die Zeit erfasst wird. Erfolgt die Erfassung also auf einem Client-PC so wird die Zeit vom Client als die zu verbuchende Zeit in die Datenbank übernommen.
Wichtig: Bitte sorgen Sie dafür, dass
- a.) Ihr Server immer die aktuelle Systemzeit hat. Verwenden Sie dazu z.B. die im Internet frei verfügbaren Synchronisierungen mit Atomuhren.
- b.) Stellen Sie sicher, dass die Systemzeit aller Clients, auch Ihrer Terminals, mit der Systemzeit des Servers synchron laufen.
Von **Kieselstein ERP** wird beim Login eine Meldung ausgegeben, wenn die Systemzeiten zwischen Client und Server um mehr als eine Minute untereinander abweichen. [Siehe dazu auch]
({{< relref "/docs/installation/13_zeitserver" >}})

### Warum ist die exakte Zeit wichtig?
Für manche Unternehmen ist es aus den verschiedensten Gründen wichtig, dass die Mitarbeiter:innen gemeinsam ihre Aufgabe erfüllen können. D.h. wenn von z.B. 10 Menschen einer Gruppe eineR fehlt, können die anderen 9 trotzdem nicht beginnen. Daher versucht man, unter anderem, die Mitarbeiter:innen durch Rundung zu Gunsten des Unternehmens dazu zu erziehen. D.h. wenn nun einE Mitarbeiter:inn um eine Minute zu spät kommt, werden z.B. 15' von der angerechneten Anwesenheitszeit abgezogen. Ist nun diese eine Minute aber ein Erfassungsproblem, da die Uhrzeit des ZE-Terminals falsch geht, so verliert der Mitarbeiter ungerechtfertigt 15Minuten. Wenn das über die ganze Woche so geht ist das mehr als eine Stunde usw.

#### Bei welchem Status der Belege kann keine Zeitbuchung erfolgen?
Bei folgenden Stati der einzelnen Belege ist keine Zeitbuchung möglich:
| Modul | Status |
| --- | --- |
| Angebot | Storniert / Erledigt |
| Los | Storniert / Erledigt / Gestoppt  |
| Auftrag | Angelegt / Erledigt |
| Projekt | Erledigt, welcher als Subeigenschaft des Projektstatus definiert wird |

Im Projekt steuert die Zeitbuchung zusätzlich der Parameter INTERN_ERLEDIGT_BEBUCHBAR (0/1) ob auf intern erledigte Projekte Zeiten gebucht werden können.
Im Los gibt es den Parameter PARAMETER_ZEITBUCHUNG_AUF_ANGELEGTE_LOSE_MOEGLICH (0/1) mit dem definiert wird, ob auf angelegte Lose Zeiten gebucht werden können.

#### Wie gibt man die Zeitdaten korrekt ein?
Die Zeitdaten stellen immer den Beginn einer Tätigkeit, eines Vorganges dar. Dies ist auch der übliche, in sehr vielen Unternehmen verwendete, Buchungsvorgang. Er deckt sich auch mit der Art der Buchungen auf den Zeiterfassungsterminals.
Es gibt lediglich bei den Administrativen Tätigkeiten und bei Tätigkeiten welche sehr oft unterbrochen werden, die Forderung einer Zeiteingabe nach Dauer. Diese wird in **Kieselstein ERP** über die sogenannte relative Zeiteingabe realisiert. Hier geben Sie lediglich die Dauer einer Tätigkeit ein. Das System bucht den Beginn der Tätigkeit so früh wie möglich und das Ende der Tätigkeit entsprechend der angegebenen Dauer. Siehe dazu auch  [Dauererfassung]({{< relref "/fertigung/zeiterfassung/dauer" >}}).

**<a name="Relative Zeitbuchung"></a>Beachten Sie bitte:**

Mit der relativen Zeitbuchung wurde nur ein Hilfsmittel geschaffen, mit dem eine Dauer eingegeben werden kann. Beim Klick auf Speichern wird die angegebene Dauer in eine Beginn und Ende Buchung umgesetzt. Alternativ dazu siehe bitte  [Dauererfassung]({{< relref "/fertigung/zeiterfassung/dauer" >}}).
Diese Eigenschaft bewirkt auch, das wenn eine Zeitbuchung herausgelöscht wird, die vorherige Tätigkeit, der vorherige Auftrag (Job) entsprechend länger dauert, bis er eben durch die nächste Tätigkeit beendet wird. Eine einfache Abhilfe ist, statt des Löschens einer Tätigkeit die Buchung auf Ende zu ändern.
Ein Beispiel dazu:

| Buchung | Beginn-Zeit | Dauer |
| --- |  --- |  --- |
| Kommt | 8:00 |  |
| Tätigkeit 1 | 8:00 | 2 Std. |
| Tätigkeit 2 | 10:00 | 1 Std. |
| Tätigkeit 3 | 11:00 | 5 Std. |
| Geht | 16:00 |  |

Eine Tätigkeit wird immer durch den Beginn einer neuen Tätigkeit beendet.
Das bedeute aber auch, wird die Tätigkeit 2 herausgelöscht, so ist die gesamte Zeit auf Tätigkeit 1 (3Std). Ändern Sie dagegen die Buchung von Tätigkeit 2 auf eine Ende Buchung, so dauert die Tätigkeit 1 weiterhin 2Std und es wird zusätzlich eine Stunde auf nicht produktiv gebucht. Genau um diese Information geht es.

#### Bei der relative Zeitbuchung erhalte ich eine Fehlermeldung
![](Sondertaetigkeit_beenden.gif)

A: Die relative Zeitbuchung bucht die Dauer der Tätigkeit, wie oben beschrieben, in die noch nicht verbrauchte Tagesarbeitszeit. Um diese richtig berechnen zu können, müssen die Unterbrechungsbuchungen abgeschlossen sein. In obigem Fall, wurde mit einer relativen Zeitbuchung mehr Zeit beansprucht, als zwischen Kommt und dem Unterbrechungsbeginn zur Verfügung steht. Da daher das Ende der relativen Buchung nicht ermittelt werden kann, wird obige Fehlermeldung ausgegeben.

![](Sondertaetigkeit_beenden_Muster.gif)

#### Kann gleichzeitig auf mehreren Losen gearbeitet werden?
Üblicher Weise wird dies vor allem in der Produktion verwendet. D.h. aufgrund der Gleichartigkeit der Tätigkeit und oder des Materials arbeitet man quasi gleichzeitig an mehreren Losen. Dies wird am [Terminal](Zeiterfassungsterminal_Nettop.htm#$PLUS) mit der $PLUS Buchung gemacht ([BDE Station siehe](BDE_Station.htm#$PLUS)).
Am **Kieselstein ERP** Client kann dies in der Losauswahlliste durch die gleichzeitige Auswahl der entsprechenden Lose realisiert werden. D.h. markieren Sie die gewünschten Lose und klicken Sie dann auf den grünen Haken.
![](Zeitverteilung1.jpg)
Bei der Tätigkeit wird die Tätigkeit des ersten Loses (aus der Liste) vorgeschlagen. Ändern Sie diese gegebenenfalls ab und klicken Sie auf Speichern. Damit wird diese Liste der gleichzeitig gestarteten Lose in einer eigenen Tabelle abgelegt. Es wird dies durch ![](Zeitverteilung2.gif) offene Zeitverteilung vorhanden angezeigt. Im Tooltip dieses Knopfes werden die gestarteten Lose und deren Tätigkeiten angezeigt. Durch Klick auf den Button, werden diese Einträge nach Rückfrage gelöscht.
Bitte beachten Sie, dass die Mehrfachauswahl der Lose nur im Neu-Dialog möglich ist. Beachten Sie bitte auch, dass mit oben beschriebener Vorgehensweise nur Tätigkeiten auf gemeinsamen Losen gestartet werden können. Um Arbeitsgänge mit Maschinen gleichzeitig starten zu können, wechseln Sie bitte in das Modul Fertigung, unterer Reiter offene AGs, oberer Reiter offene AGs. Markieren Sie nun die Arbeitsgänge die gleichzeitig gestartet werden sollten und klicken Sie auf ![](Fertigung_gemeinsam_starten.gif) Fertigung gemeinsam starten. Wählen Sie nun die Person aus, für die die gemeinsame Zeitbuchung gemacht werden sollte.
[Beschreibung Zeitverteilungstypen]( {{<relref "/fertigung/zeitwirtschaft_in_der_fertigung/ag_gemeinsam_buchen/#zeitverteilungstyp" >}} )

#### Kann nachträglich gleichzeitig auf mehreren Losen gebucht werden?
Ja. Siehe oben, geben Sie einfach beim Neu den gewünschten Zeitpunkt an und beim anschließenden notwendigerweise erforderlichen Ende oder Geht, ebenfalls den gewünschten Zeitpunkt.

#### Wie wird eine offene Zeitverteilung abgeschlossen?
Muss eine Zeitverteilung am Terminal abgeschlossen werden, so buchen Sie für den gewünschten Zeitpunkt einfach ein Ende.
Damit wird die Zeit vom Beginn der Arbeit auf den gemeinsamen Losen bis zum Endezeitpunkt anhand der Sollzeiten der beteiligten Arbeitsgänge der verschiedenen Lose, anteilig anhand der angefallenen Zeit verteilt, sodass sich insgesamt wieder eine Arbeitszeit auf den Losen zwischen dem Beginn und dem Ende ergibt.
Da diese Berechnung exakt zu dem Zeitpunkt ausgeführt wird, mit dem der Endezeitpunkt gebucht wird, bewirkt eine nachträgliche Änderung z.B. des Endezeitpunktes KEINE anteilige Verschiebung der Loszeiten. Siehe dazu unbedingt auch $PLUS Buchung bzw. Parameter Zeitverteilungstyp.

#### Kann eine einzelne Zeitbuchung in eine relative Zeitbuchung umgewandelt werden?
Da eine relative Zeitbuchung immer aus dem beiden Buchungsdatensätzen Beginn und Ende besteht, kann eine einzelne Buchungszeile nicht in eine relative Zeitbuchung umgewandelt werden.

![](einzelne_Zeitbuchung.gif)

Um eine einzelne Buchungszeile zu einer änderbaren relativen Zeitbuchung zu machen, fügen Sie entweder eine Ende-Buchung hinzu, oder Sie löschen die Buchung und fügen eine relative Zeitbuchung neu ein.

Bitte beachten Sie, dass eine relative Zeitbuchung immer aus dem Datenpaar Beginn und Ende besteht. Eine einzelne Buchung ist nie eine relative Zeitbuchung, sonder der Beginn einer Tätigkeit, der durch eine andere Tätigkeit oder durch Ende bzw. Geht abgeschlossen wird, welche zusätzlich eingebucht werden muss.

#### Wieso sind Auftrag und Position Pflichtfelder?
Das Ziel der Auftrags / Job bezogenen Zeiterfassung ist, die Zeiten der Mitarbeiter den Aufträgen zuzuordnen. Wenn nun die Freiheit geschaffen würde, dass man hier auch Handeingaben buchen könnte, so würde damit dem Wildwuchs Tür und Tor geöffnet werden. Wer von den Anwendern / Benutzer würde nachträglich die Handeingaben auf die richtigen Auftrags- (Job-) nummern umändern. Darum eben müssen Zeitbuchungen die eine Auftragszuordnung haben sollen, einem Auftrag zugeordnet werden.

#### Welcher Unterschied besteht zwischen Sondertätigkeit und Auftragszeit?
Sondertätigkeiten sind grundsätzlich Tätigkeiten, welche sich direkt auf die Anwesenheit im Unternehmen auswirken. Denken Sie z.B. an die Sondertätigkeit Arzt. Diese besagt, dass der Mitarbeiter beim Arzt ist. Zugleich ist diese Nichtanwesenheit normalerweise vom Unternehmer wie Anwesenheit zu bezahlen, aber auf die Produktivitätsstatistik des Mitarbeiters hat diese Zeit keinen Einfluss.
Auftragszeiten sind jene Zeiten die ein Mitarbeiter für einen Auftrag aufwendet.

#### Welche verschiedenen Eingabemöglichkeiten gibt es bei Sondertätigkeiten und was verwende ich wozu?
Die Sondertätigkeiten werden vom **Kieselstein ERP** Administrator an die in Ihrem Unternehmen erforderlichen Sondertätigkeiten angepasst. Es werden immer solche Begriffe gewählt, die möglichst eineindeutig die Art der Sondertätigkeit widerspiegelt.
Beispiel: Berufsschule bedeutet, dass der Mitarbeiter außer Hause ist und er in der Berufsschule sein sollte.
Beispiel: ZA bedeutet Zeitausgleich. Der Mitarbeiter hat für diesen Tag einen Zeitausgleich geplant und ist deshalb voraussichtlich nicht im Unternehmen.
Hier werden auch zusätzliche Sondertätigkeiten wie Geburt eines Kindes, Verehelichung usw. definiert und können damit erfasst werden.
Bitte beachten Sie bei den Sondertätigkeiten auch die erforderliche Zuordnung der [Abwesenheitsart](#Abwesenheitsarten).

#### Kann auch Homeoffice erfasst werden?
Ja. Legen Sie bitte eine Sondertätigkeit Homeoffice an und tragen Sie bei Bezahlt 0% ein und haken Sie nur tageweise buchbar an.
Damit werden die Summe der Homeofficetage auf der Monatsabrechnung mit ausgewiesen. Durch das Bezahlt = 0% wird davon der Gleitzeitsaldo nicht beeinflusst.

#### Wie verhält man sich, wenn Auftrag, Position oder Tätigkeit im Moment der Zeiteingabe nicht bekannt sind (alle oder einzelne)?
In diesem Falle kann der Beginn der Tätigkeit nicht verbucht werden. Als Möglichkeit können Sie die Tätigkeit auf einen anderen Auftrag buchen und dies nachträglich ändern. **Kieselstein ERP** unterscheidet bei den Änderungsbuchungen zwischen Änderungen des Zeitpunktes und Änderungen des Auftrags.

Tipp: Eine Ende Buchung machen, und in die interne Bemerkung die Tätigkeit reinschreiben. Wenn dann Auftrag, Projekt etc. bekannt ist, braucht nur die Buchung geändert werden, was etwas anderes ist, als wenn dann nachträglich eine Buchung nacherfast wird.

#### Wie korrigiert man eine falsche Eingabe bei der Zeiterfassung?
Ändern ![](aendern.gif) Sie den Eintrag entsprechend ab.

#### Was bedeutet das H neben der Dauer?<br>
![](Hand_Zeit.gif)<br>
Mit diesem Zeichen soll symbolisiert werden, dass die Zeitbuchung manipuliert wurde. Das bedeutet, dass die Zeitbuchung für diesen Mitarbeiter nicht innerhalb des eingestellten Toleranzzeitraums erfolgte. Bei einer Standardeinstellung bedeutet dies, dass der Mitarbeiter z.B. um 8:03 gekommen ist, aber ein Kommt für 8:00 gebucht hat.
Beachten Sie dazu bitte auch, dass anstelle des H (für Handbuchung) ein B angezeigt werden kann. Dies bedeutet, dass zwar der Zeitpunkt der Buchung korrekt verbucht wurde, aber nachträglich die Bewegungsart geändert wurde. Also es hatte z.B. der Mitarbeiter versehentlich ein (Auftrags-) Ende gebucht, wollte aber ein Geht buchen.
Finden Sie ein **A** in der Bemerkung, so ist das ein Hinweis darauf, dass dies eine Automatikbuchung ist. Diese erfolgte also Aufgrund definierter Pausen.

#### Warum muss ich meine Tätigkeiten am Anfang erfassen und kann nicht den Endzeitpunkt eingeben?
Siehe dazu bitte oben, Grundzüge der Erfassung der Zeitdaten

#### Die angezeigte Dauer stimmt nicht / verwirrt.<br>
![](Dauer.gif)<br>
Bitte beachten Sie, dass die angezeigte Dauer einer Tätigkeit sich auf die mit dieser Buchung startende Tätigkeit bezieht. D.h. vom Beginn der Tätigkeit, in unserem Falle zwischen Kommt und der ersten Unterbrechungsbuchung sind 5,33Std vergangen. Die Unterbrechung selbst dauerte, wie in der zweiten Zeile ersichtlich 0,75 Std.

#### Wo sieht man auf welche Maschine gebucht wurde?
[Siehe dazu bitte](Maschinenzeiterfassung.htm#Maschinenzeitbuchungen finden).

#### Anzeige der Soll-Istzeiten, deren Abweichung
In der Zeiterfassung wird für diejenigen Positionen, bei denen ein Bezug zu den Los-Sollpositionen gegeben ist, auch die Soll- und die Ist-Zeit auf der jeweiligen Position angezeigt. Damit haben Sie jederzeit einen Überblick über den Erfüllungsgrad / den Zeitverbrauch, auch während der Erfassung Ihrer Zeiten.
![](Soll_Istzeit1.gif)
Damit die Soll-Stunden und damit auch die Buchungen darauf angezeigt werden können, ist es wichtig, dass die Zeiterfassungsposition sich auf eine Los-Sollzeit-Position bezieht. Ist dies nicht gegeben, so kann auch keine Sollzeit ermittelt werden und keine kumulierte Istzeit auf diese Position ![](Soll_Istzeit2.gif) angezeigt werden.

#### Wenn ich auf eine Loszeit buche wird Fehler in Zeitdaten angezeigt
Hier ist die Situation oft so, dass wenn man in einen Tag einsteigt, reinblättert, werden keine Zeitfehler angezeigt.
Klickt man dann auf eine Zeile mit einer Zeitbuchung auf ein Los, so kommt auf einmal ![](Fehler_in_Zeitdaten1.gif) Fehler in Zeitdaten. Wieso?<br>
Hintergrund ist, dass für die Vergleichsberechnung der Soll / Istzeiten die auf dem Los insgesamt gebuchte Zeit ermittelt werden muss. Sind nun auf dem Los fehlerhafte Zeitbuchungen, so kann dies nicht berechnet werden, daher wird diese Meldung angezeigt. Um zu sehen welche Buchung wann den Fehler verursacht, fahren Sie bitte mit der Maus auf die Meldung![](Fehler_in_Zeitdaten1.gif). Warten Sie kurz und es wird die Ursache ![](Fehler_in_Zeitdaten2.gif) mit Person und Datum angezeigt. D.h. wechseln Sie bitte in die Buchungen der Person dieses Tages und korrigieren Sie diesen indirekten Fehler.

#### Darstellung der Zeiten?
Werden Zeiten mit : (Doppelpunkt) getrennt dargestellt, so sind dies Angaben im Zeitsystem, also Stunden:Minuten:Sekunden. Es gibt maximal 59 Minuten bzw. 59 Sekunden. Z.B. 2:30:00

Werden Zeiten mit , (Dezimaltrenner) getrennt dargestellt, so sind das Zeiten in der definierten Mengeneinheit, üblicherweise Stunden. Z.B. 2,5Std (passend zu obigem Beispiel).

#### Wozu dient die Sonderzeitenliste?
Es gibt verschiedene Arten von Sonderzeiten die Tageweise verbucht werden müssen. Z.B. Urlaub. Diese Sonderzeiten sehen Sie in der Sonderzeitenliste, jeweils für den in der Auswahlliste gewählten Mitarbeiter. [Definition der Sonderzeiten siehe](#Weitere Sonderzeiten).

#### Beim Eintragen einer Sonderzeit (Urlaub) erhalte ich die Fehlermeldung?
![](Sonderzeit_Fehlermeldung.gif)
Hier fehlt ein gültiges Zeitmodell für den Mitarbeiter für den gewünschten Zeitraum. Tragen Sie das Zeitmodell für den Mitarbeiter im Personalmodul nach. **Hintergrund:** Sonderzeiten werden nur dann gebucht, wenn für den entsprechenden Tag Sollstunden definiert sind. Denken Sie hier z.B. an Teilzeitkräfte, welche nur 2Tage in der Woche anwesend sein sollten, oder auch an Feiertagen. D.h. auch für die Feiertage darf kein Urlaub gebucht werden.

#### Können Sondertätigkeiten auch für mehrere Personen auf einmal eingetragen werden?
Ja.
![](Sondertaetigkeit_fuer_alle.jpg)
Bei der Erfassung einer Sondertätigkeit für eine Person kann rechts neben der Anzeige der Sondertätigkeit auch angehakt werden, dass diese Sondertätigkeit für alle Personen, die noch im Unternehmen eingetreten sind, eingetragen werden sollte. Dies ist z.B. für den Betriebsurlaub sehr praktisch.

#### Buchung in der Sonderzeitenliste sperren.
Wenn der Rolle eines Mitarbeiter das negative Recht PERS_ZEITEINGABE_NUR_BUCHEN zugeordnet ist, so darf dieser Mitarbeiter den Reiter Sonderzeiten nicht aufrufen und kann damit nur mehr die Sondertätigkeiten buchen, deren stundenweise Buchung erlaubt ist. [Siehe auch](#Wer_darf_Sondertätigkeiten_buchen).

#### Meine Arztstunden werden in der Monatsabrechnung doppelt gerechnet
Die Ursache ist vermutlich, dass diese stundenweise Abwesenheit sowohl in den Zeitdaten als auch in den Sonderzeiten erfasst wurden. Beachten Sie bitte, dass sowohl die Buchungen in den Zeitdaten als auch die Buchungen in den Sonderzeiten für die Berechnung der bezahlten Abwesenheiten herangezogen werden.
Beide Buchungsarten haben ihre Vorteile.
Buchung in den Zeitdaten: Sie wissen genau wann der Mitarbeiter z.B. beim Arzt, bei einer Behörde usw. war.
Buchung in den Sonderzeiten: Der Mitarbeiter bucht auf der Zeiterfassungsstation wie üblich sein Kommt Geht. Wenn er die Besuchsbestätigung des Arztes bringt, tragen Sie in den Sonderzeiten die Stunden die er anerkannter Weise beim Arzt war ein, ohne die Kommt/Geht Buchungen Ihres Mitarbeiters zu verändern.

#### In der Monatsabrechnung werden die Mehrstunden als Überstunden ausgewiesen?
Es kommt manchmal vor, dass im Eintrittsmonat der Person das Zeitmodell erst ab Eintrittsdatum eingetragen wird, anstatt wie auch von **Kieselstein ERP** hingewiesen ab dem 1.1. des Eintrittsjahres. D.h. korrigieren Sie bitte einfach das Datum ab dem das Zeitmodell gültig ist auf den 1.1. des Eintrittsjahres.

#### Es werden keine Überstunden ausgewiesen?
Sie haben, gedanklich alles definiert um die Überstunden ausweisen zu können, aber es werden keine Überstunden errechnet.
Bitte prüfen Sie in jedem Falle ob:
- Beim Mitarbeiter ein Kollektiv/Tarifvertrag hinterlegt ist
- beim Tarifvertrag auch die Normalstunden definiert sind
[Siehe dazu auch](Ueberstundenabrechnung.htm#Überstunden ausweisen).

#### DEFAULT_ARBEITSZEITARTIKEL was ist das?
Der sogenannte default Arbeitszeitartikel (siehe dazu auch System, Parameter) dient dazu, wenn direkte / rasche Buchungen gemacht werden sollten, damit **Kieselstein ERP** weis auf welche Tätigkeit dies gebucht werden sollte. D.h. dies ist ein sehr allgemeiner Arbeitszeitartikel.
Dieser Arbeitszeitartikel muss im Artikelstamm als Arbeitszeitartikel angelegt sein.
Buchungen die diese Definition nutzen sind z.B. ![](Starte_Taetigkeit.gif) aus Projekten, Losen, Angeboten, Aufträgen. Zusätzlich ist diese allgemeine Definition Voraussetzung für verschiedene Auswertungen.
D.h. es wird gegebenenfalls eine Fehlermeldung
![](default_Arbeitszeitartikel_fehlt.jpg)
angezeigt, wenn der default_Arbeitszeitartikel nicht eingetragen oder falsch definiert ist.
Ergänzen bzw. korrigieren Sie in diesem Falle bitte diesen Parameter.

#### Was drückt die Nettoanwesenheitszeit in der Produktivitätsstatistik aus?
Die Nettoanwesenheitszeit ist jene Zeit, die der Mitarbeiter im Unternehmen ist um seiner Tätigkeit, seiner Aufgabe nachzugehen. Also die Zeit zwischen Kommt und Geht abzüglich der Unterbrechungen (Pausen), ev. Arzt und Krank Meldungen usw.

#### Gültigkeit der Zeit-Statistiken
Die Zeit der Produktivität ist höher als die Nettoanwesenheit?

A: Für die Zeitberechnungen / Zeitstatistiken wird davon ausgegangen, dass pro Mitarbeiter und Tag für den Betrachtungszeitraum ausschließlich gültige und vollständige Zeitbuchungen enthalten sind. Um dies sicherzustellen, führen Sie bitte vor Statistikauswertungen immer die Monatsabrechnung des Mitarbeiters durch. Erst wenn hier keine Fehler mehr aufgezeigt werden, sind auch die weiteren Statistiken richtig.

<a name="Produktivitätsstatistik"></a>

#### Produktivitätsstatistik 
![](Produktivitaetsstatistik.gif)

In der Produktivitätsstatistik wird am Ende die Zusammenfassung der Stunden gegliedert nach extern, intern, nicht zuordenbar angedruckt.

| Gliederung | Bedeutung |
| --- |  --- |
| Intern  | dies sind Stunden die für Aufträge, Fertigungsaufträge aufgewendet wurden, welche als Partner (Kunde) den eigenen Mandanten eingetragen haben. |
| Extern | dies sind Stunden die für Aufträge, Fertigungsaufträge aufgewendet wurde, welche als Partner (Kunde) nicht den eigenen Mandanten eingetragen haben.Diese Darstellung bedeutet nicht, dass diese Stunden tatsächlich verrechnet wurden, sondern nur, dass Sie einem Externen (= Kunde) zugeordnet werden. |
| nicht zuordenbar | dies ist die Differenz der Stunden, welche zwischen Anwesenheitszeit des Mitarbeiters und seinen verbuchten Stunden besteht. |

Je exakter die Mitarbeiter Ihre Zeiten den verschiedenen Aufträgen, Losen, Projekten zuordnen, desto genauer sind deine Nachkalkulationen. In der Praxis hat sich gezeigt, dass nicht zuordenbare Zeiten von unter 5% durchaus erreichbar sind. In reinen Fertigungsbetrieben sind nicht zugeordnete Zeiten unter 2% durchaus realistisch. Beachten Sie hier immer auch die allgemeinen Aufträge. Es gibt immer wieder Mitarbeiter, die nie über die Sollstunden kommen, aber sehr hohe Stunden auf den allgemeinen Aufträgen gebucht haben. Diese Angaben verfälschen Ihre Nachkalkulationen und müssen hinterfragt und exakt gebucht werden. Nur wenn die Zeiten den wirklichen Aufträgen zugeordnet sind, können die Auftragsnachkalkulationen für weitere Steuerungsaufgaben herangezogen werden.

**Hinweis:** In manchen Unternehmen wird die Gruppierung über Losklassen bevorzugt. Auch dies kann mit **Kieselstein ERP** dargestellt werden.

Um eine Auswertung über alle Personen und ein Monat zu erhalten, so setzen Sie bitten den Haken bei Monatsbetrachtung ![](Monatsbetrachtung_Produktivitaet.JPG).

#### 105% gibts das ?
Dies kann in der Produktivitätsstatistik vorkommen, insbesondere dann, wenn in den Zeitmodellen Rundung zu Gunsten des Unternehmens aktiviert ist. Der Hintergrund sei an einem Beispiel erklärt:
Rundung zu Gunsten des Unternehmens, 15Minuten bei Kommt und bei Geht.
| Buchung | Erklärung |
| --- |  --- |
| Kommt um 7:01 | Anerkannte Zeit ab 7:15 |
| Geht um 16:49 | Anerkannte Zeit bis 16:45 |
Wenn der Mitarbeiter nun sofort mit der Arbeit auf Aufträgen begonnen hat, so hat er um 28Minuten unbezahlt für das Unternehmen gearbeitet. Diese 28Minuten sind in seiner Anwesenheitszeit natürlich nicht mitgerechnet, in seiner produktiven Zeit auf den Projekten jedoch schon.

#### Detaillierte Produktivitätsstatistik ?
Oft ist es in Fertigungsbetrieben erforderlich, dass Überprüft wird, welcher Mitarbeiter gestern welche Mengen (Gut-/Schlecht Stück) auf welchen Arbeitsgängen, gestern produziert wurde.
Um nur den gestrigen Tag auszuwerten klicken Sie bitte auf ![](Gestern.gif) Von bis mit Gestern vorbesetzen.
Um die Details einzusehen entfernen Sie bitte den Haken bei verdichtet.

![](Detaillierte_Produktivitaet.gif)
Sie erhalten hier eine aussagekräftige Übersicht, was hat Ihr Mitarbeiter gemacht, wie lange hat er dafür gebraucht, wieviele Rüstfehler und wieviele Umspannfehler hat er gemacht. Und wie gut hat ihn die Fertigungsleitung tatsächlich ausgelastet.
D.h. hier werden die Sollstunden den Iststunden gegenübergestellt.

#### Ich verwende die Monatsbetrachtung. Sind die Zeiten da doppelt?
Wenn in der Produktivitätsstatistik die ![](Monatsbetrachtung.gif) Monatsbetrachtung gewählt wird, so wird für jeden Mitarbeiter eine entsprechende Gesamtzeile für den gewählten Zeitraum und Monat dargestellt.

![](Monats_Produktivitaetsstatistik.jpg)
Nachdem die Kette der Belege hierarchisch ist, kann es, wie in obigem Beispiel, vorkommen, dass gleiche Zeiten für verschiedene Belegarten angedruckt werden. D.h.
Ein Los kann sich auf einen Auftrag beziehen. Damit sind die produktiven Zeiten sowohl am Los als auch am Auftrag gegeben. Bezieht sich der Auftrag, bzw. das Los auch noch auf ein Projekt, so ist diese produktive Zeit auch für das Projekt gegeben. D.h. in dieser Darstellung werden unter Umständen über die Belegkette die gleichen Zeiten angezeigt.
Werden diese nun auch mehrfach gerechnet. Selbstverständlich nicht. D.h. es werden die Zeiten immer für den untergeordneten Beleg berücksichtigt. D.h. ist eine Zeit nur beim Projekt gegeben, so wird sie nur beim Projekt berücksichtigt. Kommt die Zeit eigentlich aus dem Auftrag so wird sie nur beim Auftrag berücksichtigt und kommt die produktive Zeit aus dem Los, so wird sie nur in der Spalte Los berücksichtigt.

#### Wie finde ich die in der detaillierte Produktivitätsstatistik angezeigten Mengen?
Es kommt immer wieder vor, dass die angezeigten Mengen angezweifelt werden, nicht glaubwürdig sind, hat der Mitarbeiter wirklich diese Stückzahlen gebucht.

Gehen Sie dazu in das Los, z.B. 766 und wählen Sie den Reiter Zeitdaten. Nun klicken Sie auf den gewünschten Arbeitsgang (1.Aufspannung) und klicken auf den oberen Modulreiter Gut/Schlecht.

Nun erhalten Sie alle Gut/Schlecht- Buchungen aller Mitarbeiter auf diesen einen Arbeitsgang.

![](Zeit_Buchung_Gut_Schlecht_Mengen_detailliert.gif)

Sortieren Sie diese Liste nun
1. nach Person durch Klick auf die Spaltenüberschrift und
2. nach Zeit, durch Klick auf die Spaltenüberschrift (Zeit) Halten Sie hier die Taste Strg gedrückt, damit sie eine kombinierte Sortierung nach Mitarbeiter und Zeit der Buchung erhalten.

Nun sind die Mengen klar ersichtlich.

#### Produktivitätsstatistik Projekte zusammengefasst
Diese Auswertung wird meist in Verbindung mit einer Auswertung über alle Mitarbeiter über einen gewissen Zeitraum verwendet, um festzustellen, wieviel Zeit auf die unterschiedlichsten Projekte gebucht wurde. D.h. diese Auswertung liefert die Zeiten sortiert nach den Projekten und eine Gesamtbetrachtung der verrechenbaren / nicht verrechenbaren Stunden / Zeiten.
Genau genommen ist die Sortierung in folgender Priorisierung gestaltet:
- Bereich der Projektklammer
- Projektnummer des Projektes mit Projektklammer
- Bereich des Projektes
- Projektnummer
- Angebotsnummer
- Auftragsnummer
- Losnummer
Die Darstellung ist ebenfalls in diese vier Spalten angeordnet, wobei Projektklammer und Projekt gemeinsam dargestellt werden.

#### Produktivitäts-Tages-Statistik
Oft ist auch die Entwicklung des Zeitvergleiches der Soll-Ist-Zeiten von Bedeutung.
Verwenden Sie dafür die Produktivitätstagesstatistik.
In dieser Statistik werden die Lose und die Gut-Schlecht-Stück jedes Tages mit der Dauer der Soll- und Ist-Zeiten für jeden Mitarbeiter angeführt.
Die Sollzeit wird aus der geplanten Stückzeit multipliziert mit der Anzahl der rückgemeldeten Gut und Schlecht-Stücke errechnet.
Ist die ausgewertete Zeitbuchung die erste Buchung auf dem Arbeitsgang, so wird auch noch die (Soll-) Rüstzeit hinzugezählt.
Somit erhalten Sie eine faire Betrachtung der Soll Istzeiten mit einer entsprechenden Zuteilung auf die arbeitenden Personen, auch wenn mehrere Personen am gleichen Arbeitsgang gearbeitet haben.
Mit der Monatsbetrachtung ![](Monatsbetrachtung.JPG) werden noch verdichtet Sichten je Monat in einer Grafik angezeigt und die Anwesenheitszeit je Person angeführt.
Wird zusätzlich noch die Auswertung für alle Personen durchgeführt, so wird am Ende eine Gesamtdarstellung ausgedruckt.

In dieser Auswertung wird zusätzlich der Leistungsfaktor des Mitarbeiters für das entsprechende Monat mit ausgewertet. D.h. wenn z.B. Lehrlinge / Auszubildende mit beschäftigt werden, so wird für diesen Mitarbeiter z.B. im ersten Lehrjahr nur ein [Leistungsfaktor/leistungswert](../personal/index.htm#Leistungswert) von 30% hinterlegt.
In der Auswertung für den einzelnen Mitarbeiter = Lehrling wird zur Beurteilung ein echter Soll-Ist Vergleich der Stunden ohne Berücksichtigung des Mitarbeiters vorgenommen. In der Gesamtbetrachtung der Personalgruppe werden die Iststunden mit dem jeweiligen Leistungsfaktor multipliziert und so die nicht reduzierte Leistung des Auszubildenden entsprechend berücksichtigt.
![](Produktivitaetstagesstatistik.gif)

![](Produktivitaetstagesstatistik_Auswertung.gif)

#### Produktivität über alle Projekte
In der Produktivitätsstatistik steht mit der Funktion ![](Produktivitaet_zusammengefasst.gif)Personen zusammengefasst die Möglichkeit zur Verfügung, diese Liste Projekt bzw. Auftrags orientiert über alle Mitarbeiter hinweg darzustellen.

<a name="Maschinenerfolg"></a>

#### Maschinenerfolg
In Fertigungsbetrieben bei denen ein Mitarbeiter mehrere Maschinen bedient, ist es oft ein Beurteilungsfaktor der effizienten Arbeitseinteilung des Mitarbeiters wieviele Maschinenstunden er mit seinen Anwesenheitsstunden bewirkt. Diese Auswertung finden Sie im Modul Zeiterfassung, Info, Maschinenerfolg.
Hier kann ein einzelner Mitarbeiter oder eine Personalgruppe ausgewertet werden.
In der Auswertung sehen Sie welche Maschinenlaufzeit ein Mitarbeiter pro Anwesenheitstag bewirkt hat. Daraus ergibt sich der Maschinenerfolgsfaktor. Je höher dieser Faktor, desto mehr Maschinenlaufzeit wurde vom Mitarbeiter angestoßen.
Mit der Monatsbetrachtung werden die Daten auf das jeweilige Monat verdichtet dargestellt. So wird die Entwicklung über den Zeitraum ersichtlich.
Anmerkung: Diese Auswertung wird auch Maschinenbestenliste genannt.

#### Die Stunden bei der Tages-Produktivitätsstatistik und dem Maschinenerfolg stimmen nicht überein, warum ?
Die Tagesproduktivitätsstatistik Listet die tatsächlich auf einem Los geleisteten Iststunden (Spalte Zeit) des Mitarbeiters auf. Diese differieren in aller Regel gegenüber den anwesenden Stunden.
Die Maschinenerfolgsauswertung stellt die anwesenden Stunden des Mitarbeiters den verursachten Maschinenstunden gegenüber. D.h. die Anwesenheitszeit kann nicht mit den geleisteten Stunden aus der Tagesproduktivitätsstatistik zusammenstimmen.
Eine Verbindung finden Sie in der Produktivitätsstatistik. Hier werden die anwesenden Stunden den Stunden auf Losen, Aufträgen Projekten usw. gegenüber gestellt.

#### Wie erkenne ich Buchungsfehler am raschesten?
Verwenden Sie dazu die Mitarbeiterübersicht (Modul Zeiterfassung, Info, Mitarbeiterübersicht).
Haken Sie bitte ![](Detailinfos.gif) Detailinfos an, damit die Tage mit fehlerhaften Buchungen als Err ![](Fehlerhafte_Zeitbuchung.gif) dargestellt werden. Überprüfen Sie diese Tage.

#### Wie kann ich exakte Auftragszeiten nachträglich erfassen?
Exakte Auftragszeiten erhält man nur, wenn der Beginn und das Ende der Tätigkeiten in "exakt" diesem Moment gebucht werden. Die nachträgliche Erfassung von Auftragszeiten sind reine Schätzwerte und sollten deshalb vermieden werden. Wenn es aufgrund organisatorischer Gründe nicht anders möglich ist, als Auftragszeiten nachträglich zu erfassen, so aktivieren Sie bitte die relative Zeiteingabe im Zeiterfassungspanel.

#### Wozu diese komplizierte Erfassung?
Die **Kieselstein ERP** Zeiterfassung ist grundsätzlich für eine positionsgenaue Erfassung der Auftragszeiten ausgelegt.

Das bedeutet für die Erfassung einer Tätigkeit müssen

![](ZE_Auftrag.gif)

angegeben werden. Das bedeutet dass eine exakte Zeiterfassung auf die einzelne Position möglich ist.

Ein Beispiel aus der Softwarebranche:

Der Auftrag 05/0000024 lautet auf Entwicklung Finanzbuchhaltungsprogramm

Im Auftrag sind die Positionen:

| Pos | Dauer | Bezeichnung |
| --: |  --: |  --- |
| 1 | 10 Std | Konzept |
| 2 | 100 Std | Design |
| 3 | 80 Std | Programmierung |
| 4 | 5 Std | QS/Test |

Während der Abwicklung dieses Auftrages ergibt sich nun folgendes:

| MA | Dauer | Tätigkeit | Auftragsposition | Bemerkung |
| --- |  --: |  --- |  --- |  --- |
| JO | 3 Std | Analyse | 1 ... Konzept |  |
| MB | 8 Std | Analyse | 1 ... Konzept |  |
| MB | 25 Std | Entwurf | 2 ... Design |  |
| VF | 15 Std | Test | 2 ... Design | geht also trotz der anderen Tätigkeit auf die Auftragsposition Design, wodurch die Nachkalkulation wesentlich transparenter wird. |

Durch die Buchung der Tätigkeit Test auf die Designphase kann man in der Nachkalkulation ohne lange Rückfrage erkennen, dass hier gänzlich andere Tätigkeiten als geplant erforderlich waren, um diese Auftragsposition zu erfüllen.

**ABER!**

In vielen Branchen / Unternehmen ist diese sehr detaillierte Erfassung zu genau, verursacht einen zu hohen Aufwand. Daher kann im System, Parameter die positionsweise Erfassung abgeschaltet werden. Zusätzlich kann dort auch die Erfassung der Tätigkeit so umgestellt werden, dass keine Tätigkeit abgefragt wird und immer nur auf die in den Mandantenparametern fix hinterlegte Tätigkeit gebucht wird.

Dies bewirkt, dass nur die Auftragsnummer angegeben werden muss.

#### Bei der Erfassung der Zeiten sollen nur die Lose angezeigt werden, bei denen der Mitarbeiter als Techniker eingetragen ist.
<a name="Losauswahlliste_für_Techniker"></a>
Schalten Sie dazu den Parameter LOSAUSWAHL_TECHNIKERFILTER auf 1.

Wird nun für die Belegart Los / Fertigungsauftrag die Liste der offenen Lose angezeigt, so werden nur diejenigen Lose aufgelistet, bei denen die ausgewählte Person als Techniker hinterlegt ist. Zusätzlich können durch Klick auf "alle Lose" alle offenen Lose unabhängig vom hinterlegten Techniker angezeigt werden.

Hinterlegen mehrere Techniker siehe.

#### Wie findet man heraus, welchem Mitarbeiter welches Kurzzeichen gehört?
Wechseln Sie in das Modul Benutzer.

Hier finden Sie eine Übersicht aller definierten Benutzer. In der letzten Spalte wird das Kurzzeichen des jeweiligen Benutzers angezeigt. Durch einen Klick auf die Spaltenüberschrift Kurzzeichen wird diese Liste nach Kurzzeichen sortiert.

Zusätzlich sehen Sie in der Detailsicht in der Zeile Personal ebenfalls das Kurzzeichen.

![](personal_detail.jpg)

<a name="Monatsabrechnung"></a>

#### Wie ist die Zeitenzusammenfassung der Monatsabrechnung zu lesen?

![](Monatsabrechnung.gif)

Bereich Urlaub
| Feld | Beschreibung |
| --- |  --- |
| Resturlaub | Noch bestehender Urlaubsanspruch aus den Vorjahren des Auswertejahres |
| Anspruch aliquot | Der aliquote Urlaubsanspruch wird wie folgt berechnet:<br>Januar bis November je zwei Tage pro Monat. Für den Dezember drei Tage. Die Stunden werden anhand des jeweiligen Zeitmodells jeweils für die durchschnittlichen Monatsstunden pro Tag aus den Urlaubstagen errechnet. Die Berechnung der Tage des Beginnmonates werden auf die Arbeitstage (inkl. Feiertagen) des Monates umgerechnet und kaufmännisch gerundet. |
| Anspruch aktuell verbraucht | Seit 1.1. des Auswertejahres bis zum Ende des Abrechnungszeitraums verbrauchter Urlaub |
| Verfügbarer Urlaub | daraus ergibt sich der noch nicht verbrauchte Urlaub zur Basis des aliquoten Anspruches |
| Urlaubsanspruch aktuell | Gesamtanspruch des Abrechnungsjahres |
| Urlaub geplant | Nach dem Abrechnungszeitpunkt bereits eingeplanter Urlaub |

Linker Bereich
| Feld | Beschreibung |
| --- |  --- |
| Ist | Echte Anwesenheit im Unternehmen |
| Summe zus. Sondertätigkeiten | Plus ... Summe aus Feiertagsstunden, Arzt, Krank, Behörde, Urlaubsstunden und sonstiger bezahlter Stunden |
| Tatsächliches Ist | Ergibt ... anerkannte Stunden für die Abrechnung |
| Soll | Abzüglich ... Sollstunden des Monates |
| Feiertagssoll | Abzüglich ... Feiertagsstunden des Monats(Trennung um echte Sollstunden erkennen zu können) |
| DRZ-Pauschale | abzüglich Pauschale für den Durchrechnungszeitraum |
| Gleitzeitsaldo für Monat Jahr | Ergibt ... Gleitzeitsaldo für das gedruckte Monat |
| Überstundenpauschale | Abzüglich ... Überstundenpauschale |
| Übertrag vom Vormonat | Plus ... Übertrag aus Vormonat |
| Ausbezahlte Stunden | Abzüglich ... Ausbezahlte Stunden |
| Verbrauchte Überstunden lfd. Mnt | Plus ... die aus dem Überstundentopf verbrauchten Überstunden |
| Verfallende Überstunden | Abzüglich ... verfallende ÜberstundenSiehe Zeitmodell, Kopfdaten, Maximale Wochen-Iststunden bzw. maximal erlaubte MehrzeitD.h. durch die Begrenzung werden diese Stunden nicht mehr weiter angerechnet und verfallen daher. |
| passive Reisezeit | Der Anteil der angerechneten Gleitzeitstunden aus den anerkannten passiven Reisezeiten. [Siehe](Reisezeiten.htm#Passive Reisezeit). |
| Gleitzeitsaldo | Ergibt ... den aktuellen Gleitzeitsaldo des Monates. Dies ist zugleich der Übertrag für das Folgemonat |
| Überstunden in Normalstunden (aus Vormonaten) | Übertrag des Vormonates der Überstunden über die Faktoren umgerechnet in Normalstunden |
| Ausbezahlte Überstunden in Normalstunden (aus Vormonaten) | Ausbezahlte Überstunden des Vormonates über die Faktoren umgerechnet in Normalstunden |
| Überstunden in Normalstunden(aktueller Gesamtanspruch) | Aktueller Gesamtanspruch an Überstunden umgerechnet in Normalstunden |
| Gesamtsaldo inkl. Normalstunden aus Überstunden | Die Addition aus dem Gleitzeitsaldo des gedruckten Monates zzgl. des aktuellen Überstundenanspruches umgerechnet in Normalstunden |

{{% alert title="ACHTUNG" color="warning" %}}
Wichtige Voraussetzung für die richtige Urlaubsabrechnung ist neben der richtigen Eingabe der Ansprüche, vor allem, dass ab dem Eintritt des Mitarbeiters auch entsprechende Zeitmodelle hinterlegt sind. Wurde dem Mitarbeiter erst nach seinem Eintritt ein Zeitmodell zugeordnet, so erfolgen die Abrechnungen wie wenn der Mitarbeiter erst mit dem Zeitmodell eingetreten wäre. Da vor allem mit den Themen der Kurzarbeit eine taggenaue Berechnung der Urlaubsansprüche bei wechselnden Zeitmodellen erforderlich ist, wird der Urlaubsanspruch immer ab 1.1. des Kalenderjahres ermittelt. Das bedeutet, dass das erste Zeitmodell eines Mitarbeiters immer ab dem 1.1. des Eintrittsjahres definiert sein muss.
Bitte bedenken Sie, dass die Urlaubsabrechnung mit einem eventuell eingetragenen Austritt beendet wird.
{{% /alert %}}

#### Urlaubstage / Urlaubsanspruch bei wechselnden Zeitmodellen
Insbesondere wenn bei Teilzeitkräften Zeitmodelle zum Einsatz kommen, die keine volle Woche beanspruchen und die Urlaubsabrechnung auf Basis der Urlaubstage erfolgt (was generell so sein sollte), muss berücksichtigt werden, dass die Urlaubsanspruchswochen entsprechend auf Tage umzurechnen sind.<br>
Beispiel:<br>
| Zeitbereich | Dauer in Wochen | Aktive Tage pro Woche | Basistage | Jahres-Urlaubsanspruch |
| --- | :-: | :-: | :-: | -- | 
| KW01 bis KW10 | 10 | 2 | 20 | 1,92 |
| KW11 bis KW30 | 20 | 3 | 60 | 5,77 |
| KW31 bis KW45 | 15 | 4 | 56 | 5,77 |
| KW46 bis KW52 |  7 | 5 | 20 | 3,37 |
| Gesamt Tage | | | 175 | 16,83 |
| Voller Jahresurlaub | 52 | 5 | 260 |
Bei einem Jahresurlaubsanspruch von fünf Wochen = 25Tage
Also erhält die Person für die KW01 - KW10 25/260*20 = 1,9 Urlaubstage<br>
Bzw. kann man einfach die Gesamtjahrestage heranziehen = 25/260*175 = 16,83 Urlaubstage für dieses Jahr. [Siehe dazu gerne auch](./urlaubsabrechnung.ods)


<a name="Normalstunden / Überstundentopf"></a>

#### Wo sieht man den Normalstundentopf und den Überstundentopf bzw. die getrennte Abrechnung?
Wenn hier eine exakte Trennung gewünscht ist, so nutzen Sie bitte die Kollektivvertragsart Betriebsvereinbarung Version A.

#### Warum wird das Feiertagssoll extra ausgewiesen?
Als Unternehmer interessiert, wie viele Stunden steht das Team im Monat / Jahr effektiv zur Verfügung. Je genauer man diese Zahl kennt, desto besser lässt sich planen. So ist aus obigem Beispiel, einem Mai nach österreichischer Regelung erkennen, dass z.B. im Gegensatz zu einem März des gleichen Jahres, anstatt 176Std (geplanter Arbeitszeit) im Mai nur eine Sollzeit von 152Std gegeben ist und zusätzlich 32Feiertagsstunden zwar zu bezahlen sind, diese aber von den Mitarbeitern (laut Kollektiv- / Tarifverträgen) nicht erbracht werden.
Dies ist auch ein Faktor dafür, welche Mehrleistung erbracht werden muss um den Mitarbeitern die gewünschten Stundensätze auch für die bezahlte Nicht-Anwesenheit bezahlen zu können.
In manchen Betrieben ist es durchaus üblich, trotz der vielen Feiertage, welche oft auch noch mit den Fenster-/Brückentagen kombiniert werden, den gleichen Output anzustreben, wobei dies oft von deren internationalen Kunden, welche kein Verständnis für die österreichischen Feiertage haben, als selbstverständlich angesehen wird.

#### Kann die Monatsabrechnung auch versandt werden?
Die Monatsabrechnung kann auch per EMail versandt werden. Dafür muss einerseits die Sozialversicherungsnummer des Mitarbeiters mit einer mindesten Stellenanzahl von 10 Stellen und andererseits auch die persönliche = private EMailadresse des Mitarbeiters (Personal, Reiter Detail) hinterlegt sein.
Beim Versand wird durch Klick auf  EMail ![](EMail.gif) die Monatsabrechnung als PDF-Anhang mit einem kurzen Begleittext an den/die Mitarbeiter versandt. Das PDF ist mit der Sozialversicherungsnummer bzw. dem alternativen Parameter (siehe unten) als Passwort geschützt.

Hinweis: Wenn beim Export eine Fehlermeldung ähnlich<br>
![](EMail_Versand.jpg)<br>
auftritt, so tragen Sie bitte bei den Mitarbeitern die fehlenden Daten nach. Danach starten Sie die Monatsabrechnung neu, damit die neuen Daten übernommen werden und so an die Mitarbeiter versandt werden können.<br>
Hinweis: Mache Anwender nutzen dies auch dafür, z.B. die Monatsabrechnung für die Geschäftsleitung nicht zu versenden.

Beim Erzeugen / Versand der Monatsabrechnungen wird die gewählte Reportvariante verwendet.

Alternativ kann auch ein eigenes Passwort vergeben werden. Dieses muss im Personal, Reiter Parameter unter Versand-Kennwort definiert werden. D.h. ist das Versand-Kennwort definiert, wird dieses verwendet. Ist dieses nicht definiert, wird die Sozialversicherungsnummer verwendet, ist auch diese nicht bzw. falsch definiert, kann die Monatsabrechnung nicht versandt werden.

Die Reihenfolge ist, dass zuerst das alternative Passwort und danach die Sozialversicherungsnummer verwendet wird. Ist beides nicht gegeben, so erscheint der Mitarbeiter in obiger Fehlerliste.

#### Können die Monatsabrechnungen auch automatisch versandt werden?
Es steht auch ein Automatik-Job für den Versand der Monatsabrechnungen zur Verfügung. Die Idee dahinter ist, dass Ihre Mitarbeiter regelmäßig z.B. wöchentlich die aktuelle Info über ihre anwesenden Stunden erhalten.
Optionen des Automatikjobs:
- wöchentlich + Wochentag -> Es wird täglich geprüft, ob heute der gewählte Wochentag ist. Ist dies der Fall, werden die Monatsabrechnungen an alle Mitarbeiter, welche eine Email-Adresse haben, verschlüsselt anhand der Sozialversicherungsnummer versendet.
- monatlich -> Es wird jeden Tag geprüft, ob heute der 1\. des Monats ist. Ist dies der Fall, so wird die Monatsabrechnung des kompletten Vormonats versandt.

Wichtig: Als Absender wird die E-Mail-Absenderadresse des lpwebappzemecs verwendet, wenn nicht vorhanden, wird dies im Server-Log-File als Fehler vermerkt.

Da üblicherweise nur eingetretene Mitarbeiter diese Daten erhalten, wird die Monatsabrechnung generell nur für die Mitarbeiter erstellt, die zum ersten des jeweiligen Monates der Abrechnung eingetreten sind.

#### können auch weitere Unterlagen mitgesandt werden?
Oft ist es auch gewünscht, dass neben der reinen Monatsabrechnung auch z.B. die Gehaltsabrechnung im gleichen Vorgang an den/die Mitarbeiter gesandt werden.
Stellen Sie dafür die Parameter:
MONATSABRECHNUNG_VERSAND_PFAD = Pfad aus Server Sicht
MONATSABRECHNUNG_VERSAND_PREFIX = Prefix der zu versendenden Dateien
entsprechend ein.
Anmerkung: Ist der Parameter MONATSABRECHNUNG_VERSAND_PFAD ein "." so ist der Versand der zusätzlichen Daten (Anhang) abgeschaltet.
Zusätzlich gilt die Vereinbarung, dass die Dateien nach dem eventuellen Prefix die Personalnummer enthalten müssen und danach eine beliebige Dateinamenserweiterung z.B. .PDF.
Bitte beachten Sie, dass von **Kieselstein ERP** diese Dateien, so wie sie in dem Verzeichnis abgelegt wurden, versandt werden. Es wird **keine** wie immer geartete **Verschlüsselung** (der zusätzlichen) Dateien vorgenommen. Das bedeutet, dass z.B. die Gehaltsunterlagen bereits verschlüsselt abgelegt werden sollten.
So könnte für den Mitarbeiter mit der Personalnummer 4711 die Lohnabrechnung z.B. LOHN4711.PDF lauten. Dafür muss der Parameter MONATSABRECHNUNG_VERSAND_PREFIX auf LOHN eingestellt werden.

Das bedeutet auch, dass die Versandlogik insofern erweitert wurde, dass der Unterlagenversand zweigeteilt ist.
1. Ist eine Sozialversicherungsnummer bzw. das Versand-Kennwort für diese Person eingetragen, so wird die Monatsabrechnung für diese versandt.
2. Unabhängig von 1. wird, wenn für eine Person ein Anhang vorhanden ist, so wird dieser mit-versandt.
Das bedeutet auch, dass z.B. für die Geschäftsleitung zwar keine Monatsabrechnungen erstellt werden, da keine Sozialversicherungsnummer eingetragen, aber doch die bereits verschlüsselt erhaltenen Lohn/Gehaltsabrechnungen mit diesem Automatismus versandt werden können.
Wenn eine (Lohn-) Datei versandt, so wird diese in das Unterverzeichnis OLD_DATUM verschoben.

Was bedeutet Pfad aus Server Sicht?
Da der gesamte Versand vom **Kieselstein ERP** Applikationsserver gemacht wird, welcher üblicherweise als Dienst / Service eingerichtet ist, muss der Applikationsserver auf den angegebenen Pfad volle Zugriffsrechte haben. Denken Sie bitte auch daran, dass je nach Betriebssystemumgebung der **Kieselstein ERP** Server als Dienst mit einem Benutzerkonto eingerichtet sein muss, um als dieser Benutzer Zugriff auf die anderen Verzeichnisse zu bekommen.

#### Die Monatsabrechnung dauert (viel zu) lange
Gerade in Installationen bei denen bestehende Daten von anderen Systemen übernommen wurden kommt es manchmal vor, dass (anfänglich) die Monatsabrechnung deutlich länger dauert als man dies erwarten würden.
Hintergrund ist folgender:
In der Monatsabrechnung muss auch der aktuelle Urlaubsanspruch berechnet und gegebenenfalls aufgerollt werden. Die Urlaubsaufrollung funktioniert nun wie folgt:
Ist im aktuellen Jahr kein Urlaub eingetragen, so wird der Urlaubsanspruch vom Vorjahr verwendet. Ist auch hier kein Anspruch eingetragen so wird wiederum das Vorjahr verwendet. Es wird hier bis zum Eintrittsdatum des Mitarbeiters zurückgesucht und von da weg der Urlaub aufgerollt. Ist nun, z.B. bei übernommenen Daten noch überhaupt kein Urlaubsanspruch eingetragen, so wird immer der gesamte Buchungszeitraum aufgerollt, da ja kein Anspruch in den Übertragungsbuchungen eingetragen werden kann. Dies kann bei großen Zeiträumen (zig-Jahre), also bei langgedienten Mitarbeitern doch einige Zeit in Anspruch nehmen.
Sorgen Sie daher dafür, dass die Urlaubsansprüche bei allen Mitarbeitern ab dem Eintrittsdatum eingetragen sind. Bei Datenübernahmen korrigieren Sie gegebenenfalls den Anspruch zum Datenübernahmestichtag (Urlaubsjahreswechsel) und setzen Sie für diesen Eintrag das eingefroren Flag.

#### Wieso wird unter Anspruch aliqout etwas anderes angezeigt als unter Ende des Jahres ?
![](Urlaubanspruchdarstellung1.gif)

Anspruch aliquot

![](Urlaubanspruchdarstellung2.gif)

Unter Anspruch wird der alte gesamte Rest-Urlaubsanspruch aus den Vorjahren und der verbrauchte Urlaub aus dem Vorjahr angegeben um Ihnen eine bessere Detaillierung zu geben.

Bei

Anspruch bis Ende des Jahres

![](Urlaubanspruchdarstellung3.gif)

wird der verbleibende Saldo aus dem Rest-Urlaubsanspruch abzüglich dem verbrauchten Urlaub aus dem Vorjahr angegeben. D.h. das Ergebnis ist das gleiche, bei der Darstellung Urlaub Aliquot wird Anspruch und Verbrauch detaillierter dargestellt.

#### Kann eine Überstundenkontrollliste aus der Monatsabrechnung angedruckt werden?
In manchen Firmen ist es erforderlich, dass Überstunden von den Abteilungsleitern nachträglich freigegeben werden müssen. Ein Beispiel ist z.B. eine Tiergeburt bei der der Tierpfleger so lange da ist bis das junge Lebewesen zur Welt gekommen ist. Da hat er seine Überstunden einfach zu leisten. Trotzdem muss natürlich sein Vorgesetzter wissen, welche außergewöhnliche Zeiten denn so angefallen sind. Wenn dies gewünscht wird, so gibt es dafür in der Regel die Bedingung, dass man die Monatsabrechnungen aller derjenigen Mitarbeiter sehen möchte, die eine gewisse Stundenanzahl an zumindest einem Tag überschritten haben. Wenn dies für Sie zutreffend ist, so tragen Sie bitte im Parameter MONATSABRECHNUNG_NUR_AB_TAGESIST die entsprechenden Stunden (z.B. 10Std / Tag) ein. Wenn Sie nun die Monatsabrechnung neu aufrufen, so wird im Reportdialog der Monatsabrechnung auch die Möglichkeit
![](maximale_Tagesstunden.gif)"nur anzeigen wenn Tagesist-Summe an mindestens einem Tag >= xx Stunden ist" angeboten.
Damit werden nur für diejenigen MitarbeiterInnen die Monatsabrechnungen ausgedruckt bei denen zumindest an einem Tag im Monat die angegebene Ist-Stunden inkl. der dazu gerechneten Zeitgutschriften die angegebene Stundenanzahl überschreiten. Damit Sie sehen, an welchen Tagen dies ist, werden die Iststunden des jeweiligen Tages Fett angedruckt.

#### Wie mit akzeptierten Mehrleistungen umgehen ?
Es kommt immer wieder die Frage, wie verbucht akzeptiert man ausnahmsweise mehr geleistete Arbeitszeit?
Der Hintergrund:
Im Zeitmodell ist. z.B. definiert, dass Früheste Kommtzeit 7:00 und späteste Gehtzeit 19:00 ist. Zusätzlich ist die maximale Anwesenheitszeit auf 10Std. begrenzt. Nun ergibt sich, dass der Mitarbeiter für dringende Arbeiten benötigt wird und er daher mehr Stunden arbeitet. Andererseits ist er aber, wie üblich schon um 6:44 gekommen, hat seine Pausen gemacht, ist aber, ganz im Sinne des Unternehmens bis 20:15 geblieben und hat seine Arbeit fertig gemacht. D.h. er hat, ausnahmsweise, an diesem Tag anstatt der erlaubten 10Std. 12,75Std.. Wie können nun am einfachsten diese akzeptierten Mehrleistungen zugebucht werden.
Eine Möglichkeit ist, insbesondere wenn das regelmäßig vorkommt, dass Sie im Zeitmodell frühere Anwesenheitszeit akzeptiert bis bzw. längere Anwesenheitszeit akzeptiert ab (z.B. 20:00) eintragen.
Eine andere Möglichkeit ist, insbesondere wenn das eine Ausnahme ist, dass Sie eine Sondertätigkeit erlaubte Mehrstunden anlegen und dann dem Mitarbeiter genau für diesen Tag die vom Vorgesetzten / der Geschäftsleitung anerkannten Mehrstunden im Reiter Sonderzeiten unter Angabe der Stunden und Minuten eintragen. Passend zu unserem Beispiel 2:45.

#### Die in der Monatsabrechnung ausgewiesenen Ist-Stunden stimmen mit den Tagessummen des Zeitdatenjournals nicht überein?
Die Buchungen im Zeitdatenjournal sind als Journal, also die Darstellung der echten Buchungszeitpunkte gedacht. Wird nun z.B. in den Zeitmodellen eine Einschränkung, Ergänzung zu den mehr oder minder angerechneten Zeitbuchungen eingetragen, so müssen die errechneten Zeiten aus dem Zeitdatenjournal gegenüber der Monatsabrechnung abweichen.
Felder der Zeitmodelle die sich in dieser Form auswirken können sind:
- täglicher Zeitabzug
- maximale Wochen-Iststunden
- Zeitgutschrift Kommt / Geht
- Frühest erlaubte Kommt Zeit bzw. Späteste Gehtzeit
- maximal erlaubte Anwesenheitszeit

#### In welcher Sprache wird die Monatsabrechnung ausgedruckt?
Die Monatsabrechnung wird in der Sprache des Mandanten ausgedruckt, da davon auszugehen ist, dass diese Unternehmenssprache alle Mitarbeiter des Unternehmens verstehen. D.h. wenn Ihr Tochterunternehmen z.B. in polnisch oder slowenisch geführt wird, so muss der entsprechende Mandant im Partner auf die gewünschte Sprache gestellt sein.

#### Wo sehe ich wer anwesend ist und wer nicht?
Dies sehe Sie in der Anwesenheitsliste. Diese kann auf zwei Arten abgerufen werden.
a.) Im **Kieselstein ERP** Client im Modul Zeiterfassung ![](Zeiterfassung.gif), Modul Menü Journal die Anwesenheitsliste. Hier wird der Status aller Personen des jeweiligen Mandanten angezeigt. Voraussetzung ist, dass die Anzeige in der Anwesenheitsliste im Personalmodul nicht abgeschaltet wurde (Personal, Daten, ![](Anwesenheitsliste.gif))
In Klammern werden die Belege angeführt, auf die vor einer Sondertätigkeit zb. Pause Zeit gebucht wurde.
In dieser Liste werden auch die Einstellungen Anwesenheitsliste für Abteilungen (Modul Personal, Reiter Daten) berücksichtigt. Damit erreichen Sie, dass z.B. die Abteilungsleiter nur die Anwesenheitsliste der Mitarbeiter der eigenen Abteilung sehen.
Grundsätzlich kommen hier folgende Benutzerrechte zur Anwendung:
PERS_ANWESENHEITSLISTE_R ... Darf der Benutzer grundsätzlich die Anwesenheitsliste sehen
PERS_SICHTBARKEIT_ALLE ... Darf der Benutzer alle Mitarbeiter sehen
PERS_SICHTBARKEIT_ABTEILUNG ... Darf der Benutzer (nur) die Mitarbeiter seiner Abteilung sehen
Wenn der Benutzer nur das Recht der Anwesenheitsliste hat, aber die PERS_SICHTBARKEIT(s Rechte) nicht hat, so sieht er nur seine eigene Anwesenheit.
In der Anwesenheitsliste steht auch die Buchungsquelle zur Verfügung. Damit sehen Sie, an welchen Geräten, z.B. Terminalnamen und somit Standorten der Geräte, sich der Mitarbeiter eingebucht hat bzw. von welchem Gerät aus die Buchung bearbeitet worden ist.

b.) Im ihrem Internetbrowser. [Siehe](../System/Web_Links.htm#Anwesenheitsliste).
**Wichtig:** Dieser Zugriff ist nicht durch Benutzerberechtigungen geschützt. Die Daten stehen also Allen die in Ihrem Netzwerk Zugriff auf Ihren Server haben zur Verfügung.

<a name="Abwesenheitsarten"></a>Abwesenheitsarten

Bitte beachten Sie, dass im Zuge der DSGVO keine Informationen mehr zur Art der Abwesenheit an z.B. alle Mitarbeiter gegeben werden dürfen.
Dafür haben wir im Personal, Grunddaten, Abwesenheitsart definierbar gemacht. D.h. hier werden fix die möglichen Übersetzungen gepflegt. Bitte beachten Sie dazu, dass die Kennung fest verdrahtet sind. So bedeutet immer:
1 ... anwesend
2 ... Pause
3 ... außer Haus
4 ... noch keine Buchung
5 ... (geplant) abwesend
Sie können diese Worte gerne anders übersetzen, gerne auch in anderen Sprachen. Die Bedeutung dafür muss erhalten bleiben. Beachten Sie auch die sich aus der DSGVO gegebenen Vorschriften.
Die Art der Abwesenheit muss dann im Modul Zeiterfassung, unterer Reiter Sondertätigkeiten zu jeder Sondertätigkeit zugeordnet werden.

#### Ich möchte in der Anwesenheitsliste auch die Vortage sehen
Die Anwesenheitsliste ist nur für die aktuelle Darstellung der von Ihren Mitarbeitern im Moment durchgeführten Tätigkeiten gedacht. Für die Darstellung der vergangenen bzw. geplanten An- und Abwesenheitszeiten verwenden Sie bitte die Mitarbeiterübersicht aus dem Modul Zeitverwaltung, Info, Mitarbeiterübersicht.

![](MitarbeiterZeituebersicht.jpg)

Hier sehen Sie die aktuellen gearbeiteten Stunden, eventuelle Urlaube, Krankenstände usw. sowie die Gesamtsumme je Mitarbeiter pro Monat und die gesamte von Ihren Mitarbeitern in diesem Monat erbrachte Stundenleistung.

Zusätzlich kann mit dieser Darstellung eine Übersicht über die Arbeitsleistung des gesamten Jahres dargestellt werden.

#### Die Tagesarbeitszeiten zwischen Monatsabrechnung und Mitarbeiterübersicht stimmen nicht zusammen?
In der Mitarbeiterübersicht werden die tatsächlich gebuchten Zeiten herangezogen. Für die Mitarbeiter Monatsabrechnung werden auf den gebuchten Zeiten Rundungsregeln angewandt, wie z.B. Rundung auf 5 Minuten zu Gunsten des Unternehmens oder Kommt Buchung ist erst ab 6:45 erlaubt. Daraus können sich Differenzen Darstellung gegenüber der Monatsabrechnung ergeben. Das bedeutet jedoch auch, dass die in der Mitarbeiterübersicht angeführten Stunden immer höher sind, als die tatsächlich abgerechneten Stunden.

#### Darstellung des 13Wochen Durchrechnungszeitraumes
Mit Info, Wochenabrechnung erhalten Sie Wochenweise Soll- Istdaten Ihrer Mitarbeiter um den für den dreizehn Wochen Durchrechnungszeitraum erforderlichen Stundenausgleich ermitteln zu können.

Diese Darstellung können Sie auch für andere Zeiträume, z.B. für ein gesamtes Jahr verwenden und so pro Mitarbeiter auf einem Blatt seine Stundenentwicklung / -ansprüche darstellen.

![](Wochenabrechnung.gif)

#### Zeitsaldo
Für eine kompakte Darstellung der aufgewendeten Zeiten Ihrer gesamten Mannschaft pro Monat kann das Journal der Zeitsaldi verwendet werden. Auch hier erhalten Sie pro Mitarbeiter für das ausgewählte Monat die entsprechende Darstellung der Zeiten auf den verschiedenen Zeitarten. **Wichtig:** Um richtige Daten zu erhalten ist es erforderlich, dass vorher die Monatsabrechnungen der Mitarbeiter der entsprechenden Monate ausgedruckt wurden. Nur beim Druck der Monatsabrechnungen werden die Daten (Überstunden, Gleitzeitsaldi, usw.) tatsächlich errechnet und in der **Kieselstein ERP** Datenbank abgelegt. Der Druck des Zeitsaldojournals errechnet keine neuen Daten, sondern greift auf diese bereits errechneten Monatswerte zurück.
Sollten Sie die Auswertung des Zeitsaldos für weitere Auswertungen verwenden, finden Sie [hier eine Beschreibung]( {{<relref "/fertigung/zeiterfassung/zeitsaldo_feldbeschreibung">}} ) der einzelnen Felder dieses Reports.

#### Was sind Sollstunden?
Sollstunden oder auch Sollzeiten sind jene Stunden, die der Mitarbeiter laut Vereinbarung (Arbeitsvertrag o.ä.) erbringen sollte. Die Sollstunden werden in den Zeitmodellen definiert.

#### Was sind Iststunden?
Iststunden oder auch Istzeiten sind jene Stunden die der Mitarbeiter tatsächlich geleistet hat. Um die auszubezahlenden, im Gleitzeitsaldo zu berücksichtigende Stunden zu ermitteln werden zu den Iststunden noch weitere Stunden für bezahlte Nichtanwesenheit hinzugerechnet. Z.B. Urlaub, Feiertag, Krank usw.

#### Wie werden Feiertage berechnet / eingerechnet?
Für die Berücksichtigung der Feiertage müssen diese im Feiertagskalender eingetragen sein. Bitte beachten Sie, dass für die Gültigkeit auch die  Konfessionen der Mitarbeiter wichtig sind. Beispiel: Karfreitag für Angehörige der Evangelischen Glaubensgemeinschaft in Österreich.

Grundsätzlich sind Feiertagsstunden so zu sehen wie Ist-Stunden, die aber nicht geleistet wurden. Die Berechnung der Feiertagsstunden, welche dem Mitarbeiter als quasi Iststunden gutgeschrieben werden, erfolgt folgendermaßen:

Tages-Sollstunden

abzgl. Feiertags-Sollstunden

ergibt Feiertags-Gutstunden

Beispiel: Nationalfeiertag 26.10.2005, ist ein Mittwoch

|  | Mitarbeiter A (Normalfall) | Mitarbeiter B muss an diesem Tag 3Stunden arbeiten | Mitarbeiter C (Pflege-Vereinbarung) hat eine volle Arbeitsverpflichtung |
| --- |  --- |  --- |  --- |
| Tagessollstunden für Mittwoch | 8 | 8 | 8 |
| Feiertagssollstunden für die Tagesart Feiertag1 | 0 | 3 | 8 |
| Feiertagsgutstunden | 8 | 5 | 0 |

Um ganze oder halbe Feiertage für Tage mit anderen Sollzeiten verwenden zu können, müssen eigene Feiertagstagesarten in den Zeitmodellen definiert werden. Um dies etwas zu vereinfachen, haben wir dies so definiert, dass es keine negativen Feiertagsgutstunden geben kann. D.h. würde die Feiertagssollzeit größer als die Tagessollzeit sein so wird die Feiertagssollzeit für die Berechnung dieses Tages auf die Tagessollzeit reduziert.

Beispiel: Der 24.Dezember, welcher in vielen Branchen ein halber Feiertag ist, fiel 2004 auf einen Freitag mit 6Tagessollstunden und fiel 2003 auf einen Mittwoch mit 8Tagessollstunden. Um dies korrekt abzubilden müssen zwei unterschiedliche Feiertagsarten in den Zeitmodellen definiert werden. Einer mit 4Feiertagssollstunden und einer mit drei Feiertagssollstunden.

Hinweis: Für Teilzeitkräfte gilt normalerweise ein Durchrechnungszeitraum von 13Wochen. D.h. für die anerkannten Stunden anhand der Feiertage gilt der Tagesdurchschnitt der letzten 13 Wochen anhand der Wochentage mit Sollstunden. Diese werden bei der Feiertagsberechnung als quasi Iststunden verwendet. Der Umrechnungsfaktor vom theoretischen Soll (wie in der Tabelle oben) zum tatsächlichen Ist wird ebenfalls anhand obigem Beispiel durchgeführt. D.h. für einen Teilzeitmitarbeiter würden im Fall 2 (3Std. Sollzeit) 5/8 seines Tagesdurchschnitts als Feiertagsgutstunden gewertet. [Siehe auch](../personal/Urlaubsanspruch.htm#Halbtag)

#### Anrechnung von Urlaubsteilen auf den Gleitzeitsaldo
Buchen Sie einfach die vereinbarten Urlaubstage zusätzlich zu den geleisteten Arbeitsstunden auf die gewünschten Tage. Dadurch erhält der Mitarbeiter zusätzliche Urlaubs-Gut-Stunden was seinen Gleitzeitsaldo wiederum erhöht (ausgleicht) und zusätzlich werden die verbuchten Urlaubstage in der Urlaubsabrechnung berücksichtigt.

#### Warum werden in der Urlaubsabrechnung keine / falsche Stunden angezeigt
![](Urlaubsabrechnung.gif)

Aus diesem Bild ist ersichtlich, dass keine Urlaubsstunden berechnet werden konnten. Der Hintergrund ist meist, dass für diese Person für diesen Zeitraum kein gültiges Zeitmodell hinterlegt ist. Daher können die Urlaubstage nicht in Urlaubsstunden umgerechnet werden. Bitte definieren Sie ein gültiges Zeitmodell für Ihren Mitarbeiter.

#### Stundenweise Urlaubsabrechnung, was ist zu beachten?
Die stundenweise Urlaubsabrechnung wurde als zusätzliches Feature erstellt. Laut Gesetzgeber ist Urlaub grundsätzlich nur Tagesweise, in Ausnahmefällen Halbtageweise zu gewähren. Kürzere freie Zeiten sind nicht als Urlaub zu werten.
Trotzdem kann, auch für eine gerechtere Abrechnung, in **Kieselstein ERP** der Urlaubsanspruch stundenweise definiert werden. Bitte beachten Sie, dass die Definition der Sollstunden derzeit aus dem Zeitmodell kommt und diese mit dem stundenweisen Urlaubsanspruch übereinstimmen muss. Gerade bei Teilzeitkräften kann es hier durch die Umrechnung von Stunden:Minuten auf das Dezimalsystem in seltenen Fällen zu Abweichungen kommen.
Ein Beispiel:
Ein Mitarbeiter hat eine Soll Wochenarbeitszeit von 12Std. 25Minuten. Dies wird auf fünf Tage á 2:29 aufgeteilt.
Achten Sie darauf, dass der Urlaubsanspruch exakt umgerechnet wird. So ergeben fünf Urlaubswochen á 12Std 25Min 62:05 Std. pro Jahr oder im Dezimalsystem 62,08 Std. Dieser Wert ist als stundenweiser Urlaubsanspruch einzutragen.

#### Ein Mitarbeiter tritt während des Jahres aus und ein/zwei Monate später wieder ins Unternehmen ein. Wird der Urlaub übernommen?
Tritt ein Mitarbeiter aus dem Unternehmen aus, so wird sein Anspruch mit dem Austritt abgerechnet. Dazu gehören auch etwaige Urlaubsansprüche.
Tritt er dann nach einiger Zeit wieder ins Unternehmen ein, so wird mit seinem Eintrittsdatum der Urlaubsanspruch neu definiert. Wird von Ihnen nun ein eventuell noch nicht ausbezahlter Urlaub aus dem/einem vorherigen Dienstverhältnis übernommen, so ist dies ab dem neuen Urlaubsanspruch unter zusätzlich (Tage bzw. Stunden) einzugeben. Damit wird dies eben als zusätzlicher Urlaubsanspruch mitgerechnet.
Tritt nun ein Mitarbeiter mehrmals während des Jahres aus und dann wieder ins Unternehmen ein, so kann dies nicht direkt über den Austritt und den erneuten Eintritt abgebildet werden, da pro Jahr nur ein Wert für den Urlaubsanspruch eingegeben werden kann. Um die Urlaubsabrechnung und auch eventuelle Überstunden durchzureichen, könnte hier anstelle des Austritts ein Zeitmodell ohne Sollstunden eingetragen werden. Damit bleiben die alten Stunden- und Urlaubsguthaben erhalten.
Wichtig: Der Urlaubsanspruch wird immer ab Eintritt des Mitarbeiters bis Ende des Kalenderjahres berechnet.

#### Können die Zeitdaten auch exportiert werden?
Ja. Neben den üblichen Exportfunktionen der **Kieselstein ERP** Ausdrucke steht auch der Export nach Lohnarten zur Verfügung. [Siehe dazu bitte]( {{<relref "/docs/stammdaten/personal/exportdaten">}} ).

#### Wie bucht man Zeiten die über Mitternacht gehen?
Tragen Sie den Buchungsbeginn wie üblich mit Kommt und der entsprechenden Uhrzeit (z.B. 22:00) ein.

Als Ende tragen Sie das Buchungsende am jeweils richtigen Tag ein z.B. Geht um 06:00.

Von **Kieselstein ERP** wird der Tageswechsel bei der Monatsabrechnung richtig erkannt und entsprechend ausgewiesen. Der früher automatisch gebuchte Mitternachtssprung ist nicht mehr erforderlich. Damit können auch die automatischen Pausen, falls erforderlich, während Mitternacht z.B. von 23:50 bis 00:20 eingebucht werden.

#### Kann das Kommt auch automatisch gebucht werden?
Ja: Mit dem Parameter AUTOMATISCHES_KOMMT kann eingestellt werden, dass bei z.B. einer Beginnbuchung auf einen Auftrag automatisch ein Kommt nachgebucht wird, wenn für diesen Tag noch kein Kommt gebucht wird. Dies wird in Fertigungsbetrieben angewendet, wenn sichergestellt werden soll, dass die Mitarbeiter nur Zeiten auf den Fertigungsaufträgen buchen.

<a name="Korrektur der automatischen Buchungen"></a>

#### Korrektur der automatischen Buchungen, Nachtragen von Automatik-Buchungen
Manchmal kommt es vor, dass automatische Pausen definiert und auch eingetragen wurden. Im Nachhinein stellt sich aber heraus, dass diese Annahmen / Vorgaben nicht korrekt waren und daher geändert werden müssen.
Verwenden Sie dafür in der Zeiterfassung die Funktion Pflege, Automatikbuchungen.
![](Automatikbuchungen.gif)
Definieren Sie hier, für welchen Zeitraum und für welche Personen die Automatikbuchungen neu durchgeführt / überprüft werden sollten. Sind in dem Zeitraum bereits Automatikbuchungen eingetragen und sollten diese entfernt werden, so haken Sie bitte zusätzlich noch Alte Automatik Buchungen löschen an. Hinweis: Beim Löschen werden nur die mit Automatik Buchung gekennzeichneten Buchungen entfernt. Diese sind in der Spalte Bemerkung mit **A** ersichtlich.

#### Was geschieht wenn ich Automatikbuchungen ändere?
Werden Automatikbuchungen geändert, so wird die Eigenschaft dass diese Buchung eine Automatikbuchung ist gelöscht. Somit wird sie auch bei der oben beschriebenen Pflege nicht mehr verändert.

#### Können automatisch Pausen abgezogen werden?
Ja hierzu stehen zwei Funktionen zur Verfügung
- a.) Pausen zu bestimmten Zeiten, anhand des Zeitmodells
- b.) eine Pause nach einer Mindestanwesenheitsdauer, wobei hier zwei unterschiedliche Grenzwerte definiert werden können. Beachten Sie dazu bitte, dass hier laut dem Gesetzestext der Arbeitszeitverordnung gebucht wird. Ist ein Mitarbeiter mehr als z.B. 6:00 anwesend muss er eine Pause von z.B. 30 Minuten gemacht haben. Das bedeutet die automatische Pause wird immer dann eingebucht, wenn der Mitarbeiter mehr als die eingestellte Automatische Pause ab anwesend war.

**Hinweis:** Beachten Sie dazu auch die Rundungen zu Gunsten des Unternehmens mit und ohne Sondertätigkeiten und den automatischen täglichen Zeitabzug in den Zeitmodellen.

#### Die automatischen Pausen sollen nur ein Hinweis, keine fixe Buchung sein - wie definiert man das?
Der Parameter AUTOMATISCHE _PAUSEN_NUR_WARNUNG=1 bewirkt, dass keine automatischen Pausen gebucht werden, aber eine Fehlermeldung zu den definierten automatischen Pausen angezeigt wird.

Weiters bewirkt der Parameter, dass nur eine Pause im Zeitmodell eingetragen werden kann.

 ![](auto_pause_jeweils.JPG)

Wenn Sie in dieser Pause 6h eintragen, so soll der Mitarbeiter alle 6h eine Pause von zb. 0,5h machen. Kürzere Pausen werden nicht berücksichtigt. 

Grundsätzlich dient der Parameter der Sicherheit, dass die Pausen eingehalten werden, die Verantwortung der Buchung liegt jedoch bei den Mitarbeitern. Somit wird, wenn die Pausen nicht der Definition entsprechen nur eine entsprechende Meldung im Modul Zeiterfassung angezeigt.
![](meldung_pausen.JPG)

<a name="Pausen automatisch verschieben"></a>

#### Können automatische Pausen fiktiv verbucht werden?
Es kommt, von bestimmten Mitarbeitern, immer wieder auch der Wunsch, ich arbeite den ganzen Tag durch und dann wird mir eine Pause automatisch abgezogen. Das ist unfair. Es wäre doch viel besser, wenn, um dem Gesetz genüge zu tun, das System automatisch die Pause einbucht und dann das Geht nach hinten verschiebt.<br>
Bitte lassen Sie sich in keinem Falle von irgend jemanden zu dieser Vorgehensweise überreden.
- a.) Es ist kein Mensch in der Lage acht oder mehr Stunden ohne Pause seine Arbeit in entsprechender Qualität durchzuführen
- b.) geht es mit diesen Buchungen auch um den Unfallschutz beim Weg von oder zur Arbeit. Was würde die Versicherung sagen, wenn die Zeitbuchungen nicht schlüssig sind?
- c.) ist es schlichtweg eine ungesetzliche Umgehung der Arbeitszeitgesetze. Der Arbeitgeber haftet dafür und uns sind Strafen pro Mitarbeiter und Fall von ca. 3.700,- € bekannt. Die bezahlt die Geschäftsleitung aus dem Privatvermögen.

#### Können Zeitmodelle tageweise geändert werden ?
Ja dafür steht für berechtigte Benutzer der Klick auf den Knopf Zeitmodell direkt in der Zeiterfassung zur Verfügung. Durch die Auswahl eines anderen Zeitmodells wird nur das Zeitmodell für das gewählte Datum geändert. Dies bewirkt, dass an im Modul Personal, bei der Person unter Zeitmodell mit dem Datum das ausgewählte Zeitmodell und einen Tag danach das ursprüngliche Zeitmodell eingetragen wird. D.h. dies ist nur eine Zeitmodelländerung für einen Tag. Wenn Zeitmodelle nachhaltig geändert werden sollen, so verwenden Sie bitte die Funktion direkt im Personalmodul.

<a name="Was_ist_bei_Zeitmodelländerungen_zu_beachten"></a>

#### Was ist bei Zeitmodelländerungen zu beachten ?
Bitte beachten Sie, bei Zeitmodelländerungen, egal ob dies ausgehend von der Zeiterfassung über den Knopf Zeitmodell ![](Zeitmodell_aendern.gif) oder im Personalmodul, durch Zuordnung eines anderen Zeitmodells zum Benutzer durchgeführt wird, **<u>keine</u>** Änderungen der Automatikbuchungen nachgezogen werden. D.h. sind z.B. im Zeitmodell A keine automatischen Pausen definiert und im Zeitmodell B schon, so werden durch die reine Änderung des Zeitmodells diese automatischen Buchungen nicht nachgezogen. Verwenden Sie dafür die oben beschriebene Funktion [Korrektur der automatischen Buchungen](#Korrektur der automatischen Buchungen).

#### Warum können manche Benutzer ihre eigene Zeitbuchungen nicht löschen bzw. ändern?
Ist einer Systemrolle das einschränkende Recht PERS_ZEITEINGABE_NUR_BUCHEN zugewiesen, so können die Benutzer Ihre Zeitdaten nur hinzufügen. Bestehende Daten können nur von berechtigten Benutzern verändert werden.

#### Warum können manche Benutzer bei ihrer Zeitbuchung die Zeit und das Datum nicht auswählen und ändern?
Ist einer Systemrolle das einschränkende Recht PERS_ZEITEINGABE_NUR_BUCHEN zugewiesen, so können die Benutzer nur die aktuelle Zeit buchen, aber nicht selbst z.B. um 9:10 Uhr eine Zeitbuchung mit 9:00 Uhr erstellen. Es können nur Zeitbuchung für  den aktuellen Tag erstellt werden.

#### Kann die Sicht auf die Personendaten eingeschränkt werden?
Ja. Default ist die Sicht für Personal und Zeiterfassung auf die eigene Person eingeschränkt.

Hat ein Benutzer das Recht Pers_Sichtbarkeit_Abteilung, so darf er die Daten aller Mitarbeiter seiner Abteilung einsehen und wenn er für das Personalmodul auch das Pers_Benutzer_CUD Recht hat, diese auch bearbeiten.

Hat ein Benutzer das Recht Pers_Sichtbarkeit_Alle, so darf er alle Mitarbeiter des Mandanten einsehen und abhängig vom Personalmodul Recht diese gegebenenfalls auch bearbeiten.

#### Wie werden Artikelzuschläge verbucht?
In manchen Unternehmen werden zulagenpflichtige Tätigkeiten durchgeführt. Dies können z.B. Schmutzzulage oder Gefahrenzulagen sein. Damit diese automatisch auf den Monatsabrechnungen gelistet / angeführt werden, können in **Kieselstein ERP** diese Zulagen pro Tätigkeit = Arbeitszeitartikel definiert werden. 

Definieren Sie dazu als erstes im Personal unter Grunddaten die in Ihrem Unternehmen benötigten Zulagen.

Danach wählen Sie den unteren Modulreiter Artikelzulage und definieren die Artikeln bei denen entsprechende Zulagen gewährt werden.

![](zulage1.jpg)

Dadurch, dass nun in der Zeiterfassung ein Zulagenbehafteter Artikel verwendet wird,  

![](zulage2.jpg)

erscheint auch der Stundeneinsatz dieser Zulagenpflichtigen Tätigkeit auf der Monatsabrechnung des Mitarbeiters.

![](zulage3.jpg)

<a name="Erfassungsarten / Terminals"></a>

#### Welche Erfassungsarten stehen zur Verfügung?
In **Kieselstein ERP** stehen derzeit folgende Erfassungsarten für die Zeiterfassung zur Verfügung.

-   Manuelle Zeiterfassung

-   [BDE-Station]( {{<relref "/fertigung/zeiterfassung/bde_station">}} )

-   [Quickzeiterfassung]( {{<relref "/fertigung/zeiterfassung/quickzeiterfassung">}} )

-   [Zeiterfassungsterminal](Zeiterfassungsterminal.htm) mit [Fingerprint](Fingerprint.htm)

-   [Zeiterfassungsterminal5.6](Zeiterfassungsterminal_5_6.htm)

-   [Zeiterfassungsterminal Nettop](Zeiterfassungsterminal_Nettop.htm)

-   [Zeiterfassungsstifte (OffLine)](Zeiterfassungsstifte.htm)

-   [Zeiterfassung am PDA / Mobil / Handy](../HVMA/index.htm)

BDE-Station, Zeiterfassungsterminals und Zeiterfassungsstifte werden üblicherweise mittels Barcodescanner bedient. Die dafür erforderlichen Barcodes können direkt aus **Kieselstein ERP** erzeugt werden. Die Barcodes können gleichwertig auf allen Erfassungsgeräten verwendet werden.

#### Unterschied zwischen Personalzeiten und Maschinenzeiten?
Aus Gründen der Mehrmaschinenbedienung unterscheiden sich die Buchungen zwischen Maschinenzeiten und Personalzeiten.
a.) Personen können nur eine Tätigkeit zur gleichen Zeit ausführen
b.) Maschinen können je Maschine ebenfalls nur einen Arbeitsgang ausführen, aber annähernd gleichzeitig von einer Person bedient werden. In diesem Falle sprechen wir von der [Mehrmaschinenbedienung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ).
Aus diesem Grund müssen in der Regel bei Zeitbuchungen von Personen / Tätigkeiten nur Beginnbuchungen durchgeführt werden. Wurde eine Tätigkeit begonnen, so wird diese durch den Beginn einer anderen Tätigkeit beendet.
Bei der Maschinenbuchung können die Maschinen parallel laufen und auch von der gleichen Person bedient werden. Daher muss jede Maschinenzeit auch beendet werden.
Bucht eine Person ein GEHT, so wird damit auch die Tätigkeit beendet. Am nächsten Tag muss, zur Bestätigung, der erneute Beginn des Auftrages / der Tätigkeit eingebucht werden.
Bei der Maschinenzeit wird anhand der Einstellung automatisches Ende bei Geht, bei der Geht-Buchung des Mitarbeiters für die Maschinen ein Stopp gebucht (wenn dieses angehakt ist) oder die Maschinen läuft auch darüber hinaus weiter, z.B. für eine sogenannte Geisterschicht.

#### Ich habe Zeiten auf einen Auftrag / ein Los erfasst und sehe diese nicht?
<a name="Zeitverbuchung_Falsch"></a>
Für die Zeitauswertung ist grundsätzlich eine richtige Zeitbuchung erforderlich. D.h. die Zeiten der Mitarbeiter müssen **IMMER** zwischen Kommt und Geht eingebettet sein. Nur diese Zeiten sind anwesende Zeiten und werden in den Zeitauswertungen berücksichtigt. Sind, so wie im untenstehenden Beispiel, Zeiten außerhalb der Kommt/Geht-Zeiten oder gänzlich ohne Kommt/Geht gebucht, so sind diese zwar in den Buchungen enthalten, werden aber in den Auswertungen nicht berücksichtigt.

![](Zeiteingabe1.gif)

Die Fehlbuchungen werden aus Geschwindigkeitsgründen nur bei der Monatsabrechnung analysiert. D.h. wenn sichergestellt werden soll, dass alle Buchungen richtig verbucht sind, muss vorher die Monatsauswertung durchgeführt werden. In dieser dürfen für den Mitarbeiter keine Fehlermeldungen vorhanden sein. In obigem Fall wird bei der Monatsauswertung folgende Meldung mit angezeigt.

![](Monatsauswertung_Fehlermeldung.gif)

#### Bei den Mitarbeitern wurden Zeitmodelle hinterlegt, aber in der Monatsabrechnung werden keine Sollzeiten angezeigt.
Sowohl die Zeitmodelle selbst als auch deren Gültigkeitszeitpunkt sind kalenderfähig. D.h.

a.) das Zeitmodell muss zum gewünschten Zeitpunkt gültig sein, siehe dazu Personal, unterer Reiter Zeitmodelle definieren. In der Übersichtsliste der Zeitmodelle sehen Sie rechts das Datum ab dem jedes Zeitmodell gültig ist.

b.) Nun kann ein Zeitmodell einem Mitarbeiter ab einem gewissen Zeitpunkt zugeordnet werden. Stellen Sie auch hier sicher, dass das Zeitmodell zum gewünschten Zeitpunkt gültig ist.

**<u>Wichtiger Hinweis:</u>**

Zeitmodelle sind global gültig. Das bedeutet: Werden Zeitmodelle bzw. Zeitmodellzuordnungen aus der Vergangenheit geändert, so ändern Sie damit auch die Datenbasis für die Berechnung der Überstunden, Gleitzeitsaldi usw..

Bitte beachten Sie dies, wenn Sie an bestehenden Zeitmodellen oder deren Gültigkeitsdaten etwas ändern.

Da es in der Praxis immer wieder erforderlich ist, dass Zeitmodelle an die Gegebenheiten fein angepasst werden müssen, ist eine programmtechnische Verriegelung nicht möglich.

#### Bei der Monatsabrechnung wird kein Urlaub und ev. keine Sollstunden mehr angezeigt.
Für dieses Verhalten kann es mehrere Ursachen geben:

- Ab dem Datum ab dem keine Sollstunden angezeigt werden ist beim Mitarbeiter ein Austritt eingetragen

- Es wird ab dem Datum ein Zeitmodell verwendet, welches keine Sollstunden hat

- Die Tage sind als Feiertag definiert

**Info:** Ist ein Austritt eingetragen, so wird angenommen, dass ab diesem Datum der Mitarbeiter keine Tätigkeiten mehr zu erbringen hat. Damit ergibt sich dass keine Zeitberechnung mehr ab diesem Zeitpunkt erfolgt. In der Monatsabrechnung werden nur mehr die Tage bis zum Ende des Monates aufgeführt. Der Austritt wird im Personal unter Eintritt/Austritt gepflegt.

#### Wie wird ein Arztbesuch richtig verbucht?
In **Kieselstein ERP** können auch stundenweise Nichtanwesenheitszeiten, welche Sondertätigkeiten genannt werden, minutengenau verbucht werden. Wie bereits oben ausgeführt müssen alle Zeiten zwischen Kommt und Geht eingebettet sein.
Ein Beispiel:
Ein Mitarbeiter ist von 8:00 bis 8:45 beim Arzt und kommt mit 8:45 ins Unternehmen.
![](ZE_Arzt.jpg)
damit ist der Mitarbeiter 0,75 Std beim Arzt. Diese Arztzeit wird auch in der Monatsabrechnung entsprechend ausgewiesen. Ab 8:45 ist der Mitarbeiter wieder im Unternehmen anwesend und kann produktive Zeiten erbringen.
Diese Buchungsweise gilt für alle stundenweise verbuchbaren Sondertätigkeiten.

#### Wie wird ein Arztbesuch am Terminal richtig verbucht?
Wie bereits ausgeführt, geht es bei der Erfassung der Zeiten auch darum zwischen bezahlter und nicht bezahlter Abwesenheit zu unterscheiden.
D.h. wenn nun ein Mitarbeiter zum Arzt geht, so unterbricht er im ersten Schritt die Arbeit für Ihr Unternehmen, er ist abwesend.
Kommt er nun wieder zurück und setzt seine Arbeit fort, so wird damit die Unterbrechung beendet. In der Regel bringt der Mitarbeiter eine Bestätigung des Arztes, wie lange er denn bei diesem (in der Ordination) war. Hier stellt man oft interessante Dinge fest, wie z.B. dass die Zeit auch zum Einkaufen genutzt wird, oder dass die Zeit ausserhalb der Kernzeit ist usw..
Daher bucht der Mitarbeiter am Terminal eine gewöhnliche Pause / Unterbrechung.
Bringt er dann die Bestätigung, so:
- buchen Sie für ihn eine Sondertätigkeit mit der von Ihnen akzeptierten Dauer der Arztzeit oder
- ändern die Unterbrechungsbuchung die am Terminal gemacht wurde auf Arzt (Beginn und Ende) ab.
Damit tragen Sie ihm eine bezahlte Nichtanwesenheit in seine Zeitbuchungen ein.
Bringt der Mitarbeiter diesen Beleg nicht, oder es wird nur ein Teil der Zeit anerkannt, so bleibt dies eine nicht bezahlte Pause.

#### Wie ist **Kieselstein ERP** einzustellen, wenn eine gewisse Pausendauer zu bezahlen ist?
In einigen Ländern ist es gesetzlich geregelt, dass z.B. ein halbe Stunde Mittagspause vom Unternehmer zu bezahlen ist. Andererseits sind die Mitarbeiter auch gezwungen die Pausen in Anspruch zu nehmen. So ist die Definition für Slowenien (2019) z.B. wie folgt:
Es sind pro Woche 37,5Std gleichmäßig auf die fünf Wochenarbeitstage verteilt zu arbeiten
Zusätzlich bekommen die Mitarbeiter die vorgeschriebene Pause, welche nach 6Std zu nehmen ist bezahlt.

Um dies nun abzubilden, tragen Sie einerseits in den Kopfdaten des Zeitmodells eine Zeitgutschrift bei Geht von 30Minuten ein,
andererseits definieren Sie das Tageszeitmodell wie üblich.
Bitte beachten Sie, dass diese Definition davon ausgeht, dass die Zeitgutschrift an jedem Tag erfolgt, egal wie lange der Mitarbeiter gearbeitet hat.
Für besondere Gutstunden kann auch noch der Reiter Zeitgutschrift im Modul Zeiterfassung verwendet werden.

#### Warum stimmt die IST-Zeit in der Mitarbeiterübersicht nicht mit der IST-Zeit in der Monatsabrechnung zusammen?
In der Mitarbeiterübersicht werden die IST- Zeiten nur dann angedruckt, wenn an diesem Tag keine ganztägigen Sondertätigkeiten (Urlaub/ Krank usw.) verbucht sind.

#### Wir arbeiten kurze / lange Woche, wie kann man das abbilden ?
In manchen Branchen ist es üblich, dass abwechselnd in den Kalenderwochen kurz bzw. lange gearbeitet wird. Das hat für die Mitarbeiter den Vorteil, dass jeden zweiten Freitag freigegeben werden kann.

Da es auch immer wieder vor kommt, dass Mitarbeiter den kurze / lange Woche Rhythmus verlassen, kann dies nicht starr an die Kalenderwoche gebunden werden. Verwenden Sie dafür die Möglichkeit der Schichtzeitdefinition ![](Schichtzeit.gif). Damit kann für jeden Mitarbeiter sein eigener Zeitmodell-Wechsel-Plan hinterlegt werden.

Für die Definition der Kurzen/Langen Woche definieren Sie zuerst zwei Zeitmodelle. Z.B. ![](Kurze_Lange.gif).

In Summe ergeben die Sollstunden der beiden Zeitmodelle wieder die in Ihrer Branche übliche Wochenstundenanzahl, z.B. 38,5Std, also 77Std. für zwei Wochen. D.h. bei der kurzen Woche wird am Freitag keine Sollzeit definiert, bei der Lange Woche schon.

Nun wählen Sie im Personal den Mitarbeiter aus, für den dieser kurze/lange Woche Rythmuss definiert werden soll. Klicken Sie auf Schichtzeit ![](Schichtzeit.gif) und definieren Sie im nun folgenden Dialog die Schichten, also die Zeitmodell-Folge für den Mitarbeiter.

![](Schichtzeit_Definition.jpg)

Mit Speichern ![](Schichtzeit_Speichern.gif) werden die definierten Reihenfolgen der Zeitmodellwechsel festgelegt.

Damit werden die Reihenfolgen der Zeitmodelldefinitionen für den Mitarbeiter festgelegt. Das bei Schicht 1 angegebene Zeitmodell wird in der Gültig ab Woche eingetragen, das bei Schicht 2 angegebene Zeitmodell eine Woche später. Wurden, so wie oben, keine weiteren Schichtzeiten definiert, so wird für die dritte Woche wieder das bei Schicht 1 definierte Zeitmodell eingetragen, usw.. 

Das hat den Vorteil, sollten sich die Sollzeiten ändern, kann dies einfach durch Löschen und neu Eintragen der Gültigkeit eines Zeitmodells umdefiniert werden.

![](Schichtzeit_Reihenfolge.gif)

Sie erhalten nun in der Monatsabrechnung folgende Sollstunden:

![](Kurze_Lange_Sollstunden.gif)

#### Wie werden Schichtzeiten definiert?
Für die Definition von Schichtzeitmodellen verwenden Sie bitte ebenfalls die oben unter Kurze / Lange Woche beschriebene Vorgehensweise. Definieren Sie hier auch noch die Schicht3 und ev. Schicht4.

Je nachdem wie viele Schichten Sie verwenden wollen.

**Hinweis:**

Die Praxis hat gezeigt, dass die Schichtarbeiter gerade in kleinen Unternehmen sehr flexibel handhaben. Meist im Sinne des Unternehmens um die anstehenden Projekte zeitgerecht ausliefern zu können. Das bedeutet, wenn Sie die erlaubten Buchungszeiträume für die Schichten sehr streng definiert haben, müssen nachträglich oft die Schichtmodelle umdefiniert werden. Hier empfiehlt es sich, eher eine Art Gleitzeitmodell zu verwenden und dafür z.B. die Rundung zu Gunsten des Unternehmens zu verwenden.

#### Wie werden Schichtwechsel richtig definiert?
Gerade wenn der Schichtbeginn auf einen Sonntag fällt, z.B. Sonntag um 21:30 ist es wichtig den Schichtbeginn / den Wechsel des Zeitmodells trotzdem erst mit Montag durchzuführen.
Hintergrund: Logisch gesehen beginnt ja das neue Zeitmodell erst mit Montag und dauert bis Sonntag. Würden Sie den Wechsel bereits mit dem ersten Sonntag definieren so würde die Sollstundenanzahl des Mitarbeiters zu hoch sein.
Ein Beispiel:
![](Schichtzeitbeispiel1.gif)
In diesem Beispiel ist der Zeitmodellwechsel (ZMW) bereits mit Sonntag definiert worden. Daher hat der Mitarbeiter am Sonntag eine Sollzeit von 2,5 Std. Da er aber nur zwei Tage gearbeitet hat, fehlen ihm exakt die 2,5Std vom Sonntag.
Richtig ist der Zeitmodellwechsel wie folgt:
![](Schichtzeitbeispiel2.gif)
Beachten Sie bitte den positiven Gleitzeitsaldo von 2,5 Std. für die KW39\. Nur damit werden die tatsächlich gearbeiteten Stunden auch richtig in die Monatsabrechnung aufgenommen und in das Folgemonat übertragen.
Beachten Sie bitte, dass für die richtige Behandlung der Sonntage diese Tagesart ev. auch in den anderen Zeitmodellen definiert sein muss, aber ohne Sollstunden.

Da es bei der dritten Schicht auch immer wieder die Begrenzung der frühesten Kommt-Zeit gewünscht ist, kann hier auch definiert werden, dass diese für den Vortag gilt.
D.h. haken Sie bitte ![](Fruehest_Erlaubtes_Kommt_am_Vortag.gif) z.B. zu obigem Beispiel passen, wenn das frühest erlaubte Kommt gewünscht ist, auch das (gilt ab) Vortag an.

#### Können Schichtzeitmodelle automatisch erkannt werden?
Ja.

Die Grundidee dahinter ist, dass bei einer Kommt-Buchung das Zeitmodell mit der nächstliegenden Kommt-Buchung (Früheste erlaubte Kommt-Zeit) gesucht wird und wenn dieses Zeitmodell vom aktuell für diesen Tag gültigen Zeitmodell abweicht, wird das Zeitmodell bei dem buchenden Mitarbeiter hinterlegt.
Um diese Lösung so flexibel wie möglich zu gestalten, müssen dafür für jeden Schichtarbeiter die für ihn gültigen Schichtzeitmodelle unter Schichtzeitmodell hinterlegt werden.
Das bringt den Vorteil, dass bei unterschiedlichen Schichtzeitbeginn-Zeitpunkten z.B. verschiedener Abteilungen, die für den Mitarbeiter richtige Schichtzeit mit sehr hoher Wahrscheinlichkeit erkannt wird.
**Hinweis:** Bitte beachten Sie, dass es bei Änderungen der Definitionen der Schichtzeiten erforderlich ist, dass der **Kieselstein ERP** Client komplett neu gestartet werden muss, damit diese Prüfungen wirksam werden.

<a name="Schichtzuschlaege definieren"></a>

#### Können Schichtzeitzuschläge ausgewiesen werden?
Ja.
Von der Idee her werden Schichtzuschläge so dargestellt, dass auf der Monatsabrechnung die Stunden die für die Zuschlagsberechnung ausschlaggebend sind pro Tag ausgewiesen werden.
Bitte beachten Sie, dass für die Berechnung der Dauer des jeweiligen Zuschlages nur die Zeiten zwischen Kommt und Geht herangezogen werden. Unterbrechungen (Pausen), Arzt, Zeitausgleich, usw.  innerhalb der Tagesbuchungen als Sondertätigkeit werden wie Anwesenheitszeit behandelt.
Definieren Sie dazu in den Grunddaten des Personals die Arten der Schichtzuschläge im oberen Reiter ![](Schichtzeiten_zuschlaege.gif).
Achten Sie dabei darauf, dass die Bezeichnungen der Zuschläge so kurz wie möglich sind um diese in der Monatsabrechnung kompakt andrucken zu können.
Nachdem die Bezeichnungen der Zuschläge definiert sind, wechseln Sie im Personal in den unteren Modulreiter Schicht ![](unterer_reiter_schicht.PNG) und definieren die Zeiten die für die Zuschläge der Schichten (2\. & 3.Schicht)![](schichtzeiten_reiter.PNG). Gerade für die 3.Schicht, welche oft von 22:00-6:00 dauert, verwenden Sie bitte auch den Haken bei Rest des Tages ![](rest_des_tages.PNG), was soviel bedeutet wie dieser Zuschlag wirkt bis zum Ende des Tages (24:00) d.h. für obige Definition sind zwei Einträge zu machen: 22:00 bis Rest des Tages und (für den nächsten Tag) 0:00-06:00.

![](zeiten_schicht_liste.PNG) 
![](Schichtzuschlag_Zeiten.PNG)
Nun wählen Sie das von Ihnen definierte Zeitmodell und ordnen die definierten Schichten den entsprechenden Zeitmodellen zu ![](zeitmodell_schicht_zuordnen.PNG).
Hinweis: Beachten Sie bitte dass Zeitmodell nur tageweise geändert werden können. Die Bestimmung welcher Schichtzuschlag gewählt wird, wird mit dem ersten Kommt des Tages ermittelt. Dies könnte bei unterschiedlichen Schichten, z.B. Wechsel von der 1\. in die 3.Schicht oder Wechsel von der 3\. in die 2.Schicht dann zu Ungereimtheiten führen, es wird die erste Definition des Tages verwendet, wenn unterschiedliche Zuschläge in den jeweiligen Schichten definiert sind. Wir raten daher die Zuschläge möglichst gleich zu gestalten.

<a name="Schichtzuschlaege definieren0"></a>

#### Werden die Schichtzeitzuschläge anhand Ist- oder Sollzeiten gerechnet?
Das hängt von der Einstellung des Schichtzuschlages ab.
Das bedeutet, ist in der Definition des Schichtzuschlages ![](Schichtzeit_begrenzt_auf_Tagessoll.gif) Begrenzt auf Tagessoll **nicht** angehakt, so wird für die Ermittlung des Schichtzuschlages des jeweiligen Tages die Istzeit innerhalb des Zeitraums für den Schichtzuschlag herangezogen. Ist das Begrenzt auf Tagessoll angehakt, so wird die Dauer zusätzlich auf das Tagessoll (anhand der Schichtzeit) begrenzt.

<a name="Schichtzuschlaege definieren1"></a>

#### Sollten in der Berechnung der Schichtzeitzuschläge Pausen als Zeit oder nicht mitgerechnet werden?
Da die Auszahlung der Schichtzeit von der jeweiligen Vereinbarung mit dem Arbeitgeber abhängig ist, kann auch definiert werden, ob Pausen innerhalb des definierten Zeitraumes der Schicht als Arbeitszeit betrachtet werden sollten oder eben nicht.
Stellen Sie dazu bitte den Haken bei ![](Schichtzuschlag_Pausen_Beruecksichtigen.gif)Pausen berücksichtigen entsprechend ein. Das bedeutet ist Pausen berücksichtigen angehakt, so werden die gebuchten Pausen in der Berechnung der Dauer der Schichtzeit für den Schichtzuschlag abgezogen.

#### Wie buche ich Arztzeiten über die Pausen hinweg richtig?
In Unternehmen bei denen die exakten Anwesenheitszeiten auch ausbezahlt werden, ist es manchmal erforderlich, dass auch die Pausen während bezahlter Abwesenheiten richtig gebucht werden. Es kommt dazu, dass, aus Gründer der Nachvollziehbarkeit die Buchungen der Mitarbeiter nicht verändert werden sollten. Gehen Sie dafür z.B. wie unten beschrieben vor.

![](Arztzeiten_ueber_Pausen.gif)

#### Die Mitarbeiter stempeln vor dem offiziellen Arbeitsbeginn, die Zeiten sollen aber erst ab dem Arbeitsbeginn gerechnet werden.
Im Zeitmodell gibt es die früheste erlaubte Kommt-Zeit. Zeiten, die vorher gebucht werden, werden nicht berücksichtigt.
Die späteste erlaubte Geht-Zeit bewirkt, dass nur die Arbeitszeiten bis zu diesem Zeitpunkt verbucht werden.

#### Wie können meine Mitarbeiter Überstunden machen, wenn es fixe früheste erlaubte Kommt-Zeiten und späteste erlaubte Geht-Zeiten gibt?
Legen Sie ein zweites Zeitmodell an, mit denselben Zeiten, aber keiner späteste erlaubte Geht-Zeit. Über das Benutzerrecht PERS_ZEITERFASSUNG_ZEITMODELL_TAGEWEISE_AENDERN kann ein Zeitmodell für die Tage, an denen Überstunden gemacht werden, geändert werden. Das Zeitmodell wird von einem Befugten geändert. Pro Tag kann nur ein Zeitmodell verwendet werden.

#### Einige Mitarbeiter arbeiten auf Stundenbasis, die stark variieren.
Erstellen Sie ein Zeitmodell, ohne eine bestimmte Anzahl von Sollstunden. Die ausbezahlten Stunden tragen Sie dann zum Auszahlungsdatum unter Stundenabrechnung ein. So haben Sie immer einen exakten Status der für diesen Mitarbeiter noch nicht ausbezahlten Stunden.

#### Wie werden für die Mitarbeiter ohne Sollstunden die Urlaube eingetragen?
Wie oben beschrieben werden ja für die Mitarbeiter keine Sollstunden definiert. Somit können auch keine Stunden für den Urlaub errechnet werden. Andererseits ist es immer erforderlich, die Urlaubstage bei den Mitarbeitern ohne fixe Stundenregelung eine Einigung zu erzielen, welche Stunden pro genommenen Urlaubstag denn als Urlaubsstunden und damit theoretische Iststunden angerechnet werden. Um nun dem Mitarbeiter diese Urlaubsstunden anzurechnen / aufzubuchen, tragen Sie bitte im Modul Zeiterfassung, bei dem Mitarbeiter unter Sonderzeiten für die Urlaubstage eine stundenweise Urlaubsstundenerfassung ein. Somit werden die Urlaubsstunden als anerkannte Sonderstunden dem Mitarbeiter zugebucht und diese gehen in die Gleitzeitabrechnung mit ein.

#### Wie werden für die Mitarbeiter ohne Sollstunden Krank usw. eingetragen?
Gehen Sie wie oben für den Urlaub beschrieben vor.

#### Müssen in den Zeitmodellen alle Tage definiert werden?
Jein oder auch, das kommt darauf an.

Wenn Ihre Mitarbeiter selbst konsequent und richtig alle Tage erfassen, so müssen im jeweiligen Zeitmodell nur die Tage definiert werden für die auch Sollzeiten gegeben sind. Arbeiten Ihre Mitarbeiter aber auch manchmal an Tagen, an denen üblicherweise nicht gearbeitet wird oder vergessen sie manchmal die gesetzlichen Pausen einzuhalten, so empfiehlt es sich jede Tagesart zu definieren.

Bedenken Sie bitte dass als Tagesart nicht nur die sieben Wochentage definiert sind, sondern zumindest zwei Feiertagsarten (Halb und Ganz-Tags). Und es können für den Betriebskalender in den Personal, Grunddaten noch zusätzliche Tagesarten definiert sein.

Es sollte also jeder Tag an dem gearbeitet werden könnte vollständig definiert sein.

#### Was ist bei der Definition der Sollstunden in den Zeitmodellen zu beachten?
Hier ist insbesondere die Thematik der Umrechnung zwischen dem Stunden:Minuten System und dem Dezimalsystem zu beachten. Insbesondere die Umrechnung von z.B. 8:25 ergibt 8,4166666667 Std. Für diese Stunden werden z.B. in der Monatsabrechnung 8,42 Stunden angedruckt. Um bei einer 38,5Std Woche auf 38,5 Std. zu kommen muss der Freitag mit 4:50 definiert werden, was wiederum 4,833333 Std. im Dezimalsystem ergibt.

Der Andruck der Monatsabrechnung erfolgt gerundet, also 8,42 und 4,83 Std. Zählt man diese Stunden nun am Papier zusammen, so erhält man 38,51 Std., obwohl nur 38,50 Std. definiert sind.

Wir raten daher leicht zu konvertierende Sollzeiten zu verwenden. Z.B. auf Basis der 15Minuten = 0,25 Std.

Alternativ könnte auch die Formulardefinition für Sie geändert werden und damit die Rundungsthematik dem Mitarbeiter dargestellt werden.

#### Fehlermeldung Auftragsposition wurde gelöscht
Diese Meldung besagt, dass in den Zeitbuchungen auf eine Auftragsposition Bezug genommen wurde. Wurde nun die Auftragsposition gelöscht, so erscheint zu Ihrer Information diese Fehlermeldung, da der Bezug nicht mehr gegeben ist.

![](Auftragsposition_Geloescht.jpg)

Derzeit hat dieser Hinweis, keine Auswirkungen auf die Auswertungen.

#### Wie erfasse Loszeiten in der manuellen Zeiterfassung.
Wählen Sie nach dem Neu in der Zeiteingabe in der Zeile unter der Zeiteingabe ![](Loszeiterfassung.gif)
Belegzeit und dann rechts daneben Los. Somit kann direkt in das Los gebucht werden.

#### Wie beende ich Arbeiten auf einem Los ohne bei einem anderen Los fortzusetzen?
Verwenden Sie dazu die Ende Buchung. Damit wird die Zeit eines Mitarbeiters nicht mehr dem Los / Projekt zugeordnet, sondern es ist diese Zeit nur mehr Anwesenheitszeit. Diese Buchung steht in allen Erfassungsarten zur Verfügung. Verwenden Sie dazu die entsprechenden Codes z.B. aus der Liste der Sondertätigkeiten. Sie finden diese Liste unter Zeiterfassung, unterer Modulreiter Sondertätigkeiten, Menüeintrag Info, Sondertätigkeitenliste.
Bitte beachten Sie den Unterschied zwischen
Ende ... Beenden einer Tätigkeit eines Mitarbeiters
Geht ... Beenden der Anwesenheit des Mitarbeiters im Unternehmen, damit zugleich Beenden seiner ev. Tätigkeit
Stop ... Beenden der Maschinenlaufzeit in der Mehrmaschinenbedienung.

<a name="Weitere Sonderzeiten"></a>

#### Wo können weitere Sonderzeiten definiert werden?
Gerade in Österreich gibt es neben den üblichen Freistunden auch kollektivvertraglich zugesicherte Bezahlte Abwesenheiten wie z.B. Pflegeurlaub, Verehelichung usw. Diese zusätzlichen Sondertätigkeiten können in der Zeitverwaltung im unteren Modulreiter Sondertätigkeiten definiert werden.
Legen Sie mit Neu die gewünschte Sondertätigkeit an.
Bitte beachten Sie die drei Einstellungsmöglichkeiten:

| Feld | Bedeutung |
| --- |  --- |
| Bezahlt | Dies ist eine bezahlte Abwesenheit. D.h. diese Stunden werden in der Monatsabrechnung in den Ist-Stunden mitgerechnet. D.h. sie werden unter bezahlte Sondertätigkeit geführt. Zusätzlich kann für unserer Schweizer Anwender der Prozentsatz angegeben werden zu dem z.B. Krank mit nur 80% der Sollstunden bezahlt wird. Tragen Sie daher in der Regel 0% für nicht bezahlt und 100% für voll bezahlte Abwesenheit ein. In der Monatsabrechnung erscheinen Zeiten mit 0% weiterhin unter Sonstige Tätigkeiten nicht bezahlt. Alle anderen erscheinen um den Faktor korrigiert unter bezahlte Stunden auf.Hinweis: Bezahlt meint in diesem Zusammenhang, dass die Abwesenheitszeit zu xx% im Gleitzeitguthaben des Mitarbeiters berücksichtigt wird. Ob diese Abwesenheit tatsächlich ausbezahlt wird muss bitte in der Lohnverrechnung definiert werden. |
| Warnmeldung in Kalendertagen | Ab einer gewissen durchgängigen Krankheit eines Mitarbeiters müssen üblicherweise die Lohnkosten des Mitarbeiters nicht mehr vom Arbeitgeber, sondern von der Allgemeinheit getragen werden. Damit in der Liste der Sondertätigkeiten diese angezeigt werden, tragen Sie hier die Anzahl der Kalendertage ein, ab denen Sie eine entsprechende Info erhalten möchten. Die Auswertung dazu sehen Sie dann unter Info, Sonderzeitenliste.                    ![](Sonderzeiten_Laufende_Fehltage.gif)Die Berechnung erfolgt in der Form, dass vom Bis-Datum der Auswertung der erste Tag einer anderen (Sonder-) Tätigkeit gesucht wird. Von diesem Datum weg, wird die erste Buchung der angedruckten Sondertätigkeit gezählt. Somit können auch Spezialfälle wie Mitarbeiter ist ab Montag Krank, hat aber am Freitag noch gearbeitet abgebildet werden. |
| Nur tageweise buchbar | Diese Tätigkeit kann nur tageweise gebucht werden. D.h. sie wird in der Tages-Detailbuchung in der Auswahlmöglichkeit der Sondertätigkeiten nicht angezeigt. |
| Auf der BDE Station buchbar | Diese Sondertätigkeit wird auf in der Sondertätigkeitenliste für die BDE-Stationen mit ausgedruckt. Zusätzlich werden nur diese Tätigkeiten bei Benutzern mit PERS_ZEITEINGABE_NUR_BUCHEN in der Auswahlliste der Sondertätigkeiten angezeigt. |

**Hinweis:** Damit Stundenweise (also die nicht tageweise buchbaren) buchbare Tätigkeiten angezeigt werden, muss das Zeiterfassungsmodul neu gestartet werden.

#### Können einmal angelegte Sondertätigkeiten wieder gelöscht werden?
Sie können eine Sondertätigkeit verstecken. Wechseln Sie dazu in den unteren Reiter Sondertätigkeiten und setzen bei der gewünschten Zeile den Hacken bei Versteckt. Das können Sie bei allen Sondertätigkeiten bis auf KOMMT GEHT UNTER TELEFON und ENDE durchführen.

Wenn nun eine bereits angelegte Buchung eine versteckte Sondertätigkeit hat, werden alle Sondertätigkeiten geladen. Bei neuen Buchungen, auch bei tageweisen Sondertätigkeiten werden die Versteckten nicht angezeigt.

#### Wie geht man am besten mit Urlaubsansprüchen aus Datenübernahmen / Neustart um?
Gerade wenn Daten aus früheren Systemen, das können auch manuelle Aufzeichnungen sein, übernommen werden müssen, hat sich folgende Vorgehensweise bewährt.

Ermitteln Sie den Urlaubsanspruch in Tagen und Stunden getrennt in das aktuelle laufende Jahr und für den Resturlaub aus den Vorjahren. In **Kieselstein ERP** geben Sie nun beim Urlaubsanspruch den Resturlaub aus den Vorjahren unter dem Jahr vor dem aktuellen ein und den Anspruch für das gesamte aktuelle Jahr unter dem aktuellen Jahr ein.

Nun pflegen Sie die im aktuellen Jahre alle tatsächlich konsumierten Urlaubstage und Stunden in **Kieselstein ERP** ein. Selbstverständlich können auch die in der Zukunft geplanten Urlaubstage eingegeben werden.

So erhalten Sie eine exakte Urlaubsabrechnung, in der auch der aliquote Urlaubsanspruch richtig dargestellt wird.

#### Ein Mitarbeiter tritt während des Jahres ein, welcher Urlaubsanspruch muss eingegeben werden?
Geben Sie auch für diese Mitarbeiter den vollen Jahresanspruch ein. Von **Kieselstein ERP** wird für die Berechnung des aliquoten Anspruches folgende Formel angewandt:
Urlaubsanspruch vom 1.1\. bis zum Anspruchsdatum (z.B. Stichtag der Monatsabrechnung)
abzgl. Urlaubsanspruch vom 1.1\. bis zum Eintrittsdatum
Ergibt den tatsächlichen aliquoten Urlaubsanspruch zum gewünschten Stichtag.

<a name="Urlaubkorr"></a>

#### Wie bringe ich alte/falsche Urlaubsansprüche raus?
Gerade nach Datenübernahmen aus Alt-Systemen ergibt sich, dass manchmal Urlaubsansprüche korrigiert werden müssen.

![](Urlaub.gif)

Sollte z.B. hier der Resturlaub von 8Tagen auf 0 gesetzt werden, so ist dazu der zusätzliche Urlaub des Vorjahres entsprechend zu korrigieren.

Gehen Sie dazu unter Personal, Urlaubsanspruch Folgendes ein:

![](Urlaub2.gif)

Voraussetzung: Die Abrechnung ist für 2007.

Damit der Resturlaub auf den Monatsabrechnungen von 2007 nicht mehr aufscheint, muss die Korrektur in 2006 eingetragen werden.

Diese könnte z.B. wie folgt aussehen:

![](urlaub5.gif)

So sieht die nun korrigierte und aktualisierte Monatsabrechnung wie folgt aus.

![](Urlaub4.gif)

Bitte beachten Sie, dass die zusätzlichen Urlaubstage und auch die zusätzlichen Urlaubsstunden auch in den Folgejahren gelten. Daher ist es erforderlich, dass, wenn zusätzliche Tage definiert werden mussten, eine Neudefinition für das danach folgende Jahr durchgeführt wird.

#### Wie wird ein Gleitzeit Minus mit Urlaubstagen gegenverrechnet?
Hat ein Mitarbeiter ein deutliches Gleitzeit Minus so wird dieses oft mit Urlaubstagen gegenverrechnet.
D.h. es werden die nicht konsumierten Urlaubstage mit den zu viel konsumierten Gleitzeit Abwesenheitszeiten ausgeglichen.

Bestimmen Sie dazu wieviele Urlaubstage erforderlich sind, um das Gleitzeit Minus abzudecken.

Nun buchen Sie dem Mitarbeiter die ermittelte Gutstundenanzahl und reduzieren seinen Urlaubsanspruch um die Stunden und Tage, in dem Sie für das Jahr unter zusätzliche Tage die entsprechenden Tage mit Minus angeben. Zusätzlich geben Sie auch unter zusätzliche Stunden die Dauer als negativen Wert an.

<u>Beispiel:</u>

Der Mitarbeiter hat ein Gleitzeit Minus von 42Std.

Er arbeitet 38,5 Std an 5 Tagen die Woche. D.h. pro Tag ergeben sich durchschnittlich 7,7Std.

42 / 7,7 = 5,45 Std.

Das bedeutet Sie könnten nun dem Mitarbeiter 5,5 Urlaubstage gutschreiben.

Ändern Sie nun den Urlaubsanspruch für das Jahr und geben Sie unter zusätzliche Tage -5,5Tage ein.

Unter zusätzliche Stunden geben Sie -42,35 Std. ein.

In der Stundenabrechnung geben Sie zum gewünschten Stichtag, in unserem Beispiel der 31.12\. 42,35 Gutstunden ein.

Damit hat der Mitarbeiter letztendlich ein Saldo von 0,35Std auf seinem Gleitzeitguthaben.

Eingaben unter Urlaubsabrechnung:

![](Urlaubs-Gleitzeitausgleich1.gif)

Eingaben unter Stundenabrechnung:

![](Urlaubs-Gleitzeitausgleich2.gif)

#### Mein Mitarbeiter hat deutlich länger Urlaub gemacht.
Wie kann das gegen sein Gleitzeitguthaben aufgerechnet werden ?

Gehen Sie wie oben unter "Wie wird mein Gleitzeit Minus mit Urlaubstagen gegenverrechnet" vor, jedoch mit umgekehrten Vorzeichen.
**Hinweis:** Während des Jahres sollten eventuell zuerst die Gleitzeitguthaben abgebaut werden und dann erst die Urlaubstage. Damit vermeiden Sie am Jahresende Umbuchungen dieser Art. Verwenden Sie zur Protokollierung, zum Vorbuchen die Sondertätigkeit Zeitausgleich. Buchen Sie diese, so wie Sie den Urlaubsanspruch buchen in die Sondertätigkeiten des Mitarbeiters im Modul Zeiterfassung, Sondertätigkeiten beim jeweiligen Mitarbeiter ein.

#### Wie werden abgerechnete Minusstunden in **Kieselstein ERP** eingetragen?
Es gibt in Unternehmen Regelungen in der Gleitzeitvereinbarung, dass nur eine gewissen Anzahl an Minusstunden ins neue Jahr übertragen werden. Die Differenz auf die tatsächlichen Stunden wird beim Lohn/Gehalt abgezogen. Wie werden "abgerechnete Minusstunden" eingetragen, so dass die Information mit dem Lohnzettel übereinstimmen?
Eine Lösung ist, dass Sie die ausbezahlten/einbehaltenen Stunden im Modul Personal am Mitarbeiter, Stundenabrechnung, Gutstunden eingeben. Nun geben Sie im Kommentar die Begründung für die auf Grund des Gleitzeitsaldos abgerechneten Stunden ein. Damit wird der Gleitzeitsaldo wieder entsprechend richtig dargestellt. 
Es werden Gutstunden verwendet, weil Sie im Gegenzug dafür dem Mitarbeiter weniger Stunden ausbezahlen.

#### Beim Eintragen der Sondertätigkeit kommt keine Sollzeit gefunden?
Erscheint diese Meldung
![](Sondertaetigkeit1.jpg)
so bedeutet dies, dass für die eingegebenen Tage kein einziger Tag mit einer Sollzeit hinterlegt ist, z.B. weil dies ein Wochenende ist oder weil der (freie-)Mitarbeiter keine Sollzeiten definiert hat.
![](Sondertaetigkeit2.gif)

Sollte trotzdem die Zeitbuchung erforderlich sein, so geben Sie bitte die Tage einzeln ein.
D.h. z.B. nur für den 30.5.2020
![](Sondertaetigkeit3.gif)
so erscheint die Warnung
![](Sondertaetigkeit4.jpg)
Wird hier mit Ja bestätigt, so wird diese Sondertätigkeit trotzdem eingebucht und somit, je nach Definition der Sondertätigkeit selbst, in der Monatsabrechnung entsprechend berücksichtigt.

#### Arbeitszeitstatistik
Mit der Arbeitszeitstatistik (Zeitverwaltung, Journal) haben Sie ein praktisches Auswertewerkzeug für die von Ihren Mitarbeitern auf Projekte, Angebote, Aufträge, Fertigungsaufträge gebuchten Zeiten.
Die Sortierung der Auswertung ist immer so gestaltet, dass als letztes Kriterium der Zeitpunkt der Beginn der Tätigkeit verwendet wird. Im Feld des Beleges wird die Kurzbezeichnung der Belegart angezeigt.
Unter der Spalte Kosten finden Sie die laut Mandantenparameter [PERSONALKOSTEN_QUELLE](../personal/index.htm#Personalkosten in den Nachkalkulationen) definierten und mit der Dauer ausmultiplizierten Stundensätze.

Die Telefonzeiten werden ab der Version 8454 auch zum zugehörigen Projekt, wenn vorhanden, angezeigt, ansonsten als eigener Beleg mit der Belegnummer TZ+ Datum angeführt.

#### Die Arbeitszeitstatistik kann nicht gedruckt werden ?
Um die Arbeitszeitstatistik (Zeitverwaltung, Journal) auch tatsächlich ausdrucken zu können, ist das Recht PERS_SICHTBARKEIT_ALLE erforderlich.

#### Lohnverrechnung / Zeitabrechnung ?
Die **Kieselstein ERP** Zeiterfassung mit den angefügten Programmfunktionen ist eine reine Zeiterfassung. Es sind zwar durch die Auswertung der Überstunden, mit dem Versuch den gesamten gesetzlichen Wildwuchs zu implementieren, auch Teile / Informationen für die Lohnverrechnung enthalten. Wir müssen jedoch darauf bestehen, dass **Kieselstein ERP** Zeiterfassung nur für die Zwecke der Zeiterfassung geschaffen wurde. **D.h. die Zeiterfassung ist von der Lohnverrechnung völlig abgekoppelt.**
In der Zeitverrechnung ist es so, dass jedes Monat aufgrund seiner Kalendertage seine Sollstunden hat, die der Mitarbeiter zu erbringen hat. D.h. die Sollstunden je Monat wechseln entsprechend.
Dies hat mit der Errechnung des Lohnes eines Arbeiters nichts zu tun. Die Info ist, dass für die Ermittlung des Lohnes (und nur dafür) die Sollstunden einer Woche mit 4,33 multipliziert wird. Dies ist aber nur ein Hilfsmittel um auf den Monatslohn zu kommen. Dies hat mit der Zeitabrechnung nichts zu tun.
Dies bedeutet dass die Gegenüberstellung der Soll und Ist-Stunden pro Monat, auch für Teilzeitkräfte, so wie in **Kieselstein ERP** dargestellt durchzuführen ist.

#### Wie können die Überstunden definiert werden ?
#### Kann für den Überstunden-Saldo auch ein Zeitfaktor hinterlegt werden ?
#### Wie werden die ausbezahlten Überstunden ausgeglichen ?
Siehe dazu bitte [Überstundenabrechnung]( {{<relref "/fertigung/zeiterfassung/ueberstundenabrechnung">}} ).

### Buchen in alten / vergangenen Monaten
Um auch Korrekturen in alten Zeiten vornehmen zu können, ist das Recht FB_CHEFBUCHHALTER erforderlich.

Hat eine Rolle dieses Recht nicht, so können bis zu dem unter ZEITBUCHUNGEN_NACHTRAEGLICH_BUCHEN_BIS eingestellten Zeitraum noch im vorigen Monat Buchungen vorgenommen werden. Für Tage danach kann nur mehr im aktuellen Monat gebucht werden.

Ein Beispiel:<br>
Voraussetzung: das Chefbuchhalterrecht ist nicht gegeben und Zeitbuchungen können bis zum 15. nachträglich verändert werden.
- a.) heute sei der 14.7\. ... so kann ich ab 1.6\. Korrekturbuchungen machen
- b.) heute sei der 16.7\. ... es können Korrekturbuchungen nur ab dem 1.7\. gemacht werden.

Der Tag ab dem diese Regel greifen sollte, muss in Abstimmung mit Ihrer Personal / Lohnverrechnung festgelegt werden und kann im System unter Parameter ZEITBUCHUNGEN_NACHTRAEGLICH_BUCHEN_BIS eingestellt werden.

**Hinweis:**

Sollte, z.B. aus Gründen des Urlaubs, temporär das Nachbuchen auch länger erlaubt sein, so stellen Sie diesen Parameter auf -1.

### Aliquoter Urlaubsanspruch
Wird Tag genau gerechnet. D.h. Der eingegebene Jahres-Urlaubsanspruch wird durch 365 dividiert und mit dem Tag des Jahres multipliziert. Dies ergibt den aliquoten Urlaubsanspruch zum Stichtag.

Kurzarbeit in **Kieselstein ERP**

Es kommt in den Auf- und Ab der wirtschaftlichen Entwicklung immer wieder vor, dass Firmen mit Ihren Mitarbeitern Kurzarbeit vereinbaren müssen.

Uns sind derzeit zwei verschiedene Ausprägungen / Realisierungen der Kurzarbeit bekannt.

1.  Freistellung des Mitarbeiters für einen Zeitraum mit Wiedereinstellungsgarantie:
    In diesem Fall wird der Mitarbeiter gekündigt und erhält eine Wiedereinstellungsgarantie in der zugesichert wird, dass er nach einem gewissen Zeitraum (einige wenige Monate) wieder beschäftigt wird. Dieser Zeitraum wird üblicherweise mit dem Arbeitsmarktservice abgestimmt. Für den Mitarbeiter bedeutet dies, dass er gekündigt wird, für den Zeitraum einen (Groß-)Teil seines Gehaltes vom Arbeitsmarktservice = Staat bekommt und danach wieder ins Unternehmen eintritt. Damit verbunden ist durch die tatsächlich ausgesprochene Kündigung, dass die Ansprüche des Mitarbeiters (Urlaub, Überstunden) abgerechnet und ausbezahlt werden. Damit ist der Wiedereintritt wie eine Neuanstellung. Hier ist neben dem (Wieder-) Eintrittsdatum der Urlaubsanspruch der wichtigste Faktor. Ab den Programmversionen 2009 ist in **Kieselstein ERP** die Berechnung des aliquoten Urlaubsanspruches in der Form enthalten, dass immer der gesamte Jahres-Urlaubsanspruch in Tagen eingegeben werden muss. Der tatsächliche Urlaubsanspruch zum Stichtag wird anhand den anteiligen Urlaubstagen vom 1.1\. bis zum Stichtag abzüglich der theoretischen Urlaubstage vom 1.1\. bis zum Eintrittsdatum (einen Tag davor) errechnet. Als Eintrittsdatum der Berechnung wird immer das jüngste Eintrittsdatum verwendet.

2.  Kurzarbeit in der Form, dass der Mitarbeiter zwischen 10% und 90% seiner Sollanwesenheitszeit geringer arbeitet.
    Bei dieser Forderung kommt dazu, dass die weniger gearbeitete Zeit oft staatlichen Stellen nachgewiesen werden muss. Das bedingt, dass die bisherige Zeitvereinbarung mit dem Mitarbeiter aufrecht bleibt, er jedoch, seine geringere Anwesenheitszeit teilweise vom Staat ersetzt bekommt.
    Für die Personalverrechnung bedeutet dies wiederum, dass die Zeitmodelle der Mitarbeiter gleich bleiben müssen, um die geringere Anwesenheitszeit zu verdeutlichen. Am Ende der Kurzarbeitszeit, oder auch am Ende jedes Monats müssen die staatlichen Zuschüsse je Mitarbeiter als Gutstunden eingebucht werden. 
    Dies hat für Ihre Personalverrechnung(sabteilung) zugleich den Vorteil, dass die eingeforderten und die vom Staat vergüteten Stunden klar nachvollzogen werden können.

    Wie ist nun hier die Vorgehensweise ?
    Es bieten sich wiederum mehrere Möglichkeiten an:

    1.  Wurden mit den Mitarbeitern eine fixe Reduzierung der wöchentlichen Sollarbeitszeit von z.B. 40 auf 35Std. vereinbart, so kann für die Abrechnung gegenüber den Mitarbeitern mit einem eigenen neuen Zeitmodell diese reduzierte Sollzeit dargestellt werden. Für die Abrechnung mit den Unterstützungsstellen, wird die Sollzeit des Zeitmodells für den Zeitraum auf die bisherige Sollarbeitszeit erhöht und die Monatsabrechnungen erneut ausgedruckt. Am Ende der Unterstützungsdauer bleibt das längere Sollarbeitszeitmodell und die von den Unterstützungsstellen ausbezahlten Stunden werden als Gutstunden beim jeweiligen Mitarbeiter eingebucht. Diese Variante hat zusätzlich den Vorteil, dass bei Verwendung der stundenweisen Urlaubsabrechnung der volle Urlaubsanspruch erhalten bleibt.

    2.  Die Zeitmodelle werden wie bisher belassen und es werden die dadurch entstehenden Minus-Stunden durch die individuelle Erfassung der Gutstunden je Mitarbeiter mit Begründung ausgeglichen. Dies hat den Vorteil einer klaren und transparenten Abrechnung in der auch die Weiterrechnung des Gleitzeitsaldos aus der Zeit vor der Kurzarbeit transparent dargestellt wird.

    3.  Die Zeitmodelle werden wie bisher belassen. Am Ende der Kurzarbeitszeitperiode werden die von **Kieselstein ERP** errechneten Gleitzeitsaldi auf einen vereinbarten Wert (z.B. 0) gestellt und auf gefroren gesetzt, damit **Kieselstein ERP** diesen nicht mehr verändert.

<a name="Corona Kurzarbeit"></a>Corona / Covid-19 Kurzarbeit

Mit aktuellem Wissensstand (2020-04-16), scheint uns die Abbildung der Corona Kurzarbeit derzeit wie folgt am Einfachsten, wobei wir davon ausgehen, dass Ihre Mitarbeiter Tageweise in Kurzarbeit gesandt werden bzw. Tageweise im Unternehmen anwesend sind.
Hier geht es vor allem darum, dass die Gleitzeitsaldi Ihrer Mitarbeiter entsprechend ausgeglichen werden, also die nicht Anwesenheit wegen Corona Kurzarbeit, trotzdem in die Zeitabrechnung mit einfließt. Zugleich kommt mit dazu, dass die Urlaubsansprüche durch die Corona Kurzarbeit nicht gehemmt werden.
Das bedeutet auch, dass die Zeitmodelle der Mitarbeiter so bleiben wie bisher, es wird nur die angeordnete Kurzarbeit als bezahlte Abwesenheit erfasst.
Dafür gehen Sie wie folgt vor:

1.  legen Sie im Modul Zeiterfassung, unterer Reiter Sondertätigkeiten eine Sondertätigkeit Corona Kurzarbeit an
    ![](Corona_Kurzarbeit.jpg)

2.  weisen Sie den jeweiligen Mitarbeitern an den Kurzarbeitstagen diese Sondertätigkeit zu
    ![](Corona_Kurzarbeit2.jpg)

3.  Auswertung in der Monatsabrechnung
    ![](Corona_Kurzarbeit3.jpg)

    D.h. Sie sehen in der Monatsabrechnung sehr detailliert die für die Corona Kurzarbeit aufgewandten Stunden und können diese somit den Behörden gegenüber entsprechend argumentieren und diese auch in die Lohnverrechnung usw. entsprechend einfließen lassen.

Der Vorteil dieser Vorgehensweise ist auch, dass sich damit, also durch die geplante Abwesenheit, die verfügbare Anwesenheitszeit Ihrer Mitarbeiter ändert und sich damit auch die Kapazitätsplanung entsprechend anpasst.

Es ist damit zu rechnen, dass sich hier vom Gesetzgeber weitere Regelungen vorgegeben werden. Wir werden diese, soweit möglich, zeitnah implementieren.
Bitte beachten Sie dazu auch die Regelungen bzgl. Abbau von Überstunden / Gleitzeitsaldo und auch Urlauben.

Eine andere Möglichkeit, die jedoch wesentlich intransparenter ist, wäre, dass Sie dem Mitarbeiter seine Corona Gutstunden pauschal pro Monat/Woche im Personal einzubuchen. Also Personal, Mitarbeiter, Stundenabrechnung, Neu und mit Auszahlungsdatum und Stunden und Kommentar die entsprechenden Gut-Geschriebenen Coronastunden einzubuchen.
![](Corona_Kurzarbeit4.gif)
Da damit auch die verfügbaren Zeiten in der Zukunft nicht angepasst werden, ist die Version mit den Sondertätigkeiten eindeutig vorzuziehen.

<a name="Heilig Abend"></a>

#### Wie wird der Urlaub für den 24.12\. bzw. 31.12\. richtig verbucht?
Das hängt davon ab, ob für Sie / Ihre Mitarbeiter die tageweise oder die stundenweise Urlaubsabrechnung ausschlaggebend ist.
Hier kommt noch dazu, dass, zumindest in Österreich, bei diesen Tagen (Hl. Abend, Weihnachten, Silvester und für Bayern teilweise auch Fasching Dienstag) den Mitarbeitern ab 12:00 bezahlte Freizeit zu geben ist. [Siehe auch](../personal/Urlaubsanspruch.htm#Halbtag)
Das bedeutet, je nachdem wie Ihre Normalarbeitszeit definiert ist, dass für Mitarbeiter die den ganzen 24.12\. frei haben, neben dem halben Urlaubstag noch zusätzliche Stunden freizugeben sind. Hier hat sich bewährt, dass bei stundenweiser Urlaubsabrechnung der benötigte Urlaub in Stunden eingegeben wird, bei tageweiser Urlaubsabrechnung empfiehlt sich den halben Urlaubstag einzugeben und die zusätzlich frei genommenen Stunden als Zeitausgleich abzurechnen. D.h. diese z.B. als Zeitausgleich vorzubuchen.

Ein besonderer Fall ergibt sich, wenn der 24.12\. und somit der 31.12\. auf einen Freitag fallen und die Sollzeit des Freitags nur z.B. 4Std beträgt.
Im Volksmund wird hier allgemein von man gebe dafür einen halben Urlaubstag um am Vormittag auch frei zu bekommen gesprochen.
Das würde bewirken, dass der Mitarbeiter nur 2Std, die Hälfte von 4Std, Urlaub bekommt, was so nicht gewünscht ist.
Zusätzlich steht in manchen österreichischen Kollektivverträgen, dass am 24.12\. ab 12:00 frei zu geben ist. Hinweis: Laut [WKO](https://www.wko.at/service/arbeitsrecht-sozialrecht/Arbeiten_an_Feiertagen.html) sind sowohl der 24.12\. wie der 31.12\. KEIN Feiertag.
Das würde streng genommen bedeuten, dass die Mitarbeiter, wenn der 24.12\. auf einen Freitag fällt, einen normalen Arbeitstag haben.
Im Prinzip würde dies bedeuten, dass Sie für diesen besonderen Feiertagsfreitag eine eigene Tagesart mit den dazupassenden Sollstunden anlegen müssten.
In der Praxis bewährt hat sich, dass man, wenn der 24.12\. auf einen Freitag fällt, dass man z.B. für den 24.12\. einen ganzen Feiertag einträgt und für den 31.12\. dann einen Urlaubstag. Damit haben beiden Seiten recht, obwohl es streng betrachtet zu Lasten des Arbeitgebers geht.

#### Es werden für den 24.12\. keine Sollstunden gerechnet?
Manchmal bekommen wir von Anwendern diese Bitte um Hilfestellung.
![](fehlende_Kalender_Definition_Halbtag.jpg)
Aus diesem Bild ist ersichtlich dass
a.) der 24.12\. als voller Feiertag definiert ist. D.h. hier muss der Betriebskalender richtiggestellt werden und für den 24\. und den 31\. der Halbtag definiert werden
b.) vermutlich fehlt auch im Zeitmodell die Definition für den Halbtag. [Siehe dazu bitte die Definition des Halbtages.](../personal/Urlaubsanspruch.htm#Halbtag)

#### Wie wird eine gesetzliche Arbeitsunterbrechung eingetragen?
Es kommt immer wieder vor, dass Mitarbeiter verpflichtet sind, Staatsdienste (Katastropheneinsatz, Waffenübungen, ..) zu leisten.

Der Mitarbeiter erhält für diesen Zeitraum eine Aufwandsentschädigung, welche nicht vom Unternehmen zu tragen ist, jedoch bleiben seine Ansprüche voll und ganz bestehen.

Würde man nun für den Mitarbeiter einen Austritt eintragen und ihn, nach oft nur wenigen Tagen wieder eintreten lassen, so beginnt die Berechnung aller Ansprüche mit dem neuen Eintrittsdatum.

Es ist vielmehr so, dass der Mitarbeiter für diesen Zeitraum keine Stundenverpflichtung im Unternehmen hat.

Die Lösung dafür lautet:

Legen Sie ein Zeitmodell ohne Sollstunden an.

Hinterlegen Sie für den Zeitraum des Staatsdienstes dieses Zeitmodell beim Mitarbeiter. Da keine Sollzeiten hinterlegt sind, fallen auch keine Minusstunden an. Würde der Mitarbeiter trotzdem für Sie arbeiten, werden diese Stunden als Überstunden / Mehrzeit erfasst.

Dadurch bleiben alle Ansprüche des Mitarbeiters vollständig erhalten.

<u>Zusätzlicher Effekt:</u>

Da die Urlaubsabrechnung auf die Sollstunden pro Tag basiert, bewirkt diese Zeitmodelländerung auf 0 Sollstunden, dass um die Tage des Staatsdienstes der Mitarbeiter weniger anteilige Urlaubstage/stunden pro Jahr hat.

Mathematisch ist diese Betrachtung richtig. Es könnte sein, dass in einigen Unternehmen / Kollektiv / Tarif-Verträgen dies so geregelt ist, dass dadurch auch der Urlaubsanspruch in vollem Umfang erhalten bleibt. In diesem Falle müssten die Differenzstunden/Tage als zusätzliche Urlaubsstunden/Tage beim Urlaubsanspruch eingetragen werden.

#### Erfassen der Zeiten für Mutterschutz 
Dafür gibt es zwei Varianten, welche abhängig vom Urlaubsanspruch während der Zeit des Mutterschutzes gewählt werden müssen.

1.  Während des Mutterschutzes entsteht Urlaubsanspruch. (In Österreich 2 Monate vor und 2 Monate nach dem errechneten Geburtstermin)
    In diesem Fall bleibt das Zeitmodell der Mitarbeiterin unverändert.
    Zusätzlich wird eine Sondertätigkeit Mutterschutz angelegt und diese als 100% bezahlt definiert.
    Während der Zeit des Mutterschutzes der Mitarbeiterin wird diese, genauso wie Urlaub (auch wenn dies für die Mitarbeiterin kein Urlaub ist), hinterlegt.
    Durch diese bezahlte Abwesenheit, wie z.B. auch Berufsschule, bezahlte Ausbildung, bleibt der Urlaubsanspruch erhalten und gleichzeitig ist der Gleitzeitsaldo ausgeglichen

2.  Während der Zeit des Mutterschutzes entsteht kein Urlaubsanspruch.
    Hier wird ein Zeitmodell Mutterschutz erstellt. Dieses Zeitmodell wird so erstellt, dass es keine Sollzeiten hat und dass die Urlaubstage pro Woche mit 0,00 besetzt sind.
    Ab Beginn des Mutterschutzes wird dieses der Mitarbeiterin zugeordnet.
    Ab Beginn der Arbeitsaufnahme wird wieder das vereinbarte Zeitmodell mit entsprechenden Sollzeiten verwendet.

#### Erfassen der Zeiten für Karenz / Elternzeit 
1.  Während der Zeit der Karenz/Elternzeit entsteht kein Urlaubsanspruch, dies ist in AT/DE der übliche Fall.
    Hier wird ein Zeitmodell Karenz erstellt. Dieses Zeitmodell wird so erstellt, dass es keine Sollzeiten hat und dass die Urlaubstage pro Woche mit 0,00 besetzt sind.
    Ab Beginn der Karenz wird dieses der Mitarbeiterin zugeordnet.
    Ab Beginn der Arbeitsaufnahme wird wieder das vereinbarte Zeitmodell mit entsprechenden Sollzeiten verwendet.

2.  Während der Karenz / Elternzeit entsteht Urlaubsanspruch.
    In diesem Fall bleibt das Zeitmodell der Mitarbeiterin unverändert.
    Zusätzlich wird eine Sondertätigkeit Karenz angelegt und diese als 100% bezahlt definiert.
    Während der Zeit der Karenz der Mitarbeiterin wird diese, genauso wie Urlaub (auch wenn dies für die Mitarbeiterin kein Urlaub ist), hinterlegt.
    Durch diese bezahlte Abwesenheit, wie z.B. auch Berufsschule, bezahlte Ausbildung, bleibt der Urlaubsanspruch erhalten und gleichzeitig ist der Gleitzeitsaldo ausgeglichen

#### Alternative Möglichkeit für die Karenz- / Elternzeit 
In der Regel sind die Mütter und Väter wärend der Karenzzeit im Unternehmen nicht angestellt / beschäftigt.
D.h. eigentlich treten sie quasi aus dem Unternehmen aus und treten dann zum Ende der Karenzzeit wieder im Unternehmen, eventuell mit geänderten Zeitvereinbarungen, ein.
Insbesondere für die Darstellung der Urlaubsansprüche während der jeweiligen Karenzzeit, hat sich bewährt, dass
- der / die MitarbeiterIn zum Beginn der Karenzzeit aus dem Unternehmen austritt
- die Person wird NICHT versteckt
- mit dem Ende der Karenzzeit tritt die Person wieder ein und es wird beim Eintritt angegeben, dass dies ein Wiedereintritt ist, was eben bedeutet, dass der Urlaub aus der Zeit davor übernommen wird und nun wieder weitergerechnet wird.

Bitte beachten Sie dazu in jedem Falle auch den Parameter URLAUBSANSPRUCH_LINEAR_VERTEILT.
Steht dieser auf 1, so wird der gesamte Jahres-Urlaubsanspruch auf das gesamte Jahr verteilt.
Steht dieser auf 0, so wird bei jedem Wiedereintritt und bei jedem Zeitmodellwechsel der Urlaubsanspruch auf den sich daraus ergebenden Zeitbereich verteilt.

#### Erfassen der Zeiten für Elternteilzeit 
Legen Sie für den Zeitraum der Elternteilzeit ein Zeitmodell mit 0 Urlaubsstunden und 0 Sollstunden an. Mit Beginn der Elternteilzeit hinterlegen Sie das Zeitmodell bei der Person und drucken die Monatsabrechnung für den Letzten des Jahres in dem die Elternteilzeit endet. Nun buchen Sie die "unrunden" Zahlen aus - [siehe](#Urlaubkorr).

#### Zeiten für unbezahlten Urlaub
Oft ist es so, dass Ihre Mitarbeiter wegen Ihrer Qualifikation auch von anderen Organisationen wie Feuerwehr, Rotes Kreuz, aber auch dem Bund geschätzt und dafür für verschiedene Einsätze wie Katastropheneinsätze und Ähnliches herangezogen werden.

Diese Abwesenheitszeiten haben die Eigenschaft, dass sie, wenn sie länger als z.B. 30Tage dauern, den Urlaubsanspruch unterbrechen. Erstellen Sie daher, ähnlich wie oben unter Mutterschutz 1.) beschrieben, ein Zeitmodell z.B. Katastrophenschutz. Damit wird erreicht, dass der Urlaubsanspruch für das laufende Jahr entsprechend reduziert wird.

Dies gilt auch für Bildungskarenz, Zeiten für Präsenzdienst und ähnliches.

#### An Feiertagen wird für die Schichtarbeiter keine Rundung gerechnet?
Ihre Mitarbeiter arbeiten auch in der Schichtzeit.
Üblicherweise ist z.B. der Beginn am Sonntag um 21:30\.
Die Mitarbeiter kommen rechtzeitig, also vor 21:30\.
An allen normalen Wochentagen wird der Beginn und die Dauer der Anwesenheit richtig berechnet. Nur am Feiertag nicht.
Lösung: Definieren Sie auch den Feiertag in seiner Tagesdefinition. Ist für ein Datum im Betriebskalender eine Tagesart, z.B. Feiertag eingetragen, so gilt für dieses Datum die Tagesart des eingetragenen Tages, also z.B. der Feiertag. Damit dieser Tag richtig behandelt werden kann, muss auch diese Tagesart in jedem Zeitmodell definiert sein.
[Siehe dazu auch](../personal/Zeitmodell_Definition.htm#Welche Tage / Tagesarten müssen definiert werden).

#### Nur Anzeigen der Auswertungen aber nicht drucken
In der Zeiterfassung kann mit dem Recht PERS_ZEITERFASSUNG_NUR_DRUCKEN gesteuert werden, ob ein Mitarbeiter die Auswertungen der Zeiterfassung nur in die Vorschau "drucken" darf, oder ob er sie tatsächlich ausdrucken darf, wobei mit ausdrucken auch Faxen, Versand per EMail und speichern gekoppelt ist.

#### Können im Nachhinein Zeitmodelle bzw. Automatikbuchungen geändert werden ?
Selbstverständlich. Bitte achten Sie dabei auf:
a.) haben sich die Regeln für die automatischen Pausen geändert. Dann sollte Sie die Pflege im Modul Zeiterfassung laufen lassen um diese Dinge zu korrigieren.
b.) Es müssen die Monatsrechnungen ab den Änderungen der Zeitmodelle neu ausgedruckt werden (zumindest in die Druckvorschau) damit die Gleitzeit- und Überstundensaldi neu berechnet werden und somit die Übertragsbuchungen wieder richtig gestellt sind.

#### Die in der Monatsabrechnung ausgewiesenen Unterbrechungen stimmen mit den im jeweiligen Zeitmodelle definierten Pausen nicht überein.
Die Pausen werden immer bei einer Geht-Buchung automatisch in die Zeiterfassung des Mitarbeiters eingetragen. Es werden die zum Zeitpunkt der Geht-Buchung gültigen Regeln für die Berechnung der Pausen herangezogen. Wurden nun die Zeitmodelle geändert und keine Pflege der Zeitbuchungen durchgeführt, so sind die Pausen nach den alten Definitionen eingetragen. Führen Sie bitte die Pflege durch. Siehe oben.

#### Kann ich eine Jahresabrechnung ausdrucken?
Im Modul Zeiterfassung finden Sie unter Info, Zeitsalden die Möglichkeit die Daten der Monatsabrechnung für einen oder alle Mitarbeiter auszugeben.

#### Wie kommt man zu einer Gesamtstundenübersicht?
Im Modul Zeiterfassung, im Menü Journal, Auftragszeitstatistik erhalten Sie eine Komplettübersicht über alle Zeiten Ihrer Mitarbeiter im Zeitraum (von - bis) auf die unterschiedlichen Aufträge, bzw. Angebote.

In den Auftragszeiten sind auch die indirekten Zeiten enthalten, d.h. die Zeiten die auf Lose gebucht wurden, die dem jeweiligen Auftrag zugeordnet sind.

In der Spalte nicht Zuordenbar sind diejenigen Anwesenheitszeiten angeführt, welche nicht auf die Aufträge bzw. Angebote zugeordnet werden konnten. Also z.B. die Zeiten die der Mitarbeiter zwar anwesend war, wo er jedoch nicht auf einen Auftrag, ein Los bzw. ein Angebot gebucht hat.

In der rechten Spalte wird die Gesamtzeit aller Auftrags- und Angebotsstunden sowie den nicht zuordenbaren Stunden angezeigt.

#### Import von Sondertätigkeiten
In manchen Anwendungen ist es praktisch, wenn man bestimmte Sondertätigkeiten aus einer Excel-Datei importieren kann.

Verwenden Sie dazu den Menüpunkt Zeiterfassung, Sonderzeitenimport.

Die Definition für die Exceldatei ist folgende:

An B2 steht das Startdatum

Ab B7, vertikal nach unten stehen die Personalnummern der Personen

Ab C7 beginnen die Daten, nach rechts weiter, soviel wie Sie Daten haben. Der erste Tag ist jener mit dem Startdatum, dann nach rechts weiter lückenlos aufsteigend. Achtung: Nach jedem Monatsbereich muss nach dem letzten Tag im Monat eine (und nur eine!) Leerspalte eingefügt sein.

Da in der Regel am Ende Summierungen sind, werden die letzten vier Zeilen nicht importiert.

Es werden jedoch alle Blätter importiert.

Der Zusammenhang zwischen den Inhalten der Felder und den Sondertätigkeiten ist folgender:

In den Feldern stehen Zeichen (Ziffern bzw. Buchstaben, jeweils einer).

Diese korrespondieren mit jeweils eine Sondertätigkeit. D.h. die Zuordnung von den Zeichen zur Sondertätigkeit muss in der Definition der Sondertätigkeiten (unterer Modulreiter im Modul Zeiterfassung) im Feld Importkennzeichen vorgenommen werden.

![](Sondertaetigkeiten_Excel_Definition.gif)

Ablauf des Imports.

Beim Import, wird nach der Frage nach der zu importierenden Datei, der Zeitbereich abgefragt der importiert werden sollte. Default ist dies das aktuelle Monat.

Nun werden alle bereits gebuchten Sondertätigkeiten mit Importkennzeichen in diesem Zeitbereich gelöscht und die anhand der Daten ermittelten Sondertätigkeiten für den jeweiligen Tag eingebucht.

Somit können Sie die Sondertätigkeiten über diese Definition laufend aktualisieren.

Eine Musterdatei mit den Formeln für die Tagesberechnung senden wir Ihnen gerne zu.

**Hinweis:**

Da in der Regel ein ganzes Jahr oder mehr in diesen Definitionen enthalten ist, aber immer wieder nur einzelne Monate angezeigt, ausgedruckt werden möchten, verwenden Sie bitte die Funktion Spalten Ausblenden / Einblenden. (auszublendende Spalten markieren, rechte Maus, Ausblenden bzw. die Randspalten der ausgeblendeten Spalten markieren und rechte Maus, Spalten Einblenden).

#### Wie werden offene Buchungen angezeigt?
Gerade in der Produktion aber auch in allen Projektsteuerungen ist es immer wieder von Bedeutung zu wissen, ob auf einem Los/Fertigungsauftrag/Auftrag/Angebot/Projekt gerade gearbeitet wird. Gerade gearbeitet bedeutet, der Mitarbeiter hat seine Arbeit begonnen aber noch nicht abgeschlossen.

Diese Anzeigen sehen Sie im jeweiligen Modul unter dem Reiter Istzeiten. Wird hier beim Ende einer Buchung unter Bezeichnung OFFEN; .... angezeigt, so bedeutet dies, dass es für diese Tätigkeit noch kein gültiges Ende gibt. Der angezeigt Zeitpunkt ist der Zeitpunkt der Auswertung.

![](Offene_Buchungen.gif)

#### Protokollierung der Änderungen der Zeitdaten
Unter Info, Änderungen sehen Sie die Änderungen, die an Zeitdaten durchgeführt wurden. Diese Protokollierung ermöglicht eine wesentliche Zeitersparnis beim Such-/Prüfaufwand bei eventuellen Ungereimtheiten. Die Aufzeichnung von veränderten bzw. gelöschten Zeitbuchungen erreicht Nachvollziehbarkeit für Sie und Ihre MitarbeiterInnen und somit beidseitige Transparenz.

#### Wochenweise Freigabe der Zeitbuchungen von Mitarbeiter
Unter Info, Wochenjournal finden Sie einen Ausdruck, der auf Wochenbasis alle gebuchten Zeiten des Mitarbeiters plus Gleitzeit- und Urlaubssaldo dokumentiert. Zur Bestätigung, dass alle Zeiten der letzten Woche erfasst und korrekt sind, legen Sie diesen Ausdruck Ihren MitarbeiterInnen zur Unterschrift vor. So erhalten Sie beiderseits Einverständnis über Zeiten, die an Kunden weiterverrechnet werden können und Urlaubs- und Gleitzeitsaldi. Je weniger Zeit zwischen der Erfassung und der Freigabe von Zeiten liegt, umso leichter können Sie Ungereimtheiten klären, da die Erinnerung noch "frisch" ist.

Freigabe der Zeiten des Mitarbeiters

Um zum Beispiel für die Abrechnung der Aufwände die korrekten Arbeitsstunden vorliegen zu haben, ist es nötig zu wissen, ob alle Mitarbeiter die Zeiten für diesen Auftrag schon gebucht haben. 

Dazu gibt es die Funktion "Zeiten abschließen".

Wechseln Sie in den oberen Reiter Zeiten abschließen, klicken auf Neu und geben das Datum an, bis wann die Zeiterfassung durchgeführt wurde![](zeiten_abgeschlossen.JPG).

Dieser Eintrag kann nur geändert/gelöscht werden, wenn das Recht PERS_ZEITEN_ABSCHLIESSEN_CUD vorhanden ist.

Mit der Statistik Journal/Abgeschlossene Zeitbuchungen erhalten Sie einen Überblick, welche Mitarbeiter bis zu welchem Datum die Zeiten freigegeben haben.

#### Wie wird ein Urlaubsantrag gestellt?
<a name="Urlaubsantrag"></a>

Wenn der Parameter URLAUBSANTRAG auf 1 gestellt ist, so können im Reiter Sonderzeiten der Zeiterfassung entsprechende Urlaubsanträge gestellt werden.

Durch Klick auf ![](Urlaubsantrag.gif) Urlaubsantrag stellen, werden im nachfolgenden Dialog die entsprechenden Urlaubsantrags-Tage erfasst, ausgedruckt und eingetragen. Dies kann von jeder Person, die auf das Modul Zeiterfassung schreibenden Zugriff hat durchgeführt werden. Ein Urlaubsantrag kann für ganze und für halbe Tage gestellt werden.

Der Urlaubsantrag wird auch in der Mitarbeitereinteilung entsprechend angezeigt. Wenn ein neuer Urlaubsantrag gestellt wird, wird im Anschluss ein Mail zur Information an den Vorgesetzten geschickt.

Sollte ein Urlaubsantragsdruck versehentlich verworfen / ungültig sein, so muss der Urlaubsantragsdialog erneut aufgerufen werden. Bitte beachten Sie, dass Urlaubsanträge nur für noch nicht durch andere Sondertätigkeiten belegte Tage eingetragen werden können. Gegebenenfalls müssen die eingetragenen Urlaubsantragstage gelöscht und neu eingebucht werden.

#### An wen wird der Urlaubsantrag gesandt?
Der Urlaubsantrag wir abhängig von der Personalfunktion des beantragenden Mitarbeiters versandt

| Personalfunktion | Antragsemail wird versandt an |
| --- |  --- |
| kein Eintrag | Ist unter Abteilung ein Eintrag so wird es an den Abteilungsleiter gesandt. D.h. an die Person, bei der bei der gleichen Abteilung bei der Personalfunktion Abteilungsleiter eingetragen ist. Ist keine Abteilung eingetragen wird das EMail an die Geschäftsleitung gesandt. |
| Abteilungsleiter | Empfängt die Urlaubsanträge seiner Abteilung. Sein eigener Urlaubsantrag geht an die Geschäftsleitung. |
| Geschäftsleitung | Empfängt alle Urlaubsanträge die von Mitarbeitern die keiner Abteilung zugewiesen sind versandt werden und die Urlaubsanträge der Abteilungsleiter |

Zusätzlich wird der Urlaubsantrag in CC an die Lohnverrechnung versandt (die Personen, die als Personalfunktion Lohnverrechnung eingetragen haben). Wichtig: Wenn kein Abteilungsleiter und auch keine Geschäftsleitung definiert sind, wird das Empfänger-EMail nicht vorbesetzt, womit das EMail nicht versandt wird. Daher bitte zumindest die Geschäftsleitung definieren.

**Umwandeln eines Urlaubsantrages in genehmigten Urlaub.**
Diese Umwandlung kann nur von Mitarbeitern mit Personalfunktion (Modul Personal, Detail) Abteilungsleiter, Geschäftsleitung oder Lohnverrechnung durchgeführt werden. Abteilungsleiter dürfen dies nur für Personen der eigenen Abteilung, aber nicht für sich selbst durchführen.

Die Umwandlung erfolgt in der Form, dass im Modul Zeiterfassung, auf der jeweiligen Person im oberen Reiter Sonderzeiten die Anzeige aller Sonderzeiten des Mitarbeiters aufgerufen wird. Markieren Sie nun die genehmigten Urlaubsanträge und klicken Sie auf ![](Urlaubsantrag2.gif) Urlaubsantrag in Urlaub umwandeln. Sollte ein Urlaub nicht genehmigt werden, so wird der von Mitarbeiter eingetragene Urlaubsantrag automatisch aus den Sonderzeiten gelöscht.

Um dem Mitarbeiter den Urlaubsantrag per EMail zukommen zu lassen, einfach auf das EMail ICON des Gehnehmigungsdruckes klicken. Hier ist ale EMail Empfänger die persönliche (private) EMail-Adresse des Antragstellenden enthalten.

#### Der Urlaubsantrag bleibt im Postausgang?
Damit ein Mitarbeiter einen Urlaubsantrag verschicken kann, muss eine E-Mail Adresse hinterlegt sein. Das Eingabefeld für die E-Mail Adresse finden Sie im Modul Personal - oberer Reiter Daten. Tragen Sie hier bitte eine gültige E-Mail Adresse des Mitarbeiters innerhalb deines Unternehmens ein. Danach kann der Urlaubsantrag verschickt werden. Beim Versenden des Antrags erscheint eine Fehlermeldung zur Information des Mitarbeiters, falls keine E-Mail Adresse hinterlegt ist.

#### Kann auch Zeitausgleich oder Krankenstand beantragt werden?
Ebenso wie ein Urlaubsantrag kann auch ein Zeitausgleichsantrag gestellt werden, da auch hier die Kollegen und vor allem die Kapazitätsplanung wissen sollte, dass derjenige MitarbeiterIn geplanter Weise nicht anwesend ist. Ein Krankenstandsantrag klingt einerseits unsinnig, andererseits können Mitarbeiter auf Heimarbeitsplätzen und auch über die mobile Zeiterfassung in die Verlegenheit kommen, dass sie jetzt krank geworden sind, z.B. bei einer Verletzung am Arbeitsplatz. Damit können sie, ohne lange ins Unternehmen fahren zu müssen, den KollegInnen signalisieren, dass sie jetzt nicht da sind. Die Freigabe dieser Anträge erfolgt wie oben unter Umwandelung in genehmigten Urlaub beschrieben.

#### Welche Mitarbeiter dürfen Sondertätigkeiten buchen?
<a name="Wer_darf_Sondertätigkeiten_buchen"></a>
Um Sondertätigkeiten buchen zu können muss der Benutzer das Recht PERS_SONDERZEITEN_CUD besitzen. Besitzt er dieses Recht nicht, so kann er seine Sonderzeiten zwar einsehen aber nicht verändern. Davon ausgenommen sind Urlaubsanträge, die er trotzdem stellen kann.
Bitte beachten Sie in diesem Zusammenhang auch die Eigenschaft darf selber buchen in der Definition der Sondertätigkeiten. Diese bedeutet, dass obwohl der Mitarbeiter kein Recht hat Sondertätigkeiten zu buchen, da ihm obiges Recht fehlt, er trotzdem diese Sondertätigkeit selbst buchen darf.

#### Wie werden Telefonzeiten dokumentiert?
<a name="Telefonzeiten erfassen"></a>
Sie finden im Modul Zeiterfassung den oberen Reiter Telefonzeit, hier dokumentieren Sie Telefonate mit Partnern. Mit dem Klick auf Neu startet die Zeiterfassung, wählen Sie nun den Partner und dessen Ansprechpartner mit dem Sie telefonieren aus und erfassen das Telefonprotokoll im Feld Kommentar.

Es gibt 3 Arten von Telefonzeiten:<br>
1.) Zeiten, die einem Projekt zugeordnet sind<br>
2.) Zeiten, die nur einen Partner hinterlegt haben<br>
3.) Zeiten, welche weder Partner noch Projekt hinterlegt haben.

Zusätzlich kann beim Mitarbeitenden hinterlegt werden, dass die Telefonzeiterfassung automatisch gestartet wird, sobald diese über das Telefon-Icon wählt.

Um diese Funktion für den Mitarbeiter einzuschalten wechseln Sie in das Modul Personal, unterer Reiter Personal, oberer Reiter Daten und setzen den Haken bei Telefonzeit mit Telefonzeitstart.

![](Telefonzeit_start_aktivieren.JPG)

Wenn der Mitarbeiter nun über die Direktwahl ![](telefonzeit_starten.JPG) einen Partner aus den Modulen Partner, Kunde, Lieferant und Projekt kontaktiert, so wird die Telefonzeiterfassung gestartet und mit den bekannten Daten befüllt.

![](telefonzeiterfasst.JPG)

#### Können für Telefonzeiten auch Bilder erfasst werden ?
Da die Telefonzeiten gerne auch für die detaillierten Gesprächsnotizen verwendet werden, können auch Bilder bei jeder Telefonzeit hinterlegt werden.
Klicken Sie dazu auf die Filmrolle ![](Telefonzeit_Grafik_Erfassen.gif) und erfassen Sie die gewünschten Bilder.<br>
Bitte beachten Sie, dass bei den Bildern nur die üblichen Grafikformate gespeichert werden können, also PNG, JPG, GIF, TIFF.<br>
Die weitere Bedienung ist wie unter [Bilder in den Belegen hinzufügen]( {{<relref "/verkauf/gemeinsamkeiten/#k%c3%b6nnen-bilder-in-den-belegen-eingef%c3%bcgt-werden" >}} ) beschrieben.<br>
Um noch schneller Bilder zur Telefonzeit erfassen zu können, kann einfach ein Bild direkt im Erfassungsdialog der Telefonzeit auf die Filmrolle ![](Telefonzeit_Grafik_Erfassen.gif) gezogen werden. Die Übernahme des Bildes wird durch einen kleinen Dialog bestätigt, welcher nach ca. einer Sekunde verschwindet.<br>
Werden mehrere Bilder in dieser Art hinzugefügt, so werden diese der Reihe nach, bei den Bildern, angefügt.

#### Können die Telefonzeiten auch gedruckt werden?
Durch Klick auf den Drucker-Button ![](Telefonzeit_drucken.gif) werden die Einträge der gewählten Telefonzeit ausgedruckt.

#### Wie komme ich zum Telefonzeiteintrag im 360° Kundencockpit?
Um vom gewählten / angezeigten Telefonzeiteintrag in die Sicht des Kundencockpits zu kommen, klicken Sie auf den Button des 360° Kundencockpits ![](Telefonzeit_Kundencockpit.gif). Damit gelangen Sie, in der Form eines GoTos direkt in den gleichen Telefonzeiteintrag im Kundencockpit und sind so mit einem Klick auf der gesamten Information zu diesem Kunden bzw. Lieferanten bzw. Partner.

#### Kann ich den Zeitbereich / das Datum für die Monatsabrechnung eingeben?
Gerade wenn es um das Thema Überstunden und deren Abrechnung geht, taucht auch immer wieder die Frage nach "ich muss doch die Überstunden des aktuellen Monats mit dem Gehalt / Lohn gemeinsam ausbezahlen" auf. Daher wird oft versucht, eine Monatsabrechnung z.B. vom 25\. zum 25\. zu machen.<br>
<u>Unser Tipp:</u> Denken Sie an die klaren und sauberen Monatsabgrenzungen und auch an die Menschen die die Zeitabrechnung und die Lohnverrechnung machen müssen. Es gibt absolut keine Notwendigkeit die Überstunden in dem Monat auszubezahlen in dem sie erbracht wurden. D.h. die Monatsabrechnung eines Monats wird am Beginn des nachfolgenden Monats gemacht. Da stehen alle Zeiten (Überstunden, Arzt, Zeitausgleich, Urlaub, ...) fest. Ist die Monatsabrechnung fertig geht diese zur Lohnverrechnung, die ja nun nur mehr die auszubezahlenden Stunden bekanntgegeben bekommt. Die Lohnverrechnung erfasst dies (ebenfalls in aller Ruhe) und rechnet mit der Auszahlung des Basis Gehalts / Lohns die Überstunden des vorigen Monates ab. Damit haben alle beteiligten Personen Zeit dies in aller erforderlicher Sorgfalt zu erledigen. Es kommt natürlich mit dazu, dass ich nichts abrechnen kann, was noch nicht geleistet wurde (auch wenn manche Gesetzgeber hier von anderen Dingen träumen). D.h. für z.B. den Juli erhält der Mitarbeiter sein Basis Gehalt, seinen Basis Lohn (und ev. die Überstunden für Juni) mit Ende des Monates, vermutlich auch gerne ein paar Tage früher. Mit Anfang August wird der Juli vollständig abgerechnet und dann, so um den 10\. herum die Monatsabrechnungen an die Lohnverrechnung übermittelt. Mit ca. den 25\. August erhalten Sie die Daten von Ihrer Lohnverrechnung, in der die August Basis Gehälter/Löhne und die Überstunden des Juli abgerechnet sind.<br>
Lassen Sie sich von niemanden dazu drängen dass bis zum 5\. des Folgemonates oder ähnlichem zu machen. Denken Sie an Urlaube und Krankenstände Ihrer Personalabteilung.

#### Können auch Tageweise Gutzeiten gebucht werden
<a name="Gutzeiten pro Tag"></a>
Es steht auch die Tageweise Anrechnung von Gutstunden zur Verfügung.<br>
Der Gedankengang dahinter ist folgender:<br>
Mitarbeiter bekommen für z.B. das Umkleiden oder das Reinigen täglich eine gewisse Zeit, z.B. vor und nach dem eigentlichen Kommt und nach dem eigentlich Geht gutgeschrieben. Vor dem eigentlich Kommt deswegen, da die Anwesenheitserfassung erst mit Betreten bzw. verlassen des tatsächlichen Arbeitsplatzes gebucht wird. Nun muss sich der Mitarbeiter ja noch waschen und umziehen, also bekommt er eine entsprechende Zeitgutschrift.<br>
Definieren Sie diese tägliche Zeitgutschrift in den Details des Zeitmodells des Mitarbeiters bei Zeitgutschrift Kommt bzw. Geht. Sind hier Werte angegeben, werden diese hh:mm (Stunden : Minuten) dem Mitarbeiter beim Kommt bzw. Geht zu seinen Gunsten hinzugerechnet. Um dies auch in die Überstundenabrechnung eingehen zu lassen, wird Kommt und Geht getrennt erfasst.
Nun gibt es keine Regel ohne Ausnahme. D.h. sollte der Mitarbeiter an einem Tag eine andere Zeitgutschrift erhalten, als sich dies anhand des Zeitmodells ergibt, so können Sie im Reiter Zeitgutschrift in der Zeiterfassung für jeden Tag seine Zeitgutschrift für Kommt bzw. Geht erfassen. Da hier auch eine Gutschrift von 0:00 erfasst werden kann, kann damit die Zeitgutschrift für einen einzelnen Tag gelöscht, also auf Null gesetzt werden. Die Gutzeiten werden auf der Monatsabrechnung in der Spalte Zt-GS (Zeitgutschrift) ausgewiesen. Beachten Sie, dass diese Stunden bereits in der Ist-Anwesenheits-Zeit enthalten ist.<br>
Info: Für die Übergabe der Gutschriftsstunden über die Lohn-Export-Schnittstelle stehen die Lohnarten Zeitgutschrift Kommt / Geht zur Verfügung. Bitte beachten Sie auch hier, dass die Stunden bereits in den Kommt-Geht, also den Anwesenheitszeiten enthalten sind.

#### Was bedeutet der Haken in der rechten Spalte?
Ist in Ihrem Unternehmen auch die mobile Zeiterfassung aus der HVMA im Einsatz, so kann es beim nachträglichen Einspielen dazu kommen, dass Daten nicht mehr in Ihren **Kieselstein ERP** Datenbestand übernommen werden dürfen. Dies wird in der Auswahlliste der Zeiterfassung in der rechten Spalte mit einem Haken angezeigt. [Details siehe bitte](../HVMA/index.htm#Übertragung).

#### Zeitbestätigung?
<a name="Zeitbestätigung"></a>
Die Zeitbestätigung dient vor allem der Protokollierung der unterschriebenen [Auftragszeiten](../auftrag/index.htm#Zeitbestätigung) in Verbindung mit der [HVMA2 Offline](../HVMA/index.htm#HVMA2 Offfline) [Zeiterfassung](../HVMA/index.htm#Auftragszeiten unterschreiben).
Vor allem um den Start, aber auch die Bearbeitung ev. falsch erfasster Daten zu ermöglichen, wurde diese Funktionalität geschaffen. Es ist nicht gedacht, dass diese Funktion für die eigentliche Datenpflege verwendet wird.

Bitte beachten Sie, dass die Zeitbestätigung nur für Benutzer mit einem Recht auf die Grunddaten und natürlich mit einem Recht in das Zeiterfassungsmodul zur Verfügung steht.

#### Können auch Zeitbuchungen kopiert werden?
In manchen Situationen, z.B. bei der Nacherfassung von längeren Dienstreisen ist es immer wieder praktisch, wenn ganze Tage kopiert werden können.<br>
Dafür haben wir auch in der Zeitbuchung die Möglichkeit geschaffen, Zeitbuchungen markieren ![](Zeitbuchung_kopieren.gif) und kopieren zu können und die Tätigkeiten zu den original gebuchten Uhrzeiten an einem anderen Tag und ev. auch für eine andere Person einbuchen ![](Zeitbuchung_aus_Zwischenablage_einkopieren.gif) zu können.<br>
Bitte beachten Sie, dass das einkopieren idealerweise für Tage durchgeführt wird, an denen keine Zeitbuchungen vorhanden sind, weder Sonderzeiten noch normale Zeiten.<br>
Wir liefern, vor allem auch aus Gründen der Bedienungs-Geschwindigkeit und da sowieso eventuell vorhandene Zeitbuchungen sichtbar sind, keine wie immer gearteten Fehlermeldungen. Bitte beachten Sie auch, dass automatische Zeitbuchungen nicht mitkopiert werden, aber beim Einbuchen eines Geht, das in den kopierten Daten enthalten sein kann aber nicht muss, neu errechnet werden.

