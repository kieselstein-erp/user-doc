---
title: "Terminal Vergleich"
linkTitle: "Featurevergleich"
categories: ["Zeiterfassung"]
tags: ["Zeiterfassung","Terminal", "BDE Station", "Featurevergleich"]
weight: 200
description: >
  Featurevergleich der verschiedenen Zeiterfassungsterminals
---
Status der Möglichkeiten der verschiedenen Zeiterfassungsterminals.<br>
**Kieselstein ERP** Server Version 0.0.0.4, KES-ZE Version 0004
Lfd Nr | HTML BDE | KES-ZE | Bemerkung / Beschreibung |
---|---|---|---|
1|nur Barcode|Barcode, RFID|Anmeldung der Person<br>1. Barcode mit $P<br>2. verschiedene RFID Leser<br>3. Durch Eingabe der Ausweisnummer<br>4. Auswahlliste siehe Punkt 100, 101|
1a| | |Anzeige meiner letzten Buchungen, offene Zeitverteilung, angestempelte Maschinenparallel Bedienung, bitte immer mit der Startuhrzeit|
2|Barcode|Antippen |Kommt Buchung -> Bucht das Kommt in der Zeiterfassung entweder durch antippen eines Buttons oder durch $KOMMT als Barcode oder durch drücken von K (nach der Eingabe der Ausweisnummer)|
3|Barcode|Antippen|Geht Buchung -> Bucht das Kommt in der Zeiterfassung entweder durch antippen eines Buttons oder durch $GEHT als Barcode oder durch drücken von G|
4|Barcode|Antippen|Unter(brechung) Buchung -> Bucht die Unterbrechung = Pause in der Zeiterfassung entweder durch antippen eines Buttons oder durch $UNTER als Barcode oder durch drücken von U|
5|Barcode| |Arzt, Behörde, .... Buchung -> Bucht die Sondertätigkeit in der Zeiterfassung entweder durch auswählen aus einer Combobox oder durch $Arzt usw. als Barcode|
6| | |Status -> Button oder Barcode. Zeigt den Zeitabrechnungsstatus an. D.h. Gleitzeitsaldo bis gestern Abend und Urlaubsanspruch bis gestern Abend. Bei Urlaubsanspruch gesteuert nach Teilzeitmodell ?? D.h. einmal Stunden, einmal Tage. Sollte man generell einstellen können -> Drücken des Buttons, oder Barcode oder Drücken von S|
6a|x | |Anzeige der Monatsabrechnung bis gestern Abend, letztes Monat und ev. auch aktuelles Monat gesamt. Jedenfalls sollte der Report wissen, von wo er aufgerufen wird und den Stichtag um das im Report anpassen zu können.|
7| | |Verbindung auch von Extern, z.B. VPN bzw. https oder ... jedenfalls auch für einen DAU Anwender einfach installierbar (man sendet dem Anwender das Gerät und er muss es nur anstecken, ala Teamviewer)|
8| | |Grundsätzlich muss papierlose Fertigung möglich sein|
9| |x|Mehrsprachigkeit d.h. das Terminal wird auf eine Sprache/locale eingestellt.|
10| | |Bilder der Buttons definierbar|
11| | |Anwendung als Kommt/Geht/Pause oder als vollwertiges ZE-Terminal|
11a| | |Anzeige der Liste aller anzuzeigenden Personen (je nach Personaleigenschaft)|
12| | |Anzeige der von mir gestarteten Maschinen|
13| | |Änderung der Maschine für den (nächsten) zu buchenden Arbeitsgang, Auswahl wenn möglich, mit Maschinengruppe / Maschine aus gewünschten Arbeitsgang vorbesetzt|
14| | |Grundsätzliche Unterscheidung zwischen nur Tätigkeit oder mit Maschine|
15| | |Mehrmaschinen Bedienung, ACHTUNG: in der erweiterten Form, sodass jederzeit weitere Arbeitsgänge gestartet und gestoppt werden können|
16| | |Mehrmaschinen Bedienung im Sinne von ein Arbeitsgang wird gleichzeitig auf mehreren Maschinen gestartet|
17| | |Erfassung von Prüfwerten / Messwerten|
18| | |Anbindung von Messmitteln (USB, Bluethooth, ..) aber auch z.B. Kamera um ein Schnittbild zu Fotografieren|
19| | |Erkennung von Maschine bleibt stehen (Geisterschicht)|
20|Los, Auftrag | |Buchung von Los(nr), Auftrag(nr), Projekt(nr)|
21|x | |danach Maschine|
22|x | |danach Tätigkeit bzw. Arbeitsgang mit Unterarbeitsgang|
23|x | |oder kombinierte Barcodes, d.h. Los, Maschine, Tätigkeit bzw. AG&UAG in einem Barcode ergibt dann jeweils (20-23) einen zu buchenden Arbeitsgang|
24| | |gezieltes Maschine-Stop, wobei es hier immer Missverständnisse gibt. Üblicherweise verbinden die Menschen mit dem Stop auch, dass die Tätigkeit darauf beendet wird (siehe erweiterte Form) D.h. ev. brauchen wir ein Beende den Arbeitsgang mit Mensch und Maschine. Was dann bedeutet es läuft um die eine Maschine weniger gleichzeitig, bzw. Mensch und Maschine arbeiten beide nicht mehr.|
25| | |gezieltes Fertigbuchen, des gesamten AG's aber auch xx%|
25a| | |Wieder aufheben eines Fertiggebuchten AG's, weil verbucht usw. Ohe Änderung der %te.|
26| | |Ablieferbuchung mit Gut-Schlecht Stück, Berücksichtigung Sollsatzgröße und externer Arbeitsgang ohne SollSatzPrüfung|
27| | |Ausdrucken von Ablieferetiketten, und Hinterlegung entsprechender steuernder Texte, wo denn der Mitarbeiter den "Zettel" holen sollte|
28| | |Ändern der Losgröße|
29| | |Erfassung von Materialentnahmen, sowohl als Soll erfasst als auch zusätzliches = anderes Material. Dabei auch: dafür brauch ich das andere Material nicht mehr.|
30| | |Anzeige von Dokumenten, aus: - Dokumentenlink - Kommentar, - Dokumentenablage jeweils mit Rechten und der Möglichkeit dies am nächst passenden Drucker auszudrucken|
30a| | |Sowohl in Abhängigkeit des Loses = Artikel, als auch des Arbeitsganges (das gibts im KES noch gar nicht) und auch frei, also Filesystem bzw. Artikeleingabe / Stkl-Eingabe|
31| | |Zoom (Zweifinger) in den Dokumenten|
31a| | |Redlining unterstützen, Kommentar an Konstruktion eingebbar (kann doch eh jedes Handy ;-) ) z.B.: Artikelsperre mit Kommentar ( Kommentarfeld ploppt auf), Mail an Konstruktion?|
32| | |Displaygröße von 12" bis 27" wird unterstützt.|
33|nein| |Das Terminal ist Offline-fähig. D.h. zumindest die Ist-Zeit-Erfassung wird zwischengepuffert.|
34| | |Anzeige des Los-Fortschritts-Statuses aus einem Report|
35| | |Anzeige Artikelstammblatt des Stücklistenartikels|
35a| | |Anzeige des Statuses eines Auftrages|
35b| | |Anzeige des Statuses eines Projektes|
36| | ||
37| | |Wartungsanweisung. D.h. welche Wartungsschritte muss der Mitarbeiter zu jedem Schichtende machen, oder heute auf seiner/seinen Maschinen|
38| | |Erfassung der Durchführung der Wartungsanweisung mit Zeit und Material|
39| | ||
40| | |Urlaubsantrag, Zeitausgleichsantrag und ev. auch Krankenstandsantrag (wenn das auf einem weit entfernten Terminal gebucht wird und der Mitarbeiter in der 10km entfernten Halle krank wird, oder auf der Baustelle ist)|
41| | ||
42| | |Losauswahlliste mit den offenen Arbeitsgängen, sortiert nach Arbeitsgangbeginn inkl. ms=Reihung innerhalb des Tages|
42a| | |Darstellung der Los-Arbeitsgang-Auswahlliste dass man auch die offene Losmenge und bei Stückrück auch die offene Menge jedes einzelnen AGs sieht.|
43| | |Filterung der Losauswahlliste anhand Tätigkeit, Maschinengruppe, Maschine.|
44| | |Filter der Losauswahlliste jederzeit umschaltbar. Die Aktualisierung muss sehr rasch erfolgen. Ev. auch als neue default Filterung definierbar|
45| | |Default Filter hinterlegbar.|
46| | |Suchen eines Loses mittels Barcodescanner oder Tastatureingabe. D.h. der Scann geht auf die Losnummer, bei der Tastatureingabe auch Artikelnummer der Stkl vorstellbar. Zusätzlich sollte der gewählte Filter greifen.|
47| | |Terminalparameter global sicherbar und ladbar, aber mit .... das nicht usw.. Z.B. Filterung der Losauswahlliste nicht umschalten, alles andere schon.|
48| | |Pausenhupe von Terminal ansteuerbar|
49| | |Waagenanbindung für Pulverbeschichtung usw.|
50| | |Stückrückmeldung mit gut und schlecht Stück|
51| | |Fehlerbegründungserfassung als Zwang bei den Schlechtstück|
52| | |Foto bei Schlechtstück hinterlegbar|
53| | |Abfrage von Stückrückmeldung ist ein Parameter|
54| | |Die Stückrückmeldung kann jederzeit abgebrochen werden (muss man nicht extra aktivieren)|
55| | ||
56| | ||
57| | ||
58| | ||
59| | ||
60|x | |$PLUS Buchung: (siehe oben erweiterte Mehrmaschinenbedienung) Es werden mehrere unterschiedliche oder gleiche!! Arbeitsgänge auf unterschiedlichen Maschinen gemeinsam gestartet.|
61| | |$PLUS Buchung: Es können jederzeit einzelne Arbeitsgänge mit unterschiedlichen Maschinen hinzugefügt werden oder auch gestoppt werden.|
62| | |$PLUS Buchung: Beim Stop eines einzelnen AGs müssen die Gut/Schlecht-Stück eingegeben werden.|
63| | ||
64| | ||
65| | ||
66| | ||
67| | ||
68| | ||
69| | ||
70|nein| |Terminal läuft Bildschirm füllend, sodass man unter keinen Umständen (nur über Password) ins Betriebssystem kommt, oder man kann die Anzeigegröße definieren, sodass auch andere Programme auf der Hardware laufen und bedient werden können.|
71| | |Einbindung Anwesenheitsliste als Bildschirmschoner, aber auch Anzeige von Firmeninformationen, also anderer Weblink oder .... man baut in die Anwesenheitsliste die Zusatzinfos ein. Es ist aber in der Regel kein Platz dafür (bei den kleinen Terminals)|
72| | |Anzeige der tatsächlich laufenden bzw. NICHT laufenden Maschinen|
73| | |Rückmeldung wenn der Mitarbeiter GEHT bucht und seine Soll-Stück-Zeit noch nicht erreicht hat. Ist irgendwie böse, muss aber trotzdem motivierend rüber kommen. Muss auf jeden Fall, auch vom Inhalt her einstellbar sein.|
74| | |Anzeige des Rechner-Namens, der IP und ... im .... damit man das Terminal vernünftig einrichten kann. Ev. kann man vom Terminal aus auch die Teamviewer ID auslesen und diese auch anzeigen.|
75| | |Anzeige der anwesenden Personen als eigene Liste nach Rechten, oder mit direkter Aktivierung des Bildschirmschoners|
76| | |Zeitsynchronisierung vollautomatisch und gegen die Kieselstein-ERP Server|
77| | ||
78| | ||
79| | ||
80|nein| |Mehrere Mandanten können bei eindeutiger Ausweisnummer auf einem Terminal buchen|
81| | ||
82| | ||
83| | ||
84| | ||
97| | |Timeouts der verschiedenen Anzeigefunktionen sind einstellbar und übertragbar|
98| | |Alle Barcodes sind in der Losauswahlliste auch über Buttons bedienbar|
99| | |Auch die $PLUS Buchung kann über die Losauswahlliste erfolgen. Bzw. wird die einfache Mehr-Arbeitsgangerfassung mit Maschinen umgesetzt.|
100| | |MitarbeiterListe auf Startscreen anzeigbar --> man tippt auf seinen Namen und bekommt dann das Auswahlmenü mit KOMMT/GEHT/UNTER; Aktueller Standard bei WGT|
101| | |Sortierung der aufgelisteten Mitarbeiter nach Personalnummer --> damit nicht bei Mitarbeiteraustritt oder Zugang die Reihenfolge der Namen sich ändert und man seinen wieder suchen muss|
102| |Windows, Android|Plattformunabhängige Software|
103| | |Remotezugriff auf das Terminal per App/Web Service für Auswärtsmontagen|
104| | |Bei AG-Start Kommentar aus Artikel am Terminal anzeigen|
105| | |Terminal-Sprache als Eigenschaft der Person|
106| | |Ändern der Losgröße, Automatisches Anpassen bei Überbuchung|
107| | |Anwesenheitsliste nach Identifizierung anzeigen. Zuerst mit dem Schlüssel identifizieren und dann aus einer Button-Liste auswählen was ich sehen möchte. Da sind wieder konfigurierbare Links hinterlegt|
108| | |Alternativ dazu ist der Bildschirmschoner eine Firmen-Web-Werbungsseite bzw. wir begrüßen heute ...|
