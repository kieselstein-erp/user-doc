---
title: "Maschinenzeiterfassung"
linkTitle: "Mehrmaschinenbedienung"
categories: ["Maschinenzeiterfassung"]
tags: ["Mehrmaschinen"]
weight: 610
description: >
  Mehrmaschinenbedienung, papierlose Fertigung
---
Nachfolgend der Entwurf der Beschreibung für die Mehrmaschinenbedienung / Papierlose Fertigung

Es ist dies derzeit **nur** eine Gedankensammlung und eine Sammlung der Definitionen für die Entwicklung dieser Funktionalität(en).



Wenn man eine richtige Mehrmaschinenbedienung haben will, setzt es voraus, dass
du auf jeder Maschine ein Terminal hast. Ob ein Android oder ein großes mit dem
du auch Zeichnungen anschauen kannst, ist in diesem Sinne egal.<br>
Es muss auf jeder Maschine ein Terminal vorhanden sein, auch auf Handarbeitsplätzen.


---------
## Mehrmaschinenbedienung

Das Thema hier ist immer wieder, dass gerade von kleinen Unternehmen die Forderung kommt, dass auf jegliche Form der Fertigungsauftragsabwicklung in flexibelster Weise reagiert wird.

Meist ist am Anfang nicht einmal klar, ob nun die (Nach-)Kalkulation Mitarbeiter zentriert ist oder doch Maschinen orientiert.

Hier hat sich bewährt, dies an der Erstellung Ihrer Angebote/Offerte auszurichten.

Meist werden in der Metallverarbeitungs-Branche Maschinenstunden verkauft.

Also sind für Sie die Maschinenstunden wichtig.

Das bedeutet aber auch, dass Sie datentechnisch für alles eine Maschine angeben müssen. Also auch für so Dinge wie eine gewöhnliche Ständerbohrmaschine (die schon lange abgeschrieben ist) oder die Reinigung von Teilen mit oder ohne Reinigungsanlage usw..

Die Herausforderung ist immer, dass die Mitarbeiter eben alle Maschinen anstempeln, damit Sie im Endeffekt eine sehr gute Nachkalkulation bekommen.

**Wichtig:** Wenn Sie die Zeiten nur Maschinen orientiert erfassen, wie stellen Sie sicher, dass die Nebenzeiten die Ihre Mitarbeiter erbringen auch in Ihrer Nach-Kalkulation erfasst sind und dass diese Zeiten auch stimmen.

Ein Beispiel aus der Praxis:<br>
In einem Betrieb wurden regelmäßig zum Abladen der angelieferten Ware alle Mitarbeiter zusammengerufen um beim Abladen zu helfen.<br>
Nach der Einführung der Zeiterfassung, mit der klar aufgezeichnet wurde, wer denn wann beim Abladen mitgeholfen hat, hat sich sehr schnell gezeigt, dass die Anschaffung eines Gabelstaplers die deutlich billigere Variante ist.

Ein anderes Beispiel:<br>
Es wurden nur die Maschinenstunden und da nur die sogenannten Umspannzeiten betrachtet. Daraus ergab sich, bei entsprechender Mehrmaschinenbedienung, dass angeblich für eine 8Stunden Schicht nur 2Std. Arbeit vorhanden waren. Die Mitarbeiter hatten aber immer gut zu tun, waren gut ausgelastet. 

Was war geschehen? 

Die ganzen Nebenarbeiten, die der Mitarbeiter so erbracht hat, z.B. die Vorreinigung um überhaupt messen zu können, wurden einfach nicht geplant und nicht erfasst.

Im Endeffekt, wurde das Produkt zu billig verkauft.

Nutzen Sie Ihr ERP um diese Dinge aufzuzeigen und zu verbessern und damit nur wertige Produkte an Ihre Kunden zu verkaufen.

### HAG / UAG, Hauptarbeitsgang / Unterarbeitsgang
In einigen Metallverarbeitenden Unternehmen geht es um eine sehr exakte Planung aller Arbeitsgänge.<br>
Oft kommt mit dazu, dass auch die Abarbeitung der Arbeitsgänge den Kunden gegenüber nachgewiesen werden muss.<br>
Dafür wird dann bei den ERP Systemen, die das können, die Gliederung eines Arbeitsganges in Haupt- und Unterarbeitsgange verwendet.<br>
Zusätzlich wird dies dann noch mit der Arbeitsgangart kombiniert.<br>
So besteht ein Arbeitsgang z.B. aus folgenden Detail- bzw. Unterarbeitsgängen:
| HAG | UAG | Bezeichnung | Maschine | Mensch | AG-Art | Tr | Ts |
| --- |  --- |  --- |  --- |  --- |  --- |  --- |  --- |
| 10 | 1 | Rüsten 1.Aufspannung | DMG250 | Ja | Rüsten | 5,0 Std | 0 |
| 10 | 2 | Umspannen 1\. Aufspannung | DMG250 | Ja | Umspannen | 0 | 3 Min |
| 10 | 3 | Laufzeit | DMG250 | Nein | Laufzeit | 0 | 3 Std |
| 10 | 4 | Prüfen |   | Ja | Umspannen | 0 | 5Minuten / 5Stk |

Wichtig ist hier, dass jeweils Rüsten und Umspannen entsprechend gestempelt wird. Hier kommt dann vor allem ein guter Aufbau des Barcodes zum Tragen. D.h. es muss sichergestellt sein, dass jede Buchung mit einem einzigen Scann gemacht werden kann. Siehe dazu:
- kombinierter Barcode auf dem Fertigungsbegleitschein
- Papierlose Fertigung
Eine weitere Alternative ist hier, alle "heute" durchzuführenden Arbeitsgänge gemeinsam anzustempeln und dann am Abende / Schicht-Ende alle auf einmal zu beenden.<br>
Hier wird dann eine Verteilung nach Sollzeiten auf die gemeinsame Zeit durchgeführt.<br>
Damit eine halbwegs vernünftige Verteilung errechnet werden kann, ist es erforderlich, dass auch entsprechende Stückmeldungen mit angegeben werden. Das führt natürlich auch dazu, wenn keine Stück-Rück-Meldungen eingegeben werden, ist eine Verteilungsberechnung nicht möglich.<br>
Einige Unternehmen gehen nun dazu über, dass die theoretische Vorgabezeit anhand der Stückrückmeldungen den echten Anwesenheitszeiten der Mitarbeiter gegenüber gestellt werden. D.h. die Rüstzeit plus die Umspannzeit x Stückrückmeldung (Gut + Schlecht) wird der Schichtzeit (8Std) gegenübergestellt. Hier sollte der Mitarbeiter / die Mitarbeiterin eine mindestens 80%ige Zeit erreichen. Sind die errechneten Zeiten weit außerhalb dieses Bereiches, ist davon auszugehen, dass die Sollzeiten nicht stimmen.<br>
Für eine gute Nachkalkulation, aber auch eine entsprechende Kapazitätsplanung, gehen Sie bitte diesen Dingen nach und stellen Sie dies richtig.<br>
Das beginnt bei der Unterstützung der MitarbeiterInnen, damit diese eine Chance haben dies in der von Ihnen geplanten Zeit tatsächlich zu erledigen und geht bis zur Korrektur der Sollzeiten und damit in weiterer Folge zur Korrektur Ihres Verkaufspreises.<br>
Insofern ist für einen Fertigungsbetrieb die Einhaltung, besser Unterschreitung, der Sollzeiten und natürlich auch der geplanten Materialkosten ein wesentlicher Faktor um das Unternehmen langfristig und nachhaltig erfolgreich führen zu können.<br>

### Zeitverteilung Typ2
Siehe [Beschreibung Zeitverteilungstypen]( {{<relref "/fertigung/zeitwirtschaft_in_der_fertigung/ag_gemeinsam_buchen/#zeitverteilungstyp" >}} )