---
title: "Zeitwirtschaft in der Fertigung"
linkTitle: "Zeitwirtschaft"
categories: ["Zeitwirtschaft"]
tags: ["Zeitwirtschaft in der Fertigung"]
weight: 600
description: >
  Zeitwirtschaft in der Fertigung
---
Nachdem sich viele unserer Anwender ein Kochrezept wünschen wie man vom Kundenauftrag zur Auslieferung kommt und wie alle die Prozesse dazwischen, in einer einfachen und in einer komplexen Form aussehen, haben wir versucht einen möglichen Ablauf zu beschreiben. Siehe dazu bitte auch Zusammenhang [Stückliste <-> Fertigung]( {{<relref "/fertigung/losverwaltung/stueckliste_fertigung">}} ).

Grundsätzlich gehen wir davon aus, dass Idie Stücklisten inhaltlich richtig sind, dass die Tätigkeiten in den Arbeitsplänen erfasst sind und das Material im Reiter Positionen.

Der grundsätzliche Ablauf ist:
1. Anlegen des Kundenauftrags, also der Auftragsbestätigung mit dem geplanten Liefertermin und den geplanten Positionen mit den eventuell abweichenden Positionslieferterminen. Die Termine sind eintreffend gedacht.
2. Daraus ergibt sich, über die interne Bestellung, und der jeweiligen zukünftigen Lagerstände der neue Bedarf an zu fertigenden Stücklisten. In der internen Bestellung wird auch die Stücklistenhierarchie aufgelöst. Bitte beachten Sie, dass maximal 10Hierarchieebenen an Hilfsstücklisten aufgelöst werden.
3. Mit der der internen Bestellung erhalten Sie die Liste der anzulegenden Lose. Diese können verdichtet usw. werden und werden dann in echte Lose = Fertigungsauftäge umgewandelt. Bitte beachten Sie, dass erst durch die Anlage der Lose entsprechende Material und Zeitbedarfe entstehen.
4. Die Los-Endetermine errechnen sich entweder aus dem geplanten Liefertermin abzüglich der Kundenlieferdauer (Kunde, Konditionen), wenn dies das oberste Los vor der Aus-Lieferung ist oder wenn es ein Unterlos ist, so ergibt sich der Endetermin aus dem Beginntermin des übergeordneten Loses. D.h. die Reihenfolge der Lose ist, wenn diese nach Beginntermin und meist auch wenn sie nach Endetermin sortiert werden, so, dass klar ist, welche Lose zuerst gefertigt werden müssen.

Wie ist nun bei der Buchung der Mitarbeiterzeiten und der Maschinenzeiten vorzugehen?

Achten bitte grundsätzlich darauf, dass die Zeitbuchung in **Kieselstein ERP** immer so ist, dass jeder Zeitstempel einen Beginn darstellt.
Tipp: Auch wenn die Sollzeitberechnung aus technischen Gründen durchaus komplex sein mag, ziele darauf, so wenige Buchungen wie möglich zu produzieren. D.h. reduzieren die zu buchenden Tätigkeiten / Arbeitsgänge. Z.B. durch die Verwendung nur der Hauptarbeitsgänge. Bei vielen Anwendern reicht auch nur zu wissen wer gearbeitet hat. Was er gemacht hat, ergibt sich aus seinen Fähigkeiten, den ihm zugewiesenen Arbeiten.

**Der Ablauf innerhalb eines Loses ist nun wie folgt:**

1. Lose / Installationen ohne Maschinenzeiterfassung und ohne Stückrückmeldung
    1. Ausgeben des Loses und damit stellen des Loses in den Status in Produktion
    2. Zugleich ausdrucken des Fertigungsbegleitscheines und der Los-Ausgabeliste. Gegebenenfalls wird das Material am Fertigungsbegleitschein mitgedruckt.
    3. Die Materialwirtschaft(s MitarbeiterInnen) geben / bringen das Material in die Produktion und der Fertigungsmitarbeiter beginnt mit seiner Tätigkeit
    4. Der Beginn der Tätigkeit wird durch die Buchung mit dem Fertigungsschein signalisiert. [Buchungsablauf 1.](#1 Beginn Tätigkeit)
    5. Beginnt ein Mitarbeiter mit einer Tätigkeit auf dem gleichen Los, so wird die Zeit parallel dazu erfasst
    6. Beginnt ein Mitarbeiter, der sich bereits auf dem / einem anderen Los angestempelt hat, mit einer anderen Tätigkeit so wird, da jeder Mensch nur ein Ding zu einer Zeit machen kann, die zu vor begonnen Tätigkeit beendet und mit der neuen Tätigkeit begonnen.
    7. Bitte beachten: Es liegt in der Verantwortung der Mitarbeiter, dass die Arbeitsgänge in der technisch richtigen Reihenfolge abgearbeitet werden. Üblicherweise erfolgt dies vom ersten zum letzten Arbeitsgang hin. Es ist, insbesondere wegen der vielen möglichen parallel-Bedienungen keine Verriegelung in der Reihenfolge der Arbeitsgänge enthalten. Es wird jedoch durch die Anordnung am Terminal bzw. am Fertigungsbegleitschein die Reihenfolge entsprechend vorgegeben.
    8. Unterbrechen eines Arbeitsganges. Muss die Arbeit an einem Arbeitsgang vor seiner tatsächlichen Beendigung unterbrochen werden, so geschieht dies in der Regel deswegen, weil eine andere Arbeit dringender erledigt werden muss. D.h. hier ist wie in 1.6. beschrieben vorzugehen. Eine Ende-Buchung ist nur dann erforderlich, wenn die Arbeit beendet wird und der Mitarbeiter zwar anwesend ist, aber keine zuordenbare Tätigkeit durchführt. Wir raten dringend auch für Werkstatt zusammenräumen, Maschine reparieren usw. ein entsprechendes Los zur Verfügung zu stellen, damit alle Zeiten entsprechend gebucht werden können.
    9. Beenden eines Arbeitsganges<br>
    Ist ein Arbeitsgang komplett beendet, so kann dies mit der Fertigbuchung zurückgemeldet werden. Dies bewirkt, dass, egal welche Ist-Zeit auf dem Arbeitsgang ist, dieser Arbeitsgang abgeschlossen ist. Womit andere Personen am Los den Fortschritt erkennen können (Bis auf das Verpacken ist schon alles gemacht). Siehe [Buchungsablauf 4a oder 4b](#4 Fertigbuchen)
    10. Ablieferung. Ist ein Los teilweise fertig, so sollte / muss / kann dies auch am Terminal gebucht werden. D.h. wenn der Fertigungsmitarbeiter ein Los eigenverantwortlich beurteilen kann, dass von diesem Fertigungslos eine entsprechende Stückzahl fertig ist, so sollte er die Ablieferung am Terminal buchen. Ablieferung bedeutet: Für dieses Los = diese Stückliste sind alle Arbeitsschritte durchgeführt und die abgelieferten Teile / Mengen können vom übergeordneten Prozess weiterverwendet werden. [Siehe Buchungsablauf 5](#5 Ablieferung)

2. Lose / Installationen mit Maschinenzeiterfassung und ohne Stückrückmeldung
    1. Es ist der Ablauf grundsätzlich wie in a.) beschrieben. Es kann, wie in [Buchungsablauf 2](#2 Beginn mit geänderter Maschine) beschrieben, jedoch auch eine andere Maschine angestempelt werden.<br>
    Der wesentliche Unterschied ist, dass Maschinen in sich ebenfalls nur einen Job erledigen können, aber natürlich kann ein Mitarbeiter gleichzeitig mehrere Maschinen betreuen und der eine Mitarbeiter die Maschine starten under andere diese stoppen.
    2. Läuft die Maschine weiter oder ist sie gestoppt?<br>
    Je nach Einstellung der Auto-Stop Funktion im Arbeitsgang, welche aus der Definition in der Stückliste übernommen wird, welche wiederum von der Definition in der Maschine vorbesetzt wird, werden alle vom jeweiligen Mitarbeiter gestarteten Maschinen bei seiner Geht-Buchung mit-gestoppt oder, wenn kein Auto-Stop, so laufen die Maschinen auch nach dem Geht weiter, da z.B. die nachfolgende Schicht mit der Maschine weiter arbeitet oder eine sogenannte Geisterschicht gemacht wird.
    3. Mit dem [Buchungsablauf 3](#3 Stop Maschine) wird eine Maschine gestoppt.

3. Lose mit Maschinenzeiterfassung und Stückrückmeldung<br>
Der Ablauf ist grundsätzlich gleich wie in 1.) bzw. 2.) beschrieben, jedoch wir beim Beenden einer Tätigkeit, welche in aller Regel durch den Beginn einer neuen Tätigkeit ausgelöst wird, die sogenannte Stückrückmeldung abgefragt. D.h. der Mitarbeiter wird dazu aufgefordert, für alle von ihm/ihr angestempelten Arbeitsgänge die gefertigten Stückzahlen unterteilt in Gut-/Schlecht-/in Arbeit-Stücke zurückzumelden. Diese Abfrage läuft so lange, bis der jeweilige Arbeitsgang fertiggemeldet wurde. Dafür steht auch die Fertigmeldung direkt während der Stückrückmeldung zur Verfügung.<br>
Das bedeutet, für die Stückrückmeldung wird automatisch vor a8.) die Abfrage der Gut-/Schlechtstück eingeschoben.<br>
Hier werden alle vom Mitarbeiter angestempelten Tätigkeiten aufgelistet und es MUSS die jeweilige Stückzahl der gefertigten Teile egal ob diese nun brauchbar (Gut) oder defekt (Schlecht) sind.<br>

## die Liste der zurückzumeldenden Arbeitsgänge wird immer länger?<br>
wie oben beschrieben, gehen wir davon aus, dass ein Mitarbeiter in der Regel einen Arbeitsgang macht und diesen auch fertig macht. Ist er mit dem Arbeitsgang fertig, d.h. es sind alle geplanten / möglichen Teile diesem Arbeitsgang unterworfen worden, so sollte er das Fertig anhaken. Ist er nicht fertig geworden, so ist seine Tätigkeit vermutlich ein Geht, wodurch er ebenfalls zur Eingabe der Gut-/Schlechtstück aufgefordert wird und dann mit dem Geht seine heutige Arbeit beendet ist.<br>
Da aber, gerade durch die [Mehrmaschinenbedienung]( {{<relref "/fertigung/zeiterfassung/maschinenzeiterfassung">}} ), es durchaus erwünscht ist, dass ein guter Mitarbeiter 3-4 Maschinen gleichzeitig bedient, werden beim Wechsel eines Arbeitsganges die Stückzahlen der aktuell laufenden / offenen Arbeitsgänge abgefragt.

4. mehrere Arbeitsgänge / Lose gemeinsam buchen<br>
Mit dieser Buchung, auch [$PLUS Buchung]( {{<relref "/fertigung/zeitwirtschaft_in_der_fertigung/ag_gemeinsam_buchen" >}} ) genannt, kann die gleichzeitige Arbeit an mehreren Arbeitsgängen in der Regel unterschiedlicher Lose gestartet werden. Es hat sich bewährt, dass die gleichzeitige Bearbeitung durch eine Ende Buchung tatsächlich beendet wird. Bitte beachte, dass durch die Endebuchung die Maschinen NICHT gestoppt werden. [Siehe Buchungsablauf 6](#6 Ende).


## Lose und ihre Stati und deren Bedeutung

| Status | Bedeutung |
| --- |  --- |
| angelegt | Stückliste wurde übernommen und mit der Losgröße multipliziertD.h. das Material, welches kein Rüstmaterial ist wurde multipliziert mit der Losgröße als Sollmenge in das Los aufgenommen. Rüstmaterial wird 1:1 in das Los übernommen.Das Material befindet sich noch im Lager, ist aber zum Los-Beginntermin reserviert, sodass der Einkauf seine Bedarfe kennt.Die Arbeitsgänge sind mit Stück- und Rüstzeiten ebenfalls aus der Stückliste übernommen. Es kann noch keine Zeit auf das Los gebucht werden.Gegebenenfalls wurden auch die Prüfpläne übernommen. |
| ausgegeben | Das Material wurde, je nach Eigenschaft der Stückliste entweder ins Los gebucht oder ist im Lager geblieben und es wurden dafür Fehlmengen erzeugt (siehe Fehlmengenautomatik) Es dürfen noch keine Zeiten auf das Los gebucht werden. |
| in Produktion | Das Los ist im tatsächlichen Produktionsprozess. Es dürfen Zeiten darauf gebucht werden. |
| Gestoppt | Die Produktion dieses Loses ist aktuell gestoppt. Material und Arbeitszeiten bleiben wie gebucht erhalten. Meistens wird dieser Status eines Loses in den Auslastungsvorschau(en) angeführt, aber nicht mitgerechnet |
| teilerledigt | es sind bereits Losmengen vollständig fertig produziert und an das Lager zurückgemeldet. Das bedeutet, dass diese Mengen wieder z.B. für Lieferscheine oder übergeordnete Lose aus dem Lager entnommen werden können.Es bedeutet dies immer auch, dass jemand / eine Person entschieden hat, dass die angegeben Anzahl an Teilen / Baugruppen gut sind und funktionieren / den Fertigungsvorgaben entsprechend gefertigt wurden. |
| Erledigt | Es ist die gesamte produzierte Stückzahl, welche nicht unbedingt der Losgröße entsprechen muss, gefertigt.Ev. Überlieferungen hängen auch von den Einstellungen in den Stücklisteneigenschaften ab. |
| Storniert | Das Los wird nicht produziert. Keine Reservierung, kein Kapazitätsbedarf |

## Buchungsabläufe anhand des Zeiterfassungsterminals
### Buchungsablauf 1, Beginnbuchung der Tätigkeit
<a name="1 Beginn Tätigkeit"></a>
Ich, Tätigkeit (Ich = Barcode der Ausweisnummer, Tätigkeit in der Regel der kombinierte Barcode aus dem Fertigungsschein)<br>
Damit wird zum Zeitpunkt der Buchung die Arbeit auf dem Arbeitsgang des Loses gestartet.

### Buchungsablauf 2, Beginnbuchung der Tätigkeit mit geänderter Maschine
<a name="2 Beginn mit geänderter Maschine"></a>
Ich, Maschine, Tätigkeit.<br>
D.h. üblicherweise ist im kombinierten Barcode auch die Maschine (technisch ?? und damit die Maschine aus dem Los-Arbeitsgang) enthalten, auf der der Arbeitsgang ausgeführt werden sollte. Wird eine andere Maschine verwendet, so muss diese abweichende Maschine unmittelbar vor der Buchung der Tätigkeit (mit dem kombinierten Barcode) erfolgen

### Buchungsablauf 3, Stop der Maschinenlaufzeit
<a name="3 Stop Maschine"></a>
Ich, Stop (beim jeweiligen Arbeitsgang. Der Stop Code wird nur angedruckt, wenn für den Arbeitsgang auch eine Maschine definiert ist)

### Buchungsablauf 4a, Fertigbuchung der Tätigkeit
<a name="4 Fertigbuchen"></a>
Ich, LosNr, Fertig, Tätigkeit (es muss der Barcode Fertig definiert sein)

### Buchungsablauf 4b, Fertigbuchung der Arbeitsganges
Ich, LosNr, Arbeitsgang fertig (es muss der Barcode Fertigmelden des Arbeitsgangs definiert sein)

### Buchungsablauf 5, Ablieferung
<a name="5 Ablieferung"></a>
Ich, Los Nr, abliefern, Eingabe der vollständig gefertigten Stückzahl bzw. wenn das Terminal auf Stückzähler Ablieferung gestellt ist, Eingabe des aktuellen Zählerstandes, also der kumulierten gefertigten Stückzahl

### Buchungsablauf 6, Ende Buchung, Beenden einer / mehrere Tätigkeiten
<a name="6 Ende"></a>
Ich, Ende

### Buchungsablauf 7, Ändern der Losgröße
<a name="7 Ändern der Losgröße"></a>
Ich, Los Nr, Losgröße ändern, neue Losgröße eingeben

## Fehlermeldungen

### bei der Ablieferung

Die Sollsatzgröße für Los ... wird unterschritten.<br>
Das bedeutet, dass die für die Produktion erforderliche Menge im Los nicht verfügbar ist.
Ist die Stücklisteneigenschaft "Materialbuchung bei Ablieferung" aktiviert (Kopfdaten der Stückliste) so bedeutet dies auch, dass die benötigte Stückzahl nicht auf Lager ist.<br>
Handelt es sich bei den fehlenden Teilen / Material um Teile welche Seriennummern bzw. Chargennummern geführt sind, so kann keine automatische Nachbuchung gemacht werden. D.h. diese müssen im **Kieselstein ERP** Client oder mit dem mobilen Barcodescannern in das Los gebucht werden.<br>
Siehe dazu bitte auch den Parameter BEI_LOS_ERLEDIGEN_MATERIAL_NACHBUCHEN.

Siehe dazu bitte auch die Definition für externe Arbeitsgänge, im Reiter Bestelldaten des externen Arbeitsgangartikels.<br>
![](Externer_Arbeitsgang.png)  <br>
Wird hier auf ohne Sollsatzpr(üfung) gestellt, so kann am Terminal trotzdem abgeliefert werden, auch wenn dieser Fremdfertigungsartikel noch nicht ins Los gebucht ist.