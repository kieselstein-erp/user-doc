---
title: "Arbeitsgänge gemeinsam buchen"
linkTitle: "AG gemeinsam buchen"
categories: ["Zeitwirtschaft"]
tags: ["Zeitwirtschaft in der Fertigung"]
weight: 100
description: >
  Arbeitsgänge gemeinsam buchen
---
Arbeitsgänge gemeinsam buchen
-----------------------------

Früher auch $PLUS Buchung genannt.

Auf den **Kieselstein ERP** Terminal kann mittels Barcode Bedienung erreicht werden, dass quasi mehrere Arbeitsgänge gemeinsam gestartet werden.

Bitte beachte, dass die Behandlung der Auflösung dieser gemeinsamen Buchung nur über den Zeitverteilungstyp definiert wird. Siehe dazu den Parameter [ZEITVERTEILUNG_TYP](#zeitverteilungstyp)

Um die Erfassung der gemeinsam Bearbeitung mehrerer Arbeitsgänge zu starten, wähle am Fertigungsbegleitschein den $PLUS Code, *Arbeitsgänge gemeinsam buchen*.<br>
![](AG_gemeinsam_buchen_starten.png)<br>

Nun erscheint die Erfassungsmaske<br>
![](AG_gemeinsam_starten.png)<br>
in der du durch scann des kombinierten Barcodes ($W) die gemeinsam zu startenden Arbeitsgänge angibst.<br>
Mit dem entsprechenden X kannst du einen erfassten Arbeitsgang wieder aus der Liste herauslöschen. Am Ende der Erfassung Tippst du auf Ok, oder du scannst erneut den *Arbeitsgänge gemeinsam buchen* Barcode ($PLUS).

Bei der Anmeldung am Terminal, erscheinen nun gegebenenfalls auch die offenen Zeitverteilungen für den/die MitarbeiterIn.<br>
![](AG_offene_Zeitverteilung.png)<br>

Zusätzlich werden in der Anwesenheitsliste auch die offenen Zeitverteilungen entsprechend angezeigt.

Sollten weitere Arbeitsgänge hinzukommen, so kann durch erneuten Scann von *Arbeitsgänge gemeinsam buchen* die Liste der gemeinsam gestarteten Arbeitsgänge angezeigt werden und weitere Arbeitsgänge hinzugefügt werden. Ein herauslöschen bereits gestarteter Arbeitsgänge ist derzeit nicht vorgesehen.

Wenn du in *Arbeitsgänge gemeinsam starten* auf eine andere Maschine buchen willst, so auch hier bitte zuerst die abweichende Maschine und dann den kombinierten Arbeitsgang buchen.

## Zeitverteilungstyp
### Wie werden die Zeiten von Arbeitsgänge gemeinsam starten verteilt?
Die Zeiten werden anhand des Zeitverteilungstyp / Zeitverteilungtyp verteilt.

D.h. für die Behandlung der zu verteilenden Zeiten stehen aktuell folgende verschiedene Vorgehensweisen zur Verfügung. Diese werden durch den Parameter ZEITVERTEILUNG_TYP gesteuert.

#### Wert 0
Hier erfolgt die Zeitverteilung aus dem Gedanken heraus, dass immer ähnliche Tätigkeiten gemeinsam durchgeführt werden. Damit wird, egal wie die Sollzeiten definiert sind, die Verteilung anhand der Losgrößen vorgenommen wird.<br>
Hintergrund, wie ist mit Rüstzeiten umzugehen, werden diese als eigene Arbeitsgänge und immer eigenständig gebucht, oder sind diese in den Sollzeiten des einzelnen Arbeitsganges enthalten.<br>
Darum werden eben die Zeiten auf die einzelnen Lose anhand der Losgrößen verteilt.<br>
Der Gedankengang dazu ist: Es werden nur sehr ähnliche Arbeitsgänge auf einmal gemacht. Das heißt es ist davon auszugehen, dass die Fertigungsschritte sehr ähnlich sind. Daher ist anzunehmen, dass das Rüsten nur einmal anfällt und dass die Stückzeiten annährend gleich sind. Das bedeutet, dass die Gesamtzeit des einzelnen Arbeitsganges anhand der Losgrößen verteilt werden kann. Je Los bedeutet dies:

Dauer (der einzelnen Tätigkeit des MAs) * Losgröße (jedes betroffenen Loses) / Summe aller Losgrößen aller an der Buchung beteiligten Lose.

Bitte beachten Sie, dass mit dieser Einstellung immer die gleiche Tätigkeit für den durchgeführten Arbeitsgang gebucht wird. Also den als erstes angestempelten / ausgewählten Arbeitsgang.<br>
Bitte beachten Sie auch den wesentlichen Unterschied der Verbuchung der Maschinenzeiten.<br>
Wurden mit der $PLUS Buchung Maschinen gestartet die als Manuell zu bedienende Maschinen definiert sind, so werden die Zeiten 1:1 mit den MitarbeiterInnen Zeiten gebucht. Sind das aber Maschinen die unter die Mehrmaschinenbedienung fallen, also alle Maschinen die **nicht** als Manuell gekennzeichnet sind, so wird, die gesamte Zeit zwischen Beginn und Ende der $PLUS Buchung auf alle beteiligten Maschinen gebucht.

#### Wert 1
Hier erfolgt die Zeitverteilung anhand der zurückgemeldeten Gut-/Schlecht-Stück.<br>
D.h. es werden für jede Tätigkeit die zurückgemeldete Stückzahl mit der Sollzeit multipliziert und dies als Basiswert für die Ermittlung des Verteilungsfaktors verwendet.<br>
Ist für einen Arbeitsgang keine Sollzeit gegeben, so wird der Wert des Parameters ZEITVERTEILUNG_SOLLZEIT_WENN_0 verwendet, welcher default auf 60Minuten gestellt ist.<br>
Wird keine Stückzahl für die Gut/Schlecht-Menge angegeben, so wird die Buchung ignoriert. Damit haben Sie die Möglichkeit fälschlich gestartete Tätigkeiten aus der Verteilung auszunehmen.<br>
Im Gegensatz zur Einstellung Wert 0 wird hier diejenige Tätigkeit verwendet, welche für den Arbeitsgang definiert ist. D.h. es kann ein Mitarbeiter durchaus verschiedenste Tätigkeiten durchführen.<br>
Beachten Sie bitte auch, dass für Mehr-Maschinen die gesamte Zeit welche zwischen Beginn- und Endzeitpunkt der $PLUS-Buchung liegt, die Maschine läuft angenommen und gebucht wird. Also alle Maschinen gleichzeitig laufen.

#### Wert 2
Auch hier erfolgt die Zeitverteilung anhand der zurückgemeldeten Gut-Schlecht-Stück.<br>
Zusätzlich werden jedoch mögliche Rüst-Arbeitsgänge berücksichtigt.<br>
D.h. das Ziel ist, dass man damit die Möglichkeit hat, dass gleichzeitig gerüstet und auf anderen umgespannt / gefertigt wird.<br>
Das bedeutet, anhand der gebuchten Arbeitsgänge wird festgestellt ob überhaupt ein Rüstarbeitsgang gebucht wurde. Ist dem nicht so, wird wie unter Wert 1 beschrieben vorgegangen.<br>
Gibt es einen Rüstarbeitsgang und auch Umspann-Arbeitsgänge, so werden die Umspann-Arbeitsgänge mit ihren Sollzeiten eingebucht und die nun verbleibende Zeit wird als Rüstzeit gebucht.<br>
Ist die Summe der Umspann-Soll-Zeit größer als die verfügbare Zeit, so wird von der verfügbaren Zeit je Rüsten eine Minute abgezogen und die nun verbleibende Zeit wird auf die Umspann-Arbeitsgänge wie unter Typ 1 beschrieben verteilt.

In **Kieselstein ERP** stimmt die Zeitverteilung für den Typ 2 auch für die Mehrmaschinenbedienung.<br>
Es werden hier für die Personalzeit nur mehr die Arbeitsgänge herangezogen, die das NUR MASCHINENZEIT nicht gesetzt haben.<br>
Die $PLUS_Dauer( = Zeit zwischen $PLUS Beginn und $PLUS Ende) wird auf jede der beteiligten Mehrmaschinen gebucht.<br>
Innerhalb dieser Maschinen werden die Zeiten wiederum nach Maschinen-Sollzeit verteilt.<br>
Zusätzlich wird das Rüsten nur anhand der AG-Art berücksichtigt UND
wird eine Umspannzeit gebucht, so werden auch die reinen Maschinen-Laufzeiten ebenfalls, auch in der Stückzahl, mitgebucht.<br>
Für die Berechnung sind die Stückzahlen der Stückrückmeldung erforderlich, da anhand dieser Stückzahlen die Stückzeiten errechnet werden und so der Verteilungsfaktor ermittelt wird.

### Welchen Zeitverteilungstyp verwenden?

Zeitverteilung / Rüstzeiten, Stückzeiten / Umspannzeiten

Zeitverteilungstyp 2 macht nur Sinn, wenn man Rüsten und Lauf- und Umspannzeiten getrennt macht.<br>
Wenn das gemeinsam als Soll vorgegeben wird, ist Typ1 (oder 0) zu verwenden.<br>
D.h. auch, wenn man Rüst- und Umspannzeiten getrennt erfassen will, muss das Rüsten, die Laufzeit und die Umspannzeit ein eigener Artikel und ein eigener Arbeitsgang sein. Nur dann kann auch Zeitverteilungstyp 2 verwendet werden.<br>
<u>Hinweis:</u> Die Zeitverteilung wird als Definition für Auflösungsberechnung für die $PLUS-Buchung herangezogen.

### Ablauf der Buchungen
[Für eine ergänzende Beschreibung siehe dazu aktuell auch]( {{<relref "/fertigung/zeiterfassung/ze_terminal/moegliche_featureaufstellung/#gleichzeitige-buchung-auf-verschiedene-lose" >}} )