---
title: "Fertigung"
linkTitle: "Fertigung"
categories: ["Fertigung"]
tags: ["Fertigung"]
menu:
  main:
    weight: 20
weight: 20
description: >
  Die Module der Fertigung
---
Hier sind die mit der Herstellung eines Produktes verbundenen Module versammelt.<br>
Ob das nun die Herstellung eines Drehteiles ist, oder ein komplexes (elektronisches) Gerät oder auch eine köstliche Sachertorte. Alles will produziert werden und man muss natürlich auch wissen, welche Chargen, wo verbaut wurden und wer und wann dies und auf welcher Maschine gemacht wurde.

## [Personal]( {{<relref "/docs/stammdaten/personal"  >}} )
